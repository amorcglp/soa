<?php

/**
 * User: braz
 * Date: 08/03/19
 * Time: 11:10
 * Geração do log dos e-mails disparados pelo sistema do SOA
 * Parâmetros: data e hora do envio, variável enviado que tem o resultado do envio e a funcionalidade do sistema
 */

$dataHorarioEnvio           = isset($dataHorarioEnvio) ? $dataHorarioEnvio : null;//Formato: Fri, 3 May 2013 09:00:00 -0000
$enviado                    = isset($enviado) ? $enviado : null;
$funcionalidadeNoSistema    = isset($funcionalidadeNoSistema) ? $funcionalidadeNoSistema : null;

if($enviado!=null&&$funcionalidadeNoSistema!=null&&$dataHorarioEnvio!=null&&isset($mgClient)) {

    echo "<br>Entrou para gerar o log";

    /*
     *

    Tabela de erros do mailgun:

    200 Tudo funcionou como esperado

    400 Solicitação incorreta - geralmente faltando um parâmetro obrigatório

    401 Não autorizado - Nenhuma chave de API válida fornecida

    402 Falha na solicitação - os parâmetros foram válidos, mas a solicitação falhou 404 não encontrado - o item solicitado não existe

    413 Request Entity Too Large - O tamanho do anexo é muito grande

    500, 502, 503, 504 Erros do servidor - algo está errado no final da Mailgun

    (Observação: antes de coletar o retorno dos “eventos” do mailgun é preciso guardar o retorno do erro)

     */

    $codigoRetornoMailgun = $enviado->http_response_code;

    switch ($codigoRetornoMailgun)
    {
        case 200:
            $msgRetorno = "Tudo funcionou como esperado";
            break;
        case 400:
            $msgRetorno = "Solicitação incorreta - geralmente faltando um parâmetro obrigatório";
            break;
        case 401:
            $msgRetorno = "Não autorizado - Nenhuma chave de API válida fornecida";
            break;
        case 402:
            $msgRetorno = "Falha na solicitação - os parâmetros foram válidos, mas a solicitação falhou 404 não encontrado - o item solicitado não existe";
            break;
        case 413:
            $msgRetorno = "O tamanho do anexo é muito grande";
            break;
        default:
            $msgRetorno = "Erros do servidor - algo está errado no final da Mailgun";
            break;
    }

    echo "<br>msgRetornoMailgun:".$msgRetorno;

    /*
     * É preciso passar todas as informações usadas no disparo do e-mail se o código retornado do mailgun é 200
     */

    $domainOrigin = isset($domain) ? $domain : null;
    $emailFrom = isset($emailFrom) ? $emailFrom : null;
    $emailTo = isset($emailTo) ? $emailTo : null;
    $subject = isset($subject) ? $subject : null;
    $html = isset($html) ? $html : null;
    $seqCadast = isset($seqCadast) ? $seqCadast : null;

    if ($to != null) {
        $arrTo = explode("@", $to);
        $domainEmail = $arrTo[1];
    } else {
        $domainEmail = null;
    }

    echo "<br>domainEmail:".$domainEmail;

    /*
     * Acessar “GET /<domain>/log”
     */
    /*

    Exemplo de parametros

    :'begin'
    => 'Fri, 3 May 2013 09:00:00 -0000',
    :'ascending'
    => 'yes',
    :'limit'
    => 25,
    :'pretty'
    => 'yes',
    :'recipient' => 'joe@example.com'
    */

    $arr['begin'] = $dataHorarioEnvio;
    $arr['ascending'] = "yes";
    $arr['limit'] = 1;
    $arr['pretty'] = "yes";
    $arr['recipient'] = $emailTo;

    echo "<pre>";print_r($arr);

    $resultLog = $mgClient->get("$domain/events", $arr);

    echo "<pre>";print_r($resultLog);exit();

    //Inserir na tabela de log (mesmo obtendo a resposta do mailgun é preciso manter guardados os dados que foram enviados)

    /*
    require_once '../model/emailLogClass.php';
    $el = new emailLog();
    $el->domainOrigin;
    private $emailFrom;
    private $emailTo;
    private $subject;
    private $html;
    private $domainEmail;

    private $seqCadast;
    private $codigoRetorno;
    private $msgRetorno;

    private $tipoEvento; // pego com “GET /<domain>/events”
    private $descricaoEvento;
    private $jsonEvento;

    private $funcionalidadeNoSistema;

    /*
     * Identificar e Gravar resposta do email gun para o email enviado (código e descrição)
     * Tipos de Respostas do Mailgun (Eventos):
     *
     * Accepted - Aceito
     * Accepted (Routed) - Aceito (Roteado)
     * Delivered - Entregue
     * Failed (Permanent) - Falhou a entrega de forma permanente
     * Failed (Permanent, Delayed Bounce) - Falhou a entrega de forma permanente e atrasada
     * Failed (Temporary) - Falhou a entrega temporariamente
     * Opened - Aberto
     * Clicked - Clicado
     * Unsubscribed - Desinscrito da lista
     * Complained - Reclamado
     * Stored - Armazenado
     * Rejected - Rejeitado
     *
     * Será guardado o tipo de resposta (tipo_evento), descrição e o json do mailgun (json_resposta)
     */


}

