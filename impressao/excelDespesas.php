<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<?php
$mesAtual = isset($_REQUEST['mesAtual'])?$_REQUEST['mesAtual']:null;
$anoAtual = isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:null;
$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:null;

include_once("../model/despesaClass.php");
include_once("../lib/functions.php");

$arquivo = 'SOA-RELATORIO-FINANCEIRO-DESPESAS-'.$mesAtual.'-'.$anoAtual.'.xls';

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

$d = new Despesa();
?>
<table class="table table-striped table-bordered table-hover dataTables-example" id="editable2" >
    <thead>
        <tr>
            <td colspan="6"><b><?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);?></b></td>
        </tr>
        <tr>
            <td><b>Mês</b></td>
            <td><?php echo $mesAtual;?></td>
            <td><b>Ano</b></td>
            <td><?php echo $anoAtual;?></td>
            <td></td>
            
        </tr>
                        <tr id="linha0">
                                    <th width="80">Data</th>
                                    <th width="100">Descrição</th>
                                    <th width="100">Pago à</th>
                                    <th width="100">Valor R$</th>
                                    <th width="100">Categoria</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tabela">
                                <?php
                                $totalGeral=0;
                                $resultado = $d->listaDespesa($mesAtual,$anoAtual,$idOrganismoAfiliado);
                                $i=0;
                                if ($resultado) {
                                    foreach ($resultado as $vetor) {
                                        $i++;
                                ?>
                                <tr id="linha<?php echo $vetor['idDespesa'];?>">
                                    <td id="1td">
                                            <?php echo substr($vetor['dataDespesa'],8,2)."/".substr($vetor['dataDespesa'],5,2)."/".substr($vetor['dataDespesa'],0,4);?>
                                    </td>
                                    <td id="2td">
                                            <?php echo $vetor['descricaoDespesa'];?>                            
                                    </td>
                                    <td id="3td">
                                            <?php echo $vetor['pagoA'];?>
                                    </td>
                                    <td id="4td">
                                            <?php echo $vetor['valorDespesa'];?>
                                    </td>
                                    <td id="5td">
                                            <?php echo retornaCategoriaDespesa($vetor['categoriaDespesa']);?>
                                    </td>
                                    
                                </tr>
                                <?php 		}
                                }?>
                            </tbody>
                            <?php
                            if($resultado){
                                if(count($resultado)>1){
                                    $totalGeral = $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado);
                            ?>
                            <tr id="linhaTotal">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td align="right">
                                    <b>TOTAL GERAL</b>
                                </td>
                                <td>
                                    <div id="totalGeral">
                                        <?php echo number_format($totalGeral, 2, ',', '');?>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php }
                            }?>

                        </table>