<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    if(!class_exists("Ws"))
    {
        if(realpath('../../model/wsClass.php')){
                require '../../model/wsClass.php';
        }else{
                if(realpath('../model/wsClass.php')){
                        require '../model/wsClass.php';	
                }else{
                        require './model/wsClass.php';
                }
        } 
    }
    require_once '../lib/functions.php';
    
    //Tipo Função
    $str = isset($_REQUEST['str'])?$_REQUEST['str']:null;
    $str = substr($str,1,1000000000);
    $arrTipoFuncao = explode(",", $str);

    //Tipo Função
    $strFuncao = isset($_REQUEST['strFuncao'])?$_REQUEST['strFuncao']:null;
    $strFuncao = substr($strFuncao,1,1000000000);
    $arrFuncao = explode(",", $strFuncao);
    //echo "<pre>";print_r($arrFuncao);
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0 0 0 20px;
    text-align: justify;
    font-family: Arial;
    font-size: 14px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    </style>

    <?php 
    include_once('../model/organismoClass.php');
    $o = new organismo();
    if($_GET['idRegiao']!=0)
    {    
        $resultado = $o->listaOrganismo(null,null,null,null,$_GET['idRegiao'],null,null,null,null,null,null,1);//Filtrar por região
    }else{
        $resultado = $o->listaOrganismo(null,null,null,null,null,null,null,null,null,null,null,1);//Pegar todas as regiões
    }
    if($_GET['idOrganismoAfiliado']!="Todos")
    {    
        $resultado = $o->listaOrganismo(null,null,null,null,null,$_GET['idOrganismoAfiliado'],null,null,null,null,null,1);//Filtrar por OA
    }    
    //echo "<pre>";print_r($resultado);
    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                switch($vetor['classificacaoOrganismoAfiliado']){
                        case 1:
                                $classificacao =  "Loja";
                                break;
                        case 2:
                                $classificacao =  "Pronaos";
                                break;
                        case 3:
                                $classificacao =  "Capítulo";
                                break;
                        case 4:
                                $classificacao =  "Heptada";
                                break;
                        case 5:
                                $classificacao =  "Atrium";
                                break;
                }
                switch($vetor['tipoOrganismoAfiliado']){
                        case 1:
                                $tipo = "R+C";
                                break;
                        case 2:
                                $tipo = "TOM";
                                break;
                }
                switch ($vetor['paisOrganismoAfiliado'])
                {
                    case 1:
                        $siglaPais = "BR";
                        break;
                    case 2:
                        $siglaPais = "PT";
                        break;
                    case 3:
                        $siglaPais = "AO";
                        break;
                    case 4:
                        $siglaPais = "MZ";
                        break;
                    default :
                        $siglaPais = "BR";
                        break;
                }

                $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
                /*
                $logradouro = $vetor['enderecoOrganismoAfiliado'];
                $numero = $vetor['numeroOrganismoAfiliado'];
                $cep = $vetor['cepOrganismoAfiliado'];
                $cidade = $vetor['cidade'];
                $uf = $vetor['uf'];
                $telefone = $vetor['telefoneFixoOrganismoAfiliado']." ".$vetor['celularOrganismoAfiliado']." ".$vetor['outroTelefoneOrganismoAfiliado'];
                */

                switch ($_GET['tipo'])
                {
                        case 1:
                                $tipoRelatorioOficiais = "Atuantes";
                                $atuantes='S';
                                $naoAtuantes='N';
                                break;
                        case 2:
                                $tipoRelatorioOficiais = "Não Atuantes";
                                $atuantes='N';
                                $naoAtuantes='S';
                                break;
                        case 3:
                                $tipoRelatorioOficiais = "Atuantes e Não Atuantes";
                                $atuantes='S';
                                $naoAtuantes='S';
                                break;
                        default:
                                $tipoRelatorioOficiais = "Atuantes";
                                $atuantes='S';
                                $naoAtuantes='N';
                                break;
                }



                //Chamar webservice
                $ocultar_json=1;
                $siglaOA=strtoupper($siglaOrganismoAfiliado);
                //echo "SIGLA=>".$siglaOA;
                //echo "<br>==================================";



                //$seqFuncao=0;
                foreach($arrFuncao as $fun)
                {    
                    $seqFuncao = $fun;

                    $seqCadast 	= 0;

                    $server = 135;//PEGANDO DE PRODUÇÃO 
                    // Instancia a classe
                    $ws = new Ws($server);
                    // Nome do Método que deseja chamar
                    $method = 'RetornaRelacaoOficiais';

                    // Parametros que serão enviados à chamada
                    $params = array('CodUsuario' => 'lucianob',
                                                    'SeqCadast' => 0,
                                                    'SeqFuncao' => $seqFuncao,
                                                    'NomParcialFuncao' => '',
                                                    'NomLocaliOficial' => '',
                                                    'SigRegiaoBrasilOficial' => '',
                                                    'SigPaisOficial' => $siglaPais,
                                                    'SigOrganismoAfiliado' => $siglaOA,
                                                    'SigAgrupamentoRegiao' => '',
                                                    'IdeListarAtuantes' => $atuantes,
                                                    'IdeListarNaoAtuantes' => $naoAtuantes,
                                    'IdeTipoMembro' => '1'
                                            );

                    // Chamada do método
                    $return = $ws->callMethod($method, $params, 'lucianob');

                    $obj = json_decode(json_encode($return),true);
                    //echo "<pre>";print_r($obj);
                    ?>


                    <?php 
                    foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor)
                    {
                            if($vetor['fields']['fCodMembro']!=0)
                            {
                                //echo "<br>CodTipoFuncao=>".$vetor['fields']['fCodTipoFuncao'];
                                //echo "<br><pre>";print_r($arrTipoFuncao);
                                if(in_array($vetor['fields']['fCodTipoFuncao'],$arrTipoFuncao))
                                {  
                                    //echo "<br>CodFuncao=>".$vetor['fields']['fCodFuncao'];
                                    //echo "<br>total:";count($arrFuncao);
                                    if(in_array($vetor['fields']['fCodFuncao'],$arrFuncao))
                                    { 
                                        if($_REQUEST['terminoMandato']!=0)
                                        {
                                            $filtra=true;
                                        }else{
                                            $filtra=false;
                                        }  

                                        if($_REQUEST['terminoMandato']==0||substr($vetor['fields']['fDatTerminMandat'],0,4)==$_REQUEST['terminoMandato']&&$filtra==true)
                                        {
                                            
                                            $server2 = 135;//PEGANDO DE PRODUÇÃO 
                                            // Instancia a classe
                                            $ws2 = new Ws($server2);
                                            // Nome do Método que deseja chamar
                                            $method2 = 'RetornaDadosMembroPorSeqCadast';

                                            // Parametros que serão enviados à chamada
                                            $params2 = array('CodUsuario' => 'lucianob',
                                            'SeqCadast' => $vetor['fields']['fSeqCadast']);

                                            // Chamada do método
                                            $return2 = $ws->callMethod($method2, $params2, 'lucianob');

                                            $obj2 = json_decode(json_encode($return2),true);
                                            
                                            
                                            
                        
                    ?>
    <table border=0 width="100%">
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        <?php echo $obj2['result'][0]['fields']['fCodRosacruz'];?>
                    </td>    
            </tr>    
            <tr>
                    <td>
                        <b><?php 
                            echo $vetor['fields']['fNomClient'];
                            ?></b>
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        <?php 
                            if($obj2['result'][0]['fields']['fIdeEnderecoCorrespondencia']==1){echo $obj2['result'][0]['fields']['fNomLogradouro']." ".$obj2['result'][0]['fields']['fNumEndereco'];}else{ echo "Caixa Postal: ".$obj2['result'][0]['fields']['fNumCaixaPostal'];}
                            ?>
                    </td>    
            </tr>
            <tr>
                    <td>
                        <?php 
                            if($obj2['result'][0]['fields']['fIdeEnderecoCorrespondencia']==1){ echo $obj2['result'][0]['fields']['fNomBairro'];}else{ echo "&nbsp;";}
                            ?>
                    </td>    
            </tr>
            <tr>
                    <td>
                        <?php 
                            echo $obj2['result'][0]['fields']['fCodCepEndereco']." - ".$obj2['result'][0]['fields']['fNomCidade']." - ".$obj2['result'][0]['fields']['fSigUf'];
                            ?>
                    </td>    
            </tr>
            <?php
                                            
                                        }
                                        
                    }
                }
               ?> 
            </table>
  <div style="page-break-after: always"></div>
            
            <?php 
                                
                            
                            }
                    }
                }
            }
    }
}
?>