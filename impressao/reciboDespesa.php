<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once('../model/despesaClass.php');
    $r = new Despesa();
    $resultado = $r->buscarIdDespesa($_REQUEST['id']);
    $retorno = array();
    if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $retorno['idDespesa'] 						= $vetor['idDespesa'];
                    $data 										= date_create($vetor['dataDespesa']);
            $retorno['dataDespesa'] 					= date_format($data, 'd/m/Y');
            $retorno['descricaoDespesa'] 				= $vetor['descricaoDespesa'];
            $retorno['pagoA']							= $vetor['pagoA'];
            $retorno['valorDespesa']					= $vetor['valorDespesa'];
            $retorno['categoriaDespesa'] 				= $vetor['categoriaDespesa'];
            }
    }

    include_once('../model/organismoClass.php');
    $o = new organismo();
    $resultado2 = $o->buscaIdOrganismo($_REQUEST['idOrganismoAfiliado']);
    $retorno2 = array();
    if ($resultado2) {
            foreach($resultado2 as $vetor2)
            {
            switch($vetor2['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor2['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];

            }
    }

    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
            margin: 0;
            text-align: justify;
            font-family: Calibri;
            /* padding: 5px;
             font-family: Calibri; */
            font-size: 14px!important;
            color: #005678;
            border-collapse:collapse;
            border-color:#DCDCDC;
            max-width: 100%;
            border-spacing: 0;
    }

    p {
            -webkit-margin-before: 0em
    }

    .fonte-1{
            font-size: 1.4em;
            font-family: Arial;
            margin-bottom: 5px;
    }

    .fonte-2{
            font-size: 0.8em;
            font-family: Arial;
            margin-bottom: 20px;
    }

    .fonte-3{
            font-size: 0.9em;
            font-family: Arial;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    .linha-tracejada {
            border:1px dashed black;
            margin-top: 15px;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }
    .solado {width: 225px; height: 60px;}
    </style>

    <style media="print">
            .oculto {display: none;}
    </style>


    <div>
            <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    </div>
    <div style="border: 1px solid #191970; height: 175px; padding: 10px">
            <div style="width: 35%; float: left; height: 175px">
                    <div style="margin-top: 13px">
                            <center>
                                    <p class="solado">
                                            <img class="solado" src="../img/logo_sol_alado_azul.png">
                                    </p>
                            </center>
                            <center><div class="fonte-1">RECIBO Nº <?php echo str_pad($retorno['idDespesa'], 6, "0", STR_PAD_LEFT);?></div></center>
                            <center><div class="fonte-2">(ÚNICO, NOMINAL E INTRANSFERÍVEL)</div></center>
                            <center><p class="fonte-3"><b><?php echo $nomeOrganismo;?></b></p></center>
                    </div>
            </div>
            <div style="width: 60%; float: right; height: 175px; padding: 0px 10px 0px 15px;">
                    <p align="right">
                            Curitiba, <?php echo $retorno['dataDespesa'];?>.
                    </p>
                    <p>
                            <h3><b>Comprovante de Despesa</b></h3>
                    </p>
                    <p>
                            Foi pago à: <b><?php echo $retorno['pagoA'];?></b>, 
                            a importância abaixo.
                    </p>
                    <p>
                            TOTAL DO RECIBO: &nbsp;&nbsp;&nbsp;<?php echo $retorno['valorDespesa'];?>.
                            <?php if($retorno['descricaoDespesa']!=""){?>
                            Referente à: <?php echo $retorno['descricaoDespesa'];?>.
                            <?php }?>
                    </p>
                    <p>
                        <div>
                            <b>Carimbo e Assinatura:</b>&nbsp;&nbsp;&nbsp;__________________________________________.
                        </div>
                    </p>
            </div>
    </div>
    <hr class="linha-tracejada">

    <div style="border: 1px solid #191970; height: 175px; padding: 10px">
        <div style="width: 35%; float: left; height: 175px">
            <div style="margin-top: 13px">
                <center>
                    <p class="solado">
                        <img class="solado" src="../img/logo_sol_alado_azul.png">
                    </p>
                </center>
                <center><div class="fonte-1">RECIBO Nº <?php echo str_pad($retorno['idDespesa'], 6, "0", STR_PAD_LEFT);?></div></center>
                <center><div class="fonte-2">(ÚNICO, NOMINAL E INTRANSFERÍVEL)</div></center>
                <center><p class="fonte-3"><b><?php echo $nomeOrganismo;?></b></p></center>
            </div>
        </div>
        <div style="width: 60%; float: right; height: 175px; padding: 0px 10px 0px 15px;">
            <p align="right">
                Curitiba, <?php echo $retorno['dataDespesa'];?>.
            </p>
            <p>
            <h3><b>Comprovante de Despesa</b></h3>
            </p>
            <p>
                Foi pago à: <b><?php echo $retorno['pagoA'];?></b>,
                a importância abaixo.
            </p>
            <p>
                TOTAL DO RECIBO: &nbsp;&nbsp;&nbsp;<?php echo $retorno['valorDespesa'];?>.
                <?php if($retorno['descricaoDespesa']!=""){?>
                    Referente à: <?php echo $retorno['descricaoDespesa'];?>.
                <?php }?>
            </p>
            <p>
                <div>
                    <b>Carimbo e Assinatura:</b>&nbsp;&nbsp;&nbsp;__________________________________________.
                </div>
            </p>
        </div>
    </div>

<?php
}
?>