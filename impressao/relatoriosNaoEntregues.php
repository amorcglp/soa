<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    if(!class_exists("Ws"))
    {
        if(realpath('../../model/wsClass.php')){
                require '../../model/wsClass.php';
        }else{
                if(realpath('../model/wsClass.php')){
                        require '../model/wsClass.php';	
                }else{
                        require './model/wsClass.php';
                }
        } 
    }
    
    require('../lib/functions.php');
    
    $dataInicial = isset($_REQUEST['dataInicial'])?$_REQUEST['dataInicial']:null;
    $dataFinal = isset($_REQUEST['dataFinal'])?$_REQUEST['dataFinal']:null;
    
    $mesFinalInt = (int) substr($dataFinal,3,2);

    $mesInicio = (int) substr($dataInicial,3,2);
    $anoInicio = substr($dataInicial,6,4);

    $dtInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
    $dtFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);
    
    $anoInicial = substr($dataInicial,6,4);
    $anoFinal = substr($dataFinal,6,4);
    
    //Criar array de anos possíveis
    $arrAnos=array();
    for($i=$anoInicial;$i<=$anoFinal;$i++)
    {
        $arrAnos[] = $i;
    }

    $date1 = new DateTime($dtInicial);
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2);
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

    $total = $totalDeMeses+1;
    
    //Criar array de meses possíveis
    $arrMeses = array();
    $y=$anoInicial;
    $i=$mesInicio;

    for($k=0;$k<$total;$k++)
    {
        $arrMeses[] = mesExtensoPortugues($i)."/".$y;
        if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
        {
            $i=0;
            $y++;
        }
        $i++;
    }
    
    //Criar array de trimestres possíveis
    $arrTrimestres = array();
    $y=$anoInicial;
    $i= retornaTrimestre($mesInicio);
    $total=ceil ($total/3);

    for($k=0;$k<$total;$k++)
    {
        $arrTrimestres[] = $i."ºtrim./".$y;
        if($i==4)//Se mês impresso é Dezembro o próximo ano é diferente
        {
            $i=0;
            $y++;
        }
        $i++;
    }   
?>
<title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans" />
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    
    font-size: 16px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Open Sans;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Open Sans;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>
    <script src="../js/functions3.js"></script>
    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><h2>Relatórios Não Entregues</h2></center><br>
    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Organismo</th>
                                <th>Relatórios Não Entregues</th>
                                <th>Observações</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                        
set_time_limit(0);
//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );

$mesAtual 				= date('m');
$anoAtual 				= date('Y');

//Trimestre atual
if(date('m')==1||date('m')==2||date('m')==3)
{
    $trimestreAtual=1;
}
if(date('m')==4||date('m')==5||date('m')==6)
{
    $trimestreAtual=2;
}
if(date('m')==7||date('m')==8||date('m')==9)
{
    $trimestreAtual=3;
}
if(date('m')==10||date('m')==11||date('m')==12)
{
    $trimestreAtual=4;
}

include_once('../lib/functions.php');
include_once('../lib/phpmailer/class.phpmailer.php');
include_once('../model/regiaoRosacruzClass.php');
include_once('../model/organismoClass.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/ataReuniaoMensalAssinadaClass.php');
include_once('../model/ataPosseAssinadaClass.php');
include_once('../model/membrosRosacruzesAtivosClass.php');
include_once('../model/relatorioFinanceiroMensalClass.php');
include_once('../model/relatorioFinanceiroAnualClass.php');
include_once('../model/imovelControleClass.php');
include_once('../model/usuarioClass.php');
include_once('../model/agendaAtividadeClass.php');
include_once('../model/ataReuniaoMensalClass.php');
include_once('../model/financeiroMensalClass.php');
include_once('../model/relatorioClasseArtesaosClass.php');
include_once('../model/atividadeEstatutoMensalClass.php');
include_once('../model/columbaTrimestralClass.php');
include_once('../model/relatorioAtividadeTrimestralColumbaAssinadoClass.php');

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : $_SESSION['idOrganismoAfiliado'];

																				
							$organismo = new organismo();
                                                    
                                                    
                                                        if(isset($_REQUEST['regiao'])&&$_REQUEST['oa']=="")
                                                        {    
                                                            if($_REQUEST['regiao']!="TODAS"){ 
                                                                $resultadoOrg = $organismo->listaOrganismo(null,null,"siglaOrganismoAfiliado",null,$_REQUEST['regiao'],null,null,1);
                                                            }else{
                                                                $resultadoOrg = $organismo->listaOrganismo(null,null,"siglaOrganismoAfiliado",null,null,null,null,1);
                                                            }    
                                                        }else{
                                                            $resultadoOrg = $organismo->listaOrganismo(null,null,"siglaOrganismoAfiliado",null,null,$_REQUEST['oa'],null,1);
                                                        }
                                                        
							$naoCobrarDashboard =0;
							if($resultadoOrg)
							{
								foreach($resultadoOrg as $vetorOrg)
								{
                                                                    $naoCobrarDashboard = $vetorOrg['naoCobrarDashboard'];
									switch ($vetorOrg['classificacaoOrganismoAfiliado']) {
							            case 1:
							                $classificacao = "Loja";
							                break;
							            case 2:
							                $classificacao = "Pronaos";
							                break;
							            case 3:
							                $classificacao = "Capítulo";
							                break;
							            case 4:
							                $classificacao = "Heptada";
							                break;
							            case 5:
							                $classificacao = "Atrium";
							                break;
							        }
							        switch ($vetorOrg['tipoOrganismoAfiliado']) {
							            case 1:
							                $tipo = "R+C";
							                break;
							            case 2:
							                $tipo = "TOM";
							                break;
							        }
                                                                switch ($vetorOrg['paisOrganismoAfiliado'])
                                                                {
                                                                    case 1:
                                                                        $siglaPais = "BR";
                                                                        break;
                                                                    case 2:
                                                                        $siglaPais = "PT";
                                                                        break;
                                                                    case 3:
                                                                        $siglaPais = "AO";
                                                                        break;
                                                                    case 4:
                                                                        $siglaPais = "MZ";
                                                                        break;
                                                                    default :
                                                                        $siglaPais = "BR";
                                                                        break;
                                                                }
							
                                                                        $nomeOrganismo 			= $vetorOrg["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetorOrg["nomeOrganismoAfiliado"];
									$idOrganismoAfiliado 	= $vetorOrg['idOrganismoAfiliado'];
									$siglaOrganismo			= $vetorOrg['siglaOrganismoAfiliado'];
                                                                        $seqOrganismoAfiliado= $vetorOrg['seqCadast'];
									$total=0;//Contador de relatórios NÃO entregues
                                                                        
                                                                        
                                                                        $ocultar_json=1;
                                                                        $siglaOA = $siglaOrganismo;
                                                                        $pais = $siglaPais;
                                                                        //Consultar Webservice da Inovad
                                                                        include ('../js/ajax/retornaDadosOrganismo.php');

                                                                        $obj = json_decode(json_encode($return),true);
                                                                        //echo "<pre>";print_r($obj);

                                                                        $situacaoDB="";
                                                                        foreach ($obj['result'][0]['fields']['fArrayOA'] as $vetor)
                                                                        {
                                                                            $situacaoDB = $vetor['fields']['fSituacaoOa'];
                                                                            switch ($vetor['fields']['fSituacaoOa'])
                                                                            {
                                                                                case 'R':
                                                                                    $statusOrganismo = "Recesso";
                                                                                    break;
                                                                                case 'A':
                                                                                    $statusOrganismo = "Ativo";
                                                                                    break;
                                                                                case 'F':
                                                                                    $statusOrganismo = "Fechado";
                                                                                    break;
                                                                            }
                                                                        }
                                                                        
                                                                        if(($_REQUEST['situacao']!="TODOS"&&$situacaoDB==$_REQUEST['situacao'])||($_REQUEST['situacao']=="TODOS"))
									{
                                                                            //Encontrar Data Inicial com base no saldo inicial

                                                                            $si = new saldoInicial();
                                                                            $mesSaldoInicial = "01";
                                                                            $anoSaldoInicial = "2016";
                                                                            $trimestreSaldoInicial="1";
                                                                            $temSaldoInicial = 1;

                                                                            $resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
                                                                            $ativacao=0;
                                                                            if($resultado)
                                                                            {
                                                                                    $ativacao = count($resultado);
                                                                                    foreach($resultado as $vetor)
                                                                                    {
                                                                                            $mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
                                                                                            $anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);

                                                                                            //Trimestre atual
                                                                                            if($mesSaldoInicial==1||$mesSaldoInicial==2||$mesSaldoInicial==3)
                                                                                            {
                                                                                                $trimestreSaldoInicial=1;
                                                                                            }
                                                                                            if($mesSaldoInicial==4||$mesSaldoInicial==5||$mesSaldoInicial==6)
                                                                                            {
                                                                                                $trimestreSaldoInicial=2;
                                                                                            }
                                                                                            if($mesSaldoInicial==7||$mesSaldoInicial==8||$mesSaldoInicial==9)
                                                                                            {
                                                                                                $trimestreSaldoInicial=3;
                                                                                            }
                                                                                            if($mesSaldoInicial==10||$mesSaldoInicial==11||$mesSaldoInicial==12)
                                                                                            {
                                                                                                $trimestreSaldoInicial=4;
                                                                                            }
                                                                                    }
                                                                            }else{
                                                                                $temSaldoInicial = 0;
                                                                            }

                                                                            /**
                                                                            * Entrega da Agenda Anual do Organismo
                                                                            */
                                                                            if($_REQUEST['linha1']==1)
                                                                            {
                                                                                $ag = new AgendaAtividade();
                                                                                $cobraAgendaAnual=false;
                                                                                $totalAgendaAtividades=0;
                                                                                $resultado = $ag->listaAgendaAtividade($idOrganismoAfiliado,date('Y'));
                                                                                if (!$resultado && $temSaldoInicial == 1 && in_array(date('Y'),$arrAnos)) {
                                                                                    $cobraAgendaAnual=true;
                                                                                    $totalAgendaAtividades=1;
                                                                                    $total++;
                                                                                }
                                                                            }    
                                                                            /**
                                                                             * Entrega das Atas de Reunião Mensal Assinadas
                                                                             */

                                                                            if($_REQUEST['linha2']==1)
                                                                            {
                                                                                $arma = new ataReuniaoMensalAssinada();
                                                                                $arm = new ataReuniaoMensal();

                                                                                $mes = (int) $mesSaldoInicial;
                                                                                $ano = (int) $anoSaldoInicial;
                                                                                //echo "mes------>".$mes;
                                                                                //echo "ano------>".$ano;
                                                                                $totalAtaReuniaoMensal=0;
                                                                                $arrMesesNaoEntreguesAtaReuniaoMensal=array();
                                                                                while($ano<=$anoAtual)
                                                                                {
                                                                                        $resultado = $arma->listaAtaReuniaoMensalAssinada(null,$mes,$ano,$idOrganismoAfiliado);
                                                                                        if(!$resultado&&$temSaldoInicial==1&& in_array(mesExtensoPortugues($mes)."/".$ano,$arrMeses))
                                                                                        {
                                                                                            $resultado2 = $arm->listaAtaReuniaoMensal($mes,$ano,$idOrganismoAfiliado,true);
                                                                                            if (!$resultado2) {
                                                                                                $arrMesesNaoEntreguesAtaReuniaoMensal[] = mesExtensoPortugues($mes) . "/" . $ano;
                                                                                                $total++;
                                                                                                $totalAtaReuniaoMensal++;
                                                                                            }
                                                                                        }

                                                                                        if($mes==12&&$ano<=$anoAtual)
                                                                                        {
                                                                                                $ano++;
                                                                                                $mes=0;
                                                                                        }
                                                                                        $mes++;
                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                        {
                                                                                            $ano++;
                                                                                        }
                                                                                }
                                                                            }    
                                                                            /**
                                                                             * Entrega das Atas de Posse Assinadas
                                                                             */

                                                                            if($_REQUEST['linha3']==1)
                                                                            {
                                                                                $apa = new ataPosseAssinada();

                                                                                //$mes = (int) $mesSaldoInicial;
                                                                                $ano = (int) $anoSaldoInicial;
                                                                                //echo "mes------>".$mes;
                                                                                //echo "ano------>".$ano;
                                                                                $totalAtaPosse=0;
                                                                                $arrAnosNaoEntreguesAtaPosse=array();
                                                                                while($ano<$anoAtual||(($ano==$anoAtual)&&(date('m')>4)))
                                                                                {
                                                                                        $resultado = $apa->listaAtaPosseAssinada(null,null,$ano,$idOrganismoAfiliado);
                                                                                        if(!$resultado&&$temSaldoInicial==1&& in_array($ano,$arrAnos))
                                                                                        {
                                                                                                $arrAnosNaoEntreguesAtaPosse[]=$ano;
                                                                                                $total++;
                                                                                                $totalAtaPosse++;
                                                                                        }

                                                                                        $ano++;

                                                                                }
                                                                            }
                                                                            /**
                                                                             * Entrega do Relatório de Membros Ativos
                                                                             */
                                                                            if($_REQUEST['linha4']==1)
                                                                            {
                                                                                $mra	= new MembrosRosacruzesAtivos();

                                                                                $mes = (int) $mesSaldoInicial;
                                                                                $ano = (int) $anoSaldoInicial;
                                                                                //echo "mes------>".$mes;
                                                                                //echo "ano------>".$ano;
                                                                                $totalRelatorioMembrosAtivos=0;
                                                                                $arrMesesNaoEntreguesRelatorioMembrosAtivos=array();
                                                                                while($ano<=$anoAtual)
                                                                                {
                                                                                        $mResultado	= $mra->retornaMembrosRosacruzesAtivos($mes,$ano,$idOrganismoAfiliado);
                                                                                        if($mResultado['numeroAtualMembrosAtivos']==0&&$temSaldoInicial==1&& in_array(mesExtensoPortugues($mes)."/".$ano,$arrMeses))
                                                                                        {
                                                                                                $arrMesesNaoEntreguesRelatorioMembrosAtivos[]=mesExtensoPortugues($mes)."/".$ano;
                                                                                                $total++;
                                                                                                $totalRelatorioMembrosAtivos++;
                                                                                        }
                                                                                        if($mes==12&&$ano<=$anoAtual)
                                                                                        {
                                                                                                $ano++;
                                                                                                $mes=0;
                                                                                        }
                                                                                        $mes++;
                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                        {
                                                                                            $ano++;
                                                                                        }
                                                                                }
                                                                            }
                                                                            /**
                                                                             * Entrega do Relatório Financeiro Mensal
                                                                             */

                                                                            if($_REQUEST['linha5']==1)
                                                                            {
                                                                                $rfm = new RelatorioFinanceiroMensal();
                                                                                $fm = new FinanceiroMensal();

                                                                                $mes = (int) $mesSaldoInicial;
                                                                                $ano = (int) $anoSaldoInicial;
                                                                                //echo "mes------>".$mes;
                                                                                //echo "ano------>".$ano;
                                                                                $totalRelatorioFinanceiroMensal=0;
                                                                                $arrMesesNaoEntreguesRelatorioFinanceiroMensal=array();
                                                                                while($ano<=$anoAtual)
                                                                                {
                                                                                        $resultado = $rfm->listaRelatorioFinanceiroMensal($mes,$ano,$idOrganismoAfiliado);
                                                                                        if(!$resultado&&$temSaldoInicial==1&& in_array(mesExtensoPortugues($mes)."/".$ano,$arrMeses))
                                                                                        {
                                                                                            $resultado2 = $fm->listaFinanceiroMensal($mes,$ano,$idOrganismoAfiliado,true);
                                                                                            if (!$resultado2) {
                                                                                                $arrMesesNaoEntreguesRelatorioFinanceiroMensal[] = mesExtensoPortugues($mes) . "/" . $ano;
                                                                                                $total++;
                                                                                                $totalRelatorioFinanceiroMensal++;
                                                                                            }
                                                                                        }
                                                                                        if($mes==12&&$ano<=$anoAtual)
                                                                                        {
                                                                                                $ano++;
                                                                                                $mes=0;
                                                                                        }
                                                                                        $mes++;
                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                        {
                                                                                            $ano++;
                                                                                        }
                                                                                }
                                                                            }
                                                                            /**
                                                                             * Entrega do Relatório Financeiro Anual
                                                                             */

                                                                            if($_REQUEST['linha6']==1)
                                                                            {
                                                                                $rfa = new RelatorioFinanceiroAnual();

                                                                                //$mes = (int) $mesSaldoInicial;
                                                                                $ano = (int) $anoSaldoInicial;
                                                                                //echo "mes------>".$mes;
                                                                                //echo "ano------>".$ano;
                                                                                $totalRelatorioFinanceiroAnual=0;
                                                                                $arrAnosNaoEntreguesRelatorioFinanceiroAnual=array();
                                                                                while($ano<$anoAtual)
                                                                                {
                                                                                        $resultado = $rfa->listaRelatorioFinanceiroAnual($ano,$idOrganismoAfiliado);
                                                                                        if(!$resultado&&$temSaldoInicial==1&& in_array($ano,$arrAnos))
                                                                                        {
                                                                                                $arrAnosNaoEntreguesRelatorioFinanceiroAnual[]=$ano;
                                                                                                $total++;
                                                                                                $totalRelatorioFinanceiroAnual++;
                                                                                        }

                                                                                        $ano++;


                                                                                }
                                                                            }
                                                                            /**
                                                                             * Entrega do Relatório de Imóveis Anual
                                                                             */

                                                                            if($_REQUEST['linha7']==1)
                                                                            {
                                                                                $ic = new imovelControle();

                                                                                //$mes = (int) $mesSaldoInicial;
                                                                                $ano = (int) $anoSaldoInicial;
                                                                                //echo "mes------>".$mes;
                                                                                //echo "ano------>".$ano;
                                                                                $totalRelatorioImovelAnual=0;
                                                                                $arrAnosNaoEntreguesRelatorioImovel=array();
                                                                                while($ano<=$anoAtual)
                                                                                {
                                                                                        $resultado = $ic->listaImovelControle($idOrganismoAfiliado,$ano);
                                                                                        if(!$resultado&&$temSaldoInicial==1&& in_array($ano,$arrAnos))
                                                                                        {
                                                                                                        $arrAnosNaoEntreguesRelatorioImovel[]=$ano;
                                                                                                        $total++;
                                                                                                        $totalRelatorioImovelAnual++;
                                                                                        }

                                                                                        $ano++;

                                                                                }
                                                                            }
                                                                            //Verificar se oa tem Mestre da Classe dos Artesãos
                                                                            /*
                                                                            $ocultar_json=1;
                                                                            $naoAtuantes='N';
                                                                            $atuantes='S';
                                                                            $siglaOA=$siglaOrganismo;
                                                                            $seqFuncao='315';//Mestre da Classe dos Artesãos
                                                                            include 'js/ajax/retornaFuncaoMembro.php';
                                                                            $obj = json_decode(json_encode($return),true);
                                                                            $temMestreClasseArtesaos=0;
                                                                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                                                                            {
                                                                                $temMestreClasseArtesaos=1;

                                                                                /**
                                                                                * Entrega dos Relatórios da Classe dos Artesãos assinados
                                                                                */
                                                                                /*
                                                                               include_once('model/relatorioClasseArtesaosAssinadoClass.php');
                                                                               $rcaa = new relatorioClasseArtesaosAssinado();

                                                                               $mes = (int) $mesSaldoInicial;
                                                                               $ano = (int) $anoSaldoInicial;
                                                                               //echo "mes------>".$mes;
                                                                               //echo "ano------>".$ano;
                                                                               $totalRelatorioClasseArtesaosMensal=0;
                                                                               $arrMesesNaoEntreguesRelatorioClasseArtesaosMensal=array();
                                                                               while($mes<$mesAtual&&$ano<=$anoAtual)
                                                                               {
                                                                                       $resultado = $rcaa->listaRelatorioClasseArtesaosAssinado(null,$idOrganismoAfiliado,$mes,$ano);
                                                                                       if(!$resultado&&$temSaldoInicial==1&&$mes!=1&&$mes!=12)//Dispensar de eles entregarem relatorio em janeiro e dezembro
                                                                                       {
                                                                                               $arrMesesNaoEntreguesRelatorioClasseArtesaosMensal[]=mesExtensoPortugues($mes)."/".$ano; 
                                                                                               $total++;
                                                                                               $totalRelatorioClasseArtesaosMensal++;
                                                                                       }
                                                                                       if($mes==12&&$ano<=$anoAtual)
                                                                                       {
                                                                                               $ano++;
                                                                                               $mes=0;
                                                                                       }
                                                                                       $mes++;
                                                                               }
                                                                            }
                                                                            */
                                                                            /**
                                                                             * Entrega do Relatório de Atividades Mensal
                                                                             */
                                                                            if($_REQUEST['linha8']==1)
                                                                            {
                                                                                include_once('../model/relatorioAtividadeMensalClass.php');
                                                                                $ram = new RelatorioAtividadeMensal();
                                                                                $aem = new atividadeEstatutoMensal();

                                                                                $mes = (int) $mesSaldoInicial;
                                                                                $ano = (int) $anoSaldoInicial;
                                                                                //echo "mes------>".$mes;
                                                                                //echo "ano------>".$ano;
                                                                                $totalRelatorioAtividadeMensal=0;
                                                                                $arrMesesNaoEntreguesRelatorioAtividadeMensal=array();
                                                                                while($ano<=$anoAtual)
                                                                                {
                                                                                        $resultado = $ram->listaRelatorioAtividadeMensal($mes,$ano,$idOrganismoAfiliado);
                                                                                        if(!$resultado&&$temSaldoInicial==1&& in_array(mesExtensoPortugues($mes)."/".$ano,$arrMeses))
                                                                                        {
                                                                                            $resultado2 = $aem->listaAtividadeMensal($mes,$ano,$idOrganismoAfiliado,true);
                                                                                            if (!$resultado2) {
                                                                                                $arrMesesNaoEntreguesRelatorioAtividadeMensal[] = mesExtensoPortugues($mes) . "/" . $ano;
                                                                                                $total++;
                                                                                                $totalRelatorioAtividadeMensal++;
                                                                                            }
                                                                                        }
                                                                                        if($mes==12&&$ano<=$anoAtual)
                                                                                        {
                                                                                                $ano++;
                                                                                                $mes=0;
                                                                                        }
                                                                                        $mes++;
                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                        {
                                                                                            $ano++;
                                                                                        }   
                                                                                }
                                                                            }    
                                                                            //Verificar se oa tem Coordenadora de Columbas
                                                                            if($_REQUEST['linha9']==1)
                                                                            {
                                                                                $ocultar_json=1;
                                                                                $naoAtuantes='N';
                                                                                $atuantes='S';
                                                                                $siglaOA=$siglaOrganismo;
                                                                                $seqFuncao='609';//Orientadora de Columbas
                                                                                include '../js/ajax/retornaFuncaoMembro.php';
                                                                                $obj = json_decode(json_encode($return),true);
                                                                                $temCoordenadoraColumbas=0;
                                                                                if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                                                                                {
                                                                                    $temCoordenadoraColumbas=1;

                                                                                    /**
                                                                                    * Entrega dos Relatórios das Columbas Assinados
                                                                                    */
                                                                                    /*
                                                                                   include_once('model/relatorioTrimestralColumbaAssinadoClass.php');
                                                                                   $r = new RelatorioTrimestralColumbaAssinado();

                                                                                   $mes = (int) $mesSaldoInicial;
                                                                                   $ano = (int) $anoSaldoInicial;
                                                                                   $trimestre = (int) $trimestreSaldoInicial;
                                                                                   //echo "mes------>".$mes;
                                                                                   //echo "ano------>".$ano;
                                                                                   $totalRelatorioTrimestralColumba=0;
                                                                                   $arrMesesNaoEntreguesRelatorioTrimestralColumba=array();
                                                                                   while($trimestre<=$trimestreAtual&&$ano<=$anoAtual)
                                                                                   {
                                                                                           $resultado = $r->listaRelatorioTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
                                                                                           if(!$resultado&&$temSaldoInicial==1)
                                                                                           {
                                                                                                    $arrMesesNaoEntreguesRelatorioTrimestralColumba[]=$trimestre."º/".$ano;
                                                                                                    $total++;
                                                                                                    $totalRelatorioTrimestralColumba++;
                                                                                           }
                                                                                           if($trimestre==4&&$ano<=$anoAtual)
                                                                                           {
                                                                                                   $ano++;
                                                                                                   $trimestre=0;
                                                                                           }
                                                                                           $trimestre++;
                                                                                   }
                                                                                   */
                                                                                   /**
                                                                                    * Entrega dos Relatórios Atividades das Columbas Assinados
                                                                                    */

                                                                                   $r = new RelatorioAtividadeTrimestralColumbaAssinado();

                                                                                   $ct = new columbaTrimestral();

                                                                                   $mes = (int) $mesSaldoInicial;
                                                                                   $ano = (int) $anoSaldoInicial;
                                                                                   $trimestre = (int) $trimestreSaldoInicial;
                                                                                   //echo "mes------>".$mes;
                                                                                   //echo "ano------>".$ano;
                                                                                   $totalRelatorioAtividadeTrimestralColumba=0;
                                                                                   $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba=array();
                                                                                   while($trimestre<=$trimestreAtual&&$ano<=$anoAtual)
                                                                                   {
                                                                                           $resultado = $r->listaRelatorioAtividadeTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
                                                                                           if(!$resultado&&$temSaldoInicial==1&& in_array($trimestre."ºtrim./".$ano,$arrTrimestres))
                                                                                           {
                                                                                               $resultado2 = $ct->listaColumbaTrimestral($trimestre,$ano,$idOrganismoAfiliado,true);
                                                                                               if (!$resultado2) {
                                                                                                   $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba[] = $trimestre . "ºtrim./" . $ano;
                                                                                                   $total++;
                                                                                                   $totalRelatorioAtividadeTrimestralColumba++;
                                                                                               }
                                                                                           }
                                                                                           if($trimestre==4&&$ano<=$anoAtual)
                                                                                           {
                                                                                                   $ano++;
                                                                                                   $trimestre=0;
                                                                                           }
                                                                                           $trimestre++;
                                                                                   }
                                                                                }   
                                                                            }    
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $nomeOrganismo; ?>
                                            </td>
                                            <td>
                                                <?php 

                                                    if($ativacao==1)
                                                                                                    {
                                                                                                            echo $total;
                                                                                                    }else{
                                                                                                            echo "Módulo Financeiro não iniciado";
                                                                                                    } 
                                                                                            ?>
                                            </td>
                                            <td>
                                                <?php 
                                                if($naoCobrarDashboard==0)
                                                { 
                                                    if($ativacao==1)
                                                    {
                                                                                                if($_REQUEST['linha1']==1)
                                                                                                {    
                                                                                                    echo 'Agenda Anual de Atividades para o Portal: '.$totalAgendaAtividades;
                                                                                                    if ($cobraAgendaAnual) 
                                                                                                    { 
                                                                                                            echo ' ('.date('Y').')';
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha2']==1)
                                                                                                {
                                                                                                    echo '<br>Atas de Reunião Mensal: '.$totalAtaReuniaoMensal;
                                                                                                    if(count($arrMesesNaoEntreguesAtaReuniaoMensal)>0)
                                                                                                    { 
                                                                                                            echo ' ('.implode(", ",$arrMesesNaoEntreguesAtaReuniaoMensal).')';
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha3']==1)
                                                                                                {
                                                                                                    echo '<br>Atas de Posse: '.$totalAtaPosse;
                                                                                                    if(count($arrAnosNaoEntreguesAtaPosse)>0) 
                                                                                                    {
                                                                                                            echo ' ('.implode(", ",$arrAnosNaoEntreguesAtaPosse).')';
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha4']==1)
                                                                                                {
                                                                                                    echo '<br>Relatórios Membros Ativos: '.$totalRelatorioMembrosAtivos;
                                                                                                    if(count($arrMesesNaoEntreguesRelatorioMembrosAtivos)>0) 
                                                                                                    {
                                                                                                            echo ' ('.implode(", ",$arrMesesNaoEntreguesRelatorioMembrosAtivos).')';
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha5']==1)
                                                                                                {
                                                                                                    echo '<br>Relatórios Financ. Mensais: '.$totalRelatorioFinanceiroMensal;
                                                                                                    if(count($arrMesesNaoEntreguesRelatorioFinanceiroMensal)>0) 
                                                                                                    {
                                                                                                            echo ' ('.implode(", ",$arrMesesNaoEntreguesRelatorioFinanceiroMensal).')';
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha6']==1)
                                                                                                {
                                                                                                    echo '<br>Relatórios Financ. Anuais: '.$totalRelatorioFinanceiroAnual;
                                                                                                    if(count($arrAnosNaoEntreguesRelatorioFinanceiroAnual)>0) 
                                                                                                    {
                                                                                                            echo ' ('.implode(", ",$arrAnosNaoEntreguesRelatorioFinanceiroAnual).')';
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha7']==1)
                                                                                                {
                                                                                                    echo '<br>Relatórios Imóveis Anuais: '.$totalRelatorioImovelAnual;
                                                                                                    if(count($arrAnosNaoEntreguesRelatorioImovel)>0) 
                                                                                                    {
                                                                                                            echo ' ('.implode(", ",$arrAnosNaoEntreguesRelatorioImovel).')';
                                                                                                    }
                                                                                                }
                                                                                                    /*
                                                                                                    if($temMestreClasseArtesaos==1)
                                                                                                    {    
                                                                                                        echo "<br>Rel. Classe dos Artesãos: ".$totalRelatorioClasseArtesaosMensal;
                                                                                                        if(count($arrMesesNaoEntreguesRelatorioClasseArtesaosMensal)>0) 
                                                                                                        {
                                                                                                                echo ' ('.implode(", ",$arrMesesNaoEntreguesRelatorioClasseArtesaosMensal).')';
                                                                                                        }
                                                                                                    }
                                                                                                     */
                                                                                                if($_REQUEST['linha8']==1)
                                                                                                {
                                                                                                    echo "<br>Rel. de Atividades: ".$totalRelatorioAtividadeMensal;
                                                                                                    if(count($arrMesesNaoEntreguesRelatorioAtividadeMensal)>0) 
                                                                                                    {
                                                                                                            echo ' ('.implode(", ",$arrMesesNaoEntreguesRelatorioAtividadeMensal).')';
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha9']==1)
                                                                                                {
                                                                                                    if($temCoordenadoraColumbas==1&&$classificacao!="Pronaos")
                                                                                                    {    
                                                                                                        /*
                                                                                                        echo "<br>Rel. Trim. Columbas: ".$totalRelatorioTrimestralColumba;
                                                                                                        if(count($arrMesesNaoEntreguesRelatorioTrimestralColumba)>0) 
                                                                                                        {
                                                                                                                echo ' ('.implode(", ",$arrMesesNaoEntreguesRelatorioTrimestralColumba).')';
                                                                                                        }
                                                                                                        */
                                                                                                        echo "<br>Rel. Trim. Ativ. Columbas: ".$totalRelatorioAtividadeTrimestralColumba;
                                                                                                        if(count($arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba)>0) 
                                                                                                        {
                                                                                                                echo ' ('.implode(", ",$arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba).')';
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                                if($_REQUEST['linha10']==1)
                                                                                                {
                                                                                                    //Retornar Status do OA vindo do ORCZ
                                                                                                    echo "<br>Status do Organismo: ".$statusOrganismo; 
                                                                                                }
                                                                                    
                                                }else{
                                                    echo "Módulo Financeiro não iniciado";
                                                }
                                            }else{
                                                echo "Para esse organismo não devem ser cobrados relatórios";
                                            }     
                                            ?>
                                        </td>
                                </tr>
                            <?php
                                                                        }
                            }
                        }
		
                        ?>
                        </tbody>
                    </table>
<?php
}
?>