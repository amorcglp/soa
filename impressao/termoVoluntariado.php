<?php

if(realpath('../../webservice/wsInovad.php')){
    include '../../webservice/wsInovad.php';
}else{
    if(realpath('../webservice/wsInovad.php')){
        include '../webservice/wsInovad.php';
    }else{
        include './webservice/wsInovad.php';
    }
}

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

//if($tokenLiberado)
//{

    include_once("../lib/functions.php");

    $idTermoVoluntariado = isset($_REQUEST['idTermoVoluntariado'])?$_REQUEST['idTermoVoluntariado']:null;

    include_once("../controller/termoVoluntariadoController.php");
    $t = new termoVoluntariadoController();
    $dados = $t->buscaTermoVoluntariado($idTermoVoluntariado);

    //Procurar informações sobre o OA em que o voluntário irá exercer seu mandato
    include_once("../model/organismoClass.php");
    $o = new organismo();
    $resultado = $o->listaOrganismo(null,null,null,null,null,$dados->getFk_idOrganismoAfiliado());
    if($resultado)
    {
            foreach ($resultado as $vetor)
            {


                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    $logradouroOrganismoAfiliado = $vetor['enderecoOrganismoAfiliado'];

                    $siglaOa = $vetor['siglaOrganismoAfiliado'];

                    $numeroOrganismoAfiliado = $vetor['numeroOrganismoAfiliado'];

                    $cidadeOrganismoAfiliado = $vetor['cidade'];

                    $estadoOrganismoAfiliado = $vetor['estado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    $tipo2 = "Rosacruz";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    $tipo2= "Martinista";
                                    break;
                    }
                    //echo "sigla:".$siglaOa;
                    //PEGAR ENDEREÇO OA
                    $logradouroOrganismoAfiliado="";
                    $numeroOrganismoAfiliado="";
                    $cidadeOrganismoAfiliado="";
                    $estadoOrganismoAfiliado="";
                    $vars3 = array('sig_orgafi' => $siglaOa);
                    $resposta3 = json_decode(json_encode(restAmorc("oa",$vars3)), true);
                    //echo "teste depois da chamada do webservice";
                    $obj3 = json_decode(json_encode($resposta3), true);
                    //echo "<pre>";print_r($obj3);exit();

                    if (isset($obj3['data'][0]['nom_client'])) {
                        $logradouroOrganismoAfiliado = $obj3['data'][0]['des_lograd'];
                        $numeroOrganismoAfiliado= $obj3['data'][0]['num_endere'];
                        $cidadeOrganismoAfiliado= $obj3['data'][0]['nom_locali'];
                        $estadoOrganismoAfiliado= retornaEstado($obj3['data'][0]['sig_uf']);
                    }else{
                        $status=0;
                    }
            }
    }

    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 16px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>TERMO DE ADESÃO AO SERVIÇO VOLUNTÁRIO PARA ORGANISMOS AFILIADOS</b></p></center>
    <br>
    <p>
        Por este instrumento particular, de um lado a(o) <b><?php echo $classificacao;?> Rosacruz <?php echo $nomeOrganismoAfiliado;?>, 
        AMORC</b>, organização mística, iniciática, fraternal, de caráter e de atividades templárias, beneficente, caritativa, educacional, 
        cultural e científica, sem fins econômicos, com sede na <b><?php echo $logradouroOrganismoAfiliado;?></b>, nº <b><?php echo $numeroOrganismoAfiliado;?></b>, município de 
        <b><?php echo $cidadeOrganismoAfiliado;?></b>, estado de(o) <b><?php echo $estadoOrganismoAfiliado;?></b>, adiante sempre denominada(o)
        <b><?php echo $classificacao;?></b>, neste ato representada por seu Mestre, e de outro lado o(a) Sr.(a):<br>
        Nome do voluntário(a): <b><?php echo $dados->getNomeVoluntario();?></b><br>
        Inscrito na AMORC sob nº: <b><?php echo $dados->getCodigoAfiliacao();?></b> CPF: <b><?php echo $dados->getCpf();?></b> RG: <b><?php echo $dados->getRg();?></b><br>
        Profissão: <b><?php echo $dados->getProfissao();?></b> Nacionalidade: <b><?php echo $dados->getNacionalidade();?></b> E-mail: <b><?php echo $dados->getEmail();?></b><br>
        Endereço: <b><?php echo $dados->getLogradouro();?></b>, nº <b><?php echo $dados->getNumero();?></b> <b><?php echo $dados->getComplemento();?> <?php echo $dados->getBairro();?></b><br>
        CEP: <b><?php echo $dados->getCep();?></b> Cidade: <b><?php echo $dados->getCidade();?></b> UF: <b><?php echo $dados->getUf();?></b> Fones(s): <b><?php echo $dados->getTelefoneResidencial();?> <?php echo $dados->getTelefoneComercial();?> <?php echo $dados->getCelular();?></b><br>
        de agora em diante denominado(a) VOLUNTÁRIO(A), têm entre si ajustado o presente termo, que será regido pelas cláusulas e condições a seguir estipuladas que, reciprocamente, aceitam e outorgam a saber: <br>
        <b>CLÁUSULA 1ª</b> - O presente termo tem por objetivo a prestação de serviço voluntário previsto na Lei nº 9.608/98, cujo conteúdo declaram as partes conhecer, que não gera vínculo empregatício, nem obrigações de natureza trabalhista, previdenciária ou afim.<br>
        <b>CLÁUSULA 2ª</b> - O(A) VOLUNTÁRIO(A) aceita atuar como Voluntário nos termos do presente Termo de Adesão, e conforme as seguintes especificações:<br>
        Trabalho voluntário na área/setor de: <b><?php echo $dados->getAreaTrabalhoVoluntario();?></b>, período: <b><?php echo $dados->getAnoInicial();?></b> à <b><?php echo $dados->getAnoFinal();?></b><br>
        Tarefa específica: <b><?php echo $dados->getTarefaEspecifica();?></b><br>
        Dia(s) da semana do voluntariado: <b><?php echo $dados->getDiasSemanaVoluntariado();?></b><br>
        <b>CLÁUSULA 3ª</b> - O(A) VOLUNTÁRIO(A), nada receberá a título de remuneração pelos préstimos despendidos, considerando mesmo que tais préstimos são de cunho essencialmente altruístico.<br>
        <b>CLÁUSULA 4ª</b> - O(A) VOLUNTÁRIO(A) não está autorizado(a) a efetuar despesas em nome da(o) <b><?php echo $classificacao;?></b>, nem representá-la em qualquer hipótese, salvo por determinação expressa e formal de seus dirigentes.<br>
        <b>CLÁUSULA 5ª</b> - O(A) VOLUNTÁRIO(A) declara conhecer e compromete-se a cumprir fielmente o estatuto da Antiga e Mística Ordem Rosae Crucis - AMORC, o Acordo de Afiliação, o estatuto da(o) <b><?php echo $classificacao;?></b>, o Manual Administrativo e Ritualístico para Organismos Afiliados e o regimento interno.<br>
        E, por estarem justas e acertadas, formalizam as partes o presente termo assinado em 2 (duas) vias de igual teor e na presença de 2 (duas) testemunhas.<br>
    <center><?php echo $cidadeOrganismoAfiliado;?>, <?php echo substr($dados->getDataTermoVoluntariado(),0,2);?> de <?php echo mesExtensoPortugues(substr($dados->getDataTermoVoluntariado(),3,2));?> de <?php echo substr($dados->getDataTermoVoluntariado(),6,4);?></center><br>
    </p>
    <br><br>
    <center>
            <table cellpading="4">
                    <tr>
                            <td><center>___________________________________________</center></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><center>___________________________________________</center></td>
                    </tr>
                    <tr>
                            <td><center>Assinatura do(a) Voluntário(a)</center></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><center>Mestre do(a) <?php echo $classificacao;?></center></td>
                    </tr>
            </table>
    </center>
    <br><br>
    Testemunhas:<br><br>
    <center>
            <table cellpading="4">
                    <tr>
                            <td><center>___________________________________________</center></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><center>___________________________________________</center></td>
                    </tr>
                    <tr>
                            <td><center>Nome / CPF</center></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><center>Nome / CPF</center></td>
                    </tr>
            </table>
    </center>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div style="page-break-after: always;"></div>

    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>LEI DO VOLUNTARIADO</b></p></center>
    <br>
    <p>
        Lei n.º 9.608, de 18 de fevereiro de 1998<br>
        Dispõe sobre o serviço voluntário e dá outras providências.<br><br>
        <b>O PRESIDENTE DA REPÚBLICA</b>
        Faço saber que o Congresso Nacional decreta e eu sanciono a seguinte Lei:<br><br>
        <b>Art. 1.</b> Considera-se serviço voluntário, para fins desta Lei, a atividade não remunerada, prestada por
    pessoa física a entidade pública de qualquer natureza ou instituição privada de fins não lucrativos, que tenha
    objetivos cívicos, culturais, educacionais, científicos, recreativos ou de assistência social, inclusive, mutualidade.<br>
    <b>Parágrafo único:</b> O serviço voluntário não gera vínculo empregatício nem obrigação de natureza trabalhista,
    previdenciária ou afim.<br><br>
    <b>Art. 2.</b> O serviço voluntário será exercido mediante a celebração de termo de adesão entre a entidade,
    pública ou privada, e o prestador do serviço voluntário, dele devendo constar o objeto e as condições do seu
    exercício.<br><br>
    <b>Art. 3.</b> O prestador do serviço voluntário poderá ser ressarcido pelas despesas que comprovadamente
    realizar no desempenho das atividades voluntárias.<br>
    <b>Parágrafo único:</b> As despesas a serem ressarcidas deverão estar expressamente autorizadas pela entidade
    a que for prestado o serviço voluntário.<br><br>
    <b>Art. 4.</b> Esta Lei entra em vigor na data de sua publicação.<br><br>
    <b>Art. 5.</b> Revogam-se as disposições em contrário.<br><br>
    Brasília, 18 de fevereiro de 1998; 177 da Independência e 110 da República.<br><br>
    FERNANDO HENRIQUE CARDOSO<br>
    Paulo Paiva<br><br>
    <div style="text-align: right">(Publicado no Diário Oficial da União, de 19/02/98)</div>
    </p>
<?php
//}
?>