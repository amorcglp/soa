<?php

/*
 * Token
 */

foreach(['../../sec/token.php', '../sec/token.php', './sec/token.php'] as $path) {
  if(realpath($path)) {
      require_once $path;
      break;
  }
}

$db = include('../firebase.php');

if($tokenLiberado)
{

    $mesAtual 							= null;
    $anoAtual 							= isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:null;
    $idOrganismoAfiliado 				= isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:null;
    //$saldoAnosAnteriores				= isset($_REQUEST['saldoAnosAnteriores'])?$_REQUEST['saldoAnosAnteriores']:0;
    $numeroMembrosAtivosAnterior		= isset($_REQUEST['numeroMembrosAtivosAnterior'])?$_REQUEST['numeroMembrosAtivosAnterior']:0;

    include_once('../lib/functions.php');
    include_once('../model/organismoClass.php');
    include_once('../model/recebimentoClass.php');
    include_once('../model/despesaClass.php');
    include_once('../model/rendimentoClass.php');
    include_once('../model/membrosRosacruzesAtivosClass.php');
    include_once('../model/dividaClass.php');
    include_once('../model/quitacaoDividaClass.php');
//    include_once('../model/saldoFinanceiroClass.php');

    $r                          = new Recebimento();
    $d                          = new Despesa();
    $rendimento                 = new Rendimento();
    $membrosRosacruzesAtivos    = new MembrosRosacruzesAtivos();
    $divida                     = new Divida();
    $quitacaoDivida             = new QuitacaoDivida();
    $o                          = new organismo();
//    $sf                         = new saldoFinanceiro();

    $resultado = $o->buscaIdOrganismo($idOrganismoAfiliado);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
                    $nomeOrganismoAfiliado 	= $vetor['nomeOrganismoAfiliado'];
                    $classificacaoOA		= $vetor['classificacaoOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }
                    switch ($vetor['paisOrganismoAfiliado'])
                    {
                        case 1:
                            $siglaPais = "BR";
                            break;
                        case 2:
                            $siglaPais = "PT";
                            break;
                        case 3:
                            $siglaPais = "AO";
                            break;
                        case 4:
                            $siglaPais = "MZ";
                            break;
                        default :
                            $siglaPais = "BR";
                            break;
                    }
            }
    }

    $saldoAnosAnteriores=0;
    $anoAnterior = $anoAtual-1;
    $mesAnterior = 12;

    include_once('../model/saldoInicialClass.php');
    $si = new saldoInicial();

    $mesSaldoInicial = $si->getMesSaldoInicial($idOrganismoAfiliado);
    $anoSaldoInicial = $si->getAnoSaldoInicial($idOrganismoAfiliado);
    $valorSaldoInicial = $si->getValorSaldoInicial($idOrganismoAfiliado);

    if($anoAnterior<$anoSaldoInicial) {
        $saldoAnosAnteriores = $valorSaldoInicial;
    } else {

//        $balance = $db->collection("balances/" . $idOrganismoAfiliado . "/balance");
//        $queryBalance = $balance->documents();
//        foreach ($queryBalance as $documentBalance) {
//            if ($documentBalance->exists()) {
//                $dataBalance = $documentBalance->data();
//                if ($dataBalance['year'] == $anoAnterior && $dataBalance['month'] == $mesAnterior)
//                    $saldoAnosAnteriores = $dataBalance['previousBalance'];
//
//            }
//        }

        $dataSaldoInicial                   = $si->getDataCompletaSaldoInicial($idOrganismoAfiliado);
        $totalRecebimentoAteMesAnterior     = $r->totalRecebimentosAteMesAnterior($idOrganismoAfiliado, $dataSaldoInicial, $anoAnterior, $mesAnterior);
        $totalDespesaAteMesAnterior         = $d->totalDespesasAteMesAnterior($idOrganismoAfiliado, $dataSaldoInicial, $anoAnterior, $mesAnterior);
        $saldoAnosAnteriores               = (($totalRecebimentoAteMesAnterior - $totalDespesaAteMesAnterior) + $valorSaldoInicial);

    }

    //echo "SA".$saldoAnosAnteriores;

    /**
     * Cálculo Entradas
     */
    $mensalidades 				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,1);
    $mensalidadesAtriumMartinista           = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,15);
    $totalEntradaMensalidades		= $mensalidades+$mensalidadesAtriumMartinista;
    $comissoesEntrada			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,4);
    $construcao				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,7);
    $donativos				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,2);
    $atividadesSociais			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,5);
    $convencoesEntrada			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,8);
    $jornadasEntrada			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,10);
    $regiaoEntrada                  	= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,16);
    $totalEntradaConvencoesJornadas         = $convencoesEntrada+$jornadasEntrada+$regiaoEntrada;
    $receitasFinanceiras                    = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,9);
    $bazar					= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,3);
    $suprimentos				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,11);
    $totalEntradaBazarSuprimentos           = $bazar+$suprimentos;
    $recebimentosDiversos                   = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,6);
    //Subtotal
    $subtotalEntrada			= $totalEntradaMensalidades+$comissoesEntrada+$construcao+$donativos
                                                                            +$atividadesSociais+$totalEntradaConvencoesJornadas
                                                                            +$receitasFinanceiras+$totalEntradaBazarSuprimentos+$recebimentosDiversos;
    $glpTrimestralidades                    = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,12);
    $glpOutros				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,14);
    $totalEntradaTrimesOutros		= $glpTrimestralidades+$glpOutros;
    $glpSuprimentos				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,13);
    //Soma das Entradas
    $somaEntradas				= $subtotalEntrada+$totalEntradaTrimesOutros+$glpSuprimentos;

    //Total das Entradas
    $totalEntradas = $somaEntradas+$saldoAnosAnteriores;

    /**
     * Cálculo Saídas
     */
    $alugueis 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,1);
    $comissoes 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,2);
    $luz	 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,3);
    $agua	 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,4);
    $telefone 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,5);
    $totalSaidaLuzAguaTelefone		= $luz+$agua+$telefone;
    $tarifas 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,6);
    $impostos				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,22);
    $totalSaidaTarifasImpostos		= $tarifas+$impostos;
    $manutencao				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,7);
    $beneficienciaSocial			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,8);
    $boletim 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,9);
    $convencoesSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,10);
    $jornadasSaida				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,11);
    $regiaoSaida				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,23);
    $totalSaidaConvencoesJornadas           = $convencoesSaida+$jornadasSaida+$regiaoSaida;
    $reunioesSociais			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,12);
    $despesaCorreio				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,13);
    $anuncios 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,14);
    $cartaConstitutivaGLP			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,15);
    $cantina 				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,16);
    $despesasGerais				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,17);


    //Subtotal Saída
    $subtotalSaida				= $alugueis+$comissoes+$totalSaidaLuzAguaTelefone+$totalSaidaTarifasImpostos+$manutencao
                                                                    +$beneficienciaSocial+$boletim+$totalSaidaConvencoesJornadas+$reunioesSociais
                                                                    +$despesaCorreio+$anuncios+$cartaConstitutivaGLP+$cantina+$despesasGerais;

    $glpRemessaTrimestralidade		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,18);
    $glpOutrosSaida				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,20);
    $totalGlpRemessTrimesOutros		= $glpRemessaTrimestralidade+$glpOutrosSaida;
    $glpPagamentosSuprimentos		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,19);
    $investimentos				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,21);

    //Total Saída
    $totalSaidas				= $subtotalSaida+$totalGlpRemessTrimesOutros+$glpPagamentosSuprimentos+$investimentos;

    //SALDO DO ANO
    $saldoAno 				= $totalEntradas-$totalSaidas;

    /**
     * Cálculo dos Rendimentos do OA
     */
    $mesAtual 					= 12;
    $dinheiroEmCaixa 				= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,1,1,true);
    $bancos			 		= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,2,1,true);
    
    $atriumMartinista 				= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,3,1,true);
    $aplicacaoPoupanca 				= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,4,1,true);
    $aplicacaoCDB	 			= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,5,1,true);
    $aplicacaoRDB	 			= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,6,1,true);
    $aplicacaoOutros 				= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,7,1,true);
    
    $fundosVinculados				= $atriumMartinista+$aplicacaoPoupanca+$aplicacaoCDB+$aplicacaoRDB+$aplicacaoOutros;
    
    $totalRendimentos				= $dinheiroEmCaixa+$bancos+$fundosVinculados;
    $mesAtual 					= null;

    /**
     * Cálculo dos Rendimentos da Região
     */
    $dinheiroEmCaixaRegiao			= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,1,2,true);
    $bancosRegiao					= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,2,2,true);
    $atriumMartinistaRegiao 		= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,3,2,true);
    $aplicacaoPoupancaRegiao		= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,4,2,true);
    $aplicacaoCDBRegiao	 			= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,5,2,true);
    $aplicacaoRDBRegiao	 			= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,6,2,true);
    $aplicacaoOutrosRegiao 			= $rendimento->retornaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado,7,2,true);
    $fundosVinculadosRegiao			= $atriumMartinistaRegiao+$aplicacaoPoupancaRegiao+$aplicacaoCDBRegiao+$aplicacaoRDBRegiao+$aplicacaoOutrosRegiao;
    $totalRendimentosRegiao			= $dinheiroEmCaixaRegiao+$bancosRegiao+$fundosVinculadosRegiao;

    /*
     * Seleção dos rosacruzes ativos no OA
     */
    $mediaNumeroMembrosAtivos = $membrosRosacruzesAtivos->retornaMediaMembrosRosacruzesAtivos($anoAtual,$idOrganismoAfiliado);

?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: left;
    font-family: Avenir LT Std;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>211 - <?php if(date('m')!=12){?>PRÉVIA DO <?php } ?>RELATÓRIO FINANCEIRO ANUAL</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado." ".$siglaOrganismoAfiliado; ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $anoAtual; ?>
                    </span>
            </div>
    </center>
    <br>
    <table border="1" width="100%">
      <tr>
        <th colspan="2">Entradas</th>
        <th colspan="2">Saídas</th>
      </tr>
      <tr>
        <td>Mensalidades: Organismo Afiliado + Atrium Martinista</td>
        <td><?php echo number_format($totalEntradaMensalidades, 2, ',', '.');?></td>
        <td>Aluguéis</td>
        <td><?php echo number_format($alugueis, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Comissões</td>
        <td><?php echo number_format($comissoesEntrada, 2, ',', '.');?></td>
        <td>Comissões</td>
        <td><?php echo number_format($comissoes, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Construção</td>
        <td><?php echo number_format($construcao, 2, ',', '.');?></td>
        <td>Luz, água, telefone</td>
        <td><?php echo number_format($totalSaidaLuzAguaTelefone, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Donativos e Lei de AMRA</td>
        <td><?php echo number_format($donativos, 2, ',', '.');?></td>
        <td>Tarifas, taxas e impostos</td>
        <td><?php echo number_format($totalSaidaTarifasImpostos, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Atividades Sociais e Culturais</td>
        <td><?php echo number_format($atividadesSociais, 2, ',', '.');?></td>
        <td>Manutenção</td>
        <td><?php echo number_format($manutencao, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Região, Convenções e Jornadas Místicas</td>
        <td><?php echo number_format($totalEntradaConvencoesJornadas, 2, ',', '.');?></td>
        <td>Beneficência Social</td>
        <td><?php echo number_format($beneficienciaSocial, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Receita Financeira</td>
        <td><?php echo number_format($receitasFinanceiras, 2, ',', '.');?></td>
        <td>Boletim</td>
        <td><?php echo number_format($boletim, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Bazar e Suprimentos</td>
        <td><?php echo number_format($totalEntradaBazarSuprimentos, 2, ',', '.');?></td>
        <td>Região, Convenções e Jornadas Místicas</td>
        <td><?php echo number_format($totalSaidaConvencoesJornadas, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>Recebimentos Diversos</td>
        <td><?php echo number_format($recebimentosDiversos, 2, ',', '.');?></td>
        <td>Reuniões Sociais</td>
        <td><?php echo number_format($reunioesSociais, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Despesa de Correio</td>
        <td><?php echo number_format($despesaCorreio, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>Anúncios</td>
        <td><?php echo number_format($anuncios, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td><b>SUBTOTAL</b></td>
        <td><?php echo number_format($subtotalEntrada, 2, ',', '.');?></td>
        <td>Carta Constitutiva (GLP)</td>
        <td><?php echo number_format($cartaConstitutivaGLP, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>GLP-Trimestralidades e outros valores</td>
        <td><?php echo number_format($totalEntradaTrimesOutros, 2, ',', '.');?></td>
        <td>Cantina</td>
        <td><?php echo number_format($cantina, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td>GLP-Suprimentos</td>
        <td><?php echo number_format($glpSuprimentos, 2, ',', '.');?></td>
        <td>Despesas Gerais</td>
        <td><?php echo number_format($despesasGerais, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td><b>SOMA DAS ENTRADAS</b></td>
        <td><?php echo number_format($somaEntradas, 2, ',', '.');?></td>
        <td><b>SUBTOTAL</b></td>
        <td><?php echo number_format($subtotalSaida, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>GLP - Remessa Trim. e Outros</td>
        <td><?php echo number_format($totalGlpRemessTrimesOutros, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td>GLP - Pagamentos Suprimentos</td>
        <td><?php echo number_format($glpPagamentosSuprimentos, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td width="30%"><b>SALDO ANO ANTERIOR</b></td>
        <td width="20%"><?php echo number_format($saldoAnosAnteriores, 2, ',', '.');?></td>
        <td width="35%">Investimentos (terrenos, prédios, móveis, computadores, ar-condicionado, etc)</td>
        <td width="15%"><?php echo number_format($investimentos, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td><b>TOTAL DAS ENTRADAS</b></td>
        <td><?php echo number_format($totalEntradas, 2, ',', '.');?></td>
        <td><b>TOTAL DAS SAIDAS</b></td>
        <td><?php echo number_format($totalSaidas, 2, ',', '.');?></td>
      </tr>
      <tr>
        <td align="right"><b>SALDO DO ANO</b></td>
        <td><?php echo number_format($saldoAno, 2, ',', '.');?></td>
        <td></td>
        <td></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
        <tr>
                <td width="16%">Saldo Caixa</td>
                <td width="13%"><?php echo number_format($dinheiroEmCaixa, 2, ',', '.');?></td>
                <?php if($totalRendimentosRegiao>0){?>
                <td width="23%">Saldo Caixa Região</td>
                <td width="16%"><?php echo number_format($dinheiroEmCaixaRegiao, 2, ',', '.');?></td>
                <?php }?>
            </tr>
            <tr>
                <td>Saldo Bancos</td>
                <td><?php echo number_format($bancos, 2, ',', '.');?></td>
                <?php if($totalRendimentosRegiao>0){?>
                <td>Saldo Bancos Região</td>
                <td><?php echo number_format($bancosRegiao, 2, ',', '.');?></td>
                <?php }?>
            </tr>
            <tr>
                <td>Saldo Fundos Vinculados</td>
                <td><?php echo number_format($fundosVinculados, 2, ',', '.');?></td>
                <?php if($totalRendimentosRegiao>0){?>
                <td>Saldo Fundos Vinculados Região</td>
                <td><?php echo number_format($fundosVinculadosRegiao, 2, ',', '.');?></td>
                <?php }?>
            </tr>
            <tr>
                <td><b>Total</b></td>
                <td><?php echo number_format($totalRendimentos, 2, ',', '.');?></td>
                <?php if($totalRendimentosRegiao>0){?>
                <td><b>Total</b></td>
                <td><?php echo number_format($totalRendimentosRegiao, 2, ',', '.');?></td>
                <?php }?>
            </tr>
    </table>
    <?php 
    $totalDivida = $divida->retornaDivida(null,$anoAtual,$idOrganismoAfiliado);
    if($totalDivida>0)
    {
    ?>
    <br>
    <table border="1" width="100%">
        <tr>
                <td colspan="5" align="center"><b>DÍVIDAS</b></td>
            </tr>
            <tr>
                <td width="20%" align="center"><b>Descrição</b></td>
                <td width="20%" align="center"><b>Início</b></td>
                <td width="20%" align="center"><b>Fim</b></td>
                <td width="20%" align="center"><b>Valor</b></td>
                <td width="20%" align="center"><b>Situação</b></td>
            </tr>
            <?php 
            $resultado = $divida->listaDivida($idOrganismoAfiliado,null,$anoAtual);
            if($resultado)
            {
                $totalDivida=0;
                    foreach ($resultado as $vetor)
                    {
                        
                        $resultado7 = $quitacaoDivida->listaQuitacaoDivida($idOrganismoAfiliado,null,null,null,null,$vetor['idDivida']);
                        $i=0;
                        if($resultado7)
                        {    
                            foreach ($resultado7 as $vetor7)
                            {
                                $valorDivida = (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
                                $valorPagamento = (float) str_replace(",",".",str_replace(".","",$vetor7['valorPagamento']));
                                if($i==0)
                                {    
                                    $valorRestante = $valorDivida - $valorPagamento;
                                }else{
                                    $valorRestante = $valorRestante - $valorPagamento;
                                }
                                $i++;

                            }
                            $valorRestanteFormatado = number_format($valorRestante, 2, ',', '.');
                            
            ?>
            <tr>
                <td align="center"><?php echo $vetor['descricaoDivida'];?></td>
                <td align="center"><?php echo substr($vetor['inicioDivida'],8,2)."/".substr($vetor['inicioDivida'],5,2)."/".substr($vetor['inicioDivida'],0,4);?></td>
                <td align="center"><?php echo substr($vetor['fimDivida'],8,2)."/".substr($vetor['fimDivida'],5,2)."/".substr($vetor['fimDivida'],0,4);?></td>
                <td align="center"><?php echo $vetor['valorDivida'];?></td>
                <td align="center"><?php if($valorRestanteFormatado=="-0,00"||$valorRestanteFormatado=="0,00"){ echo "Pago";}else{ echo "Pendente";}?></td>
            </tr>
            <?php 
                        }
                        $totalDivida += (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
                    }
                    if(count($resultado)>1)
                    {
                            ?>
                            <tr>
                                <td align="center">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                                <td align="center"><b>Total</b></td>
                                <td align="center"><?php echo number_format($totalDivida, 2, ',', '.');?></td>
                                <td align="center">&nbsp;</td>
                            </tr>
                            <?php 
                    }
            }
            ?>
    </table>
    <?php
    } 
    ?>
    <?php 
    // $totalQuitacaoDivida = $quitacaoDivida->retornaQuitacaoDivida(null,$anoAtual,$idOrganismoAfiliado);
    // if($totalQuitacaoDivida>0)
    // {
    ?>
    <br>
    <table border="1" width="100%">
        <tr>
                <td colspan="5" align="center"><b>QUITAÇÃO DE DÍVIDAS</b></td>
            </tr>
            <tr>
                <td width="25%" align="center"><b>Descrição</b></td>
                <td width="25%" align="center"><b>Valor Integral</b></td>
                <td width="25%" align="center"><b>Data do Pagamento</b></td>
                <td width="25%" align="center"><b>Valor do Pagamento</b></td>
                <td width="25%" align="center"><b>Valor Restante</b></td>
            </tr>
            <?php 
            $resultado = $quitacaoDivida->listaQuitacaoDivida($idOrganismoAfiliado,null,null,null,null,null,$anoAtual);
            $i=0;
            if($resultado)
            {
                    foreach ($resultado as $vetor)
                    {
                        $valorDivida = (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
                        $valorPagamento = (float) str_replace(",",".",str_replace(".","",$vetor['valorPagamento']));
                        if($i==0||$fk_idDivida!=$vetor['fk_idDivida'])
                        {    
                            $valorRestante = $valorDivida - $valorPagamento;
                        }else{
                            $valorRestante = $valorRestante - $valorPagamento;
                        }
            ?>
            <tr>
                <td align="center"><?php echo $vetor['descricaoDivida'];?></td>
                <td align="center"><?php echo $vetor['valorDivida'];?></td>
                <td align="center"><?php echo substr($vetor['dataPagamento'],8,2)."/".substr($vetor['dataPagamento'],5,2)."/".substr($vetor['dataPagamento'],0,4);?></td>
                <td align="center"><?php echo $vetor['valorPagamento'];?></td>
                <td align="center"><?php echo number_format($valorRestante, 2, ',', '.');?></td>
            </tr>
            <?php 
                        $fk_idDivida = $vetor['fk_idDivida'];
                        $i++;
                    }
            }
            ?>
    </table>
    <?php
    // } 
    ?>
    <br>
    <table border="1" width="100%">
        <tr>
                <td colspan="5" align="center"><b>MÉDIA DO NÚMERO DE ROSACRUZES ATIVOS NO ORGANISMO AFILIADO</b></td>
            </tr>
            <tr>
                <td colspan="5" align="center"><b><?php echo $mediaNumeroMembrosAtivos;?></b></td>
            </tr>
    </table>
    <br>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='201';//Mestre do OA
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $mestreOA="";
    $mestreOA2 = "";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $mestreOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $mestreOA = $arr[0];
        $mestreOA2 = $arr[1];
    }
    ?>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='203';//Secretario do OA
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $secretarioOA="";
    $secretarioOA2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $secretarioOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $secretarioOA = $arr[0];
        $secretarioOA2 = $arr[1];
    }
    ?>
    <?php 
    if($classificacaoOA==2)
    {
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='205';//Guardião do OA
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $guardiaoOA="";
    $guardiaoOA2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $guardiaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $guardiaoOA = $arr[0];
        $guardiaoOA2 = $arr[1];
    }
    }
    ?>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='207';//Presidente da Junta Depositária
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $presidenteJuntaDepositariaOA="";
    $presidenteJuntaDepositariaOA2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $presidenteJuntaDepositariaOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $presidenteJuntaDepositariaOA = $arr[0];
        $presidenteJuntaDepositariaOA2 = $arr[1];
    }
    ?>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='209';//Secretário da Junta Depositária
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $secretarioJuntaDepositariaOA="";
    $secretarioJuntaDepositariaOA2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $secretarioJuntaDepositariaOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $secretarioJuntaDepositariaOA = $arr[0];
        $secretarioJuntaDepositariaOA2 = $arr[1];
    }
    ?>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='211';//Tesoureiro da Junta Depositária
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $tesoureiroJuntaDepositariaOA="";
    $tesoureiroJuntaDepositariaOA2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $tesoureiroJuntaDepositariaOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $tesoureiroJuntaDepositariaOA = $arr[0];
        $tesoureiroJuntaDepositariaOA2 = $arr[1];
    }
    ?>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='213';//Mestre Auxiliar
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $mestreAuxiliarOA="";
    $mestreAuxiliarOA2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $mestreAuxiliarOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $mestreAuxiliarOA = $arr[0];
        $mestreAuxiliarOA2 = $arr[1];
    }
    ?>
    <table border="1" width="100%">
            <tr>
                <td width="25%"><b>Mestre</b></td>
                <td width="40%"><?php echo $mestreOA;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php if($mestreOA2!=""){?>
            <tr>
                <td width="25%"><b>Mestre Retirante</b></td>
                <td width="40%"><?php echo $mestreOA2;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php }?>
            <tr>
                <td><b>Secretário</b></td>
                <td><?php echo $secretarioOA;?></td>
                <td><b>Assinatura:</b></td>
                <td></td>
            </tr>
            <?php if($secretarioOA2!=""){?>
            <tr>
                <td width="25%"><b>Secretário Retirante</b></td>
                <td width="40%"><?php echo $secretarioOA2;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php }?>
            <?php 
            if($classificacaoOA==2)
            {
            ?>
            <tr>
                <td><b>Guardião</b></td>
                <td><?php echo $guardiaoOA;?></td>
                <td><b>Assinatura:</b></td>
                <td></td>
            </tr>
            <?php if($guardiaoOA2!=""){?>
            <tr>
                <td width="25%"><b>Guardião Retirante</b></td>
                <td width="40%"><?php echo $guardiaoOA2;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php }?>
            <?php 
            }
            ?>
            <?php 
            if($classificacaoOA!=2)
            {
            ?>
            <tr>
                <td><b>Presidente da Junta Depositária</b></td>
                <td><?php echo $presidenteJuntaDepositariaOA;?></td>
                <td><b>Assinatura:</b></td>
                <td></td>
            </tr>
            <?php if($presidenteJuntaDepositariaOA2!=""){?>
            <tr>
                <td width="25%"><b>Presidente da Junta Depositária Retirante</b></td>
                <td width="40%"><?php echo $presidenteJuntaDepositariaOA2;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php }?>
            <tr>
                <td><b>Secretário da Junta Depositária</b></td>
                <td><?php echo $secretarioJuntaDepositariaOA;?></td>
                <td><b>Assinatura:</b></td>
                <td></td>
            </tr>
            <?php if($secretarioJuntaDepositariaOA2!=""){?>
            <tr>
                <td width="25%"><b>Secretário da Junta Depositária Retirante</b></td>
                <td width="40%"><?php echo $secretarioJuntaDepositariaOA2;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php }?>
            <tr>
                <td><b>Tesoureiro da Junta Depositária</b></td>
                <td><?php echo $tesoureiroJuntaDepositariaOA;?></td>
                <td><b>Assinatura:</b></td>
                <td></td>
            </tr>
            <?php if($tesoureiroJuntaDepositariaOA2!=""){?>
            <tr>
                <td width="25%"><b>Tesoureiro da Junta Depositária Retirante</b></td>
                <td width="40%"><?php echo $tesoureiroJuntaDepositariaOA2;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php }?>
            <tr>
                <td><b>Mestre Auxiliar</b></td>
                <td><?php echo $mestreAuxiliarOA;?></td>
                <td><b>Assinatura:</b></td>
                <td></td>
            </tr>
            <?php if($mestreAuxiliarOA2!=""){?>
            <tr>
                <td width="25%"><b>Mestre Auxiliar Retirante</b></td>
                <td width="40%"><?php echo $mestreAuxiliarOA2;?></td>
                <td width="9%"><b>Assinatura:</b></td>
                <td width="26%"></td>
            </tr>
            <?php }?>
            <?php 
            }
            ?>
    </table>
    <br>
    <table width="100%">
            <tr>
                    <td align="center" colspan="3"><b>Comissão de Verificação</b></td>
            </tr>
            <tr>
                    <td width="33%">&nbsp;</td>
                    <td width="33%">&nbsp;</td>
                    <td width="33%">&nbsp;</td>
            </tr>
            <tr>
                    <td width="33%"><center>____________________________</center></td>
                    <td width="33%"><center>____________________________</center></td>
                    <td width="33%"><center>____________________________</center></td>
            </tr>
    </table>
<?php
}
?>