<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idRelatorioGrandeConselheiro = isset($_REQUEST['idRelatorioGrandeConselheiro'])?$_REQUEST['idRelatorioGrandeConselheiro']:null;

    include_once("../controller/relatorioGrandeConselheiroController.php");
    $r = new relatorioGrandeConselheiroController();
    $dados = $r->buscaRelatorioGrandeConselheiro($idRelatorioGrandeConselheiro);

    //Verificação se GC possui plano de ação cadastrado no SOA
    include_once("../model/planoAcaoRegiaoClass.php");
    $p = new PlanoAcaoRegiao();
    $resultadoPlanoAcao = $p->listaPlanoAcaoRegiao(null,$dados->getCriadoPorId());
    $totalPlanoAcao=0;
    if($resultadoPlanoAcao)
    {
            $totalPlanoAcao = count($resultadoPlanoAcao);
    }

    include_once("../lib/functions.php");


    $rgc = new relatorioGrandeConselheiro();
    $resultado4 = $rgc->buscarAssinaturasEmRelatorioGrandeConselheiroPorDocumento($_REQUEST['idRelatorioGrandeConselheiro']);
    $assinaturas="<br>";
    if($resultado4)
    {
        foreach($resultado4 as $vetor4)
        {
            $nomeUsuario = $vetor4['nomeUsuario'];
            $codigoAfiliacao = $vetor4['codigoDeAfiliacao'];
            $funcao = $vetor4['nomeFuncao'];

            $assinaturas .= $nomeUsuario." [".$codigoAfiliacao."]"." - ".$funcao."<br>";
        }
    }

    $temNumeroAssinatura=false;
    $rgc2 = new relatorioGrandeConselheiro();
    $resultado = $rgc2->buscarIdRelatorioGrandeConselheiro($_REQUEST['idRelatorioGrandeConselheiro']);
    if($resultado) {
        foreach ($resultado as $vetor) {

            if(trim($vetor['numeroAssinatura'])!="")
            {
                $numeroAssinatura = $vetor['numeroAssinatura'];
                $temNumeroAssinatura=true;
            }else{
                $numeroAssinatura = aleatorioAssinatura();
                $rgc2->setIdRelatorioGrandeConselheiro($vetor['idRelatorioGrandeConselheiro']);
                $rgc2->atualizaNumeroAssinatura($numeroAssinatura);
            }

            //Verificar se está entregue
            $pronto = ($vetor['entregue']==1)? true : false;

        }
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>RELATÓRIO SEMESTRAL DO GRANDE CONSELHEIRO</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Região:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $dados->getRegiao(); ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            G.C.:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $dados->getCriadoPor(); ?> - <?php echo $dados->getCodigoAfiliacao(); ?> 
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Semestre:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php if($dados->getSemestre()==1){ echo "1º";}?>
                            <?php if($dados->getSemestre()==2){ echo "2º";}?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $dados->getAno();?>
                    </span>
                    <br>
                    <b class="fonte tamanho-fonte_2">
                            <?php if($totalPlanoAcao>0){?>
                            Possui Plano de Ação cadastrado no SOA
                            <?php }else{?>
                            Não possui Plano de Ação cadastrado no SOA
                            <?php }?>
                    </b>
            </div>
    </center>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>01) O(a) Frater/Soror trabalha com um Plano de Gestão em sua Região?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta1()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta1()==2){ echo "Não";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario1();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>02) Organismos Afiliados que permanecem sob sua supervisão:</th>
      </tr>
      <tr>
        <td>Resposta: <br><br>
                    <?php
                    include_once '../model/organismoClass.php';
                    include_once '../lib/functions.php';
                    $o = new organismo();
                    $resultado = $o->listaOrganismo(null,null,null,$dados->getRegiao());

                    include_once '../model/relatorioGrandeConselheiroSupervisaoClass.php';
                    $s = new relatorioGrandeConselheiroSupervisao();
                    $resultadoOas = $s->listaOrganismos($idRelatorioGrandeConselheiro);
                    $arrOAsMarcados = array();
                    if($resultadoOas)
                    {
                            foreach($resultadoOas as $vetorOas)
                            {
                                    $arrOAsMarcados[] = $vetorOas['fk_idOrganismoAfiliado'];
                            }
                    }

                            if($resultado)
                            {
                                    foreach($resultado as $vetor)
                                    {
                                            $nomeOrganismo = organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'],$vetor['tipoOrganismoAfiliado'],$vetor['nomeOrganismoAfiliado'],$vetor['siglaOrganismoAfiliado']); 

                                            if(in_array($vetor['idOrganismoAfiliado'],$arrOAsMarcados))
                                            {
                                                    echo $nomeOrganismo."<br>";
                                            }
                                    }
                            }
                            ?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>03) Consta em seu Plano a permanente Expansão Rosacruz?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta3()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta3()==2){ echo "Não";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario3();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>04) Desenvolve atividades junto a Comunidade que possam permitir o acesso ao Organismo Afiliado de não-membros?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta4()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta4()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta4()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario4();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>05) Sua Região desenvolve atividades abertas a Não-Membros (Seção 700)? Em quais Organismos Afiliados? Solicitamos informar dias e horários.</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta5()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta5()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta5()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario5();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>06) Como anda a comunicação entre os Organismos Afiliados e o Grande Conselheiro. Você envia regularmente circulares atualizando os assuntos da GLP?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta6()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta6()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta6()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario6();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>07) O(a) Frater/Soror tem procurado praticar a integração com outras Regiões?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta7()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta7()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta7()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario7();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>08) O(a) Frater/Soror tem acompanhado através do "Check List" enviado pela GLP, a atualização do Manual Administrativo?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta8()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta8()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta8()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario8();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>09) Os Organismos Afiliados de sua Região tem promovido trabalhos metafísicos visando harmonização e sustentação dos mesmos?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta9()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta9()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta9()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario9();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>10) Sua Região tem desenvolvido atividades da Classe dos Artesãos?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta10()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta10()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta10()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario10();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>11) Considera o número de membros de sua Região razoável e de acordo com a realidade e especificidades de sua circunscrição?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta11()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta11()==2){ echo "Não";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario11();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>12) O(a) Frater/Soror tem um Monitor Regional para cada Organismo Afiliado e se mantém em frequente comunicação com eles?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta12()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta12()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta12()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario12();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>13) Monitores Regionais que lhe auxiliam na sua Região:</th>
      </tr>
      <tr>
        <td>Resposta: <br><br>
                    <?php
                    $ocultar_json=1;
                    $nomeParcialFuncao='MONITOR';
                    $atuantes='S';
                    $naoAtuantes='N';
                    include '../js/ajax/retornaFuncaoMembro.php';
                    $obj = json_decode(json_encode($return),true);

                    include_once '../model/relatorioGrandeConselheiroMonitorClass.php';
                    $m = new relatorioGrandeConselheiroMonitor();
                    $resultadoMon = $m->listaMonitores($idRelatorioGrandeConselheiro);
                    $arrMonitoresMarcados = array();
                    if($resultadoMon)
                    {
                            foreach($resultadoMon as $vetorMon)
                            {
                                    $arrMonitoresMarcados[] = $vetorMon['seqCadast'];
                            }
                    }
                    ?>
                    <?php 
                    foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor)
                    {
                            if($vetor['fields']['fCodMembro']!=0)
                            {
                                    if(substr($vetor['fields']['fSigOrgafi'],0,3)==$dados->getRegiao()){

                                                    if(in_array($vetor['fields']['fSeqCadast'],$arrMonitoresMarcados))
                                                    {
                                                            echo $vetor['fields']['fNomClient']."<br>";
                                                    }
                                    }
                            }
                    }
                    ?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>14) Sua Região tem desenvolvido atividades que permitem descobrir potenciais servidores entre os neófitos? Quais?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta14()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta14()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta14()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario14();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>15) Está previsto em seu modelo de gestão algum tipo de treinamento Reuniões Preparatórias de Oficiais, Seminários, Reproari, Redig, etc.?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta15()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta15()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta15()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario15();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>16) Consta em seu modelo de gestão participar de algumas Assembléias Gerais em Lojas, Capítulos e Pronaos?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta16()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta16()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta16()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario16();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>17) Com que frequência o(a) Frater/Soror tem reuniões presenciais com os Monitores Regionais e Oficiais de Organismos Afiliados?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta17()==1){ echo "1 vez por mês";}?>
                    <?php if($dados->getPergunta17()==2){ echo "1 vez por trimestre";}?>
                    <?php if($dados->getPergunta17()==3){ echo "1 vez por semestre";}?>
                    <?php if($dados->getPergunta17()==4){ echo "1 vez por ano";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario17();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>18) O(a) Frater/Soror supervisiona os Relatórios Mensais Financeiros e Atas para acompanhamento do desempenho e situação política financeira de seus OAs?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta18()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta18()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta18()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario18();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>19) O(a) Frater/Soror tem um Monitor para acompanhar a remessa regular desses documentos? (Controladoria)</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta19()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta19()==2){ echo "Não";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario19();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>20) A sua Região tem realizado ERIS? Jornadas Místicas?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta20()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta20()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta20()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario20();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>21) A sua Região tem realizado Convenções Regionais?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta21()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta21()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta21()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario21();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>22) Em sua Região recentemente houve a Implantação de GDs, fundação de Pronaos, Elevação de Capítulo a Loja? Quais?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta22()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta22()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta22()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario22();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>23) O(a) Frater/Soror tem se colocado como um amigo do Organismo Afiliado, evitando posicionamentos radicais, sendo um ponto de apoio e procurando difundir o espírito fraternal entre os Rosacruzes e os Oficiais?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta23()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta23()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta23()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario23();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>24) O(a) Frater/Soror tem visitado cada Organismo Afiliado de sua Região, pelo menos uma vez durante o ano?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta24()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta24()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta24()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario24();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>25) Como Mestre Provincial o Irmão/Irmã tem participado de Conventículos e Reuniões da TOM?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta25()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta25()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta25()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario25();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>26) O(a) Frater/Soror tem se empenhado para que todos os Organismos de sua Região funcionem em harmonia e cordialidade, com estrita observância do Manual Administrativo, Ritualístico e Iniciático?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta26()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta26()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta26()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario26();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>27) Verifica se os símbolos tradicionais da Ordem estão sendo respeitados e estão devidamente dispostos nos nossos Templos?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta27()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta27()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta27()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario27();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>28) Quando há violação das normas, diferenças de opinião, alguma irregularidade manifesta no trabalho, decoro ou procedimentos, o(a) Frater/Soror apresenta sugestões e conselhos e serve de mediador na administração de conflitos?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta28()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta28()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta28()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario28();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>29) O(a) Frater/Soror faz os ajustes e alterações que julga necessários para resolver a situação e mantém a GLP informada?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta29()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta29()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta29()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario29();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>30) O(a) Frater/Soror procura se manter informado sobre a seleção de Oficiais, para o novo Ano Rosacruz? (Obs. Quando julgar necessário, através de carta, telefonema, e-mail ou relatório separado, deverá comunicar-nos seu parecer sobre as recomendações)</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta30()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta30()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta30()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario30();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>31) O(a) Frater/Soror cuida para que na apresentação de discursos preparados pelos estudantes rosacruzes, somente sejam esboçados conceitos Rosacruzes e assuntos condizentes com os objetivos e ensinamentos da Ordem? (Obs. Zelar para que não sejam muitos extensos. Não devem ultrapassar uma hora! E sempre nos enviar uma cópia daqueles que julgar interessantes para publicação)</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta31()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta31()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta31()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario31();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>32) Tem algum assunto relevante, que julgue ser necessário a apresentação, na próxima Reunião do Grande Conselho?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta32()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta32()==2){ echo "Não";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario32();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>33) O(a) Frater/Soror está atento(a) sobre atividades de outros movimentos que possam prejudicar a Ordem de alguma maneira, ou através de convites para afiliação ou por proselitismo de outras organizações? (Obs. Comunicar o fato e se possível, enviar cópia da literatura para exame e arquivo da GLP. O(a) Frater/Soror também deve estar a par de eventuais ataques à AMORC, por fanáticos religiosos e ou outros grupos)</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta33()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta33()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta33()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario33();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>34) O(a) Frater/Soror procura manter e zelar pela boa imagem da AMORC na sua Região, desfazendo qualquer mal-entendido?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta34()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta34()==2){ echo "Não";}?>
                    <?php if($dados->getPergunta34()==3){ echo "Raramente";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario34();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>35) O(a) Frater/Soror poderia nos enviar um relatório sucinto, à parte, relacionando todos os Organismos Afiliados de sua região, mencionando a situação de cada um e se os mesmos estão cumprindo as determinações do Acordo de Afiliação?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta35()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta35()==2){ echo "Não";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario35();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>36) O(a) Grande Conselheiro(a) conta com apoio direto da Divisão de Organismos Afiliados, que deve assisti-lo nessa tarefa. Qual a sua opnião sobre a referida Divisão quanto a estar atuando de forma satisfatória?</th>
      </tr>
      <tr>
        <td>Classifique de 1 a 5 (1 mais baixa, 5 mais alta): 
                    <?php if($dados->getPergunta36()==1){ echo "1";}?>
                    <?php if($dados->getPergunta36()==2){ echo "2";}?>
                    <?php if($dados->getPergunta36()==3){ echo "3";}?>
                    <?php if($dados->getPergunta36()==4){ echo "4";}?>
                    <?php if($dados->getPergunta36()==5){ echo "5";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario36();?></td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>37) O(a) Frater/Soror tem alguma sugestão para que esse relacionamento possa ser melhorado?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getPergunta37()==1){ echo "Sim";}?>
                    <?php if($dados->getPergunta37()==2){ echo "Não";}?>
        </td>
      </tr>
      <tr>
        <td>Comentários: <?php echo $dados->getComentario37();?></td>
      </tr>
    </table>
    <?php if($pronto){?>
    <br><br>
    <b>Concordaram e assinaram eletrônicamente este documento:</b><br><br>
<?php }?>

    <center><?php if($pronto){ if($assinaturas!="<br>"){echo $assinaturas;}else{ echo "ESSE DOCUMENTO NÃO FOI ASSINADO";}}?></center><br>
    <hr>
    <center><b>Código da Assinatura Eletrônica: <font color="blue"><?php echo $numeroAssinatura; ?></font></b>
        <br><br>(Documento gerado eletronicamente, para validar sua autenticidade utilize a opção validar documento dentro do sistema SOA)
    </center>
<?php
}
?>