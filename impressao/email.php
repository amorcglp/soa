<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idEmail 				= isset($_REQUEST['idEmail'])?$_REQUEST['idEmail']:null;
    $vinculoEmailEnviado 	= isset($_REQUEST['vinculoEmailEnviado'])?$_REQUEST['vinculoEmailEnviado']:null;

    include_once '../model/emailDeParaClass.php';
    $edp = new EmailDePara();

    include_once '../model/usuarioClass.php';
    $u = new Usuario();

    include_once '../model/emailClass.php';
    $e = new email();
    $retorno = $e->buscarEmailPorId($_REQUEST['idEmail']);
    $assunto="";
    $de="";
    $mensagem="";
    $origem="";
    if($retorno)
    {
            foreach ($retorno as $vetor)
            {
                    $origem		= $vetor['de'];
                    $assunto 	= $vetor['assunto'];
                    $de 		= $vetor['nomeUsuario'];
                    $mensagem 	= $vetor['mensagem'];
                    $data		= substr($vetor['dataCadastro'],8,2)."/".substr($vetor['dataCadastro'],5,2)."/".substr($vetor['dataCadastro'],0,4)." ".substr($vetor['dataCadastro'],11,8);
            }
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
    font-size: 14px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>
    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                    </div>
                    <h2>Imprimir Mensagem</h2>
                    <div class="mail-tools tooltip-demo m-t-md">
                            <hr>
                            <h5>
                                    <span class="pull-right font-noraml"><?php echo $data;?>
                                    </span>
                                    <br> 
                                    <span class="font-noraml">De: </span>
                                    <?php echo $de;?><br> 
                                    <span class="font-noraml">Para: </span>
                                    <?php
                                    $edp = new EmailDePara();
                                    if($vinculoEmailEnviado!=null&&$vinculoEmailEnviado!=0)
                                    {
                                            $retorno5 = $edp->selecionarDestinatarios($vinculoEmailEnviado);
                                    }else{
                                            $retorno5 = $edp->selecionarDestinatarios($idEmail);
                                    }
                                    $i=0;
                                    if($retorno5)
                                    {
                                            foreach($retorno5 as $vetor5)
                                            {
                                                    $u = new Usuario();
                                                    $retorno6 = $u->buscarIdUsuario($vetor5['para']);
                                                    if($retorno6)
                                                    {
                                                            foreach($retorno6 as $vetor6)
                                                            {
                                                                    if($i==0)
                                                                    {
                                                                            echo $vetor6['nomeUsuario'];
                                                                    }else{
                                                                            echo ", ".$vetor6['nomeUsuario'];
                                                                    }
                                                                    $i++;
                                                            }
                                                    }
                                            }
                                    }
                                    ?>
                                    .
                            </h5>
                            <h3>
                                    <span class="font-noraml">Assunto: </span>
                                    <?php echo $assunto;?>
                            </h3>
                    </div>
            </div>
            <hr>
            <div class="mail-box">
                    <div class="mail-body">
                    <?php echo $mensagem;?>
                    </div>
            </div>
    </div>
<?php
}
?>