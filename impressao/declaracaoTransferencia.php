<?php 
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    include_once('../model/transferenciaMembroClass.php');
    include_once('../model/organismoClass.php');
    include_once('../lib/functions.php');
    $t = new transferenciaMembro();
    $resultado = $t->buscaTransferenciaPeloId($_REQUEST['idTransferenciaMembro']);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    $tipo2 = "Rosacruz";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    $tipo2= "Martinista";
                                    break;
                    }

                    $o = new organismo();
                    $resultado2 = $o->listaOrganismo(null,null,null,null,null,$vetor['fk_idOrganismoAfiliadoDestino']);

                    if ($resultado2) {
                            foreach ($resultado2 as $vetor2) {

                                    switch ($vetor2['classificacaoOrganismoAfiliado']) {
                                            case 1:
                                                    $classificacao2 = "Loja";
                                                    break;
                                            case 2:
                                                    $classificacao2 = "Pronaos";
                                                    break;
                                            case 3:
                                                    $classificacao2 = "Capítulo";
                                                    break;
                                            case 4:
                                                    $classificacao2 = "Heptada";
                                                    break;
                                            case 5:
                                                    $classificacao2 = "Atrium";
                                                    break;
                                    }
                                    switch ($vetor2['tipoOrganismoAfiliado']) {
                                            case 1:
                                                    $tipo2 = "R+C";
                                                    break;
                                            case 2:
                                                    $tipo2 = "TOM";
                                                    break;
                                    }
                            }
                    }

                    $nome 									= $vetor['nome'];
                    $codigoAfiliacao						= $vetor['codigoAfiliacao'];
                    $seqCadastMembro  						= $vetor['seqCadastMembro'];
                    $fk_idOrganismoAfiliadoDestino			= $vetor['fk_idOrganismoAfiliadoDestino'];
                    $motivoTransferencia					= $vetor['motivoTransferencia'];
                    $outrasInformacoes 						= $vetor['outrasInformacoes'];
                    $observacoes	    					= $vetor['observacoes'];
                    $siglaOAOrigem		    				= $vetor['siglaOAOrigem'];
                    $dataTransferencia    					= substr($vetor['dataTransferencia'],8,2)."/".substr($vetor['dataTransferencia'],5,2)."/".substr($vetor['dataTransferencia'],0,4)." ".substr($vetor['dataTransferencia'],11,8);
                    $mestreOAOrigem						    = $vetor['mestreOAOrigem'];
                    $secretarioOAOrigem						= $vetor['secretarioOAOrigem'];
            }
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 14px!important;
    color: rgb(85, 85, 85);
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>217 - DECLARAÇÃO DE TRANSFERÊNCIA</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 720px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Origem:
                    </b>
                    <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
                    -
                    <b class="fonte tamanho-fonte_2">
                            Destino:
                    </b>
                    <?php echo $classificacao2." ".$tipo2." ".$vetor2['nomeOrganismoAfiliado'];?>
            </div>
    </center>
    <br>
    <hr>
    <b>Data da Transferência:</b> <?php echo $dataTransferencia;?><br>
    <b>Membro:</b> <?php echo $codigoAfiliacao;?> - <?php echo $nome;?><br><br>
    <b>Exerceu os seguintes cargos:</b><br><br>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='S';
    $atuantes='S';
    $seqCadast=$seqCadastMembro;
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayOficiais']);
    $arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];

    if(count($arrFuncoes)>0)
    {
            ?>
            <table border="1" width="100%">
                    <tr>
                            <th>Cargo</th>
                            <th>Ano</th>
                            <th>Organismo</th>
                    </tr>
                    <?php 
                            foreach($arrFuncoes as $vetor)
                            {
                    ?>
                    <tr>
                            <td><?php echo $vetor['fields']['fDesFuncao'];?></td>
                            <td><?php echo substr($vetor['fields']['fDatEntrad'],0,4);?></td>
                            <td><?php echo $vetor['fields']['fNomeOa'];?></td>
                    </tr>
                    <?php 
                            }
                    ?>
            </table>
            <?php 	
    }else{
            echo "--";
    }
    ?>
    <br><br>
    <b>Iniciação de Templo:</b><br><br>
    <?php 
    $ideCompanheiro='N';
    $seqCadast=$seqCadastMembro;
    include '../js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayObrigacoes']);

    $arrIniciacoes = $obj['result'][0]['fields']['fArrayObrigacoes'];

    if(count($arrIniciacoes)>0)
    {
            ?>
            <table border="1" width="100%">
                    <tr>
                            <th>Iniciação</th>
                            <th>Data</th>
                            <th>Sigla do Organismo</th>
                    </tr>
                    <?php 
                            foreach($arrIniciacoes as $vetor)
                            {
                    ?>
                    <tr>
                            <td>
                                    <?php 
                                            $ideCompanheiro='N';
                                            $seqTipoObriga=$vetor['fields']['fSeqTipoObriga'];
                                            include '../js/ajax/retornaTipoObrigacaoRitualistica.php';
                                            $obj2 = json_decode(json_encode($return),true);
                                            //echo "<pre>".print_r($obj2['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga']);
                                            echo $obj2['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga'];
                                    ?>
                            </td>
                            <td><?php echo substr($vetor['fields']['fDatObriga'],8,2)."/".substr($vetor['fields']['fDatObriga'],5,2)."/".substr($vetor['fields']['fDatObriga'],0,4);?></td>
                            <td><?php echo $vetor['fields']['fSigOrgafi'];?></td>
                    </tr>
                    <?php 
                            }
                    ?>
            </table>
            <?php 	
    }else{
            echo "--";
    }

    ?>
    <br><br>
    <b>Motivo da Transferência:</b> <?php echo $motivoTransferencia;?><br>
    <?php if($outrasInformacoes!=""){?><b>Outras Informações:</b> <?php echo $outrasInformacoes;?><br><?php }?>
    <?php if($observacoes!=""){?><b>Observações:</b> <?php echo $observacoes;?><?php }?>
    <br>
    <br><br>
    <center>
            <table cellpading="4">
                    <tr>
                            <td><center>___________________________________________</center></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><center>___________________________________________</center></td>
                    </tr>
                    <tr>
                            <td><center><?php echo $mestreOAOrigem;?></center></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><center><?php echo $secretarioOAOrigem;?></center></td>
                    </tr>
                    <tr>
                            <td><center>Mestre do Organismo Afiliado</center></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td><center>Secretario do Organismo Afiliado</center></td>
                    </tr>
            </table>
    </center>
<?php
}
?>