<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once('../model/organismoClass.php');
    $o = new organismo();
    $resultado = $o->listaOrganismo(null,null,null,null,null,$_GET['idOrganismoAfiliado'],null,null,null,null,null,1);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }
                    switch ($vetor['paisOrganismoAfiliado'])
                    {
                        case 1:
                            $siglaPais = "BR";
                            break;
                        case 2:
                            $siglaPais = "PT";
                            break;
                        case 3:
                            $siglaPais = "AO";
                            break;
                        case 4:
                            $siglaPais = "MZ";
                            break;
                        default :
                            $siglaPais = "BR";
                            break;
                    }

                    $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
                    $logradouro = $vetor['enderecoOrganismoAfiliado'];
                    $numero = $vetor['numeroOrganismoAfiliado'];
                    $cep = $vetor['cepOrganismoAfiliado'];
                    $cidade = $vetor['cidade'];
                    $uf = $vetor['uf'];
                    $telefone = $vetor['telefoneFixoOrganismoAfiliado']." ".$vetor['celularOrganismoAfiliado']." ".$vetor['outroTelefoneOrganismoAfiliado'];
            }
    }

    switch ($_GET['tipo'])
    {
            case 1:
                    $tipoRelatorioOficiais = "Atuantes";
                    $atuantes='S';
                    $naoAtuantes='N';
                    break;
            case 2:
                    $tipoRelatorioOficiais = "Não Atuantes";
                    $atuantes='N';
                    $naoAtuantes='S';
                    break;
            case 3:
                    $tipoRelatorioOficiais = "Atuantes e Não Atuantes";
                    $atuantes='S';
                    $naoAtuantes='S';
                    break;
            default:
                    $tipoRelatorioOficiais = "Atuantes";
                    $atuantes='S';
                    $naoAtuantes='N';
                    break;
    }

    //Tipo Função
    $str = isset($_REQUEST['str'])?$_REQUEST['str']:null;
    $str = substr($str,1,1000000000);
    $arrTipoFuncao = explode(",", $str);

    //Tipo Função
    $strFuncao = isset($_REQUEST['strFuncao'])?$_REQUEST['strFuncao']:null;
    $strFuncao = substr($strFuncao,1,1000000000);
    $arrFuncao = explode(",", $strFuncao);

    //Chamar webservice
    $ocultar_json=1;
    $siglaOA=strtoupper($siglaOrganismoAfiliado);

    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>";print_r($obj);
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 16px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado. " - ".$siglaOrganismoAfiliado; ?>
                    <b class="fonte tamanho-fonte_2">
                            Tipo de Relatório:
                    </b>
                    <?php echo $tipoRelatorioOficiais; ?><br>
                    <b class="fonte tamanho-fonte_2">
                            Endereço:
                    </b>
                    <?php echo $logradouro.", ".$numero; ?>
                    <b class="fonte tamanho-fonte_2">
                            Cep:
                    </b>
                    <?php echo $cep; ?>
                    <b class="fonte tamanho-fonte_2">
                            Cidade:
                    </b>
                    <?php echo $cidade; ?> - <?php echo $uf;?><br>
                    <b class="fonte tamanho-fonte_2">
                            Telefone:
                    </b>
                    <?php echo $telefone; ?>
            </div>
    </center>
    <center><p class="fonte tamanho-fonte_2"><b>DADOS DE OFICIAIS</b></p></center>
    <?php 
    foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor)
    {
            if($vetor['fields']['fCodMembro']!=0)
            {
                //echo "<br>CodTipoFuncao=>".$vetor['fields']['fCodTipoFuncao'];
                //echo "<br><pre>";print_r($arrTipoFuncao);
                if(in_array($vetor['fields']['fCodTipoFuncao'],$arrTipoFuncao))
                {  
                    //echo "<br>CodFuncao=>".$vetor['fields']['fCodFuncao'];
                    //echo "<br>total:";count($arrFuncao);
                    if(in_array($vetor['fields']['fCodFuncao'],$arrFuncao))
                    {    
                        $tipoMembro=1;
                        $seqCadast=$vetor['fields']['fSeqCadast'];
                        include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
                        $obj7 = json_decode(json_encode($return),true);
                        
                        //echo "<pre>";print_r($obj7);
                      
                        $telefoneFixo = $obj7['result'][0]['fields']['fNumTelefoFixo'];
                        $telefoneCelular = $obj7['result'][0]['fields']['fNumCelula'];
                        $telefoneOutro = $obj7['result'][0]['fields']['fNumOutroTelefo'];
                        
                        if($_REQUEST['terminoMandato']!=0)
                        {
                            $filtra=true;
                        }else{
                            $filtra=false;
                        }  
                        
                        if($_REQUEST['terminoMandato']==0||substr($vetor['fields']['fDatTerminMandat'],0,4)==$_REQUEST['terminoMandato']&&$filtra==true)
                        {        
                        
                    ?>
    <table border="1" width="100%">		
            <tr>
                    <td width="20%"><b>Cód. Membro:</b></td>
                    <td width="35%"><?php echo $vetor['fields']['fCodMembro'];?>
                    </td>
                    <td  width="12%"><b>Nome:</b></td>
                    <td><?php echo $vetor['fields']['fNomClient'];?>
                    </td>
            </tr>
            <tr>
                    <td><b>Telefone:</b></td>
                    <td><?php
                        if(trim($vetor['fields']['fNumTelefo'])!="")
                        {    
                            echo $vetor['fields']['fNumTelefo']."<br>";
                        }
                        
                        if(trim($telefoneFixo)!="")
                        {    
                            echo "Fixo: ".substr($telefoneFixo,0,2)." ".substr($telefoneFixo,3,10)."<br>";
                        }
                        if(trim($telefoneCelular)!="")
                        {    
                            echo "Celular: ".substr($telefoneCelular,0,2)." ".substr($telefoneCelular,3,10)."<br>";
                        }
                        if(trim($telefoneOutro)!="")
                        {    
                            echo "Outro Tel: ".substr($telefoneOutro,0,2)." ".substr($telefoneOutro,3,10);
                        }
                        ?>
                    </td>
                    <td><b>E-mail:</b></td>
                    <td><?php echo $vetor['fields']['fDesEmail'];?>
                    </td>
            </tr>
            <tr>	
                    <td><b>Função:</b></td>
                    <td colspan="3"><?php echo $vetor['fields']['fDesFuncao'];?>
                    </td>
            </tr>
            <tr>
                    <td><b>Data de Entrada:</b></td>
                    <td><center>
                    <?php if($vetor['fields']['fDatEntrad']!=""){echo substr($vetor['fields']['fDatEntrad'],8,2)."/".substr($vetor['fields']['fDatEntrad'],5,2)."/".substr($vetor['fields']['fDatEntrad'],0,4); }else{ echo "--";}?>
                            </center>
                    </td>
                    <td><b>Término:</b></td>
                    <td>
                            <center>
                            <?php if($vetor['fields']['fDatTerminMandat']!=""){ echo substr($vetor['fields']['fDatTerminMandat'],8,2)."/".substr($vetor['fields']['fDatTerminMandat'],5,2)."/".substr($vetor['fields']['fDatTerminMandat'],0,4); }else{ echo "--";}?>
                            </center>
                    </td>
            </tr>
            <tr>
                    <td><b>Data da Saída:</b></td>
                    <td>
                            <center>
                            <?php if($vetor['fields']['fDatSaida']!=""){ echo substr($vetor['fields']['fDatSaida'],8,2)."/".substr($vetor['fields']['fDatSaida'],5,2)."/".substr($vetor['fields']['fDatSaida'],0,4); }else{ echo "--";}?>
                            </center>
                    </td>
                    <td><b></b></td>
                    <td>
                            <center>

                            </center>
                    </td>
            </tr>
            <tr>
                    <td>
                            <?php //echo $nomeOrganismo;?>
                    </td>														
                    <td>
                    </td>
                    <td>
                            <?php //echo $nomeOrganismo;?>
                    </td>														
                    <td>
                    </td>
            </tr>
            <?php
                        }
                    }
                }
               ?> 
            </table>
            <?php
            if(in_array($vetor['fields']['fCodTipoFuncao'],$arrTipoFuncao))
            {
                if(in_array($vetor['fields']['fCodFuncao'],$arrFuncao))
                { 
                ?>
                <br><br>
                <?php
                }
            }    
            ?>
            <?php 
            }
    }
}
?>