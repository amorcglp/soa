<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once("../lib/webservice/retornaInformacoesMembro.php");
    include_once('../model/planoAcaoOrganismoClass.php');
    include_once('../lib/functions.php');
    $p = new PlanoAcaoOrganismo();
    $resultado = $p->buscarIdPlanoAcaoOrganismo($_REQUEST['idPlanoAcaoOrganismo']);
    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                            case 1:
                                    $classificacao = "Loja";
                                    break;
                            case 2:
                                    $classificacao = "Pronaos";
                                    break;
                            case 3:
                                    $classificacao = "Capítulo";
                                    break;
                            case 4:
                                    $classificacao = "Heptada";
                                    break;
                            case 5:
                                    $classificacao = "Atrium";
                                    break;
                    }
                    switch ($vetor['tipoOrganismoAfiliado']) {
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $nomeOrganismo 			= $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor["nomeOrganismoAfiliado"];
                    $titulo 				=$vetor['tituloPlano'];
                    $criadoPor 				=$vetor['nomeUsuario'];
                    $descricao				=$vetor['descricaoPlano'];
                    $prioridade				=prioridade($vetor['prioridade']);
                    $palavrasChave			=$vetor['palavrasChave'];
            }
    }

    /**
     * Atualizar percentual do Plano de Ação
     */
    include_once("../model/planoAcaoOrganismoMetaClass.php");
    $parm = new planoAcaoOrganismoMeta();

    $resultadoMeta = $parm->listaMeta($_REQUEST['idPlanoAcaoOrganismo']);	

    if($resultadoMeta)
    {
            $totalMeta = count($resultadoMeta);
    }else{
            $totalMeta = 0;
    }

    $resultadoMetaConcluida = $parm->listaMeta($_REQUEST['idPlanoAcaoOrganismo'],1);	

    if($resultadoMetaConcluida)
    {
            $totalMetaConcluida = count($resultadoMetaConcluida);
    }else{
            $totalMetaConcluida = 0;
    }


    /*
     * Cálculo da percentagem concluída
     */
    if($totalMetaConcluida>0)
    {
            $percentual = round(($totalMetaConcluida*100)/$totalMeta);
    }else{
            $percentual = 0;
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Arial;
    font-size: 14px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <!--
    <div style="width: 225px; height: 105px; margin-bottom: 30px">
      <img class="solado" src="../img/solado_atas.png">
    <center><p class="fonte tamanho-fonte" style="margin-top: 8px"><b>Grande Loja da Jurisdição <br>de Língua Portuguesa, AMORC</b></p></center><br>
    </div>
    -->
    <center><p><b>PLANO DE AÇÃO DO ORGANISMO</b></p></center>
    <hr>
    <h2>Plano: <?php echo $titulo;?></h2>
    <p><b>Criado por</b>: <?php echo $criadoPor;?></p>
    <p><b>Organismo</b>: <?php echo $nomeOrganismo;?></p>
    <p><b>Participantes</b>:
    <?php 
    include_once('../model/planoAcaoOrganismoParticipanteClass.php');
    $parp = new planoAcaoOrganismoParticipante();
    $resultado2 = $parp->listaParticipantes($_REQUEST['idPlanoAcaoOrganismo']);

    $i=0; 
    if($resultado2)
    {
            foreach($resultado2 as $vetor2)
            {
                    if($i==0)
                    {
                            echo retornaNomeCompleto($vetor2['seqCadast']);
                    }else{
                            echo ", ".retornaNomeCompleto($vetor2['seqCadast']);
                    }
                    $i++;

            }
    }
    ?>
    </p>
    <p>
            <b>Progresso</b>:
            <?php echo $percentual;?>
            %
    </p>
    <p align="justify">
            <b>Descrição</b>
            <?php echo $descricao;?>
    </p>
    <p>
            <b>Prioridade</b>:
            <?php echo $prioridade;?>
    </p>
    <p>
            <b>Palavras-chave</b>:
            <?php echo $palavrasChave;?>
    </p>
    <?php 
    include_once('../model/planoAcaoOrganismoMetaClass.php');
    $meta = new planoAcaoOrganismoMeta();
    $resultado3 = $meta->listaMeta($_REQUEST['idPlanoAcaoOrganismo']);

    $i=1; 
    if($resultado3)
    {
            ?>
            <h2>Metas</h2>
            <?php 
            foreach($resultado3 as $vetor3)
            {
            ?>
            <table border=1 width="100%">
                    <tr>	
                            <td width="15%"><b>Meta <?php echo $i;?></b></td>
                            <td width="85%"><?php echo $vetor3['tituloMeta'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Início</b></td>
                            <td width="85%"><?php echo substr($vetor3['inicio'],8,2)."/".substr($vetor3['inicio'],5,2)."/".substr($vetor3['inicio'],0,4);?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Fim</b></td>
                            <td width="85%"><?php echo substr($vetor3['fim'],8,2)."/".substr($vetor3['fim'],5,2)."/".substr($vetor3['fim'],0,4);?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>O que?</b></td>
                            <td width="85%"><?php echo $vetor3['oque'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Por que?</b></td>
                            <td width="85%"><?php echo $vetor3['porque'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Quem?</b></td>
                            <td width="85%"><?php echo $vetor3['quem'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Onde?</b></td>
                            <td width="85%"><?php echo $vetor3['onde'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Quando?</b></td>
                            <td width="85%"><?php echo $vetor3['quando'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Como?</b></td>
                            <td width="85%"><?php echo $vetor3['como'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Quanto?</b></td>
                            <td width="85%"><?php echo $vetor3['quanto'];?></td>
                    </tr>
                    <tr>	
                            <td width="15%"><b>Status</b></td>
                            <td width="85%"><?php if($vetor3['statusMeta']==1){ echo "Completa";}else{echo "Incompleta";}?></td>
                    </tr>
            </table>
            <br><br>
            <?php
                    $i++; 
            }
    }	
    ?>
    <?php 
    include_once('../model/planoAcaoOrganismoAtualizacaoClass.php');
    $atualizacao = new planoAcaoOrganismoAtualizacao();
    $resultado4 = $atualizacao->listaAtualizacao($_REQUEST['idPlanoAcaoOrganismo']);

    if($resultado4)
    {?>
    <h2>Atualizações</h2>
    <?php 
            foreach($resultado4 as $vetor4)
            {
            ?>
            <table width="100%">
                    <tr>	
                            <td><b><?php echo $vetor4['nomeUsuario'];?></b> postou a mensagem.</td>
                    </tr>
                    <tr>	
                            <td><?php echo substr($vetor4['dataCadastro'],8,2)."/".substr($vetor4['dataCadastro'],5,2)."/".substr($vetor4['dataCadastro'],0,4);?> às <?php echo substr($vetor4['dataCadastro'],11,8)?></td>
                    </tr>
                    <tr>	
                            <td><?php echo $vetor4['mensagem'];?></td>
                    </tr>
            </table>
            <hr>
            <?php
                    $i++; 
            }
    }	
}    
?>