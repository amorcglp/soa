<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    if(!class_exists("Ws"))
    {
        if(realpath('../../model/wsClass.php')){
                require '../../model/wsClass.php';
        }else{
                if(realpath('../model/wsClass.php')){
                        require '../model/wsClass.php';	
                }else{
                        require './model/wsClass.php';
                }
        } 
    }
    require_once '../lib/functions.php';
    
    $anoRosacruzSelecionado = isset($_REQUEST['anoRosacruz'])?$_REQUEST['anoRosacruz']:null;
    
    $dataEmissaoSelecionado = isset($_REQUEST['dataEmissao'])?$_REQUEST['dataEmissao']:null;
    
    //Corte da data de Emissão
    $diaSelecionado = substr($dataEmissaoSelecionado,0,2);
    $mesNumerico = (int) substr($dataEmissaoSelecionado,3,2);
    $mesSelecionado = mesExtensoPortugues($mesNumerico);
    $anoSelecionado = substr($dataEmissaoSelecionado,6,4);
    
    //Tipo Função
    $str = isset($_REQUEST['str'])?$_REQUEST['str']:null;
    $str = substr($str,1,1000000000);
    $arrTipoFuncao = explode(",", $str);

    //Tipo Função
    $strFuncao = isset($_REQUEST['strFuncao'])?$_REQUEST['strFuncao']:null;
    $strFuncao = substr($strFuncao,1,1000000000);
    $arrFuncao = explode(",", $strFuncao);
    //echo "<pre>";print_r($arrFuncao);
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 16px!important;
    color: #A52A2A;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style>
        @page {size:landscape;}
@media print{
*{
margin:0;
padding:0;
}
.LandscapeDiv{
	width: 100%;
	height: 100%;
	filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=3);
}
}
</style>
    <style media="print">
            .oculto {display: none;}
    </style>
    <link rel="stylesheet" href="../fonts/monotype/font.css">
    <div id="conteiner" class="LandscapeDiv">
    <?php 
    include_once('../model/organismoClass.php');
    $o = new organismo();
    if($_GET['idRegiao']!=0)
    {    
        $resultado = $o->listaOrganismo(null,null,null,null,$_GET['idRegiao'],null,null,null,null,null,null,1);//Filtrar por região
    }else{
        $resultado = $o->listaOrganismo(null,null,null,null,null,null,null,null,null,null,null,1);//Pegar todas as regiões
    }
    if($_GET['idOrganismoAfiliado']!="Todos")
    {    
        $resultado = $o->listaOrganismo(null,null,null,null,null,$_GET['idOrganismoAfiliado'],null,null,null,null,null,1);//Filtrar por OA
    }    
    //echo "<pre>";print_r($resultado);
    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                switch($vetor['classificacaoOrganismoAfiliado']){
                        case 1:
                                $classificacao =  "Loja";
                                break;
                        case 2:
                                $classificacao =  "Pronaos";
                                break;
                        case 3:
                                $classificacao =  "Capítulo";
                                break;
                        case 4:
                                $classificacao =  "Heptada";
                                break;
                        case 5:
                                $classificacao =  "Atrium";
                                break;
                }
                switch($vetor['tipoOrganismoAfiliado']){
                        case 1:
                                $tipo = "R+C";
                                break;
                        case 2:
                                $tipo = "TOM";
                                break;
                }
                switch ($vetor['paisOrganismoAfiliado'])
                {
                    case 1:
                        $siglaPais = "BR";
                        break;
                    case 2:
                        $siglaPais = "PT";
                        break;
                    case 3:
                        $siglaPais = "AO";
                        break;
                    case 4:
                        $siglaPais = "MZ";
                        break;
                    default :
                        $siglaPais = "BR";
                        break;
                }

                $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
                /*
                $logradouro = $vetor['enderecoOrganismoAfiliado'];
                $numero = $vetor['numeroOrganismoAfiliado'];
                $cep = $vetor['cepOrganismoAfiliado'];
                $cidade = $vetor['cidade'];
                $uf = $vetor['uf'];
                $telefone = $vetor['telefoneFixoOrganismoAfiliado']." ".$vetor['celularOrganismoAfiliado']." ".$vetor['outroTelefoneOrganismoAfiliado'];
                */
                
                $nomeOrganismo = $classificacao . " " . $tipo . " " . $vetor["nomeOrganismoAfiliado"] . ",  AMORC";

                switch ($_GET['tipo'])
                {
                        case 1:
                                $tipoRelatorioOficiais = "Atuantes";
                                $atuantes='S';
                                $naoAtuantes='N';
                                break;
                        case 2:
                                $tipoRelatorioOficiais = "Não Atuantes";
                                $atuantes='N';
                                $naoAtuantes='S';
                                break;
                        case 3:
                                $tipoRelatorioOficiais = "Atuantes e Não Atuantes";
                                $atuantes='S';
                                $naoAtuantes='S';
                                break;
                        default:
                                $tipoRelatorioOficiais = "Atuantes";
                                $atuantes='S';
                                $naoAtuantes='N';
                                break;
                }



                //Chamar webservice
                $ocultar_json=1;
                $siglaOA=strtoupper($siglaOrganismoAfiliado);
                //echo "SIGLA=>".$siglaOA;
                //echo "<br>==================================";



                //$seqFuncao=0;
                foreach($arrFuncao as $fun)
                {    
                    $seqFuncao = $fun;

                    $seqCadast 	= 0;

                    $server = 135;//PEGANDO DE PRODUÇÃO 
                    // Instancia a classe
                    $ws = new Ws($server);
                    // Nome do Método que deseja chamar
                    $method = 'RetornaRelacaoOficiais';

                    // Parametros que serão enviados à chamada
                    $params = array('CodUsuario' => 'lucianob',
                                                    'SeqCadast' => 0,
                                                    'SeqFuncao' => $seqFuncao,
                                                    'NomParcialFuncao' => '',
                                                    'NomLocaliOficial' => '',
                                                    'SigRegiaoBrasilOficial' => '',
                                                    'SigPaisOficial' => $siglaPais,
                                                    'SigOrganismoAfiliado' => $siglaOA,
                                                    'SigAgrupamentoRegiao' => '',
                                                    'IdeListarAtuantes' => $atuantes,
                                                    'IdeListarNaoAtuantes' => $naoAtuantes,
                                    'IdeTipoMembro' => '1'
                                            );

                    // Chamada do método
                    $return = $ws->callMethod($method, $params, 'lucianob');

                    $obj = json_decode(json_encode($return),true);
                    //echo "<pre>";print_r($obj);
                    ?>


                    <?php 
                    foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor)
                    {
                            if($vetor['fields']['fCodMembro']!=0)
                            {
                                //echo "<br>CodTipoFuncao=>".$vetor['fields']['fCodTipoFuncao'];
                                //echo "<br><pre>";print_r($arrTipoFuncao);
                                if(in_array($vetor['fields']['fCodTipoFuncao'],$arrTipoFuncao))
                                {  
                                    //echo "<br>CodFuncao=>".$vetor['fields']['fCodFuncao'];
                                    //echo "<br>total:";count($arrFuncao);
                                    if(in_array($vetor['fields']['fCodFuncao'],$arrFuncao))
                                    { 
                                        if($_REQUEST['terminoMandato']!=0)
                                        {
                                            $filtra=true;
                                        }else{
                                            $filtra=false;
                                        }  

                                        if($_REQUEST['terminoMandato']==0||substr($vetor['fields']['fDatTerminMandat'],0,4)==$_REQUEST['terminoMandato']&&$filtra==true)
                                        {
                        
                    ?>
    <center>
    <table border=0 width="100%">
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>    
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>

                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
            <tr>
                    <td style="font-family: 'times';font-size:24"><div style="width:100%;height: 120px"></div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <i>
                            <?php 
                            $arrNome = explode(" ",$vetor['fields']['fNomClient']);
                            foreach($arrNome as $v)
                            {  
                                if(strlen($v)==2)//Se for preposição
                                {
                                    echo mb_strtolower($v,'UTF-8')." ";
                                }else{
                                    echo ucfirst(mb_strtolower($v,'UTF-8'))." ";
                                }
                            }
                            ?>
                            </i>    
                    </td>
            </tr>
            <tr>
                    <td align="left" width="100%">
                        <div style="width:100%;height: -12px">
                       <table width="100%">
                           <tr>
                                <td width="74%">&nbsp;</td>   
                                <td width="26%" style="font-family: 'times';font-size:18">
                                    <?php 
                                    
                                     $arrNome = explode(" ",$vetor['fields']['fDesFuncao']);

                                     if(isset($arrNome[0]))//1ª palavra
                                     {
                                          echo ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ";
                                          if($arrNome[1]=="REG."||$arrNome[1]=="REG"||$arrNome[0]=="ORADOR")
                                          {
                                              echo "Regional ";
                                          }
                                          if($arrNome[1]=="CONSELHEIRO")
                                          {
                                              echo "Conselheiro ";
                                          }
                                     }
                                    ?>
                                </td>
                           </tr>
                       </table>
                        </div> 
                    </td>    
            </tr>
            
            <tr>
                <td style="font-family: 'times';font-size:18">
                    <div style="width:100%;height: 22px">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php 
                            //Resto do Nome da Função
                            if(isset($arrNome[1]))
                            {    
                                for($i=1;$i<count($arrNome);$i++){
                                    if($arrNome[1]!="REG."&&$arrNome[1]!="REG"&&$arrNome[0]!="ORADOR"&&$arrNome[1]!="CONSELHEIRO")
                                    {
                                      if(strlen($arrNome[$i])==2)//Se for preposição
                                      {
                                          echo mb_strtolower($arrNome[$i],'UTF-8')." ";
                                      }else{    
                                          echo ucfirst(mb_strtolower($arrNome[$i],'UTF-8'))." ";
                                      }
                                    }
                                }
                            }
                        ?>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if(in_array(100, $arrTipoFuncao)&&in_array(800,$arrTipoFuncao)){?>Região <?php echo strtoupper(substr($siglaOrganismoAfiliado,0,3));?><?php }else{ echo strtoupper($nomeOrganismo);}?>
                    </td>    
            </tr>
            <tr>
                <td style="font-family: 'times';font-size:18;">
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if($vetor['fields']['fDatTerminMandat']!=""){ echo substr($vetor['fields']['fDatTerminMandat'],8,2)."/".substr($vetor['fields']['fDatTerminMandat'],5,2)."/".substr($vetor['fields']['fDatTerminMandat'],0,4); }else{ echo "--";}?>
                            
                    </td>
            </tr>
            <tr>
                <td style="font-family: 'times';font-size:18">
                    <center>
                    <table border="0" width="100%">
                        <tr> 
                            
                            <td width="61%" style="font-family: 'times';font-size:18">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <?php echo $anoRosacruzSelecionado;?></td>
                            <td width="12%" style="font-family: 'times';font-size:18">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i><b><?php //echo substr($vetor['fields']['fDatEntrad'],8,2);?><?php echo $diaSelecionado;?></b></i></td>
                            <td width="11%" style="font-family: 'times';font-size:18">&nbsp;&nbsp;<i><b><?php //echo mesExtensoPortugues(substr($vetor['fields']['fDatEntrad'],5,2));?><?php echo $mesSelecionado;?></b></i></td>
                            <td width="16%" style="font-family: 'times';font-size:18"><i><b><?php //echo substr($vetor['fields']['fDatEntrad'],0,4);?><?php echo $anoSelecionado;?></b></i></td>
                        </tr>
                    </table>        
                    </center>    
                    </td>
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            <tr>
                    <td>
                        &nbsp;
                    </td>    
            </tr>
            
            
            
            
            
            <?php
                        }
                    }
                }
               ?> 
            </table>
    </center>
            <div style="page-break-after: always"></div>
            <?php 
                                
                            
                            
                            }
                    }
                }
            }
    }
}
?>
    </div>