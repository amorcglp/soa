<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $mesAtual                       = isset($_REQUEST['mesAtual']) ? $_REQUEST['mesAtual'] : null;
    $anoAtual                       = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual'] : null;
    $idOrganismoAfiliado            = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : null;
    $numeroMembrosAtivosAnterior    = isset($_REQUEST['numeroMembrosAtivosAnterior']) ? $_REQUEST['numeroMembrosAtivosAnterior'] : 0;
    $elevacaoLojaWS                   = isset($_REQUEST['elevacaoLoja']) ? $_REQUEST['elevacaoLoja'] : "";
    $elevacaoCapituloWS               = isset($_REQUEST['elevacaoCapitulo']) ? $_REQUEST['elevacaoCapitulo'] : "";
    $elevacaoPronaosWS                = isset($_REQUEST['elevacaoPronaos']) ? $_REQUEST['elevacaoPronaos'] : "";
    
    //Tratamento das datas
    $elevacaoLoja                   = substr($elevacaoLojaWS,0,7);
    $elevacaoCapitulo               = substr($elevacaoCapituloWS,0,7);
    $elevacaoPronaos                = substr($elevacaoPronaosWS,0,7);
    
    
    
    $elevacaoLoja                   = $elevacaoLoja."-01";
    $elevacaoCapitulo               = $elevacaoCapitulo."-01";
    $elevacaoPronaos                = $elevacaoPronaos."-01";
    $dataAtual                      = $anoAtual."-".$mesAtual."-01";

    include_once('../lib/functions.php');
    include_once('../controller/organismoController.php');
    include_once('../controller/atividadeEstatutoController.php');
    include_once('../controller/atividadeEstatutoTipoController.php');
    include_once('../controller/atividadeIniciaticaController.php');


    $aim = new atividadeIniciatica();
    $aic = new atividadeIniciaticaController();

    $aem = new atividadeEstatuto();
    $aec = new atividadeEstatutoController();

    $om = new organismo();
    $oc = new organismoController();

    /*
    $dadosOrganismo     = $oc->buscaOrganismo($idOrganismoAfiliado);

    $retornoEstatutoTipo = $aem->listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacaoOrganismoAfiliado);
    if($retornoEstatutoTipo){
        foreach($retornoEstatutoTipo as $vetorEstatutoTipo){
            $idTipoAtividadeEstatuto        = $vetorEstatutoTipo['idTipoAtividadeEstatuto'];
            $nomeTipoAtividadeEstatuto      = $vetorEstatutoTipo['nomeTipoAtividadeEstatuto'];
            $qntTipoAtividadeEstatuto       = $vetorEstatutoTipo['qntTipoAtividadeEstatuto'];
            $qntAtividades = $aem->listaAtividadeEstatutoParaRelatorio($idOrganismoAfiliado,$mesAtual,$anoAtual,$idTipoAtividadeEstatuto);
            if($qntAtividades >= $qntTipoAtividadeEstatuto){
                echo "<li><span class='fa fa-thumbs-up'></span> Atividade <label>" . $nomeTipoAtividadeEstatuto . "</label> (".$qntAtividades."/".$qntTipoAtividadeEstatuto.")</li>";
            } else {
                echo "<li><span class='fa fa-thumbs-down'></span> Atividade <label>" . $nomeTipoAtividadeEstatuto . "</label> (".$qntAtividades."/".$qntTipoAtividadeEstatuto.")</li>";
            }
        }
    }
    echo "<li><span class='fa fa-thumbs-up'></span> <label>Rosacruzes Ativos no Organismo Afiliado</label>: ".$numeroMembrosAtivosAnterior."</li>";
    */


    /**
     * *************************************************************************************************************************************************************************************
     */


    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        table , body{
            margin: 0;
            text-align: left;
            font-family: Avenir LT Std;
            font-size: 12px!important;
            color: #000;
            border-collapse:collapse;
            border-color:#DCDCDC;
            max-width: 100%;
            border-spacing: 0;
        }
        .fonte{
            font-family: Minion Pro;
        }

        .tamanho-fonte{
            font-size: 14px;
        }

        .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
        }

        .fonte-3 {
            font-family: Avenir LT Std;
        }

        .cinza{
            background-color:#F5F5F5;
        }
        .centro{
            text-align:center;
        }

        /* Table Base */

        .table {
          width: 100%;
        }

        .table th,
        .table td {
          font-weight: normal;
          font-size: 15px;
          padding: 8px 15px;
          line-height: 20px;
          text-align: left;
          vertical-align: middle;
          
        }
        .table thead th {
          vertical-align: bottom;
          font-weight:bold;
        }
        .table .t-small {
          width: 5%;
        }
        .table .t-medium {
          width: 10%;
        }
        .table .t-nome {
          width: 30%;
        }
        .table .t-status {
          font-weight: bold;
        }
        .table .t-active {
          color: #46a546;
        }
        .table .t-inactive {
          color: #e00300;
        }
        .table .t-draft {
          color: #f89406;
        }
        .table .t-scheduled {
          color: #049cdb;
        }

        /* Small Sizes */
        @media (max-width: 480px) {
          .table-action thead {
            display: none;
          }
          .table-action tr {
            border-bottom: 1px solid #dddddd;
          }
          .table-action td {
            border: 0;
          }
          .table-action td:not(:first-child) {
            display: block;
          }
        }

        #cabecalho {height: 90px}
        #oa {width: 260px; height: 19px; padding-left: 14px}
        #data {width: 131px; height: 19px}
        #ano {width: 61px; height: 19px}

        .solado {}

    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2" style="margin-bottom: 2px"><b>RELATÓRIO MENSAL DE COMPROMISSO AO ACORDO DE AFILIAÇÃO</b></p></center>
    <center><p class="fonte tamanho-fonte_2"><b>E OBRIGAÇÕES ESTATUTÁRIAS.</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado); ?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Mês:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo mesExtensoPortugues($mesAtual); ?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $anoAtual; ?>
                    </span>
            </div>
    </center>
    <br>
    <?php
    
    $elevacao="";
    $textoClassificacao="";
    if($elevacaoPronaosWS!=""&&$dataAtual>=$elevacaoPronaos) {
        $elevacao=2;
        $classificacao=2;
        $textoClassificacao="Pronaos";
    }
    if($elevacaoCapituloWS!=""&&$dataAtual>=$elevacaoCapitulo) {
        $elevacao=3;
        $classificacao=3;
        $textoClassificacao="Capítulo";
    }
    if($elevacaoLojaWS!=""&&$dataAtual>=$elevacaoLoja) {
        $elevacao=1;
        $classificacao=1;
        $textoClassificacao="Loja";
    }
    
    $dadosOrganismo     = $oc->buscaOrganismo($idOrganismoAfiliado);
    $atuacaoTemporaria  = $dadosOrganismo->getAtuacaoTemporaria();
    
    if($elevacaoLoja=="-01"&&$elevacaoCapitulo=="-01"&&$elevacaoPronaos=="-01")
    {
        $classificacao = $dadosOrganismo->getClassificacaoOrganismoAfiliado();
        $classificacaoNoSoa = $dadosOrganismo->getClassificacaoOrganismoAfiliado();
    }
    
    if($atuacaoTemporaria!=0)
    {
        $classificacao=$atuacaoTemporaria;
        switch ($atuacaoTemporaria) {
            case 1:
                $textoClassificacao = "Loja";
                break;
            case 2:
                $textoClassificacao = "Pronaos";
                break;
            case 3:
                $textoClassificacao = "Capítulo";
                break;
            case 4:
                $textoClassificacao = "Heptada";
                break;
            case 5:
                $textoClassificacao = "Atrium";
                break;
            default:
                $textoClassificacao = "Loja";
                break;
        }
    }    

    $classificacao = $aem->listaAtividadeEstatutoParaRelatorioGetClassificacao($idOrganismoAfiliado,$mesAtual,$anoAtual);
    
    $retornoEstatutoTipo = $aem->listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacao,true,$mesAtual);
    
//teste
    ?>
    <center>
        <table border="1" class="table table-action">
            <thead>
                <tr>
                    <th width="5%"><b>Itens</b></th>
                    <th width="45%"><b>Descrição da Atividade para <?php echo $textoClassificacao;?></b></th>
                    <th width="16%"><center><b>Acordo<br> de<br> Afiliação</b></center></th>
                    <th width="17%"><center><b>Número<br> de<br> Atividades</b></center></th>
                    <th width="17%"><center><b>Número<br> de<br> Participantes</b></center></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i=1;
                if($retornoEstatutoTipo){
                    foreach($retornoEstatutoTipo as $vetorEstatutoTipo){
												$exibir = false;
												$qntAtividades = $aem->listaAtividadeEstatutoParaRelatorio($idOrganismoAfiliado,$mesAtual,$anoAtual,$vetorEstatutoTipo['idTipoAtividadeEstatuto']);
												if($vetorEstatutoTipo['qntTipoAtividadeEstatuto'] == 0){
														if($qntAtividades > 0){
																$exibir = true;
														}
												} else {
														$exibir = true;
												}
												if($exibir){
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $vetorEstatutoTipo['nomeTipoAtividadeEstatuto'] ?></td>
                    <td><center><?php echo $vetorEstatutoTipo['qntTipoAtividadeEstatuto'] ?></center></td>
                    <td>
                        <center>
                        <?php
                        echo $qntAtividades>0 ? $qntAtividades : "Não houve";
                        ?>
                        </center>
                    </td>
                    <td>
                        <center>
                            <?php
                            $qntParticipantes = $aem->retornaNumeroDeParticipantes($idOrganismoAfiliado,$mesAtual,$anoAtual,$vetorEstatutoTipo['idTipoAtividadeEstatuto']);
                            if($qntParticipantes){
                                foreach($qntParticipantes as $VetorQntParticipantes){
                                    echo $VetorQntParticipantes['nroParticipantes'];
                                    if($VetorQntParticipantes['nroParticipantes'] == null){
                                        echo "--";
                                    }
                                }
                            }
                            ?>
                        </center>
                    </td>
                </tr>
                <?php
                    $i++;
												}
                    }
                }
                if($dadosOrganismo->getClassificacaoOrganismoAfiliado() == 1){
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td>Iniciação de Grau de Templo</td>
                    <td><center>1</center></td>
                    <td>
                        <center>
                            <?php
                            $qntAtividadesIniciaticas = $aim->retornaNumeroDeAtividadesParaRelatorio($idOrganismoAfiliado,$mesAtual,$anoAtual);
                            echo $qntAtividadesIniciaticas>0 ? $qntAtividadesIniciaticas : "Não houve";
                            ?>
                        </center>
                    </td>
                    <td>
                        <center>
                            <?php
                            $qntParticipantesIniciaticas = $aim->retornaNumeroDeParticipantes($idOrganismoAfiliado,$mesAtual,$anoAtual);
                            echo $qntParticipantesIniciaticas>0 ? $qntParticipantesIniciaticas : "--";
                            ?>
                        </center>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td colspan="3">Rosacruzes Ativos no Organismo Afiliado</td>
                    <td><center><?php echo $numeroMembrosAtivosAnterior; ?></center></td>
                </tr>
            </tbody>
        </table>
    </center>
    <br>
    <table class="table">
        <tbody>
            <center>
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="25%" style="text-align: right">Data:</td>
                    <td width="25%" style="text-align: left">
                        <?php
                            echo date('d/m/Y');
                        ?>
                    </td>
                    <td width="25%">&nbsp;</td>
                </tr>
            </center>
        </tbody>
    </table>
    <?php
    require_once '../model/atividadeEstatutoMensalClass.php';
    $aem = new atividadeEstatutoMensal();
    $resultado4 = $aem->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado);
    $assinaturas="<br>";
    if($resultado4)
    {
    foreach($resultado4 as $vetor4)
    {
    $nomeUsuario = $vetor4['nomeUsuario'];
    $codigoAfiliacao = $vetor4['codigoDeAfiliacao'];
    $funcao = $vetor4['nomeFuncao'];

    $assinaturas .= $nomeUsuario." [".$codigoAfiliacao."]"." - ".$funcao."<br>";
    }
    }

    //Verificar se está entregue
    $resultado = $aem->listaAtividadeMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
    if($resultado)
    {
    foreach ($resultado as $vetor)
    {
    if(trim($vetor['numeroAssinatura'])!="")
    {
    $numeroAssinatura = $vetor['numeroAssinatura'];
    $temNumeroAssinatura=true;
    }else{
    $numeroAssinatura = aleatorioAssinatura();
    $aem->atualizaNumeroAssinatura($mesAtual,$anoAtual,$idOrganismoAfiliado,$numeroAssinatura);
    }
    }
    $pronto = true;
    }else{
    $pronto = false;
    }
    ?>
    <br>
    <?php if($pronto){?>
    <br><br>
    <b>Concordaram e assinaram eletrônicamente este documento:</b><br><br>
<?php }?>

    <center><?php if($pronto){
    if($assinaturas!="<br>"){echo $assinaturas;}else{ echo "NINGUÉM ASSINOU ESSE DOCUMENTO";}?></center><br>
    <hr>
    <center><b>Código da Assinatura Eletrônica: <font color="blue"><?php echo $numeroAssinatura; ?></font></b>
        <br><br>(Documento gerado eletronicamente, para validar sua autenticidade utilize a opção validar documento dentro do sistema SOA)
    </center>
<?php }else{
    echo "NINGUÉM ASSINOU ESSE DOCUMENTO ELETRONICAMENTE<br><br>";
}?>
<?php
}
?>
