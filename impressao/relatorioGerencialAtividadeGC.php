<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    set_time_limit(0);
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    ini_set("display_errors", 1 );

    $str = isset($_REQUEST['str'])?$_REQUEST['str']:null;
    
    $str2 = isset($_REQUEST['str2'])?$_REQUEST['str2']:null;
    
    $str3 = isset($_REQUEST['str3'])?$_REQUEST['str3']:null;

    $classificacaoOa = isset($_REQUEST['classificacaoOa'])?$_REQUEST['classificacaoOa']:null;
    $dataInicial = isset($_REQUEST['dataInicial'])?$_REQUEST['dataInicial']:null;
    $dataFinal = isset($_REQUEST['dataFinal'])?$_REQUEST['dataFinal']:null;

    $mesInicio = (int) substr($dataInicial,3,2);
    $anoInicio = substr($dataInicial,6,4);

    $dtInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
    $dtFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);

    $date1 = new DateTime($dtInicial); 
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2); 
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

    $totalIntervalo = $totalDeMeses+1;
    
    include_once('../lib/functions.php');
    include_once('../model/ataReuniaoMensalClass.php');
    include_once('../model/dividaClass.php');
    include_once('../model/quitacaoDividaClass.php');
    include_once('../model/ataReuniaoMensalAssinadaClass.php');
    include_once('../model/organismoClass.php');
    include_once('../model/recebimentoClass.php');
    include_once('../model/despesaClass.php');
    include_once('../model/membrosRosacruzesAtivosClass.php');
    include_once '../model/atividadeEstatutoTipoClass.php';
    include_once '../model/atividadeEstatutoClass.php';
    include_once('../controller/atividadeIniciaticaController.php');



    $a                                                      = new atividadeEstatutoTipo();
    $b                                                      = new atividadeEstatuto();
    $ataReuniaoMensal					= new ataReuniaoMensal();
    $ataReuniaoMensalAssinada				= new ataReuniaoMensalAssinada();
    $divida         					= new Divida();
    $quitacaoDivida       					= new QuitacaoDivida();
    $r 							= new Recebimento();
    $d 							= new Despesa();
    $o 							= new organismo();
    $mra                                                    = new MembrosRosacruzesAtivos();
    $aim                                                = new atividadeIniciatica();


    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>RELATÓRIO GERENCIAL</b></p></center>
    <?php 
    if($_REQUEST['filtro']==1)
    {    
        $regiao = $_REQUEST['regiao'];
        $oa = null;
    }else{
        $regiao = null;
        $oa = $_REQUEST['oa'];
    }
    $resultado = $o->listaOrganismo(null,null,null,null,$regiao,$oa,null,null,null,null,$str3,1);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];    
                    $classificacaoOa = $vetor['classificacaoOrganismoAfiliado'];

    ?>
    <br>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            Mês Inicial:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo substr($_REQUEST['dataInicial'],3,7); ?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Mês Final:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo substr($_REQUEST['dataFinal'],3,7); ?>
                    </span>
            </div>
    </center>
    <?php
    $arrAtividadesMarcadas=  explode(",", $str);
    $arrAtividadesMarcadas2=  explode(",", $str2);

    $y= (int) $anoInicio;
    $totalMesesSelecionado= $totalIntervalo+$mesInicio;
    $totalMeses = $totalMesesSelecionado;
    $mesASerInformado=$mesInicio;
    //echo "totalMeses=>".$totalMeses;exit();
    ?>
    <br>
    <table border="1" width="100%">
      <tr>
        <th rowspan="2" align="center">ITEM</th>  
        <th rowspan="2" align="center">Descrição da Atividade</th>
        <th rowspan="2" align="center">Acordo de Afiliaç.</th>
        <?php
        for($i=$mesInicio;$i<$totalMeses;$i++)
        {
            $mesExtenso = mesExtensoPortugues($mesASerInformado);
        ?>
        <th colspan="2" align="center"><?php echo $mesExtenso;?></th>
        <?php
            if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
            {
                $y++;
                $mesASerInformado=1;
            }else{
                $mesASerInformado++;
            }
        }
        ?>

      </tr>
      <tr>
          <?php
        for($i=$mesInicio;$i<$totalMeses;$i++)
        {
            $mesExtenso = mesExtensoPortugues($mesASerInformado);
        ?>
        <th align="center">Nº Ativid.</th>
        <th align="center">Nº Partic.</th>
        <?php 
        }?>
      </tr>
    <?php
    $t=1;
    $resultado = $a->listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacaoOa,null,1);

            if($resultado)
            {
                foreach($resultado as $vetor)
                {   
                    if(in_array($vetor['idTipoAtividadeEstatuto'], $arrAtividadesMarcadas))
                    {        
            ?>
              <tr>
                <td align="center"><?php echo $t;?></td>  
                <td align="center"><?php echo $vetor['nomeTipoAtividadeEstatuto'];?></td>
                <td align="center"><?php echo $vetor['qntTipoAtividadeEstatuto']; ?></td>
                <?php  
                 $y= (int) $anoInicio;
                    for($i=$mesInicio;$i<$totalMeses;$i++)
                    {
                       
    ?>
                    <td align="center">
                            <?php 
                                //Número de Atividades
                                $qtdAtividades = $b->listaAtividadeEstatutoParaRelatorio($idOrganismoAfiliado,$i,$y,$vetor['idTipoAtividadeEstatuto']);
                                if($qtdAtividades>0)
                                {
                                    echo $qtdAtividades;
                                }else{
                                    echo "--";
                                }            
                            ?>
                    </td>
                    <td align="center">
                            <?php 
                                //Número de Participantes
                                $resultado2 = $b->retornaNumeroDeParticipantes($idOrganismoAfiliado, $i, $y, $vetor['idTipoAtividadeEstatuto']); 
                                if($resultado2)
                                {
                                    $nroParticipantes = $resultado2[0]['nroParticipantes'];
                                    if($nroParticipantes>0)
                                    {    
                                        echo $nroParticipantes;
                                    }else{
                                        echo "--";
                                    }
                                }    
                            ?>
                    </td>
                <?php 
                            if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                            {
                                $totalMeses = $totalMeses - 12;
                                $i=0;
                                $y++;
                            }
                    }
                    ?> 
              </tr> 
              
            <?php   
                        $t++;
                    }

                }
            ?>
              <?php if($classificacaoOa==1){?>  
              
              <?php
                     $y= (int) $anoInicio;
                     $totalMeses = $totalMesesSelecionado;
                    for($q=1;$q<=12;$q++){
                        if(in_array($q, $arrAtividadesMarcadas2))
                        {        
                  ?>
                <tr> 
                    <td align="center"><?php echo $t;?></td>
                    <td align="center">Iniciação ao <?php echo $q;?>º Grau de Templo</td>
                    <td align="center">1 (todos os graus)</td>
                    <?php 
                            for($i=$mesInicio;$i<$totalMeses;$i++)
                            {  
                                $qntAtividadesIniciaticas = $aim->retornaNumeroDeAtividadesParaRelatorio($idOrganismoAfiliado,$i,$y,$q);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntAtividadesIniciaticas>0){
                                                echo $qntAtividadesIniciaticas;    
                                            }else{
                                                echo "--";
                                            }
                                        ?>
                                </td>
                            <?php 
                                $qntParticipantes = $aim->retornaNumeroDeParticipantes($idOrganismoAfiliado,$i,$y,$q);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntParticipantes>0||$qntParticipantes!=""){
                                                echo $qntParticipantes;    
                                            }else{
                                                echo "--";
                                            }
                                        ?>
                                </td>
                            <?php    
                                if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                                {
                                    $totalMeses = $totalMeses - 12;
                                    $i=0;
                                    $y++;
                                }
                            }
                        }
                        $t++;
                    ?>
                </tr> 
              <?php }?>
              <?php }?>
              <?php if($classificacaoOa==1){
                   $y= (int) $anoInicio;
                   $totalMeses = $totalMesesSelecionado;
                        if(in_array(13, $arrAtividadesMarcadas2))
                        { 
                  ?>  
                <tr>              
                    <td align="center"><?php echo $t++;?></td>
                    <td align="center">Iniciação à Loja</td>
                    <td align="center">1</td>
                    <?php 
                            for($i=$mesInicio;$i<$totalMeses;$i++)
                            {  
                                $qntAtividadesIniciaticas = $aim->retornaNumeroDeAtividadesParaRelatorio($idOrganismoAfiliado,$i,$y,13);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntAtividadesIniciaticas>0){
                                                echo $qntAtividadesIniciaticas;    
                                            }else{
                                                echo "--";
                                            }    
                                        ?>
                                </td>
                            <?php
                                $qntParticipantes = $aim->retornaNumeroDeParticipantes($idOrganismoAfiliado,$i,$y,13);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntParticipantes>0||$qntParticipantes!=""){
                                                echo $qntParticipantes;    
                                            }else{
                                                echo "--";
                                            }    
                                        ?>
                                </td>
                            <?php
                                if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                                {
                                    $totalMeses = $totalMeses - 12;
                                    $i=0;
                                    $y++;
                                }
                            }
                        }
                    ?>
                </tr> 
              <?php }?>  
              <?php if($classificacaoOa==2){
                   $y= (int) $anoInicio;
                   $totalMeses = $totalMesesSelecionado;
                        if(in_array(14, $arrAtividadesMarcadas2))
                        {
                  ?>  
                <tr>
                    <td align="center"><?php echo $t++;?></td>
                    <td align="center">Iniciação ao Pronaos</td>
                    <td align="center">1</td>
                    <?php 
                            for($i=$mesInicio;$i<$totalMeses;$i++)
                            {  
                                $qntAtividadesIniciaticas = $aim->retornaNumeroDeAtividadesParaRelatorio($idOrganismoAfiliado,$i,$y,14);
                                ?>    
                                <td align="center">
                                        <?php 
                                           if($qntAtividadesIniciaticas>0){
                                                echo $qntAtividadesIniciaticas;    
                                            }else{
                                                echo "--";
                                            }    
                                        ?>
                                </td>
                            <?php
                                $qntParticipantes = $aim->retornaNumeroDeParticipantes($idOrganismoAfiliado,$i,$y,14);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntParticipantes>0||$qntParticipantes!=""){
                                                echo $qntParticipantes;    
                                            }else{
                                                echo "--";
                                            }   
                                        ?>
                                </td>
                            <?php
                                if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                                {
                                    $totalMeses = $totalMeses - 12;
                                    $i=0;
                                    $y++;
                                }
                            }
                        }
                    ?>
                </tr> 
              <?php }?>
              <?php if($classificacaoOa==3){
                   $y= (int) $anoInicio;
                   $totalMeses = $totalMesesSelecionado;
                        if(in_array(16, $arrAtividadesMarcadas2))
                        {
                  ?>  
                <tr>              
                    <td align="center"><?php echo $t++;?></td>
                    <td align="center">Iniciação ao Capítulo</td>
                    <td align="center">1</td>
                    <?php 
                            for($i=$mesInicio;$i<$totalMeses;$i++)
                            {  
                                $qntAtividadesIniciaticas = $aim->retornaNumeroDeAtividadesParaRelatorio($idOrganismoAfiliado,$i,$y,16);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntAtividadesIniciaticas>0){
                                                echo $qntAtividadesIniciaticas;    
                                            }else{
                                                echo "--";
                                            }    
                                        ?>
                                </td>
                            <?php 
                                $qntParticipantes = $aim->retornaNumeroDeParticipantes($idOrganismoAfiliado,$i,$y,16);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntParticipantes>0||$qntParticipantes!=""){
                                                echo $qntParticipantes;    
                                            }else{
                                                echo "--";
                                            }   
                                        ?>
                                </td>
                            <?php
                                if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                                {
                                    $totalMeses = $totalMeses - 12;
                                    $i=0;
                                    $y++;
                                }
                            }
                        }
                    ?>
                </tr> 
              <?php }?>  
              <?php if(in_array(15, $arrAtividadesMarcadas2))
                    {
                        $y= (int) $anoInicio;
                        $totalMeses = $totalMesesSelecionado;
                  ?>  
                <tr>              
                    <td align="center"><?php echo $t++;?></td>
                    <td align="center">Discurso de Orientação</td>
                    <td align="center">1</td>
                    <?php 
                        for($i=$mesInicio;$i<$totalMeses;$i++)
                        {  
                            $qntAtividadesIniciaticas = $aim->retornaNumeroDeAtividadesParaRelatorio($idOrganismoAfiliado,$i,$y,15);
                            ?>    
                            <td align="center">
                                    <?php 
                                        if($qntAtividadesIniciaticas>0){
                                            echo $qntAtividadesIniciaticas;    
                                        }else{
                                            echo "--";
                                        }    
                                    ?>
                            </td>
                        <?php 
                            $qntParticipantes = $aim->retornaNumeroDeParticipantes($idOrganismoAfiliado,$i,$y,15);
                                ?>    
                                <td align="center">
                                        <?php 
                                            if($qntParticipantes>0||$qntParticipantes!=""){
                                                echo $qntParticipantes;    
                                            }else{
                                                echo "--";
                                            }    
                                        ?>
                                </td>
                            <?php
                            if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                            {
                                $totalMeses = $totalMeses - 12;
                                $i=0;
                                $y++;
                            }
                        }
                    ?>
                </tr>   
                    <?php }?>
                    <?php if($_REQUEST['linha8']==1){
                        $y= (int) $anoInicio;
                        $totalMeses = $totalMesesSelecionado;
                        ?>    
                        <th colspan="3" align="center">Rosacruzes Ativos no Organismo Afiliado</th>
                    <?php }?>
                    <?php 
                        for($i=$mesInicio;$i<$totalMeses;$i++)
                        {  
                            $mCalc	= $mra->retornaMembrosRosacruzesAtivos($i,$y,$idOrganismoAfiliado);
                            $mCalc	= $mCalc['numeroAtualMembrosAtivos'];
                            if($_REQUEST['linha8']==1){?>    
                            <td align="center" colspan="2">
                                    <?php 
                                        echo $mCalc;    
                                    ?>
                            </td>
                        <?php }
                            if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                            {
                                $totalMeses = $totalMeses - 12;
                                $i=0;
                                $y++;
                            }
                        }
                    ?>
              </tr>
            </table>
            <br><br>
            <?php
            }
        }  
    }
}
?>        