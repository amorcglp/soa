<?php
set_time_limit(0);
//echo ini_get('max_execution_time');
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $mesAtual 							= null;
    $anoAtual 							= isset($_REQUEST['ano'])?$_REQUEST['ano']:null;
    $idOrganismoAfiliado 				= isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:null;
    $saldoAnoAnterior					= isset($_REQUEST['saldoAnoAnterior'])?$_REQUEST['saldoAnoAnterior']:0;
    $numeroMembrosAtivosAnterior		= isset($_REQUEST['numeroMembrosAtivosAnterior'])?$_REQUEST['numeroMembrosAtivosAnterior']:0;

    include_once('../lib/functions.php');
    include_once('../model/organismoClass.php');
    include_once('../model/recebimentoClass.php');
    include_once('../model/despesaClass.php');
    include_once('../model/membrosRosacruzesAtivosClass.php');

    $r 							= new Recebimento();
    $d 							= new Despesa();
    $o 							= new organismo();
    $mra						= new MembrosRosacruzesAtivos();
    
    if($idOrganismoAfiliado==0)
    {
        $resultado = $o->listaOrganismo(null,null,null,null,null,null,null,1,null,null,null,1,0,100);
    }else{    
        $resultado = $o->buscaIdOrganismo($idOrganismoAfiliado);
    }

    //exit();
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>EXTRATO FINANCEIRO</b></p></center>
    <?php 
    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }
            
                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];

    /**
     * Ano e saldo anterior
     */

    /*
     * Calcular saldo dos anos anteriores
     */
    //Encontrar Saldo e Data Inicial
    include_once('../model/saldoInicialClass.php');
    $si = new saldoInicial();
    $mesSaldoInicial = "01";
    $anoSaldoInicial = "2016";
    $saldoInicial=0;
    $resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    $mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
                    $anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
                    $saldoInicial = (float) str_replace(",",".",str_replace(".","",$vetor['saldoInicial']));
            }
    }

    //Encontrar Saldo dos Meses Anteriores
    $mes=intval($mesSaldoInicial);
    $ano=intval($anoSaldoInicial);
    $saldoMesesAnteriores=0;
    $saldoGeral=0;
    //echo "<br>mesSaldoInicial=>".$mes;
    //echo "<br>anoSaldoInicial=>".$ano;
    $y=0;
    if($anoAtual==$anoSaldoInicial)
    {
                    $saldoGeral = $saldoInicial;
                    $saldoAnterior = $saldoGeral;
    }else{
            $i=0;
            while($mes<$mesAtual&&$ano<=$anoAtual)
            {
                            //echo "<br>data=>".$mes."-".$ano;
                            if($mes<=12)
                            {
                                    if($mes==$mesSaldoInicial)
                                    {
                                            $saldoMesesAnteriores = $saldoInicial;
                                    }
                                    //echo "i:".$i."-mes".$mes."<br>";
                                    //echo "saldoDoMesAnteriorParaSerCalculado:".$saldoMesesAnteriores."<br>";
                                    $saldoGeral = retornaSaldo($r,$d,$mes,$ano,$idOrganismoAfiliado,$saldoMesesAnteriores);
                                    $saldoAnterior = $saldoGeral;
                                    //echo "saldoMesesAnteriores".$saldoMesesAnteriores."<br>";
                                    $mes++;	
                            }else{
                                    $ano++;
                            }	

                            //echo "<br>saldoGeral=>".$saldoGeral;
                    $i++;
            }
    }

    $anoAnterior 			= $anoAtual-1;
    //$totalEntradaAnterior	= $r->retornaEntrada(null,$anoAnterior,$idOrganismoAfiliado);
    //$totalSaidaAnterior		= retornaSaidas($r,$d,null,$anoAnterior,$idOrganismoAfiliado);
    //$saldoAnterior 			= $totalEntradaAnterior-$totalSaidaAnterior;

    /**
     * Cálculo Entradas
     */
    $rJan						= $r->retornaEntrada(1,$anoAtual,$idOrganismoAfiliado);
    $rFev						= $r->retornaEntrada(2,$anoAtual,$idOrganismoAfiliado);
    $rMar						= $r->retornaEntrada(3,$anoAtual,$idOrganismoAfiliado);
    $rAbr						= $r->retornaEntrada(4,$anoAtual,$idOrganismoAfiliado);
    $rMai						= $r->retornaEntrada(5,$anoAtual,$idOrganismoAfiliado);
    $rJun						= $r->retornaEntrada(6,$anoAtual,$idOrganismoAfiliado);
    $rJul						= $r->retornaEntrada(7,$anoAtual,$idOrganismoAfiliado);
    $rAgo						= $r->retornaEntrada(8,$anoAtual,$idOrganismoAfiliado);
    $rSet						= $r->retornaEntrada(9,$anoAtual,$idOrganismoAfiliado);
    $rOut						= $r->retornaEntrada(10,$anoAtual,$idOrganismoAfiliado);
    $rNov						= $r->retornaEntrada(11,$anoAtual,$idOrganismoAfiliado);
    $rDez						= $r->retornaEntrada(12,$anoAtual,$idOrganismoAfiliado);

    /**
     * Cálculo Saídas
     */
    $dJan						= retornaSaidas($r,$d,1,$anoAtual,$idOrganismoAfiliado);
    $dFev						= retornaSaidas($r,$d,2,$anoAtual,$idOrganismoAfiliado);
    $dMar						= retornaSaidas($r,$d,3,$anoAtual,$idOrganismoAfiliado);
    $dAbr						= retornaSaidas($r,$d,4,$anoAtual,$idOrganismoAfiliado);
    $dMai						= retornaSaidas($r,$d,5,$anoAtual,$idOrganismoAfiliado);
    $dJun						= retornaSaidas($r,$d,6,$anoAtual,$idOrganismoAfiliado);
    $dJul						= retornaSaidas($r,$d,7,$anoAtual,$idOrganismoAfiliado);
    $dAgo						= retornaSaidas($r,$d,8,$anoAtual,$idOrganismoAfiliado);
    $dSet						= retornaSaidas($r,$d,9,$anoAtual,$idOrganismoAfiliado);
    $dOut						= retornaSaidas($r,$d,10,$anoAtual,$idOrganismoAfiliado);
    $dNov						= retornaSaidas($r,$d,11,$anoAtual,$idOrganismoAfiliado);
    $dDez						= retornaSaidas($r,$d,12,$anoAtual,$idOrganismoAfiliado);

    /**
     * Cálculo do Saldo
     */
    $sJan						= $saldoAnterior+$rJan-$dJan;
    $sFev						= $sJan+$rFev-$dFev;
    $sMar						= $sFev+$rMar-$dMar;
    $sAbr						= $sMar+$rAbr-$dAbr;
    $sMai						= $sAbr+$rMai-$dMai;
    $sJun						= $sMai+$rJun-$dJun;
    $sJul						= $sJun+$rJul-$dJul;
    $sAgo						= $sJul+$rAgo-$dAgo;
    $sSet						= $sAgo+$rSet-$dSet;
    $sOut						= $sSet+$rOut-$dOut;
    $sNov						= $sOut+$rNov-$dNov;
    $sDez						= $sNov+$rDez-$dDez;

    /**
     * Membros Ativos
     */
    $mJan						= $mra->retornaMembrosRosacruzesAtivos(1,$anoAtual,$idOrganismoAfiliado);
    $mJan						= $mJan['numeroAtualMembrosAtivos'];

    $mFev						= $mra->retornaMembrosRosacruzesAtivos(2,$anoAtual,$idOrganismoAfiliado);
    $mFev						= $mFev['numeroAtualMembrosAtivos'];

    $mMar						= $mra->retornaMembrosRosacruzesAtivos(3,$anoAtual,$idOrganismoAfiliado);
    $mMar						= $mMar['numeroAtualMembrosAtivos'];

    $mAbr						= $mra->retornaMembrosRosacruzesAtivos(4,$anoAtual,$idOrganismoAfiliado);
    $mAbr						= $mAbr['numeroAtualMembrosAtivos'];

    $mMai						= $mra->retornaMembrosRosacruzesAtivos(5,$anoAtual,$idOrganismoAfiliado);
    $mMai						= $mMai['numeroAtualMembrosAtivos'];

    $mJun						= $mra->retornaMembrosRosacruzesAtivos(6,$anoAtual,$idOrganismoAfiliado);
    $mJun						= $mJun['numeroAtualMembrosAtivos'];

    $mJul						= $mra->retornaMembrosRosacruzesAtivos(7,$anoAtual,$idOrganismoAfiliado);
    $mJul						= $mJul['numeroAtualMembrosAtivos'];

    $mAgo						= $mra->retornaMembrosRosacruzesAtivos(8,$anoAtual,$idOrganismoAfiliado);
    $mAgo						= $mAgo['numeroAtualMembrosAtivos'];

    $mSet						= $mra->retornaMembrosRosacruzesAtivos(9,$anoAtual,$idOrganismoAfiliado);
    $mSet						= $mSet['numeroAtualMembrosAtivos'];

    $mOut						= $mra->retornaMembrosRosacruzesAtivos(10,$anoAtual,$idOrganismoAfiliado);
    $mOut						= $mOut['numeroAtualMembrosAtivos'];

    $mNov						= $mra->retornaMembrosRosacruzesAtivos(11,$anoAtual,$idOrganismoAfiliado);
    $mNov						= $mNov['numeroAtualMembrosAtivos'];

    $mDez						= $mra->retornaMembrosRosacruzesAtivos(12,$anoAtual,$idOrganismoAfiliado);
    $mDez						= $mDez['numeroAtualMembrosAtivos'];
    ?>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $anoAtual; ?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Saldo do Ano Anterior:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo number_format($saldoAnterior, 2, ',', '.'); ?>
                    </span>
            </div>
    </center>
    <br>
    <table border="1" width="100%">
      <tr>
        <th align="center">Mês</th>
        <th align="center">Recebimentos</th>
        <th align="center">Despesas</th>
        <th align="center">Saldo</th>
        <th align="center">Membros Ativos</th>
      </tr>
      <tr>
        <td align="center">Janeiro</td>
        <td align="center"><?php echo number_format($rJan, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dJan, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sJan, 2, ',', '.');?></td>
        <td align="center"><?php echo $mJan;?></td>
      </tr>
      <tr>
        <td align="center">Fevereiro</td>
        <td align="center"><?php echo number_format($rFev, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dFev, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sFev, 2, ',', '.');?></td>
        <td align="center"><?php echo $mFev;?></td>
      </tr>
      <tr>
        <td align="center">Março</td>
        <td align="center"><?php echo number_format($rMar, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dMar, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sMar, 2, ',', '.');?></td>
        <td align="center"><?php echo $mMar;?></td>
      </tr>
      <tr>
        <td align="center">Abril</td>
        <td align="center"><?php echo number_format($rAbr, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dAbr, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sAbr, 2, ',', '.');?></td>
        <td align="center"><?php echo $mAbr;?></td>
      </tr>
      <tr>
        <td align="center">Maio</td>
         <td align="center"><?php echo number_format($rMai, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dMai, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sMai, 2, ',', '.');?></td>
        <td align="center"><?php echo $mMai;?></td>
      </tr>
      <tr>
        <td align="center">Junho</td>
         <td align="center"><?php echo number_format($rJun, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dJun, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sJun, 2, ',', '.');?></td>
        <td align="center"><?php echo $mJun;?></td>
      </tr>
      <tr>
        <td align="center">Julho</td>
         <td align="center"><?php echo number_format($rJul, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dJul, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sJul, 2, ',', '.');?></td>
        <td align="center"><?php echo $mJul;?></td>
      </tr>
      <tr>
        <td align="center">Agosto</td>
         <td align="center"><?php echo number_format($rAgo, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dAgo, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sAgo, 2, ',', '.');?></td>
        <td align="center"><?php echo $mAgo;?></td>
      </tr>
      <tr>
        <td align="center">Setembro</td>
         <td align="center"><?php echo number_format($rSet, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dSet, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sSet, 2, ',', '.');?></td>
        <td align="center"><?php echo $mSet;?></td>
      </tr>
      <tr>
        <td align="center">Outubro</td>
         <td align="center"><?php echo number_format($rOut, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dOut, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sOut, 2, ',', '.');?></td>
        <td align="center"><?php echo $mOut;?></td>
      </tr>
      <tr>
        <td align="center">Novembro</td>
         <td align="center"><?php echo number_format($rNov, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dNov, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sNov, 2, ',', '.');?></td>
        <td align="center"><?php echo $mNov;?></td>
      </tr>
      <tr>
        <td align="center">Dezembro</td>
         <td align="center"><?php echo number_format($rDez, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($dDez, 2, ',', '.');?></td>
        <td align="center"><?php echo number_format($sDez, 2, ',', '.');?></td>
        <td align="center"><?php echo $mDez;?></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td align="right"><b>SALDO FINAL</b></td>
        <td align="center"><b><?php echo number_format($sDez, 2, ',', '.');?></b></td>
        <td></td>
      </tr>
    </table>
    <br><br>
<?php
            }
    }
}
?>