<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once('../model/ataPosseClass.php');
    include_once '../model/ataPosseEmpossadoClass.php';
    include_once('../lib/functions.php');
    require_once("../model/criaSessaoClass.php");

    $sessao = new criaSessao();

    //Internacionalização
    i18n($sessao->getValue("paisUsuario"));

    $ap = new ataPosse();
    $resultado = $ap->buscarIdAtaPosse($_REQUEST['idAta']);
    $arrAtaPosse = array();
    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    $tipo2 = "Rosacruz";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    $tipo2= "Martinista";
                                    break;
                    }

                    $data 									= $vetor['dataPosse'];
                    $horaInicio 							= $vetor['horaInicioPosse'];
                    $enderecoPosse  						= $vetor['enderecoPosse'];
                    $numeroPosse    						= $vetor['numeroPosse'];
                    $bairroPosse    						= $vetor['bairroPosse'];
                    $cidadePosse    						= $vetor['cidadePosse'];
                    $mestreRetirante    					= $vetor['mestreRetirante'];
                    $secretarioRetirante    				= $vetor['secretarioRetirante'];
                    $presidenteJuntaDepositariaRetirante    = $vetor['presidenteJuntaDepositariaRetirante'];
                    $secretarioJuntaDepositariaRetirante    = $vetor['secretarioJuntaDepositariaRetirante'];
                    $tesoureiroJuntaDepositariaRetirante    = $vetor['tesoureiroJuntaDepositariaRetirante'];
                    $guardiaoRetirante    					= $vetor['guardiaoRetirante'];
                    //$substituicaoPosse	    			= $vetor['substituicaoPosse'];
                    $nomeDirigiuPosse	    				= $vetor['nomeDirigiuPosse'];
                    $cargoDirigiuPosse	    				= $vetor['cargoDirigiuPosse'];
                    $detalhesAdicionaisPosse				= $vetor['detalhesAdicionaisPosse'];
                    $enderecoOrganismoAfiliado				= $vetor['enderecoOrganismoAfiliado'];
                    $numeroOrganismoAfiliado				= $vetor['numeroOrganismoAfiliado'];
                    $bairroOrganismoAfiliado				= $vetor['bairroOrganismoAfiliado'];
                    $cidadeOrganismoAfiliado				= $vetor['cidade'];
                    $uf										= $vetor['uf'];
                    $cepOrganismoAfiliado					= $vetor['cepOrganismoAfiliado'];
            }
    }

    $anoRosacruz = date('Y')+1353;

    $ape = new ataPosseEmpossado();
    $resultado = $ape->listaEmpossados($_REQUEST['idAta'], 1);
    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    $nome1				= $vetor['nome'];
                    $inicioMandato1		= substr($vetor['inicioMandato'],8,2)."/".substr($vetor['inicioMandato'],5,2)."/".substr($vetor['inicioMandato'],0,4);
                    $fimMandato1		= substr($vetor['fimMandato'],8,2)."/".substr($vetor['fimMandato'],5,2)."/".substr($vetor['fimMandato'],0,4);
                    $indicado1			= $vetor['indicado'];
                    $codigoAfiliacao1   = $vetor['codigoAfiliacao'];
                    $endereco1   		= $vetor['endereco'];
                    $cidade1   			= $vetor['cidade'];
                    $cep1 		  		= $vetor['cep'];
                    $naturalidade1 		= $vetor['naturalidade'];
                    $dataNascimento1 	= substr($vetor['dataNascimento'],8,2)."/".substr($vetor['dataNascimento'],5,2)."/".substr($vetor['dataNascimento'],0,4);
                    $nacionalidade1 	= $vetor['nacionalidade'];
                    $estadoCivil1  		= estadoCivil($vetor['estadoCivil']);
                    $profissao1   		= $vetor['profissao'];
                    $rg1  		 		= $vetor['rg'];
                    $cpf1  		 		= $vetor['cpf'];
            }
    }else{
            $indicado1="";
            $nome1="";
    }

    $ape = new ataPosseEmpossado();
    $resultado2 = $ape->listaEmpossados($_REQUEST['idAta'], 2);
    if($resultado2)
    {
            foreach($resultado2 as $vetor)
            {
                    $nome2				= $vetor['nome'];
                    $inicioMandato2		= substr($vetor['inicioMandato'],8,2)."/".substr($vetor['inicioMandato'],5,2)."/".substr($vetor['inicioMandato'],0,4);
                    $fimMandato2		= substr($vetor['fimMandato'],8,2)."/".substr($vetor['fimMandato'],5,2)."/".substr($vetor['fimMandato'],0,4);
                    $indicado2			= $vetor['indicado'];
                    $codigoAfiliacao2   = $vetor['codigoAfiliacao'];
                    $endereco2   		= $vetor['endereco'];
                    $cidade2   			= $vetor['cidade'];
                    $cep2 		  		= $vetor['cep'];
                    $naturalidade2 		= $vetor['naturalidade'];
                    $dataNascimento2 	= substr($vetor['dataNascimento'],8,2)."/".substr($vetor['dataNascimento'],5,2)."/".substr($vetor['dataNascimento'],0,4);
                    $nacionalidade2 	= $vetor['nacionalidade'];
                    $estadoCivil2 		= estadoCivil($vetor['estadoCivil']);
                    $profissao2   		= $vetor['profissao'];
                    $rg2  		 		= $vetor['rg'];
                    $cpf2  		 		= $vetor['cpf'];
            }
    }else{
            $indicado2="";
            $nome2="";
    }

    $ape = new ataPosseEmpossado();
    $resultado3 = $ape->listaEmpossados($_REQUEST['idAta'], 3);
    if($resultado3)
    {
            foreach($resultado3 as $vetor)
            {
                    $nome3				= $vetor['nome'];
                    $inicioMandato3		= substr($vetor['inicioMandato'],8,2)."/".substr($vetor['inicioMandato'],5,2)."/".substr($vetor['inicioMandato'],0,4);
                    $fimMandato3		= substr($vetor['fimMandato'],8,2)."/".substr($vetor['fimMandato'],5,2)."/".substr($vetor['fimMandato'],0,4);
                    $indicado3			= $vetor['indicado'];
                    $codigoAfiliacao3   = $vetor['codigoAfiliacao'];
                    $endereco3   		= $vetor['endereco'];
                    $cidade3   			= $vetor['cidade'];
                    $cep3 		  		= $vetor['cep'];
                    $naturalidade3 		= $vetor['naturalidade'];
                    $dataNascimento3 	= substr($vetor['dataNascimento'],8,2)."/".substr($vetor['dataNascimento'],5,2)."/".substr($vetor['dataNascimento'],0,4);
                    $nacionalidade3 	= $vetor['nacionalidade'];
                    $estadoCivil3  		= estadoCivil($vetor['estadoCivil']);
                    $profissao3   		= $vetor['profissao'];
                    $rg3  		 		= $vetor['rg'];
                    $cpf3  		 		= $vetor['cpf'];
            }
    }else{
            $indicado3="";
            $nome3="";
    }

    $ape = new ataPosseEmpossado();
    $resultado4 = $ape->listaEmpossados($_REQUEST['idAta'], 4);
    if($resultado4)
    {
            foreach($resultado4 as $vetor)
            {
                    $nome4				= $vetor['nome'];
                    $inicioMandato4		= substr($vetor['inicioMandato'],8,2)."/".substr($vetor['inicioMandato'],5,2)."/".substr($vetor['inicioMandato'],0,4);
                    $fimMandato4		= substr($vetor['fimMandato'],8,2)."/".substr($vetor['fimMandato'],5,2)."/".substr($vetor['fimMandato'],0,4);
                    $indicado4			= $vetor['indicado'];
                    $codigoAfiliacao4   = $vetor['codigoAfiliacao'];
                    $endereco4   		= $vetor['endereco'];
                    $cidade4   			= $vetor['cidade'];
                    $cep4 		  		= $vetor['cep'];
                    $naturalidade4 		= $vetor['naturalidade'];
                    $dataNascimento4 	= substr($vetor['dataNascimento'],8,2)."/".substr($vetor['dataNascimento'],5,2)."/".substr($vetor['dataNascimento'],0,4);
                    $nacionalidade4 	= $vetor['nacionalidade'];
                    $estadoCivil4  		= estadoCivil($vetor['estadoCivil']);
                    $profissao4   		= $vetor['profissao'];
                    $rg4  		 		= $vetor['rg'];
                    $cpf4  		 		= $vetor['cpf'];
            }
    }else{
            $indicado4="";
            $nome4="";
    }

    $ape = new ataPosseEmpossado();
    $resultado5 = $ape->listaEmpossados($_REQUEST['idAta'], 5);
    if($resultado5)
    {
            foreach($resultado5 as $vetor)
            {
                    $nome5				= $vetor['nome'];
                    $inicioMandato5		= substr($vetor['inicioMandato'],8,2)."/".substr($vetor['inicioMandato'],5,2)."/".substr($vetor['inicioMandato'],0,4);
                    $fimMandato5		= substr($vetor['fimMandato'],8,2)."/".substr($vetor['fimMandato'],5,2)."/".substr($vetor['fimMandato'],0,4);
                    $indicado5			= $vetor['indicado'];
                    $codigoAfiliacao5   = $vetor['codigoAfiliacao'];
                    $endereco5   		= $vetor['endereco'];
                    $cidade5   			= $vetor['cidade'];
                    $cep5 		  		= $vetor['cep'];
                    $naturalidade5 		= $vetor['naturalidade'];
                    $dataNascimento5 	= substr($vetor['dataNascimento'],8,2)."/".substr($vetor['dataNascimento'],5,2)."/".substr($vetor['dataNascimento'],0,4);
                    $nacionalidade5 	= $vetor['nacionalidade'];
                    $estadoCivil5  		= estadoCivil($vetor['estadoCivil']);
                    $profissao5   		= $vetor['profissao'];
                    $rg5  		 		= $vetor['rg'];
                    $cpf5  		 		= $vetor['cpf'];
            }
    }else{
            $indicado5="";
            $nome5="";
    }

    $ape = new ataPosseEmpossado();
    $resultado6 = $ape->listaEmpossados($_REQUEST['idAta'], 6);
    if($resultado6)
    {
            foreach($resultado6 as $vetor)
            {
                    $nome6				= $vetor['nome'];
                    $inicioMandato6		= substr($vetor['inicioMandato'],8,2)."/".substr($vetor['inicioMandato'],5,2)."/".substr($vetor['inicioMandato'],0,4);
                    $fimMandato6		= substr($vetor['fimMandato'],8,2)."/".substr($vetor['fimMandato'],5,2)."/".substr($vetor['fimMandato'],0,4);
                    $indicado6			= $vetor['indicado'];
                    $codigoAfiliacao6   = $vetor['codigoAfiliacao'];
                    $endereco6   		= $vetor['endereco'];
                    $cidade6   			= $vetor['cidade'];
                    $cep6 		  		= $vetor['cep'];
                    $naturalidade6 		= $vetor['naturalidade'];
                    $dataNascimento6 	= substr($vetor['dataNascimento'],8,2)."/".substr($vetor['dataNascimento'],5,2)."/".substr($vetor['dataNascimento'],0,4);
                    $nacionalidade6 	= $vetor['nacionalidade'];
                    $estadoCivil6  		= estadoCivil($vetor['estadoCivil']);
                    $profissao6   		= $vetor['profissao'];
                    $rg6  		 		= $vetor['rg'];
                    $cpf6  		 		= $vetor['cpf'];
            }
    }else{
            $indicado6="";
            $nome6="";
    }

    $ape = new ataPosseEmpossado();
    $resultado7 = $ape->listaEmpossados($_REQUEST['idAta'], 7);
    if($resultado7)
    {
            foreach($resultado7 as $vetor)
            {
                    $nome7				= $vetor['nome'];
                    $inicioMandato7		= substr($vetor['inicioMandato'],8,2)."/".substr($vetor['inicioMandato'],5,2)."/".substr($vetor['inicioMandato'],0,4);
                    $fimMandato7		= substr($vetor['fimMandato'],8,2)."/".substr($vetor['fimMandato'],5,2)."/".substr($vetor['fimMandato'],0,4);
                    $indicado7			= $vetor['indicado'];
                    $codigoAfiliacao7   = $vetor['codigoAfiliacao'];
                    $endereco7   		= $vetor['endereco'];
                    $cidade7   			= $vetor['cidade'];
                    $cep7 		  		= $vetor['cep'];
                    $naturalidade7 		= $vetor['naturalidade'];
                    $dataNascimento7 	= substr($vetor['dataNascimento'],8,2)."/".substr($vetor['dataNascimento'],5,2)."/".substr($vetor['dataNascimento'],0,4);
                    $nacionalidade7 	= $vetor['nacionalidade'];
                    $estadoCivil7  		= estadoCivil($vetor['estadoCivil']);
                    $profissao7   		= $vetor['profissao'];
                    $rg7  		 		= $vetor['rg'];
                    $cpf7  		 		= $vetor['cpf'];
            }
    }else{
            $indicado7="";
            $nome7="";
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 16px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>206 - ATA DA REUNIÃO ADMINISTRATIVA ORDINÁRIA DE POSSE DE OFICIAIS</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
                    <b class="fonte tamanho-fonte_2">
                            Mês:
                    </b>
                    <?php echo mesExtensoPortugues(substr($data,5,2)); ?>
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <?php echo substr($data,0,4); ?>
            </div>
    </center>
    <br>
    <p><?php if(substr($data,8,2)!="01"){?>Aos <?php echo str_replace("real","",str_replace("reais","",valorPorExtenso(substr($data,8,2)))); ?> dias<?php }else{ echo "No primeiro dia";}?> do mês de <?php echo mesExtensoPortugues(substr($data,5,2));?> do ano de <?php echo trim(str_replace("reais","",valorPorExtenso(substr($data,0,4)))); ?>, Ano Rosacruz <?php echo substr($anoRosacruz,0,1);?>.<?php echo substr($anoRosacruz,1,3);?>, às <?php echo substr($horaInicio,0,2);?>h<?php echo substr($horaInicio,3,2);?>, os membros do(a)  <?php echo $classificacao." ".$tipo2." ".$nomeOrganismoAfiliado; ?> - AMORC 
    se reuniram em sua sede sito à <?php echo $enderecoPosse;?>, nº. <?php echo $numeroPosse;?> - bairro <?php echo $bairroPosse;?>, para a indicação dos Oficiais 
    Administrativos pelo(a) <?php echo $cargoDirigiuPosse;?> e posse dos mesmos. Abrindo a reunião o(a) <?php echo $cargoDirigiuPosse;?> <?php echo $nomeDirigiuPosse;?> indicou para os cargos os seguintes 
    Oficiais Administrativos:

    <?php if($nome1!=""){  echo $nome1;?> para o cargo de Mestre do(a) <?php echo $classificacao?>. <?php }?>
    <?php if($nome5!=""){  echo " ".$nome5;?> para o cargo de Secretário(a) do(a) <?php echo $classificacao?>.<?php }?>
    <?php if($nome7!=""){  echo " ".$nome7;?> para o cargo de Guardião. <?php }?>
    <?php if($nome6!=""){  echo " ".$nome6;?> para o cargo de Presidente da Junta Depositária. <?php }?>
    <?php if($nome3!=""){  echo " ".$nome3;?> para o cargo de Secretário(a) da Junta Depositária. <?php }?>
    <?php if($nome4!=""){  echo " ".$nome4;?> para o cargo de Tesoureiro(a) da Junta Depositária. <?php }?>
    <?php if($nome2!=""){  echo " ".$nome2;?> para o cargo de Mestre Auxiliar. <?php  }?>


    Em seguida convidou os Oficiais Administrativos para comporem a mesa, informando aos presentes que esta reunião tem por finalidade empossar nos cargos administrativos membros 
    aprovados na forma estatutária. A seguir e de acordo com o Estatuto do(a) <?php echo $classificacao;?>, foram proclamadas as posses dos seguintes Oficiais Administrativos:
    <b>
    <?php if($nome1!=""){  echo " ".$nome1;?> para o cargo de Mestre do(a) <?php echo $classificacao?>, para o período de <?php echo $inicioMandato1;?> ao <?php echo $fimMandato1;?>.<?php }?>
    <?php if($nome5!=""){  echo " ".$nome5;?> para o cargo de Secretário(a) do(a) <?php echo $classificacao?> para o período de <?php echo $inicioMandato5;?> ao <?php echo $fimMandato5;?>.<?php }?>
    <?php if($nome7!=""){  echo " ".$nome7;?> para o cargo de Guardião para o período de <?php echo $inicioMandato7;?> ao <?php echo $fimMandato7;?>.<?php }?>
    <?php if($nome6!=""){  echo " ".$nome6;?> para o cargo de Presidente da Junta Depositária para o período de <?php echo $inicioMandato6;?> ao <?php echo $fimMandato6;?>.<?php }?>
    <?php if($nome3!=""){  echo " ".$nome3;?> para o cargo de Secretário(a) da Junta Depositária, para o período de <?php echo $inicioMandato3;?> ao <?php echo $fimMandato3;?>.<?php }?>
    <?php if($nome4!=""){  echo " ".$nome4;?> para o cargo de Tesoureiro(a) da Junta Depositária, para o período de <?php echo $inicioMandato4;?> ao <?php echo $fimMandato4;?>.<?php }?>
    <?php if($nome2!=""){  echo " ".$nome2;?> para o cargo de Mestre Auxiliar, para o período de <?php echo $inicioMandato2;?> ao <?php echo $fimMandato2;?>.<?php }?>
    </b>
     <?php echo str_replace("<p>","",str_replace("</p>","",$detalhesAdicionaisPosse));?>. E nada mais havendo a ser tratado foi encerrada esta reunião e lavrada para ser 
    assinada por todos os Oficiais presentes. <?php echo trim($cidadePosse)?>, <?php echo substr($data,8,2);?> de <?php echo mesExtensoPortugues(substr($data,5,2));?> de <?php echo substr($data,0,4);?>.</p>

    <table width="100%">
            <?php if($mestreRetirante!=""){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Mestre Retirante  <?php echo $mestreRetirante;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($indicado1==1){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%"><?php if($indicado1==1){?>Novo <?php }?>Mestre <?php echo $nome1;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($secretarioRetirante!=""){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Secretario(a) Retirante <?php echo $secretarioRetirante;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($indicado5==1){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%"><?php if($indicado5==1){?>Novo(a) <?php }?>Secretario(a) do(a) <?php echo $classificacao?> <?php echo $nome5;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($guardiaoRetirante!=""){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Guardião Retirante <?php echo $guardiaoRetirante;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($indicado7==1){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%"><?php if($indicado7==1){?>Novo(a) <?php }?>Guardião do(a) <?php echo $classificacao?> <?php echo $nome7;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($presidenteJuntaDepositariaRetirante!=""){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Presidente da Junta Depositária Retirante <?php echo $presidenteJuntaDepositariaRetirante;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($indicado6==1){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%"><?php if($indicado6==1){?>Novo <?php }?>Presidente da Junta Depositária <?php echo $nome6;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($secretarioJuntaDepositariaRetirante!=""){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Secretário(a) da Junta Depositária Retirante <?php echo $secretarioJuntaDepositariaRetirante;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($indicado3==1){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%"><?php if($indicado3==1){?>Novo <?php }?>Secretário(a) da Junta Depositária <?php echo $nome3;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($tesoureiroJuntaDepositariaRetirante!=""){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Tesoureiro(a) da Junta Depositária Retirante <?php echo $tesoureiroJuntaDepositariaRetirante;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($indicado4==1){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%"><?php if($indicado4==1){?>Novo <?php }?>Tesoureiro(a) da Junta Depositária <?php echo $nome4;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
            <?php if($indicado2==1){?>
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%"><?php if($indicado2==1){?>Novo <?php }?>Mestre Auxiliar <?php if($indicado2==0){?>(Prorrogado) <?php }?><?php echo $nome2;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
            <?php }?>
    </table>
    <?php if($nome1!=""||$nome2!=""||$nome3!=""||$nome4!=""||$nome5!=""||$nome6!=""){?>
    <br><br><br>
    <div style="page-break-before: always;"> </div>
    <br><br>
    <center>
            <b><?php echo strtoupper($classificacao." ".$tipo2." ".$nomeOrganismoAfiliado);?> - AMORC</b>
            <br><br>
            Rua: <?php echo $enderecoOrganismoAfiliado;?>, nº <?php echo $numeroOrganismoAfiliado;?> - <?php echo $bairroOrganismoAfiliado;?> - <?php echo $cidadeOrganismoAfiliado;?> - <?php echo $uf;?> - CEP. <?php echo $cepOrganismoAfiliado;?>
            <br><br>
            <b>QUALIFICAÇÃO DE OFICIAIS ADMINISTRATIVOS DA <?php echo strtoupper($classificacao." ".$tipo2." ".$nomeOrganismoAfiliado);?> - AMORC - ANO R+C <?php echo substr($anoRosacruz,0,1);?>.<?php echo substr($anoRosacruz,1,3);?></b>
    </center>
    <?php }?>
    <?php if($nome1!=""){?>
    <br><br>
    <table border="1" style="border:1px solid black" width="100%">
            <tr bgcolor="#CCC">
                    <td width="25%"><center><b>MESTRE</b></center></td><td><center><b><?php echo $nome1;?> - N. CHAVE: <?php echo $codigoAfiliacao1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Endereço e Cep:</b></center></td><td><center><b><?php echo $endereco1;?> - <?php echo $cep1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Naturalidade e Data de Nascimento:</b></center></td><td><center><b><?php echo $naturalidade1;?> - <?php echo $dataNascimento1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Nacionalidade:</b></center></td><td><center><b><?php echo $nacionalidade1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Estado civil:</b></center></td><td><center><b><?php echo $estadoCivil1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Profissão:</b></center></td><td><center><b><?php echo $profissao1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b><?php echo $GLOBALS['i18n']['rg'];?>:</b></center></td><td><center><b><?php echo $rg1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>CPF:</b></center></td><td><center><b><?php echo $cpf1;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Cidade onde reside:</b></center></td><td><center><b><?php echo $cidade1;?></b></center></td>
            </tr>
    </table>
    <br><br><br><br><br>
    <center>
    <table cellpadding="10" width="100%">
            <tr><td><center>______________________________________________________________</center></td><td><center>__________________</center></td></tr>
            <tr><td><center>Assinatura</center></td><td><center>Rúbrica</center></td></tr>
    </table>
    <?php }?>
    <?php if($nome5!=""){?>
    <center>
    <div style="page-break-before: always;"> </div>
    <br><br>
    <table border="1" style="border:1px solid black" width="100%">
            <tr bgcolor="#CCC">
                    <td width="25%"><center><b>SECRETÁRIO(A) DO(A) <?php echo strtoupper($classificacao);?></b></center></td><td><center><b><?php echo $nome5;?> - N. CHAVE: <?php echo $codigoAfiliacao5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Endereço e Cep:</b></center></td><td><center><b><?php echo $endereco5;?> - <?php echo $cep5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Naturalidade e Data de Nascimento:</b></center></td><td><center><b><?php echo $naturalidade5;?> - <?php echo $dataNascimento5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Nacionalidade:</b></center></td><td><center><b><?php echo $nacionalidade5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Estado civil:</b></center></td><td><center><b><?php echo $estadoCivil5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Profissão:</b></center></td><td><center><b><?php echo $profissao5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b><?php echo $GLOBALS['i18n']['rg'];?>:</b></center></td><td><center><b><?php echo $rg5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>CPF:</b></center></td><td><center><b><?php echo $cpf5;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Cidade onde reside:</b></center></td><td><center><b><?php echo $cidade5;?></b></center></td>
            </tr>
    </table>
    <br><br><br><br><br>
    <center>
    <table cellpadding="10" width="100%">
            <tr><td><center>______________________________________________________________</center></td><td><center>__________________</center></td></tr>
            <tr><td><center>Assinatura</center></td><td><center>Rúbrica</center></td></tr>
    </table>
    <?php }?>
    <?php if($nome7!=""){?>
    <center>
    <br><br><br><br><br>
    <table border="1" style="border:1px solid black" width="100%">
            <tr bgcolor="#CCC">
                    <td width="25%"><center><b>GUARDIÃO</b></center></td><td><center><b><?php echo $nome7;?> - N. CHAVE: <?php echo $codigoAfiliacao7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Endereço e Cep:</b></center></td><td><center><b><?php echo $endereco7;?> - <?php echo $cep7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Naturalidade e Data de Nascimento:</b></center></td><td><center><b><?php echo $naturalidade7;?> - <?php echo $dataNascimento7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Nacionalidade:</b></center></td><td><center><b><?php echo $nacionalidade7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Estado civil:</b></center></td><td><center><b><?php echo $estadoCivil7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Profissão:</b></center></td><td><center><b><?php echo $profissao7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b><?php echo $GLOBALS['i18n']['rg'];?>:</b></center></td><td><center><b><?php echo $rg7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>CPF:</b></center></td><td><center><b><?php echo $cpf7;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Cidade onde reside:</b></center></td><td><center><b><?php echo $cidade7;?></b></center></td>
            </tr>
    </table>
    <br><br><br><br><br>
    <center>
    <table cellpadding="10" width="100%">
            <tr><td><center>______________________________________________________________</center></td><td><center>__________________</center></td></tr>
            <tr><td><center>Assinatura</center></td><td><center>Rúbrica</center></td></tr>
    </table>
    <center>
    <?php }?>
    <?php if($nome6!=""){?>
    <center>
    <br><br><br><br><br>
    <table border="1" style="border:1px solid black" width="100%">
            <tr bgcolor="#CCC">
                    <td width="25%"><center><b>PRESIDENTE DA JUNTA DEPOSITÁRIA</b></center></td><td><center><b><?php echo $nome6;?> - N. CHAVE: <?php echo $codigoAfiliacao6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Endereço e Cep:</b></center></td><td><center><b><?php echo $endereco6;?> - <?php echo $cep6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Naturalidade e Data de Nascimento:</b></center></td><td><center><b><?php echo $naturalidade6;?> - <?php echo $dataNascimento6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Nacionalidade:</b></center></td><td><center><b><?php echo $nacionalidade6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Estado civil:</b></center></td><td><center><b><?php echo $estadoCivil6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Profissão:</b></center></td><td><center><b><?php echo $profissao6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b><?php echo $GLOBALS['i18n']['rg'];?>:</b></center></td><td><center><b><?php echo $rg6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>CPF:</b></center></td><td><center><b><?php echo $cpf6;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Cidade onde reside:</b></center></td><td><center><b><?php echo $cidade6;?></b></center></td>
            </tr>
    </table>
    <br><br><br><br><br>
    <center>
    <table cellpadding="10" width="100%">
            <tr><td><center>______________________________________________________________</center></td><td><center>__________________</center></td></tr>
            <tr><td><center>Assinatura</center></td><td><center>Rúbrica</center></td></tr>
    </table>
    <center>
    <?php }?>
    <?php if($nome3!=""){?>
    <br><br>
    <table border="1" style="border:1px solid black" width="100%">
            <tr bgcolor="#CCC">
                    <td width="25%"><center><b>SECRETÁRIO(A) DA JUNTA DEPOSITÁRIA</b></center></td><td><center><b><?php echo $nome3;?> - N. CHAVE: <?php echo $codigoAfiliacao3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Endereço e Cep:</b></center></td><td><center><b><?php echo $endereco3;?> - <?php echo $cep3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Naturalidade e Data de Nascimento:</b></center></td><td><center><b><?php echo $naturalidade3;?> - <?php echo $dataNascimento3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Nacionalidade:</b></center></td><td><center><b><?php echo $nacionalidade3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Estado civil:</b></center></td><td><center><b><?php echo $estadoCivil3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Profissão:</b></center></td><td><center><b><?php echo $profissao3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b><?php echo $GLOBALS['i18n']['rg'];?>:</b></center></td><td><center><b><?php echo $rg3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>CPF:</b></center></td><td><center><b><?php echo $cpf3;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Cidade onde reside:</b></center></td><td><center><b><?php echo $cidade3;?></b></center></td>
            </tr>
    </table>
    <br><br><br><br><br>
    <center>
    <table cellpadding="10" width="100%">
            <tr><td><center>______________________________________________________________</center></td><td><center>__________________</center></td></tr>
            <tr><td><center>Assinatura</center></td><td><center>Rúbrica</center></td></tr>
    </table>
    <?php }?>
    <?php if($nome4!=""){?>
    <center>
    <br><br><br><br><br>
    <table border="1" style="border:1px solid black" width="100%">
            <tr bgcolor="#CCC">
                    <td width="25%"><center><b>TESOUREIRO(A) DA JUNTA DEPOSITÁRIA</b></center></td><td><center><b><?php echo $nome4;?> - N. CHAVE: <?php echo $codigoAfiliacao4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Endereço e Cep:</b></center></td><td><center><b><?php echo $endereco4;?> - <?php echo $cep4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Naturalidade e Data de Nascimento:</b></center></td><td><center><b><?php echo $naturalidade4;?> - <?php echo $dataNascimento4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Nacionalidade:</b></center></td><td><center><b><?php echo $nacionalidade4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Estado civil:</b></center></td><td><center><b><?php echo $estadoCivil4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Profissão:</b></center></td><td><center><b><?php echo $profissao4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b><?php echo $GLOBALS['i18n']['rg'];?>:</b></center></td><td><center><b><?php echo $rg4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>CPF:</b></center></td><td><center><b><?php echo $cpf4;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Cidade onde reside:</b></center></td><td><center><b><?php echo $cidade4;?></b></center></td>
            </tr>
    </table>
    <br><br><br><br><br>
    <center>
    <table cellpadding="10" width="100%">
            <tr><td><center>______________________________________________________________</center></td><td><center>__________________</center></td></tr>
            <tr><td><center>Assinatura</center></td><td><center>Rúbrica</center></td></tr>
    </table>
    <?php }?>
    <?php if($nome2!=""){?>
    <center>
    <br><br><br><br><br>
    <table border="1" style="border:1px solid black" width="100%">
            <tr bgcolor="#CCC">
                    <td width="25%"><center><b>MESTRE AUXILIAR</b></center></td><td><center><b><?php echo $nome2;?> - N. CHAVE: <?php echo $codigoAfiliacao2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Endereço e Cep:</b></center></td><td><center><b><?php echo $endereco2;?> - <?php echo $cep2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Naturalidade e Data de Nascimento:</b></center></td><td><center><b><?php echo $naturalidade2;?> - <?php echo $dataNascimento2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Nacionalidade:</b></center></td><td><center><b><?php echo $nacionalidade2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Estado civil:</b></center></td><td><center><b><?php echo $estadoCivil2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Profissão:</b></center></td><td><center><b><?php echo $profissao2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b><?php echo $GLOBALS['i18n']['rg'];?>:</b></center></td><td><center><b><?php echo $rg2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>CPF:</b></center></td><td><center><b><?php echo $cpf2;?></b></center></td>
            </tr>
            <tr>
                    <td width="25%"><center><b>Cidade onde reside:</b></center></td><td><center><b><?php echo $cidade2;?></b></center></td>
            </tr>
    </table>
    <br><br><br><br><br>
    <center>
    <table cellpadding="10" width="100%">
            <tr><td><center>______________________________________________________________</center></td><td><center>__________________</center></td></tr>
            <tr><td><center>Assinatura</center></td><td><center>Rúbrica</center></td></tr>
    </table>
    <center>
    <?php }?>
<?php 
}
?>