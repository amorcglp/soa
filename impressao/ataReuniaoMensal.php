<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once('../model/ataReuniaoMensalClass.php');
    include_once '../model/ataReuniaoMensalOficialClass.php';
    include_once '../model/ataReuniaoMensalTopicoClass.php';
    include_once("../lib/webservice/retornaInformacoesMembro.php");
    include_once('../lib/functions.php');
    $arm = new ataReuniaoMensal();
    $resultado = $arm->buscarIdAtaReuniaoMensal($_REQUEST['idAta']);
    $arrAtaReuniaoMensal = array();
    $topico_um		="";
    $topico_dois	="";
    $topico_tres	="";
    $topico_quatro	="";
    $topico_cinco	="";
    $topico_seis	="";
    $topico_sete	="";
    $topico_oito	="";
    $topico_nove	="";
    $topico_dez		="";
    $numeroAssinatura="";
    $temNumeroAssinatura=false;
    $gestaoAnterior=false;
    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];
                    if(trim($vetor['numeroAssinatura'])!="")
                    {
                        $numeroAssinatura = $vetor['numeroAssinatura'];
                        $temNumeroAssinatura=true;
                    }else{
                        $numeroAssinatura = aleatorioAssinatura();
                        $arm->setIdAtaReuniaoMensal($vetor['idAtaReuniaoMensal']);
                        $arm->atualizaNumeroAssinatura($numeroAssinatura);
                    }



                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $data 			= $vetor['dataAtaReuniaoMensal'];
                    $mesCompetencia         = $vetor['mesCompetencia'];
                    $anoCompetencia         = $vetor['anoCompetencia'];
                    $horaInicial 	= substr($vetor['horaInicialAtaReuniaoMensal'],0,5);
                    $horaFinal	 	= substr($vetor['horaFinalAtaReuniaoMensal'],0,5);
                    $nomePresidente = $vetor['nomePresidenteAtaReuniaoMensal'];
                    $numeroMembros 	= $vetor['numeroMembrosAtaReuniaoMensal'];

                    $anoGestaoAtual=substr($vetor['data'],0,4);
                    $gestaoAnterior = verificaSeGestaoAnterior($anoGestaoAtual,$anoCompetencia);

                    $armo = new ataReuniaoMensalOficial();
                    if(!is_numeric($_GET['idAta'])){
                            exit();
                    }
                    $resultado2 = $armo->listaOficiaisAdministrativos($_REQUEST['idAta']);
                    $i=0;
                    if($resultado2)
                    {
                            foreach($resultado2 as $vetor2)
                            {
                                    if($i==0)
                                    {
                                            $oficiaisAdministrativos = retornaNomeCompleto($vetor2['fk_seq_cadastMembro'])." [".retornaCodigoAfiliacao($vetor2['fk_seq_cadastMembro'])."]";
                                    }else{
                                            $oficiaisAdministrativos .= ", ".retornaNomeCompleto($vetor2['fk_seq_cadastMembro'])." [".retornaCodigoAfiliacao($vetor2['fk_seq_cadastMembro'])."]";
                                    }
                                    $i++;
                            }
                    }

                    $armt = new ataReuniaoMensalTopico();
                    $resultado3 = $armt->listaTopicos($_REQUEST['idAta']);
                    if($resultado3)
                    {
                            foreach($resultado3 as $vetor3)
                            {
                                    $topico_um		=$vetor3['topico_um'];
                                    $topico_dois	=$vetor3['topico_dois'];
                                    $topico_tres	=$vetor3['topico_tres'];
                                    $topico_quatro	=$vetor3['topico_quatro'];
                                    $topico_cinco	=$vetor3['topico_cinco'];
                                    $topico_seis	=$vetor3['topico_seis'];
                                    $topico_sete	=$vetor3['topico_sete'];
                                    $topico_oito	=$vetor3['topico_oito'];
                                    $topico_nove	=$vetor3['topico_nove'];
                                    $topico_dez		=$vetor3['topico_dez'];
                            }	
                    }

                    //Verificar se está entregue
                    $pronto = ($vetor['entregue']==1)? true : false;

            }
    }

    $arm = new ataReuniaoMensal();
    $resultado4 = $arm->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($_REQUEST['idAta']);
    $assinaturas="<br>";
    if($resultado4)
    {
        foreach($resultado4 as $vetor4)
        {
            $nomeUsuario = $vetor4['nomeUsuario'];
            $codigoAfiliacao = $vetor4['codigoDeAfiliacao'];
            $funcao = $vetor4['nomeFuncao'];

            $assinaturas .= $nomeUsuario." [".$codigoAfiliacao."]"." - ".$funcao."<br>";
        }
    }

    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 14px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 100px; margin-bottom: 30px;border-color: 1px solid green">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>204 - ATA DA REUNIÃO ADMINISTRATIVA MENSAL</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
                    <b class="fonte tamanho-fonte_2">
                            Mês:
                    </b>
                    <?php echo mesExtensoPortugues($mesCompetencia); ?>
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <?php echo $anoCompetencia; ?>
            </div>
    </center>
    <br>
    <p style="text-indent: 47px">Ata da reunião administrativa mensal, realizada <?php if(substr($data,8,2)!="01"){?>aos <?php echo str_replace("real","",str_replace("reais","",valorPorExtenso(substr($data,8,2)))); ?> dias<?php }else{ echo "no primeiro dia";}?> do mês de <?php echo mesExtensoPortugues(substr($data,5,2)); ?> do ano de <?php echo substr($data,0,4); ?>, na Sede deste Organismo Afiliado, com início às <?php echo $horaInicial; ?>, e término às <?php echo $horaFinal; ?>, sob a presidência do(a) <?php echo $nomePresidente; ?> e com a presença do(s) Oficial(is) Administrativo(s): 
    <?php echo $oficiaisAdministrativos; ?><?php if($numeroMembros>0){?>, mais <?php echo $numeroMembros; ?> Rosacruzes, cujas assinaturas constam do livro de presença<?php }?>.</p>
    <?php if($topico_um){?>
    <p><b>LEITURA DO RELATÓRIO MENSAL ANTERIOR: </b><?php echo  str_replace("<p>","",$topico_um);?> </p>
    <?php }?>
    <?php if($topico_dois){?>
    <p><b>LEITURA DA ATA ANTERIOR: </b><?php echo str_replace("<p>","",$topico_dois);?></p>
    <?php }?>
    <?php if($topico_quatro){?>
    <p><b>ASSUNTOS PENDENTES: </b><?php echo str_replace("<p>","",$topico_quatro);?></p>
    <?php }?>
    <?php if($topico_cinco){?>
    <p><b>ASSUNTOS NOVOS: </b><?php echo str_replace("<p>","",$topico_cinco);?></p>
    <?php }?>
    <?php if($topico_seis){?>
    <p><b>EXPANSÃO DA ORDEM: </b><?php echo str_replace("<p>","",$topico_seis);?></p>
    <?php }?>

    <?php if($topico_oito){?>
    <p><b>LEITURA DAS COMUNICAÇÕES DA SUPREMA GRANDE LOJA, DA GLP E GRANDE CONSELHEIRO(A): </b><?php echo str_replace("<p>","",$topico_oito);?></p>
    <?php }?>
    <?php if($topico_nove){?>
    <p><b>LEITURA DAS COMUNICAÇÕES GERAIS: </b><?php echo str_replace("<p>","",$topico_nove);?></p>
    <?php }?>
    <?php if($topico_tres){?>
    <p><b>RELATÓRIO DAS COMISSÕES: </b><?php echo str_replace("<p>","",$topico_tres);?></p>
    <?php }?>
    <?php if($topico_sete){?>
    <p><b>PALAVRA LIVRE: </b><?php echo str_replace("<p>","",$topico_sete);?></p>
    <?php }?>
    <?php if($topico_dez){?>
    <p><b>ENCERRAMENTO: </b><?php echo str_replace("<p>","",$topico_dez);?></p>
    <?php }?>
    <?php if($pronto){?>
    <br><br>
    <b>Concordaram e assinaram eletrônicamente este documento:</b><br><br>
    <?php }?>

        <center><?php if($pronto){ if($assinaturas!="<br>"){echo $assinaturas;}else{ echo "NINGUÉM ASSINOU ESSE DOCUMENTO";}}?></center><br>

        <?php if($pronto){if($gestaoAnterior){ echo "<br><br>Obs. Devido não ter sido elaborado na gestão (".$anoCompetencia."). A Gestão Atual (".$anoGestaoAtual.") está atualizando e assinando para ficar em dia no sistema.<br><br>";}}?>

        <hr>
        <center><b>Código da Assinatura Eletrônica: <font color="blue"><?php echo $numeroAssinatura; ?></font></b>
            <br><br>(Documento gerado eletronicamente, para validar sua autenticidade utilize a opção validar documento dentro do sistema SOA)
        </center>
        <?php

        ?>
    <!--
    <?php //if ($tipo=="R+C"){?>
        <table width="100%">
                <tr><td width="50%"><center>____________________________</center></td><td><center>____________________________</center></td></tr>
                <tr><td width="50%"><center>Mestre do Organismo Afiliado</center></td><td><center>Secretário do Organismo Afiliado</center></td></tr>
        </table>
    <?php //}else{
        ?>
        <table width="100%">
                <tr><td width="50%"><center>____________________________</center></td><td><center>____________________________</center></td></tr>
                <tr><td width="50%"><center>Mestre da Heptada</center></td><td><center>Arquivista</center></td></tr>
        </table>
        <?php
    //}
?>
<?php
}
?>