<?php
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
?>

    <!-- Sweet Alert -->
    <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <?php
    $nome = isset($_REQUEST['nome']) ? $_REQUEST['nome'] : null;
    $codigoAfiliacao = isset($_REQUEST['codigoAfiliacao']) ? $_REQUEST['codigoAfiliacao'] : null;
    $idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : null;
    $numeroSolicitacoes = isset($_REQUEST['numeroSolicitacoes']) ? $_REQUEST['numeroSolicitacoes'] : 0;

    //Selecionar o OA
    require_once ("../lib/functions.php");
    require_once ("../model/organismoClass.php");
    $o = new organismo();
    $resultado = $o->buscaIdOrganismo($idOrganismoAfiliado);
    $nomeOa = "";
    if ($resultado) {
        foreach ($resultado as $vetor) {
            $nomeOa = organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'], $vetor['tipoOrganismoAfiliado'], $vetor['nomeOrganismoAfiliado'], $vetor['siglaOrganismoAfiliado']);
        }
    }

    $ocultar_json = 1;
    $nomeMembro = $nome;
    $tipoMembro = 1;
    $nomeOAAtuacao = "";
    include '../js/ajax/retornaDadosMembro.php';
    $obj = json_decode(json_encode($return), true);
    $mestreOA = "";
    if (count($obj['result']) > 0) {
        $siglaOA = $obj['result'][0]['fields']['fSigOaAtuacaoRosacr'];
        $dataQuitacao = substr($obj['result'][0]['fields']['fDatQuitacRosacr'],0,10);
        //echo "------------------------------------------------------------------>".$dataQuitacao;
        if ($siglaOA != "") {
            $o = new organismo();
            $resultado = $o->listaOrganismo(null, $siglaOA);
            if ($resultado) {
                foreach ($resultado as $vetor) {
                    $nomeOAAtuacao = organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'], $vetor['tipoOrganismoAfiliado'], $vetor['nomeOrganismoAfiliado'], $vetor['siglaOrganismoAfiliado']);
                    //echo "------------------------------------------------------------------->".$nomeOAAtuacao;		
                }
            }
        }
    }

    if(strtotime($dataQuitacao)>=strtotime(date('Y-m-d')))
    {
    ?>

    <style>
        table , body{
            margin: 0;
            text-align: justify;
            font-family: Calibri;
            /* padding: 5px;
             font-family: Calibri; */
            font-size: 14px!important;
            color: #005678;
            border-collapse:collapse;
            border-color:#DCDCDC;
            max-width: 100%;
            border-spacing: 0;
        }

        p {
            -webkit-margin-before: 0em
        }

        .fonte-1{
            font-size: 1.4em;
            font-family: Arial;
            margin-bottom: 5px;
        }

        .fonte-2{
            font-size: 0.8em;
            font-family: Arial;
            margin-bottom: 20px;
        }

        .fonte-3{
            font-size: 0.9em;
            font-family: Arial;
        }

        .cinza{
            background-color:#F5F5F5;
        }
        .centro{
            text-align:center;
        }

        /* Table Base */

        .table { 
            width: 100%;
            margin-bottom: 20px;
        }

        .table th,
        .table td {
            font-weight: normal;
            font-size: 12px;
            padding: 8px 15px;
            line-height: 20px;
            text-align: left;
            vertical-align: middle;
            border-top: 1px solid #dddddd;
        }
        .table thead th {
            vertical-align: bottom;
            font-weight:bold;
        }      
        .table .t-small {
            width: 5%;
        }
        .table .t-medium {
            width: 10%;
        }
        .table .t-nome {
            width: 30%;
        }
        .table .t-status {
            font-weight: bold;
        }
        .table .t-active {
            color: #46a546;
        }
        .table .t-inactive {
            color: #e00300;
        }
        .table .t-draft {
            color: #f89406;
        }
        .table .t-scheduled {
            color: #049cdb;
        }

        .linha-tracejada {
            border:1px dashed black;
            margin-top: 15px;
        }

        /* Small Sizes */
        @media (max-width: 480px) { 
            .table-action thead {
                display: none;
            }
            .table-action tr {
                border-bottom: 1px solid #dddddd;
            }
            .table-action td {
                border: 0;
            }
            .table-action td:not(:first-child) {
                display: block;
            }
        }
        .solado {width: 225px; height: 60px;}
    </style>
    <style media="print">
        .oculto {display: none;}
    </style>
    <?php
    if ($numeroSolicitacoes >= 3) {
        ?>
        <script>
            window.onload = function () {
                swal({
                    title: "Aviso!",
                    text: "O número de solicitações é maior ou igual à 3! Convide o membro para fazer parte do Organismo!",
                    type: "error",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
        <?php
    }
    ?>
    <div>
        <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    </div>
    <div style="border: 1px solid #191970; height: 185px; padding: 10px">
        <div style="width: 35%; float: left; height: 175px">
            <div style="margin-top: 13px">
                <center>
                    <p class="solado">
                        <img class="solado" src="../img/logo_sol_alado_azul.png">
                    </p>
                </center>
                <center><div class="fonte-1">AUTORIZAÇÃO TEMPLO</div></center>
                <center><div class="fonte-2">(ÚNICA, NOMINAL E INTRANSFERÍVEL)</div></center>
                <center><p class="fonte-3"><b>Número de Solicitações: <?php echo $numeroSolicitacoes; ?></b></p></center>
                <center><p class="fonte-3"><b><?php echo $nomeOa; ?></b></p></center>
            </div>
        </div>
        <div style="width: 60%; float: right; height: 175px; padding: 0px 10px 0px 15px;">
            <p align="right">
                Curitiba, <?php echo date("d/m/Y"); ?>.
            </p>
            <p>
                Respeitável Guardião
            </p>
            <p>
                O portador desta, <b><?php echo $nome; ?></b>, Cód. de Afiliação 
                <b><?php echo $codigoAfiliacao; ?></b> 
                poderá assistir a Convocação Ritualística desde que comprove estar ativo na AMORC.
    <?php if ($nomeOAAtuacao != "" && $nomeOa != $nomeOAAtuacao) { ?>Cujo Organismo de origem é <?php echo $nomeOAAtuacao; ?>.<?php } ?>
            </p>
            <p>
                A secretaria: [&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]
                &nbsp;&nbsp;&nbsp;__________________________________.
            </p>
            <div>
                <b>Obs.: Está autorização é válida de ___/___/___ até ___/___/___</b>.
            </div>
        </div>
    </div>
    <hr class="linha-tracejada">
    <?php }else{
    ?>
        <script>
            window.onload = function () {
                swal({
                    title: "Aviso!",
                    text: "Membro inativo! Não é possível imprimir autorização!",
                    type: "error",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
        <?php
    }
    ?>
    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
<?php
}
?>
