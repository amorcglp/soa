<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<?php
$mesAtual = isset($_REQUEST['mesAtual'])?$_REQUEST['mesAtual']:null;
$anoAtual = isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:null;
$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:null;

include_once("../model/rendimentoClass.php");
include_once("../lib/functions.php");

$arquivo = 'SOA-RELATORIO-FINANCEIRO-SALDO-'.$mesAtual.'-'.$anoAtual.'.xls';

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

$rend = new Rendimento();
?>
<table class="table table-striped table-bordered table-hover dataTables-example" id="editable2" >
    <thead>
        <tr>
            <td colspan="6"><b><?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);?></b></td>
        </tr>
        <tr>
            <td><b>Mês</b></td>
            <td><?php echo $mesAtual;?></td>
            <td><b>Ano</b></td>
            <td><?php echo $anoAtual;?></td>
            <td></td>
            
        </tr>
                        <tr id="linha0">
							            	<th width="100">Data</th>
							                <th width="100">Descrição</th>
							                <th width="100">Valor</th>
							                <th width="100">Categoria</th>
							                <th width="100">Atribuído à</th>
							                
							            </tr>
							            </thead>
							            <tbody id="tabela">
							            	<?php
							            		$totalG=0;
                                                                                $resultado7 = $rend->listaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado);
												$i=0;
                                                                                if ($resultado7) {
                                                                                    foreach ($resultado7 as $vetor) {
                                                                                            $i++;
                                                                                            $valor = str_replace(",",".",str_replace(".","",$vetor['valorRendimento']));
                                                                                            $totalG += preg_replace("/[^0-9.]/", "",$valor);
                                                                                    ?>
                                                                                    <tr id="linha<?php echo $vetor['idRendimento'];?>">
                                                                                            <td id="1td"><?php echo substr($vetor['dataVerificacao'],8,2)."/".substr($vetor['dataVerificacao'],5,2)."/".substr($vetor['dataVerificacao'],0,4);?></div></td>
                                                                                            <td id="2td"><?php echo $vetor['descricaoRendimento'];?></div></td>
                                                                                            <td id="3td"><?php echo $vetor['valorRendimento'];?></div></td>
                                                                                            <td id="4td"><?php echo retornaCategoriaRendimento($vetor['categoriaRendimento']);?></div></td>
                                                                                            <td id="5td"><?php echo retornaAtribuidoARendimento($vetor['atribuidoA']);?></div></td>
                                                                                            
                                                                                    </tr>
                                                                            <?php 		}
					                            	}?>
					                         
							            </tbody>
						            </table>