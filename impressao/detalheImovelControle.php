<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once("../controller/imovelControleController.php");
    require_once ("../model/imovelControleClass.php");
    require_once ("../model/usuarioClass.php");

    $idImovelControle	= isset($_REQUEST['idImovelControle']) ? json_decode($_REQUEST['idImovelControle']) : '';

    $icc = new imovelControleController();
    $dados = $icc->buscaImovelControle($idImovelControle);
    ?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Sistema dos Organismos Afiliados</title>

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
                    <div class="gray-bg" style="min-height: 815px;">
                <div class="wrapper wrapper-content  animated fadeInRight article">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="text-center article-title">
                                        <h1>
                                            <?php
                                            include_once("../controller/organismoController.php");
                                            require_once ("../model/organismoClass.php");

                                            $oc = new organismoController();
                                            $dadosOa = $oc->buscaOrganismo($dados->getFk_idOrganismoAfiliado());
                                            ?>
                                            <?php
                                            switch ($dadosOa->getClassificacaoOrganismoAfiliado()) {
                                                case 1:
                                                    echo "Loja";
                                                    break;
                                                case 2:
                                                    echo "Pronaos";
                                                    break;
                                                case 3:
                                                    echo "Capítulo";
                                                    break;
                                                case 4:
                                                    echo "Heptada";
                                                    break;
                                                case 5:
                                                    echo "Atrium";
                                                    break;
                                            }
                                            ?>
                                            <?php
                                            switch ($dadosOa->getTipoOrganismoAfiliado()) {
                                                case 1:
                                                    echo "R+C";
                                                    break;
                                                case 2:
                                                    echo "TOM";
                                                    break;
                                            }
                                            ?>
                                            <?php echo $dadosOa->getNomeOrganismoAfiliado(); ?>
                                            -
                                            <?php echo $dadosOa->getSiglaOrganismoAfiliado(); ?>
                                        </h1>
                                    </div>
                                    <hr>
                                            <div class="row">					
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Imóvel: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                        <?php
                                                        include_once("../controller/imovelController.php");
                                                        require_once ("../model/imovelClass.php");

                                                        $ic = new imovelController();
                                                        $dadosI = $ic->buscaImovel($dados->getFk_idImovel());
                                                        ?>
                                                        <?php echo $dadosI->getEnderecoImovel().' - '.$dadosI->getNumeroImovel().' - '.$dadosI->getComplementoImovel();?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Status: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <div class="statusTarget<?php echo $vetor['idImovelControle']?>">
                                                                                                            <?php switch($dados->getStatusImovelControle()){
                                                                                                                    case 0:
                                                                                                                            echo "<span class='badge badge-primary'>Ativo</span>";
                                                                                                                            break;
                                                                                                                    case 1:
                                                                                                                            echo "<span class='badge badge-danger'>Inativo</span>";
                                                                                                                            break;
                                                                                                            }
                                                                                                            ?>
                                                                                                            </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Referente ao Ano de: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <?php echo $dados->getReferenteAnoImovelControle(); ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">IPTU: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <?php switch($dados->getPropriedadeIptuImovelControle()){
                                                                                                                    case 0:
                                                                                                                            echo "Pago";
                                                                                                                            break;
                                                                                                                    case 1:
                                                                                                                            echo "Em debito";
                                                                                                                            break;
                                                                                                                    case 2:
                                                                                                                            echo "Isento";
                                                                                                                            break;
                                                                                                                    case 3:
                                                                                                                            echo "Imune";
                                                                                                                            break;
                                                                                                                    case 4:
                                                                                                                            echo "Outro";
                                                                                                                            break;
                                                                                                                    }
                                                                                                            ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php if($dados->getPropriedadeIptuImovelControle()==4){?>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Observações IPTU: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <?php echo $dados->getPropriedadeIptuObsImovelControle(); ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php }?>
                                            <hr>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Houve Construção? </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <?php switch($dados->getConstrucaoImovelControle()){
                                                                                                                    case 0:
                                                                                                                            echo "Sim";
                                                                                                                            break;
                                                                                                                    case 1:
                                                                                                                            echo "Não";
                                                                                                                            break;
                                                                                                                    }
                                                                                                            ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php if($dados->getConstrucaoImovelControle() == 1){?>
                                            <hr>
                                            <?php }?>
                                            <?php if($dados->getConstrucaoImovelControle() == 0){?>
                                                    <hr>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Restrições na Construção: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getConstrucaoRestricoesImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Sim";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Não";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php if($dados->getConstrucaoRestricoesAnotacoesImovelControle() != ''){?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">São elas: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px; text-align: justify">
                                                                                    <?php echo $dados->getConstrucaoRestricoesAnotacoesImovelControle(); ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php }?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Construção Concluída: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getConstrucaoConcluidaImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Sim";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Não";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php if($dados->getConstrucaoTerminoDataImovelControle() != ''){?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Data/Prazo de Término da Construção: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php echo $dados->getConstrucaoTerminoDataImovelControle();?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php }?>
                                                    <hr>
                                            <?php }?>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Houve Manutenção? </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <?php switch($dados->getManutencaoImovelControle()){
                                                                                                                    case 0:
                                                                                                                            echo "Sim";
                                                                                                                            break;
                                                                                                                    case 1:
                                                                                                                            echo "Não";
                                                                                                                            break;
                                                                                                                    }
                                                                                                            ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <?php if($dados->getManutencaoImovelControle() == 1){?>
                                            <hr>
                                            <?php }?>
                                            <?php if($dados->getManutencaoImovelControle() == 0){?>
                                                    <hr>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Pintura: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getManutencaoPinturaImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Sim";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Não";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php if($dados->getManutencaoPinturaImovelControle() == 0){?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getManutencaoPinturaTipoImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Anual";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Bienal";
                                                                                                                                    break;
                                                                                                                            case 2:
                                                                                                                                    echo "Trienal";
                                                                                                                                    break;
                                                                                                                            case 3:
                                                                                                                                    echo "Quadrienal";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php }?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Reparo de Hidráulica: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getManutencaoHidraulicaImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Sim";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Não";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php if($dados->getManutencaoHidraulicaImovelControle() == 0){?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getManutencaoHidraulicaTipoImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Anual";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Bienal";
                                                                                                                                    break;
                                                                                                                            case 2:
                                                                                                                                    echo "Trienal";
                                                                                                                                    break;
                                                                                                                            case 3:
                                                                                                                                    echo "Quadrienal";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php }?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Reparo de Elétrica: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getManutencaoEletricaImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Sim";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Não";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php if($dados->getManutencaoEletricaImovelControle() == 0){?>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    <?php switch($dados->getManutencaoEletricaTipoImovelControle()){
                                                                                                                            case 0:
                                                                                                                                    echo "Anual";
                                                                                                                                    break;
                                                                                                                            case 1:
                                                                                                                                    echo "Bienal";
                                                                                                                                    break;
                                                                                                                            case 2:
                                                                                                                                    echo "Trienal";
                                                                                                                                    break;
                                                                                                                            case 3:
                                                                                                                                    echo "Quadrienal";
                                                                                                                                    break;
                                                                                                                            }
                                                                                                                    ?>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <?php }?>
                                                    <hr>
                                            <?php }?>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Anotações Complementares: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px; text-align: justify">
                                                                            <?php echo $dados->getAnotacoesImovelControle(); ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                    <hr>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Último a atualizar: </label>
                                            <div class="col-sm-8">
                                                <?php
                                                $um = new Usuario();
                                                $resultadoAtualizadoPor = $um->buscaUsuario($dados->getFk_seqCadastAtualizadoPor());
                                                if ($resultadoAtualizadoPor) {
                                                    foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                                        echo $vetorAtualizadoPor['nomeUsuario'];
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- Mainly scripts -->
        <script src="../js/jquery-2.1.1.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="../js/inspinia.js"></script>
        <script src="../js/plugins/pace/pace.min.js"></script>
    </body>
    </html>

<?php
}
?>