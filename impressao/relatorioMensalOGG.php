<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $mesAtual                       = isset($_REQUEST['mesAtual']) ? $_REQUEST['mesAtual'] : null;
    $anoAtual                       = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual'] : null;
    $idOrganismoAfiliado            = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : null;

    include_once('../lib/functions.php');
    include_once('../controller/organismoController.php');
    include_once("../model/convocacaoRitualisticaOGGClass.php");
    include_once("../model/convocacaoRitualisticaOGGMembroClass.php");
    include_once("../model/convocacaoRitualisticaOGGNaoMembroClass.php");

    $om = new organismo();
    $oc = new organismoController();

    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        table , body{
            margin: 0;
            text-align: left;
            font-family: Avenir LT Std;
            font-size: 12px!important;
            color: #000;
            border-collapse:collapse;
            border-color:#DCDCDC;
            max-width: 100%;
            border-spacing: 0;
        }
        .fonte{
            font-family: Minion Pro;
        }

        .tamanho-fonte{
            font-size: 14px;
        }

        .tamanho-fonte_2{
            margin-top:0;
            font-size: 12px!important;
        }

        .fonte-3 {
            font-family: Avenir LT Std;
        }

        .cinza{
            background-color:#F5F5F5;
        }
        .centro{
            text-align:center;
        }

        /* Table Base */

        .table {
          width: 100%;
        }

        .table th,
        .table td {
          font-weight: normal;
          font-size: 12px;
          padding: 8px 15px;
          line-height: 20px;
          text-align: left;
          vertical-align: middle;
          
        }
        .table thead th {
          vertical-align: bottom;
          font-weight:bold;
        }
        .table .t-small {
          width: 5%;
        }
        .table .t-medium {
          width: 10%;
        }
        .table .t-nome {
          width: 30%;
        }
        .table .t-status {
          font-weight: bold;
        }
        .table .t-active {
          color: #46a546;
        }
        .table .t-inactive {
          color: #e00300;
        }
        .table .t-draft {
          color: #f89406;
        }
        .table .t-scheduled {
          color: #049cdb;
        }

        /* Small Sizes */
        @media (max-width: 480px) {
          .table-action thead {
            display: none;
          }
          .table-action tr {
            border-bottom: 1px solid #dddddd;
          }
          .table-action td {
            border: 0;
          }
          .table-action td:not(:first-child) {
            display: block;
          }
        }

        #cabecalho {height: 90px}
        #oa {width: 260px; height: 19px; padding-left: 14px}
        #data {width: 131px; height: 19px}
        #ano {width: 61px; height: 19px}

        .solado {}

    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2" style="margin-bottom: 2px"><b>RELATÓRIO MENSAL - ORDEM GUIAS DO GRAAL - OGG</b></p></center>
    <br>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado); ?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Mês:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo mesExtensoPortugues($mesAtual); ?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $anoAtual; ?>
                    </span>
            </div>
    </center>
    <br>
    <?php
    $dadosOrganismo     =  $oc->buscaOrganismo($idOrganismoAfiliado);
        
    

    ?>
    <center>
        <table border="1" class="table table-action">
            <thead>
                <tr>
                    <th width="5%" colspan="2"><b><center>COMENDADOR - AUXILIARES - OFICIAIS / NOMES E Nºs INSCRICAO</center></b></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td width="25%">Mestre</td>
                    <td>
                        <?php 
                            $tipoMembro=1;
                            $siglaPais='BR';
                            $ocultar_json=1;
                            $naoAtuantes='N';
                            $atuantes='S';
                            $siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
                            $seqFuncao='201';
                            include '../js/ajax/retornaFuncaoMembro.php';
                            //echo "<pre>";print_r($return);
                            $obj = json_decode(json_encode($return),true);
                            $funcaoOA="";
                            $cod="";
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                            {
                                    $funcaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                                    $cod=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'];
                            }
                            if($funcaoOA!="")
                            {    
                                echo $funcaoOA." - Nº ".$cod;
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Comendador</td>
                    <td>
                        <?php 
                            $ocultar_json=1;
                            $naoAtuantes='S';
                            $atuantes='S';
                            $siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
                            $seqFuncao='605';
                            include '../js/ajax/retornaFuncaoMembro.php';
                            $obj = json_decode(json_encode($return),true);
                            //echo "<pre>";print_r($return);
                            $funcaoOA="";
                            $cod="";
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                            {
                                    $funcaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                                    $cod=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'];
                            }
                            if($funcaoOA!="")
                            {    
                                echo $funcaoOA." - Nº ".$cod;
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Mestre OGG</td>
                    <td>
                        <?php 
                        /*
                            $ocultar_json=1;
                            $naoAtuantes='N';
                            $atuantes='S';
                            $siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
                            $seqFuncao='703';
                            include '../js/ajax/retornaFuncaoMembro.php';
                            $obj = json_decode(json_encode($return),true);
                            $funcaoOA="";
                            $funcaoOA2="";
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                            {
                                    $funcaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                            }
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
                            {
                                $arr = ordenaOficialAtuanteRetirante(
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                                        );
                                $funcaoOA = $arr[0];
                                $funcaoOA2 = $arr[1];
                            }
                            echo $funcaoOA;*/
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Guardião-Graal</td>
                    <td>
                        <?php 
                        /*
                            $ocultar_json=1;
                            $naoAtuantes='N';
                            $atuantes='S';
                            $siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
                            $seqFuncao='705';
                            include '../js/ajax/retornaFuncaoMembro.php';
                            $obj = json_decode(json_encode($return),true);
                            $funcaoOA="";
                            $funcaoOA2="";
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                            {
                                    $funcaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                            }
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
                            {
                                $arr = ordenaOficialAtuanteRetirante(
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                                        );
                                $funcaoOA = $arr[0];
                                $funcaoOA2 = $arr[1];
                            }
                            echo $funcaoOA;*/
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Sentinela</td>
                    <td>
                        <?php 
                        /*
                            $ocultar_json=1;
                            $naoAtuantes='N';
                            $atuantes='S';
                            $siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
                            $seqFuncao='709';//Secretario do OA
                            include '../js/ajax/retornaFuncaoMembro.php';
                            $obj = json_decode(json_encode($return),true);
                            $funcaoOA="";
                            $funcaoOA2="";
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                            {
                                    $funcaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                            }
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
                            {
                                $arr = ordenaOficialAtuanteRetirante(
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                                        );
                                $funcaoOA = $arr[0];
                                $funcaoOA2 = $arr[1];
                            }
                            echo $funcaoOA;*/
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Sonoplasta</td>
                    <td>
                        <?php 
                        /*
                            $ocultar_json=1;
                            $naoAtuantes='N';
                            $atuantes='S';
                            $siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
                            $seqFuncao='711';//Secretario do OA
                            include '../js/ajax/retornaFuncaoMembro.php';
                            $obj = json_decode(json_encode($return),true);
                            $funcaoOA="";
                            $funcaoOA2="";
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                            {
                                    $funcaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                            }
                            if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
                            {
                                $arr = ordenaOficialAtuanteRetirante(
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                                        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                                        );
                                $funcaoOA = $arr[0];
                                $funcaoOA2 = $arr[1];
                            }
                            echo $funcaoOA;*/
                        ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <h3>Convocações Ritualísticas</h3>
        <table border="1" width="100%">
            <tr>
                <td rowspan="2"><center>DIA</center></td><td colspan="3"><center>FREQUÊNCIA</center></td><td rowspan="2"><center>TITULO DA MENSAGEM</center></td><td rowspan="2"><center>COMENDADOR</center></td>
            </tr>
            <tr>
                <td><center>Adultos</center></td><td><center>Jovens</center></td><td><center>TOTAL</center></td>
            </tr>
            <?php 
                include_once("../controller/convocacaoRitualisticaOGGController.php");
                $cr = new convocacaoRitualisticaOGGController();
                
                $resultado = $cr->lista($idOrganismoAfiliado,$mesAtual,$anoAtual);
                $totalGeral=0;
                if($resultado)
                {
                    foreach ($resultado as $vetor)
                    {    
                        $dataConvocacao = substr($vetor['dataConvocacao'],8,2)."/".substr($vetor['dataConvocacao'],5,2)."/".substr($vetor['dataConvocacao'],0,4). " ".$vetor['horaConvocacao'];

                        $membros = new convocacaoRitualisticaOGGMembro();
                        $totalMembrosAdulto = $membros->total($vetor['idConvocacaoRitualisticaOGG'],null,1);
                        $totalMembrosJovem = $membros->total($vetor['idConvocacaoRitualisticaOGG'],null,2);

                        $nao_membros = new convocacaoRitualisticaOGGNaoMembro();
                        $totalNaoMembrosAdulto = $nao_membros->total($vetor['idConvocacaoRitualisticaOGG'],null,1);
                        $totalNaoMembrosJovem = $nao_membros->total($vetor['idConvocacaoRitualisticaOGG'],null,2);

                        $totalJovem = $totalMembrosJovem+$totalNaoMembrosJovem;
                        $totalAdulto = $totalMembrosAdulto+$totalNaoMembrosAdulto;

                        $total = $totalJovem+$totalAdulto;
                        
                        $totalGeral += $total;
                
            ?>
            <tr>
                <td><center><?php echo substr($vetor['dataConvocacao'],8,2);?></center></td><td><center><?php echo $totalAdulto;?></center></td><td><center><?php echo $totalJovem;?></center></td><td><center><?php echo $total;?></center></td><td><center><?php echo $vetor['tituloMensagem'];?></center></td><td><center><?php echo $vetor['nomeComendador'];?></center></td>
            </tr>
            <?php
                    }
                }
            ?>
            <tr>
                <td></td><td></td><td><center>TOTAL</center></td><td><center><?php echo $totalGeral;?></center></td><td></td><td></td>
            </tr>
        </table>
        <h3>O CHAMADO</h3>
        <table border="1" width="100%">
            <tr>
                <td rowspan="2"><center>DIA</center></td><td colspan="4"><center>FREQUÊNCIA</center></td><td rowspan="2"><center>MESTRE OGG</center></td>
            </tr>
            <tr>
                <td><center>Buscadores Adultos</center></td><td><center>Buscadores Jovens</center></td><td><center>Testemunhas</center></td><td><center>TOTAL</center></td>
            </tr>
            <?php 
                $comentarios="";
                $totalGeral=0;
                include_once("../controller/ritoPassagemOGGController.php");
                $cr = new ritoPassagemOGGController();
                $resultado = $cr->lista($idOrganismoAfiliado,$mesAtual,$anoAtual,1);

                if($resultado)
                {
                    foreach ($resultado as $vetor)
                    {    
                        $data = substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)."/".substr($vetor['data'],0,4). " ".$vetor['hora'];

                        $membros = new ritoPassagemOGGMembro();
                        $totalMembrosAdulto = $membros->total($vetor['idRitoPassagemOGG'],null,1);
                        $totalMembrosJovem = $membros->total($vetor['idRitoPassagemOGG'],null,2);

                        $testemunhas = new ritoPassagemOGGMembroTestemunha();
                        $totalMembrosTestemunhaAdulto = $testemunhas->total($vetor['idRitoPassagemOGG'],null,1);
                        $totalMembrosTestemunhaJovem = $testemunhas->total($vetor['idRitoPassagemOGG'],null,2);

                        $nao_membros = new ritoPassagemOGGNaoMembro();
                        $totalNaoMembrosAdulto = $nao_membros->total($vetor['idRitoPassagemOGG'],null,1);
                        $totalNaoMembrosJovem = $nao_membros->total($vetor['idRitoPassagemOGG'],null,2);

                        $totalJovem = $totalMembrosJovem+$totalNaoMembrosJovem;
                        $totalAdulto = $totalMembrosAdulto+$totalNaoMembrosAdulto;

                        $totalTestemunha = $totalMembrosTestemunhaJovem+$totalMembrosTestemunhaAdulto;

                        $total = $totalJovem+$totalAdulto+$totalTestemunha;
                        
                        $totalGeral += $total;
                
            ?>
            <tr>
                <td><center><?php echo substr($vetor['data'],8,2);?></center></td><td><center><?php echo $totalAdulto;?></center></td><td><center><?php echo $totalJovem;?></center></td><td><center><?php echo $totalTestemunha;?></center></td><td><center><?php echo $total;?></center></td><td><center><?php echo $vetor['nomeMestre'];?></center></td>
            </tr>
            <?php
                        $comentarios .= "Data: ".substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)." - ".$vetor['comentario']."<br>";
                    }
                }
            ?>
            <tr>
                <td></td><td></td><td></td><td><center>TOTAL</center></td><td><center><?php echo $totalGeral;?></center></td><td></td>
            </tr>
        </table>
        <h3>RITOS DE PASSAGEM</h3>
        <table border="1" width="100%">
            <tr>
                <td rowspan="2"><center>DIA</center></td><td colspan="4"><center>FREQUÊNCIA</center></td><td rowspan="2"><center>RITO</center></td><td rowspan="2"><center>MESTRE OGG</center></td>
            </tr>
            <tr>
                <td><center>Buscadores Adultos</center></td><td><center>Buscadores Jovens</center></td><td><center>Testemunhas</center></td><td><center>TOTAL</center></td>
            </tr>
            <?php 
                include_once("../controller/ritoPassagemOGGController.php");
                $cr = new ritoPassagemOGGController();
                $resultado = $cr->lista($idOrganismoAfiliado,$mesAtual,$anoAtual,null,1);
                $totalGeral=0;
                if($resultado)
                {
                    foreach ($resultado as $vetor)
                    {    
                        $data = substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)."/".substr($vetor['data'],0,4). " ".$vetor['hora'];

                        $membros = new ritoPassagemOGGMembro();
                        $totalMembrosAdulto = $membros->total($vetor['idRitoPassagemOGG'],null,1);
                        $totalMembrosJovem = $membros->total($vetor['idRitoPassagemOGG'],null,2);

                        $testemunhas = new ritoPassagemOGGMembroTestemunha();
                        $totalMembrosTestemunhaAdulto = $testemunhas->total($vetor['idRitoPassagemOGG'],null,1);
                        $totalMembrosTestemunhaJovem = $testemunhas->total($vetor['idRitoPassagemOGG'],null,2);

                        $nao_membros = new ritoPassagemOGGNaoMembro();
                        $totalNaoMembrosAdulto = $nao_membros->total($vetor['idRitoPassagemOGG'],null,1);
                        $totalNaoMembrosJovem = $nao_membros->total($vetor['idRitoPassagemOGG'],null,2);

                        $totalJovem = $totalMembrosJovem+$totalNaoMembrosJovem;
                        $totalAdulto = $totalMembrosAdulto+$totalNaoMembrosAdulto;

                        $totalTestemunha = $totalMembrosTestemunhaJovem+$totalMembrosTestemunhaAdulto;

                        $total = $totalJovem+$totalAdulto+$totalTestemunha;
                        
                        $totalGeral += $total;
                
            ?>
            <tr>
                <td><center><?php echo substr($vetor['data'],8,2);?></center></td>
                <td><center><?php echo $totalAdulto;?></center></td>
                <td><center><?php echo $totalJovem;?></center></td>
                <td><center><?php echo $totalTestemunha;?></center></td>
                <td><center><?php echo $total;?></center></td>
                <td><center><?php 
                                switch ($vetor['rito']){
                                    case 1:
                                        echo "O Chamado";
                                        break;
                                    case 2:
                                        echo "Guardião do Castelo";
                                        break;
                                    case 3:
                                        echo "Escudeiro da Verdade";
                                        break;
                                    case 4:
                                        echo "Fiel Servidor";
                                        break;
                                    case 5:
                                        echo "Cavaleiro da Perseverança";
                                        break;
                                    case 6:
                                        echo "Guias do Graal";
                                        break;
                                }
                            ?></center></td>
                <td><center><?php echo $vetor['nomeMestre'];?></center></td>
            </tr>
            <?php
                        $comentarios .= "Data: ".substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)." - ".$vetor['comentario']."<br>";
                    }
                }
            ?>
            <tr>
                <td></td><td></td><td></td><td><center>TOTAL</center></td><td><center><?php echo $totalGeral;?></center></td><td></td><td></td>
            </tr>
        </table>
        <h3>DEMAIS COMENTÁRIOS DO COMENDADOR</h3>
        <table border="1" width="100%">
            <tr>
                <td><?php echo $comentarios;?></td>
            </tr>
        </table>
    </center>
    <table class="table">
        <tbody>
            <center>
                <tr>
                    <td width="25%">&nbsp;</td>
                    <td width="25%" style="text-align: right">Data:</td>
                    <td width="25%" style="text-align: left">
                        <?php
                            echo date('d/m/Y');
                        ?>
                    </td>
                    <td width="25%">&nbsp;</td>
                </tr>
            </center>
        </tbody>
    </table>
    <?php
        require_once '../model/oggMensalClass.php';
        $om = new oggMensal();
        $resultado4 = $om->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado);
        $assinaturas="<br>";
        if($resultado4)
        {
            foreach($resultado4 as $vetor4)
            {
                $nomeUsuario = $vetor4['nomeUsuario'];
                $codigoAfiliacao = $vetor4['codigoDeAfiliacao'];
                $funcao = $vetor4['nomeFuncao'];

                $assinaturas .= $nomeUsuario." [".$codigoAfiliacao."]"." - ".$funcao."<br>";
            }
        }

        //Verificar se está entregue
        $resultado = $om->listaOggMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
        if($resultado)
        {
            foreach ($resultado as $vetor)
            {
                if(trim($vetor['numeroAssinatura'])!="")
                {
                    $numeroAssinatura = $vetor['numeroAssinatura'];
                    $temNumeroAssinatura=true;
                }else{
                    $numeroAssinatura = aleatorioAssinatura();
                    $om->atualizaNumeroAssinatura($mesAtual,$anoAtual,$idOrganismoAfiliado,$numeroAssinatura);
                }
            }
            $pronto = true;
        }else{
            $pronto = false;
        }
        ?>
        <br>
        <?php if($pronto){?>
        <br><br>
        <b>Concordaram e assinaram eletrônicamente este documento:</b><br><br>
    <?php }?>

        <center><?php if($pronto){
        if($assinaturas!="<br>"){echo $assinaturas;}else{ echo "NINGUÉM ASSINOU ESSE DOCUMENTO";}?></center><br>
        <hr>
        <center><b>Código da Assinatura Eletrônica: <font color="blue"><?php echo $numeroAssinatura; ?></font></b>
            <br><br>(Documento gerado eletronicamente, para validar sua autenticidade utilize a opção validar documento dentro do sistema SOA)
        </center>
        <?php }else{
            echo "NINGUÉM ASSINOU ESSE DOCUMENTO ELETRONICAMENTE<br><br>";
        }
}
?>
