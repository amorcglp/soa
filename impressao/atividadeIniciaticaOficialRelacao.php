<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idAtividadeIniciaticaOficial = isset($_REQUEST['idAtividadeIniciaticaOficial']) ? $_REQUEST['idAtividadeIniciaticaOficial'] : '';
    $idOrganismo = isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';

    include_once("../controller/atividadeIniciaticaOficialController.php");
    include_once("../controller/membroOAController.php");
    include_once("../controller/organismoController.php");
    $aioc 	= new atividadeIniciaticaOficialController();
    $moam 	= new membroOA();
    $oc 	= new organismoController();

    $nomeOrganismo = '';
    $dadosOa = $oc->buscaOrganismo($idOrganismo);
    switch ($dadosOa->getClassificacaoOrganismoAfiliado()) {
            case 1: $nomeOrganismo = "Loja "; break;
            case 2: $nomeOrganismo = "Pronaos "; break;
            case 3: $nomeOrganismo = "Capítulo "; break;
            case 4: $nomeOrganismo = "Heptada "; break;
            case 5: $nomeOrganismo = "Atrium "; break;
    }
    switch ($dadosOa->getTipoOrganismoAfiliado()) {
            case 1: $nomeOrganismo .= "R+C "; break;
            case 2: $nomeOrganismo .= "TOM "; break;
    }
    $nomeOrganismo .= $dadosOa->getNomeOrganismoAfiliado()." - ";
    $nomeOrganismo .= $dadosOa->getSiglaOrganismoAfiliado();

    $dados 			= $aioc->buscaAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial);
    $ocultar_json=1;

    $seqCadast = $dados->getMestreAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMestre          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMestre                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codAfiliacaoMestre             = "";
        $nomeMestre                     = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getMestreAuxAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMestreAux           = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMestreAux                      = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoMestreAux           = "";
        $nomeMestreAux                      = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getArquivistaAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoArquivista         = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeArquivista                 = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoArquivista         = "";
        $nomeArquivista                 = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getCapelaoAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoCapelao          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeCapelao                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoCapelao             = "";
        $nomeCapelao                     = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getMatreAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMatre          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMatre                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoMatre             = "";
        $nomeMatre                     = "";
    }
    $seqCadast = 0;


    $seqCadast = $dados->getGrandeSacerdotisaAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGrandeSacerdotisa          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGrandeSacerdotisa                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGrandeSacerdotisa             = "";
        $nomeGrandeSacerdotisa                     = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getGuiaAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGuia          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGuia                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGuia             = "";
        $nomeGuia                     = "";
    }
    $seqCadast = 0;


    $seqCadast = $dados->getGuardiaoInternoAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGuardiaoInterno          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGuardiaoInterno                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGuardiaoInterno             = "";
        $nomeGuardiaoInterno                     = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getGuardiaoExternoAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGuardiaoExterno          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGuardiaoExterno                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGuardiaoExterno             = "";
        $nomeGuardiaoExterno                     = "";
    }
    $seqCadast = 0;


    $seqCadast = $dados->getArchoteAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoArchote          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeArchote                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoArchote             = "";
        $nomeArchote                     = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getMedalhistaAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMedalhista          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMedalhista                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoMedalhista             = "";
        $nomeMedalhista                     = "";
    }
    $seqCadast = 0;


    $seqCadast = $dados->getArautoAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoArauto          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeArauto                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoArauto             = "";
        $nomeArauto                     = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getAdjutorAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoAdjutor          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeAdjutor                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoAdjutor             = "";
        $nomeAdjutor                     = "";
    }
    $seqCadast = 0;

    $seqCadast = $dados->getSonoplastaAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoSonoplasta          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeSonoplasta                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoSonoplasta             = "";
        $nomeSonoplasta                     = "";
    }
    $seqCadast = 0;

    /*
    $retornoColumba 			= $moam->buscaMembroOaPeloSeqCadast($dados->getColumbaAtividadeIniciaticaOficial());
    if($retornoColumba){
            foreach($retornoColumba as $vetorColumba){
                    $codigoAfiliacaoColumba 		= $vetorColumba['codigoAfiliacao'];
                    $nomeColumba					= $vetorColumba['nomeMembroOa'];
            }
    }
    $retornoRecepcao 			= $moam->buscaMembroOaPeloSeqCadast($dados->getRecepcaoAtividadeIniciaticaOficial());
    if($retornoRecepcao){
            foreach($retornoRecepcao as $vetorRecepcao){
                    $codigoAfiliacaoRecepcao 		= $vetorRecepcao['codigoAfiliacao'];
                    $nomeRecepcao					= $vetorRecepcao['nomeMembroOa'];
            }
    }

    // ----------------------------------------------------------------------------------------------
    $nomeOrganismo = $idOrganismo != '' ? retornaNomeCompletoOrganismoAfiliado($idOrganismo) : '';

    $dados 			= $aioc->buscaAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial);

    $seqCadast = $dados->getMestreAtividadeIniciaticaOficial();
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMestre         = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMestre                 = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codAfiliacaoMestre         = "";
        $nomeMestre                 = "";
    }
    // ----------------------------------------------------------------------------------------------
    */
    ?>


    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    font-family: Helvetica, Arial, sans-serif;
    font-size: 12px!important;
    color: rgb(85, 85, 85);
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Times New Roman;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }
    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 4px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    </style>
    <style media="print">
    .botao {
    display: none;
    }
    </style>

    <style>
            #cab_cabecalho {width: 100%; height: 120px}
            #cabecalho {height: 114px}
            #oa {width: 260px; height: 19px; padding-left: 14px}
            #data {width: 131px; height: 19px}
            #ano {width: 61px; height: 19px}
            .solado {}

            #cab_centro {width: 100%; height: 50px}
            #cab_centro_esquerda {float: left; height: 50px; width: 50%;}
            #cab_centro_esquerda_corpo {width: 300px;}
            #cab_centro_direita {float: right; height: 50px; width: 50%;}
            #cab_centro_direita_corpo {float: right; width: 150px;}

            #cab_rodape {width: 100%; height: 15px}
            #cab_rodape_esquerda {float: left; width: 33%; height: 15px}
            #cab_rodape_centro {float: left; width: 33%; height: 15px}
            #cab_rodape_direita {float: right; width: 33%; height: 15px}
    </style>

    <div id="cab_cabecalho">
            <div style="width: 225px; height: 70px; margin-bottom: 15px">
                    <img class="solado" src="../img/solado_atas.png">
                    <!--
                    <center><p class="fonte tamanho-fonte" style="margin-top: 8px"><b>Grande Loja da Jurisdição <br>de LÍngua Portuguesa, AMORC</b></p></center>
                    -->
            <br>
            </div>
            <center>
            <p class="fonte tamanho-fonte_2">
                <b>
                    Equipe Iniciática <?php echo $dados->getTipoAtividadeIniciaticaOficial();?> - Ano R+C <?php echo $dados->getAnoAtividadeIniciaticaOficial();?>
                </b>
            </p>
        </center>
            <br><br>
    </div>

    <div id="cab_centro">
            <div id="cab_centro_esquerda">
                    <div id="cab_centro_esquerda_corpo">
                <p><b>Organismo Afiliado:</b> <?php echo $nomeOrganismo;?><br></p>
                <p><b>Descrição:</b> <?php echo $dados->getAnotacoesAtividadeIniciaticaOficial();?><br></p>
                    </div>
            </div>
            <div id="cab_centro_direita">
                    <div id="cab_centro_direita_corpo">
                <p><b>Data de Cadastro:</b> <?php echo $dados->getDataRegistroAtividadeIniciaticaOficial();?><br></p>
                <?php if($dados->getDataAtualizadaAtividadeIniciaticaOficial() != "00/00/0000 - 00:00"){ ?>
                <p><b>Data de Atualização:</b> <?php echo $dados->getDataAtualizadaAtividadeIniciaticaOficial();?><br></p>
                <?php } ?>
                    </div>
            </div>
    </div>
    <br>

    <div class="centro botao"><br><br><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a><br></div>
    <br>
    <b>LISTA DE OFICIAIS</b>
    <center>
            <table border="1" class="table table-action">
                    <thead>
                            <tr>
                                    <th class="t-medium">Função</th>
                                    <th class="t-nome">Nome</th>
                                    <th class="t-medium">Nº GLP</th>
                            </tr>
                    </thead>
                    <tbody>
                            <tr class="">
                                    <td>Mestre</td>
                                    <td><?php if($nomeMestre != ""){ echo $nomeMestre;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoMestre != ""){ echo $codigoAfiliacaoMestre;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Mestre Auxiliar</td>
                                    <td><?php if($nomeMestreAux != ""){ echo $nomeMestreAux;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoMestreAux != ""){ echo $codigoAfiliacaoMestreAux;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Arquivista</td>
                                    <td><?php if($nomeArquivista != ""){ echo $nomeArquivista;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoArquivista != ""){ echo $codigoAfiliacaoArquivista;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Capelão</td>
                                    <td><?php if($nomeCapelao != ""){ echo $nomeCapelao;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoCapelao != ""){ echo $codigoAfiliacaoCapelao;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Matre</td>
                                    <td><?php if($nomeMatre != ""){ echo $nomeMatre;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoMatre != ""){ echo $codigoAfiliacaoMatre;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Grande Sacerdotisa</td>
                                    <td><?php if($nomeGrandeSacerdotisa != ""){ echo $nomeGrandeSacerdotisa;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoGrandeSacerdotisa != ""){ echo $codigoAfiliacaoGrandeSacerdotisa;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Guia</td>
                                    <td><?php if($nomeGuia != ""){ echo $nomeGuia;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoGuia != ""){ echo $codigoAfiliacaoGuia;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Guardião Interno</td>
                                    <td><?php if($nomeGuardiaoInterno != ""){ echo $nomeGuardiaoInterno;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoGuardiaoInterno != ""){ echo $codigoAfiliacaoGuardiaoInterno;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Guardião Externo</td>
                                    <td><?php if($nomeGuardiaoExterno != ""){ echo $nomeGuardiaoExterno;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoGuardiaoExterno != ""){ echo $codigoAfiliacaoGuardiaoExterno;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Portador do Archote</td>
                                    <td><?php if($nomeArchote != ""){ echo $nomeArchote;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoArchote != ""){ echo $codigoAfiliacaoArchote;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Medalhista</td>
                                    <td><?php if($nomeMedalhista != ""){ echo $nomeMedalhista;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoMedalhista != ""){ echo $codigoAfiliacaoMedalhista;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Arauto</td>
                                    <td><?php if($nomeArauto != ""){ echo $nomeArauto;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoArauto != ""){ echo $codigoAfiliacaoArauto;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Adjutor</td>
                                    <td><?php if($nomeAdjutor != ""){ echo $nomeAdjutor;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoAdjutor != ""){ echo $codigoAfiliacaoAdjutor;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Sonoplasta</td>
                                    <td><?php if($nomeSonoplasta != ""){ echo $nomeSonoplasta;} else { echo 'Não definido!' ;}?></td>
                                    <td><?php if($codigoAfiliacaoSonoplasta != ""){ echo $codigoAfiliacaoSonoplasta;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Columba</td>
                                    <td><?php echo 'Definido conforme escala!' ;?></td>
                                    <td><?php echo '--' ;?></td>
                            </tr>
                            <tr class="">
                                    <td>Recepção</td>
                                    <td><?php echo 'Definido conforme escala!' ;?></td>
                                    <td><?php echo '--' ;?></td>
                            </tr>
                    </tbody>
            </table>
    </center>
<?php
}
?>