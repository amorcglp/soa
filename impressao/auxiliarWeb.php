<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once("../lib/functions.php");

    $ididAuxiliarWeb = isset($_REQUEST['idAuxiliarWeb'])?$_REQUEST['idAuxiliarWeb']:null;

    include_once("../controller/auxiliarWebController.php");
    $a = new auxiliarWebController();
    $dados = $a->buscaAuxiliarWeb($ididAuxiliarWeb);

    //Procurar informações sobre o OA em que o voluntário irá exercer seu mandato
    include_once("../model/organismoClass.php");
    $o = new organismo();
    $resultado = $o->listaOrganismo(null,null,null,null,null,$dados->getFk_idOrganismoAfiliado());
    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    $logradouroOrganismoAfiliado = $vetor['enderecoOrganismoAfiliado'];

                    $numeroOrganismoAfiliado = $vetor['numeroOrganismoAfiliado'];

                    $cidadeOrganismoAfiliado = $vetor['cidade'];

                    $estadoOrganismoAfiliado = $vetor['estado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    $tipo2 = "Rosacruz";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    $tipo2= "Martinista";
                                    break;
                    }
            }
    }

    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 16px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>TERMO DE COMPROMISSO E RESPONSABILIDADE</b></p></center>
    <br>
    <p>
    Eu <?php echo $dados->getNome();?>, CPF nº <?php echo $dados->getCpf();?>, residente no endereço <?php echo $dados->getLogradouro();?>, nº <?php echo $dados->getNumero();?> <?php echo $dados->getComplemento();?>, CEP <?php echo $dados->getCep();?>, bairro <?php echo $dados->getBairro();?>, <?php echo $dados->getCidade();?>, <?php echo $dados->getUf();?>, 
        <?php switch ($dados->getPais()){
                case 1:
                    echo "Brasil";
                    break;
                case 2:
                    echo "Portugal";
                    break;
                case 3:
                    echo "Angola";
                    break;
              }
        ?>, declaro que, a partir desta data, assumo a inteira responsabilidade sobre o conteúdo publicado nas redes sociais (fanpage, perfil, website etc.) em nome do(a) <?php echo $classificacao;?> <?php echo $tipo;?> <?php echo $nomeOrganismoAfiliado;?>.
        <br><br>
        Declaro também que li e concordo com os termos da seção <b>Informática e Internet</b> do Manual Administrativo da AMORC e me comprometo a cumprir e fazer cumprir o estabelecido nessa seção no exercício desta função.
        <br><br>
        A presente declaração será válida até a data em que, formalmente, eu seja desligado desta função por vontade própria ou por decisão dos gestores da AMORC.
        <br><br>
        Para que produza os efeitos legais, firmo o presente.
        <br><br>    
    <center><?php echo $cidadeOrganismoAfiliado;?>, <?php echo substr($dados->getDataTermoCompromisso(),0,2);?> de <?php echo mesExtensoPortugues(substr($dados->getDataTermoCompromisso(),3,2));?> de <?php echo substr($dados->getDataTermoCompromisso(),6,4);?></center><br>
    </p>
    <br><br>
    <center>
            <table cellpading="4">
                    <tr>
                            <td><center>___________________________________________</center></td>
                    </tr>
                    <tr>
                            <td><center>Assinatura do(a) Responsável</center></td>
                    </tr>
            </table>
    </center>
    <br><br>
    <b>Como Mestre(a) da(o) <?php echo $classificacao;?> <?php echo $tipo;?> <?php echo $nomeOrganismoAfiliado;?>, e corresponsável pelos conteúdos publicados nas páginas da internet que representam este Organismo Afiliado da AMORC, declaro estar ciente do presente TERMO DE COMPROMISSO E RESPONSABILIDADE e da nomeação de <?php echo $dados->getNome();?>, como responsável/moderador dos conteúdos publicados na Web.
        <br><br><br><br>
    <center><?php echo $cidadeOrganismoAfiliado;?>, <?php echo substr($dados->getDataTermoCompromisso(),0,2);?> de <?php echo mesExtensoPortugues(substr($dados->getDataTermoCompromisso(),3,2));?> de <?php echo substr($dados->getDataTermoCompromisso(),6,4);?></center><br>
    </p>
    <br><br>
    <center>
            <table cellpading="4">
                    <tr>
                            <td><center>___________________________________________</center></td>
                    </tr>
                    <tr>
                            <td><center>Assinatura do(a) Mestre do(a) <?php echo $classificacao;?></center></td>
                    </tr>
            </table>
    </center>
    </b>
<?php
}
?>