<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idAtividadeIniciaticaOficial       = isset($_REQUEST['idAtividadeIniciaticaOficial']) ? $_REQUEST['idAtividadeIniciaticaOficial'] : '';
    $idAtividadeIniciatica              = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';
    $idOrganismo                        = isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';

    $codAfiliacaoColumba                = isset($_REQUEST['codAfiliacaoColumba']) ? $_REQUEST['codAfiliacaoColumba'] : '';
    $nomeColumba                        = isset($_REQUEST['nomeColumba']) ? $_REQUEST['nomeColumba'] : '';
    $codAfiliacaoRecepcao               = isset($_REQUEST['codAfiliacaoRecepcao']) ? $_REQUEST['codAfiliacaoRecepcao'] : '';
    $nomeRecepcao                       = isset($_REQUEST['nomeRecepcao']) ? $_REQUEST['nomeRecepcao'] : '';

    include_once("../controller/atividadeIniciaticaOficialController.php");
    include_once("../controller/atividadeIniciaticaColumbaController.php");
    include_once("../controller/atividadeIniciaticaController.php");
    include_once("../controller/membroOAController.php");
    include_once("../controller/organismoController.php");
    $aioc 	= new atividadeIniciaticaOficialController();
    $aicc 	= new atividadeIniciaticaColumbaController();
    $aic 	= new atividadeIniciaticaController();
    $aim 	= new atividadeIniciatica();
    $moam 	= new membroOA();
    $oc 	= new organismoController();

    $nomeOrganismo = '';
    $dadosOa = $oc->buscaOrganismo($idOrganismo);
    switch ($dadosOa->getClassificacaoOrganismoAfiliado()) {
            case 1: $nomeOrganismo = "Loja "; break;
            case 2: $nomeOrganismo = "Pronaos "; break;
            case 3: $nomeOrganismo = "Capítulo "; break;
            case 4: $nomeOrganismo = "Heptada "; break;
            case 5: $nomeOrganismo = "Atrium "; break;
    }
    switch ($dadosOa->getTipoOrganismoAfiliado()) {
            case 1: $nomeOrganismo .= "R+C "; break;
            case 2: $nomeOrganismo .= "TOM "; break;
    }
    $nomeOrganismo .= $dadosOa->getNomeOrganismoAfiliado()." - ";
    $nomeOrganismo .= $dadosOa->getSiglaOrganismoAfiliado();

    $dados 			    = $aioc->buscaAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial);
    $dadosCol 		    = $aicc->buscaAtividadeIniciaticaColumbaPorId($idAtividadeIniciaticaOficial);
    $dadosAtiv          = $aic->buscaAtividadeIniciatica($idAtividadeIniciatica);
    $retornoSuplente    = $aim->listaAtividadeSuplente($idAtividadeIniciatica);
    if($retornoSuplente){
        foreach($retornoSuplente as $vetorSuplente){
						//echo "<pre>";print_r($vetorSuplente);echo "</pre>";

            $mestreAtividadeIniciatica_suplente             = $vetorSuplente['mestreAtividadeIniciatica_suplente'];
            $mestreAuxAtividadeIniciatica_suplente          = $vetorSuplente['mestreAuxAtividadeIniciatica_suplente'];
            $arquivistaAtividadeIniciatica_suplente         = $vetorSuplente['arquivistaAtividadeIniciatica_suplente'];
            $capelaoAtividadeIniciatica_suplente            = $vetorSuplente['capelaoAtividadeIniciatica_suplente'];
            $matreAtividadeIniciatica_suplente              = $vetorSuplente['matreAtividadeIniciatica_suplente'];
            $grandeSacerdotisaAtividadeIniciatica_suplente  = $vetorSuplente['grandeSacerdotisaAtividadeIniciatica_suplente'];
            $guiaAtividadeIniciatica_suplente               = $vetorSuplente['guiaAtividadeIniciatica_suplente'];
            $guardiaoInternoAtividadeIniciatica_suplente    = $vetorSuplente['guardiaoInternoAtividadeIniciatica_suplente'];
            $guardiaoExternoAtividadeIniciatica_suplente    = $vetorSuplente['guardiaoExternoAtividadeIniciatica_suplente'];
            $archoteAtividadeIniciatica_suplente            = $vetorSuplente['archoteAtividadeIniciatica_suplente'];
            $medalhistaAtividadeIniciatica_suplente         = $vetorSuplente['medalhistaAtividadeIniciatica_suplente'];
            $arautoAtividadeIniciatica_suplente             = $vetorSuplente['arautoAtividadeIniciatica_suplente'];
            $adjutorAtividadeIniciatica_suplente            = $vetorSuplente['adjutorAtividadeIniciatica_suplente'];
            $sonoplastaAtividadeIniciatica_suplente         = $vetorSuplente['sonoplastaAtividadeIniciatica_suplente'];
            $columbaAtividadeIniciatica_suplente            = $vetorSuplente['columbaAtividadeIniciatica_suplente'];
        }
    }
    $ocultar_json=1;

    if(!isset($mestreAtividadeIniciatica_suplente) || $mestreAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getMestreAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $mestreAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMestre          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMestre                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codAfiliacaoMestre             = "";
        $nomeMestre                     = "";
    }
    $seqCadast = 0;

		if(!isset($mestreAuxAtividadeIniciatica_suplente) || $mestreAuxAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getMestreAuxAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $mestreAuxAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMestreAux           = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMestreAux                      = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoMestreAux           = "";
        $nomeMestreAux                      = "";
    }
    $seqCadast = 0;

		if(!isset($arquivistaAtividadeIniciatica_suplente) || $arquivistaAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getArquivistaAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $arquivistaAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoArquivista         = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeArquivista                 = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoArquivista         = "";
        $nomeArquivista                 = "";
    }
    $seqCadast = 0;

		if(!isset($capelaoAtividadeIniciatica_suplente) || $capelaoAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getCapelaoAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $capelaoAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoCapelao          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeCapelao                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoCapelao             = "";
        $nomeCapelao                     = "";
    }
    $seqCadast = 0;

		if(!isset($matreAtividadeIniciatica_suplente) || $matreAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getMatreAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $matreAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMatre          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMatre                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoMatre             = "";
        $nomeMatre                     = "";
    }
    $seqCadast = 0;

		if(!isset($grandeSacerdotisaAtividadeIniciatica_suplente) || $grandeSacerdotisaAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getGrandeSacerdotisaAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $grandeSacerdotisaAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGrandeSacerdotisa          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGrandeSacerdotisa                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGrandeSacerdotisa             = "";
        $nomeGrandeSacerdotisa                     = "";
    }
    $seqCadast = 0;

		if(!isset($guiaAtividadeIniciatica_suplente) || $guiaAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getGuiaAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $guiaAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGuia          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGuia                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGuia             = "";
        $nomeGuia                     = "";
    }
    $seqCadast = 0;

		if(!isset($guardiaoInternoAtividadeIniciatica_suplente) || $guardiaoInternoAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getGuardiaoInternoAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $guardiaoInternoAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGuardiaoInterno          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGuardiaoInterno                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGuardiaoInterno             = "";
        $nomeGuardiaoInterno                     = "";
    }
    $seqCadast = 0;

		if(!isset($guardiaoExternoAtividadeIniciatica_suplente) || $guardiaoExternoAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getGuardiaoExternoAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $guardiaoExternoAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoGuardiaoExterno          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeGuardiaoExterno                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoGuardiaoExterno             = "";
        $nomeGuardiaoExterno                     = "";
    }
    $seqCadast = 0;

		if(!isset($archoteAtividadeIniciatica_suplente) || $archoteAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getArchoteAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $archoteAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoArchote          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeArchote                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoArchote             = "";
        $nomeArchote                     = "";
    }
    $seqCadast = 0;

		if(!isset($medalhistaAtividadeIniciatica_suplente) || $medalhistaAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getMedalhistaAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $medalhistaAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoMedalhista          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeMedalhista                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoMedalhista             = "";
        $nomeMedalhista                     = "";
    }
    $seqCadast = 0;

		if(!isset($arautoAtividadeIniciatica_suplente) || $arautoAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getArautoAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $arautoAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoArauto          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeArauto                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoArauto             = "";
        $nomeArauto                     = "";
    }
    $seqCadast = 0;

		if(!isset($adjutorAtividadeIniciatica_suplente) || $adjutorAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getAdjutorAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $adjutorAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoAdjutor          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeAdjutor                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoAdjutor             = "";
        $nomeAdjutor                     = "";
    }
    $seqCadast = 0;

		if(!isset($sonoplastaAtividadeIniciatica_suplente) || $sonoplastaAtividadeIniciatica_suplente == 0) {
        $seqCadast = $dados->getSonoplastaAtividadeIniciaticaOficial();
    } else {
        $seqCadast = $sonoplastaAtividadeIniciatica_suplente;
    }
    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codigoAfiliacaoSonoplasta          = $obj2['result'][0]['fields']['fCodRosacruz'];
        $nomeSonoplasta                     = $obj2['result'][0]['fields']['fNomCliente'];
    } else {
        $codigoAfiliacaoSonoplasta             = "";
        $nomeSonoplasta                     = "";
    }
    $seqCadast = 0;


    $seqCadast = $columbaAtividadeIniciatica_suplente;

    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
        $codAfiliacaoColumba          = $obj2['result'][0]['fields']['fCodOgg'];
        $nomeColumba                  = $obj2['result'][0]['fields']['fNomCliente'];
    }

    //$seqCadast = 0;


    ?>


    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    font-family: Helvetica, Arial, sans-serif;
    font-size: 12px!important;
    color: rgb(85, 85, 85);
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Times New Roman;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }
    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table {
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 4px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) {
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    </style>
    <style media="print">
    .botao {
    display: none;
    }
    </style>

    <style>
            #cab_cabecalho {width: 100%; height: 120px}
            #cabecalho {height: 114px}
            #oa {width: 260px; height: 19px; padding-left: 14px}
            #data {width: 131px; height: 19px}
            #ano {width: 61px; height: 19px}
            .solado {}

            #cab_centro {width: 100%; height: 50px}
            #cab_centro_esquerda {float: left; height: 50px; width: 50%;}
            #cab_centro_esquerda_corpo {width: 300px;}
            #cab_centro_direita {float: right; height: 50px; width: 50%;}
            #cab_centro_direita_corpo {float: right; width: 150px;}

            #cab_rodape {width: 100%; height: 15px}
            #cab_rodape_esquerda {float: left; width: 33%; height: 15px}
            #cab_rodape_centro {float: left; width: 33%; height: 15px}
            #cab_rodape_direita {float: right; width: 33%; height: 15px}
    </style>

    <div id="cab_cabecalho">
            <div style="width: 225px; height: 70px; margin-bottom: 15px">
                    <img class="solado" src="../img/solado_atas.png">
                    <!--<center><p class="fonte tamanho-fonte" style="margin-top: 8px"><b>Grande Loja da Jurisdição <br>de LÍngua Portuguesa, AMORC</b></p></center>--><br>
            </div>
            <center><p class="fonte tamanho-fonte_2"><b>INICIAÇÃO AO <?php echo $dadosAtiv->getTipoAtividadeIniciatica();?>º GRAU DE TEMPLO</b></p></center>
            <br><br>
    </div>

    <div id="cab_centro">
            <div id="cab_centro_esquerda">
                    <div id="cab_centro_esquerda_corpo">
                            <p><b>Organismo Afiliado:</b> <br><?php echo $nomeOrganismo;?><br></p>
                <p><b>Ano R+C da Equipe:</b><br> <?php echo $dados->getAnoAtividadeIniciaticaOficial();?><br></p>
                    </div>
            </div>
            <div id="cab_centro_direita">
                    <div id="cab_centro_direita_corpo">
                <p><b>Data da Atividade:</b> <?php echo $dadosAtiv->getDataRealizadaAtividadeIniciatica();?><br></p>
                <p><b>Hora da Atividade:</b><br> <?php echo $dadosAtiv->getHoraRealizadaAtividadeIniciatica();?><br></p>
                    </div>
            </div>
    </div>
    <br>
    <br>
    <div class="centro botao"><br><br><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a><br></div>
    <br>
    <center><b>LISTA DE OFICIAIS</b></center>
    <br>
    <center>
            <table border="1" class="table table-action">
                    <thead>
                            <tr>
                                    <th class="t-medium">Função</th>
                                    <th class="t-nome">Nome</th>
                                    <th class="t-medium" style="min-width: 380px">Assinatura</th>
                                    <th class="t-medium">Nº GLP</th>
                            </tr>
                    </thead>
                    <tbody>
                            <tr class="">
                                    <td>Mestre</td>
                                    <td><?php if($nomeMestre != ""){ echo ucwords(mb_strtolower($nomeMestre));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoMestre != ""){ echo $codigoAfiliacaoMestre;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Mestre Auxiliar</td>
                                    <td><?php if($nomeMestreAux != ""){ echo ucwords(mb_strtolower($nomeMestreAux));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoMestreAux != ""){ echo $codigoAfiliacaoMestreAux;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Arquivista</td>
                                    <td><?php if($nomeArquivista != ""){ echo ucwords(mb_strtolower($nomeArquivista));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoArquivista != ""){ echo $codigoAfiliacaoArquivista;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Capelão</td>
                                    <td><?php if($nomeCapelao != ""){ echo ucwords(mb_strtolower($nomeCapelao));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoCapelao != ""){ echo $codigoAfiliacaoCapelao;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Matre</td>
                                    <td><?php if($nomeMatre != ""){ echo ucwords(mb_strtolower($nomeMatre));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoMatre != ""){ echo $codigoAfiliacaoMatre;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Grande Sacerdotisa</td>
                                    <td><?php if($nomeGrandeSacerdotisa != ""){ echo ucwords(mb_strtolower($nomeGrandeSacerdotisa));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoGrandeSacerdotisa != ""){ echo $codigoAfiliacaoGrandeSacerdotisa;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Guia</td>
                                    <td><?php if($nomeGuia != ""){ echo ucwords(mb_strtolower($nomeGuia));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoGuia != ""){ echo $codigoAfiliacaoGuia;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Guardião Interno</td>
                                    <td><?php if($nomeGuardiaoInterno != ""){ echo ucwords(mb_strtolower($nomeGuardiaoInterno));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoGuardiaoInterno != ""){ echo $codigoAfiliacaoGuardiaoInterno;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Guardião Externo</td>
                                    <td><?php if($nomeGuardiaoExterno != ""){ echo ucwords(mb_strtolower($nomeGuardiaoExterno));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoGuardiaoExterno != ""){ echo $codigoAfiliacaoGuardiaoExterno;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Portador do Archote</td>
                                    <td><?php if($nomeArchote != ""){ echo ucwords(mb_strtolower($nomeArchote));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoArchote != ""){ echo $codigoAfiliacaoArchote;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Medalhista</td>
                                    <td><?php if($nomeMedalhista != ""){ echo ucwords(mb_strtolower($nomeMedalhista));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoMedalhista != ""){ echo $codigoAfiliacaoMedalhista;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Arauto</td>
                                    <td><?php if($nomeArauto != ""){ echo ucwords(mb_strtolower($nomeArauto));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoArauto != ""){ echo $codigoAfiliacaoArauto;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Adjutor</td>
                                    <td><?php if($nomeAdjutor != ""){ echo ucwords(mb_strtolower($nomeAdjutor));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoAdjutor != ""){ echo $codigoAfiliacaoAdjutor;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Sonoplasta</td>
                                    <td><?php if($nomeSonoplasta != ""){ echo ucwords(mb_strtolower($nomeSonoplasta));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                                    <td><?php if($codigoAfiliacaoSonoplasta != ""){ echo $codigoAfiliacaoSonoplasta;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Columba</td>
                    <td><?php if($nomeColumba != ""){ echo ucwords(mb_strtolower($nomeColumba));} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                    <td><?php if($codAfiliacaoColumba != ""){ echo $codAfiliacaoColumba;} else { echo '--' ;}?></td>
                            </tr>
                            <tr class="">
                                    <td>Recepção</td>
                    <td><?php if($nomeRecepcao != ""){ echo $nomeRecepcao;} else { echo 'Não definido!' ;}?></td>
                                    <td> </td>
                    <td><?php if($codAfiliacaoRecepcao != ""){ echo $codAfiliacaoRecepcao;} else { echo '--' ;}?></td>
                            </tr>
                    </tbody>
            </table>
    </center>
<?php
}
?>
