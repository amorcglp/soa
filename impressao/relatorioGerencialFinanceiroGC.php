<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    set_time_limit(0);
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    ini_set("display_errors", 1 );

    $dataInicial = isset($_REQUEST['dataInicial'])?$_REQUEST['dataInicial']:null;
    $dataFinal = isset($_REQUEST['dataFinal'])?$_REQUEST['dataFinal']:null;
    
    $mesFinalInt = (int) substr($dataFinal,3,2);

    $mesInicio = (int) substr($dataInicial,3,2);
    $anoInicio = substr($dataInicial,6,4);

    $dtInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
    $dtFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);
    
    $anoFinal = substr($dataFinal,6,4);

    $date1 = new DateTime($dtInicial);
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2);
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

    $totalIntervalo = $totalDeMeses+2;

    include_once('../lib/functions.php');
    include_once('../model/ataReuniaoMensalClass.php');
    include_once('../model/dividaClass.php');
    include_once('../model/quitacaoDividaClass.php');
    include_once('../model/ataReuniaoMensalAssinadaClass.php');
    include_once('../model/organismoClass.php');
    include_once('../model/recebimentoClass.php');
    include_once('../model/despesaClass.php');
    include_once('../model/membrosRosacruzesAtivosClass.php');

    $ataReuniaoMensal					= new ataReuniaoMensal();
    $ataReuniaoMensalAssinada				= new ataReuniaoMensalAssinada();
    $divida         					= new Divida();
    $quitacaoDivida       					= new QuitacaoDivida();
    $r 							= new Recebimento();
    $d 							= new Despesa();
    $o 							= new organismo();
    $mra                                                    = new MembrosRosacruzesAtivos();


    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        table, body {
            margin: 0;
            text-align: justify;
            font-family: Avenir LT Std;
            font-size: 12px !important;
            color: #000;
            border-collapse: collapse;
            border-color: #DCDCDC;
            max-width: 100%;
            border-spacing: 0;
        }

        .fonte {
            font-family: Minion Pro;
        }

        .tamanho-fonte {
            font-size: 14px;
        }

        .tamanho-fonte_2 {
            margin-top: 0;
            font-size: 15px !important;
        }

        .fonte-3 {
            font-family: Avenir LT Std;
        }

        .cinza {
            background-color: #F5F5F5;
        }

        .centro {
            text-align: center;
        }

        /* Table Base */

        .table {
            width: 100%;
        }

            .table th,
            .table td {
                font-weight: normal;
                font-size: 10px;
                padding: 8px 15px;
                line-height: 20px;
                text-align: left;
                vertical-align: middle;
                border-top: 1px solid #dddddd;
            }

            .table thead th {
                vertical-align: bottom;
                font-weight: bold;
            }

            .table .t-small {
                width: 5%;
            }

            .table .t-medium {
                width: 10%;
            }

            .table .t-nome {
                width: 30%;
            }

            .table .t-status {
                font-weight: bold;
            }

            .table .t-active {
                color: #46a546;
            }

            .table .t-inactive {
                color: #e00300;
            }

            .table .t-draft {
                color: #f89406;
            }

            .table .t-scheduled {
                color: #049cdb;
            }

        /* Small Sizes */
        @media (max-width: 480px) {
            .table-action thead {
                display: none;
            }

            .table-action tr {
                border-bottom: 1px solid #dddddd;
            }

            .table-action td {
                border: 0;
            }

                .table-action td:not(:first-child) {
                    display: block;
                }
        }

        #cabecalho {
            height: 90px;
        }

        #oa {
            width: 260px;
            height: 19px;
            padding-left: 14px;
        }

        #data {
            width: 131px;
            height: 19px;
        }

        #ano {
            width: 61px;
            height: 19px;
        }

        .solado {
        }
    </style>
    <style media="print">
        .oculto {
            display: none;
        }
    </style>

    <div class="centro oculto">
        <a href="#" onclick="window.print();" class="botao">
            <img src="../img/impressora.png" />
        </a>
    </div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
        <img class="solado" src="../img/header.png" />
    </div>
    <center>
        <p class="fonte tamanho-fonte_2">
            <b>RELATÓRIO GERENCIAL</b>
        </p>
    </center>
    <?php
    if($_REQUEST['filtro']==1)
    {
        $regiao = $_REQUEST['regiao'];
        $oa = null;
    }else{
        $regiao = null;
        $oa = $_REQUEST['oa'];
    }
    $resultado = $o->listaOrganismo(null,null,null,null,$regiao,$oa,null,null,null,null,null,1);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];

    /**
     * Ano e saldo anterior
     */

    /*
     * Calcular saldo dos anos anteriores
     */
    //Encontrar Saldo e Data Inicial
    include_once('../model/saldoInicialClass.php');
    $si = new saldoInicial();
    $dataSaldoInicial="2016-01-01";
    $mesSaldoInicial = "01";
    $anoSaldoInicial = "2016";
    $saldoInicial=0;
    $resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    $dataSaldoInicial = $vetor['dataSaldoInicial'];
                    $mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
                    $anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
                    $saldoInicial = (float) str_replace(",",".",str_replace(".","",$vetor['saldoInicial']));
            }
    }


    $dtInicial =$dataSaldoInicial;
    $dtFinal = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-01";

    $date1 = new DateTime($dtInicial);
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2);
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');
    $sCalc=0;
    $difCalc=0;

    if($totalDeMeses==0)
    {
        $sInicialFiltro = $saldoInicial;
    }else{
        $totalIntervalo2 = $totalDeMeses+1;
        $y=$anoSaldoInicial;
        $total = $totalIntervalo2;
        for($i=$mesSaldoInicial;$i<$total;$i++)//Janeiro a Fevereiro
        {

           $rCalc						= $r->retornaEntrada($i,$y,$idOrganismoAfiliado);

           $dCalc						= $d->retornaSaida($i,$y,$idOrganismoAfiliado);

           //$difCalc                                         = ($rCalc-$dCalc);

           if($i==$mesSaldoInicial&&$y==$anoInicio)
           {

              $sInicialFiltro					= ($saldoInicial+$rCalc)-$dCalc;
           }else{
              $sInicialFiltro					= ($sCalc+$rCalc)-$dCalc;
           }

           if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
           {
               $y++;
           }
        }
    }

    $sAnteriorCalc = $sInicialFiltro;

    ?>
    <br />
    <center class="tamanho-fonte">
        <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
            <b class="fonte tamanho-fonte_2">
                Organismo Afiliado:
            </b>
            <span class="fonte tamanho-fonte_2">
                <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
            </span>
            <b class="fonte tamanho-fonte_2">
                Mês Inicial:
            </b>
            <span class="fonte tamanho-fonte_2">
                <?php echo substr($_REQUEST['dataInicial'],3,7); ?>
            </span>
            <b class="fonte tamanho-fonte_2">
                Mês Final:
            </b>
            <span class="fonte tamanho-fonte_2">
                <?php echo substr($_REQUEST['dataFinal'],3,7); ?>
            </span>
        </div>
    </center>
    <br />
    <table border="1" width="100%">
        <tr>
            <th align="center">Mês</th>
            <th align="center">Ano</th>
            <?php if($_REQUEST['linha1']==1){?>
            <th align="center">RAM</th>
            <?php }?>
            <?php if($_REQUEST['linha2']==1){?>
            <th align="center">RAM - Lançamento no SOA</th>
            <?php }?>
            <?php if($_REQUEST['linha3']==1){?>
            <th align="center">RAM - Enviado no SOA</th>
            <?php }?>
            <?php if($_REQUEST['linha11']==1){?>
            <th align="center">RAM - Monitor Regional presente?</th>
            <?php }?>
            <?php if($_REQUEST['linha4']==1){?>
            <th align="center">Saldo Anterior</th>
            <?php }?>
            <?php if($_REQUEST['linha9']==1){?>
            <th align="center">Entradas</th>
            <?php }?>
            <?php if($_REQUEST['linha10']==1){?>
            <th align="center">Saídas</th>
            <?php }?>
            <?php if($_REQUEST['linha5']==1){?>
            <th align="center">Saldo Atual</th>
            <?php }?>
            <?php if($_REQUEST['linha6']==1){?>
            <th align="center">Divida Atual GLP</th>
            <?php }?>
            <?php if($_REQUEST['linha7']==1){?>
            <th align="center">Dívida Atual Outros</th>
            <?php }?>
            <?php if($_REQUEST['linha8']==1){?>
            <th align="center">Membros Ativos</th>
            <?php }?>
        </tr>
        <?php
    $y= (int) $anoInicio;
    $total = $totalIntervalo;
    $mesASerInformado=$mesInicio;
    $i=$mesInicio;
    $totalAnos = (int) ($total/12);
    $poteAno=0;
    $sCalc=0;
    $difCalc=0;
    //echo "-->".$total;
    while($i<$total)
    {
        $mesExtenso = mesExtensoPortugues($mesASerInformado);
        $resultadoAta = $ataReuniaoMensal->listaAtaReuniaoMensal($i,$y,$idOrganismoAfiliado);
        $resultadoAtaAssinada = $ataReuniaoMensalAssinada->listaAtaReuniaoMensalAssinada(null,$i,$y,$idOrganismoAfiliado);
        $resultadoDividaGLP = $divida->listaDivida($idOrganismoAfiliado, $i, $y, null,true);
        $resultadoDividaOutros = $divida->listaDivida($idOrganismoAfiliado, $i, $y, null,null,true);
        $resultadoQuitacaoDividaGLP = $quitacaoDivida->listaQuitacaoDivida($idOrganismoAfiliado, $i, $y, true);
        $resultadoQuitacaoDividaOutros = $quitacaoDivida->listaQuitacaoDivida($idOrganismoAfiliado, $i, $y, null,true);

        /**
        * Cálculo Entradas
        */
       $rCalc						= $r->retornaEntrada($i,$y,$idOrganismoAfiliado);

       /**
        * Cálculo Saídas
        */
       $dCalc						= $d->retornaSaida($i,$y,$idOrganismoAfiliado);

       //$difCalc                                             = ($rCalc-$dCalc);
       /**
        * Cálculo do Saldo
        */
       //$sJan						= $saldoAnterior+$rJan-$dJan;
       if($i==$mesInicio&&$y==$anoInicio)
       {

          $sCalc						= ($sInicialFiltro+$rCalc)-$dCalc;
       }else{
          $sCalc						= ($sAnteriorCalc+$rCalc)-$dCalc;
       }

       /**
        * Membros Ativos
        */
       $mCalc						= $mra->retornaMembrosRosacruzesAtivos($i,$y,$idOrganismoAfiliado);
       $mCalc						= $mCalc['numeroAtualMembrosAtivos'];

       /*
        * Calculo da dívida GLP
        */
       $totalDividaGLP=0;
       if($resultadoDividaGLP)
       {
           foreach($resultadoDividaGLP as $vetor)
           {
               $valorDivida = (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
               $totalDividaGLP += $valorDivida;

           }
       }

       /*
        * Calculo da dívida Outros
        */
       $totalDividaOutros=0;
       if($resultadoDividaOutros)
       {
           foreach($resultadoDividaOutros as $vetor)
           {
               $valorDivida = (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
               $totalDividaOutros += $valorDivida;

           }
       }

        /*
        * Calculo da quitação de dívidas GLP
        */
       $totalPagamentoGLP=0;
       if($resultadoQuitacaoDividaGLP)
       {
           foreach($resultadoQuitacaoDividaGLP as $vetor)
           {
               $valorPagamento = (float) str_replace(",",".",str_replace(".","",$vetor['valorPagamento']));
               $totalPagamentoGLP += $valorPagamento;

           }
       }

       /*
        * Calculo da quitação de dívidas Outros
        */
       $totalPagamentoOutros=0;
       if($resultadoQuitacaoDividaOutros)
       {
           foreach($resultadoQuitacaoDividaOutros as $vetor)
           {
               $valorPagamento = (float) str_replace(",",".",str_replace(".","",$vetor['valorPagamento']));
               $totalPagamentoOutros += $valorPagamento;

           }
       }

       /*
        * Calculo da diferenca entre dividas e pagamentos
        */
       $totalDividaGLPSaida = $totalDividaGLP-$totalPagamentoGLP;
       $totalDividaOutrosSaida = $totalDividaOutros-$totalPagamentoOutros;
        ?>
        <tr>
            <td align="center">
                <?php echo $mesExtenso;?>
            </td>
            <td align="center">
                <?php echo $y; ?>
            </td>
            <?php if($_REQUEST['linha1']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAta)
                        {
                            $dataRAM = $resultadoAta[0]['dataAtaReuniaoMensal'];
                            $dataRAM = substr($dataRAM,8,2)."/".substr($dataRAM,5,2);
                            echo $dataRAM;
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha2']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAta)
                        {
                            $dataRAM = $resultadoAta[0]['data'];
                            $dataRAM = substr($dataRAM,8,2)."/".substr($dataRAM,5,2);
                            echo $dataRAM;
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha3']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAtaAssinada)
                        {
                            $dataRAM = $resultadoAtaAssinada[0]['dataCadastro'];
                            $dataRAM = substr($dataRAM,8,2)."/".substr($dataRAM,5,2);
                            echo $dataRAM;
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha11']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAta)
                        {
                            if($resultadoAta[0]['monitorRegionalPresente']==1)
                            {
                                echo "Sim";
                            }else{
                                echo "Não";
                            }    
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha4']==1){?>
            <td align="center">
                <?php
                        echo number_format($sAnteriorCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha9']==1){?>
            <td align="center">
                <?php
                        echo number_format($rCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha10']==1){?>
            <td align="center">
                <?php
                        echo number_format($dCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha5']==1){?>
            <td align="center">
                <?php
                        echo number_format($sCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha6']==1){?>
            <td align="center">
                <?php echo number_format($totalDividaGLPSaida, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha7']==1){?>
            <td align="center">
                <?php echo number_format($totalDividaOutrosSaida, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha8']==1){?>
            <td align="center">
                <?php
                        echo $mCalc;
                ?>
            </td>
            <?php }?>
        </tr>
        <?php
        $sAnteriorCalc=$sCalc;
        //echo "anoCorrente:".$y;
        //echo "anoFinal:".$anoFinal;
        if($i==12&&$y<$anoFinal)//Se mês impresso é Dezembro o próximo ano é diferente
        {
            //echo "entrouuuu aquiiii";
            $total = $total-12;
            $y++;
            $i=1;
            $mesASerInformado=1;
        }else{
            $mesASerInformado++;
            $i++;
        }
        
    }
        ?>

    </table>
    <br />
    <br />
    <?php
       }
    }
}
?>