<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    set_time_limit(0);
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    ini_set("display_errors", 1 );

    $str = isset($_REQUEST['str'])?$_REQUEST['str']:null;
    
    $str2 = isset($_REQUEST['str2'])?$_REQUEST['str2']:null;
    
    $str3 = isset($_REQUEST['str3'])?$_REQUEST['str3']:null;

    $classificacaoOa = isset($_REQUEST['classificacaoOa'])?$_REQUEST['classificacaoOa']:null;
    $dataInicial = isset($_REQUEST['dataInicial'])?$_REQUEST['dataInicial']:null;
    $dataFinal = isset($_REQUEST['dataFinal'])?$_REQUEST['dataFinal']:null;

    $mesInicio = (int) substr($dataInicial,3,2);
    $anoInicio = substr($dataInicial,6,4);

    $dtInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
    $dtFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);

    $date1 = new DateTime($dtInicial); 
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2); 
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

    $totalIntervalo = $totalDeMeses+1;

    include_once('../lib/functions.php');
    include_once('../model/organismoClass.php');
    include_once '../model/atividadeEstatutoTipoClass.php';
    include_once '../model/atividadeEstatutoClass.php';
    include_once('../controller/atividadeIniciaticaController.php');


    $a                                                      = new atividadeEstatutoTipo();
    $b                                                      = new atividadeEstatuto();
    $o 							= new organismo();
    $aim                                                = new atividadeIniciatica();

    $arrAtividadesMarcadas=  explode(",", $str);
    $arrAtividadesMarcadas2=  explode(",", $str2);

    $y= (int) $anoInicio;
    $totalMeses = $totalIntervalo+$mesInicio;
    $mesASerInformado=$mesInicio;
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>RELATÓRIO GERENCIAL</b></p></center>
    <?php 
    if($_REQUEST['filtro']==1)
    {    
        $regiao = $_REQUEST['regiao'];
        $oa = null;
    }else{
        $regiao = null;
        $oa = $_REQUEST['oa'];
    }
    $resultado = $o->listaOrganismo(null,null,null,null,$regiao,$oa,null,null,null,null,$str3,1);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];    


    ?>
    <br>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            Mês Inicial:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo substr($_REQUEST['dataInicial'],3,7); ?>
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Mês Final:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo substr($_REQUEST['dataFinal'],3,7); ?>
                    </span>
            </div>
    </center>

        <style>
            <!--
            /* Font Definitions */
            @font-face {
                font-family: "Cambria Math";
                panose-1: 2 4 5 3 5 4 6 3 2 4;
                mso-font-charset: 0;
                mso-generic-font-family: roman;
                mso-font-pitch: variable;
                mso-font-signature: -536870145 1107305727 0 0 415 0;
            }

            @font-face {
                font-family: Calibri;
                panose-1: 2 15 5 2 2 2 4 3 2 4;
                mso-font-charset: 0;
                mso-generic-font-family: swiss;
                mso-font-pitch: variable;
                mso-font-signature: -536859905 -1073732485 9 0 511 0;
            }
            /* Style Definitions */
            p.MsoNormal, li.MsoNormal, div.MsoNormal {
                mso-style-unhide: no;
                mso-style-qformat: yes;
                mso-style-parent: "";
                margin-top: 0cm;
                margin-right: 0cm;
                margin-bottom: 8.0pt;
                margin-left: 0cm;
                line-height: 105%;
                mso-pagination: widow-orphan;
                font-size: 11.0pt;
                font-family: "Calibri",sans-serif;
                mso-ascii-font-family: Calibri;
                mso-ascii-theme-font: minor-latin;
                mso-fareast-font-family: Calibri;
                mso-fareast-theme-font: minor-latin;
                mso-hansi-font-family: Calibri;
                mso-hansi-theme-font: minor-latin;
                mso-bidi-font-family: "Times New Roman";
                mso-bidi-theme-font: minor-bidi;
                mso-fareast-language: PT-BR;
            }

            p.msonormal0, li.msonormal0, div.msonormal0 {
                mso-style-name: msonormal;
                mso-style-unhide: no;
                mso-margin-top-alt: auto;
                margin-right: 0cm;
                mso-margin-bottom-alt: auto;
                margin-left: 0cm;
                mso-pagination: widow-orphan;
                font-size: 12.0pt;
                font-family: "Times New Roman",serif;
                mso-fareast-font-family: "Times New Roman";
                mso-fareast-theme-font: minor-fareast;
            }

            .MsoChpDefault {
                mso-style-type: export-only;
                mso-default-props: yes;
                font-size: 10.0pt;
                mso-ansi-font-size: 10.0pt;
                mso-bidi-font-size: 10.0pt;
                font-family: "Calibri",sans-serif;
                mso-ascii-font-family: Calibri;
                mso-ascii-theme-font: minor-latin;
                mso-fareast-font-family: Calibri;
                mso-fareast-theme-font: minor-latin;
                mso-hansi-font-family: Calibri;
                mso-hansi-theme-font: minor-latin;
                mso-bidi-font-family: "Times New Roman";
                mso-bidi-theme-font: minor-bidi;
                mso-fareast-language: PT-BR;
            }

            @page WordSection1 {
                size: 595.3pt 841.9pt;
                margin: 70.85pt 3.0cm 70.85pt 3.0cm;
                mso-header-margin: 35.4pt;
                mso-footer-margin: 35.4pt;
                mso-paper-source: 0;
            }

            div.WordSection1 {
                page: WordSection1;
            }
            -->
        </style>

    <?php 
        //Contar Resultados

        $e=0;   
        $y= (int) $anoInicio;
        for($i=$mesInicio;$i<$totalMeses;$i++)
        {
            $resultado = $b->listaAtividadeEstatuto($idOrganismoAfiliado, $i, $y,2,$str);
            if($resultado)
            {
                foreach($resultado as $vetor)
                {
                    $e++;
                }
            }
        }    
        $totalResultados = $e;

                if($totalResultados>0)
                {    
    ?>
                <?php
        $c=1;   
        $y= (int) $anoInicio;
        for($i=$mesInicio;$i<$totalMeses;$i++)
        { 
            $resultado = $b->listaAtividadeEstatuto($idOrganismoAfiliado, $i, $y, 2,$str);
            if($resultado)
            {
                foreach($resultado as $vetor)
                {    
                    /*
                    $mod = $c%2;
                    if($mod==0)
                    {
                        $cor = '#FFF';
                    }else{
                        $cor = '#CCCCCC';
                    }
                    */
            ?>
                <br>
                <center>

                <table class=MsoTable15Grid7Colorful border=1 cellspacing=0 cellpadding=0
                       width=614 style='width:460.45pt;border-collapse:collapse;border:none;
     mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
     153;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
                    <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-bottom:solid #666666 1.0pt;
      mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:153;
      mso-border-bottom-alt:solid #666666 .5pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;background:white;mso-background-themecolor:
      background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:517'>
                                <b>
                                    <i>
                                        <span style='color:black;
      mso-themecolor:text1'>ID<o:p></o:p></span>
                                    </i>
                                </b>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border:none;
      border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;mso-border-bottom-alt:solid #666666 .5pt;
      mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:153;
      background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:1'><b><span style='color:black;mso-themecolor:text1'><?php echo $vetor['idAtividadeEstatuto'];?><o:p></o:p></span></b></p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:0'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Atividade<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;background:#CCCCCC;mso-background-themecolor:
      text1;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    <?php echo $vetor['nomeTipoAtividadeEstatuto']." ".$vetor['complementoNomeAtividadeEstatuto'];?><o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:1'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Orador<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'><?php echo $vetor['nomeOradorDiscursoAtividadeEstatuto'];?><o:p></o:p></span></p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:2'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Título do Discurso<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;background:#CCCCCC;mso-background-themecolor:
      text1;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'> 
                                    <?php echo $vetor['tituloDiscursoAtividadeEstatuto'];?><o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:3'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Cadastrado por<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=240 valign=top style='width:179.95pt;border-top:none;border-left:
      none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;mso-border-right-themecolor:
      text1;mso-border-right-themetint:153;mso-border-top-alt:solid #666666 .5pt;
      mso-border-top-themecolor:text1;mso-border-top-themetint:153;mso-border-left-alt:
      solid #666666 .5pt;mso-border-left-themecolor:text1;mso-border-left-themetint:
      153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
      153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'><?php echo $vetor['nomeUsuario'];?><o:p></o:p></span></p>
                        </td>
                        <td width=245 valign=top style='width:184.0pt;border:solid #666666 1.0pt;
      mso-border-themecolor:text1;mso-border-themetint:153;border-left:none;
      mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Realizada em:
                                    <?php $data = substr($vetor['dataAtividadeEstatuto'],8,2)."/".substr($vetor['dataAtividadeEstatuto'],5,2)."/".substr($vetor['dataAtividadeEstatuto'],0,4);?>
                                <?php echo $data;?> às <?php echo substr($vetor['horaAtividadeEstatuto'],0,5);?><o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:4'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Participantes<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=240 valign=top style='width:179.95pt;border-top:none;border-left:
      none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;mso-border-right-themecolor:
      text1;mso-border-right-themetint:153;mso-border-top-alt:solid #666666 .5pt;
      mso-border-top-themecolor:text1;mso-border-top-themetint:153;mso-border-left-alt:
      solid #666666 .5pt;mso-border-left-themecolor:text1;mso-border-left-themetint:
      153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
      153;background:#CCCCCC;mso-background-themecolor:text1;mso-background-themetint:
      51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Visitantes:
                                    <?php echo $vetor['nroParticipanteAtividadeEstatuto'];?><o:p></o:p>
                                </span>
                            </p>
                        </td>
                        <td width=245 valign=top style='width:184.0pt;border-top:none;border-left:
      none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;mso-border-right-themecolor:
      text1;mso-border-right-themetint:153;mso-border-top-alt:solid #666666 .5pt;
      mso-border-top-themecolor:text1;mso-border-top-themetint:153;mso-border-left-alt:
      solid #666666 .5pt;mso-border-left-themecolor:text1;mso-border-left-themetint:
      153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
      153;background:#CCCCCC;mso-background-themecolor:text1;mso-background-themetint:
      51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Frequentadores:
                                    <?php echo $vetor['nroFrequentadorAtividadeEstatuto'];?><o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:5'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Local<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'><o:p><?php echo $vetor['localAtividadeEstatuto'];?></o:p></span></p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:6'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Oficiais<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;background:#CCCCCC;mso-background-themecolor:
      text1;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    <?php 

                                    $resultado2 = $b->listaAtividadeEstatutoOficiais($vetor['idAtividadeEstatuto']);
                                    $q=0;
                                    if($resultado2)
                                    {
                                        foreach($resultado2 as $vetor2)
                                        {
                                            if($q==0)
                                            {
                                                echo $vetor2['nomeOficial'];
                                            }else{
                                                echo ", ".$vetor2['nomeOficial'];
                                            }    
                                            $q++;
                                        }    
                                    }    
                                    ?>
                                    <o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:7;mso-yfti-lastrow:yes'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Descrição<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'><?php echo $vetor['descricaoAtividadeEstatuto'];;?><o:p></o:p></span></p>
                        </td>
                    </tr>
                </table>

            </center>
            <br>    

                <?php

                            //$c++;
                            }
                        }

                    if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
                    {
                        $y++;
                    }
                }
            }
?> 
     
            </table>

            <p class=MsoNormal>
                <o:p>&nbsp;</o:p>
            </p>

        </div>

    <?php 
        }

    }
}
?>