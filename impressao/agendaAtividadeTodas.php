<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    $anoAtual = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual'] : date('Y');
    //$idOrganismoAfiliado = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;

    include_once('../lib/functions.php');
    include_once('../model/organismoClass.php');
    include_once('../model/agendaAtividadeClass.php');

    $a = new AgendaAtividade();
    $o = new organismo();

    $resultado = $o->listaOrganismo(null,null,null,null,null,null,null,null,null,null,null,1);
    
    $resultadoAno = $a->listaAnosAgendaAtividade();
    
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel=File-List href="agenda_arquivos/filelist.xml">
    <link rel=themeData href="agenda_arquivos/themedata.thmx">
    <link rel=colorSchemeMapping href="agenda_arquivos/colorschememapping.xml">
    <script>
        function mudaAno(ano)
        {
            //alert(ano);
            location.href='agendaAtividadeTodas.php?anoAtual='+ano;
        }
    </script>
    <style>
        body {
            -webkit-print-color-adjust: exact;
        }
        <!--
        /* Font Definitions */
        @font-face {
            font-family: "Cambria Math";
            panose-1: 2 4 5 3 5 4 6 3 2 4;
            mso-font-charset: 1;
            mso-generic-font-family: roman;
            mso-font-pitch: variable;
            mso-font-signature: 0 0 0 0 0 0;
        }

        @font-face {
            font-family: "Calibri Light";
            panose-1: 2 15 3 2 2 2 4 3 2 4;
            mso-font-charset: 0;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536859905 -1073732485 9 0 511 0;
        }

        @font-face {
            font-family: Calibri;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
            mso-font-charset: 0;
            mso-generic-font-family: swiss;
            mso-font-pitch: variable;
            mso-font-signature: -536859905 -1073732485 9 0 511 0;
        }
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal {
            mso-style-unhide: no;
            mso-style-qformat: yes;
            mso-style-parent: "";
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 8.0pt;
            margin-left: 0cm;
            line-height: 107%;
            mso-pagination: widow-orphan;
            font-size: 11.0pt;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        .MsoChpDefault {
            mso-style-type: export-only;
            mso-default-props: yes;
            font-family: "Calibri",sans-serif;
            mso-ascii-font-family: Calibri;
            mso-ascii-theme-font: minor-latin;
            mso-fareast-font-family: Calibri;
            mso-fareast-theme-font: minor-latin;
            mso-hansi-font-family: Calibri;
            mso-hansi-theme-font: minor-latin;
            mso-bidi-font-family: "Times New Roman";
            mso-bidi-theme-font: minor-bidi;
            mso-fareast-language: EN-US;
        }

        .MsoPapDefault {
            mso-style-type: export-only;
            margin-bottom: 8.0pt;
            line-height: 107%;
        }

        @page WordSection1 {
            size: 595.3pt 841.9pt;
            margin: 70.85pt 3.0cm 70.85pt 3.0cm;
            mso-header-margin: 35.4pt;
            mso-footer-margin: 35.4pt;
            mso-paper-source: 0;
        }

        div.WordSection1 {
            page: WordSection1;
        }
        -->
    </style>
    <style media="print">
            .oculto {
                display: none;
                font-size: 12px!important;
            }
    </style>
    <center>
    <div class="centro oculto">
        <a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a>
    </div>
    </center>    
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
        <img class="solado" src="../img/header.png">
    </div>
    <center>
    <div class="centro oculto">
        <?php if($resultadoAno)
        {?>
            <br>
            <span id="labelAno"> Selecione o Ano da Agenda de Atividades: </span>
            <select id="anoCompetencia" name="anoCompetencia" onchange="mudaAno(document.getElementById('anoCompetencia').value)">
                <option>Selecione</option>
                <?php
                foreach($resultadoAno as $vetor)
                {?>
                    <option value="<?php echo $vetor['anoCompetencia'];?>"
                        <?php if(isset($anoAtual)&&$anoAtual==$vetor['anoCompetencia']){ echo "selected";}?>
                    ><?php echo $vetor['anoCompetencia'];?></option>
                    <?php
                }?>
            </select>
            <br>
        <?php }?>
        <br>
    </div>
    </center>    
    <center><p class="fonte tamanho-fonte_2"><b>RELATÓRIO DA AGENDA DE ATIVIDADES</b></p></center>
    <?php

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];
                    $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
                    $nomeOrganismoAfiliado 	= $vetor['nomeOrganismoAfiliado'];
                    $classificacaoOA		= $vetor['classificacaoOrganismoAfiliado'];
                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }
           

    if(isset($classificacao)&&isset($tipo)&&isset($nomeOrganismoAfiliado)&&isset($siglaOrganismoAfiliado))
    {
    ?>
    
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado." ".$siglaOrganismoAfiliado; ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $anoAtual; ?>
                    </span>
            </div>
    </center>
    <br><br>
    <center>
        <h3>Lista de Atividades</h3>
    </center>
    <?php
    }
    $resultado = $a->listaAgendaAtividade($idOrganismoAfiliado,$anoAtual);
    if(!$resultado)
    {
        echo "<center><h4>Nenhum registro</h4></center>";
    }else{
    ?>  
    
<div class=WordSection1>

        <table class=MsoTable15Plain5 border=0 cellspacing=0 cellpadding=0 width="100%"
               style='border-collapse:collapse;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
            <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
                <td width=113 valign=top style='width:84.9pt;border:none;border-bottom:solid #7F7F7F 1.0pt;
  mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:128;
  mso-border-bottom-alt:solid #7F7F7F .5pt;mso-border-bottom-themecolor:text1;
  mso-border-bottom-themetint:128;background:white;mso-background-themecolor:
  background1;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=left style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:left;line-height:normal;mso-yfti-cnfc:517'>
                        <i>
                            <span style='font-size:13.0pt;mso-bidi-font-size:11.0pt;font-family:"Calibri Light",sans-serif;
  mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
  mso-fareast-theme-font:major-fareast;mso-hansi-theme-font:major-latin;
  mso-bidi-font-family:"Times New Roman";mso-bidi-theme-font:major-bidi'>Atividade<o:p></o:p></span>
                        </i>
                    </p>
                </td>
                <td width=113 valign=top style='width:84.95pt;border:none;border-bottom:solid #7F7F7F 1.0pt;
  mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:128;
  mso-border-bottom-alt:solid #7F7F7F .5pt;mso-border-bottom-themecolor:text1;
  mso-border-bottom-themetint:128;background:white;mso-background-themecolor:
  background1;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'>
                        <i>
                            <span style='font-size:13.0pt;mso-bidi-font-size:11.0pt;font-family:"Calibri Light",sans-serif;
  mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
  mso-fareast-theme-font:major-fareast;mso-hansi-theme-font:major-latin;
  mso-bidi-font-family:"Times New Roman";mso-bidi-theme-font:major-bidi'>Local<o:p></o:p></span>
                        </i>
                    </p>
                </td>
                <td width=113 valign=top style='width:84.95pt;border:none;border-bottom:solid #7F7F7F 1.0pt;
  mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:128;
  mso-border-bottom-alt:solid #7F7F7F .5pt;mso-border-bottom-themecolor:text1;
  mso-border-bottom-themetint:128;background:white;mso-background-themecolor:
  background1;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'>
                        <i>
                            <span style='font-size:13.0pt;mso-bidi-font-size:11.0pt;font-family:"Calibri Light",sans-serif;
  mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
  mso-fareast-theme-font:major-fareast;mso-hansi-theme-font:major-latin;
  mso-bidi-font-family:"Times New Roman";mso-bidi-theme-font:major-bidi'>Data/Hora<o:p></o:p></span>
                        </i>
                    </p>
                </td>
                <td width=15 valign=top style='width:15.95pt;border:none;border-bottom:solid #7F7F7F 1.0pt;
  mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:128;
  mso-border-bottom-alt:solid #7F7F7F .5pt;mso-border-bottom-themecolor:text1;
  mso-border-bottom-themetint:128;background:white;mso-background-themecolor:
  background1;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'>
                        <i>
                            <span style='font-size:13.0pt;mso-bidi-font-size:11.0pt;font-family:"Calibri Light",sans-serif;
  mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
  mso-fareast-theme-font:major-fareast;mso-hansi-theme-font:major-latin;
  mso-bidi-font-family:"Times New Roman";mso-bidi-theme-font:major-bidi'>Ano<o:p></o:p></span>
                        </i>
                    </p>
                </td>
                <td width=113 valign=top style='width:84.95pt;border:none;border-bottom:solid #7F7F7F 1.0pt;
  mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:128;
  mso-border-bottom-alt:solid #7F7F7F .5pt;mso-border-bottom-themecolor:text1;
  mso-border-bottom-themetint:128;background:white;mso-background-themecolor:
  background1;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;mso-yfti-cnfc:1'>
                        <i>
                            <span style='font-size:13.0pt;mso-bidi-font-size:11.0pt;font-family:"Calibri Light",sans-serif;
  mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
  mso-fareast-theme-font:major-fareast;mso-hansi-theme-font:major-latin;
  mso-bidi-font-family:"Times New Roman";mso-bidi-theme-font:major-bidi'>Tags<o:p></o:p></span>
                        </i>
                    </p>
                </td>
            </tr>
        <?php if($resultado)
        {
            $i=0;
            foreach($resultado as $vetor)
            {
                if($i%2){
                    $color="#ccc";
                }else{
                    $color="";
                }
            ?>
            <tr style='mso-yfti-irow:0;' >
                <td width=113 valign=top style='width:84.9pt;border:none;border-right:solid #7F7F7F 1.0pt;
  mso-border-right-themecolor:text1;mso-border-right-themetint:128;mso-border-right-alt:
  solid #7F7F7F .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
  128;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal;mso-yfti-cnfc:68;background-color:<?php echo $color;?>;'>
                        <i>
                            <span style='font-size:13.0pt;mso-bidi-font-size:11.0pt;font-family:"Calibri Light",sans-serif;
  mso-ascii-theme-font:major-latin;mso-fareast-font-family:"Times New Roman";
  mso-fareast-theme-font:major-fareast;mso-hansi-theme-font:major-latin;
  mso-bidi-font-family:"Times New Roman";mso-bidi-theme-font:major-bidi'><o:p><?php echo $vetor['nomeTipoAtividadeEstatuto'];?></o:p></span>
                        </i>
                    </p>
                </td>
                <td width=113 valign=top style='width:84.95pt;background:#F2F2F2;mso-background-themecolor:
  background1;mso-background-themeshade:242;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64;background-color:<?php echo $color;?>;'><o:p><?php echo $vetor['local'];?></o:p></p>
                </td>
                <td width=113 valign=top style='width:84.95pt;background:#F2F2F2;mso-background-themecolor:
  background1;mso-background-themeshade:242;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64;background-color:<?php echo $color;?>;'><o:p><?php if($vetor['recorrente']==0){?>
                                            Data Inicial: <?php echo substr($vetor['dataInicial'],8,2)."/".substr($vetor['dataInicial'],5,2)."/".substr($vetor['dataInicial'],0,4); ?> às <?php echo $vetor['horaInicial']; ?><br>
                                            Data Final: <?php echo substr($vetor['dataFinal'],8,2)."/".substr($vetor['dataFinal'],5,2)."/".substr($vetor['dataFinal'],0,4); ?>  às <?php echo $vetor['horaFinal']; ?><br>
                                            <?php }?>
                                            <?php if($vetor['recorrente']==1){?>
                                            <?php echo $vetor['diasHorarios']; ?>
                                            <?php }?></o:p></p>
                </td>
                <td width=15 valign=top style='width:15.95pt;background:#F2F2F2;mso-background-themecolor:
  background1;mso-background-themeshade:242;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;mso-yfti-cnfc:64;background-color:<?php echo $color;?>;'><o:p><?php echo $vetor['anoCompetencia'];?></o:p></p>
                </td>
                <td width=113 valign=top style='width:84.95pt;background:#F2F2F2;mso-background-themecolor:
  background1;mso-background-themeshade:242;padding:0cm 5.4pt 0cm 5.4pt'>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                       normal;mso-yfti-cnfc:64;background-color:<?php echo $color;?>;'><o:p>
                           <?php switch ($vetor['tag']){ 
                                    case 0:
                                        echo "Eventos";
                                        break;
                                    case 1:
                                        echo "Erin";
                                        break;
                                    case 2:
                                        echo "Iniciações";
                                        break;
                                    case 3:
                                        echo "Atividades do Organismo (Somente para membros)";
                                        break;
                            }?></o:p></p>
                </td>
            </tr>
            <?php $i++;}
        }?>
            
        </table>

        <p class=MsoNormal><o:p>&nbsp;</o:p></p>

    </div>

    <?php }
}  
}
}
    ?>
    