<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<?php
$mesAtual = isset($_REQUEST['mesAtual'])?$_REQUEST['mesAtual']:null;
$anoAtual = isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:null;
$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:null;

include_once("../model/recebimentoClass.php");
include_once("../lib/functions.php");

$arquivo = 'SOA-RELATORIO-FINANCEIRO-RECEBIMENTOS-'.$mesAtual.'-'.$anoAtual.'.xls';

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

$r = new Recebimento();
?>
<table class="table table-striped table-bordered table-hover dataTables-example" id="editable2" >
    <thead>
        <tr>
            <td colspan="6"><b><?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);?></b></td>
        </tr>
        <tr>
            <td><b>Mês</b></td>
            <td><?php echo $mesAtual;?></td>
            <td><b>Ano</b></td>
            <td><?php echo $anoAtual;?></td>
            <td></td>
            <td></td>
        </tr>
                        <tr id="linha0">
                                <th width="80">Data</th>
                                <th width="100">Descrição</th>
                                <th width="100">Recebemos de</th>
                                <th width="100">Cód. de Afiliação</th>
                                <th width="100">Valor R$</th>
                                <th width="100">Categoria</th>
                                
                        </tr>
    </thead>
    <tbody id="tabela">
        <?php
                        $totalGeral=0;
                        $resultado = $r->listaRecebimento($mesAtual,$anoAtual,$idOrganismoAfiliado);
                        $i=0;
                        if ($resultado) {
                                foreach ($resultado as $vetor) {
                                        $i++;
?>
                                        <tr id="linha<?php echo $vetor['idRecebimento'];?>">
                                                <td id="1td"><?php echo substr($vetor['dataRecebimento'],8,2)."/".substr($vetor['dataRecebimento'],5,2)."/".substr($vetor['dataRecebimento'],0,4);?></td>
                                                <td id="2td"><?php echo $vetor['descricaoRecebimento'];?></div></td>
                                                <td id="3td"><?php echo $vetor['recebemosDe'];?></td>
                                                <td id="4td"><?php if($vetor['codigoAfiliacao']==""){echo "--";}else{echo $vetor['codigoAfiliacao'];}?></td>
                                                <td id="5td"><?php echo $vetor['valorRecebimento'];?></td>
                                                <td id="6td"><?php echo retornaCategoriaRecebimento($vetor['categoriaRecebimento']);?></td>
                                                
                                        </tr>
                        <?php
                                }
                        }
?>
                        <?php
if($resultado){
    if(count($resultado)>1){
        $totalGeral = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado);
?>
                                    </tbody>
                                            <tr id="linhaTotal">
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td align="right"><b>TOTAL GERAL</b></td>
                                                    <td><div id="totalGeral"><?php echo number_format($totalGeral, 2, ',', '');?></div></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                            </tr>
                                            <?php
    }
}
 ?>
                </table>