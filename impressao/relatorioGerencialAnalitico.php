<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
?>
    <html>

    <head>
        <meta http-equiv=Content-Type content="text/html; charset=UTF-8">

        <style>
            <!--
            /* Font Definitions */
            @font-face {
                font-family: "Cambria Math";
                panose-1: 2 4 5 3 5 4 6 3 2 4;
                mso-font-charset: 0;
                mso-generic-font-family: roman;
                mso-font-pitch: variable;
                mso-font-signature: -536870145 1107305727 0 0 415 0;
            }

            @font-face {
                font-family: Calibri;
                panose-1: 2 15 5 2 2 2 4 3 2 4;
                mso-font-charset: 0;
                mso-generic-font-family: swiss;
                mso-font-pitch: variable;
                mso-font-signature: -536859905 -1073732485 9 0 511 0;
            }
            /* Style Definitions */
            p.MsoNormal, li.MsoNormal, div.MsoNormal {
                mso-style-unhide: no;
                mso-style-qformat: yes;
                mso-style-parent: "";
                margin-top: 0cm;
                margin-right: 0cm;
                margin-bottom: 8.0pt;
                margin-left: 0cm;
                line-height: 105%;
                mso-pagination: widow-orphan;
                font-size: 11.0pt;
                font-family: "Calibri",sans-serif;
                mso-ascii-font-family: Calibri;
                mso-ascii-theme-font: minor-latin;
                mso-fareast-font-family: Calibri;
                mso-fareast-theme-font: minor-latin;
                mso-hansi-font-family: Calibri;
                mso-hansi-theme-font: minor-latin;
                mso-bidi-font-family: "Times New Roman";
                mso-bidi-theme-font: minor-bidi;
                mso-fareast-language: PT-BR;
            }

            p.msonormal0, li.msonormal0, div.msonormal0 {
                mso-style-name: msonormal;
                mso-style-unhide: no;
                mso-margin-top-alt: auto;
                margin-right: 0cm;
                mso-margin-bottom-alt: auto;
                margin-left: 0cm;
                mso-pagination: widow-orphan;
                font-size: 12.0pt;
                font-family: "Times New Roman",serif;
                mso-fareast-font-family: "Times New Roman";
                mso-fareast-theme-font: minor-fareast;
            }

            .MsoChpDefault {
                mso-style-type: export-only;
                mso-default-props: yes;
                font-size: 10.0pt;
                mso-ansi-font-size: 10.0pt;
                mso-bidi-font-size: 10.0pt;
                font-family: "Calibri",sans-serif;
                mso-ascii-font-family: Calibri;
                mso-ascii-theme-font: minor-latin;
                mso-fareast-font-family: Calibri;
                mso-fareast-theme-font: minor-latin;
                mso-hansi-font-family: Calibri;
                mso-hansi-theme-font: minor-latin;
                mso-bidi-font-family: "Times New Roman";
                mso-bidi-theme-font: minor-bidi;
                mso-fareast-language: PT-BR;
            }

            @page WordSection1 {
                size: 595.3pt 841.9pt;
                margin: 70.85pt 3.0cm 70.85pt 3.0cm;
                mso-header-margin: 35.4pt;
                mso-footer-margin: 35.4pt;
                mso-paper-source: 0;
            }

            div.WordSection1 {
                page: WordSection1;
            }
            -->
        </style>
    </head>

    <body lang=PT-BR style='tab-interval:35.4pt'>

        <div class=WordSection1>

            <div align=center>

                <table class=MsoTable15Grid7Colorful border=1 cellspacing=0 cellpadding=0
                       width=614 style='width:460.45pt;border-collapse:collapse;border:none;
     mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
     153;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
                    <tr style='mso-yfti-irow:-1;mso-yfti-firstrow:yes;mso-yfti-lastfirstrow:yes'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-bottom:solid #666666 1.0pt;
      mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:153;
      mso-border-bottom-alt:solid #666666 .5pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;background:white;mso-background-themecolor:
      background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:517'>
                                <b>
                                    <i>
                                        <span style='color:black;
      mso-themecolor:text1'>ID<o:p></o:p></span>
                                    </i>
                                </b>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border:none;
      border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;mso-border-bottom-alt:solid #666666 .5pt;
      mso-border-bottom-themecolor:text1;mso-border-bottom-themetint:153;
      background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:1'><b><span style='color:black;mso-themecolor:text1'>2250<o:p></o:p></span></b></p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:0'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Atividade<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;background:#CCCCCC;mso-background-themecolor:
      text1;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Atividade
                                    Cultural â€“ Social<o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:1'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Orador<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'>JoÃ£o da Silva<o:p></o:p></span></p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:2'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>TÃ­tulo do Discurso<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;background:#CCCCCC;mso-background-themecolor:
      text1;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Palavra
                                    Perdida<o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:3'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Cadastrado por<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=240 valign=top style='width:179.95pt;border-top:none;border-left:
      none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;mso-border-right-themecolor:
      text1;mso-border-right-themetint:153;mso-border-top-alt:solid #666666 .5pt;
      mso-border-top-themecolor:text1;mso-border-top-themetint:153;mso-border-left-alt:
      solid #666666 .5pt;mso-border-left-themecolor:text1;mso-border-left-themetint:
      153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
      153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'>JoÃ£o da Silva<o:p></o:p></span></p>
                        </td>
                        <td width=245 valign=top style='width:184.0pt;border:solid #666666 1.0pt;
      mso-border-themecolor:text1;mso-border-themetint:153;border-left:none;
      mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Realizada em:
                                    10/08/2016<o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:4'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Participantes<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=240 valign=top style='width:179.95pt;border-top:none;border-left:
      none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;mso-border-right-themecolor:
      text1;mso-border-right-themetint:153;mso-border-top-alt:solid #666666 .5pt;
      mso-border-top-themecolor:text1;mso-border-top-themetint:153;mso-border-left-alt:
      solid #666666 .5pt;mso-border-left-themecolor:text1;mso-border-left-themetint:
      153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
      153;background:#CCCCCC;mso-background-themecolor:text1;mso-background-themetint:
      51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Visitantes:
                                    05<o:p></o:p>
                                </span>
                            </p>
                        </td>
                        <td width=245 valign=top style='width:184.0pt;border-top:none;border-left:
      none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:text1;
      mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;mso-border-right-themecolor:
      text1;mso-border-right-themetint:153;mso-border-top-alt:solid #666666 .5pt;
      mso-border-top-themecolor:text1;mso-border-top-themetint:153;mso-border-left-alt:
      solid #666666 .5pt;mso-border-left-themecolor:text1;mso-border-left-themetint:
      153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:text1;mso-border-themetint:
      153;background:#CCCCCC;mso-background-themecolor:text1;mso-background-themetint:
      51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    Frequentadores:
                                    15<o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:5'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Local<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'><o:p>&nbsp;</o:p></span></p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:6'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:68'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>Oficiais<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;background:#CCCCCC;mso-background-themecolor:
      text1;mso-background-themetint:51;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal;mso-yfti-cnfc:64'>
                                <span style='color:black;mso-themecolor:text1'>
                                    AntÃ´nio
                                    da Silva, JoÃ£o Marcos, Maria Silva<o:p></o:p>
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr style='mso-yfti-irow:7;mso-yfti-lastrow:yes'>
                        <td width=129 valign=top style='width:96.5pt;border:none;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-right-alt:
      solid #666666 .5pt;mso-border-right-themecolor:text1;mso-border-right-themetint:
      153;background:white;mso-background-themecolor:background1;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
      justify;line-height:normal;mso-yfti-cnfc:4'>
                                <i>
                                    <span style='color:black;
      mso-themecolor:text1'>DescriÃ§Ã£o<o:p></o:p></span>
                                </i>
                            </p>
                        </td>
                        <td width=485 colspan=2 valign=top style='width:363.95pt;border-top:none;
      border-left:none;border-bottom:solid #666666 1.0pt;mso-border-bottom-themecolor:
      text1;mso-border-bottom-themetint:153;border-right:solid #666666 1.0pt;
      mso-border-right-themecolor:text1;mso-border-right-themetint:153;mso-border-top-alt:
      solid #666666 .5pt;mso-border-top-themecolor:text1;mso-border-top-themetint:
      153;mso-border-left-alt:solid #666666 .5pt;mso-border-left-themecolor:text1;
      mso-border-left-themetint:153;mso-border-alt:solid #666666 .5pt;mso-border-themecolor:
      text1;mso-border-themetint:153;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
      normal'><span style='color:black;mso-themecolor:text1'><o:p>&nbsp;</o:p></span></p>
                        </td>
                    </tr>
                </table>

            </div>

            <p class=MsoNormal><o:p>&nbsp;</o:p></p>

        </div>

    </body>

    </html>
<?php
}
?>