<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    set_time_limit(0);
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    ini_set("display_errors", 1 );

    $dataInicial = isset($_REQUEST['dataInicial'])?$_REQUEST['dataInicial']:null;
    $dataFinal = isset($_REQUEST['dataFinal'])?$_REQUEST['dataFinal']:null;
    
    $mesFinalInt = (int) substr($dataFinal,3,2);

    $mesInicio = (int) substr($dataInicial,3,2);
    $anoInicio = substr($dataInicial,6,4);

    $dtInicial = substr($dataInicial,6,4)."-01-01";
    $dtFinal = (substr($dataFinal,6,4)+1)."-01-01";
    
    
    $date1 = new DateTime($dtInicial);
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2);
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

    $totalIntervalo = $totalDeMeses+1;
    
    $arquivo = 'SOA-RELATORIO-FINANCEIRO-'.$dtInicial.'-'.$dtFinal.'.xls';

    header ("Content-type: application/x-msexcel");
    header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
    header ("Content-Description: PHP Generated Data" );
    
    include_once('../lib/functions.php');
    include_once('../model/ataReuniaoMensalClass.php');
    include_once('../model/dividaClass.php');
    include_once('../model/quitacaoDividaClass.php');
    include_once('../model/ataReuniaoMensalAssinadaClass.php');
    include_once('../model/organismoClass.php');
    include_once('../model/recebimentoClass.php');
    include_once('../model/despesaClass.php');
    include_once('../model/membrosRosacruzesAtivosClass.php');

    $ataReuniaoMensal					= new ataReuniaoMensal();
    $ataReuniaoMensalAssinada				= new ataReuniaoMensalAssinada();
    $divida         					= new Divida();
    $quitacaoDivida       					= new QuitacaoDivida();
    $r 							= new Recebimento();
    $d 							= new Despesa();
    $o 							= new organismo();
    $mra                                                    = new MembrosRosacruzesAtivos();


    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        
    </style>
    <style media="print">
        
    </style>

    
    <?php
    if($_REQUEST['filtro']==1)
    {
        $regiao = $_REQUEST['regiao'];
        $oa = null;
    }else{
        $regiao = null;
        $oa = $_REQUEST['oa'];
    }
    $resultado = $o->listaOrganismo(null,null,null,null,$regiao,$oa,null,null,null,null,null,1);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];
    

    ?>
    <br />
    <table border="1" width="100%">
        <tr>
            <td colspan="6"><b>RELATÓRIO GERENCIAL (DESPESAS)</b></td>
        </tr>
        <tr>
            <td>
                Organismo Afiliado:
            </td>
            <td>
                <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
            </td>
            <td>
                Mês Inicial:
            </b>
            <td>
                <?php echo substr($_REQUEST['dataInicial'],3,7); ?>
            </td>
            <td>
                Mês Final:
            </td>
            <td>
                <?php echo substr($_REQUEST['dataFinal'],3,7); ?>
            </td>
        </tr>
        <tr>
            <th align="center">Mês</th>
            <th align="center">Ano</th>
            <?php //if($_REQUEST['linha1']==1){?>
            <th align="center">Comissões</th>
            <?php //}?>
            <?php //if($_REQUEST['linha2']==1){?>
            <th align="center">Reg. Conv. Jo.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha3']==1){?>
            <th align="center">Reun. Sociais</th>
            <?php //}?>
            <?php //if($_REQUEST['linha11']==1){?>
            <th align="center">Correios</th>
            <?php //}?>
            <?php //if($_REQUEST['linha4']==1){?>
            <th align="center">Anúncios</th>
            <?php //}?>
            <?php //if($_REQUEST['linha9']==1){?>
            <th align="center">Carta Const.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha10']==1){?>
            <th align="center">Cantina</th>
            <?php //}?>
            <?php //if($_REQUEST['linha5']==1){?>
            <th align="center">Desp. Gerais</th>
            <?php //}?>
            <?php //if($_REQUEST['linha6']==1){?>
            <th align="center">GLP Trim.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha7']==1){?>
            <th align="center">GLP Sumprim.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha12']==1){?>
            <th align="center">Investimentos</th>
            <?php //}?>
        </tr>
        <?php
    $y= (int) $anoInicio;
    $total = $totalIntervalo+$mesInicio;
    $mesASerInformado=$mesInicio;
    $i=$mesInicio;
    $totalAnos = (int) ($total/12);
    $poteAno=0;
    $total1=0;
    $total2=0;
    $total3=0;
    $total4=0;
    $total5=0;
    $total6=0;
    $total7=0;
    $total8=0;
    $total9=0;
    $total10=0;
    $total11=0;
    while($i<13)
    {
        $mesExtenso = mesExtensoPortugues($mesASerInformado);
       
       //Outras Despesas
      
       $comissoes 				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,2);
       $total1+=$comissoes;     
       $convencoesSaida                         = $d->retornaSaida($i,$y,$idOrganismoAfiliado,10);
       $jornadasSaida				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,11);
       $regiaoSaida				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,23);
       $totalSaidaConvencoesJornadas            = $convencoesSaida+$jornadasSaida+$regiaoSaida;
       $total2+=$totalSaidaConvencoesJornadas;
       $reunioesSociais                         = $d->retornaSaida($i,$y,$idOrganismoAfiliado,12);
       $total3+=$reunioesSociais;
       $despesaCorreio				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,13);
       $total4+=$despesaCorreio;
       $anuncios 				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,14);
       $total5+=$anuncios;
       $cartaConstitutivaGLP			= $d->retornaSaida($i,$y,$idOrganismoAfiliado,15);
       $total6+=$cartaConstitutivaGLP;
       $cantina 				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,16);
       $total7+=$cantina;
       $despesasGerais				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,17);
       $total8+=$despesasGerais;
       $glpRemessaTrimestralidade		= $d->retornaSaida($i,$y,$idOrganismoAfiliado,18);
       $total9+=$glpRemessaTrimestralidade;
       $glpPagamentosSuprimentos		= $d->retornaSaida($i,$y,$idOrganismoAfiliado,19);
       $total10+=$glpPagamentosSuprimentos;
       $investimentos				= $d->retornaSaida($i,$y,$idOrganismoAfiliado,21);
       $total11+=$investimentos;
        ?>
        <tr>
            <td align="center">
                <?php echo $mesExtenso;?>
            </td>
            <td align="center">
                <?php echo $y; ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($comissoes, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($totalSaidaConvencoesJornadas, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($reunioesSociais, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($despesaCorreio, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($anuncios, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($cartaConstitutivaGLP, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($cantina, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($despesasGerais, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php echo number_format($glpRemessaTrimestralidade, 2, ',', '.');?>
            </td>
            <td align="center">
                <?php echo number_format($glpPagamentosSuprimentos, 2, ',', '.');?>
            </td>
            <td align="center">
                <?php echo number_format($investimentos, 2, ',', '.');?>
            </td>
        </tr>
        <?php
        
        
            $mesASerInformado++;
            $i++;
        
    }
        ?>
        <tr>
            <td colspan="2" align="right"><b>Total</b></td>
            <td align="center">
                <?php
                    echo number_format($total1, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total2, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total3, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total4, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total5, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total6, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total7, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total8, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total9, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total10, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total11, 2, ',', '.');
                ?>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <?php
       }
    }
}
?>