<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once("../controller/imovelController.php");
    require_once ("../model/imovelClass.php");
    require_once ("../model/usuarioClass.php");

    $idImovel	= isset($_REQUEST['idImovel']) ? json_decode($_REQUEST['idImovel']) : '';

    $icc = new imovelController();
    $dados = $icc->buscaImovel($idImovel);
    ?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Sistema dos Organismos Afiliados</title>

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <link href="../font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

    </head>
    <body>
                    <div class="gray-bg" style="min-height: 815px;">
                <div class="wrapper wrapper-content  animated fadeInRight article">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="text-center article-title">
                                        <h1>
                                            <?php
                                            include_once("../controller/organismoController.php");
                                            require_once ("../model/organismoClass.php");

                                            $oc = new organismoController();
                                            $dadosOa = $oc->buscaOrganismo($dados->getFk_idOrganismoAfiliado());
                                            switch ($dadosOa->getClassificacaoOrganismoAfiliado()) {
                                                case 1: echo "Loja "; break;
                                                case 2: echo "Pronaos "; break;
                                                case 3: echo "Capítulo "; break;
                                                case 4: echo "Heptada "; break;
                                                case 5: echo "Atrium "; break;
                                            }
                                            switch ($dadosOa->getTipoOrganismoAfiliado()) {
                                                case 1: echo "R+C "; break;
                                                case 2: echo "TOM "; break;
                                            }
                                            echo $dadosOa->getNomeOrganismoAfiliado().' - '.$dadosOa->getSiglaOrganismoAfiliado();
                                            ?>
                                        </h1>
                                    </div>
                                    <hr>
                                            <div class="row">					
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Imóvel: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                        <?php echo $dados->getEnderecoImovel().' - '.$dados->getNumeroImovel().' - '.$dados->getComplementoImovel();?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Status: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <div class="statusTarget<?php echo $dados->getStatusImovel()?>">
                                                                                                            <?php
                                                        switch($dados->getStatusImovel()){
                                                                                                                    case 0: echo "<span class='badge badge-primary'>Ativo</span>"; break;
                                                                                                                    case 1: echo "<span class='badge badge-danger'>Inativo</span>"; break;
                                                                                                            }
                                                                                                            ?>
                                                                                                            </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Horário de visualização: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <?php echo date('H:i d/m/Y'); ?>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Bairro: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $dados->getBairroImovel(); ?></p></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Cidade: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-8" style="max-width: 320px">
                                                    <p class="form-control-static">
                                                        <?php
                                                        include_once '../model/imovelClass.php';
                                                        $im = new imovel();
                                                        $resultadoCidade = $im->buscaCidade($dados->getFk_idCidade());

                                                        if ($resultadoCidade) {
                                                            foreach ($resultadoCidade as $vetorCidade) {

                                                                echo $vetorCidade['nome'];
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Estado: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-8" style="max-width: 320px">
                                                    <p class="form-control-static">
                                                        <?php
                                                        include_once '../model/imovelClass.php';
                                                        $im = new imovel();
                                                        $resultadoEstado = $im->buscaEstado($dados->getFk_idEstado());

                                                        if ($resultadoEstado) {
                                                            foreach ($resultadoEstado as $vetorEstado) {

                                                                echo $vetorEstado['nome'];
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">País: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-8" style="max-width: 320px">
                                                    <p class="form-control-static">
                                                        <?php
                                                        switch ($dados->getFkIdPais()){
                                                            case 1: echo "Brasil"; break;
                                                            case 2: echo "Portugal"; break;
                                                            case 3: echo "Angola"; break;
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">CEP: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $dados->getCepImovel(); ?></p></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Área Total (m²): </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-1" style="max-width: 320px"><p class="form-control-static"><?php echo $dados->getAreaTotalImovel(); ?></p></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Área Contruída (m²): </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-1" style="max-width: 320px"><p class="form-control-static"><?php echo $dados->getAreaConstruidaImovel(); ?></p></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Tipo de Propriedade: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-12" style="max-width: 320px">
                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                        <?php
                                                        switch($dados->getPropriedadeTipoImovel()){
                                                            case '': echo "Não Informado"; break;
                                                            case 0: echo "Prédio"; break;
                                                            case 1: echo "Sobrado"; break;
                                                            case 2: echo "Casa"; break;
                                                            case 3: echo "Barracão"; break;
                                                            case 4: echo "Outros"; break;
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                            <hr>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Status da Propriedade: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-12" style="max-width: 320px">
                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                        <?php switch($dados->getPropriedadeStatusImovel()){
                                                            case 0: echo "Não Informado"; break;
                                                            case 1: echo "Própria"; break;
                                                            case 2: echo "Alugada"; break;
                                                            case 3: echo "Cedida";  break;
                                                            case 4: echo "Comodato"; break;
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($dados->getPropriedadeStatusImovel() != 0){ ?>
                                        <?php if($dados->getPropriedadeStatusImovel() == 1){ ?>
                                            <hr>
                                            <div id="statusProprio">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Data de Fundação</label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-8" style="max-width: 320px">
                                                                <p class="form-control-static">
                                                                    <?php if($dados->getDataFundacaoImovel()!='00/00/0000'){ echo $dados->getDataFundacaoImovel();  } else { echo 'Não Informado';} ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Propriedade em nome: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php switch($dados->getPropriedadeEmNomeImovel()){
                                                                        case 0: echo "Indefinido"; break;
                                                                        case 1: echo "GLB"; break;
                                                                        case 2: echo "GLP"; break;
                                                                        case 3: echo "O.A."; break;
                                                                        case 4: echo "Outros"; break;
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Possui Escritura: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php switch($dados->getPropriedadeEscrituraImovel()){
                                                                        case 0:
                                                                            echo "Sim";
                                                                            break;
                                                                        case 1:
                                                                            echo "Não";
                                                                            break;
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if($dados->getPropriedadeEscrituraImovel() == 0){ ?>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Tipo de Escritura: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php
                                                                        require_once ("../model/imovelClass.php");
                                                                        $im = new imovel();
                                                                        $resultado2 = $im->buscaTipoEscrituraImovel($dados->getFk_idTipoEscrituraImovel());
                                                                        if ($resultado2) {
                                                                            foreach ($resultado2 as $vetor2) {
                                                                                echo $vetor2['tipoEscrituraImovel'];
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Data da Escritura: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php if($dados->getPropriedadeEscrituraDataImovel()!='00/00/0000'){ echo $dados->getPropriedadeEscrituraDataImovel();  } else { echo 'Não Informado';} ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <hr>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Possui Registro de Imóvel: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php
                                                                    switch($dados->getRegistroImovel()){
                                                                        case 0: echo "Sim"; break;
                                                                        case 1: echo "Não"; break;
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if($dados->getRegistroImovel() == 0){ ?>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Data do Registro de Imóvel: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php if($dados->getRegistroImovelDataImovel()!='00/00/0000'){ echo $dados->getRegistroImovelDataImovel();  } else { echo 'Não Informado';} ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Nº da Matrícula: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php echo $dados->getRegistroImovelNumeroImovel(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <hr>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Valor Venal: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php if($dados->getValorVenalImovel()!=''){ echo 'R$ '.$dados->getValorVenalImovel().' ,00';}else{ echo 'Não Informado';} ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Valor Atual Aproximado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php if($dados->getValorAproximadoImovel()!=''){ echo 'R$ '.$dados->getValorAproximadoImovel().' ,00';}else{ echo 'Não Informado';} ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Imóvel Avaliado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php
                                                                    switch($dados->getAvaliacaoImovel()){
                                                                        case 0: echo "Sim"; break;
                                                                        case 1: echo "Não"; break;
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if($dados->getAvaliacaoImovel() == 0){?>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Valor Avaliado: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php if($dados->getAvaliacaoValorImovel()!=''){ echo 'R$ '.$dados->getAvaliacaoValorImovel().' ,00';}else{ echo 'Não Informado';} ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Nome do Avaliador: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php if($dados->getAvaliacaoNomeImovel()!=''){ echo $dados->getAvaliacaoNomeImovel();  } else { echo 'Não Informado';} ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Creci do Avaliador: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php if($dados->getAvaliacaoCreciImovel()!=''){ echo $dados->getAvaliacaoCreciImovel();  } else { echo 'Não Informado';} ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="col-sm-4 control-label" style="text-align: right">Data da Avaliação: </label>
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                        <?php if($dados->getAvaliacaoDataImovel()!='00/00/0000'){ echo $dados->getAvaliacaoDataImovel();  } else { echo 'Não Informado';} ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                            </div>
                                        <?php } ?>
                                        <?php if(($dados->getPropriedadeStatusImovel() == 2) || ($dados->getPropriedadeStatusImovel() == 3) || ($dados->getPropriedadeStatusImovel() == 4)){ ?>
                                            <div id="statusOutro">
                                                <hr>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Locador/Cessionário: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php if($dados->getLocadorImovel()!=''){ echo $dados->getLocadorImovel();  } else { echo 'Não Informado';} ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Locatário/Cedente: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                    <?php if($dados->getLocatarioImovel()!=''){ echo $dados->getLocatarioImovel();  } else { echo 'Não Informado';} ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Início do Contrato/Acordo: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($dados->getAluguelContratoInicioImovel()!='00/00/0000'){ echo $dados->getAluguelContratoInicioImovel();} else { echo 'Não Informado';}?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Término do Contrato/Acordo: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($dados->getAluguelContratoFimImovel()!='00/00/0000'){ echo $dados->getAluguelContratoFimImovel();} else { echo 'Não Informado';}?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Valor de Aluguel: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($dados->getAluguelValorImovel()!=''){ echo 'R$ '.$dados->getAluguelValorImovel().' ,00';}else{ echo 'Não Informado';} ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <hr>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Último a atualizar: </label>
                                            <div class="col-sm-8">
                                                <?php
                                                $um = new Usuario();
                                                $resultadoAtualizadoPor = $um->buscaUsuario($dados->getFk_seqCadastAtualizadoPor());
                                                if ($resultadoAtualizadoPor) {
                                                    foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                                        echo $vetorAtualizadoPor['nomeUsuario'];
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Mainly scripts -->
        <script src="../js/jquery-2.1.1.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="../js/inspinia.js"></script>
        <script src="../js/plugins/pace/pace.min.js"></script>
    </body>
    </html>
<?php
}
?>
					