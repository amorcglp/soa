<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idAtividadeIniciatica              = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';
    $idOrganismo                        = isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';

    include_once("../controller/atividadeIniciaticaOficialController.php");
    include_once("../controller/atividadeIniciaticaColumbaController.php");
    include_once("../controller/atividadeIniciaticaController.php");
    include_once("../controller/membroOAController.php");
    include_once("../controller/organismoController.php");
    include_once("../lib/functions.php");
    include_once("../lib/webservice/retornaInformacoesMembro.php");
    include_once("../model/atividadeIniciaticaClass.php");
    $aioc 	= new atividadeIniciaticaOficialController();
    $aim 	= new atividadeIniciaticaController();
    $aicc 	= new atividadeIniciaticaColumbaController();
    $aic 	= new atividadeIniciaticaController();
    $a      = new atividadeIniciatica();
    $moam 	= new membroOA();
    $oc 	= new organismoController();


		$resultado = $aioc->listaAtividadeIniciaticaOficial($idOrganismo);
    if ($resultado) {
        foreach ($resultado as $vetor) {
            $idAtividadeIniciaticaOficial       = $vetor['idAtividadeIniciaticaOficial'];
        }
    }

    $nomeOrganismo = '';
    $dadosOa = $oc->buscaOrganismo($idOrganismo);
    switch ($dadosOa->getClassificacaoOrganismoAfiliado()) {
            case 1: $nomeOrganismo = "Loja "; break;
            case 2: $nomeOrganismo = "Pronaos "; break;
            case 3: $nomeOrganismo = "Capítulo "; break;
            case 4: $nomeOrganismo = "Heptada "; break;
            case 5: $nomeOrganismo = "Atrium "; break;
    }
    switch ($dadosOa->getTipoOrganismoAfiliado()) {
            case 1: $nomeOrganismo .= "R+C "; break;
            case 2: $nomeOrganismo .= "TOM "; break;
    }
    $nomeOrganismo .= $dadosOa->getNomeOrganismoAfiliado()." - ";
    $nomeOrganismo .= $dadosOa->getSiglaOrganismoAfiliado();

    if(isset($idAtividadeIniciaticaOficial)) {
        $dados      = $aioc->buscaAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial);
    }
    $dadosAtiv      = $aic->buscaAtividadeIniciatica($idAtividadeIniciatica);

    ?>


    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    font-family: Helvetica, Arial, sans-serif;
    font-size: 12px!important;
    color: rgb(85, 85, 85);
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Times New Roman;
    }

    .tamanho-fonte{
            font-size: 16px;
    }

    .tamanho-fonte_2{
            font-size: 15px;
    }
    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table {
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 4px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) {
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    </style>
    <style media="print">
    .botao {
    display: none;
    }
    </style>

    <style>
            #cab_cabecalho {width: 100%; height: 120px}
            #cabecalho {height: 114px}
            #oa {width: 260px; height: 19px; padding-left: 14px}
            #data {width: 131px; height: 19px}
            #ano {width: 61px; height: 19px}
            .solado {}

            #cab_centro {width: 100%; height: 50px}
            #cab_centro_esquerda {float: left; height: 50px; width: 50%;}
            #cab_centro_esquerda_corpo {width: 300px;}
            #cab_centro_direita {float: right; height: 50px; width: 50%;}
            #cab_centro_direita_corpo {float: right; width: 150px;}

            #cab_rodape {width: 100%; height: 15px}
            #cab_rodape_esquerda {float: left; width: 33%; height: 15px}
            #cab_rodape_centro {float: left; width: 33%; height: 15px}
            #cab_rodape_direita {float: right; width: 33%; height: 15px}
    </style>

    <div id="cab_cabecalho">
            <div style="width: 225px; height: 70px; margin-bottom: 15px">
                    <img class="solado" src="../img/solado_atas.png">
            </div>
            <center><p class="fonte tamanho-fonte_2"><b><?php 
                                if($dadosAtiv->getTipoAtividadeIniciatica()<=12)
                                {    
                                        echo 'INICIAÇÃO AO '.$dadosAtiv->getTipoAtividadeIniciatica().'º GRAU DE TEMPLO';
                                }
                                if($dadosAtiv->getTipoAtividadeIniciatica()==13)
                                {    
                                        echo 'INICIAÇÃO A LOJA';
                                }
                                if($dadosAtiv->getTipoAtividadeIniciatica()==15)
                                {    
                                        echo 'DISCURSO DE ORIENTAÇÃO';
                                }
                                        ?>
                    </b></p></center>
            <br><br>
    </div>

    <div id="cab_centro">
            <div id="cab_centro_esquerda">
                    <div id="cab_centro_esquerda_corpo">
                            <p><b>Organismo Afiliado:</b> <br><?php echo $nomeOrganismo;?><br></p>
                <?php if(isset($idAtividadeIniciaticaOficial)) { ?>
                <p><b>Ano R+C da Equipe:</b><br> 
                    <?php
                        $dataAtividade = $dadosAtiv->getDataRealizadaAtividadeIniciatica();
                        $dia = substr($dataAtividade,0,2);
                        $mes = substr($dataAtividade,3,2);
                        $ano = substr($dataAtividade,6,4);
                        echo anoRC($ano, $mes, $dia);
                    ?><br></p>
                <?php } ?>
                    </div>
            </div>
            <div id="cab_centro_direita">
                    <div id="cab_centro_direita_corpo">
                <p><b>Data da Atividade:</b> <?php echo $dadosAtiv->getDataRealizadaAtividadeIniciatica();?><br></p>
                <p><b>Hora da Atividade:</b><br> <?php echo $dadosAtiv->getHoraRealizadaAtividadeIniciatica();?><br></p>
                    </div>
            </div>
    </div>
    <br>
    <br>
    <div class="centro botao"><br><br><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a><br></div>
    <br>
    <center><b>LISTA DE MEMBROS QUE COMPARECERAM NA INICIAÇÃO</b></center>
    <br>
    <center>
            <table border="1" class="table table-action">
                    <thead>
                            <tr>
                                    <th  width="45%">Nome</th>
                                    <th  width="40%">Assinatura</th>
                                    <th  width="15%">Nº GLP</th>
                            </tr>
                    </thead>
                    <tbody>
											<?php
											$pote = 0;
											$resultadoMembros = $a->listaAtividadeIniciaticaMembros($idAtividadeIniciatica);
											if ($resultadoMembros) {
													foreach ($resultadoMembros as $vetorMembros) {
														if ($vetorMembros['statusAtividadeIniciaticaMembro'] == 2){
															$pote++;
											?>
                            <tr class="">
                                    <td><?php echo retornaNomeFormatado($vetorMembros['nomeAtividadeIniciaticaMembro'],4); ?></td>
                                    <td> </td>
                                    <td><?php echo retornaCodigoAfiliacao($vetorMembros['seqCadastAtividadeIniciaticaMembro']); ?></td>
                            </tr>
											<?php
														}
													}
											}
											for ($i=0; $i < (15-$pote); $i++) {
											?>
                            <tr class="">
                                    <td>&nbsp;</td>
                                    <td> </td>
                                    <td></td>
                            </tr>
											<?php } ?>
                    </tbody>
            </table>
    </center>
<?php
}
?>
