<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    require_once("../lib/functions.php");
    require_once("../lib/webservice/retornaInformacoesMembro.php");

    $idRelatorioClasseArtesaos = isset($_REQUEST['idRelatorioClasseArtesaos'])?$_REQUEST['idRelatorioClasseArtesaos']:null;

    include_once("../controller/relatorioClasseArtesaosController.php");
    $r = new relatorioClasseArtesaosController();
    $dados = $r->buscaRelatorioClasseArtesaos($idRelatorioClasseArtesaos);

    include_once("../controller/organismoController.php");
    $o = new organismoController();
    $dadosOA = $o->buscaOrganismo($dados->getFk_idOrganismoAfiliado());
    $siglaOrganismoAfiliado = $dadosOA->getSiglaOrganismoAfiliado();

    //Mestre do OA
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='201';//Mestre do OA
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $mestreOA="";
    $mestreOA2 = "";
    $mestreCodigoOA="";
    $mestreCodigoOA2 = "";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
        $mestreOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];  
        $mestreCodigoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fCodMembro']
                );
        $mestreOA = $arr[0];
        $mestreOA2 = $arr[1];
        $mestreCodigoOA = $arr['cod'][0];
        $mestreCodigoOA2 = $arr['cod'][1];
    } 

    //Mestre da Classe dos Artesãos
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='315';//Mestre da Classe dos Artesãos
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $mestreClasseArtesaos="";
    $mestreClasseArtesaos2 = "";
    $mestreCodigoClasseArtesaos="";
    $mestreCodigoClasseArtesaos2 = "";

    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
        $mestreClasseArtesaos=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient']; 
        $mestreCodigoClasseArtesaos=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fCodMembro']
                );
        $mestreClasseArtesaos = $arr[0];
        $mestreClasseArtesaos2 = $arr[1];
        $mestreCodigoClasseArtesaos = $arr['cod'][0];
        $mestreCodigoClasseArtesaos2 = $arr['cod'][1];
    }

    $rca = new relatorioClasseArtesaos();
    $resultado4 = $rca->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($_REQUEST['idRelatorioClasseArtesaos']);
    $assinaturas="<br>";
    if($resultado4)
    {
        foreach($resultado4 as $vetor4)
        {
            $nomeUsuario = $vetor4['nomeUsuario'];
            $codigoAfiliacao = $vetor4['codigoDeAfiliacao'];
            $funcao = $vetor4['nomeFuncao'];

            $assinaturas .= $nomeUsuario." [".$codigoAfiliacao."]"." - ".$funcao."<br>";
        }
    }

    $temNumeroAssinatura=false;
    $rca2 = new relatorioClasseArtesaos();
    $resultado = $rca2->buscarIdRelatorioClasseArtesaos($_REQUEST['idRelatorioClasseArtesaos']);
    if($resultado) {
        foreach ($resultado as $vetor) {

            if(trim($vetor['numeroAssinatura'])!="")
            {
                $numeroAssinatura = $vetor['numeroAssinatura'];
                $temNumeroAssinatura=true;
            }else{
                $numeroAssinatura = aleatorioAssinatura();
                $rca2->setIdRelatorioClasseArtesaos($vetor['idRelatorioClasseArtesaos']);
                $rca2->atualizaNumeroAssinatura($numeroAssinatura);
            }

            //Verificar se está entregue
            $pronto = ($vetor['entregue']==1)? true : false;

        }
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: justify;
    font-family: Avenir LT Std;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>RELATÓRIO DE ATIVIDADE DA CLASSE DOS ARTESÃOS</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo retornaNomeCompletoOrganismoAfiliado($dados->getFk_idOrganismoAfiliado()); ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            Data do Encontro:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $dados->getDataEncontro(); ?>
                    </span>	
                <br>
                    <!--
                    <b class="fonte tamanho-fonte_2">
                            Ano R+C:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php //echo anoRC(substr($dados->getDataEncontro(),6,4),substr($dados->getDataEncontro(),3,2),substr($dados->getDataEncontro(),0,2)); ?>
                    </span>
                    -->
                    <b class="fonte tamanho-fonte_2">
                            Nº de Participantes:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $dados->getNumeroParticipantes(); ?>
                    </span>	

            </div>
    </center>
    <br>
    <h4>ORIENTAÇÕES</h4>
    <ol>
        <li>Para usufruir da força da Egrégora Rosacruz reunida, realize a Classe no dia e no horário programados.</li>
        <li>O objetivo do encontro dos Artesãos é servir ao Cósmico, aos rosacruzes e à humanidade.</li>
        <li>Pedimos a gentileza do(a) Mestre da Classe dos Artesãos preencher todos os dados solicitados.</li>
        <li>Utilize apenas o presente formulário para enviar as informações.</li>
        <li>Remeta este formulário à Grande Loja após a realização da Classe.</li>
    </ol>
    <br>
    <h4>OBSERVAÇÕES</h4>
    <ol>
        <li>A Classe dos Artesãos é realizada no Templo de uma Loja ou de um Capítulo.</li>
        <li>Para assistir à Classe é imperativo ter alcançado o Décimo Segundo Grau dos estudos, ter recebido em Loja as Iniciações de todos os Graus de Templo e recomenda-se que seja membro ativo de uma Loja, de um Capítulo ou de um Pronaos. Aqueles que fizeram todas as Iniciações do 1º ao 12º Grau em Loja estão com o processo validado para frequentar a Classe dos Artesãos.</li>
        <li>Um membro que esteja frequentando a Classe dos Artesãos pode comparecer como visitante a uma outra Classe em outro Capítulo ou Loja.</li>
        <li>Os Artesãos ativos na G.L.P que ainda não estão afiliados a um Organismo Afiliado não estão autorizados a participar da Classe dos Artesãos como convidados.</li>
    </ol>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Mestre do Organismo Afiliado:</th>
      </tr>
      <tr>
        <td>
            <?php if($mestreOA!=""){ echo $mestreCodigoOA." - ".$mestreOA;}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <?php if($mestreOA2!=""){?>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Mestre do Organismo Afiliado Retirante:</th>
      </tr>
      <tr>
        <td>
            <?php if($mestreOA2!=""){ echo $mestreCodigoOA2." - ".$mestreOA2;}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <?php }?>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Mestre da Classe dos Artesãos:</th>
      </tr>
      <tr>
        <td>
            <?php if($mestreClasseArtesaos!=""){ echo $mestreCodigoClasseArtesaos." - ".$mestreClasseArtesaos;}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <?php if($mestreClasseArtesaos2!=""){?>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Mestre da Classe dos Artesãos Retirante:</th>
      </tr>
      <tr>
        <td>
            <?php if($mestreClasseArtesaos2!=""){ echo $mestreCodigoClasseArtesaos2." - ".$mestreClasseArtesaos2;}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <?php }?>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Natureza da mensagem apresentada:</th>
      </tr>
      <tr>
        <td>
            <?php if($dados->getNaturezaMensagemApresentada()!=""){ echo $dados->getNaturezaMensagemApresentada();}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Questões Levantadas:</th>
      </tr>
      <tr>
        <td><?php echo $dados->getQuestoesLevantadas();?>	
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Clima geral da classe:</th>
      </tr>
      <tr>
        <td>
            <?php if($dados->getClimaGeralClasse()!=""){ echo $dados->getClimaGeralClasse();}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Surgiu algum problema?</th>
      </tr>
      <tr>
        <td>Resposta: 
                    <?php if($dados->getSurgiuProblema()==1){ echo "Sim";}?>
                    <?php if($dados->getSurgiuProblema()==2){ echo "Não";}?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Em caso afirmativo, pedimos a gentileza de descrever o ocorrido:</th>
      </tr>
      <tr>
        <td>
                    <?php if($dados->getSurgiuProblemaQual()!=""){ echo $dados->getSurgiuProblemaQual();}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Outras observações que o(a) Mestre de Classe dos Artesãos deseja fazer:</th>
      </tr>
      <tr>
        <td>
                    <?php if($dados->getObservacoes()!=""){ echo $dados->getObservacoes();}else{ echo "--";}?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" width="100%">
      <tr>
        <th>Número de Afiliação dos Membros participantes na Classe dos Artesãos:</th>
      </tr>
      <tr>
        <td>
                    <?php

                    include_once '../model/relatorioClasseArtesaosMembrosClass.php';
                    $rcam = new relatorioClasseArtesaosMembros();

                    $resultado = $rcam->lista($idRelatorioClasseArtesaos);

                    if($resultado)
                    { 
                        foreach ($resultado as $vetor)
                        {
                                echo retornaCodigoAfiliacao($vetor['fk_seq_cadastMembro']). " - ".  retornaNomeCompleto($vetor['fk_seq_cadastMembro'])."<br>";
                        }
                    }
                    ?>
        </td>
      </tr>
    </table>
    <br><br>
    Data: <?php echo date('d/m/Y');?>
    <br><br><br><br>
    <?php if($pronto){?>
    <br><br>
    <b>Concordaram e assinaram eletrônicamente este documento:</b><br><br>
<?php }?>

    <center><?php if($pronto){ if($assinaturas!="<br>"){echo $assinaturas;}else{ echo "NINGUÉM ASSINOU ESSE DOCUMENTO";}}?></center><br>
    <hr>
    <center><b>Código da Assinatura Eletrônica: <font color="blue"><?php echo $numeroAssinatura; ?></font></b>
        <br><br>(Documento gerado eletronicamente, para validar sua autenticidade utilize a opção validar documento dentro do sistema SOA)
    </center>
    <?php

    ?>
    <!--
    <table width="100%">
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Mestre da Classe dos Artesãos  <?php echo $mestreClasseArtesaos;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
    </table>
    <?php //if($mestreClasseArtesaos2!=""){?>
    <br><br>
    <table width="100%">
            <tr><td width="50%">__________________________________________________________________________</td></tr>
            <tr><td width="50%">Mestre da Classe dos Artesãos Retirante  <?php echo $mestreClasseArtesaos2;?></td></tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>&nbsp;</td></tr>
    </table>
    <?php //}?>-->
<?php
}
?>