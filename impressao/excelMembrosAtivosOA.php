<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<?php
include_once("../model/membrosRosacruzesAtivosClass.php");
include_once("../model/organismoClass.php");
include_once("../lib/functions.php");

//Pegar filtros
$regiao                     = isset($_REQUEST['regiao'])?$_REQUEST['regiao']:null;
$fk_idOrganismoAfiliado     = isset($_REQUEST['fk_idOrganismoAfiliado'])?$_REQUEST['fk_idOrganismoAfiliado']:null;
$mesInicial                 = isset($_REQUEST['mesInicial'])?$_REQUEST['mesInicial']:null;
$anoInicial                 = isset($_REQUEST['anoInicial'])?$_REQUEST['anoInicial']:null;
$mesFinal                   = isset($_REQUEST['mesFinal'])?$_REQUEST['mesFinal']:null;
$anoFinal                   = isset($_REQUEST['anoFinal'])?$_REQUEST['anoFinal']:null;
$tipoRelatorio              = isset($_REQUEST['tipoRelatorio'])?$_REQUEST['tipoRelatorio']:null;

if(substr($fk_idOrganismoAfiliado,0,1)=="S"||substr($fk_idOrganismoAfiliado,0,1)==""||$fk_idOrganismoAfiliado==0)
{
    $fk_idOrganismoAfiliado=null;
}  

if($regiao==0)
{
    $regiao=null;
}    

$dataInicial    = "01/".$mesInicial."/".$anoInicial;
$dataFinal      = "01/".$mesFinal."/".$anoFinal;

$dtInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
$dtFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);
//echo $dtInicial;
$date1 = new DateTime($dtInicial); 
$date2 = new DateTime($dtFinal);

$data1  = $date1->format('Y-m-d H:i:s');
$data2  = $date2->format('Y-m-d H:i:s');

$diff = $date1->diff($date2); 
$totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

$totalIntervalo = $totalDeMeses+1;

$totalMeses = $totalIntervalo+$mesInicial;

$membrosAtivos = new MembrosRosacruzesAtivos();
$organismos    = new organismo();

/* Verificação se preencheu antes da plotagem*/
$msg=false;

for($ano=$anoInicial; $ano <= $anoFinal; $ano++) {
    $y = (int)$anoInicial;
    $oas = $organismos->listaOrganismo(null, null, null, null, $regiao, $fk_idOrganismoAfiliado, null, 1);
    if ($oas) {
        foreach ($oas as $oa) {
            $idOrganismo = $oa['idOrganismoAfiliado'];
            $naoCobrarDashboard = $oa['naoCobrarDashboard'];
            $preencheu = false;
            if ($ano != $anoInicial) {
                $mesInicial = 1;
            }
            for ($i = $mesInicial; $i <= $totalMeses; $i++) {
                if ($i <= 12) {
                    ?>
                    <?php

                    if ($membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo) != 0 && $membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo) != "") {
                        $preencheu = true;
                    }

                    ?>
                    <?php
                }
                if ($anoFinal == $ano) {
                    $totalMeses = $mesFinal;
                }
            }
            if ($tipoRelatorio == 2 && $preencheu == TRUE) {
                $msg=true;
            }
        }
    }
}

if($msg){
?>
<script>alert('Esse organismo possui dados preenchidos, por favor selecione `Organismos que preencheram` para gerar corretamente o Relatório.');
    location.href='../painelDeControle.php?corpo=buscaRelatorioMembrosAtivos';</script>
<?php
exit();
}

//Informações do Arquivo
$arquivo = 'MEMBROS-ATIVOS-OA-GERAL.xls';

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

for($ano=$anoInicial; $ano <= $anoFinal; $ano++){
    $y= (int) $anoInicial;
?>
    <table class="table table-striped table-bordered table-hover dataTables-example" id="editable2">
        <thead>
            <tr>
                <td colspan="13"><b>Membros Ativos Ano: <?php echo $ano;?></b></td>
            </tr>
            <tr id="linha0">
                    <th width="400">Organismo</th>
                    <?php 
                    if($ano!=$anoInicial)
                    {    
                        $mesInicial=1;
                    }
                    for($i=$mesInicial;$i<=$totalMeses;$i++)
                    {
                        if($i<=12)
                        {    
                    ?>
                    <th width="200"><?php echo substr(mesExtensoPortugues($i),0,3);?> / <?php echo $ano;?></th>
                    <?php
                        }  
                        if($anoFinal==$ano)
                        {
                            $totalMeses= (int) $mesFinal;
                        } 
                    }
                    ?>
            </tr>
        </thead>
        <tbody id="tabela">
            <?php
            $oas = $organismos->listaOrganismo(null,null,null,null,$regiao,$fk_idOrganismoAfiliado,null,1);
            if($oas){
                foreach ($oas as $oa) {
                    $idOrganismo = $oa['idOrganismoAfiliado'];
                    $naoCobrarDashboard = $oa['naoCobrarDashboard'];
                    $preencheu=false;
                    if($ano!=$anoInicial)
                    {    
                        $mesInicial=1;
                    }  
                    for($i=$mesInicial;$i<=$totalMeses;$i++)
                    {
                        if($i<=12)
                        {
                    ?>
                            <?php 

                            if($membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo)!=0&&$membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo)!="")
                            {
                                $preencheu=true;
                            }        

                            ?>
                    <?php
                        }
                        if($anoFinal==$ano)
                        {
                            $totalMeses=$mesFinal;
                        }
                    }
                    if($tipoRelatorio==2&&$preencheu==TRUE)
                    {
                        ?>
                            <script>alert('Esse organismo possui dados preenchidos, por favor selecione `Organismos que preencheram` para gerar corretamente o Relatório.');window.close();</script>
                        <?php
                            exit();
                    }
                    if($tipoRelatorio==1&&$preencheu==TRUE&&$naoCobrarDashboard==0)
                    {
            ?>
                <tr>
                    <td id="1td" width="400">
                        <?php 
   
                            echo retornaNomeCompletoOrganismoAfiliado($idOrganismo);
                         
                        ?>
                    </td>
                    <?php 
                    if($ano!=$anoInicial)
                    {    
                        $mesInicial=1;
                    }  
                    for($i=$mesInicial;$i<=$totalMeses;$i++)
                    {
                        if($i<=12)
                        {
                    ?>
                    <td id="td">
                    <?php 
                    
                    if($membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo)!=0&&$membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo)!="")
                    {    
                        echo $membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo); 
                    }else{
                        if($tipoRelatorio==1)
                        {    
                            echo "--";
                        }
                    }
                    ?>
                    </td>
                    <?php
                        }
                        if($anoFinal==$ano)
                        {
                            $totalMeses=$mesFinal;
                        }
                    }
                    ?>
                </tr>
            <?php 
                    }
                    //NÃO PREENCHIDOS
                    if($tipoRelatorio==2&&$preencheu==FALSE&&$naoCobrarDashboard==0)
                    {
            ?>
                <tr>
                    <td id="1td" width="400">
                        <?php 
   
                            echo retornaNomeCompletoOrganismoAfiliado($idOrganismo);
                         
                        ?>
                    </td>
                    <?php 
                    if($ano!=$anoInicial)
                    {    
                        $mesInicial=1;
                    }  
                    for($i=$mesInicial;$i<=$totalMeses;$i++)
                    {
                        if($i<=12)
                        {
                    ?>
                    <td id="td">
                    <?php 
                    
                    if($membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo)!=0&&$membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo)!="")
                    {    
                        echo $membrosAtivos->retornaNroMembrosRosacruzesAtivos($i, $ano, $idOrganismo); 
                    }else{
                        if($tipoRelatorio==1)
                        {    
                            echo "--";
                        }
                    }
                    ?>
                    </td>
                    <?php
                        }
                        if($anoFinal==$ano)
                        {
                            $totalMeses=$mesFinal;
                        }
                    }
                    ?>
                </tr>
            <?php 
                    }
                }
            }
            ?>
        </tbody>
    </table>
<?php  
}
?>