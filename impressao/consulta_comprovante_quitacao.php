<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    session_start();
    date_default_timezone_set('America/Sao_Paulo');
    $codigoAfiliacao=isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:null;
    $ideCompanheiro=isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:'N';
    $nome=isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
    $dataAdmissao =isset($_REQUEST['dataAdmissao'])?$_REQUEST['dataAdmissao']:null;
    $dataQuitacaoRC =isset($_REQUEST['dataQuitacaoRC'])?$_REQUEST['dataQuitacaoRC']:null;
    $dataQuitacaoTOM =isset($_REQUEST['dataQuitacaoTOM'])?$_REQUEST['dataQuitacaoTOM']:null;
    $loteAtualRC =isset($_REQUEST['loteAtualRC'])?$_REQUEST['loteAtualRC']:null;
    $loteAtualTOM =isset($_REQUEST['loteAtualTOM'])?$_REQUEST['loteAtualTOM']:null;

    include_once '../model/organismoClass.php';
    $o = new organismo();

    include_once '../model/usuarioClass.php';
    $u = new Usuario();

    $resultado = $u->buscaUsuario($_SESSION['seqCadast']);
    $nomeUsuario="";
    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    $nomeUsuario = $vetor['nomeUsuario'];
            }
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    /* font-family: Helvetica, Arial, sans-serif; */
    font-family: Calibri;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 154px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
    .oculto {
    display: none;
    }

    </style>


    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <br class="oculto">

    <center>
    <div id="cabecalho">
            <center><img class="solado" src="../img/header.png"></center>
    </div>
    <h2>Comprovante de Quitação</h2>
    <div id="informacoesMembro" style="text-align: left;">
    Nome: <?php echo $nome;?><br>
    Código de Afiliação: <?php echo $codigoAfiliacao;?><br>
    Data de Admissão: <?php echo $dataAdmissao;?><br>
    <?php if($dataQuitacaoRC!="--"){?>Lote Atual R+C: <?php echo $loteAtualRC;?><br><?php }?>
    Quitação R+C até: <?php echo $dataQuitacaoRC;?><br>
    <?php if($dataQuitacaoTOM!="--"){?>Lote Atual TOM: <?php echo $loteAtualTOM;?><br><?php }?>
    Quitação TOM até: <?php echo $dataQuitacaoTOM;?><br>
    </div>
    <br>__________________________________
    <br>Assinatura
    <br><br>
    Impresso em <?php echo date("d/m/Y");?> às <?php echo date("H:i:s");?> - Documento de uso na AMORC<br>
    <?php echo $nomeUsuario;?> 

    </center>
<?php
}
?>