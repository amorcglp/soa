<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    

    set_time_limit(0);
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    ini_set("display_errors", 1 );

    $dataInicial = isset($_REQUEST['dataInicial'])?$_REQUEST['dataInicial']:null;
    $dataFinal = isset($_REQUEST['dataFinal'])?$_REQUEST['dataFinal']:null;
    
    $mesFinalInt = (int) substr($dataFinal,3,2);

    $mesInicio = (int) substr($dataInicial,3,2);
    $anoInicio = substr($dataInicial,6,4);

    $dtInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
    $dtFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);

    $date1 = new DateTime($dtInicial);
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2);
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

    $totalIntervalo = $totalDeMeses+1;
    
    $arquivo = 'SOA-RELATORIO-FINANCEIRO-'.$dtInicial.'-'.$dtFinal.'.xls';

    header ("Content-type: application/x-msexcel");
    header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
    header ("Content-Description: PHP Generated Data" );

    include_once('../lib/functions.php');
    include_once('../model/ataReuniaoMensalClass.php');
    include_once('../model/dividaClass.php');
    include_once('../model/quitacaoDividaClass.php');
    include_once('../model/ataReuniaoMensalAssinadaClass.php');
    include_once('../model/organismoClass.php');
    include_once('../model/recebimentoClass.php');
    include_once('../model/despesaClass.php');
    include_once('../model/membrosRosacruzesAtivosClass.php');

    $ataReuniaoMensal					= new ataReuniaoMensal();
    $ataReuniaoMensalAssinada				= new ataReuniaoMensalAssinada();
    $divida         					= new Divida();
    $quitacaoDivida       					= new QuitacaoDivida();
    $r 							= new Recebimento();
    $d 							= new Despesa();
    $o 							= new organismo();
    $mra                                                    = new MembrosRosacruzesAtivos();


    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        
    </style>
    <style media="print">
        .oculto {
            display: none;
        }
    </style>
    <!--
    <div class="centro oculto">
        <a href="#" onclick="window.print();" class="botao">
            <img src="../img/impressora.png" />
        </a>
    </div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
        <img class="solado" src="../img/header.png" />
    </div>
    -->
    <?php
    if($_REQUEST['filtro']==1)
    {
        $regiao = $_REQUEST['regiao'];
        $oa = null;
    }else{
        $regiao = null;
        $oa = $_REQUEST['oa'];
    }
    $resultado = $o->listaOrganismo(null,null,null,null,$regiao,$oa,null,null,null,null,null,1);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];

    /*
     * Calcular saldo dos anos anteriores
     */
    //Encontrar Saldo e Data Inicial
    include_once('../model/saldoInicialClass.php');
    $si = new saldoInicial();
    $dataSaldoInicial="2016-01-01";
    $mesSaldoInicial = "01";
    $anoSaldoInicial = "2016";
    $saldoInicial=0;
    $resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    $dataSaldoInicial = $vetor['dataSaldoInicial'];
                    $mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
                    $anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
                    $saldoInicial = (float) str_replace(",",".",str_replace(".","",$vetor['saldoInicial']));
            }
    }


    $dtInicial =$dataSaldoInicial;
    $dtFinal = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-01";

    $date1 = new DateTime($dtInicial);
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2);
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');
    $sCalc=0;
    $difCalc=0;

    if($totalDeMeses==0)
    {
        $sInicialFiltro = $saldoInicial;
    }else{
        $totalIntervalo2 = $totalDeMeses+1;
        $y=$anoSaldoInicial;
        $total = $totalIntervalo2;
        for($i=$mesSaldoInicial;$i<$total;$i++)//Janeiro a Fevereiro
        {

           $rCalc						= $r->retornaEntrada($i,$y,$idOrganismoAfiliado);

           $dCalc						= retornaSaidas($r,$d,$i,$y,$idOrganismoAfiliado);

           $difCalc                                         = ($rCalc-$dCalc);

           if($i==$mesSaldoInicial)
           {

              $sInicialFiltro					= $saldoInicial+($rCalc-$dCalc);
           }else{
              $sInicialFiltro					= $sCalc+$difCalc;
           }

           if($i==12)//Se mês impresso é Dezembro o próximo ano é diferente
           {
               $y++;
           }
        }
    }

    $sAnteriorCalc = $sInicialFiltro;

    ?>
    <br />
    <br />
    <table border="1" width="100%">
        <tr>
            <td colspan="6"><b>RELATÓRIO GERENCIAL (GERAL)</b></td>
        </tr>
        <tr>
            <td>
                Organismo Afiliado:
            </td>
            <td>
                <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
            </td>
            <td>
                Mês Inicial:
            </b>
            <td>
                <?php echo substr($_REQUEST['dataInicial'],3,7); ?>
            </td>
            <td>
                Mês Final:
            </td>
            <td>
                <?php echo substr($_REQUEST['dataFinal'],3,7); ?>
            </td>
        </tr>
        <tr>
            <th align="center">Mês</th>
            <th align="center">Ano</th>
            <?php if($_REQUEST['linha1']==1){?>
            <th align="center">RAM</th>
            <?php }?>
            <?php if($_REQUEST['linha2']==1){?>
            <th align="center">RAM - Lançamento no SOA</th>
            <?php }?>
            <?php if($_REQUEST['linha3']==1){?>
            <th align="center">RAM - Enviado no SOA</th>
            <?php }?>
            <?php if($_REQUEST['linha11']==1){?>
            <th align="center">RAM - Monitor Regional presente?</th>
            <?php }?>
            <?php if($_REQUEST['linha4']==1){?>
            <th align="center">Saldo Anterior</th>
            <?php }?>
            <?php if($_REQUEST['linha9']==1){?>
            <th align="center">Entradas</th>
            <?php }?>
            <?php if($_REQUEST['linha10']==1){?>
            <th align="center">Saídas</th>
            <?php }?>
            <?php if($_REQUEST['linha5']==1){?>
            <th align="center">Saldo Atual</th>
            <?php }?>
            <?php if($_REQUEST['linha6']==1){?>
            <th align="center">Divida Atual GLP</th>
            <?php }?>
            <?php if($_REQUEST['linha7']==1){?>
            <th align="center">Dívida Atual Outros</th>
            <?php }?>
            <?php if($_REQUEST['linha12']==1){?>
            <th align="center">Receitas totais Organismo</th>
            <?php }?>
            <?php if($_REQUEST['linha13']==1){?>
            <th align="center">Despesas totais Organismo</th>
            <?php }?>
            <?php if($_REQUEST['linha14']==1){?>
            <th align="center">Mensalidades Receita</th>
            <?php }?>
            <?php if($_REQUEST['linha15']==1){?>
            <th align="center">AMRA Receita</th>
            <?php }?>
            <?php if($_REQUEST['linha16']==1){?>
            <th align="center">Outras Receitas</th>
            <?php }?>
            <?php if($_REQUEST['linha17']==1){?>
            <th align="center">Luz, Água, Fone Despesa</th>
            <?php }?>
            <?php if($_REQUEST['linha18']==1){?>
            <th align="center">Taxas, Imp. Despesa</th>
            <?php }?>
            <?php if($_REQUEST['linha19']==1){?>
            <th align="center">Manutenção Despesa</th>
            <?php }?>
            <?php if($_REQUEST['linha20']==1){?>
            <th align="center">Outras Despesas</th>
            <?php }?>
            <?php if($_REQUEST['linha8']==1){?>
            <th align="center">Membros Ativos</th>
            <?php }?>
        </tr>
        <?php
    $y= (int) $anoInicio;
    $total = $totalIntervalo+$mesInicio;
    $mesASerInformado=$mesInicio;
    $i=$mesInicio;
    $totalAnos = (int) ($total/12);
    $poteAno=0;
    while($i<13)
    {
        $mesExtenso = mesExtensoPortugues($mesASerInformado);
        $resultadoAta = $ataReuniaoMensal->listaAtaReuniaoMensal($i,$y,$idOrganismoAfiliado);
        $resultadoAtaAssinada = $ataReuniaoMensalAssinada->listaAtaReuniaoMensalAssinada(null,$i,$y,$idOrganismoAfiliado);
        $resultadoDividaGLP = $divida->listaDivida($idOrganismoAfiliado, $i, $y, null,true);
        $resultadoDividaOutros = $divida->listaDivida($idOrganismoAfiliado, $i, $y, null,null,true);
        $resultadoQuitacaoDividaGLP = $quitacaoDivida->listaQuitacaoDivida($idOrganismoAfiliado, $i, $y, true);
        $resultadoQuitacaoDividaOutros = $quitacaoDivida->listaQuitacaoDivida($idOrganismoAfiliado, $i, $y, null,true);

        /**
        * Cálculo Entradas
        */
       $rCalc						= $r->retornaEntrada($i,$y,$idOrganismoAfiliado);

       /**
        * Cálculo Saídas
        */
       $dCalc						= retornaSaidas($r,$d,$i,$y,$idOrganismoAfiliado);

       $difCalc                                         = ($rCalc-$dCalc);
       /**
        * Cálculo do Saldo
        */
       //$sJan						= $saldoAnterior+$rJan-$dJan;
       if($i==$mesInicio)
       {

          $sCalc						= $sInicialFiltro+($rCalc-$dCalc);
       }else{
          $sCalc						= $sCalc+$difCalc;
       }

       /**
        * Membros Ativos
        */
       $mCalc						= $mra->retornaMembrosRosacruzesAtivos($i,$y,$idOrganismoAfiliado);
       $mCalc						= $mCalc['numeroAtualMembrosAtivos'];

       /*
        * Calculo da dívida GLP
        */
       $totalDividaGLP=0;
       if($resultadoDividaGLP)
       {
           foreach($resultadoDividaGLP as $vetor)
           {
               $valorDivida = (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
               $totalDividaGLP += $valorDivida;

           }
       }

       /*
        * Calculo da dívida Outros
        */
       $totalDividaOutros=0;
       if($resultadoDividaOutros)
       {
           foreach($resultadoDividaOutros as $vetor)
           {
               $valorDivida = (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
               $totalDividaOutros += $valorDivida;

           }
       }

        /*
        * Calculo da quitação de dívidas GLP
        */
       $totalPagamentoGLP=0;
       if($resultadoQuitacaoDividaGLP)
       {
           foreach($resultadoQuitacaoDividaGLP as $vetor)
           {
               $valorPagamento = (float) str_replace(",",".",str_replace(".","",$vetor['valorPagamento']));
               $totalPagamentoGLP += $valorPagamento;

           }
       }

       /*
        * Calculo da quitação de dívidas Outros
        */
       $totalPagamentoOutros=0;
       if($resultadoQuitacaoDividaOutros)
       {
           foreach($resultadoQuitacaoDividaOutros as $vetor)
           {
               $valorPagamento = (float) str_replace(",",".",str_replace(".","",$vetor['valorPagamento']));
               $totalPagamentoOutros += $valorPagamento;

           }
       }

       /*
        * Calculo da diferenca entre dividas e pagamentos
        */
       $totalDividaGLPSaida = $totalDividaGLP-$totalPagamentoGLP;
       $totalDividaOutrosSaida = $totalDividaOutros-$totalPagamentoOutros;
       
       /*
        * Cálculo da análise mais profunda das receitas e despesas do Organismo
        */
       
       //Receitas totais Organismo
       $totalReceitasTotaisOrganismo = $r->retornaEntrada($i,$y,$idOrganismoAfiliado);
       //Despesas totais Organismo
       $totalDespesasTotaisOrganismo = retornaSaidas($r,$d,$i,$y,$idOrganismoAfiliado);
       //Mensalidades Receita
       $totalMensalidadesReceita = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,1);
       //AMRA Receita
       $totalAMRAReceita = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,2);
       //Outras Receitas
       $totalOutrasReceitas=0;
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,3);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,4);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,5);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,6);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,7);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,8);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,9);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,10);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,11);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,12);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,13);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,14);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,15);
       $totalOutrasReceitas += $r->retornaEntrada($i,$y,$idOrganismoAfiliado,16);
       
       //Luz, Água Fone Despesa
       $totalLuzAguaFoneDespesa=0;
       $totalLuzAguaFoneDespesa += $d->retornaSaida($i,$y,$idOrganismoAfiliado,3);
       $totalLuzAguaFoneDespesa += $d->retornaSaida($i,$y,$idOrganismoAfiliado,4);
       $totalLuzAguaFoneDespesa += $d->retornaSaida($i,$y,$idOrganismoAfiliado,5);
       //Taxas, Impostos Despesa
       $totalTaxasImpDespesa=0;
       $totalLuzAguaFoneDespesa += $d->retornaSaida($i,$y,$idOrganismoAfiliado,6);
       $totalLuzAguaFoneDespesa += $d->retornaSaida($i,$y,$idOrganismoAfiliado,22);
       //Manutenção Despesa
       $totalManutencaoDespesa = $d->retornaSaida($i,$y,$idOrganismoAfiliado,7);
       //Outras Despesas
       $totalOutrasDespesas = 0;
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,1);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,2);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,8);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,9);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,10);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,11);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,12);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,13);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,14);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,15);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,16);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,17);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,18);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,19);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,20);
       $totalOutrasDespesas += $d->retornaSaida($i,$y,$idOrganismoAfiliado,21);
       
        ?>
        <tr>
            <td align="center">
                <?php echo $mesExtenso;?>
            </td>
            <td align="center">
                <?php echo $y; ?>
            </td>
            <?php if($_REQUEST['linha1']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAta)
                        {
                            $dataRAM = $resultadoAta[0]['dataAtaReuniaoMensal'];
                            $dataRAM = substr($dataRAM,8,2)."/".substr($dataRAM,5,2);
                            echo $dataRAM;
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha2']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAta)
                        {
                            $dataRAM = $resultadoAta[0]['data'];
                            $dataRAM = substr($dataRAM,8,2)."/".substr($dataRAM,5,2);
                            echo $dataRAM;
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha3']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAtaAssinada)
                        {
                            $dataRAM = $resultadoAtaAssinada[0]['dataCadastro'];
                            $dataRAM = substr($dataRAM,8,2)."/".substr($dataRAM,5,2);
                            echo $dataRAM;
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha11']==1){?>
            <td align="center">
                <?php
                        //echo "<pre>";print_r($resultado);
                        if($resultadoAta)
                        {
                            if($resultadoAta[0]['monitorRegionalPresente']==1)
                            {
                                echo "Sim";
                            }else{
                                echo "Não";
                            }    
                        }else{
                            echo "--";
                        }
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha4']==1){?>
            <td align="center">
                <?php
                        echo number_format($sAnteriorCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha9']==1){?>
            <td align="center">
                <?php
                        echo number_format($rCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha10']==1){?>
            <td align="center">
                <?php
                        echo number_format($dCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha5']==1){?>
            <td align="center">
                <?php
                        echo number_format($sCalc, 2, ',', '.');
                ?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha6']==1){?>
            <td align="center">
                <?php echo number_format($totalDividaGLPSaida, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha7']==1){?>
            <td align="center">
                <?php echo number_format($totalDividaOutrosSaida, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha12']==1){?>
            <td align="center">
                <?php echo number_format($totalReceitasTotaisOrganismo, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha13']==1){?>
            <td align="center">
                <?php echo number_format($totalDespesasTotaisOrganismo, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha14']==1){?>
            <td align="center">
                <?php echo number_format($totalMensalidadesReceita, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha15']==1){?>
            <td align="center">
                <?php echo number_format($totalAMRAReceita, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha16']==1){?>
            <td align="center">
                <?php echo number_format($totalOutrasReceitas, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha17']==1){?>
            <td align="center">
                <?php echo number_format($totalLuzAguaFoneDespesa, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha18']==1){?>
            <td align="center">
                <?php echo number_format($totalTaxasImpDespesa, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha19']==1){?>
            <td align="center">
                <?php echo number_format($totalManutencaoDespesa, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha20']==1){?>
            <td align="center">
                <?php echo number_format($totalOutrasDespesas, 2, ',', '.');?>
            </td>
            <?php }?>
            <?php if($_REQUEST['linha8']==1){?>
            <td align="center">
                <?php
                        echo $mCalc;
                ?>
            </td>
            <?php }?>
        </tr>
        <?php
        $sAnteriorCalc=$sCalc;
        
            $mesASerInformado++;
            $i++;
        
        
    }
        ?>

    </table>
    <br />
    <br />
    <?php
       }
    }
}
?>
