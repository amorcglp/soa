<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $trimestreAtual = isset($_REQUEST['trimestreAtual']) ? $_REQUEST['trimestreAtual'] : null;
    $anoAtual = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual'] : null;
    $idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : null;

    include_once('../lib/webservice/retornaInformacoesMembro.php');
    include_once('../lib/functions.php');
    include_once('../model/organismoClass.php');
    include_once('../model/atividadeColumbaClass.php');

    $ac = new atividadeColumba();
    $o = new organismo();

    $resultado = $o->buscaIdOrganismo($idOrganismoAfiliado);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
                    $nomeOrganismoAfiliado 	= $vetor['nomeOrganismoAfiliado'];
                    $classificacaoOA		= $vetor['classificacaoOrganismoAfiliado'];
                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }
            }
    }

    $resultado = $ac->listaAtividadeColumba($idOrganismoAfiliado,null,$trimestreAtual,$anoAtual);

    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    margin: 0;
    text-align: left;
    font-family: Avenir LT Std;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .fonte{
            font-family: Minion Pro;
    }

    .tamanho-fonte{
            font-size: 14px;
    }

    .tamanho-fonte_2{
            margin-top:0;
            font-size: 15px!important;
    }

    .fonte-3 {
            font-family: Avenir LT Std;
    }

    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 10px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 90px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
            .oculto {display: none;}
    </style>

    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <div style="width: 225px; height: 120px; margin-bottom: 30px">
    <img class="solado" src="../img/header.png">
    </div>
    <center><p class="fonte tamanho-fonte_2"><b>RELATÓRIO DE ATIVIDADES TRIMESTRAL DA ORIENTADORA DE COLUMBAS</b></p></center>
    <center class="tamanho-fonte">
            <div style="border: 1px solid #000; padding: 5px; width: 620px; font-family: Avenir LT Std;">
                    <b class="fonte tamanho-fonte_2">
                            Organismo Afiliado:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado." ".$siglaOrganismoAfiliado; ?>
                    </span>	
                    <b class="fonte tamanho-fonte_2">
                            Trimestre:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $trimestreAtual; ?>º
                    </span>
                    <b class="fonte tamanho-fonte_2">
                            Ano:
                    </b>
                    <span class="fonte tamanho-fonte_2">
                            <?php echo $anoAtual; ?>
                    </span>
            </div>
    </center>
    <?php
    if($resultado)
    {
        if(count($resultado)>0)
        {    
    ?> 
    <br><br>
    <table border="1" width="100%">
      <tr>
        <td rowspan="2"><center><b>Nº</b></center></td>
        <td rowspan="2" width="25%"><center><b>Columbas Atuantes</b></center></td>
        <td rowspan="2"><center><b>Nº de Inscrição na OGG</b></center></td>
        <td colspan="2"><center><b>Pais Ativos no OA</b></center></td>
        <td colspan="9"><center><b>ATIVIDADES QUE PARTICIPOU</b></center></td>
      </tr>
      <tr>
        <td><center><b>Sim</b></center></td>
        <td><center><b>Não</b></center></td>
        <td><center><b>Reuniões</b></center></td>
        <td><center><b>Ensaios</b></center></td>
        <td><center><b>Iniciações Loja/Capítulo</b></center></td>
        <td><center><b>Aposição de Nome</b></center></td>
        <td><center><b>Casamentos</b></center></td>
        <td><center><b>Convocações Ritualísticas</b></center></td>
        <td><center><b>Iniciações de Grau</b></center></td>
        <td><center><b>Instalação</b></center></td>
        <td><center><b>Avaliação</b></center></td>
      </tr>
      <?php
        $i=1;
        $comentarios="";
            foreach($resultado as $vetor)
            {
                $codigoAfiliacao = retornaCodigoAfiliacao($vetor['seqCadastMembro'],2);
                if($vetor['comentarios']!="")
                {    
                    $comentarios .= "<hr>";
                    $comentarios .= "A respeito da Columba ".retornaNomeCompleto($vetor['seqCadastMembro'])." - ".$vetor['comentarios'];
                }
                ?>
                    <tr>
                        <td><center><?php echo $i;?></center></td>
                        <td><center><?php echo retornaNomeCompleto($vetor['seqCadastMembro']);?></center></td>
                        <td><center><?php echo $codigoAfiliacao;?></center></td>
                        <td><center><?php if($vetor['paisAtivosOA']==1){ echo "X";}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['paisAtivosOA']==2){ echo "X";}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['reunioes']>0){ echo $vetor['reunioes'];}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['ensaios']>0){ echo $vetor['ensaios'];}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['iniciacoesLojaCapitulo']>0){ echo $vetor['iniciacoesLojaCapitulo'];}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['aposicaoNome']>0){ echo $vetor['aposicaoNome'];}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['casamento']>0){ echo $vetor['casamento'];}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['convocacoesRitualisticas']>0){ echo $vetor['convocacoesRitualisticas'];}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['iniciacoesGraus']>0){ echo $vetor['iniciacoesGraus'];}else{ echo "--";}?></center></td>
                        <td><center><?php if($vetor['instalacao']>0){ echo $vetor['instalacao'];}else{ echo "--";}?></center></td>
                        <td><center><?php switch ($vetor['avaliacao']){ 
                                    case 1:
                                        echo "Regular";
                                        break;
                                    case 2:
                                        echo "Bom";
                                        break;
                                    case 3:
                                        echo "Ótima";
                                        break;
                                    default :
                                        echo "--";
                                        break;
                                    }
                        ?></center></td>
                    </tr>
                <?php
                $i++;
            }       
      ?>
    </table>
    <?php if($comentarios!="")
    {
    ?>
    <br><br>
    <h3>Comentários:</h3>
    <?php echo $comentarios;?>
    <?php }?>
    <br><br><br>
    <?php 
    /*
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='201';//Mestre do OA
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $mestreOA="";
    $mestreOA2 = "";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
        $mestreOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];

    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $mestreOA = $arr[0];
        $mestreOA2 = $arr[1];
    } 
    */
    ?>
    <?php
        }
    }
    if(!$resultado){
            echo "<br><br><center><h4>Nenhuma atividade registrada neste trimestre</h4></center><br><br>";
    }
    ?>
    <?php 
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismoAfiliado;
    $seqFuncao='609';//Orientadora de Columba -> Antes estava Coordenadora de Columba
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $coordenadoraColumba="";
    $coordenadoraColumba2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
            $coordenadoraColumba=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
            $codigoAfiliacaoCC=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
                $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
                $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
                );
        $coordenadoraColumba = $arr[0];
        $coordenadoraColumba2 = $arr[1];
        $codigoAfiliacaoCC=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fCodMembro'];
        $codigoAfiliacaoCC2=$obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fCodMembro'];
    }
    ?>
    <?php
        require_once '../model/columbaTrimestralClass.php';
        $ct = new columbaTrimestral();
        $resultado5 = $ct->buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
        $assinaturas="<br>";
        if($resultado5)
        {
            foreach($resultado5 as $vetor5)
            {
                $nomeUsuario = $vetor5['nomeUsuario'];
                $codigoAfiliacao = $vetor5['codigoDeAfiliacao'];
                $funcao = $vetor5['nomeFuncao'];

                $assinaturas .= $nomeUsuario." [".$codigoAfiliacao."]"." - ".$funcao."<br>";
            }
        }

        //Verificar se está entregue
        $resultado = $ct->listaColumbaTrimestral($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
        if($resultado)
        {
            foreach ($resultado as $vetor)
            {
                if(trim($vetor['numeroAssinatura'])!="")
                {
                    $numeroAssinatura = $vetor['numeroAssinatura'];
                    $temNumeroAssinatura=true;
                }else{
                    $numeroAssinatura = aleatorioAssinatura();
                    $ct->atualizaNumeroAssinatura($trimestreAtual,$anoAtual,$idOrganismoAfiliado,$numeroAssinatura);
                }
            }
            $pronto = true;
        }else{
            $pronto = false;
        }
        ?>
        <br>
        <?php if($pronto){?>
        <br><br>
        <b>Concordaram e assinaram eletrônicamente este documento:</b><br><br>
    <?php }?>

        <center><?php if($pronto){
        if($assinaturas!="<br>"){echo $assinaturas;}else{ echo "NINGUÉM ASSINOU ESSE DOCUMENTO";}?></center><br>
        <hr>
        <center><b>Código da Assinatura Eletrônica: <font color="blue"><?php echo $numeroAssinatura; ?></font></b>
            <br><br>(Documento gerado eletronicamente, para validar sua autenticidade utilize a opção validar documento dentro do sistema SOA)
        </center>
        <?php }else{
            echo "NINGUÉM ASSINOU ESSE DOCUMENTO ELETRONICAMENTE<br><br>";
        }
}
?>