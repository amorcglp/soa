<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    set_time_limit(0);
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    ini_set("display_errors", 1 );

    $dataInicial = isset($_REQUEST['dataInicial'])?$_REQUEST['dataInicial']:null;
    $dataFinal = isset($_REQUEST['dataFinal'])?$_REQUEST['dataFinal']:null;
    
    $mesFinalInt = (int) substr($dataFinal,3,2);

    $mesInicio = (int) substr($dataInicial,3,2);
    $anoInicio = substr($dataInicial,6,4);

    $dtInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
    $dtFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);

    $date1 = new DateTime($dtInicial);
    $date2 = new DateTime($dtFinal);

    $data1  = $date1->format('Y-m-d H:i:s');
    $data2  = $date2->format('Y-m-d H:i:s');

    $diff = $date1->diff($date2);
    $totalDeMeses = ($diff->format('%Y')*12)+$diff->format('%m');

    $totalIntervalo = $totalDeMeses+1;
    
    $arquivo = 'SOA-RELATORIO-FINANCEIRO-'.$dtInicial.'-'.$dtFinal.'.xls';

    header ("Content-type: application/x-msexcel");
    header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
    header ("Content-Description: PHP Generated Data" );

    include_once('../lib/functions.php');
    include_once('../model/ataReuniaoMensalClass.php');
    include_once('../model/dividaClass.php');
    include_once('../model/quitacaoDividaClass.php');
    include_once('../model/ataReuniaoMensalAssinadaClass.php');
    include_once('../model/organismoClass.php');
    include_once('../model/recebimentoClass.php');
    include_once('../model/despesaClass.php');
    include_once('../model/membrosRosacruzesAtivosClass.php');

    $ataReuniaoMensal					= new ataReuniaoMensal();
    $ataReuniaoMensalAssinada				= new ataReuniaoMensalAssinada();
    $divida         					= new Divida();
    $quitacaoDivida       					= new QuitacaoDivida();
    $r 							= new Recebimento();
    $d 							= new Despesa();
    $o 							= new organismo();
    $mra                                                    = new MembrosRosacruzesAtivos();


    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        
    </style>

    <?php
    if($_REQUEST['filtro']==1)
    {
        $regiao = $_REQUEST['regiao'];
        $oa = null;
    }else{
        $regiao = null;
        $oa = $_REQUEST['oa'];
    }
    $resultado = $o->listaOrganismo(null,null,null,null,$regiao,$oa,null,null,null,null,null,1);

    if($resultado)
    {
            foreach ($resultado as $vetor)
            {
                    $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];

    ?>
    <br />
    <table border="1" width="100%">
        <tr>
            <td colspan="6"><b>RELATÓRIO GERENCIAL (RECEITAS)</b></td>
        </tr>
        <tr>
            <td>
                Organismo Afiliado:
            </td>
            <td>
                <?php echo $classificacao." ".$tipo." ".$nomeOrganismoAfiliado; ?>
            </td>
            <td>
                Mês Inicial:
            </b>
            <td>
                <?php echo substr($_REQUEST['dataInicial'],3,7); ?>
            </td>
            <td>
                Mês Final:
            </td>
            <td>
                <?php echo substr($_REQUEST['dataFinal'],3,7); ?>
            </td>
        </tr>
        <tr>
            <th align="center">Mês</th>
            <th align="center">Ano</th>
            <?php //if($_REQUEST['linha1']==1){?>
            <th align="center">Comissões</th>
            <?php //}?>
            <?php //if($_REQUEST['linha2']==1){?>
            <th align="center">Construção</th>
            <?php //}?>
            <?php //if($_REQUEST['linha3']==1){?>
            <th align="center">Ativ. Soc./Cul.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha11']==1){?>
            <th align="center">Reg. Conv. Jo.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha4']==1){?>
            <th align="center">Rec. Financ.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha9']==1){?>
            <th align="center">Bazar e Supr.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha10']==1){?>
            <th align="center">Rec.Divers.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha5']==1){?>
            <th align="center">GLP Trim.</th>
            <?php //}?>
            <?php //if($_REQUEST['linha6']==1){?>
            <th align="center">GLP Suprim.</th>
            <?php //}?>           
        </tr>
        <?php
    $y= (int) $anoInicio;
    $total = $totalIntervalo+$mesInicio;
    $mesASerInformado=$mesInicio;
    $i=$mesInicio;
    $totalAnos = (int) ($total/12);
    $poteAno=0;
    $total1=0;
    $total2=0;
    $total3=0;
    $total4=0;
    $total5=0;
    $total6=0;
    $total7=0;
    $total8=0;
    $total9=0;
    while($i<13)
    {
        $mesExtenso = mesExtensoPortugues($mesASerInformado);
       
       //Outras Receitas
      
       $comissoesEntrada		    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,4);
       $total1+=$comissoesEntrada;
       $construcao			    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,7);
       $total2+=$construcao;
       $atividadesSociais                   = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,5);
       $total3+=$atividadesSociais;
       
       $convencoesEntrada		    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,8);
       $jornadasEntrada                     = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,10);
       $regiaoEntrada			    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,16);
       $totalEntradaConvencoesJornadas      = $convencoesEntrada+$jornadasEntrada+$regiaoEntrada;
       $total4+=$totalEntradaConvencoesJornadas;
       
       $receitasFinanceiras		    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,9);
       $total5+=$receitasFinanceiras;
       
       $bazar				    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,3);
       $suprimentos			    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,11);
       $totalEntradaBazarSuprimentos        = $bazar+$suprimentos;
       $total6+=$totalEntradaBazarSuprimentos;
       
       $recebimentosDiversos		    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,6);
       $total7+=$recebimentosDiversos;
       $glpTrimestralidades		    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,12);
       $total8+=$glpTrimestralidades;
       $glpSuprimentos			    = $r->retornaEntrada($i,$y,$idOrganismoAfiliado,13);
       $total9+=$glpSuprimentos;
        ?>
        <tr>
            <td align="center">
                <?php echo $mesExtenso;?>
            </td>
            <td align="center">
                <?php echo $y; ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($comissoesEntrada, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($construcao, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($atividadesSociais, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($totalEntradaConvencoesJornadas, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($receitasFinanceiras, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($totalEntradaBazarSuprimentos, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($recebimentosDiversos, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($glpTrimestralidades, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php echo number_format($glpSuprimentos, 2, ',', '.');?>
            </td>
        </tr>
        <?php
        
            $mesASerInformado++;
            $i++;
        
        
    }
        ?>
        <tr>
            <td colspan="2" align="right"><b>Total</b></td>
            <td align="center">
                <?php
                    echo number_format($total1, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total2, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total3, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total4, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total5, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total6, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total7, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total8, 2, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                    echo number_format($total9, 2, ',', '.');
                ?>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <?php
       }
    }
}
?>