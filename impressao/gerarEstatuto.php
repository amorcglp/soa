<?php


    $idEstatuto = isset($_SESSION['idEstatuto']) ? $_SESSION['idEstatuto'] : $_REQUEST['idEstatuto'];

    if (realpath('../../model/estatutoClass.php')) {
        include_once('../../model/estatutoClass.php');
    } else {
        include_once('../model/estatutoClass.php');
    }
    if (realpath('../../lib/functions.php')) {
        include_once('../../lib/functions.php');
    } else {
        include_once('../lib/functions.php');
    }
    if (realpath("../../model/criaSessaoClass.php")) {
        require_once("../../model/criaSessaoClass.php");
    } else {
        require_once("../model/criaSessaoClass.php");
    }

    $sessao = new criaSessao();

    //Internacionalização
    i18n($sessao->getValue("paisUsuario"));

    $ap = new estatuto();
    $resultado = $ap->buscaIdEstatuto($idEstatuto);
    $arrEstatuto = array();
    if ($resultado) {
        foreach ($resultado as $vetor) {
            $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

            switch ($vetor['classificacaoOrganismoAfiliado']) {
                case 1:
                    $classificacao = "Loja";
                    break;
                case 2:
                    $classificacao = "Pronaos";
                    break;
                case 3:
                    $classificacao = "Capítulo";
                    break;
                case 4:
                    $classificacao = "Heptada";
                    break;
                case 5:
                    $classificacao = "Atrium";
                    break;
            }
            switch ($vetor['tipoOrganismoAfiliado']) {
                case 1:
                    $tipo = "R+C";
                    $tipo2 = "Rosacruz";
                    break;
                case 2:
                    $tipo = "TOM";
                    $tipo2 = "Martinista";
                    break;
            }

            switch ($vetor['paisOrganismoAfiliado']) {
                case 1:
                    $siglaPais = "BR";
                    break;
                case 2:
                    $siglaPais = "PT";
                    break;
                case 3:
                    $siglaPais = "AO";
                    break;
                case 4:
                    $siglaPais = "MZ";
                    break;
                default :
                    $siglaPais = "BR";
                    break;
            }
            $siglaOrganismo = $vetor['siglaOrganismoAfiliado'];
            $nomeOrganismo = $siglaOrganismo . " - " . $classificacao . " " . $tipo . " " . $nomeOrganismoAfiliado;
            $nomeOrganismo2 = $classificacao . " " . $tipo . " " . $nomeOrganismoAfiliado;
            $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];

            $seqOrganismoAfiliado = $vetor['seqCadast'];

            $ocultar_json = 1;
            $siglaOA = $siglaOrganismo;
            $pais = $siglaPais;
            //Consultar Webservice da Inovad
            if (realpath('../../js/ajax/retornaDadosOrganismo.php')) {
                include ('../../js/ajax/retornaDadosOrganismo.php');
            } else {
                include ('../js/ajax/retornaDadosOrganismo.php');
            }

            $obj = json_decode(json_encode($return), true);
            //echo "<pre>";print_r($obj);

            $dataElevacaoPronaos = "";
            $dataElevacaoCapitulo = "";
            $dataElevacaoLoja = "";
            $cidade = "";
            $uf = "";
            $rua = "";
            $numero = "";
            $bairro = "";
            $cnpj = "";
            $situacaoDB = "";
            if(isset($obj['result'][0]['fields']['fArrayOA']))
            {
                foreach ($obj['result'][0]['fields']['fArrayOA'] as $vetor2) {
                    $situacaoDB = $vetor2['fields']['fSituacaoOa'];
                    switch ($vetor2['fields']['fSituacaoOa']) {
                        case 'R':
                            $statusOrganismo = "Recesso";
                            break;
                        case 'A':
                            $statusOrganismo = "Ativo";
                            break;
                        case 'F':
                            $statusOrganismo = "Fechado";
                            break;
                    }

                    //Pegar Datas Históricas
                    $dataElevacaoPronaos = $vetor2['fields']['fDatElevacPronau'];
                    $dataElevacaoPronaos = substr($dataElevacaoPronaos, 8, 2) . " de " . mesExtensoPortugues(substr($dataElevacaoPronaos, 5, 2)) . " de " . substr($dataElevacaoPronaos, 0, 4);

                    $dataElevacaoCapitulo = $vetor2['fields']['fDatElevacCapitu'];
                    $dataElevacaoCapitulo = substr($dataElevacaoCapitulo, 8, 2) . " de " . mesExtensoPortugues(substr($dataElevacaoCapitulo, 5, 2)) . " de " . substr($dataElevacaoCapitulo, 0, 4);

                    $dataElevacaoLoja = $vetor2['fields']['fDatElevacLoja'];
                    $dataElevacaoLoja = substr($dataElevacaoLoja, 8, 2) . " de " . mesExtensoPortugues(substr($dataElevacaoLoja, 5, 2)) . " de " . substr($dataElevacaoLoja, 0, 4);

                    //Pegar endereço do OA
                    $cidade = $vetor2['fields']['fNomLocali'];
                    $uf = $vetor2['fields']['fSigUf'];
                    $rua = $vetor2['fields']['fDesLograd'];
                    $numero = $vetor2['fields']['fNumEndere'];
                    $bairro = $vetor2['fields']['fNomBairro'];

                    //Pegar CNPJ
                    $cnpj = $vetor2['fields']['fNumCnpj'];
                }
            }
            $numeroOficio = $vetor['numeroOficio'];
            if($numeroOficio!=0) {
                $numeroOficio = $vetor['numeroOficio']."º";
            }else{
                $numeroOficio ="";
            }
            $cidadeOficio = $vetor['cidadeOficio'];
            $ufOficio = $vetor['ufOficio'];

            $cartorioPronaos = $vetor['cartorioPronaos'];
            $comarcaPronaos = $vetor['comarcaPronaos'];
            $numeroPronaos = $vetor['numeroPronaos'];
            $numeroLeiMunicipalPronaos = $vetor['numeroLeiMunicipalPronaos'];
            $dataLeiMunicipalPronaos = substr($vetor['dataLeiMunicipalPronaos'], 8, 2) . " de " . mesExtensoPortugues(substr($vetor['dataLeiMunicipalPronaos'], 5, 2)) . " de " . substr($vetor['dataLeiMunicipalPronaos'], 0, 4);
            $numeroLeiEstadualPronaos = $vetor['numeroLeiEstadualPronaos'];
            $dataLeiEstadualPronaos = substr($vetor['dataLeiEstadualPronaos'], 8, 2) . " de " . mesExtensoPortugues(substr($vetor['dataLeiEstadualPronaos'], 5, 2)) . " de " . substr($vetor['dataLeiEstadualPronaos'], 0, 4);

            $cartorioCapitulo = $vetor['cartorioCapitulo'];
            $comarcaCapitulo = $vetor['comarcaCapitulo'];
            $numeroCapitulo = $vetor['numeroCapitulo'];
            $numeroLeiMunicipalCapitulo = $vetor['numeroLeiMunicipalCapitulo'];
            $dataLeiMunicipalCapitulo = substr($vetor['dataLeiMunicipalCapitulo'], 8, 2) . " de " . mesExtensoPortugues(substr($vetor['dataLeiMunicipalCapitulo'], 5, 2)) . " de " . substr($vetor['dataLeiMunicipalCapitulo'], 0, 4);
            $numeroLeiEstadualCapitulo = $vetor['numeroLeiEstadualCapitulo'];
            $dataLeiEstadualCapitulo = substr($vetor['dataLeiEstadualCapitulo'], 8, 2) . " de " . mesExtensoPortugues(substr($vetor['dataLeiEstadualCapitulo'], 5, 2)) . " de " . substr($vetor['dataLeiEstadualCapitulo'], 0, 4);

            $cartorioLoja = $vetor['cartorioLoja'];
            $comarcaLoja = $vetor['comarcaLoja'];
            $numeroLoja = $vetor['numeroLoja'];
            $numeroLeiMunicipalLoja = $vetor['numeroLeiMunicipalLoja'];
            $dataLeiMunicipalLoja = substr($vetor['dataLeiMunicipalLoja'], 8, 2) . " de " . mesExtensoPortugues(substr($vetor['dataLeiMunicipalLoja'], 5, 2)) . " de " . substr($vetor['dataLeiMunicipalLoja'], 0, 4);
            $numeroLeiEstadualLoja = $vetor['numeroLeiEstadualLoja'];
            $dataLeiEstadualLoja = substr($vetor['dataLeiEstadualLoja'], 8, 2) . " de " . mesExtensoPortugues(substr($vetor['dataLeiEstadualLoja'], 5, 2)) . " de " . substr($vetor['dataLeiEstadualLoja'], 0, 4);

            $dataAssembleiaGeral = substr($vetor['dataAssembleiaGeral'], 8, 2) . " de " . mesExtensoPortugues(substr($vetor['dataAssembleiaGeral'], 5, 2)) . " de " . substr($vetor['dataAssembleiaGeral'], 0, 4);
            
            $numeroPresentes = $vetor['numeroPresentes'];
            $nomeAdvogado = $vetor['nomeAdvogado'];
            $numeroOAB = $vetor['numeroOAB'];
        }
    }

    $anoRosacruz = anoRC(date('Y'), date('m'), date('d'));
    
    ?>

<?php
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismo;
$seqFuncao='201';//Mestre do OA
//$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
include '../js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$mestreOA="";
$mestreOA2 = "";
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $mestreOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];

}
if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
{
    $arr = ordenaOficialAtuanteRetirante(
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
    );
    $mestreOA = $arr[0];
    $mestreOA2 = $arr[1];
}
if($mestreOA=="")
{
    $mestreOA = $mestreOA2;
}
?>
<?php
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismo;
$seqFuncao='203';//Secretario do OA
//$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
include '../js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$secretarioOA="";
$secretarioOA2="";
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $secretarioOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
}
if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
{
    $arr = ordenaOficialAtuanteRetirante(
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
    );
    $secretarioOA = $arr[0];
    $secretarioOA2 = $arr[1];
}
if($secretarioOA=="")
{
    $secretarioOA = $secretarioOA2;
}
?>
<?php
if($classificacaoOA==2)
{
    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';
    $siglaOA=$siglaOrganismo;
    $seqFuncao='205';//Guardião do OA
    //$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
    include '../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    $guardiaoOA="";
    $guardiaoOA2="";
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
    {
        $guardiaoOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
    }
    if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
    {
        $arr = ordenaOficialAtuanteRetirante(
            $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
            $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
            $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
            $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
        );
        $guardiaoOA = $arr[0];
        $guardiaoOA2 = $arr[1];
    }
}
if($guardiaoOA=="")
{
    $guardiaoOA = $guardiaoOA2;
}
?>
<?php
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismo;
$seqFuncao='207';//Presidente da Junta Depositária
//$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
include '../js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$presidenteJuntaDepositariaOA="";
$presidenteJuntaDepositariaOA2="";
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $presidenteJuntaDepositariaOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
}
if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
{
    $arr = ordenaOficialAtuanteRetirante(
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
    );
    $presidenteJuntaDepositariaOA = $arr[0];
    $presidenteJuntaDepositariaOA2 = $arr[1];
}
if($presidenteJuntaDepositariaOA=="")
{
    $presidenteJuntaDepositariaOA = $presidenteJuntaDepositariaOA2;
}
?>
<?php
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismo;
$seqFuncao='209';//Secretário da Junta Depositária
//$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
include '../js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$secretarioJuntaDepositariaOA="";
$secretarioJuntaDepositariaOA2="";
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $secretarioJuntaDepositariaOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
}
if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
{
    $arr = ordenaOficialAtuanteRetirante(
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
    );
    $secretarioJuntaDepositariaOA = $arr[0];
    $secretarioJuntaDepositariaOA2 = $arr[1];
}
if($secretarioJuntaDepositariaOA=="")
{
    $secretarioJuntaDepositariaOA = $secretarioJuntaDepositariaOA2;
}
?>
<?php
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismo;
$seqFuncao='211';//Tesoureiro da Junta Depositária
//$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
include '../js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$tesoureiroJuntaDepositariaOA="";
$tesoureiroJuntaDepositariaOA2="";
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $tesoureiroJuntaDepositariaOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
}
if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
{
    $arr = ordenaOficialAtuanteRetirante(
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
    );
    $tesoureiroJuntaDepositariaOA = $arr[0];
    $tesoureiroJuntaDepositariaOA2 = $arr[1];
}
if($tesoureiroJuntaDepositariaOA=="")
{
    $tesoureiroJuntaDepositariaOA = $tesoureiroJuntaDepositariaOA2;
}
?>
<?php
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismo;
$seqFuncao='213';//Mestre Auxiliar
//$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
include '../js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$mestreAuxiliarOA="";
$mestreAuxiliarOA2="";
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $mestreAuxiliarOA=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
}
if(isset($obj['result'][0]['fields']['fArrayOficiais'][1]))
{
    $arr = ordenaOficialAtuanteRetirante(
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
        $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
        $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
    );
    $mestreAuxiliarOA = $arr[0];
    $mestreAuxiliarOA2 = $arr[1];
}
if($mestreAuxiliarOA=="")
{
    $mestreAuxiliarOA = $mestreAuxiliarOA2;
}
?>
<?php

require '../vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf();

if($classificacao=="Loja"||$classificacao=="Capítulo") {

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template1.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template2.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template3.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template4.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $css = file_get_contents('teste4/estatuto.css');
    $mpdf->WriteHTML($css, 1);

    $html =
        '<br><br><br><br><br><br><br><br><br><p><b>Art. 1º -</b> ';
    if ($classificacao == "Loja") {
        $artigo = "A";
    } else {
        $artigo = "O";
    }
    $html .= $artigo . ' "' . $nomeOrganismo2 . ', AMORC", com CNPJ/MF nº ' . $cnpj . ', registrada no Registro Civil de Pessoas Jurídicas, ' . $numeroOficio
        . ' Ofício de Títulos e Documentos da Comarca de ' . $cidadeOficio . ', Estado de ' . $ufOficio . ', República Federativa do Brasil, '
        . 'rege-se pelo presente Estatuto.'
        . '</p>';

    $html .= '<p><b>&sect; 1º -</b> ' . $artigo . ' "' . $nomeOrganismo2 . ', AMORC", adiante sempre denominado(a) "' . strtoupper($classificacao) . '"'
        . ', mediante sucessivas alterações estatutárias, teve as seguintes denominações da mesma e única pessoa jurídica:</p>';

    $html .= '<ol type="I">';

    if ($dataElevacaoPronaos != "") {
        $html .= '<li> "PRONAOS ROSACRUZ ' . $nomeOrganismoAfiliado . ', AMORC", fundado em ' . $dataElevacaoPronaos . ' com estatuto registrado no Cartório ' . $cartorioPronaos . ''
            . ' da Comarca de ' . $comarcaPronaos . ' sob nº ' . $numeroPronaos . ', inscrito no CNPJ/MF sob o nº ' . $cnpj . ', reconhecido como Utilidade Pública a nível '
            . 'municipal pela Lei Municipal nº ' . $numeroLeiMunicipalPronaos . ', de ' . $dataLeiMunicipalPronaos . ', e a nível estadual pela Lei Estadual '
            . 'nº ' . $numeroLeiEstadualPronaos . ', de ' . $dataLeiEstadualPronaos . '.</li>';
    }
    if ($dataElevacaoCapitulo != "") {
        $html .= '<li> "CAPÍTULO ROSACRUZ ' . $nomeOrganismoAfiliado . ', AMORC", elevado em ' . $dataElevacaoCapitulo . ' com estatuto registrado no Cartório ' . $cartorioCapitulo . ''
            . ' da Comarca de ' . $comarcaCapitulo . ' sob nº ' . $numeroCapitulo . ', inscrito no CNPJ/MF sob o nº ' . $cnpj . ', reconhecido como Utilidade Pública a nível '
            . 'municipal pela Lei Municipal nº ' . $numeroLeiMunicipalCapitulo . ', de ' . $dataLeiMunicipalCapitulo . ', e a nível estadual pela Lei Estadual '
            . 'nº ' . $numeroLeiEstadualCapitulo . ', de ' . $dataLeiEstadualCapitulo . '.</li>';
    }
    if ($dataElevacaoLoja != "") {
        $html .= '<li> "LOJA ROSACRUZ ' . $nomeOrganismoAfiliado . ', AMORC", elevado em ' . $dataElevacaoLoja . ' com estatuto registrado no Cartório ' . $cartorioLoja . ''
            . ' da Comarca de ' . $comarcaLoja . ' sob nº ' . $numeroLoja . ', inscrito no CNPJ/MF sob o nº ' . $cnpj . ', reconhecido como Utilidade Pública a nível '
            . 'municipal pela Lei Municipal nº ' . $numeroLeiMunicipalLoja . ', de ' . $dataLeiMunicipalLoja . ', e a nível estadual pela Lei Estadual '
            . 'nº ' . $numeroLeiEstadualLoja . ', de ' . $dataLeiEstadualLoja . '.</li>';
    }
    $html .= '</ol>';

    $html .= '<p><b>&sect; 2º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' tem sua sede e foro na cidade de ' . $cidade . ', Estado de ' . retornaEstado($uf) . ', '
        . 'República Federativa do Brasil, na Rua ' . $rua . ', nº ' . $numero . ', Bairro ' . $bairro . ', local onde se acha edificado seu Templo.'
        . '</p>';

    $html .= '<p><b>&sect; 3º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' foi concebido(a) e constituído(a), desde sua fundação, com o objetivo essencial'
        . ' de ser reconhecido(a) como um Organismo Afiliado da "Antiga e Mística Ordem Rosae Crucis, AMORC, Grande Loja da Jurisdição de Língua Portuguesa",'
        . ' doravante denominada AMORC-GLP ou simplesmente "Grande Loja", anteriormente denominada "Antiga e Mística Ordem Rosae Crucis - AMORC - Grande Loja do'
        . ' Brasil", fundada em 09 de maio de 1956, na cidade do Rio de Janeiro, Estado do Rio de Janeiro, República Federativa do Brasil, que tem sua sede e foro '
        . 'na cidade de Curitiba, Estado do Paraná, República Federativa do Brasil, na Rua Nicarágua, 2620, Bairro Bacacheri, local onde se acha edificado seu Grande'
        . ' Templo.</p>';

    $html .= '<p><b>&sect; 4º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' tem prazo indeterminado de duração.</p>';

    $html .= '<p><b>&sect; 5º -</b> Os vocábulos "místico", "misticismo" e expressões afins, utilizados pelo(a) ' . strtoupper($classificacao) . ' e pela AMORC-GLP,'
        . ' têm significados embasados na convicção da Ordem Rosacruz, AMORC, de que o Ser Humano pode se religar diretamente à sua origem divina, ao Deus do seu '
        . 'coração  e da sua compreensão, mediante o respeito, o culto e a harmonização com as leis cósmicas, com a natureza, com seus semelhantes e consigo mesmo.</p>';

    $html .= '<p><b>Art. 2º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' é um(a) organização mística, iniciática, fraternal, de caráter e de atividades templárias,'
        . 'beneficiente, caritativa, de cunho educacional dirigido para a ciência, filosofia, arte e cultura, sem fins econômicos, visando operacionalizar e administrar '
        . 'o Misticismo Rosacruz nos termos e limites estabelecidos pela AMORC - GLP, esta reconhecida por Carta Constitutiva outorgada pela Suprema Grande Loja da'
        . ' Antiga e Mística Ordem Rosae Crucis, AMORC, sediada em San José, Califórnia, EUA.</p>';

    $mpdf->WriteHTML($html, 2);

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template5.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $html =
        '<br><br><br><br><br><br><br><br><br><p class="normal"><b>Art. 3º -</b> ';
    $html .= 'A LOJA é constituída por um número ilimitado de integrantes, todos Membros de Sanctum da AMORC-GLP, ou de outra Grande Loja reconhecida pela Suprema '
        . 'Grande Loja da AMORC, que estejam em dia com seus deveres e obrigações estatutárias, com a primordial finalidade de praticar coletivamente os ensinamentos, '
        . 'cerimônias, rituais e iniciações rosacruzes preconizados e autorizados pela Grande Loja.</p>';

    $html .= '<p class="normal"><b>&sect; 1º -</b> ' . $artigo . ' ' . strtoupper($classificacao) . ' tem por objetivos:</p>';

    $html .= '<ol type="I" class="normal">';

    $html .= '<li>preservar e cultuar a Tradição Rosacruz e proporcionar a seus Membros ambiente dotado de recursos materiais e competências humanas que possibilitem a '
        . 'participação em rituais, cerimônias templárias, experimentos, práticas e atividades planejados ou recomendados pela AMORC-GLP;</li>';

    $html .= '<li>difundir o idealismo místico e metafísico mediante a prática da filosofia rosacruz aplicável ao cotidiano de seus Membros;</li>';

    $html .= '<li>propiciar aos seus Membros a vivência do processo iniciático rosacruz, incluindo atividades de templo, destinada a despertar as faculdades latentes do '
        . 'Ser Humano;</li>';

    $html .= '<li>ensinar, promover e perpetuar os princípios e leis tradicionais dos antigos rosacruzes, conforme aplicáveis às condições e necessidades atuais do Ser Humano;</li>';

    $html .= '<li>promover o espírito de fraternidade e compreensão entre os seres humanos e a boa vontade e cooperação internacionais;</li>';

    $html .= '<li>cooperar com as entidades cujos objetivos estejam de acordo com os da Ordem Rosacruz, AMORC, em benefício da humanidade em geral, em favor do Ser Humano '
        . 'como um '
        . 'todo, para intensificar sua felicidade e seu bem estar;</li>';

    $html .= '<li>perpetuar e desenvolver o Legado da Tradição e da Cultura Rosacruz, respeitando os seus princípios e os vínculos normativos, fraternais, intelectuais e '
        . 'espirituais '
        . 'com a AMORC-GLP e com a Antiga e Mística Ordem Rosae Crucis, AMORC e seus organismos devidamente constituídos em todo o mundo;</li>';

    $html .= '<li>promover a felicidade e o bem-estar do Ser Humano.</li>';

    $html .= '</ol>';

    $html .= '<p class="normal"><b>&sect; 2º -</b> ' . $artigo . ' ' . strtoupper($classificacao) . ' cumpre esses fins por meio de um sistema de instrução';
    if ($classificacao == "Loja") {
        $html .= ' e de um processo iniciático regular,*';
    }

    $html .= ' cujos conteúdos e formas são autorizados e recomendados exclusivamente pela AMORC-GLP, objetivando a transmissão do conhecimento teórico e a aplicação '
        . 'prática das Leis Divinas, Cósmicas e Naturais que se manifestam no Universo, em torno do Ser Humano e nele próprio.</p>';

    $html .= '<p class="normal"><b>&sect; 3º -</b> ' . $artigo . ' ' . strtoupper($classificacao) . ' pode realizar, promover, estimular e desenvolver atividades culturais, educacionais, '
        . 'artísticas e científicas de natureza ampla, incluindo a manutenção e operação de museus, bibliotecas, auditórios, espaços, centros e salas de cultura, '
        . 'de arte e templos para atividades cerimoniais e ritualísticas, voltadas para as crianças, jovens, adultos e idosos, em sua Sede e em seu Templo.</p>';

    $html .= '<p class="normal"><b>&sect; 4º -</b> A veiculação dos conteúdos místicos, educacionais, filosóficos e científicos será feita mediante todas as formas de comunicação, '
        . 'e por ocasião de reuniões, convocações, cursos, seminários etc., transmitindo a LOJA a seus Membros, bem como a eventuais interessados, o conhecimento '
        . 'e a aplicação das Leis Naturais e Cósmicas que se manifestam no Universo e no próprio Ser Humano, preservando e cultuando a Tradição Rosacruz através '
        . 'de Cerimônias e Rituais realizados em seu Templo ou instalações.</p>';

    $mpdf->WriteHTML($html, 2);

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template6.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template7.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template8.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template9.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template10.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template11.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template12.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template13.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template14.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template15.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template16.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template17.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template18.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template19.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template20.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template21.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template22.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $css = file_get_contents('teste4/estatuto.css');
    $mpdf->WriteHTML($css, 1);

    $html =
        '<br><br><br><br><br><br><br><br><br><p class="normal"><b>Art. 47º -</b> ';
    $html .= 'Os Membros-Dirigentes do(a) ' . strtoupper($classificacao) . ' emitirão manuais, regimentos, procedimentos e instruções que se fizerem necessários como adequação'
        . ' e cumprimento deste Estatuto, visando sempre a melhor forma de consolidação da cultura rosacruz na área de atuação da ' . strtoupper($classificacao) . '.</p>';

    $html .= '<p class="normal"><b>Art 48º -</b> São ratificados neste ato os propósitos místicos, iniciáticos e templários da AMORC-GLP e deste(a) ' . strtoupper($classificacao) . ', objetivando'
        . ' propiciar aos Fratres e Sorores um sistema educacional que possibilite o desenvolvimento pessoal e a busca da reintegração à essência divina, sob as garantias'
        . ' estabelecidas pela Constituição da República Federativa do Brasil, especialmente as previstas pelo art. 5º, incisos VI e VIII, pelo art. 150, VI "b", e as '
        . 'determinadas pelo art. 44,&sect; 1º, do Código Civil.</p>';

    $html .= '<p class="normal"><b>Art 49º -</b> O presente Estatuto foi aprovado em Assembléia Geral realizada em ' . $dataAssembleiaGeral . ', com a presença de ' . $numeroPresentes . ''
        . ' Membros-Estudantes em situação regular com suas obrigações e contribuições para com ' . $artigo . ' ' . strtoupper($classificacao) . ' e para com a AMORC-GLP, que o '
        . ' subscrevem, na cidade de ' . $cidade . ', Estado de ' . retornaEstado($uf) . ', Brasil.</p>';

    $html .= '<p class="normal"><b>Art 50º -</b> Revogam-se as disposições em contrário.</p>';

    $html .= '<p class="normal centro">' . $cidade . ', ' . date('d') . ' de ' . mesExtensoPortugues(date('m')) . ' de ' . date('Y') . '</p>';

    $html .= '<p class="normal">SEGUEM-SE:<br><br>Nome e assinatura dos Membros-Estudantes:';

    $html .= '<table border="0" width="100%" class="normal">
            <tr>
                <td width="25%" class="normal"><b>Mestre</b></td>
                <td width="40%" class="normal">'.$mestreOA.'</td>
                <td width="35%" class="normal"><b>Ass:___________________________</b></td>
            </tr>
            <tr>
                <td class="normal"><b>Secretário</b></td>
                <td class="normal">'.$secretarioOA.'</td>
                <td class="normal"><b>Ass:___________________________</b></td>
            </tr>
            <tr>
                <td class="normal"><b>Presidente da Junta Depositária</b></td>
                <td class="normal">'.$presidenteJuntaDepositariaOA.'</td>
                <td class="normal"><b>Ass:___________________________</b></td>
            </tr>
            <tr>
                <td class="normal"><b>Secretário da Junta Depositária</b></td>
                <td class="normal">'.$secretarioJuntaDepositariaOA.'</td>
                <td class="normal"><b>Ass:___________________________</b></td>
            </tr>
            <tr>
                <td class="normal"><b>Tesoureiro da Junta Depositária</b></td>
                <td class="normal">'.$tesoureiroJuntaDepositariaOA.'</td>
                <td class="normal"><b>Ass:___________________________</b></td>
            </tr>
            <tr>
                <td class="normal"><b>Advogado</b></td>
                <td class="normal">'.$nomeAdvogado.' <b>nº OAB:</b> '.$numeroOAB.'</td>
                <td class="normal"><b>Ass:___________________________</b></td>
            </tr>
    </table>';
}//FIM IMPRESSÃO LOJA E CAPÍTULO

if($classificacao=="Pronaos") {

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template1_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template2_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template3_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template4_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $css = file_get_contents('teste4/estatuto.css');
    $mpdf->WriteHTML($css, 1);

    $html =
        '<br><br><br><br><br><br><br><br><br><br><br><p><b>Art. 1º -</b> ';
    if ($classificacao == "Loja") {
        $artigo = "A";
    } else {
        $artigo = "O";
    }
    $html .= $artigo . ' "' . $nomeOrganismo2 . ', AMORC", com CNPJ/MF nº ' . $cnpj . ', registrada no Registro Civil de Pessoas Jurídicas, ' . $numeroOficio
        . ' Ofício de Títulos e Documentos da Comarca de ' . $cidadeOficio . ', Estado de ' . $ufOficio . ', República Federativa do Brasil, '
        . 'rege-se pelo presente Estatuto.'
        . '</p>';

    $html .= '<ol type="I">';

    if ($dataElevacaoPronaos != "") {
        $html .= '<li>O "PRONAOS ROSACRUZ ' . $nomeOrganismoAfiliado . ', AMORC", fundado em ' . $dataElevacaoPronaos . ' com estatuto registrado no Cartório ' . $cartorioPronaos . ''
            . ' da Comarca de ' . $comarcaPronaos . ' sob nº ' . $numeroPronaos . ', inscrito no CNPJ/MF sob o nº ' . $cnpj . ', reconhecido como Utilidade Pública a nível '
            . 'municipal pela Lei Municipal nº ' . $numeroLeiMunicipalPronaos . ', de ' . $dataLeiMunicipalPronaos . ', e a nível estadual pela Lei Estadual '
            . 'nº ' . $numeroLeiEstadualPronaos . ', de ' . $dataLeiEstadualPronaos . '. PREENCHER SOMENTE SE TIVER O(S) RECONHECIMENTOS</li>';
    }

    $html .= '</ol>';

    $html .= '<p><b>&sect; 2º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' tem sua sede e foro na cidade de ' . $cidade . ', Estado de ' . retornaEstado($uf) . ', '
        . 'República Federativa do Brasil, na Rua ' . $rua . ', nº ' . $numero . ', Bairro ' . $bairro . ', local onde se acha edificado seu Templo.'
        . '</p>';

    $html .= '<p><b>&sect; 3º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' foi concebido(a) e constituído(a), desde sua fundação, com o objetivo essencial'
        . ' de ser reconhecido(a) como um Organismo Afiliado da "Antiga e Mística Ordem Rosae Crucis, AMORC, Grande Loja da Jurisdição de Língua Portuguesa",'
        . ' doravante denominada AMORC-GLP ou simplesmente "Grande Loja", anteriormente denominada "Antiga e Mística Ordem Rosae Crucis - AMORC - Grande Loja do'
        . ' Brasil", fundada em 09 de maio de 1956, na cidade do Rio de Janeiro, Estado do Rio de Janeiro, República Federativa do Brasil, que tem sua sede e foro '
        . 'na cidade de Curitiba, Estado do Paraná, República Federativa do Brasil, na Rua Nicarágua, 2620, Bairro Bacacheri, local onde se acha edificado seu Grande'
        . ' Templo.</p>';

    $html .= '<p><b>&sect; 4º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' tem prazo indeterminado de duração.</p>';

    $html .= '<p><b>&sect; 5º -</b> Os vocábulos "místico", "misticismo" e expressões afins, utilizados pelo(a) ' . strtoupper($classificacao) . ' e pela AMORC-GLP,'
        . ' têm significados embasados na convicção da Ordem Rosacruz, AMORC, de que o Ser Humano pode se religar diretamente à sua origem divina, ao Deus do seu '
        . 'coração  e da sua compreensão, mediante o respeito, o culto e a harmonização com as leis cósmicas, com a natureza, com seus semelhantes e consigo mesmo.</p>';

    $html .= '<p><b>Art. 2º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' é um(a) organização mística, iniciática, fraternal, de caráter e de atividades templárias,'
        . 'beneficiente, caritativa, de cunho educacional dirigido para a ciência, filosofia, arte e cultura, sem fins econômicos, visando operacionalizar e administrar '
        . 'o Misticismo Rosacruz nos termos e limites estabelecidos pela AMORC - GLP, esta reconhecida por Carta Constitutiva outorgada pela Suprema Grande Loja da'
        . ' Antiga e Mística Ordem Rosae Crucis, AMORC, sediada em San José, Califórnia, EUA.</p>';

    $html .= '<p><b>Art. 3º -</b> ' . $artigo . ' "' . strtoupper($classificacao) . ' é constituído por um número ilimitado de integrantes, todos Membros de Sanctum '
        . 'da AMORC-GLP, ou de outra Grande Loja reconhecida pela Suprema Grande Loja da AMORC, que estejam em dia com seus deveres e obrigações estatuárias, com a '
        . 'primordial finalidade de praticar coletivamente os ensinamentos, cerimônias e rituais rosacruzes preconizados e autorizados pela Grande Loja.</p>';


    $mpdf->WriteHTML($html, 2);

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template5_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template6_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template7_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template8_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template9_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template10_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template11_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template12_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template13_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template14_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template15_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template16_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template17_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template18_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template19_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $mpdf->SetImportUse();
    $pagecount = $mpdf->SetSourceFile('teste4/template20_pronaos.pdf');
    $tplId = $mpdf->ImportPage($pagecount);
    $mpdf->SetPageTemplate($tplId);
// Do not add page until page template set, as it is inserted at the start of each page
    $mpdf->AddPage();

    $css = file_get_contents('teste4/estatuto.css');

    $mpdf->WriteHTML($css, 1);

    $html = '<br><br><br><br><br><br><br><br><br><br><br>';

    $html .= '<p class="normal"><b>Art 43º -</b> O ' . strtoupper($classificacao) . ' reger-se-á por este Estatuto e, supletivamente, dado o seu caráter Rosacruz,'
        . 'pelos regulamentos, normas, procedimentos e instruções emanados da AMORC GLP e de sua Diretoria e '
        . 'pelos documentos emitidos pela SUPREMA GRANDE LOJA, AMORC e por sua Diretoria, bem como pela Tradição Rosacruz e Martinista.</p>';

    $html .= '<p class="normal"><b>Art 44º -</b> A qualidade de Organismo Afiliado à AMORC GLP não desobriga o ' . strtoupper($classificacao) . ' de atender e '
        . 'cumprir todas as exigências legais e fiscais como pessoa jurídica, as quais devem ser mantidas em dia, conforme prazos estabelecidos pelas autoridades governamentais, sendo que os '
        . 'Membros-Dirigentes são os únicos responsáveis pelo correto cumprimento de tais obrigações junto às autoridades connstituídas, bem como pelas omissões neste sentido.</p>';

    $html .= '<p class="normal"><b>Art. 45º -</b> ';
    $html .= 'Os Membros-Dirigentes do(a) ' . strtoupper($classificacao) . ' emitirão manuais, regimentos, procedimentos e instruções que se fizerem necessários como adequação'
        . ' e cumprimento deste Estatuto, visando sempre a melhor forma de consolidação da cultura rosacruz na área de atuação da ' . strtoupper($classificacao) . '.</p>';

    $html .= '<p class="normal"><b>Art 46º -</b> São ratificados neste ato os propósitos místicos, iniciáticos e templários da AMORC-GLP e deste(a) ' . strtoupper($classificacao) . ', objetivando'
        . ' propiciar aos Fratres e Sorores um sistema educacional que possibilite o desenvolvimento pessoal e a busca da reintegração à essência divina, sob as garantias'
        . ' estabelecidas pela Constituição da República Federativa do Brasil, especialmente as previstas pelo art. 5º, incisos VI e VIII, pelo art. 150, VI "b", e as '
        . 'determinadas pelo art. 44,&sect; 1º, do Código Civil.</p>';

    $html .= '<p class="normal"><b>Art 47º -</b> O presente Estatuto foi aprovado em Assembléia Geral realizada em ' . $dataAssembleiaGeral . ', com a presença de ' . $numeroPresentes . ''
        . ' Membros-Estudantes em situação regular com suas obrigações e contribuições para com ' . $artigo . ' ' . strtoupper($classificacao) . ' e para com a AMORC-GLP, que o '
        . ' subscrevem, na cidade de ' . $cidade . ', Estado de ' . retornaEstado($uf) . ', Brasil.</p>';

    $html .= '<p class="normal"><b>Art 48º -</b> Revogam-se as disposições em contrário.</p>';

    $html .= '<p class="normal centro">' . $cidade . ', ' . date('d') . ' de ' . mesExtensoPortugues(date('m')) . ' de ' . date('Y') . '</p>';

    $html .= '<p class="normal">SEGUEM-SE:<br><br>Nome e assinatura dos Membros-Estudantes:</p>';

    $html .= '<table border="0" width="100%" class="normal">
            <tr>
                <td width="15%" class="normal"><b>Mestre</b></td>
                <td width="50%" class="normal">'.$mestreOA.'</td>
                <td width="35%" class="normal"><b>Ass:_____________________________</b></td>
            </tr>
            <tr>
                <td class="normal"><b>Secretário</b></td>
                <td class="normal">'.$secretarioOA.'</td>
                <td class="normal"><b>Ass:_____________________________</b></td>
            </tr>
            <tr>
                <td class="normal"><b>Guardião</b></td>
                <td class="normal">'.$guardiaoOA.'</td>
                <td class="normal"><b>Ass:_____________________________</b></td>
            </tr> 
            <tr>
                <td class="normal"><b>Advogado</b></td>
                <td class="normal">'.$nomeAdvogado.' <b>nº OAB:</b> '.$numeroOAB.'</td>
                <td class="normal"><b>Ass:_____________________________</b></td>
            </tr>
    </table>';

}//FIM IMPRESSÃO PRONAOS

$mpdf->WriteHTML($html);

$mpdf->useSubstitutions = true; // optional - just as an example
//$mpdf->SetHeader($url . "\n\n" . 'Page {PAGENO}');  // optional - just as an example
//$stylesheet = file_get_contents('teste4/bfREcieV/bfREcieV.css'); // external css
//$mpdf->WriteHTML($stylesheet,1);
//$mpdf->WriteHTML($html);
//$mpdf->AddPage();
$mpdf->SetDisplayMode('fullpage');
//$mpdf->debug = true;
$mpdf->allow_charset_conversion = true;
$mpdf->charset_in='UTF-8';

$mpdf->Output("estatuto.pdf","D");

 
?>