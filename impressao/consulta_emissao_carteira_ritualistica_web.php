<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    session_start();
    $codigoAfiliacao=isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:null;
    $ideCompanheiro=isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:'N';
    $nome=isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
    $dataAdmissao =isset($_REQUEST['dataAdmissao'])?$_REQUEST['dataAdmissao']:null;

    $ocultar_json=1;

    include '../js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
    include_once '../lib/functions.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayObrigacoes']);

    $arrIniciacoes = $obj['result'][0]['fields']['fArrayObrigacoes'];

    include_once '../model/organismoClass.php';
    $o = new organismo();

    include_once '../model/usuarioClass.php';
    $u = new Usuario();

    $resultado = $u->buscaUsuario($_SESSION['seqCadast']);
    $nomeUsuario="";
    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    $nomeUsuario = $vetor['nomeUsuario'];
            }
    }
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    table , body{
    /* font-family: Helvetica, Arial, sans-serif; */
    font-family: Calibri;
    font-size: 12px!important;
    color: #000;
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 20px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 154px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
    .oculto {
    display: none;
    }

    </style>


    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <br class="oculto">

    <center>
    <?php 
    if(count($arrIniciacoes)>0)
    { 
            $g=1;
    ?>
    <div id="cabecalho">
            <center><img class="solado" src="../img/header.png"></center>
    </div>
    <div id="informacoesMembro" style="text-align: left;">
    Nome: <?php echo $nome;?><br>
    Código de Afiliação: <?php echo $codigoAfiliacao;?><br>
    Data de Admissão: <?php echo $dataAdmissao;?><br>
    </div>
    <h2>Registro de Iniciações</h2>
    <table border="1">
            <thead>
            <tr>
                    <th>Grau</th>
                    <th>Nome do Organismo</th>
                    <th>Data</th>
                    <th>Ano R+C</th>
                    <th>Assinatura do Mestre</th>
            </tr>
            </thead>
            <tbody>
                    <?php 
                    foreach($arrIniciacoes as $vetor)
                    { 
                            if(comparaIniciacoesDeGrau($vetor['fields']['fSeqTipoObriga']))
                            {
                            ?>
                    <tr class="">
                            <td id="oa">
                                    <center>
                                    <?php 
                                            $ideCompanheiro='N';
                                            $seqTipoObriga=$vetor['fields']['fSeqTipoObriga'];
                                            include '../js/ajax/retornaTipoObrigacaoRitualistica.php';
                                            $obj2 = json_decode(json_encode($return),true);
                                            //echo "<pre>".print_r($obj2['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga']);
                                            echo $obj2['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga'];
                                    ?>
                                    </center>
                            </td>
                            <td id="oa">
                            <center>
                            <?php 
                            $nomeOa="";

                            if($vetor['fields']['fSigOrgafi']!="")
                            {
                                    $o = new organismo();
                                    $resultado2 = $o->listaOrganismo(null,trim($vetor['fields']['fSigOrgafi']));
                                    if($resultado2)
                                    {
                                            //echo count($resultado2);
                                            foreach($resultado2 as $vetor2)
                                            {
                                                    $nomeOa = organismoAfiliadoNomeCompleto($vetor2['classificacaoOrganismoAfiliado'],$vetor2['tipoOrganismoAfiliado'],$vetor2['nomeOrganismoAfiliado'],$vetor2['siglaOrganismoAfiliado']);
                                            }
                                    }
                            }
                            if($nomeOa!="")
                            {
                                    echo $nomeOa;
                            }else{
                                    echo "OA precisa ser cadastrado";	
                            }
                                    ?></center></td>
                            <td id="data"><center><?php echo substr($vetor['fields']['fDatObriga'],8,2)."/".substr($vetor['fields']['fDatObriga'],5,2)."/".substr($vetor['fields']['fDatObriga'],0,4);?></center></td>
                            <td id="ano"><center><?php echo anoRC(substr($vetor['fields']['fDatObriga'],0,4),substr($vetor['fields']['fDatObriga'],5,2),substr($vetor['fields']['fDatObriga'],8,2));?></center></td>
                            <td id="oa"><center>VALIDADO PELO SISTEMA</center></td>
                    </tr>
                    <?php
                                    $g++; 
                            }	
                    }?>
            </tbody>
    </table>
    <br><br>
    Impresso em <?php echo date("d/m/Y");?> às <?php echo date("H:i:s");?> - Documento de uso na AMORC<br>
    <?php echo $nomeUsuario;?> 
    <?php }else{?>
            <b><font color="red">Erro ao trazer as iniciações!</font></b>
    <?php }?>
    </center>
<?php
}
?>