<?php 
//Carteirinha calibrada apenas para a impressora da Carol
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $codigoAfiliacao=isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:null;
    $ideCompanheiro=isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:'N';

    $ocultar_json=1;

    include '../js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
    include_once '../lib/functions.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayObrigacoes']);

    $arrIniciacoes = $obj['result'][0]['fields']['fArrayObrigacoes'];

    include_once '../model/organismoClass.php';
    $o = new organismo();
    ?>
    <title>SOA - Sistema de Organismos Afiliados</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
    body{
    /* font-family: Helvetica, Arial, sans-serif; */
    font-family: Calibri;
    font-size: 12px!important;
    color: rgb(85, 85, 85);
    border-collapse:collapse;
    border-color:#DCDCDC;
    max-width: 100%;
    border-spacing: 0;
    }
    table{
        /* font-family: Helvetica, Arial, sans-serif; */
        margin-top: 0px;
        font-family: Calibri;
        font-size: 12px!important;
        color: rgb(85, 85, 85);
        border-collapse:collapse;
        border-color:#DCDCDC;
        max-width: 100%;
        border-spacing: 0;
    }
    .cinza{
            background-color:#F5F5F5;
    }
    .centro{
            text-align:center;
    }

    /* Table Base */

    .table { 
      width: 100%;
      margin-bottom: 20px;
    }

    .table th,
    .table td {
      font-weight: normal;
      font-size: 12px;
      padding: 8px 15px;
      line-height: 22px;
      text-align: left;
      vertical-align: middle;
      border-top: 1px solid #dddddd;
    }
    .table thead th {
      vertical-align: bottom;
      font-weight:bold;
    }      
    .table .t-small {
      width: 5%;
    }
    .table .t-medium {
      width: 10%;
    }
    .table .t-nome {
      width: 30%;
    }
    .table .t-status {
      font-weight: bold;
    }
    .table .t-active {
      color: #46a546;
    }
    .table .t-inactive {
      color: #e00300;
    }
    .table .t-draft {
      color: #f89406;
    }
    .table .t-scheduled {
      color: #049cdb;
    }

    /* Small Sizes */
    @media (max-width: 480px) { 
      .table-action thead {
        display: none;
      }
      .table-action tr {
        border-bottom: 1px solid #dddddd;
      }
      .table-action td {
        border: 0;
      }
      .table-action td:not(:first-child) {
        display: block;
      }
    }

    #cabecalho {height: 114px}
    #oa {width: 260px; height: 19px; padding-left: 14px}
    #data {width: 131px; height: 19px}
    #ano {width: 61px; height: 19px}
    .solado {}
    </style>
    <style media="print">
    .oculto {
    display: none;
    }

    </style>

    <div id="cabecalho">
            <center class="oculto"><img class="solado oculto" src="../img/solado.png"></center>
            <br class="oculto">
            <center class="oculto"><p class="oculto">- Coloque a Carteirinha com padrão oficial da AMORC (envelope nº10).</p></center>
            <center class="oculto"><p class="oculto">- Layout deve ser "Paisagem".</p></center>
            <center class="oculto"><p class="oculto">- A impressora deve ser de um modelo que não dobre a Carteirinha ao ser impressa.</p></center>
            <center class="oculto"><p class="oculto">- Na opção de impressão "Margens", selecione "Nenhum".</p></center>
    </div>
    <br class="oculto">
    <br class="oculto">
    <br class="oculto">
    <br class="oculto">
    <br class="oculto">
    <br class="oculto">
    <div class="centro oculto"><a href="#" onclick="window.print();" class="botao"><img src="../img/impressora.png"></a></div>
    <br class="oculto">

    <center>
    <?php 
    if(count($arrIniciacoes)>0)
    { 
    ?>
    <table border="0" style="margin-top: 9px!important;height: 100%!important;">
            <thead>
            <tr>
                    <th class="oculto">Nome da Loja</th>
                    <th class="oculto">Data</th>
                    <th class="oculto">Ano R+C</th>
                    <th class="oculto">Assinatura do Mestre</th>
            </tr>
            </thead>
            <tbody>
                    <?php 
                    foreach($arrIniciacoes as $vetor)
                    { 
                            if(comparaIniciacoesDeGrau($vetor['fields']['fSeqTipoObriga']))
                            {
                                    $nomeOa="";
                                    if($vetor['fields']['fSigOrgafi']!="")
                                    {
                                            $resultado = $o->listaOrganismo(null,$vetor['fields']['fSigOrgafi']);	
                                            if($resultado)
                                            {
                                                    foreach($resultado as $vetor2)
                                                    {
                                                            $nomeOa = organismoAfiliadoNomeCompleto($vetor2['classificacaoOrganismoAfiliado'],$vetor2['tipoOrganismoAfiliado'],$vetor2['nomeOrganismoAfiliado'],$vetor2['siglaOrganismoAfiliado']);
                                                    }
                                            }
                                    }
                            ?>
                    <tr style="height: 20px">
                            <td id="oa"><?php echo $nomeOa;?></td>
                            <td id="data"><center><?php echo substr($vetor['fields']['fDatObriga'],8,2)."/".substr($vetor['fields']['fDatObriga'],5,2)."/".substr($vetor['fields']['fDatObriga'],0,4);?></center></td>
                            <td id="ano"><center><?php echo anoRC(substr($vetor['fields']['fDatObriga'],0,4),substr($vetor['fields']['fDatObriga'],5,2),substr($vetor['fields']['fDatObriga'],8,2));?></center></td>
                            <td id="oa"></td>
                    </tr>
                    <?php 
                            }		
                    }?>
            </tbody>
    </table>
    <?php }else{?>
            <b><font color="red">Erro ao trazer as iniciações!</font></b>
    <?php }?>
    </center>
<?php
}
?>