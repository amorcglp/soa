<!-- Sweet Alert -->
<link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- Sweet alert -->
<script src="../js/plugins/sweetalert/sweetalert.min.js"></script>
<style>
    .btn-danger{
        background-color: #B22222 !important;
    }
</style>
<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

function UrlAtual(){
    $dominio= $_SERVER['HTTP_HOST'];
    $url = $dominio;
    return $url;
}

if($tokenLiberado)
{
    if(UrlAtual()=="treinamento.amorc.org.br") {
        $ambienteTreinamento=true;
    }
    $imprime=true;
    //Fazer requisição do webservice
    ini_set("soap.wsdl_cache_enabled", "0"); 
    date_default_timezone_set('America/Sao_Paulo');
    include_once '../lib/functions.php';
    include_once '../model/boletoContribuicaoGlpClass.php';
    $bcg = new boletoContribuicaoGlp();

    $numeroBoleto=0;
    $codigoContaContribuicao=0;
    //if($bcg->verificaToken($_REQUEST['token'])==true)
    //{
            $resultado = $bcg->listaBoleto($_REQUEST['token']);
            if($_REQUEST['token']==$_SESSION['tokenBoleto'])
            {
                if(UrlAtual()=="treinamento.amorc.org.br") {
                    $client = new SoapClient("http://192.1.1.108:8085/wsdl/IAmorcSSDM");
                }else{
                    $client = new SoapClient("http://192.1.1.135:8085/wsdl/IAmorcSSDM");
                }
                    $client->soap_defencoding = 'UTF-8';
                    $client->decode_utf8 = true;
                    $DatHoraProces = date('Y-m-d\TH:i:s');
                    $NumProces = getRequisitionValidationCode($_REQUEST['codigoAfiliacao'],false);

                    $response1 		= $client->PreviaBoletoContribuicao(
                                                                    $_REQUEST['codigoAfiliacao'],
                                                                    $_REQUEST['tipoMembro'],
                                                                    $_REQUEST['quantidade'],
                                                                    $DatHoraProces,$NumProces);
                    
                    $nome=$_REQUEST['nome'];
                    $codigoAfiliacao=$_REQUEST['codigoAfiliacao'];
                    $tipoMembro=$_REQUEST['tipoMembro'];
                    $endereco=$_REQUEST['endereco'];
                    $numero=$_REQUEST['numero'];
                    $cidade=$_REQUEST['cidade'];
                    $estado=$_REQUEST['estado'];
                    $cep=$_REQUEST['cep'];
                    $valorBoleto=$_GET['valorBoleto'];
                    $codigoContaContribuicao = $response1->CodContaContribuicao;
                    $pagueNoProximoDiaUtil="";
                    //echo "codigo Conta Contribuição=>".$codigoContaContribuicao;
                    //$valorBoleto = (float) $response1->ValContribuicao+$response1->ValTaxaAfiliacao;
                    
                    $valorUnitario = $response1->ValContribuicao;
                    
            
                    /**
                     * Webservice SOAP
                     */
                if(UrlAtual()=="treinamento.amorc.org.br") {
                    $client = new SoapClient("http://192.1.1.108:8085/wsdl/IAmorcSSDM");
                }else{
                    $client = new SoapClient("http://192.1.1.135:8085/wsdl/IAmorcSSDM");
                }
                    $client->soap_defencoding = 'UTF-8';
                    $client->decode_utf8 = true;
                    $DatHoraProces = date('Y-m-d\TH:i:s');
                    $NumProces = getRequisitionValidationCode($_REQUEST['codigoAfiliacao'],false);

                    /**
                     * Gerar data do vencimento
                     */
                    $dataVencimento = SomaDiasUteis(date("d/m/Y"),10);
                    $dataVencimentoWS = substr($dataVencimento,6,4)."-".substr($dataVencimento,3,2)."-".substr($dataVencimento,0,2)."T00:00:00";
                    //echo $dataVencimento;

                    /*
                    echo "codigoAfiliacao: " . $codigoAfiliacao . "<br />";
                    echo "tipoMembro: " . $tipoMembro . "<br />";
                    echo "seqCadast: " . $seqCadast . "<br />";
                    echo "codigoVeiculo: " . $codigoVeiculo . "<br />";
                    echo "codigoContaContribuicao: " . $codigoContaContribuicao . "<br />";
                    echo "valorBoleto: " . $valorBoleto . "<br />";
                    echo "dataVencimentoWS: " . $dataVencimentoWS . "<br />";
                    echo "DatHoraProces: " . $DatHoraProces . "<br />";
                    echo "NumProces: " . $NumProces . "<br />";
                    */
                    
                    //Pesquisar número do boleto se for não pesquisar no webservice
                    $boletoReemitido = false;
                    if(isset($_REQUEST['numeroBoleto']))
                    {
                            $boletoReemitido = true;
                            $numeroBoleto = $_REQUEST['numeroBoleto'];
                        }
                    }else{
                        echo "Não foi possível gerar o boleto";
                    }
                    
                        if(!$boletoReemitido)
                        {    
                            $response 		= $client->GerarBoletoContribuicao(
                                                                            $_REQUEST['codigoAfiliacao'],
                                                                            $_REQUEST['tipoMembro'],
                                                                            $_REQUEST['seqCadast'],
                                                                            0,
                                                                            $codigoContaContribuicao,
                                                                            $valorBoleto,
                                                                            $dataVencimentoWS,
                                                                            $DatHoraProces,$NumProces);
                            
                            /*                                             
                            echo $_REQUEST['codigoAfiliacao']."<br>";
                            echo $_REQUEST['tipoMembro']."<br>";
                            echo $_REQUEST['seqCadast']."<br>";
                            echo $codigoContaContribuicao."<br>";
                            echo $_REQUEST['valorBoleto']."<br>";
                            echo $dataVencimentoWS."<br>";
                            echo $DatHoraProces."<br>";
                            echo $NumProces."<br>";
                            
                            echo "<pre>";print_r($response);*/
                            $dados = $response;
                            $numeroBoleto = $dados->NumBoleto;

                            //Registrar Boleto no BB
                            //echo "<<<<<Emitiu o boleto>>>>>";
                            $ambienteTreinamento=false;
                            if(UrlAtual()=="treinamento.amorc.org.br") {

                                if ($numeroBoleto != 0 && $numeroBoleto != "") {
                                    //echo "teste braz";
                                    $responseRegistrarBoleto = $client->RegistrarBoleto(
                                        $numeroBoleto,
                                        $DatHoraProces, $NumProces);
                                    //echo "<br>posteste braz";
                                    //echo "<pre>";print_r($responseRegistrarBoleto);exit();
                                    $numeroBoletoRegistrado = $responseRegistrarBoleto->NumBoleto;
                                    if ($numeroBoletoRegistrado != $numeroBoleto) {
                                        $pagueNoProximoDiaUtil = "Sr. cliente, por favor pague o boleto no próximo dia útil, após a geração do boleto.";
                                    }
                                }
                                $ambienteTreinamento=true;
                            }
                            if($numeroBoleto==0)
                            {
                                echo "Não foi possível gerar o boleto";
                            }    
                            //echo "numeroBoleto".$numeroBoleto;
                            if($dados->Reemissao=="S")
                            {    
                                $imprime=false;
                            ?>
                            
                                <script>
                                    window.onload = function () {
                                        swal({
                                            title: "ATENÇÃO!",
                                            text: "Este boleto foi reemitido pois o último boleto gerado com o mesmo valor consta em aberto. Se deseja realizar mais de uma contribuição selecione outra forma de contribuição!",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonClass: "btn-primary",
                                            cancelButtonClass: "btn-danger",
                                            cancelButtonText: "Cancelar",
                                            cancelButtonColor: '#B22222',
                                            confirmButtonText: "Reemitir Boleto",
                                            confirmButtonColor: '#0000FF',
                                            
                                            closeOnConfirm: false
                                          },
                                          function(isConfirm){
                                              if (isConfirm) {
                                                    location.href= 'gerarBoleto.php?token=<?php echo $_REQUEST['token'];?>'
                                                    +'&codigoAfiliacao=<?php echo $_REQUEST['codigoAfiliacao'];?>'
                                                    +'&tipoMembro=<?php echo $_REQUEST['tipoMembro'];?>'
                                                    +'&seqCadast=<?php echo $_REQUEST['seqCadast'];?>'
                                                    +'&quantidade=<?php echo $_REQUEST['seqCadast'];?>'
                                                    +'&nome=<?php echo $_REQUEST['nome'];?>'
                                                    +'&endereco=<?php echo $_REQUEST['endereco'];?>'
                                                    +'&numero=<?php echo $_REQUEST['numero'];?>'
                                                    +'&cidade=<?php echo $_REQUEST['cidade'];?>'
                                                    +'&estado=<?php echo $_REQUEST['estado'];?>'
                                                    +'&cep=<?php echo $_REQUEST['cep'];?>'
                                                    +'&valorBoleto=<?php echo $valorBoleto;?>'
                                                    +'&numeroBoleto=<?php echo $dados->NumBoleto;?>';
                                             } else {
                                                 window.close();
                                             }
                                          });
                                    }
                                </script>
                            <?php 
                            }
                        }

                        if($imprime)    
                        {  
                        
                            if($numeroBoleto!=0&&$numeroBoleto!="")
                            {  

                                    $bcg1 = new boletoContribuicaoGlp();

                                    $bcg1->setToken($_REQUEST['token']);
                                    $bcg1->setCodigoAfiliacao($_REQUEST['codigoAfiliacao']);
                                    $bcg1->setTipoMembro($_REQUEST['tipoMembro']);
                                    $bcg1->setSeqCadast($_REQUEST['seqCadast']);
                                    $bcg1->setCodigoVeiculo(0);
                                    $bcg1->setCodigoContaContribuicao($response1->CodContaContribuicao);
                                    $bcg1->setValorBoleto($valorBoleto);
                                    $bcg1->setQuantidade($_REQUEST['quantidade']);
                                    $bcg1->setNome($_REQUEST['nome']);
                                    $bcg1->setEndereco($_REQUEST['endereco']);
                                    $bcg1->setNumero($_REQUEST['numero']);
                                    $bcg1->setCidade($_REQUEST['cidade']);
                                    $bcg1->setEstado($_REQUEST['estado']);
                                    $bcg1->setCep($_REQUEST['cep']);
                                    $bcg1->setNumeroBoleto($numeroBoleto);
                                    $bcg1->setUsuario($_SESSION['seqCadast']);
                                    //echo "<pre>";print_r($bcg1);
                                    $bcg1->cadastroBoletoContribuicaoGlp();



                            // +----------------------------------------------------------------------+
                            // | BoletoPhp - Versão Beta                                              |
                            // +----------------------------------------------------------------------+
                            // | Este arquivo está disponível sob a Licença GPL disponível pela Web   |
                            // | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
                            // | Você deve ter recebido uma cópia da GNU Public License junto com     |
                            // | esse pacote; se não, escreva para:                                   |
                            // |                                                                      |
                            // | Free Software Foundation, Inc.                                       |
                            // | 59 Temple Place - Suite 330                                          |
                            // | Boston, MA 02111-1307, USA.                                          |
                            // +----------------------------------------------------------------------+
                            // +----------------------------------------------------------------------+
                            // | Originado do Projeto BBBoletoFree que tiveram colaborações de Daniel |
                            // | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
                            // | PHPBoleto de João Prado Maia e Pablo Martins F. Costa				        |
                            // | 														                                   			  |
                            // | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
                            // | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
                            // +----------------------------------------------------------------------+
                            // +--------------------------------------------------------------------------------------------------------+
                            // | Equipe Coordenação Projeto BoletoPhp: <boletophp@boletophp.com.br>              		             				|
                            // | Desenvolvimento Boleto Banco do Brasil: Daniel William Schultz / Leandro Maniezo / Rogério Dias Pereira|
                            // +--------------------------------------------------------------------------------------------------------+
                            // ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
                            // Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//
                            // DADOS DO BOLETO PARA O SEU CLIENTE
                            $dias_de_prazo_para_pagamento = 5;
                            $taxa_boleto = 0;//2.95;
                            $data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
                            $valor_cobrado = $valorBoleto; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
                            $valor_cobrado = str_replace(",", ".",$valor_cobrado);
                            $valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');
                            $dadosboleto["nosso_numero"] = $numeroBoleto;
                            $dadosboleto["numero_documento"] = $numeroBoleto;	// Num do pedido ou do documento
                            $dadosboleto["data_vencimento"] = $dataVencimento; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
                            $dadosboleto["data_documento"] = date("d/m/Y"); // Data de emissão do Boleto
                            $dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
                            $dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
                            // DADOS DO SEU CLIENTE
                            $dadosboleto["sacado"] = $nome; //." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$codigoAfiliacao;
                            /*
                            switch ($tipoMembro)
                            {
                                    case 1:
                                            $dadosboleto["sacado"] .= "(R+C)";
                                            break;
                                    case 2:
                                            $dadosboleto["sacado"] .= "(OGG)";
                                            break;
                                    case 4:
                                            $dadosboleto["sacado"] .= "(Não Membro)";
                                            break;
                                    case 5:
                                            $dadosboleto["sacado"] .= "(Cliente)";
                                            break;
                                    case 6:
                                            $dadosboleto["sacado"] .= "(TOM)";
                                            break;
                                    case 7:
                                            $dadosboleto["sacado"] .= "(Assinante Rosacruz)";
                                            break;
                                    case 8:
                                            $dadosboleto["sacado"] .= "(AMA)";
                                            break;
                                    case 9:
                                            $dadosboleto["sacado"] .= "(Assinante Amorc Cultural)";
                                            break;
                                    case 13:
                                            $dadosboleto["sacado"] .= "(Assinante ORCJ)";
                                            break;
                                    default:
                                            $dadosboleto["sacado"] .= "(R+C)";
                                            break;
                            }
                            */
                            $dadosboleto["endereco1"] = $endereco.", ".$numero;
                            $dadosboleto["endereco2"] = $cidade." - ".$estado." -  CEP: ".$cep;
                            // INFORMACOES PARA O CLIENTE
                            $dadosboleto["demonstrativo1"] = $pagueNoProximoDiaUtil;
                            $dadosboleto["demonstrativo2"] = "";//.number_format($taxa_boleto, 2, ',', '');
                            $dadosboleto["demonstrativo3"] = "";
                            // INSTRUÇÕES PARA O CAIXA
                            $dadosboleto["instrucoes1"] = "";
                            $dadosboleto["instrucoes2"] = "";
                            $dadosboleto["instrucoes3"] = "";
                            $dadosboleto["instrucoes4"] = "";
                            // DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
                            $dadosboleto["quantidade"] = $_REQUEST['quantidade'];
                            $dadosboleto["valor_unitario"] = $valorUnitario;
                            $dadosboleto["aceite"] = "N";		
                            $dadosboleto["especie"] = "R$";
                            $dadosboleto["especie_doc"] = "DM";
                            // ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
                            // DADOS DA SUA CONTA - BANCO DO BRASIL
                            $dadosboleto["agencia"] = "3041"; // Num da agencia, sem digito
                            $dadosboleto["conta"] = "111945"; 	// Num da conta, sem digito
                            // DADOS PERSONALIZADOS - BANCO DO BRASIL
                            $dadosboleto["convenio"] = "2805763";  // Num do convênio - REGRA: 6 ou 7 ou 8 dígitos
                            $dadosboleto["contrato"] = "000/925637"; // Num do seu contrato
                            $dadosboleto["carteira"] = "17";
                            $dadosboleto["variacao_carteira"] = "";  // Variação da Carteira, com traço (opcional) => ORDEM: - 027
                            // TIPO DO BOLETO
                            $dadosboleto["formatacao_convenio"] = "7"; // REGRA: 8 p/ Convênio c/ 8 dígitos, 7 p/ Convênio c/ 7 dígitos, ou 6 se Convênio c/ 6 dígitos
                            $dadosboleto["formatacao_nosso_numero"] = "2"; // REGRA: Usado apenas p/ Convênio c/ 6 dígitos: informe 1 se for NossoNúmero de até 5 dígitos ou 2 para opção de até 17 dígitos
                            /*
                            #################################################
                            DESENVOLVIDO PARA CARTEIRA 18
                            - Carteira 18 com Convenio de 8 digitos
                              Nosso número: pode ser até 9 dígitos
                            - Carteira 18 com Convenio de 7 digitos
                              Nosso número: pode ser até 10 dígitos
                            - Carteira 18 com Convenio de 6 digitos
                              Nosso número:
                              de 1 a 99999 para opção de até 5 dígitos
                              de 1 a 99999999999999999 para opção de até 17 dígitos
                            #################################################
                            */
                            // SEUS DADOS
                            $dadosboleto["identificacao"] = "Antiga e M&iacute;stica Ordem Rosacruz - AMORC - GLP";
                            $dadosboleto["cpf_cnpj"] = "76.565.720./0001-66";
                            $dadosboleto["endereco"] = "Rua Nicar&aacute;gua, 2620 - Bacacheri";
                            $dadosboleto["cidade_uf"] = "Curitiba / PR";
                            $dadosboleto["cedente"] = "Antiga e M&iacute;stica Ordem Rosacruz - AMORC - GLP";
                            // NÃO ALTERAR!
                            include("../lib/boleto/include/funcoes_bb.php"); 
                            include("../lib/boleto/include/layout_bb.php");

                            }

                            //}else{
                            //        echo "Não é possível gerar o boleto";
                            //}
                            ?>
                            <script language=javascript>    
                                 document.onkeydown = function () { 
                                       switch (event.keyCode) {
                                         case 116 :  
                                            event.returnValue = false;
                                            event.keyCode = 0;           
                                            return false;             
                                          case 82 : 
                                            if (event.ctrlKey) {  
                                               event.returnValue = false;
                                              event.keyCode = 0;             
                                              return false;
                                       }
                                     }
                                 } 
                            </script>
                        <?php
                        
                    }
            
}
?>