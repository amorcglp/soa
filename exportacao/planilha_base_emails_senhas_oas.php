<meta charset="utf-8">
<?php

require_once '../model/organismoClass.php';
$o = new organismo();
$resultado = $o->listaOrganismo();

ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 0);

$arquivo = 'planilhaBaseEmailsSenhasOas.xls';

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

require_once '../lib/functions.php';
				
?>
<center>
<table>
	<tr>
		<th style="width: 300px">E-mail</th>
        <th>Senha</th>
	</tr>
    <?php foreach ($resultado as $v)
     {
     ?>
	<tr>
		<td><?php echo $v['emailOrganismoAfiliado'];?></td>
        <td><?php echo $v['senhaEmailOrganismoAfiliado'];?></td>
	</tr>
    <?php }?>
</table>
</center>
<?php 
exit;
?>