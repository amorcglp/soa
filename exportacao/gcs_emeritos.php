<meta charset="utf-8">
<?php
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 0);

$arquivo = 'relatorio_gcs_emeritos.xls';

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

require_once '../webservice/wsInovad.php';
require_once '../lib/functions.php';
				
?>
<center>
<table>
	<tr>
		<th><img src="http://soa.amorc.org.br/img/solado.png"></th>
	</tr>
	<tr>
		
	</tr>
	<tr>
		
	</tr>
	<tr>
		
	</tr>
	<tr>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<th>&nbsp;</th>
	</tr>
	<tr>

	</tr>
	<tr>
		<th>&nbsp;</th>
	</tr>
</table>
<table class="table table-action">
	<thead>
    <tr>
        <th colspan="8" style="font-family:Calibri;color:#000;text-align:center;white-space: nowrap;"><b><font size="3">RELAÇÃO DOS GRANDES CONSELHEIROS EMÉRITOS</font></b></th>
    </tr>
	<tr style="height:50px">
	<!--<th rowspan="100"><font color="white">**</font></th>-->
	<th style="border:1px solid black;background-color:#00688B;color:#fff">QTD.</th>
	<th style="border:1px solid black;background-color:#00688B;color:#fff;width: 60px!important;">INSCRIÇÃO</th>
	<th style="border:1px solid black;background-color:#00688B;color:#fff">REG.</th>	
	<th style="border:1px solid black;background-color:#00688B;color:#fff">GESTÃO</th>
        <th style="border:1px solid black;background-color:#00688B;color:#fff">TRANSIÇÃO</th>
        <th style="border:1px solid black;background-color:#00688B;color:#fff">ANIVERSÁRIO</th>
	<th style="border:1px solid black;background-color:#00688B;color:#fff">NOME</th>
	<th style="border:1px solid black;background-color:#00688B;color:#fff">ENDEREÇO</th>
	</tr>
	</thead>
	<tbody>
	<?php 
            $y=0;
            $u=0;
            $p=1;
            $temRegistro=true;
            $x=0;
            $z=0;
            $aux=0;
            $arrFinal=array();
            while($temRegistro==true)
            {    
                $arr = array(0 => 152);
                $vars = array('order_by' => 'sig_uf','seq_funcoes[]' => $arr, 'page' => $p);//Pegar Ex- grandes conselheiros
                $resposta = json_decode(json_encode(restAmorc("membros/oficiais",$vars)),true);
                $obj = json_decode(json_encode($resposta),true);

                $newArray = array();

                foreach ($obj['data'] as $k => $value){
                    $newArray[$k] = array(
                        'sig_orgafi' => $value['funcoes'][0]['oa']['sig_orgafi'],
                        'm'   => $value
                    );
                }


                $obj = arraySort($newArray, 'sig_orgafi');


                //transformar no velho array

                $oldArray = array();

                foreach ($obj as $k => $value){
                    $oldArray[$k] = $value['m'];
                }

                $obj = $oldArray;

                //echo "<pre>";print_r($obj);
                //exit();

                if(isset($obj[7]))
                {
                    //echo "aqui";
                    foreach($obj as $m)
                    {
                        //echo "aqui2";
                            //echo "<pre>";print_r($m);

                            $arrFuncoes = $m['funcoes'];
                            $seqCadast = $m['seq_cadast'];
                            $b = 0;
                            foreach ($arrFuncoes as $f) {
                                //echo "aqui3";
                                $seqFuncao = $f['funcao']['seq_funcao'];

                                if ($seqFuncao == 152) {
                                    $array = [0 => $seqCadast];
                                    $teste = '';
                                    foreach ($array as $key => $value) {
                                        if ($teste != '') {
                                            $teste = $teste . ', ' . $value;
                                        } else {
                                            $teste = $value;
                                        }
                                    }

                                    //Pesquisar Transição
                                    $vars3 = array('seq_cadast' => $teste, 'limit' => 1);//Pegar Ex- grandes conselheiros
                                    $resposta3 = json_decode(json_encode(restAmorc("membros", $vars3)), true);
                                    $obj3 = json_decode(json_encode($resposta3), true);

                                    //echo "<pre>";print_r($obj3);exit();
                                    $transicao = false;
                                    foreach ($obj3['data'] as $mem) {
                                        $situacaoMembro = trim($mem['rosacruz']['ide_tipo_situac_membro']);
                                        $codDesligamento = substr(trim($mem['rosacruz']['cod_deslig']), 0, 1);

                                        if ($situacaoMembro == 7 && $codDesligamento == 7) {
                                            $transicao = true;
                                        }
                                    }

                                    $siglaOA = $f['oa']['sig_orgafi'];
                                    $dtEntrada = $f['dat_entrad'];
                                    $dtEntrada = substr($dtEntrada, 8, 2) . "/" . substr($dtEntrada, 5, 2) . "/" . substr($dtEntrada, 0, 4);
                                    if($f['dat_saida']!="") {
                                        $dtSaida = $f['dat_saida'];
                                    }else{
                                        $dtSaida = $f['dat_termin_mandat'];
                                    }
                                    $dtSaida = substr($dtSaida, 8, 2) . "/" . substr($dtSaida, 5, 2) . "/" . substr($dtSaida, 0, 4);
                                    $dtNascimento = $m['dat_nascim'];
                                    $dtNascimento = substr($dtNascimento, 8, 2) . "/" . substr($dtNascimento, 5, 2) . "/" . substr($dtNascimento, 0, 4);

                                    if ($aux != $m['seq_cadast']) {

                                        $arrFinal[$u]['qtd'] = $y+1;
                                        $arrFinal[$u]['inscricao'] = trim($m['cod_rosacr']);
                                        $arrFinal[$u]['reg'] = substr($siglaOA, 0, 3);
                                        if(trim($dtSaida)=="//") {
                                            $arrFinal[$u]['gestao'] = "Entrou ".$dtEntrada;
                                        }else{
                                            $arrFinal[$u]['gestao'] = $dtEntrada . " à " . $dtSaida;
                                        }
                                        $arrFinal[$u]['transicao'] = ($transicao) ? "&#10014; SIM":"NÃO";
                                        if($transicao)
                                        {
                                            $x++;
                                        }else{
                                            $z++;
                                        }
                                        $arrFinal[$u]['aniversario'] = $dtNascimento;
                                        $arrFinal[$u]['nome'] = $m['nom_client'];
                                        $arrFinal[$u]['enderecolinha1'] .= $m['des_lograd']." ".$m['num_endere']."-".$m['nom_bairro'];
                                        $arrFinal[$u]['enderecolinha2'] .= $m['des_comple_lograd'];
                                        if ($m['ide_endere_corres'] == 1) {
                                            $arrFinal[$u]['enderecolinha3'] .= trim($m['cod_cep']);
                                        }else{
                                            $arrFinal[$u]['enderecolinha3'] .= $m['num_caixa_postal'] . "<br>" . $m['cod_cep_caixa_postal'];
                                        }
                                        $arrFinal[$u]['enderecolinha3'] .= " - ".$m['nom_locali']." - ".$m['sig_uf'];
                                        $arrFinal[$u]['enderecolinha4'] .= "FONE:";
                                        $arrFinal[$u]['enderecolinha4'] .= $m['num_telefo'];

                                        if (trim($m['num_telefo_fixo']) != "" || trim($m['num_celula']) != "" || trim($m['num_outro_telefo']) != "") {
                                            $arrFinal[$u]['enderecolinha4'] .= "/";
                                        }
                                        $arrFinal[$u]['enderecolinha4'] .= $m['num_telefo_fixo'];
                                        if (trim($m['num_celula'] || trim($m['num_outro_telefo']) != "") != "") {
                                            $arrFinal[$u]['enderecolinha4'] .= "/";
                                        }
                                        $arrFinal[$u]['enderecolinha4'] .= $m['num_celula'];
                                        if (trim($m['num_outro_telefo']) != "") {
                                            $arrFinal[$u]['enderecolinha4'] .= "/";
                                        }
                                        $arrFinal[$u]['enderecolinha4'] .= $m['num_outro_telefo'];


                                        $y++;
                                        $u++;
                                    }
                                    $aux = $m['seq_cadast'];
                                }

                            }

                    }
                }
                
                
                //Continuar chamando paginação
                $p++;
                $arr2 = array(0 => 152);
                $vars2 = array('order_by' => 'sig_uf','seq_funcoes[]' => $arr2, 'page' => $p);//Pegar Ex- grandes conselheiros
                $resposta2 = json_decode(json_encode(restAmorc("membros/oficiais",$vars2)),true);
                $obj2 = json_decode(json_encode($resposta2),true);
                //echo "<pre>";print_r($obj);exit();
                
                if(isset($obj2['data'][0]))
                {
                    $temRegistro=true;
                }else{
                    $temRegistro=false;
                }
            }
            //Imprimir array final ordenado
            $arrFinal = arraySort($arrFinal, 'reg');

            $e=0;
            foreach($arrFinal as $f)
            {
                ?>
                <tr style="color:#000;">
                    <td align="center"
                        style="border:1px solid black;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo  $e+1;?></td>
                    <td align="center"
                        style="border:1px solid black;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo  $f['inscricao'];?></td>
                    <td align="center"
                        style="border:1px solid black;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo  $f['reg'];?></td>
                    <td align="center"
                        style="border:1px solid black;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo  $f['gestao'];?></td>
                    <td align="center"
                        style="border:1px solid black;font-size: 20px;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo $f['transicao']; ?>
                    </td>
                    <td align="center"
                        style="border:1px solid black;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo $f['aniversario'];?></td>
                    <td align="center"
                        style="border:1px solid black;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo $f['nome']; ?></td>
                    <td align="center"
                        style="border:1px solid black;color:<?php if ($transicao) {
                            echo "#FF8247";
                        } else {
                            echo "#000";
                        } ?>"><?php echo $f['enderecolinha1']; ?>
                                        <?php if($f['enderecolinha2']!=""){ ?><br><?php }?><?php echo $f['enderecolinha2'];?>
                                        <?php if($f['enderecolinha3']!=""){ ?><br><?php }?><?php echo $f['enderecolinha3']; ?>
                                        <?php if($f['enderecolinha4']!=""){ ?><br><?php }?><?php echo $f['enderecolinha4']; ?>
                    </td>
                </tr>
                <?php
                $e++;
            }
            ?>
            <tr style="color:#000;">
                <td align="center" style="border:1px solid black;" colspan="7">TOTAL DE EX-GCS Nesta lista atualizados no sistema:</td>
                    <td align="center" style="border:1px solid black;"><?php echo $z;?>
                    </td>
            </tr>
            <tr style="color:#000;">
                <td align="center" style="border:1px solid black;" colspan="7">Grandes Conselheiros que já passaram pela TRANSIÇÃO:</td>
                    <td align="center" style="border:1px solid black;"><?php echo $x;?>
                    </td>
            </tr>
	</tbody>
</table>
</center>
<?php 
exit;
?>