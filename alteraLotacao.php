<?php

include('https.php');

require_once("model/criaSessaoClass.php");
require_once("model/organismoClass.php");
$sessao = new criaSessao();
$organismo = new organismo();

$sigla = isset($_REQUEST['sigla'])?$_REQUEST['sigla']:null;
$corpo = isset($_REQUEST['corpo'])?$_REQUEST['corpo']:null;

/**
 * Alterar Lotação
 */
$sessao->setValue("siglaOA", $sigla);

$resultado  = $organismo->listaOrganismo(null,$sessao->getValue("siglaOA"));
if($resultado)
{
	foreach($resultado as $vetor)
	{
		$sessao->setValue("idOrganismoAfiliado", $vetor["idOrganismoAfiliado"]);
	}
}

?>
<script>
        location.href="./painelDeControle.php<?php if($corpo!=null&&$corpo!=""){?>?corpo=<?php echo $corpo;?><?php }?>";
</script>