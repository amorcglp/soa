<?php

if (isset($_POST['regiaoRosacruz'])) {
    include_once('../controller/regiaoRosacruzController.php');

    $rrc = new regiaoRosaCruzController();
    $rrc->alteraRegiaoRosacruz();
}else if (isset($_POST['fk_idAgendaAtividade'])&&$_POST['fk_idAgendaAtividade']!=0) {
    //echo "aqui";exit();
    include_once ('../controller/agendaAtividadeController.php');

    $aac = new agendaAtividadeController();
    $aac->atualizaAgendaAtividade($_POST['fk_idAgendaAtividade']);
}else if (isset($_POST['idAuxiliarWeb'])) {
    //echo "aqui";exit();
    include_once ('../controller/auxiliarWebController.php');

    $aw = new auxiliarWebController();
    $aw->alteraAuxiliarWeb($_POST['idAuxiliarWeb']);
}
else if (isset($_POST['idRitoPassagemOGG'])) { /** Convocacao Ritualistica - OGG */
//echo 'entrou aqui';
    include_once('../controller/ritoPassagemOGGController.php');

    $c = new ritoPassagemOGGController();
    $c->altera();
}
else if (isset($_POST['nomeDepartamento'])) {
    include_once('../controller/departamentoController.php');

    $cc = new departamentoController();
    $cc->alteraDepartamento();
} else if (isset($_POST['nomeSecaoMenu'])) {
    include_once('../controller/secaoMenuController.php');

    $sm = new secaoMenuController();
    $sm->alteraSecaoMenu();
} else if (isset($_POST['nomeSubMenu'])) {
    include_once('../controller/subMenuController.php');

    $sm = new subMenuController();
    $sm->alteraSubMenu();
} else if (isset($_POST['nomeOpcaoSubMenu'])) {
    include_once('../controller/opcaoSubMenuController.php');

    $osm = new opcaoSubMenuController();
    $osm->alteraOpcaoSubMenu();
} else if (isset($_POST['nivelDePermissao'])) {
    include_once('../controller/nivelDePermissaoController.php');

    $npc = new nivelDePermissaoController();
    $npc->alteraNivelDePermissao();
}else if (isset($_POST['nomeMembroOa'])&&isset($_POST['dataInicial'])&&isset($_POST['motivo'])) {
    include_once ('../controller/isencaoOAController.php');

    $i = new isencaoOAController();
    $i->alteraIsencaoOa();
}  else if (isset($_POST['tipoObjetoDashboard'])) {
    include_once('../controller/tipoObjetoDashboardController.php');

    $todc = new tipoObjetoDashboardController();
    $todc->alteraTipoObjetoDashboard();
} else if (isset($_POST['nomeObjetoDashboard'])) {
    include_once('../controller/objetoDashboardController.php');

    $odc = new objetoDashboardController();
    $odc->alteraObjetoDashboard();
} else if (isset($_POST['nomeFuncao'])) {
    include_once('../controller/funcaoController.php');

    $fc = new funcaoController();
    $fc->alteraFuncao();
} else if (isset($_POST['tituloAnotacoes'])) {
    include_once('../controller/anotacoesController.php');

    $ac = new anotacoesController();
    $ac->alteraAnotacoes();
} else if (isset($_POST['nomeOrganismoAfiliado'])) {
    include_once('../controller/organismoController.php');

    $oc = new organismoController();
    $oc->alteraOrganismo();
} else if (isset($_POST['referenteAnoImovelControle'])) {
    include_once('../controller/imovelControleController.php');

    $icc = new imovelControleController();
    $icc->alteraImovelControle();
} else if (isset($_POST['enderecoImovel'])) {

    include_once('../controller/imovelController.php');

    $ic = new imovelController();
    $ic->alteraImovel();
} else if (isset($_POST['form_perfilUsuario'])) {
    include_once('../controller/perfilUsuarioController.php');

    $pc = new perfilController();
    $pc->alteraUsuario();
} else if (isset($_POST['nomeUsuario'])) {

    include_once('../controller/usuarioController.php');

    $uc = new usuarioController();
    $uc->alteraUsuario();
} else if (isset($_POST['fk_idAtaReuniaoMensal'])) {

    include_once('../controller/ataReuniaoMensalController.php');

    $armc = new ataReuniaoMensalController();
    $armc->alteraAtaReuniaoMensal();
} else if (isset($_POST['tituloNotificacao'])) {

    include_once('../controller/notificacaoAtualizacaoServerController.php');

    $nsc = new notificacaoServerController();
    $nsc->alteraNotificacaoServer();
}else if (isset($_POST['tituloRespostaFaq'])) {

    include_once('../controller/faqRespostaController.php');

    $frc = new faqRespostaController();
    $frc->alteraRespostaFaq();
}else if (isset($_POST['dataPosse'])) {

    include_once('../controller/ataPosseController.php');

    $apc = new ataPosseController();
    $apc->alteraAtaPosse();
}  else if (isset($_POST['prioridade'])&&isset($_POST['regiao'])) {
    include_once ('../controller/planoAcaoRegiaoController.php');

    $parc = new planoAcaoRegiaoController();
    $parc->alteraPlanoAcaoRegiao();
}else if (isset($_POST['prioridade'])&&isset($_POST['fk_idOrganismoAfiliado'])) {
    include_once ('../controller/planoAcaoOrganismoController.php');

    $paoc = new planoAcaoOrganismoController();
    $paoc->alteraPlanoAcaoOrganismo();
}else if (isset($_POST['palavraChave'])&&isset($_POST['fk_idOrganismoAfiliado'])) {

    include_once('../controller/livroOrganismoController.php');

    $loc = new livroOrganismoController();
    $loc->alteraLivroOrganismo();
} else if (isset($_POST['numeroMensagem'])) {
    include_once ('../controller/discursoController.php');

    $dc = new DiscursoController();
    $dc->alteraDiscurso();
}else if (isset($_POST['palavraChave'])&&isset($_POST['regiao'])) {

    include_once('../controller/livroRegiaoController.php');

    $lrc = new livroRegiaoController();
    $lrc->alteraLivroRegiao();
}
else if (isset($_POST['formularioAlteraOficiais'])) { /** Redirecionamento respons�vel pelo alterar de ----------------- Atividades Inici�ticas - Equipes de Oficiais --------------------- */
    include_once ('../controller/atividadeIniciaticaOficialController.php');

    $aioc = new atividadeIniciaticaOficialController();
    $aioc->alteraOficiaisAtividadeIniciaticaOficial();
}
else if (isset($_POST['tipoAtividadeIniciatica'])) {
    include_once ('../controller/atividadeIniciaticaController.php');

    $aioc = new atividadeIniciaticaController();
    $aioc->alteraAtividadeIniciatica();
}else if (isset($_POST['fk_idEmail'])) {
    include_once ('../controller/emailController.php');
    $ec = new emailController();
    $ec->alteraEmail();
}else if (isset($_POST['regiao'])&&isset($_POST['semestre'])&&isset($_POST['pergunta1'])) {
    include_once ('../controller/relatorioGrandeConselheiroController.php');

    $r = new relatorioGrandeConselheiroController();
    $r->alteraRelatorioGrandeConselheiro();
}else if (isset($_POST['fk_idDivida'])&&isset($_POST['dataPagamento'])&&isset($_POST['valorPagamento'])) {
    include_once ('../controller/quitacaoDividaController.php');

    $qd = new quitacaoDividaController;
    $qd->alteraQuitacaoDivida();
}else if (isset($_POST['dataTermoVoluntariado'])) {
    include_once ('../controller/termoVoluntariadoController.php');

    $tvc = new termoVoluntariadoController();
    $tvc->alteraTermoVoluntariado();
}
else if (isset($_POST['nomeTipoAtividadeEstatuto'])) { /** Redirecionamento respons�vel pelo alterar de ----------------- Tipos de Atividades de Estatuto --------------------- */
    include_once ('../controller/atividadeEstatutoTipoController.php');

    $aetc = new atividadeEstatutoTipoController();
    $aetc->alteraTipoAtividadeEstatuto();
}
else if (isset($_POST['descricaoAtividadeEstatuto'])) { /** Redirecionamento respons�vel pelo alterar de ----------------- Atividades de Estatuto ------------------------------ */
    include_once ('../controller/atividadeEstatutoController.php');

    $aec = new atividadeEstatutoController();
    $aec->alteraAtividadeEstatuto();
}
else if (isset($_POST['descricaoAtividadeIniciaticaColumba'])) { /** Redirecionamento responsável pelo alterar de ----------------- Columbas ------------------------------ */
    include_once ('../controller/atividadeIniciaticaColumbaController.php');

    $acc = new atividadeIniciaticaColumbaController();
    $acc->alteraAtividadeIniciaticaColumba();
}else if (isset($_POST['naturezaMensagemApresentada'])) {
    include_once ('../controller/relatorioClasseArtesaosController.php');

    $rca = new relatorioClasseArtesaosController();
    $rca->alteraRelatorioClasseArtesaos();
}else if (isset($_POST['h_seqCadastColumba'])&&isset($_POST['paisAtivosOA'])&&isset($_POST['reunioes'])) {
    include_once ('../controller/atividadeColumbaController.php');

    $ac = new atividadeColumbaController();
    $ac->alteraAtividadeColumba($_POST['idAtividadeColumba']);
}
else if (isset($_POST['nomeImovelAnexoTipo'])) { /** Redirecionamento responsável pelo alterar de ----------------- Tipos de Anexos ------------------------------ */
    include_once ('../controller/imovelAnexoTipoController.php');
    $iatc = new imovelAnexoTipoController();
    $iatc->alteraImovelAnexoTipo();
}else if (isset($_POST['fk_idBaixaAutomaticaFuncoesUsuarioAgenda'])&&isset($_POST['dataAgendadaInicial'])&&isset($_POST['dataAgendadaFinal'])&&isset($_POST['usuario'])) {
    $dataAgendadaInicial = substr($_POST['dataAgendadaInicial'],6,4)."-".substr($_POST['dataAgendadaInicial'],3,2)."-".substr($_POST['dataAgendadaInicial'],0,2);
    $dataAgendadaFinal = substr($_POST['dataAgendadaFinal'],6,4)."-".substr($_POST['dataAgendadaFinal'],3,2)."-".substr($_POST['dataAgendadaFinal'],0,2);
    include_once ('../model/baixaAutomaticaFuncoesUsuarioClass.php');

    $b = new baixaAutomaticaFuncoesUsuario();
    if($b->atualizaAgenda($dataAgendadaInicial,$dataAgendadaFinal,$_POST['quantidadeDiasProrrogacaoBaixa'],$_POST['usuario'],$_POST['fk_idBaixaAutomaticaFuncoesUsuarioAgenda']))
    {
        echo "<script>location.href='../painelDeControle.php?corpo=buscaAgendaBaixaFuncaoUsuario';</script>";
    }
}else if (isset($_POST['nome'])&&isset($_POST['descricao'])&&isset($_POST['classiOaTipoAtividadeAgenda'])) { 

    include_once ('../controller/atividadeAgendaTipoController.php');

    $a = new atividadeAgendaTipoController();
    $a->alteraTipoAtividadeAgenda();
}
else if (isset($_POST['tituloVideoAula'])&&isset($_POST['descricaoVideoAula'])) { 

    include_once ('../controller/videoAulaController.php');

    $va = new videoAulaController();
    $va->alteraVideoAula();
}
else if (isset($_POST['fk_idOrganismoAfiliado'])&&isset($_POST['classificacao'])&&isset($_POST['data'])) { /** Historico OAs */

    include_once('../controller/historicoOAController.php');

    $h = new historicoOAController();
    $h->alteraHistorico();
}
else if (isset($_POST['idConvocacaoRitualistica'])) { /** Historico OAs */

    include_once('../controller/convocacaoRitualisticaOGGController.php');

    $c = new convocacaoRitualisticaOGGController();
    $c->altera();
}
else if (isset($_POST['idPublico'])) { 

    include_once('../controller/publicoController.php');

    $p = new publicoController();
    $p->alteraPublico();
}
else if (isset($_POST['fk_idEstatuto'])) {
    //echo "entrou";
    include_once('../controller/estatutoController.php');

    $e = new estatutoController();
    $e->alteraEstatuto();
}
else if (isset($_POST['tipo'])&&isset($_POST['modalidadeMensalIndividual'])) {

    include_once('../controller/tabelaContribuicaoController.php');

    $t = new tabelaContribuicaoController();
    $t->alteraTabelaContribuicao();
}
?>

