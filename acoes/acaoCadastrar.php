<?php

if (isset($_POST['nomeUsuario'])) {
    include_once("../controller/usuarioController.php");

    $uc = new usuarioController();
    $uc->cadastroUsuario();
}else if (isset($_POST['idAuxiliarWeb'])) {
    //echo "aqui";exit();
    include_once ('../controller/auxiliarWebController.php');

    $aw = new auxiliarWebController();
    $aw->cadastroAuxiliarWeb();
} 
else if (isset($_POST['regiaoRosacruz'])) {
    include_once('../controller/regiaoRosacruzController.php');

    $rc = new regiaoRosacruzController();
    $rc->cadastraRegiaoRosacruz();
} 
else if (isset($_POST['dataConvocacao'])&&isset($_POST['horaConvocacao'])) { /** Convocacao Ritualistica - OGG */
//echo 'entrou aqui';
    include_once('../controller/convocacaoRitualisticaOGGController.php');

    $c = new convocacaoRitualisticaOGGController();
    $c->cadastro();
}
else if (isset($_POST['idRitoPassagemOGG'])) { /** Convocacao Ritualistica - OGG */
//echo 'entrou aqui';
    include_once('../controller/ritoPassagemOGGController.php');

    $c = new ritoPassagemOGGController();
    $c->cadastro();
}
else if (isset($_POST['nomeDepartamento'])) {
    include_once ('../controller/departamentoController.php');

    $dc = new departamentoController();
    $dc->cadastroDepartamento();
} else if (isset($_POST['nomeOrganismoAfiliado'])) {
    include_once ('../controller/organismoController.php');

    $oc = new organismoController();
    $oc->cadastroOrganismo();
} else if (isset($_POST['nomeFuncao'])) {
    include_once ('../controller/funcaoController.php');

    $fc = new funcaoController();
    $fc->cadastroFuncao();
} else if (isset($_POST['nomeSecaoMenu'])) {
    include_once('../controller/secaoMenuController.php');

    $sm = new secaoMenuController();
    $sm->cadastraSecaoMenu();
} else if (isset($_POST['nomeSubMenu'])) {
    include_once('../controller/subMenuController.php');

    $sm = new subMenuController();
    $sm->cadastraSubMenu();
} else if (isset($_POST['nomeOpcaoSubMenu'])) {
    include_once('../controller/opcaoSubMenuController.php');

    $osm = new opcaoSubMenuController();
    $osm->cadastraOpcaoSubMenu();
} else if (isset($_POST['nivelDePermissao'])) {
    include_once('../controller/nivelDePermissaoController.php');

    $npc = new nivelDePermissaoController();
    $npc->cadastraNivelDePermissao();
} else if (isset($_POST['tipoObjetoDashboard'])) {
    include_once('../controller/tipoObjetoDashboardController.php');

    $todc = new tipoObjetoDashboardController();
    $todc->cadastraTipoObjetoDashboard();
} else if (isset($_POST['nomeObjetoDashboard'])) {
    include_once('../controller/objetoDashboardController.php');

    $odc = new objetoDashboardController();
    $odc->cadastraObjetoDashboard();
} else if (isset($_POST['tituloAnotacoes'])) {
    include_once ('../controller/anotacoesController.php');

    $ac = new anotacoesController();
    $ac->cadastroAnotacoes();
}

else if (isset($_POST['enderecoImovel'])) {
    include_once ('../controller/imovelController.php');

    $ic = new imovelController();
    $ic->cadastroImovel();
}

else if (isset($_POST['dataAtaReuniaoMensal'])) {
    include_once ('../controller/ataReuniaoMensalController.php');

    $armc = new ataReuniaoMensalController();
    $armc->cadastroAtaReuniaoMensal();
} else if (isset($_POST['referenteAnoImovelControle'])) {
    include_once ('../controller/imovelControleController.php');

    $icc = new imovelControleController();
    $icc->cadastroImovelControle();
} else if (isset($_POST['tituloNotificacao'])) {
    include_once('../controller/notificacaoAtualizacaoServerController.php');

    $nc = new notificacaoServerController();
    $nc->cadastroNotificacaoServer();
} else if (isset($_POST['nomeUsuarioFaq'])) {
    include_once('../controller/faqController.php');

    $fc = new faqController();
    $fc->cadastroFaq();
} else if (isset($_POST['tituloRespostaFaq'])) {
    include_once('../controller/faqRespostaController.php');

    $frc = new faqRespostaController();
    $frc->cadastroRespostaFaq();
} else if (isset($_POST['dataPosse'])) {
    include_once ('../controller/ataPosseController.php');

    $apc = new ataPosseController();
    $apc->cadastroAtaPosse();
} else if (isset($_POST['motivoTransferencia'])) {
    include_once ('../controller/transferenciaMembroController.php');

    $tmc = new transferenciaMembroController();
    $tmc->cadastroTransferenciaMembro();
} else if (isset($_POST['nomeMembroOa'])&&isset($_POST['dataInicial'])&&isset($_POST['motivo'])) {
    include_once ('../controller/isencaoOAController.php');

    $i = new isencaoOAController();
    $i->cadastroIsencaoOA();
} else if (isset($_POST['nomeMembroOa'])) {
    include_once ('../controller/membroOAController.php');

    $m = new membroOAController();
    $m->cadastroMembroOA();
}  else if (isset($_POST['prioridade'])&&isset($_POST['regiao'])) {
    include_once ('../controller/planoAcaoRegiaoController.php');

    $parc = new planoAcaoRegiaoController();
    $parc->cadastroPlanoAcaoRegiao();
}else if (isset($_POST['prioridade'])&&isset($_POST['fk_idOrganismoAfiliado'])) {
    include_once ('../controller/planoAcaoOrganismoController.php');

    $paoc = new planoAcaoOrganismoController();
    $paoc->cadastroPlanoAcaoOrganismo();
}else if (isset($_POST['palavraChave'])&&isset($_POST['fk_idOrganismoAfiliado'])) {
    include_once ('../controller/livroOrganismoController.php');

    $loc = new livroOrganismoController();
    $loc->cadastroLivroOrganismo();
} else if (isset($_POST['numeroMensagem'])) {
    include_once ('../controller/discursoController.php');

    $dc = new DiscursoController();
    $dc->cadastroDiscurso();
} else if (isset($_POST['palavraChave'])&&isset($_POST['regiao'])) {
    include_once ('../controller/livroRegiaoController.php');

    $lrc = new livroRegiaoController();
    $lrc->cadastroLivroRegiao();
} else if (isset($_POST['tipoAtividadeIniciatica'])) {
    include_once ('../controller/atividadeIniciaticaController.php');

    $aic = new atividadeIniciaticaController();
    $aic->cadastroAtividadeIniciatica();
} else if (isset($_POST['anotacoesAtividadeIniciaticaOficial'])) {
    include_once ('../controller/atividadeIniciaticaOficialController.php');

    $aioc = new atividadeIniciaticaOficialController();
    $aioc->cadastroAtividadeIniciaticaOficial();
} else if (isset($_POST['tipoAtividadeIniciatica'])) {
    include_once ('../controller/atividadeIniciaticaController.php');

    $aioc = new atividadeIniciaticaController();
    $aioc->cadastroAtividadeIniciatica();
}else if (isset($_POST['regiao'])&&isset($_POST['semestre'])&&isset($_POST['pergunta1'])) {
    include_once ('../controller/relatorioGrandeConselheiroController.php');

    $r = new relatorioGrandeConselheiroController();
    $r->cadastroRelatorioGrandeConselheiro();
}
else if (isset($_POST['fk_idDivida'])&&isset($_POST['dataPagamento'])&&isset($_POST['valorPagamento'])) {
    include_once ('../controller/quitacaoDividaController.php');

    $qd = new quitacaoDividaController;
    $qd->cadastroQuitacaoDivida();
}else if (isset($_POST['dataTermoVoluntariado'])) {
    include_once ('../controller/termoVoluntariadoController.php');

    $tvc = new termoVoluntariadoController();
    $tvc->cadastroTermoVoluntariado();
}
else if (isset($_POST['nomeTipoAtividadeEstatuto'])) { /** Redirecionamento respons�vel pelo cadastro de Tipos de Atividades de Estatuto */

    include_once ('../controller/atividadeEstatutoTipoController.php');

    $aetc = new atividadeEstatutoTipoController();
    $aetc->cadastroTipoAtividadeEstatuto();
}
else if (isset($_POST['fk_idTipoAtividadeEstatuto'])&&!isset($_POST['idAgendaAtividade'])) { /** Redirecionamento respons�vel pelo cadastro de Atividades de Estatuto */

    include_once ('../controller/atividadeEstatutoController.php');

    $aec = new atividadeEstatutoController();
    $aec->cadastroAtividadeEstatuto();
}
else if (isset($_POST['codAfiliacaoAtividadeIniciaticaColumba'])) { /** Redirecionamento responsável pelo cadastro de Columbas */

    include_once ('../controller/atividadeIniciaticaColumbaController.php');

    $acc = new atividadeIniciaticaColumbaController();
    $acc->cadastroAtividadeIniciaticaColumba();
}else if (isset($_POST['naturezaMensagemApresentada'])) {
    include_once ('../controller/relatorioClasseArtesaosController.php');

    $rca = new relatorioClasseArtesaosController();
    $rca->cadastroRelatorioClasseArtesaos();
}else if (isset($_POST['dataPrevistaInstalacao'])) {
    include_once ('../controller/columbaRelatorioTrimestralController.php');

    $cni = new columbaRelatorioTrimestralController();
    $cni->cadastroColumba();
}else if (isset($_POST['h_seqCadastColumba'])&&isset($_POST['paisAtivosOA'])&&isset($_POST['reunioes'])) {
    include_once ('../controller/atividadeColumbaController.php');

    $ac = new atividadeColumbaController();
    $ac->cadastroAtividadeColumba();
}else if (isset($_POST['nomeImovelAnexoTipo'])) { /** Redirecionamento responsável pelo cadastro de Tipos de Anexos */

    include_once ('../controller/imovelAnexoTipoController.php');

    $iatc = new imovelAnexoTipoController();
    $iatc->cadastroTipoAtividadeEstatuto();
}else if (isset($_POST['idAgendaAtividade'])) {

    include_once ('../controller/agendaAtividadeController.php');

    $aac = new agendaAtividadeController();
    $aac->cadastroAgendaAtividade();
}else if (isset($_POST['tituloTicket'])) { /** Redirecionamento responsável pelo cadastro de Tikets */

    include_once('../controller/ticketController.php');

    $tc = new ticketController();
    $tc->cadastroTicket();
}else if (isset($_POST['dataAgendadaInicial'])&&isset($_POST['dataAgendadaFinal'])&&isset($_POST['usuario'])) { /** Redirecionamento responsável pelo cadastro de Tikets */
    
    $dataAgendadaInicial = substr($_POST['dataAgendadaInicial'],6,4)."-".substr($_POST['dataAgendadaInicial'],3,2)."-".substr($_POST['dataAgendadaInicial'],0,2);
    $dataAgendadaFinal = substr($_POST['dataAgendadaFinal'],6,4)."-".substr($_POST['dataAgendadaFinal'],3,2)."-".substr($_POST['dataAgendadaFinal'],0,2);
    include_once ('../model/baixaAutomaticaFuncoesUsuarioClass.php');

    $b = new baixaAutomaticaFuncoesUsuario();
    if($b->cadastraAgenda($dataAgendadaInicial,$dataAgendadaFinal,$_POST['quantidadeDiasProrrogacaoBaixa'],$_POST['usuario']))
    {
        echo "<script>location.href='../painelDeControle.php?corpo=buscaAgendaBaixaFuncaoUsuario';</script>";
    }
}
else if (isset($_POST['nome'])&&isset($_POST['descricao'])&&isset($_POST['classiOaTipoAtividadeAgenda'])) { 

    include_once ('../controller/atividadeAgendaTipoController.php');

    $a = new atividadeAgendaTipoController();
    $a->cadastroTipoAtividadeAgenda();
}
else if (isset($_POST['tituloVideoAula'])&&isset($_POST['descricaoVideoAula'])) { 

    include_once ('../controller/videoAulaController.php');

    $va = new videoAulaController();
    $va->cadastroVideoAula();
}
else if (isset($_POST['anoEstatuto'])) { /** Redirecionamento responsável pelo cadastro de Estatutos */

    include_once('../controller/estatutoController.php');

    $ec = new estatutoController();
    $ec->cadastroEstatuto();
}
else if (isset($_POST['fk_idOrganismoAfiliado'])&&isset($_POST['classificacao'])&&isset($_POST['data'])) { /** Historico OAs */

    include_once('../controller/historicoOAController.php');

    $h = new historicoOAController();
    $h->cadastraHistorico();
}
else if (isset($_POST['publico'])) { 

    include_once('../controller/publicoController.php');

    $p = new publicoController();
    $p->cadastroPublico();
}
else if (isset($_POST['fk_idOrganismoAfiliado'])&&isset($_POST['numeroOficio'])&&isset($_POST['cidadeOficio'])&&isset($_POST['ufOficio'])&&isset($_POST['dataAssembleiaGeral'])) {

    include_once('../controller/estatutoController.php');

    $e = new estatutoController();
    $e->cadastroEstatuto();
}
else if (isset($_POST['tipo'])&&isset($_POST['modalidadeMensalIndividual'])) {

    include_once('../controller/tabelaContribuicaoController.php');

    $t = new tabelaContribuicaoController();
    $t->cadastraTabelaContribuicao();
}
?>