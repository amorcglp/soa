<?php

if(isset($_SESSION)) {
    session_destroy();
} else {
    session_start();
}
ob_start();

/*
* Verificar token Facebook
*/
include_once("../model/usuarioClass.php");
$u = new Usuario();
if(isset($_GET['code']))
{

    $par="";
    
    if (isset($_GET['registroFacebook'])) {
        if ($_GET['registroFacebook'] == true) {
            $par = "?registroFacebook=true&seqCadast=".$_GET['seqCadast'];
        }
    }
    
    if(UrlAtual()=="treinamento.amorc.org.br")
    {    
        /*Treinamento*/
        $CLIENT_ID     = '933766273415925';
        $CLIENT_SECRET = '36479fe57badf049e6326e42e0b5b321';
        $REDIRECT_URI  = 'https://treinamento.amorc.org.br/acoes/acaoLogin.php'.$par;
        require '../vendor/autoload.php';//Composer - v 1.2.0
    }
    
    if(UrlAtual()=="soa.amorc.org.br")
    {
        /*Produção*/
        $CLIENT_ID     = '639511822894182';
        $CLIENT_SECRET = 'b9225f8c5729606f9293437b0057c5ab';
        $REDIRECT_URI  = 'https://soa.amorc.org.br/acoes/acaoLogin.php'.$par;
        require '../vendor/autoload.php';//Composer - v 1.2.0
    }
    if(UrlAtual()=="localhost")
    {
        /*Localhost*/
        $CLIENT_ID     = '933787073413845';
        $CLIENT_SECRET = 'a78834742d2cfea51ccd7ab4b7e41dec';
        $REDIRECT_URI  = 'https://localhost/soa_producao_st/acoes/acaoLogin.php'.$par;
        require 'C:/ProgramData/ComposerSetup/bin/vendor/autoload.php';//Composer
    }
    
       

    $AUTHORIZATION_ENDPOINT = 'https://graph.facebook.com/oauth/authorize';
    $TOKEN_ENDPOINT         = 'https://graph.facebook.com/oauth/access_token';

    $client = new OAuth2\Client($CLIENT_ID, $CLIENT_SECRET);
    $params = array('code' => $_GET['code'], 'redirect_uri' => $REDIRECT_URI);
    $response = $client->getAccessToken($TOKEN_ENDPOINT, 'authorization_code', $params);
    //echo "res=>".print_r($response);
    parse_str($response['result'], $info);
    $client->setAccessToken($info['access_token']);
    $response = $client->fetch('https://graph.facebook.com/me');
    
    //var_dump($response, $response['result']);
    //exit();
    
    if(isset($response['result']['id']))
    {    
        //Efetuar registro do facebook na 1ª autenticação
        if(isset($_GET['registroFacebook']))
        {
            //$appId = "933766273415925";
            //$_SESSION['fb_'.$appId.'_access_token'] = '' ;
            //echo "vinculo...";exit();
            $u->vinculaIdFacebookContaSOA($response['result']['id'],$_GET['seqCadast']);
        }

       if($u->verificaTokenFacebook($response['result']['id']))
       {
           $resultado = $u->selecionaDadosUsuarioPeloToken($response['result']['id']);
           //echo "<pre>";print_r($resultado);
           //exit();
           if($resultado)
           {
               foreach ($resultado as $vetor)
               {
                   $_POST['verificarHash'] = true;
                   $_POST['loginUsuario'] = $vetor['loginUsuario'];
                   $_POST['senhaUsuario'] = $vetor['senhaUsuario'];
               }    
           }    
       }else{
           //echo "Acesso negado";
           echo "<script>location.href='../login.php?vinculeFacebook=true';</script>";
           exit();
       }   
    }else{
        echo "<script>location.href='../login.php?tokenExpirado=true';</script>";
        exit();
    }
}

require_once ("../lib/functions.php");
require_once ("../controller/tentativaLoginController.php");

$ip             = $_SERVER['REMOTE_ADDR'];
$tentativa      = new tentativaLoginController();
$resultado      = $tentativa->selecionaUltimaTentativaLogin($ip);

if(count($resultado) > 2) { // && $ip != "179.184.37.5" && $ip != "179.217.79.31" && substr($ip,0,8) != "192.1.1."

    $resultado          = $tentativa->selecionaUltimaTentativaLogin($ip, true);
    $ultimoRegistro     = $resultado[0]['data'];

    $datatime1  = new DateTime($ultimoRegistro);
    $datatime2  = new DateTime(date('Y/m/d H:i:s'));
    $data1      = $datatime1->format('Y-m-d H:i:s');
    $data2      = $datatime2->format('Y-m-d H:i:s');
    $diff       = $datatime1->diff($datatime2);

    $minutosPassados    = $diff->format('%i');
    if ($minutosPassados > 2) {

        if ($tentativa->limpaTentativaLogin($ip))
        {    
            echo '<script type="text/javascript"> window.location = "../login.php?desbloqueado=1"; </script>';
        }

    } else {
        echo '<script type="text/javascript"> window.location = "../login.php?tentou=1"; </script>';
    }

} else {
    $_SESSION["paisUsuario"] = ip_visitor_country();

    require_once ("../controller/loginController.php");

    $lc = new loginController();
    $lc->efetuaLogin($ip);
}
