<?php

//require_once ("consultaClass.php");
require_once ("conexaoClass.php");

class AgendaAtividade{

    private $idAgendaAtividade;    
    private $fk_idOrganismoAfiliado;
    private $tipoAtividade;
    private $complementoNomeAtividade;
    private $local;
    private $anoCompetencia;
    private $diasHorarios;
    private $observacoes;
    private $usuario;
    private $alteracaoUsuario;
    private $recorrente;
    private $dataInicial;
    private $dataFinal;
    private $horaInicial;
    private $horaFinal;
    private $tag;
    private $fk_idPublico;
    private $statusAgenda;
    private $ultimoAtualizar;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAgendaAtividade() {
        return $this->idAgendaAtividade;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getTipoAtividade() {
        return $this->tipoAtividade;
    }

    function getComplementoNomeAtividade() {
        return $this->complementoNomeAtividade;
    }

    function getLocal() {
        return $this->local;
    }

    function getAnoCompetencia() {
        return $this->anoCompetencia;
    }

    function getDiasHorarios() {
        return $this->diasHorarios;
    }

    function getObservacoes() {
        return $this->observacoes;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getAlteracaoUsuario() {
        return $this->alteracaoUsuario;
    }

    function getRecorrente() {
        return $this->recorrente;
    }

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function getHoraInicial() {
        return $this->horaInicial;
    }

    function getHoraFinal() {
        return $this->horaFinal;
    }

    function getTag() {
        return $this->tag;
    }

    function getFk_idPublico() {
        return $this->fk_idPublico;
    }
    
    function getStatusAgenda() {
        return $this->statusAgenda;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdAgendaAtividade($idAgendaAtividade) {
        $this->idAgendaAtividade = $idAgendaAtividade;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setTipoAtividade($tipoAtividade) {
        $this->tipoAtividade = $tipoAtividade;
    }

    function setComplementoNomeAtividade($complementoNomeAtividade) {
        $this->complementoNomeAtividade = $complementoNomeAtividade;
    }

    function setLocal($local) {
        $this->local = $local;
    }

    function setAnoCompetencia($anoCompetencia) {
        $this->anoCompetencia = $anoCompetencia;
    }

    function setDiasHorarios($diasHorarios) {
        $this->diasHorarios = $diasHorarios;
    }

    function setObservacoes($observacoes) {
        $this->observacoes = $observacoes;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setAlteracaoUsuario($alteracaoUsuario) {
        $this->alteracaoUsuario = $alteracaoUsuario;
    }

    function setRecorrente($recorrente) {
        $this->recorrente = $recorrente;
    }

    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    function setHoraInicial($horaInicial) {
        $this->horaInicial = $horaInicial;
    }

    function setHoraFinal($horaFinal) {
        $this->horaFinal = $horaFinal;
    }

    function setTag($tag) {
        $this->tag = $tag;
    }

    function setFk_idPublico($fk_idPublico) {
        $this->fk_idPublico = $fk_idPublico;
    }

    function setStatusAgenda($status) {
        $this->statusAgenda = $status;
    }
    
    function setUltimoAtualizar($ultimo) {
        $this->ultimoAtualizar = $ultimo;
    }

    //--------------------------------------------------------------------------

    public function cadastroAgendaAtividade(){

        $sql = "INSERT INTO agendaAtividade
                                 (fk_idOrganismoAfiliado,
                                  tipoAtividade,
                                  complementoNomeAtividade,
                                  local,
                                  anoCompetencia,
                                  diasHorarios,
                                  observacoes,
                                  alteracaoUsuario,
                                  usuario,
                                  dataCadastro,
                                  recorrente,
                                  dataInicial,
                                  dataFinal,
                                  horaInicial,
                                  horaFinal,
                                  tag,
                                  fk_idPublico
                                  )
                            VALUES (
                            		:fk_idOrganismoAfiliado,
                            		:tipoAtividade,
                            		:complementoNomeAtividade,
                            		:local,
                            		:anoCompetencia,
                                        :diasHorarios,
                                        :observacoes,
                                        0,
                            		:usuario,
                                    now(),
                                    :recorrente,
                                    :dataInicial,
                                    :dataFinal,
                                    :horaInicial,
                                    :horaFinal,
                                    :tag,
                                    :fk_idPublico
                                    )";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        //echo "<pre>";print_r($this);echo $sql;exit();
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':tipoAtividade', $this->tipoAtividade, PDO::PARAM_INT);
        $sth->bindParam(':complementoNomeAtividade', $this->complementoNomeAtividade, PDO::PARAM_STR);
        $sth->bindParam(':local', $this->local, PDO::PARAM_STR);
        $sth->bindParam(':anoCompetencia', $this->anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':diasHorarios', $this->diasHorarios, PDO::PARAM_STR);
        $sth->bindParam(':observacoes', $this->observacoes, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        $sth->bindParam(':recorrente', $this->recorrente, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $this->dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $this->dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':horaInicial', $this->horaInicial, PDO::PARAM_STR);
        $sth->bindParam(':horaFinal', $this->horaFinal, PDO::PARAM_STR);
        $sth->bindParam(':tag', $this->tag, PDO::PARAM_INT);
        $sth->bindParam(':fk_idPublico', $this->fk_idPublico, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaAgendaAtividade($id){

        $sql = "UPDATE agendaAtividade SET
                                  complementoNomeAtividade = :complementoNomeAtividade,
                                  tipoAtividade= :tipoAtividade,
                                  local= :local,
                                  anoCompetencia= :anoCompetencia,
                                  diasHorarios= :diasHorarios,
                                  observacoes= :observacoes,
                                  alteracaoUsuario= :alteracaoUsuario,
                                  recorrente = :recorrente,
                                  dataInicial = :dataInicial,
                                  dataFinal = :dataFinal,
                                  horaInicial = :horaInicial,
                                  horaFinal = :horaFinal,
                                  tag = :tag,
                                  fk_idPublico = :fk_idPublico,
                                  ultimoAtualizar = :ultimoAtualizar
                                  WHERE idAgendaAtividade= :idAgendaAtividade
                                  ";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipoAtividade', $this->tipoAtividade, PDO::PARAM_INT);
        $sth->bindParam(':complementoNomeAtividade', $this->complementoNomeAtividade, PDO::PARAM_STR);
        $sth->bindParam(':local', $this->local, PDO::PARAM_STR);
        $sth->bindParam(':anoCompetencia', $this->anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':diasHorarios', $this->diasHorarios, PDO::PARAM_STR);
        $sth->bindParam(':observacoes', $this->observacoes, PDO::PARAM_STR);
        $sth->bindParam(':alteracaoUsuario', $this->alteracaoUsuario, PDO::PARAM_INT);
        $sth->bindParam(':idAgendaAtividade', $id, PDO::PARAM_INT);
        
        $sth->bindParam(':recorrente', $this->recorrente, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $this->dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $this->dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':horaInicial', $this->horaInicial, PDO::PARAM_STR);
        $sth->bindParam(':horaFinal', $this->horaFinal, PDO::PARAM_STR);
        $sth->bindParam(':tag', $this->tag, PDO::PARAM_INT);
        $sth->bindParam(':fk_idPublico', $this->fk_idPublico, PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idAgendaAtividade) as lastid FROM agendaAtividade";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function listaAgendaAtividade($idOrganismoAfiliado=null,$anoAtual=null){

    	$sql = "SELECT * FROM agendaAtividade as a
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
        inner join atividadeEstatuto_tipo on idTipoAtividadeEstatuto = tipoAtividade
        inner join usuario as u on u.seqCadast = a.usuario
    	where 1=1 ";

    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
        if($anoAtual!=null)
    	{
    		$sql .= " and anoCompetencia = :anoAtual";
    	}
    	$sql .= " order by tipoAtividade";
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaAnosAgendaAtividade($idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM agendaAtividade
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
        inner join atividadeEstatuto_tipo on idTipoAtividadeEstatuto = tipoAtividade
    	where 1=1 ";
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
    	$sql .= " group by anoCompetencia order by anoCompetencia";
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaCadastrado($tipoAtividade,$anoCompetencia,$idOrganismoAfiliado){

    	$sql = "SELECT * FROM agendaAtividade
    			where tipoAtividade= :tipoAtividade"
                . " and anoCompetencia= :anoCompetencia"
                . " and fk_idOrganismoAfiliado= :fk_idOrganismoAfiliado";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipoAtividade', $tipoAtividade, PDO::PARAM_INT);
        $sth->bindParam(':anoCompetencia', $anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,false,false,true)){
	        return true;
	    }else{
	        return false;
        }
    }

    public function buscaAgendaAtividadePorId($id){

        $sql = "SELECT * FROM  agendaAtividade WHERE   idAgendaAtividade = :idAgendaAtividade";

        //echo $sql."<br>";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAgendaAtividade', $id, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function excluirAgendaAtividade($id){

        $sql = "DELETE FROM agendaAtividade
                                  WHERE idAgendaAtividade=:idAgendaAtividade
                                  ";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAgendaAtividade', $id, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusAgendaAtividade() {
        
	$sql = "UPDATE agendaAtividade SET `statusAgenda` = :status
                               WHERE `idAgendaAtividade` = :idAgendaAtividade";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusAgenda, PDO::PARAM_INT);
        $sth->bindParam(':idAgendaAtividade', $this->idAgendaAtividade, PDO::PARAM_INT);
		
        if ($c->run($sth,true)) {
            $arr = array();
            $arr['status'] = $this->statusAgenda;
            return $arr;
        } else {
            return false;
        }
    }

    public function excluirAtividadesRecorrentes($idOa){

        $sql = "DELETE FROM agendaAtividade
                                  WHERE fk_idOrganismoAfiliado=:idOa
                                  and recorrente = 1
                                  ";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOa', $idOa, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
