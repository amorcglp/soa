<?php

require_once ("conexaoClass.php");

class logRegistroOficial{

    private $idLogRegistroOficial;
    private $siglaOrganismoAfiliado;
    private $codAfiliacao;
    private $companheiro;
    private $tipoCargo;
    private $tipoMembro;
    private $funcao;
    private $dataEntrada;
    private $dataSaida;
    private $dataTermino;
    private $usuario;
    private $dataCadastro;

    function getIdLogRegistroOficial() {
        return $this->idLogRegistroOficial;
    }

    function getSiglaOrganismoAfiliado() {
        return $this->siglaOrganismoAfiliado;
    }

    function getCodAfiliacao() {
        return $this->codAfiliacao;
    }

    function getCompanheiro() {
        return $this->companheiro;
    }

    function getTipoCargo() {
        return $this->tipoCargo;
    }

    function getTipoMembro() {
        return $this->tipoMembro;
    }

    function getFuncao() {
        return $this->funcao;
    }

    function getDataEntrada() {
        return $this->dataEntrada;
    }

    function getDataSaida() {
        return $this->dataSaida;
    }

    function getDataTermino() {
        return $this->dataTermino;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getDataCadastro() {
        return $this->dataCadastro;
    }

    function setIdLogRegistroOficial($idLogRegistroOficial) {
        $this->idLogRegistroOficial = $idLogRegistroOficial;
    }

    function setSiglaOrganismoAfiliado($siglaOrganismoAfiliado) {
        $this->siglaOrganismoAfiliado = $siglaOrganismoAfiliado;
    }

    function setCodAfiliacao($codAfiliacao) {
        $this->codAfiliacao = $codAfiliacao;
    }

    function setCompanheiro($companheiro) {
        $this->companheiro = $companheiro;
    }

    function setTipoCargo($tipoCargo) {
        $this->tipoCargo = $tipoCargo;
    }

    function setTipoMembro($tipoMembro) {
        $this->tipoMembro = $tipoMembro;
    }

    function setFuncao($funcao) {
        $this->funcao = $funcao;
    }

    function setDataEntrada($dataEntrada) {
        $this->dataEntrada = $dataEntrada;
    }

    function setDataSaida($dataSaida) {
        $this->dataSaida = $dataSaida;
    }

    function setDataTermino($dataTermino) {
        $this->dataTermino = $dataTermino;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

        
        
    //--------------------------------------------------------------------------

    /**
     * [Realiza cadastro de ]
     * @return [boolean] [Em caso de sucesso no retorno do PDO retorna TRUE]
     */
    public function cadastro(){

        $sql = "INSERT INTO logRegistroOficial 
            (siglaOrganismoAfiliado, 
            codAfiliacao, 
            companheiro, 
            tipoCargo, 
            tipoMembro, 
            funcao, 
            dataEntrada, 
            dataTerminoMandato, 
            usuario, 
            dataCadastro) 
        VALUES (
        :siglaOrganismoAfiliado,
        :codAfiliacao,
        :companheiro,
        :tipoCargo,
        :tipoMembro,
        :funcao,
        :dataEntrada,
        :dataTermino,
        :usuario,
        now()
        )";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':siglaOrganismoAfiliado', $this->siglaOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':codAfiliacao', $this->codAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':companheiro', $this->companheiro, PDO::PARAM_STR);
        $sth->bindParam(':tipoCargo', $this->tipoCargo, PDO::PARAM_INT);
        $sth->bindParam(':tipoMembro', $this->tipoMembro, PDO::PARAM_INT);
        $sth->bindParam(':funcao', $this->funcao, PDO::PARAM_INT);
        $sth->bindParam(':dataEntrada', $this->dataEntrada, PDO::PARAM_STR);
        $sth->bindParam(':dataTermino', $this->dataTermino, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
