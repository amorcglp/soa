<?php

require_once ("conexaoClass.php");

class opcaoSubMenu {

    public $idOpcaoSubMenu;
    public $nomeOpcaoSubMenu;
    public $descricaoOpcaoSubMenu;
    public $arquivoOpcaoSubMenu;
    public $prioridadeOpcaoSubMenu;
    public $iconeOpcaoSubMenu;
    public $fk_idSubMenu;
    public $fk_idSecaoMenu;
    public $statusOpcaoSubMenu;

    //Getter e Setter
    function getIdOpcaoSubMenu() {
        return $this->idOpcaoSubMenu;
    }

	function getNomeOpcaoSubMenu() {
        return $this->nomeOpcaoSubMenu;
    }
    
	function getDescricaoOpcaoSubMenu() {
        return $this->descricaoOpcaoSubMenu;
    }
    
	function getArquivoOpcaoSubMenu() {
        return $this->arquivoOpcaoSubMenu;
    }
    
	function getPrioridadeOpcaoSubMenu() {
        return $this->prioridadeOpcaoSubMenu;
    }
    
	function getIconeOpcaoSubMenu() {
        return $this->iconeOpcaoSubMenu;
    }
    
	function getFkIdSubMenu() {
        return $this->fk_idSubMenu;
    }
    
	function getFkIdSecaoMenu() {
        return $this->fk_idSecaoMenu;
    }
    
	function getStatusOpcaoSubMenu() {
        return $this->statusOpcaoSubMenu;
    }
    
    function setIdOpcaoSubMenu($idOpcaoSubMenu) {
        $this->idOpcaoSubMenu = $idOpcaoSubMenu;
    }

	function setNomeOpcaoSubMenu($nomeOpcaoSubMenu) {
        $this->nomeOpcaoSubMenu = $nomeOpcaoSubMenu;
    }
    
    function setDescricaoOpcaoSubMenu($descricaoOpcaoSubMenu) {
        $this->descricaoOpcaoSubMenu = $descricaoOpcaoSubMenu;
    }
    
	function setArquivoOpcaoSubMenu($arquivoOpcaoSubMenu) {
        $this->arquivoOpcaoSubMenu = $arquivoOpcaoSubMenu;
    }
    
	function setPrioridadeOpcaoSubMenu($prioridadeOpcaoSubMenu) {
        $this->prioridadeOpcaoSubMenu = $prioridadeOpcaoSubMenu;
    }
    
	function setIconeOpcaoSubMenu($iconeOpcaoSubMenu) {
        $this->iconeOpcaoSubMenu = $iconeOpcaoSubMenu;
    }
    
	function setFkIdSubMenu($fk_idSubMenu) {
        $this->fk_idSubMenu = $fk_idSubMenu;
    }
    
	function setFkIdSecaoMenu($fk_idSecaoMenu) {
        $this->fk_idSecaoMenu = $fk_idSecaoMenu;
    }
    
	function setStatusOpcaoSubMenu($statusOpcaoSubMenu) {
        $this->statusOpcaoSubMenu = $statusOpcaoSubMenu;
    }

    public function cadastraOpcaoSubMenu() {
        
        $sql = "INSERT INTO opcaoSubMenu (idOpcaoSubMenu, nomeOpcaoSubMenu, descricaoOpcaoSubMenu, arquivoOpcaoSubMenu, prioridadeOpcaoSubMenu, iconeOpcaoSubMenu, fk_idSubMenu )
                                    VALUES (:id,
                                            :nome,
                                            :descricao,
                                            :arquivo,
                                            :prioridade,
                                            :icone,
                                            :idSubMenu
                                            )"; 
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idOpcaoSubMenu, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nomeOpcaoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoOpcaoSubMenu , PDO::PARAM_STR);
        $sth->bindParam(':arquivo',$this->arquivoOpcaoSubMenu , PDO::PARAM_STR);
        $sth->bindParam(':prioridade',$this->prioridadeOpcaoSubMenu , PDO::PARAM_INT);
        $sth->bindParam(':icone', $this->iconeOpcaoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':idSubMenu', $this->fk_idSubMenu, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraOpcaoSubMenu($id) {
        
        $sql = "UPDATE opcaoSubMenu 
                            SET 
                                `nomeOpcaoSubMenu` = :nome,
                                `descricaoOpcaoSubMenu` = :descricao,
                                `arquivoOpcaoSubMenu` = :arquivo,
                                `prioridadeOpcaoSubMenu` = :prioridade,
                                `iconeOpcaoSubMenu` = :icone,
                                `fk_idSubMenu` = :idSubMenu                   
                            WHERE idOpcaoSubMenu = :id";
        
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nome', $this->nomeOpcaoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoOpcaoSubMenu , PDO::PARAM_STR);
        $sth->bindParam(':arquivo',$this->arquivoOpcaoSubMenu , PDO::PARAM_STR);
        $sth->bindParam(':prioridade',$this->prioridadeOpcaoSubMenu , PDO::PARAM_INT);
        $sth->bindParam(':icone', $this->iconeOpcaoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':idSubMenu', $this->fk_idSubMenu, PDO::PARAM_INT);
        $sth->bindParam(':id', $id , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusOpcaoSubMenu() {
        
        $sql = "UPDATE opcaoSubMenu SET `statusOpcaoSubMenu` = :status "
                . " WHERE `idOpcaoSubMenu` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusOpcaoSubMenu, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idOpcaoSubMenu, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusOpcaoSubMenu();
            return $arr;
        } else {
            return false;
        }
    }

    public function listaOpcaoSubMenu($arquivo=null) {

    	$sql = "SELECT * FROM opcaoSubMenu as sm
        						inner join subMenu as s on sm.fk_idSubMenu = s.idSubMenu
        						inner join secaoMenu as sec on s.fk_idSecaoMenu = sec.idSecaoMenu
        						where 1=1 ";
    	if($arquivo!=null)
    	{
    		$sql .= " and arquivoOpcaoSubMenu = :arquivo";
    	}
        $sql .= "  ORDER BY nomeOpcaoSubMenu";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($arquivo!=null)
    	{
            $sth->bindParam(':arquivo', $arquivo , PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeOpcaoSubMenu($idOpcaoSubMenu) {
        
        $sql = "DELETE FROM opcaoSubMenu WHERE idOpcaoSubMenu = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idOpcaoSubMenu, PDO::PARAM_INT);
        $resultado = $c->run($sth,true);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaOpcaoSubMenu($subMenu=null,$status=0) {
        
    	$sql = "SELECT * FROM opcaoSubMenu
                                 WHERE `nomeOpcaoSubMenu` LIKE :nome";
    	if($subMenu!=null)
    	{
    		$sql .= " and fk_idSubMenu=:subMenu";
    	}
    	$sql .= " and statusOpcaoSubMenu=:status";
    	
        $sql .= " ORDER BY nomeOpcaoSubMenu ASC";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $nome = '%'.$this->getNomeOpcaoSubMenu().'%';
        $sth->bindParam(':nome', $nome, PDO::PARAM_STR);
        if($subMenu!=null)
    	{
            $sth->bindParam(':subMenu', $subMenu, PDO::PARAM_STR);
        }
        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdOpcaoSubMenu($id) {
        $sql = "SELECT * FROM opcaoSubMenu as sm 
        					  inner join subMenu as s on sm.fk_idSubMenu = s.idSubMenu
        					  inner join secaoMenu as sec on s.fk_idSecaoMenu = sec.idSecaoMenu
                              WHERE idOpcaoSubMenu = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {    
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

}

?>