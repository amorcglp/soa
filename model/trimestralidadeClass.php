<?php

require_once ("conexaoClass.php");

class trimestralidade{

    private $idtrimestralidade;    
    private $dataTrimestralidade;
    private $valorTrimestralidade;
    private $usuario;
    private $ultimoAtualizar;

    //METODO SET/GET -----------------------------------------------------------

    function getIdTrimestralidade() {
        return $this->idTrimestralidade;
    }
    
	function getDataTrimestralidade() {
        return $this->dataTrimestralidade;
    }
    
	function getValorTrimestralidade() {
        return $this->valorTrimestralidade;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }
    
    function setIdTrimestralidade($idTrimestralidade) {
        $this->idTrimestralidade = $idTrimestralidade;
    }
    
	function setDataTrimestralidade($dataTrimestralidade) {
        $this->dataTrimestralidade = $dataTrimestralidade;
    }
    
	function setValorTrimestralidade($valorTrimestralidade) {
        $this->valorTrimestralidade = $valorTrimestralidade;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }
    
    //--------------------------------------------------------------------------

    public function cadastroTrimestralidade(){

    	$sql = "INSERT INTO trimestralidade  
                                 (
                                  dataTrimestralidade,
                                  valorTrimestralidade,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                            		:dataTrimestralidade,
                            		:valorTrimestralidade,
                            		:usuario,
                                    now()
                                    )";
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getDataTrimestralidade             = $this->getDataTrimestralidade();
        $getValorTrimestralidade            = $this->getValorTrimestralidade();
        $getUsuario                         = $this->getUsuario();

        $sth->bindParam(':dataTrimestralidade', $getDataTrimestralidade, PDO::PARAM_STR);
        $sth->bindParam(':valorTrimestralidade', $getValorTrimestralidade, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $getUsuario, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }
    
	public function atualizaTrimestralidade($id){

        $sql="UPDATE trimestralidade SET  
                                  dataTrimestralidade = :dataTrimestralidade,
                                  valorTrimestralidade = :valorTrimestralidade,
                                  ultimoAtualizar = :ultimoAtualizar    
                                  WHERE idTrimestralidade = :id
                            ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getDataTrimestralidade             = $this->getDataTrimestralidade();
        $getValorTrimestralidade            = $this->getValorTrimestralidade();
        $getUltimoAtualizar                 = $this->getUltimoAtualizar();

        $sth->bindParam(':dataTrimestralidade', $getDataTrimestralidade, PDO::PARAM_STR);
        $sth->bindParam(':valorTrimestralidade', $getValorTrimestralidade, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $getUltimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idTrimestralidade) as lastid FROM trimestralidade";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	
    
    public function listaTrimestralidade(){

    	$sql = "SELECT * FROM trimestralidade";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}

?>
