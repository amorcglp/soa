<?php

require_once ("conexaoClass.php");

class PlanoAcaoOrganismo {

    private $idPlanoAcaoOrganismo;
    private $tituloPlano;
    private $statusPlano;
    private $criadoPor;
    private $fk_idOrganismoAfiliado;
    private $dataCriacao;
    private $descricaoPlano;
    private $prioridade;
    private $palavraChave;
    private $ultimoAtualizar;

    function getIdPlanoAcaoOrganismo() {
        return $this->idPlanoAcaoOrganismo;
    }

    function getTituloPlano() {
        return $this->tituloPlano;
    }

    function getStatusPlano() {
        return $this->statusPlano;
    }

    function getCriadoPor() {
        return $this->criadoPor;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function getDescricaoPlano() {
        return $this->descricaoPlano;
    }

    function getPrioridade() {
        return $this->prioridade;
    }

    function getPalavraChave() {
        return $this->palavraChave;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdPlanoAcaoOrganismo($idPlanoAcaoOrganismo) {
        $this->idPlanoAcaoOrganismo = $idPlanoAcaoOrganismo;
    }

    function setTituloPlano($tituloPlano) {
        $this->tituloPlano = $tituloPlano;
    }

    function setStatusPlano($statusPlano) {
        $this->statusPlano = $statusPlano;
    }

    function setCriadoPor($criadoPor) {
        $this->criadoPor = $criadoPor;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function setDescricaoPlano($descricaoPlano) {
        $this->descricaoPlano = $descricaoPlano;
    }

    function setPrioridade($prioridade) {
        $this->prioridade = $prioridade;
    }

    function setPalavraChave($palavraChave) {
        $this->palavraChave = $palavraChave;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

//--------------------------------------------------------------------------

    public function cadastroPlanoAcaoOrganismo() {

        $sql = "INSERT INTO planoAcaoOrganismo  
                                 (tituloPlano,
                                  statusPlano,
                                  criadoPor,
                                  fk_idOrganismoAfiliado,
                                  dataCriacao,
                                  descricaoPlano,
                                  prioridade,
                                  palavrasChave
                                  )
                                   
                            VALUES (
                            		:titulo,
                            		:status,
                            		:criadoPor,
                                    :idOrganismoAfiliado,
                                    now(),
                                    :descricao,
                                    :prioridade,
                                    :palavraChave
                                    )";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->tituloPlano, PDO::PARAM_STR);
        $sth->bindParam(':status', $this->statusPlano, PDO::PARAM_INT);
        $sth->bindParam(':criadoPor',  $this->criadoPor, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoPlano, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $this->prioridade, PDO::PARAM_INT);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
        	$ultimo_id=0;
        	$resultado = $this->selecionaUltimoId();
        	if($resultado)
        	{
        		foreach ($resultado as $vetor)
        		{
        			$ultimo_id = $vetor['lastid']; 
        		}
        	}
        	return $ultimo_id;
   
        }else{
            return false;
        }
    }

    public function alteraStatusPlano() {
        
        $sql = "UPDATE planoAcaoOrganismo SET `statusPlano` = :status
                               WHERE `idPlanoAcaoOrganismo` = :idPlanoAcaoOrganismo";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusPlano, PDO::PARAM_INT);
        $sth->bindParam(':idPlanoAcaoOrganismo', $this->idPlanoAcaoOrganismo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusPlano();
            return $arr;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idPlanoAcaoOrganismo) as lastid FROM planoAcaoOrganismo";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraPlanoAcaoOrganismo($idPlanoAcaoOrganismo) {

        $sql = "UPDATE planoAcaoOrganismo SET
                                    tituloPlano = :titulo,	  
                                    descricaoPlano = :descricao,
                                    palavrasChave = :palavraChave,
                                    prioridade = :prioridade,
                                    ultimoAtualizar = :ultimoAtualizar    
                                  WHERE idPlanoAcaoOrganismo = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);



        $sth->bindParam(':titulo', $this->tituloPlano, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoPlano, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $this->prioridade, PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar',  $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id',  $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        };
    }

    # Seleciona 

    public function listaPlanoAcaoOrganismo($organismo = null,$criadoPor=null,$pesquisar=null,$linha_inicial=null, $qtdRegistros=null) {
            
        $sql = "SELECT * FROM planoAcaoOrganismo as pao "
                . " inner join organismoAfiliado as o on pao.fk_idOrganismoAfiliado = o.idOrganismoAfiliado "
                . " inner join usuario as u on u.seqCadast = pao.criadoPor "
                . " where 1=1 ";
        
        if($pesquisar!=null)
        {
            $sql .= " and ((o.nomeOrganismoAfiliado like :p)"
                    . " or(o.siglaOrganismoAfiliado like :p)"
                    . " or(pao.tituloPlano like :p)"
                    . " or(u.nomeUsuario like :p)"
                    . " or(pao.palavrasChave like :p))";
        }    
        if ($organismo != null) {
            $sql .= " and organismo=:organismo";
        }
        if($criadoPor!=null)
        {
                $sql .= " and pao.criadoPor=:criadoPor";
        }
        if($qtdRegistros>0&&$qtdRegistros!=null)
        {    
            $sql .= "LIMIT ".$linha_inicial.", " . $qtdRegistros;
        }
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($organismo != null) {
            $sth->bindParam(':organismo', $organismo, PDO::PARAM_INT);
        }
        if($criadoPor!=null)
        {
            $sth->bindParam(':criadoPor', $criadoPor, PDO::PARAM_INT);
        }
        if($pesquisar!=null)
        {
            $pesq = "%".$pesquisar."%";
            $sth->bindParam(':p', $pesq, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaExiste() {

        $sql = "SELECT * FROM planoAcaoOrganismo where 1=1 
    			and tituloPlano = :titulo
                        and fk_idOrganismoAfiliado =  :idOrganismoAfiliado
                        and criadoPor = :criadoPor
			and palavrasChave = :palavraChave
                ";
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->tituloPlano, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':criadoPor', $this->criadoPor, PDO::PARAM_INT);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaTituloPlanoAcaoOrganismo() {
        $sql = "SELECT *   FROM    planoAcaoOrganismo
                                    WHERE  tituloPlano LIKE :titulo
                                    ORDER BY tituloPlano ASC";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $getTituloPlano = '%'.str_replace("  "," ",trim($this->getTituloPlano())).'%';
        $sth->bindParam(':titulo', $getTituloPlano, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdPlanoAcaoOrganismo($idPlanoAcaoOrganismo) {
        
        $sql = "SELECT * FROM  planoAcaoOrganismo as a
    				inner join usuario as u on criadoPor = seqCadast
    				inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
                                WHERE   idPlanoAcaoOrganismo = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function excluirPlanoAcaoOrganismo($idPlanoAcaoOrganismo) {
        
        $sql = "DELETE FROM  planoAcaoOrganismo 
                                WHERE   idPlanoAcaoOrganismo = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}

?>