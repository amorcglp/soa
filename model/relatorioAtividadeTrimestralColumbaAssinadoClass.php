<?php
require_once ("conexaoClass.php");

class RelatorioAtividadeTrimestralColumbaAssinado {

    public $idRelatorioAtividadeTrimestralColumbaAssinado;
    public $fk_idOrganismoAfiliado;
    public $trimestre;
    public $ano;
    public $caminho;
    public $usuario;

    /* Funções GET e SET */

    function getIdRelatorioAtividadeTrimestralColumbaAssinado() {
        return $this->idRelatorioAtividadeTrimestralColumbaAssinado;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getTrimestre() {
        return $this->trimestre;
    }

    function getAno() {
        return $this->ano;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setIdRelatorioAtividadeTrimestralColumbaAssinado($idRelatorioAtividadeTrimestralColumbaAssinado) {
        $this->idRelatorioAtividadeTrimestralColumbaAssinado = $idRelatorioAtividadeTrimestralColumbaAssinado;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setTrimestre($trimestre) {
        $this->trimestre = $trimestre;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    
    public function cadastraRelatorioTrimestralColumbaAssinado() {
        
        $sql = "INSERT INTO relatorioAtividadeTrimestralColumbaAssinado 
                                    (fk_idOrganismoAfiliado, 
                                    trimestre, 
                                    ano,
                                    caminho,
                                    usuario,
                                    dataCadastro
                                    )
                                    VALUES (
                                            :id,
                                            :trimestre,
                                            :ano,
                                            '',
                                            :usuario,
                                            now()
                                            )";
        $id=1;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        //$sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $resultado = $this->selecionaUltimoIdInserido();
            if($resultado)
            {
                foreach ($resultado as $vetor)
                {
                    $id = $vetor['idRelatorioAtividadeTrimestralColumbaAssinado'];
                }
            }
        }
        return $id;
    }

    public function alteraRelatorioAtividadeTrimestralColumbaAssinado($id) {

        $sql = "UPDATE relatorioAtividadeTrimestralColumbaAssinado SET 
                            trimestre = :trimestre,
                            ano = :ano,
                            caminho = :caminho
                            WHERE idRelatorioAtividadeTrimestralColumbaAssinado = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function atualizaCaminhoRelatorioAtividadeTrimestralColumbaAssinado($id) {

        $sql = "UPDATE relatorioAtividadeTrimestralColumbaAssinado SET 
                                                 caminho = :caminho
                            WHERE idRelatorioAtividadeTrimestralColumbaAssinado = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){

            return true;
        } else {

            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioAtividadeTrimestralColumbaAssinado'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                    $proximo_id = $vetor['Auto_increment']; 
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM relatorioAtividadeTrimestralColumbaAssinado "
                . " ORDER BY idRelatorioAtividadeTrimestralColumbaAssinado DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRelatorioAtividadeTrimestralColumbaAssinado($trimestre=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioAtividadeTrimestralColumbaAssinado as rfm
    		inner join organismoAfiliado as oa on rfm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on rfm.usuario = u.seqCadast
    	where 1=1 ";
        
    	if($trimestre!=null)
    	{
    		$sql .= " and trimestre=:trimestre";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($trimestre=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioAtividadeTrimestralColumbaAssinado where 1=1 ";
    	if($trimestre!=null)
    	{
    		$sql .= " and trimestre=:trimestre";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($trimestre!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_STR);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':ano', $ano , PDO::PARAM_STR);
        }
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function retornaIdRelatorioAtividadeTrimestralColumbaAssinado() {
    	
    	$sql = "SELECT * FROM relatorioAtividadeTrimestralColumbaAssinado 
                                 WHERE `trimestre` = :trimestre
                                 and `ano` = :ano
                                 and `fk_idOrganismoAfiliado` =  :idOrganismoAfiliado
                                 ";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_STR);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
		
        $id=0;
        if ($resultado) {
        	foreach ($resultado as $vetor)
        	{
        		$id = $vetor['idRelatorioAtividadeTrimestralColumbaAssinado'];
        	}
        }
        return $id;
    }

    public function buscarIdRelatorioAtividadeTrimestralColumbaAssinado($idRelatorioAtividadeTrimestralColumbaAssinado) {

        $sql = "SELECT * FROM relatorioAtividadeTrimestralColumbaAssinado 
                WHERE `idRelatorioAtividadeTrimestralColumbaAssinado` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioAtividadeTrimestralColumbaAssinado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioAtividadeTrimestralColumbaAssinado($id){
        
        $sql="DELETE FROM relatorioAtividadeTrimestralColumbaAssinado "
                . " WHERE idRelatorioAtividadeTrimestralColumbaAssinado = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}
?>