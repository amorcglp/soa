<?php

class criaSessao {

    public function __construct() {
        @session_start();
    }

    public function setValue($var, $value) {
        $_SESSION[$var] = $value;
    }

    public function getValue($var) {
        if (isset($_SESSION[$var])) {

            return $_SESSION[$var];
        } else {
            return false;
        }
    }

    public function freeSession() {

        $_SESSION = array();
        @session_destroy();
    }

}

?>