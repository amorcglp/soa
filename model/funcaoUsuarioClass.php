<?php
require_once ("consultaClass.php");

class FuncaoUsuario {

    public $fk_idFuncao;
    public $fk_seq_cadast;
    public $dataInicioMandato;
    public $dataFimMandato;
    public $siglaOA;
    
    /* Funções GET e SET */

    function getFk_idFuncao() {
        return $this->fk_idFuncao;
    }

    function getFk_seq_cadast() {
        return $this->fk_seq_cadast;
    }

    function getDataInicioMandato() {
        return $this->dataInicioMandato;
    }

    function getDataFimMandato() {
        return $this->dataFimMandato;
    }

    function getSiglaOA() {
        return $this->siglaOA;
    }

    function setFk_idFuncao($fk_idFuncao) {
        $this->fk_idFuncao = $fk_idFuncao;
    }

    function setFk_seq_cadast($fk_seq_cadast) {
        $this->fk_seq_cadast = $fk_seq_cadast;
    }

    function setDataInicioMandato($dataInicioMandato) {
        $this->dataInicioMandato = $dataInicioMandato;
    }

    function setDataFimMandato($dataFimMandato) {
        $this->dataFimMandato = $dataFimMandato;
    }

    function setSiglaOA($siglaOA) {
        $this->siglaOA = $siglaOA;
    }

    
    public function cadastra() {
        
    	$sql = "INSERT INTO funcao_usuario 
                    (`fk_idFuncao`, `fk_seq_cadast`,`dataInicioMandato`,`dataFimMandato`,`siglaOA`)
                    VALUES (:fk_idFuncao,
                            :fk_seq_cadast,
                            :dataInicioMandato,
                            :dataFimMandato,"
                         . ":siglaOA"
        . ")";
    	//echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadast', $this->fk_seq_cadast, PDO::PARAM_INT);
        $sth->bindParam(':dataInicioMandato', $this->dataInicioMandato, PDO::PARAM_STR);
        $sth->bindParam(':dataFimMandato', $this->dataFimMandato, PDO::PARAM_STR);
        $sth->bindParam(':siglaOA', $this->siglaOA, PDO::PARAM_STR);  

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function atualiza() {
        
    	$sql = "UPDATE funcao_usuario SET `dataInicioMandato`=':dataInicioMandato',
                                    `dataFimMandato`=:dataFimMandato,
                                    `siglaOA`=:dataFimMandato,    
                                    WHERE `fk_idFuncao`=:fk_idFuncao 
                                    AND `fk_seq_cadast`=:fk_seq_cadast
                                    ";
    	//echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadast', $this->fk_seq_cadast, PDO::PARAM_INT);
        $sth->bindParam(':dataInicioMandato', $this->dataInicioMandato, PDO::PARAM_STR);
        $sth->bindParam(':dataFimMandato', $this->dataFimMandato, PDO::PARAM_STR);
        $sth->bindParam(':siglaOA', $this->siglaOA, PDO::PARAM_STR);   

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function remove($data) {
        
        $sql = "DELETE FROM funcao_usuario 
                    WHERE fk_idFuncao = :fk_idFuncao
                    and fk_seq_cadast = :fk_seq_cadast
                    and dataFimMandato < :dataFimMandato
                    ";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        $sth->bindParam(':fk_idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadast', $this->fk_seq_cadast, PDO::PARAM_INT);
        $sth->bindParam(':dataFimMandato', $this->dataFimMandato, PDO::PARAM_STR);
        
        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function removePorUsuario() {
        
        $sql = "DELETE FROM funcao_usuario "
                . "WHERE fk_seq_cadast = :fk_seq_cadast";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_seq_cadast', $this->fk_seq_cadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }   
       
    }
    /*
    public function limpaFuncaoUsuario() {
        
        $sql = "TRUNCATE funcao_usuario";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    */
    public function verificaSeJaExiste() {
        
        $sql = "SELECT * FROM funcao_usuario
                                 WHERE `fk_idFuncao` = :fk_idFuncao
                                 and fk_seq_cadast = :fk_seq_cadast
                              ";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadast', $this->fk_seq_cadast, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }   
    }

    public function buscaPorUsuario() {
        
    	$sql = "SELECT * FROM funcao_usuario
                                 WHERE `fk_seq_cadast` = :fk_seq_cadast
                              ";
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_seq_cadast', $this->fk_seq_cadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
            
        $arr = array();
        $i=0;
        $string="0";
        
        foreach($resultado as $vetor)
        {
        	if($i==0)
        	{
        		$string = $vetor['fk_idFuncao'];
        	}else{
        		$string .= ",".$vetor['fk_idFuncao'];
        	}
        	$i++;
        }
        
        return $string;
    }
    
    public function buscaPorFuncao($seqCadast) {
        
    	$sql = "SELECT * FROM funcao_usuario as fu
    				inner join funcao as f on fk_idFuncao = f.idFuncao
                                 WHERE `fk_seq_cadast` = :fk_seq_cadast LIMIT 1
                              ";
    	//echo $sql;
	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_seq_cadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaPorFuncaoPorLogin($login) {

        $sql = "SELECT f.nomeFuncao FROM funcao as f 
          left join funcao_usuario as fu on fk_idFuncao = f.idFuncao 
          left join usuario as u on fu.fk_seq_cadast = u.seqCadast 
          WHERE u.`loginUsuario` = :login                              ";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':login', $login, PDO::PARAM_STR);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaPorMaisDeUmaFuncao($seqCadast) {
        
    	$sql = "SELECT * FROM funcao_usuario as fu
    				left join funcao as f on fk_idFuncao = f.idFuncao
                                 WHERE `fk_seq_cadast` = :fk_seq_cadast
                                 and statusFuncao = 0
                              ";
    	//echo $sql;
	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_seq_cadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaExistePorParametro($seqCadast, $funcao) {
        
        $sql = "SELECT * FROM funcao_usuario
                                 WHERE `fk_idFuncao` = :funcao
                                 and fk_seq_cadast = :fk_seq_cadast
                              ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_seq_cadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);

        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function retornaDadosPorFuncaoPorUsuario($seqCadast, $funcao) {

        $sql = "SELECT * FROM funcao_usuario
                                 WHERE `fk_idFuncao` = :funcao
                                 and fk_seq_cadast = :fk_seq_cadast
                              ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_seq_cadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}
?>