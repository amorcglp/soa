<?php

require_once ("conexaoClass.php");

class imovelControle {

    private $idImovelControle;
    private $referenteAnoImovelControle;
    private $propriedadeIptuImovelControle;
    private $propriedadeIptuObsImovelControle;
    private $propriedadeIptuAnexoImovelControle;
    private $construcaoImovelControle;
    private $construcaoAlvaraImovelControle;
    private $construcaoAlvaraAnexoImovelControle;
    private $construcaoRestricoesImovelControle;
    private $construcaoRestricoesAnotacoesImovelControle;
    private $construcaoConcluidaImovelControle;
    private $construcaoTerminoDataImovelControle;
    private $construcaoInssImovelControle;
    private $construcaoInssAnexoImovelControle;
    private $manutencaoImovelControle;
    private $manutencaoPinturaImovelControle;
    private $manutencaoPinturaTipoImovelControle;
    private $manutencaoHidraulicaImovelControle;
    private $manutencaoHidraulicaTipoImovelControle;
    private $manutencaoEletricaImovelControle;
    private $manutencaoEletricaTipoImovelControle;
    private $anotacoesImovelControle;
    private $statusImovelControle;
    private $fk_seqCadastAtualizadoPor;
    private $fk_idOrganismoAfiliado;
    private $fk_idImovel;

    //--- GETERS -----------------------------------------------------
    function getIdImovelControle() {
        return $this->idImovelControle;
    }

    function getReferenteAnoImovelControle() {
        return $this->referenteAnoImovelControle;
    }

    function getPropriedadeIptuImovelControle() {
        return $this->propriedadeIptuImovelControle;
    }

    function getPropriedadeIptuObsImovelControle() {
        return $this->propriedadeIptuObsImovelControle;
    }

    function getPropriedadeIptuAnexoImovelControle() {
        return $this->propriedadeIptuAnexoImovelControle;
    }

    function getConstrucaoImovelControle() {
        return $this->construcaoImovelControle;
    }

    function getConstrucaoAlvaraImovelControle() {
        return $this->construcaoAlvaraImovelControle;
    }

    function getConstrucaoAlvaraAnexoImovelControle() {
        return $this->construcaoAlvaraAnexoImovelControle;
    }

    function getConstrucaoRestricoesImovelControle() {
        return $this->construcaoRestricoesImovelControle;
    }

    function getConstrucaoRestricoesAnotacoesImovelControle() {
        return $this->construcaoRestricoesAnotacoesImovelControle;
    }

    function getConstrucaoConcluidaImovelControle() {
        return $this->construcaoConcluidaImovelControle;
    }

    function getConstrucaoTerminoDataImovelControle() {
        return $this->construcaoTerminoDataImovelControle;
    }

    function getConstrucaoInssImovelControle() {
        return $this->construcaoInssImovelControle;
    }

    function getConstrucaoInssAnexoImovelControle() {
        return $this->construcaoInssAnexoImovelControle;
    }

    function getManutencaoImovelControle() {
        return $this->manutencaoImovelControle;
    }

    function getManutencaoPinturaImovelControle() {
        return $this->manutencaoPinturaImovelControle;
    }

    function getManutencaoPinturaTipoImovelControle() {
        return $this->manutencaoPinturaTipoImovelControle;
    }

    function getManutencaoHidraulicaImovelControle() {
        return $this->manutencaoHidraulicaImovelControle;
    }

    function getManutencaoHidraulicaTipoImovelControle() {
        return $this->manutencaoHidraulicaTipoImovelControle;
    }

    function getManutencaoEletricaImovelControle() {
        return $this->manutencaoEletricaImovelControle;
    }

    function getManutencaoEletricaTipoImovelControle() {
        return $this->manutencaoEletricaTipoImovelControle;
    }

    function getAnotacoesImovelControle() {
        return $this->anotacoesImovelControle;
    }

    function getStatusImovelControle() {
        return $this->statusImovelControle;
    }

    function getFk_seqCadastAtualizadoPor() {
        return $this->fk_seqCadastAtualizadoPor;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getFk_idImovel() {
        return $this->fk_idImovel;
    }

    //--- SETERS -----------------------------------------------------
    function setIdImovelControle($idImovelControle) {
        $this->idImovelControle = $idImovelControle;
    }

    function setReferenteAnoImovelControle($referenteAnoImovelControle) {
        $this->referenteAnoImovelControle = $referenteAnoImovelControle;
    }

    function setPropriedadeIptuImovelControle($propriedadeIptuImovelControle) {
        $this->propriedadeIptuImovelControle = $propriedadeIptuImovelControle;
    }

    function setPropriedadeIptuObsImovelControle($propriedadeIptuObsImovelControle) {
        $this->propriedadeIptuObsImovelControle = $propriedadeIptuObsImovelControle;
    }

    function setPropriedadeIptuAnexoImovelControle($propriedadeIptuAnexoImovelControle) {
        $this->propriedadeIptuAnexoImovelControle = $propriedadeIptuAnexoImovelControle;
    }

    function setConstrucaoImovelControle($construcaoImovelControle) {
        $this->construcaoImovelControle = $construcaoImovelControle;
    }

    function setConstrucaoAlvaraImovelControle($construcaoAlvaraImovelControle) {
        $this->construcaoAlvaraImovelControle = $construcaoAlvaraImovelControle;
    }

    function setConstrucaoAlvaraAnexoImovelControle($construcaoAlvaraAnexoImovelControle) {
        $this->construcaoAlvaraAnexoImovelControle = $construcaoAlvaraAnexoImovelControle;
    }

    function setConstrucaoRestricoesImovelControle($construcaoRestricoesImovelControle) {
        $this->construcaoRestricoesImovelControle = $construcaoRestricoesImovelControle;
    }

    function setConstrucaoRestricoesAnotacoesImovelControle($construcaoRestricoesAnotacoesImovelControle) {
        $this->construcaoRestricoesAnotacoesImovelControle = $construcaoRestricoesAnotacoesImovelControle;
    }

    function setConstrucaoConcluidaImovelControle($construcaoConcluidaImovelControle) {
        $this->construcaoConcluidaImovelControle = $construcaoConcluidaImovelControle;
    }

    function setConstrucaoTerminoDataImovelControle($construcaoTerminoDataImovelControle) {
        $this->construcaoTerminoDataImovelControle = $construcaoTerminoDataImovelControle;
    }

    function setConstrucaoInssImovelControle($construcaoInssImovelControle) {
        $this->construcaoInssImovelControle = $construcaoInssImovelControle;
    }

    function setConstrucaoInssAnexoImovelControle($construcaoInssAnexoImovelControle) {
        $this->construcaoInssAnexoImovelControle = $construcaoInssAnexoImovelControle;
    }

    function setManutencaoImovelControle($manutencaoImovelControle) {
        $this->manutencaoImovelControle = $manutencaoImovelControle;
    }

    function setManutencaoPinturaImovelControle($manutencaoPinturaImovelControle) {
        $this->manutencaoPinturaImovelControle = $manutencaoPinturaImovelControle;
    }

    function setManutencaoPinturaTipoImovelControle($manutencaoPinturaTipoImovelControle) {
        $this->manutencaoPinturaTipoImovelControle = $manutencaoPinturaTipoImovelControle;
    }

    function setManutencaoHidraulicaImovelControle($manutencaoHidraulicaImovelControle) {
        $this->manutencaoHidraulicaImovelControle = $manutencaoHidraulicaImovelControle;
    }

    function setManutencaoHidraulicaTipoImovelControle($manutencaoHidraulicaTipoImovelControle) {
        $this->manutencaoHidraulicaTipoImovelControle = $manutencaoHidraulicaTipoImovelControle;
    }

    function setManutencaoEletricaImovelControle($manutencaoEletricaImovelControle) {
        $this->manutencaoEletricaImovelControle = $manutencaoEletricaImovelControle;
    }

    function setManutencaoEletricaTipoImovelControle($manutencaoEletricaTipoImovelControle) {
        $this->manutencaoEletricaTipoImovelControle = $manutencaoEletricaTipoImovelControle;
    }

    function setAnotacoesImovelControle($anotacoesImovelControle) {
        $this->anotacoesImovelControle = $anotacoesImovelControle;
    }

    function setStatusImovelControle($statusImovelControle) {
        $this->statusImovelControle = $statusImovelControle;
    }

    function setFk_seqCadastAtualizadoPor($fk_seqCadastAtualizadoPor) {
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setFk_idImovel($fk_idImovel) {
        $this->fk_idImovel = $fk_idImovel;
    }

    //--------------------------------------------------------------------------

    public function cadastroImovelControle() {

        $query = "INSERT INTO imovelControle
                                 (referenteAnoImovelControle,
                                  propriedadeIptuImovelControle,
                                  propriedadeIptuObsImovelControle,
                                  construcaoImovelControle,
                                  construcaoRestricoesImovelControle,
                                  construcaoRestricoesAnotacoesImovelControle,
                                  construcaoConcluidaImovelControle,
                                  construcaoTerminoDataImovelControle,
                                  manutencaoImovelControle,
                                  manutencaoPinturaImovelControle,
                                  manutencaoPinturaTipoImovelControle,
                                  manutencaoHidraulicaImovelControle,
                                  manutencaoHidraulicaTipoImovelControle,
                                  manutencaoEletricaImovelControle,
                                  manutencaoEletricaTipoImovelControle,
                                  anotacoesImovelControle,
                                  fk_seqCadastAtualizadoPor,
                                  fk_idOrganismoAfiliado,
                                  fk_idImovel)
                            VALUES (:referente,
                                    :propriedadeIptu,
                                    :propriedadeIptuObs,
                                    :construcao,
                                    :construcaoRestricoes,
                                    :construcaoRestricoesAnotacoes,
                                    :construcaoConcluida,
                                    :construcaoTerminoData,
                                    :manutencao,
                                    :manutencaoPintura,
                                    :manutencaoPinturaTipo,
                                    :manutencaoHidraulica,
                                    :manutencaoHidraulicaTipo,
                                    :manutencaoEletrica,
                                    :manutencaoEletricaTipo,
                                    :anotacoes,
                                    :seqCadastAtualizadoPor,
                                    :idOrganismoAfiliado,
                                    :idImovel)";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($query);
        
        $sth->bindParam(':referente', $this->referenteAnoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':propriedadeIptu', $this->propriedadeIptuImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':propriedadeIptuObs', $this->propriedadeIptuObsImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':construcao', $this->construcaoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':construcaoRestricoes', $this->construcaoRestricoesImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':construcaoRestricoesAnotacoes', $this->construcaoRestricoesAnotacoesImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':construcaoConcluida', $this->construcaoConcluidaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':construcaoTerminoData', $this->construcaoTerminoDataImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':manutencao', $this->manutencaoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoPintura', $this->manutencaoPinturaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoPinturaTipo', $this->manutencaoPinturaTipoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoHidraulica', $this->manutencaoHidraulicaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoHidraulicaTipo', $this->manutencaoHidraulicaTipoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoEletrica', $this->manutencaoEletricaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoEletricaTipo', $this->manutencaoEletricaTipoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':anotacoes', $this->anotacoesImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':idImovel', $this->fk_idImovel, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaIdImovelControle($idImovelControle) 
    {    
        $sql = "SELECT * FROM  imovelControle
                                WHERE   idImovelControle = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idImovelControle, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaImovelControlePeloImovelAno($idImovel, $data) {
        
        $sql = "SELECT * FROM  imovelControle
                                WHERE fk_idImovel = :id and referenteAnoImovelControle = :data";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idImovel, PDO::PARAM_INT);
        $sth->bindParam(':data', $data, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraImovelControle($idImovelControle) {

        $query = "UPDATE imovelControle SET
                                  `referenteAnoImovelControle` = :referente,
                                  `propriedadeIptuImovelControle` = :propriedadeIptu,
                                  `propriedadeIptuObsImovelControle` = :propriedadeIptuObs";

        //if ($this->getPropriedadeIptuAnexoImovelControle() != "") {$query .= ", `propriedadeIptuAnexoImovelControle` = '" . $this->getPropriedadeIptuAnexoImovelControle() . "'";}

        $query .= ", `construcaoImovelControle` = :construcao";

        //if ($this->getConstrucaoAlvaraAnexoImovelControle() != "") {$query .= ", `construcaoAlvaraAnexoImovelControle` = '" . $this->getConstrucaoAlvaraAnexoImovelControle() . "'";}

        $query .= ", `construcaoRestricoesImovelControle` = :construcaoRestricoes,
                                  `construcaoRestricoesAnotacoesImovelControle` = :construcaoRestricoesAnotacoes,
                                  `construcaoConcluidaImovelControle` = :construcaoConcluida,
                                  `construcaoTerminoDataImovelControle` = :construcaoTerminoData";

        //if ($this->getConstrucaoInssAnexoImovelControle() != "") { $query .= ", `construcaoInssAnexoImovelControle` = '" . $this->getConstrucaoInssAnexoImovelControle() . "'";}

        $query .= ", `manutencaoImovelControle` = :manutencao,
                                  `manutencaoPinturaImovelControle` = :manutencaoPintura,
                                  `manutencaoPinturaTipoImovelControle` = :manutencaoPinturaTipo,
                                  `manutencaoHidraulicaImovelControle` = :manutencaoHidraulica,
                                  `manutencaoHidraulicaTipoImovelControle` = :manutencaoHidraulicaTipo,
                                  `manutencaoEletricaImovelControle` = :manutencaoEletrica,
                                  `manutencaoEletricaTipoImovelControle` = :manutencaoEletricaTipo,
                                  `anotacoesImovelControle` = :anotacoes,
                                  `fk_seqCadastAtualizadoPor` = :seqCadastAtualizadoPor,
                                  `fk_idOrganismoAfiliado` = :idOrganismoAfiliado,
                                  `fk_idImovel` = :idImovel
                                  WHERE idImovelControle = :idImovelControle";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($query);
        
        $sth->bindParam(':referente', $this->referenteAnoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':propriedadeIptu', $this->propriedadeIptuImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':propriedadeIptuObs', $this->propriedadeIptuObsImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':construcao', $this->construcaoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':construcaoRestricoes', $this->construcaoRestricoesImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':construcaoRestricoesAnotacoes', $this->construcaoRestricoesAnotacoesImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':construcaoConcluida', $this->construcaoConcluidaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':construcaoTerminoData', $this->construcaoTerminoDataImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':manutencao', $this->manutencaoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoPintura', $this->manutencaoPinturaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoPinturaTipo', $this->manutencaoPinturaTipoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoHidraulica', $this->manutencaoHidraulicaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoHidraulicaTipo', $this->manutencaoHidraulicaTipoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoEletrica', $this->manutencaoEletricaImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':manutencaoEletricaTipo', $this->manutencaoEletricaTipoImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':anotacoes', $this->anotacoesImovelControle, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':idImovel', $this->fk_idImovel, PDO::PARAM_INT);
        $sth->bindParam(':idImovelControle', $idImovelControle, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        };
    }

    public function listaImovelControle($idOrganismoAfiliado, $data) 
    {

        $sql = "SELECT * FROM imovelControle";

        if ($idOrganismoAfiliado != "") {
            $sql .= " WHERE fk_idOrganismoAfiliado = :id "
                    . " and referenteAnoImovelControle=:data";
        }
        $sql .= " ORDER BY idImovelControle ASC";

        //echo $query."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($idOrganismoAfiliado != "") {
            $sth->bindParam(':id',  $idOrganismoAfiliado, PDO::PARAM_INT);
            $sth->bindParam(':data',  $data, PDO::PARAM_INT);
        }
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaImovelControle($idOrganismoAfiliado, $data) 
    {

        $sql = "SELECT * FROM imovelControle";

        if ($idOrganismoAfiliado != "") {
            $sql .= " WHERE fk_idOrganismoAfiliado = :id "
                    . " and referenteAnoImovelControle=:data";
        }
        $sql .= " ORDER BY idImovelControle ASC";

        //echo $query."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($idOrganismoAfiliado != "") {
            $sth->bindParam(':id',  $idOrganismoAfiliado, PDO::PARAM_INT);
            $sth->bindParam(':data',  $data, PDO::PARAM_INT);
        }
        $resultado = $c->run($sth);
        if ($resultado){
            return true;
        }else{
            return false;
        }
    }

    public function listaImovelPeloOA($oa) {

        $sql = "SELECT * FROM imovel where fk_idOrganismoAfiliado = :id ORDER BY idImovel ASC";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id',  $oa, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaAnosJaCadastrados($fk_idImovel) {

        $sql = "SELECT referenteAnoImovelControle FROM imovelControle "
                . " where fk_idImovel=:id ORDER BY referenteAnoImovelControle DESC";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id',  $fk_idImovel, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraStatusImovelControle() {

        $sql = "UPDATE imovelControle SET `statusImovelControle` = :status "
                . " WHERE `idImovelControle` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idImovelControle, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusImovelControle();
            return $arr;
        } else {
            return false;
        }
    }

    public function buscaAno($fk_idImovel, $referenteAnoImovelControle) {

        $sql = "SELECT referenteAnoImovelControle FROM imovelControle where fk_idImovel=:idImovel and referenteAnoImovelControle=:ano";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idImovel', $fk_idImovel, PDO::PARAM_INT);
        $sth->bindParam(':ano', $referenteAnoImovelControle, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaImovelControleAnexo($fk_idImovelControle, $fk_idImovelAnexoTipo){

        $sql = "SELECT * FROM imovelControleAnexo
                  WHERE excluido = '1'
                  and fk_idImovelControle = :idImovelControle
                  and fk_idImovelAnexoTipo = :idAnexoTipo
                  ORDER BY idImovelControleAnexo DESC";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idImovelControle',  $fk_idImovelControle, PDO::PARAM_INT);
        $sth->bindParam(':idAnexoTipo',  $fk_idImovelAnexoTipo, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function excluiImovelControleAnexo($idAnexo){
        
        $sql="UPDATE imovelControleAnexo SET excluido='0' "
                . " WHERE idImovelControleAnexo = :id ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idAnexo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function cadastroImovelControleAnexo($nome_original,$nome_arquivo,$ext,$full_path,$data,$excluido,$idImovelAnexoTipo,$fk_idImovelControle){

        $sql = "INSERT INTO imovelControleAnexo (nome_original,nome_arquivo,ext,full_path,data,excluido,fk_idImovelAnexoTipo,fk_idImovelControle)
                            VALUES (:nomeOriginal,
                                    :nomeArquivo,
                                    :ext,
                                    :fullPath,
                                    :data,
                                    :excluido,
                                    :idImovelAnexoTipo,
                                    :idImovelControle)";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nomeOriginal',$nome_original, PDO::PARAM_STR);
        $sth->bindParam(':nomeArquivo', $nome_arquivo, PDO::PARAM_STR);
        $sth->bindParam(':ext', $ext, PDO::PARAM_STR);
        $sth->bindParam(':fullPath', $full_path, PDO::PARAM_STR);
        $sth->bindParam(':data', $data, PDO::PARAM_STR);
        $sth->bindParam(':excluido', $excluido, PDO::PARAM_INT);
        $sth->bindParam(':idImovelAnexoTipo', $idImovelAnexoTipo, PDO::PARAM_INT);
        $sth->bindParam(':idImovelControle', $fk_idImovelControle, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}

?>
