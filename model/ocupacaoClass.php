<?php

require_once ("conexaoClass.php");

class ocupacao {

    public $SeqOcupac;
    public $DesOcupac;
    
    function getSeqOcupac() {
        return $this->SeqOcupac;
    }

    function getDesOcupac() {
        return $this->DesOcupac;
    }

    function setSeqOcupac($SeqOcupac) {
        $this->SeqOcupac = $SeqOcupac;
    }

    function setDesOcupac($DesOcupac) {
        $this->DesOcupac = $DesOcupac;
    }

        
    public function lista($cod=null) {
        
        $sql = "SELECT * FROM ocupacao where 1=1 ";
        if($cod!=null)
        {
            $sql .= " and SeqOcupac=:cod";
        } 
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($cod!=null)
        {
            $sth->bindParam(':cod', $cod , PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>