<?php

require_once ("conexaoClass.php");

class NotificacaoServer {

    public $idNotificacao;
    public $tituloNotificacao;
    public $dataNotificacao;
    public $descricaoNotificacao;
    public $tipoNotificacao;
    public $statusNotificacao;
    public $dataAgendada;

    /* Funções GET e SET */

    function getIdNotificacao() {
        return $this->idNotificacao;
    }

    function getTituloNotificacao() {
        return $this->tituloNotificacao;
    }

    function getDataNotificacao() {
        return $this->dataNotificacao;
    }

    function getDescricaoNotificacao() {
        return $this->descricaoNotificacao;
    }

    function getTipoNotificacao() {
        return $this->tipoNotificacao;
    }

    function getStatusNotificacao() {
        return $this->statusNotificacao;
    }

    function getDataAgendada() {
        return $this->dataAgendada;
    }

    function setIdNotificacao($idNotificacao) {
        $this->idNotificacao = $idNotificacao;
    }

    function setTituloNotificacao($tituloNotificacao) {
        $this->tituloNotificacao = $tituloNotificacao;
    }

    function setDataNotificacao($dataNotificacao) {
        $this->dataNotificacao = $dataNotificacao;
    }

    function setDescricaoNotificacao($descricaoNotificacao) {
        $this->descricaoNotificacao = $descricaoNotificacao;
    }

    function setTipoNotificacao($tipoNotificacao) {
        $this->tipoNotificacao = $tipoNotificacao;
    }

    function setStatusNotificacao($statusNotificacao) {
        $this->statusNotificacao = $statusNotificacao;
    }

    function setDataAgendada($dataAgendada) {
        $this->dataAgendada = $dataAgendada;
    }

    
    public function cadastraNotificacaoServer() {
        
        $sql = "INSERT INTO notificacoesServer (`tituloNotificacao`, 
        			`dataNotificacao`, `descricaoNotificacao`,`tipoNotificacao`,`statusNotificacao`,`dataAgendada`)
                                    VALUES (
                                            :titulo,
                                            now(),
                                            :descricao,
                                            :tipo,
                                            '1',
                                            :dataAgendada)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $id =$this->getIdNotificacao();
        $titulo =$this->getTituloNotificacao();
        $descricao =$this->getDescricaoNotificacao();
        $tipo =$this->getTipoNotificacao();
        $dataAgendada =$this->getDataAgendada();
        
        //$sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':titulo', $titulo , PDO::PARAM_STR);
        $sth->bindParam(':descricao', $descricao, PDO::PARAM_STR);
        $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        $sth->bindParam(':dataAgendada', $dataAgendada, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraNotificacaoServer($id) {

        $sql = "UPDATE notificacoesServer SET `tituloNotificacao` = :titulo,
                                                 `descricaoNotificacao` = :descricao,
                                                 `tipoNotificacao` = :tipo,
                                                 `dataAgendada` = :dataAgendada
                            WHERE `idNotificacao` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $titulo =$this->getTituloNotificacao();
        $descricao =$this->getDescricaoNotificacao();
        $tipo =$this->getTipoNotificacao();
        $dataAgendada =$this->getDataAgendada();
        
        $sth->bindParam(':titulo', $titulo , PDO::PARAM_STR);
        $sth->bindParam(':descricao', $descricao, PDO::PARAM_STR);
        $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':dataAgendada', $dataAgendada, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraStatusNotificacaoServer() {
        
        $sql = "UPDATE notificacoesServer SET `statusNotificacao` = :status
                               WHERE `idNotificacao` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $status =$this->getStatusNotificacao();
        $id =$this->getIdNotificacao();
        
        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $status;
            return $arr;
        }else{
            return false;
        }
    }

    public function listaNotificacaoServer($ativo=null,$data=null,$igualDataNotificacao=null) {
        //echo "teste";
    	$sql = "SELECT * FROM notificacoesServer where 1=1 ";
    	if($ativo!=null)
    	{
    		$sql .= " and statusNotificacao = 0 ";
    	}
        if($data!=null)
    	{
    		$sql .= " and dataAgendada = :data";
    	}
        
        if($igualDataNotificacao!=null)
    	{
    		//$sql .= " and DATE(dataNotificacao) = :igualDataNotificacao";
                $sql .= " group by idNotificacao";
                $sql .= " order by idNotificacao";
    	}
        
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($data!=null)
    	{
            $sth->bindParam(':data', $data, PDO::PARAM_STR);
        }
        /*
        if($igualDataNotificacao!=null)
    	{
            $sth->bindParam(':igualDataNotificacao', $data, PDO::PARAM_STR);
        }
        */
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeNotificacao($idNotificacao) {
        
        $sql = "DELETE FROM notificacoesServer WHERE `idNotificacao` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idNotificacao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaTituloNotificacaoServer() {
        
        $sql = "SELECT * FROM notificacoesServer
                                 WHERE `tituloNotificacao` LIKE :titulo
                              ORDER BY `tituloNotificacao` ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $titulo = '%'.$this->tituloNotificacao.'%';
        
        $sth->bindParam(':titulo', $titulo, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdNotificacaoServer($id) {
        
        $sql = "SELECT * FROM notificacoesServer 
                                 WHERE `idNotificacao` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>