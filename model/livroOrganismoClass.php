<?php

require_once ("conexaoClass.php");

class livroOrganismo{

    private $idLivroOrganismo;
    private $fk_idOrganismoAfiliado;
    private $titulo;
    private $assunto;
    private $descricao;
    private $palavraChave;    
    private $usuario;
    private $usuarioAlteracao;

    //METODO SET/GET -----------------------------------------------------------

    function getIdLivroOrganismo() {
        return $this->idLivroOrganismo;
    }
    
 	function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getAssunto() {
        return $this->assunto;
    }

    function getDescricao() {
        return $this->descricao;
    }
    
	function getPalavraChave() {
        return $this->palavraChave;
    }

	function getUsuario() {
        return $this->usuario;
    }
    
	function getUsuarioAlteracao() {
        return $this->usuarioAlteracao;
    }
    
	function setIdLivroOrganismo($idLivroOrganismo) {
        $this->idLivroOrganismo = $idLivroOrganismo;
    }
    
	function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }
    
	function setPalavraChave($palavraChave) {
        $this->palavraChave = $palavraChave;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
	function setUsuarioAlteracao($usuarioAlteracao) {
        $this->usuarioAlteracao = $usuarioAlteracao;
    }
    
    //--------------------------------------------------------------------------

    public function cadastroLivroOrganismo(){

        $sql="INSERT INTO livroOrganismo  
                                 (fk_idOrganismoAfiliado,
                                  titulo,
                                  assunto,
                                  descricao,
                                  palavraChave,
                                  usuario,
                                  usuarioAlteracao,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                                    :idOrganismoAfiliado,
                                    :titulo,
                                    :assunto,
                                    :descricao,
                                    :palavraChave,
                                    :usuario,
                                    '0',
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':assunto',  $this->assunto, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        $sth->bindParam(':usuario',  $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
        	return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idLivroOrganismo) as lastid FROM livroOrganismo";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	

    public function alteraLivroOrganismo($idLivroOrganismo){
    	
        $sql = "UPDATE livroOrganismo SET  
                                titulo = :titulo,
                                assunto = :assunto,
                                descricao = :descricao,
                                palavraChave = :palavraChave,
                                usuarioAlteracao = :usuarioAlteracao
                                  WHERE idLivroOrganismo = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':assunto',  $this->assunto, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        $sth->bindParam(':usuarioAlteracao',  $this->usuarioAlteracao, PDO::PARAM_INT);
        $sth->bindParam(':id', $idLivroOrganismo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        };
    }

    public function listaLivroOrganismo(){

    	$sql = "SELECT * FROM livroOrganismo as lo
    	inner join organismoAfiliado as o on lo.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
    	inner join usuario as u on u.seqCadast = lo.usuario
    	order by idLivroOrganismo DESC
    	";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaExiste(){

    	$sql = "SELECT * FROM livroOrganismo where 1=1 
    		and titulo = :titulo
                and fk_idOrganismoAfiliado =  :idOrganismoAfiliado
                and assunto = :assunto
		and palavraChave = :palavraChave
                ";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':assunto', $this->assunto, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaTituloLivroOrganismo(){
        
    	$sql = "SELECT *   FROM    livroOrganismo
                                    WHERE  titulo LIKE :titulo
                                    ORDER BY titulo ASC";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $titulo = '%'.str_replace("  "," ",trim($this->getTitulo())).'%';
        $sth->bindParam(':titulo', $titulo, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdLivroOrganismo($idLivroOrganismo){
        
    	$sql="SELECT * FROM  livroOrganismo as a
        		inner join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                        WHERE   idLivroOrganismo = :id";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idLivroOrganismo, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeLivroOrganismo($idLivroOrganismo){
        
        $sql = "DELETE FROM livroOrganismo WHERE idLivroOrganismo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idLivroOrganismo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
