<?php

require_once ("conexaoClass.php");

class convocacaoRitualisticaOGG {

    private $idConvocacaoRitualisticaOGG;
    private $fk_idOrganismoAfiliado;
    private $dataConvocacao;
    private $horaConvocacao;
    private $tituloMensagem;
    private $seqCadastComendador;
    private $nomeComendador;
    private $usuario;
    private $ultimoAtualizar;
    
    
    function getIdConvocacaoRitualisticaOGG() {
        return $this->idConvocacaoRitualisticaOGG;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getDataConvocacao() {
        return $this->dataConvocacao;
    }

    function getHoraConvocacao() {
        return $this->horaConvocacao;
    }

    function getTituloMensagem() {
        return $this->tituloMensagem;
    }

    function getSeqCadastComendador() {
        return $this->seqCadastComendador;
    }

    function getNomeComendador() {
        return $this->nomeComendador;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdConvocacaoRitualisticaOGG($idConvocacaoRitualisticaOGG) {
        $this->idConvocacaoRitualisticaOGG = $idConvocacaoRitualisticaOGG;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setDataConvocacao($dataConvocacao) {
        $this->dataConvocacao = $dataConvocacao;
    }

    function setHoraConvocacao($horaConvocacao) {
        $this->horaConvocacao = $horaConvocacao;
    }

    function setTituloMensagem($tituloMensagem) {
        $this->tituloMensagem = $tituloMensagem;
    }

    function setSeqCadastComendador($seqCadastComendador) {
        $this->seqCadastComendador = $seqCadastComendador;
    }

    function setNomeComendador($nomeComendador) {
        $this->nomeComendador = $nomeComendador;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    public function cadastro(){

        $sql = "INSERT INTO convocacaoRitualisticaOGG 
            (fk_idOrganismoAfiliado, dataConvocacao, horaConvocacao, tituloMensagem, seqCadastComendador, nomeComendador, usuario,dataCadastro) 
        VALUES (
        :fk_idOrganismoAfiliado,
        :dataConvocacao,
        :horaConvocacao,
        :tituloMensagem,
        :seqCadastComendador,
        :nomeComendador,
        :usuario,
        now()
        )";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataConvocacao', $this->dataConvocacao, PDO::PARAM_STR);
        $sth->bindParam(':horaConvocacao', $this->horaConvocacao, PDO::PARAM_STR);
        $sth->bindParam(':tituloMensagem', $this->tituloMensagem, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastComendador', $this->seqCadastComendador, PDO::PARAM_INT);
        $sth->bindParam(':nomeComendador', $this->nomeComendador, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            $ultimo_id = $this->selecionaUltimoIdCadastrado();
            return $ultimo_id;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'convocacaoRitualisticaOGG'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoIdCadastrado(){

        $sql= "SELECT `idConvocacaoRitualisticaOGG` as id FROM `convocacaoRitualisticaOGG` ORDER BY idConvocacaoRitualisticaOGG DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['id'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }
        
    # Seleciona Evento
    public function lista($id=null,$idOrganismoAfiliado=null,$mes=null,$ano=null) 
    {
        $sql = "SELECT * FROM convocacaoRitualisticaOGG where 1=1 ";
        if($id!=null)
        {
                $sql .= " and idConvocacaoRitualisticaOGG=:id";
        }
        if($idOrganismoAfiliado!=null)
        {
                $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }
        if($mes!=null)
        {
                $sql .= " and MONTH(dataConvocacao)=:mes";
        }
        if($ano!=null)
        {
                $sql .= " and YEAR(dataConvocacao)=:ano";
        }
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
        {
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($mes!=null)
        {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null)
        {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
                return $resultado;
        }else{
                return false;
        }
    }
    
    public function remove($idConvocacaoRitualisticaOGG=null){

        $sql= "DELETE FROM convocacaoRitualisticaOGG";

    	if($idConvocacaoRitualisticaOGG!=null) {
    		$sql .= " where idConvocacaoRitualisticaOGG=:fk_idConvocacaoRitualisticaOGG";
    	}
    	
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idConvocacaoRitualisticaOGG!=null){
            $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function altera(){

        $sql = "UPDATE convocacaoRitualisticaOGG SET
            fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado, 
            dataConvocacao=:dataConvocacao,
            horaConvocacao= :horaConvocacao,
            tituloMensagem= :tituloMensagem,
            seqCadastComendador= :seqCadastComendador,
            nomeComendador= :nomeComendador,
            alteradoPorUsuario=:alteradoPorUsuario
            where
            idConvocacaoRitualisticaOGG = :id
        ";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataConvocacao', $this->dataConvocacao, PDO::PARAM_STR);
        $sth->bindParam(':horaConvocacao', $this->horaConvocacao, PDO::PARAM_STR);
        $sth->bindParam(':tituloMensagem', $this->tituloMensagem, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastComendador', $this->seqCadastComendador, PDO::PARAM_INT);
        $sth->bindParam(':nomeComendador', $this->nomeComendador, PDO::PARAM_STR);
        $sth->bindParam(':alteradoPorUsuario', $this->ultimoAtualizar, PDO::PARAM_STR);
        $sth->bindParam(':id', $this->idConvocacaoRitualisticaOGG, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function dataCriacao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM convocacaoRitualisticaOGG where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataConvocacao)=:mes";
        $sql .= " and YEAR(dataConvocacao)=:ano";
        $sql .= " order by dataCadastro ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function dataUltimaAlteracao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM convocacaoRitualisticaOGG where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataConvocacao)=:mes";
        $sql .= " and YEAR(dataConvocacao)=:ano";
        $sql .= " order by dataCadastro ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function responsaveis($mes,$ano, $idOrganismoAfiliado,$ultimaAlteracao=null) {

        if($ultimaAlteracao==null)
        {
            $sql = "SELECT * FROM convocacaoRitualisticaOGG  as r  left join usuario on seqCadast = usuario where 1=1 ";
        }else{
            $sql = "SELECT * FROM convocacaoRitualisticaOGG  as r left join usuario on seqCadast = alteradoPorUsuario where 1=1 ";
        }

        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataConvocacao)=:mes";
        $sql .= " and YEAR(dataConvocacao)=:ano and seqCadast <>0 order by r.dataCadastro";

        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $arr=array();

        $i=0;
        if($resultado)
        {
            foreach($resultado as $v)
            {
                $arr['data'][$i] = $v['dataCadastro'];
                $arr['seq'][$i] = $v['seqCadast'];
                $arr['nome'][$i] = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
                $i++;
            }

        }
        return $arr;
    }


}

?>