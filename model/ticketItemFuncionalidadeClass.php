<?php

require_once ("consultaClass.php");

class TicketItemFuncionalidade{

    private $idTicketItemFuncionalidade;
    private $idOrganismoAfiliado;
    private $fk_idFuncionalidade;
    private $fk_idItemFuncionalidade;
    private $seqCadast;
    private $motivo;
    private $fk_idFinalidade;
    private $fk_idTicket;
    private $mes;
    private $ano;

    //METODO SET/GET -----------------------------------------------------------

    function getIdTicketItemFuncionalidade() {
        return $this->idTicketItemFuncionalidade;
    }

    function getIdOrganismoAfiliado() {
        return $this->idOrganismoAfiliado;
    }

    function getFk_idFuncionalidade() {
        return $this->fk_idFuncionalidade;
    }

    function getFk_idItemFuncionalidade() {
        return $this->fk_idItemFuncionalidade;
    }

    function getSeqCadast() {
        return $this->seqCadast;
    }

    function getMotivo() {
        return $this->motivo;
    }

    function getFk_idFinalidade() {
        return $this->fk_idFinalidade;
    }

    function getFk_idTicket() {
        return $this->fk_idTicket;
    }

    function getMes() {
        return $this->mes;
    }

    function getAno() {
        return $this->ano;
    }

    function setIdTicketItemFuncionalidade($idTicketItemFuncionalidade) {
        $this->idTicketItemFuncionalidade = $idTicketItemFuncionalidade;
    }

    function setIdOrganismoAfiliado($idOrganismoAfiliado) {
        $this->idOrganismoAfiliado = $idOrganismoAfiliado;
    }

    function setFk_idFuncionalidade($fk_idFuncionalidade) {
        $this->fk_idFuncionalidade = $fk_idFuncionalidade;
    }

    function setFk_idItemFuncionalidade($fk_idItemFuncionalidade) {
        $this->fk_idItemFuncionalidade = $fk_idItemFuncionalidade;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    function setMotivo($motivo) {
        $this->motivo = $motivo;
    }

    function setFk_idFinalidade($fk_idFinalidade) {
        $this->fk_idFinalidade = $fk_idFinalidade;
    }

    function setFk_idTicket($fk_idTicket) {
        $this->fk_idTicket = $fk_idTicket;
    }

    function setMes($mes) {
        $this->mes = $mes;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    
        
    //--------------------------------------------------------------------------

    public function cadastroVinculoItemTicket(){

        $sql = "INSERT INTO ticketItemFuncionalidade
                                 (idOrganismoAfiliado,
                                  fk_idFuncionalidade,
                                  fk_idItemFuncionalidade,
                                  seqCadast,
                                  motivo,
                                  fk_idFinalidade,
                                  fk_idTicket,
                                  dataCadastro,
                                  mes,
                                  ano)
                            VALUES (:idOrganismoAfiliado,
		                            :fk_idFuncionalidade,
		                            :fk_idItemFuncionalidade,
		                            :seqCadast,
		                            :motivo,
		                            :fk_idFinalidade,
                                            :fk_idTicket,
		                            now(),
                                            :mes,
                                            :ano)";
        //echo "| SQL: ".$sql."| <br>";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $this->idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':fk_idFuncionalidade', $this->fk_idFuncionalidade, PDO::PARAM_INT);
        $sth->bindParam(':fk_idItemFuncionalidade', $this->fk_idItemFuncionalidade, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':motivo', $this->motivo, PDO::PARAM_STR);
        $sth->bindParam(':fk_idFinalidade', $this->fk_idFinalidade, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicket', $this->fk_idTicket, PDO::PARAM_INT);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE 'ticketItemFuncionalidade'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoIdInserido(){

        $sql= "SELECT MAX(idTicketItemFuncionalidad) as lastid FROM ticketItemFuncionalidade";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function verificaVinculoItemTicket($idFuncionalidade,$idItemFuncionalidade,$seqCadast,$idFinalidade){

    	$sql = "SELECT * FROM ticketItemFuncionalidade
                 WHERE 1=1";
        $sql .= $idFuncionalidade!=null ? " and fk_idFuncionalidade=:idFuncionalidade" : "";
        $sql .= $idItemFuncionalidade!=null ? " and fk_idItemFuncionalidade=:idItemFuncionalidade" : "";
        $sql .= $seqCadast!=null ? " and seqCadast=:seqCadast" : "";
        $sql .= $idFinalidade!=null ? " and fk_idFinalidade=:idFinalidade" : "";
    	//echo "| SQL: ".$sql."| <br>";
        //
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($idFuncionalidade!=null)
            $sth->bindParam(':idFuncionalidade', $idFuncionalidade, PDO::PARAM_INT);
        if ($idItemFuncionalidade!=null)
            $sth->bindParam(':idItemFuncionalidade', $idItemFuncionalidade, PDO::PARAM_INT);
        if ($seqCadast!=null)
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        if ($idFinalidade!=null)
            $sth->bindParam(':idFinalidade', $idFinalidade, PDO::PARAM_INT);
        
        $resultado = $c->run($sth,null,null,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }
    
    public function listaVinculoItemTicket($idFuncionalidade,$idItemFuncionalidade,$seqCadast,$idFinalidade,$idTicket){
        /*
        echo "| par: ".$idFuncionalidade."| <br>";
        echo "| par: ".$seqCadast."| <br>";
        echo "| par: ".$idFinalidade."| <br>";
        echo "| par: ".$idTicket."| <br>";
        */
    	$sql = "SELECT * FROM ticketItemFuncionalidade as tif
                 inner join usuario as u on u.seqCadast = tif.seqCadast
                 WHERE 1=1";
        $sql .= $idFuncionalidade!=null ? " and fk_idFuncionalidade=:idFuncionalidade" : "";
        $sql .= $idItemFuncionalidade!=null ? " and fk_idItemFuncionalidade=:idItemFuncionalidade" : "";
        $sql .= $seqCadast!=null ? " and tif.seqCadast=:seqCadast" : "";
        $sql .= $idFinalidade!=null ? " and fk_idFinalidade=:idFinalidade" : "";
        $sql .= $idTicket!=null ? " and fk_idTicket=:idTicket" : "";
    	//echo "| SQL: ".$sql."| <br>";
        //
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($idFuncionalidade!=null)
            $sth->bindParam(':idFuncionalidade', $idFuncionalidade, PDO::PARAM_INT);
        if ($idItemFuncionalidade!=null)
            $sth->bindParam(':idItemFuncionalidade', $idItemFuncionalidade, PDO::PARAM_INT);
        if ($seqCadast!=null)
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        if ($idFinalidade!=null)
            $sth->bindParam(':idFinalidade', $idFinalidade, PDO::PARAM_INT);
        if ($idTicket!=null)
            $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
/*
    public function retornaQuantidadeTicket($idOrganismo,$status,$organismo,$funcionalidade,$atribuicao){

        $sql = "SELECT COUNT(*) as total FROM ticket WHERE 1=1";
        $sql .= $idOrganismo!=null ? " and fk_idOrganismoAfiliado=:idOrganismo" : "";
        $sql .= $status!=null ? " and statusTicket=:status" : "";
        $sql .= $organismo!=null ? " and fk_idOrganismoAfiliado=:organismo" : "";
        $sql .= $funcionalidade!=null ? " and fk_idFuncionalidade=:funcionalidade" : "";
        $sql .= $atribuicao!=null ? " and atribuidoTicket=:atribuicao" : "";
        //echo "| SQL: ".$sql."| <br>";
        //
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($idOrganismo!=null)
            $sth->bindParam(':idOrganismo', $idOrganismo, PDO::PARAM_INT);
        if ($status!=null)
            $sth->bindParam(':status', $status, PDO::PARAM_INT);
        if ($organismo!=null)
            $sth->bindParam(':organismo', $organismo, PDO::PARAM_INT);
        if ($funcionalidade!=null)
            $sth->bindParam(':funcionalidade', $funcionalidade, PDO::PARAM_INT);
        if ($atribuicao!=null)
            $sth->bindParam(':atribuicao', $atribuicao, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function retornaQntTicket(){

        $sql = "SELECT COUNT(*) as total FROM ticket WHERE 1=1";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

	public function alteraStatusTicket($idTicket,$status) {
        $sql = "UPDATE ticket SET `statusTicket` = :statusTicket WHERE `idTicket` = :idTicket";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusTicket', $status, PDO::PARAM_INT);
        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaFuncionalidade($id=null){

        $sql = "SELECT * FROM funcionalidade WHERE 1=1 ";
        
        if($id!=null)
        {
            $sql .= " and idFuncionalidade=:id ";
        }    
        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaTicket($idTicket){

        $sql = "SELECT * FROM ticket where idTicket=:idTicket";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaFuncionalidade($idFuncionalidade){

        $sql = "SELECT * FROM funcionalidade where idFuncionalidade=:idFuncionalidade";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idFuncionalidade', $idFuncionalidade, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function renovarDataAtualizacaoTicket($idTicket){
        $sql = "UPDATE ticket SET dataAtualizacaoTicket=NOW() WHERE idTicket = :idTicket";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAnexo($fk_idTicket){

        $sql = "SELECT * FROM ticketAnexo
                  WHERE excluido = '1'
                  and fk_idTicket = :fk_idTicket
                  ORDER BY idTicketAnexo DESC";

        //echo $query;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function excluiAnexo($idAnexo){

        $sql = "UPDATE ticketAnexo SET excluido='0' WHERE idTicketAnexo = :idAnexo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAnexo', $idAnexo, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function cadastroAnexo($nome_original,$nome_arquivo,$ext,$full_path,$data,$excluido,$fk_idTicket){

        $sql = "INSERT INTO ticketAnexo (nome_original,nome_arquivo,ext,full_path,data_cadastro,excluido,fk_idTicket)
                            VALUES (:nome_original,
                                    :nome_arquivo,
                                    :ext,
                                    :full_path,
                                    :data_cadastro,
                                    :excluido,
                                    :fk_idTicket)";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nome_original', $nome_original, PDO::PARAM_STR);
        $sth->bindParam(':nome_arquivo', $nome_arquivo, PDO::PARAM_STR);
        $sth->bindParam(':ext', $ext, PDO::PARAM_STR);
        $sth->bindParam(':full_path', $full_path, PDO::PARAM_STR);
        $sth->bindParam(':data_cadastro', $data, PDO::PARAM_STR);
        $sth->bindParam(':excluido', $excluido, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraStatusTicketUsuario($status, $fk_idTicket, $seqCadast=null) {

        $sql = "UPDATE ticket_usuario
                  SET `statusTicketUsuario` = :status
                  WHERE `fk_idTicket` = :fk_idTicket";
        $sql .= $seqCadast!=null ? " and `fk_seqCadast` = :seqCadast" : "";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        if($seqCadast!=null)
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function hasAuthorization($seqCadast, $fk_idTicket) {

        $sql = "SELECT * FROM ticket_usuario
                  WHERE fk_seqCadast = :fk_seqCadast
                  and fk_idTicket = :fk_idTicket";
        //echo "| SQL: ".$query."| <br>";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaEnvolvidos($fk_idTicket,$fk_idDepartamento) {

        $sql = "SELECT tu.fk_seqCadast,u.nomeUsuario,u.avatarUsuario FROM ticket_usuario as tu INNER JOIN usuario as u
                  ON u.seqCadast=tu.fk_seqCadast
                  WHERE 1=1 and u.statusUsuario=0
                  and tu.fk_idTicket = :fk_idTicket
                  and tu.fk_idDepartamento = :fk_idDepartamento";
        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        $sth->bindParam(':fk_idDepartamento', $fk_idDepartamento, PDO::PARAM_INT);

        $data = array(
            'fk_idTicket'=>$fk_idTicket,
            'fk_idDepartamento'=>$fk_idDepartamento
        );
        //echo "QUERY: " . $this->parms("listaEnvolvidos",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function cadastroEnvolvidos($fk_idTicket,$fk_seqCadast,$fk_idDepartamento,$statusTicketUsuario){

        $sql = "INSERT INTO ticket_usuario
                                 (fk_idTicket,
                                  fk_seqCadast,
                                  fk_idDepartamento,
                                  statusTicketUsuario)
                            VALUES (:fk_idTicket,
		                            :fk_seqCadast,
		                            :fk_idDepartamento,
		                            :statusTicketUsuario)";
        //echo $sql."<br>";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $fk_seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idDepartamento', $fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':statusTicketUsuario', $statusTicketUsuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaTicketDropdown($ativo=null,$seqCadast=null) {

        $sql = "SELECT t.idTicket,t.tituloTicket,t.statusTicket,t.descricaoTicket,t.dataAtualizacaoTicket FROM ticket as t
                  left join ticket_usuario as tu on tu.fk_idTicket=t.idTicket where 1=1";
        if($ativo!=null) {
            $sql .= " and tu.statusTicketUsuario=0";
        }
        if($seqCadast!=null) {
            $sql .= " and tu.fk_seqCadast=:seqCadast";
        }
        $sql .= " order by t.dataAtualizacaoTicket desc,t.dataCriacaoTicket desc"; // LIMIT 4
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null)
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $data = array(
            'seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("listaTicketDropdown",$sql,$data);

        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function lista($seq_cadast=null,$fk_idTicket=null){

        $sql = "SELECT COUNT(*) as total FROM ticket_usuario where 1=1";
        if($seq_cadast!=null) {
            $sql .= " and fk_seqCadast=:seqCadast";
        }
        if($fk_idTicket!=null) {
            $sql .= " and fk_idTicket=:fk_idTicket";
        }
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seq_cadast!=null)
        {    
            $sth->bindParam(':seqCadast', $seq_cadast, PDO::PARAM_INT);
        }
        if($fk_idTicket!=null)
        {    
            $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        //echo "<pre>";print_r($resultado);
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function alteraAtribuicaoTicket($idTicket,$seqCadastAtribuido,$seqCadastAtribuidor) {
        $sql = "UPDATE ticket SET
                    `atribuidoTicket` = :seqCadastAtribuido,
                    `atribuidorTicket` = :seqCadastAtribuidor
                    WHERE `idTicket` = :idTicket";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadastAtribuido', $seqCadastAtribuido, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastAtribuidor', $seqCadastAtribuidor, PDO::PARAM_INT);
        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }
     */
}
?>
