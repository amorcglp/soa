<?php

require_once 'conexaoClass.php';

class ImpressaoDiscurso {
    
    private $idImpressaoDiscurso;
    private $motivoDaImpressao;
    private $fk_idImpressao;
    private $fk_idDiscurso;
    private $qtdeImpressao;
    
    public function getIdImpressaoDiscurso() {
        return $this->idImpressaoDiscurso;
    }

    public function getMotivoDaImpressao() {
        return $this->motivoDaImpressao;
    }

    public function getFkidImpressao() {
        return $this->fk_idImpressao;
    }

    public function getFkIdDiscurso() {
        return $this->fk_idDiscurso;
    }
    
    public function setIdImpressaoDiscurso($idImpressaoDiscurso) {
        $this->idImpressaoDiscurso = $idImpressaoDiscurso;
    }

    public function setMotivoDaImpressao($motivoDaImpressao) {
        $this->motivoDaImpressao = $motivoDaImpressao;
    }

    public function setFkidImpressao($fk_idImpressao) {
        $this->fk_idImpressao = $fk_idImpressao;
    }

    public function setFkIdDiscurso($fk_idDiscurso) {
        $this->fk_idDiscurso = $fk_idDiscurso;
    }
    
    public function setQtdeImpressaoDiscurso($qtdeImpressao) {
        $this->qtdeImpressao = $qtdeImpressao;
    }
    
    public function cadastraImpressaoDiscurso() {
        
        $sql = "INSERT INTO `impressao_discurso` (";
        $sql .= "`motivoDaImpressao`, ";
        $sql .= "`fk_idImpressao`, ";
        $sql .= "`fk_idDiscurso`";
        $sql .= ") VALUES (";
        $sql .= ":motivo, ";
        $sql .=  ":idImpressao, ";
        $sql .=  ":idDiscurso)";
     
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':motivo',$this->motivoDaImpressao , PDO::PARAM_STR);
        $sth->bindParam(':idImpressao', $this->fk_idImpressao, PDO::PARAM_INT);
        $sth->bindParam(':idDiscurso', $this->fk_idDiscurso, PDO::PARAM_INT);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function listaImpressaoDiscurso() {
        $sql = "SELECT * FROM `impressao_discurso`";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function getIdQtdeImpressao($idDiscurso=null) {
        if($idDiscurso!=null) {
            $sql = "SELECT `qtdeImpressao` FROM `discurso` WHERE `idDiscurso` = :id";
        
            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql);

            $sth->bindParam(':id',  $idDiscurso, PDO::PARAM_INT);

            $resultado = $c->run($sth);
            if ($resultado){
                return $resultado;
            }else{
                return false;
            }
        }
        return $this->qtdeImpressao;
    }
    
    public function inseriQtdeImpressao($id) {
        
        $sql = "UPDATE `discurso` SET ";
        $sql .= "`qtdeImpressao` = :qtdeImpressao";
        $sql .= " WHERE `idDiscurso` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':qtdeImpressao',  $this->qtdeImpressao, PDO::PARAM_INT);
        $sth->bindParam(':id',  $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function buscaIdImpressao() {
        
        $sql = "SELECT * FROM `impressao_discurso` WHERE `fk_idImpressao` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id',  $this->fk_idImpressao, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaIdDiscurso() {
        
        $sql = "SELECT * FROM `impressao_discurso` WHERE `fk_idDiscurso` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id',  $this->fk_idDiscurso, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function excluiImpressaoDiscurso() {
        
        $sql = "DELETE FROM `impressao_discurso` WHERE `fk_idDiscurso` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idDiscurso, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}
