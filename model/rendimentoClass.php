<?php
require_once ("conexaoClass.php");

class Rendimento {

    public $idRendimento;
    public $fk_idOrganismoAfiliado;
    public $dataVerificacao;
    public $descricaoRendimento;
    public $valorRendimento;
    public $categoriaRendimento;
    public $atribuidoA;
    public $usuario;
    public $ultimoAtualizar;

    /* Funções GET e SET */

    function getIdRendimento() {
        return $this->idRendimento;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    
	function getDataVerificacao() {
        return $this->dataVerificacao;
    }

    function getDescricaoRendimento() {
        return $this->descricaoRendimento;
    }

    function getValorRendimento() {
        return $this->valorRendimento;
    }
    
	function getCategoriaRendimento() {
        return $this->categoriaRendimento;
    }
        
	function getAtribuidoA() {
        return $this->atribuidoA;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdRendimento($idRendimento) {
        $this->idRendimento = $idRendimento;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }
    
	function setDataVerificacao($dataVerificacao) {
        $this->dataVerificacao = $dataVerificacao;
    }

	function setDescricaoRendimento($descricaoRendimento) {
        $this->descricaoRendimento = $descricaoRendimento;
    }
    
	function setValorRendimento($valorRendimento) {
        $this->valorRendimento = $valorRendimento;
    }

    function setCategoriaRendimento($categoriaRendimento) {
        $this->categoriaRendimento = $categoriaRendimento;
    }
    
	function setAtribuidoA($atribuidoA) {
        $this->atribuidoA = $atribuidoA;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    public function cadastraRendimento() {
        
    	$sql = "INSERT INTO rendimento 
                                (`fk_idOrganismoAfiliado`,
                                `dataVerificacao`, 
                                `descricaoRendimento`, 
                                `valorRendimento`,
                                `categoriaRendimento`,
                                `atribuidoA`,
                                `usuario`,
                                `dataCadastro`
                                )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :dataVerificacao,
                                            :descricao,
                                            :valor,
                                            :categoria,
                                            :atribuidoA,
                                            :usuario,
                                            now()
                                            )";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFkIdOrganismoAfiliado       = $this->getFkIdOrganismoAfiliado();
        $getDataVerificacao             = $this->getDataVerificacao();
        $getDescricaoRendimento         = $this->getDescricaoRendimento();
        $getValorRendimento             = $this->getValorRendimento();
        $getCategoriaRendimento         = $this->getCategoriaRendimento();
        $getAtribuidoA                  = $this->getAtribuidoA();
        $getUsuario                     = $this->getUsuario();

        $sth->bindParam(':idOrganismoAfiliado', $getFkIdOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataVerificacao', $getDataVerificacao, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $getDescricaoRendimento, PDO::PARAM_STR);
        $sth->bindParam(':valor', $getValorRendimento, PDO::PARAM_STR);
        $sth->bindParam(':categoria', $getCategoriaRendimento, PDO::PARAM_INT);
        $sth->bindParam(':atribuidoA', $getAtribuidoA, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $getUsuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraRendimento($id) {

        $sql = "UPDATE rendimento SET
                        `dataVerificacao` = :dataVerificacao, 
                        `descricaoRendimento` = :descricao,
                        `valorRendimento` = :valor,
                        `categoriaRendimento` = :categoria,
                        `atribuidoA` = :atribuidoA,
                        `ultimoAtualizar` = :ultimoAtualizar
                            WHERE `idRendimento` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getDataVerificacao             = $this->getDataVerificacao();
        $getDescricaoRendimento         = $this->getDescricaoRendimento();
        $getValorRendimento             = $this->getValorRendimento();
        $getCategoriaRendimento         = $this->getCategoriaRendimento();
        $getAtribuidoA                  = $this->getAtribuidoA();
        $getUltimoAtualizar             = $this->getUltimoAtualizar();

        $sth->bindParam(':dataVerificacao', $getDataVerificacao, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $getDescricaoRendimento, PDO::PARAM_STR);
        $sth->bindParam(':valor', $getValorRendimento, PDO::PARAM_STR);
        $sth->bindParam(':categoria', $getCategoriaRendimento, PDO::PARAM_INT);
        $sth->bindParam(':atribuidoA', $getAtribuidoA, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $getUltimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId()
    {

        $sql= "SHOW TABLE STATUS LIKE 'rendimento'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM rendimento ORDER BY idRendimento DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRendimento($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM rendimento where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataVerificacao)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataVerificacao)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	$sql .= " order by dataVerificacao ASC";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_STR);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $data = array(
            'mesAtual'=>$mesAtual,
            'anoAtual'=>$anoAtual,
            'idOrganismoAfiliado'=>$fk_idOrganismoAfiliado
        );

        //echo $this->parms("listaRendimento",$sql,$data);

        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaRendimento($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null,$categoriaRendimento=null,$atribuidoA=null,$ultimoMes=null) {
        
    	$sql = "SELECT SUM(REPLACE( REPLACE(valorRendimento, '.' ,'' ), ',', '.' )) "
                . " as total FROM rendimento where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataVerificacao)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataVerificacao)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
	if($categoriaRendimento!=null)
    	{
    		$sql .= " and categoriaRendimento=:categoriaRendimento";
    	}
	if($atribuidoA!=null)
    	{
    		$sql .= " and atribuidoA=:atribuidoA";
    	}
    	
    	if($ultimoMes!=null)
    	{
    		$sql .= " group by MONTH(dataVerificacao) order by dataVerificacao DESC limit 0,1";
    	}else{
    		$sql .= " order by dataVerificacao ASC";
    	}
    	
    	//echo "<br><br>".$sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_STR);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($categoriaRendimento!=null)
    	{
            $sth->bindParam(':categoriaRendimento', $categoriaRendimento, PDO::PARAM_INT);
        }
        if($atribuidoA!=null)
    	{
            $sth->bindParam(':atribuidoA', $atribuidoA, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);

        $total=0;
        if ($resultado) {
            foreach($resultado as $vetor)
        	{
        		$total = $vetor['total'];
        	}
            return $total;
        } else {
            return false;
        }
    }

    public function removeRendimento($idRendimento) {
        
        $sql = "DELETE FROM rendimento WHERE `idRendimento` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRendimento, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdRendimento($idRendimento) {
        
        $sql = "SELECT * FROM rendimento 
                                 WHERE `idRendimento` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRendimento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function dataCriacao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM rendimento where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataVerificacao)=:mes";
        $sql .= " and YEAR(dataVerificacao)=:ano";
        $sql .= " order by dataCadastro ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function dataUltimaAlteracao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM rendimento where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataVerificacao)=:mes";
        $sql .= " and YEAR(dataVerificacao)=:ano";
        $sql .= " order by dataCadastro DESC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function responsaveis($mes,$ano, $idOrganismoAfiliado,$ultimaAlteracao=null) {

        if($ultimaAlteracao==null)
        {
            $sql = "SELECT * FROM rendimento as rend left join usuario on seqCadast = usuario where 1=1 ";
        }else{
            $sql = "SELECT * FROM rendimento as rend left join usuario on seqCadast = ultimoAtualizar where 1=1 ";
        }

        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataVerificacao)=:mes";
        $sql .= " and YEAR(dataVerificacao)=:ano and seqCadast <>0  order by rend.dataCadastro";

        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $arr=array();

        $i=0;
        if($resultado)
        {
            foreach($resultado as $v)
            {
                $arr['data'][$i] = $v['dataCadastro'];
                $arr['seq'][$i] = $v['seqCadast'];
                $arr['nome'][$i] = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
                $i++;
            }

        }
        return $arr;
    }


    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }
}
?>