<?php

require_once ("conexaoClass.php");

class Cidade {

    private $id;
    private $estado;
    private $uf;
    private $nome;
    
	function getId() {
        return $this->id;
    }
    
	function getEstado() {
        return $this->estado;
    }

    function getUf() {
        return $this->uf;
    }
    
	function getNome() {
        return $this->nome;
    }
    
	function setId($id) {
        $this->id = $id;
    }
    
	function setEstado($estado) {
        $this->estado = $estado;
    }

    function setUf($uf) {
        $this->uf = $uf;
    }
    
	function setNome($nome) {
        $this->nome = $nome;
    }
    
    # Seleciona 
    public function listaCidade($estado=null) 
    {
        $sql = "SELECT * FROM cidade where 1=1 ";
        if($estado!=null)
        {
            $sql .= " and estado=:estado";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        if($estado!=null)
        {    
            $sth->bindParam(':estado', $estado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaCidadeTexto($id=null) 
    {
        $sql = "SELECT * FROM cidade where 1=1 ";
        if($id!=null)
        {
                $sql .= " and id=:id";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
        {    
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $vetor)
            {
                    return $vetor['nome'];
            }
        }
        return false;

    }
    
    public function retornaIdCidade($cidade=null,$uf=null) 
    {
        $sql = "SELECT * FROM cidade where nome=:cidade and uf=:uf limit 0,1 ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':cidade', $cidade, PDO::PARAM_STR);
        $sth->bindParam(':uf', $uf, PDO::PARAM_STR);
        

        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $vetor)
            {
                    return $vetor['id'];
            }
        }
        return false;

    }
}

?>