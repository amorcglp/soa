<?php

require_once ("conexaoClass.php");

class atividadeIniciaticaOficial{

    private $idAtividadeIniciaticaOficial;
    private $anoAtividadeIniciaticaOficial;
    private $nroEquipeAtividadeIniciaticaOficial;
    private $dataRegistroAtividadeIniciaticaOficial;
    private $dataAtualizadaAtividadeIniciaticaOficial;
    private $anotacoesAtividadeIniciaticaOficial;
    private $tipoAtividadeIniciaticaOficial;
    private $mestreAtividadeIniciaticaOficial;
    private $mestreAuxAtividadeIniciaticaOficial;
    private $arquivistaAtividadeIniciaticaOficial;
    private $capelaoAtividadeIniciaticaOficial;
    private $matreAtividadeIniciaticaOficial;
    private $grandeSacerdotisaAtividadeIniciaticaOficial;
    private $guiaAtividadeIniciaticaOficial;
    private $guardiaoInternoAtividadeIniciaticaOficial;
    private $guardiaoExternoAtividadeIniciaticaOficial;
    private $archoteAtividadeIniciaticaOficial;
    private $medalhistaAtividadeIniciaticaOficial;
    private $arautoAtividadeIniciaticaOficial;
    private $adjutorAtividadeIniciaticaOficial;
    private $sonoplastaAtividadeIniciaticaOficial;
    private $fk_idOrganismoAfiliado;


    //--- GETERS -----------------------------------------------------
    public function getIdAtividadeIniciaticaOficial() {
        return $this->idAtividadeIniciaticaOficial;
    }
    public function getAnoAtividadeIniciaticaOficial() {
        return $this->anoAtividadeIniciaticaOficial;
    }
    public function getNroEquipeAtividadeIniciaticaOficial() {
        return $this->nroEquipeAtividadeIniciaticaOficial;
    }
    public function getDataRegistroAtividadeIniciaticaOficial() {
        return $this->dataRegistroAtividadeIniciaticaOficial;
    }
    public function getDataAtualizadaAtividadeIniciaticaOficial() {
        return $this->dataAtualizadaAtividadeIniciaticaOficial;
    }
    public function getAnotacoesAtividadeIniciaticaOficial() {
        return $this->anotacoesAtividadeIniciaticaOficial;
    }
    public function getTipoAtividadeIniciaticaOficial() {
        return $this->tipoAtividadeIniciaticaOficial;
    }
    public function getMestreAtividadeIniciaticaOficial() {
        return $this->mestreAtividadeIniciaticaOficial;
    }
    public function getMestreAuxAtividadeIniciaticaOficial() {
        return $this->mestreAuxAtividadeIniciaticaOficial;
    }
    public function getArquivistaAtividadeIniciaticaOficial() {
        return $this->arquivistaAtividadeIniciaticaOficial;
    }
    public function getCapelaoAtividadeIniciaticaOficial() {
        return $this->capelaoAtividadeIniciaticaOficial;
    }
    public function getMatreAtividadeIniciaticaOficial() {
        return $this->matreAtividadeIniciaticaOficial;
    }
    public function getGrandeSacerdotisaAtividadeIniciaticaOficial() {
        return $this->grandeSacerdotisaAtividadeIniciaticaOficial;
    }
    public function getGuiaAtividadeIniciaticaOficial() {
        return $this->guiaAtividadeIniciaticaOficial;
    }
    public function getGuardiaoInternoAtividadeIniciaticaOficial() {
        return $this->guardiaoInternoAtividadeIniciaticaOficial;
    }
    public function getGuardiaoExternoAtividadeIniciaticaOficial() {
        return $this->guardiaoExternoAtividadeIniciaticaOficial;
    }
    public function getArchoteAtividadeIniciaticaOficial() {
        return $this->archoteAtividadeIniciaticaOficial;
    }
    public function getMedalhistaAtividadeIniciaticaOficial() {
        return $this->medalhistaAtividadeIniciaticaOficial;
    }
    public function getArautoAtividadeIniciaticaOficial() {
        return $this->arautoAtividadeIniciaticaOficial;
    }
    public function getAdjutorAtividadeIniciaticaOficial() {
        return $this->adjutorAtividadeIniciaticaOficial;
    }
    public function getSonoplastaAtividadeIniciaticaOficial() {
        return $this->sonoplastaAtividadeIniciaticaOficial;
    }
    public function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    //--- SETERS -----------------------------------------------------
    public function setIdAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial) {
        $this->idAtividadeIniciaticaOficial = $idAtividadeIniciaticaOficial;
    }
    public function setAnoAtividadeIniciaticaOficial($anoAtividadeIniciaticaOficial) {
        $this->anoAtividadeIniciaticaOficial = $anoAtividadeIniciaticaOficial;
    }
    public function setNroEquipeAtividadeIniciaticaOficial($nroEquipeAtividadeIniciaticaOficial) {
        $this->nroEquipeAtividadeIniciaticaOficial = $nroEquipeAtividadeIniciaticaOficial;
    }
    public function setDataRegistroAtividadeIniciaticaOficial($dataRegistroAtividadeIniciaticaOficial) {
        $this->dataRegistroAtividadeIniciaticaOficial = $dataRegistroAtividadeIniciaticaOficial;
    }
    public function setDataAtualizadaAtividadeIniciaticaOficial($dataAtualizadaAtividadeIniciaticaOficial) {
        $this->dataAtualizadaAtividadeIniciaticaOficial = $dataAtualizadaAtividadeIniciaticaOficial;
    }
    public function setAnotacoesAtividadeIniciaticaOficial($anotacoesAtividadeIniciaticaOficial) {
        $this->anotacoesAtividadeIniciaticaOficial = $anotacoesAtividadeIniciaticaOficial;
    }
    public function setTipoAtividadeIniciaticaOficial($tipoAtividadeIniciaticaOficial) {
        $this->tipoAtividadeIniciaticaOficial = $tipoAtividadeIniciaticaOficial;
    }
    public function setMestreAtividadeIniciaticaOficial($mestreAtividadeIniciaticaOficial) {
        $this->mestreAtividadeIniciaticaOficial = $mestreAtividadeIniciaticaOficial;
    }
    public function setMestreAuxAtividadeIniciaticaOficial($mestreAuxAtividadeIniciaticaOficial) {
        $this->mestreAuxAtividadeIniciaticaOficial = $mestreAuxAtividadeIniciaticaOficial;
    }
    public function setArquivistaAtividadeIniciaticaOficial($arquivistaAtividadeIniciaticaOficial) {
        $this->arquivistaAtividadeIniciaticaOficial = $arquivistaAtividadeIniciaticaOficial;
    }
    public function setCapelaoAtividadeIniciaticaOficial($capelaoAtividadeIniciaticaOficial) {
        $this->capelaoAtividadeIniciaticaOficial = $capelaoAtividadeIniciaticaOficial;
    }
    public function setMatreAtividadeIniciaticaOficial($matreAtividadeIniciaticaOficial) {
        $this->matreAtividadeIniciaticaOficial = $matreAtividadeIniciaticaOficial;
    }
    public function setGrandeSacerdotisaAtividadeIniciaticaOficial($grandeSacerdotisaAtividadeIniciaticaOficial) {
        $this->grandeSacerdotisaAtividadeIniciaticaOficial = $grandeSacerdotisaAtividadeIniciaticaOficial;
    }
    public function setGuiaAtividadeIniciaticaOficial($guiaAtividadeIniciaticaOficial) {
        $this->guiaAtividadeIniciaticaOficial = $guiaAtividadeIniciaticaOficial;
    }
    public function setGuardiaoInternoAtividadeIniciaticaOficial($guardiaoInternoAtividadeIniciaticaOficial) {
        $this->guardiaoInternoAtividadeIniciaticaOficial = $guardiaoInternoAtividadeIniciaticaOficial;
    }
    public function setGuardiaoExternoAtividadeIniciaticaOficial($guardiaoExternoAtividadeIniciaticaOficial) {
        $this->guardiaoExternoAtividadeIniciaticaOficial = $guardiaoExternoAtividadeIniciaticaOficial;
    }
    public function setArchoteAtividadeIniciaticaOficial($archoteAtividadeIniciaticaOficial) {
        $this->archoteAtividadeIniciaticaOficial = $archoteAtividadeIniciaticaOficial;
    }
    public function setMedalhistaAtividadeIniciaticaOficial($medalhistaAtividadeIniciaticaOficial) {
        $this->medalhistaAtividadeIniciaticaOficial = $medalhistaAtividadeIniciaticaOficial;
    }
    public function setArautoAtividadeIniciaticaOficial($arautoAtividadeIniciaticaOficial) {
        $this->arautoAtividadeIniciaticaOficial = $arautoAtividadeIniciaticaOficial;
    }
    public function setAdjutorAtividadeIniciaticaOficial($adjutorAtividadeIniciaticaOficial) {
        $this->adjutorAtividadeIniciaticaOficial = $adjutorAtividadeIniciaticaOficial;
    }
    public function setSonoplastaAtividadeIniciaticaOficial($sonoplastaAtividadeIniciaticaOficial) {
        $this->sonoplastaAtividadeIniciaticaOficial = $sonoplastaAtividadeIniciaticaOficial;
    }
    public function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }


    //--------------------------------------------------------------------------

	public function cadastroAtividadeIniciaticaOficial(){

        $sql = "INSERT INTO atividadeIniciaticaOficial
                                 (anoAtividadeIniciaticaOficial,
                                  dataRegistroAtividadeIniciaticaOficial,
                                  anotacoesAtividadeIniciaticaOficial,
                                  tipoAtividadeIniciaticaOficial,
                                  mestreAtividadeIniciaticaOficial,
                                  mestreAuxAtividadeIniciaticaOficial,
                                  arquivistaAtividadeIniciaticaOficial,
                                  capelaoAtividadeIniciaticaOficial,
                                  matreAtividadeIniciaticaOficial,
                                  grandeSacerdotisaAtividadeIniciaticaOficial,
                                  guiaAtividadeIniciaticaOficial,
                                  guardiaoInternoAtividadeIniciaticaOficial,
                                  guardiaoExternoAtividadeIniciaticaOficial,
                                  archoteAtividadeIniciaticaOficial,
                                  medalhistaAtividadeIniciaticaOficial,
                                  arautoAtividadeIniciaticaOficial,
                                  adjutorAtividadeIniciaticaOficial,
                                  sonoplastaAtividadeIniciaticaOficial,
                                  fk_idOrganismoAfiliado)
                            VALUES (:anoAtividadeIniciaticaOficial,
                                    NOW(),
                                    :anotacoesAtividadeIniciaticaOficial,
                                    :tipoAtividadeIniciaticaOficial,
                                    :mestreAtividadeIniciaticaOficial,
                                    :mestreAuxAtividadeIniciaticaOficial,
                                    :arquivistaAtividadeIniciaticaOficial,
                                    :capelaoAtividadeIniciaticaOficial,
                                    :matreAtividadeIniciaticaOficial,
                                    :grandeSacerdotisaAtividadeIniciaticaOficial,
                                    :guiaAtividadeIniciaticaOficial,
                                    :guardiaoInternoAtividadeIniciaticaOficial,
                                    :guardiaoExternoAtividadeIniciaticaOficial,
                                    :archoteAtividadeIniciaticaOficial,
                                    :medalhistaAtividadeIniciaticaOficial,
                                    :arautoAtividadeIniciaticaOficial,
                                    :adjutorAtividadeIniciaticaOficial,
                                    :sonoplastaAtividadeIniciaticaOficial,
                                    :fk_idOrganismoAfiliado)";
        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $sth->bindParam(':anoAtividadeIniciaticaOficial', $this->anoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':anotacoesAtividadeIniciaticaOficial', $this->anotacoesAtividadeIniciaticaOficial, PDO::PARAM_STR);
        $sth->bindParam(':tipoAtividadeIniciaticaOficial', $this->tipoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':mestreAtividadeIniciaticaOficial', $this->mestreAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':mestreAuxAtividadeIniciaticaOficial', $this->mestreAuxAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':arquivistaAtividadeIniciaticaOficial', $this->arquivistaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':capelaoAtividadeIniciaticaOficial', $this->capelaoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':matreAtividadeIniciaticaOficial', $this->matreAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':grandeSacerdotisaAtividadeIniciaticaOficial', $this->grandeSacerdotisaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':guiaAtividadeIniciaticaOficial', $this->guiaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':guardiaoInternoAtividadeIniciaticaOficial', $this->guardiaoInternoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':guardiaoExternoAtividadeIniciaticaOficial', $this->guardiaoExternoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':archoteAtividadeIniciaticaOficial', $this->archoteAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':medalhistaAtividadeIniciaticaOficial', $this->medalhistaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':arautoAtividadeIniciaticaOficial', $this->arautoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':adjutorAtividadeIniciaticaOficial', $this->adjutorAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':sonoplastaAtividadeIniciaticaOficial', $this->sonoplastaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial)
    {
        $sql = "UPDATE atividadeIniciaticaOficial SET
                                  `anoAtividadeIniciaticaOficial` = :anoAtividadeIniciaticaOficial,
                                  `dataAtualizadaAtividadeIniciaticaOficial` =  NOW(),
                                  `anotacoesAtividadeIniciaticaOficial` =  :anotacoesAtividadeIniciaticaOficial,
                                  `tipoAtividadeIniciaticaOficial` =  :tipoAtividadeIniciaticaOficial,
                                  WHERE idAtividadeIniciaticaOficial = :idAtividadeIniciaticaOficial";

        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $sth->bindParam(':anoAtividadeIniciaticaOficial', $this->anoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':anotacoesAtividadeIniciaticaOficial', $this->anotacoesAtividadeIniciaticaOficial, PDO::PARAM_STR);
        $sth->bindParam(':tipoAtividadeIniciaticaOficial', $this->tipoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeIniciaticaOficial', $idAtividadeIniciaticaOficial, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraOficiaisAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial) {

        $query = "UPDATE atividadeIniciaticaOficial SET dataAtualizadaAtividadeIniciaticaOficial = NOW()";

    if($this->getAnotacoesAtividadeIniciaticaOficial()!='<p><br></p>') {
        $query .= ", `anotacoesAtividadeIniciaticaOficial` = :anotacoesAtividadeIniciaticaOficial";
    }
    //if($this->getMestreAtividadeIniciaticaOficial()!=0){
        $query .= ", `mestreAtividadeIniciaticaOficial` = :mestreAtividadeIniciaticaOficial";
    //}
    //if($this->getMestreAuxAtividadeIniciaticaOficial()!=0){
        $query .= ", `mestreAuxAtividadeIniciaticaOficial` = :mestreAuxAtividadeIniciaticaOficial";
    //}
    //if($this->getArquivistaAtividadeIniciaticaOficial()!=0){
        $query .= ", `arquivistaAtividadeIniciaticaOficial` = :arquivistaAtividadeIniciaticaOficial";
    //}
    //if($this->getCapelaoAtividadeIniciaticaOficial()!=0){
        $query .= ", `capelaoAtividadeIniciaticaOficial` = :capelaoAtividadeIniciaticaOficial";
    //}
    //if($this->getMatreAtividadeIniciaticaOficial()!=0){
        $query .= ", `matreAtividadeIniciaticaOficial` = :matreAtividadeIniciaticaOficial";
    //}
    //if($this->getGrandeSacerdotisaAtividadeIniciaticaOficial()!=0){
        $query .= ", `grandeSacerdotisaAtividadeIniciaticaOficial` = :grandeSacerdotisaAtividadeIniciaticaOficial";
    //}
    //if($this->getGuiaAtividadeIniciaticaOficial()!=0){
        $query .= ", `guiaAtividadeIniciaticaOficial` = :guiaAtividadeIniciaticaOficial";
    //}
    //if($this->getGuardiaoInternoAtividadeIniciaticaOficial()!=0){
        $query .= ", `guardiaoInternoAtividadeIniciaticaOficial` = :guardiaoInternoAtividadeIniciaticaOficial";
    //}
    //if($this->getGuardiaoExternoAtividadeIniciaticaOficial()!=0){
        $query .= ", `guardiaoExternoAtividadeIniciaticaOficial` = :guardiaoExternoAtividadeIniciaticaOficial";
    //}
    //if($this->getArchoteAtividadeIniciaticaOficial()!=0){
        $query .= ", `archoteAtividadeIniciaticaOficial` = :archoteAtividadeIniciaticaOficial";
    //}
    //if($this->getArchoteAtividadeIniciaticaOficial()!=0){
        $query .= ", `medalhistaAtividadeIniciaticaOficial` = :medalhistaAtividadeIniciaticaOficial";
    //}
    //if($this->getArautoAtividadeIniciaticaOficial()!=0){
        $query .= ", `arautoAtividadeIniciaticaOficial` = :arautoAtividadeIniciaticaOficial";
    //}
    //if($this->getAdjutorAtividadeIniciaticaOficial()!=0){
        $query .= ", `adjutorAtividadeIniciaticaOficial` = :adjutorAtividadeIniciaticaOficial";
    //}
    //if($this->getSonoplastaAtividadeIniciaticaOficial()!=0){
        $query .= ", `sonoplastaAtividadeIniciaticaOficial` = :sonoplastaAtividadeIniciaticaOficial";
    //}
    $query .= " WHERE idAtividadeIniciaticaOficial = :idAtividadeIniciaticaOficial";

        //echo $query;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        if($this->getAnotacoesAtividadeIniciaticaOficial()!='<p><br></p>') {
            $sth->bindParam(':anotacoesAtividadeIniciaticaOficial', $this->anotacoesAtividadeIniciaticaOficial, PDO::PARAM_INT);
        }

        $sth->bindParam(':mestreAtividadeIniciaticaOficial', $this->mestreAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':mestreAuxAtividadeIniciaticaOficial', $this->mestreAuxAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':arquivistaAtividadeIniciaticaOficial', $this->arquivistaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':capelaoAtividadeIniciaticaOficial', $this->capelaoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':matreAtividadeIniciaticaOficial', $this->matreAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':grandeSacerdotisaAtividadeIniciaticaOficial', $this->grandeSacerdotisaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':guiaAtividadeIniciaticaOficial', $this->guiaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':guardiaoInternoAtividadeIniciaticaOficial', $this->guardiaoInternoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':guardiaoExternoAtividadeIniciaticaOficial', $this->guardiaoExternoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':archoteAtividadeIniciaticaOficial', $this->archoteAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':medalhistaAtividadeIniciaticaOficial', $this->medalhistaAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':arautoAtividadeIniciaticaOficial', $this->arautoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':adjutorAtividadeIniciaticaOficial', $this->adjutorAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':sonoplastaAtividadeIniciaticaOficial', $this->sonoplastaAtividadeIniciaticaOficial, PDO::PARAM_INT);

        $sth->bindParam(':idAtividadeIniciaticaOficial', $idAtividadeIniciaticaOficial, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeIniciaticaOficial($idOrganismoAfiliado,$anoAtividadeIniciaticaOficial='',$anoAtividadeIniciaticaOficialMenosUm=''){

      	$sql = "SELECT * FROM atividadeIniciaticaOficial";
      	if ($idOrganismoAfiliado != "") {
  			     $sql .= " WHERE fk_idOrganismoAfiliado = :idOrganismoAfiliado";
  		  }
        if ($anoAtividadeIniciaticaOficialMenosUm != "") {
            $sql .= " and (anoAtividadeIniciaticaOficial = :anoAtividadeIniciaticaOficial or
                              anoAtividadeIniciaticaOficial = :anoAtividadeIniciaticaOficialMenosUm)";
        } else {
            if ($anoAtividadeIniciaticaOficial != "") {
                $sql .= " and anoAtividadeIniciaticaOficial = :anoAtividadeIniciaticaOficial";
            }
        }
    	  $sql .= " ORDER BY `atividadeIniciaticaOficial`.`anoAtividadeIniciaticaOficial` DESC";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($idOrganismoAfiliado != "") {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if ($anoAtividadeIniciaticaOficialMenosUm != "") {
            $sth->bindParam(':anoAtividadeIniciaticaOficial', $anoAtividadeIniciaticaOficial, PDO::PARAM_INT);
            $sth->bindParam(':anoAtividadeIniciaticaOficialMenosUm', $anoAtividadeIniciaticaOficialMenosUm, PDO::PARAM_INT);
        } else {
            if ($anoAtividadeIniciaticaOficial != "") {
                $sth->bindParam(':anoAtividadeIniciaticaOficial', $anoAtividadeIniciaticaOficial, PDO::PARAM_INT);
            }
        }

        $resultado = $c->run($sth);

        $data = array(
            'idOrganismoAfiliado'=>$idOrganismoAfiliado,
            'anoAtividadeIniciaticaOficial'=>$anoAtividadeIniciaticaOficial
        );
        //echo $this->parms("listaAtividadeIniciaticaOficial",$sql,$data);

        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial){

        $query = "SELECT * FROM  atividadeIniciaticaOficial
                    WHERE   idAtividadeIniciaticaOficial = :idAtividadeIniciaticaOficial";

        //echo $query;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':idAtividadeIniciaticaOficial', $idAtividadeIniciaticaOficial, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function retornaAnosJaCadastrados($fk_idOrganismoAfiliado){

        $query = "SELECT anoAtividadeIniciaticaOficial FROM atividadeIniciaticaOficial
        where fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado ORDER BY anoAtividadeIniciaticaOficial DESC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function retornaEquipesJaCadastrados($fk_idOrganismoAfiliado,$anoAtividadeIniciaticaOficial){

        $query = "SELECT tipoAtividadeIniciaticaOficial FROM atividadeIniciaticaOficial
                    where fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado
                    and anoAtividadeIniciaticaOficial=:anoAtividadeIniciaticaOficial
                    ORDER BY anoAtividadeIniciaticaOficial DESC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':anoAtividadeIniciaticaOficial', $anoAtividadeIniciaticaOficial, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function retornaOficiaisOrganismo($fk_idOrganismoAfiliado,$seqCadastMembroOa){

        $query = "SELECT * FROM membroOa
                    where fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado
                    and seqCadastMembroOa=:seqCadastMembroOa LIMIT 1";

        //echo $query;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembroOa', $seqCadastMembroOa, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}

?>
