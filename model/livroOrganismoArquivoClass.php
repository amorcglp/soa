<?php

require_once ("conexaoClass.php");

class livroOrganismoArquivo{

    private $idLivroOrganismoArquivo;
    private $fk_idLivroOrganismo;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdLivroOrganismoArquivo() {
        return $this->idLivroOrganismoArquivo;
    }

    function getFkIdLivroOrganismo() {
        return $this->fk_idLivroOrganismo;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdLivroOrganismoArquivo($idLivroOrganismoArquivo) {
        $this->idLivroOrganismoArquivo = $idLivroOrganismoArquivo;
    }

    function setFkIdLivroOrganismo($fk_idLivroOrganismo) {
        $this->fk_idLivroOrganismo = $fk_idLivroOrganismo;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    //--------------------------------------------------------------------------

    public function cadastroLivroOrganismoArquivo(){

        $sql="INSERT INTO livroOrganismo_arquivo  
                                 (
                                  fk_idLivroOrganismo,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:id,
                                    :caminho,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idLivroOrganismo, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'livroOrganismo_arquivo'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
	        foreach ($resultado as $vetor)
	    	{
	    		$proximo_id = $vetor['Auto_increment']; 
	    	}
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaLivroOrganismoArquivo($id){

    	$sql = "SELECT * FROM livroOrganismo_arquivo
    	where fk_idLivroOrganismo = :id";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeLivroOrganismoArquivo($idLivroOrganismoArquivo){
        
        $sql = "DELETE FROM livroOrganismo_arquivo WHERE idLivroOrganismoArquivo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idLivroOrganismoArquivo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    
}

?>
