<?php

require_once ("conexaoClass.php");

class ataReuniaoMensalAssinada{

    private $idAtaReuniaoMensalAssinada;
    private $fk_idAtaReuniaoMensal;
    private $caminhoAtaReuniaoMensalAssinada;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtaReuniaoMensalAssinada() {
        return $this->idAtaReuniaoMensalAssinada;
    }

    function getFkIdAtaReuniaoMensal() {
        return $this->fk_idAtaReuniaoMensal;
    }

    function getCaminhoAtaReuniaoMensalAssinada() {
        return $this->caminhoAtaReuniaoMensalAssinada;
    }

    function setIdAtaReuniaoMensalAssinada($idAtaReuniaoMensalAssinada) {
        $this->idAtaReuniaoMensalAssinada = $idAtaReuniaoMensalAssinada;
    }

    function setFkIdAtaReuniaoMensal($fk_idAtaReuniaoMensal) {
        $this->fk_idAtaReuniaoMensal = $fk_idAtaReuniaoMensal;
    }

    function setCaminhoAtaReuniaoMensalAssinada($caminhoAtaReuniaoMensalAssinada) {
        $this->caminhoAtaReuniaoMensalAssinada = $caminhoAtaReuniaoMensalAssinada;
    }

    //--------------------------------------------------------------------------

    public function cadastroAtaReuniaoMensalAssinada(){

        $sql = "INSERT INTO ataReuniaoMensal_assinada
                                 (
                                  fk_idAtaReuniaoMensal,
                                  caminhoAtaReuniaoMensalAssinada,
                                  dataCadastro
                                  )
                            VALUES (:fk_idAtaReuniaoMensal,
                                    :caminhoAtaReuniaoMensalAssinada,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtaReuniaoMensal', $this->fk_idAtaReuniaoMensal, PDO::PARAM_INT);
        $sth->bindParam(':caminhoAtaReuniaoMensalAssinada', $this->caminhoAtaReuniaoMensalAssinada, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE 'ataReuniaoMensal_assinada'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function listaAtaReuniaoMensalAssinada($id=null,$mes=null,$ano=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM ataReuniaoMensal_assinada
    	inner join ataReuniaoMensal on fk_idAtaReuniaoMensal = idAtaReuniaoMensal where 1=1";

    	if($id!=null){
    		$sql .= " and fk_idAtaReuniaoMensal = :fk_idAtaReuniaoMensal";
    	}
    	if($mes!=null){
    		$sql .= " and mesCompetencia = :mesCompetencia";
    	}
    	if($ano!=null){
    		$sql .= " and anoCompetencia = :anoCompetencia";
    	}
    	if($idOrganismoAfiliado!=null){
    		$sql .= " and fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado";
    	}

    	//echo "<br><br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($id!=null){
            $sth->bindParam(':fk_idAtaReuniaoMensal', $id, PDO::PARAM_INT);
        }
        if($mes!=null){
            $sth->bindParam(':mesCompetencia', $mes, PDO::PARAM_INT);
        }
        if($ano!=null){
            $sth->bindParam(':anoCompetencia', $ano, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null){
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeAtaReuniaoMensalAssinada($idAtaReuniaoMensalAssinada){

        $sql = "DELETE FROM ataReuniaoMensal_assinada WHERE idAtaReuniaoMensalAssinada = :idAtaReuniaoMensalAssinada";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaReuniaoMensalAssinada', $idAtaReuniaoMensalAssinada, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


}

?>
