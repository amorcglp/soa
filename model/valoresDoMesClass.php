<?php

require_once ("conexaoClass.php");

class valoresDoMes{

    private $mes;
    private $ano;
    private $dataValores;
    private $fk_idOrganismoAfiliado;
    private $totalEntradasDoMes;
    private $totalSaidasDoMes;
    private $totalSaldoDoMes;

    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @param mixed $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getDataValores()
    {
        return $this->dataValores;
    }

    /**
     * @param mixed $dataValores
     */
    public function setDataValores($dataValores)
    {
        $this->dataValores = $dataValores;
    }

    /**
     * @return mixed
     */
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }

    /**
     * @param mixed $fk_idOrganismoAfiliado
     */
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getTotalEntradasDoMes()
    {
        return $this->totalEntradasDoMes;
    }

    /**
     * @param mixed $totalEntradasDoMes
     */
    public function setTotalEntradasDoMes($totalEntradasDoMes)
    {
        $this->totalEntradasDoMes = $totalEntradasDoMes;
    }

    /**
     * @return mixed
     */
    public function getTotalSaidasDoMes()
    {
        return $this->totalSaidasDoMes;
    }

    /**
     * @param mixed $totalSaidasDoMes
     */
    public function setTotalSaidasDoMes($totalSaidasDoMes)
    {
        $this->totalSaidasDoMes = $totalSaidasDoMes;
    }

    /**
     * @return mixed
     */
    public function getTotalSaldoDoMes()
    {
        return $this->totalSaldoDoMes;
    }

    /**
     * @param mixed $totalSaldoDoMes
     */
    public function setTotalSaldoDoMes($totalSaldoDoMes)
    {
        $this->totalSaldoDoMes = $totalSaldoDoMes;
    }




    public function cadastro(){

    	$sql = "INSERT INTO valoresDoMes  
                                 (mes,
                                  ano,
                                  dataValores,
                                  fk_idOrganismoAfiliado,
                                  dataCadastro,
                                  totalEntradasDoMes,
                                  totalSaidasDoMes,
                                  totalSaldoDoMes
                                  )
                            VALUES (
                                    :mes,
                                    :ano,
                                    :dataValores,
                            		:idOrganismoAfiliado,
                                    now(),
                                    :totalEntradasDoMes,
                                    :totalSaidasDoMes,
                                    :totalSaldoDoMes
                                    )";
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':dataValores', $this->dataValores, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':totalEntradasDoMes', $this->totalEntradasDoMes, PDO::PARAM_STR);
        $sth->bindParam(':totalSaidasDoMes', $this->totalSaidasDoMes, PDO::PARAM_STR);
        $sth->bindParam(':totalSaldoDoMes', $this->totalSaldoDoMes, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function lista($mes=null,$ano=null,$idOrganismoAfiliado=null,$ultimoRegistro=null){

        $sql = "SELECT 
                      CAST(totalEntradasDoMes  AS DECIMAL(12,2)) as totalEntradasDoMes, 
                      CAST(totalSaidasDoMes  AS DECIMAL(12,2)) as totalSaidasDoMes, 
                      CAST(totalSaldoDoMes  AS DECIMAL(12,2)) as totalSaldoDoMes
                      FROM valoresDoMes
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";

        if($mes!=null)
        {
            $sql .= " and mes=:mes";
        }

        if($ano!=null)
        {
            $sql .= " and ano=:ano";
        }

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }

        if($ultimoRegistro!=null)
        {
            $sql .= " order by ano,mes DESC LIMIT 0,1";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mes!=null)
        {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }

        if($ano!=null)
        {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function atualizaValoresDoMes($mes,$ano,$idOrganismoAfiliado,$saldoMes,$totalEntradas,$totalSaidas){

        $sql="UPDATE valoresDoMes SET  
                                  totalEntradasDoMes = :totalEntradasDoMes,
                                  totalSaidasDoMes = :totalSaidasDoMes,
                                  totalSaldoDoMes = :totalSaldoDoMes,
                                  dataCadastro = now()    
                                  WHERE mes = :mes and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado
                            ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':totalEntradasDoMes', $totalEntradas, PDO::PARAM_STR);
        $sth->bindParam(':totalSaidasDoMes', $totalSaidas, PDO::PARAM_STR);
        $sth->bindParam(':totalSaldoDoMes', $saldoMes, PDO::PARAM_STR);
        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function updateValoresDoMesCompleto()
    {
        $sql="UPDATE valoresDoMes SET  
                                  totalEntradasDoMes = :totalEntradasDoMes,
                                  totalSaidasDoMes = :totalSaidasDoMes,
                                  totalSaldoDoMes = :totalSaldoDoMes,
                                  dataCadastro = now()    
                                  WHERE mes = :mes and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado
                            ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':totalEntradasDoMes', $this->totalEntradasDoMes, PDO::PARAM_STR);
        $sth->bindParam(':totalSaidasDoMes', $this->totalSaidasDoMes, PDO::PARAM_STR);
        $sth->bindParam(':totalSaldoDoMes', $this->totalSaldoDoMes, PDO::PARAM_STR);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }

    }

    public function limpar($idOrganismoAfiliado=null){

        $sql = "DELETE FROM valoresDoMes
    	 where fk_idOrganismoAfiliado=:id";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}

?>
