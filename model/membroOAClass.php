<?php

require_once ("conexaoClass.php");

class membroOA{

    private $idMembroOa;    
    private $fk_idOrganismoAfiliado;
    private $codigoAfiliacao;
    private $seqCadastMembroOa;
    private $principalCompanheiro;
    private $nomeMembroOa;
    private $usuario;

    //METODO SET/GET -----------------------------------------------------------

    function getIdMembroOa() {
        return $this->idMembroOa;
    }
    
	function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    
	function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }
    
	function getNomeMembroOa() {
        return $this->nomeMembroOa;
    }
    
	function getSeqCadastMembroOa() {
        return $this->seqCadastMembroOa;
    }
    
	function getPrincipalCompanheiro() {
        return $this->principalCompanheiro;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
    function setIdMembroOa($idMembroOa) {
        $this->idMembroOa = $idMembroOa;
    }
    
	function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }
    
	function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }
    
	function setNomeMembroOa($nomeMembroOa) {
        $this->nomeMembroOa = $nomeMembroOa;
    }
    
	function setSeqCadastMembroOa($seqCadastMembroOa) {
        $this->seqCadastMembroOa = $seqCadastMembroOa;
    }
    
	function setPrincipalCompanheiro($principalCompanheiro) {
        $this->principalCompanheiro = $principalCompanheiro;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    //--------------------------------------------------------------------------

    public function cadastroMembroOa(){

        $sql="INSERT INTO membroOA  
                                 (fk_idOrganismoAfiliado,
                                  codigoAfiliacao,
                                  nomeMembroOa,
                                  seqCadastMembroOa,
                                  principalCompanheiro,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                            		:idOrganismoAfiliado,
                            		:codigoAfiliacao,
                            		:nome,
                            		:seqCadast,
                            		:principal,
                            		:usuario,
                                    now()
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nomeMembroOa, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);
        $sth->bindParam(':principal', $this->principalCompanheiro, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function atualizaOrganismoMembroOa(){

        $sql="UPDATE membroOA SET  
                                  fk_idOrganismoAfiliado=:idOrganismoAfiliado
                                  WHERE seqCadastMembroOa=:seqCadast
                                  ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);
       
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaFalecimento(){

        $sql="UPDATE membroOA SET  
                                  falecimento=1
                                  WHERE seqCadastMembroOa=:seqCadast
                                  ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaNaoFalecimento(){

        $sql="UPDATE membroOA SET  
                                  falecimento=0
                                  WHERE seqCadastMembroOa=:seqCadast
                                  ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idMembroOa) as lastid FROM membroOA";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	
    
    public function listaMembroOA($idOrganismoAfiliado=null,$falecimento=null,$letraNome=null){

    	$sql = "SELECT * FROM membroOA
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1 ";

        if ($letraNome != null && $letraNome != "todos") {
            $sql .= " and nomeMembroOa like :letraNome";
        }

    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}

        if($falecimento!=null||$falecimento==0)
        {
            $sql .= " and falecimento = :falecimento";
        }

    	$sql .= " order by codigoAfiliacao";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if($falecimento!=null||$falecimento==0)
        {
            $sth->bindParam(':falecimento', $falecimento, PDO::PARAM_INT);
        }

        if ($letraNome != null && $letraNome != "todos") {
            //echo "1";
            $ln=$letraNome."%";
            $sth->bindParam(':letraNome', $ln, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($seqCadastMembroOa){

    	$sql = "SELECT * FROM membroOA 
    			where seqCadastMembroOa=:seqCadast";
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadastMembroOa, PDO::PARAM_INT);
      
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaMembroOaPeloSeqCadast($seqCadast){

        $sql = "SELECT * FROM  membroOA WHERE   seqCadastMembroOa = :seqCadast";

        //echo $sql."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
}

?>
