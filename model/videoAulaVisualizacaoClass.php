<?php

require_once ("conexaoClass.php");

class videoAulaVisualizacao{

    private $idVideoAulaVisualizacao;
    private $seqCadast;
    private $fk_idVideoAula;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdVideoAulaVisualizacao() {
        return $this->idVideoAulaVisualizacao;
    }

    function getSeqCadast() {
        return $this->seqCadast;
    }

    function getFk_idVideoAula() {
        return $this->fk_idVideoAula;
    }

    function setIdVideoAulaVisualizacao($idVideoAulaVisualizacao) {
        $this->idVideoAulaVisualizacao = $idVideoAulaVisualizacao;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    function setFk_idVideoAula($fk_idVideoAula) {
        $this->fk_idVideoAula = $fk_idVideoAula;
    }

    
    //--------------------------------------------------------------------------

    public function cadastroVideoAulaVisualizacao(){

        $sql="INSERT INTO videoAulaVisualizacao  
                                 (seqCadast,
                                  fk_idVideoAula,
                                  dataCadastro)
                                  
                            VALUES (:seqCadast,
                                    :fk_idVideoAula,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idVideoAula', $this->fk_idVideoAula, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function contaVisualizacao($seqCadast,$idVideoAula=null){

    	$sql = " SELECT * FROM videoAulaVisualizacao "
                . " inner join videoAula on idVideoAula = fk_idVideoAula "
                . " where seqCadast = :seqCadast "
                . " and status = 0 ";
        if($idVideoAula!=null)
        {
            $sql .= " and fk_idVideoAula = :idVideoAula";
        }    
        $sql .= " group by idVideoAula";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        if($idVideoAula!=null)
        {
            $sth->bindParam(':idVideoAula', $idVideoAula, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }
    
}

?>
