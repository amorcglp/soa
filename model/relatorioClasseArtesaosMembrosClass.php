<?php

require_once ("conexaoClass.php");

class relatorioClasseArtesaosMembros{

    private $idRelatorioClasseArtesaosMembros;
    private $fk_idRelatorioClasseArtesaos;
    private $fk_seq_cadastMembro;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdRelatorioClasseArtesaosMembros() {
        return $this->idRelatorioClasseArtesaosMembros;
    }

    function getFk_idRelatorioClasseArtesaos() {
        return $this->fk_idRelatorioClasseArtesaos;
    }

    function getFk_seq_cadastMembro() {
        return $this->fk_seq_cadastMembro;
    }

    function setIdRelatorioClasseArtesaosMembros($idRelatorioClasseArtesaosMembros) {
        $this->idRelatorioClasseArtesaosMembros = $idRelatorioClasseArtesaosMembros;
    }

    function setFk_idRelatorioClasseArtesaos($fk_idRelatorioClasseArtesaos) {
        $this->fk_idRelatorioClasseArtesaos = $fk_idRelatorioClasseArtesaos;
    }

    function setFk_seq_cadastMembro($fk_seq_cadastMembro) {
        $this->fk_seq_cadastMembro = $fk_seq_cadastMembro;
    }

    //--------------------------------------------------------------------------

    public function cadastro(){

        $sql="INSERT INTO relatorioClasseArtesaos_membros  
                                 (
                                  fk_idRelatorioClasseArtesaos,
                                  fk_seq_cadastMembro
                                  )
                                   
                            VALUES (:idRelatorioClasseArtesaos,
                                    :seqCadastMembro)";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idRelatorioClasseArtesaos', $this->fk_idRelatorioClasseArtesaos, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $this->fk_seq_cadastMembro, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function lista($idRelatorioClasseArtesaos=null,$v=null){

    	$sql = "SELECT * FROM relatorioClasseArtesaos_membros
    	where 1=1 
    	 ";   
    	if($idRelatorioClasseArtesaos!=null)
    	{
    		$sql .= " and fk_idRelatorioClasseArtesaos=:id";
    	}	 
        if($v!=null)
    	{
    		$sql .= " and fk_seq_cadastMembro=:v";
    	}
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idRelatorioClasseArtesaos!=null)
    	{
            $sth->bindParam(':id', $idRelatorioClasseArtesaos, PDO::PARAM_INT);
        }
        if($v!=null)
    	{
            $sth->bindParam(':v', $v, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function remove($idRelatorioClasseArtesaos=null,$id=null)
    {
        $sql= "DELETE FROM relatorioClasseArtesaos_membros WHERE 1=1";
    	if($idRelatorioClasseArtesaos!=null)
    	{
    		$sql .= " and fk_idRelatorioClasseArtesaos = :idRelatorioClasseArtesaos";
    	}
    	if($id!=null)
    	{
    		$sql .= " and idRelatorioClasseArtesaosMembros = :id";
    	}
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idRelatorioClasseArtesaos!=null)
    	{
            $sth->bindParam(':idRelatorioClasseArtesaos', $idRelatorioClasseArtesaos, PDO::PARAM_INT);
        }
        if($id!=null)
    	{
            $sth->bindParam(':id',$id , PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
