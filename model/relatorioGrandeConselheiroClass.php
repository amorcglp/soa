<?php

require_once ("conexaoClass.php");

class RelatorioGrandeConselheiro {

    private $idRelatorioGrandeConselheiro;
    private $criadoPor;
    private $criadoPorId;
    private $codigoAfiliacao;
    private $dataCriacao;
    private $regiao;
    private $semestre;
    private $ano;
    private $pergunta1;
    private $pergunta3;
    private $pergunta4;
    private $pergunta5;
    private $pergunta6;
    private $pergunta7;
    private $pergunta8;
    private $pergunta9;
    private $pergunta10;
    private $pergunta11;
    private $pergunta12;
    private $pergunta14;
    private $pergunta15;
    private $pergunta16;
    private $pergunta17;
    private $pergunta18;
    private $pergunta19;
    private $pergunta20;
    private $pergunta21;
    private $pergunta22;
    private $pergunta23;
    private $pergunta24;
    private $pergunta25;
    private $pergunta26;
    private $pergunta27;
    private $pergunta28;
    private $pergunta29;
    private $pergunta30;
    private $pergunta31;
    private $pergunta32;
    private $pergunta33;
    private $pergunta34;
    private $pergunta35;
    private $pergunta36;
    private $pergunta37;
    private $comentario1;
    private $comentario3;
    private $comentario4;
    private $comentario5;
    private $comentario6;
    private $comentario7;
    private $comentario8;
    private $comentario9;
    private $comentario10;
    private $comentario11;
    private $comentario12;
    private $comentario14;
    private $comentario15;
    private $comentario16;
    private $comentario17;
    private $comentario18;
    private $comentario19;
    private $comentario20;
    private $comentario21;
    private $comentario22;
    private $comentario23;
    private $comentario24;
    private $comentario25;
    private $comentario26;
    private $comentario27;
    private $comentario28;
    private $comentario29;
    private $comentario30;
    private $comentario31;
    private $comentario32;
    private $comentario33;
    private $comentario34;
    private $comentario35;
    private $comentario36;
    private $comentario37;

    function getIdRelatorioGrandeConselheiro() {
        return $this->idRelatorioGrandeConselheiro;
    }

    function getCriadoPor() {
        return $this->criadoPor;
    }

    function getCriadoPorId() {
        return $this->criadoPorId;
    }

    function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }

    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function getRegiao() {
        return $this->regiao;
    }

    function getSemestre() {
        return $this->semestre;
    }

    function getAno() {
        return $this->ano;
    }

    function getPergunta1() {
        return $this->pergunta1;
    }

    function getPergunta3() {
        return $this->pergunta3;
    }

    function getPergunta4() {
        return $this->pergunta4;
    }

    function getPergunta5() {
        return $this->pergunta5;
    }

    function getPergunta6() {
        return $this->pergunta6;
    }

    function getPergunta7() {
        return $this->pergunta7;
    }

    function getPergunta8() {
        return $this->pergunta8;
    }

    function getPergunta9() {
        return $this->pergunta9;
    }

    function getPergunta10() {
        return $this->pergunta10;
    }

    function getPergunta11() {
        return $this->pergunta11;
    }

    function getPergunta12() {
        return $this->pergunta12;
    }

    function getPergunta14() {
        return $this->pergunta14;
    }

    function getPergunta15() {
        return $this->pergunta15;
    }

    function getPergunta16() {
        return $this->pergunta16;
    }

    function getPergunta17() {
        return $this->pergunta17;
    }

    function getPergunta18() {
        return $this->pergunta18;
    }

    function getPergunta19() {
        return $this->pergunta19;
    }

    function getPergunta20() {
        return $this->pergunta20;
    }

    function getPergunta21() {
        return $this->pergunta21;
    }

    function getPergunta22() {
        return $this->pergunta22;
    }

    function getPergunta23() {
        return $this->pergunta23;
    }

    function getPergunta24() {
        return $this->pergunta24;
    }

    function getPergunta25() {
        return $this->pergunta25;
    }

    function getPergunta26() {
        return $this->pergunta26;
    }

    function getPergunta27() {
        return $this->pergunta27;
    }

    function getPergunta28() {
        return $this->pergunta28;
    }

    function getPergunta29() {
        return $this->pergunta29;
    }

    function getPergunta30() {
        return $this->pergunta30;
    }

    function getPergunta31() {
        return $this->pergunta31;
    }

    function getPergunta32() {
        return $this->pergunta32;
    }

    function getPergunta33() {
        return $this->pergunta33;
    }

    function getPergunta34() {
        return $this->pergunta34;
    }

    function getPergunta35() {
        return $this->pergunta35;
    }

    function getPergunta36() {
        return $this->pergunta36;
    }

    function getPergunta37() {
        return $this->pergunta37;
    }

    function getComentario1() {
        return $this->comentario1;
    }

    function getComentario3() {
        return $this->comentario3;
    }

    function getComentario4() {
        return $this->comentario4;
    }

    function getComentario5() {
        return $this->comentario5;
    }

    function getComentario6() {
        return $this->comentario6;
    }

    function getComentario7() {
        return $this->comentario7;
    }

    function getComentario8() {
        return $this->comentario8;
    }

    function getComentario9() {
        return $this->comentario9;
    }

    function getComentario10() {
        return $this->comentario10;
    }

    function getComentario11() {
        return $this->comentario11;
    }

    function getComentario12() {
        return $this->comentario12;
    }

    function getComentario14() {
        return $this->comentario14;
    }

    function getComentario15() {
        return $this->comentario15;
    }

    function getComentario16() {
        return $this->comentario16;
    }

    function getComentario17() {
        return $this->comentario17;
    }

    function getComentario18() {
        return $this->comentario18;
    }

    function getComentario19() {
        return $this->comentario19;
    }

    function getComentario20() {
        return $this->comentario20;
    }

    function getComentario21() {
        return $this->comentario21;
    }

    function getComentario22() {
        return $this->comentario22;
    }

    function getComentario23() {
        return $this->comentario23;
    }

    function getComentario24() {
        return $this->comentario24;
    }

    function getComentario25() {
        return $this->comentario25;
    }

    function getComentario26() {
        return $this->comentario26;
    }

    function getComentario27() {
        return $this->comentario27;
    }

    function getComentario28() {
        return $this->comentario28;
    }

    function getComentario29() {
        return $this->comentario29;
    }

    function getComentario30() {
        return $this->comentario30;
    }

    function getComentario31() {
        return $this->comentario31;
    }

    function getComentario32() {
        return $this->comentario32;
    }

    function getComentario33() {
        return $this->comentario33;
    }

    function getComentario34() {
        return $this->comentario34;
    }

    function getComentario35() {
        return $this->comentario35;
    }

    function getComentario36() {
        return $this->comentario36;
    }

    function getComentario37() {
        return $this->comentario37;
    }

    function setIdRelatorioGrandeConselheiro($idRelatorioGrandeConselheiro) {
        $this->idRelatorioGrandeConselheiro = $idRelatorioGrandeConselheiro;
    }

    function setCriadoPor($criadoPor) {
        $this->criadoPor = $criadoPor;
    }

    function setCriadoPorId($criadoPorId) {
        $this->criadoPorId = $criadoPorId;
    }

    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }

    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function setRegiao($regiao) {
        $this->regiao = $regiao;
    }

    function setSemestre($semestre) {
        $this->semestre = $semestre;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setPergunta1($pergunta1) {
        $this->pergunta1 = $pergunta1;
    }

    function setPergunta3($pergunta3) {
        $this->pergunta3 = $pergunta3;
    }

    function setPergunta4($pergunta4) {
        $this->pergunta4 = $pergunta4;
    }

    function setPergunta5($pergunta5) {
        $this->pergunta5 = $pergunta5;
    }

    function setPergunta6($pergunta6) {
        $this->pergunta6 = $pergunta6;
    }

    function setPergunta7($pergunta7) {
        $this->pergunta7 = $pergunta7;
    }

    function setPergunta8($pergunta8) {
        $this->pergunta8 = $pergunta8;
    }

    function setPergunta9($pergunta9) {
        $this->pergunta9 = $pergunta9;
    }

    function setPergunta10($pergunta10) {
        $this->pergunta10 = $pergunta10;
    }

    function setPergunta11($pergunta11) {
        $this->pergunta11 = $pergunta11;
    }

    function setPergunta12($pergunta12) {
        $this->pergunta12 = $pergunta12;
    }

    function setPergunta14($pergunta14) {
        $this->pergunta14 = $pergunta14;
    }

    function setPergunta15($pergunta15) {
        $this->pergunta15 = $pergunta15;
    }

    function setPergunta16($pergunta16) {
        $this->pergunta16 = $pergunta16;
    }

    function setPergunta17($pergunta17) {
        $this->pergunta17 = $pergunta17;
    }

    function setPergunta18($pergunta18) {
        $this->pergunta18 = $pergunta18;
    }

    function setPergunta19($pergunta19) {
        $this->pergunta19 = $pergunta19;
    }

    function setPergunta20($pergunta20) {
        $this->pergunta20 = $pergunta20;
    }

    function setPergunta21($pergunta21) {
        $this->pergunta21 = $pergunta21;
    }

    function setPergunta22($pergunta22) {
        $this->pergunta22 = $pergunta22;
    }

    function setPergunta23($pergunta23) {
        $this->pergunta23 = $pergunta23;
    }

    function setPergunta24($pergunta24) {
        $this->pergunta24 = $pergunta24;
    }

    function setPergunta25($pergunta25) {
        $this->pergunta25 = $pergunta25;
    }

    function setPergunta26($pergunta26) {
        $this->pergunta26 = $pergunta26;
    }

    function setPergunta27($pergunta27) {
        $this->pergunta27 = $pergunta27;
    }

    function setPergunta28($pergunta28) {
        $this->pergunta28 = $pergunta28;
    }

    function setPergunta29($pergunta29) {
        $this->pergunta29 = $pergunta29;
    }

    function setPergunta30($pergunta30) {
        $this->pergunta30 = $pergunta30;
    }

    function setPergunta31($pergunta31) {
        $this->pergunta31 = $pergunta31;
    }

    function setPergunta32($pergunta32) {
        $this->pergunta32 = $pergunta32;
    }

    function setPergunta33($pergunta33) {
        $this->pergunta33 = $pergunta33;
    }

    function setPergunta34($pergunta34) {
        $this->pergunta34 = $pergunta34;
    }

    function setPergunta35($pergunta35) {
        $this->pergunta35 = $pergunta35;
    }

    function setPergunta36($pergunta36) {
        $this->pergunta36 = $pergunta36;
    }

    function setPergunta37($pergunta37) {
        $this->pergunta37 = $pergunta37;
    }

    function setComentario1($comentario1) {
        $this->comentario1 = $comentario1;
    }

    function setComentario3($comentario3) {
        $this->comentario3 = $comentario3;
    }

    function setComentario4($comentario4) {
        $this->comentario4 = $comentario4;
    }

    function setComentario5($comentario5) {
        $this->comentario5 = $comentario5;
    }

    function setComentario6($comentario6) {
        $this->comentario6 = $comentario6;
    }

    function setComentario7($comentario7) {
        $this->comentario7 = $comentario7;
    }

    function setComentario8($comentario8) {
        $this->comentario8 = $comentario8;
    }

    function setComentario9($comentario9) {
        $this->comentario9 = $comentario9;
    }

    function setComentario10($comentario10) {
        $this->comentario10 = $comentario10;
    }

    function setComentario11($comentario11) {
        $this->comentario11 = $comentario11;
    }

    function setComentario12($comentario12) {
        $this->comentario12 = $comentario12;
    }

    function setComentario14($comentario14) {
        $this->comentario14 = $comentario14;
    }

    function setComentario15($comentario15) {
        $this->comentario15 = $comentario15;
    }

    function setComentario16($comentario16) {
        $this->comentario16 = $comentario16;
    }

    function setComentario17($comentario17) {
        $this->comentario17 = $comentario17;
    }

    function setComentario18($comentario18) {
        $this->comentario18 = $comentario18;
    }

    function setComentario19($comentario19) {
        $this->comentario19 = $comentario19;
    }

    function setComentario20($comentario20) {
        $this->comentario20 = $comentario20;
    }

    function setComentario21($comentario21) {
        $this->comentario21 = $comentario21;
    }

    function setComentario22($comentario22) {
        $this->comentario22 = $comentario22;
    }

    function setComentario23($comentario23) {
        $this->comentario23 = $comentario23;
    }

    function setComentario24($comentario24) {
        $this->comentario24 = $comentario24;
    }

    function setComentario25($comentario25) {
        $this->comentario25 = $comentario25;
    }

    function setComentario26($comentario26) {
        $this->comentario26 = $comentario26;
    }

    function setComentario27($comentario27) {
        $this->comentario27 = $comentario27;
    }

    function setComentario28($comentario28) {
        $this->comentario28 = $comentario28;
    }

    function setComentario29($comentario29) {
        $this->comentario29 = $comentario29;
    }

    function setComentario30($comentario30) {
        $this->comentario30 = $comentario30;
    }

    function setComentario31($comentario31) {
        $this->comentario31 = $comentario31;
    }

    function setComentario32($comentario32) {
        $this->comentario32 = $comentario32;
    }

    function setComentario33($comentario33) {
        $this->comentario33 = $comentario33;
    }

    function setComentario34($comentario34) {
        $this->comentario34 = $comentario34;
    }

    function setComentario35($comentario35) {
        $this->comentario35 = $comentario35;
    }

    function setComentario36($comentario36) {
        $this->comentario36 = $comentario36;
    }

    function setComentario37($comentario37) {
        $this->comentario37 = $comentario37;
    }

//--------------------------------------------------------------------------

    public function cadastroRelatorioGrandeConselheiro() {

        $sql = "INSERT INTO relatorioGrandeConselheiro  
                                 (
                                  criadoPor,
                                  dataCriacao,
                                  regiao,
                                  semestre,
                                  ano,
                                  pergunta1,
                                  pergunta3,
                                  pergunta4,
                                  pergunta5,
                                  pergunta6,
                                  pergunta7,
                                  pergunta8,
                                  pergunta9,
                                  pergunta10,
                                  pergunta11,
                                  pergunta12,
                                  pergunta14,
                                  pergunta15,
                                  pergunta16,
                                  pergunta17,
                                  pergunta18,
                                  pergunta19,
                                  pergunta20,
                                  pergunta21,
                                  pergunta22,
                                  pergunta23,
                                  pergunta24,
                                  pergunta25,
                                  pergunta26,
                                  pergunta27,
                                  pergunta28,
                                  pergunta29,
                                  pergunta30,
                                  pergunta31,
                                  pergunta32,
                                  pergunta33,
                                  pergunta34,
                                  pergunta35,
                                  pergunta36,
                                  pergunta37,
                                  comentario1,
                                  comentario3,
                                  comentario4,
                                  comentario5,
                                  comentario6,
                                  comentario7,
                                  comentario8,
                                  comentario9,
                                  comentario10,
                                  comentario11,
                                  comentario12,
                                  comentario14,
                                  comentario15,
                                  comentario16,
                                  comentario17,
                                  comentario18,
                                  comentario19,
                                  comentario20,
                                  comentario21,
                                  comentario22,
                                  comentario23,
                                  comentario24,
                                  comentario25,
                                  comentario26,
                                  comentario27,
                                  comentario28,
                                  comentario29,
                                  comentario30,
                                  comentario31,
                                  comentario32,
                                  comentario33,
                                  comentario34,
                                  comentario35,
                                  comentario36,
                                  comentario37
                                  )
                                   
                            VALUES (
                                    :criadoPor,
                                    now(),
                                    :regiao,
                                    :semestre,
                                    :ano,
                                    :pergunta1,
                                    :pergunta3,
                                    :pergunta4,
                                    :pergunta5,
                                    :pergunta6,
                                    :pergunta7,
                                    :pergunta8,
                                    :pergunta9,
                                    :pergunta10,
                                    :pergunta11,
                                    :pergunta12,
                                    :pergunta14,
                                    :pergunta15,
                                    :pergunta16,
                                    :pergunta17,
                                    :pergunta18,
                                    :pergunta19,
                                    :pergunta20,
                                    :pergunta21,
                                    :pergunta22,
                                    :pergunta23,
                                    :pergunta24,
                                    :pergunta25,
                                    :pergunta26,
                                    :pergunta27,
                                    :pergunta28,
                                    :pergunta29,
                                    :pergunta30,
                                    :pergunta31,
                                    :pergunta32,
                                    :pergunta33,
                                    :pergunta34,
                                    :pergunta35,
                                    :pergunta36,
                                    :pergunta37,
                                    :comentario1,
                                    :comentario3,
                                    :comentario4,
                                    :comentario5,
                                    :comentario6,
                                    :comentario7,
                                    :comentario8,
                                    :comentario9,
                                    :comentario10,
                                    :comentario11,
                                    :comentario12,
                                    :comentario14,
                                    :comentario15,
                                    :comentario16,
                                    :comentario17,
                                    :comentario18,
                                    :comentario19,
                                    :comentario20,
                                    :comentario21,
                                    :comentario22,
                                    :comentario23,
                                    :comentario24,
                                    :comentario25,
                                    :comentario26,
                                    :comentario27,
                                    :comentario28,
                                    :comentario29,
                                    :comentario30,
                                    :comentario31,
                                    :comentario32,
                                    :comentario33,
                                    :comentario34,
                                    :comentario35,
                                    :comentario36,
                                    :comentario37
                                    
                                    )";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getCriadoPor               = $this->getCriadoPor();
        $getRegiao                  = $this->getRegiao();
        $getSemestre                = $this->getSemestre();
        $getAno                     = $this->getAno();

        $sth->bindParam(':criadoPor', $getCriadoPor, PDO::PARAM_INT);
        $sth->bindParam(':regiao', $getRegiao, PDO::PARAM_INT);
        $sth->bindParam(':semestre', $getSemestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $getAno, PDO::PARAM_INT);

        $sth->bindParam(':pergunta1', $this->pergunta1, PDO::PARAM_INT);
        $sth->bindParam(':pergunta3', $this->pergunta3, PDO::PARAM_INT);
        $sth->bindParam(':pergunta4', $this->pergunta4, PDO::PARAM_INT);
        $sth->bindParam(':pergunta5', $this->pergunta5, PDO::PARAM_INT);
        $sth->bindParam(':pergunta6', $this->pergunta6, PDO::PARAM_INT);
        $sth->bindParam(':pergunta7', $this->pergunta7, PDO::PARAM_INT);
        $sth->bindParam(':pergunta8', $this->pergunta8, PDO::PARAM_INT);
        $sth->bindParam(':pergunta9', $this->pergunta8, PDO::PARAM_INT);
        $sth->bindParam(':pergunta10', $this->pergunta10, PDO::PARAM_INT);
        $sth->bindParam(':pergunta11', $this->pergunta11, PDO::PARAM_INT);
        $sth->bindParam(':pergunta12', $this->pergunta12, PDO::PARAM_INT);
        $sth->bindParam(':pergunta14', $this->pergunta14, PDO::PARAM_INT);
        $sth->bindParam(':pergunta15', $this->pergunta15, PDO::PARAM_INT);
        $sth->bindParam(':pergunta16', $this->pergunta16, PDO::PARAM_INT);
        $sth->bindParam(':pergunta17', $this->pergunta17, PDO::PARAM_INT);
        $sth->bindParam(':pergunta18', $this->pergunta18, PDO::PARAM_INT);
        $sth->bindParam(':pergunta19', $this->pergunta19, PDO::PARAM_INT);
        $sth->bindParam(':pergunta20', $this->pergunta20, PDO::PARAM_INT);
        $sth->bindParam(':pergunta21', $this->pergunta21, PDO::PARAM_INT);
        $sth->bindParam(':pergunta22', $this->pergunta22, PDO::PARAM_INT);
        $sth->bindParam(':pergunta23', $this->pergunta23, PDO::PARAM_INT);
        $sth->bindParam(':pergunta24', $this->pergunta24, PDO::PARAM_INT);
        $sth->bindParam(':pergunta25', $this->pergunta25, PDO::PARAM_INT);
        $sth->bindParam(':pergunta26', $this->pergunta26, PDO::PARAM_INT);
        $sth->bindParam(':pergunta27', $this->pergunta27, PDO::PARAM_INT);
        $sth->bindParam(':pergunta28', $this->pergunta28, PDO::PARAM_INT);
        $sth->bindParam(':pergunta29', $this->pergunta29, PDO::PARAM_INT);
        $sth->bindParam(':pergunta30', $this->pergunta30, PDO::PARAM_INT);
        $sth->bindParam(':pergunta31', $this->pergunta31, PDO::PARAM_INT);
        $sth->bindParam(':pergunta32', $this->pergunta32, PDO::PARAM_INT);
        $sth->bindParam(':pergunta33', $this->pergunta33, PDO::PARAM_INT);
        $sth->bindParam(':pergunta34', $this->pergunta34, PDO::PARAM_INT);
        $sth->bindParam(':pergunta35', $this->pergunta35, PDO::PARAM_INT);
        $sth->bindParam(':pergunta36', $this->pergunta36, PDO::PARAM_INT);
        $sth->bindParam(':pergunta37', $this->pergunta37, PDO::PARAM_INT);
        
        $sth->bindParam(':comentario1', $this->comentario1, PDO::PARAM_STR);
        $sth->bindParam(':comentario3', $this->comentario3, PDO::PARAM_STR);
        $sth->bindParam(':comentario4', $this->comentario4, PDO::PARAM_STR);
        $sth->bindParam(':comentario5', $this->comentario5, PDO::PARAM_STR);
        $sth->bindParam(':comentario6', $this->comentario6, PDO::PARAM_STR);
        $sth->bindParam(':comentario7', $this->comentario7, PDO::PARAM_STR);
        $sth->bindParam(':comentario8', $this->comentario8, PDO::PARAM_STR);
        $sth->bindParam(':comentario9', $this->comentario9, PDO::PARAM_STR);
        $sth->bindParam(':comentario10', $this->comentario10, PDO::PARAM_STR);
        $sth->bindParam(':comentario11', $this->comentario11, PDO::PARAM_STR);
        $sth->bindParam(':comentario12', $this->comentario12, PDO::PARAM_STR);
        $sth->bindParam(':comentario14', $this->comentario14, PDO::PARAM_STR);
        $sth->bindParam(':comentario15', $this->comentario15, PDO::PARAM_STR);
        $sth->bindParam(':comentario16', $this->comentario16, PDO::PARAM_STR);
        $sth->bindParam(':comentario17', $this->comentario17, PDO::PARAM_STR);
        $sth->bindParam(':comentario18', $this->comentario18, PDO::PARAM_STR);
        $sth->bindParam(':comentario19', $this->comentario19, PDO::PARAM_STR);
        $sth->bindParam(':comentario20', $this->comentario20, PDO::PARAM_STR);
        $sth->bindParam(':comentario21', $this->comentario21, PDO::PARAM_STR);
        $sth->bindParam(':comentario22', $this->comentario22, PDO::PARAM_STR);
        $sth->bindParam(':comentario23', $this->comentario23, PDO::PARAM_STR);
        $sth->bindParam(':comentario24', $this->comentario24, PDO::PARAM_STR);
        $sth->bindParam(':comentario25', $this->comentario25, PDO::PARAM_STR);
        $sth->bindParam(':comentario26', $this->comentario26, PDO::PARAM_STR);
        $sth->bindParam(':comentario27', $this->comentario27, PDO::PARAM_STR);
        $sth->bindParam(':comentario28', $this->comentario28, PDO::PARAM_STR);
        $sth->bindParam(':comentario29', $this->comentario29, PDO::PARAM_STR);
        $sth->bindParam(':comentario30', $this->comentario30, PDO::PARAM_STR);
        $sth->bindParam(':comentario31', $this->comentario31, PDO::PARAM_STR);
        $sth->bindParam(':comentario32', $this->comentario32, PDO::PARAM_STR);
        $sth->bindParam(':comentario33', $this->comentario33, PDO::PARAM_STR);
        $sth->bindParam(':comentario34', $this->comentario34, PDO::PARAM_STR);
        $sth->bindParam(':comentario35', $this->comentario35, PDO::PARAM_STR);
        $sth->bindParam(':comentario36', $this->comentario36, PDO::PARAM_STR);
        $sth->bindParam(':comentario37', $this->comentario37, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            $ultimo_id = 0;
            $resultado2 = $this->selecionaUltimoId();
            if ($resultado2) {
                foreach ($resultado2 as $vetor) {
                    $ultimo_id = $vetor['lastid'];
                }
            }
            return $ultimo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idRelatorioGrandeConselheiro) as lastid FROM relatorioGrandeConselheiro";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraRelatorioGrandeConselheiro($idRelatorioGrandeConselheiro) {

        $sql = "UPDATE relatorioGrandeConselheiro SET
        			semestre = :semestre,	  
                                  ano =  :ano,
				  				  pergunta1 = :pergunta1,
				  				  pergunta3 = :pergunta3,
				  				  pergunta4 = :pergunta4,
				  				  pergunta5 = :pergunta5,
				  				  pergunta6 = :pergunta6,
				  				  pergunta7 = :pergunta7,
				  				  pergunta8 = :pergunta8,
				  				  pergunta9 = :pergunta9,
				  				  pergunta10 = :pergunta10,
				  				  pergunta11 = :pergunta11,
				  				  pergunta12 = :pergunta12,
				  				  pergunta14 = :pergunta14,
				  				  pergunta15 = :pergunta15,
				  				  pergunta16 = :pergunta16,
				  				  pergunta17 = :pergunta17,
				  				  pergunta18 = :pergunta18,
				  				  pergunta19 = :pergunta19,
				  				  pergunta20 = :pergunta20,
				  				  pergunta21 = :pergunta21,
				  				  pergunta22 = :pergunta22,
				  				  pergunta23 = :pergunta23,
				  				  pergunta24 = :pergunta24,
				  				  pergunta25 = :pergunta25,
				  				  pergunta26 = :pergunta26,
				  				  pergunta27 = :pergunta27,
				  				  pergunta28 = :pergunta28,
				  				  pergunta29 = :pergunta29,
				  				  pergunta30 = :pergunta30,
				  				  pergunta31 = :pergunta31,
				  				  pergunta32 = :pergunta32,
				  				  pergunta33 = :pergunta33,
				  				  pergunta34 = :pergunta34,
				  				  pergunta35 = :pergunta35,
				  				  pergunta36 = :pergunta36,
				  				  pergunta37 = :pergunta37,
				  				  comentario1 = :comentario1,
				  				  comentario3 = :comentario3,
				  				  comentario4 = :comentario4,
				  				  comentario5 = :comentario5,
				  				  comentario6 = :comentario6,
				  				  comentario7 = :comentario7,
				  				  comentario8 = :comentario8,
				  				  comentario9 = :comentario9,
				  				  comentario10 =:comentario10,
				  				  comentario11 =:comentario11,
				  				  comentario12 = :comentario12,
				  				  comentario14 = :comentario14,
				  				  comentario15 = :comentario15,
				  				  comentario16 = :comentario16,
				  				  comentario17 = :comentario17,
				  				  comentario18 = :comentario18,
				  				  comentario19 = :comentario19,
				  				  comentario20 = :comentario20,
				  				  comentario21 = :comentario21,
				  				  comentario22 = :comentario22,
				  				  comentario23 = :comentario23,
				  				  comentario24 = :comentario24,
				  				  comentario25 = :comentario25,
				  				  comentario26 = :comentario26,
				  				  comentario27 = :comentario27,
				  				  comentario28 = :comentario28,
				  				  comentario29 = :comentario29,
				  				  comentario30 = :comentario30,
				  				  comentario31 = :comentario31,
				  				  comentario32 = :comentario32,
				  				  comentario33 = :comentario33,
				  				  comentario34 = :comentario34,
				  				  comentario35 = :comentario35,
				  				  comentario36 = :comentario36,
				  				  comentario37 = :comentario37
                                  WHERE idRelatorioGrandeConselheiro = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getSemestre        = $this->getSemestre();
        $getAno             = $this->getAno();

        $sth->bindParam(':semestre', $getSemestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $getAno, PDO::PARAM_INT);
        
        $sth->bindParam(':id', $idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        
        $sth->bindParam(':pergunta1', $this->pergunta1, PDO::PARAM_INT);
        $sth->bindParam(':pergunta3', $this->pergunta3, PDO::PARAM_INT);
        $sth->bindParam(':pergunta4', $this->pergunta4, PDO::PARAM_INT);
        $sth->bindParam(':pergunta5', $this->pergunta5, PDO::PARAM_INT);
        $sth->bindParam(':pergunta6', $this->pergunta6, PDO::PARAM_INT);
        $sth->bindParam(':pergunta7', $this->pergunta7, PDO::PARAM_INT);
        $sth->bindParam(':pergunta8', $this->pergunta8, PDO::PARAM_INT);
        $sth->bindParam(':pergunta9', $this->pergunta9, PDO::PARAM_INT);
        $sth->bindParam(':pergunta10', $this->pergunta10, PDO::PARAM_INT);
        $sth->bindParam(':pergunta11', $this->pergunta11, PDO::PARAM_INT);
        $sth->bindParam(':pergunta12', $this->pergunta12, PDO::PARAM_INT);
        $sth->bindParam(':pergunta14', $this->pergunta14, PDO::PARAM_INT);
        $sth->bindParam(':pergunta15', $this->pergunta15, PDO::PARAM_INT);
        $sth->bindParam(':pergunta16', $this->pergunta16, PDO::PARAM_INT);
        $sth->bindParam(':pergunta17', $this->pergunta17, PDO::PARAM_INT);
        $sth->bindParam(':pergunta18', $this->pergunta18, PDO::PARAM_INT);
        $sth->bindParam(':pergunta19', $this->pergunta19, PDO::PARAM_INT);
        $sth->bindParam(':pergunta20', $this->pergunta20, PDO::PARAM_INT);
        $sth->bindParam(':pergunta21', $this->pergunta21, PDO::PARAM_INT);
        $sth->bindParam(':pergunta22', $this->pergunta22, PDO::PARAM_INT);
        $sth->bindParam(':pergunta23', $this->pergunta23, PDO::PARAM_INT);
        $sth->bindParam(':pergunta24', $this->pergunta24, PDO::PARAM_INT);
        $sth->bindParam(':pergunta25', $this->pergunta25, PDO::PARAM_INT);
        $sth->bindParam(':pergunta26', $this->pergunta26, PDO::PARAM_INT);
        $sth->bindParam(':pergunta27', $this->pergunta27, PDO::PARAM_INT);
        $sth->bindParam(':pergunta28', $this->pergunta28, PDO::PARAM_INT);
        $sth->bindParam(':pergunta29', $this->pergunta29, PDO::PARAM_INT);
        $sth->bindParam(':pergunta30', $this->pergunta30, PDO::PARAM_INT);
        $sth->bindParam(':pergunta31', $this->pergunta31, PDO::PARAM_INT);
        $sth->bindParam(':pergunta32', $this->pergunta32, PDO::PARAM_INT);
        $sth->bindParam(':pergunta33', $this->pergunta33, PDO::PARAM_INT);
        $sth->bindParam(':pergunta34', $this->pergunta34, PDO::PARAM_INT);
        $sth->bindParam(':pergunta35', $this->pergunta35, PDO::PARAM_INT);
        $sth->bindParam(':pergunta36', $this->pergunta36, PDO::PARAM_INT);
        $sth->bindParam(':pergunta37', $this->pergunta37, PDO::PARAM_INT);
        
        $sth->bindParam(':comentario1', $this->comentario1, PDO::PARAM_STR);
        $sth->bindParam(':comentario3', $this->comentario3, PDO::PARAM_STR);
        $sth->bindParam(':comentario4', $this->comentario4, PDO::PARAM_STR);
        $sth->bindParam(':comentario5', $this->comentario5, PDO::PARAM_STR);
        $sth->bindParam(':comentario6', $this->comentario6, PDO::PARAM_STR);
        $sth->bindParam(':comentario7', $this->comentario7, PDO::PARAM_STR);
        $sth->bindParam(':comentario8', $this->comentario8, PDO::PARAM_STR);
        $sth->bindParam(':comentario9', $this->comentario9, PDO::PARAM_STR);
        $sth->bindParam(':comentario10', $this->comentario10, PDO::PARAM_STR);
        $sth->bindParam(':comentario11', $this->comentario11, PDO::PARAM_STR);
        $sth->bindParam(':comentario12', $this->comentario12, PDO::PARAM_STR);
        $sth->bindParam(':comentario14', $this->comentario14, PDO::PARAM_STR);
        $sth->bindParam(':comentario15', $this->comentario15, PDO::PARAM_STR);
        $sth->bindParam(':comentario16', $this->comentario16, PDO::PARAM_STR);
        $sth->bindParam(':comentario17', $this->comentario17, PDO::PARAM_STR);
        $sth->bindParam(':comentario18', $this->comentario18, PDO::PARAM_STR);
        $sth->bindParam(':comentario19', $this->comentario19, PDO::PARAM_STR);
        $sth->bindParam(':comentario20', $this->comentario20, PDO::PARAM_STR);
        $sth->bindParam(':comentario21', $this->comentario21, PDO::PARAM_STR);
        $sth->bindParam(':comentario22', $this->comentario22, PDO::PARAM_STR);
        $sth->bindParam(':comentario23', $this->comentario23, PDO::PARAM_STR);
        $sth->bindParam(':comentario24', $this->comentario24, PDO::PARAM_STR);
        $sth->bindParam(':comentario25', $this->comentario25, PDO::PARAM_STR);
        $sth->bindParam(':comentario26', $this->comentario26, PDO::PARAM_STR);
        $sth->bindParam(':comentario27', $this->comentario27, PDO::PARAM_STR);
        $sth->bindParam(':comentario28', $this->comentario28, PDO::PARAM_STR);
        $sth->bindParam(':comentario29', $this->comentario29, PDO::PARAM_STR);
        $sth->bindParam(':comentario30', $this->comentario30, PDO::PARAM_STR);
        $sth->bindParam(':comentario31', $this->comentario31, PDO::PARAM_STR);
        $sth->bindParam(':comentario32', $this->comentario32, PDO::PARAM_STR);
        $sth->bindParam(':comentario33', $this->comentario33, PDO::PARAM_STR);
        $sth->bindParam(':comentario34', $this->comentario34, PDO::PARAM_STR);
        $sth->bindParam(':comentario35', $this->comentario35, PDO::PARAM_STR);
        $sth->bindParam(':comentario36', $this->comentario36, PDO::PARAM_STR);
        $sth->bindParam(':comentario37', $this->comentario37, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        };
    }

    # Seleciona 

    public function listaRelatorioGrandeConselheiro($regiao = null) {
        
        $sql = "SELECT * FROM relatorioGrandeConselheiro as rgc 
		inner join regiaoRosacruz as rr on rgc.regiao = rr.idRegiaoRosacruz
		where 1=1 ";
        if ($regiao != null) {
            $sql .= " and regiao=:regiao";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($regiao != null) {
            $sth->bindParam(':regiao', $regiao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaExiste() {

        $sql = "SELECT * FROM relatorioGrandeConselheiro where 1=1 
                and regiao =  :regiao
				and semestre = :semestre
				and ano = :ano
                ";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getRegiao                                  = $this->getRegiao();
        $getSemestre                                = $this->getSemestre();
        $getAno                                     = $this->getAno();

        $sth->bindParam(':regiao', $getRegiao, PDO::PARAM_INT);
        $sth->bindParam(':semestre', $getSemestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $getAno, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdRelatorioGrandeConselheiro($idRelatorioGrandeConselheiro) {
        
        $sql = "SELECT * FROM  relatorioGrandeConselheiro
    				inner join usuario as u on criadoPor = seqCadast
    				inner join regiaoRosacruz as rr on regiao = idRegiaoRosacruz
                                WHERE   idRelatorioGrandeConselheiro = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function excluirPlanoAcaoRegiao($idRelatorioGrandeConselheiro) {
        
        $sql = "DELETE FROM  relatorioGrandeConselheiro 
                                WHERE   idRelatorioGrandeConselheiro = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaAssinatura($codigo){

        $sql = "SELECT * FROM  relatorioGrandeConselheiro as a
                WHERE   numeroAssinatura = :codigo";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinaturaIndividual($codigo,$codigoIndividual){

        $sql = "SELECT * FROM  relatorioGrandeConselheiro as a
                inner join relatorioGrandeConselheiro_assinatura_eletronica on fk_idRelatorioGrandeConselheiro = idRelatorioGrandeConselheiro
                WHERE   numeroAssinatura = :codigo and codigoAssinaturaIndividual = :codigoIndividual";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);
        $sth->bindParam(':codigoIndividual', $codigoIndividual , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function entregar($id,$quemEntregou)
    {
        $sql = "UPDATE relatorioGrandeConselheiro SET
        						  entregue =  1, dataEntrega=now(), quemEntregou = :quemEntregou
                                  WHERE idRelatorioGrandeConselheiro = :id";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregou', $quemEntregou, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscarRelatorioGrandeConselheiroQuePrecisaAssinatura($idRegiao)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  relatorioGrandeConselheiro as a
                WHERE   entregue = 1 and regiao = :regiao";

        $sql .= " order by ano,semestre";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':regiao', $idRegiao, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function assinaturaEletronicaRelatorioGrandeConselheiro($seqCadast,$ip,$idRegiao,$funcao,$idRelatorio,$codigoAssinaturaIndividual){

        //echo "idAta=>".$idAta;
        $sql = "INSERT INTO relatorioGrandeConselheiro_assinatura_eletronica
                                 (fk_idRelatorioGrandeConselheiro,
                                  codigoAssinaturaIndividual,
                                  seqCadast,
                                  fk_idFuncao,
                                  ip,
                                  regiao,
                                  data
                                  )
                            VALUES (:fk_idRelatorioGrandeConselheiro,
                                    :codigoAssinaturaIndividual,
                                    :seqCadast,
                                    :fk_idFuncao,
                                    :ip,
                                    :regiao,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth->bindParam(':regiao', $idRegiao, PDO::PARAM_INT);
        $sth->bindParam(':fk_idFuncao', $funcao, PDO::PARAM_INT);
        $sth->bindParam(':fk_idRelatorioGrandeConselheiro', $idRelatorio, PDO::PARAM_INT);
        $sth->bindParam(':codigoAssinaturaIndividual', $codigoAssinaturaIndividual, PDO::PARAM_INT);


        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarAssinaturasEmRelatorioGrandeConselheiro($seqCadast=null,$idRegiao=null,$codigoIndividual=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT *,a.data as dataEntrega FROM  relatorioGrandeConselheiro_assinatura_eletronica as a
                left join relatorioGrandeConselheiro as at on at.idRelatorioGrandeConselheiro = a.fk_idRelatorioGrandeConselheiro
                left join usuario as u on u.seqCadast = a.seqCadast
                left join regiaoRosacruz as o on a.regiao = o.idRegiaoRosacruz
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE  1=1 ";

        if($seqCadast!=null)
        {
            $sql .= " and a.seqCadast = :seqCadast";
        }

        if($idRegiao!=null)
        {
            $sql .= " and a.regiao = :regiao group by idRelatorioGrandeConselheiroAssinaturaEletronica";
        }

        if($codigoIndividual!=null)
        {
            $sql .= " and a.codigoAssinaturaIndividual = :codigoIndividual";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null) {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }

        if($idRegiao!=null)
        {
            $sth->bindParam(':regiao', $idRegiao, PDO::PARAM_INT);
        }

        if($codigoIndividual!=null)
        {
            $sth->bindParam(':codigoIndividual', $codigoIndividual, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);

        if($codigoIndividual!=null)
        {
            return $resultado;
        }else{
            if($idRegiao==null) {

                if ($resultado) {
                    $arr = array();
                    foreach ($resultado as $v) {
                        $arr[] = $v['fk_idRelatorioGrandeConselheiro'];
                    }
                    return $arr;
                } else {
                    return false;
                }
            }else{
                return $resultado;
            }
        }

    }

    public function buscarAssinaturasEmRelatorioGrandeConselheiroPorDocumento($id,$idFuncao=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  relatorioGrandeConselheiro_assinatura_eletronica as a
                left join usuario as u on u.seqCadast = a.seqCadast
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE   fk_idRelatorioGrandeConselheiro = :id";
        if($idFuncao!=null)
        {
            $sql .= " and fk_idFuncao = :idFuncao";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        if($idFuncao!=null)
        {
            $sth->bindParam(':idFuncao', $idFuncao, PDO::PARAM_INT);
        }

        if($idFuncao==null)
        {
            //echo "aqui Fio2!";
            $resultado = $c->run($sth);
            if ($resultado){
                return $resultado;
            } else {
                return false;
            }
        }else{
            //echo "aqui Fio1!";
            $resultado = $c->run($sth,null,null,true);
            if ($resultado){

                return true;
            } else {
                return false;
            }
        }

    }

    public function entregarCompletamente($id,$quemEntregouCompletamente)
    {
        $sql = "UPDATE relatorioGrandeConselheiro SET
        						  entregueCompletamente =  1,
        						  quemEntregouCompletamente = :quemEntregouCompletamente,
        						  dataEntregouCompletamente = now()
                                  WHERE idRelatorioGrandeConselheiro = :id";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregouCompletamente', $quemEntregouCompletamente, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function atualizaNumeroAssinatura($numeroAssinatura)
    {
        $sql = "UPDATE relatorioGrandeConselheiro SET
        						  numeroAssinatura =  :numeroAssinatura
                                  WHERE idRelatorioGrandeConselheiro = :id";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':numeroAssinatura', $numeroAssinatura, PDO::PARAM_STR);
        $sth->bindParam(':id', $this->idRelatorioGrandeConselheiro, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>