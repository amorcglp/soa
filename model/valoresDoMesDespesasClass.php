<?php

require_once ("conexaoClass.php");

class valoresDoMesDespesas{

    private $mes;
    private $ano;
    private $fk_idOrganismoAfiliado;
    private $valorDespesa;

    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @param mixed $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }

    /**
     * @param mixed $fk_idOrganismoAfiliado
     */
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getValorDespesa()
    {
        return $this->valorDespesa;
    }

    /**
     * @param mixed $valorDespesa
     */
    public function setValorDespesa($valorDespesa)
    {
        $this->valorDespesa = $valorDespesa;
    }






    public function cadastro(){

    	$sql = "INSERT INTO valoresDoMesDespesas  
                                 (mes,
                                  ano,
                                  fk_idOrganismoAfiliado,
                                  valorDespesa,
                                  dataCadastro
                                  )
                            VALUES (
                                    :mes,
                                    :ano,
                            		:idOrganismoAfiliado,
                            		:valorDespesa,
                                    now()
                                    )";
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':valorDespesa', $this->valorDespesa, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function lista($mes=null,$ano=null,$idOrganismoAfiliado=null,$ultimoRegistro=null){

        $sql = "SELECT 
                      CAST(valorDespesa  AS DECIMAL(12,2)) as valorDespesa
                      FROM valoresDoMesDespesas
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";

        if($mes!=null)
        {
            $sql .= " and mes=:mes";
        }

        if($ano!=null)
        {
            $sql .= " and ano=:ano";
        }

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }

        if($ultimoRegistro!=null)
        {
            $sql .= " order by ano,mes DESC LIMIT 0,1";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mes!=null)
        {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }

        if($ano!=null)
        {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function atualizaValoresDoMes($mes,$ano,$idOrganismoAfiliado,$totalSaidas){

        $sql="UPDATE valoresDoMesDespesas SET  
                                  valorDespesa = :totalSaidasDoMes,
                                  dataCadastro = now()    
                                  WHERE mes = :mes and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado
                            ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        //echo "total Saidas=>".$totalSaidas;
        $sth->bindParam(':totalSaidasDoMes', $totalSaidas, PDO::PARAM_STR);
        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function limpar($idOrganismoAfiliado=null){

        $sql = "DELETE FROM valoresDoMesDespesas
    	 where fk_idOrganismoAfiliado=:id";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
