<?php

require_once ("conexaoClass.php");

class Mural {

    private $idMural;
    private $mensagemMural;
    private $dtCadastroMural;
    private $dtAtualizacaoMural;
    private $exibicaoMural;
    private $status;
    private $fk_idMuralCategoria;
    private $fk_idRegiaoRosacruz;
    private $fk_idOrganismoAfiliado;
    private $fk_seqCadastAtualizadoPor;

    //--- GETTERS -----------------------------------------------------
    public function getIdMural()
    {
        return $this->idMural;
    }
    public function getMensagemMural()
    {
        return $this->mensagemMural;
    }
    public function getDtCadastroMural()
    {
        return $this->dtCadastroMural;
    }
    public function getDtAtualizacaoMural()
    {
        return $this->dtAtualizacaoMural;
    }
    public function getExibicaoMural()
    {
        return $this->exibicaoMural;
    }
    public function getFkIdRegiaoRosacruz()
    {
        return $this->fk_idRegiaoRosacruz;
    }
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }
    public function getFkSeqCadastAtualizadoPor()
    {
        return $this->fk_seqCadastAtualizadoPor;
    }
    
    //--- SETTERS -----------------------------------------------------
    public function setIdMural($idMural)
    {
        $this->idMural = $idMural;
    }
    public function setMensagemMural($mensagemMural)
    {
        $this->mensagemMural = $mensagemMural;
    }
    public function setDtCadastroMural($dtCadastroMural)
    {
        $this->dtCadastroMural = $dtCadastroMural;
    }
    public function setDtAtualizacaoMural($dtAtualizacaoMural)
    {
        $this->dtAtualizacaoMural = $dtAtualizacaoMural;
    }
    public function setExibicaoMural($exibicaoMural)
    {
        $this->exibicaoMural = $exibicaoMural;
    }
    public function setFkIdRegiaoRosacruz($fk_idRegiaoRosacruz)
    {
        $this->fk_idRegiaoRosacruz = $fk_idRegiaoRosacruz;
    }
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }
    public function setFkSeqCadastAtualizadoPor($fk_seqCadastAtualizadoPor)
    {
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }
    //--------------------------------------------------------------------------

    public function cadastroPublicacaoMural($mensagemMural,$exibicaoMural,$fk_idMuralCategoria,$fk_idRegiaoRosacruz,$fk_idOrganismoAfiliado,$fk_seqCadastAtualizadoPor,$fk_idMuralImagem,$fk_idMuralPublico){

        $sql = "INSERT INTO mural
                                 (mensagemMural,
                                  dtCadastroMural,
                                  exibicaoMural,
                                  status,
                                  fk_idMuralImagem,
                                  fk_idMuralCategoria,
                                  fk_idMuralPublico,
                                  fk_idRegiaoRosacruz,
                                  fk_idOrganismoAfiliado,
                                  fk_seqCadastAtualizadoPor)
                            VALUES (:mensagemMural,
                                    NOW(),
                                    :exibicaoMural,
                                    1,
                                    :fk_idMuralImagem,
                                    :fk_idMuralCategoria,
                                    :fk_idMuralPublico,
                                    :fk_idRegiaoRosacruz,
                                    :fk_idOrganismoAfiliado,
                                    :fk_seqCadastAtualizadoPor)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mensagemMural', $mensagemMural, PDO::PARAM_STR);
        $sth->bindParam(':exibicaoMural', $exibicaoMural, PDO::PARAM_INT);
        $sth->bindParam(':fk_idMuralCategoria', $fk_idMuralCategoria, PDO::PARAM_INT);
        $sth->bindParam(':fk_idMuralPublico', $fk_idMuralPublico, PDO::PARAM_INT);
        $sth->bindParam(':fk_idRegiaoRosacruz', $fk_idRegiaoRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':fk_idMuralImagem', $fk_idMuralImagem, PDO::PARAM_INT);

        $data = array(
            'mensagemMural'=>$mensagemMural,
            'exibicaoMural'=>$exibicaoMural,
            'fk_idMuralImagem'=>$fk_idMuralImagem,
            'fk_idMuralCategoria'=>$fk_idMuralCategoria,
            'fk_idRegiaoRosacruz'=>$fk_idRegiaoRosacruz,
            'fk_idOrganismoAfiliado'=>$fk_idOrganismoAfiliado,
            'fk_seqCadastAtualizadoPor'=>$fk_seqCadastAtualizadoPor
        );
        //echo "QUERY: " . $this->parms("cadastroPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoIdPublicacao(){

        $sql= "SELECT MAX(idMural) as lastid FROM mural";

        $ultimo_id=0;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado)
        {
            foreach ($resultado as $vetor)
            {
                $ultimo_id = $vetor['lastid']; 
            }
        }
        return $ultimo_id;
    }

    public function alteraPublicacaoMural($mensagemMural, $exibicaoMural, $fk_idMuralCategoria,$idMural,$fk_idMuralPublico)
    {
        $sql = "UPDATE mural SET
                                 mensagemMural = :mensagemMural,
                                 dtAtualizacaoMural = NOW(),
                                 exibicaoMural = :exibicaoMural,
                                 fk_idMuralCategoria = :fk_idMuralCategoria,
                                 fk_idMuralPublico = :fk_idMuralPublico
                                 WHERE idMural = :idMural";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mensagemMural', $mensagemMural, PDO::PARAM_STR);
        $sth->bindParam(':exibicaoMural', $exibicaoMural, PDO::PARAM_INT);
        $sth->bindParam(':fk_idMuralCategoria', $fk_idMuralCategoria, PDO::PARAM_INT);
        $sth->bindParam(':fk_idMuralPublico', $fk_idMuralPublico, PDO::PARAM_INT);
        $sth->bindParam(':idMural', $idMural, PDO::PARAM_INT);

        $data = array(
            'mensagemMural'=>$mensagemMural,
            'exibicaoMural'=>$exibicaoMural,
            'fk_idMuralCategoria'=>$fk_idMuralCategoria,
            'idMural'=>$idMural
        );
        //echo "QUERY: " . $this->parms("alteraPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaMural($fk_idRegiaoRosacruz,$fk_idOrganismoAfiliado,$status,$inicio,$maximo,$filtroFavorito,$filtroPropriasPublicacoes,$seqCadast,$filtroCategorias=null,$filtroPublicos=null,$filtroOrganismos=null) {

        //echo "<pre>";print_r($filtroPublicos);echo "</pre>";

        $sql = "SELECT m.*, u.seqCadast,u.nomeUsuario, u.avatarUsuario,
                    (SELECT oa.classificacaoOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as classificacaoOrganismoAfiliado,
                    (SELECT oa.nomeOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as nomeOrganismoAfiliado,
                    (SELECT oa.siglaOrganismoAfiliado FROM organismoAfiliado as oa 
                    WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as siglaOrganismoAfiliado,
                    (SELECT COUNT(*) FROM mural_favorito as mf 
                    where m.idMural=mf.fk_idMural 
                    and fk_seqCadast=:seqCadastUsuarioLogado) as favorito,
                    (SELECT COUNT(*) FROM mural_curtidas as mc 
                    where m.idMural=mc.fk_idMural 
                    and fk_seqCadast=:seqCadastUsuarioLogado) as curtido,
                    (SELECT COUNT(*) FROM mural_curtidas as mcq 
                    where m.idMural=mcq.fk_idMural) as qnt_curtido
                FROM mural as m INNER JOIN usuario as u on u.seqCadast=m.fk_seqCadastAtualizadoPor 
                WHERE 1=1";

        if($filtroFavorito==1){
            $sql .= " and (SELECT COUNT(*) FROM mural_favorito as mf 
                    where m.idMural=mf.fk_idMural 
                    and fk_seqCadast=:seqCadastFiltroFavorito) = 1";
        }

        if($filtroPropriasPublicacoes==1){
            $sql .= " and m.fk_seqCadastAtualizadoPor = :seqCadastFiltroPropriasPublicacoes";
        }

        $sql .= $status!=null ? " and status = :status" : "";

        $sqlCategorias = "";
        if(count($filtroCategorias) > 0 && $filtroCategorias!=null){
            $sqlCategorias = " and (";
            for ($i=0; $i < count($filtroCategorias); $i++) { 
                if($i > 0){ $sqlCategorias .= " OR"; }
                $sqlCategorias .= " fk_idMuralCategoria=:categoria".$i;
            }
            $sqlCategorias .= ")";
        }
        $sql .= $sqlCategorias;

        $sqlPublicos = "";
        if(count($filtroPublicos) > 0 && $filtroPublicos!=null){
            $sqlPublicos = " and (";
            for ($i=0; $i < count($filtroPublicos); $i++) {
                if($i > 0){ $sqlPublicos .= " OR"; }
                $sqlPublicos .= " fk_idMuralPublico=:publico".$i;
            }
            $sqlPublicos .= ")";
        }
        $sql .= $sqlPublicos;

        $sqlOrganismos = "";
        if(count($filtroOrganismos) > 0 && $filtroOrganismos!=null){
            $sqlOrganismos = " and (";
            for ($i=0; $i < count($filtroOrganismos); $i++) { 
                if($i > 0){ $sqlOrganismos .= " OR"; }
                $sqlOrganismos .= " fk_idOrganismoAfiliado=:organismos".$i;
            }
            $sqlOrganismos .= ")";
        }
        $sql .= $sqlOrganismos;

        $sql .= " and ";
        $sql .= $status!=null ? "(" : "";
        $sql .= "m.exibicaoMural = 1";
        $sql .= " OR (m.exibicaoMural=2 AND m.fk_idRegiaoRosacruz = :fk_idRegiaoRosacruz)";
        $sql .= " OR (m.exibicaoMural=3 AND m.fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado)";
        $sql .= $status!=null ? ")" : "";
        $sql .= " order by IF(m.dtAtualizacaoMural <> '0000-00-00 00:00:00',m.dtAtualizacaoMural,m.dtCadastroMural) DESC";
        $sql .= " LIMIT :inicio, :maximo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadastUsuarioLogado', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastUsuarioLogado', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idRegiaoRosacruz', $fk_idRegiaoRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindValue(':inicio', (int) trim($inicio), PDO::PARAM_INT);
        $sth->bindValue(':maximo', (int) trim($maximo), PDO::PARAM_INT);

        if($filtroFavorito==1){
            $sth->bindParam(':seqCadastFiltroFavorito', $seqCadast, PDO::PARAM_INT);
        }
        if($filtroPropriasPublicacoes==1){
            $sth->bindParam(':seqCadastFiltroPropriasPublicacoes', $seqCadast, PDO::PARAM_INT);
        }
        if($status!=null){
            $sth->bindParam(':status', $status, PDO::PARAM_INT);
        }

        if(count($filtroCategorias) > 0 && $filtroCategorias!=null){
            for ($i=0; $i < count($filtroCategorias); $i++) {
                $sth->bindParam(':categoria'.$i, $filtroCategorias[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroPublicos) > 0 && $filtroPublicos!=null){
            for ($i=0; $i < count($filtroPublicos); $i++) {
                $sth->bindParam(':publico'.$i, $filtroPublicos[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroOrganismos) > 0 && $filtroOrganismos!=null){
            for ($i=0; $i < count($filtroOrganismos); $i++) {
                $sth->bindParam(':organismos'.$i, $filtroOrganismos[$i], PDO::PARAM_INT);
            }
        }

        $data = array(
            'seqCadastUsuarioLogado'=>$seqCadast,
            'seqCadastUsuarioLogado'=>$seqCadast,
            'fk_idRegiaoRosacruz'=>$fk_idRegiaoRosacruz,
            'fk_idOrganismoAfiliado'=>$fk_idOrganismoAfiliado,
            'inicio'=>$inicio,
            'maximo'=>$maximo,
            'seqCadastFiltroFavorito'=>$seqCadast,
            'seqCadastFiltroPropriasPublicacoes'=>$seqCadast,
            'status'=>$status,
            'seqCadastFiltroPropriasPublicacoes'=>$seqCadast,
        );
        //echo "QUERY: " . $this->parms("listaMural",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaQuantidadePublicacaoMural($fk_idRegiaoRosacruz,$fk_idOrganismoAfiliado,$status,$filtroFavorito,$filtroPropriasPublicacoes,$seqCadast,$filtroCategorias=null,$filtroOrganismos=null,$filtroPublicos=null) {

        $sql = "SELECT COUNT(idMural) as qnt FROM mural as m INNER JOIN usuario as u on u.seqCadast=m.fk_seqCadastAtualizadoPor WHERE 1=1";

        if($filtroFavorito==1){
            $sql .= " and (SELECT COUNT(*) FROM mural_favorito as mf 
                    where m.idMural=mf.fk_idMural 
                    and mf.fk_seqCadast=:seqCadastFiltroFavorito) = 1";
        }
        if($filtroPropriasPublicacoes==1){
            $sql .= " and m.fk_seqCadastAtualizadoPor = :seqCadastFiltroPropriasPublicacoes";
        }

        $sql .= $status!=null ? " and m.status = :status" : "";

        $sqlCategorias = "";
        if(count($filtroCategorias) > 0 && $filtroCategorias!=null){
            $sqlCategorias = " and (";
            for ($i=0; $i < count($filtroCategorias); $i++) { 
                if($i > 0){ $sqlCategorias .= " OR"; }
                $sqlCategorias .= " fk_idMuralCategoria=:categoria".$i;
            }
            $sqlCategorias .= ")";
        }
        $sql .= $sqlCategorias;

        $sqlPublicos = "";
        if(count($filtroPublicos) > 0 && $filtroPublicos!=null){
            $sqlPublicos = " and (";
            for ($i=0; $i < count($filtroPublicos); $i++) {
                if($i > 0){ $sqlPublicos .= " OR"; }
                $sqlPublicos .= " fk_idMuralPublico=:publico".$i;
            }
            $sqlPublicos .= ")";
        }
        $sql .= $sqlPublicos;

        $sqlOrganismos = "";
        if(count($filtroOrganismos) > 0 && $filtroOrganismos!=null){
            $sqlOrganismos = " and (";
            for ($i=0; $i < count($filtroOrganismos); $i++) { 
                if($i > 0){ $sqlOrganismos .= " OR"; }
                $sqlOrganismos .= " fk_idOrganismoAfiliado=:organismos".$i;
            }
            $sqlOrganismos .= ")";
        }
        $sql .= $sqlOrganismos;

        $sql .= " and ";
        $sql .= $status!=null ? "(" : "";
        $sql .= "m.exibicaoMural = 1";
        $sql .= " OR (m.exibicaoMural=2 AND m.fk_idRegiaoRosacruz = :fk_idRegiaoRosacruz)";
        $sql .= " OR (m.exibicaoMural=3 AND m.fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado)";
        $sql .= $status!=null ? ")" : "";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if(count($filtroCategorias) > 0 && $filtroCategorias!=null){
            for ($i=0; $i < count($filtroCategorias); $i++) {
                $sth->bindParam(':categoria'.$i, $filtroCategorias[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroPublicos) > 0 && $filtroPublicos!=null){
            for ($i=0; $i < count($filtroPublicos); $i++) {
                $sth->bindParam(':publico'.$i, $filtroPublicos[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroOrganismos) > 0 && $filtroOrganismos!=null){
            for ($i=0; $i < count($filtroOrganismos); $i++) {
                $sth->bindParam(':organismos'.$i, $filtroOrganismos[$i], PDO::PARAM_INT);
            }
        }

        $sth->bindParam(':fk_idRegiaoRosacruz', $fk_idRegiaoRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        if($filtroFavorito==1){
            $sth->bindParam(':seqCadastFiltroFavorito', $seqCadast, PDO::PARAM_INT);
        }

        if($filtroPropriasPublicacoes==1){
            $sth->bindParam(':seqCadastFiltroPropriasPublicacoes', $seqCadast, PDO::PARAM_INT);
        }
        if($status!=null){
            $sth->bindParam(':status', $status, PDO::PARAM_INT);
        }

        if($status!=null){
            $data = array(
                'fk_idRegiaoRosacruz'=>$fk_idRegiaoRosacruz,
                'fk_idOrganismoAfiliado'=>$fk_idOrganismoAfiliado,
                'status'=>$status
            );
        } else {
            $data = array(
                'fk_idRegiaoRosacruz'=>$fk_idRegiaoRosacruz,
                'fk_idOrganismoAfiliado'=>$fk_idOrganismoAfiliado
            );
        }
        //echo "QUERY: " . $this->parms("retornaQuantidadePublicacaoMural",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                $qnt = $vetor['qnt']; 
            }
        }
        return $qnt;
    }

    public function jaFavoritado($idPublicacao,$seqCadast) {

        $sql = "SELECT COUNT(*) as qnt FROM mural_favorito WHERE 1=1";
        $sql .= " and fk_idMural = :fk_idMural";
        $sql .= " and fk_seqCadast = :fk_seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("jaFavoritado",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $qnt = $vetor['qnt']; 
            }
        }
        return $qnt;
    }

    public function favoritarPublicacaoMural($idPublicacao,$seqCadast){

        $sql = "INSERT INTO mural_favorito
                                 (fk_idMural,
                                  fk_seqCadast)
                            VALUES (:fk_idMural,
                                    :fk_seqCadast)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("favoritarPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    } 

    public function desfavoritarPublicacaoMural($idPublicacao,$seqCadast){

        $sql = "DELETE FROM mural_favorito 
                            WHERE fk_idMural= :fk_idMural 
                            and fk_seqCadast= :fk_seqCadast;";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("desfavoritarPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    } 

    public function jaCurtido($idPublicacao,$seqCadast) {

        $sql = "SELECT COUNT(*) as qnt FROM mural_curtidas WHERE 1=1";
        $sql .= " and fk_idMural = :fk_idMural";
        $sql .= " and fk_seqCadast = :fk_seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("jaFavoritado",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $qnt = $vetor['qnt']; 
            }
        }
        return $qnt;
    }

    public function curtirPublicacaoMural($idPublicacao,$seqCadast){

        $sql = "INSERT INTO mural_curtidas
                                 (fk_idMural,
                                  fk_seqCadast)
                            VALUES (:fk_idMural,
                                    :fk_seqCadast)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("curtirPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    } 

    public function descurtirPublicacaoMural($idPublicacao,$seqCadast){

        $sql = "DELETE FROM mural_curtidas 
                            WHERE fk_idMural= :fk_idMural 
                            and fk_seqCadast= :fk_seqCadast;";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("descurtirPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    } 

    public function buscarPublicacaoMural($idMural,$seqCadastUsuarioLogado)
    {
        $sql = "SELECT m.*, 
                     u.nomeUsuario, 
                     u.avatarUsuario, 
                     oa.classificacaoOrganismoAfiliado, 
                     oa.nomeOrganismoAfiliado, 
                     oa.siglaOrganismoAfiliado,
                     (SELECT COUNT(*) FROM mural_favorito as mf 
                        where mf.fk_idMural=m.idMural
                        and fk_seqCadast=:seqCadastUsuarioLogado) as favorito
                FROM mural AS m 
                INNER JOIN usuario AS u ON u.seqCadast=m.fk_seqCadastAtualizadoPor 
                INNER JOIN organismoAfiliado AS oa ON oa.idOrganismoAfiliado=m.fk_idOrganismoAfiliado
                WHERE idMural = :idMural";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idMural',  $idMural, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastUsuarioLogado',  $seqCadastUsuarioLogado, PDO::PARAM_INT);

        $data = array(
            'idMural'=>$idMural,
            'seqCadastUsuarioLogado'=>$seqCadastUsuarioLogado
        );
        //echo "QUERY: " . $this->parms("buscarPublicacaoMural",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alterarStatusPublicacaoMural($idMural,$status)
    {
        $sql = "UPDATE mural SET status = :status WHERE idMural = :idMural";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':status', $status , PDO::PARAM_STR);
        $sth->bindParam(':idMural', $idMural , PDO::PARAM_INT);

        $data = array(
            'status'=>$status,
            'idMural'=>$idMural
        );
        //echo "QUERY: " . $this->parms("alterarStatusPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function listaComentarioPelaPublicacao($fk_idPublicacao,$exibirInativos) {

        $sql = "SELECT mc.*, u.nomeUsuario, u.avatarUsuario FROM mural_comentario as mc INNER JOIN usuario as u on u.seqCadast=mc.fk_seqCadastAtualizadoPor WHERE 1=1";
        $sql .= $exibirInativos==false ? " and mc.status = 1" : "";
        $sql .= " and mc.fk_idMural = :fk_idPublicacao";
        $sql .= " order by mc.dtCadastroMuralComentario ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idPublicacao', $fk_idPublicacao, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $data = array(
            'fk_idPublicacao'=>$fk_idPublicacao
        );
        //echo "QUERY: " . $this->parms("listaComentarioPelaPublicacao",$sql,$data) . "\n";
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function cadastroComentarioPublicacaoMural($mensagemMuralComentario,$fk_idMural,$seqCadast){

        $sql = "INSERT INTO mural_comentario
                                 (mensagemMuralComentario,
                                  dtCadastroMuralComentario,
                                  fk_idMural,
                                  fk_seqCadastAtualizadoPor)
                            VALUES (:mensagemMuralComentario,
                                    NOW(),
                                    :fk_idMural,
                                    :seqCadast)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mensagemMuralComentario', $mensagemMuralComentario, PDO::PARAM_STR);
        $sth->bindParam(':fk_idMural', $fk_idMural, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'mensagemMuralComentario'=>$mensagemMuralComentario,
            'fk_idMural'=>$fk_idMural,
            'seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("cadastroComentarioPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoIdPublicacaoImagem(){

        $sql= "SELECT MAX(idMuralImagem) as lastid FROM mural_imagem";

        $ultimo_id=0;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado)
        {
            foreach ($resultado as $vetor)
            {
                $ultimo_id = $vetor['lastid']; 
            }
        }
        return $ultimo_id;
    }

    public function cadastroPublicacaoImagem($nome_original,$nome_arquivo,$ext,$full_path,$status){

        $sql = "INSERT INTO mural_imagem (nome_original,nome_arquivo,ext,full_path,data,status)
                            VALUES (:nomeOriginal,
                                    :nomeArquivo,
                                    :ext,
                                    :fullPath,
                                    NOW(),
                                    :status)";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nomeOriginal', $nome_original, PDO::PARAM_STR);
        $sth->bindParam(':nomeArquivo', $nome_arquivo, PDO::PARAM_STR);
        $sth->bindParam(':ext', $ext, PDO::PARAM_STR);
        $sth->bindParam(':fullPath', $full_path, PDO::PARAM_STR);
        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return $this->selecionaUltimoIdPublicacaoImagem();
        }else{
            return 0;
        }
    }

    public function buscaImagemPeloId($idMuralImagem)
    {
        $sql="SELECT * FROM mural_imagem WHERE idMural = :idMuralImagem";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idMuralImagem',  $idMuralImagem, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaFullPathImagemPeloId($idMuralImagem)
    {
        $sql="SELECT * FROM mural_imagem WHERE idMuralImagem = :idMuralImagem";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idMuralImagem',  $idMuralImagem, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $vetor){
                $full_path = $vetor['full_path'];
            }
            return $full_path;
        }else{
            return "";
        }
    }

    public function alteraImagemPublicacaoMural($idMural,$fk_idMuralImagem)
    {
        $sql = "UPDATE mural SET fk_idMuralImagem = :fk_idMuralImagem WHERE idMural = :idMural";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMuralImagem', $fk_idMuralImagem, PDO::PARAM_INT);
        $sth->bindParam(':idMural', $idMural, PDO::PARAM_INT);

        $data = array(
            'fk_idMuralImagem'=>$fk_idMuralImagem,
            'idMural'=>$idMural
        );
        //echo "QUERY: " . $this->parms("alteraImagemPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

    /*
        Página de Administração
     */
    
    public function listaMuralAdmin($filtroRegiaoRosacruz, 
                                    $filtroOrganismoAfiliado, 
                                    $filtroCategoria,
                                    $filtroPublico,
                                    $filtroFavorito, 
                                    $seqCadast, 
                                    $inicio, 
                                    $maximo) {

        //echo "<pre>";print_r($filtroCategoria);echo "</pre>";

        $sql = "SELECT m.*, u.nomeUsuario, u.avatarUsuario,
                    (SELECT oa.classificacaoOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as classificacaoOrganismoAfiliado,
                    (SELECT oa.nomeOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as nomeOrganismoAfiliado,
                    (SELECT oa.siglaOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as siglaOrganismoAfiliado,
                    (SELECT COUNT(*) FROM mural_curtidas as mcq where m.idMural=mcq.fk_idMural) as qnt_curtido,
                    (SELECT COUNT(*) FROM mural_visualizado as mv where mv.fk_idMural=m.idMural and mv.fk_seqCadast=:seqCadast) as visualizado, 
                    (SELECT COUNT(*) FROM mural_favorito as mf where mf.fk_idMural=m.idMural 
                    and fk_seqCadast=:seqCadast) as favorito
                FROM mural as m INNER JOIN usuario as u on u.seqCadast=m.fk_seqCadastAtualizadoPor WHERE 1=1";

        if($filtroFavorito==1){
            $sql .= " and (SELECT COUNT(*) FROM mural_favorito as mf 
                    where m.idMural=mf.fk_idMural 
                    and fk_seqCadast=:seqCadast) = 1";
        }

        $sqlRegiaoRosacruz = "";
        if(count($filtroRegiaoRosacruz) > 0){
            $sqlRegiaoRosacruz = " and (";
            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) { 
                if($i > 0){ $sqlRegiaoRosacruz .= " OR"; }
                $sqlRegiaoRosacruz .= " m.fk_idRegiaoRosacruz=:regiaoRosacruz".$i;
            }
            $sqlRegiaoRosacruz .= ")";
        }
        $sql .= $sqlRegiaoRosacruz;

        $sqlOrganismos = "";
        if(count($filtroOrganismoAfiliado) > 0){
            $sqlOrganismos = " and (";
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) { 
                if($i > 0){ $sqlOrganismos .= " OR"; }
                $sqlOrganismos .= " m.fk_idOrganismoAfiliado=:organismo".$i;
            }
            $sqlOrganismos .= ")";
        }
        $sql .= $sqlOrganismos;

        $sqlCategoria = "";
        if(count($filtroCategoria) > 0){
            $sqlCategoria = " and (";
            for ($i=0; $i < count($filtroCategoria); $i++) { 
                if($i > 0){ $sqlCategoria .= " OR"; }
                $sqlCategoria .= " m.fk_idMuralCategoria=:categoria".$i;
            }
            $sqlCategoria .= ")";
        }
        $sql .= $sqlCategoria;

        $sqlPublico = "";
        if(count($filtroPublico) > 0){
            $sqlPublico = " and (";
            for ($i=0; $i < count($filtroPublico); $i++) {
                if($i > 0){ $sqlPublico .= " OR"; }
                $sqlPublico .= " m.fk_idMuralPublico=:publico".$i;
            }
            $sqlPublico .= ")";
        }
        $sql .= $sqlPublico;

        $sql .= " order by IF(m.dtAtualizacaoMural <> '0000-00-00 00:00:00', m.dtAtualizacaoMural, m.dtCadastroMural) DESC";
        $sql .= " LIMIT :inicio, :maximo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindValue(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindValue(':seqCadast', $seqCadast, PDO::PARAM_INT);

        $sth->bindValue(':inicio', (int) trim($inicio), PDO::PARAM_INT);
        $sth->bindValue(':maximo', (int) trim($maximo), PDO::PARAM_INT);

        if(count($filtroRegiaoRosacruz) > 0){
            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) {
                $sth->bindParam(':regiaoRosacruz'.$i, $filtroRegiaoRosacruz[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroOrganismoAfiliado) > 0){
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) {
                $sth->bindParam(':organismo'.$i, $filtroOrganismoAfiliado[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroCategoria) > 0){
            for ($i=0; $i < count($filtroCategoria); $i++) {
                $sth->bindParam(':categoria'.$i, $filtroCategoria[$i], PDO::PARAM_INT);
            }
        }

        if(count($filtroPublico) > 0){
            for ($i=0; $i < count($filtroPublico); $i++) {
                $sth->bindParam(':publico'.$i, $filtroPublico[$i], PDO::PARAM_INT);
            }
        }

        $data = array(
            'seqCadast'=>$seqCadast,
            'inicio'=>$inicio,
            'maximo'=>$maximo,
        );
        if(count($filtroRegiaoRosacruz) > 0){
            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) {
                $data[':regiaoRosacruz'.$i] = $filtroRegiaoRosacruz[$i];
            }
        }
        if(count($filtroOrganismoAfiliado) > 0){
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) { 
                $data[':organismo'.$i] = $filtroOrganismoAfiliado[$i];
            }
        }
        if(count($filtroCategoria) > 0){
            for ($i=0; $i < count($filtroCategoria); $i++) { 
                $data[':categoria'.$i] = $filtroCategoria[$i];
            }
        }
        if(count($filtroPublico) > 0){
            for ($i=0; $i < count($filtroPublico); $i++) {
                $data[':publico'.$i] = $filtroPublico[$i];
            }
        }
        //echo "QUERY: " . $this->parms("listaMuralAdmin",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaQuantidadePublicacaoMuralAdmin($filtroRegiaoRosacruz,$filtroOrganismoAfiliado,$filtroCategoria,$filtroFavorito,$filtroPublico) {

        $sql = "SELECT COUNT(idMural) as qnt FROM mural as m INNER JOIN usuario as u on u.seqCadast=m.fk_seqCadastAtualizadoPor WHERE 1=1";

        $sqlRegiaoRosacruz = "";
        if(count($filtroRegiaoRosacruz) > 0){
            $sqlRegiaoRosacruz = " and (";
            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) { 
                if($i > 0){ $sqlRegiaoRosacruz .= " OR"; }
                $sqlRegiaoRosacruz .= " m.fk_idRegiaoRosacruz=:regiaoRosacruz".$i;
            }
            $sqlRegiaoRosacruz .= ")";
        }
        $sql .= $sqlRegiaoRosacruz;

        $sqlOrganismos = "";
        if(count($filtroOrganismoAfiliado) > 0){
            $sqlOrganismos = " and (";
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) { 
                if($i > 0){ $sqlOrganismos .= " OR"; }
                $sqlOrganismos .= " m.fk_idOrganismoAfiliado=:organismo".$i;
            }
            $sqlOrganismos .= ")";
        }
        $sql .= $sqlOrganismos;

        $sqlCategoria = "";
        if(count($filtroCategoria) > 0){
            $sqlCategoria = " and (";
            for ($i=0; $i < count($filtroCategoria); $i++) { 
                if($i > 0){ $sqlCategoria .= " OR"; }
                $sqlCategoria .= " m.fk_idMuralCategoria=:categoria".$i;
            }
            $sqlCategoria .= ")";
        }
        $sql .= $sqlCategoria;

        $sqlPublico = "";
        if(count($filtroPublico) > 0){
            $sqlPublico = " and (";
            for ($i=0; $i < count($filtroPublico); $i++) {
                if($i > 0){ $sqlPublico .= " OR"; }
                $sqlPublico .= " m.fk_idMuralPublico=:publico".$i;
            }
            $sqlPublico .= ")";
        }
        $sql .= $sqlPublico;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if(count($filtroRegiaoRosacruz) > 0){
            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) {
                $sth->bindParam(':regiaoRosacruz'.$i, $filtroRegiaoRosacruz[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroOrganismoAfiliado) > 0){
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) {
                $sth->bindParam(':organismo'.$i, $filtroOrganismoAfiliado[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroCategoria) > 0){
            for ($i=0; $i < count($filtroCategoria); $i++) {
                $sth->bindParam(':categoria'.$i, $filtroCategoria[$i], PDO::PARAM_INT);
            }
        }

        $data = array();
        if(count($filtroRegiaoRosacruz) > 0){
            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) {
                $data[':regiaoRosacruz'.$i] = $filtroRegiaoRosacruz[$i];
            }
        }
        if(count($filtroOrganismoAfiliado) > 0){
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) { 
                $data[':organismo'.$i] = $filtroOrganismoAfiliado[$i];
            }
        }
        if(count($filtroCategoria) > 0){
            for ($i=0; $i < count($filtroCategoria); $i++) { 
                $data[':categoria'.$i] = $filtroCategoria[$i];
            }
        }
        if(count($filtroPublico) > 0){
            for ($i=0; $i < count($filtroPublico); $i++) {
                $data[':publico'.$i] = $filtroPublico[$i];
            }
        }
        //echo "QUERY: " . $this->parms("retornaQuantidadePublicacaoMuralAdmin",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                $qnt = $vetor['qnt']; 
            }
        }
        return $qnt;
    }

    public function alterarStatusComentarioPublicacaoMural($idComentario,$status)
    {
        $sql = "UPDATE mural_comentario SET status = :status WHERE idMuralComentario = :idComentario";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':status', $status , PDO::PARAM_STR);
        $sth->bindParam(':idComentario', $idComentario , PDO::PARAM_INT);

        $data = array(
            'status'=>$status,
            'idMural'=>$idComentario
        );
        //echo "QUERY: " . $this->parms("alterarStatusComentarioPublicacaoMural",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function jaVisualizado($idPublicacao,$seqCadast) {

        $sql = "SELECT COUNT(*) as qnt FROM mural_visualizado WHERE 1=1";
        $sql .= " and fk_idMural = :fk_idMural";
        $sql .= " and fk_seqCadast = :fk_seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("jaVisualizado",$sql,$data) . "\n";

        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $qnt = $vetor['qnt']; 
            }
        }
        return $qnt;
    }

    public function visualizarPublicacao($idPublicacao,$seqCadast){

        $sql = "INSERT INTO mural_visualizado
                                 (fk_idMural,
                                  fk_seqCadast)
                            VALUES (:fk_idMural,
                                    :fk_seqCadast)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idMural', $idPublicacao, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);

        $data = array(
            'fk_idMural'=>$idPublicacao,
            'fk_seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("visualizarPublicacao",$sql,$data) . "\n";

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function totalPublicacoesMural() {

        $sql = "SELECT COUNT(*) as qnt FROM mural WHERE 1=1";
        
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        
        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $qnt = $vetor['qnt']; 
            }
        }
        return $qnt;
    }
    
    public function listaUltimasPublicacoesMural($filtroRegiaoRosacruz=null,$filtroOrganismoAfiliado=null,$filtroCategoria=null,$filtroFavorito=null,$seqCadast=null) {

        $sql = "SELECT m.*, u.nomeUsuario, u.avatarUsuario,
                    (SELECT oa.classificacaoOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as classificacaoOrganismoAfiliado,
                    (SELECT oa.nomeOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as nomeOrganismoAfiliado,
                    (SELECT oa.siglaOrganismoAfiliado FROM organismoAfiliado as oa WHERE m.fk_idOrganismoAfiliado=oa.idOrganismoAfiliado) as siglaOrganismoAfiliado,
                    (SELECT COUNT(*) FROM mural_curtidas as mcq where m.idMural=mcq.fk_idMural) as qnt_curtido,
                    (SELECT COUNT(*) FROM mural_visualizado as mv where mv.fk_idMural=m.idMural and mv.fk_seqCadast=:seqCadast) as visualizado, 
                    (SELECT COUNT(*) FROM mural_favorito as mf where mf.fk_idMural=m.idMural 
                    and fk_seqCadast=:seqCadast) as favorito
                FROM mural as m INNER JOIN usuario as u on u.seqCadast=m.fk_seqCadastAtualizadoPor WHERE 1=1";

        if($filtroFavorito==1){
            $sql .= " and (SELECT COUNT(*) FROM mural_favorito as mf 
                    where m.idMural=mf.fk_idMural 
                    and fk_seqCadast=:seqCadast) = 1";
        }

        /** erro ao dar count em parâmetro nulo */
//        $sqlRegiaoRosacruz = "";
//        if($filtroRegiaoRosacruz != null){
//            $sqlRegiaoRosacruz = " and (";
//            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) {
//                if($i > 0){ $sqlRegiaoRosacruz .= " OR"; }
//                $sqlRegiaoRosacruz .= " m.fk_idRegiaoRosacruz=:regiaoRosacruz".$i;
//            }
//            $sqlRegiaoRosacruz .= ")";
//        }
//        $sql .= $sqlRegiaoRosacruz;

        $sqlOrganismos = "";
        if(count($filtroOrganismoAfiliado)){
            $sqlOrganismos = " and (";
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) { 
                if($i > 0){ $sqlOrganismos .= " OR"; }
                $sqlOrganismos .= " m.fk_idOrganismoAfiliado=:organismo".$i;
            }
            $sqlOrganismos .= ")";
        }
        $sql .= $sqlOrganismos;

        $sqlCategoria = "";
        if(count($filtroCategoria)){
            $sqlCategoria = " and (";
            for ($i=0; $i < count($filtroCategoria); $i++) { 
                if($i > 0){ $sqlCategoria .= " OR"; }
                $sqlCategoria .= " m.fk_idMuralCategoria=:categoria".$i;
            }
            $sqlCategoria .= ")";
        }
        $sql .= $sqlCategoria;
        
        $sql .= " and status=1";
                    
        $sql .=" order by idMural desc limit 0,4";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindValue(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindValue(':seqCadast', $seqCadast, PDO::PARAM_INT);

//        if(count($filtroRegiaoRosacruz)){
//            for ($i=0; $i < count($filtroRegiaoRosacruz); $i++) {
//                $sth->bindParam(':regiaoRosacruz'.$i, $filtroRegiaoRosacruz[$i], PDO::PARAM_INT);
//            }
//        }
        if(count($filtroOrganismoAfiliado)){
            for ($i=0; $i < count($filtroOrganismoAfiliado); $i++) {
                $sth->bindParam(':organismo'.$i, $filtroOrganismoAfiliado[$i], PDO::PARAM_INT);
            }
        }
        if(count($filtroCategoria)){
            for ($i=0; $i < count($filtroCategoria); $i++) {
                $sth->bindParam(':categoria'.$i, $filtroCategoria[$i], PDO::PARAM_INT);
            }
        }

        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }



}

?>
