<?php
require_once ("conexaoClass.php");

class RelatorioFinanceiroMensal {

    public $idRelatorioFinanceiroMensal;
    public $fk_idOrganismoAfiliado;
    public $mes;
    public $ano;
    public $caminhoRelatorioFinanceiroMensal;
    public $usuario;

    /* Funções GET e SET */

    function getIdRelatorioFinanceiroMensal() {
        return $this->idRelatorioFinanceiroMensal;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getMes() {
        return $this->mes;
    }

    function getAno() {
        return $this->ano;
    }
    
	function getCaminhoRelatorioFinanceiroMensal() {
        return $this->caminhoRelatorioFinanceiroMensal;
    }
        
	function getUsuario() {
        return $this->usuario;
    }

    function setIdRelatorioFinanceiroMensal($idRelatorioFinanceiroMensal) {
        $this->idRelatorioFinanceiroMensal = $idRelatorioFinanceiroMensal;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }

	function setMes($mes) {
        $this->mes = $mes;
    }
    
	function setAno($ano) {
        $this->ano = $ano;
    }

    function setCaminhoRelatorioFinanceiroMensal($caminhoRelatorioFinanceiroMensal) {
        $this->caminhoRelatorioFinanceiroMensal = $caminhoRelatorioFinanceiroMensal;
    }
   
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function cadastraRelatorioFinanceiroMensal() {
        
        $sql = "INSERT INTO relatorioFinanceiroMensal 
                                    (
                                    fk_idOrganismoAfiliado, 
                                    mes, 
                                    ano,
                                    caminhoRelatorioFinanceiroMensal,
                                    usuario,
                                    dataCadastro
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :mes,
                                            :ano,
                                            '',
                                            :usuario,
                                            now()
                                            )";
        $id=1;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        /*
        echo "<br>=================================";
        echo "<br>".$this->fk_idOrganismoAfiliado;
        echo "<br>".$this->mes;
        echo "<br>".$this->ano;
        echo "<br>".$this->caminhoRelatorioFinanceiroMensal;
        echo "<br>".$this->usuario;
        echo "<br>=================================";
        echo "<pre>";print_r($this);
        exit();
        */
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_INT);
        //$sth->bindParam(':caminho', $this->caminhoRelatorioFinanceiroMensal, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $resultado = $this->selecionaUltimoIdInserido();
            if($resultado)
            {
                    foreach ($resultado as $vetor)
                    {
                            $id = $vetor['idRelatorioFinanceiroMensal'];
                    }
            }
        }
        return $id;
    }

    public function alteraRelatorioFinanceiroMensal($id) {

        $sql = "UPDATE relatorioFinanceiroMensal SET 
        			 `mes` = :mes,
                                 `ano` = :ano,
                                 `caminhoRelatorioFinanceiroMensal` = :caminho
                            WHERE `idRelatorioFinanceiroMensal` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_STR);
        $sth->bindParam(':caminho', $this->caminhoRelatorioFinanceiroMensal, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function atualizaCaminhoRelatorioFinanceiroMensalAssinado($id) {

        $sql = "UPDATE relatorioFinanceiroMensal SET 
                            `caminhoRelatorioFinanceiroMensal` = :caminho
                            WHERE `idRelatorioFinanceiroMensal` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':caminho', $this->caminhoRelatorioFinanceiroMensal, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioFinanceiroMensal'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM relatorioFinanceiroMensal ORDER BY idRelatorioFinanceiroMensal DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRelatorioFinanceiroMensal($mes=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioFinanceiroMensal as rfm
    		inner join organismoAfiliado as oa on rfm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on rfm.usuario = u.seqCadast
    	where 1=1 ";
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_STR);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($mes=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioFinanceiroMensal where 1=1 ";
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_STR);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function retornaIdRelatorioFinanceiroMensal() {
    	
    	$sql = "SELECT * FROM relatorioFinanceiroMensal 
                                 WHERE `mes` = :mes
                                 and `ano` = :ano
                                 and `fk_idOrganismoAfiliado` =  :idOrganismoAfiliado
                                 ";
    	//echo $sql;
   	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
		
        $id=0;
        if ($resultado) {
        	foreach ($resultado as $vetor)
        	{
        		$id = $vetor['idRelatorioFinanceiroMensal'];
        	}
        }
        return $id;
    }

    public function buscarIdRelatorioFinanceiroMensal($idRelatorioFinanceiroMensal) {
        
        $sql = "SELECT * FROM relatorioFinanceiroMensal 
                                 WHERE `idRelatorioFinanceiroMensal` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioFinanceiroMensal, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioFinanceiroMensal($id){
        
        $sql="DELETE FROM relatorioFinanceiroMensal WHERE idRelatorioFinanceiroMensal = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}
?>