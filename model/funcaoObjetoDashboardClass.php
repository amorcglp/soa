<?php
require_once ("conexaoClass.php");

class FuncaoObjetoDashboard {

    public $fk_idFuncao;
    public $fk_idObjetoDashboard;
    
    /* Funções GET e SET */

    function getFkIdFuncao() {
        return $this->fk_idFuncao;
    }

    function getFkIdObjetoDashboard() {
        return $this->fk_idObjetoDashboard;
    }

    function setFkIdFuncao($fk_idFuncao) {
        $this->fk_idFuncao = $fk_idFuncao;
    }

    function setFkIdObjetoDashboard($fk_idObjetoDashboard) {
        $this->fk_idObjetoDashboard = $fk_idObjetoDashboard;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO funcao_objetoDashboard (`fk_idFuncao`, `fk_idObjetoDashboard`)
                                    VALUES (:idFuncao,
                                            :idObjetoDashboard)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':idObjetoDashboard', $this->fk_idObjetoDashboard, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM funcao_objetoDashboard 
                    WHERE fk_idFuncao = :idFuncao
                    and fk_idObjetoDashboard = :idObjetoDashboard";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':idObjetoDashboard', $this->fk_idObjetoDashboard, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaPorFuncao($funcoesUsuarioString=null) {
    	
    	$sql = "SELECT * FROM funcao_objetoDashboard
                WHERE 1=1 ";
    	if($funcoesUsuarioString==null)
    	{
    		$sql .= " and `fk_idFuncao` = :idFuncao ";
    	}else{
            $ids = explode(",", $funcoesUsuarioString);
            $inQuery = implode(",", array_fill(0, count($ids), "?"));
            $sql .= " and fk_idFuncao IN (".$inQuery.")"; 
    	}
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($funcoesUsuarioString!=null)
    	{
            foreach ($ids as $k => $id)
            {    
                $sth->bindValue(($k+1), $id);
            }
        }else{
            $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}
?>​