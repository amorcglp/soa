<?php

require_once ("conexaoClass.php");

class mensalidadeOA {

    private $idMensalidadeOa;
    private $fk_idOrganismoAfiliado;
    private $dataPagamento;
    private $seqCadastMembroOa;
    private $mes;
    private $ano;
    private $semestre;
    private $usuario;
    private $ultimoAtualizar;

    //METODO SET/GET -----------------------------------------------------------

    function getIdMensalidadeOa() {
        return $this->idMensalidadeOa;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getDataPagamento() {
        return $this->dataPagamento;
    }

    function getSeqCadastMembroOa() {
        return $this->seqCadastMembroOa;
    }

    function getMes() {
        return $this->mes;
    }

    function getAno() {
        return $this->ano;
    }

    function getSemestre() {
        return $this->semestre;
    }

    function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdMensalidadeOa($idMensalidadeOa) {
        $this->idMensalidadeOa = $idMensalidadeOa;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setDataPagamento($dataPagamento) {
        $this->dataPagamento = $dataPagamento;
    }

    function setSeqCadastMembroOa($seqCadastMembroOa) {
        $this->seqCadastMembroOa = $seqCadastMembroOa;
    }

    function setMes($mes) {
        $this->mes = $mes;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setSemestre($semestre) {
        $this->semestre = $semestre;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    //--------------------------------------------------------------------------

    public function cadastroMensalidadeOa() {

        $sql = "INSERT INTO mensalidadeOA  
                                 (fk_idOrganismoAfiliado,
                                  seqCadastMembroOa,
                                  dataPagamento,
                                  mes,
                                  ano,
                                  semestre,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                            		:id,
                            		:seqCadast,
                            		:dataPagamento,
                            		:mes,
                            		:ano,
                            		:semestre,
                            		:usuario,
                                    now()
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);
        $sth->bindParam(':dataPagamento', $this->dataPagamento, PDO::PARAM_STR);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':semestre', $this->semestre, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);

        //echo "[cadastroMensalidadeOa: " . $sql."]<br>";

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaMensalidadeOa() {

        $sql = "UPDATE mensalidadeOA SET  dataPagamento = :dataPagamento,
                                ultimoAtualizar = :ultimoAtualizar
                                WHERE
                                  fk_idOrganismoAfiliado = :id
                                  AND seqCadastMembroOa = :seqCadast
                                  AND mes = :mes
                                  AND ano = :ano
                                  AND semestre = :semestre     
                            ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        //echo "[dataPagamento: " . $this->getDataPagamento()."]<br>";
        //echo "[ultimoAtualizar: " . $this->getUsuario()."]<br>";
        //echo "[id: " . $this->getFk_idOrganismoAfiliado()."]<br>";
        //echo "[seqCadast: " . $this->getSeqCadastMembroOa()."]<br>";
        //echo "[mes: " . $this->getMes()."]<br>";
        //echo "[ano: " . $this->getAno()."]<br>";
        //echo "[semestre: " . $this->getSemestre()."]<br>";

        $sth->bindParam(':dataPagamento', $this->dataPagamento, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $this->usuario, PDO::PARAM_INT);
        
        $sth->bindParam(':id', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':semestre', $this->semestre, PDO::PARAM_INT);

        //echo "[atualizaMensalidadeOa: " . $sql."]<br>";

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idMensalidadeOa) as lastid FROM mensalidadeOA";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaMensalidadeOA($seqCadastMembroOa = null, $mes = null, $semestre = null, $ano = null, $idOrganismoAfiliado = null) {

        $sql = "SELECT * FROM mensalidadeOA
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if ($seqCadastMembroOa != null) 
        {
            
            $sql .= " and seqCadastMembroOa=:seqCadast";
        }
        if ($mes != null) {
            $sql .= " and mes=:mes";
        }
        if ($semestre != null) {
            $sql .= " and semestre=:semestre";
        }
        if ($ano != null) {
            $sql .= " and ano=:ano";
        }
        if ($idOrganismoAfiliado != null) {
            $sql .= " and fk_idOrganismoAfiliado=:idTermoVoluntariado";
        }

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($seqCadastMembroOa != null) 
        {
            $sth->bindParam(':seqCadast', $seqCadastMembroOa, PDO::PARAM_INT);
        }
        if ($mes != null) {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if ($semestre != null) {
            $sth->bindParam(':semestre', $semestre, PDO::PARAM_INT);
        }
        if ($ano != null) {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if ($idOrganismoAfiliado != null) {
            $sth->bindParam(':idTermoVoluntariado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        //echo "[listaMensalidadeOA: " . $sql."]<br>";
        $data = array(
            'seqCadast'=>$seqCadastMembroOa,
            'mes'=>$mes,
            'semestre'=>$semestre,
            'ano'=>$ano,
            'idTermoVoluntariado'=>$idOrganismoAfiliado,
        );

        //echo $this->parms("listaMensalidadeOA",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaMensalidadeOA($seqCadastMembroOa = null, $mes = null, $ano = null, $idOrganismoAfiliado = null) {

        $sql = "SELECT * FROM mensalidadeOA
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if ($seqCadastMembroOa != null) {
            $sql .= " and seqCadastMembroOa=:seqCadast";
        }
        if ($mes != null) {
            $sql .= " and mes=:mes";
        }
        if ($ano != null) {
            $sql .= " and ano=:ano";
        }
        if ($idOrganismoAfiliado != null) {
            $sql .= " and fk_idOrganismoAfiliado=:id";
        }
        $sql .= " and dataPagamento <> '0000-00-00'";
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($seqCadastMembroOa != null) {
            $sth->bindParam(':seqCadast', $seqCadastMembroOa, PDO::PARAM_INT);
        }
        if ($mes != null) {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if ($ano != null) {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if ($idOrganismoAfiliado != null) {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function verificaAtivoAte($seqCadastMembroOa = null, $idOrganismoAfiliado = null) {

        $sql = "SELECT * FROM mensalidadeOA
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if ($seqCadastMembroOa != null) {
            $sql .= " and seqCadastMembroOa=:seqCadast";
        }
        if ($idOrganismoAfiliado != null) {
            $sql .= " and fk_idOrganismoAfiliado=:id";
        }
        $sql .= " and dataPagamento <> '0000-00-00'
    	order by dataPagamento desc
    	limit 0,1
    	";
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($seqCadastMembroOa != null) {
            $sth->bindParam(':seqCadast', $seqCadastMembroOa, PDO::PARAM_INT);
        }
        if ($idOrganismoAfiliado != null) {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function verificaSeJaCadastrado($seqCadastMembroOa, $mes, $semestre, $ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM mensalidadeOA
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	and seqCadastMembroOa=:seqCadast
    	and mes=:mes
    	and semestre=:semestre
        and ano=:ano
    	and fk_idOrganismoAfiliado=:id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadastMembroOa, PDO::PARAM_INT);
        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':semestre', $semestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);

        $data = array(
            'seqCadast'=>$seqCadastMembroOa,
            'mes'=>$mes,
            'semestre'=>$semestre,
            'ano'=>$ano,
            'id'=>$idOrganismoAfiliado,
        );

        //echo $this->parms("verificaSeJaCadastrado",$sql,$data);

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}

?>
