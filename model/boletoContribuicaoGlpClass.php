<?php

require_once ("conexaoClass.php");

class boletoContribuicaoGlp {

    private $idBoletoContribuicaoGlp;
    private $token;
    private $codigoAfiliacao;
    private $tipoMembro;
    private $seqCadast;
    private $codigoVeiculo;
    private $codigoContaContribuicao;
    private $valorBoleto;
    private $quantidade;
    private $nome;
    private $endereco;
    private $numero;
    private $cidade;
    private $estado;
    private $cep;
    private $numeroBoleto;
    private $usuario;

    //METODO SET/GET -----------------------------------------------------------

    function getIdBoletoContribuicaoGlp() {
        return $this->idBoletoContribuicaoGlp;
    }

    function getToken() {
        return $this->token;
    }

    function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }

    function getTipoMembro() {
        return $this->tipoMembro;
    }

    function getSeqCadast() {
        return $this->seqCadast;
    }

    function getCodigoVeiculo() {
        return $this->codigoVeiculo;
    }

    function getCodigoContaContribuicao() {
        return $this->codigoContaContribuicao;
    }

    function getValorBoleto() {
        return $this->valorBoleto;
    }
    
    function getQuantidade() {
        return $this->quantidade;
    }

    function getNome() {
        return $this->nome;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getNumero() {
        return $this->numero;
    }

    function getCidade() {
        return $this->cidade;
    }

    function getEstado() {
        return $this->estado;
    }

    function getCep() {
        return $this->cep;
    }

    function getNumeroBoleto() {
        return $this->numeroBoleto;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setIdBoletoContribuicaoGlp($idBoletoContribuicaoGlp) {
        $this->idBoletoContribuicaoGlp = $idBoletoContribuicaoGlp;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }

    function setTipoMembro($tipoMembro) {
        $this->tipoMembro = $tipoMembro;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    function setCodigoVeiculo($codigoVeiculo) {
        $this->codigoVeiculo = $codigoVeiculo;
    }

    function setCodigoContaContribuicao($codigoContaContribuicao) {
        $this->codigoContaContribuicao = $codigoContaContribuicao;
    }

    function setValorBoleto($valorBoleto) {
        $this->valorBoleto = $valorBoleto;
    }
    
    function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    function setNumeroBoleto($numeroBoleto) {
        $this->numeroBoleto = $numeroBoleto;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    //--------------------------------------------------------------------------

    public function cadastroBoletoContribuicaoGlp() {

        $sql = "INSERT INTO boleto_contribuicao_glp  
                                 (token,
                                  codigoAfiliacao,
                                  tipoMembro,
                                  seqCadast,
                                  codigoVeiculo,
                                  codigoContaContribuicao,
                                  valorBoleto,
                                  quantidade,
                                  nome,
                                  endereco,
                                  numero,
                                  cidade,
                                  estado,
                                  cep,
                                  numeroBoleto,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                            		:token,
                            		:codigoAfiliacao,
                            		:tipoMembro,
                            		:seqCadast,
                            		:codigoVeiculo,
                            		:codigoContaContribuicao,
                            		:valorBoleto,
                                        :quantidade,
                            		:nome,
                            		:endereco,
                            		:numero,
                            		:cidade,
                            		:estado,
                            		:cep,
                            		:numeroBoleto,
                            		:usuario,
                                    now()
                                    )";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        $sth->bindParam(':token', $this->token, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':tipoMembro', $this->tipoMembro, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':codigoVeiculo', $this->codigoVeiculo, PDO::PARAM_INT);  
        $sth->bindParam(':codigoContaContribuicao', $this->codigoContaContribuicao, PDO::PARAM_INT);
        $sth->bindParam(':valorBoleto', $this->valorBoleto, PDO::PARAM_STR);  
        $sth->bindParam(':quantidade', $this->quantidade, PDO::PARAM_INT); 
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':endereco', $this->endereco, PDO::PARAM_STR);  
        $sth->bindParam(':numero', $this->numero, PDO::PARAM_INT);
        $sth->bindParam(':cidade', $this->cidade, PDO::PARAM_STR);  
        $sth->bindParam(':estado', $this->estado, PDO::PARAM_STR);  
        $sth->bindParam(':cep', $this->cep, PDO::PARAM_STR);  
        $sth->bindParam(':numeroBoleto', $this->numeroBoleto, PDO::PARAM_INT);  
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT); 
        
        /*
        $data = array(
            'token'=>$this->token,
            'codigoAfiliacao'=>$this->codigoAfiliacao,
            'tipoMembro'=>$this->tipoMembro,
            'seqCadast'=>$this->seqCadast,
            'codigoVeiculo'=>$this->codigoVeiculo,
            'codigoContaContribuicao'=>$this->codigoContaContribuicao,
            'valorBoleto'=>$this->valorBoleto,
            'quantidade'=>$this->quantidade,
            'nome'=>$this->nome,
            'endereco'=>$this->endereco,
            'numero'=>$this->numero,
            'cidade'=>$this->cidade,
            'estado'=>$this->estado,
            'cep'=>$this->cep,
            'numeroBoleto'=>0,
            'usuario'=>$this->usuario
        );
        echo $this->parms("cadastroBoletoContribuicaoGlp",$sql,$data);exit();
        */
        
        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idBoletoContribuicaoGlp) as lastid FROM boleto_contribuicao_glp";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function verificaToken($token = null) 
    {
        $sql = "SELECT * FROM boleto_contribuicao_glp
    	where 1=1 ";
        if ($token != null) {
            $sql .= " and token=:token";
        }
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);
        
        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }   
    }

    public function listaBoleto($token = null) {

        $sql = "SELECT * FROM boleto_contribuicao_glp
    	where 1=1 ";
        if ($token != null) {
            $sql .= " and token=:token";
        }
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        if ($token != null) 
        {
            $sth->bindParam(':token', $token, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function atualizarNumeroBoleto() 
    {
        $sql = "UPDATE boleto_contribuicao_glp set 
            numeroBoleto=:numeroBoleto
            where token=:token";
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        $sth->bindParam(':numeroBoleto', $this->numeroBoleto, PDO::PARAM_INT);
        $sth->bindParam(':token', $this->token, PDO::PARAM_STR);
        
        if ($c->run($sth,true)) 
        {
            return true;
        } else {
            return false;
        }   
    }
    
    public function verificaBoleto($numeroBoleto = null) 
    {
        $sql = "SELECT * FROM boleto_contribuicao_glp
    	where 1=1 ";
        if ($numeroBoleto != null) {
            $sql .= " and numeroBoleto=:numeroBoleto";
        }
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        $sth->bindParam(':numeroBoleto', $numeroBoleto, PDO::PARAM_STR);
        
        if ($c->run($sth,null,null,true)) {
            return true;
        } else {
            return false;
        }   
    }

}

?>