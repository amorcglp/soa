<?php

require_once ("conexaoClass.php");

class planoAcaoRegiaoArquivo{

    private $idPlanoRegiaoArquivo;
    private $fk_idPlanoAcaoRegiao;
    private $caminhoPlanoRegiaoArquivo;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoRegiaoArquivo() {
        return $this->idPlanoRegiaoArquivo;
    }

    function getFkIdPlanoAcaoRegiao() {
        return $this->fk_idPlanoAcaoRegiao;
    }

    function getCaminhoPlanoRegiaoArquivo() {
        return $this->caminhoPlanoRegiaoArquivo;
    }

    function setIdPlanoRegiaoArquivo($idPlanoRegiaoArquivo) {
        $this->idPlanoRegiaoArquivo = $idPlanoRegiaoArquivo;
    }

    function setFkIdPlanoAcaoRegiao($fk_idPlanoAcaoRegiao) {
        $this->fk_idPlanoAcaoRegiao = $fk_idPlanoAcaoRegiao;
    }

    function setCaminhoPlanoRegiaoArquivo($caminhoPlanoRegiaoArquivo) {
        $this->caminhoPlanoRegiaoArquivo = $caminhoPlanoRegiaoArquivo;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoRegiaoArquivo(){

    	$sql = "INSERT INTO planoAcaoRegiao_arquivo  
                                 (
                                  fk_idPlanoAcaoRegiao,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idPlanoAcaoRegiao,
                                    :caminho,
                                    now())";
    	//echo $sql; exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoRegiao', $this->fk_idPlanoAcaoRegiao, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminhoPlanoRegiaoArquivo, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'planoAcaoRegiao_arquivo'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
	        foreach ($resultado as $vetor)
	    	{
	    		$proximo_id = $vetor['Auto_increment']; 
	    	}
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaPlanoRegiaoArquivo($id=null)
    {

    	$sql = "SELECT * FROM planoAcaoRegiao_arquivo
    	inner join planoAcaoRegiao on fk_idPlanoAcaoRegiao = idPlanoAcaoRegiao
    	where 1=1";
        
    	if($id!=null)
    	{
            $sql .= " and fk_idPlanoAcaoRegiao = :id";
    	}
    	
    	//echo "<br><br>".$sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);        
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removePlanoAcaoRegiaoArquivo($idPlanoAcaoRegiaoArquivo){
        
    	$sql = "DELETE FROM planoAcaoRegiao_arquivo WHERE idPlanoAcaoRegiaoArquivo = :id";
        
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idPlanoAcaoRegiaoArquivo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    
}

?>
