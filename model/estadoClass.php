<?php

require_once ("conexaoClass.php");

class Estado {

    private $id;
    private $uf;
    private $nome;
    
    function getId() {
        return $this->id;
    }

    function getUf() {
        return $this->uf;
    }
    
    function getNome() {
        return $this->nome;
    }
    
    function setId($id) {
        $this->id = $id;
    }

    function setUf($uf) {
        $this->uf = $uf;
    }
    
    function setNome($nome) {
        $this->nome = $nome;
    }
    
    # Seleciona Evento
    public function listaEstado($uf=null) 
    {
        $sql = "SELECT * FROM estado where 1=1 ";
        if($uf!=null)
        {
                $sql .= " and uf=:uf";
        }

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':uf', $uf, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        if ($resultado){
                return $resultado;
        }else{
                return false;
        }
    }

    public function retornaUfTexto($id=null) 
    {
        $sql = "SELECT * FROM estado where 1=1 ";
        if($id!=null)
        {
                $sql .= " and id=:id";
        }
            
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
                foreach($resultado as $vetor)
                {
                        return $vetor['uf'];
                }
        }
        return false;

    }
    
    public function retornaIdUf($uf) 
    {
        $sql = "SELECT * FROM estado where uf=:uf limit 0,1";
            
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':uf', $uf, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
                foreach($resultado as $vetor)
                {
                        return $vetor['id'];
                }
        }
        return false;

    }
}

?>