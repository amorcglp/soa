<?php

require_once ("conexaoClass.php");

class tabelaContribuicao {

    public $idTabelaContribuicao;
    public $tipo;
    
    public $modalidadeMensalIndividual;
    
    public $modalidadeTrimestralIndividualReal;
    public $modalidadeTrimestralIndividualDolar;
    public $modalidadeTrimestralIndividualEuro;
    
    public $modalidadeAnualIndividualReal;
    public $modalidadeAnualIndividualCFD;
    
    public $modalidadeMensalDual;
    
    public $modalidadeTrimestralDualReal;
    public $modalidadeTrimestralDualDolar;
    public $modalidadeTrimestralDualEuro;
    
    public $modalidadeAnualDualReal;
    public $modalidadeAnualDualCFD;
    
    public $taxaInscricaoReal;
    public $taxaInscricaoDolar;
    public $taxaInscricaoEuro;
    
    public $usuario;
    public $usuarioAlteracao;
    
    //Getter e Setter
    function getIdTabelaContribuicao() {
        return $this->idTabelaContribuicao;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getModalidadeMensalIndividual() {
        return $this->modalidadeMensalIndividual;
    }

    function getModalidadeTrimestralIndividualReal() {
        return $this->modalidadeTrimestralIndividualReal;
    }

    function getModalidadeTrimestralIndividualDolar() {
        return $this->modalidadeTrimestralIndividualDolar;
    }

    function getModalidadeTrimestralIndividualEuro() {
        return $this->modalidadeTrimestralIndividualEuro;
    }

    function getModalidadeAnualIndividualReal() {
        return $this->modalidadeAnualIndividualReal;
    }

    function getModalidadeAnualIndividualCFD() {
        return $this->modalidadeAnualIndividualCFD;
    }

    function getModalidadeMensalDual() {
        return $this->modalidadeMensalDual;
    }

    function getModalidadeTrimestralDualReal() {
        return $this->modalidadeTrimestralDualReal;
    }

    function getModalidadeTrimestralDualDolar() {
        return $this->modalidadeTrimestralDualDolar;
    }

    function getModalidadeTrimestralDualEuro() {
        return $this->modalidadeTrimestralDualEuro;
    }

    function getModalidadeAnualDualReal() {
        return $this->modalidadeAnualDualReal;
    }

    function getModalidadeAnualDualCFD() {
        return $this->modalidadeAnualDualCFD;
    }

    function getTaxaInscricaoReal() {
        return $this->taxaInscricaoReal;
    }

    function getTaxaInscricaoDolar() {
        return $this->taxaInscricaoDolar;
    }

    function getTaxaInscricaoEuro() {
        return $this->taxaInscricaoEuro;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getUsuarioAlteracao() {
        return $this->usuarioAlteracao;
    }

    function setIdTabelaContribuicao($idTabelaContribuicao) {
        $this->idTabelaContribuicao = $idTabelaContribuicao;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setModalidadeMensalIndividual($modalidadeMensalIndividual) {
        $this->modalidadeMensalIndividual = $modalidadeMensalIndividual;
    }

    function setModalidadeTrimestralIndividualReal($modalidadeTrimestralIndividualReal) {
        $this->modalidadeTrimestralIndividualReal = $modalidadeTrimestralIndividualReal;
    }

    function setModalidadeTrimestralIndividualDolar($modalidadeTrimestralIndividualDolar) {
        $this->modalidadeTrimestralIndividualDolar = $modalidadeTrimestralIndividualDolar;
    }

    function setModalidadeTrimestralIndividualEuro($modalidadeTrimestralIndividualEuro) {
        $this->modalidadeTrimestralIndividualEuro = $modalidadeTrimestralIndividualEuro;
    }

    function setModalidadeAnualIndividualReal($modalidadeAnualIndividualReal) {
        $this->modalidadeAnualIndividualReal = $modalidadeAnualIndividualReal;
    }

    function setModalidadeAnualIndividualCFD($modalidadeAnualIndividualCFD) {
        $this->modalidadeAnualIndividualCFD = $modalidadeAnualIndividualCFD;
    }

    function setModalidadeMensalDual($modalidadeMensalDual) {
        $this->modalidadeMensalDual = $modalidadeMensalDual;
    }

    function setModalidadeTrimestralDualReal($modalidadeTrimestralDualReal) {
        $this->modalidadeTrimestralDualReal = $modalidadeTrimestralDualReal;
    }

    function setModalidadeTrimestralDualDolar($modalidadeTrimestralDualDolar) {
        $this->modalidadeTrimestralDualDolar = $modalidadeTrimestralDualDolar;
    }

    function setModalidadeTrimestralDualEuro($modalidadeTrimestralDualEuro) {
        $this->modalidadeTrimestralDualEuro = $modalidadeTrimestralDualEuro;
    }

    function setModalidadeAnualDualReal($modalidadeAnualDualReal) {
        $this->modalidadeAnualDualReal = $modalidadeAnualDualReal;
    }

    function setModalidadeAnualDualCFD($modalidadeAnualDualCFD) {
        $this->modalidadeAnualDualCFD = $modalidadeAnualDualCFD;
    }

    function setTaxaInscricaoReal($taxaInscricaoReal) {
        $this->taxaInscricaoReal = $taxaInscricaoReal;
    }

    function setTaxaInscricaoDolar($taxaInscricaoDolar) {
        $this->taxaInscricaoDolar = $taxaInscricaoDolar;
    }

    function setTaxaInscricaoEuro($taxaInscricaoEuro) {
        $this->taxaInscricaoEuro = $taxaInscricaoEuro;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setUsuarioAlteracao($usuarioAlteracao) {
        $this->usuarioAlteracao = $usuarioAlteracao;
    }

    
    public function cadastraTabelaContribuicao() {
        
        $sql = "INSERT INTO tabelaContribuicao (
                                        tipo,
                                        modalidadeMensalIndividual,
                                        modalidadeTrimestralIndividualReal,
                                        modalidadeTrimestralIndividualDolar,
                                        modalidadeTrimestralIndividualEuro,
                                        modalidadeAnualIndividualReal,
                                        modalidadeAnualIndividualCFD,
                                        modalidadeMensalDual,
                                        modalidadeTrimestralDualReal,
                                        modalidadeTrimestralDualDolar,
                                        modalidadeTrimestralDualEuro,
                                        modalidadeAnualDualReal,
                                        modalidadeAnualDualCFD,
                                        taxaInscricaoReal,
                                        taxaInscricaoDolar,
                                        taxaInscricaoEuro,
                                        usuario,
                                        data
                                    )
                                    VALUES (
                                        :tipo,
                                        :modalidadeMensalIndividual,
                                        :modalidadeTrimestralIndividualReal,
                                        :modalidadeTrimestralIndividualDolar,
                                        :modalidadeTrimestralIndividualEuro,
                                        :modalidadeAnualIndividualReal,
                                        :modalidadeAnualIndividualCFD,
                                        :modalidadeMensalDual,
                                        :modalidadeTrimestralDualReal,
                                        :modalidadeTrimestralDualDolar,
                                        :modalidadeTrimestralDualEuro,
                                        :modalidadeAnualDualReal,
                                        :modalidadeAnualDualCFD,
                                        :taxaInscricaoReal,
                                        :taxaInscricaoDolar,
                                        :taxaInscricaoEuro,
                                        :usuario,    
                                        now()
                                            )";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        
        $sth->bindParam(':modalidadeMensalIndividual', $this->modalidadeMensalIndividual, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeTrimestralIndividualReal', $this->modalidadeTrimestralIndividualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralIndividualDolar', $this->modalidadeTrimestralIndividualDolar, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralIndividualEuro', $this->modalidadeTrimestralIndividualEuro, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeAnualIndividualReal', $this->modalidadeAnualIndividualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeAnualIndividualCFD', $this->modalidadeAnualIndividualCFD, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeMensalDual', $this->modalidadeMensalDual, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeTrimestralDualReal', $this->modalidadeTrimestralDualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralDualDolar', $this->modalidadeTrimestralDualDolar, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralDualEuro', $this->modalidadeTrimestralDualEuro, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeAnualDualReal', $this->modalidadeAnualDualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeAnualDualCFD', $this->modalidadeAnualDualCFD, PDO::PARAM_STR);
        
        $sth->bindParam(':taxaInscricaoReal', $this->taxaInscricaoReal, PDO::PARAM_STR);
        $sth->bindParam(':taxaInscricaoDolar', $this->taxaInscricaoDolar, PDO::PARAM_STR);
        $sth->bindParam(':taxaInscricaoEuro', $this->taxaInscricaoEuro, PDO::PARAM_STR);
        
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_STR);
        
        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function alteraTabelaContribuicao($id) {
        
        $sql = "UPDATE tabelaContribuicao 
                            SET 
                                tipo =:tipo,
                                modalidadeMensalIndividual     =   :modalidadeMensalIndividual,
                                modalidadeTrimestralIndividualReal     =   :modalidadeTrimestralIndividualReal,
                                modalidadeTrimestralIndividualDolar     =   :modalidadeTrimestralIndividualDolar,
                                modalidadeTrimestralIndividualEuro     =   :modalidadeTrimestralIndividualEuro,
                                modalidadeAnualIndividualReal     =   :modalidadeAnualIndividualReal,
                                modalidadeAnualIndividualCFD     =   :modalidadeAnualIndividualCFD,
                                modalidadeMensalDual     =   :modalidadeMensalDual,
                                modalidadeTrimestralDualReal     =   :modalidadeTrimestralDualReal,
                                modalidadeTrimestralDualDolar     =   :modalidadeTrimestralDualDolar,
                                modalidadeTrimestralDualEuro     =   :modalidadeTrimestralDualEuro,
                                modalidadeAnualDualReal     =   :modalidadeAnualDualReal,
                                modalidadeAnualDualCFD     =   :modalidadeAnualDualCFD,
                                taxaInscricaoReal     =   :taxaInscricaoReal,
                                taxaInscricaoDolar     =   :taxaInscricaoDolar,
                                taxaInscricaoEuro     =   :taxaInscricaoEuro,
                                usuarioAlteracao     =   :usuarioAlteracao,    
                                dataAlteracao     =   now()
                            WHERE idTabelaContribuicao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idTabelaContribuicao, PDO::PARAM_INT);

        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        
        $sth->bindParam(':modalidadeMensalIndividual', $this->modalidadeMensalIndividual, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeTrimestralIndividualReal', $this->modalidadeTrimestralIndividualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralIndividualDolar', $this->modalidadeTrimestralIndividualDolar, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralIndividualEuro', $this->modalidadeTrimestralIndividualEuro, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeAnualIndividualReal', $this->modalidadeAnualIndividualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeAnualIndividualCFD', $this->modalidadeAnualIndividualCFD, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeMensalDual', $this->modalidadeMensalDual, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeTrimestralDualReal', $this->modalidadeTrimestralDualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralDualDolar', $this->modalidadeTrimestralDualDolar, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeTrimestralDualEuro', $this->modalidadeTrimestralDualEuro, PDO::PARAM_STR);
        
        $sth->bindParam(':modalidadeAnualDualReal', $this->modalidadeAnualDualReal, PDO::PARAM_STR);
        $sth->bindParam(':modalidadeAnualDualCFD', $this->modalidadeAnualDualCFD, PDO::PARAM_STR);
        
        $sth->bindParam(':taxaInscricaoReal', $this->taxaInscricaoReal, PDO::PARAM_STR);
        $sth->bindParam(':taxaInscricaoDolar', $this->taxaInscricaoDolar, PDO::PARAM_STR);
        $sth->bindParam(':taxaInscricaoEuro', $this->taxaInscricaoEuro, PDO::PARAM_STR);
        
        $sth->bindParam(':usuarioAlteracao', $this->usuarioAlteracao, PDO::PARAM_STR);
        
        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function listaTabelaContribuicao($tipo=null) {

    	$sql = "SELECT * FROM tabelaContribuicao where 1=1 " ;
        if($tipo !=null)
        {
            $sql .= " and tipo = :tipo";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($tipo !=null)
        {
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdTabelaContribuicao($id) {
        
        $sql = "SELECT * FROM tabelaContribuicao 
                              WHERE idTabelaContribuicao = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        

        $resultado = $c->run($sth);
        
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    /**
     * @param $tipo
     * @param $campoDB
     * @param $valor
     * @return bool
     */
    public function editarCampoTabelaContribuicao($tipo, $campoDB, $valor) {

        $sql = "UPDATE tabelaContribuicao 
                              SET ".$campoDB." = :valor
                              WHERE tipo = :tipo";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        $sth->bindParam(':valor', $valor, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

}

?>