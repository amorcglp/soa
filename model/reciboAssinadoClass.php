<?php

require_once ("conexaoClass.php");

class reciboAssinado{

    private $idReciboAssinado;
    private $fk_idRecebimento;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdReciboAssinado() {
        return $this->idReciboAssinado;
    }

    function getFk_idRecebimento() {
        return $this->fk_idRecebimento;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdReciboAssinado($idReciboAssinado) {
        $this->idReciboAssinado = $idReciboAssinado;
    }

    function setFk_idRecebimento($fk_idRecebimento) {
        $this->fk_idRecebimento = $fk_idRecebimento;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    
    
    //--------------------------------------------------------------------------

    public function cadastroReciboAssinado(){

        $sql="INSERT INTO reciboAssinado  
                                 (
                                  fk_idRecebimento,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:id,
                                    :caminho,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idRecebimento, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'reciboAssinado'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                    $proximo_id = $vetor['Auto_increment']; 
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaReciboAssinado($id=null){

    	$sql = "SELECT * FROM reciboAssinado
    	inner join recebimento on fk_idRecebimento = idRecebimento
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idRecebimento = :id";
    	}
        
    	//echo "<br><br>".$sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeReciboAssinado($idReciboAssinado){
        
        $sql= "DELETE FROM reciboAssinado WHERE idReciboAssinado = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idReciboAssinado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
