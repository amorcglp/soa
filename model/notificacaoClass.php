<?php

require_once ("conexaoClass.php");

class Notificacao{

    private $idNotificacao;
    private $mensagemNotificacao;
    private $dataNotificacao;
    private $timestampNotificacao;
    private $tipoNotificacao;
    private $statusNotificacao;
    private $fk_seqCadastRemetente;
    private $fk_idTicket;

    //METODO SET/GET -----------------------------------------------------------
    public function getIdNotificacao() {
        return $this->idNotificacao;
    }
    public function getMensagemNotificacao() {
        return $this->mensagemNotificacao;
    }
    public function getDataNotificacao() {
        return $this->dataNotificacao;
    }
    public function getTimestampNotificacao() {
        return $this->timestampNotificacao;
    }
    public function getTipoNotificacao() {
        return $this->tipoNotificacao;
    }
    public function getStatusNotificacao() {
        return $this->statusNotificacao;
    }
    public function getFkSeqCadastRemetente() {
        return $this->fk_seqCadastRemetente;
    }
    public function getFkIdTicket() {
        return $this->fk_idTicket;
    }

    public function setIdNotificacao($idNotificacao) {
        $this->idNotificacao = $idNotificacao;
    }
    public function setMensagemNotificacao($mensagemNotificacao) {
        $this->mensagemNotificacao = $mensagemNotificacao;
    }
    public function setDataNotificacao($dataNotificacao) {
        $this->dataNotificacao = $dataNotificacao;
    }
    public function setTimestampNotificacao($timestampNotificacao) {
        $this->timestampNotificacao = $timestampNotificacao;
    }
    public function setTipoNotificacao($tipoNotificacao) {
        $this->tipoNotificacao = $tipoNotificacao;
    }
    public function setStatusNotificacao($statusNotificacao) {
        $this->statusNotificacao = $statusNotificacao;
    }
    public function setFkSeqCadastRemetente($fk_seqCadastRemetente) {
        $this->fk_seqCadastRemetente = $fk_seqCadastRemetente;
    }
    public function setFkIdTicket($fk_idTicket) {
        $this->fk_idTicket = $fk_idTicket;
    }

    //--------------------------------------------------------------------------

    public function cadastroNotificacao($mensagemNotificacao,$fk_seqCadastRemetente,$fk_idTicket){

        $sql = "INSERT INTO notificacao
                                 (
                                  mensagemNotificacao,
                                  dataNotificacao,
                                  timestampNotificacao,
                                  fk_seqCadastRemetente,
                                  fk_idTicket
                                  )
                            VALUES (:mensagem,
		                            NOW(),
		                            current_timestamp(),
		                            :seqCadast,
		                            :idTicket)";
        //$data = array( 'mensagem'=>$mensagemNotificacao, 'seqCadast'=> $fk_seqCadastRemetente, 'idTicket'=>$fk_idTicket);
        //echo $this->parms("cadastroNotificacao",$sql,$data);
        
        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mensagem', $mensagemNotificacao, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $fk_seqCadastRemetente , PDO::PARAM_STR);
        $sth->bindParam(':idTicket', $fk_idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idNotificacao) as lastid FROM notificacao";

        $ultimo_id=0;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado)
        {
        	foreach ($resultado as $vetor)
        	{
        		$ultimo_id = $vetor['lastid'];
        	}
        }
        return $ultimo_id;
    }

    public function cadastroNotificacaoUsuario($idNotificacao,$seqCadast){

        $sql="INSERT INTO notificacao_usuario
                                 (
                                  fk_idNotificacao,
                                  fk_seqCadast
                                  )
                            VALUES (:id,
		                    :seqCadast)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idNotificacao, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function retornaListaNotificacao($fk_idTicket=null){

    	$sql = "SELECT * FROM notificacao WHERE 1=1";
        $sql .= ($fk_idTicket!=null) ? " and fk_idTicket=:id ORDER BY idNotificacao DESC" : " and fk_idTicket=:id";

    	//echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($fk_idTicket!=null)
        {
            $sth->bindParam(':id', $fk_idTicket, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaNumeroNotificacao($fk_idTicket=null){

        $sql = "SELECT COUNT(*) as nro FROM notificacao WHERE 1=1";
        $sql .= ($fk_idTicket!=null) ? " and fk_idTicket=:id": "";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($fk_idTicket!=null)
        {
            $sth->bindParam(':id', $fk_idTicket, PDO::PARAM_INT);
        }

        //$data = array( 'id'=>$fk_idTicket );
        //echo $this->parms("retornaNumeroNotificacao",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $vetor) {
                return $vetor['nro'];
            }
        }else{
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}
?>
