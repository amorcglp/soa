<?php

require_once ("conexaoClass.php");

class enviaEmailMembroAtividadeIniciatica {

    private $data;
    private $seqCadastMembro;
    private $fk_idAtividadeIniciatica;
    private $email;
    private $token;

    function getData() {
        return $this->data;
    }

    function getSeqCadastMembro() {
        return $this->seqCadastMembro;
    }

    function getFk_idAtividadeIniciatica() {
        return $this->fk_idAtividadeIniciatica;
    }

    function getEmail() {
        return $this->email;
    }

    function getToken() {
        return $this->token;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setSeqCadastMembro($seqCadastMembro) {
        $this->seqCadastMembro = $seqCadastMembro;
    }

    function setFk_idAtividadeIniciatica($fk_idAtividadeIniciatica) {
        $this->fk_idAtividadeIniciatica = $fk_idAtividadeIniciatica;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setToken($token) {
        $this->token = $token;
    }

    
    # 

    public function verificaToken($token) {
        
        $sql = "SELECT * from enviaEmailMembroAtividadeIniciatica
							where token =:token";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':token',  $token, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    function insereToken($seqCadastMembro, $idAtividadeIniciatica, $email, $token) {
        
        $sql = "INSERT INTO enviaEmailMembroAtividadeIniciatica (data,seqCadastMembro,fk_idAtividadeIniciatica,email,token,envioEmail) values 
    			(now(),:seqCadast,:idAtividadeIniciatica,:email,:token,'0')";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':email', $email, PDO::PARAM_STR);
        $sth->bindParam(':token', $token, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function enviouEmail($token) {

        $sql = "UPDATE enviaEmailMembroAtividadeIniciatica SET envioEmail='1'
		WHERE token = :token";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':token',  $token, PDO::PARAM_INT);

        if ($c->run($sth,true)){

            return true;
        } else {

            return false;
        }
    }
    
    public function erroEmail($token) {

        $sql = "UPDATE enviaEmailMembroAtividadeIniciatica SET erroEmail='1'
		WHERE token = :token";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':token',  $token, PDO::PARAM_STR);

        if ($c->run($sth,true)){

            return true;
        } else {

            return false;
        }
    }

    public function selecionaToken($token) {
        
        $sql = "SELECT * from enviaEmailMembroAtividadeIniciatica
			where token =:token";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':token',  $token, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}

?>