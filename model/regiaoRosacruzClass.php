<?php

require_once ("conexaoClass.php");

class regiaoRosacruz {

    public $idRegiaoRosacruz;
    public $regiaoRosacruz;
    public $divisaoPolitica;
    public $paisRosacruz;
    public $descricaoRegiaoRosacruz;
    public $statusRegiaoRosacruz;

    //Getter e Setter
    function getIdRegiaoRosacruz() {
        return $this->idRegiaoRosacruz;
    }

    function getRegiaoRosacruz() {
        return $this->regiaoRosacruz;
    }

    function getDivisaoPolitica() {
        return $this->divisaoPolitica;
    }

    function getPaisRosacruz() {
        return $this->paisRosacruz;
    }

    function getDescricaoRegiaoRosacruz() {
        return $this->descricaoRegiaoRosacruz;
    }
    
	function getStatusRegiaoRosacruz() {
        return $this->statusRegiaoRosacruz;
    }

    function setIdRegiaoRosacruz($idRegiaoRosacruz) {
        $this->idRegiaoRosacruz = $idRegiaoRosacruz;
    }

    function setRegiaoRosacruz($regiaoRosacruz) {
        $this->regiaoRosacruz = $regiaoRosacruz;
    }

    function setDivisaoPolitica($divisaoPolitica) {
        $this->divisaoPolitica = $divisaoPolitica;
    }

    function setPaisRosacruz($paisRosacruz) {
        $this->paisRosacruz = $paisRosacruz;
    }

    function setDescricaoRegiaoRosacruz($descricaoRegiaoRosacruz) {
        $this->descricaoRegiaoRosacruz = $descricaoRegiaoRosacruz;
    }
    
	function setStatusRegiaoRosacruz($statusRegiaoRosacruz) {
        $this->statusRegiaoRosacruz = $statusRegiaoRosacruz;
    }

    public function cadastraRegiaoRosacruz() {
        
        $sql = "INSERT INTO regiaoRosacruz (idRegiaoRosacruz, regiaoRosacruz, divisaoPolitica, paisRosacruz, descricaoRegiaoRosacruz)
                                    VALUES (:id,
                                            :regiao,
                                            :divisao,
                                            :pais,
                                            :descricao)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idRegiaoRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':regiao', $this->regiaoRosacruz, PDO::PARAM_STR);
        $sth->bindParam(':divisao', $this->divisaoPolitica , PDO::PARAM_STR);
        $sth->bindParam(':pais', $this->paisRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoRegiaoRosacruz, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraRegiaoRosacruz($id) {
        
        $sql = "UPDATE regiaoRosacruz 
                            SET 
                                `regiaoRosacruz` = :regiao,
                                `divisaoPolitica` = :divisao,
                                `paisRosacruz` = :pais,
                                `descricaoRegiaoRosacruz` = :descricao
                            WHERE idRegiaoRosacruz = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':regiao', $this->regiaoRosacruz, PDO::PARAM_STR);
        $sth->bindParam(':divisao', $this->divisaoPolitica , PDO::PARAM_STR);
        $sth->bindParam(':pais', $this->paisRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoRegiaoRosacruz, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusRegiaoRosacruz() {
        
        $sql = "UPDATE regiaoRosacruz SET `statusRegiaoRosacruz` = :status"
                . " WHERE `idRegiaoRosacruz` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusRegiaoRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idRegiaoRosacruz, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusRegiaoRosacruz();
            return $arr;
        } else {
            return false;
        }
    }

    public function listaRegiaoRosacruz($ativa=null,$regiao=null,$idRegiao=null) {

       $sql =" SELECT * FROM regiaoRosacruz where 1=1 ";
       if($ativa==1)
       {
       		$sql .= " and statusRegiaoRosacruz = 0 ";
       }
       if($regiao!=null)
       {
       		$sql .= " and regiaoRosacruz =:id";
       }
       if($idRegiao!=null)
       {
       		$sql .= " and idRegiaoRosacruz =:idRegiao";
       }
       $sql .=" ORDER BY regiaoRosacruz";
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($regiao!=null)
        {
            $sth->bindParam(':id', $regiao, PDO::PARAM_INT);
        }
        if($idRegiao!=null)
        {
            $sth->bindParam(':idRegiao', $idRegiao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaRegiaoRosacruzCadastradaMembroPotencial($hoje=null) {

        $sql =" SELECT regiao FROM membroPotencialOa as m "
               . "where 1=1 "
               . "and DATE(m.dataCadastro) = :hoje "
               . "group by regiao ORDER BY m.siglaOA";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':hoje', $hoje, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeRegiaoRosacruz($idRegiaoRosacruz) {
        
        $sql = "DELETE FROM regiaoRosacruz WHERE idRegiaoRosacruz = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRegiaoRosacruz, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaRegiaoRosacruz() {
        
        $sql = "SELECT * FROM regiaoRosacruz
                                 WHERE `regiaoRosacruz` LIKE :regiao
                                 ORDER BY regiaoRosacruz ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getRegiaoRosacruz = '%'.$this->getRegiaoRosacruz().'%';

        $sth->bindParam(':regiao', $getRegiaoRosacruz, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaRegiaoRosacruzEquals() {
        $sql = "SELECT * FROM regiaoRosacruz
                                 WHERE `regiaoRosacruz` = :regiao
                                 ORDER BY regiaoRosacruz ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':regiao', $this->regiaoRosacruz, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdRegiaoRosacruz() {
        $sql = "SELECT * FROM regiaoRosacruz 
                              WHERE idRegiaoRosacruz = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {
            $id = $_GET['id'];
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaIdRegiaoRosacruz() {
        $sql = "SELECT * FROM regiaoRosacruz
                                 WHERE `regiaoRosacruz` = :regiao
                                 ORDER BY regiaoRosacruz ASC";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':regiao', $this->regiaoRosacruz, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        $id=null;
        if ($resultado){
            foreach ($resultado as $v)
            {
                $id=$v['idRegiaoRosacruz'];
            }
            return $id;
        }else{
            return false;
        }
    }

}

?>