<?php

require_once ("conexaoClass.php");

class emailLog{

    private $idEmailLog;

    private $domainOrigin;
    private $emailFrom;
    private $emailTo;
    private $subject;
    private $html;
    private $domainEmail;

    private $seqCadast;
    private $codigoRetorno;
    private $msgRetorno;

    private $tipoEvento; // pego com “GET /<domain>/log”
    private $descricaoEvento;
    private $jsonEvento;

    private $funcionalidadeNoSistema;

    private $api='mailgun';

    /**
     * @return mixed
     */
    public function getIdEmailLog()
    {
        return $this->idEmailLog;
    }

    /**
     * @param mixed $idEmailLog
     */
    public function setIdEmailLog($idEmailLog)
    {
        $this->idEmailLog = $idEmailLog;
    }

    /**
     * @return mixed
     */
    public function getDomainOrigin()
    {
        return $this->domainOrigin;
    }

    /**
     * @param mixed $domainOrigin
     */
    public function setDomainOrigin($domainOrigin)
    {
        $this->domainOrigin = $domainOrigin;
    }

    /**
     * @return mixed
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * @param mixed $emailFrom
     */
    public function setEmailFrom($emailFrom)
    {
        $this->emailFrom = $emailFrom;
    }

    /**
     * @return mixed
     */
    public function getEmailTo()
    {
        return $this->emailTo;
    }

    /**
     * @param mixed $emailTo
     */
    public function setEmailTo($emailTo)
    {
        $this->emailTo = $emailTo;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param mixed $html
     */
    public function setHtml($html)
    {
        $this->html = $html;
    }

    /**
     * @return mixed
     */
    public function getDomainEmail()
    {
        return $this->domainEmail;
    }

    /**
     * @param mixed $domainEmail
     */
    public function setDomainEmail($domainEmail)
    {
        $this->domainEmail = $domainEmail;
    }

    /**
     * @return mixed
     */
    public function getSeqCadast()
    {
        return $this->seqCadast;
    }

    /**
     * @param mixed $seqCadast
     */
    public function setSeqCadast($seqCadast)
    {
        $this->seqCadast = $seqCadast;
    }

    /**
     * @return mixed
     */
    public function getCodigoRetorno()
    {
        return $this->codigoRetorno;
    }

    /**
     * @param mixed $codigoRetorno
     */
    public function setCodigoRetorno($codigoRetorno)
    {
        $this->codigoRetorno = $codigoRetorno;
    }

    /**
     * @return mixed
     */
    public function getMsgRetorno()
    {
        return $this->msgRetorno;
    }

    /**
     * @param mixed $msgRetorno
     */
    public function setMsgRetorno($msgRetorno)
    {
        $this->msgRetorno = $msgRetorno;
    }

    /**
     * @return mixed
     */
    public function getTipoEvento()
    {
        return $this->tipoEvento;
    }

    /**
     * @param mixed $tipoEvento
     */
    public function setTipoEvento($tipoEvento)
    {
        $this->tipoEvento = $tipoEvento;
    }

    /**
     * @return mixed
     */
    public function getDescricaoEvento()
    {
        return $this->descricaoEvento;
    }

    /**
     * @param mixed $descricaoEvento
     */
    public function setDescricaoEvento($descricaoEvento)
    {
        $this->descricaoEvento = $descricaoEvento;
    }

    /**
     * @return mixed
     */
    public function getJsonEvento()
    {
        return $this->jsonEvento;
    }

    /**
     * @param mixed $jsonEvento
     */
    public function setJsonEvento($jsonEvento)
    {
        $this->jsonEvento = $jsonEvento;
    }

    /**
     * @return mixed
     */
    public function getFuncionalidadeNoSistema()
    {
        return $this->funcionalidadeNoSistema;
    }

    /**
     * @param mixed $funcionalidadeNoSistema
     */
    public function setFuncionalidadeNoSistema($funcionalidadeNoSistema)
    {
        $this->funcionalidadeNoSistema = $funcionalidadeNoSistema;
    }

    /**
     * @return string
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param string $api
     */
    public function setApi($api)
    {
        $this->api = $api;
    }



    //--------------------------------------------------------------------------

    /**
     * [Realiza cadastro de log de email]
     * @return [boolean] [Em caso de sucesso no retorno do PDO retorna TRUE]
     */
    public function cadastro(){

        $sql = "INSERT INTO emailLog (
                    domainOrigin,
                    emailFrom,
                    emailTo,
                    subject,
                    html,
                    domainEmail,
                    seqCadast,
                    codigoRetorno,
                    msgRetorno,
                    tipoEvento,
                    descricaoEvento,
                    jsonEvento,
                    funcionalidadeNoSistema,
                    api,
                    dataHora
) 
        VALUES (
        :domainOrigin,
        :emailFrom,
        :emailTo,
        :subject,
        :html,
        :domainEmail,
        :seqCadast,
        :codigoRetorno,
        :msgRetorno,
        :tipoEvento,
        :descricaoEvento,
        :jsonEvento,
        :funcionalidadeNoSistema,
        :api,
        now()
        )";

        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':domainOrigin', $this->domainOrigin, PDO::PARAM_INT);
        $sth->bindParam(':emailFrom', $this->emailFrom, PDO::PARAM_STR);
        $sth->bindParam(':emailTo', $this->emailTo, PDO::PARAM_STR);
        $sth->bindParam(':subject', $this->subject, PDO::PARAM_STR);
        $sth->bindParam(':html', $this->html, PDO::PARAM_STR);
        $sth->bindParam(':domainEmail', $this->domainEmail, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_STR);
        $sth->bindParam(':codigoRetorno', $this->codigoRetorno, PDO::PARAM_STR);
        $sth->bindParam(':msgRetorno', $this->msgRetorno, PDO::PARAM_STR);
        $sth->bindParam(':tipoEvento', $this->tipoEvento, PDO::PARAM_STR);
        $sth->bindParam(':descricaoEvento', $this->descricaoEvento, PDO::PARAM_STR);
        $sth->bindParam(':jsonEvento', $this->jsonEvento, PDO::PARAM_STR);
        $sth->bindParam(':funcionalidadeNoSistema', $this->funcionalidadeNoSistema, PDO::PARAM_STR);
        $sth->bindParam(':api', $this->jsonEvento, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * [Lista o log de email]
     * @return [boolean] [Em caso de sucesso no retorno do PDO retorna array com todas informações da tabela emailLog]
     */

    public function lista(){

        $sql = "SELECT * FROM emailLog";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>
