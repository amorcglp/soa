<?php

require_once ("conexaoClass.php");

class notificacaoRelatoriosNaoEntreguesSmsMestre {

    public $idNotificacaoRelatoriosNaoEntreguesSmsMestre;
    public $fk_idUsuario;
    public $fk_idOrganismoAfiliado;
    public $nome;
    public $celular;
    public $data;

    //Getter e Setter
    function getIdNotificacaoRelatoriosNaoEntreguesSmsMestre() {
        return $this->idNotificacaoRelatoriosNaoEntreguesSmsMestre;
    }

    function getFk_idUsuario() {
        return $this->fk_idUsuario;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getNome() {
        return $this->nome;
    }

    function getCelular() {
        return $this->celular;
    }

    function getData() {
        return $this->data;
    }

    function setIdNotificacaoRelatoriosNaoEntreguesSmsMestre($idNotificacaoRelatoriosNaoEntreguesSmsMestre) {
        $this->idNotificacaoRelatoriosNaoEntreguesSmsMestre = $idNotificacaoRelatoriosNaoEntreguesSmsMestre;
    }

    function setFk_idUsuario($fk_idUsuario) {
        $this->fk_idUsuario = $fk_idUsuario;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setData($data) {
        $this->data = $data;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO notificacao_relatorios_nao_entregues_sms_mestre 
                    (idNotificacaoRelatoriosNaoEntreguesSmsMestre, 
                    fk_idUsuario, 
                    fk_idOrganismoAfiliado, 
                    nome, 
                    celular, 
                    data)
                                    VALUES (:id,
                                            :idUsuario,
                                            :idOrganismoAfiliado,
                                            :nome,
                                            :celular,
                                            now())";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idNotificacaoRelatoriosNaoEntreguesSmsMestre, PDO::PARAM_INT);
        $sth->bindParam(':idUsuario', $this->fk_idUsuario, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':celular', $this->celular, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function lista() {

        $sql = "SELECT * FROM notificacao_relatorios_nao_entregues_sms_mestre";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
       
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function remove($id) {
        
        $sql = "DELETE FROM notificacao_relatorios_nao_entregues_sms_mestre "
                . " WHERE idNotificacaoRelatoriosNaoEntreguesSmsMestre = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $resultado = $c->run($sth,true);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>