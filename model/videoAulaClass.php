<?php

require_once ("conexaoClass.php");

class videoAula{

    private $idVideoAula;
    private $titulo;
    private $descricao;
    private $dataInicial;
    private $dataFinal;
    private $link;
    private $status;
    private $usuario;
    private $ultimoAtualizar;

    //METODO SET/GET -----------------------------------------------------------

    function getIdVideoAula() {
        return $this->idVideoAula;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getDataInicial() {
        return $this->dataInicial;
    }

    function getDataFinal() {
        return $this->dataFinal;
    }

    function getLink() {
        return $this->link;
    }

    function getStatus() {
        return $this->status;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdVideoAula($idVideoAula) {
        $this->idVideoAula = $idVideoAula;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }

    function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }

    function setLink($link) {
        $this->link = $link;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    
    //--------------------------------------------------------------------------

    public function cadastroVideoAula(){

        $sql="INSERT INTO videoAula  
                                 (titulo,
                                  descricao,
                                  dataInicial,
                                  dataFinal,
                                  link,
                                  status,
                                  usuario,
                                  dataCadastro)
                                  
                            VALUES (:titulo,
                                    :descricao,
                                    :dataInicial,
                                    :dataFinal,
                                    :link,
                                    :status,
                                    :usuario,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':dataInicial', $this->dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $this->dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':link', $this->link, PDO::PARAM_STR);
        $sth->bindParam(':status', $this->status, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraVideoAula($idVideoAula){
    	
        $sql="UPDATE videoAula SET  
                                  titulo = :titulo,
                                  descricao =  :descricao,
                                  dataInicial =  :dataInicial,
				  dataFinal = :dataFinal,
                                  link = :link,
                                  status = :status,
                                  ultimoAtualizar = :ultimoAtualizar
                                  WHERE idVideoAula = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':dataInicial', $this->dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $this->dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':link', $this->link, PDO::PARAM_STR);
        $sth->bindParam(':status', $this->status, PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $idVideoAula, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusVideoAula() {
        
	$sql = "UPDATE videoAula SET `status` = :status
                               WHERE `idVideoAula` = :idVideoAula";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->status, PDO::PARAM_INT);
        $sth->bindParam(':idVideoAula', $this->idVideoAula, PDO::PARAM_INT);
		
        if ($c->run($sth,true)) {
            $arr = array();
            $arr['status'] = $this->status;
            return $arr;
        } else {
            return false;
        }
    }

    public function listaVideoAula($ativas=null){

    	$sql = " SELECT * FROM videoAula ";
        if($ativas!=null)
        {
            $sql .= " where status = 0";
        }    
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaNomeVideoAula(){
        
    	$sql = "SELECT *   FROM    videoAula
                                    WHERE  titulo LIKE ':nome'
                                    ORDER BY titulo ASC";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $nome = '%'.str_replace("  "," ",trim($this->getTitulo())).'%';
        $sth->bindParam(':nome', $nome, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscarIdVideoAula($idVideoAula){
        $sql="SELECT * FROM  videoAula
                                WHERE   idVideoAula = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idVideoAula, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeVideoAula($idVideoAula){
        
        $sql="DELETE FROM videoAula WHERE idVideoAula = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idVideoAula, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }

    
}

?>
