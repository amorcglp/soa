<?php

require_once ("conexaoClass.php");

class imovel{

    private $idImovel;
    private $enderecoImovel;
    private $numeroImovel;
    private $bairroImovel;
    private $complementoImovel;
    private $fk_idCidade;
    private $fk_idEstado;
    private $fk_idPais;
    private $cepImovel;
    private $dataFundacaoImovel;
    private $statusImovel;
    private $fk_seqCadastAtualizadoPor;
    private $fk_idOrganismoAfiliado;
    private $propriedadeTipoImovel;
    private $propriedadeEmNomeImovel;
    private $propriedadeStatusImovel;
    private $propriedadeEscrituraDataImovel;
    private $registroImovelNumeroImovel;
    private $registroImovelDataImovel;
    private $avaliacaoImovel;
    private $avaliacaoValorImovel;
    private $avaliacaoNomeImovel;
    private $avaliacaoCreciImovel;
    private $avaliacaoDataImovel;
    private $areaTotalImovel;
    private $areaConstruidaImovel;
    private $propriedadeEscrituraImovel;
    private $registroImovel;
    private $registroNomeImovel;
    private $valorVenalImovel;
    private $valorAproximadoImovel;
    private $locadorImovel;
    private $locatarioImovel;
    private $aluguelContratoInicioImovel;
    private $aluguelContratoFimImovel;
    private $aluguelValorImovel;
    private $fk_idTipoEscrituraImovel;
    private $descricaoImovel;

    private $fk_idImovel;
    private $numeroImovelMatricula;
    
    //--- GETERS -----------------------------------------------------
	function getIdImovel() {
        return $this->idImovel;
    }
    function getEnderecoImovel() {
        return $this->enderecoImovel;
    }
    function getNumeroImovel() {
        return $this->numeroImovel;
    }
	function getComplementoImovel() {
        return $this->complementoImovel;
    }
    function getBairroImovel() {
        return $this->bairroImovel;
    }
	function getFk_idCidade() {
        return $this->fk_idCidade;
    }
	function getFk_idEstado() {
        return $this->fk_idEstado;
    }
    public function getFkIdPais() {
        return $this->fk_idPais;
    }
	function getCepImovel() {
        return $this->cepImovel;
    }
	function getDataFundacaoImovel() {
        return $this->dataFundacaoImovel;
    }
	function getStatusImovel() {
        return $this->statusImovel;
    }
    function getFk_seqCadastAtualizadoPor() {
        return $this->fk_seqCadastAtualizadoPor;
    }
    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    function getPropriedadeTipoImovel() {
        return $this->propriedadeTipoImovel;
    }
    function getPropriedadeEmNomeImovel() {
        return $this->propriedadeEmNomeImovel;
    }
    function getPropriedadeStatusImovel() {
        return $this->propriedadeStatusImovel;
    }
    function getPropriedadeEscrituraDataImovel() {
        return $this->propriedadeEscrituraDataImovel;
    }
    function getRegistroImovelNumeroImovel() {
        return $this->registroImovelNumeroImovel;
    }
    function getRegistroImovelDataImovel() {
        return $this->registroImovelDataImovel;
    }
    function getAvaliacaoImovel() {
        return $this->avaliacaoImovel;
    }
    function getAvaliacaoValorImovel() {
        return $this->avaliacaoValorImovel;
    }
    function getAvaliacaoNomeImovel() {
        return $this->avaliacaoNomeImovel;
    }
    function getAvaliacaoCreciImovel() {
        return $this->avaliacaoCreciImovel;
    }
    function getAvaliacaoDataImovel() {
        return $this->avaliacaoDataImovel;
    }
    public function getAreaTotalImovel() {
        return $this->areaTotalImovel;
    }
    public function getAreaConstruidaImovel() {
        return $this->areaConstruidaImovel;
    }
    public function getPropriedadeEscrituraImovel() {
        return $this->propriedadeEscrituraImovel;
    }
    public function getRegistroImovel() {
        return $this->registroImovel;
    }
    public function getRegistroNomeImovel() {
        return $this->registroNomeImovel;
    }
    public function getValorVenalImovel() {
        return $this->valorVenalImovel;
    }
    public function getValorAproximadoImovel() {
        return $this->valorAproximadoImovel;
    }
    public function getLocadorImovel() {
        return $this->locadorImovel;
    }
    public function getAluguelContratoInicioImovel() {
        return $this->aluguelContratoInicioImovel;
    }
    public function getLocatarioImovel() {
        return $this->locatarioImovel;
    }
    public function getAluguelContratoFimImovel() {
        return $this->aluguelContratoFimImovel;
    }
    public function getAluguelValorImovel() {
        return $this->aluguelValorImovel;
    }
    public function getFk_idTipoEscrituraImovel() {
        return $this->fk_idTipoEscrituraImovel;
    }
    public function getDescricaoImovel() {
        return $this->descricaoImovel;
    }

    public function getFk_idImovel() {
        return $this->fk_idImovel;
    }
    public function getNumeroImovelMatricula() {
        return $this->numeroImovelMatricula;
    }
	
    //--- SETERS -----------------------------------------------------
    function setIdImovel($idImovel) {
        $this->idImovel = $idImovel;
    }
    function setEnderecoImovel($enderecoImovel) {
        $this->enderecoImovel = $enderecoImovel;
    }
    function setNumeroImovel($numeroImovel) {
        $this->numeroImovel = $numeroImovel;
    }
    function setComplementoImovel($complementoImovel) {
        $this->complementoImovel = $complementoImovel;
    }
	function setBairroImovel($bairroImovel) {
        $this->bairroImovel = $bairroImovel;
    }
    function setFk_idCidade($fk_idCidade) {
        $this->fk_idCidade = $fk_idCidade;
    }
    function setFk_idEstado($fk_idEstado) {
        $this->fk_idEstado = $fk_idEstado;
    }
    public function setFkIdPais($fk_idPais) {
        $this->fk_idPais = $fk_idPais;
    }
	function setCepImovel($cepImovel) {
        $this->cepImovel = $cepImovel;
    }
	function setDataFundacaoImovel($dataFundacaoImovel) {
        $this->dataFundacaoImovel = $dataFundacaoImovel;
    }
	function setStatusImovel($statusImovel) {
        $this->statusImovel = $statusImovel;
    }
    function setFk_seqCadastAtualizadoPor($fk_seqCadastAtualizadoPor) {
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }
    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }
    function setPropriedadeTipoImovel($propriedadeTipoImovel) {
        $this->propriedadeTipoImovel = $propriedadeTipoImovel;
    }
    function setPropriedadeEmNomeImovel($propriedadeEmNomeImovel) {
        $this->propriedadeEmNomeImovel = $propriedadeEmNomeImovel;
    }
    function setPropriedadeStatusImovel($propriedadeStatusImovel) {
        $this->propriedadeStatusImovel = $propriedadeStatusImovel;
    }
    function setPropriedadeEscrituraDataImovel($propriedadeEscrituraDataImovel) {
        $this->propriedadeEscrituraDataImovel = $propriedadeEscrituraDataImovel;
    }
    function setRegistroImovelNumeroImovel($registroImovelNumeroImovel) {
        $this->registroImovelNumeroImovel = $registroImovelNumeroImovel;
    }
    function setRegistroImovelDataImovel($registroImovelDataImovel) {
        $this->registroImovelDataImovel = $registroImovelDataImovel;
    }
    function setAvaliacaoImovel($avaliacaoImovel) {
        $this->avaliacaoImovel = $avaliacaoImovel;
    }
    function setAvaliacaoValorImovel($avaliacaoValorImovel) {
        $this->avaliacaoValorImovel = $avaliacaoValorImovel;
    }
    function setAvaliacaoNomeImovel($avaliacaoNomeImovel) {
        $this->avaliacaoNomeImovel = $avaliacaoNomeImovel;
    }
    function setAvaliacaoCreciImovel($avaliacaoCreciImovel) {
        $this->avaliacaoCreciImovel = $avaliacaoCreciImovel;
    }
    function setAvaliacaoDataImovel($avaliacaoDataImovel) {
        $this->avaliacaoDataImovel = $avaliacaoDataImovel;
    }
    public function setAreaTotalImovel($areaTotalImovel) {
        $this->areaTotalImovel = $areaTotalImovel;
    }
    public function setAreaConstruidaImovel($areaConstruidaImovel) {
        $this->areaConstruidaImovel = $areaConstruidaImovel;
    }
    public function setPropriedadeEscrituraImovel($propriedadeEscrituraImovel) {
        $this->propriedadeEscrituraImovel = $propriedadeEscrituraImovel;
    }
    public function setRegistroImovel($registroImovel) {
        $this->registroImovel = $registroImovel;
    }
    public function setRegistroNomeImovel($registroNomeImovel) {
        $this->registroNomeImovel = $registroNomeImovel;
    }
    public function setValorVenalImovel($valorVenalImovel) {
        $this->valorVenalImovel = $valorVenalImovel;
    }
    public function setValorAproximadoImovel($valorAproximadoImovel) {
        $this->valorAproximadoImovel = $valorAproximadoImovel;
    }
    public function setLocadorImovel($locadorImovel) {
        $this->locadorImovel = $locadorImovel;
    }
    public function setAluguelContratoInicioImovel($aluguelContratoInicioImovel) {
        $this->aluguelContratoInicioImovel = $aluguelContratoInicioImovel;
    }
    public function setLocatarioImovel($locatarioImovel) {
        $this->locatarioImovel = $locatarioImovel;
    }
    public function setAluguelContratoFimImovel($aluguelContratoFimImovel) {
        $this->aluguelContratoFimImovel = $aluguelContratoFimImovel;
    }
    public function setAluguelValorImovel($aluguelValorImovel) {
        $this->aluguelValorImovel = $aluguelValorImovel;
    }
    public function setFk_idTipoEscrituraImovel($fk_idTipoEscrituraImovel) {
        $this->fk_idTipoEscrituraImovel = $fk_idTipoEscrituraImovel;
    }
    public function setDescricaoImovel($descricaoImovel) {
        $this->descricaoImovel = $descricaoImovel;
    }

    public function setFk_idImovel($fk_idImovel) {
        $this->fk_idImovel = $fk_idImovel;
    }
    public function setNumeroImovelMatricula($numeroImovelMatricula) {
        $this->numeroImovelMatricula = $numeroImovelMatricula;
    }


    //--------------------------------------------------------------------------

    public function cadastroImovel(){

        $sql = "INSERT INTO imovel
                                 (enderecoImovel,
                                  numeroImovel,
                                  complementoImovel,
                                  bairroImovel,
                                  fk_idCidade,
                                  fk_idEstado,
                                  fk_idPais,
                                  cepImovel,
                                  dataFundacaoImovel,
                                  propriedadeTipoImovel,
                                  propriedadeEmNomeImovel,
                                  propriedadeStatusImovel,
                                  propriedadeEscrituraDataImovel,
                                  registroImovelNumeroImovel,
                                  registroImovelDataImovel,
                                  avaliacaoImovel,
                                  avaliacaoValorImovel,
                                  avaliacaoNomeImovel,
                                  avaliacaoCreciImovel,
                                  avaliacaoDataImovel,
                                  areaTotalImovel,
                                  areaConstruidaImovel,
                                  propriedadeEscrituraImovel,
                                  registroImovel,
                                  registroNomeImovel,
                                  valorVenalImovel,
                                  valorAproximadoImovel,
                                  locadorImovel,
                                  locatarioImovel,
                                  aluguelContratoInicioImovel,
                                  aluguelContratoFimImovel,
                                  aluguelValorImovel,
                                  descricaoImovel,
                                  fk_idTipoEscrituraImovel,
                                  fk_seqCadastAtualizadoPor,
                                  fk_idOrganismoAfiliado)
                            VALUES (:endereco,
                                    :numero,
                                    :complemento,
                                    :bairro,
                                    :idCidade,
                                    :idEstado,
                                    :idPais,
                                    :cep,
                                    :dataFundacao,
                                    :propriedadeTipo,
                                    :propriedadeEmNome,
                                    :propriedadeStatus,
                                    :propriedadeEscrituraData,
                                    :registroImovelNumero,
                                    :registroImovelData,
                                    :avaliacao,
                                    :avaliacaoValor,
                                    :avaliacaoNome,
                                    :avaliacaoCreci,
                                    :avaliacaoData,
                                    :areaTotal,
                                    :areaConstruida,
                                    :propriedadeEscritura,
                                    :registro,
                                    :registroNome,
                                    :valorVenal,
                                    :valorAproximado,
                                    :locador,
                                    :locatario,
                                    :aluguelContratoInicio,
                                    :aluguelContratoFim,
                                    :aluguelValor,
                                    :descricao,
                                    :idTipoEscritura,
                                    :seqCadastAtualizadoPor,
                                    :idOrganismoAfiliado)";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':endereco',$this->enderecoImovel , PDO::PARAM_STR);
        $sth->bindParam(':numero',$this->numeroImovel , PDO::PARAM_INT);
        $sth->bindParam(':complemento',$this->complementoImovel , PDO::PARAM_STR);
        $sth->bindParam(':bairro',$this->bairroImovel , PDO::PARAM_STR);
        $sth->bindParam(':idCidade',$this->fk_idCidade , PDO::PARAM_INT);
        $sth->bindParam(':idEstado',$this->fk_idEstado , PDO::PARAM_INT);
        $sth->bindParam(':idPais',$this->fk_idPais , PDO::PARAM_INT);
        $sth->bindParam(':cep',$this->cepImovel , PDO::PARAM_STR);
        $sth->bindParam(':dataFundacao',$this->dataFundacaoImovel , PDO::PARAM_STR);
        $sth->bindParam(':propriedadeTipo',$this->propriedadeTipoImovel , PDO::PARAM_INT);
        $sth->bindParam(':propriedadeEmNome',$this->propriedadeEmNomeImovel , PDO::PARAM_STR);
        $sth->bindParam(':propriedadeStatus',$this->propriedadeStatusImovel , PDO::PARAM_INT);
        $sth->bindParam(':propriedadeEscrituraData',$this->propriedadeEscrituraDataImovel , PDO::PARAM_STR);
        $sth->bindParam(':registroImovelNumero',$this->registroImovelNumeroImovel , PDO::PARAM_STR);
        $sth->bindParam(':registroImovelData',$this->registroImovelDataImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacao',$this->avaliacaoImovel , PDO::PARAM_INT);
        $sth->bindParam(':avaliacaoValor',$this->avaliacaoValorImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacaoNome',$this->avaliacaoNomeImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacaoCreci',$this->avaliacaoCreciImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacaoData',$this->avaliacaoDataImovel , PDO::PARAM_STR);
        $sth->bindParam(':areaTotal',$this->areaTotalImovel, PDO::PARAM_STR);
        $sth->bindParam(':areaConstruida',$this->areaConstruidaImovel , PDO::PARAM_STR);
        $sth->bindParam(':propriedadeEscritura',$this->propriedadeEscrituraImovel , PDO::PARAM_INT);
        $sth->bindParam(':registro',$this->registroImovel , PDO::PARAM_INT);
        $sth->bindParam(':registroNome',$this->registroNomeImovel , PDO::PARAM_STR);
        $sth->bindParam(':valorVenal',$this->valorVenalImovel , PDO::PARAM_STR);
        $sth->bindParam(':valorAproximado',$this->valorAproximadoImovel , PDO::PARAM_STR);
        $sth->bindParam(':locador',$this->locadorImovel , PDO::PARAM_STR);
        $sth->bindParam(':locatario',$this->locatarioImovel , PDO::PARAM_STR);
        $sth->bindParam(':aluguelContratoInicio',$this->aluguelContratoInicioImovel , PDO::PARAM_STR);
        $sth->bindParam(':aluguelContratoFim',$this->aluguelContratoFimImovel , PDO::PARAM_STR);
        $sth->bindParam(':aluguelValor',$this->aluguelValorImovel , PDO::PARAM_STR);
        $sth->bindParam(':descricao',$this->descricaoImovel , PDO::PARAM_STR);
        $sth->bindParam(':idTipoEscritura',$this->fk_idTipoEscrituraImovel , PDO::PARAM_INT);
        $sth->bindParam(':seqCadastAtualizadoPor',$this->fk_seqCadastAtualizadoPor , PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado',$this->fk_idOrganismoAfiliado , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $idImovel
     * @return bool
     */
    public function alteraImovel($idImovel)
    {
        $sql = "UPDATE imovel SET
                                 `enderecoImovel` = :endereco,
                                 `numeroImovel` =  :numero,
				 `complementoImovel` = :complemento,
                                 `bairroImovel` = :bairro,
                                 `fk_idCidade` = :idCidade,
                                 `fk_idEstado` = :idEstado,
                                 `fk_idPais` = :idPais,
                                 `cepImovel` = :cep,
                                 `dataFundacaoImovel` = :dataFundacao,
                                 `propriedadeTipoImovel` =  :propriedadeTipo,
				 `propriedadeEmNomeImovel` = :propriedadeEmNome,
                                 `propriedadeStatusImovel` = :propriedadeStatus,
                                 `propriedadeEscrituraDataImovel` = :propriedadeEscrituraData,
                                 `registroImovelNumeroImovel` = :registroImovelNumero,
                                 `registroImovelDataImovel` = :registroImovelData,
                                 `avaliacaoValorImovel` = :avaliacaoValor,
				 `avaliacaoImovel` = :avaliacao,
                                 `avaliacaoNomeImovel` = :avaliacaoNome,
                                 `avaliacaoCreciImovel` = :avaliacaoCreci,
                                 `avaliacaoDataImovel` = :avaliacaoData,
                                 `areaTotalImovel` = :areaTotal,
                                 `areaConstruidaImovel` = :areaConstruida,
                                 `propriedadeEscrituraImovel` = :propriedadeEscritura,
                                 `registroImovel` = :registro,
                                 `registroNomeImovel` = :registroNome,
                                 `valorVenalImovel` = :valorVenal,
                                 `valorAproximadoImovel` = :valorAproximado,
                                 `locadorImovel` = :locador,
                                 `locatarioImovel` = :locatario,
                                 `aluguelContratoInicioImovel` = :aluguelContratoInicio,
                                 `aluguelContratoFimImovel` = :aluguelContratoFim,
                                 `aluguelValorImovel` = :aluguelValor,
                                 `descricaoImovel` = :descricao,
                                 `fk_idTipoEscrituraImovel` = :idTipoEscritura,
                                 `fk_seqCadastAtualizadoPor` = :seqCadastAtualizadoPor,
                                 `fk_idOrganismoAfiliado` = :idOrganismoAfiliado
                                  WHERE idImovel = :id";

        /*
             $query .= ", `locadorImovel` = '".$this->getLocadorImovel()."',
                                     `locatarioImovel` = '".$this->getLocatarioImovel()."',
                                     `aluguelContratoInicioImovel` = '".$this->getAluguelContratoInicioImovel()."',
                                     `aluguelContratoFimImovel` = '".$this->getAluguelContratoFimImovel()."',
                                     `aluguelValorImovel` = '".$this->getAluguelValorImovel()."',
                                     `aluguelImovel` = '".$this->getAluguelImovel()."'";
            if ($this->getAluguelAnexoImovel() != "") {
                        $query .= ", `aluguelAnexoImovel` = '".$this->getAluguelAnexoImovel()."'";
            }
                        $query .= ", `fk_idTipoEscrituraImovel` = '".$this->getFk_idTipoEscrituraImovel()."',
                                     `fk_seqCadastAtualizadoPor` = '".$this->getFk_seqCadastAtualizadoPor()."',
                                     `fk_idOrganismoAfiliado` = '".$this->getFk_idOrganismoAfiliado()."'
                                      WHERE idImovel = ".$idImovel."";
         */

        //echo $sql;exit();

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':endereco',$this->enderecoImovel , PDO::PARAM_STR);
        $sth->bindParam(':numero',$this->numeroImovel , PDO::PARAM_INT);
        $sth->bindParam(':complemento',$this->complementoImovel , PDO::PARAM_STR);
        $sth->bindParam(':bairro',$this->bairroImovel , PDO::PARAM_STR);
        $sth->bindParam(':idCidade',$this->fk_idCidade , PDO::PARAM_INT);
        $sth->bindParam(':idEstado',$this->fk_idEstado , PDO::PARAM_INT);
        $sth->bindParam(':idPais',$this->fk_idPais , PDO::PARAM_INT);
        $sth->bindParam(':cep',$this->cepImovel , PDO::PARAM_STR);
        $sth->bindParam(':dataFundacao',$this->dataFundacaoImovel , PDO::PARAM_STR);
        $sth->bindParam(':propriedadeTipo',$this->propriedadeTipoImovel , PDO::PARAM_INT);
        $sth->bindParam(':propriedadeEmNome',$this->propriedadeEmNomeImovel , PDO::PARAM_STR);
        $sth->bindParam(':propriedadeStatus',$this->propriedadeStatusImovel , PDO::PARAM_INT);
        $sth->bindParam(':propriedadeEscrituraData',$this->propriedadeEscrituraDataImovel , PDO::PARAM_STR);
        $sth->bindParam(':registroImovelNumero',$this->registroImovelNumeroImovel , PDO::PARAM_STR);
        $sth->bindParam(':registroImovelData',$this->registroImovelDataImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacao',$this->avaliacaoImovel , PDO::PARAM_INT);
        $sth->bindParam(':avaliacaoValor',$this->avaliacaoValorImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacaoNome',$this->avaliacaoNomeImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacaoCreci',$this->avaliacaoCreciImovel , PDO::PARAM_STR);
        $sth->bindParam(':avaliacaoData',$this->avaliacaoDataImovel , PDO::PARAM_STR);
        $sth->bindParam(':areaTotal',$this->areaTotalImovel, PDO::PARAM_STR);
        $sth->bindParam(':areaConstruida',$this->areaConstruidaImovel , PDO::PARAM_STR);
        $sth->bindParam(':propriedadeEscritura',$this->propriedadeEscrituraImovel , PDO::PARAM_INT);
        $sth->bindParam(':registro',$this->registroImovel , PDO::PARAM_INT);
        $sth->bindParam(':registroNome',$this->registroNomeImovel , PDO::PARAM_STR);
        $sth->bindParam(':valorVenal',$this->valorVenalImovel , PDO::PARAM_STR);
        $sth->bindParam(':valorAproximado',$this->valorAproximadoImovel , PDO::PARAM_STR);
        $sth->bindParam(':locador',$this->locadorImovel , PDO::PARAM_STR);
        $sth->bindParam(':locatario',$this->locatarioImovel , PDO::PARAM_STR);
        $sth->bindParam(':aluguelContratoInicio',$this->aluguelContratoInicioImovel , PDO::PARAM_STR);
        $sth->bindParam(':aluguelContratoFim',$this->aluguelContratoFimImovel , PDO::PARAM_STR);
        $sth->bindParam(':aluguelValor',$this->aluguelValorImovel , PDO::PARAM_STR);
        $sth->bindParam(':descricao',$this->descricaoImovel , PDO::PARAM_STR);
        $sth->bindParam(':idTipoEscritura',$this->fk_idTipoEscrituraImovel , PDO::PARAM_INT);
        $sth->bindParam(':seqCadastAtualizadoPor',$this->fk_seqCadastAtualizadoPor , PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado',$this->fk_idOrganismoAfiliado , PDO::PARAM_INT);
        $sth->bindParam(':id',$idImovel , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function listaImovel($idOrganismoAfiliado=null, $regiao=null, $statusImovel=null, $inner=null,$agruparOA=null){
		
    	$sql = "SELECT i.*,o.fk_idRegiaoOrdemRosacruz,rr.regiaoRosacruz, o.* FROM imovel as i ";
        
        if($inner==true)
        {    
            $sql .= " inner join organismoAfiliado as o on i.fk_idOrganismoAfiliado = o.idOrganismoAfiliado 
    			inner join regiaoRosacruz as rr on o.fk_idRegiaoOrdemRosacruz = rr.idRegiaoRosacruz ";
        }else{
            $sql .= " left join organismoAfiliado as o on i.fk_idOrganismoAfiliado = o.idOrganismoAfiliado 
    			left join regiaoRosacruz as rr on o.fk_idRegiaoOrdemRosacruz = rr.idRegiaoRosacruz ";
        }        
        
    	$sql .= " WHERE 1=1 ";
    	
    	if ($idOrganismoAfiliado != "") {
			$sql .= " and i.fk_idOrganismoAfiliado = :idOrganismoAfiliado";
		}
		
    	if ($regiao != null) {
			$sql .= " and rr.regiaoRosacruz = :regiao";
		}

        if ($statusImovel === 0) {
            $sql .= " and i.statusImovel = 0";
        }
	if($agruparOA!=null)
        {    
            $sql .= " GROUP BY idOrganismoAfiliado ";
        }
        
        $sql .= " ORDER BY i.idImovel ASC";
    	//echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($idOrganismoAfiliado != "") {
            $sth->bindParam(':idOrganismoAfiliado',  $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if ($regiao != null) {
            $sth->bindParam(':regiao',  $regiao, PDO::PARAM_STR);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdImovel($idImovel){
        $sql="SELECT * FROM  imovel
                                WHERE   idImovel = :id";
        
        //echo "SELECT * FROM  imovel WHERE   idImovel = '".$idImovel."' <br>";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id',  $idImovel, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function alteraStatusImovel() {
		
        $sql = "UPDATE imovel SET `statusImovel` = :status "
                . " WHERE `idImovel` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':status',  $this->statusImovel, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idImovel, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            	$arr = array();
		$arr['status'] = $this->statusImovel;
            return $arr;
        } else {
            return false;
        }
    }
    
    public function cadastroImovelGaleriaImagens($nome_original,$nome_arquivo,$ext,$full_path,$data,$excluido,$fk_idImovel){
		
        $sql = "INSERT INTO imovelGaleria (nome_original,nome_arquivo,ext,full_path,data,excluido,fk_idImovel)
                    VALUES (:nomeOriginal,
                            :nomeArquivo,
                            :ext,
                            :fullPath,
                            :data,
                            :excluido,
                            :idImovel)";

        //echo $sql;
	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nomeOriginal',  $nome_original, PDO::PARAM_STR);
        $sth->bindParam(':nomeArquivo', $nome_arquivo, PDO::PARAM_STR);
        $sth->bindParam(':ext',  $ext, PDO::PARAM_STR);
        $sth->bindParam(':fullPath', $full_path, PDO::PARAM_STR);
        $sth->bindParam(':data',  $data, PDO::PARAM_STR);
        $sth->bindParam(':excluido', $excluido, PDO::PARAM_INT);
        $sth->bindParam(':idImovel',  $fk_idImovel, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }
    
    public function listaImovelGaleria($idImovel){
		
    	$sql = "SELECT * FROM imovelGaleria WHERE excluido = '1' "
                . " and fk_idImovel = :id ORDER BY idImovelGaleria DESC";
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id',  $idImovel, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function excluiImovelGaleria($idImagem){
        
        $sql="UPDATE imovelGaleria SET excluido='0' "
                . " WHERE idImovelGaleria = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idImagem, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function cadastroTipoDeEscritura($tipoEscritura){

        $sql = "INSERT INTO imovel_escritura (tipoEscrituraImovel)
                            VALUES (:tipoEscritura)";

        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipoEscritura', $tipoEscritura, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdTipoEscritura(){

        $sql= "SELECT MAX(idTipoEscrituraImovel) as lastid FROM imovel_escritura";

        $ultimo_id=0;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado)
        {
        	foreach ($resultado as $vetor)
        	{
        		$ultimo_id = $vetor['lastid']; 
        	}
        }
        return $ultimo_id;
    }

    public function listaTipoEscrituraImovel(){

        $sql = "SELECT * FROM imovel_escritura ORDER BY tipoEscrituraImovel ASC";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    public function buscaTipoEscrituraImovel($idTipoEscrituraImovel){

        $sql = "SELECT tipoEscrituraImovel FROM imovel_escritura where idTipoEscrituraImovel=:id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idTipoEscrituraImovel, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaPais(){

        $sql = "SELECT * FROM pais";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    public function listaEstado(){

        $sql = "SELECT * FROM estado";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    public function buscaEstado($idEstado){

        $sql = "SELECT * FROM estado where id=:id";

        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idEstado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    public function listaEstados($idPais){

        $sql = "SELECT * FROM estado where pais=:id";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idPais, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    public function listaCidades($idEstado){

        $sql = "SELECT * FROM cidade where estado=:id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idEstado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaCidade($idCidade){

        $sql = "SELECT * FROM cidade where id=:id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idCidade, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaImovelAnexo($fk_idImovel, $fk_idImovelAnexoTipo){

        $sql = "SELECT * FROM imovelAnexo
                  WHERE excluido = '1'
                  and fk_idImovel = :idImovel
                  and fk_idImovelAnexoTipo = :idImovelAnexoTipo
                  ORDER BY idImovelAnexo DESC";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idImovel', $fk_idImovel, PDO::PARAM_INT);
        $sth->bindParam(':idImovelAnexoTipo', $fk_idImovelAnexoTipo, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function excluiImovelAnexo($idAnexo){
        
        $sql="UPDATE imovelAnexo SET excluido='0' WHERE idImovelAnexo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idAnexo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function cadastroImovelAnexo($nome_original,$nome_arquivo,$ext,$full_path,$data,$excluido,$idImovelAnexoTipo,$fk_idImovel){

        $sql = "INSERT INTO imovelAnexo (nome_original,nome_arquivo,ext,full_path,data,excluido,fk_idImovelAnexoTipo,fk_idImovel)
                            VALUES (:nomeOriginal,
                                    :nomeArquivo,
                                    :ext,
                                    :fullPath,
                                    :dataCadastro,
                                    :excluido,
                                    :idImovelAnexoTipo,
                                    :idImovel)";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        // echo $c->parms('cadastroImovelAnexo', $sql, [
        //     "nomeOriginal" => $nome_original,
        //     "nomeArquivo" => $nome_arquivo,
        //     "ext"  => $ext,
        //     "full_path" => $full_path,
        //     "dataCadastro" => $data,
        //     "excluido" => $excluido,
        //     "idImovelAnexoTipo" => $idImovelAnexoTipo,
        //     "idImovel" => $fk_idImovel
        // ]);

        $sth->bindParam(':nomeOriginal',  $nome_original, PDO::PARAM_STR);
        $sth->bindParam(':nomeArquivo', $nome_arquivo, PDO::PARAM_STR);
        $sth->bindParam(':ext',  $ext, PDO::PARAM_STR);
        $sth->bindParam(':fullPath', $full_path, PDO::PARAM_STR);
        $sth->bindParam(':dataCadastro',  $data, PDO::PARAM_STR);
        $sth->bindParam(':excluido', $excluido, PDO::PARAM_INT);
        $sth->bindParam(':idImovelAnexoTipo',  $idImovelAnexoTipo, PDO::PARAM_INT);
        $sth->bindParam(':idImovel',  $fk_idImovel, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idImovel) as lastid FROM imovel";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $ultimo_id=0;
        $resultado=$c->run($sth);
        if($resultado) 
        {
            foreach ($resultado as $vetor) {
                $ultimo_id = $vetor['lastid'];
            }
        }
        return $ultimo_id;
    }

    public function cadastroImovelMatricula($fk_idImovel,$numeroImovelMatricula){

        $sql = "INSERT INTO imovelMatricula (fk_idImovel,
                                            numeroImovelMatricula,
                                            dataCadastroImovelMatricula)
                            VALUES (:idImovel,:numeroImovelMatricula,NOW());";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idImovel',  $fk_idImovel, PDO::PARAM_INT);
        $sth->bindParam(':numeroImovelMatricula',  $numeroImovelMatricula, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraImovelMatricula($fk_idImovel,$numeroImovelMatricula,$numeroImovelMatriculaNovo){

        $sql="UPDATE imovelMatricula SET numeroImovelMatricula=:numeroImovelMatriculaNovo, 
                                         dataAlteraImovelMatricula=NOW()"
                . " WHERE fk_idImovel = :idImovel and numeroImovelMatricula=:numeroImovelMatricula;";

        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idImovel',  $fk_idImovel, PDO::PARAM_INT);
        $sth->bindParam(':numeroImovelMatricula',  $numeroImovelMatricula, PDO::PARAM_INT);
        $sth->bindParam(':numeroImovelMatriculaNovo',  $numeroImovelMatriculaNovo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function deletaImovelMatricula($fk_idImovel,$numeroImovelMatricula){

        $sql="DELETE FROM `imovelMatricula` WHERE 
                        fk_idImovel = :idImovel and 
                        numeroImovelMatricula=:numeroImovelMatricula;";

        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idImovel',  $fk_idImovel, PDO::PARAM_INT);
        $sth->bindParam(':numeroImovelMatricula',  $numeroImovelMatricula, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function listaImovelMatricula($fk_idImovel){

        $sql = "SELECT * FROM imovelMatricula WHERE 1=1
                  and fk_idImovel = :fk_idImovel
                  ORDER BY dataCadastroImovelMatricula";
        
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_idImovel', $fk_idImovel, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaJaExisteImovelMatricula($fk_idImovel,$numeroImovelMatricula){

        $sql = "SELECT * FROM imovelMatricula WHERE 1=1
                  and fk_idImovel = :fk_idImovel
                  and numeroImovelMatricula = :numeroImovelMatricula";
        
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_idImovel', $fk_idImovel, PDO::PARAM_INT);
        $sth->bindParam(':numeroImovelMatricula', $numeroImovelMatricula, PDO::PARAM_INT);
        
        return $c->run($sth,null,null,true);
    }
    
}

?>
