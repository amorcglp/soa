<?php

require_once ("conexaoClass.php");

class relatorioGrandeConselheiroSupervisao{

    private $fk_idRelatorioGrandeConselheiro;
    private $fk_idOrganismoAfiliado;
    
    //METODO SET/GET -----------------------------------------------------------

    function getFk_idRelatorioGrandeConselheiro() {
        return $this->fk_idRelatorioGrandeConselheiro;
    }
    
	function getFK_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
	}
  
	function setFk_idRelatorioGrandeConselheiro($fk_idRelatorioGrandeConselheiro) {
        $this->fk_idRelatorioGrandeConselheiro = $fk_idRelatorioGrandeConselheiro;
    }
    
	function setFK_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    //--------------------------------------------------------------------------

    public function cadastroRelatorioGrandeConselheiroSupervisao(){

        $sql="INSERT INTO relatorioGrandeConselheiro_supervisao  
                            (
                             fk_idRelatorioGrandeConselheiro,
                             fk_idOrganismoAfiliado
                             )     
                            VALUES (:idOrganismoAfiliado,
                                    :idRelatorioGrandeConselheiro)";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFK_idOrganismoAfiliado                              = $this->getFK_idOrganismoAfiliado();
        $getFk_idRelatorioGrandeConselheiro                     = $this->getFk_idRelatorioGrandeConselheiro();

        $sth->bindParam(':idOrganismoAfiliado', $getFK_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':idRelatorioGrandeConselheiro', $getFk_idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function listaOrganismos($idRelatorioGrandeConselheiro=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM relatorioGrandeConselheiro_supervisao
    	where 1=1 
    	 ";   
    	if($idRelatorioGrandeConselheiro!=null)
    	{
    		$sql .= " and fk_idRelatorioGrandeConselheiro=:idRelatorioGrandeConselheiro";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idRelatorioGrandeConselheiro!=null)
    	{
            $sth->bindParam(':idRelatorioGrandeConselheiro', $idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeOrganismos($idRelatorioGrandeConselheiro=null){
        
    	$sql = "DELETE FROM relatorioGrandeConselheiro_supervisao WHERE 1=1";
    	if($idRelatorioGrandeConselheiro!=null)
    	{
    		$sql .= " and fk_idRelatorioGrandeConselheiro = :id";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idRelatorioGrandeConselheiro!=null)
    	{
            $sth->bindParam(':id', $idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }   
}

?>
