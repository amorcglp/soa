<?php

require_once ("conexaoClass.php");

class tentativaLogin{

    private $idTentativaLogin;
    private $ip;
    private $login;
    private $senha;
    private $data;

    function getIdTentativaLogin() {
        return $this->idTentativaLogin;
    }

    function getIp() {
        return $this->ip;
    }

    function getLogin() {
        return $this->login;
    }

    function getSenha() {
        return $this->senha;
    }

    function getData() {
        return $this->data;
    }

    function setIdTentativaLogin($idTentativaLogin) {
        $this->idTentativaLogin = $idTentativaLogin;
    }

    function setIp($ip) {
        $this->ip = $ip;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setData($data) {
        $this->data = $data;
    }

    
        
    //--------------------------------------------------------------------------

    /**
     * [Realiza cadastro de Tentativa de Login]
     * @return [boolean] [Em caso de sucesso no retorno do PDO retorna TRUE]
     */
    public function cadastroTentativaLogin(){

        $sql = "INSERT INTO tentativa_login 
            (ip, login, senha, data) 
        VALUES (
        :ip,
        :login,
        :senha,
        now()
        )";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':ip', $this->ip, PDO::PARAM_STR);
        $sth->bindParam(':login', $this->login, PDO::PARAM_STR);
        $sth->bindParam(':senha', $this->senha, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function selecionaUltimaTentativaLogin($ip, $limite=null){

        $sql = "SELECT * FROM tentativa_login where ip=:ip ORDER BY data DESC ";
        
        if($limite!=null)
        {    
              $sql .= " limit 1";
        }

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':ip', $ip, PDO::PARAM_STR);

        return $c->run($sth);
    }
    
    public function limpaTentativaLogin($ip){

        $sql = "DELETE FROM tentativa_login where ip=:ip ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':ip', $ip, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}

?>
