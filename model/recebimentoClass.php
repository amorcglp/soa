<?php
require_once ("conexaoClass.php");

class Recebimento {

    public $idRecebimento;
    public $fk_idOrganismoAfiliado;
    public $dataRecebimento;
    public $descricaoRecebimento;
    public $recebemosDe;
    public $codigoAfiliacao;
    public $valorRecebimento;
    public $categoriaRecebimento;
    public $usuario;
    public $ultimoAtualizar;
    public $idRefDocumentoFirebase;

    /* Funções GET e SET */

    function getIdRecebimento() {
        return $this->idRecebimento;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getDataRecebimento() {
        return $this->dataRecebimento;
    }

    function getDescricaoRecebimento() {
        return $this->descricaoRecebimento;
    }
    
	function getRecebemosDe() {
        return $this->recebemosDe;
    }
    
	function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }
    
	function getValorRecebimento() {
        return $this->valorRecebimento;
    }
    
	function getCategoriaRecebimento() {
        return $this->categoriaRecebimento;
    }
    
    function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function getIdRefDocumentoFirebase() {
        return $this->idRefDocumentoFirebase;
    }


    function setIdRecebimento($idRecebimento) {
        $this->idRecebimento = $idRecebimento;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }

	function setDataRecebimento($dataRecebimento) {
        $this->dataRecebimento = $dataRecebimento;
    }
    
	function setDescricaoRecebimento($descricaoRecebimento) {
        $this->descricaoRecebimento = $descricaoRecebimento;
    }

    function setRecebemosDe($recebemosDe) {
        $this->recebemosDe = $recebemosDe;
    }
    
    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }
    
    function setValorRecebimento($valorRecebimento) {
        $this->valorRecebimento = $valorRecebimento;
    }
    
	function setCategoriaRecebimento($categoriaRecebimento) {
        $this->categoriaRecebimento = $categoriaRecebimento;
    }
    
    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    function setIdRefDocumentoFirebase($idRefDocumentoFirebase) {
        $this->idRefDocumentoFirebase = $idRefDocumentoFirebase;
    }

    public function cadastraRecebimento() {
        
        $sql = "INSERT INTO recebimento 
                                    (`fk_idOrganismoAfiliado`, 
                                    `dataRecebimento`, 
                                    `descricaoRecebimento`,
                                    `recebemosDe`,
                                    `codigoAfiliacao`,
                                    `valorRecebimento`,
                                    `categoriaRecebimento`,
                                    `usuario`,
                                    `dataCadastro`,
                                    `idRefDocumentoFirebase`
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :dataRecebimento,
                                            :descricao,
                                            :recebemosDe,
                                            :codigoAfiliacao,
                                            :valor,
                                            :categoria,
                                            :usuario,
                                            now(),
                                            :idRefDocumentoFirebase
                                            )";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataRecebimento', $this->dataRecebimento, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoRecebimento, PDO::PARAM_STR);
        $sth->bindParam(':recebemosDe',$this->recebemosDe, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao',$this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':valor', $this->valorRecebimento, PDO::PARAM_STR);
        $sth->bindParam(':categoria', $this->categoriaRecebimento, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        $sth->bindParam(':idRefDocumentoFirebase', $this->idRefDocumentoFirebase, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraRecebimento($id) {

        $sql = "UPDATE recebimento SET 
        					 `dataRecebimento` = :dataRecebimento,
                                                 `descricaoRecebimento` = :descricao,
                                                 `recebemosDe` = :recebemosDe,
                                                 `codigoAfiliacao` = :codigoAfiliacao,
                                                 `valorRecebimento` = :valor,
                                                 `categoriaRecebimento` = :categoria,
                                                 `ultimoAtualizar` = :ultimoAtualizar,
                                                 `idRefDocumentoFirebase` = :idRefDocumentoFirebase   
                            WHERE `idRecebimento` = :idRecebimento";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':dataRecebimento', $this->dataRecebimento, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoRecebimento , PDO::PARAM_STR);
        $sth->bindParam(':recebemosDe',$this->recebemosDe , PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao',$this->codigoAfiliacao , PDO::PARAM_INT);
        $sth->bindParam(':valor', $this->valorRecebimento , PDO::PARAM_STR);
        $sth->bindParam(':categoria', $this->categoriaRecebimento, PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar', $this->usuario, PDO::PARAM_INT);
        $sth->bindParam(':idRecebimento', $id, PDO::PARAM_INT);
        $sth->bindParam(':idRefDocumentoFirebase', $this->idRefDocumentoFirebase, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaIdRefDocumentoFirebase($id,$idRefDocumentoFirebase) {

        $sql = "UPDATE recebimento SET 
                                       `idRefDocumentoFirebase` = :idRefDocumentoFirebase   
                            WHERE `idRecebimento` = :idRecebimento";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idRecebimento', $id, PDO::PARAM_INT);
        $sth->bindParam(':idRefDocumentoFirebase', $idRefDocumentoFirebase, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'recebimento'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM recebimento ORDER BY idRecebimento DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRecebimento($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null,$ultimoRegistro=null) {
        
    	$sql = "SELECT * FROM recebimento where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataRecebimento)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataRecebimento)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	if($ultimoRegistro!=null)
        {
            $sql .= " order by dataRecebimento DESC limit 0,1";
        }else {
            $sql .= " order by dataRecebimento ASC";
        }

    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaEntrada($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null,$categoriaRecebimento=null,$diaAtual=null,$idIgnorar=null) {
        
    	$sql = "SELECT SUM(REPLACE( REPLACE(valorRecebimento, '.' ,'' ), ',', '.' )) as total FROM recebimento where 1=1 ";
        
	if($diaAtual!=null)
    	{
    		$sql .= " and DAY(dataRecebimento)=:diaAtual";
    	}
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataRecebimento)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataRecebimento)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
	    if($categoriaRecebimento!=null)
    	{
    		$sql .= " and categoriaRecebimento=:categoria";
    	}
        if($idIgnorar!=null)
        {
            $sql .= " and idRecebimento<>:idIgnorar";
        }

    	$sql .= " order by dataRecebimento ASC";
    	//echo "<br>".$sql;
       
        $total=0;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($diaAtual!=null)
    	{
            $sth->bindParam(':diaAtual', $diaAtual, PDO::PARAM_INT);
        }
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($categoriaRecebimento!=null)
    	{
            $sth->bindParam(':categoria', $categoriaRecebimento, PDO::PARAM_INT);
        }
        if($idIgnorar!=null)
        {
            $sth->bindParam(':idIgnorar', $idIgnorar, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
        	foreach($resultado as $vetor)
        	{
        		$total = floatval($vetor['total']);
        	}
            return $total;
        } else {
            return false;
        }
    }

    public function removeRecebimento($idRecebimento) 
    {
        
        $sql = "DELETE FROM recebimento WHERE `idRecebimento` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRecebimento, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdRecebimento($idRecebimento) 
    {
        
        $sql = "SELECT * FROM recebimento 
                                 WHERE `idRecebimento` = :idRecebimento";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idRecebimento', $idRecebimento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function validaRecebimentos($dataRecebimento,$fk_idOrganismoAfiliado, $recebemosDe, $valorRecebimento) {

        $sql = "SELECT * FROM recebimento where 1=1 ";
            $sql .= " and dataRecebimento=:dataRecebimento";
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
            $sql .= " and recebemosDe=:recebemosDe";
            $sql .= " and valorRecebimento=:valorRecebimento";
        $sql .= " order by dataRecebimento ASC";
        //echo "<br>".$sql;

        $total=0;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':dataRecebimento', $dataRecebimento, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':recebemosDe', $recebemosDe, PDO::PARAM_STR);
        $sth->bindParam(':valorRecebimento', $valorRecebimento, PDO::PARAM_STR);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return true;
            }

        }
        return false;
    }

    public function dataCriacao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM recebimento where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataRecebimento)=:mes";
        $sql .= " and YEAR(dataRecebimento)=:ano";
        $sql .= " order by dataCadastro ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function dataUltimaAlteracao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM recebimento where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataRecebimento)=:mes";
        $sql .= " and YEAR(dataRecebimento)=:ano";
        $sql .= " order by dataCadastro DESC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function responsaveis($mes,$ano, $idOrganismoAfiliado,$ultimaAlteracao=null) {

        $sql = "SELECT * FROM recebimento  as r  left join usuario on seqCadast = ultimoAtualizar where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataRecebimento)=:mes";
        $sql .= " and YEAR(dataRecebimento)=:ano and seqCadast <>0 order by r.dataCadastro";

        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $arr=array();

        $i=0;
        if($resultado)
        {
            foreach($resultado as $v)
            {
                $arr['data'][$i] = $v['dataCadastro'];
                $arr['seq'][$i] = $v['seqCadast'];
                $arr['nome'][$i] = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
                $i++;
            }

        }
        return $arr;
    }

    public function getTotalRegistros($fk_idOrganismoAfiliado) {

        $sql = "SELECT COUNT(idRecebimento) AS total FROM recebimento WHERE fk_idOrganismoAfiliado=:id";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $total=0;

        if ($resultado){
            foreach ($resultado as $v)
            {
                $total = $v['total'];
            }
            return $total;
        }else{
            return false;
        }
    }

    public function totalRecebimentosAteMesAnterior($fk_idOrganismoAfiliado, $dataInicial, $ano, $mes) {

        $sql = "SELECT SUM(CAST(REPLACE(REPLACE(valorRecebimento, '.', ''), ',', '.') AS DOUBLE)) FROM recebimento where 1=1";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and Date(dataRecebimento) >= :dataInicial";
        $sql .= " and Date(dataRecebimento) <= :dataFinal";

        $ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));
        $dataFinal = $ano . "-" . $mes . "-" . $ultimo_dia;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $dataFinal, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        return $resultado[0][0];
    }

}
?>