<?php

require_once ("conexaoClass.php");

class estatutoAssinado{

    private $idEstatutoAssinado;
    private $fk_idEstatuto;
    private $caminho;

    //METODO SET/GET -----------------------------------------------------------

    function getIdEstatutoAssinado() {
        return $this->idEstatutoAssinado;
    }

    function getFk_idEstatuto() {
        return $this->fk_idEstatuto;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdEstatutoAssinado($idEstatutoAssinado) {
        $this->idEstatutoAssinado = $idEstatutoAssinado;
    }

    function setFk_idEstatuto($fk_idEstatuto) {
        $this->fk_idEstatuto = $fk_idEstatuto;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    
    //--------------------------------------------------------------------------

    public function cadastroEstatutoAssinado(){

        $sql = "INSERT INTO estatutoAssinado
                                 (
                                  fk_idEstatuto,
                                  caminho,
                                  dataCadastro
                                  )
                            VALUES (:fk_idEstatuto,
                                    :caminho,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idEstatuto', $this->fk_idEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

	public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE 'estatutoAssinado'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function listaEstatutoAssinado($id=null,$mes=null,$ano=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM estatutoAssinado inner join estatuto on fk_idEstatuto = idEstatuto where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idEstatuto=:id";
    	}
    	if($mes!=null)
    	{
    		$sql .= " and MONTH(dataAssembleiaGeral)=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and YEAR(dataAssembleiaGeral)=:ano";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo "<br><br>".$sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        if($mes!=null)
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        if($ano!=null)
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        if($idOrganismoAfiliado!=null)
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeEstatutoAssinado($idEstatuto){
        
        $sql = "DELETE FROM estatutoAssinado WHERE idEstatutoAssinado=:idEstatutoAssinado";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idEstatutoAssinado', $idEstatuto, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


}

?>