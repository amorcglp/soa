<?php

require_once ("conexaoClass.php");

class isencaoOA{

    private $idIsencaoOa;    
    private $fk_idOrganismoAfiliado;
    private $codigoAfiliacao;
    private $seqCadastMembroOa;
    private $dataInicial;
    private $dataFinal;
    private $motivo;
    private $nomeMembroOa;
    private $usuario;
    private $ultimoAtualizar;

    //METODO SET/GET -----------------------------------------------------------

    function getIdIsencaoOa() {
        return $this->idIsencaoOa;
    }
    
	function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    
	function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }
    
	function getNomeMembroOa() {
        return $this->nomeMembroOa;
    }
    
	function getSeqCadastMembroOa() {
        return $this->seqCadastMembroOa;
    }
    
	function getDataInicial() {
        return $this->dataInicial;
    }
    
	function getDataFinal() {
        return $this->dataFinal;
    }
    
	function getMotivo() {
        return $this->motivo;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }
    
    function setIdIsencaoOa($idIsencaoOa) {
        $this->idIsencaoOa = $idIsencaoOa;
    }
    
	function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }
    
	function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }
    
	function setNomeMembroOa($nomeMembroOa) {
        $this->nomeMembroOa = $nomeMembroOa;
    }
    
	function setSeqCadastMembroOa($seqCadastMembroOa) {
        $this->seqCadastMembroOa = $seqCadastMembroOa;
    }
    
	function setDataInicial($dataInicial) {
        $this->dataInicial = $dataInicial;
    }
    
	function setDataFinal($dataFinal) {
        $this->dataFinal = $dataFinal;
    }
    
	function setMotivo($motivo) {
        $this->motivo = $motivo;
    }
    
    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }
    
    //--------------------------------------------------------------------------

    public function cadastroIsencaoOa(){

        $sql = "INSERT INTO isencaoOA  
                                 (fk_idOrganismoAfiliado,
                                  codigoAfiliacao,
                                  nomeMembroOa,
                                  seqCadastMembroOa,
                                  dataInicial,
                                  dataFinal,
                                  motivo,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                            		:idOrganismoAfiliado,
                            		:codigoAfiliacao,
                            		:nome,
                            		:seqCadast,
                            		:dataInicial,
                            		:dataFinal,
                            		:motivo,
                            		:usuario,
                                    now()
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nomeMembroOa, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $this->dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal',  $this->dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':motivo',  $this->motivo, PDO::PARAM_STR);
        $sth->bindParam(':usuario',  $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraIsencaoOa($id){
    	
        $sql = "UPDATE isencaoOA SET  
                          codigoAfiliacao =  :codigoAfiliacao,
                          nomeMembroOa =  :nome,
                          seqCadastMembroOa = :seqCadast,
                          dataInicial = :dataInicial,
                          dataFinal = :dataFinal,
                          motivo = :motivo,
                          ultimoAtualizar = :usuario    
                          WHERE idIsencaoOa = :id";
        
	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nomeMembroOa, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $this->dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal',  $this->dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':motivo',  $this->motivo, PDO::PARAM_STR);
        $sth->bindParam(':usuario',  $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }	
    
    public function atualizaOrganismoIsencaoOa(){

        $sql="UPDATE isencaoOA SET  
                                  fk_idOrganismoAfiliado=:idOrganismoAfiliado
                                  WHERE seqCadastMembroOa=:seqCadast
                                  ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado',  $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadastMembroOa, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idIsencaoOa) as lastid FROM isencaoOA";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	
    
    public function listaIsencaoOA($idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM isencaoOA
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1 ";
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
    	$sql .= " order by codigoAfiliacao";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado',  $idOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($seqCadastMembroOa,$dataInicial,$dataFinal,$idOrganismoAfiliado){

    	$sql = "SELECT * FROM isencaoOA 
    			where seqCadastMembroOa=:seqCadast
    			and dataInicial=:dataInicial
    			and dataFinal=:dataFinal
    			and fk_idOrganismoAfiliado=:idOrganismoAfiliado
    			";
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadastMembroOa, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado',  $idOrganismoAfiliado, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaIsencaoOaPeloSeqCadast($seqCadast){

        $sql = "SELECT codigoAfiliacao,nomeMembroOa FROM  isencaoOA WHERE   seqCadastMembroOa = :seqCadast";

        //echo $sql."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaIsencaoOaMembroHoje($seqCadast,$idOrganismoAfiliado)
    {
        $sql = "SELECT * FROM  isencaoOA 
        	WHERE   seqCadastMembroOa = :seqCadast
        	and fk_idOrganismoAfiliado = :idOrganismoAfiliado
        	and dataInicial <= :hoje
        	and dataFinal >= :hoje";

        //echo $sql."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $hoje=date("Y-m-d");
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado',  $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':hoje', $hoje, PDO::PARAM_STR);

        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function buscaIsencaoOaPeloId($id){

        $sql = "SELECT * FROM  isencaoOA WHERE   idIsencaoOa = :id";

        //echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeIsencaoOa($id){
		
    	$sql = "DELETE FROM isencaoOA WHERE idIsencaoOa = :id";
        
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
}

?>
