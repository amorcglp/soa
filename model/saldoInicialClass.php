<?php

require_once ("conexaoClass.php");

class saldoInicial{

    private $idSaldoInicial;    
    private $fk_idOrganismoAfiliado;
    private $dataSaldoInicial;
    private $saldoInicial;
    private $usuario;
    private $ultimoAtualizar;

    //METODO SET/GET -----------------------------------------------------------

    function getIdSaldoInicial() {
        return $this->idSaldoInicial;
    }
    
	function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    
	function getDataSaldoInicial() {
        return $this->dataSaldoInicial;
    }
    
	function getSaldoInicial() {
        return $this->saldoInicial;
    }
    
    function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }
    
    function setIdSaldoInicial($idSaldoInicial) {
        $this->idSaldoInicial = $idSaldoInicial;
    }
    
	function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }
    
	function setDataSaldoInicial($dataSaldoInicial) {
        $this->dataSaldoInicial = $dataSaldoInicial;
    }
    
	function setSaldoInicial($saldoInicial) {
        $this->saldoInicial = $saldoInicial;
    }
    
    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }
    
    //--------------------------------------------------------------------------

    public function cadastroSaldoInicial(){

    	$sql = "INSERT INTO saldoInicial  
                                 (fk_idOrganismoAfiliado,
                                  dataSaldoInicial,
                                  saldoInicial,
                                  usuario,
                                  dataCadastro
                                  )
                            VALUES (
                            		:idOrganismoAfiliado,
                            		:data,
                            		:saldo,
                            		:usuario,
                                    now()
                                    )";
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFk_idOrganismoAfiliado      = $this->getFk_idOrganismoAfiliado();
        $getDataSaldoInicial            = $this->getDataSaldoInicial();
        $getSaldoInicial                = $this->getSaldoInicial();
        $getUsuario                     = $this->getUsuario();

        $sth->bindParam(':idOrganismoAfiliado', $getFk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':data', $getDataSaldoInicial, PDO::PARAM_STR);
        $sth->bindParam(':saldo', $getSaldoInicial, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $getUsuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function atualizaSaldoInicial($id){

        $sql="UPDATE saldoInicial SET  
                                  dataSaldoInicial = :data,
                                  saldoInicial = :saldo,
                                  ultimoAtualizar = :ultimoAtualizar    
                                  WHERE idSaldoInicial = :id
                            ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getDataSaldoInicial            = $this->getDataSaldoInicial();
        $getSaldoInicial                = $this->getSaldoInicial();
        $getUltimoAtualizar             = $this->getUltimoAtualizar();

        $sth->bindParam(':data', $getDataSaldoInicial, PDO::PARAM_STR);
        $sth->bindParam(':saldo', $getSaldoInicial, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $getUltimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idSaldoInicial) as lastid FROM saldoInicial";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	
    
    public function listaSaldoInicial($idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
 	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:id";
                $sql .= " order by idSaldoInicial desc limit 0,1";
    	}
        
    	
    	//echo $sql;
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function getMesSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:id";
            $sql .= " order by idSaldoInicial desc limit 0,1";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $mes=0;
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $v)
            {
               $mes = substr($v['dataSaldoInicial'],5,2);
            }
            return $mes;
        }else{
            return false;
        }
    }

    public function getDataCompletaSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:id";
            $sql .= " order by idSaldoInicial desc limit 0,1";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $data=0;
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $v)
            {
                $data = $v['dataSaldoInicial'];
            }
            return $data;
        }else{
            return false;
        }
    }

    public function getUsuarioSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:id";
            $sql .= " order by idSaldoInicial desc limit 0,1";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $usuario=0;
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $v)
            {
                $usuario = $v['usuario'];
            }
            return $usuario;
        }else{
            return false;
        }
    }

    public function getUltimoAtualizarSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:id";
            $sql .= " order by idSaldoInicial desc limit 0,1";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $usuario=0;
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $v)
            {
                $usuario = $v['ultimoAtualizar'];
            }
            return $usuario;
        }else{
            return false;
        }
    }

    public function getAnoSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:id";
            $sql .= " order by idSaldoInicial desc limit 0,1";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $ano=0;
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $v)
            {
                $ano = substr($v['dataSaldoInicial'],0,4);
            }
            return $ano;
        }else{
            return false;
        }
    }

    public function getValorSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:id";
            $sql .= " order by idSaldoInicial desc limit 0,1";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $valor=0;
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $v)
            {
                $valor = floatval(str_replace(",",".",str_replace(".","",$v['saldoInicial'])));
            }
            //echo $valor;
            return $valor;
        }else{
            return false;
        }
    }
    
    public function limparSaldoInicial($idOrganismoAfiliado=null){

    	$sql = "DELETE FROM saldoInicial
    	 where fk_idOrganismoAfiliado=:id";
    	
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function temSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT * FROM saldoInicial
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:id";
            $sql .= " order by idSaldoInicial desc limit 0,1";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if ($c->run($sth,false,false,true)){
            return 1;
        }else{
            return 0;
        }
    }

    public function semSaldoInicial($idOrganismoAfiliado=null){

        $sql = "SELECT case classificacaoOrganismoAfiliado
        when '1' then 'Loja'
        when '2' then 'Pronaos'
        when '3' then 'Capítulo'
        when '4' then 'Heptada'
        when '5' then 'Atrium'
    end as classificacao

, 

case tipoOrganismoAfiliado
        when '1' then 'R+C'
        when '2' then 'TOM'
    end as tipo

, 
 

nomeOrganismoAfiliado, siglaOrganismoAfiliado, emailOrganismoAfiliado,
case statusOrganismoAfiliado
        when '0' then 'Ativo'
        when '1' then 'Inativo'
    end as statusOrganismo
    , o.*, rr.*

FROM `organismoAfiliado` as o 
inner join regiaoRosacruz as rr on fk_idRegiaoOrdemRosacruz = idRegiaoRosacruz
WHERE idOrganismoAfiliado NOT IN (SELECT fk_idOrganismoAfiliado from saldoInicial) 
    	";

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and idOrganismoAfiliado=:id";
        }
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}

?>
