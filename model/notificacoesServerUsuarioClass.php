<?php

require_once ("conexaoClass.php");

class notificacoesServerUsuario{

    private $fk_idNotificacao;
    private $fk_seq_cadast;
    
    //METODO SET/GET -----------------------------------------------------------

    function getFk_idNotificacao() {
        return $this->fk_idNotificacao;
    }
    
	function getFk_seqCadast() {
        return $this->fk_seq_cadast;
    }

	function setFk_idNotificacao($fk_idNotificacao) {
        $this->fk_idNotificacao = $fk_idNotificacao;
    }
    
	function setFk_seqCadast($seqCadast) {
        $this->fk_seq_cadast = $seqCadast;
    }

    //--------------------------------------------------------------------------

    public function cadastro(){

        $sql="INSERT INTO notificacoesServer_usuario  
                                 (
                                  fk_idNotificacao,
                                  fk_seq_cadast
                                  )
                            VALUES (:id,
                                    :seqCadast)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idNotificacao, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->fk_seq_cadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function lista($seq_cadast=null,$idNotificacao=null){

    	$sql = "SELECT * FROM notificacoesServer_usuario
    	where 1=1 
    	 ";   
    	if($seq_cadast!=null)
    	{
    		$sql .= " and fk_seq_cadast=:seqCadast";
    	}
    	if($idNotificacao!=null)
    	{
    		$sql .= " and fk_idNotificacao=:id";
    	}	 	 
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seq_cadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seq_cadast, PDO::PARAM_INT);
        }
        if($idNotificacao!=null)
    	{
            $sth->bindParam(':id', $idNotificacao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaNotificacoesServer(){

    	$sql = "SELECT count(*) as qtd FROM notificacoesServer";   
          
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                $qnt = $vetor['qtd']; 
            }
        }
        return $qnt;
    }
    
    public function listaUltimasNotificacoesServer($seq_cadast){

    	$sql = "SELECT * FROM notificacoesServer "
                . " inner join notificacoesServer_usuario on fk_idNotificacao <> idNotificacao"
                . " where 1=1 ";
        if($seq_cadast!=null)
    	{
    		$sql .= " and fk_seq_cadast=:seqCadast";
    	}
        $sql .=" group by idNotificacao order by idNotificacao desc ";  
    	
        //echo $sql;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seq_cadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seq_cadast, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaVisualizadas($seq_cadast=null){

    	$sql = "SELECT count(*) as qtd FROM notificacoesServer_usuario
            inner join notificacoesServer on fk_idNotificacao = idNotificacao
    	where 1=1 
    	 ";   
    	if($seq_cadast!=null)
    	{
    		$sql .= " and fk_seq_cadast=:seqCadast";
    	}
    	
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seq_cadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seq_cadast, PDO::PARAM_INT);
        }
        
        
        $resultado = $c->run($sth);
        $qnt=0;
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                $qnt = $vetor['qtd']; 
            }
        }
        return $qnt;
    }
    
    public function totalNaoVisualizadas($seq_cadast)
    {
        $totalVisualizadas = $this->listaVisualizadas($seq_cadast);
        $totalNotificacoes = $this->listaNotificacoesServer();
        //echo "->".$totalVisualizadas;
        //echo "->".$totalNotificacoes;
        
        return ($totalNotificacoes-$totalVisualizadas);
    }        

    public function removeOficiais($seq_cadast){
        
        $sql="DELETE FROM notificacoesServer_usuario WHERE fk_seq_cadast = :seqCadast";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seq_cadast, PDO::PARAM_INT);
        $resultado = $c->run($sth,true);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaVisualizacao($seq_cadast=null,$idNotificacao=null){

    	$sql = "SELECT * FROM notificacoesServer_usuario
    	where 1=1 
    	 ";   
    		$sql .= " and fk_seq_cadast=:seqCadast";

    		$sql .= " and fk_idNotificacao=:id";
	 	 
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seq_cadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seq_cadast, PDO::PARAM_INT);
        }
        if($idNotificacao!=null)
    	{
            $sth->bindParam(':id', $idNotificacao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    
}

?>
