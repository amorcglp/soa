<?php

require_once ("conexaoClass.php");

class ataReuniaoMensalOficial{

    private $idAtaReuniaoMensalOficial;
    private $fk_idAtaReuniaoMensal;
    private $fk_seq_cadastMembro;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtaReuniaoMensalOficial() {
        return $this->idAtaReuniaoMensalOficial;
    }

    function getFk_idAtaReuniaoMensal() {
        return $this->fk_idAtaReuniaoMensal;
    }

	function getFk_seqCadastMembro() {
        return $this->fk_seq_cadastMembro;
    }

    function setIdAtaReuniaoMensalOficial($idAtaReuniaoMensalOficial) {
        $this->idAtaReuniaoMensalOficial = $idAtaReuniaoMensalOficial;
    }

	function setFk_idAtaReuniaoMensal($fk_idAtaReuniaoMensal) {
        $this->fk_idAtaReuniaoMensal = $fk_idAtaReuniaoMensal;
    }

	function setFk_seqCadastMembro($seqCadastMembro) {
        $this->fk_seq_cadastMembro = $seqCadastMembro;
    }

    //--------------------------------------------------------------------------

    public function cadastroAtaReuniaoMensalOficial(){

        $sql = "INSERT INTO ataReuniaoMensal_oficial
                                 (
                                  fk_idAtaReuniaoMensal,
                                  fk_seq_cadastMembro
                                  )
                            VALUES (:fk_idAtaReuniaoMensal,
                                    :fk_seq_cadastMembro)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtaReuniaoMensal', $this->fk_idAtaReuniaoMensal, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadastMembro', $this->fk_seq_cadastMembro, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function listaOficiaisAdministrativos($idAtaReuniaoMensal=null,$v=null){

    	$sql = "SELECT * FROM ataReuniaoMensal_oficial where 1=1";

    	if($idAtaReuniaoMensal!=null) {
    		$sql .= " and fk_idAtaReuniaoMensal=:fk_idAtaReuniaoMensal";
    	}
        if($v!=null) {
    		$sql .= " and fk_seq_cadastMembro=:fk_seq_cadastMembro";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idAtaReuniaoMensal!=null){
            $sth->bindParam(':fk_idAtaReuniaoMensal', $idAtaReuniaoMensal, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':fk_seq_cadastMembro', $v, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeOficiais($idAtaReuniaoMensal=null,$id=null){

        $sql= "DELETE FROM ataReuniaoMensal_oficial WHERE 1=1";

    	if($idAtaReuniaoMensal!=null) {
    		$sql .= " and fk_idAtaReuniaoMensal=:fk_idAtaReuniaoMensal";
    	}
    	if($id!=null) {
    		$sql .= " and idAtaReuniaoMensalOficial=:idAtaReuniaoMensalOficial";
    	}

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idAtaReuniaoMensal!=null){
            $sth->bindParam(':fk_idAtaReuniaoMensal', $idAtaReuniaoMensal, PDO::PARAM_INT);
        }
        if($id!=null){
            $sth->bindParam(':idAtaReuniaoMensalOficial', $id, PDO::PARAM_INT);
        }

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>
