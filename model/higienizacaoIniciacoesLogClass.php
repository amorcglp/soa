<?php

require_once ("conexaoClass.php");

class higienizacaoIniciacoesLog {

    public $idHigienizacaoIniciacoesLog;
    public $fk_idAtividadeIniciatica;
    public $seqCadast;
    public $seqTipoObrigacao;
    public $localObrigacao;
    public $dataObrigacao;
    public $regiao;
    public $siglaOA;

    /**
     * @return mixed
     */
    public function getIdHigienizacaoIniciacoesLog()
    {
        return $this->idHigienizacaoIniciacoesLog;
    }

    /**
     * @param mixed $idHigienizacaoIniciacoesLog
     */
    public function setIdHigienizacaoIniciacoesLog($idHigienizacaoIniciacoesLog)
    {
        $this->idHigienizacaoIniciacoesLog = $idHigienizacaoIniciacoesLog;
    }

    /**
     * @return mixed
     */
    public function getFkIdAtividadeIniciatica()
    {
        return $this->fk_idAtividadeIniciatica;
    }

    /**
     * @param mixed $fk_idAtividadeIniciatica
     */
    public function setFkIdAtividadeIniciatica($fk_idAtividadeIniciatica)
    {
        $this->fk_idAtividadeIniciatica = $fk_idAtividadeIniciatica;
    }

    /**
     * @return mixed
     */
    public function getSeqCadast()
    {
        return $this->seqCadast;
    }

    /**
     * @param mixed $seqCadast
     */
    public function setSeqCadast($seqCadast)
    {
        $this->seqCadast = $seqCadast;
    }

    /**
     * @return mixed
     */
    public function getSeqTipoObrigacao()
    {
        return $this->seqTipoObrigacao;
    }

    /**
     * @param mixed $seqTipoObrigacao
     */
    public function setSeqTipoObrigacao($seqTipoObrigacao)
    {
        $this->seqTipoObrigacao = $seqTipoObrigacao;
    }

    /**
     * @return mixed
     */
    public function getLocalObrigacao()
    {
        return $this->localObrigacao;
    }

    /**
     * @param mixed $localObrigacao
     */
    public function setLocalObrigacao($localObrigacao)
    {
        $this->localObrigacao = $localObrigacao;
    }

    /**
     * @return mixed
     */
    public function getDataObrigacao()
    {
        return $this->dataObrigacao;
    }

    /**
     * @param mixed $dataObrigacao
     */
    public function setDataObrigacao($dataObrigacao)
    {
        $this->dataObrigacao = $dataObrigacao;
    }

    /**
     * @return mixed
     */
    public function getRegiao()
    {
        return $this->regiao;
    }

    /**
     * @param mixed $regiao
     */
    public function setRegiao($regiao)
    {
        $this->regiao = $regiao;
    }

    /**
     * @return mixed
     */
    public function getSiglaOA()
    {
        return $this->siglaOA;
    }

    /**
     * @param mixed $siglaOA
     */
    public function setSiglaOA($siglaOA)
    {
        $this->siglaOA = $siglaOA;
    }



    public function cadastraHigienizacaoIniciacoesLog() {
        
        $sql = "INSERT INTO higienizacaoIniciacoesLog 
                                  (
                                  fk_idAtividadeIniciatica,
                                  seqCadast,
                                  seqTipoObrigacao,
                                  localObrigacao,
                                  dataObrigacao,
                                  regiao, 
                                  siglaOA, 
                                  dataCadastro)
                                    VALUES (
                                            :fk_idAtividadeIniciatica,
                                            :seqCadast,
                                            :seqTipoObrigacao,
                                            :localObrigacao,
                                            :dataObrigacao,
                                            :regiao,
                                            :siglaOA,
                                            now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $this->fk_idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':seqTipoObrigacao', $this->seqTipoObrigacao, PDO::PARAM_INT);
        $sth->bindParam(':localObrigacao', $this->localObrigacao, PDO::PARAM_STR);
        $sth->bindParam(':dataObrigacao', $this->dataObrigacao, PDO::PARAM_STR);
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_STR);
        $sth->bindParam(':siglaOA', $this->siglaOA, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    
    public function listaHigienizacaoIniciacoesLog() {

        $sql =" SELECT * FROM higienizacaoIniciacoesLog as b "
               . " inner join organismoAfiliado as o on o.siglaOrganismoAfiliado = b.siglaOA "
               . " where 1=1 ";
        $sql .=" ORDER BY o.siglaOrganismoAfiliado";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaHigienizacaoIniciacoesLogHoje() {

        $sql =" SELECT * FROM higienizacaoIniciacoesLog as b "
            . " inner join organismoAfiliado as o on o.siglaOrganismoAfiliado = b.siglaOA "
            . " where 1=1 and DATE(dataCadastro)='".date('Y-m-d')."'";

        $sql .=" ORDER BY o.siglaOrganismoAfiliado";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function limpaTabelaLog() {
        
        $sql = "TRUNCATE higienizacaoIniciacoesLog";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

}

?>