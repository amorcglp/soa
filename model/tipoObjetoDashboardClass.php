<?php

require_once ("conexaoClass.php");

class tipoObjetoDashboard {

    public $idTipoObjetoDashboard;
    public $tipoObjetoDashboard;
    public $porLinhaObjetoDashboard;
    public $descricaoTipoObjetoDashboard;
    public $statusTipoObjetoDashboard;

    //Getter e Setter
    function getIdTipoObjetoDashboard() {
        return $this->idTipoObjetoDashboard;
    }

	function getTipoObjetoDashboard() {
        return $this->tipoObjetoDashboard;
    }
    
	function getPorLinhaObjetoDashboard() {
        return $this->porLinhaObjetoDashboard;
    }
    
	function getDescricaoTipoObjetoDashboard() {
        return $this->descricaoTipoObjetoDashboard;
    }
    
	function getStatusTipoObjetoDashboard() {
        return $this->statusTipoObjetoDashboard;
    }
    
    function setIdTipoObjetoDashboard($idTipoObjetoDashboard) {
        $this->idTipoObjetoDashboard = $idTipoObjetoDashboard;
    }

	function setTipoObjetoDashboard($tipoObjetoDashboard) {
        $this->tipoObjetoDashboard = $tipoObjetoDashboard;
    }
    
	function setPorLinhaObjetoDashboard($porLinhaObjetoDashboard) {
        $this->porLinhaObjetoDashboard = $porLinhaObjetoDashboard;
    }
    
    function setDescricaoTipoObjetoDashboard($descricaoTipoObjetoDashboard) {
        $this->descricaoTipoObjetoDashboard = $descricaoTipoObjetoDashboard;
    }
    
	function setStatusTipoObjetoDashboard($statusTipoObjetoDashboard) {
        $this->statusTipoObjetoDashboard = $statusTipoObjetoDashboard;
    }

    public function cadastraTipoObjetoDashboard() {
        
        $sql = "INSERT INTO tipoObjetoDashboard (idTipoObjetoDashboard, 
                        tipoObjetoDashboard, porLinhaObjetoDashboard, descricaoTipoObjetoDashboard)
                                    VALUES (:id,
                                            :tipo,
                                            :porLinha,
                                            :descricao
                                            )";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getIdTipoObjetoDashboard           = $this->getIdTipoObjetoDashboard();
        $getTipoObjetoDashboard             = $this->getTipoObjetoDashboard();
        $getPorLinhaObjetoDashboard         = $this->getPorLinhaObjetoDashboard();
        $getDescricaoTipoObjetoDashboard    = $this->getDescricaoTipoObjetoDashboard();

        $sth->bindParam(':id', $getIdTipoObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':tipo', $getTipoObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':porLinha', $getPorLinhaObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $getDescricaoTipoObjetoDashboard, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function alteraTipoObjetoDashboard($id) {
        
        $sql = "UPDATE tipoObjetoDashboard 
                            SET 
                                `tipoObjetoDashboard` = :tipo,
                                `porLinhaObjetoDashboard` = :porLinha,
                                `descricaoTipoObjetoDashboard` = :descricao
                            WHERE idTipoObjetoDashboard = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getTipoObjetoDashboard             = $this->getTipoObjetoDashboard();
        $getPorLinhaObjetoDashboard         = $this->getPorLinhaObjetoDashboard();
        $getDescricaoTipoObjetoDashboard    = $this->getDescricaoTipoObjetoDashboard();

        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':tipo', $getTipoObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':porLinha', $getPorLinhaObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $getDescricaoTipoObjetoDashboard, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function alteraStatusTipoObjetoDashboard() {
        
        $sql = "UPDATE tipoObjetoDashboard SET `statusTipoObjetoDashboard` = :status"
                . "                WHERE `idTipoObjetoDashboard` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getStatusTipoObjetoDashboard       = $this->getStatusTipoObjetoDashboard();
        $getIdTipoObjetoDashboard           = $this->getIdTipoObjetoDashboard();

        $sth->bindParam(':status', $getStatusTipoObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':id', $getIdTipoObjetoDashboard, PDO::PARAM_INT);
		
        if ($c->run($sth,true)) {
            $arr = array();
            $arr['status'] = $this->getStatusTipoObjetoDashboard();
            return $arr;
        } else {
            return false;
        }
    }

    public function listaTipoObjetoDashboard($ativo=null) {

    	$sql = "SELECT * FROM tipoObjetoDashboard where 1=1 ";
    	if($ativo==1)
    	{
    		$sql .= " and statusTipoObjetoDashboard = 0 ";
    	}
        $sql .= " ORDER BY tipoObjetoDashboard";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeTipoObjetoDashboard($idTipoObjetoDashboard) {
        
        $sql = "DELETE FROM tipoObjetoDashboard WHERE idTipoObjetoDashboard = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idTipoObjetoDashboard, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function buscaTipoObjetoDashboard() {
        
        $sql = "SELECT * FROM tipoObjetoDashboard
                                 WHERE `tipoObjetoDashboard` LIKE :tipo
                                 ORDER BY tipoObjetoDashboard ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getTipoObjetoDashboard = '%'.$this->getTipoObjetoDashboard().'%';

        $sth->bindParam(':tipo', $getTipoObjetoDashboard, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdTipoObjetoDashboard() {
        
        $sql = "SELECT * FROM tipoObjetoDashboard 
                              WHERE idTipoObjetoDashboard = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {
            $id = $_GET['id'];
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

}

?>