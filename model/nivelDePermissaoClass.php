<?php

require_once ("conexaoClass.php");

class nivelDePermissao {

    public $idNivelDePermissao;
    public $nivelDePermissao;
    public $descricaoNivelDePermissao;
    public $statusNivelDePermissao;

    //Getter e Setter
    function getIdNivelDePermissao() {
        return $this->idNivelDePermissao;
    }

	function getNivelDePermissao() {
        return $this->nivelDePermissao;
    }
    
	function getDescricaoNivelDePermissao() {
        return $this->descricaoNivelDePermissao;
    }
    
	function getStatusNivelDePermissao() {
        return $this->statusNivelDePermissao;
    }
    
    function setIdNivelDePermissao($idNivelDePermissao) {
        $this->idNivelDePermissao = $idNivelDePermissao;
    }

	function setNivelDePermissao($nivelDePermissao) {
        $this->nivelDePermissao = $nivelDePermissao;
    }
    
    function setDescricaoNivelDePermissao($descricaoNivelDePermissao) {
        $this->descricaoNivelDePermissao = $descricaoNivelDePermissao;
    }
    
	function setStatusNivelDePermissao($statusNivelDePermissao) {
        $this->statusNivelDePermissao = $statusNivelDePermissao;
    }

    public function cadastraNivelDePermissao() {
        
        $sql = "INSERT INTO nivelDePermissao (idNivelDePermissao, nivelDePermissao, descricaoNivelDePermissao)
                                    VALUES (:id,
                                            :nivel,
                                            :descricao
                                            )";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idNivelDePermissao, PDO::PARAM_INT);
        $sth->bindParam(':nivel', $this->nivelDePermissao, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoNivelDePermissao, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraNivelDePermissao($id) 
    {
        
        $sql = "UPDATE nivelDePermissao 
                            SET 
                                `nivelDePermissao` = :nivel,
                                `descricaoNivelDePermissao` = :descricao
                            WHERE idNivelDePermissao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
               
        $sth->bindParam(':nivel', $this->nivelDePermissao, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoNivelDePermissao, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusNivelDePermissao() {
        
        $sql = "UPDATE nivelDePermissao SET `statusNivelDePermissao` = :status"
                . "                WHERE `idNivelDePermissao` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
              
        $status = $this->getStatusNivelDePermissao();
        $id = $this->getIdNivelDePermissao();
        
        $sth->bindParam(':status', $status , PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status']=$status;
            return $arr;
        }else{
            return false;
        }
    }

    public function listaNivelDePermissao() {

        $sql = "SELECT * FROM nivelDePermissao
                              ORDER BY nivelDePermissao";


        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeNivelDePermissao($idNivelDePermissao) {
        
        $sql = "DELETE FROM nivelDePermissao WHERE idNivelDePermissao = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idNivelDePermissao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaNivelDePermissao() {
        
        $sql = "SELECT * FROM nivelDePermissao
                                 WHERE `nivelDePermissao` LIKE :nivel
                                 ORDER BY nivelDePermissao ASC";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $nivel = '%'.$this->getNivelDePermissao().'%';
        $sth->bindParam(':nivel', $nivel , PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdNivelDePermissao() {
        
        $sql = "SELECT * FROM nivelDePermissao 
                              WHERE idNivelDePermissao = :nivel";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {
            $id = $_GET['id'];
            $sth->bindParam(':nivel', $id, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

}

?>