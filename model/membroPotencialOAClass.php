<?php

require_once ("conexaoClass.php");

class membroPotencialOA {

    private $idMembroPotencialOA;
    private $fk_idOrganismoAfiliado;
    private $regiao;
    private $siglaOA;
    private $seqCadastMembroOa;
    private $fIdeTipoPessoa;
    private $fNomCliente;
    private $fNomConjuge;
    private $fIdeSexo;
    private $fDatNascimento;
    private $fCodCpf;
    private $fCodRg;
    private $fDesOrgaoEmissao;
    private $fCodCnpj;
    private $fCodInscricaoEstadual;
    private $fCodEstadoCivil;
    private $fCodGrauInstrucao;
    private $fSeqOcupacao;
    private $fSeqEspecializacao;
    private $fSeqFormacao;
    private $fCodCliente;
    private $fCodNaoMembro;
    private $fCodRosacruz;
    private $fCodOgg;
    private $fCodAma;
    private $fCodAssinanteRosacruz;
    private $fCodAssinanteAmorcCultural;
    private $fCodAssinanteOrdemJuvenil;
    private $fCodArtista;
    private $fCodPalestrante;
    private $fCodLeitor;
    private $fSigOA;
    private $fIdeTipoOa;
    private $fIdeTipoTom;
    private $fNomLogradouro;
    private $fNumEndereco;
    private $fDesComplementoEndereco;
    private $fNomBairro;
    private $fNomCidade;
    private $fSigUf;
    private $fCodCepEndereco;
    private $fSigPaisEndereco;
    private $fNumCaixaPostal;
    private $fCodCepCaixaPostal;
    private $fIdeEnderecoCorrespondencia;
    private $fNumTelefone;
    private $fNumFax;
    private $fNumTelefoFixo;
    private $fNumCelula;
    private $fNumOutroTelefo;
    private $fDesOutroTelefo;
    private $fDesEmail;
    private $fDatImplantacaoCadastro;
    private $fDatCadastro;
    private $fCodUsuarioCadastro;
    private $fIdeTipoFiliacRosacruz;
    private $fSeqCadastPrincipalRosacruz;
    private $fSeqCadastCompanheiroRosacruz;
    private $fIdeTipoFiliacTom;
    private $fSeqCadastPrincipalTom;
    private $fSeqCadastCompanheiroTom;
    private $fIdeContribuicaoCarne;
    private $fCodRosacruzCompanheiro;
    private $fIdeTipoMoeda;
    private $fIdeModoFaturamento;
    private $fIdeDependenciaAdministrativa;
    private $fDesObservacao;
    private $fIdeTipoSituacMembroRosacr;
    private $fIdeTipoSituacMembroOjp;
    private $fIdeTipoSituacMembroTom;
    private $fIdeTipoSituacMembroAma;
    private $fIdeTipoSituacRemessRosacr;
    private $fIdeTipoSituacRemessOjp;
    private $fIdeTipoSituacRemessTom;
    private $fIdeTipoSituacRemessAma;
    private $fDatQuitacRosacr;
    private $fDatQuitacOjp;
    private $fDatQuitacTom;
    private $fDatQuitacAma;
    private $fNumLoteAtualRosacr;
    private $fNumLoteAtualOjp;
    private $fNumLoteAtualTom;
    private $fNumLoteAtualAma;
    private $fNumLoteLimiteRosacr;
    private $fNumLoteLimiteTom;
    private $fSeqCadastOaAtuacao;
    private $fSigOaAtuacao;
    private $fDatAdmissRosacr;


    //METODO SET/GET -----------------------------------------------------------
    function getFSeqOcupacao() {
        return $this->fSeqOcupacao;
    }

    function getFSeqEspecializacao() {
        return $this->fSeqEspecializacao;
    }

    function getFSeqFormacao() {
        return $this->fSeqFormacao;
    }

    function setFSeqOcupacao($fSeqOcupacao) {
        $this->fSeqOcupacao = $fSeqOcupacao;
    }

    function setFSeqEspecializacao($fSeqEspecializacao) {
        $this->fSeqEspecializacao = $fSeqEspecializacao;
    }

    function setFSeqFormacao($fSeqFormacao) {
        $this->fSeqFormacao = $fSeqFormacao;
    }

    function getIdMembroPotencialOA() {
        return $this->idMembroPotencialOA;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getRegiao() {
        return $this->regiao;
    }

    function getSiglaOA() {
        return $this->siglaOA;
    }

    function getSeqCadastMembroOa() {
        return $this->seqCadastMembroOa;
    }

    function getFIdeTipoPessoa() {
        return $this->fIdeTipoPessoa;
    }

    function getFNomCliente() {
        return $this->fNomCliente;
    }

    function getFNomConjuge() {
        return $this->fNomConjuge;
    }

    function getFIdeSexo() {
        return $this->fIdeSexo;
    }

    function getFDatNascimento() {
        return $this->fDatNascimento;
    }

    function getFCodCpf() {
        return $this->fCodCpf;
    }

    function getFCodRg() {
        return $this->fCodRg;
    }

    function getFDesOrgaoEmissao() {
        return $this->fDesOrgaoEmissao;
    }

    function getFCodCnpj() {
        return $this->fCodCnpj;
    }

    function getFCodInscricaoEstadual() {
        return $this->fCodInscricaoEstadual;
    }

    function getFCodEstadoCivil() {
        return $this->fCodEstadoCivil;
    }

    function getFCodGrauInstrucao() {
        return $this->fCodGrauInstrucao;
    }

    function getFDesOcupacao() {
        return $this->fDesOcupacao;
    }

    function getFDesEspecializacao() {
        return $this->fDesEspecializacao;
    }

    function getFDesFormacao() {
        return $this->fDesFormacao;
    }

    function getFCodCliente() {
        return $this->fCodCliente;
    }

    function getFCodNaoMembro() {
        return $this->fCodNaoMembro;
    }

    function getFCodRosacruz() {
        return $this->fCodRosacruz;
    }

    function getFCodOgg() {
        return $this->fCodOgg;
    }

    function getFCodAma() {
        return $this->fCodAma;
    }

    function getFCodAssinanteRosacruz() {
        return $this->fCodAssinanteRosacruz;
    }

    function getFCodAssinanteAmorcCultural() {
        return $this->fCodAssinanteAmorcCultural;
    }

    function getFCodAssinanteOrdemJuvenil() {
        return $this->fCodAssinanteOrdemJuvenil;
    }

    function getFCodArtista() {
        return $this->fCodArtista;
    }

    function getFCodPalestrante() {
        return $this->fCodPalestrante;
    }

    function getFCodLeitor() {
        return $this->fCodLeitor;
    }

    function getFSigOA() {
        return $this->fSigOA;
    }

    function getFIdeTipoOa() {
        return $this->fIdeTipoOa;
    }

    function getFIdeTipoTom() {
        return $this->fIdeTipoTom;
    }

    function getFNomLogradouro() {
        return $this->fNomLogradouro;
    }

    function getFNumEndereco() {
        return $this->fNumEndereco;
    }

    function getFDesComplementoEndereco() {
        return $this->fDesComplementoEndereco;
    }

    function getFNomBairro() {
        return $this->fNomBairro;
    }

    function getFNomCidade() {
        return $this->fNomCidade;
    }

    function getFSigUf() {
        return $this->fSigUf;
    }

    function getFCodCepEndereco() {
        return $this->fCodCepEndereco;
    }

    function getFSigPaisEndereco() {
        return $this->fSigPaisEndereco;
    }

    function getFNumCaixaPostal() {
        return $this->fNumCaixaPostal;
    }

    function getFCodCepCaixaPostal() {
        return $this->fCodCepCaixaPostal;
    }

    function getFIdeEnderecoCorrespondencia() {
        return $this->fIdeEnderecoCorrespondencia;
    }

    function getFNumTelefone() {
        return $this->fNumTelefone;
    }

    function getFNumFax() {
        return $this->fNumFax;
    }

    function getFNumTelefoFixo() {
        return $this->fNumTelefoFixo;
    }

    function getFNumCelula() {
        return $this->fNumCelula;
    }

    function getFNumOutroTelefo() {
        return $this->fNumOutroTelefo;
    }

    function getFDesOutroTelefo() {
        return $this->fDesOutroTelefo;
    }

    function getFDesEmail() {
        return $this->fDesEmail;
    }

    function getFDatImplantacaoCadastro() {
        return $this->fDatImplantacaoCadastro;
    }

    function getFDatCadastro() {
        return $this->fDatCadastro;
    }

    function getFCodUsuarioCadastro() {
        return $this->fCodUsuarioCadastro;
    }

    function getFIdeTipoFiliacRosacruz() {
        return $this->fIdeTipoFiliacRosacruz;
    }

    function getFSeqCadastPrincipalRosacruz() {
        return $this->fSeqCadastPrincipalRosacruz;
    }

    function getFSeqCadastCompanheiroRosacruz() {
        return $this->fSeqCadastCompanheiroRosacruz;
    }

    function getFIdeTipoFiliacTom() {
        return $this->fIdeTipoFiliacTom;
    }

    function getFSeqCadastPrincipalTom() {
        return $this->fSeqCadastPrincipalTom;
    }

    function getFSeqCadastCompanheiroTom() {
        return $this->fSeqCadastCompanheiroTom;
    }

    function getFIdeContribuicaoCarne() {
        return $this->fIdeContribuicaoCarne;
    }

    function getFCodRosacruzCompanheiro() {
        return $this->fCodRosacruzCompanheiro;
    }

    function getFIdeTipoMoeda() {
        return $this->fIdeTipoMoeda;
    }

    function getFIdeModoFaturamento() {
        return $this->fIdeModoFaturamento;
    }

    function getFIdeDependenciaAdministrativa() {
        return $this->fIdeDependenciaAdministrativa;
    }

    function getFDesObservacao() {
        return $this->fDesObservacao;
    }

    function getFIdeTipoSituacMembroRosacr() {
        return $this->fIdeTipoSituacMembroRosacr;
    }

    function getFIdeTipoSituacMembroOjp() {
        return $this->fIdeTipoSituacMembroOjp;
    }

    function getFIdeTipoSituacMembroTom() {
        return $this->fIdeTipoSituacMembroTom;
    }

    function getFIdeTipoSituacMembroAma() {
        return $this->fIdeTipoSituacMembroAma;
    }

    function getFIdeTipoSituacRemessRosacr() {
        return $this->fIdeTipoSituacRemessRosacr;
    }

    function getFIdeTipoSituacRemessOjp() {
        return $this->fIdeTipoSituacRemessOjp;
    }

    function getFIdeTipoSituacRemessTom() {
        return $this->fIdeTipoSituacRemessTom;
    }

    function getFIdeTipoSituacRemessAma() {
        return $this->fIdeTipoSituacRemessAma;
    }

    function getFDatQuitacRosacr() {
        return $this->fDatQuitacRosacr;
    }

    function getFDatQuitacOjp() {
        return $this->fDatQuitacOjp;
    }

    function getFDatQuitacTom() {
        return $this->fDatQuitacTom;
    }

    function getFDatQuitacAma() {
        return $this->fDatQuitacAma;
    }

    function getFNumLoteAtualRosacr() {
        return $this->fNumLoteAtualRosacr;
    }

    function getFNumLoteAtualOjp() {
        return $this->fNumLoteAtualOjp;
    }

    function getFNumLoteAtualTom() {
        return $this->fNumLoteAtualTom;
    }

    function getFNumLoteAtualAma() {
        return $this->fNumLoteAtualAma;
    }

    function getFNumLoteLimiteRosacr() {
        return $this->fNumLoteLimiteRosacr;
    }

    function getFNumLoteLimiteTom() {
        return $this->fNumLoteLimiteTom;
    }

    function getFSeqCadastOaAtuacao() {
        return $this->fSeqCadastOaAtuacao;
    }

    function getFSigOaAtuacao() {
        return $this->fSigOaAtuacao;
    }
    
    function getFDatAdmissRosacr() {
        return $this->fDatAdmissRosacr;
    }

    function setIdMembroPotencialOA($idMembroPotencialOA) {
        $this->idMembroPotencialOA = $idMembroPotencialOA;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setRegiao($regiao) {
        $this->regiao = $regiao;
    }

    function setSiglaOA($siglaOA) {
        $this->siglaOA = $siglaOA;
    }

    function setSeqCadastMembroOa($seqCadastMembroOa) {
        $this->seqCadastMembroOa = $seqCadastMembroOa;
    }

    function setFIdeTipoPessoa($fIdeTipoPessoa) {
        $this->fIdeTipoPessoa = $fIdeTipoPessoa;
    }

    function setFNomCliente($fNomCliente) {
        $this->fNomCliente = $fNomCliente;
    }

    function setFNomConjuge($fNomConjuge) {
        $this->fNomConjuge = $fNomConjuge;
    }

    function setFIdeSexo($fIdeSexo) {
        $this->fIdeSexo = $fIdeSexo;
    }

    function setFDatNascimento($fDatNascimento) {
        $this->fDatNascimento = $fDatNascimento;
    }

    function setFCodCpf($fCodCpf) {
        $this->fCodCpf = $fCodCpf;
    }

    function setFCodRg($fCodRg) {
        $this->fCodRg = $fCodRg;
    }

    function setFDesOrgaoEmissao($fDesOrgaoEmissao) {
        $this->fDesOrgaoEmissao = $fDesOrgaoEmissao;
    }

    function setFCodCnpj($fCodCnpj) {
        $this->fCodCnpj = $fCodCnpj;
    }

    function setFCodInscricaoEstadual($fCodInscricaoEstadual) {
        $this->fCodInscricaoEstadual = $fCodInscricaoEstadual;
    }

    function setFCodEstadoCivil($fCodEstadoCivil) {
        $this->fCodEstadoCivil = $fCodEstadoCivil;
    }

    function setFCodGrauInstrucao($fCodGrauInstrucao) {
        $this->fCodGrauInstrucao = $fCodGrauInstrucao;
    }

    function setFDesOcupacao($fDesOcupacao) {
        $this->fDesOcupacao = $fDesOcupacao;
    }

    function setFDesEspecializacao($fDesEspecializacao) {
        $this->fDesEspecializacao = $fDesEspecializacao;
    }

    function setFDesFormacao($fDesFormacao) {
        $this->fDesFormacao = $fDesFormacao;
    }

    function setFCodCliente($fCodCliente) {
        $this->fCodCliente = $fCodCliente;
    }

    function setFCodNaoMembro($fCodNaoMembro) {
        $this->fCodNaoMembro = $fCodNaoMembro;
    }

    function setFCodRosacruz($fCodRosacruz) {
        $this->fCodRosacruz = $fCodRosacruz;
    }

    function setFCodOgg($fCodOgg) {
        $this->fCodOgg = $fCodOgg;
    }

    function setFCodAma($fCodAma) {
        $this->fCodAma = $fCodAma;
    }

    function setFCodAssinanteRosacruz($fCodAssinanteRosacruz) {
        $this->fCodAssinanteRosacruz = $fCodAssinanteRosacruz;
    }

    function setFCodAssinanteAmorcCultural($fCodAssinanteAmorcCultural) {
        $this->fCodAssinanteAmorcCultural = $fCodAssinanteAmorcCultural;
    }

    function setFCodAssinanteOrdemJuvenil($fCodAssinanteOrdemJuvenil) {
        $this->fCodAssinanteOrdemJuvenil = $fCodAssinanteOrdemJuvenil;
    }

    function setFCodArtista($fCodArtista) {
        $this->fCodArtista = $fCodArtista;
    }

    function setFCodPalestrante($fCodPalestrante) {
        $this->fCodPalestrante = $fCodPalestrante;
    }

    function setFCodLeitor($fCodLeitor) {
        $this->fCodLeitor = $fCodLeitor;
    }

    function setFSigOA($fSigOA) {
        $this->fSigOA = $fSigOA;
    }

    function setFIdeTipoOa($fIdeTipoOa) {
        $this->fIdeTipoOa = $fIdeTipoOa;
    }

    function setFIdeTipoTom($fIdeTipoTom) {
        $this->fIdeTipoTom = $fIdeTipoTom;
    }

    function setFNomLogradouro($fNomLogradouro) {
        $this->fNomLogradouro = $fNomLogradouro;
    }

    function setFNumEndereco($fNumEndereco) {
        $this->fNumEndereco = $fNumEndereco;
    }

    function setFDesComplementoEndereco($fDesComplementoEndereco) {
        $this->fDesComplementoEndereco = $fDesComplementoEndereco;
    }

    function setFNomBairro($fNomBairro) {
        $this->fNomBairro = $fNomBairro;
    }

    function setFNomCidade($fNomCidade) {
        $this->fNomCidade = $fNomCidade;
    }

    function setFSigUf($fSigUf) {
        $this->fSigUf = $fSigUf;
    }

    function setFCodCepEndereco($fCodCepEndereco) {
        $this->fCodCepEndereco = $fCodCepEndereco;
    }

    function setFSigPaisEndereco($fSigPaisEndereco) {
        $this->fSigPaisEndereco = $fSigPaisEndereco;
    }

    function setFNumCaixaPostal($fNumCaixaPostal) {
        $this->fNumCaixaPostal = $fNumCaixaPostal;
    }

    function setFCodCepCaixaPostal($fCodCepCaixaPostal) {
        $this->fCodCepCaixaPostal = $fCodCepCaixaPostal;
    }

    function setFIdeEnderecoCorrespondencia($fIdeEnderecoCorrespondencia) {
        $this->fIdeEnderecoCorrespondencia = $fIdeEnderecoCorrespondencia;
    }

    function setFNumTelefone($fNumTelefone) {
        $this->fNumTelefone = $fNumTelefone;
    }

    function setFNumFax($fNumFax) {
        $this->fNumFax = $fNumFax;
    }

    function setFNumTelefoFixo($fNumTelefoFixo) {
        $this->fNumTelefoFixo = $fNumTelefoFixo;
    }

    function setFNumCelula($fNumCelula) {
        $this->fNumCelula = $fNumCelula;
    }

    function setFNumOutroTelefo($fNumOutroTelefo) {
        $this->fNumOutroTelefo = $fNumOutroTelefo;
    }

    function setFDesOutroTelefo($fDesOutroTelefo) {
        $this->fDesOutroTelefo = $fDesOutroTelefo;
    }

    function setFDesEmail($fDesEmail) {
        $this->fDesEmail = $fDesEmail;
    }

    function setFDatImplantacaoCadastro($fDatImplantacaoCadastro) {
        $this->fDatImplantacaoCadastro = $fDatImplantacaoCadastro;
    }

    function setFDatCadastro($fDatCadastro) {
        $this->fDatCadastro = $fDatCadastro;
    }

    function setFCodUsuarioCadastro($fCodUsuarioCadastro) {
        $this->fCodUsuarioCadastro = $fCodUsuarioCadastro;
    }

    function setFIdeTipoFiliacRosacruz($fIdeTipoFiliacRosacruz) {
        $this->fIdeTipoFiliacRosacruz = $fIdeTipoFiliacRosacruz;
    }

    function setFSeqCadastPrincipalRosacruz($fSeqCadastPrincipalRosacruz) {
        $this->fSeqCadastPrincipalRosacruz = $fSeqCadastPrincipalRosacruz;
    }

    function setFSeqCadastCompanheiroRosacruz($fSeqCadastCompanheiroRosacruz) {
        $this->fSeqCadastCompanheiroRosacruz = $fSeqCadastCompanheiroRosacruz;
    }

    function setFIdeTipoFiliacTom($fIdeTipoFiliacTom) {
        $this->fIdeTipoFiliacTom = $fIdeTipoFiliacTom;
    }

    function setFSeqCadastPrincipalTom($fSeqCadastPrincipalTom) {
        $this->fSeqCadastPrincipalTom = $fSeqCadastPrincipalTom;
    }

    function setFSeqCadastCompanheiroTom($fSeqCadastCompanheiroTom) {
        $this->fSeqCadastCompanheiroTom = $fSeqCadastCompanheiroTom;
    }

    function setFIdeContribuicaoCarne($fIdeContribuicaoCarne) {
        $this->fIdeContribuicaoCarne = $fIdeContribuicaoCarne;
    }

    function setFCodRosacruzCompanheiro($fCodRosacruzCompanheiro) {
        $this->fCodRosacruzCompanheiro = $fCodRosacruzCompanheiro;
    }

    function setFIdeTipoMoeda($fIdeTipoMoeda) {
        $this->fIdeTipoMoeda = $fIdeTipoMoeda;
    }

    function setFIdeModoFaturamento($fIdeModoFaturamento) {
        $this->fIdeModoFaturamento = $fIdeModoFaturamento;
    }

    function setFIdeDependenciaAdministrativa($fIdeDependenciaAdministrativa) {
        $this->fIdeDependenciaAdministrativa = $fIdeDependenciaAdministrativa;
    }

    function setFDesObservacao($fDesObservacao) {
        $this->fDesObservacao = $fDesObservacao;
    }

    function setFIdeTipoSituacMembroRosacr($fIdeTipoSituacMembroRosacr) {
        $this->fIdeTipoSituacMembroRosacr = $fIdeTipoSituacMembroRosacr;
    }

    function setFIdeTipoSituacMembroOjp($fIdeTipoSituacMembroOjp) {
        $this->fIdeTipoSituacMembroOjp = $fIdeTipoSituacMembroOjp;
    }

    function setFIdeTipoSituacMembroTom($fIdeTipoSituacMembroTom) {
        $this->fIdeTipoSituacMembroTom = $fIdeTipoSituacMembroTom;
    }

    function setFIdeTipoSituacMembroAma($fIdeTipoSituacMembroAma) {
        $this->fIdeTipoSituacMembroAma = $fIdeTipoSituacMembroAma;
    }

    function setFIdeTipoSituacRemessRosacr($fIdeTipoSituacRemessRosacr) {
        $this->fIdeTipoSituacRemessRosacr = $fIdeTipoSituacRemessRosacr;
    }

    function setFIdeTipoSituacRemessOjp($fIdeTipoSituacRemessOjp) {
        $this->fIdeTipoSituacRemessOjp = $fIdeTipoSituacRemessOjp;
    }

    function setFIdeTipoSituacRemessTom($fIdeTipoSituacRemessTom) {
        $this->fIdeTipoSituacRemessTom = $fIdeTipoSituacRemessTom;
    }

    function setFIdeTipoSituacRemessAma($fIdeTipoSituacRemessAma) {
        $this->fIdeTipoSituacRemessAma = $fIdeTipoSituacRemessAma;
    }

    function setFDatQuitacRosacr($fDatQuitacRosacr) {
        $this->fDatQuitacRosacr = $fDatQuitacRosacr;
    }

    function setFDatQuitacOjp($fDatQuitacOjp) {
        $this->fDatQuitacOjp = $fDatQuitacOjp;
    }

    function setFDatQuitacTom($fDatQuitacTom) {
        $this->fDatQuitacTom = $fDatQuitacTom;
    }

    function setFDatQuitacAma($fDatQuitacAma) {
        $this->fDatQuitacAma = $fDatQuitacAma;
    }

    function setFNumLoteAtualRosacr($fNumLoteAtualRosacr) {
        $this->fNumLoteAtualRosacr = $fNumLoteAtualRosacr;
    }

    function setFNumLoteAtualOjp($fNumLoteAtualOjp) {
        $this->fNumLoteAtualOjp = $fNumLoteAtualOjp;
    }

    function setFNumLoteAtualTom($fNumLoteAtualTom) {
        $this->fNumLoteAtualTom = $fNumLoteAtualTom;
    }

    function setFNumLoteAtualAma($fNumLoteAtualAma) {
        $this->fNumLoteAtualAma = $fNumLoteAtualAma;
    }

    function setFNumLoteLimiteRosacr($fNumLoteLimiteRosacr) {
        $this->fNumLoteLimiteRosacr = $fNumLoteLimiteRosacr;
    }

    function setFNumLoteLimiteTom($fNumLoteLimiteTom) {
        $this->fNumLoteLimiteTom = $fNumLoteLimiteTom;
    }

    function setFSeqCadastOaAtuacao($fSeqCadastOaAtuacao) {
        $this->fSeqCadastOaAtuacao = $fSeqCadastOaAtuacao;
    }

    function setFSigOaAtuacao($fSigOaAtuacao) {
        $this->fSigOaAtuacao = $fSigOaAtuacao;
    }

    function setFDatAdmissRosacr($fDatAdmissRosacr) {
        $this->fDatAdmissRosacr = $fDatAdmissRosacr;
    }
        
    //--------------------------------------------------------------------------

    public function cadastroMembroPotencialOA($pdo) {

            $sql = "INSERT INTO membroPotencialOa  
                                 (fk_idOrganismoAfiliado,
                                  regiao,
                                  siglaOA,
                                  seqCadastMembroOa,
                                  fIdeTipoPessoa,
                                  fNomCliente,
                                  fNomConjuge,
                                  fIdeSexo,
                                  fDatNascimento,
                                  fCodCpf,
                                  fCodRg,
                                  fDesOrgaoEmissao,
                                  fCodCnpj,
                                  fCodInscricaoEstadual,
                                  fCodEstadoCivil,
                                  fCodGrauInstrucao,
                                  fSeqOcupacao,
                                  fSeqEspecializacao,
                                  fSeqFormacao,
                                  fCodCliente,
                                  fCodNaoMembro,
                                  fCodRosacruz,
                                  fCodOgg,
                                  fCodAma,
                                  fCodAssinanteRosacruz,
                                  fCodAssinanteAmorcCultural,
                                  fCodAssinanteOrdemJuvenil,
                                  fCodArtista,
                                  fCodPalestrante,
                                  fCodLeitor,
                                  fSigOA,
                                  fIdeTipoOa,
                                  fIdeTipoTom,
                                  fNomLogradouro,
                                  fNumEndereco,
                                  fDesComplementoEndereco,
                                  fNomBairro,
                                  fNomCidade,
                                  fSigUf,
                                  fCodCepEndereco,
                                  fSigPaisEndereco,
                                  fNumCaixaPostal,
                                  fCodCepCaixaPostal,
                                  fIdeEnderecoCorrespondencia,
                                  fNumTelefone,
                                  fNumFax,
                                  fNumTelefoFixo,
                                  fNumCelula,
                                  fNumOutroTelefo,
                                  fDesOutroTelefo,
                                  fDesEmail,
                                  fDatImplantacaoCadastro,
                                  fDatCadastro,
                                  fCodUsuarioCadastro,
                                  fIdeTipoFiliacRosacruz,
                                  fSeqCadastPrincipalRosacruz,
                                  fSeqCadastCompanheiroRosacruz,
                                  fIdeTipoFiliacTom,
                                  fSeqCadastPrincipalTom,
                                  fSeqCadastCompanheiroTom,
                                  fIdeContribuicaoCarne,
                                  fCodRosacruzCompanheiro,
                                  fIdeTipoMoeda,
                                  fIdeModoFaturamento,
                                  fIdeDependenciaAdministrativa,
                                  fDesObservacao,
                                  fIdeTipoSituacMembroRosacr,
                                  fIdeTipoSituacMembroOjp,
                                  fIdeTipoSituacMembroTom,
                                  fIdeTipoSituacMembroAma,
                                  fIdeTipoSituacRemessRosacr,
                                  fIdeTipoSituacRemessOjp,
                                  fIdeTipoSituacRemessTom,
                                  fIdeTipoSituacRemessAma,
                                  fDatQuitacRosacr,
                                  fDatQuitacOjp,
                                  fDatQuitacTom,
                                  fDatQuitacAma,
                                  fNumLoteAtualRosacr,
                                  fNumLoteAtualOjp,
                                  fNumLoteAtualTom,
                                  fNumLoteAtualAma,
                                  fNumLoteLimiteRosacr,
                                  fNumLoteLimiteTom,
                                  fSeqCadastOaAtuacao,
                                  fSigOaAtuacao,
                                  fDatAdmissRosacr,
                                  dataCadastro
                                  ) 
                            VALUES (
                            		:fk_idOrganismoAfiliado,
                                        :regiao,
                            		:siglaOA,
                            		:seqCadastMembroOa,
                                        :fIdeTipoPessoa,
                                        :fNomCliente,
                                        :fNomConjuge,
                                        :fIdeSexo,
                                        :fDatNascimento,
                                        :fCodCpf,
                                        :fCodRg,
                                        :fDesOrgaoEmissao,
                                        :fCodCnpj,
                                        :fCodInscricaoEstadual,
                                        :fCodEstadoCivil,
                                        :fCodGrauInstrucao,
                                        :fSeqOcupacao,
                                        :fSeqEspecializacao,
                                        :fSeqFormacao,
                                        :fCodCliente,
                                        :fCodNaoMembro,
                                        :fCodRosacruz,
                                        :fCodOgg,
                                        :fCodAma,
                                        :fCodAssinanteRosacruz,
                                        :fCodAssinanteAmorcCultural,
                                        :fCodAssinanteOrdemJuvenil,
                                        :fCodArtista,
                                        :fCodPalestrante,
                                        :fCodLeitor,
                                        :fSigOA,
                                        :fIdeTipoOa,
                                        :fIdeTipoTom,
                                        :fNomLogradouro,
                                        :fNumEndereco,
                                        :fDesComplementoEndereco,
                                        :fNomBairro,
                                        :fNomCidade,
                                        :fSigUf,
                                        :fCodCepEndereco,
                                        :fSigPaisEndereco,
                                        :fNumCaixaPostal,
                                        :fCodCepCaixaPostal,
                                        :fIdeEnderecoCorrespondencia,
                                        :fNumTelefone,
                                        :fNumFax,
                                        :fNumTelefoFixo,
                                        :fNumCelula,
                                        :fNumOutroTelefo,
                                        :fDesOutroTelefo,
                                        :fDesEmail,
                                        :fDatImplantacaoCadastro,
                                        :fDatCadastro,
                                        :fCodUsuarioCadastro,
                                        :fIdeTipoFiliacRosacruz,
                                        :fSeqCadastPrincipalRosacruz,
                                        :fSeqCadastCompanheiroRosacruz,
                                        :fIdeTipoFiliacTom,
                                        :fSeqCadastPrincipalTom,
                                        :fSeqCadastCompanheiroTom,
                                        :fIdeContribuicaoCarne,
                                        :fCodRosacruzCompanheiro,
                                        :fIdeTipoMoeda,
                                        :fIdeModoFaturamento,
                                        :fIdeDependenciaAdministrativa,
                                        :fDesObservacao,
                                        :fIdeTipoSituacMembroRosacr,
                                        :fIdeTipoSituacMembroOjp,
                                        :fIdeTipoSituacMembroTom,
                                        :fIdeTipoSituacMembroAma,
                                        :fIdeTipoSituacRemessRosacr,
                                        :fIdeTipoSituacRemessOjp,
                                        :fIdeTipoSituacRemessTom,
                                        :fIdeTipoSituacRemessAma,
                                        :fDatQuitacRosacr,
                                        :fDatQuitacOjp,
                                        :fDatQuitacTom,
                                        :fDatQuitacAma,
                                        :fNumLoteAtualRosacr,
                                        :fNumLoteAtualOjp,
                                        :fNumLoteAtualTom,
                                        :fNumLoteAtualAma,
                                        :fNumLoteLimiteRosacr,
                                        :fNumLoteLimiteTom,
                                        :fSeqCadastOaAtuacao,
                                        :fSigOaAtuacao,
                                        :fDatAdmissRosacr,
                                    now()
                                    )";
            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql);

            $sth->bindParam(':fk_idOrganismoAfiliado',$this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
            $sth->bindParam(':regiao',$this->regiao, PDO::PARAM_STR);
            $sth->bindParam(':siglaOA',$this->siglaOA, PDO::PARAM_STR);
            $sth->bindParam(':seqCadastMembroOa',$this->seqCadastMembroOa,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoPessoa',$this->fIdeTipoPessoa, PDO::PARAM_STR);
            $sth->bindParam(':fNomCliente',$this->fNomCliente, PDO::PARAM_STR);
            $sth->bindParam(':fNomConjuge',$this->fNomConjuge, PDO::PARAM_STR);
            $sth->bindParam(':fIdeSexo',$this->fIdeSexo, PDO::PARAM_STR);
            $sth->bindParam(':fDatNascimento',$this->fDatNascimento, PDO::PARAM_STR);
            $sth->bindParam(':fCodCpf',$this->fCodCpf,PDO::PARAM_STR);
            $sth->bindParam(':fCodRg',$this->fCodRg,PDO::PARAM_STR);
            $sth->bindParam(':fDesOrgaoEmissao',$this->fDesOrgaoEmissao,PDO::PARAM_STR);
            $sth->bindParam(':fCodCnpj',$this->fCodCnpj,PDO::PARAM_STR);
            $sth->bindParam(':fCodInscricaoEstadual',$this->fCodInscricaoEstadual,PDO::PARAM_STR);
            $sth->bindParam(':fCodEstadoCivil',$this->fCodEstadoCivil,PDO::PARAM_INT);
            $sth->bindParam(':fCodGrauInstrucao',$this->fCodGrauInstrucao,PDO::PARAM_INT);
            $sth->bindParam(':fSeqOcupacao',$this->fSeqOcupacao,PDO::PARAM_INT);
            $sth->bindParam(':fSeqEspecializacao',$this->fSeqEspecializacao,PDO::PARAM_INT);
            $sth->bindParam(':fSeqFormacao',$this->fSeqFormacao,PDO::PARAM_INT);
            $sth->bindParam(':fCodCliente',$this->fCodCliente,PDO::PARAM_INT);
            $sth->bindParam(':fCodNaoMembro',$this->fCodNaoMembro,PDO::PARAM_INT);
            $sth->bindParam(':fCodRosacruz',$this->fCodRosacruz,PDO::PARAM_INT);
            $sth->bindParam(':fCodOgg',$this->fCodOgg,PDO::PARAM_INT);
            $sth->bindParam(':fCodAma',$this->fCodAma,PDO::PARAM_INT);
            $sth->bindParam(':fCodAssinanteRosacruz',$this->fCodAssinanteRosacruz,PDO::PARAM_INT);
            $sth->bindParam(':fCodAssinanteAmorcCultural',$this->fCodAssinanteAmorcCultural,PDO::PARAM_INT);
            $sth->bindParam(':fCodAssinanteOrdemJuvenil',$this->fCodAssinanteOrdemJuvenil,PDO::PARAM_INT);
            $sth->bindParam(':fCodArtista',$this->fCodArtista,PDO::PARAM_INT);
            $sth->bindParam(':fCodPalestrante',$this->fCodPalestrante,PDO::PARAM_INT);
            $sth->bindParam(':fCodLeitor',$this->fCodLeitor,PDO::PARAM_INT);
            $sth->bindParam(':fSigOA',$this->fSigOA,PDO::PARAM_STR);
            $sth->bindParam(':fIdeTipoOa',$this->fIdeTipoOa,PDO::PARAM_STR);
            $sth->bindParam(':fIdeTipoTom',$this->fIdeTipoTom,PDO::PARAM_STR);
            $sth->bindParam(':fNomLogradouro',$this->fNomLogradouro,PDO::PARAM_STR);
            $sth->bindParam(':fNumEndereco',$this->fNumEndereco,PDO::PARAM_INT);
            $sth->bindParam(':fDesComplementoEndereco',$this->fDesComplementoEndereco,PDO::PARAM_STR);
            $sth->bindParam(':fNomBairro',$this->fNomBairro,PDO::PARAM_STR);
            $sth->bindParam(':fNomCidade',$this->fNomCidade,PDO::PARAM_STR);
            $sth->bindParam(':fSigUf',$this->fSigUf,PDO::PARAM_STR);
            $sth->bindParam(':fCodCepEndereco',$this->fCodCepEndereco,PDO::PARAM_STR);
            $sth->bindParam(':fSigPaisEndereco',$this->fSigPaisEndereco,PDO::PARAM_STR);
            $sth->bindParam(':fNumCaixaPostal',$this->fNumCaixaPostal,PDO::PARAM_STR);
            $sth->bindParam(':fCodCepCaixaPostal',$this->fCodCepCaixaPostal,PDO::PARAM_STR);
            $sth->bindParam(':fIdeEnderecoCorrespondencia',$this->fIdeEnderecoCorrespondencia,PDO::PARAM_INT);
            $sth->bindParam(':fNumTelefone',$this->fNumTelefone,PDO::PARAM_STR);
            $sth->bindParam(':fNumFax',$this->fNumFax,PDO::PARAM_STR);
            $sth->bindParam(':fNumTelefoFixo',$this->fNumTelefoFixo,PDO::PARAM_STR);
            $sth->bindParam(':fNumCelula',$this->fNumCelula,PDO::PARAM_STR);
            $sth->bindParam(':fNumOutroTelefo',$this->fNumOutroTelefo,PDO::PARAM_STR);
            $sth->bindParam(':fDesOutroTelefo',$this->fDesOutroTelefo,PDO::PARAM_STR);
            $sth->bindParam(':fDesEmail',$this->fDesEmail,PDO::PARAM_STR);
            $sth->bindParam(':fDatImplantacaoCadastro',$this->fDatImplantacaoCadastro,PDO::PARAM_STR);
            $sth->bindParam(':fDatCadastro',$this->fDatCadastro,PDO::PARAM_STR);
            $sth->bindParam(':fCodUsuarioCadastro',$this->fCodUsuarioCadastro,PDO::PARAM_STR);
            $sth->bindParam(':fIdeTipoFiliacRosacruz',$this->fIdeTipoFiliacRosacruz,PDO::PARAM_INT);
            $sth->bindParam(':fSeqCadastPrincipalRosacruz',$this->fSeqCadastPrincipalRosacruz, PDO::PARAM_INT);
            $sth->bindParam(':fSeqCadastCompanheiroRosacruz',$this->fSeqCadastCompanheiroRosacruz,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoFiliacTom',$this->fIdeTipoFiliacTom,PDO::PARAM_INT);
            $sth->bindParam(':fSeqCadastPrincipalTom',$this->fSeqCadastPrincipalTom,PDO::PARAM_INT);
            $sth->bindParam(':fSeqCadastCompanheiroTom',$this->fSeqCadastCompanheiroTom,PDO::PARAM_INT);
            $sth->bindParam(':fIdeContribuicaoCarne',$this->fIdeContribuicaoCarne,PDO::PARAM_STR);
            $sth->bindParam(':fCodRosacruzCompanheiro',$this->fCodRosacruzCompanheiro,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoMoeda',$this->fIdeTipoMoeda,PDO::PARAM_INT);
            $sth->bindParam(':fIdeModoFaturamento',$this->fIdeModoFaturamento,PDO::PARAM_INT);
            $sth->bindParam(':fIdeDependenciaAdministrativa',$this->fIdeDependenciaAdministrativa,PDO::PARAM_INT);
            $sth->bindParam(':fDesObservacao',$this->fDesObservacao,PDO::PARAM_STR);
            $sth->bindParam(':fIdeTipoSituacMembroRosacr',$this->fIdeTipoSituacMembroRosacr,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoSituacMembroOjp',$this->fIdeTipoSituacMembroOjp,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoSituacMembroTom',$this->fIdeTipoSituacMembroTom,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoSituacMembroAma',$this->fIdeTipoSituacMembroAma,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoSituacRemessRosacr',$this->fIdeTipoSituacRemessRosacr,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoSituacRemessOjp',$this->fIdeTipoSituacRemessOjp,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoSituacRemessTom',$this->fIdeTipoSituacRemessTom,PDO::PARAM_INT);
            $sth->bindParam(':fIdeTipoSituacRemessAma',$this->fIdeTipoSituacRemessAma,PDO::PARAM_INT);
            $sth->bindParam(':fDatQuitacRosacr',$this->fDatQuitacRosacr,PDO::PARAM_STR);
            $sth->bindParam(':fDatQuitacOjp',$this->fDatQuitacOjp,PDO::PARAM_STR);
            $sth->bindParam(':fDatQuitacTom',$this->fDatQuitacTom,PDO::PARAM_STR);
            $sth->bindParam(':fDatQuitacAma',$this->fDatQuitacAma,PDO::PARAM_STR);
            $sth->bindParam(':fNumLoteAtualRosacr',$this->fNumLoteAtualRosacr,PDO::PARAM_INT);
            $sth->bindParam(':fNumLoteAtualOjp',$this->fNumLoteAtualOjp,PDO::PARAM_INT);
            $sth->bindParam(':fNumLoteAtualTom',$this->fNumLoteAtualTom,PDO::PARAM_INT);
            $sth->bindParam(':fNumLoteAtualAma',$this->fNumLoteAtualAma,PDO::PARAM_INT);
            $sth->bindParam(':fNumLoteLimiteRosacr',$this->fNumLoteLimiteRosacr,PDO::PARAM_INT);
            $sth->bindParam(':fNumLoteLimiteTom',$this->fNumLoteLimiteTom,PDO::PARAM_INT);
            $sth->bindParam(':fSeqCadastOaAtuacao',$this->fSeqCadastOaAtuacao,PDO::PARAM_INT);
            $sth->bindParam(':fSigOaAtuacao',$this->fSigOaAtuacao,PDO::PARAM_STR);
            $sth->bindParam(':fDatAdmissRosacr',$this->fDatAdmissRosacr, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function limpaTabela() {

        $sql = "TRUNCATE membroPotencialOa";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $resultado = $c->run($sth,true);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaMembroPotencialOA($idOrganismoAfiliado = null, $siglaOA = null) 
    {

        $sql = "SELECT * FROM membroPotencialOa
    	where 1=1 ";
        if ($idOrganismoAfiliado != null) {
            $sql .= " and fk_idOrganismoAfiliado = ':siglaOA'";
        }
        if ($siglaOA != null) {
            $sql .= " and siglaOA = ':siglaOA'";
        }
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($idOrganismoAfiliado != null) {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if ($siglaOA != null) {
            $sth->bindParam(':siglaOA', $siglaOA, PDO::PARAM_STR);
        }       
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function totalMembroPotencialOA($idOrganismoAfiliado = null, $siglaOA = null) {

        $sql = "SELECT * FROM membroPotencialOa
    	where 1=1 ";
        if ($siglaOA != null) {
            $sql .= " and siglaOA = ':siglaOA'";
        }
        if ($idOrganismoAfiliado != null) {
            $sql .= " and fk_idOrganismoAfiliado = ':idOrganismoAfiliado'";
        }
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($siglaOA != null) {
            $sth->bindParam(':siglaOA', $siglaOA, PDO::PARAM_STR);
        }
        if ($idOrganismoAfiliado != null) {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return count($resultado['idMembroPotencialOA']);
        }else{
            return false;
        }
    }

    public function verificaSeJaCadastrado($seqCadastMembroOa) 
    {

        $sql = "SELECT * FROM membroPotencialOa 
    			where seqCadastMembroOa=':seqCadast'";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadastMembroOa, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
                return true;
            } else {
                return false;
        }
    }

    public function listaMembrosPorIdOrganismo($fk_idOrganismoAfiliado) {

        $sql = "SELECT * FROM membroPotencialOa
    			where fk_idOrganismoAfiliado=':idOrganismoAfiliado'";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaMembrosPorIdOrganismoFiltrado($fk_idOrganismoAfiliado) {

        $sql = "SELECT seqCadastMembroOa,fNomCliente,fCodRosacruz,fNumLoteAtualRosacr,fDatImplantacaoCadastro,fIdeTipoSituacRemessRosacr 
            FROM membroPotencialOa
    			where fk_idOrganismoAfiliado=':idOrganismoAfiliado'";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaMembrosPorIdOrganismoFiltradoPorData($fk_idOrganismoAfiliado) {

        $sql = "SELECT seqCadastMembroOa,fNomCliente,fCodRosacruz,fNumLoteAtualRosacr,fDatImplantacaoCadastro,fIdeTipoSituacRemessRosacr,fDesEmail,fNumTelefoFixo,fNumCelula,fNumOutroTelefo,fDatAdmissRosacr FROM membroPotencialOa
    			where fk_idOrganismoAfiliado=':idOrganismoAfiliado'"
                . "and date(dataCadastro)='" . date('Y-m-d') . "'";

        //echo "SQL(listaMembrosPorIdOrganismoFiltradoPorData): " . $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>
