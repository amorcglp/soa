<?php

require_once ("conexaoClass.php");

class ataPosseAssinada{

    private $idAtaPosseAssinada;
    private $fk_idAtaPosse;
    private $caminhoAtaPosseAssinada;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtaPosseAssinada() {
        return $this->idAtaPosseAssinada;
    }

    function getFkIdAtaPosse() {
        return $this->fk_idAtaPosse;
    }

    function getCaminhoAtaPosseAssinada() {
        return $this->caminhoAtaPosseAssinada;
    }

    function setIdAtaPosseAssinada($idAtaPosseAssinada) {
        $this->idAtaPosseAssinada = $idAtaPosseAssinada;
    }

    function setFkIdAtaPosse($fk_idAtaPosse) {
        $this->fk_idAtaPosse = $fk_idAtaPosse;
    }

    function setCaminhoAtaPosseAssinada($caminhoAtaPosseAssinada) {
        $this->caminhoAtaPosseAssinada = $caminhoAtaPosseAssinada;
    }

    //--------------------------------------------------------------------------

    public function cadastroAtaPosseAssinada(){

        $sql = "INSERT INTO ataPosse_Assinada
                                 (
                                  fk_idAtaPosse,
                                  caminhoAtaPosseAssinada,
                                  dataCadastro
                                  )
                            VALUES (:fk_idAtaPosse,
                                    :caminhoAtaPosseAssinada,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtaPosse', $this->fk_idAtaPosse, PDO::PARAM_INT);
        $sth->bindParam(':caminhoAtaPosseAssinada', $this->caminhoAtaPosseAssinada, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

	public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE 'ataPosse_Assinada'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function listaAtaPosseAssinada($id=null,$mes=null,$ano=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM ataPosse_Assinada inner join ataPosse on fk_idAtaPosse = idAtaPosse where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idAtaPosse=:id";
    	}
    	if($mes!=null)
    	{
    		$sql .= " and MONTH(dataPosse)=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and YEAR(dataPosse)=:ano";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo "<br><br>".$sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        if($id!=null)
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        if($mes!=null)
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        if($ano!=null)
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        if($idOrganismoAfiliado!=null)
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeAtaPosseAssinada($idAtaPosseAssinada){
        $sql = "DELETE FROM ataPosse_Assinada WHERE idAtaPosseAssinada=:idAtaPosseAssinada";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaPosseAssinada', $idAtaPosseAssinada, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


}

?>