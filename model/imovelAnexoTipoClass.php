<?php

require_once ("conexaoClass.php");

class ImovelAnexoTipo{

    private $idImovelAnexoTipo;
    private $nomeImovelAnexoTipo;
    private $descricaoImovelAnexoTipo;
    private $anualImovelAnexoTipo;
    private $statusImovelAnexoTipo;
    private $cadastroDataImovelAnexoTipo;
    private $atualizadoDataImovelAnexoTipo;
    private $fk_seqCadastAtualizadoPor;

    //--- GETERS -----------------------------------------------------
    public function getIdImovelAnexoTipo(){
        return $this->idImovelAnexoTipo;
    }
    public function getNomeImovelAnexoTipo(){
        return $this->nomeImovelAnexoTipo;
    }
    public function getDescricaoImovelAnexoTipo(){
        return $this->descricaoImovelAnexoTipo;
    }
    public function getAnualImovelAnexoTipo(){
        return $this->anualImovelAnexoTipo;
    }
    public function getStatusImovelAnexoTipo(){
        return $this->statusImovelAnexoTipo;
    }
    public function getCadastroDataImovelAnexoTipo(){
        return $this->cadastroDataImovelAnexoTipo;
    }
    public function getAtualizadoDataImovelAnexoTipo(){
        return $this->atualizadoDataImovelAnexoTipo;
    }
    public function getFkSeqCadastAtualizadoPor() {
        return $this->fk_seqCadastAtualizadoPor;
    }
    
    //--- SETERS -----------------------------------------------------
    public function setIdImovelAnexoTipo($idImovelAnexoTipo){
        $this->idImovelAnexoTipo = $idImovelAnexoTipo;
    }
    public function setNomeImovelAnexoTipo($nomeImovelAnexoTipo){
        $this->nomeImovelAnexoTipo = $nomeImovelAnexoTipo;
    }
    public function setDescricaoImovelAnexoTipo($descricaoImovelAnexoTipo){
        $this->descricaoImovelAnexoTipo = $descricaoImovelAnexoTipo;
    }
    public function setAnualImovelAnexoTipo($anualImovelAnexoTipo){
        $this->anualImovelAnexoTipo = $anualImovelAnexoTipo;
    }
    public function setStatusImovelAnexoTipo($statusImovelAnexoTipo){
        $this->statusImovelAnexoTipo = $statusImovelAnexoTipo;
    }
    public function setCadastroDataImovelAnexoTipo($cadastroDataImovelAnexoTipo){
        $this->cadastroDataImovelAnexoTipo = $cadastroDataImovelAnexoTipo;
    }
    public function setAtualizadoDataImovelAnexoTipo($atualizadoDataImovelAnexoTipo){
        $this->atualizadoDataImovelAnexoTipo = $atualizadoDataImovelAnexoTipo;
    }
    public function setFkSeqCadastAtualizadoPor($fk_seqCadastAtualizadoPor){
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }
    
    //--------------------------------------------------------------------------

	public function cadastroImovelAnexoTipo(){

        $sql = "INSERT INTO imovelAnexo_tipo
                                 (nomeImovelAnexoTipo,
                                  descricaoImovelAnexoTipo,
                                  anualImovelAnexoTipo,
                                  cadastroDataImovelAnexoTipo,
                                  fk_seqCadastAtualizadoPor)
                            VALUES (:nome,
                                    :descricao,
                                    :anual,
                                    NOW(),
                                    :atualizadoPor)";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nome', $this->nomeImovelAnexoTipo, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoImovelAnexoTipo, PDO::PARAM_STR);
        $sth->bindParam(':anual', $this->anualImovelAnexoTipo, PDO::PARAM_INT);
        $sth->bindParam(':atualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraImovelAnexoTipo($idImovelAnexoTipo)
    {

        $sql = "UPDATE imovelAnexo_tipo SET
                                  nomeImovelAnexoTipo = :nome,
                                  descricaoImovelAnexoTipo =  :descricao,
				  anualImovelAnexoTipo = :anual,
                                  atualizadoDataImovelAnexoTipo = NOW(),
                                  fk_seqCadastAtualizadoPor = :atualizadoPor
                                  WHERE idImovelAnexoTipo = :id";
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nome', $this->nomeImovelAnexoTipo, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoImovelAnexoTipo, PDO::PARAM_STR);
        $sth->bindParam(':anual', $this->anualImovelAnexoTipo, PDO::PARAM_INT);
        $sth->bindParam(':atualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':id', $idImovelAnexoTipo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function listaImovelAnexoTipo($anual=null,$status=null){

        $sql = "SELECT * FROM imovelAnexo_tipo WHERE 1=1";
        $sql .= ($status !== null) ? " and statusImovelAnexoTipo=:status" : "";
        $sql .= ($anual !== null) ? " and anualImovelAnexoTipo=:anual" : "";
        $sql .= " ORDER BY idImovelAnexoTipo ASC";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($status !== null) {
            $sth->bindParam(':status', $status, PDO::PARAM_INT);
        }
        if ($anual !== null) {
            $sth->bindParam(':anual', $anual, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdImovelAnexoTipo($idImovelAnexoTipo){
        
        $sql="SELECT * FROM  imovelAnexo_tipo
                                WHERE   idImovelAnexoTipo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idImovelAnexoTipo, PDO::PARAM_INT);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function alteraStatusImovelAnexoTipo() {
		
        $sql = "UPDATE imovelAnexo_tipo SET statusImovelAnexoTipo = :status "
                . " WHERE idImovelAnexoTipo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusImovelAnexoTipo, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idImovelAnexoTipo, PDO::PARAM_INT);

        if ($c->run($sth, true)) {
            	$arr = array();
		$arr['status'] = $this->statusImovelAnexoTipo;
            return $arr;
        } else {
            return false;
        }
    }
    
}

?>
