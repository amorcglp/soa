<?php

require_once ("conexaoClass.php");

class funcao{

    private $idFuncao;
    private $nomeFuncao;
    private $vinculoExterno;
    private $atuaRegiao;
    private $descricaoFuncao;
    private $fk_idDepartamento;
    public $statusFuncao;

    //METODO SET/GET -----------------------------------------------------------

    function getIdFuncao() {
        return $this->idFuncao;
    }

    function getNomeFuncao() {
        return $this->nomeFuncao;
    }

    function getVinculoExterno() {
        return $this->vinculoExterno;
    }
    
	function getAtuaRegiao() {
        return $this->atuaRegiao;
    }

    function getDescricaoFuncao() {
        return $this->descricaoFuncao;
    }

    function getFk_idDepartamento() {
        return $this->fk_idDepartamento;
    }
    
	function getStatusFuncao() {
        return $this->statusFuncao;
    }

    function setIdFuncao($idFuncao) {
        $this->idFuncao = $idFuncao;
    }

    function setNomeFuncao($nomeFuncao) {
        $this->nomeFuncao = $nomeFuncao;
    }

    function setVinculoExterno($vinculoExterno) {
        $this->vinculoExterno = $vinculoExterno;
    }
    
	function setAtuaRegiao($atuaRegiao) {
        $this->atuaRegiao = $atuaRegiao;
    }

    function setDescricaoFuncao($descricaoFuncao) {
        $this->descricaoFuncao = $descricaoFuncao;
    }

    function setFk_idDepartamento($fk_idDepartamento) {
        $this->fk_idDepartamento = $fk_idDepartamento;
    }
    
	function setStatusFuncao($statusFuncao) {
        $this->statusFuncao = $statusFuncao;
    }

    //--------------------------------------------------------------------------

    public function cadastroFuncao(){

        $sql="INSERT INTO funcao  
                                 (nomeFuncao,
                                  vinculoExterno,
                                  atuaRegiao,
                                  descricaoFuncao,
                                  fk_idDepartamento)
                                  
                            VALUES (:nome,
                                    :vinculoExterno,
                                    :atuaRegiao,
                                    :descricao,
                                    :idDepartamento)";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nome', $this->nomeFuncao, PDO::PARAM_STR);
        $sth->bindParam(':vinculoExterno', $this->vinculoExterno, PDO::PARAM_INT);
        $sth->bindParam(':atuaRegiao', $this->atuaRegiao, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoFuncao, PDO::PARAM_STR);
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }

    public function alteraFuncao($idFuncao){
    	
        $sql="UPDATE funcao SET  
                                  nomeFuncao = :nome,
                                  vinculoExterno =  :vinculoExterno,
                                  atuaRegiao =  :atuaRegiao,
				  descricaoFuncao = :descricao,
                                  fk_idDepartamento = :idDepartamento
                                  WHERE idFuncao = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nome', $this->nomeFuncao, PDO::PARAM_STR);
        $sth->bindParam(':vinculoExterno', $this->vinculoExterno, PDO::PARAM_INT);
        $sth->bindParam(':atuaRegiao', $this->atuaRegiao, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoFuncao, PDO::PARAM_STR);
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':id', $idFuncao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusFuncao() {
        
	$sql = "UPDATE funcao SET `statusFuncao` = :status
                               WHERE `idFuncao` = :idFuncao";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusFuncao, PDO::PARAM_INT);
        $sth->bindParam(':idFuncao', $this->idFuncao, PDO::PARAM_INT);
		
        if ($c->run($sth,true)) {
            $arr = array();
            $arr['status'] = $status;
            return $arr;
        } else {
            return false;
        }
    }

    public function listaFuncao($parametro=null,$atuaRegiao=null,$funcoes=null,$tipo=null,$ativo=null){

    	$sql = " SELECT *,f.vinculoExterno as vinculoExternoFuncao FROM funcao as f ";
        if($funcoes==null)
        {    
            $sql .= " inner join departamento as d on fk_idDepartamento = idDepartamento ";
        }
    	$sql .= " where 1=1 
    	 ";   
    	if($parametro!=null)
    	{
    		$sql .= " and fk_idDepartamento=:parametro";
    	}
    	if($atuaRegiao!=null)
    	{
    		$sql .= " and atuaRegiao=:atuaRegiao";
    	}
        if($ativo!=null)
    	{
    		$sql .= " and statusFuncao=0";
    	}
        if($funcoes!=null)
        {
            $ids = explode(",", $funcoes);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {    
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and LEFT(f.vinculoExterno,1) IN (" . $inQuery . ") ";
        }
        if($tipo!=null)
    	{
            if($tipo!=0)
            {
                if($tipo==2)
                {
                    $sql .= " and LEFT(f.nomeFuncao,3) = 'EX-'";
                }else{
                    $sql .= " and LEFT(f.nomeFuncao,3) != 'EX-'";
                }    
            }    
    	}
    	$sql .= " ORDER BY nomeFuncao ASC";
    	//echo $sql;echo $funcoes;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($parametro!=null)
    	{
            $sth->bindParam(':parametro', $parametro, PDO::PARAM_INT);
        }
        if($atuaRegiao!=null)
    	{
            $sth->bindParam(':atuaRegiao', $atuaRegiao, PDO::PARAM_INT);
        }
        if($funcoes!=null)
        {
            foreach ($ids as $u => $id)
            {    
                 $sth->bindValue(":".$u, $id);
            }
        }
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaNomeFuncao(){
        
    	$sql = "SELECT *   FROM    funcao
                                    WHERE  nomeFuncao LIKE ':nome'
                                    ORDER BY nomeFuncao ASC";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $nome = '%'.str_replace("  "," ",trim($this->getNomeFuncao())).'%';
        $sth->bindParam(':nome', $nome, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaVinculoExterno(){
        
    	$sql = "SELECT *   FROM    funcao
                                    WHERE  vinculoExterno = :vinculoExterno
                                    and statusFuncao=0
                                    ORDER BY nomeFuncao ASC";
    	//echo "<br>".$sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $vinculo = $this->getVinculoExterno();
        
        $sth->bindParam(':vinculoExterno', $vinculo, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdFuncao($idFuncao){
        $sql="SELECT * FROM  funcao
                                WHERE   idFuncao = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idFuncao, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscarIdVinculoExterno($idFuncao){
        
        $sql="SELECT * FROM  funcao
                WHERE   vinculoExterno = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idFuncao, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeFuncao($idFuncao){
        
        $sql="DELETE FROM funcao WHERE idFuncao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idFuncao, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }

    public function listaFuncaoPorIdFuncao($parametro=null,$atuaRegiao=null,$funcoes=null,$tipo=null,$ativo=null){

        $sql = " SELECT *,f.vinculoExterno as vinculoExternoFuncao FROM funcao as f ";
        if($funcoes==null)
        {
            $sql .= " inner join departamento as d on fk_idDepartamento = idDepartamento ";
        }
        $sql .= " where 1=1 
    	 ";
        if($parametro!=null)
        {
            $sql .= " and fk_idDepartamento=:parametro";
        }
        if($atuaRegiao!=null)
        {
            $sql .= " and atuaRegiao=:atuaRegiao";
        }
        if($ativo!=null)
        {
            $sql .= " and statusFuncao=0";
        }
        if($funcoes!=null)
        {
            $ids = explode(",", $funcoes);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and f.idFuncao IN (" . $inQuery . ") ";
        }
        if($tipo!=null)
        {
            if($tipo!=0)
            {
                if($tipo==2)
                {
                    $sql .= " and LEFT(f.nomeFuncao,3) = 'EX-'";
                }else{
                    $sql .= " and LEFT(f.nomeFuncao,3) != 'EX-'";
                }
            }
        }
        $sql .= " ORDER BY nomeFuncao ASC";
        //echo $sql;echo $funcoes;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($parametro!=null)
        {
            $sth->bindParam(':parametro', $parametro, PDO::PARAM_INT);
        }
        if($atuaRegiao!=null)
        {
            $sth->bindParam(':atuaRegiao', $atuaRegiao, PDO::PARAM_INT);
        }
        if($funcoes!=null)
        {
            foreach ($ids as $u => $id)
            {
                $sth->bindValue(":".$u, $id);
            }
        }

        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }

    
}

?>
