<?php

require_once ("conexaoClass.php");

class auxiliarWeb {

    private $idAuxiliarWeb;
    private $fk_idOrganismoAfiliado;
    private $dataTermoCompromisso;
    private $nome;
    private $codigoAfiliacao;
    private $cpf;
    private $email;
    private $logradouro;
    private $numero;
    private $complemento;
    private $bairro;
    private $cep;
    private $cidade;
    private $uf;
    private $pais;
    private $telefoneResidencial;
    private $telefoneComercial;
    private $celular;
    private $usuario;
    private $ultimoAtualizar;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAuxiliarWeb() {
        return $this->idAuxiliarWeb;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getDataTermoCompromisso() {
        return $this->dataTermoCompromisso;
    }

    function getNome() {
        return $this->nome;
    }

    function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getEmail() {
        return $this->email;
    }

    function getLogradouro() {
        return $this->logradouro;
    }

    function getNumero() {
        return $this->numero;
    }

    function getComplemento() {
        return $this->complemento;
    }

    function getBairro() {
        return $this->bairro;
    }

    function getCep() {
        return $this->cep;
    }

    function getCidade() {
        return $this->cidade;
    }

    function getUf() {
        return $this->uf;
    }

    function getPais() {
        return $this->pais;
    }

    function getTelefoneResidencial() {
        return $this->telefoneResidencial;
    }

    function getTelefoneComercial() {
        return $this->telefoneComercial;
    }

    function getCelular() {
        return $this->celular;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdAuxiliarWeb($idAuxiliarWeb) {
        $this->idAuxiliarWeb = $idAuxiliarWeb;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setDataTermoCompromisso($dataTermoCompromisso) {
        $this->dataTermoCompromisso = $dataTermoCompromisso;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setComplemento($complemento) {
        $this->complemento = $complemento;
    }

    function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    function setUf($uf) {
        $this->uf = $uf;
    }

    function setPais($pais) {
        $this->pais = $pais;
    }

    function setTelefoneResidencial($telefoneResidencial) {
        $this->telefoneResidencial = $telefoneResidencial;
    }

    function setTelefoneComercial($telefoneComercial) {
        $this->telefoneComercial = $telefoneComercial;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    
    //--------------------------------------------------------------------------

    public function cadastroAuxiliarWeb() {

        $sql = "INSERT INTO auxiliarWeb  
                                 (
                                  fk_idOrganismoAfiliado,
                                  dataTermoCompromisso,
                                  nome,
                                  codigoAfiliacao,
                                  cpf,
                                  email,
                                  logradouro,
                                  numero,
                                  complemento,
                                  bairro,
                                  cep,
                                  cidade,
                                  uf,
                                  pais,
                                  telefoneResidencial,
                                  telefoneComercial,
                                  celular,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                                    :fk_idOrganismoAfiliado,
                                    :dataTermoCompromisso,    
                                    :nome,
                                    :codigoAfiliacao,
                                    :cpf,
                                    :email,
                                    :logradouro,
                                    :numero,
                                    :complemento,
                                    :bairro,
                                    :cep,
                                    :cidade,
                                    :uf,
                                    :pais,
                                    :telefoneResidencial,
                                    :telefoneComercial,
                                    :celular,
                                    :usuario,
                                    now())";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataTermoCompromisso', $this->dataTermoCompromisso, PDO::PARAM_STR);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':cpf', $this->cpf, PDO::PARAM_STR);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);
        $sth->bindParam(':logradouro', $this->logradouro, PDO::PARAM_STR);
        $sth->bindParam(':numero', $this->numero, PDO::PARAM_INT);
        $sth->bindParam(':complemento', $this->complemento, PDO::PARAM_STR);
        $sth->bindParam(':bairro', $this->bairro, PDO::PARAM_STR);
        $sth->bindParam(':cep', $this->cep, PDO::PARAM_STR);
        $sth->bindParam(':cidade', $this->cidade, PDO::PARAM_STR);
        $sth->bindParam(':uf', $this->uf, PDO::PARAM_STR);
        $sth->bindParam(':pais', $this->pais, PDO::PARAM_INT);
        $sth->bindParam(':telefoneResidencial', $this->telefoneResidencial, PDO::PARAM_STR);
        $sth->bindParam(':telefoneComercial', $this->telefoneComercial, PDO::PARAM_STR);
        $sth->bindParam(':celular', $this->celular, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idAuxiliarWeb) as lastid FROM auxiliarWeb";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                    $lastid = $vetor['lastid']; 
            }
            return $lastid;
        }else{
            return false;
        }
    }

    public function alteraAuxiliarWeb($idAuxiliarWeb) {

        $sql = "UPDATE auxiliarWeb SET  
                                    dataTermoCompromisso = :dataTermoCompromisso,    
                                    nome = :nome,
                                    codigoAfiliacao = :codigoAfiliacao,
                                    cpf = :cpf,
                                    email = :email,
                                    logradouro = :logradouro,
                                    numero = :numero,
                                    complemento = :complemento,
                                    bairro = :bairro,
                                    cep = :cep,
                                    cidade = :cidade,
                                    uf = :uf,
                                    pais = :pais,
                                    telefoneResidencial = :telefoneResidencial,
                                    telefoneComercial = :telefoneComercial,
                                    celular = :celular,
                                    ultimoAtualizar = :ultimoAtualizar
                                  WHERE idAuxiliarWeb = :idAuxiliarWeb";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        //$sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataTermoCompromisso', $this->dataTermoCompromisso, PDO::PARAM_STR);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':cpf', $this->cpf, PDO::PARAM_STR);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);
        $sth->bindParam(':logradouro', $this->logradouro, PDO::PARAM_STR);
        $sth->bindParam(':numero', $this->numero, PDO::PARAM_INT);
        $sth->bindParam(':complemento', $this->complemento, PDO::PARAM_STR);
        $sth->bindParam(':bairro', $this->bairro, PDO::PARAM_STR);
        $sth->bindParam(':cep', $this->cep, PDO::PARAM_STR);
        $sth->bindParam(':cidade', $this->cidade, PDO::PARAM_STR);
        $sth->bindParam(':uf', $this->uf, PDO::PARAM_STR);
        $sth->bindParam(':pais', $this->pais, PDO::PARAM_INT);
        $sth->bindParam(':telefoneResidencial', $this->telefoneResidencial, PDO::PARAM_STR);
        $sth->bindParam(':telefoneComercial', $this->telefoneComercial, PDO::PARAM_STR);
        $sth->bindParam(':celular', $this->celular, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':idAuxiliarWeb', $idAuxiliarWeb, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {

            return false;
        };
    }

    public function listaAuxiliarWeb() {

        $sql = "SELECT * FROM auxiliarWeb
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaExiste() {

        $sql = "SELECT * FROM auxiliarWeb where 1=1 
    			and nome = :nome
                        and fk_idOrganismoAfiliado =  :idOrganismoAfiliado
                ";
        
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdAuxiliarWeb($idAuxiliarWeb) {
        
        $sql = "SELECT * FROM  auxiliarWeb as a
        	inner join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                WHERE   idAuxiliarWeb = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idAuxiliarWeb, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeAuxiliarWeb($idAuxiliarWeb) {
        
        $sql = "DELETE FROM auxiliarWeb WHERE idAuxiliarWeb = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idAuxiliarWeb, PDO::PARAM_INT);

        $resultado = $c->run($sth,true);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>
