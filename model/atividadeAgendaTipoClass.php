<?php

require_once ("conexaoClass.php");

class AtividadeAgendaTipo{

    private $idAtividadeAgendaTipo;
    private $nome;
    private $descricao;
    private $classiOaTipoAtividadeAgenda;
    private $statusTipoAtividadeAgenda;
    private $usuario;
    private $usuarioAtualizacao;


    //--- GETERS E SETERS -----------------------------------------------------
    function getIdAtividadeAgendaTipo() {
        return $this->idAtividadeAgendaTipo;
    }

    function getNome() {
        return $this->nome;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getClassiOaTipoAtividadeAgenda() {
        return $this->classiOaTipoAtividadeAgenda;
    }

    function getStatusTipoAtividadeAgenda() {
        return $this->statusTipoAtividadeAgenda;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getUsuarioAtualizacao() {
        return $this->usuarioAtualizacao;
    }

    function setIdAtividadeAgendaTipo($idAtividadeAgendaTipo) {
        $this->idAtividadeAgendaTipo = $idAtividadeAgendaTipo;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setClassiOaTipoAtividadeAgenda($classiOaTipoAtividadeAgenda) {
        $this->classiOaTipoAtividadeAgenda = $classiOaTipoAtividadeAgenda;
    }

    function setStatusTipoAtividadeAgenda($statusTipoAtividadeAgenda) {
        $this->statusTipoAtividadeAgenda = $statusTipoAtividadeAgenda;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setUsuarioAtualizacao($usuarioAtualizacao) {
        $this->usuarioAtualizacao = $usuarioAtualizacao;
    }


    //--------------------------------------------------------------------------

    public function cadastroTipoAtividadeAgenda(){

        $sql = "INSERT INTO atividadeAgenda_tipo
                                 (nome,
                                  descricao,
                                  classiOaTipoAtividadeAgenda,
                                  statusTipoAtividadeAgenda,
                                  usuario,
                                  dataCadastro)
                            VALUES (:nome,
                                    :descricao,
                                    :classiOaTipoAtividadeAgenda,
                                    :statusTipoAtividadeAgenda,
                                    :usuario,
                                    NOW())";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':classiOaTipoAtividadeAgenda', $this->classiOaTipoAtividadeAgenda, PDO::PARAM_INT);
        $sth->bindParam(':statusTipoAtividadeAgenda', $this->statusTipoAtividadeAgenda, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraTipoAtividadeAgenda($idTipoAtividadeAgenda){

        $sql = "UPDATE atividadeAgenda_tipo SET
                                  nome = :nome,
                                  descricao =  :descricao,
			          classiOaTipoAtividadeAgenda = :classiOaTipoAtividadeAgenda,
                                  statusTipoAtividadeAgenda = :statusTipoAtividadeAgenda,
                                  usuarioAtualizacao = :usuarioAtualizacao,
                                  dataAtualizado = NOW()
                                  WHERE idAtividadeAgendaTipo = :idAtividadeAgendaTipo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':classiOaTipoAtividadeAgenda', $this->classiOaTipoAtividadeAgenda, PDO::PARAM_INT);
        $sth->bindParam(':statusTipoAtividadeAgenda', $this->statusTipoAtividadeAgenda, PDO::PARAM_INT);
        $sth->bindParam(':usuarioAtualizacao', $this->usuarioAtualizacao, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeAgendaTipo', $idTipoAtividadeAgenda, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        };
    }

    public function listaAtividadeAgendaTipo(){

        $sql="SELECT * FROM atividadeAgenda_tipo ORDER BY idAtividadeAgendaTipo ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaAtividadeAgendaTipoPelaClassificacaoOa($classificacaoOa){

        $sql = "SELECT * FROM atividadeAgenda_tipo where classiOaTipoAtividadeAgenda=:classiOaTipoAtividadeAgenda ORDER BY idTipoAtividadeAgenda ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':classiOaTipoAtividadeAgenda', $classificacaoOa, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdTipoAtividadeAgenda($idTipoAtividadeAgenda){
        $sql="SELECT * FROM  atividadeAgenda_tipo
                                WHERE   idAtividadeAgendaTipo = :idAtividadeAgendaTipo";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeAgendaTipo', $idTipoAtividadeAgenda, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    
    public function alteraStatusTipoAtividadeAgenda() {

        $sql = "UPDATE atividadeAgenda_tipo SET statusTipoAtividadeAgenda = :statusTipoAtividadeAgenda "
                . "WHERE idAtividadeAgendaTipo = :idAtividadeAgendaTipo";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusTipoAtividadeAgenda', $this->statusTipoAtividadeAgenda, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeAgendaTipo', $this->idAtividadeAgendaTipo, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            	$arr = array();
		$arr['status'] = $this->getStatusTipoAtividadeAgenda();
            return $arr;
        } else {
            return false;
        }
    }
    

}

?>
