<?php
require_once ("conexaoClass.php");

class RelatorioAtividadeMensal {

    public $idRelatorioAtividadeMensal;
    public $fk_idOrganismoAfiliado;
    public $mes;
    public $ano;
    public $caminhoRelatorioAtividadeMensal;
    public $usuario;

    /* Funções GET e SET */

    function getIdRelatorioAtividadeMensal() {
        return $this->idRelatorioAtividadeMensal;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getMes() {
        return $this->mes;
    }

    function getAno() {
        return $this->ano;
    }
    
	function getCaminhoRelatorioAtividadeMensal() {
        return $this->caminhoRelatorioAtividadeMensal;
    }
        
	function getUsuario() {
        return $this->usuario;
    }

    function setIdRelatorioAtividadeMensal($idRelatorioAtividadeMensal) {
        $this->idRelatorioAtividadeMensal = $idRelatorioAtividadeMensal;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }

	function setMes($mes) {
        $this->mes = $mes;
    }
    
	function setAno($ano) {
        $this->ano = $ano;
    }

    function setCaminhoRelatorioAtividadeMensal($caminhoRelatorioAtividadeMensal) {
        $this->caminhoRelatorioAtividadeMensal = $caminhoRelatorioAtividadeMensal;
    }
   
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function cadastraRelatorioAtividadeMensal() {
        
        $sql = "INSERT INTO relatorioAtividadeMensal
                                    (`fk_idOrganismoAfiliado`, 
                                    `mes`, 
                                    `ano`,
                                    `caminhoRelatorioAtividadeMensal`,
                                    `usuario`,
                                    `dataCadastro`
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :mes,
                                            :ano,
                                            '',
                                            :usuario,
                                            now()
                                            )";
        $id=1;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        //$sth->bindParam(':caminho',  $this->caminhoRelatorioAtividadeMensal, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
        	$resultado = $this->selecionaUltimoIdInserido();
			if($resultado)
			{
				foreach ($resultado as $vetor)
				{
					$id = $vetor['idRelatorioAtividadeMensal'];
				}
			}
        }
        return $id;
    }

    public function alteraRelatorioAtividadeMensal($id) {

        $sql = "UPDATE relatorioAtividadeMensal SET
        					 `mes` = :mes,
                                                 `ano` = :ano,
                                                 `caminhoRelatorioAtividadeMensal` = :caminho
                            WHERE `idRelatorioAtividadeMensal` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':caminho',  $this->caminhoRelatorioAtividadeMensal, PDO::PARAM_STR);
        $sth->bindParam(':id', $id , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function atualizaCaminhoRelatorioAtividadeMensalAssinado($id) {

        $sql = "UPDATE relatorioAtividadeMensal SET
                            `caminhoRelatorioAtividadeMensal` = :caminho
                            WHERE `idRelatorioAtividadeMensal` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':caminho',  $this->caminhoRelatorioAtividadeMensal, PDO::PARAM_STR);
        $sth->bindParam(':id', $id , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioAtividadeMensal'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM relatorioAtividadeMensal ORDER BY idRelatorioAtividadeMensal DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRelatorioAtividadeMensal($mes=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioAtividadeMensal as rfm
    		inner join organismoAfiliado as oa on rfm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on rfm.usuario = u.seqCadast
    	where 1=1 ";
        
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql."<br><br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($mes=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioAtividadeMensal where 1=1 ";
        
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function retornaIdRelatorioAtividadeMensal() {
    	
    	$sql = "SELECT * FROM relatorioAtividadeMensal
                                 WHERE `mes` = :mes
                                 and `ano` = :ano
                                 and `fk_idOrganismoAfiliado` =  :idOrganismoAfiliado
                                 ";
    	//echo $sql;
   	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        
        $id=0;
        if ($resultado){
        	foreach ($resultado as $vetor)
        	{
        		$id = $vetor['idRelatorioAtividadeMensal'];
        	}
        }
        return $id;
    }

    public function buscarIdRelatorioAtividadeMensal($idRelatorioAtividadeMensal) {
        
        $sql = "SELECT * FROM relatorioAtividadeMensal
                                 WHERE `idRelatorioAtividadeMensal` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioAtividadeMensal, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioAtividadeMensal($id){
        
        $sql="DELETE FROM relatorioAtividadeMensal WHERE idRelatorioAtividadeMensal = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}
?>