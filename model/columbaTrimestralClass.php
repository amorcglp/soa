<?php
require_once ("conexaoClass.php");

class columbaTrimestral {

    public $idColumbaTrimestral;
    public $fk_idOrganismoAfiliado;
    public $trimestre;
    public $ano;
    public $entregue;
    public $dataEntrega;
    public $quemEntregou;

    /**
     * @return mixed
     */
    public function getIdColumbaTrimestral()
    {
        return $this->idColumbaTrimestral;
    }

    /**
     * @param mixed $idColumbaTrimestral
     */
    public function setIdColumbaTrimestral($idColumbaTrimestral)
    {
        $this->idColumbaTrimestral = $idColumbaTrimestral;
    }

    /**
     * @return mixed
     */
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }

    /**
     * @param mixed $fk_idOrganismoAfiliado
     */
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getTrimestre()
    {
        return $this->trimestre;
    }

    /**
     * @param mixed $trimestre
     */
    public function setTrimestre($trimestre)
    {
        $this->trimestre = $trimestre;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getEntregue()
    {
        return $this->entregue;
    }

    /**
     * @param mixed $entregue
     */
    public function setEntregue($entregue)
    {
        $this->entregue = $entregue;
    }

    /**
     * @return mixed
     */
    public function getDataEntrega()
    {
        return $this->dataEntrega;
    }

    /**
     * @param mixed $dataEntrega
     */
    public function setDataEntrega($dataEntrega)
    {
        $this->dataEntrega = $dataEntrega;
    }

    /**
     * @return mixed
     */
    public function getQuemEntregou()
    {
        return $this->quemEntregou;
    }

    /**
     * @param mixed $quemEntregou
     */
    public function setQuemEntregou($quemEntregou)
    {
        $this->quemEntregou = $quemEntregou;
    }





    public function cadastraColumbaTrimestral() {
        
        $sql = "INSERT INTO columbaTrimestral 
                                    (
                                    fk_idOrganismoAfiliado, 
                                    trimestre, 
                                    ano,
                                    entregue,
                                    dataEntrega,
                                    quemEntregou
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :trimestre,
                                            :ano,
                                            :entregue,
                                            now(),
                                            :quemEntregou
                                            )";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_STR);
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_STR);
        $sth->bindParam(':entregue', $this->entregue, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregou', $this->quemEntregou, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function entregaColumbaTrimestral() {

        $sql = "UPDATE columbaTrimestral SET 
        			 entregue = 1,
                            WHERE trimestre = :trimestre and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_STR);
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaColumbaTrimestral($trimestre=null,$ano=null,$fk_idOrganismoAfiliado=null,$entregueCompletamente=false) {
        
    	$sql = "SELECT * FROM columbaTrimestral as fm
    		inner join organismoAfiliado as oa on fm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on fm.quemEntregou = u.seqCadast
    	where 1=1 ";
    	if($trimestre!=null)
    	{
    		$sql .= " and trimestre=:trimestre";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        if($entregueCompletamente)
        {
            $sql .= " and entregueCompletamente=1";
        }
    	$sql .= " order by ano, trimestre ";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($trimestre!=null)
    	{
            $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_STR);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }


    public function buscarIdColumbaTrimestral($id) {
        
        $sql = "SELECT * FROM columbaTrimestral 
                                 WHERE idColumbaTrimestral = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeColumbaTrimestral($id){
        
        $sql="DELETE FROM columbaTrimestral WHERE idColumbaTrimestral = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaNumeroAssinatura($trimestreAtual, $anoAtual,$idOrganismoAfiliado,$numeroAssinatura)
    {

        $sql = "UPDATE columbaTrimestral SET
        						  numeroAssinatura =  :numeroAssinatura
                                  WHERE trimestre = :trimestreAtual and ano = :anoAtual and fk_idOrganismoAfiliado = :idOrganismoAfiliado ";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':numeroAssinatura', $numeroAssinatura, PDO::PARAM_STR);
        $sth->bindParam(':trimestreAtual', $trimestreAtual, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function assinaturaEletronicaColumbaTrimestral($seqCadast,$ip,$idOrganismoAfiliado,$funcao,$trimestre,$ano,$codigoAssinaturaIndividual){

        //echo "idAta=>".$idAta;
        $sql = "INSERT INTO columbaTrimestral_assinatura_eletronica
                                 (ano,
                                  trimestre,
                                  fk_idOrganismoAfiliado,
                                  codigoAssinaturaIndividual,
                                  seqCadast,
                                  fk_idFuncao,
                                  ip,
                                  data
                                  )
                            VALUES (:ano,
                                    :trimestre,
                                    :fk_idOrganismoAfiliado,
                                    :codigoAssinaturaIndividual,
                                    :seqCadast,
                                    :fk_idFuncao,
                                    :ip,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth->bindParam(':fk_idFuncao', $funcao, PDO::PARAM_INT);
        $sth->bindParam(':codigoAssinaturaIndividual', $codigoAssinaturaIndividual, PDO::PARAM_INT);



        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarAssinaturasEmColumbaTrimestral($seqCadast=null,$idOrganismoAfiliado=null,$codigoIndividual=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  columbaTrimestral_assinatura_eletronica as a
                left join columbaTrimestral as fm on fm.fk_idOrganismoAfiliado = a.fk_idOrganismoAfiliado and fm.trimestre = a.trimestre and fm.ano = a.ano
                left join usuario as u on u.seqCadast = a.seqCadast
                left join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                where 1=1 ";

        if($seqCadast!=null)
        {
            $sql .= " and a.seqCadast = :seqCadast";
        }

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and a.fk_idOrganismoAfiliado = :idOrganismoAfiliado group by idColumbaTrimestralAssinaturaEletronica";
        }

        if($codigoIndividual!=null)
        {
            $sql .= " and a.codigoAssinaturaIndividual = :codigoIndividual";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null) {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if($codigoIndividual!=null)
        {
            $sth->bindParam(':codigoIndividual', $codigoIndividual, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);

        if($codigoIndividual!=null)
        {
            return $resultado;
        }else {
            if ($idOrganismoAfiliado == null) {
                if ($resultado) {
                    $arr = array();
                    foreach ($resultado as $v) {
                        $arr[] = $v['trimestre'] . "@@" . $v['ano'] . "@@" . $v['fk_idOrganismoAfiliado'];
                    }
                    return $arr;
                } else {
                    return false;
                }
            } else {
                return $resultado;
            }
        }
    }

    public function buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestreAtual,$anoAtual,$idOrganismoAfiliado,$idFuncao=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  columbaTrimestral_assinatura_eletronica as a
                left join usuario as u on u.seqCadast = a.seqCadast
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE  trimestre = :trimestreAtual and ano = :anoAtual and fk_idOrganismoAfiliado = :idOrganismoAfiliado ";
        if($idFuncao!=null)
        {
            $sql .= " and fk_idFuncao = :idFuncao";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':trimestreAtual', $trimestreAtual, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if($idFuncao!=null)
        {
            $sth->bindParam(':idFuncao', $idFuncao, PDO::PARAM_INT);
        }

        if($idFuncao==null)
        {
            $resultado = $c->run($sth);
            if ($resultado){
                return $resultado;
            } else {
                return false;
            }
        }else{
            $resultado = $c->run($sth,null,null,true);
            if ($resultado){
                return true;
            } else {
                return false;
            }
        }

    }

    public function entregarCompletamente($trimestreAtual,$anoAtual,$idOrganismoAfiliado,$quemEntregouCompletamente)
    {
        $sql = "UPDATE columbaTrimestral SET
        						  entregueCompletamente =  1,
        						  quemEntregouCompletamente = :quemEntregouCompletamente,
        						  dataEntregouCompletamente = now()
                                  WHERE trimestre = :trimestreAtual
                                  and ano = :anoAtual
                                  and fk_idOrganismoAfiliado = :idOrganismoAfiliado ";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':trimestreAtual', $trimestreAtual, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregouCompletamente', $quemEntregouCompletamente, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinatura($codigo){

        $sql = "SELECT * FROM  columbaTrimestral as a
                WHERE   numeroAssinatura = :codigo";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinaturaIndividual($codigo,$codigoIndividual){

        $sql = "SELECT * FROM  columbaTrimestral as f
                inner join columbaTrimestral_assinatura_eletronica as fmae on fmae.ano = f.ano and fmae.trimestre = f.trimestre and fmae.fk_idOrganismoAfiliado =f.fk_idOrganismoAfiliado 
                WHERE   numeroAssinatura = :codigo and codigoAssinaturaIndividual = :codigoIndividual";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);
        $sth->bindParam(':codigoIndividual', $codigoIndividual , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function updateAtividadeColumbaTrimestral()
    {
        $sql = "UPDATE columbaTrimestral SET
        						  quemEntregou =  :quemEntregou,
        						  entregue = :entregue
                                  WHERE trimestre = :trimestre and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregou', $this->quemEntregou, PDO::PARAM_INT);
        $sth->bindParam(':entregue', $this->entregue, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


}
?>