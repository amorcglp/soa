<?php

require_once ("conexaoClass.php");

class planoAcaoOrganismoMeta{

    private $idPlanoAcaoOrganismoMeta;
    private $fk_idPlanoAcaoOrganismo;
    private $seqCadast;
    private $statusMeta;
    private $tituloMeta;
    private $inicio;
    private $fim;
    private $oque;
    private $porque;
    private $quem;
    private $onde;
    private $quando;
    private $como;
    private $quanto;
    private $ordenacao;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoAcaoOrganismoMeta() {
        return $this->idPlanoAcaoOrganismoMeta;
    }

    function getFk_idPlanoAcaoOrganismo() {
        return $this->fk_idPlanoAcaoOrganismo;
    }
    
	function getSeqCadast() {
        return $this->seqCadast;
    }
    
	function getStatusMeta() {
        return $this->statusMeta;
    }

    function getTituloMeta() {
        return $this->tituloMeta;
    } 
    
    function getInicio() {
        return $this->inicio;
    } 
    
    function getFim() {
        return $this->fim;
    } 
    
    function getOque() {
        return $this->oque;
    } 
    
    function getPorque() {
        return $this->porque;
    } 
    
    function getQuem() {
        return $this->quem;
    } 
    
    function getOnde() {
        return $this->onde;
    } 
    
    function getQuando() {
        return $this->quando;
    } 
    
    function getComo() {
        return $this->como;
    } 
    
    function getQuanto() {
        return $this->quanto;
    } 
    
    function getOrdenacao() {
        return $this->ordenacao;
    } 

    function setIdPlanoAcaoOrganismoMeta($idPlanoAcaoOrganismoMeta) {
        $this->idPlanoAcaoOrganismoMeta = $idPlanoAcaoOrganismoMeta;
    }
  
	function setFk_idPlanoAcaoOrganismo($fk_idPlanoAcaoOrganismo) {
        $this->fk_idPlanoAcaoOrganismo = $fk_idPlanoAcaoOrganismo;
    }
    
	function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }
    
	function setStatusMeta($statusMeta) {
        $this->statusMeta = $statusMeta;
    }
    
	function setTituloMeta($tituloMeta) {
        $this->tituloMeta = $tituloMeta;
    }
    
	function setInicio($inicio) {
        $this->inicio = $inicio;
    }
    
	function setFim($fim) {
        $this->fim = $fim;
    }
    
	function setOque($oque) {
        $this->oque = $oque;
    }
    
	function setPorque($porque) {
        $this->porque = $porque;
    }
    
	function setQuem($quem) {
        $this->quem = $quem;
    }
    
	function setOnde($onde) {
        $this->onde = $onde;
    }
    
	function setQuando($quando) {
        $this->quando = $quando;
    }
    
	function setComo($como) {
        $this->como = $como;
    }
    
	function setQuanto($quanto) {
        $this->quanto = $quanto;
    }
    
	function setOrdenacao($ordenacao) {
        $this->ordenacao = $ordenacao;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoAcaoOrganismoMeta(){

        $sql="INSERT INTO planoAcaoOrganismo_meta  
                                 (
                                  fk_idPlanoAcaoOrganismo,
                                  seqCadast,
                                  statusMeta,
                                  tituloMeta,
                                  inicio,
                                  fim,
                                  oque,
                                  porque,
                                  quem,
                                  onde,
                                  quando,
                                  como,
                                  quanto,
                                  ordenacao,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idPlanoAcaoOrganismo,
                                    :seqCadast,
                                    :status,
                                    :titulo,
                                    :inicio,
                                    :fim,
                                    :oque,
                                    :porque,
                                    :quem,
                                    :onde,
                                    :quando,
                                    :como,
                                    :quanto,
                                    :ordenacao,
                                    now()
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoOrganismo',$this->fk_idPlanoAcaoOrganismo, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':status',  $this->statusMeta, PDO::PARAM_INT);
        $sth->bindParam(':titulo', $this->tituloMeta, PDO::PARAM_STR);
        $sth->bindParam(':inicio', $this->inicio, PDO::PARAM_STR);
        $sth->bindParam(':fim',  $this->fim, PDO::PARAM_STR);
        $sth->bindParam(':oque', $this->oque, PDO::PARAM_STR);
        $sth->bindParam(':porque', $this->porque, PDO::PARAM_STR);
        $sth->bindParam(':quem', $this->quem, PDO::PARAM_STR);
        $sth->bindParam(':onde',  $this->onde, PDO::PARAM_STR);
        $sth->bindParam(':quando', $this->quando, PDO::PARAM_STR);
        $sth->bindParam(':como', $this->como, PDO::PARAM_STR);
        $sth->bindParam(':quanto',  $this->quanto, PDO::PARAM_STR);
        $sth->bindParam(':ordenacao', $this->ordenacao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusMeta() {
            
	$sql = "UPDATE planoAcaoOrganismo_meta SET `statusMeta` = :status
                               WHERE `idPlanoAcaoOrganismoMeta` = :id";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status',$this->statusMeta, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idPlanoAcaoOrganismoMeta, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusMeta();
            return $arr;
        } else {
            return false;
        }
    }
   
    
    public function listaMeta($idPlanoAcaoOrganismo=null,$statusMeta=null,$id=null){

    	$sql = "SELECT * FROM planoAcaoOrganismo_meta
    	where 1=1 
    	 ";
    	if($id!=null)
    	{
    		$sql .= " and idPlanoAcaoOrganismoMeta=:id";
    	}   
    	if($idPlanoAcaoOrganismo!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoOrganismo=:idPlanoAcaoOrganismo";
    	}
    	if($statusMeta!=null)
    	{
    		$sql .= " and statusMeta=:status";
    	}	 
    	$sql .= " order by ordenacao asc";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($idPlanoAcaoOrganismo!=null)
    	{
            $sth->bindParam(':idPlanoAcaoOrganismo', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        }
        if($statusMeta!=null)
    	{
            $sth->bindParam(':status', $statusMeta, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        //echo "<pre>";print_r($resultado);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
	public function listaMetaOrganismo($idOrganismoAfiliado=null,$statusMeta=0,$ano=null,$statusNaoMeta=0){
		//echo "statusmeta=>".$statusMeta."<br><br>";
    	$sql = "SELECT * FROM planoAcaoOrganismo_meta
    	left join planoAcaoOrganismo on fk_idPlanoAcaoOrganismo = idPlanoAcaoOrganismo 
    	where 1=1 
    	 "; 
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	if($statusMeta!=null)
    	{
    		$sql .= " and statusMeta=:status";
    	}else{
                if($statusNaoMeta!=null)
	    	{
	    		$sql .= " and statusMeta=0";
	    	}
    	}	 
    	$sql .= " and statusPlano=1 ";
    	if($ano!=null)
    	{
    		$sql .= " and YEAR(inicio)=:ano";
    	}
    	$sql .= " group by idPlanoAcaoOrganismoMeta";
    	//echo $sql."<br><br>";
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($statusMeta!=null)
    	{
            $sth->bindParam(':status', $statusMeta, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaUltimaMeta($idPlanoAcaoOrganismo){

    	$sql = "SELECT * FROM planoAcaoOrganismo_meta
    	where 1=1 
    	 ";   
    	if($idPlanoAcaoOrganismo!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoOrganismo=:id";
    	}	 
    	$sql .= " order by dataCadastro desc limit 1";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idPlanoAcaoOrganismo!=null)
    	{
            $sth->bindParam(':id', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
        	foreach($resultado as $vetor)
        	{
        		$ultimaMeta = $vetor['dataCadastro'];
        	}
            return $ultimaMeta;
        }else{
            return "";
        }
    }

    public function removeMeta(){
        
        $sql="DELETE FROM planoAcaoOrganismo_meta WHERE idPlanoAcaoOrganismoMeta = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idPlanoAcaoOrganismoMeta, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }  

    public function removeMetas($idPlanoAcaoOrganismo){
        
        $sql="DELETE FROM planoAcaoOrganismo_meta WHERE fk_idPlanoAcaoOrganismo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>