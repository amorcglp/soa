<?php
require_once ("conexaoClass.php");

class autorizacaoTemplo {

    public $idAutorizacaoTemplo;
    public $fk_idOrganismoAfiliado;
    public $seqCadastMembro;
    public $usuario;

    /* FunÃ§Ãµes GET e SET */

    function getIdAutorizacaoTemplo() {
        return $this->idAutorizacaoTemplo;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getSeqCadastMembro() {
        return $this->seqCadastMembro;
    }

	function getUsuario() {
        return $this->usuario;
    }

    function setIdAutorizacaoTemplo($idAutorizacaoTemplo) {
        $this->idAutorizacaoTemplo = $idAutorizacaoTemplo;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }

	function setSeqCadastMembro($seqCadastMembro) {
        $this->seqCadastMembro = $seqCadastMembro;
    }

	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function cadastra() {
    	$sql = "INSERT INTO autorizacaoTemplo
        							(`fk_idOrganismoAfiliado`,
        							`seqCadastMembro`,
        							`usuario`,
        							`dataCadastro`
        							)
                                    VALUES (
                                            :fk_idOrganismoAfiliado,
                                            :seqCadastMembro,
                                            :usuario,
                                            now()
                                            )";
    	//echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastMembro', $this->seqCadastMembro, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE 'autorizacaoTemplo'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

	public function selecionaUltimoIdInserido(){

        $sql= "SELEC * FROM autorizacaoTemplo ORDER BY idAutorizacaoTemplo DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function selecionaTotalSolicitacoes() {

    	$sql = "SELECT * FROM autorizacaoTemplo where 1=1
    		and seqCadastMembro=:seqCadastMembro
    		and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado";

    	//echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadastMembro', $this->seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeSolicitacao($idAutorizacaoTemplo) {

        $query = "DELETE FROM autorizacaoTemplo WHERE `idAutorizacaoTemplo` = :idAutorizacaoTemplo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':idAutorizacaoTemplo', $idAutorizacaoTemplo, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscarIdSolicitacao($idAutorizacaoTemplo) {

        $sql = "SELECT * FROM autorizacaoTemplo WHERE `idAutorizacaoTemplo` = :idAutorizacaoTemplo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAutorizacaoTemplo', $idAutorizacaoTemplo, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
}
?>