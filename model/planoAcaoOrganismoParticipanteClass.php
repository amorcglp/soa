<?php

require_once ("conexaoClass.php");

class planoAcaoOrganismoParticipante{

    private $idPlanoAcaoOrganismoParticipante;
    private $fk_idPlanoAcaoOrganismo;
    private $seqCadast;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoAcaoOrganismoParticipante() {
        return $this->idPlanoAcaoOrganismoParticipante;
    }

    function getFk_idPlanoAcaoOrganismo() {
        return $this->fk_idPlanoAcaoOrganismo;
    }
    
	function getSeqCadast() {
        return $this->seqCadast;
    }

    function setIdPlanoAcaoOrganismoParticipante($idPlanoAcaoOrganismoParticipante) {
        $this->idPlanoAcaoOrganismoParticipante = $idPlanoAcaoOrganismoParticipante;
    }
  
	function setFk_idPlanoAcaoOrganismo($fk_idPlanoAcaoOrganismo) {
        $this->fk_idPlanoAcaoOrganismo = $fk_idPlanoAcaoOrganismo;
    }
    
	function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoAcaoOrganismoParticipante(){

        $sql="INSERT INTO planoAcaoOrganismo_participante  
                                 (
                                  fk_idPlanoAcaoOrganismo,
                                  seqCadast
                                  )
                                   
                            VALUES (:idPlanoAcaoOrganismo,
                                    :seqCadast)";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoOrganismo', $this->fk_idPlanoAcaoOrganismo, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function listaParticipantes($idPlanoAcaoOrganismo=null,$seqCadast=null){

    	$sql = "SELECT * FROM planoAcaoOrganismo_participante
    	where 1=1 
    	 ";   
    	if($idPlanoAcaoOrganismo!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoOrganismo=:idPlanoAcaoOrganismo";
    	}
    	if($seqCadast!=null)
    	{
    		$sql .= " and seqCadast=:seqCadast";
    	}	 
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoOrganismo!=null)
    	{
            $sth->bindParam(':idPlanoAcaoOrganismo', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        }
        if($seqCadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeParticipantes($idPlanoAcaoOrganismo=null,$id=null)
    {
    	$sql= "DELETE FROM planoAcaoOrganismo_participante WHERE 1=1";
    	if($idPlanoAcaoOrganismo!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoOrganismo = :idPlanoAcaoOrganismo";
    	}
    	if($id!=null)
    	{
    		$sql .= " and idPlanoAcaoOrganismoParticipante = :id";
    	}
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoOrganismo!=null)
    	{
            $sth->bindParam(':idPlanoAcaoOrganismo', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        }
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }   
}

?>
