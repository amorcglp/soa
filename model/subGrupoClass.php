<?php

require_once ("conexaoClass.php");

class Subgrupo {
    # Seleciona Evento 

    public function selecionaSubgrupos($id = null, $idgrupo = null, $idusuario = null, $vinculo_externo = null) {
        
        $sql = "SELECT sub.*, g.grupo, sub.vinculo_externo";
        if ($idusuario != 0 && $idusuario != "") {
            $sql .= " , us.sigla_orgafi ";
        }
        $sql .= "
		from subgrupo as sub
		inner join grupo as g on fkgrupo = idgrupo ";
        if ($idusuario != 0 && $idusuario != "") {
            $sql .= " inner join  usuario_subgrupo as us on sub.idsubgrupo = us.fksubgrupo ";
        }
        $sql .= " where 1=1 ";
        if ($id != 0 && $id != "") {
            $sql .= " and idsubgrupo=:id";
        }
        if ($idgrupo != 0 && $idgrupo != "") {
            $sql .= " and fkgrupo=:idgrupo";
        }
        if ($idusuario != 0 && $idusuario != "") {
            $sql .= " and us.fkusuario=:idusuario";
        }
        if ($vinculo_externo != 0 && $vinculo_externo != "" && $vinculo_externo != null) {
            $sql .= " and sub.vinculo_externo=:vinculoExterno";
        }
        $sql .= " order by subgrupo";
        //echo $sql."<br><br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($id != 0 && $id != "") {
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if ($idgrupo != 0 && $idgrupo != "") {
            $sth->bindParam(':idgrupo', $idgrupo, PDO::PARAM_INT);
        }
        if ($idusuario != 0 && $idusuario != "") {
            $sth->bindParam(':idusuario', $idusuario, PDO::PARAM_INT);
        }
        if ($vinculo_externo != 0 && $vinculo_externo != "" && $vinculo_externo != null) {
            $sth->bindParam(':vinculoExterno', $vinculo_externo, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        $subgrupos=array();
        if ($resultado){
            foreach ($resultado as $dados) {
                $subgrupos['idsubgrupo'][] = $dados['idsubgrupo'];
                $subgrupos['subgrupo'][] = $dados['subgrupo'];
                $subgrupos['fkgrupo'][] = $dados['fkgrupo'];
                $subgrupos['vinculo_externo'][] = $dados['vinculo_externo'];
                $subgrupos['grupo'][] = utf8_encode($dados['grupo']);
                $subgrupos['descricao'][] = utf8_encode($dados['descricao']);
                if ($idusuario != 0 && $idusuario != "") {
                    $subgrupos['sigla_orgafi'][$dados['idsubgrupo']] = $dados['sigla_orgafi'];
                }
            }
        }

        return $subgrupos;
    }

}

?>