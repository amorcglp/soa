<?php

require_once ("conexaoClass.php");

class atividadeIniciatica{

    private $idAtividadeIniciatica;
    private $tipoAtividadeIniciatica;
    private $localAtividadeIniciatica;
    private $dataRealizadaAtividadeIniciatica;
    private $horaRealizadaAtividadeIniciatica;
    private $dataRegistroAtividadeIniciatica;
    private $dataAtualizadaAtividadeIniciatica;
    private $anotacoesAtividadeIniciatica;
    private $statusAtividadeIniciatica;
    private $fk_seqCadastAtualizadoPor;
    private $fk_idAtividadeIniciaticaMembro;
    private $fk_idAtividadeIniciaticaOficial;
    private $fk_idAtividadeIniciaticaColumba;
    private $fk_seqCadastRecepcao;
    private $codAfiliacaoRecepcao;
    private $nomeRecepcao;
    private $fk_idOrganismoAfiliado;


    //--- GETERS -----------------------------------------------------
    public function getIdAtividadeIniciatica() {
        return $this->idAtividadeIniciatica;
    }
    public function getTipoAtividadeIniciatica() {
        return $this->tipoAtividadeIniciatica;
    }
    public function getLocalAtividadeIniciatica() {
        return $this->localAtividadeIniciatica;
    }
    public function getDataRealizadaAtividadeIniciatica() {
        return $this->dataRealizadaAtividadeIniciatica;
    }
    public function getHoraRealizadaAtividadeIniciatica() {
        return $this->horaRealizadaAtividadeIniciatica;
    }
    public function getDataRegistroAtividadeIniciatica() {
        return $this->dataRegistroAtividadeIniciatica;
    }
    public function getDataAtualizadaAtividadeIniciatica() {
        return $this->dataAtualizadaAtividadeIniciatica;
    }
    public function getAnotacoesAtividadeIniciatica() {
        return $this->anotacoesAtividadeIniciatica;
    }
    public function getStatusAtividadeIniciatica() {
        return $this->statusAtividadeIniciatica;
    }
    public function getFk_seqCadastAtualizadoPor() {
        return $this->fk_seqCadastAtualizadoPor;
    }
    public function getFk_idAtividadeIniciaticaMembro() {
        return $this->fk_idAtividadeIniciaticaMembro;
    }
    public function getFk_idAtividadeIniciaticaOficial() {
        return $this->fk_idAtividadeIniciaticaOficial;
    }
    public function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    public function getFkIdAtividadeIniciaticaColumba() {
        return $this->fk_idAtividadeIniciaticaColumba;
    }
    public function getFkSeqCadastRecepcao() {
        return $this->fk_seqCadastRecepcao;
    }
    public function getCodAfiliacaoRecepcao() {
        return $this->codAfiliacaoRecepcao;
    }
    public function getNomeRecepcao() {
        return $this->nomeRecepcao;
    }

    //--- SETERS -----------------------------------------------------
    public function setIdAtividadeIniciatica($idAtividadeIniciatica) {
        $this->idAtividadeIniciatica = $idAtividadeIniciatica;
    }
    public function setTipoAtividadeIniciatica($tipoAtividadeIniciatica) {
        $this->tipoAtividadeIniciatica = $tipoAtividadeIniciatica;
    }
    public function setLocalAtividadeIniciatica($localAtividadeIniciatica) {
        $this->localAtividadeIniciatica = $localAtividadeIniciatica;
    }
    public function setDataRealizadaAtividadeIniciatica($dataRealizadaAtividadeIniciatica) {
        $this->dataRealizadaAtividadeIniciatica = $dataRealizadaAtividadeIniciatica;
    }
    public function setHoraRealizadaAtividadeIniciatica($horaRealizadaAtividadeIniciatica) {
        $this->horaRealizadaAtividadeIniciatica = $horaRealizadaAtividadeIniciatica;
    }
    public function setDataRegistroAtividadeIniciatica($dataRegistroAtividadeIniciatica) {
        $this->dataRegistroAtividadeIniciatica = $dataRegistroAtividadeIniciatica;
    }
    public function setDataAtualizadaAtividadeIniciatica($dataAtualizadaAtividadeIniciatica) {
        $this->dataAtualizadaAtividadeIniciatica = $dataAtualizadaAtividadeIniciatica;
    }
    public function setAnotacoesAtividadeIniciatica($anotacoesAtividadeIniciatica) {
        $this->anotacoesAtividadeIniciatica = $anotacoesAtividadeIniciatica;
    }
    public function setStatusAtividadeIniciatica($statusAtividadeIniciatica) {
        $this->statusAtividadeIniciatica = $statusAtividadeIniciatica;
    }
    public function setFk_seqCadastAtualizadoPor($fk_seqCadastAtualizadoPor) {
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }
    public function setFk_idAtividadeIniciaticaMembro($fk_idAtividadeIniciaticaMembro) {
        $this->fk_idAtividadeIniciaticaMembro = $fk_idAtividadeIniciaticaMembro;
    }
    public function setFk_idAtividadeIniciaticaOficial($fk_idAtividadeIniciaticaOficial) {
        $this->fk_idAtividadeIniciaticaOficial = $fk_idAtividadeIniciaticaOficial;
    }
    public function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }
    public function setFkIdAtividadeIniciaticaColumba($fk_idAtividadeIniciaticaColumba) {
        $this->fk_idAtividadeIniciaticaColumba = $fk_idAtividadeIniciaticaColumba;
    }
    public function setFkSeqCadastRecepcao($fk_seqCadastRecepcao) {
        $this->fk_seqCadastRecepcao = $fk_seqCadastRecepcao;
    }
    public function setCodAfiliacaoRecepcao($codAfiliacaoRecepcao) {
        $this->codAfiliacaoRecepcao = $codAfiliacaoRecepcao;
    }
    public function setNomeRecepcao($nomeRecepcao) {
        $this->nomeRecepcao = $nomeRecepcao;
    }


    //--------------------------------------------------------------------------

    public function cadastroAtividadeIniciatica(){

        $sql = "INSERT INTO atividadeIniciatica
                                 (tipoAtividadeIniciatica,
                                  localAtividadeIniciatica,
                                  dataRealizadaAtividadeIniciatica,
                                  horaRealizadaAtividadeIniciatica,
                                  dataRegistroAtividadeIniciatica,
                                  anotacoesAtividadeIniciatica,
                                  agilAtividadeIniciatica,
                                  fk_seqCadastAtualizadoPor,
                                  fk_idAtividadeIniciaticaOficial,
                                  fk_idAtividadeIniciaticaColumba,
                                  fk_seqCadastRecepcao,
                                  codAfiliacaoRecepcao,
                                  nomeRecepcao,
                                  fk_idOrganismoAfiliado)
                            VALUES (:tipoAtividadeIniciatica,
                                    :localAtividadeIniciatica,
                                    :dataRealizadaAtividadeIniciatica,
                                    :horaRealizadaAtividadeIniciatica,
                                    NOW(),
                                    :anotacoesAtividadeIniciatica,
                                    '0',
                                    :fk_seqCadastAtualizadoPor,
                                    :fk_idAtividadeIniciaticaOficial,
                                    :fk_idAtividadeIniciaticaColumba,
                                    :fk_seqCadastRecepcao,
                                    :codAfiliacaoRecepcao,
                                    :nomeRecepcao,
                                    :fk_idOrganismoAfiliado)";

        //echo $query;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipoAtividadeIniciatica', $this->tipoAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':localAtividadeIniciatica', $this->localAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':dataRealizadaAtividadeIniciatica', $this->dataRealizadaAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':horaRealizadaAtividadeIniciatica', $this->horaRealizadaAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':anotacoesAtividadeIniciatica', $this->anotacoesAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeIniciaticaOficial', $this->fk_idAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeIniciaticaColumba', $this->fk_idAtividadeIniciaticaColumba, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastRecepcao', $this->fk_seqCadastRecepcao, PDO::PARAM_INT);
        $sth->bindParam(':codAfiliacaoRecepcao', $this->codAfiliacaoRecepcao, PDO::PARAM_INT);
        $sth->bindParam(':nomeRecepcao', $this->nomeRecepcao, PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $data = array(
            'tipoAtividadeIniciatica'=>$this->tipoAtividadeIniciatica,
            'localAtividadeIniciatica'=>$this->localAtividadeIniciatica,
            'dataRealizadaAtividadeIniciatica'=>$this->dataRealizadaAtividadeIniciatica,
            'horaRealizadaAtividadeIniciatica'=>$this->horaRealizadaAtividadeIniciatica,
            'anotacoesAtividadeIniciatica'=>$this->anotacoesAtividadeIniciatica,
            'fk_seqCadastAtualizadoPor'=>$this->fk_seqCadastAtualizadoPor,
            'fk_idAtividadeIniciaticaOficial'=>$this->fk_idAtividadeIniciaticaOficial,
            'fk_idAtividadeIniciaticaColumba'=>$this->fk_idAtividadeIniciaticaColumba,
            'fk_seqCadastRecepcao'=>$this->fk_seqCadastRecepcao,
            'codAfiliacaoRecepcao'=>$this->codAfiliacaoRecepcao,
            'nomeRecepcao'=>$this->nomeRecepcao,
            'fk_idOrganismoAfiliado'=>$this->fk_idOrganismoAfiliado
        );
        //echo $this->parms("cadastroAtividadeIniciatica",$sql,$data);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }

    }

    public function alteraAtividadeIniciatica($idAtividadeIniciatica)
    {
        $sql = "UPDATE atividadeIniciatica SET
                                  tipoAtividadeIniciatica = :tipoAtividadeIniciatica,
                                  localAtividadeIniciatica = :localAtividadeIniciatica,
                                  dataRealizadaAtividadeIniciatica = :dataRealizadaAtividadeIniciatica,
                                  horaRealizadaAtividadeIniciatica = :horaRealizadaAtividadeIniciatica,
                                  dataAtualizadaAtividadeIniciatica = NOW(),
                                  anotacoesAtividadeIniciatica = :anotacoesAtividadeIniciatica,
                                  fk_seqCadastAtualizadoPor = :fk_seqCadastAtualizadoPor,
                                  fk_idAtividadeIniciaticaOficial = :fk_idAtividadeIniciaticaOficial,
                                  fk_idAtividadeIniciaticaColumba = :fk_idAtividadeIniciaticaColumba,
                                  fk_seqCadastRecepcao = :fk_seqCadastRecepcao,
                                  codAfiliacaoRecepcao = :codAfiliacaoRecepcao,
                                  nomeRecepcao = :nomeRecepcao
                                  WHERE idAtividadeIniciatica = :idAtividadeIniciatica";

        //echo $query;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipoAtividadeIniciatica', $this->tipoAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':localAtividadeIniciatica', $this->localAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':dataRealizadaAtividadeIniciatica', $this->dataRealizadaAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':horaRealizadaAtividadeIniciatica', $this->horaRealizadaAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':anotacoesAtividadeIniciatica', $this->anotacoesAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeIniciaticaOficial', $this->fk_idAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeIniciaticaColumba', $this->fk_idAtividadeIniciaticaColumba, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastRecepcao', $this->fk_seqCadastRecepcao, PDO::PARAM_INT);
        $sth->bindParam(':codAfiliacaoRecepcao', $this->codAfiliacaoRecepcao, PDO::PARAM_INT);
        $sth->bindParam(':nomeRecepcao', $this->nomeRecepcao, PDO::PARAM_STR);
        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeIniciatica($idOrganismoAfiliado=null,$tipoAtividadeIniciatica=null,$agil=null,$statusAtividadeIniciatica=null,$start=null,$limit=null,$membros=false,$arr=array(),$arrMembrosSemRegistro=array()){

        /*
        echo "idOa:".$idOrganismoAfiliado;
        echo "statusAtividade:".$statusAtividadeIniciatica;
        echo "start:".$start;
        echo "limit:".$limit;
*/
        $seqCadastsCPD=array();

        if(!$membros) {
            $query = "SELECT * FROM atividadeIniciatica as ai ";
        }else{
            $query = "SELECT  aim.*,
                              ai.*,
                              oa.*
                              
                               FROM atividadeIniciaticaMembro as aim ";
        }

    	if($membros) {
            $query .= " left join atividadeIniciatica as ai on ai.idAtividadeIniciatica = aim.fk_idAtividadeIniciatica 
                        left join organismoAfiliado as oa on oa.idOrganismoAfiliado = ai.fk_idOrganismoAfiliado                        
                         ";
        }

        $query .= " WHERE 1=1 ";

        if(count($arrMembrosSemRegistro)==0)
        {
            if ($idOrganismoAfiliado !== null) {
                $query .= " and fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado";
            }
            if ($tipoAtividadeIniciatica !== null) {
                $query .= " and tipoAtividadeIniciatica = :tipoAtividadeIniciatica";
            }
            if ($agil !== null) {
                $query .= " and agilAtividadeIniciatica = :agilAtividadeIniciatica";
            }
            if ($statusAtividadeIniciatica !== null) {
                $query .= " and ai.statusAtividadeIniciatica = :statusAtividadeIniciatica";
            }
            if($membros){


                $strCPD = implode(",",$arr);

                if(count($arr)>0) {
                    //Excluir pessoas do cpd
                    $query .= " and idAtividadeIniciaticaMembro not in (" . $strCPD . ")";
                }
                //Selecionar apenas o status Atividade Iniciatica 4 [Erro no processo] ou 2 concluida
                //$query .= " and statusAtividadeIniciaticaMembro = 2 and (mensagemAtividadeIniciaticaMembro = ''
                //or mensagemAtividadeIniciaticaMembro = 'Erro no processo')";

                $query .= " 
              
              and aim.fk_idAtividadeIniciatica is not NULL and statusAtividadeIniciaticaMembro = 2 ";
            }

            if($membros)
            {
                $query .= " AND fk_idOrganismoAfiliado <> 0 
                        AND aim.idAtividadeIniciaticaMembro not in (SELECT hi.fk_idAtividadeIniciaticaMembro FROM higienizacaoIniciacoes as hi)
            ";
            }
        }

        if($membros){


            $strCPD = implode(",",$arrMembrosSemRegistro);

            if(count($arrMembrosSemRegistro)>0) {
                //Excluir pessoas do cpd
                $query .= " and idAtividadeIniciaticaMembro in (" . $strCPD . ")";
            }
        }
        if(!$membros) {
            $query .= " ORDER BY ai.idAtividadeIniciatica ASC ";
        }else{
            $query .= " ORDER BY ai.idAtividadeIniciatica ASC ";
        }

        if ($limit != null) {
            $query .= " limit :start, :limit";
        }

        //echo " || " . $query . " || ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        if(count($arrMembrosSemRegistro)==0) {

            if ($idOrganismoAfiliado !== null) {
                $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
            }
            if ($tipoAtividadeIniciatica !== null) {
                $sth->bindParam(':tipoAtividadeIniciatica', $tipoAtividadeIniciatica, PDO::PARAM_INT);
            }
            if ($agil !== null) {
                $sth->bindParam(':agilAtividadeIniciatica', $agil, PDO::PARAM_INT);
            }
            if ($statusAtividadeIniciatica !== null) {
                $sth->bindParam(':statusAtividadeIniciatica', $statusAtividadeIniciatica, PDO::PARAM_INT);
            }
        }

        if ($limit != null) {
            $sth->bindValue(':start', (int) trim($start), PDO::PARAM_INT);
            //echo "11";
            $sth->bindValue(':limit', (int) trim($limit), PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdAtividadeIniciatica($idAtividadeIniciatica){

        $sql = "SELECT * FROM  atividadeIniciatica WHERE   idAtividadeIniciatica = :idAtividadeIniciatica";
        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

	public function alteraStatusAtividadeIniciatica() {

        $sql = "UPDATE atividadeIniciatica SET `statusAtividadeIniciatica` = :statusAtividadeIniciatica
                WHERE `idAtividadeIniciatica` = :idAtividadeIniciatica";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusAtividadeIniciatica', $this->statusAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeIniciatica', $this->idAtividadeIniciatica, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusAtividadeIniciatica();
            return $arr;
        } else {
            return false;
        }
    }

    public function alteraStatusAtividadeIniciaticaRealizada($idAtividadeIniciatica=null) {

        $sql = "UPDATE atividadeIniciatica SET `statusAtividadeIniciatica` = 2 "
                . " WHERE `idAtividadeIniciatica` = :idAtividadeIniciatica";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraStatusAtividadeIniciaticaCancelada($idAtividadeIniciatica=null) {

        $sql = "UPDATE atividadeIniciatica SET `statusAtividadeIniciatica` = 3 "
                . " WHERE `idAtividadeIniciatica` = :idAtividadeIniciatica";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        $data = array('idAtividadeIniciatica'=>$idAtividadeIniciatica);
        //echo "QUERY: " . $this->parms("alteraStatusAtividadeIniciaticaCancelada",$sql,$data);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function cadastraMembroAtividadeIniciatica($seqCadast,$idAtividadeIniciatica, $nomeMembro) {

        $sql = "INSERT INTO atividadeIniciaticaMembro
                                 (fk_idAtividadeIniciatica,
                                  seqCadastAtividadeIniciaticaMembro,
                                  nomeAtividadeIniciaticaMembro,
                                  dataCadastroAtividadeIniciaticaMembro,
                                  statusAtividadeIniciaticaMembro)
                            VALUES (:fk_idAtividadeIniciatica,
                                    :seqCadastAtividadeIniciaticaMembro,
                                    :nomeAtividadeIniciaticaMembro,
                                    NOW(),
                                    0)";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastAtividadeIniciaticaMembro', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':nomeAtividadeIniciaticaMembro', $nomeMembro, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function deletaMembroAtividadeIniciatica($idAtividadeIniciatica) {
        $sql = "DELETE FROM atividadeIniciaticaMembro WHERE fk_idAtividadeIniciatica= :fk_idAtividadeIniciatica";
        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function deletaAtividadeIniciatica($idAtividadeIniciatica) {

        $sql = "DELETE FROM atividadeIniciatica WHERE idAtividadeIniciatica=:idAtividadeIniciatica";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeIniciaticaMembros($idAtividadeIniciatica){
        $sql = "SELECT * FROM  atividadeIniciaticaMembro as aim 
                inner join atividadeIniciatica as ai on aim.fk_idAtividadeIniciatica = ai.idAtividadeIniciatica
                WHERE   fk_idAtividadeIniciatica=:fk_idAtividadeIniciatica
                group by seqCadastAtividadeIniciaticaMembro";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        $retorno = $c->run($sth);

        $data = array(
            'fk_idAtividadeIniciatica'=>$idAtividadeIniciatica
        );
        //echo "QUERY: " . $this->parms("listaAtividadeIniciaticaMembros",$sql,$data);

        if ($retorno){
            return $retorno;
        } else {
            return 0;
        }
    }

    public function confirmaAgendamentoMembroAtividadeIniciatica($seqCadast,$idAtividadeIniciatica) {

        $sql = "UPDATE atividadeIniciaticaMembro SET statusAtividadeIniciaticaMembro = 1
                  WHERE fk_idAtividadeIniciatica=:fk_idAtividadeIniciatica
                  and seqCadastAtividadeIniciaticaMembro=:seqCadastAtividadeIniciaticaMembro";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastAtividadeIniciaticaMembro', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function cancelaAgendamentoMembroAtividadeIniciatica($seqCadast=null,$idAtividadeIniciatica) {

        $sql = "UPDATE atividadeIniciaticaMembro SET statusAtividadeIniciaticaMembro = 3
                  WHERE fk_idAtividadeIniciatica=:fk_idAtividadeIniciatica";
        $sql .= $seqCadast !== null ? " and seqCadastAtividadeIniciaticaMembro=:seqCadastAtividadeIniciaticaMembro" : "";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        if($seqCadast !== null){
          $sth->bindParam(':seqCadastAtividadeIniciaticaMembro', $seqCadast, PDO::PARAM_INT);
        }

        $data = array('fk_idAtividadeIniciatica'=>$idAtividadeIniciatica);
        if($seqCadast !== null){
          $data = array('fk_idAtividadeIniciatica'=>$idAtividadeIniciatica, 'seqCadastAtividadeIniciaticaMembro'=>$seqCadast);
        }
        //echo "QUERY: " . $this->parms("cancelaAgendamentoMembroAtividadeIniciatica",$sql,$data);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function confirmaParticipacaoMembroAtividadeIniciatica($seqCadast,$idAtividadeIniciatica) {

        $sql = "UPDATE atividadeIniciaticaMembro SET statusAtividadeIniciaticaMembro = 2
                WHERE fk_idAtividadeIniciatica = :fk_idAtividadeIniciatica
                and seqCadastAtividadeIniciaticaMembro = :seqCadastAtividadeIniciaticaMembro";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastAtividadeIniciaticaMembro', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function verificaMembroAgendado($seqCadast, $tipoAtividade) {

        $sql = "SELECT aim.*,ai.* FROM atividadeIniciaticaMembro as aim INNER JOIN atividadeIniciatica as ai
                ON aim.fk_idAtividadeIniciatica=ai.idAtividadeIniciatica
    			where aim.seqCadastAtividadeIniciaticaMembro=:seqCadast
    			and (aim.statusAtividadeIniciaticaMembro=0 or aim.statusAtividadeIniciaticaMembro=1 or aim.statusAtividadeIniciaticaMembro=2)
                and ai.tipoAtividadeIniciatica=:tipoAtividade";

        //echo "Query: [" . $sql . "]";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':tipoAtividade', $tipoAtividade, PDO::PARAM_INT);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function verificaMembroJaAgendado($seqCadast, $tipoAtividade) {

        $sql = "SELECT aim.*,ai.* FROM atividadeIniciaticaMembro as aim INNER JOIN atividadeIniciatica as ai
                ON aim.fk_idAtividadeIniciatica=ai.idAtividadeIniciatica
    			where aim.seqCadastAtividadeIniciaticaMembro=:seqCadast
    			and (aim.statusAtividadeIniciaticaMembro=0 or aim.statusAtividadeIniciaticaMembro=1 or aim.statusAtividadeIniciaticaMembro=2)
                and ai.tipoAtividadeIniciatica=:tipoAtividade";

        //echo "Query: [" . $sql . "]";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':tipoAtividade', $tipoAtividade, PDO::PARAM_INT);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function cadastroAtividadeIniciaticaAgil($fk_idOrganismoAfiliado,$fk_idAtividadeIniciaticaOficial,$fk_idAtividadeIniciaticaColumba,
                                                    $codAfiliacaoRecepcao, $nomeRecepcao, $recepcaoAtividadeIniciaticaOficial, $tipoAtividadeIniciatica, $dataRealizadaAtividadeIniciatica,
                                                    $horaRealizadaAtividadeIniciatica, $localAtividadeIniciatica, $anotacoesAtividadeIniciatica, $fk_seqCadastAtualizadoPor){

        $sql = "INSERT INTO atividadeIniciatica
                                 (tipoAtividadeIniciatica,
                                  localAtividadeIniciatica,
                                  dataRealizadaAtividadeIniciatica,
                                  horaRealizadaAtividadeIniciatica,
                                  dataRegistroAtividadeIniciatica,
                                  anotacoesAtividadeIniciatica,
                                  agilAtividadeIniciatica,
                                  fk_seqCadastAtualizadoPor,
                                  fk_idAtividadeIniciaticaOficial,
                                  fk_idAtividadeIniciaticaColumba,
                                  fk_seqCadastRecepcao,
                                  codAfiliacaoRecepcao,
                                  nomeRecepcao,
                                  fk_idOrganismoAfiliado)
                            VALUES (:tipoAtividadeIniciatica,
                                    :localAtividadeIniciatica,
                                    :dataRealizadaAtividadeIniciatica,
                                    :horaRealizadaAtividadeIniciatica,
                                    NOW(),
                                    :anotacoesAtividadeIniciatica,
                                    '1',
                                    :fk_seqCadastAtualizadoPor,
                                    :fk_idAtividadeIniciaticaOficial,
                                    :fk_idAtividadeIniciaticaColumba,
                                    :fk_seqCadastRecepcao,
                                    :codAfiliacaoRecepcao,
                                    :nomeRecepcao,
                                    :fk_idOrganismoAfiliado)";
        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':tipoAtividadeIniciatica', $tipoAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':localAtividadeIniciatica', $localAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':dataRealizadaAtividadeIniciatica', $dataRealizadaAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':horaRealizadaAtividadeIniciatica', $horaRealizadaAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':anotacoesAtividadeIniciatica', $anotacoesAtividadeIniciatica, PDO::PARAM_STR);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeIniciaticaOficial', $fk_idAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeIniciaticaColumba', $fk_idAtividadeIniciaticaColumba, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastRecepcao', $recepcaoAtividadeIniciaticaOficial, PDO::PARAM_INT);
        $sth->bindParam(':codAfiliacaoRecepcao', $codAfiliacaoRecepcao, PDO::PARAM_INT);
        $sth->bindParam(':nomeRecepcao', $nomeRecepcao, PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }

    }

    public function selecionaUltimoIdInserido()
    {

        $sql= "SELECT * FROM atividadeIniciatica ORDER BY idAtividadeIniciatica DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function retornaQntVagasDisponiveis($idAtividade) {

        $sql = "SELECT COUNT(*) as total FROM atividadeIniciaticaMembro WHERE fk_idAtividadeIniciatica=:idAtividade
                and statusAtividadeIniciaticaMembro <> 3";

        //echo "Query: [" . $sql . "]";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividade', $idAtividade, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function verificaMembroNaAtividadeIniciatica($seqCadst, $idAtividade){

        $sql = "SELECT * FROM atividadeIniciaticaMembro "
                . " WHERE fk_idAtividadeIniciatica=:idAtividade and seqCadastAtividadeIniciaticaMembro=:seqCadst";

        //echo "Query: [" . $sql . "]";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividade', $idAtividade, PDO::PARAM_INT);
        $sth->bindParam(':seqCadst', $seqCadst, PDO::PARAM_INT);

        $data = array(
            'idAtividade'=>$idAtividade,
            'seqCadst'=>$seqCadst
        );
        //echo $this->parms("verificaMembroNaAtividadeIniciatica",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function alteraMembroAtividadeIniciatica($idAtividadeIniciatica,$seqCadastMembro)
    {
        $sql = "UPDATE atividadeIniciaticaMembro SET statusAtividadeIniciaticaMembro = 0
                WHERE fk_idAtividadeIniciatica = :idAtividadeIniciatica and seqCadastAtividadeIniciaticaMembro = :seqCadastMembro";

        //echo "Query(alteraMembroAtividadeIniciatica): [" . $query . "]";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        $data = array(
            'seqCadastMembro'=>$seqCadastMembro,
            'idAtividadeIniciatica'=>$idAtividadeIniciatica
        );
        //echo $this->parms("alteraMembroAtividadeIniciatica",$sql,$data);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function verificaExistenciaRegistroAtividadeSuplente($idAtividade) {

        $sql = "SELECT * FROM atividadeIniciatica_suplente WHERE fk_idAtividadeIniciatica=:idAtividade";

        //echo "Query(verificaExistenciaRegistroAtividadeSuplente): [" . $sql . "]";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividade', $idAtividade, PDO::PARAM_INT);

        $data = array(
            'idAtividade'=>$idAtividade
        );
        //echo $this->parms("verificaExistenciaRegistroAtividadeSuplente",$sql,$data);

        if ($c->run($sth,null,null.true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraRegistroAtividadeSuplente($idAtividadeIniciatica, $tipoOficialSuplente, $seqCadastMembro) {

        $query = "UPDATE atividadeIniciatica_suplente SET ";

        $query .= ($tipoOficialSuplente == 1) ? "mestreAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 2) ? "mestreAuxAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 3) ? "arquivistaAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 4) ? "capelaoAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 5) ? "matreAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 6) ? "grandeSacerdotisaAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 7) ? "guiaAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 8) ? "guardiaoInternoAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 9) ? "guardiaoExternoAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 10) ? "archoteAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 11) ? "medalhistaAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 12) ? "arautoAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 13) ? "adjutorAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 14) ? "sonoplastaAtividadeIniciatica_suplente=:seqCadastMembro" : "";
        $query .= ($tipoOficialSuplente == 15) ? "columbaAtividadeIniciatica_suplente=:seqCadastMembro" : "";

        $query .= " WHERE fk_idAtividadeIniciatica = :idAtividadeIniciatica";

        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        $data = array(
            'seqCadastMembro'=>$seqCadastMembro,
            'idAtividadeIniciatica'=>$idAtividadeIniciatica
        );
        //echo $this->parms("alteraRegistroAtividadeSuplente",$query,$data);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function cadastraRegistroAtividadeSuplente($idAtividadeIniciatica, $tipoOficialSuplente, $seqCadastMembro) {

        $query = "INSERT INTO atividadeIniciatica_suplente
                                 (fk_idAtividadeIniciatica,
                                  dataRegistroAtividadeIniciatica_suplente,";
        $query .= ($tipoOficialSuplente == 1) ? "mestreAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 2) ? "mestreAuxAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 3) ? "arquivistaAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 4) ? "capelaoAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 5) ? "matreAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 6) ? "grandeSacerdotisaAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 7) ? "guiaAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 8) ? "guardiaoInternoAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 9) ? "guardiaoExternoAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 10) ? "archoteAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 11) ? "medalhistaAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 12) ? "arautoAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 13) ? "adjutorAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 14) ? "sonoplastaAtividadeIniciatica_suplente)" : "";
        $query .= ($tipoOficialSuplente == 15) ? "columbaAtividadeIniciatica_suplente)" : "";

        $query .= " VALUES (:idAtividadeIniciatica,
                            NOW(),
                            :seqCadastMembro)";

        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);

        $data = array(
            'idAtividadeIniciatica'=>$idAtividadeIniciatica,
            'seqCadastMembro'=>$seqCadastMembro
        );
        //echo $this->parms("cadastraRegistroAtividadeSuplente",$query,$data);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeSuplente($idAtividade) {

        $sql = "SELECT * FROM atividadeIniciatica_suplente WHERE fk_idAtividadeIniciatica= :idAtividade";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividade', $idAtividade, PDO::PARAM_INT);

        $resultado=$c->run($sth);

        $data = array(
            'idAtividade'=>$idAtividade
        );
        //echo "QUERY: " . $this->parms("listaAtividadeSuplente",$sql,$data);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function deletarOficialAdjunto($idAtividadeIniciatica,$tipoMembroAdjunto) {

        $query = "UPDATE atividadeIniciatica_suplente SET ";

        $query .= ($tipoMembroAdjunto == 1) ? "mestreAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 2) ? "mestreAuxAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 3) ? "arquivistaAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 4) ? "capelaoAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 5) ? "matreAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 6) ? "grandeSacerdotisaAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 7) ? "guiaAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 8) ? "guardiaoInternoAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 9) ? "guardiaoExternoAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 10) ? "archoteAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 11) ? "medalhistaAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 12) ? "arautoAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 13) ? "adjutorAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 14) ? "sonoplastaAtividadeIniciatica_suplente='0'" : "";
        $query .= ($tipoMembroAdjunto == 15) ? "columbaAtividadeIniciatica_suplente='0'" : "";

        $query .= " WHERE fk_idAtividadeIniciatica = :idAtividadeIniciatica";

        //echo "Query: [" . $sql . "]";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function retornaNumeroDeAtividadesParaRelatorio($idOrganismo=null,$mesAtual=null,$anoAtual=null,$tipoAtividadeIniciatica=null){

        $sql = "SELECT COUNT(*) as total FROM atividadeIniciatica where 1=1 and statusAtividadeIniciatica=2";
        $sql .= $mesAtual != null ? " and MONTH(dataRealizadaAtividadeIniciatica)=:mesAtual" : "";
        $sql .= $anoAtual != null ? " and YEAR(dataRealizadaAtividadeIniciatica)=:anoAtual" : "";
        $sql .= $idOrganismo != null ? " and fk_idOrganismoAfiliado=:idOrganismo" : "";
        $sql .= $tipoAtividadeIniciatica != null ? " and tipoAtividadeIniciatica=:tipoAtividadeIniciatica" : "";
        //$sql .= " order by dataAtividadeEstatuto ASC";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual != null){
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual != null){
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($idOrganismo != null){
            $sth->bindParam(':idOrganismo', $idOrganismo, PDO::PARAM_INT);
        }
        if($tipoAtividadeIniciatica != null){
            $sth->bindParam(':tipoAtividadeIniciatica', $tipoAtividadeIniciatica, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function retornaNumeroDeParticipantes($idOrganismo=null,$mesAtual=null,$anoAtual=null,$tipoAtividadeIniciatica=null){

        $sql = "SELECT aim.* FROM atividadeIniciatica as ai
                            RIGHT JOIN atividadeIniciaticaMembro as aim ON ai.idAtividadeIniciatica=aim.fk_idAtividadeIniciatica
                            where 1=1 and ai.statusAtividadeIniciatica=2 and aim.statusAtividadeIniciaticaMembro=2";
        $sql .= $mesAtual != null ? " and MONTH(ai.dataRealizadaAtividadeIniciatica)=:mesAtual" : "";
        $sql .= $anoAtual != null ? " and YEAR(ai.dataRealizadaAtividadeIniciatica)=:anoAtual" : "";
        $sql .= $idOrganismo != null ? " and fk_idOrganismoAfiliado=:idOrganismo" : "";
        $sql .= $tipoAtividadeIniciatica != null ? " and tipoAtividadeIniciatica=:tipoAtividadeIniciatica" : "";
        //$sql .= " order by dataAtividadeEstatuto ASC";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual != null){
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual != null){
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($idOrganismo != null){
            $sth->bindParam(':idOrganismo', $idOrganismo, PDO::PARAM_INT);
        }
        if($tipoAtividadeIniciatica != null){
            $sth->bindParam(':tipoAtividadeIniciatica', $tipoAtividadeIniciatica, PDO::PARAM_INT);
        }

        $data = array(
            'mesAtual'=>$mesAtual,
            'anoAtual'=>$anoAtual,
            'idOrganismo'=>$idOrganismo,
        );
        //echo "QUERY: " . $this->parms("retornaNumeroDeParticipantes",$sql,$data);

        $resultado = $c->run($sth,null,true);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function mudaStatusErroMembroAtividadeIniciatica($seqCadastMembro,$idAtividadeIniciatica,$msgRetornoWebService)
    {
        $query = "UPDATE atividadeIniciaticaMembro SET statusAtividadeIniciaticaMembro = 4,
                  mensagemAtividadeIniciaticaMembro = :msgRetornoWebService
                  WHERE fk_idAtividadeIniciatica = :idAtividadeIniciatica
                  and seqCadastAtividadeIniciaticaMembro = :seqCadastMembro";

        //echo "Query(alteraMembroAtividadeIniciatica): [" . $query . "]";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':msgRetornoWebService', $msgRetornoWebService, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function mudaStatusCompareceuMembroAtividadeIniciatica($seqCadastMembro,$idAtividadeIniciatica,$msgRetornoWebService)
    {
        $query = "UPDATE atividadeIniciaticaMembro SET statusAtividadeIniciaticaMembro = 2,
                  mensagemAtividadeIniciaticaMembro = :msgRetornoWebService
                  WHERE fk_idAtividadeIniciatica = :idAtividadeIniciatica
                  and seqCadastAtividadeIniciaticaMembro = :seqCadastMembro";

        //echo "Query(alteraMembroAtividadeIniciatica): [" . $query . "]";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':msgRetornoWebService', $msgRetornoWebService, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function marcaCompareceuMembroAtividadeIniciatica($seqCadastMembro,$idAtividadeIniciatica,$msg)
    {
        $query = "UPDATE atividadeIniciaticaMembro SET 
                  mensagemAtividadeIniciaticaMembro = :msg
                  WHERE fk_idAtividadeIniciatica = :idAtividadeIniciatica
                  and seqCadastAtividadeIniciaticaMembro = :seqCadastMembro";

        //echo "Query(alteraMembroAtividadeIniciatica): [" . $query . "]";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $sth->bindParam(':idAtividadeIniciatica', $idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':msg', $msg, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function retornaStatusAtividadeIniciatica($idAtividade){

        $sql = "SELECT statusAtividadeIniciatica as status FROM atividadeIniciatica WHERE idAtividadeIniciatica=:idAtividade";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividade', $idAtividade, PDO::PARAM_INT);

        $data = array(
            'idAtividade'=>$idAtividade
        );
        //echo $this->parms("retornaStatusAtividadeIniciatica",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $item) {
                return $item['status'];
            }
        } else {
            return false;
        }
    }

    public function verificaExistenciaRegistroAtividadeIniciatica($seqCadast,$fk_idOrganismoAfiliado, $tipoAtividadeIniciatica,$dataRealizadaAtividadeIniciatica) {

        $sql = "SELECT * FROM atividadeIniciatica 
                left join atividadeIniciaticaMembro on fk_idAtividadeIniciatica = idAtividadeIniciatica
                WHERE fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado
                and tipoAtividadeIniciatica = :tipoAtividadeIniciatica
                and dataRealizadaAtividadeIniciatica = :dataRealizadaAtividadeIniciatica 
                and seqCadastAtividadeIniciaticaMembro = :seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':tipoAtividadeIniciatica', $tipoAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':dataRealizadaAtividadeIniciatica', $dataRealizadaAtividadeIniciatica, PDO::PARAM_INT);

        $data = array(
            'fk_idOrganismoAfiliado'=>$fk_idOrganismoAfiliado,
            'tipoAtividadeIniciatica'=>$tipoAtividadeIniciatica,
            'dataRealizadaAtividadeIniciatica'=>$dataRealizadaAtividadeIniciatica
        );
        //echo $this->parms("verificaExistenciaRegistroAtividadeSuplente",$sql,$data);

        if ($c->run($sth,null,null.true)){
            return true;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}

?>
