<?php

require_once 'conexaoClass.php';

class Documento {
    
    private $idDocumento;
    private $arquivo;
    private $tipo;
    private $extensao;
    private $paginaImpressao;
    private $descricao;
    private $linkVisualizacao;
    
    /*
     * Métodos GET e SET
     */
    
    public function setIdDocumento($idDocumento) {
        $this->idDocumento = $idDocumento;
    }
     
    public function setArquivo($arquivo) {
        $this->arquivo = $arquivo;
    }
    
    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }
    
    public function setExtensao($extensao) {
        $this->extensao = $extensao;
    }
    
    public function setPaginaImpressao($paginaImpressao) {
        $this->paginaImpressao = $paginaImpressao;
    }
    
    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }
    
    public function setLinkVisualizacao($linkVisualizacao) {
        $this->linkVisualizacao = $linkVisualizacao;
    }
    
    public function getIdDocumento() {
        return $this->idDocumento;
    }
    
    public function getArquivo() {
        return $this->arquivo;
    }
    
    public function getTipo() {
        return $this->tipo;
    }
    
    public function getExtensao() {
        return $this->extensao;
    }
    
    public function getPaginaImpressao() {
        return $this->paginaImpressao;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }
    
    public function getLinkVisualizacao() {
        return $this->linkVisualizacao;
    }
    
    public function cadastraDocumento() {
        
        $sql = "INSERT INTO `documento` (";
        $sql .= "`idDocumento`, ";
        $sql .= "`arquivo`, ";
        $sql .= "`tipo`, ";
        $sql .= "`extensao`, ";
        $sql .= "`paginaImpressao`, ";
        $sql .= "`descricao`, ";
        $sql .= "`linkVisualizacao`";
        $sql .= ") VALUES (";
        $sql .= ":idDocumento, ";
        $sql .= ":arquivo, ";
        $sql .= ":tipo, ";
        $sql .= ":extensao, ";
        $sql .= ":paginaImpressao, ";
        $sql .= ":descricao, ";
        $sql .= ":linkVisualizacao) ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDocumento', $this->idDocumento, PDO::PARAM_INT);
        $sth->bindParam(':arquivo', $this->arquivo, PDO::PARAM_STR);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_STR);
        $sth->bindParam(':extensao', $this->extensao, PDO::PARAM_STR);
        $sth->bindParam(':paginaImpressao', $this->paginaImpressao, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':linkVisualizacao', $this->linkVisualizacao, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraDocumento() {
        
        $sql .= "UPDATE `documento` SET ";
        $sql .= "`arquivo` = :arquivo, ";
        $sql .= "`tipo` = :tipo, ";
        $sql .= "`extensao` = :extensao, ";
        $sql .= "`paginaImpressao` = :paginaImpressao, ";
        $sql .= "`descricao` = :descricao";
        $sql .= " WHERE `idDocumento` = :idDocumento";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':arquivo', $this->arquivo, PDO::PARAM_STR);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_STR);
        $sth->bindParam(':extensao', $this->extensao, PDO::PARAM_STR);
        $sth->bindParam(':paginaImpressao', $this->paginaImpressao, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':idDocumento', $this->idDocumento, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraDescricaoDocumento() {
        
        $sql = "UPDATE `documento` SET ";
        $sql .= "`descricao` = :descricao ";
        $sql .= "WHERE `idDocumento` = :idDocumento";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':idDocumento', $this->idDocumento, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function listaDocumento($tipo = null, $ext = null) {
        
        $sql = "SELECT * FROM documento WHERE 1";
        if($tipo != null) {
            $sql .= " and tipo = :tipo";
        }
        
        if($ext != null) {
            $sql .= " and extensao = :ext";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($tipo != null) {
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_STR);        
        }
        if($ext != null) {
            $sth->bindParam(':ext', $ext, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function getIdValorMaximo() {
        
        $sql = "SELECT MAX(idDocumento) AS maxId FROM documento";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscarDocumento($arq) {
        
        $sql = "SELECT * FROM documento WHERE arquivo = :arq";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':arq', $arq, PDO::PARAM_STR);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscarDocumentoId($id) {
        
        $sql = "SELECT * FROM documento WHERE idDocumento = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_STR);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function deletaDocumento($id) {
        
        $sql = "DELETE FROM `documento` WHERE `idDocumento` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_STR);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}
