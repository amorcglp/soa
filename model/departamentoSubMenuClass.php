<?php
require_once ("conexaoClass.php");

class DepartamentoSubMenu {

    public $fk_idDepartamento;
    public $fk_idSubMenu;

    function getFkIdDepartamento() {
        return $this->fk_idDepartamento;
    }

    function getFkIdSubMenu() {
        return $this->fk_idSubMenu;
    }

    function setFkIdDepartamento($fk_idDepartamento) {
        $this->fk_idDepartamento = $fk_idDepartamento;
    }

    function setFkIdSubMenu($fk_idSubMenu) {
        $this->fk_idSubMenu = $fk_idSubMenu;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO departamento_subMenu (`fk_idDepartamento`, `fk_idSubMenu`)
                                    VALUES (:idDepartamento,
                                            :idSubMenu)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idSubMenu',$this->fk_idSubMenu , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM departamento_subMenu 
                    WHERE fk_idDepartamento = :idDepartamento
                    and fk_idSubMenu = :idSubMenu";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idSubMenu',$this->fk_idSubMenu , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaPorDepartamento() {
        
        $sql = "SELECT * FROM departamento_subMenu
                                 WHERE `fk_idDepartamento` = :idDepartamento";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
}
?>