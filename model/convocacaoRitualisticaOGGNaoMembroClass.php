<?php

require_once ("conexaoClass.php");

class convocacaoRitualisticaOGGNaoMembro{

    private $idConvocacaoRitualisticaOGGNaoMembro;
    private $fk_idConvocacaoRitualisticaOGG;
    private $nome;
    private $tipo;
    private $email;

    //METODO SET/GET -----------------------------------------------------------

    function getIdConvocacaoRitualisticaOGGNaoMembro() {
        return $this->idConvocacaoRitualisticaOGGNaoMembro;
    }

    function getFk_idConvocacaoRitualisticaOGG() {
        return $this->fk_idConvocacaoRitualisticaOGG;
    }

    function getNome() {
        return $this->nome;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getEmail() {
        return $this->email;
    }

    function setIdConvocacaoRitualisticaOGGNaoMembro($idConvocacaoRitualisticaOGGNaoMembro) {
        $this->idConvocacaoRitualisticaOGGNaoMembro = $idConvocacaoRitualisticaOGGNaoMembro;
    }

    function setFk_idConvocacaoRitualisticaOGG($fk_idConvocacaoRitualisticaOGG) {
        $this->fk_idConvocacaoRitualisticaOGG = $fk_idConvocacaoRitualisticaOGG;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    
    
    
    //--------------------------------------------------------------------------

    public function cadastro(){

        $sql = "INSERT INTO convocacaoRitualisticaOGG_naomembro
                                 (
                                  fk_idConvocacaoRitualisticaOGG,
                                  nome,
                                  tipo,
                                  email
                                  )
                            VALUES (:fk_idConvocacaoRitualisticaOGG,
                                    :nome,
                                    :tipo,
                                    :email)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $this->fk_idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function lista($idConvocacaoRitualisticaOGG=null,$v=null,$tipo=null,$mes=null,$ano=null,$fk_idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM convocacaoRitualisticaOGG_naomembro "
                . " inner join convocacaoRitualisticaOGG on fk_idConvocacaoRitualisticaOGG = idConvocacaoRitualisticaOGG "
                . " where 1=1";

    	if($idConvocacaoRitualisticaOGG!=null) {
    		$sql .= " and fk_idConvocacaoRitualisticaOGG=:fk_idConvocacaoRitualisticaOGG";
    	}
        if($v!=null) {
    		$sql .= " and nome=:nome";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
        if($mes!=null) {
    		$sql .= " and MONTH(dataConvocacao)=:mes";
    	}
        if($ano!=null) {
    		$sql .= " and YEAR(dataConvocacao)=:ano";
    	}
        if($fk_idOrganismoAfiliado!=null) {
    		$sql .= " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado";
    	}
        
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idConvocacaoRitualisticaOGG!=null){
            $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':nome', $v, PDO::PARAM_STR);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $v, PDO::PARAM_INT);
        }
        if($mes!=null){
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null){
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function total($idConvocacaoRitualisticaOGG=null,$v=null,$tipo=null,$mes=null,$ano=null,$fk_idOrganismoAfiliado=null){

    	$sql = "SELECT count(idConvocacaoRitualisticaOGGNaoMembro) as total FROM convocacaoRitualisticaOGG_naomembro "
                . "  inner join convocacaoRitualisticaOGG on fk_idConvocacaoRitualisticaOGG = idConvocacaoRitualisticaOGG "
                . " where 1=1";

    	if($idConvocacaoRitualisticaOGG!=null) {
    		$sql .= " and fk_idConvocacaoRitualisticaOGG=:fk_idConvocacaoRitualisticaOGG";
    	}
        if($v!=null) {
    		$sql .= " and nome=:nome";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
        if($mes!=null) {
    		$sql .= " and MONTH(dataConvocacao)=:mes";
    	}
        if($ano!=null) {
    		$sql .= " and YEAR(dataConvocacao)=:ano";
    	}
        if($fk_idOrganismoAfiliado!=null) {
    		$sql .= " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idConvocacaoRitualisticaOGG!=null){
            $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':nome', $v, PDO::PARAM_STR);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        }
        if($mes!=null){
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null){
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }
    
    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

    public function remove($idConvocacaoRitualisticaOGG=null,$id=null){

        $sql= "DELETE FROM convocacaoRitualisticaOGG_naomembro WHERE 1=1";

    	if($idConvocacaoRitualisticaOGG!=null) {
    		$sql .= " and fk_idConvocacaoRitualisticaOGG=:fk_idConvocacaoRitualisticaOGG";
    	}
    	if($id!=null) {
    		$sql .= " and idConvocacaoRitualisticaOGGNaoMembro=:id";
    	}

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idConvocacaoRitualisticaOGG!=null){
            $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        }
        if($id!=null){
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>
