<?php

require_once ("conexaoClass.php");

class subMenu {

    public $idSubMenu;
    public $nomeSubMenu;
    public $descricaoSubMenu;
    public $arquivoSubMenu;
    public $prioridadeSubMenu;
    public $iconeSubMenu;
    public $fk_idSecaoMenu;
    public $statusSubMenu;
    public $msgManutencao;

    //Getter e Setter
    function getIdSubMenu() {
        return $this->idSubMenu;
    }

	function getNomeSubMenu() {
        return $this->nomeSubMenu;
    }
    
	function getDescricaoSubMenu() {
        return $this->descricaoSubMenu;
    }
    
	function getArquivoSubMenu() {
        return $this->arquivoSubMenu;
    }
    
	function getPrioridadeSubMenu() {
        return $this->prioridadeSubMenu;
    }
    
	function getIconeSubMenu() {
        return $this->iconeSubMenu;
    }
    
	function getFkIdSecaoMenu() {
        return $this->fk_idSecaoMenu;
    }
    
	function getStatusSubMenu() {
        return $this->statusSubMenu;
    }

    function getMsgManutencao() {
        return $this->msgManutencao;
    }
    
    function setIdSubMenu($idSubMenu) {
        $this->idSubMenu = $idSubMenu;
    }

	function setNomeSubMenu($nomeSubMenu) {
        $this->nomeSubMenu = $nomeSubMenu;
    }
    
    function setDescricaoSubMenu($descricaoSubMenu) {
        $this->descricaoSubMenu = $descricaoSubMenu;
    }
    
	function setArquivoSubMenu($arquivoSubMenu) {
        $this->arquivoSubMenu = $arquivoSubMenu;
    }
    
	function setPrioridadeSubMenu($prioridadeSubMenu) {
        $this->prioridadeSubMenu = $prioridadeSubMenu;
    }
    
	function setIconeSubMenu($iconeSubMenu) {
        $this->iconeSubMenu = $iconeSubMenu;
    }
    
	function setFkIdSecaoMenu($fk_idSecaoMenu) {
        $this->fk_idSecaoMenu = $fk_idSecaoMenu;
    }
    
	function setStatusSubMenu($statusSubMenu) {
        $this->statusSubMenu = $statusSubMenu;
    }

    function setMsgManutencao($msgManutencao) {
        $this->msgManutencao = $msgManutencao;
    }

    public function cadastraSubMenu() {
        
        $sql = "INSERT INTO subMenu (nomeSubMenu, msgManutencao, descricaoSubMenu, arquivoSubMenu, prioridadeSubMenu, iconeSubMenu, fk_idSecaoMenu,statusSubMenu )
                                    VALUES (
                                            :nome,
                                            :msgManutencao,
                                            :descricao,
                                            :arquivo,
                                            :prioridade,
                                            :icone,
                                            :fk_idSecaoMenu,
                                            0
                                            )";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        //$getIdSubMenu                   = $this->getIdSubMenu();
        $getNomeSubMenu                 = $this->getNomeSubMenu();
        $getDescricaoSubMenu            = $this->getDescricaoSubMenu();
        $getArquivoSubMenu              = $this->getArquivoSubMenu();
        $getPrioridadeSubMenu           = $this->getPrioridadeSubMenu();
        $getIconeSubMenu                = $this->getIconeSubMenu();
        $getFkIdSecaoMenu               = $this->getFkIdSecaoMenu();
        $getMsgManutencao               = $this->getMsgManutencao();
        /*
        $data = array(
            'nome'=>$getNomeSubMenu,
            'descricao'=>$getDescricaoSubMenu,
            'arquivo'=>$getArquivoSubMenu,
            'prioridade'=>$getPrioridadeSubMenu,
            'icone'=>$getIconeSubMenu,
            'fk_idSecaoMenu'=>$getFkIdSecaoMenu   
        );
        echo "QUERY: " . $this->parms("cadastraSubMenu",$sql,$data) . "\n";
        exit();
        */
        //$sth->bindParam(':id', $getIdSubMenu, PDO::PARAM_INT);
        $sth->bindParam(':nome', $getNomeSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $getDescricaoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':arquivo', $getArquivoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $getPrioridadeSubMenu, PDO::PARAM_INT);
        $sth->bindParam(':icone', $getIconeSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':fk_idSecaoMenu', $getFkIdSecaoMenu, PDO::PARAM_INT);
        $sth->bindParam(':msgManutencao', $getMsgManutencao, PDO::PARAM_STR);
        
        if ($c->run($sth,null)){
            return true;
        }else{
            return false;
        }
    }
    
    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

    public function alteraSubMenu($id) {
        $sql = "UPDATE subMenu 
                            SET 
                                `nomeSubMenu` = :nome,
                                `descricaoSubMenu` = :descricao,
                                `arquivoSubMenu` = :arquivo,
                                `prioridadeSubMenu` = :prioridade,
                                `iconeSubMenu` = :icone,
                                `fk_idSecaoMenu` = :fk_idSecaoMenu,
                                `msgManutencao` = :msgManutencao
                            WHERE idSubMenu = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getNomeSubMenu                 = $this->getNomeSubMenu();
        $getDescricaoSubMenu            = $this->getDescricaoSubMenu();
        $getArquivoSubMenu              = $this->getArquivoSubMenu();
        $getPrioridadeSubMenu           = $this->getPrioridadeSubMenu();
        $getIconeSubMenu                = $this->getIconeSubMenu();
        $getFkIdSecaoMenu               = $this->getFkIdSecaoMenu();
        $getMsgManutencao               = $this->getMsgManutencao();

        $sth->bindParam(':nome', $getNomeSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $getDescricaoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':arquivo', $getArquivoSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $getPrioridadeSubMenu, PDO::PARAM_INT);
        $sth->bindParam(':icone', $getIconeSubMenu, PDO::PARAM_STR);
        $sth->bindParam(':fk_idSecaoMenu', $getFkIdSecaoMenu, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':msgManutencao', $getMsgManutencao, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusSubMenu() {
        
        $sql = "UPDATE subMenu SET `statusSubMenu` = :status"
                . "                WHERE `idSubMenu` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getStatusSubMenu               = $this->getStatusSubMenu();
        $getIdSubMenu                   = $this->getIdSubMenu();

        $sth->bindParam(':status', $getStatusSubMenu, PDO::PARAM_INT);
        $sth->bindParam(':id', $getIdSubMenu, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusSubMenu();
            return $arr;
        } else {
            return false;
        }
    }

    public function listaSubMenu($param=null,$arquivo=null,$status=0) {

    	$sql = "SELECT * FROM subMenu as sm
        						inner join secaoMenu as s on sm.fk_idSecaoMenu = s.idSecaoMenu 
        						where 1=1";
    	if($param!=null)
    	{
    		$sql .= " and fk_idSecaoMenu=:param";
    	}
    	if($arquivo!=null)
    	{
    		$sql .= " and arquivoSubMenu=:arquivo";
    	}
    	
    	$sql .= " and statusSubMenu=:status";
    	
        $sql .= " ORDER BY nomeSubMenu";
        
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($param!=null)
    	{
            $sth->bindParam(':param', $param, PDO::PARAM_INT);
        }
        if($arquivo!=null)
    	{
            $sth->bindParam(':arquivo', $arquivo, PDO::PARAM_STR);
        }
        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeSubMenu($idSubMenu) {
        
        $sql = "DELETE FROM subMenu WHERE idSubMenu = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idSubMenu, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaSubMenu() {
        $sql = "SELECT * FROM subMenu
                                 WHERE `nomeSubMenu` LIKE '%:nome%'
                                 ORDER BY nomeSubMenu ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getNomeSubMenu = $this->getNomeSubMenu();

        $sth->bindParam(':nome', $getNomeSubMenu, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdSubMenu() {
        
        $sql = "SELECT * FROM subMenu 
                    WHERE idSubMenu = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {    
            $sth->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}

?>