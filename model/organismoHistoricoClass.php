<?php
require_once ("conexaoClass.php");

class organismoHistorico {

    public $idOrganismoAfiliadoHistorico;
    public $fk_idOrganismoAfiliado;
    public $classificacao;
    public $data;
    public $statusHistorico;

    /* Funções GET e SET */

    function getIdOrganismoAfiliadoHistorico() {
        return $this->idOrganismoAfiliadoHistorico;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getClassificacao() {
        return $this->classificacao;
    }

    function getData() {
        return $this->data;
    }

    function getStatusHistorico() {
        return $this->statusHistorico;
    }

    function setIdOrganismoAfiliadoHistorico($idOrganismoAfiliadoHistorico) {
        $this->idOrganismoAfiliadoHistorico = $idOrganismoAfiliadoHistorico;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setClassificacao($classificacao) {
        $this->classificacao = $classificacao;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setStatusHistorico($statusHistorico) {
        $this->statusHistorico = $statusHistorico;
    }

            

    public function cadastraHistorico() {
        
    	$sql = "INSERT INTO organismoAfiliadoHistorico 
                                (`fk_idOrganismoAfiliado`, 
                                `classificacao`,                          
                                `data`,
                                `statusHistorico`
                                )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :classificacao,                                            
                                            :data,
                                            0
                                            )";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':classificacao', $this->classificacao, PDO::PARAM_STR);
        $sth->bindParam(':data', $this->data, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function listaHistorico() {

    	$sql = "SELECT * FROM organismoAfiliadoHistorico as oah
        						inner join organismoAfiliado as o on oah.fk_idOrganismoAfiliado = o.idOrganismoAfiliado 
        						where 1=1
            group by fk_idOrganismoAfiliado                                            
            ";
    	
        $sql .= " ORDER BY idOrganismoAfiliadoHistorico";
        
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaIdHistorico($id) {
        
        $sql = "SELECT * FROM organismoAfiliadoHistorico 
                    WHERE idOrganismoAfiliadoHistorico = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
       
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function alteraHistorico($id) {
        $sql = "UPDATE organismoAfiliadoHistorico 
                            SET 
                                `fk_idOrganismoAfiliado` = :fk_idOrganismoAfiliado,
                                `classificacao` = :classificacao,
                                `data` = :data
                            WHERE idOrganismoAfiliadoHistorico = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':classificacao', $this->classificacao, PDO::PARAM_STR);
        $sth->bindParam(':data', $this->data, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function removeHistorico($id) {
        
        $sql = "DELETE FROM idOrganismoAfiliadoHistorico WHERE idOrganismoAfiliadoHistorico = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusHistorico() {
        
	$sql = "UPDATE organismoAfiliadoHistorico SET `statusHistorico` = :status
                               WHERE `idOrganismoAfiliadoHistorico` = :idOrganismoAfiliadoHistorico";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusHistorico, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliadoHistorico', $this->idOrganismoAfiliadoHistorico, PDO::PARAM_INT);
		
        if ($c->run($sth,true)) {
            $arr = array();
            $arr['status'] = $this->statusHistorico;
            return $arr;
        } else {
            return false;
        }
    }
}
?>