<?php

require_once ("conexaoClass.php");

class organismo{

    private $idOrganismoAfiliado;
    private $nomeOrganismoAfiliado;
    private $siglaOrganismoAfiliado;
    private $eraSiglaOrganismoAfiliado;
    private $paisOrganismoAfiliado;
    private $tipoOrganismoAfiliado;
    private $statusOrganismoAfiliado;
    private $classificacaoOrganismoAfiliado;
    private $atuacaoTemporaria;
    private $cepOrganismoAfiliado;
    private $fk_idRegiaoOrdemRosacruz;
    private $codigoAfiliacaoOrganismoAfiliado;
    private $enderecoOrganismoAfiliado;
    private $numeroOrganismoAfiliado;
    private $emailOrganismoAfiliado;
    private $senhaEmailOrganismoAfiliado;
    private $situacaoOrganismoAfiliado;
    private $fk_idEstado;
    private $fk_idCidade;
    private $bairroOrganismoAfiliado;
    private $complementoOrganismoAfiliado;
    private $cnpjOrganismoAfiliado;
    private $operadoraCelularOrganismoAfiliado;
    private $celularOrganismoAfiliado;
    private $telefoneFixoOrganismoAfiliado;
    private $outroTelefoneOrganismoAfiliado;
    private $faxOrganismoAfiliado;
    private $caixaPostalOrganismoAfiliado;
    private $cepCaixaPostalOrganismoAfiliado;
    private $enderecoCorrespondenciaOrganismoAfiliado;
    private $seqCadast;
    private $latitude;
    private $longitude;
    private $naoCobrarDashboard;


    //--- GETERS e SETERS -----------------------------------------------------

    function getIdOrganismoAfiliado() {
        return $this->idOrganismoAfiliado;
    }

    function getNomeOrganismoAfiliado() {
        return $this->nomeOrganismoAfiliado;
    }

    function getSiglaOrganismoAfiliado() {
        return $this->siglaOrganismoAfiliado;
    }

    function getEraSiglaOrganismoAfiliado() {
        return $this->eraSiglaOrganismoAfiliado;
    }

    function getPaisOrganismoAfiliado() {
        return $this->paisOrganismoAfiliado;
    }

    function getTipoOrganismoAfiliado() {
        return $this->tipoOrganismoAfiliado;
    }

    function getStatusOrganismoAfiliado() {
        return $this->statusOrganismoAfiliado;
    }

    function getClassificacaoOrganismoAfiliado() {
        return $this->classificacaoOrganismoAfiliado;
    }

    function getAtuacaoTemporaria() {
        return $this->atuacaoTemporaria;
    }

    function getCepOrganismoAfiliado() {
        return $this->cepOrganismoAfiliado;
    }

    function getFk_idRegiaoOrdemRosacruz() {
        return $this->fk_idRegiaoOrdemRosacruz;
    }

    function getCodigoAfiliacaoOrganismoAfiliado() {
        return $this->codigoAfiliacaoOrganismoAfiliado;
    }

    function getEnderecoOrganismoAfiliado() {
        return $this->enderecoOrganismoAfiliado;
    }

    function getNumeroOrganismoAfiliado() {
        return $this->numeroOrganismoAfiliado;
    }

    function getEmailOrganismoAfiliado() {
        return $this->emailOrganismoAfiliado;
    }

    function getSenhaEmailOrganismoAfiliado() {
        return $this->senhaEmailOrganismoAfiliado;
    }

    function getSituacaoOrganismoAfiliado() {
        return $this->situacaoOrganismoAfiliado;
    }

    function getFk_idEstado() {
        return $this->fk_idEstado;
    }

    function getFk_idCidade() {
        return $this->fk_idCidade;
    }

    function getBairroOrganismoAfiliado() {
        return $this->bairroOrganismoAfiliado;
    }

    function getComplementoOrganismoAfiliado() {
        return $this->complementoOrganismoAfiliado;
    }

    function getCnpjOrganismoAfiliado() {
        return $this->cnpjOrganismoAfiliado;
    }

    function getOperadoraCelularOrganismoAfiliado() {
        return $this->operadoraCelularOrganismoAfiliado;
    }

    function getCelularOrganismoAfiliado() {
        return $this->celularOrganismoAfiliado;
    }

    function getTelefoneFixoOrganismoAfiliado() {
        return $this->telefoneFixoOrganismoAfiliado;
    }

    function getOutroTelefoneOrganismoAfiliado() {
        return $this->outroTelefoneOrganismoAfiliado;
    }

    function getFaxOrganismoAfiliado() {
        return $this->faxOrganismoAfiliado;
    }

    function getCaixaPostalOrganismoAfiliado() {
        return $this->caixaPostalOrganismoAfiliado;
    }

    function getCepCaixaPostalOrganismoAfiliado() {
        return $this->cepCaixaPostalOrganismoAfiliado;
    }

    function getEnderecoCorrespondenciaOrganismoAfiliado() {
        return $this->enderecoCorrespondenciaOrganismoAfiliado;
    }

    function getSeqCadast() {
        return $this->seqCadast;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getNaoCobrarDashboard() {
        return $this->naoCobrarDashboard;
    }

    function setIdOrganismoAfiliado($idOrganismoAfiliado) {
        $this->idOrganismoAfiliado = $idOrganismoAfiliado;
    }

    function setNomeOrganismoAfiliado($nomeOrganismoAfiliado) {
        $this->nomeOrganismoAfiliado = $nomeOrganismoAfiliado;
    }

    function setSiglaOrganismoAfiliado($siglaOrganismoAfiliado) {
        $this->siglaOrganismoAfiliado = $siglaOrganismoAfiliado;
    }

    function setEraSiglaOrganismoAfiliado($eraSiglaOrganismoAfiliado) {
        $this->eraSiglaOrganismoAfiliado = $eraSiglaOrganismoAfiliado;
    }

    function setPaisOrganismoAfiliado($paisOrganismoAfiliado) {
        $this->paisOrganismoAfiliado = $paisOrganismoAfiliado;
    }

    function setTipoOrganismoAfiliado($tipoOrganismoAfiliado) {
        $this->tipoOrganismoAfiliado = $tipoOrganismoAfiliado;
    }

    function setStatusOrganismoAfiliado($statusOrganismoAfiliado) {
        $this->statusOrganismoAfiliado = $statusOrganismoAfiliado;
    }

    function setClassificacaoOrganismoAfiliado($classificacaoOrganismoAfiliado) {
        $this->classificacaoOrganismoAfiliado = $classificacaoOrganismoAfiliado;
    }

    function setAtuacaoTemporaria($atuacaoTemporaria) {
        $this->atuacaoTemporaria = $atuacaoTemporaria;
    }

    function setCepOrganismoAfiliado($cepOrganismoAfiliado) {
        $this->cepOrganismoAfiliado = $cepOrganismoAfiliado;
    }

    function setFk_idRegiaoOrdemRosacruz($fk_idRegiaoOrdemRosacruz) {
        $this->fk_idRegiaoOrdemRosacruz = $fk_idRegiaoOrdemRosacruz;
    }

    function setCodigoAfiliacaoOrganismoAfiliado($codigoAfiliacaoOrganismoAfiliado) {
        $this->codigoAfiliacaoOrganismoAfiliado = $codigoAfiliacaoOrganismoAfiliado;
    }

    function setEnderecoOrganismoAfiliado($enderecoOrganismoAfiliado) {
        $this->enderecoOrganismoAfiliado = $enderecoOrganismoAfiliado;
    }

    function setNumeroOrganismoAfiliado($numeroOrganismoAfiliado) {
        $this->numeroOrganismoAfiliado = $numeroOrganismoAfiliado;
    }

    function setEmailOrganismoAfiliado($emailOrganismoAfiliado) {
        $this->emailOrganismoAfiliado = $emailOrganismoAfiliado;
    }

    function setSenhaEmailOrganismoAfiliado($senhaEmailOrganismoAfiliado) {
        $this->senhaEmailOrganismoAfiliado = $senhaEmailOrganismoAfiliado;
    }

    function setSituacaoOrganismoAfiliado($situacaoOrganismoAfiliado) {
        $this->situacaoOrganismoAfiliado = $situacaoOrganismoAfiliado;
    }

    function setFk_idEstado($fk_idEstado) {
        $this->fk_idEstado = $fk_idEstado;
    }

    function setFk_idCidade($fk_idCidade) {
        $this->fk_idCidade = $fk_idCidade;
    }

    function setBairroOrganismoAfiliado($bairroOrganismoAfiliado) {
        $this->bairroOrganismoAfiliado = $bairroOrganismoAfiliado;
    }

    function setComplementoOrganismoAfiliado($complementoOrganismoAfiliado) {
        $this->complementoOrganismoAfiliado = $complementoOrganismoAfiliado;
    }

    function setCnpjOrganismoAfiliado($cnpjOrganismoAfiliado) {
        $this->cnpjOrganismoAfiliado = $cnpjOrganismoAfiliado;
    }

    function setOperadoraCelularOrganismoAfiliado($operadoraCelularOrganismoAfiliado) {
        $this->operadoraCelularOrganismoAfiliado = $operadoraCelularOrganismoAfiliado;
    }

    function setCelularOrganismoAfiliado($celularOrganismoAfiliado) {
        $this->celularOrganismoAfiliado = $celularOrganismoAfiliado;
    }

    function setTelefoneFixoOrganismoAfiliado($telefoneFixoOrganismoAfiliado) {
        $this->telefoneFixoOrganismoAfiliado = $telefoneFixoOrganismoAfiliado;
    }

    function setOutroTelefoneOrganismoAfiliado($outroTelefoneOrganismoAfiliado) {
        $this->outroTelefoneOrganismoAfiliado = $outroTelefoneOrganismoAfiliado;
    }

    function setFaxOrganismoAfiliado($faxOrganismoAfiliado) {
        $this->faxOrganismoAfiliado = $faxOrganismoAfiliado;
    }

    function setCaixaPostalOrganismoAfiliado($caixaPostalOrganismoAfiliado) {
        $this->caixaPostalOrganismoAfiliado = $caixaPostalOrganismoAfiliado;
    }

    function setCepCaixaPostalOrganismoAfiliado($cepCaixaPostalOrganismoAfiliado) {
        $this->cepCaixaPostalOrganismoAfiliado = $cepCaixaPostalOrganismoAfiliado;
    }

    function setEnderecoCorrespondenciaOrganismoAfiliado($enderecoCorrespondenciaOrganismoAfiliado) {
        $this->enderecoCorrespondenciaOrganismoAfiliado = $enderecoCorrespondenciaOrganismoAfiliado;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setNaoCobrarDashboard($naoCobrarDashboard) {
        $this->naoCobrarDashboard = $naoCobrarDashboard;
    }

        
    
    
    //--------------------------------------------------------------------------

    public function cadastroOrganismo(){

    	$sql = "INSERT INTO organismoAfiliado  
                                 (nomeOrganismoAfiliado,
                                  siglaOrganismoAfiliado,
                                  eraSiglaOrganismoAfiliado,
                                  paisOrganismoAfiliado,
                                  tipoOrganismoAfiliado,
                                  classificacaoOrganismoAfiliado,
                                  classificacaoTemporaria,
                                  cepOrganismoAfiliado,
                                  fk_idRegiaoOrdemRosacruz,
                                  codigoAfiliacaoOrganismoAfiliado,
                                  enderecoOrganismoAfiliado,
                                  numeroOrganismoAfiliado,
                                  emailOrganismoAfiliado,
                                  senhaEmailOrganismoAfiliado,
                                  situacaoOrganismoAfiliado,
                                  fk_idEstado,
                                  fk_idCidade,
                                  bairroOrganismoAfiliado,
                                  complementoOrganismoAfiliado,
                                  cnpjOrganismoAfiliado,
                                  operadoraCelularOrganismoAfiliado,
                                  celularOrganismoAfiliado,
                                  telefoneFixoOrganismoAfiliado,
                                  outroTelefoneOrganismoAfiliado,
                                  faxOrganismoAfiliado,
                                  caixaPostalOrganismoAfiliado,
                                  cepCaixaPostalOrganismoAfiliado,
                                  enderecoCorrespondenciaOrganismoAfiliado,
                                  seqCadast,
                                  latitude,
                                  longitude,
                                  naoCobrarDashboard
                                  )
                            VALUES (:nomeOrganismoAfiliado,
                                    :siglaOrganismoAfiliado,
                                    :eraSiglaOrganismoAfiliado,
                                    :paisOrganismoAfiliado,
                                    :tipoOrganismoAfiliado,
                                    :classificacaoOrganismoAfiliado,
                                    0,
                                    :cepOrganismoAfiliado,
                                    :fk_idRegiaoOrdemRosacruz,
                                    :codigoAfiliacaoOrganismoAfiliado,
                                    :enderecoOrganismoAfiliado,
                                    :numeroOrganismoAfiliado,
                                    :emailOrganismoAfiliado,
                                    :senhaEmailOrganismoAfiliado,
                                    :situacaoOrganismoAfiliado,
                                    :fk_idEstado,
                                    :fk_idCidade,
                                    :bairroOrganismoAfiliado,
                                    :complementoOrganismoAfiliado,
                                    :cnpjOrganismoAfiliado,
                                    :operadoraCelularOrganismoAfiliado,
                                    :celularOrganismoAfiliado,
                                    :telefoneFixoOrganismoAfiliado,
                                    :outroTelefoneOrganismoAfiliado,
                                    :faxOrganismoAfiliado,
                                    :caixaPostalOrganismoAfiliado,
                                    :cepCaixaPostalOrganismoAfiliado,
                                    :enderecoCorrespondenciaOrganismoAfiliado,
                                    :seqCadast,
                                    :latitude,
                                    :longitude,
                                    :naoCobrarDashboard
                                    )";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getCodigoAfiliacaoOrganismoAfiliado = trim($this->getCodigoAfiliacaoOrganismoAfiliado);

        $sth->bindParam(':nomeOrganismoAfiliado', $this->nomeOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':siglaOrganismoAfiliado', $this->siglaOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':eraSiglaOrganismoAfiliado', $this->eraSiglaOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':paisOrganismoAfiliado', $this->paisOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':tipoOrganismoAfiliado', $this->tipoOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':classificacaoOrganismoAfiliado', $this->classificacaoOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':cepOrganismoAfiliado', $this->cepOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':fk_idRegiaoOrdemRosacruz', $this->fk_idRegiaoOrdemRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':codigoAfiliacaoOrganismoAfiliado', $getCodigoAfiliacaoOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':enderecoOrganismoAfiliado', $this->enderecoOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':numeroOrganismoAfiliado', $this->numeroOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':emailOrganismoAfiliado', $this->emailOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':senhaEmailOrganismoAfiliado', $this->senhaEmailOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':situacaoOrganismoAfiliado', $this->situacaoOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':fk_idEstado', $this->fk_idEstado, PDO::PARAM_INT);
        $sth->bindParam(':fk_idCidade', $this->fk_idCidade, PDO::PARAM_INT);
        $sth->bindParam(':bairroOrganismoAfiliado', $this->bairroOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':complementoOrganismoAfiliado', $this->complementoOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':cnpjOrganismoAfiliado', $this->cnpjOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':operadoraCelularOrganismoAfiliado', $this->operadoraCelularOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':celularOrganismoAfiliado', $this->celularOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':telefoneFixoOrganismoAfiliado', $this->telefoneFixoOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':outroTelefoneOrganismoAfiliado', $this->outroTelefoneOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':faxOrganismoAfiliado', $this->faxOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':caixaPostalOrganismoAfiliado', $this->caixaPostalOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':cepCaixaPostalOrganismoAfiliado', $this->cepCaixaPostalOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':enderecoCorrespondenciaOrganismoAfiliado', $this->enderecoCorrespondenciaOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':latitude', $this->latitude, PDO::PARAM_STR);
        $sth->bindParam(':longitude', $this->longitude, PDO::PARAM_STR);
        $sth->bindParam(':naoCobrarDashboard', $this->naoCobrarDashboard, PDO::PARAM_INT);
        https://soa.dev:44300/painelDeControle.php?corpo=#
        if ($c->run($sth,true)){
            $ultimo_id = $this->selecionaUltimoIdCadastrado();
        	return $ultimo_id;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoIdCadastrado(){

        $sql= "SELECT `idOrganismoAfiliado` as id FROM `organismoAfiliado` ORDER BY idOrganismoAfiliado DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['id'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function alteraOrganismo($idOrganismoAfiliado){
        $sql ="UPDATE organismoAfiliado SET  
                                  nomeOrganismoAfiliado = :nomeOrganismoAfiliado,
                                  siglaOrganismoAfiliado =  :siglaOrganismoAfiliado,
                                  eraSiglaOrganismoAfiliado =  :eraSiglaOrganismoAfiliado,
				  paisOrganismoAfiliado = :paisOrganismoAfiliado,
                                  tipoOrganismoAfiliado = :tipoOrganismoAfiliado,
                                  classificacaoOrganismoAfiliado = :classificacaoOrganismoAfiliado,
                                  classificacaoTemporaria = :atuacaoTemporaria,
                                  cepOrganismoAfiliado = :cepOrganismoAfiliado,
                                  fk_idRegiaoOrdemRosacruz = :fk_idRegiaoOrdemRosacruz,
                                  codigoAfiliacaoOrganismoAfiliado = :codigoAfiliacaoOrganismoAfiliado,
                                  enderecoOrganismoAfiliado = :enderecoOrganismoAfiliado,
                                  numeroOrganismoAfiliado = :numeroOrganismoAfiliado,
                                  emailOrganismoAfiliado = :emailOrganismoAfiliado,
                                  senhaEmailOrganismoAfiliado = :senhaEmailOrganismoAfiliado,
                                  situacaoOrganismoAfiliado = :situacaoOrganismoAfiliado,
                                  fk_idEstado = :fk_idEstado,
                                  fk_idCidade = :fk_idCidade,
                                  bairroOrganismoAfiliado = :bairroOrganismoAfiliado,
                                  complementoOrganismoAfiliado = :complementoOrganismoAfiliado,
                                  cnpjOrganismoAfiliado = :cnpjOrganismoAfiliado,
                                  operadoraCelularOrganismoAfiliado = :operadoraCelularOrganismoAfiliado,
                                  celularOrganismoAfiliado = :celularOrganismoAfiliado,
                                  telefoneFixoOrganismoAfiliado = :telefoneFixoOrganismoAfiliado,
                                  outroTelefoneOrganismoAfiliado = :outroTelefoneOrganismoAfiliado,
                                  faxOrganismoAfiliado = :faxOrganismoAfiliado,
                                  caixaPostalOrganismoAfiliado = :caixaPostalOrganismoAfiliado,
                                  cepCaixaPostalOrganismoAfiliado = :cepCaixaPostalOrganismoAfiliado,
                                  enderecoCorrespondenciaOrganismoAfiliado = :enderecoCorrespondenciaOrganismoAfiliado,
                                  latitude = :latitude,
                                  longitude = :longitude,
                                  naoCobrarDashboard = :naoCobrarDashboard
                                  WHERE idOrganismoAfiliado = :idOrganismoAfiliado";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $cod = addslashes($this->codigoAfiliacaoOrganismoAfiliado);

        $sth->bindParam(':nomeOrganismoAfiliado', $this->nomeOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':siglaOrganismoAfiliado', $this->siglaOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':eraSiglaOrganismoAfiliado', $this->eraSiglaOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':paisOrganismoAfiliado', $this->paisOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':tipoOrganismoAfiliado', $this->tipoOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':classificacaoOrganismoAfiliado', $this->classificacaoOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':atuacaoTemporaria', $this->atuacaoTemporaria, PDO::PARAM_INT);
        $sth->bindParam(':cepOrganismoAfiliado', $this->cepOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':fk_idRegiaoOrdemRosacruz', $this->fk_idRegiaoOrdemRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':codigoAfiliacaoOrganismoAfiliado', $cod, PDO::PARAM_INT);
        $sth->bindParam(':enderecoOrganismoAfiliado', $this->enderecoOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':numeroOrganismoAfiliado', $this->numeroOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':emailOrganismoAfiliado', $this->emailOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':senhaEmailOrganismoAfiliado', $this->senhaEmailOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':situacaoOrganismoAfiliado', $this->situacaoOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':fk_idEstado', $this->fk_idEstado, PDO::PARAM_INT);
        $sth->bindParam(':fk_idCidade', $this->fk_idCidade, PDO::PARAM_INT);
        $sth->bindParam(':bairroOrganismoAfiliado', $this->bairroOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':complementoOrganismoAfiliado', $this->complementoOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':cnpjOrganismoAfiliado', $this->cnpjOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':operadoraCelularOrganismoAfiliado', $this->operadoraCelularOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':celularOrganismoAfiliado', $this->celularOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':telefoneFixoOrganismoAfiliado', $this->telefoneFixoOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':outroTelefoneOrganismoAfiliado', $this->outroTelefoneOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':faxOrganismoAfiliado', $this->faxOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':caixaPostalOrganismoAfiliado', $this->caixaPostalOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':cepCaixaPostalOrganismoAfiliado', $this->cepCaixaPostalOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':enderecoCorrespondenciaOrganismoAfiliado', $this->enderecoCorrespondenciaOrganismoAfiliado, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':latitude', $this->latitude, PDO::PARAM_STR);
        $sth->bindParam(':longitude', $this->longitude, PDO::PARAM_STR);
        $sth->bindParam(':naoCobrarDashboard', $this->naoCobrarDashboard, PDO::PARAM_INT);

        if ($c->run($sth,true)){

            return true;
        }else{

            return false;
        };
    }

    public function listaOrganismo($p=null,$siglaOA=null,$orderBy=null,$regiao=null,$idRegiaoRosacruz=null,$idOrganismoAfiliado=null,$menosId=null, $tipo=null, $arrOALotacao=null, $classificacaoOa=null,$classificacoesOa=null,$ativo=null,$start=null,$limit=null){

    	$sql ="SELECT *,c.nome as cidade, e.nome as estado FROM organismoAfiliado as o
    	left join cidade as c on fk_idCidade = c.id
    	left join estado as e on fk_idEstado = e.id
    	left join regiaoRosacruz as r on o.fk_idRegiaoOrdemRosacruz = r.idRegiaoRosacruz
    	where 1=1 ";
    	if($p!=null)
    	{
    		$sql .= " and (nomeOrganismoAfiliado like :p or siglaOrganismoAfiliado like :p) ";
    	}
    	if($siglaOA!=null)
    	{
    		$sql .= " and UPPER(siglaOrganismoAfiliado) = :siglaOA ";
    	}
    	if($regiao!=null)
    	{
    		$sql .= " and siglaOrganismoAfiliado like :regiao ";
    	}
    	if($idRegiaoRosacruz!=null)
    	{
    		$sql .= " and fk_idRegiaoOrdemRosacruz = :idRegiaoRosacruz ";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and idOrganismoAfiliado = :idOrganismoAfiliado ";
    	}
    	if($tipo!=null)
    	{
    		$sql .= " and tipoOrganismoAfiliado = :tipo ";
    	}
    	if($menosId!=null)
    	{
    		$sql .= " and idOrganismoAfiliado <> :menosId ";
    	}
        if($classificacaoOa!=null)
    	{
    		$sql .= " and classificacaoOrganismoAfiliado = :classificacaoOa ";
    	}
        if($classificacoesOa!=null)
    	{
            $ids = explode(",", $classificacoesOa);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {    
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and classificacaoOrganismoAfiliado IN (".$inQuery.")";
    	}
        if($arrOALotacao!=null&&count($arrOALotacao)>0)
    	{
            $x=0;
            //Eliminar posições vazias
            $arrOALotacao = array_filter($arrOALotacao);
            $sql .= " and UPPER(siglaOrganismoAfiliado) IN (";
            foreach($arrOALotacao as $sigla)
            {
                if(ctype_alnum($sigla))//Ver se a sigla é alphanumerica
                {
                    if($x==0)
                    {
                        $sql .= " '".strtoupper($sigla)."'";
                    }else{
                        $sql .= ", '".strtoupper($sigla)."'";
                    }
                }
                $x++;
            }
            $sql .= ")";
    	}
        //echo "--->Ativo:".$ativo;
        if($ativo!=null)
    	{
    		$sql .= " and statusOrganismoAfiliado != 1 ";
    	}
    	if($orderBy==null)
    	{
    		$sql .=" ORDER BY idOrganismoAfiliado ASC";
    	}else{
    		$sql .=" ORDER BY ".$orderBy." ASC";
    	}
        if ($limit != null) {
		$sql .= " limit :start, :limit";
	}
        //echo $sql;exit;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $siglaOA = strtoupper($siglaOA);

        if($p!=null)
    	{
            $p = '%'.$p.'%';
            $sth->bindParam(':p', $p , PDO::PARAM_STR);
        }
        if($siglaOA!=null)
    	{
            $sth->bindParam(':siglaOA', $siglaOA, PDO::PARAM_STR);
        }
        if($regiao!=null)
    	{
            $regiao = $regiao."%";
            $sth->bindParam(':regiao', $regiao, PDO::PARAM_STR);
        }
        if($idRegiaoRosacruz!=null)
    	{
            $sth->bindParam(':idRegiaoRosacruz', $idRegiaoRosacruz, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($tipo!=null)
    	{
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_STR);
        }
        if($menosId!=null)
    	{
            $sth->bindParam(':menosId', $menosId, PDO::PARAM_STR);
        }
        /*
        if($orderBy!=null)
    	{
            $sth->bindParam(':orderBy', $orderBy, PDO::PARAM_STR);
        }*/
        if($classificacaoOa!=null)
    	{
            $sth->bindParam(':classificacaoOa', $classificacaoOa, PDO::PARAM_INT);
        }
        if($classificacoesOa!=null)
        {
            foreach ($ids as $u => $id)
            {    
                 $sth->bindValue(":".$u, $id);
            }
        }   
          
        if ($start > 0) {
            //echo "10";
            $sth->bindValue(':start', (int) trim($start), PDO::PARAM_INT);
        }
       
        if ($limit != null) {
           // echo "11";
            $sth->bindValue(':limit', (int) trim($limit), PDO::PARAM_INT);
        }
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaCadastrado($seqCadast=null){

    	$sql ="SELECT * FROM organismoAfiliado where 1=1 ";
    	if($seqCadast!=null)
    	{
    		$sql .= " and seqCadast = :seqCadast ";
    	}
    	$sql .=" ORDER BY idOrganismoAfiliado ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }

        if ($c->run($sth,null,null,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaIdOrganismo($idOrganismoAfiliado=null,$email=null)
    {

        $query = "SELECT * FROM  organismoAfiliado
                                WHERE   1=1 ";
        if($idOrganismoAfiliado!=null)
        {
        	$query .= " and idOrganismoAfiliado = :idOrganismoAfiliado";
        }
    	if($email!=null)
        {
        	$query .= " and TRIM(emailOrganismoAfiliado) = :email";
        }
        //echo "<pre>";print_r($query);exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($email!=null)
        {
            $sth->bindParam(':email', $email, PDO::PARAM_STR);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaOrganismoPelaSigla($sigla=null,$siglaAntiga=null){

        $query = "SELECT * FROM  organismoAfiliado WHERE   1=1 ";
        if($sigla!=null)
        {
            $query .= " and siglaOrganismoAfiliado = :sigla";
        }
        if($siglaAntiga!=null)
        {
            $query .= " and eraSiglaOrganismoAfiliado = :siglaAntiga";
        }

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        if($sigla!=null)
        {
            $sth->bindParam(':sigla', $sigla, PDO::PARAM_STR);
        }
        if($siglaAntiga!=null)
        {
            $sth->bindParam(':siglaAntiga', $siglaAntiga, PDO::PARAM_STR);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraStatusOrganismo() {

    	$sql = "UPDATE organismoAfiliado SET statusOrganismoAfiliado = :statusOrganismoAfiliado "
                . "WHERE idOrganismoAfiliado = :idOrganismoAfiliado";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusOrganismoAfiliado', $this->statusOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            	$arr = array();
                $arr['status'] = $this->getStatusOrganismoAfiliado();
            return $arr;
        } else {
            return false;
        }
    }

    public function alteraSenhaEmailOrganismo($id,$senha) {

    	$sql = "UPDATE organismoAfiliado SET senhaEmailOrganismoAfiliado = :senha "
                . "WHERE idOrganismoAfiliado = :id";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':senha', $senha, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function cadastroPlanilhaSenhasAtualizadas(){

        $sql = "INSERT INTO planilhaSenhasAtualizadas  
                                 (dataCadastro
                                  )
                            VALUES (now()
                                    )";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdCadastradoPlanilhaSenhasAtualizadas(){

        $sql= "SELECT `id` as id FROM `planilhaSenhasAtualizadas` ORDER BY id DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['id'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function retornaOaNaoCadastrado($arrCadastrados){

        $str = implode(",",$arrCadastrados);

        $sql = "SELECT `idOrganismoAfiliado` as id, siglaOrganismoAfiliado as sigla FROM `organismoAfiliado` ";

        if(count($arrCadastrados)>0)
        {
            $sql .= "         where idOrganismoAfiliado 
                        NOT IN (".$str.") ";
        }

        $sql .= "
                        ORDER BY idOrganismoAfiliado ASC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $arr=array();

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $arr['id'] = $vetor['id'];
                $arr['sigla'] = $vetor['sigla'];
            }
            return $arr;
        } else {
            return false;
        }
    }

    public function retornaSiglaOa($id){

        $sql = "SELECT `idOrganismoAfiliado` as id, siglaOrganismoAfiliado as sigla FROM `organismoAfiliado` 
                    where idOrganismoAfiliado 
                    IN (".$id.") 
                    ORDER BY idOrganismoAfiliado ASC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $arr=array();

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $arr['id'] = $vetor['id'];
                $arr['sigla'] = $vetor['sigla'];
            }
            return $arr;
        } else {
            return false;
        }
    }

    public function getIdUltimoOrganismoCadastrado(){

        $sql ="SELECT idOrganismoAfiliado as id FROM organismoAfiliado as o
    	order by idOrganismoAfiliado desc limit 0,1";

        //echo $sql;exit;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);

        if ($resultado){
            //echo "<pre>";print_r($resultado);
            return $resultado[0]['id'];
        }else{
            return false;
        }
    }

    public function retornaOaNaoCadastradoFirebase($arrFoco){

        $sql = "SELECT `idOrganismoAfiliado` as id, siglaOrganismoAfiliado as sigla FROM `organismoAfiliado` 
                    where 
                    1=1
                    and ( 
                    ";
        $x=0;
                foreach($arrFoco as $k => $v)
                {
                    if($x==0)
                    {
                        $sql .= " idOrganismoAfiliado = ".$v;
                    }else{
                        $sql .= " or idOrganismoAfiliado = ".$v;
                    }
                    $x++;

                }
        $sql .= " ) ORDER BY idOrganismoAfiliado ASC LIMIT 1";

                //echo "<br>Sql:".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $id=0;

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $id = $vetor['id'];
            }
            return $id;
        } else {
            return false;
        }
    }

    // rk
    public function getAllId($data=null) {
        $sql = $data != null ? "SELECT idOrganismoAfiliado as Id FROM `organismoAfiliado` WHERE `statusOrganismoAfiliado` = " . $data['status']
                             : "SELECT idOrganismoAfiliado as Id FROM `organismoAfiliado`";

        $c = new conexaoSOA();
        return $c->run($c->openSOA()->prepare($sql));
    }

}

?>
