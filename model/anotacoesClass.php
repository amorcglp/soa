<?php

require_once ("conexaoClass.php");

class anotacoes{

    private $idAnotacoes;
    private $tituloAnotacoes;
    private $conteudoAnotacoes;
    private $dataAnotacoes;
    private $horaAnotacoes;
    private $fk_idUsuario;

    function getIdAnotacoes() {
        return $this->idAnotacoes;
    }

    function getTituloAnotacoes() {
        return $this->tituloAnotacoes;
    }

    function getConteudoAnotacoes() {
        return $this->conteudoAnotacoes;
    }

    function getDataAnotacoes() {
        return $this->dataAnotacoes;
    }

    function getHoraAnotacoes() {
        return $this->horaAnotacoes;
    }

    function getFk_idUsuario() {
        return $this->fk_idUsuario;
    }

    function setIdAnotacoes($idAnotacoes) {
        $this->idAnotacoes = $idAnotacoes;
    }

    function setTituloAnotacoes($tituloAnotacoes) {
        $this->tituloAnotacoes = $tituloAnotacoes;
    }

    function setConteudoAnotacoes($conteudoAnotacoes) {
        $this->conteudoAnotacoes = $conteudoAnotacoes;
    }

    function setDataAnotacoes($dataAnotacoes) {
        $this->dataAnotacoes = $dataAnotacoes;
    }

    function setHoraAnotacoes($horaAnotacoes) {
        $this->horaAnotacoes = $horaAnotacoes;
    }

    function setFk_idUsuario($fk_idUsuario) {
        $this->fk_idUsuario = $fk_idUsuario;
    }

    //--------------------------------------------------------------------------

    /**
     * [Realiza cadastro de ]
     * @return [boolean] [Em caso de sucesso no retorno do PDO retorna TRUE]
     */
    public function cadastroAnotacoes(){

        $sql = "INSERT INTO anotacoes (idAnotacoes, tituloAnotacoes, conteudoAnotacoes, dataAnotacoes, horaAnotacoes, fk_idUsuario) 
        VALUES (
        :idAnotacoes,
        :tituloAnotacoes,
        :conteudoAnotacoes,
        :dataAnotacoes,
        :horaAnotacoes,
        :fk_idUsuario
        )";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAnotacoes', $this->idAnotacoes, PDO::PARAM_INT);
        $sth->bindParam(':tituloAnotacoes', $this->tituloAnotacoes, PDO::PARAM_STR);
        $sth->bindParam(':conteudoAnotacoes', $this->conteudoAnotacoes, PDO::PARAM_STR);
        $sth->bindParam(':dataAnotacoes', $this->dataAnotacoes, PDO::PARAM_STR);
        $sth->bindParam(':horaAnotacoes', $this->horaAnotacoes, PDO::PARAM_STR);
        $sth->bindParam(':fk_idUsuario', $this->fk_idUsuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraAnotacoes($id){
        
        $sql = "UPDATE anotacoes SET
        `tituloAnotacoes` = :tituloAnotacoes,
        `conteudoAnotacoes` = :conteudoAnotacoes,
        `dataAnotacoes` = :dataAnotacoes,
        `horaAnotacoes` = :horaAnotacoes
        WHERE idAnotacoes = :idAnotacoes";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAnotacoes', $id, PDO::PARAM_INT);
        $sth->bindParam(':tituloAnotacoes', $this->tituloAnotacoes, PDO::PARAM_STR);
        $sth->bindParam(':conteudoAnotacoes', $this->conteudoAnotacoes, PDO::PARAM_STR);
        $sth->bindParam(':dataAnotacoes', $this->dataAnotacoes, PDO::PARAM_STR);
        $sth->bindParam(':horaAnotacoes', $this->horaAnotacoes, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function listaAnotacoes($fk_idUsuario){

        $sql = "SELECT * FROM anotacoes where fk_idUsuario=:fk_idUsuario ORDER BY dataAnotacoes ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idUsuario', $fk_idUsuario, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdAnotacoes($idAnotacoes){
        $sql = "SELECT * FROM  anotacoes WHERE   idAnotacoes = :idAnotacoes";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAnotacoes', $idAnotacoes, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeAnotacoes($idAnotacoes){
        $sql = "DELETE FROM anotacoes WHERE idAnotacoes = :idAnotacoes";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAnotacoes', $idAnotacoes, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }


}

?>
