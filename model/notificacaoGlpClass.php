<?php

require_once ("conexaoClass.php");

class NotificacaoGlp{

    private $fk_idNotificacaoGlp;
    private $fk_seqCadast;
    private $idNotificacaoGlp;
    private $tituloNotificacaoGlp;
    private $mensagemNotificacaoGlp;
    private $dataNotificacaoGlp;
    private $tipoNotificacaoGlp;
    private $statusNotificacaoGlp;
    private $fk_seqCadastRemetente;
    
    //METODO SET/GET -----------------------------------------------------------

    function getFk_idNotificacaoGlp() {
        return $this->fk_idNotificacaoGlp;
    }
    
	function getFk_seqCadast() {
        return $this->fk_seqCadast;
    }

	function setFk_idNotificacaoGlp($fk_idNotificacaoGlp) {
        $this->fk_idNotificacaoGlp = $fk_idNotificacaoGlp;
    }
    
	function setFk_seqCadast($fk_seqCadast) {
        $this->fk_seqCadast = $fk_seqCadast;
    }

    //--------------------------------------------------------------------------

    public function cadastroNotificacao($tituloNotificacaoGlp,$mensagemNotificacaoGlp,$tipoNotificacaoGlp,$fk_seqCadastRemetente){

        $sql="INSERT INTO notificacaoGLP  
                                 (
                                  tituloNotificacaoGlp,
                                  mensagemNotificacaoGlp,
                                  dataNotificacaoGlp,
                                  tipoNotificacaoGlp,
                                  fk_seqCadastRemetente
                                  )
                            VALUES (:titulo,
		                            :mensagem,
		                            NOW(),
		                            :tipo,
		                            :seqCadast)";  
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $tituloNotificacaoGlp, PDO::PARAM_STR);
        $sth->bindParam(':mensagem', $mensagemNotificacaoGlp , PDO::PARAM_STR);
        $sth->bindParam(':tipo', $tipoNotificacaoGlp, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $fk_seqCadastRemetente, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idNotificacaoGlp) as lastid FROM notificacaoGLP";
        
        $ultimo_id=0;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado)
        {
        	foreach ($resultado as $vetor)
        	{
        		$ultimo_id = $vetor['lastid']; 
        	}
        }
        return $ultimo_id;
    }
    
    public function cadastroNotificacaoGlp($idNotificacao,$seqCadast){

        $sql="INSERT INTO notificacaoGLP_usuario  
                                 (
                                  fk_idNotificacaoGlp,
                                  fk_seqCadast
                                  )
                            VALUES (:id,
                            :seqCadast)";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idNotificacao, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    public function listaNotificacaoOficialGlp($fk_seqCadast=null,$fk_idNotificacaoGlp=null,$pagina=null){

    	$sql = "SELECT * FROM notificacaoGLP_usuario WHERE 1=1";
        $sql .= ($fk_seqCadast!=null) ? " and fk_seqCadast=:seqCadast": "";
        $sql .= ($fk_idNotificacaoGlp!=null) ? " and fk_idNotificacaoGlp=:id" : "";
    	$sql .= ($pagina != null) ? " ORDER BY fk_idNotificacaoGlp DESC LIMIT :pagina,10" : " ORDER BY fk_idNotificacaoGlp DESC LIMIT 0,10";
    	
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        if($fk_seqCadast!=null)
        {    
            $sth->bindParam(':seqCadast', $fk_seqCadast, PDO::PARAM_INT);
        }
        if($fk_idNotificacaoGlp!=null)
        {
            $sth->bindParam(':id', $fk_idNotificacaoGlp, PDO::PARAM_INT);
        }
        if($pagina!=null)
        {    
            $pagina = (($pagina-1)*10);
            $sth->bindParam(':pagina', $pagina, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function retornaNumeroNotificacaoOficialGlp($fk_seqCadast=null)
    {
        $sql = "SELECT * FROM notificacaoGLP_usuario WHERE 1=1";
        $sql .= ($fk_seqCadast!=null) ? " and fk_seqCadast=:seqCadast": "";

        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($fk_seqCadast!=null)
        {
            $sth->bindParam(':seqCadast', $fk_seqCadast, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return count($resultado);
        }else{
            return false;
        }
    }
    
    public function listaNotificacaoGlp($idNotificacaoGlp=null){

    	$sql = "SELECT * FROM notificacaoGLP as nglp where 1=1 ";
    	if($idNotificacaoGlp!=null)
    	{
    		$sql .= " and idNotificacaoGlp=:id";
    	}
    	
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idNotificacaoGlp!=null)
    	{
            $sth->bindParam(':id', $idNotificacaoGlp, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

	public function listaNotificacaoDropdown($ativo=null,$seqCadast) {

    	$sql = "SELECT nglp.*,nglpu.* FROM notificacaoGLP as nglp "
                . " inner join notificacaoGLP_usuario as nglpu on nglpu.fk_idNotificacaoGlp = nglp.idNotificacaoGlp where 1=1";
    	if($ativo!=null)
    	{
    		$sql .= " and statusNotificacaoGlpUsuario = 0 ";
    	}
	if($seqCadast!=null)
    	{
    		$sql .= " and fk_seqCadast =:seqCadast";
    	}
        $sql .= " order by idNotificacaoGlp desc";
        
        //echo $sql;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seqCadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function lista($seq_cadast=null,$idNotificacaoGlp=null){

    	$sql = "SELECT * FROM notificacaoGLP_usuario
    	where 1=1
    	 ";   
    	if($seq_cadast!=null)
    	{
    		$sql .= " and fk_seqCadast=:seqCadast";
    	}
    	if($idNotificacaoGlp!=null)
    	{
    		$sql .= " and fk_idNotificacaoGlp=:id";
    	}	 	 
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seq_cadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        if($idNotificacaoGlp!=null)
    	{
            $sth->bindParam(':id', $idNotificacaoGlp, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return count($resultado);
        }else{
            return false;
        }
    }
    
    public function buscaDepartamento($idDepartamento) {
        
        $sql = "SELECT * FROM departamento 
                                 WHERE `idDepartamento` = :id LIMIT 1";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idDepartamento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function alteraStatusNotificacaoGlp() {
        
        $sql = "UPDATE notificacaoGLP_usuario SET `statusNotificacaoGlpUsuario` = 1 "
                . "WHERE `fk_idNotificacaoGlp` = :id "
                . "and `fk_seqCadast` = :seqCadast";
		
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idNotificacaoGlp, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->fk_seqCadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraStatusNotificacao($status, $idNotificacao,$seqCadast) {

        $sql = "UPDATE notificacaoGLP_usuario
                  SET `statusNotificacaoGlpUsuario` = :status
                  WHERE `fk_idNotificacaoGlp` = :id
                  and `fk_seqCadast` = :seqCadast";

        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idNotificacao, PDO::PARAM_INT);
        $sth->bindParam(':status', $status , PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}
?>
