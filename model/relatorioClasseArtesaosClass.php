<?php

require_once ("conexaoClass.php");

class relatorioClasseArtesaos {

    private $idRelatorioClasseArtesaos;
    private $fk_idOrganismoAfiliado;
    private $mesCompetencia;
    private $anoCompetencia;
    private $dataEncontro;
    private $numeroParticipantes;
    private $naturezaMensagemApresentada;
    private $questoesLevantadas;
    private $climaGeralClasse;
    private $surgiuProblema;
    private $surgiuProblemaQual;
    private $observacoes;
    private $ultimoAtualizar;
    private $usuario;

    //METODO SET/GET -----------------------------------------------------------

    function getIdRelatorioClasseArtesaos() {
        return $this->idRelatorioClasseArtesaos;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getMesCompetencia() {
        return $this->mesCompetencia;
    }

    function getAnoCompetencia() {
        return $this->anoCompetencia;
    }

    function getDataEncontro() {
        return $this->dataEncontro;
    }

    function getNumeroParticipantes() {
        return $this->numeroParticipantes;
    }

    function getNaturezaMensagemApresentada() {
        return $this->naturezaMensagemApresentada;
    }

    function getQuestoesLevantadas() {
        return $this->questoesLevantadas;
    }

    function getClimaGeralClasse() {
        return $this->climaGeralClasse;
    }

    function getSurgiuProblema() {
        return $this->surgiuProblema;
    }

    function getSurgiuProblemaQual() {
        return $this->surgiuProblemaQual;
    }

    function getObservacoes() {
        return $this->observacoes;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setIdRelatorioClasseArtesaos($idRelatorioClasseArtesaos) {
        $this->idRelatorioClasseArtesaos = $idRelatorioClasseArtesaos;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setMesCompetencia($mesCompetencia) {
        $this->mesCompetencia = $mesCompetencia;
    }

    function setAnoCompetencia($anoCompetencia) {
        $this->anoCompetencia = $anoCompetencia;
    }

    function setDataEncontro($dataEncontro) {
        $this->dataEncontro = $dataEncontro;
    }

    function setNumeroParticipantes($numeroParticipantes) {
        $this->numeroParticipantes = $numeroParticipantes;
    }

    function setNaturezaMensagemApresentada($naturezaMensagemApresentada) {
        $this->naturezaMensagemApresentada = $naturezaMensagemApresentada;
    }

    function setQuestoesLevantadas($questoesLevantadas) {
        $this->questoesLevantadas = $questoesLevantadas;
    }

    function setClimaGeralClasse($climaGeralClasse) {
        $this->climaGeralClasse = $climaGeralClasse;
    }

    function setSurgiuProblema($surgiuProblema) {
        $this->surgiuProblema = $surgiuProblema;
    }

    function setSurgiuProblemaQual($surgiuProblemaQual) {
        $this->surgiuProblemaQual = $surgiuProblemaQual;
    }

    function setObservacoes($observacoes) {
        $this->observacoes = $observacoes;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    //--------------------------------------------------------------------------

    public function cadastroRelatorioClasseArtesaos() {

        $sql = "INSERT INTO relatorioClasseArtesaos  
                                 (
                                  fk_idOrganismoAfiliado,
                                  mesCompetencia,
                                  anoCompetencia,
                                  dataEncontro,
                                  numeroParticipantes,
                                  naturezaMensagemApresentada,
                                  questoesLevantadas,
                                  climaGeralClasse,
                                  surgiuProblema,
                                  surgiuProblemaQual,
                                  observacoes,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                                    :idOrganismoAfiliado,
                                    :mes,    
                                    :ano,    
                                    :dataEncontro,
                                    :numeroParticipantes,
                                    :naturezaMensagemApresentada,
                                    :questoesLevantadas,
                                    :climaGeralClasse,
                                    :surgiuProblema,
                                    :surgiuProblemaQual,
                                    :observacoes,
                                    :usuario,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':mes', $this->mesCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':dataEncontro', $this->dataEncontro, PDO::PARAM_STR);
        $sth->bindParam(':numeroParticipantes', $this->numeroParticipantes, PDO::PARAM_INT);
        $sth->bindParam(':naturezaMensagemApresentada', $this->naturezaMensagemApresentada, PDO::PARAM_STR);
        $sth->bindParam(':questoesLevantadas',  $this->questoesLevantadas, PDO::PARAM_STR);
        $sth->bindParam(':climaGeralClasse', $this->climaGeralClasse, PDO::PARAM_STR);
        $sth->bindParam(':surgiuProblema', $this->surgiuProblema, PDO::PARAM_INT);
        $sth->bindParam(':surgiuProblemaQual', $this->surgiuProblemaQual, PDO::PARAM_STR);
        $sth->bindParam(':observacoes',  $this->observacoes, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            /*
              $con = new conexaoSOA();
              $conexao = $con->openSOA();
              return $conexao->lastInsertId();
             */
            $ultimo_id = 0;
            $resultado = $this->selecionaUltimoId();
            if ($resultado) {
                foreach ($resultado as $vetor) {
                    $ultimo_id = $vetor['lastid'];
                }
            }
            return $ultimo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idRelatorioClasseArtesaos) as lastid FROM relatorioClasseArtesaos";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraRelatorioClasseArtesaos($idRelatorioClasseArtesaos) {

        $sql = "UPDATE relatorioClasseArtesaos SET  
                                    mesCompetencia = :mes,    
                                    anoCompetencia = :ano,
                                    dataEncontro = :dataEncontro,
                                    numeroParticipantes = :numeroParticipantes,
				    naturezaMensagemApresentada = :naturezaMensagemApresentada,
				    questoesLevantadas = :questoesLevantadas,
				    climaGeralClasse = :climaGeralClasse,
				    surgiuProblema = :surgiuProblema,
				    surgiuProblemaQual = :surgiuProblemaQual,
				    observacoes = :observacoes,
                                    ultimoAtualizar = :ultimoAtualizar
                                  WHERE idRelatorioClasseArtesaos = :idRelatorioClasseArtesaos";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mesCompetencia, PDO::PARAM_STR);
        $sth->bindParam(':ano', $this->anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':dataEncontro', $this->dataEncontro, PDO::PARAM_STR);
        $sth->bindParam(':numeroParticipantes', $this->numeroParticipantes, PDO::PARAM_INT);
        $sth->bindParam(':naturezaMensagemApresentada', $this->naturezaMensagemApresentada, PDO::PARAM_STR);
        $sth->bindParam(':questoesLevantadas',  $this->questoesLevantadas, PDO::PARAM_STR);
        $sth->bindParam(':climaGeralClasse', $this->climaGeralClasse, PDO::PARAM_STR);
        $sth->bindParam(':surgiuProblema', $this->surgiuProblema, PDO::PARAM_INT);
        $sth->bindParam(':surgiuProblemaQual', $this->surgiuProblemaQual, PDO::PARAM_STR);
        $sth->bindParam(':observacoes',  $this->observacoes, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':idRelatorioClasseArtesaos', $idRelatorioClasseArtesaos, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        };
    }

    public function listaRelatorioClasseArtesaos($idOa=null,$mes=false,$ano=false,$entregueCompletamente=false) {

        $sql = "SELECT * FROM relatorioClasseArtesaos
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado ";

        if($mes!=null)
        {
            $sql .= " and mesCompetencia=:mes";
        }
        if($ano!=null)
        {
            $sql .= " and anoCompetencia=:ano";
        }

        if($idOa!=null)
        {
            $sql .= " where fk_idOrganismoAfiliado = :idOrganismoAfiliado ";
        }

        if($entregueCompletamente)
        {
            $sql .= " and entregueCompletamente=1";
        }

        $sql .= " order by anoCompetencia, mesCompetencia ";

        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mes!=null)
        {
            $sth->bindParam(':mes', $mes, PDO::PARAM_STR);
        }

        if($ano!=null)
        {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }

        if($idOa!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado',$idOa, PDO::PARAM_INT);
        }


        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaExiste() {

        $sql = "SELECT * FROM relatorioClasseArtesaos where 1=1 
    			and mesCompetencia = :mes
                        and anoCompetencia = :ano    
                        and fk_idOrganismoAfiliado =  :idOrganismoAfiliado
                        and dataEncontro = :dataEncontro
                ";
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mesCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado',$this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataEncontro', $this->dataEncontro, PDO::PARAM_STR);
        
        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscarIdRelatorioClasseArtesaos($idRelatorioClasseArtesaos) {
        
        $sql = "SELECT * FROM  relatorioClasseArtesaos as a
        	inner join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                WHERE   idRelatorioClasseArtesaos = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioClasseArtesaos, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeTermoVoluntariado($idRelatorioClasseArtesaos) {
        
        $sql = "DELETE FROM relatorioClasseArtesaos WHERE idRelatorioClasseArtesaos = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioClasseArtesaos, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaAssinatura($codigo){

        $sql = "SELECT * FROM  relatorioClasseArtesaos as a
                WHERE   numeroAssinatura = :codigo";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinaturaIndividual($codigo,$codigoIndividual){

        $sql = "SELECT * FROM  relatorioClasseArtesaos as a
                inner join relatorioClasseArtesaos_assinatura_eletronica on fk_idRelatorioClasseArtesaos = idRelatorioClasseArtesaos
                WHERE   numeroAssinatura = :codigo and codigoAssinaturaIndividual = :codigoIndividual";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);
        $sth->bindParam(':codigoIndividual', $codigoIndividual , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function entregar($id,$quemEntregou)
    {
        $sql = "UPDATE relatorioClasseArtesaos SET
        						  entregue =  1, dataEntrega=now(), quemEntregou = :quemEntregou
                                  WHERE idRelatorioClasseArtesaos = :id";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregou', $quemEntregou, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscarRelatorioClasseArtesaosQuePrecisaAssinatura($idOrganismoAfiliado)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  relatorioClasseArtesaos as a
                WHERE   entregue = 1 and fk_idOrganismoAfiliado = :idOrganismoAfiliado";

        $sql .= " order by anoCompetencia,mesCompetencia";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function assinaturaEletronicaRelatorioClasseArtesaos($seqCadast,$ip,$idOrganismoAfiliado,$funcao,$idRelatorio,$codigoAssinaturaIndividual){

        //echo "idAta=>".$idAta;
        $sql = "INSERT INTO relatorioClasseArtesaos_assinatura_eletronica
                                 (fk_idRelatorioClasseArtesaos,
                                  codigoAssinaturaIndividual,
                                  seqCadast,
                                  fk_idFuncao,
                                  ip,
                                  fk_idOrganismoAfiliado,
                                  data
                                  )
                            VALUES (:fk_idRelatorioClasseArtesaos,
                                    :codigoAssinaturaIndividual,
                                    :seqCadast,
                                    :fk_idFuncao,
                                    :ip,
                                    :fk_idOrganismoAfiliado,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':fk_idFuncao', $funcao, PDO::PARAM_INT);
        $sth->bindParam(':fk_idRelatorioClasseArtesaos', $idRelatorio, PDO::PARAM_INT);
        $sth->bindParam(':codigoAssinaturaIndividual', $codigoAssinaturaIndividual, PDO::PARAM_INT);


        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarAssinaturasEmRelatorioClasseArtesaos($seqCadast=null,$idOrganismoAfiliado=null,$codigoIndividual=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT *,a.data as dataEntrega FROM  relatorioClasseArtesaos_assinatura_eletronica as a
                left join relatorioClasseArtesaos as at on at.idRelatorioClasseArtesaos = a.fk_idRelatorioClasseArtesaos
                left join usuario as u on u.seqCadast = a.seqCadast
                left join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE  1=1 ";

        if($seqCadast!=null)
        {
            $sql .= " and a.seqCadast = :seqCadast";
        }

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and a.fk_idOrganismoAfiliado = :idOrganismoAfiliado group by idRelatorioClasseArtesaosAssinaturaEletronica";
        }

        if($codigoIndividual!=null)
        {
            $sql .= " and a.codigoAssinaturaIndividual = :codigoIndividual";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null) {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if($codigoIndividual!=null)
        {
            $sth->bindParam(':codigoIndividual', $codigoIndividual, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);

        if($codigoIndividual!=null)
        {
            return $resultado;
        }else{
            if($idOrganismoAfiliado==null) {

                if ($resultado) {
                    $arr = array();
                    foreach ($resultado as $v) {
                        $arr[] = $v['fk_idRelatorioClasseArtesaos'];
                    }
                    return $arr;
                } else {
                    return false;
                }
            }else{
                return $resultado;
            }
        }

    }

    public function buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($id,$idFuncao=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  relatorioClasseArtesaos_assinatura_eletronica as a
                left join usuario as u on u.seqCadast = a.seqCadast
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE   fk_idRelatorioClasseArtesaos = :id";
        if($idFuncao!=null)
        {
            $sql .= " and fk_idFuncao = :idFuncao";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        if($idFuncao!=null)
        {
            $sth->bindParam(':idFuncao', $idFuncao, PDO::PARAM_INT);
        }

        if($idFuncao==null)
        {
            //echo "aqui Fio2!";
            $resultado = $c->run($sth);
            if ($resultado){
                return $resultado;
            } else {
                return false;
            }
        }else{
            //echo "aqui Fio1!";
            $resultado = $c->run($sth,null,null,true);
            if ($resultado){

                return true;
            } else {
                return false;
            }
        }

    }

    public function entregarCompletamente($id,$quemEntregouCompletamente)
    {
        $sql = "UPDATE relatorioClasseArtesaos SET
        						  entregueCompletamente =  1,
        						  quemEntregouCompletamente = :quemEntregouCompletamente,
        						  dataEntregouCompletamente = now()
                                  WHERE idRelatorioClasseArtesaos = :id";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregouCompletamente', $quemEntregouCompletamente, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function atualizaNumeroAssinatura($numeroAssinatura)
    {
        $sql = "UPDATE relatorioClasseArtesaos SET
        						  numeroAssinatura =  :numeroAssinatura
                                  WHERE idRelatorioClasseArtesaos = :id";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':numeroAssinatura', $numeroAssinatura, PDO::PARAM_STR);
        $sth->bindParam(':id', $this->idRelatorioClasseArtesaos, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


}

?>
