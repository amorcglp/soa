<?php

require_once ("conexaoClass.php");

class convocacaoRitualisticaOGGMembro{

    private $idConvocacaoRitualisticaOGGMembro;
    private $fk_idConvocacaoRitualisticaOGG;
    private $fk_seq_cadastMembro;
    private $tipo;
    private $email;

    //METODO SET/GET -----------------------------------------------------------

    function getIdConvocacaoRitualisticaOGGMembro() {
        return $this->idConvocacaoRitualisticaOGGMembro;
    }

    function getFk_idConvocacaoRitualisticaOGG() {
        return $this->fk_idConvocacaoRitualisticaOGG;
    }

    function getFk_seq_cadastMembro() {
        return $this->fk_seq_cadastMembro;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getEmail() {
        return $this->email;
    }

    function setIdConvocacaoRitualisticaOGGMembro($idConvocacaoRitualisticaOGGMembro) {
        $this->idConvocacaoRitualisticaOGGMembro = $idConvocacaoRitualisticaOGGMembro;
    }

    function setFk_idConvocacaoRitualisticaOGG($fk_idConvocacaoRitualisticaOGG) {
        $this->fk_idConvocacaoRitualisticaOGG = $fk_idConvocacaoRitualisticaOGG;
    }

    function setFk_seq_cadastMembro($fk_seq_cadastMembro) {
        $this->fk_seq_cadastMembro = $fk_seq_cadastMembro;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setEmail($email) {
        $this->email = $email;
    }

            
    //--------------------------------------------------------------------------

    public function cadastro(){

        $sql = "INSERT INTO convocacaoRitualisticaOGG_membro
                                 (
                                  fk_idConvocacaoRitualisticaOGG,
                                  fk_seq_cadastMembro,
                                  tipo,
                                  email
                                  )
                            VALUES (:fk_idConvocacaoRitualisticaOGG,
                                    :fk_seq_cadastMembro,
                                    :tipo,
                                    :email)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $this->fk_idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadastMembro', $this->fk_seq_cadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function lista($idConvocacaoRitualisticaOGG=null,$v=null,$tipo=null){

    	$sql = "SELECT * FROM convocacaoRitualisticaOGG_membro where 1=1";

    	if($idConvocacaoRitualisticaOGG!=null) {
    		$sql .= " and fk_idConvocacaoRitualisticaOGG=:fk_idConvocacaoRitualisticaOGG";
    	}
        if($v!=null) {
    		$sql .= " and fk_seq_cadastMembro=:fk_seq_cadastMembro";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idConvocacaoRitualisticaOGG!=null){
            $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':fk_seq_cadastMembro', $v, PDO::PARAM_INT);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function total($idConvocacaoRitualisticaOGG=null,$v=null,$tipo=null,$mes=null,$ano=null,$fk_idOrganismoAfiliado=null){

    	$sql = "SELECT count(idConvocacaoRitualisticaOGGMembro) as total FROM convocacaoRitualisticaOGG_membro "
                . " inner join convocacaoRitualisticaOGG on fk_idConvocacaoRitualisticaOGG = idConvocacaoRitualisticaOGG "
                . " where 1=1";

    	if($idConvocacaoRitualisticaOGG!=null) {
    		$sql .= " and fk_idConvocacaoRitualisticaOGG=:fk_idConvocacaoRitualisticaOGG";
    	}
        if($v!=null) {
    		$sql .= " and fk_seq_cadastMembro=:fk_seq_cadastMembro";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
        if($mes!=null) {
    		$sql .= " and MONTH(dataConvocacao)=:mes";
    	}
        if($ano!=null) {
    		$sql .= " and YEAR(dataConvocacao)=:ano";
    	}
        if($fk_idOrganismoAfiliado!=null) {
    		$sql .= " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idConvocacaoRitualisticaOGG!=null){
            $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':fk_seq_cadastMembro', $v, PDO::PARAM_INT);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        }
        if($mes!=null){
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null){
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function remove($idConvocacaoRitualisticaOGG=null,$id=null){

        $sql= "DELETE FROM convocacaoRitualisticaOGG_membro WHERE 1=1";

    	if($idConvocacaoRitualisticaOGG!=null) {
    		$sql .= " and fk_idConvocacaoRitualisticaOGG=:fk_idConvocacaoRitualisticaOGG";
    	}
    	if($id!=null) {
    		$sql .= " and idConvocacaoRitualisticaOGGMembro=:id";
    	}

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idConvocacaoRitualisticaOGG!=null){
            $sth->bindParam(':fk_idConvocacaoRitualisticaOGG', $idConvocacaoRitualisticaOGG, PDO::PARAM_INT);
        }
        if($id!=null){
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>
