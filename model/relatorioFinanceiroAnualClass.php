<?php
require_once ("conexaoClass.php");

class RelatorioFinanceiroAnual {

    public $idRelatorioFinanceiroAnual;
    public $fk_idOrganismoAfiliado;
    public $ano;
    public $caminhoRelatorioFinanceiroAnual;
    public $usuario;

    /* Funções GET e SET */

    function getIdRelatorioFinanceiroAnual() {
        return $this->idRelatorioFinanceiroAnual;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getAno() {
        return $this->ano;
    }
    
	function getCaminhoRelatorioFinanceiroAnual() {
        return $this->caminhoRelatorioFinanceiroAnual;
    }
        
	function getUsuario() {
        return $this->usuario;
    }

    function setIdRelatorioFinanceiroAnual($idRelatorioFinanceiroAnual) {
        $this->idRelatorioFinanceiroAnual = $idRelatorioFinanceiroAnual;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }
    
	function setAno($ano) {
        $this->ano = $ano;
    }

    function setCaminhoRelatorioFinanceiroAnual($caminhoRelatorioFinanceiroAnual) {
        $this->caminhoRelatorioFinanceiroAnual = $caminhoRelatorioFinanceiroAnual;
    }
   
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function cadastraRelatorioFinanceiroAnual() {
        
        $sql = "INSERT INTO relatorioFinanceiroAnual 
                                    (`fk_idOrganismoAfiliado`, 
                                    `ano`,
                                    `caminhoRelatorioFinanceiroAnual`,
                                    `usuario`,
                                    `dataCadastro`
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :ano,
                                            :caminho,
                                            :usuario,
                                            now()
                                            )";
        $id=1;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_STR);
        $sth->bindParam(':caminho', $this->caminhoRelatorioFinanceiroAnual, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $resultado = $this->selecionaUltimoIdInserido();
            if($resultado)
            {
                foreach ($resultado as $vetor)
                {
                    $id = $vetor['idRelatorioFinanceiroAnual'];
                }
            }
        }
        return $id;
    }

    public function alteraRelatorioFinanceiroAnual($id) {

        $sql = "UPDATE relatorioFinanceiroAnual SET 
                                                 `ano` = :ano,
                                                 `caminhoRelatorioFinanceiroAnual` = :caminho
                            WHERE `idRelatorioFinanceiroAnual` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_STR);
        $sth->bindParam(':caminho', $this->caminhoRelatorioFinanceiroAnual, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function atualizaCaminhoRelatorioFinanceiroAnualAssinado($id) {

        $sql = "UPDATE relatorioFinanceiroAnual SET 
                                                 `caminhoRelatorioFinanceiroAnual` = :caminho
                            WHERE `idRelatorioFinanceiroAnual` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':caminho', $this->caminhoRelatorioFinanceiroAnual, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioFinanceiroAnual'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM relatorioFinanceiroAnual ORDER BY idRelatorioFinanceiroAnual DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRelatorioFinanceiroAnual($ano=null,$fk_idOrganismoAfiliado=null) {
    	$sql = "SELECT * FROM relatorioFinanceiroAnual as rfm
    		inner join organismoAfiliado as oa on rfm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on rfm.usuario = u.seqCadast
    	where 1=1 ";
    	if($ano!=null)
    	{
            $sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($ano=null,$fk_idOrganismoAfiliado=null) {
    	$sql = "SELECT * FROM relatorioFinanceiroAnual where 1=1 ";
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function retornaIdRelatorioFinanceiroAnual() {
    	
    	$sql = "SELECT * FROM relatorioFinanceiroAnual 
                                 WHERE 
                                 `ano` = :ano
                                 and `fk_idOrganismoAfiliado` =  :idOrganismoAfiliado
                                 ";
    	//echo $sql;
   	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        
        $id=0;
        if ($resultado) {
        	foreach ($resultado as $vetor)
        	{
        		$id = $vetor['idRelatorioFinanceiroAnual'];
        	}
        }
        return $id;
    }

    public function buscarIdRelatorioFinanceiroAnual($idRelatorioFinanceiroAnual) {
        
        $sql = "SELECT * FROM relatorioFinanceiroAnual 
                                 WHERE `idRelatorioFinanceiroAnual` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioFinanceiroAnual, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioFinanceiroAnual($id){
        
        $sql="DELETE FROM relatorioFinanceiroAnual WHERE idRelatorioFinanceiroAnual = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}
?>