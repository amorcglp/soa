<?php

require_once ("conexaoClass.php");

class tipoMoeda {

    public $SeqMoeda;
    public $DesTipoMoeda;
    
    function getSeqMoeda() {
        return $this->SeqMoeda;
    }

    function getDesTipoMoeda() {
        return $this->DesTipoMoeda;
    }

    function setSeqMoeda($SeqMoeda) {
        $this->SeqMoeda = $SeqMoeda;
    }

    function setDesTipoMoeda($DesTipoMoeda) {
        $this->DesTipoMoeda = $DesTipoMoeda;
    }
           
    public function lista() {
        
        $sql = "SELECT * FROM tipo_moeda";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>