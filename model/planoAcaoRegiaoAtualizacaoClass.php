<?php

require_once ("conexaoClass.php");

class planoAcaoRegiaoAtualizacao{

    private $idPlanoAcaoRegiaoAtualizacao;
    private $fk_idPlanoAcaoRegiao;
    private $seqCadast;
    private $mensagem;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoAcaoRegiaoAtualizacao() {
        return $this->idPlanoAcaoRegiaoAtualizacao;
    }

    function getFk_idPlanoAcaoRegiao() {
        return $this->fk_idPlanoAcaoRegiao;
    }
    
	function getSeqCadast() {
        return $this->seqCadast;
    }
    
	function getMensagem() {
        return $this->mensagem;
    } 

    function setIdPlanoAcaoRegiaoAtualizacao($idPlanoAcaoRegiaoAtualizacao) {
        $this->idPlanoAcaoRegiaoAtualizacao = $idPlanoAcaoRegiaoAtualizacao;
    }
  
	function setFk_idPlanoAcaoRegiao($fk_idPlanoAcaoRegiao) {
        $this->fk_idPlanoAcaoRegiao = $fk_idPlanoAcaoRegiao;
    }
    
	function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }
    
	function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoAcaoRegiaoAtualizacao(){

        $sql="INSERT INTO planoAcaoRegiao_atualizacao  
                                 (
                                  fk_idPlanoAcaoRegiao,
                                  seqCadast,
                                  mensagem,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idPlanoAcaoRegiao,
                                    :seqCadast,
                                    :mensagem,
                                    now()
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoRegiao', $this->fk_idPlanoAcaoRegiao, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':mensagem', $this->mensagem, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function listaAtualizacao($idPlanoAcaoRegiao=null,$idPlanoAcaoRegiaoAtualizacao=null){

    	$sql = "SELECT * FROM planoAcaoRegiao_atualizacao as p
    		inner join usuario as u on p.seqCadast = u.seqCadast
    	where 1=1 
    	 ";   
    	if($idPlanoAcaoRegiao!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoRegiao=:idPlanoAcaoRegiao";
    	}
    	if($idPlanoAcaoRegiaoAtualizacao!=null)
    	{
    		$sql .= " and idPlanoAcaoRegiaoAtualizacao=:idPlanoAcaoRegiaoAtualizacao";
    	}	 
    	$sql .= " order by dataCadastro desc";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoRegiao!=null)
    	{
            $sth->bindParam(':idPlanoAcaoRegiao', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        }
        if($idPlanoAcaoRegiaoAtualizacao!=null)
    	{
            $sth->bindParam(':idPlanoAcaoRegiaoAtualizacao', $idPlanoAcaoRegiaoAtualizacao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaUltimaAtualizacao($idPlanoAcaoRegiao){

    	$sql = "SELECT * FROM planoAcaoRegiao_atualizacao
    	where 1=1 ";   
    	if($idPlanoAcaoRegiao!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoRegiao=:idPlanoAcaoRegiao";
    	}	 
    	$sql .= " order by dataCadastro desc limit 1";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoRegiao!=null)
    	{
            $sth->bindParam(':idPlanoAcaoRegiao', $idPlanoAcaoRegiao, PDO::PARAM_INT);        
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
        	foreach($resultado as $vetor)
        	{
        		$ultimaAtualizacao = $vetor['dataCadastro'];
        	}
            return $ultimaAtualizacao;
        }else{
            return "";
        }
    }

    public function removeAtualizacao(){
        
        $sql="DELETE FROM planoAcaoRegiao_atualizacao WHERE idPlanoAcaoRegiaoAtualizacao = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idPlanoAcaoRegiaoAtualizacao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function removeAtualizacoes($idPlanoAcaoRegiao){
        $sql="DELETE FROM planoAcaoRegiao_atualizacao WHERE fk_idPlanoAcaoRegiao = :idPlanoAcaoRegiao";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoRegiao', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
