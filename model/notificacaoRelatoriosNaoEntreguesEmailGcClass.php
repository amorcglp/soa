<?php

require_once ("conexaoClass.php");

class notificacaoRelatoriosNaoEntreguesEmailGc {

    public $idNotificacaoRelatoriosNaoEntreguesEmailGc;
    public $fk_idUsuario;
    public $regiao;
    public $nome;
    public $email;
    public $data;

    //Getter e Setter
    function getIdNotificacaoRelatoriosNaoEntreguesEmailGc() {
        return $this->idNotificacaoRelatoriosNaoEntreguesEmailGc;
    }

    function getFk_idUsuario() {
        return $this->fk_idUsuario;
    }

    function getRegiao() {
        return $this->regiao;
    }

    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getData() {
        return $this->data;
    }

    function setIdNotificacaoRelatoriosNaoEntreguesEmailGc($idNotificacaoRelatoriosNaoEntreguesEmailGc) {
        $this->idNotificacaoRelatoriosNaoEntreguesEmailGc = $idNotificacaoRelatoriosNaoEntreguesEmailGc;
    }

    function setFk_idUsuario($fk_idUsuario) {
        $this->fk_idUsuario = $fk_idUsuario;
    }

    function setRegiao($regiao) {
        $this->regiao = $regiao;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setData($data) {
        $this->data = $data;
    }

    
    public function cadastra() {
        
        $sql = "INSERT INTO notificacao_relatorios_nao_entregues_email_gc 
                    (idNotificacaoRelatoriosNaoEntreguesEmailGc, 
                    fk_idUsuario, 
                    regiao, 
                    nome, 
                    email, 
                    data)
                                    VALUES (:id,
                                            :idUsuario,
                                            :regiao,
                                            :nome,
                                            :email,
                                            now())";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idNotificacaoRelatoriosNaoEntreguesEmailGc, PDO::PARAM_INT);
        $sth->bindParam(':idUsuario', $this->fk_idUsuario, PDO::PARAM_INT);
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaExiste($fk_idUsuario=null,$regiao=null) {

        $sql = "SELECT * FROM notificacao_relatorios_nao_entregues_email_gc where 1=1 ";
        
        if($fk_idUsuario!=null)
        {
            $sql .= " and fk_idUsuario=:idUsuario";
        }
        if($regiao!=null)
        {
            $sql .= " and regiao=:regiao";
        }
        
        $sql .= " and MONTH(data)='".date('m')."' and YEAR(data)='".date('Y')."'";
        /*
        echo $sql;
        echo "/fk=>".$fk_idUsuario;
        echo "/regiao=>".$regiao;exit();
        */    
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($fk_idUsuario!=null)
        {
            $sth->bindParam(':idUsuario', $fk_idUsuario, PDO::PARAM_INT);
        }
        if($regiao!=null)
        {
            $sth->bindParam(':regiao', $regiao, PDO::PARAM_STR);
        }
        $resultado = $c->run($sth);
       //echo print_r($resultado);
        if (count($resultado)>0){
            //echo "true";
            return true;
        }else{
            //echo "false";
            return false;
        }
    }

    public function lista() {

        $sql = "SELECT * FROM notificacao_relatorios_nao_entregues_email_gc";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
       
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function remove($id) {
        
        $sql = "DELETE FROM notificacao_relatorios_nao_entregues_email_gc "
                . "WHERE idNotificacaoRelatoriosNaoEntreguesEmailGc = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
       
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>