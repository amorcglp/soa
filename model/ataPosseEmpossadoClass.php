<?php

require_once ("conexaoClass.php");

class ataPosseEmpossado{

    private $idAtaPosseEmpossado;
    private $fk_idAtaPosse;
    private $nome;
    private $codigoAfiliacao;
    private $endereco;
    private $cidade;
    private $cep;
    private $naturalidade;
    private $dataNascimento;
    private $nacionalidade;
    private $estadoCivil;
    private $profissao;
    private $rg;
    private $cpf;
    private $inicioMandato;
    private $fimMandato;
    private $indicado;
    private $funcao;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtaPosseEmpossado() {
        return $this->idAtaPosseEmpossado;
    }

    function getFk_idAtaPosse() {
        return $this->fk_idAtaPosse;
    }

    function getNome() {
        return $this->nome;
    }

    function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }

    function getEndereco() {
        return $this->endereco;
    }

    function getCidade() {
        return $this->cidade;
    }

    function getCep() {
        return $this->cep;
    }

    function getNaturalidade() {
        return $this->naturalidade;
    }

    function getDataNascimento() {
        return $this->dataNascimento;
    }

    function getNacionalidade() {
        return $this->nacionalidade;
    }

    function getEstadoCivil() {
        return $this->estadoCivil;
    }

    function getProfissao() {
        return $this->profissao;
    }

    function getRg() {
        return $this->rg;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getInicioMandato() {
        return $this->inicioMandato;
    }

    function getFimMandato() {
        return $this->fimMandato;
    }

    function getIndicado() {
        return $this->indicado;
    }

    function getFuncao() {
        return $this->funcao;
    }

    function setIdAtaPosseEmpossado($idAtaPosseEmpossado) {
        $this->idAtaPosseEmpossado = $idAtaPosseEmpossado;
    }

    function setFk_idAtaPosse($fk_idAtaPosse) {
        $this->fk_idAtaPosse = $fk_idAtaPosse;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }

    function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    function setNaturalidade($naturalidade) {
        $this->naturalidade = $naturalidade;
    }

    function setDataNascimento($dataNascimento) {
        $this->dataNascimento = $dataNascimento;
    }

    function setNacionalidade($nacionalidade) {
        $this->nacionalidade = $nacionalidade;
    }

    function setEstadoCivil($estadoCivil) {
        $this->estadoCivil = $estadoCivil;
    }

    function setProfissao($profissao) {
        $this->profissao = $profissao;
    }

    function setRg($rg) {
        $this->rg = $rg;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setInicioMandato($inicioMandato) {
        $this->inicioMandato = $inicioMandato;
    }

    function setFimMandato($fimMandato) {
        $this->fimMandato = $fimMandato;
    }

    function setIndicado($indicado) {
        $this->indicado = $indicado;
    }

    function setFuncao($funcao) {
        $this->funcao = $funcao;
    }

    //--------------------------------------------------------------------------

    public function cadastroAtaPosseEmpossado(){

        $sql = "INSERT INTO ataPosse_empossado
        (
        fk_idAtaPosse,
        nome,
        codigoAfiliacao,
        endereco,
        cidade,
        cep,
        naturalidade,
        dataNascimento,
        nacionalidade,
        estadoCivil,
        profissao,
        rg,
        cpf,
        inicioMandato,
        fimMandato,
        indicado,
        funcao
        )
        VALUES (:fk_idAtaPosse,
        :nome,
        :codigoAfiliacao,
        :endereco,
        :cidade,
        :cep,
        :naturalidade,
        :dataNascimento,
        :nacionalidade,
        :estadoCivil,
        :profissao,
        :rg,
        :cpf,
        :inicioMandato,
        :fimMandato,
        :indicado,
        :funcao
        )";


        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtaPosse', $this->fk_idAtaPosse, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_STR);
        $sth->bindParam(':endereco', $this->endereco, PDO::PARAM_STR);
        $sth->bindParam(':cidade', $this->cidade, PDO::PARAM_STR);
        $sth->bindParam(':cep', $this->cep, PDO::PARAM_STR);
        $sth->bindParam(':naturalidade', $this->naturalidade, PDO::PARAM_STR);
        $sth->bindParam(':dataNascimento', $this->dataNascimento, PDO::PARAM_STR);
        $sth->bindParam(':nacionalidade', $this->nacionalidade, PDO::PARAM_STR);
        $sth->bindParam(':estadoCivil', $this->estadoCivil, PDO::PARAM_INT);
        $sth->bindParam(':profissao', $this->profissao, PDO::PARAM_STR);
        $sth->bindParam(':rg', $this->rg, PDO::PARAM_STR);
        $sth->bindParam(':cpf', $this->cpf, PDO::PARAM_STR);
        $sth->bindParam(':inicioMandato', $this->inicioMandato, PDO::PARAM_STR);
        $sth->bindParam(':fimMandato', $this->fimMandato, PDO::PARAM_STR);
        $sth->bindParam(':indicado', $this->indicado, PDO::PARAM_INT);
        $sth->bindParam(':funcao', $this->funcao, PDO::PARAM_INT);
/*
        $data = array(
            'fk_idAtaPosse'=>$this->fk_idAtaPosse,
            'nome'=>$this->nome,
            'codigoAfiliacao'=>$this->codigoAfiliacao,
            'endereco'=>$this->endereco,
            'cidade'=>$this->cidade,
            'cep'=>$this->cep,
            'naturalidade'=>$this->naturalidade,
            'dataNascimento'=>$this->dataNascimento,
            'nacionalidade'=>$this->nacionalidade,
            'estadoCivil'=>$this->estadoCivil,
            'profissao'=>$this->profissao,
            'rg'=>$this->rg,
            'cpf'=>$this->cpf,
            'inicioMandato'=>$this->inicioMandato,
            'fimMandato'=>$this->fimMandato,
            'indicado'=>$this->indicado,
            'funcao'=>$this->funcao
        );

        echo parms("cadastroAtaPosseEmpossado", $sql, $data);
*/
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function listaEmpossados($idAtaPosse,$funcao){

    	$sql = "SELECT * FROM ataPosse_empossado where 1=1";

    	if($idAtaPosse!=null){
    		$sql .= " and fk_idAtaPosse=:fk_idAtaPosse";
    	}
    	if($funcao!=null){
    		$sql .= " and funcao=:funcao";
    	}
    	//echo $sql."<br>";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idAtaPosse!=null){
            $sth->bindParam(':fk_idAtaPosse', $idAtaPosse, PDO::PARAM_INT);
        }
        if($funcao!=null){
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeEmpossados($idAtaPosse){
        $sql = "DELETE FROM ataPosse_empossado WHERE fk_idAtaPosse = :idAtaPosse";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaPosse', $idAtaPosse, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}

?>
