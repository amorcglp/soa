<?php
require_once ("conexaoClass.php");

class Divida {

    public $idDivida;
    public $fk_idOrganismoAfiliado;
    public $inicioDivida;
    public $fimDivida;
    public $descricaoDivida;
    public $valorDivida;
    public $observacaoDivida;
    public $categoriaDivida;
    public $usuario;
    public $ultimoAtualizar;

    /* Funções GET e SET */

    function getIdDivida() {
        return $this->idDivida;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getInicioDivida() {
        return $this->inicioDivida;
    }

	function getFimDivida() {
        return $this->fimDivida;
    }
    
	function getDescricaoDivida() {
        return $this->descricaoDivida;
    }
        
	function getValorDivida() {
        return $this->valorDivida;
    }
    
	function getObservacaoDivida() {
        return $this->observacaoDivida;
    }
    
    function getCategoriaDivida() {
        return $this->categoriaDivida;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdDivida($idDivida) {
        $this->idDivida = $idDivida;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }

	function setInicioDivida($inicioDivida) {
        $this->inicioDivida = $inicioDivida;
    }

    function setFimDivida($fimDivida) {
        $this->fimDivida = $fimDivida;
    }
    
	function setDescricaoDivida($descricaoDivida) {
        $this->descricaoDivida = $descricaoDivida;
    }
    
	function setValorDivida($valorDivida) {
        $this->valorDivida = $valorDivida;
    }
    
	function setObservacaoDivida($observacaoDivida) {
        $this->observacaoDivida = $observacaoDivida;
    }
    
    function setCategoriaDivida($categoriaDivida) {
        $this->categoriaDivida = $categoriaDivida;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    public function cadastraDivida() {
        
    	$sql = "INSERT INTO divida 
                                (`fk_idOrganismoAfiliado`, 
                                `inicioDivida`, 
                                `fimDivida`,
                                `descricaoDivida`,
                                `valorDivida`,
                                `observacaoDivida`,
                                `categoriaDivida`,
                                `usuario`,
                                `dataCadastro`
                                )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :inicio,
                                            :fim,
                                            :descricao,
                                            :valor,
                                            :observacao,
                                            :categoria,
                                            :usuario,
                                            now()
                                            )";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':inicio', $this->inicioDivida, PDO::PARAM_STR);
        $sth->bindParam(':fim',  $this->fimDivida, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoDivida, PDO::PARAM_STR);
        $sth->bindParam(':valor', $this->valorDivida, PDO::PARAM_STR);
        $sth->bindParam(':observacao',  $this->observacaoDivida, PDO::PARAM_STR);
        $sth->bindParam(':categoria',  $this->categoriaDivida, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraDivida($id) {

        $sql = "UPDATE divida SET 
                        `inicioDivida` = :inicio,
                        `fimDivida` = :fim,
                        `descricaoDivida` = :descricao,
                        `valorDivida` = :valor,
                        `observacaoDivida` = :observacao,
                        `categoriaDivida` = :categoria,
                        `ultimoAtualizar` = :ultimoAtualizar    
                            WHERE `idDivida` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
       
        $sth->bindParam(':inicio', $this->inicioDivida, PDO::PARAM_STR);
        $sth->bindParam(':fim',  $this->fimDivida, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoDivida, PDO::PARAM_STR);
        $sth->bindParam(':valor', $this->valorDivida, PDO::PARAM_STR);
        $sth->bindParam(':observacao',  $this->observacaoDivida, PDO::PARAM_STR);
        $sth->bindParam(':categoria',  $this->categoriaDivida, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'divida'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM divida ORDER BY idDivida DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaDivida($fk_idOrganismoAfiliado=null, $mesAtual=null,$anoAtual=null,$categoria=null,$categoriasGLP=null,$categoriasOutros=null) {
        
    	$sql = "SELECT * FROM divida where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(inicioDivida)<=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(inicioDivida)<=:anoAtual";
    	}
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(fimDivida)>=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(fimDivida)>=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        if($categoria!=null)
    	{
    		$sql .= " and categoriaDivida=:categoria";
    	}
        if($categoriasGLP!=null)
    	{
    		$sql .= " and categoriaDivida in (15,18,19,20) ";
    	}
        if($categoriasOutros!=null)
    	{
    		$sql .= " and categoriaDivida in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,21,22,23) ";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual!=null) {
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null) {
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($categoria!=null)
    	{
            $sth->bindParam(':categoria', $categoria, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaDivida($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT SUM(REPLACE( REPLACE(valorDivida, '.' ,'' ), ',', '.' )) as total FROM divida where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(inicioDivida)<=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(inicioDivida)<=:anoAtual";
    	}
		if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(fimDivida)>=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(fimDivida)>=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        
        $total=0;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null) {
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null) {
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $vetor)
        	{
        		$total = $vetor['total'];
        	}
            return $total;
        } else {
            return false;
        }
    }

    public function removeDivida($idDivida) {
        
        $sql = "DELETE FROM divida WHERE `idDivida` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idDivida, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdDivida($idDivida) {
        
        $sql = "SELECT * FROM divida 
                                 WHERE `idDivida` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idDivida, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
}
?>