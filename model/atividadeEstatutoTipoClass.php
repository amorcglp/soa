<?php

require_once ("conexaoClass.php");

class AtividadeEstatutoTipo{

    private $idTipoAtividadeEstatuto;
    private $nomeTipoAtividadeEstatuto;
    private $descricaoTipoAtividadeEstatuto;
    private $classiOaTipoAtividadeEstatuto;
    private $qntTipoAtividadeEstatuto;
    private $statusTipoAtividadeEstatuto;
    private $portalTipoAtividadeEstatuto;
    private $estatutoTipoAtividadeEstatuto;
    private $dataCadastroTipoAtividadeEstatuto;
    private $dataAtualizadoTipoAtividadeEstatuto;
    private $fk_seqCadastAtualizadoPor;

    private $janeiroTipoAtividadeEstatuto;
    private $fevereiroTipoAtividadeEstatuto;
    private $marcoTipoAtividadeEstatuto;
    private $abrilTipoAtividadeEstatuto;
    private $maioTipoAtividadeEstatuto;
    private $junhoTipoAtividadeEstatuto;
    private $julhoTipoAtividadeEstatuto;
    private $agostoTipoAtividadeEstatuto;
    private $setembroTipoAtividadeEstatuto;
    private $outubroTipoAtividadeEstatuto;
    private $novembroTipoAtividadeEstatuto;
    private $dezembroTipoAtividadeEstatuto;


    //--- GETERS -----------------------------------------------------
    function getIdTipoAtividadeEstatuto() {
        return $this->idTipoAtividadeEstatuto;
    }
    public function getNomeTipoAtividadeEstatuto(){
        return $this->nomeTipoAtividadeEstatuto;
    }
    public function getDescricaoTipoAtividadeEstatuto(){
        return $this->descricaoTipoAtividadeEstatuto;
    }
    public function getClassiOaTipoAtividadeEstatuto(){
        return $this->classiOaTipoAtividadeEstatuto;
    }
    public function getQntTipoAtividadeEstatuto(){
        return $this->qntTipoAtividadeEstatuto;
    }
    public function getStatusTipoAtividadeEstatuto(){
        return $this->statusTipoAtividadeEstatuto;
    }
    public function getPortalTipoAtividadeEstatuto() {
        return $this->portalTipoAtividadeEstatuto;
    }
    public function getEstatutoTipoAtividadeEstatuto() {
        return $this->estatutoTipoAtividadeEstatuto;
    }
    public function getDataCadastroTipoAtividadeEstatuto(){
        return $this->dataCadastroTipoAtividadeEstatuto;
    }
    public function getDataAtualizadoTipoAtividadeEstatuto(){
        return $this->dataAtualizadoTipoAtividadeEstatuto;
    }
    public function getFkSeqCadastAtualizadoPor(){
        return $this->fk_seqCadastAtualizadoPor;
    }

    public function getJaneiroTipoAtividadeEstatuto() {
        return $this->janeiroTipoAtividadeEstatuto;
    }
    public function getFevereiroTipoAtividadeEstatuto() {
        return $this->fevereiroTipoAtividadeEstatuto;
    }
    public function getMarcoTipoAtividadeEstatuto() {
        return $this->marcoTipoAtividadeEstatuto;
    }
    public function getAbrilTipoAtividadeEstatuto() {
        return $this->abrilTipoAtividadeEstatuto;
    }
    public function getMaioTipoAtividadeEstatuto() {
        return $this->maioTipoAtividadeEstatuto;
    }
    public function getJunhoTipoAtividadeEstatuto() {
        return $this->junhoTipoAtividadeEstatuto;
    }
    public function getJulhoTipoAtividadeEstatuto() {
        return $this->julhoTipoAtividadeEstatuto;
    }
    public function getAgostoTipoAtividadeEstatuto() {
        return $this->agostoTipoAtividadeEstatuto;
    }
    public function getSetembroTipoAtividadeEstatuto() {
        return $this->setembroTipoAtividadeEstatuto;
    }
    public function getOutubroTipoAtividadeEstatuto() {
        return $this->outubroTipoAtividadeEstatuto;
    }
    public function getNovembroTipoAtividadeEstatuto() {
        return $this->novembroTipoAtividadeEstatuto;
    }
    public function getDezembroTipoAtividadeEstatuto() {
        return $this->dezembroTipoAtividadeEstatuto;
    }

    //--- SETERS -----------------------------------------------------
	function setIdTipoAtividadeEstatuto($idTipoAtividadeEstatuto) {
        $this->idTipoAtividadeEstatuto = $idTipoAtividadeEstatuto;
    }
    public function setNomeTipoAtividadeEstatuto($nomeTipoAtividadeEstatuto){
        $this->nomeTipoAtividadeEstatuto = $nomeTipoAtividadeEstatuto;
    }
    public function setDescricaoTipoAtividadeEstatuto($descricaoTipoAtividadeEstatuto){
        $this->descricaoTipoAtividadeEstatuto = $descricaoTipoAtividadeEstatuto;
    }
    public function setClassiOaTipoAtividadeEstatuto($classiOaTipoAtividadeEstatuto){
        $this->classiOaTipoAtividadeEstatuto = $classiOaTipoAtividadeEstatuto;
    }
    public function setQntTipoAtividadeEstatuto($qntTipoAtividadeEstatuto){
        $this->qntTipoAtividadeEstatuto = $qntTipoAtividadeEstatuto;
    }
    public function setStatusTipoAtividadeEstatuto($statusTipoAtividadeEstatuto){
        $this->statusTipoAtividadeEstatuto = $statusTipoAtividadeEstatuto;
    }
    public function setPortalTipoAtividadeEstatuto($portalTipoAtividadeEstatuto) {
        $this->portalTipoAtividadeEstatuto = $portalTipoAtividadeEstatuto;
    }
    public function setEstatutoTipoAtividadeEstatuto($estatutoTipoAtividadeEstatuto) {
        $this->estatutoTipoAtividadeEstatuto = $estatutoTipoAtividadeEstatuto;
    }
    public function setDataCadastroTipoAtividadeEstatuto($dataCadastroTipoAtividadeEstatuto){
        $this->dataCadastroTipoAtividadeEstatuto = $dataCadastroTipoAtividadeEstatuto;
    }
    public function setDataAtualizadoTipoAtividadeEstatuto($dataAtualizadoTipoAtividadeEstatuto){
        $this->dataAtualizadoTipoAtividadeEstatuto = $dataAtualizadoTipoAtividadeEstatuto;
    }
    public function setFkSeqCadastAtualizadoPor($fk_seqCadastAtualizadoPor){
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }

    public function setJaneiroTipoAtividadeEstatuto($janeiroTipoAtividadeEstatuto) {
        $this->janeiroTipoAtividadeEstatuto = $janeiroTipoAtividadeEstatuto;
    }
    public function setFevereiroTipoAtividadeEstatuto($fevereiroTipoAtividadeEstatuto) {
        $this->fevereiroTipoAtividadeEstatuto = $fevereiroTipoAtividadeEstatuto;
    }
    public function setMarcoTipoAtividadeEstatuto($marcoTipoAtividadeEstatuto) {
        $this->marcoTipoAtividadeEstatuto = $marcoTipoAtividadeEstatuto;
    }
    public function setAbrilTipoAtividadeEstatuto($abrilTipoAtividadeEstatuto) {
        $this->abrilTipoAtividadeEstatuto = $abrilTipoAtividadeEstatuto;
    }
    public function setMaioTipoAtividadeEstatuto($maioTipoAtividadeEstatuto) {
        $this->maioTipoAtividadeEstatuto = $maioTipoAtividadeEstatuto;
    }
    public function setJunhoTipoAtividadeEstatuto($junhoTipoAtividadeEstatuto) {
        $this->junhoTipoAtividadeEstatuto = $junhoTipoAtividadeEstatuto;
    }
    public function setJulhoTipoAtividadeEstatuto($julhoTipoAtividadeEstatuto) {
        $this->julhoTipoAtividadeEstatuto = $julhoTipoAtividadeEstatuto;
    }
    public function setAgostoTipoAtividadeEstatuto($agostoTipoAtividadeEstatuto) {
        $this->agostoTipoAtividadeEstatuto = $agostoTipoAtividadeEstatuto;
    }
    public function setSetembroTipoAtividadeEstatuto($setembroTipoAtividadeEstatuto) {
        $this->setembroTipoAtividadeEstatuto = $setembroTipoAtividadeEstatuto;
    }
    public function setOutubroTipoAtividadeEstatuto($outubroTipoAtividadeEstatuto) {
        $this->outubroTipoAtividadeEstatuto = $outubroTipoAtividadeEstatuto;
    }
    public function setNovembroTipoAtividadeEstatuto($novembroTipoAtividadeEstatuto) {
        $this->novembroTipoAtividadeEstatuto = $novembroTipoAtividadeEstatuto;
    }
    public function setDezembroTipoAtividadeEstatuto($dezembroTipoAtividadeEstatuto) {
        $this->dezembroTipoAtividadeEstatuto = $dezembroTipoAtividadeEstatuto;
    }


    //--------------------------------------------------------------------------

    public function cadastroTipoAtividadeEstatuto(){

        $sql = "INSERT INTO atividadeEstatuto_tipo
                                (nomeTipoAtividadeEstatuto,
                                descricaoTipoAtividadeEstatuto,
                                classiOaTipoAtividadeEstatuto,
                                qntTipoAtividadeEstatuto,
                                portalTipoAtividadeEstatuto,
                                estatutoTipoAtividadeEstatuto,
                                janeiroTipoAtividadeEstatuto,
                                fevereiroTipoAtividadeEstatuto,
                                marcoTipoAtividadeEstatuto,
                                abrilTipoAtividadeEstatuto,
                                maioTipoAtividadeEstatuto,
                                junhoTipoAtividadeEstatuto,
                                julhoTipoAtividadeEstatuto,
                                agostoTipoAtividadeEstatuto,
                                setembroTipoAtividadeEstatuto,
                                outubroTipoAtividadeEstatuto,
                                novembroTipoAtividadeEstatuto,
                                dezembroTipoAtividadeEstatuto,
                                dataCadastroTipoAtividadeEstatuto,
                                fk_seqCadastAtualizadoPor)
                            VALUES (:nomeTipoAtividadeEstatuto,
                                    :descricaoTipoAtividadeEstatuto,
                                    :classiOaTipoAtividadeEstatuto,
                                    :qntTipoAtividadeEstatuto,
                                    :portalTipoAtividadeEstatuto,
                                    :estatutoTipoAtividadeEstatuto,
                                    :janeiroTipoAtividadeEstatuto,
                                    :fevereiroTipoAtividadeEstatuto,
                                    :marcoTipoAtividadeEstatuto,
                                    :abrilTipoAtividadeEstatuto,
                                    :maioTipoAtividadeEstatuto,
                                    :junhoTipoAtividadeEstatuto,
                                    :julhoTipoAtividadeEstatuto,
                                    :agostoTipoAtividadeEstatuto,
                                    :setembroTipoAtividadeEstatuto,
                                    :outubroTipoAtividadeEstatuto,
                                    :novembroTipoAtividadeEstatuto,
                                    :dezembroTipoAtividadeEstatuto,
                                    NOW(),
                                    :fk_seqCadastAtualizadoPor)";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nomeTipoAtividadeEstatuto', $this->nomeTipoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':descricaoTipoAtividadeEstatuto', $this->descricaoTipoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':classiOaTipoAtividadeEstatuto', $this->classiOaTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':qntTipoAtividadeEstatuto', $this->qntTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':portalTipoAtividadeEstatuto', $this->portalTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':estatutoTipoAtividadeEstatuto', $this->estatutoTipoAtividadeEstatuto, PDO::PARAM_INT);

        $sth->bindParam(':janeiroTipoAtividadeEstatuto', $this->janeiroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':fevereiroTipoAtividadeEstatuto', $this->fevereiroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':marcoTipoAtividadeEstatuto', $this->marcoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':abrilTipoAtividadeEstatuto', $this->abrilTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':maioTipoAtividadeEstatuto', $this->maioTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':junhoTipoAtividadeEstatuto', $this->junhoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':julhoTipoAtividadeEstatuto', $this->julhoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':agostoTipoAtividadeEstatuto', $this->agostoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':setembroTipoAtividadeEstatuto', $this->setembroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':outubroTipoAtividadeEstatuto', $this->outubroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':novembroTipoAtividadeEstatuto', $this->novembroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':dezembroTipoAtividadeEstatuto', $this->dezembroTipoAtividadeEstatuto, PDO::PARAM_INT);

        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);

        /*
        $data = array(
            'nomeTipoAtividadeEstatuto'=>$this->nomeTipoAtividadeEstatuto,
            'descricaoTipoAtividadeEstatuto'=>$this->descricaoTipoAtividadeEstatuto,
            'classiOaTipoAtividadeEstatuto'=>$this->classiOaTipoAtividadeEstatuto,
            'qntTipoAtividadeEstatuto'=>$this->qntTipoAtividadeEstatuto,
            'portalTipoAtividadeEstatuto'=>$this->portalTipoAtividadeEstatuto,
            'estatutoTipoAtividadeEstatuto'=>$this->estatutoTipoAtividadeEstatuto,
            'fk_seqCadastAtualizadoPor'=>$this->fk_seqCadastAtualizadoPor,
            'janeiroTipoAtividadeEstatuto'=>$this->janeiroTipoAtividadeEstatuto,
            'fevereiroTipoAtividadeEstatuto'=>$this->fevereiroTipoAtividadeEstatuto,
            'marcoTipoAtividadeEstatuto'=>$this->marcoTipoAtividadeEstatuto,
            'abrilTipoAtividadeEstatuto'=>$this->abrilTipoAtividadeEstatuto,
            'maioTipoAtividadeEstatuto'=>$this->maioTipoAtividadeEstatuto,
            'junhoTipoAtividadeEstatuto'=>$this->junhoTipoAtividadeEstatuto,
            'julhoTipoAtividadeEstatuto'=>$this->julhoTipoAtividadeEstatuto,
            'agostoTipoAtividadeEstatuto'=>$this->agostoTipoAtividadeEstatuto,
            'setembroTipoAtividadeEstatuto'=>$this->setembroTipoAtividadeEstatuto,
            'outubroTipoAtividadeEstatuto'=>$this->outubroTipoAtividadeEstatuto,
            'novembroTipoAtividadeEstatuto'=>$this->novembroTipoAtividadeEstatuto,
            'dezembroTipoAtividadeEstatuto'=>$this->dezembroTipoAtividadeEstatuto
        );
        echo $this->parms("cadastroTipoAtividadeEstatuto",$sql,$data);
        */

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraTipoAtividadeEstatuto($idTipoAtividadeEstatuto){

        $sql = "UPDATE atividadeEstatuto_tipo SET
                                nomeTipoAtividadeEstatuto = :nomeTipoAtividadeEstatuto,
                                descricaoTipoAtividadeEstatuto =  :descricaoTipoAtividadeEstatuto,
                                classiOaTipoAtividadeEstatuto = :classiOaTipoAtividadeEstatuto,
                                qntTipoAtividadeEstatuto = :qntTipoAtividadeEstatuto,
                                portalTipoAtividadeEstatuto = :portalTipoAtividadeEstatuto,
                                estatutoTipoAtividadeEstatuto = :estatutoTipoAtividadeEstatuto,
                                janeiroTipoAtividadeEstatuto = :janeiroTipoAtividadeEstatuto,
                                fevereiroTipoAtividadeEstatuto = :fevereiroTipoAtividadeEstatuto,
                                marcoTipoAtividadeEstatuto = :marcoTipoAtividadeEstatuto,
                                abrilTipoAtividadeEstatuto = :abrilTipoAtividadeEstatuto,
                                maioTipoAtividadeEstatuto = :maioTipoAtividadeEstatuto,
                                junhoTipoAtividadeEstatuto = :junhoTipoAtividadeEstatuto,
                                julhoTipoAtividadeEstatuto = :julhoTipoAtividadeEstatuto,
                                agostoTipoAtividadeEstatuto = :agostoTipoAtividadeEstatuto,
                                setembroTipoAtividadeEstatuto = :setembroTipoAtividadeEstatuto,
                                outubroTipoAtividadeEstatuto = :outubroTipoAtividadeEstatuto,
                                novembroTipoAtividadeEstatuto = :novembroTipoAtividadeEstatuto,
                                dezembroTipoAtividadeEstatuto = :dezembroTipoAtividadeEstatuto,
                                dataAtualizadoTipoAtividadeEstatuto = NOW(),
                                fk_seqCadastAtualizadoPor = :fk_seqCadastAtualizadoPor
                                WHERE idTipoAtividadeEstatuto = :idTipoAtividadeEstatuto";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nomeTipoAtividadeEstatuto', $this->nomeTipoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':descricaoTipoAtividadeEstatuto', $this->descricaoTipoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':classiOaTipoAtividadeEstatuto', $this->classiOaTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':qntTipoAtividadeEstatuto', $this->qntTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':portalTipoAtividadeEstatuto', $this->portalTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':estatutoTipoAtividadeEstatuto', $this->estatutoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':idTipoAtividadeEstatuto', $idTipoAtividadeEstatuto, PDO::PARAM_INT);

        $sth->bindParam(':janeiroTipoAtividadeEstatuto', $this->janeiroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':fevereiroTipoAtividadeEstatuto', $this->fevereiroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':marcoTipoAtividadeEstatuto', $this->marcoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':abrilTipoAtividadeEstatuto', $this->abrilTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':maioTipoAtividadeEstatuto', $this->maioTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':junhoTipoAtividadeEstatuto', $this->junhoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':julhoTipoAtividadeEstatuto', $this->julhoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':agostoTipoAtividadeEstatuto', $this->agostoTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':setembroTipoAtividadeEstatuto', $this->setembroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':outubroTipoAtividadeEstatuto', $this->outubroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':novembroTipoAtividadeEstatuto', $this->novembroTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':dezembroTipoAtividadeEstatuto', $this->dezembroTipoAtividadeEstatuto, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeEstatutoTipo($classiOaTipoAtividadeEstatuto,$estatutoTipoAtividadeEstatuto,$statusTipoAtividadeEstatuto){

        $sql = "SELECT * FROM atividadeEstatuto_tipo WHERE 1=1";

        $sql .= $classiOaTipoAtividadeEstatuto!=null ? " and classiOaTipoAtividadeEstatuto=:classiOaTipoAtividadeEstatuto" : "";
        $sql .= $estatutoTipoAtividadeEstatuto!=null ? " and estatutoTipoAtividadeEstatuto=:estatutoTipoAtividadeEstatuto" : "";
        $sql .= $statusTipoAtividadeEstatuto!=null ? " and statusTipoAtividadeEstatuto=0" : "";

        $sql .= " ORDER BY ordemTipoAtividadeEstatuto ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($classiOaTipoAtividadeEstatuto!=null){
          $sth->bindParam(':classiOaTipoAtividadeEstatuto', $classiOaTipoAtividadeEstatuto, PDO::PARAM_INT);
        }
        if($estatutoTipoAtividadeEstatuto!=null){
          $sth->bindParam(':estatutoTipoAtividadeEstatuto', $estatutoTipoAtividadeEstatuto, PDO::PARAM_INT);
        }

        $data = array(
            'classiOaTipoAtividadeEstatuto'=>$classiOaTipoAtividadeEstatuto,
            'estatutoTipoAtividadeEstatuto'=>$estatutoTipoAtividadeEstatuto
        );
        //echo "QUERY: " . $this->parms("listaAtividadeEstatutoTipo",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacaoOa=null,$portal=null,$estatuto=null,$status=null,$classificacoesOa=null){

        $sql = "SELECT * FROM atividadeEstatuto_tipo where 1=1 ";

        $sql .= ($classificacaoOa != null) ? " and classiOaTipoAtividadeEstatuto=:classiOaTipoAtividadeEstatuto" : "";
        $sql .= ($portal != null) ? " and portalTipoAtividadeEstatuto=1" : "";
        $sql .= ($estatuto != null) ? " and estatutoTipoAtividadeEstatuto=1" : "";
        $sql .= ($status != null) ? " and statusTipoAtividadeEstatuto=0" : "";

        if($classificacoesOa!=null)
    	{
            $ids = explode(",", $classificacoesOa);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and classiOaTipoAtividadeEstatuto IN (".$inQuery.")";
    	}

        $sql .= " ORDER BY nomeTipoAtividadeEstatuto ASC";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($classificacaoOa!=null)
        {
            $sth->bindParam(':classiOaTipoAtividadeEstatuto', $classificacaoOa, PDO::PARAM_INT);
        }

        if($classificacoesOa!=null)
        {
            foreach ($ids as $u => $id)
            {
                 $sth->bindValue(":".$u, $id);
            }
        }

        $data = array(
            'classiOaTipoAtividadeEstatuto'=>$classificacaoOa,
        );
        //echo "QUERY: " . $this->parms("listaAtividadeEstatutoTipoPelaClassificacaoOa",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdTipoAtividadeEstatuto($idTipoAtividadeEstatuto){

        $sql = "SELECT * FROM  atividadeEstatuto_tipo WHERE   idTipoAtividadeEstatuto=:idTipoAtividadeEstatuto";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idTipoAtividadeEstatuto', $idTipoAtividadeEstatuto, PDO::PARAM_INT);

        $data = array(
            'idTipoAtividadeEstatuto'=>$idTipoAtividadeEstatuto,
        );
        //echo "QUERY: " . $this->parms("buscaIdTipoAtividadeEstatuto",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function alteraStatusTipoAtividadeEstatuto() {

        $sql = "UPDATE atividadeEstatuto_tipo SET statusTipoAtividadeEstatuto = :statusTipoAtividadeEstatuto "
                . "WHERE idTipoAtividadeEstatuto = :idTipoAtividadeEstatuto";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusTipoAtividadeEstatuto', $this->statusTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':idTipoAtividadeEstatuto', $this->idTipoAtividadeEstatuto, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusTipoAtividadeEstatuto();
            return $arr;
        } else {
            return false;
        }
    }

    public function alteraHierarquiaTipo($idAtividade, $ordem) {

        $sql = "UPDATE atividadeEstatuto_tipo SET ordemTipoAtividadeEstatuto = :ordemTipoAtividadeEstatuto "
                . "WHERE idTipoAtividadeEstatuto = :idTipoAtividadeEstatuto";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':ordemTipoAtividadeEstatuto', $ordem, PDO::PARAM_INT);
        $sth->bindParam(':idTipoAtividadeEstatuto', $idAtividade, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }

    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}

?>
