<?php

require_once ("conexaoClass.php");

class ataReuniaoMensal{

    private $idAtaReuniaoMensal;
    private $dataAtaReuniaoMensal;
    private $horaInicialAtaReuniaoMensal;
    private $horaFinalAtaReuniaoMensal;
    private $mesCompetencia;
    private $anoCompetencia;
    private $numeroMembrosAtaReuniaoMensal;
    private $fk_idOrganismoAfiliado;
    private $presidenteAtaReuniaoMensal;
    private $nomePresidenteAtaReuniaoMensal;
    private $monitorRegionalPresente;
    private $companheiro;
    private $fk_seq_cadast;
    private $ultimoAtualizar;
    private $caminhoAtaReuniaoMensalAssinada;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtaReuniaoMensal() {
        return $this->idAtaReuniaoMensal;
    }

    function getDataAtaReuniaoMensal() {
        return $this->dataAtaReuniaoMensal;
    }

    function getHoraInicialAtaReuniaoMensal() {
        return $this->horaInicialAtaReuniaoMensal;
    }

    function getHoraFinalAtaReuniaoMensal() {
        return $this->horaFinalAtaReuniaoMensal;
    }

	function getMesCompetencia() {
        return $this->mesCompetencia;
    }

	function getAnoCompetencia() {
        return $this->anoCompetencia;
    }

	function getNumeroMembrosAtaReuniaoMensal() {
        return $this->numeroMembrosAtaReuniaoMensal;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

	function getPresidenteAtaReuniaoMensal() {
        return $this->presidenteAtaReuniaoMensal;
    }

	function getNomePresidenteAtaReuniaoMensal() {
        return $this->nomePresidenteAtaReuniaoMensal;
    }
    
    function getMonitorRegionalPresente() {
        return $this->monitorRegionalPresente;
    }

	function getCompanheiro() {
        return $this->companheiro;
    }

	function getFk_seqCadast() {
        return $this->fk_seq_cadast;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

	function getCaminhoAtaReuniaoMensalAssinada() {
        return $this->caminhoAtaReuniaoMensalAssinada;
    }

    function setIdAtaReuniaoMensal($idAtaReuniaoMensal) {
        $this->idAtaReuniaoMensal = $idAtaReuniaoMensal;
    }

    function setDataAtaReuniaoMensal($dataAtaReuniaoMensal) {
        $this->dataAtaReuniaoMensal = $dataAtaReuniaoMensal;
    }

    function setHoraInicialAtaReuniaoMensal($horaInicialAtaReuniaoMensal) {
        $this->horaInicialAtaReuniaoMensal = $horaInicialAtaReuniaoMensal;
    }

    function setHoraFinalAtaReuniaoMensal($horaFinalAtaReuniaoMensal) {
        $this->horaFinalAtaReuniaoMensal = $horaFinalAtaReuniaoMensal;
    }

	function setMesCompetencia($mesCompetencia) {
        $this->mesCompetencia = $mesCompetencia;
    }

	function setAnoCompetencia($anoCompetencia) {
        $this->anoCompetencia = $anoCompetencia;
    }

	function setNumeroMembrosAtaReuniaoMensal($numeroMembrosAtaReuniaoMensal) {
        $this->numeroMembrosAtaReuniaoMensal = $numeroMembrosAtaReuniaoMensal;
    }

	function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

	function setPresidenteAtaReuniaoMensal($presidenteAtaReuniaoMensal) {
        $this->presidenteAtaReuniaoMensal = $presidenteAtaReuniaoMensal;
    }

    function setNomePresidenteAtaReuniaoMensal($nomePresidenteAtaReuniaoMensal) {
        $this->nomePresidenteAtaReuniaoMensal = $nomePresidenteAtaReuniaoMensal;
    }
    
    function setMonitorRegionalPresente($monitorRegionalPresente) {
        $this->monitorRegionalPresente = $monitorRegionalPresente;
    }

    function setCompanheiro($companheiro) {
        $this->companheiro = $companheiro;
    }

	function setFk_seqCadast($fk_seqCadast) {
        $this->fk_seq_cadast = $fk_seqCadast;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

	function setCaminhoAtaReuniaoMensalAssinada($caminhoAtaReuniaoMensalAssinada) {
        $this->caminhoAtaReuniaoMensalAssinada = $caminhoAtaReuniaoMensalAssinada;
    }

    //--------------------------------------------------------------------------

    public function cadastroAtaReuniaoMensal(){

        $sql = "INSERT INTO ataReuniaoMensal
                                 (dataAtaReuniaoMensal,
                                  fk_idOrganismoAfiliado,
                                  presidenteAtaReuniaoMensal,
                                  nomePresidenteAtaReuniaoMensal,
                                  companheiro,
                                  horaInicialAtaReuniaoMensal,
                                  horaFinalAtaReuniaoMensal,
                                  mesCompetencia,
                                  anoCompetencia,
                                  numeroMembrosAtaReuniaoMensal,
                                  monitorRegionalPresente,
                                  fk_seq_cadast,
                                  data
                                  )
                            VALUES (:dataAtaReuniaoMensal,
                                    :fk_idOrganismoAfiliado,
                                    :presidenteAtaReuniaoMensal,
                                    :nomePresidenteAtaReuniaoMensal,
                                    :companheiro,
                                    :horaInicialAtaReuniaoMensal,
                                    :horaFinalAtaReuniaoMensal,
                                    :mesCompetencia,
                                    :anoCompetencia,
                                    :numeroMembrosAtaReuniaoMensal,
                                    :monitorRegionalPresente,
                                    :fk_seq_cadast,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':dataAtaReuniaoMensal', $this->dataAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':presidenteAtaReuniaoMensal', $this->presidenteAtaReuniaoMensal, PDO::PARAM_INT);
        $sth->bindParam(':nomePresidenteAtaReuniaoMensal', $this->nomePresidenteAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':companheiro', $this->companheiro, PDO::PARAM_INT);
        $sth->bindParam(':horaInicialAtaReuniaoMensal', $this->horaInicialAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':horaFinalAtaReuniaoMensal', $this->horaFinalAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':mesCompetencia', $this->mesCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':anoCompetencia', $this->anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':numeroMembrosAtaReuniaoMensal', $this->numeroMembrosAtaReuniaoMensal, PDO::PARAM_INT);
        $sth->bindParam(':monitorRegionalPresente', $this->monitorRegionalPresente, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadast', $this->fk_seq_cadast, PDO::PARAM_INT);

        if ($c->run($sth,true)){
        	$ultimo_id = $this->selecionaUltimoIdCadastrado();
        	return $ultimo_id;
        }else{
            return false;
        }
    }

	public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'ataReuniaoMensal'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoIdCadastrado(){

        $sql= "SELECT `idAtaReuniaoMensal` as id FROM `ataReuniaoMensal` ORDER BY idAtaReuniaoMensal DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['id'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function alteraAtaReuniaoMensal($idAtaReuniaoMensal){

        $sql = "UPDATE ataReuniaoMensal SET
                                dataAtaReuniaoMensal = :dataAtaReuniaoMensal,
                                presidenteAtaReuniaoMensal = :presidenteAtaReuniaoMensal,
                                nomePresidenteAtaReuniaoMensal = :nomePresidenteAtaReuniaoMensal,
                                companheiro = :companheiro,
                                horaInicialAtaReuniaoMensal = :horaInicialAtaReuniaoMensal,
                                horaFinalAtaReuniaoMensal = :horaFinalAtaReuniaoMensal,
                                mesCompetencia = :mesCompetencia,
                                anoCompetencia = :anoCompetencia,
                                numeroMembrosAtaReuniaoMensal = :numeroMembrosAtaReuniaoMensal,
                                monitorRegionalPresente = :monitorRegionalPresente,
                                ultimo_atualizar = :ultimoAtualizar
                            WHERE idAtaReuniaoMensal = :idAtaReuniaoMensal";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        /*
        echo "<br>===============================";
        echo "<br>=>".$this->dataAtaReuniaoMensal;
        echo "<br>=>".$this->presidenteAtaReuniaoMensal;
        echo "<br>=>".$this->nomePresidenteAtaReuniaoMensal;
        echo "<br>=>".$this->companheiro;
        echo "<br>=>".$this->horaInicialAtaReuniaoMensal;
        echo "<br>=>".$this->horaFinalAtaReuniaoMensal;
        echo "<br>=>".$this->mesCompetencia;
        echo "<br>=>".$this->anoCompetencia;
        echo "<br>=>".$this->numeroMembrosAtaReuniaoMensal;
        echo "<br>=>".$this->ultimoAtualizar;
        echo "<br>=>".$this->idAtaReuniaoMensal;
        echo "<br>===============================";
        */
        $sth->bindParam(':dataAtaReuniaoMensal', $this->dataAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':presidenteAtaReuniaoMensal', $this->presidenteAtaReuniaoMensal, PDO::PARAM_INT);
        $sth->bindParam(':nomePresidenteAtaReuniaoMensal', $this->nomePresidenteAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':companheiro', $this->companheiro, PDO::PARAM_INT);
        $sth->bindParam(':horaInicialAtaReuniaoMensal', $this->horaInicialAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':horaFinalAtaReuniaoMensal', $this->horaFinalAtaReuniaoMensal, PDO::PARAM_STR);
        $sth->bindParam(':mesCompetencia', $this->mesCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':anoCompetencia', $this->anoCompetencia, PDO::PARAM_INT);
        $sth->bindParam(':numeroMembrosAtaReuniaoMensal', $this->numeroMembrosAtaReuniaoMensal, PDO::PARAM_INT);
        $sth->bindParam(':monitorRegionalPresente', $this->monitorRegionalPresente, PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':idAtaReuniaoMensal', $idAtaReuniaoMensal, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function atualizaCaminhoAtaReuniaoMensalAssinada($idAtaReuniaoMensal)
    {
    	$sql = "UPDATE ataReuniaoMensal SET
        						  caminhoAtaReuniaoMensalAssinada =  :caminhoAtaReuniaoMensalAssinada
                                  WHERE idAtaReuniaoMensal = :idAtaReuniaoMensal";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':caminhoAtaReuniaoMensalAssinada', $this->caminhoAtaReuniaoMensalAssinada, PDO::PARAM_STR);
        $sth->bindParam(':idAtaReuniaoMensal', $idAtaReuniaoMensal, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function atualizaNumeroAssinatura($numeroAssinatura)
    {
        $sql = "UPDATE ataReuniaoMensal SET
        						  numeroAssinatura =  :numeroAssinatura
                                  WHERE idAtaReuniaoMensal = :idAtaReuniaoMensal";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':numeroAssinatura', $numeroAssinatura, PDO::PARAM_STR);
        $sth->bindParam(':idAtaReuniaoMensal', $this->idAtaReuniaoMensal, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    /*
    public function alteraStatusAtaReuniaoMensal() {

        $sql = "UPDATE ataReuniaoMensal SET `statusAtaReuniaoMensal` = :status"
               . "                WHERE `idAtaReuniaoMensal` = :id";

       $c = new conexaoSOA();
       $con = $c->openSOA();
       $sth = $con->prepare($sql);

       $sth->bindParam(':statusAtaReuniaoMensal', $this->getStatusAtaReuniaoMensal(), PDO::PARAM_INT);
       $sth->bindParam(':idAtaReuniaoMensal', $this->idAtaReuniaoMensal , PDO::PARAM_INT);

       if ($c->run($sth,true)){
           $arr = array();
           $arr['status'] = $this->getStatusAtaReuniaoMensal();
           return $arr;
       } else {
           return false;
       }
    }
    */

    public function listaAtaReuniaoMensal($mesCompetencia=null,$anoCompetencia=null,$idOrganismoAfiliado=null,$entregueCompletamente=false){

    	$sql = "SELECT * FROM ataReuniaoMensal
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
        where 1=1 ";
    	//echo $sql;
        if($mesCompetencia!=null)
        {
            $sql .= " and mesCompetencia=:mesCompetencia";
        }
        if($anoCompetencia!=null)
        {
            $sql .= " and anoCompetencia=:anoCompetencia";
        }
        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }
        if($entregueCompletamente)
        {
            $sql .= " and entregueCompletamente=1";
        }

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesCompetencia!=null)
        {
            $sth->bindParam(':mesCompetencia', $mesCompetencia , PDO::PARAM_INT);
        }
        if($anoCompetencia!=null)
        {
            $sth->bindParam(':anoCompetencia', $anoCompetencia , PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado , PDO::PARAM_INT);
        }

        $data = array(
            'mesCompetencia'=>$mesCompetencia,
            'anoCompetencia'=>$anoCompetencia,
            'idOrganismoAfiliado'=>$idOrganismoAfiliado,
        );
        //echo "QUERY: " . $this->parms("listaAtaReuniaoMensal",$sql,$data);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

	public function verificaSeJaExiste(){

    	$sql = "SELECT * FROM ataReuniaoMensal where 1=1
    			and dataAtaReuniaoMensal = :dataAtaReuniaoMensal
                and fk_idOrganismoAfiliado =  :fk_idOrganismoAfiliado
                and horaInicialAtaReuniaoMensal = :horaInicialAtaReuniaoMensal
				and horaFinalAtaReuniaoMensal = :horaFinalAtaReuniaoMensal";
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':dataAtaReuniaoMensal', $this->dataAtaReuniaoMensal , PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado , PDO::PARAM_INT);
        $sth->bindParam(':horaInicialAtaReuniaoMensal', $this->horaInicialAtaReuniaoMensal , PDO::PARAM_STR);
        $sth->bindParam(':horaFinalAtaReuniaoMensal', $this->horaFinalAtaReuniaoMensal , PDO::PARAM_STR);

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaNomeAtaReuniaoMensal(){
    	$sql = "SELECT * FROM ataReuniaoMensal
                                    WHERE  nomeAtaReuniaoMensal LIKE :nomeAtaReuniaoMensal
                                    ORDER BY nomeAtaReuniaoMensal ASC";
    	//echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $nomeAtaReuniao = '%'.str_replace("  "," ",trim($this->getNomeAtaReuniaoMensal())).'%';
        $sth->bindParam(':nomeAtaReuniaoMensal', $nomeAtaReuniao , PDO::PARAM_STR);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscarIdAtaReuniaoMensal($idAtaReuniaoMensal){
    	$sql = "SELECT *,u.nomeUsuario as quemEntregou FROM  ataReuniaoMensal as a
				inner join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
				left join usuario u on a.quemEntregou = u.seqCadast
                WHERE   idAtaReuniaoMensal = :idAtaReuniaoMensal
                group by idAtaReuniaoMensal";
    	//echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaReuniaoMensal', $idAtaReuniaoMensal , PDO::PARAM_INT);

        $resultado = $c->run($sth);

        //echo "<pre>";print_r($resultado);

        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeAtaReuniaoMensal($idAtaReuniaoMensal){
        $sql = "DELETE FROM ataReuniaoMensal WHERE idAtaReuniaoMensal = :idAtaReuniaoMensal";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaReuniaoMensal', $idAtaReuniaoMensal, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinatura($codigo){

        $sql = "SELECT * FROM  ataReuniaoMensal as a
                WHERE   numeroAssinatura = :codigo";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinaturaIndividual($codigo,$codigoIndividual){

        $sql = "SELECT * FROM  ataReuniaoMensal as a
                inner join ataReuniaoMensal_assinatura_eletronica on fk_idAtaReuniaoMensal = idAtaReuniaoMensal
                WHERE   numeroAssinatura = :codigo and codigoAssinaturaIndividual = :codigoIndividual";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);
        $sth->bindParam(':codigoIndividual', $codigoIndividual , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function entregar($id,$quemEntregou)
    {
        $sql = "UPDATE ataReuniaoMensal SET
        						  entregue =  1, dataEntrega=now(), quemEntregou = :quemEntregou
                                  WHERE idAtaReuniaoMensal = :idAtaReuniaoMensal";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaReuniaoMensal', $id, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregou', $quemEntregou, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscarAtaReuniaoMensalQuePrecisaAssinatura($idOrganismoAfiliado)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  ataReuniaoMensal as a
                WHERE   entregue = 1 and fk_idOrganismoAfiliado = :idOrganismoAfiliado";

        $sql .= " order by anoCompetencia,mesCompetencia";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function assinaturaEletronicaAtaReuniaoMensal($seqCadast,$ip,$idOrganismoAfiliado,$funcao,$idAta,$codigoAssinaturaIndividual){

        //echo "idAta=>".$idAta;
        $sql = "INSERT INTO ataReuniaoMensal_assinatura_eletronica
                                 (fk_idAtaReuniaoMensal,
                                  codigoAssinaturaIndividual,
                                  seqCadast,
                                  fk_idFuncao,
                                  ip,
                                  fk_idOrganismoAfiliado,
                                  data
                                  )
                            VALUES (:fk_idAtaReuniaoMensal,
                                    :codigoAssinaturaIndividual,
                                    :seqCadast,
                                    :fk_idFuncao,
                                    :ip,
                                    :fk_idOrganismoAfiliado,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':fk_idFuncao', $funcao, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtaReuniaoMensal', $idAta, PDO::PARAM_INT);
        $sth->bindParam(':codigoAssinaturaIndividual', $codigoAssinaturaIndividual, PDO::PARAM_INT);


        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarAssinaturasEmAtaReuniaoMensal($seqCadast=null,$idOrganismoAfiliado=null,$codigoIndividual=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT *,a.data as dataEntrega FROM  ataReuniaoMensal_assinatura_eletronica as a
                left join ataReuniaoMensal as at on at.idAtaReuniaoMensal = a.fk_idAtaReuniaoMensal
                left join usuario as u on u.seqCadast = a.seqCadast
                left join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE  1=1 ";

        if($seqCadast!=null)
        {
            $sql .= " and a.seqCadast = :seqCadast";
        }

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and a.fk_idOrganismoAfiliado = :idOrganismoAfiliado group by idAtaReuniaoMensalAssinaturaEletronica";
        }

        if($codigoIndividual!=null)
        {
            $sql .= " and a.codigoAssinaturaIndividual = :codigoIndividual";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null) {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if($codigoIndividual!=null)
        {
            $sth->bindParam(':codigoIndividual', $codigoIndividual, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);

        if($codigoIndividual!=null)
        {
            return $resultado;
        }else{
            if($idOrganismoAfiliado==null) {

                if ($resultado) {
                    $arr = array();
                    foreach ($resultado as $v) {
                        $arr[] = $v['fk_idAtaReuniaoMensal'];
                    }
                    return $arr;
                } else {
                    return false;
                }
            }else{
                return $resultado;
            }
        }

    }

    public function buscarAssinaturasEmAtaReuniaoMensalPorDocumento($idAta,$idFuncao=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  ataReuniaoMensal_assinatura_eletronica as a
                left join usuario as u on u.seqCadast = a.seqCadast
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE   fk_idAtaReuniaoMensal = :idAta";
        if($idFuncao!=null)
        {
            $sql .= " and fk_idFuncao = :idFuncao";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAta', $idAta, PDO::PARAM_INT);

        if($idFuncao!=null)
        {
            $sth->bindParam(':idFuncao', $idFuncao, PDO::PARAM_INT);
        }

        if($idFuncao==null)
        {
            //echo "aqui Fio2!";
            $resultado = $c->run($sth);
            if ($resultado){
                return $resultado;
            } else {
                return false;
            }
        }else{
            //echo "aqui Fio1!";
            $resultado = $c->run($sth,null,null,true);
            if ($resultado){

                return true;
            } else {
                return false;
            }
        }

    }

    public function entregarCompletamente($id,$quemEntregouCompletamente)
    {
        $sql = "UPDATE ataReuniaoMensal SET
        						  entregueCompletamente =  1,
        						  quemEntregouCompletamente = :quemEntregouCompletamente,
        						  dataEntregouCompletamente = now()
                                  WHERE idAtaReuniaoMensal = :idAtaReuniaoMensal";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaReuniaoMensal', $id, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregouCompletamente', $quemEntregouCompletamente, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}

?>
