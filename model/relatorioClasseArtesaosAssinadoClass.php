<?php

require_once ("conexaoClass.php");

class relatorioClasseArtesaosAssinado{

    private $idRelatorioClasseArtesaosAssinado;
    private $fk_idRelatorioClasseArtesaos;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdRelatorioClasseArtesaosAssinado() {
        return $this->idRelatorioClasseArtesaosAssinado;
    }

    function getFk_idRelatorioClasseArtesaos() {
        return $this->fk_idRelatorioClasseArtesaos;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdRelatorioClasseArtesaosAssinado($idRelatorioClasseArtesaosAssinado) {
        $this->idRelatorioClasseArtesaosAssinado = $idRelatorioClasseArtesaosAssinado;
    }

    function setFk_idRelatorioClasseArtesaos($fk_idRelatorioClasseArtesaos) {
        $this->fk_idRelatorioClasseArtesaos = $fk_idRelatorioClasseArtesaos;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    
    //--------------------------------------------------------------------------

    public function cadastroRelatorioClasseArtesaosAssinado(){

        $sql="INSERT INTO relatorioClasseArtesaos_assinado  
                                 (
                                  fk_idRelatorioClasseArtesaos,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:id,
                                    :caminho,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idRelatorioClasseArtesaos, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioClasseArtesaos_assinado'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                    $proximo_id = $vetor['Auto_increment']; 
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaRelatorioClasseArtesaosAssinado($id=null,$idOrganismoAfiliado=null,$mes=null,$ano=null){

    	$sql = "SELECT * FROM relatorioClasseArtesaos_assinado
    	inner join relatorioClasseArtesaos on fk_idRelatorioClasseArtesaos = idRelatorioClasseArtesaos
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idRelatorioClasseArtesaos = :id";
    	}
        if($mes!=null)
    	{
    		$sql .= " and mesCompetencia = :mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and anoCompetencia = :ano";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
        
    	//echo "<br><br>".$sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_STR);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioClasseArtesaosAssinado($idRelatorioClasseArtesaosAssinado){
        
        $sql= "DELETE FROM relatorioClasseArtesaos_assinado WHERE idRelatorioClasseArtesaosAssinado = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioClasseArtesaosAssinado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
