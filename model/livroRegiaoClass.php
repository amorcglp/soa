<?php

require_once ("conexaoClass.php");

class livroRegiao{

    private $idLivroRegiao;
    private $regiao;
    private $titulo;
    private $assunto;
    private $descricao;
    private $palavraChave;    
    private $usuario;
    private $usuarioAlteracao;

    //METODO SET/GET -----------------------------------------------------------

    function getIdLivroRegiao() {
        return $this->idLivroRegiao;
    }
    
 	function getRegiao() {
        return $this->regiao;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getAssunto() {
        return $this->assunto;
    }

    function getDescricao() {
        return $this->descricao;
    }
    
    function getPalavraChave() {
        return $this->palavraChave;
    }

    function getUsuario() {
        return $this->usuario;
    }
    
    function getUsuarioAlteracao() {
        return $this->usuarioAlteracao;
    }
    
    function setIdLivroRegiao($idLivroRegiao) {
        $this->idLivroRegiao = $idLivroRegiao;
    }
    
	function setRegiao($regiao) {
        $this->regiao = $regiao;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }
    
	function setPalavraChave($palavraChave) {
        $this->palavraChave = $palavraChave;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
	function setUsuarioAlteracao($usuarioAlteracao) {
        $this->usuarioAlteracao = $usuarioAlteracao;
    }
    
    //--------------------------------------------------------------------------

    public function cadastroLivroRegiao(){

        $sql="INSERT INTO livroRegiao  
                                 (regiao,
                                  titulo,
                                  assunto,
                                  descricao,
                                  palavraChave,
                                  usuario,
                                  usuarioAlteracao,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                                    :regiao,
                                    :titulo,
                                    :assunto,
                                    :descricao,
                                    :palavraChave,
                                    :usuario,
                                    '0',
                                    now())";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_INT);
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':assunto', $this->assunto, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        $sth->bindParam(':usuario',  $this->usuario, PDO::PARAM_INT);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idLivroRegiao) as lastid FROM livroRegiao";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	

    public function alteraLivroRegiao($idLivroRegiao){
    	
        $sql="UPDATE livroRegiao SET  
                            titulo = :titulo,
                            assunto = :assunto,
                            descricao = :descricao,
                            palavraChave = :palavraChave,
                            usuarioAlteracao = :usuarioAlteracao
                     WHERE idLivroRegiao = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':assunto', $this->assunto, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricao, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        $sth->bindParam(':usuarioAlteracao',  $this->usuarioAlteracao, PDO::PARAM_INT);
        $sth->bindParam(':id', $idLivroRegiao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function listaLivroRegiao(){

    	$sql = "SELECT * FROM livroRegiao as lo
    	inner join regiaoRosacruz as rr on lo.regiao = rr.idRegiaoRosacruz
    	inner join usuario as u on u.seqCadast = lo.usuario
    	";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaExiste(){

    	$sql = "SELECT * FROM livroRegiao where 1=1 
    		and titulo = :titulo
                and regiao =  :regiao
                and assunto = :assunto
		and palavraChave = :palavraChave
                ";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->titulo, PDO::PARAM_STR);
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_INT);
        $sth->bindParam(':assunto', $this->assunto, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave , PDO::PARAM_STR);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaTituloLivroRegiao(){
        
    	$sql = "SELECT *   FROM    livroRegiao
                                    WHERE  titulo LIKE :titulo
                                    ORDER BY titulo ASC";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $titulo = '%'.str_replace("  "," ",trim($this->getTitulo())).'%';
        
        $sth->bindParam(':titulo', $titulo, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdLivroRegiao($idLivroRegiao){
        
    	$sql="SELECT * FROM  livroRegiao as a
                    inner join regiaoRosacruz as rr on rr.idRegiaoRosacruz = a.regiao
                                WHERE   idLivroRegiao = :id";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idLivroRegiao, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeLivroRegiao($idLivroRegiao){
        
        $sql="DELETE FROM livroRegiao WHERE idLivroRegiao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idLivroRegiao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    
}

?>
