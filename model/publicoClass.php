<?php

require_once ("conexaoClass.php");

class Publico {

    private $idPublico;
    private $publico;
    
    
    function getIdPublico() {
        return $this->idPublico;
    }

    function getPublico() {
        return $this->publico;
    }

    function setIdPublico($idPublico) {
        $this->idPublico = $idPublico;
    }

    function setPublico($publico) {
        $this->publico = $publico;
    }
    
    public function cadastroPublico()
    {
        $sql = "INSERT INTO publico (publico) value (:publico) ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
            
        $sth->bindParam(':publico', $this->publico, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }        

    # Seleciona 
    public function listaPublico($id) 
    {
        $sql = "SELECT * FROM publico where 1=1 ";
        if($id!=null)
        {
            $sql .= " and idPublico=:id";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        if($id!=null)
        {    
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function alteraPublico()
    {
        $sql = "UPDATE publico SET publico=:publico WHERE idPublico=:id ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
            
        $sth->bindParam(':publico', $this->publico, PDO::PARAM_STR);
        $sth->bindParam(':id', $this->idPublico, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }    
}

?>