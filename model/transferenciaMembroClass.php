<?php

require_once ("conexaoClass.php");

class transferenciaMembro{

    private $idTransferenciaMembro;
    private $nome;
    private $codigoAfiliacao;
    private $seqCadastMembro;    
    private $fk_idOrganismoAfiliadoOrigem;
    private $fk_idOrganismoAfiliadoDestino;
    private $motivoTransferencia;
    private $outrasInformacoes;
    private $observacoes;
    private $siglaOAOrigem;
    private $usuario;
    private $dataTransferencia;
    private $mestreOAOrigem;
    private $secretarioOAOrigem;

    //METODO SET/GET -----------------------------------------------------------

    function getIdTransferenciaMembro() {
        return $this->idTransferenciaMembro;
    }
    
    function getNome()
    {
    	return $this->nome;
    }
    
	function getCodigoAfiliacao()
    {
    	return $this->codigoAfiliacao;
    }
    
	function getSeqCadastMembro()
    {
    	return $this->seqCadastMembro;
    }
        
	function getFk_idOrganismoAfiliadoOrigem() {
        return $this->fk_idOrganismoAfiliadoOrigem;
    }
    
	function getFk_idOrganismoAfiliadoDestino() {
        return $this->fk_idOrganismoAfiliadoDestino;
    }
    
	function getMotivoTransferencia() {
        return $this->motivoTransferencia;
    }
    
	function getOutrasInformacoes() {
        return $this->outrasInformacoes;
    }
    
	function getObservacoes() {
        return $this->observacoes;
    }
    
	function getSiglaOAOrigem() {
        return $this->siglaOAOrigem;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
	function getDataTransferencia() {
        return $this->dataTransferencia;
    }
    
	function getMestreOAOrigem() {
        return $this->mestreOAOrigem;
    }
    
	function getSecretarioOAOrigem() {
        return $this->secretarioOAOrigem;
    }
    
    function setIdTransferenciaMembro($idTransferenciaMembro) {
        $this->idTransferenciaMembro = $idTransferenciaMembro;
    }
    
	function setNome($nome) {
        $this->nome = $nome;
    }
    
	function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }
    
	function setSeqCadastMembro($seqCadastMembro) {
        $this->seqCadastMembro = $seqCadastMembro;
    }
    
	function setFk_idOrganismoAfiliadoOrigem($fk_idOrganismoAfiliadoOrigem) {
        $this->fk_idOrganismoAfiliadoOrigem = $fk_idOrganismoAfiliadoOrigem;
    }
    
	function setFk_idOrganismoAfiliadoDestino($fk_idOrganismoAfiliadoDestino) {
        $this->fk_idOrganismoAfiliadoDestino = $fk_idOrganismoAfiliadoDestino;
    }
    
	function setMotivoTransferencia($motivoTransferencia) {
        $this->motivoTransferencia = $motivoTransferencia;
    }
    
	function setOutrasInformacoes($outrasInformacoes) {
        $this->outrasInformacoes = $outrasInformacoes;
    }
    
	function setObservacoes($observacoes) {
        $this->observacoes = $observacoes;
    }
    
	function setSiglaOAOrigem($siglaOAOrigem) {
        $this->siglaOAOrigem = $siglaOAOrigem;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
	function setDataTransferencia($dataTransferencia) {
        $this->dataTransferencia = $dataTransferencia;
    }
    
	function setMestreOAOrigem($mestreOAOrigem) {
        $this->mestreOAOrigem = $mestreOAOrigem;
    }
    
	function setSecretarioOAOrigem($secretarioOAOrigem) {
        $this->secretarioOAOrigem = $secretarioOAOrigem;
    }
    
    //--------------------------------------------------------------------------

    public function cadastroTransferenciaMembro(){

        $sql="INSERT INTO transferenciaMembro  
                                 (
                                  nome,
                                  codigoAfiliacao,
                                  seqCadastMembro,
                                  fk_idOrganismoAfiliadoOrigem,
                                  fk_idOrganismoAfiliadoDestino,
                                  motivoTransferencia,
                                  outrasInformacoes,
                                  observacoes,
                                  siglaOAOrigem,
                                  usuario,
                                  dataTransferencia,
                                  mestreOAOrigem,
                                  secretarioOAOrigem
                                  )
                                   
                            VALUES (
                            		:nome,
                            		:codigoAfiliacao,
                            		:seqCadastMembro,
                            		:fk_idOrganismoAfiliadoOrigem,
                            		:fk_idOrganismoAfiliadoDestino,
                            		:motivoTransferencia,
                            		:outrasInformacoes,
                            		:observacoes,
                            		:siglaOAOrigem,
                            		:usuario,
                            		now(),
                            		:mestreOAOrigem,
                            		:secretarioOAOrigem
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getNome                            = $this->getNome();
        $getCodigoAfiliacao                 = $this->getCodigoAfiliacao();
        $getSeqCadastMembro                 = $this->getSeqCadastMembro();
        $getFk_idOrganismoAfiliadoOrigem    = $this->getFk_idOrganismoAfiliadoOrigem();
        $getFk_idOrganismoAfiliadoDestino   = $this->getFk_idOrganismoAfiliadoDestino();
        $getMotivoTransferencia             = $this->getMotivoTransferencia();
        $getOutrasInformacoes               = $this->getOutrasInformacoes();
        $getObservacoes                     = $this->getObservacoes();
        $getSiglaOAOrigem                   = $this->getSiglaOAOrigem();
        $getUsuario                         = $this->getUsuario();
        $getMestreOAOrigem                  = $this->getMestreOAOrigem();
        $getSecretarioOAOrigem              = $this->getSecretarioOAOrigem();

        $sth->bindParam(':nome', $getNome, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $getCodigoAfiliacao, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastMembro', $getSeqCadastMembro, PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliadoOrigem', $getFk_idOrganismoAfiliadoOrigem, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliadoDestino', $getFk_idOrganismoAfiliadoDestino, PDO::PARAM_INT);
        $sth->bindParam(':motivoTransferencia', $getMotivoTransferencia, PDO::PARAM_INT);
        $sth->bindParam(':outrasInformacoes', $getOutrasInformacoes, PDO::PARAM_STR);
        $sth->bindParam(':observacoes', $getObservacoes, PDO::PARAM_INT);
        $sth->bindParam(':siglaOAOrigem', $getSiglaOAOrigem, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $getUsuario, PDO::PARAM_STR);
        $sth->bindParam(':mestreOAOrigem', $getMestreOAOrigem, PDO::PARAM_INT);
        $sth->bindParam(':secretarioOAOrigem', $getSecretarioOAOrigem, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idTransferenciaMembro) as lastid FROM transferenciaMembro";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	
    
    public function listaTransferenciaMembro($idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM transferenciaMembro
    	inner join organismoAfiliado on fk_idOrganismoAfiliadoOrigem = idOrganismoAfiliado
    	where 1=1 ";
        
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliadoOrigem = :idOrganismoAfiliado";
    	}
    	$sql .= " order by codigoAfiliacao";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($seqCadastMembro,$dataTransferencia,$origem,$destino){

    	$sql = "SELECT * FROM transferenciaMembro 
    			where seqCadastMembro=:seqCadastMembro
    			and DATE(dataTransferencia)=:dataTransferencia
    			and fk_idOrganismoAfiliadoOrigem=:fk_idOrganismoAfiliadoOrigem
    			and fk_idOrganismoAfiliadoDestino=:fk_idOrganismoAfiliadoDestino
    			";
    	
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':dataTransferencia', $dataTransferencia, PDO::PARAM_STR);
        $sth->bindParam(':fk_idOrganismoAfiliadoOrigem', $origem, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliadoDestino', $destino, PDO::PARAM_INT);

        if ($c->run($sth,false,false,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function buscaTransferenciaPeloId($id){

        $sql = "SELECT * FROM  transferenciaMembro
        			inner join organismoAfiliado on fk_idOrganismoAfiliadoOrigem = idOrganismoAfiliado 
        			WHERE idTransferenciaMembro = :id";

        //echo $sql."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaTransferenciaPeloSeqCadast($seqCadast){

        $sql = "SELECT * FROM  transferenciaMembro WHERE   seqCadastMembro = :seqCadast";

        //echo $sql."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
}

?>
