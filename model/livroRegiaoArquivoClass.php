<?php

require_once ("conexaoClass.php");

class livroRegiaoArquivo{

    private $idLivroRegiaoArquivo;
    private $fk_idLivroRegiao;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdLivroRegiaoArquivo() {
        return $this->idLivroRegiaoArquivo;
    }

    function getFkIdLivroRegiao() {
        return $this->fk_idLivroRegiao;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdLivroRegiaoArquivo($idLivroRegiaoArquivo) {
        $this->idLivroRegiaoArquivo = $idLivroRegiaoArquivo;
    }

    function setFkIdLivroRegiao($fk_idLivroRegiao) {
        $this->fk_idLivroRegiao = $fk_idLivroRegiao;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    //--------------------------------------------------------------------------

    public function cadastroLivroRegiaoArquivo(){

        $sql="INSERT INTO livroRegiao_arquivo  
                                 (
                                  fk_idLivroRegiao,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idLivroRegiao,
                                    :caminho,
                                    now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idLivroRegiao', $this->fk_idLivroRegiao, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'livroRegiao_arquivo'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
	        foreach ($resultado as $vetor)
	    	{
	    		$proximo_id = $vetor['Auto_increment']; 
	    	}
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaLivroRegiaoArquivo($id){

    	$sql = "SELECT * FROM livroRegiao_arquivo
    	where fk_idLivroRegiao = :id";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeLivroRegiaoArquivo($idLivroRegiaoArquivo){
        
        $sql="DELETE FROM livroRegiao_arquivo WHERE idLivroRegiaoArquivo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idLivroRegiaoArquivo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    
}

?>
