<?php
require_once ("conexaoClass.php");

class RelatorioOGGMensal {

    public $idRelatorioOGGMensal;
    public $fk_idOrganismoAfiliado;
    public $mes;
    public $ano;
    public $caminhoRelatorioOGGMensal;
    public $usuario;


    /* Funções GET e SET */


    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getIdRelatorioOGGMensal()
    {
        return $this->idRelatorioOGGMensal;
    }

    /**
     * @param mixed $idRelatorioOGGMensal
     */
    public function setIdRelatorioOGGMensal($idRelatorioOGGMensal)
    {
        $this->idRelatorioOGGMensal = $idRelatorioOGGMensal;
    }

    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @param mixed $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getCaminhoRelatorioOGGMensal()
    {
        return $this->caminhoRelatorioOGGMensal;
    }

    /**
     * @param mixed $caminhoRelatorioOGGMensal
     */
    public function setCaminhoRelatorioOGGMensal($caminhoRelatorioOGGMensal)
    {
        $this->caminhoRelatorioOGGMensal = $caminhoRelatorioOGGMensal;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }


        
    public function cadastraRelatorioOGGMensal() {
        
        $sql = "INSERT INTO relatorioOGGMensal
                                    (`fk_idOrganismoAfiliado`, 
                                    `mes`, 
                                    `ano`,
                                    `caminhoRelatorioOGGMensal`,
                                    `usuario`,
                                    `dataCadastro`
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :mes,
                                            :ano,
                                            '',
                                            :usuario,
                                            now()
                                            )";
        $id=1;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        //$sth->bindParam(':caminho',  $this->caminhoRelatorioAtividadeMensal, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
        	$resultado = $this->selecionaUltimoIdInserido();
			if($resultado)
			{
				foreach ($resultado as $vetor)
				{
					$id = $vetor['idRelatorioOGGMensal'];
				}
			}
        }
        return $id;
    }

    public function alteraRelatorioOGGMensal($id) {

        $sql = "UPDATE relatorioOGGMensal SET
        					 `mes` = :mes,
                                                 `ano` = :ano,
                                                 `caminhoRelatorioOGGMensal` = :caminho
                            WHERE `idRelatorioOGGMensal` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':caminho',  $this->caminhoRelatorioOGGMensal, PDO::PARAM_STR);
        $sth->bindParam(':id', $id , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function atualizaCaminhoRelatorioOGGMensalAssinado() {

        $sql = "UPDATE relatorioOGGMensal SET
                            `caminhoRelatorioOGGMensal` = :caminho
                            WHERE `idRelatorioOGGMensal` = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':caminho',  $this->caminhoRelatorioOGGMensal, PDO::PARAM_STR);
        $sth->bindParam(':id', $this->idRelatorioOGGMensal , PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioOGGMensal'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM relatorioOGGMensal ORDER BY idRelatorioOGGMensal DESC LIMIT 1";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRelatorioOGGMensal($mes=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioOGGMensal as rfm
    		inner join organismoAfiliado as oa on rfm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on rfm.usuario = u.seqCadast
    	where 1=1 ";
        
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        $sql .= " order by ano, mes ";
    	//echo $sql."<br><br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($mes=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioOGGMensal where 1=1 ";
        
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function retornaIdRelatorioOGGMensal() {
    	
    	$sql = "SELECT * FROM relatorioOGGMensal
                                 WHERE `mes` = :mes
                                 and `ano` = :ano
                                 and `fk_idOrganismoAfiliado` =  :idOrganismoAfiliado
                                 ";
    	//echo $sql;
   	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        
        $id=0;
        if ($resultado){
        	foreach ($resultado as $vetor)
        	{
        		$id = $vetor['idRelatorioOGGMensal'];
        	}
        }
        return $id;
    }

    public function buscarIdRelatorioOGGMensal($idRelatorioAtividadeMensal) {
        
        $sql = "SELECT * FROM relatorioOGGMensal
                                 WHERE `idRelatorioOGGMensal` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioAtividadeMensal, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioOGGMensal($id){
        
        $sql="DELETE FROM relatorioOGGMensal WHERE idRelatorioOGGMensal = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}
?>