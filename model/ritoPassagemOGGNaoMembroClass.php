<?php

require_once ("conexaoClass.php");

class ritoPassagemOGGNaoMembro{

    private $idRitoPassagemOGGNaoMembro;
    private $fk_idRitoPassagemOGG;
    private $nome;
    private $tipo;
    private $email;

    //METODO SET/GET -----------------------------------------------------------

    function getIdRitoPassagemOGGNaoMembro() {
        return $this->idRitoPassagemOGGNaoMembro;
    }

    function getFk_idRitoPassagemOGG() {
        return $this->fk_idRitoPassagemOGG;
    }

    function getNome() {
        return $this->nome;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getEmail() {
        return $this->email;
    }

    function setIdRitoPassagemOGGNaoMembro($idRitoPassagemOGGNaoMembro) {
        $this->idRitoPassagemOGGNaoMembro = $idRitoPassagemOGGNaoMembro;
    }

    function setFk_idRitoPassagemOGG($fk_idRitoPassagemOGG) {
        $this->fk_idRitoPassagemOGG = $fk_idRitoPassagemOGG;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    
    
    
    //--------------------------------------------------------------------------

    public function cadastro(){

        $sql = "INSERT INTO ritoPassagemOGG_naomembro
                                 (
                                  fk_idRitoPassagemOGG,
                                  nome,
                                  tipo,
                                  email
                                  )
                            VALUES (:fk_idRitoPassagemOGG,
                                    :nome,
                                    :tipo,
                                    :email)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idRitoPassagemOGG', $this->fk_idRitoPassagemOGG, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function lista($idRitoPassagemOGG=null,$v=null,$tipo=null){

    	$sql = "SELECT * FROM ritoPassagemOGG_naomembro where 1=1";

    	if($idRitoPassagemOGG!=null) {
    		$sql .= " and fk_idRitoPassagemOGG=:fk_idRitoPassagemOGG";
    	}
        if($v!=null) {
    		$sql .= " and nome=:nome";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idRitoPassagemOGG!=null){
            $sth->bindParam(':fk_idRitoPassagemOGG', $idRitoPassagemOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':nome', $v, PDO::PARAM_STR);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $v, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function total($idRitoPassagemOGG=null,$v=null,$tipo=null,$mes=null,$ano=null,$fk_idOrganismoAfiliado=null){

    	$sql = "SELECT count(idRitoPassagemOGGNaoMembro) as total FROM ritoPassagemOGG_naomembro "
                . " inner join ritoPassagemOGG on fk_idRitoPassagemOGG = idRitoPassagemOGG"
                . " where 1=1";

    	if($idRitoPassagemOGG!=null) {
    		$sql .= " and fk_idRitoPassagemOGG=:fk_idRitoPassagemOGG";
    	}
        if($v!=null) {
    		$sql .= " and nome=:nome";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
        if($mes!=null) {
    		$sql .= " and MONTH(data)=:mes";
    	}
        if($ano!=null) {
    		$sql .= " and YEAR(data)=:ano";
    	}
        if($fk_idOrganismoAfiliado!=null) {
    		$sql .= " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idRitoPassagemOGG!=null){
            $sth->bindParam(':fk_idRitoPassagemOGG', $idRitoPassagemOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':nome', $v, PDO::PARAM_STR);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        }
        if($mes!=null){
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null){
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }
    
    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

    public function remove($idRitoPassagemOGG=null,$id=null){

        $sql= "DELETE FROM ritoPassagemOGG_naomembro WHERE 1=1";

    	if($idRitoPassagemOGG!=null) {
    		$sql .= " and fk_idRitoPassagemOGG=:fk_idRitoPassagemOGG";
    	}
    	if($id!=null) {
    		$sql .= " and idRitoPassagemOGGNaoMembro=:id";
    	}

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idRitoPassagemOGG!=null){
            $sth->bindParam(':fk_idRitoPassagemOGG', $idRitoPassagemOGG, PDO::PARAM_INT);
        }
        if($id!=null){
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>
