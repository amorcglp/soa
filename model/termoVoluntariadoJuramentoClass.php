<?php

require_once ("conexaoClass.php");

class termoVoluntariadoJuramento{

    private $idTermoVoluntariadoJuramento;
    private $fk_idTermoVoluntariado;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdTermoVoluntariadoJuramento() {
        return $this->idTermoVoluntariadoJuramento;
    }

    function getFkIdTermoVoluntariado() {
        return $this->fk_idTermoVoluntariado;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdTermoVoluntariadoJuramento($idTermoVoluntariadoJuramento) {
        $this->idTermoVoluntariadoJuramento = $idTermoVoluntariadoJuramento;
    }

    function setFkIdTermoVoluntariado($fk_idTermoVoluntariado) {
        $this->fk_idTermoVoluntariado = $fk_idTermoVoluntariado;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    //--------------------------------------------------------------------------

    public function cadastroTermoVoluntariadoJuramento(){

        
        $sql="INSERT INTO termoVoluntariado_juramento  
                            (
                             fk_idTermoVoluntariado,
                             caminho,
                             dataCadastro
                             )
                            VALUES (:fk_idTermoVoluntariado,
                                    :caminho,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idTermoVoluntariado', $this->fk_idTermoVoluntariado, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        
        //echo "fk_idTermoVoluntariado:".$this->fk_idTermoVoluntariado;
        //echo "<br>caminho:".$this->caminho;
        //echo $sql;
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'termoVoluntariado_juramento'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado){
	        foreach ($resultado as $vetor)
	    	{
	    		$proximo_id = $vetor['Auto_increment']; 
	    	}
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaTermoVoluntariadoJuramento($id=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM termoVoluntariado_juramento
    	inner join termoVoluntariado on fk_idTermoVoluntariado = idTermoVoluntariado
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idTermoVoluntariado = :fk_idTermoVoluntariado";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado";
    	}
    	//echo "<br><br>".$sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':fk_idTermoVoluntariado', $id, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeTermoVoluntariadoJuramento($idTermoVoluntariadoJuramento){
        
        $sql = "DELETE FROM termoVoluntariado_juramento WHERE idTermoVoluntariadoJuramento = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idTermoVoluntariadoJuramento, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }
}

?>
