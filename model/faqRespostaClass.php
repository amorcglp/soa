<?php

require_once ("conexaoClass.php");

class FaqResposta {

    public $idRespostaFaq;
    public $tituloRespostaFaq;
    public $responsavelRespostaFaq;
    public $respostaFaq;
    public $dataRespostaFaq;

    /* Funções GET e SET */

    function getIdRespostaFaq() {
        return $this->idRespostaFaq;
    }

    function getTituloRespostaFaq() {
        return $this->tituloRespostaFaq;
    }

    function getResponsavelRespostaFaq() {
        return $this->responsavelRespostaFaq;
    }

    function getRespostaFaq() {
        return $this->respostaFaq;
    }

    function getDataRespostaFaq() {
        return $this->dataRespostaFaq;
    }

    function setIdRespostaFaq($idRespostaFaq) {
        $this->idRespostaFaq = $idRespostaFaq;
    }

    function setTituloRespostaFaq($tituloRespostaFaq) {
        $this->tituloRespostaFaq = $tituloRespostaFaq;
    }

    function setResponsavelRespostaFaq($responsavelRespostaFaq) {
        $this->responsavelRespostaFaq = $responsavelRespostaFaq;
    }

    function setRespostaFaq($respostaFaq) {
        $this->respostaFaq = $respostaFaq;
    }

    function setDataRespostaFaq($dataRespostaFaq) {
        $this->dataRespostaFaq = $dataRespostaFaq;
    }

    public function cadastraRespostaFaq() {
        
        $sql = "INSERT INTO respostafaq (`idRespostaFaq`, `tituloRespostaFaq`, `responsavelRespostaFaq`, `respostaFaq`, `dataRespostaFaq`)
                                    VALUES (:id,
                                            :titulo,
                                            :responsavel,
                                            :descricao,
                                            now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idRespostaFaq, PDO::PARAM_INT);
        $sth->bindParam(':titulo', $this->tituloRespostaFaq, PDO::PARAM_STR);
        $sth->bindParam(':responsavel', $this->responsavelRespostaFaq, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->respostaFaq, PDO::PARAM_STR);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }

    public function listaRespostaFaq() {

        $sql = "SELECT * FROM respostafaq";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdRespostaFaq() {
        
        $sql = "SELECT * FROM respostafaq 
                                 WHERE `idRespostaFaq` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        if(is_numeric($sth))
        {    
            $sth->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraRespostaFaq($id) {
        
        $sql = "UPDATE respostafaq SET  
                              `tituloRespostaFaq` = :titulo,
        		      `responsavelRespostaFaq` = :responsavel,
        		      `respostaFaq` = :descricao,
        		      `dataRespostaFaq` = now()
                        WHERE `idRespostaFaq` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
  
        $sth->bindParam(':titulo', $this->tituloRespostaFaq, PDO::PARAM_STR);
        $sth->bindParam(':responsavel', $this->responsavelRespostaFaq, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->respostaFaq, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }

    public function removeRespostaFaq($idRespostaFaq) {
        
        $sql = "DELETE FROM respostafaq WHERE `idRespostaFaq` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRespostaFaq, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }

}

?>