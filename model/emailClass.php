<?php

require_once ("conexaoClass.php");

class Email {

    private $idEmail;
    private $assunto;
    private $mensagem;
    private $de;
    private $dataCadastro;

    function getIdEmail() {
        return $this->idEmail;
    }

    function getAssunto() {
        return $this->assunto;
    }

    function getMensagem() {
        return $this->mensagem;
    }

    function getDe() {
        return $this->de;
    }

    function setIdEmail($idEmail) {
        $this->idEmail = $idEmail;
    }

    function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    function setDe($de) {
        $this->de = $de;
    }

    # 

    public function cadastraEmail() {
        
        $sql = "INSERT INTO email  (de,	
                                  dataCadastro
                                  )   
                            VALUES (
                            		:de,
                                    now()
                                    )";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':de',  $this->de, PDO::PARAM_INT);

        if ($c->run($sth, true)) {

            $ultimo_id = 0;
            $resultado = $this->selecionaUltimoId();
            if ($resultado) {
                foreach ($resultado as $vetor) {
                    $ultimo_id = $vetor['lastid'];
                }
            }
            return $ultimo_id;
        } else {
            return false;
        }
    }

    function atualizaEmail() {
        
        $sql = ("UPDATE email SET 
					assunto=:assunto, 
					mensagem=:mensagem 
				WHERE idEmail=:id");
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':assunto',  $this->assunto, PDO::PARAM_INT);
        $sth->bindParam(':mensagem',  $this->mensagem, PDO::PARAM_INT);
        $sth->bindParam(':id',  $this->idEmail, PDO::PARAM_INT);

	if ($c->run($sth, true)) {
            $retorno = true;
        }else{
            
        }
        return $retorno;
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idEmail) as lastid FROM email";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaEmail($pastaInterna = null, $seqCadast = null, $search = null) 
    {

        $sql = "SELECT *,e.dataCadastro as dataCad FROM email as e
        inner join emailDePara as edp on e.idEmail=edp.fk_idEmail
        inner join usuario as u on e.de = u.seqCadast
        left join emailAnexo as ea on e.idEmail=ea.fk_idEmail
        where e.assunto <> '' and e.mensagem <> '' ";
        if ($search != null) {
            $sql .= " and 
        		(
        			assunto like :search
        			|| mensagem like :search
        			|| nome_arquivo like :search
        			|| nomeUsuario like :search
        			|| e.dataCadastro like :search
        		)";
            $sql .= " and edp.para=:seqCadast";
        } else {
            if ($pastaInterna != null) {
                $sql .= " and edp.pastaInterna=:pastaInterna";
            }

            if ($pastaInterna == 2) {
                $sql .= " and edp.enviado=1";
                $sql .= " and e.de=:seqCadast";
            } else {
                if ($pastaInterna != 4) {
                    $sql .= " and edp.para=:seqCadast";
                    $sql .= " and edp.enviado=0";
                } else {
                    $sql .= " and ( edp.para=:seqCadast or edp.para=0 )";
                }
            }
        }
        $sql .= " and edp.pastaInterna <> 5";
        $sql .= " 
        group by e.idEmail
        order by e.dataCadastro desc";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($search != null) {
            $search = '%'.$search.'%';
            $sth->bindParam(':search', $search , PDO::PARAM_STR);
            $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);
        }else{
            if ($pastaInterna != null) {
                $sth->bindParam(':pastaInterna',  $pastaInterna, PDO::PARAM_INT);
            }
            if ($pastaInterna == 2) {
                $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);
            }else{
                $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);
            }
        }    
        

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarEmailPorId($id) {

        $sql = "SELECT * FROM email as e
        inner join emailDePara as edp on e.idEmail=edp.fk_idEmail
        inner join usuario as u on e.de = u.seqCadast
        where assunto <> '' and mensagem <> '' ";
        if ($id != null) {
            $sql .= " and idEmail=:id";
        }
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id',  $id, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function limparEmails() {
        
        $sql = "DELETE FROM email 
            where assunto = '' and mensagem = '' ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

	if ($c->run($sth, true)) {
            return true;
        } else {
            return false;
        }
    }

}

?>