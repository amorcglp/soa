<?php

include_once("conexaoClass.php");

class login {

    public $loginUsuario;
    public $senhaUsuario;        

    public function efetuaLogin() {
        $sql = "SELECT * FROM usuario as u
                              WHERE  (loginUsuario = :loginUsuario)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':loginUsuario', $this->loginUsuario, PDO::PARAM_STR);

        return $c->run($sth);
    }
    
    public function verificaHash($login,$hash) {
        
        
            $sql = "SELECT * FROM usuario as u
                                  WHERE  loginUsuario = :loginUsuario
                                  and senhaUsuario = :hash";

            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql);
            
            $sth->bindParam(':loginUsuario', $login, PDO::PARAM_STR);
            $sth->bindParam(':hash', $hash, PDO::PARAM_STR);

            if ($c->run($sth,null,null,true)){
                return true;
            } else {
                return false;
            }
            
        
    }
    
    public function removeUsuarioDeUsuariosOnline($seqCadast)
    {
        $sql = 'DELETE FROM usersonline WHERE seqCadast=:seqCadast'; //deleta os ips com mais de 5 minutos
    
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_STR);

        if($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }        

}

?>
