<?php

require_once ("conexaoClass.php");

class planoAcaoOrganismoAtualizacao{

    private $idPlanoAcaoOrganismoAtualizacao;
    private $fk_idPlanoAcaoOrganismo;
    private $seqCadast;
    private $mensagem;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoAcaoOrganismoAtualizacao() {
        return $this->idPlanoAcaoOrganismoAtualizacao;
    }

    function getFk_idPlanoAcaoOrganismo() {
        return $this->fk_idPlanoAcaoOrganismo;
    }
    
	function getSeqCadast() {
        return $this->seqCadast;
    }
    
	function getMensagem() {
        return $this->mensagem;
    } 

    function setIdPlanoAcaoOrganismoAtualizacao($idPlanoAcaoOrganismoAtualizacao) {
        $this->idPlanoAcaoOrganismoAtualizacao = $idPlanoAcaoOrganismoAtualizacao;
    }
  
	function setFk_idPlanoAcaoOrganismo($fk_idPlanoAcaoOrganismo) {
        $this->fk_idPlanoAcaoOrganismo = $fk_idPlanoAcaoOrganismo;
    }
    
	function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }
    
	function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoAcaoOrganismoAtualizacao(){

        $sql="INSERT INTO planoAcaoOrganismo_atualizacao  
                                 (
                                  fk_idPlanoAcaoOrganismo,
                                  seqCadast,
                                  mensagem,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idPlanoAcaoOrganismo,
                                    :seqCadast,
                                    :mensagem,
                                    now()
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoOrganismo', $this->fk_idPlanoAcaoOrganismo, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':mensagem', $this->mensagem, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function listaAtualizacao($idPlanoAcaoOrganismo=null,$idPlanoAcaoOrganismoAtualizacao=null){

    	$sql = "SELECT * FROM planoAcaoOrganismo_atualizacao as p
    	inner join usuario as u on p.seqCadast = u.seqCadast
    	where 1=1 
    	 ";   
    	if($idPlanoAcaoOrganismo!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoOrganismo=:idPlanoAcaoOrganismo";
    	}
    	if($idPlanoAcaoOrganismoAtualizacao!=null)
    	{
    		$sql .= " and idPlanoAcaoOrganismoAtualizacao=:idPlanoAcaoOrganismoAtualizacao";
    	}	 
    	$sql .= " order by dataCadastro desc";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoOrganismo!=null)
    	{
            $sth->bindParam(':idPlanoAcaoOrganismo', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        }
        if($idPlanoAcaoOrganismoAtualizacao!=null)
    	{
            $sth->bindParam(':idPlanoAcaoOrganismoAtualizacao', $idPlanoAcaoOrganismoAtualizacao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaUltimaAtualizacao($idPlanoAcaoOrganismo){

    	$sql = "SELECT * FROM planoAcaoOrganismo_atualizacao
    	where 1=1 
    	 ";   
    	if($idPlanoAcaoOrganismo!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoOrganismo=:idPlanoAcaoOrganismo";
    	}	 
    	$sql .= " order by dataCadastro desc limit 1";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoOrganismo!=null)
    	{
            $sth->bindParam(':idPlanoAcaoOrganismo', $idPlanoAcaoOrganismo, PDO::PARAM_INT);        
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
        	foreach($resultado as $vetor)
        	{
        		$ultimaAtualizacao = $vetor['dataCadastro'];
        	}
            return $ultimaAtualizacao;
        }else{
            return "";
        }
    }

    public function removeAtualizacao(){
        
        $sql="DELETE FROM planoAcaoOrganismo_atualizacao WHERE idPlanoAcaoOrganismoAtualizacao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idPlanoAcaoOrganismoAtualizacao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }  

    public function removeAtualizacoes($idPlanoAcaoOrganismo){
        
        $sql="DELETE FROM planoAcaoOrganismo_atualizacao WHERE fk_idPlanoAcaoOrganismo = :idPlanoAcaoOrganismo";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoOrganismo', $idPlanoAcaoOrganismo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
