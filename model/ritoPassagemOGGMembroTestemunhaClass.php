<?php

require_once ("conexaoClass.php");

class ritoPassagemOGGMembroTestemunha{

    private $idRitoPassagemOGGMembroTestemunha;
    private $fk_idRitoPassagemOGG;
    private $fk_seq_cadastMembro;
    private $tipo;
    private $email;

    //METODO SET/GET -----------------------------------------------------------

    function getIdRitoPassagemOGGMembroTestemunha() {
        return $this->idRitoPassagemOGGMembroTestemunha;
    }

    function getFk_idRitoPassagemOGG() {
        return $this->fk_idRitoPassagemOGG;
    }

    function getFk_seq_cadastMembro() {
        return $this->fk_seq_cadastMembro;
    }

    function getTipo() {
        return $this->tipo;
    }

    function getEmail() {
        return $this->email;
    }

    function setIdRitoPassagemOGGMembroTestemunha($idRitoPassagemOGGMembroTestemunha) {
        $this->idRitoPassagemOGGMembroTestemunha = $idRitoPassagemOGGMembroTestemunha;
    }

    function setFk_idRitoPassagemOGG($fk_idRitoPassagemOGG) {
        $this->fk_idRitoPassagemOGG = $fk_idRitoPassagemOGG;
    }

    function setFk_seq_cadastMembro($fk_seq_cadastMembro) {
        $this->fk_seq_cadastMembro = $fk_seq_cadastMembro;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    function setEmail($email) {
        $this->email = $email;
    }

            
    //--------------------------------------------------------------------------

    public function cadastro(){

        $sql = "INSERT INTO ritoPassagemOGG_membro_testemunha
                                 (
                                  fk_idRitoPassagemOGG,
                                  fk_seq_cadastMembro,
                                  tipo,
                                  email
                                  )
                            VALUES (:fk_idRitoPassagemOGG,
                                    :fk_seq_cadastMembro,
                                    :tipo,
                                    :email)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idRitoPassagemOGG', $this->fk_idRitoPassagemOGG, PDO::PARAM_INT);
        $sth->bindParam(':fk_seq_cadastMembro', $this->fk_seq_cadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }


    public function lista($idRitoPassagemOGG=null,$v=null,$tipo=null){

    	$sql = "SELECT * FROM ritoPassagemOGG_membro_testemunha where 1=1";

    	if($idRitoPassagemOGG!=null) {
    		$sql .= " and fk_idRitoPassagemOGG=:fk_idRitoPassagemOGG";
    	}
        if($v!=null) {
    		$sql .= " and fk_seq_cadastMembro=:fk_seq_cadastMembro";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idRitoPassagemOGG!=null){
            $sth->bindParam(':fk_idRitoPassagemOGG', $idRitoPassagemOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':fk_seq_cadastMembro', $v, PDO::PARAM_INT);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function total($idRitoPassagemOGG=null,$v=null,$tipo=null,$mes=null,$ano=null,$fk_idOrganismoAfiliado=null){

    	$sql = "SELECT count(idRitoPassagemOGGMembroTestemunha) as total FROM ritoPassagemOGG_membro_testemunha "
                . " inner join ritoPassagemOGG on fk_idRitoPassagemOGG = idRitoPassagemOGG"
                . " where 1=1";

    	if($idRitoPassagemOGG!=null) {
    		$sql .= " and fk_idRitoPassagemOGG=:fk_idRitoPassagemOGG";
    	}
        if($v!=null) {
    		$sql .= " and fk_seq_cadastMembro=:fk_seq_cadastMembro";
    	}
        if($tipo!=null) {
    		$sql .= " and tipo=:tipo";
    	}
        if($mes!=null) {
    		$sql .= " and MONTH(data)=:mes";
    	}
        if($ano!=null) {
    		$sql .= " and YEAR(data)=:ano";
    	}
        if($fk_idOrganismoAfiliado!=null) {
    		$sql .= " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idRitoPassagemOGG!=null){
            $sth->bindParam(':fk_idRitoPassagemOGG', $idRitoPassagemOGG, PDO::PARAM_INT);
        }
        if($v!=null){
            $sth->bindParam(':fk_seq_cadastMembro', $v, PDO::PARAM_INT);
        }
        if($tipo!=null){
            $sth->bindParam(':tipo', $tipo, PDO::PARAM_INT);
        }
        if($mes!=null){
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null){
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function remove($idRitoPassagemOGG=null,$id=null){

        $sql= "DELETE FROM ritoPassagemOGG_membro_testemunha WHERE 1=1";

    	if($idRitoPassagemOGG!=null) {
    		$sql .= " and fk_idRitoPassagemOGG=:fk_idRitoPassagemOGG";
    	}
    	if($id!=null) {
    		$sql .= " and idRitoPassagemOGGMembroTestemunha=:id";
    	}

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idRitoPassagemOGG!=null){
            $sth->bindParam(':fk_idRitoPassagemOGG', $idRitoPassagemOGG, PDO::PARAM_INT);
        }
        if($id!=null){
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>
