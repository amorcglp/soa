<?php

require_once ("conexaoClass.php");

class baixaAutomaticaFuncoesUsuario {

    public $idBaixaAutomaticaFuncoesUsuario;
    public $regiao;
    public $siglaOA;

    //Getter e Setter
    function getIdBaixaAutomaticaFuncoesUsuario() {
        return $this->idBaixaAutomaticaFuncoesUsuario;
    }

    function getRegiao() {
        return $this->regiao;
    }

    function setIdBaixaAutomaticaFuncoesUsuario($idBaixaAutomaticaFuncoesUsuario) {
        $this->idBaixaAutomaticaFuncoesUsuario = $idBaixaAutomaticaFuncoesUsuario;
    }

    function setRegiao($regiao) {
        $this->regiao = $regiao;
    }
    function getSiglaOA() {
        return $this->siglaOA;
    }

    function setSiglaOA($siglaOA) {
        $this->siglaOA = $siglaOA;
    }

    
    public function cadastraBaixaAutomaticaFuncoesUsuario() {
        
        $sql = "INSERT INTO baixaAutomaticaFuncoesUsuario (regiao, siglaOA, dataCadastro)
                                    VALUES (:regiao,
                                            :siglaOA,
                                            now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 
        
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_STR);
        $sth->bindParam(':siglaOA', $this->siglaOA, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    
    public function listaBaixaAutomaticaFuncoesUsuario() {

        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario as b "
               . " inner join organismoAfiliado as o on o.siglaOrganismoAfiliado = b.siglaOA "
               . " where 1=1 ";
        $sql .=" ORDER BY o.siglaOrganismoAfiliado";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaBaixaAutomaticaFuncoesUsuarioHoje($dataAgendadaInicial=null,$dataAgendadaFinal=null) {

       $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario as b "
               . " inner join organismoAfiliado as o on o.siglaOrganismoAfiliado = b.siglaOA "
               . " where 1=1 ";
       if($dataAgendadaInicial!=null)
       {    
               $sql .= " and DATE(b.dataCadastro) >= :dataAgendadaInicial";
       }
       if($dataAgendadaFinal!=null)
       {    
               $sql .= " and DATE(b.dataCadastro) <= :dataAgendadaFinal";
       }
       //echo $sql;
       
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
       
        if($dataAgendadaInicial!=null)
        {
            $sth->bindParam(':dataAgendadaInicial', $dataAgendadaInicial, PDO::PARAM_STR);
        }
        
        if($dataAgendadaFinal!=null)
        {
            $sth->bindParam(':dataAgendadaFinal', $dataAgendadaFinal, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function limpaTabela() {
        
        $sql = "TRUNCATE baixaAutomaticaFuncoesUsuario";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function verificaSeJaExisteLog($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_log as b "
               . " where 1=1 ";
       if($seqCadast!=null)
       {    
               $sql .= " and seqCadast = :seqCadast";
       }
       if($codAfiliacaoMembroOficial!=null)
       {    
               $sql .= " and codAfiliacaoMembroOficial = :codAfiliacaoMembroOficial";
       }
       if($tipoMembro!=null)
       {    
               $sql .= " and tipoMembro = :tipoMembro";
       }
       if($siglaOrganismoOriginal!=null)
       {    
               $sql .= " and siglaOrganismo = :siglaOrganismoOriginal";
       }
       if($tipoCargo!=null)
       {    
               $sql .= " and tipoCargo = :tipoCargo";
       }
       if($funcao!=null)
       {    
               $sql .= " and funcao = :funcao";
       }
       if($dataEntrada!=null)
       {    
               $sql .= " and dataEntrada = :dataEntrada";
       }
       if($dataTerminoMandato!=null)
       {    
               $sql .= " and dataTerminoMandato = :dataTerminoMandato";
       }
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seqCadast!=null)
        {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        
        if($codAfiliacaoMembroOficial!=null)
        {
            $sth->bindParam(':codAfiliacaoMembroOficial', $codAfiliacaoMembroOficial, PDO::PARAM_INT);
        }
        
        if($tipoMembro!=null)
        {
            $sth->bindParam(':tipoMembro', $tipoMembro, PDO::PARAM_INT);
        }
        
        if($siglaOrganismoOriginal!=null)
        {
            $sth->bindParam(':siglaOrganismoOriginal', $siglaOrganismoOriginal, PDO::PARAM_STR);
        }
        
        if($tipoCargo!=null)
        {
            $sth->bindParam(':tipoCargo', $tipoCargo, PDO::PARAM_INT);
        }
        
        if($funcao!=null)
        {
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
        }
        
        if($dataEntrada!=null)
        {
            $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
        }
        
        if($dataTerminoMandato!=null)
        {
            $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);
        }

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }        
    
    public function cadastraLog($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato)
    {
        if(!$this->verificaSeJaExisteLog($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato))
        {
            $sql = "INSERT INTO baixaAutomaticaFuncoesUsuario_log 
                                    (seqCadast, 
                                    codAfiliacaoMembroOficial, 
                                    tipoMembro,
                                    companheiro,
                                    siglaOrganismo,
                                    tipoCargo,
                                    funcao,
                                    dataEntrada,
                                    dataTerminoMandato,
                                    dataCadastro)
                                    VALUES (:seqCadast,
                                            :codAfiliacao,
                                            :tipoMembro,
                                            :companheiro,
                                            :siglaOrganismoOriginal,
                                            :tipoCargo,
                                            :funcao,
                                            :dataEntrada,
                                            :dataTerminoMandato,    
                                            now())";
            //echo $sql;
            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql); 

            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
            $sth->bindParam(':codAfiliacao', $codAfiliacaoMembroOficial, PDO::PARAM_INT);
            $sth->bindParam(':tipoMembro', $tipoMembro, PDO::PARAM_INT);
            $sth->bindParam(':companheiro', $companheiro, PDO::PARAM_STR);
            $sth->bindParam(':siglaOrganismoOriginal', $siglaOrganismoOriginal, PDO::PARAM_STR);
            $sth->bindParam(':tipoCargo', $tipoCargo, PDO::PARAM_INT);
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
            $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
            $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);


            if ($c->run($sth,true)) {
                return true;
            } else {
                return false;
            }
        }else{
            return true;
        }
    }
    
    public function cadastraDuplicados($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato)
    {
        if(!$this->verificaSeJaExisteDuplicados($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato))
        {
            $sql = "INSERT INTO baixaAutomaticaFuncoesUsuario_duplicados 
                                    (seqCadast, 
                                    codAfiliacaoMembroOficial, 
                                    tipoMembro,
                                    companheiro,
                                    siglaOrganismo,
                                    tipoCargo,
                                    funcao,
                                    dataEntrada,
                                    dataTerminoMandato,
                                    dataCadastro)
                                    VALUES (:seqCadast,
                                            :codAfiliacao,
                                            :tipoMembro,
                                            :companheiro,
                                            :siglaOrganismoOriginal,
                                            :tipoCargo,
                                            :funcao,
                                            :dataEntrada,
                                            :dataTerminoMandato,    
                                            now())";
            //echo $sql;
            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql); 

            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
            $sth->bindParam(':codAfiliacao', $codAfiliacaoMembroOficial, PDO::PARAM_INT);
            $sth->bindParam(':tipoMembro', $tipoMembro, PDO::PARAM_INT);
            $sth->bindParam(':companheiro', $companheiro, PDO::PARAM_STR);
            $sth->bindParam(':siglaOrganismoOriginal', $siglaOrganismoOriginal, PDO::PARAM_STR);
            $sth->bindParam(':tipoCargo', $tipoCargo, PDO::PARAM_INT);
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
            $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
            $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);

            if ($c->run($sth,true)) {
                return true;
            } else {
                return false;
            }
        }else{
            return true;
        }
    }
    
    public function verificaSeJaExisteNotLog($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato)
    {
       $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_notlog as b "
               . " where 1=1 ";
       if($seqCadast!=null)
       {    
               $sql .= " and seqCadast = :seqCadast";
       }
       if($codAfiliacaoMembroOficial!=null)
       {    
               $sql .= " and codAfiliacaoMembroOficial = :codAfiliacaoMembroOficial";
       }
       if($tipoMembro!=null)
       {    
               $sql .= " and tipoMembro = :tipoMembro";
       }
       if($siglaOrganismoOriginal!=null)
       {    
               $sql .= " and siglaOrganismo = :funcao";
       }
       if($tipoCargo!=null)
       {    
               $sql .= " and tipoCargo = :tipoCargo";
       }
       if($funcao!=null)
       {    
               $sql .= " and funcao = :funcao";
       }
       if($dataEntrada!=null)
       {    
               $sql .= " and dataEntrada = :dataEntrada";
       }
       if($dataTerminoMandato!=null)
       {    
               $sql .= " and dataTerminoMandato = :dataTerminoMandato";
       }
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seqCadast!=null)
        {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        
        if($codAfiliacaoMembroOficial!=null)
        {
            $sth->bindParam(':codAfiliacaoMembroOficial', $codAfiliacaoMembroOficial, PDO::PARAM_INT);
        }
        
        if($tipoMembro!=null)
        {
            $sth->bindParam(':tipoMembro', $tipoMembro, PDO::PARAM_INT);
        }
        
        if($siglaOrganismoOriginal!=null)
        {
            $sth->bindParam(':siglaOrganismoOriginal', $siglaOrganismoOriginal, PDO::PARAM_STR);
        }
        
        if($tipoCargo!=null)
        {
            $sth->bindParam(':tipoCargo', $tipoCargo, PDO::PARAM_INT);
        }
        
        if($funcao!=null)
        {
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
        }
        
        if($dataEntrada!=null)
        {
            $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
        }
        
        if($dataTerminoMandato!=null)
        {
            $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);
        }

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }   
    
    public function excluiNotLog($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato)
    {
        $sql =" DELETE FROM baixaAutomaticaFuncoesUsuario_notlog "
               . " where ";
               $sql .= " seqCadast = :seqCadast";   
               $sql .= " and codAfiliacaoMembroOficial = :codAfiliacao";
               $sql .= " and tipoMembro = :tipoMembro";
               $sql .= " and siglaOrganismo = :siglaOrganismoOriginal";    
               $sql .= " and tipoCargo = :tipoCargo";
               $sql .= " and funcao = :funcao";
               $sql .= " and dataEntrada = :dataEntrada";
               $sql .= " and dataTerminoMandato = :dataTerminoMandato";
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 

            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
            $sth->bindParam(':codAfiliacao', $codAfiliacaoMembroOficial, PDO::PARAM_INT);
            $sth->bindParam(':tipoMembro', $tipoMembro, PDO::PARAM_INT);
            $sth->bindParam(':companheiro', $companheiro, PDO::PARAM_STR);
            $sth->bindParam(':siglaOrganismoOriginal', $siglaOrganismoOriginal, PDO::PARAM_STR);
            $sth->bindParam(':tipoCargo', $tipoCargo, PDO::PARAM_INT);
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
            $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
            $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);


        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }   
    
    public function verificaSeJaExisteDuplicados($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_duplicados as b "
               . " where 1=1 ";
       if($seqCadast!=null)
       {    
               $sql .= " and seqCadast = :seqCadast";
       }
       if($codAfiliacaoMembroOficial!=null)
       {    
               $sql .= " and codAfiliacaoMembroOficial = :codAfiliacaoMembroOficial";
       }
       if($tipoMembro!=null)
       {    
               $sql .= " and tipoMembro = :tipoMembro";
       }
       if($siglaOrganismoOriginal!=null)
       {    
               $sql .= " and siglaOrganismo = :siglaOrganismoOriginal";
       }
       if($tipoCargo!=null)
       {    
               $sql .= " and tipoCargo = :tipoCargo";
       }
       if($funcao!=null)
       {    
               $sql .= " and funcao = :funcao";
       }
       if($dataEntrada!=null)
       {    
               $sql .= " and dataEntrada = :dataEntrada";
       }
       if($dataTerminoMandato!=null)
       {    
               $sql .= " and dataTerminoMandato = :dataTerminoMandato";
       }
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seqCadast!=null)
        {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        
        if($codAfiliacaoMembroOficial!=null)
        {
            $sth->bindParam(':codAfiliacaoMembroOficial', $codAfiliacaoMembroOficial, PDO::PARAM_INT);
        }
        
        if($tipoMembro!=null)
        {
            $sth->bindParam(':tipoMembro', $tipoMembro, PDO::PARAM_INT);
        }
        
        if($siglaOrganismoOriginal!=null)
        {
            $sth->bindParam(':siglaOrganismoOriginal', $siglaOrganismoOriginal, PDO::PARAM_STR);
        }
        
        if($tipoCargo!=null)
        {
            $sth->bindParam(':tipoCargo', $tipoCargo, PDO::PARAM_INT);
        }
        
        if($funcao!=null)
        {
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
        }
        
        if($dataEntrada!=null)
        {
            $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
        }
        
        if($dataTerminoMandato!=null)
        {
            $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);
        }

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function verificaSeJaExisteAgenda($dataAgendadaInicial)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_agenda as b "
               . " where 1=1 ";
       if($dataAgendadaInicial!=null)
       {    
               $sql .= " and dataAgendadaInicial = :dataAgendadaInicial";
       }
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($dataAgendadaInicial!=null)
        { 
            $sth->bindParam(':dataAgendadaInicial', $dataAgendadaInicial, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function cadastraNotLog($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato,$existemMaisDeUmCadastro=null)
    {
        if($existemMaisDeUmCadastro==null)
        {
            $existemMaisDeUmCadastro=0;
        }    
        if(!$this->verificaSeJaExisteNotLog($seqCadast,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato))
        {        
            $sql = "INSERT INTO baixaAutomaticaFuncoesUsuario_notlog 
                                    (seqCadast, 
                                    codAfiliacaoMembroOficial, 
                                    tipoMembro,
                                    companheiro,
                                    siglaOrganismo,
                                    tipoCargo,
                                    funcao,
                                    dataEntrada,
                                    dataTerminoMandato,
                                    existemMaisDeUmCadastro,
                                    dataCadastro)
                                    VALUES (:seqCadast,
                                            :codAfiliacao,
                                            :tipoMembro,
                                            :companheiro,
                                            :siglaOrganismoOriginal,
                                            :tipoCargo,
                                            :funcao,
                                            :dataEntrada,
                                            :dataTerminoMandato,    
                                            :existemMaisDeUmCadastro,    
                                            now())";
            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql); 

            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
            $sth->bindParam(':codAfiliacao', $codAfiliacaoMembroOficial, PDO::PARAM_INT);
            $sth->bindParam(':tipoMembro', $tipoMembro, PDO::PARAM_INT);
            $sth->bindParam(':companheiro', $companheiro, PDO::PARAM_STR);
            $sth->bindParam(':siglaOrganismoOriginal', $siglaOrganismoOriginal, PDO::PARAM_STR);
            $sth->bindParam(':tipoCargo', $tipoCargo, PDO::PARAM_INT);
            $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
            $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
            $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);
            $sth->bindParam(':existemMaisDeUmCadastro', $existemMaisDeUmCadastro, PDO::PARAM_INT);

            if ($c->run($sth,true)) {
                return true;
            } else {
                return false;
            }
        }else{
            return true;
        }    
    }
    
    public function cadastraAgenda($dataAgendadaInicial,$dataAgendadaFinal,$quantidadeDiasProrrogacaoBaixa,$usuario)
    {
        if(!$this->verificaSeJaExisteAgenda($dataAgendadaInicial))
        {        
            $sql = "INSERT INTO baixaAutomaticaFuncoesUsuario_agenda 
                                    (dataAgendadaInicial,
                                    dataAgendadaFinal,
                                    quantidadeDiasProrrogacaoBaixa,
                                    usuario, 
                                    dataCadastro)
                                    VALUES (:dataAgendadaInicial,
                                            :dataAgendadaFinal,
                                            :quantidadeDiasProrrogacaoBaixa,    
                                            :usuario,
                                            now())";
            //echo $sql;
            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql); 

            $sth->bindParam(':dataAgendadaInicial', $dataAgendadaInicial, PDO::PARAM_STR);
            $sth->bindParam(':dataAgendadaFinal', $dataAgendadaFinal, PDO::PARAM_STR);
            $sth->bindParam(':quantidadeDiasProrrogacaoBaixa', $quantidadeDiasProrrogacaoBaixa, PDO::PARAM_INT);
            $sth->bindParam(':usuario', $usuario, PDO::PARAM_INT);

            if ($c->run($sth,true)) 
            {
                return true;
            } else {
                return false;
            }
        }else{
            return true;
        }    
    }
        
    public function atualizaBaixaFuncaoPendente($seqCadast,$status,$funcao,$siglaOA,$dataEntrada,$dataTerminoMandato)
    {        
        $sql = "UPDATE baixaAutomaticaFuncoesUsuario_notlog 
                                    SET
                                    status=:status
                                    where    
                                    seqCadast = :seqCadast
                                    and funcao = :funcao     
                                    and siglaOrganismo = :siglaOA        
                                    and dataEntrada = :dataEntrada
                                    and dataTerminoMandato = :dataTerminoMandato";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 

        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
        $sth->bindParam(':siglaOA', $siglaOA, PDO::PARAM_STR);
        $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
        $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);

        if ($c->run($sth,true)) 
        {
            return true;
        } else {
            return false;
        }    
    }
    
    public function atualizaBaixaDuplicados($seqCadast,$status,$funcao,$siglaOA,$dataEntrada,$dataTerminoMandato)
    {        
        $sql = "UPDATE baixaAutomaticaFuncoesUsuario_duplicados 
                                    SET
                                    status=:status
                                    where    
                                    seqCadast = :seqCadast
                                    and funcao = :funcao     
                                    and siglaOrganismo = :siglaOA        
                                    and dataEntrada = :dataEntrada
                                    and dataTerminoMandato = :dataTerminoMandato";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 

        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':funcao', $funcao, PDO::PARAM_INT);
        $sth->bindParam(':siglaOA', $siglaOA, PDO::PARAM_STR);
        $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);
        $sth->bindParam(':dataTerminoMandato', $dataTerminoMandato, PDO::PARAM_STR);

        if ($c->run($sth,true)) 
        {
            return true;
        } else {
            return false;
        }    
    }
    
    public function atualizaAgenda($dataAgendadaInicial,$dataAgendadaFinal,$quantidadeDiasProrrogacaoBaixa,$usuario,$id)
    {        
        $sql = "UPDATE baixaAutomaticaFuncoesUsuario_agenda 
                                    SET
                                    dataAgendadaInicial=:dataAgendadaInicial,
                                    dataAgendadaFinal=:dataAgendadaFinal,
                                    quantidadeDiasProrrogacaoBaixa=:quantidade,    
                                    usuarioAlteracao=:usuario    
                                    where    
                                    idBaixaAutomaticaFuncoesUsuarioAgenda = :id
                                    ";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 

        $sth->bindParam(':dataAgendadaInicial', $dataAgendadaInicial, PDO::PARAM_INT);
        $sth->bindParam(':dataAgendadaFinal', $dataAgendadaFinal, PDO::PARAM_INT);
        $sth->bindParam(':quantidade', $quantidadeDiasProrrogacaoBaixa, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $usuario, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_STR);

        if ($c->run($sth,true)) 
        {
            return true;
        } else {
            return false;
        }    
    }
    
    public function selecionaDuplicados($status=null)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_duplicados as b "
               . " where 1=1 ";
        if($status==null)
        {
            $sql .= " and status='0'";
        }elseif($status==1){
            $sql .= " and status='1'";
        }
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function selecionaNotLog($status=null)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_notlog as b "
               . " where 1=1 ";
        if($status==null)
        {
            $sql .= " and status='0'";
        }elseif($status==1){
            $sql .= " and status='1'";
        }    
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function selecionaLog($status=null)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_log as b "
               . " where 1=1 ";
        if($status==null)
        {
            $sql .= " and status='0'";
        }elseif($status==1){
            $sql .= " and status='1'";
        }
       //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function selecionaAgenda($id=null,$hoje=null)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_agenda as b "
               . " where 1=1 ";
        if($id!=null)
        {
            $sql .= " and idBaixaAutomaticaFuncoesUsuarioAgenda=:id";
        }    
        if($hoje!=null)
        {
            $sql .= " and DATE(dataAgendadaInicial) <= :hoje";
            $sql .= " and DATE(dataAgendadaFinal) >= :hoje";
        }    
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
        {
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($hoje!=null)
        {
            $sth->bindParam(':hoje', $hoje, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function cadastraEnvioEmailPendentesDuplicados() {
        
        $sql = "INSERT INTO baixaautomaticafuncoesusuario_email_pedentes_duplicados 
                                    (dataCadastro)
                                    VALUES (now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql); 

        if ($c->run($sth,true)) 
        {
            return true;
        } else {
            return false;
        }
    }
    
    public function selecionaEnvioEmailPendentesDuplicados($data=null)
    {
        $sql =" SELECT * FROM baixaAutomaticaFuncoesUsuario_email_pedentes_duplicados as b "
               . " where 1=1 ";
        if($data!=null)
        {
            $sql .= " and DATE(dataCadastro)=:data";
        }
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($data!=null)
        {
            $sth->bindParam(':data', $data, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>