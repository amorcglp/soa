<?php

require_once ("conexaoClass.php");

class ataPosse {

    private $idAtaPosse;
    private $fk_idOrganismoAfiliado;
    private $enderecoPosse;
    private $numeroPosse;
    private $bairroPosse;
    private $cidadePosse;
    private $dataPosse;
    private $horaInicioPosse;
    private $mestreRetirante;
    private $secretarioRetirante;
    private $presidenteJuntaDepositariaRetirante;
    private $secretarioJuntaDepositariaRetirante;
    private $tesoureiroJuntaDepositariaRetirante;
    private $guardiaoRetirante;
    //private $substituicaoPosse;
    private $nomeDirigiuPosse;
    private $cargoDirigiuPosse;
    private $detalhesAdicionaisPosse;
    private $caminhoAtaPosseAssinada;
    private $ultimoAtualizar;
    private $fk_idUsuario;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtaPosse() {
        return $this->idAtaPosse;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getEnderecoPosse() {
        return $this->enderecoPosse;
    }

    function getNumeroPosse() {
        return $this->numeroPosse;
    }

    function getBairroPosse() {
        return $this->bairroPosse;
    }

    function getCidadePosse() {
        return $this->cidadePosse;
    }

    function getDataPosse() {
        return $this->dataPosse;
    }

    function getHoraInicioPosse() {
        return $this->horaInicioPosse;
    }

    function getMestreRetirante() {
        return $this->mestreRetirante;
    }

    function getSecretarioRetirante() {
        return $this->secretarioRetirante;
    }

    function getPresidenteJuntaDepositariaRetirante() {
        return $this->presidenteJuntaDepositariaRetirante;
    }

    function getSecretarioJuntaDepositariaRetirante() {
        return $this->secretarioJuntaDepositariaRetirante;
    }

    function getTesoureiroJuntaDepositariaRetirante() {
        return $this->tesoureiroJuntaDepositariaRetirante;
    }

    function getGuardiaoRetirante() {
        return $this->guardiaoRetirante;
    }

    /*
      function getSubstituicaoPosse() {
      return $this->substituicaoPosse;
      }
     */

      function getNomeDirigiuPosse() {
        return $this->nomeDirigiuPosse;
    }

    function getCargoDirigiuPosse() {
        return $this->cargoDirigiuPosse;
    }

    function getDetalhesAdicionaisPosse() {
        return $this->detalhesAdicionaisPosse;
    }

    function getCaminhoAtaPosseAssinada() {
        return $this->caminhoAtaPosseAssinada;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function getFk_idUsuario() {
        return $this->fk_idUsuario;
    }

    function setIdAtaPosse($idAtaPosse) {
        $this->idAtaPosse = $idAtaPosse;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setEnderecoPosse($enderecoPosse) {
        $this->enderecoPosse = $enderecoPosse;
    }

    function setNumeroPosse($numeroPosse) {
        $this->numeroPosse = $numeroPosse;
    }

    function setBairroPosse($bairroPosse) {
        $this->bairroPosse = $bairroPosse;
    }

    function setCidadePosse($cidadePosse) {
        $this->cidadePosse = $cidadePosse;
    }

    function setDataPosse($dataPosse) {
        $this->dataPosse = $dataPosse;
    }

    function setHoraInicioPosse($horaInicioPosse) {
        $this->horaInicioPosse = $horaInicioPosse;
    }

    function setMestreRetirante($mestreRetirante) {
        $this->mestreRetirante = $mestreRetirante;
    }

    function setSecretarioRetirante($secretarioRetirante) {
        $this->secretarioRetirante = $secretarioRetirante;
    }

    function setPresidenteJuntaDepositariaRetirante($presidenteJuntaDepositariaRetirante) {
        $this->presidenteJuntaDepositariaRetirante = $presidenteJuntaDepositariaRetirante;
    }

    function setSecretarioJuntaDepositariaRetirante($secretarioJuntaDepositariaRetirante) {
        $this->secretarioJuntaDepositariaRetirante = $secretarioJuntaDepositariaRetirante;
    }

    function setTesoureiroJuntaDepositariaRetirante($tesoureiroJuntaDepositariaRetirante) {
        $this->tesoureiroJuntaDepositariaRetirante = $tesoureiroJuntaDepositariaRetirante;
    }

    function setGuardiaoRetirante($guardiaoRetirante) {
        $this->guardiaoRetirante = $guardiaoRetirante;
    }

    /*
      function setSubstituicaoPosse($substituicaoPosse) {
      $this->substituicaoPosse = $substituicaoPosse;
      }
     */

      function setNomeDirigiuPosse($nomeDirigiuPosse) {
        $this->nomeDirigiuPosse = $nomeDirigiuPosse;
    }

    function setCargoDirigiuPosse($cargoDirigiuPosse) {
        $this->cargoDirigiuPosse = $cargoDirigiuPosse;
    }

    function setDetalhesAdicionaisPosse($detalhesAdicionaisPosse) {
        $this->detalhesAdicionaisPosse = $detalhesAdicionaisPosse;
    }

    function setCaminhoAtaPosseAssinada($caminhoAtaPosseAssinada) {
        $this->caminhoAtaPosseAssinada = $caminhoAtaPosseAssinada;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    function setFk_idUsuario($fk_idUsuario) {
        $this->fk_idUsuario = $fk_idUsuario;
    }

    //--------------------------------------------------------------------------

    public function cadastroAtaPosse() {

        $sql = "INSERT INTO ataPosse
        (fk_idOrganismoAfiliado,
        enderecoPosse,
        numeroPosse,
        bairroPosse,
        cidadePosse,
        dataPosse,
        horaInicioPosse,
        mestreRetirante,
        secretarioRetirante,
        presidenteJuntaDepositariaRetirante,
        secretarioJuntaDepositariaRetirante,
        tesoureiroJuntaDepositariaRetirante,
        guardiaoRetirante,
        nomeDirigiuPosse,
        cargoDirigiuPosse,
        detalhesAdicionaisPosse,
        fk_idUsuario,
        data
        )
        VALUES (
        :fk_idOrganismoAfiliado,
        :enderecoPosse,
        :numeroPosse,
        :bairroPosse,
        :cidadePosse,
        :dataPosse,
        :horaInicioPosse,
        :mestreRetirante,
        :secretarioRetirante,
        :presidenteJuntaDepositariaRetirante,
        :secretarioJuntaDepositariaRetirante,
        :tesoureiroJuntaDepositariaRetirante,
        :guardiaoRetirante,
        :nomeDirigiuPosse,
        :cargoDirigiuPosse,
        :detalhesAdicionaisPosse,
        :fk_idUsuario,
        now()
        )";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':enderecoPosse', $this->enderecoPosse, PDO::PARAM_STR);
        $sth->bindParam(':numeroPosse', $this->numeroPosse, PDO::PARAM_STR);
        $sth->bindParam(':bairroPosse', $this->bairroPosse, PDO::PARAM_STR);
        $sth->bindParam(':cidadePosse', $this->cidadePosse, PDO::PARAM_STR);
        $sth->bindParam(':dataPosse', $this->dataPosse, PDO::PARAM_STR);
        $sth->bindParam(':horaInicioPosse', $this->horaInicioPosse, PDO::PARAM_STR);
        $sth->bindParam(':mestreRetirante', $this->mestreRetirante, PDO::PARAM_STR);
        $sth->bindParam(':secretarioRetirante', $this->secretarioRetirante, PDO::PARAM_STR);
        $sth->bindParam(':presidenteJuntaDepositariaRetirante', $this->presidenteJuntaDepositariaRetirante, PDO::PARAM_STR);
        $sth->bindParam(':secretarioJuntaDepositariaRetirante', $this->secretarioJuntaDepositariaRetirante, PDO::PARAM_STR);
        $sth->bindParam(':tesoureiroJuntaDepositariaRetirante', $this->tesoureiroJuntaDepositariaRetirante, PDO::PARAM_STR);
        $sth->bindParam(':guardiaoRetirante', $this->guardiaoRetirante, PDO::PARAM_STR);
        $sth->bindParam(':nomeDirigiuPosse', $this->nomeDirigiuPosse, PDO::PARAM_STR);
        $sth->bindParam(':cargoDirigiuPosse', $this->cargoDirigiuPosse, PDO::PARAM_STR);
        $sth->bindParam(':detalhesAdicionaisPosse', $this->detalhesAdicionaisPosse, PDO::PARAM_STR);
        $sth->bindParam(':fk_idUsuario', $this->fk_idUsuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            $ultimo_id = $this->selecionaUltimoIdCadastrado();
            return $ultimo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE ataPosse";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoIdCadastrado(){

        $sql= "SELECT `idAtaPosse` as id FROM `ataPosse` ORDER BY idAtaPosse DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['id'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function alteraAtaPosse($idAtaPosse) {

        $sql = "UPDATE ataPosse SET
        enderecoPosse = :enderecoPosse,
        numeroPosse = :numeroPosse,
        bairroPosse = :bairroPosse,
        cidadePosse = :cidadePosse,
        dataPosse = :dataPosse,
        horaInicioPosse = :horaInicioPosse,
        mestreRetirante = :mestreRetirante,
        secretarioRetirante = :secretarioRetirante,
        presidenteJuntaDepositariaRetirante = :presidenteJuntaDepositariaRetirante,
        secretarioJuntaDepositariaRetirante = :secretarioJuntaDepositariaRetirante,
        tesoureiroJuntaDepositariaRetirante = :tesoureiroJuntaDepositariaRetirante,
        guardiaoRetirante = :guardiaoRetirante,
        nomeDirigiuPosse = :nomeDirigiuPosse,
        cargoDirigiuPosse = :cargoDirigiuPosse,
        detalhesAdicionaisPosse = :detalhesAdicionaisPosse,
        ultimo_atualizar = :ultimo_atualizar
        WHERE idAtaPosse = :idAtaPosse";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':enderecoPosse', $this->enderecoPosse, PDO::PARAM_STR);
        $sth->bindParam(':numeroPosse', $this->numeroPosse, PDO::PARAM_STR);
        $sth->bindParam(':bairroPosse', $this->bairroPosse, PDO::PARAM_STR);
        $sth->bindParam(':cidadePosse', $this->cidadePosse, PDO::PARAM_STR);
        $sth->bindParam(':dataPosse', $this->dataPosse, PDO::PARAM_STR);
        $sth->bindParam(':horaInicioPosse', $this->horaInicioPosse, PDO::PARAM_STR);
        $sth->bindParam(':mestreRetirante', $this->mestreRetirante, PDO::PARAM_STR);
        $sth->bindParam(':secretarioRetirante', $this->secretarioRetirante, PDO::PARAM_STR);
        $sth->bindParam(':presidenteJuntaDepositariaRetirante', $this->presidenteJuntaDepositariaRetirante, PDO::PARAM_STR);
        $sth->bindParam(':secretarioJuntaDepositariaRetirante', $this->secretarioJuntaDepositariaRetirante, PDO::PARAM_STR);
        $sth->bindParam(':tesoureiroJuntaDepositariaRetirante', $this->tesoureiroJuntaDepositariaRetirante, PDO::PARAM_STR);
        $sth->bindParam(':guardiaoRetirante', $this->guardiaoRetirante, PDO::PARAM_STR);
        $sth->bindParam(':nomeDirigiuPosse', $this->nomeDirigiuPosse, PDO::PARAM_STR);
        $sth->bindParam(':cargoDirigiuPosse', $this->cargoDirigiuPosse, PDO::PARAM_STR);
        $sth->bindParam(':detalhesAdicionaisPosse', $this->detalhesAdicionaisPosse, PDO::PARAM_STR);
        $sth->bindParam(':ultimo_atualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':idAtaPosse', $idAtaPosse, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function atualizaCaminhoAtaPosseAssinada($idAtaPosse) {
        $sql = "UPDATE ataPosse SET
        caminhoAtaPosseAssinada =  :caminhoAtaPosseAssinada
        WHERE idAtaPosse = :idAtaPosse";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':enderecoPosse', $this->caminhoAtaPosseAssinada, PDO::PARAM_STR);
        $sth->bindParam(':idAtaPosse', $idAtaPosse, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtaPosse($idOA=null) {

        $sql = "SELECT * FROM ataPosse inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado where 1=1 ";

        if($idOA!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }
        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOA!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOA, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function verificaSeJaExiste() {

        $sql = "SELECT * FROM ataPosse where 1=1
        and fk_idOrganismoAfiliado =  :fk_idOrganismoAfiliado
        and enderecoPosse = :enderecoPosse
        and numeroPosse = :numeroPosse
        and bairroPosse = :bairroPosse
        and cidadePosse = :cidadePosse
        and dataPosse = :dataPosse
        and horaInicioPosse = :horaInicioPosse";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':enderecoPosse', $this->enderecoPosse, PDO::PARAM_STR);
        $sth->bindParam(':numeroPosse', $this->numeroPosse, PDO::PARAM_STR);
        $sth->bindParam(':bairroPosse', $this->bairroPosse, PDO::PARAM_STR);
        $sth->bindParam(':cidadePosse', $this->cidadePosse, PDO::PARAM_STR);
        $sth->bindParam(':dataPosse', $this->dataPosse, PDO::PARAM_STR);
        $sth->bindParam(':horaInicioPosse', $this->horaInicioPosse, PDO::PARAM_STR);

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscarIdAtaPosse($idAtaPosse) {
        
        $sql = "SELECT *,c.nome as cidade FROM  ataPosse as a
        inner join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
        left join estado as e on o.fk_idEstado = e.id
        left join cidade as c on o.fk_idCidade = c.id
        WHERE   idAtaPosse = :idAtaPosse";
        
        //echo $sql;
        
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaPosse', $idAtaPosse, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        //echo "<br>".$idAtaPosse;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeAtaPosse($idAtaPosse) {

        $sql = "DELETE FROM ataPosse WHERE idAtaPosse = :idAtaPosse";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtaPosse', $idAtaPosse, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>
