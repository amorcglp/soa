<?php

require_once 'conexaoClass.php';

class Impressao {
    private $idImpressao;
    private $nomeUsuario;
    private $dataHora;
    private $ipMaquina;
    private $codigoDeAfiliacao;
    private $fk_seqCadast;
    private $fk_idDocumento;
    
    public function getIdImpressao() {
        return $this->idImpressao;
    }
    
    public function getNomeUsuario() {
        return $this->nomeUsuario;
    }
    
    public function getDataHora() {
        $this->dataHora;
    }
    
    public function getIpMaquina() {
        return $this->ipMaquina;
    }
    
    public function getCodigoDeAfiliacao() {
        return $this->codigoDeAfiliacao;
    }
    
    public function getFk_seqCadast() {
        return $this->fk_seqCadast;
    }
    
    public function getFk_idDocumento() {
        return $this->fk_idDocumento;
    }
    
    public function setIdImpressao($idImpressao) {
        $this->idImpressao = $idImpressao;
    }
    
    public function setNomeUsuario($nomeUsuario) {
        $this->nomeUsuario = $nomeUsuario;
    }
    
    public function setDataHora($dataHora) {
        $this->dataHora = $dataHora;
    }
    
    public function setIpMaquina($ipMaquina) {
        $this->ipMaquina = $ipMaquina;
    }
    
    public function setCodigoDeAfiliacao($codigoDeAfiliacao) {
        $this->codigoDeAfiliacao = $codigoDeAfiliacao;
    }
    
    public function setFk_seqCadast($fk_seqCadast) {
        $this->fk_seqCadast = $fk_seqCadast;
    }
    
    public function setFk_idDocumento($fk_idDocumento) {
        $this->fk_idDocumento = $fk_idDocumento;
    }
    
    public function cadastraImpressao() {
        
        $sql = "INSERT INTO impressao "
                . " (nomeUsuario, dataHora,ipMaquina, codigoDeAfiliacao, "
                . " fk_seqCadast, fk_idDocumento)"
                . " VALUES "
                . " (:nome, "
                . " now(), "
                . " :ip, "
                . " :codigoDeAfiliacao, "
                . " :seqCadast, "
                . " :idDocumento)";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        /*
        echo "<br>==============";
        echo "<br>=>".$this->nomeUsuario;
        echo "<br>=>".$this->ipMaquina;
        echo "<br>=>".$this->codigoDeAfiliacao;
        echo "<br>=>".$this->fk_seqCadast;
        echo "<br>=>".$this->fk_idDocumento;
        echo "<br>==============";
        */
        //exit();
        $sth->bindParam(':nome', $this->nomeUsuario, PDO::PARAM_STR);
        $sth->bindParam(':ip', $this->ipMaquina, PDO::PARAM_STR);
        if($this->codigoDeAfiliacao!=0)
        {    
            $sth->bindParam(':codigoDeAfiliacao', $this->codigoDeAfiliacao, PDO::PARAM_INT);
        }else{
            $codigoNulo = 0;
            $sth->bindParam(':codigoDeAfiliacao', $codigoNulo , PDO::PARAM_INT);
        }
        $sth->bindParam(':seqCadast', $this->fk_seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':idDocumento', $this->fk_idDocumento, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function listaImpressao($siglaOA=null)
    {
        
        $sql = "SELECT * FROM impressao as i ";

        if($siglaOA!=null)
        {
            $sql .= " inner join usuario as u on seqCadast = fk_seqCadast
                        inner join organismoAfiliado on u.siglaOA = siglaOrganismoAfiliado
            ";
        }

        $sql .= " WHERE 1=1 ";

        if($siglaOA!=null)
        {
            $sql .= " and siglaOA = :siglaOA";
        }

        $sql .= " ORDER BY `dataHora` DESC";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($siglaOA!=null)
        {
            $sth->bindParam(':siglaOA', $siglaOA, PDO::PARAM_STR);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaImpressao() {
        
        $sql = "SELECT * FROM impressao WHERE idImpressao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id',  $this->idImpressao, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaNomeUsuarioImpressao() {
        
        $sql = "SELECT * FROM impressao WHERE nomeUsuario = :nomeUsuario";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nomeUsuario',  $this->nomeUsuario, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function buscaDocumentoImpresso() {
        
        $sql = "SELECT * FROM impressao WHERE fk_idDocumento = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id',  $this->fk_idDocumento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaCodigoDeAfiliacao() {
        
        $sql = "SELECT * FROM impressao WHERE codigoDeAfiliacao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id',  $this->codigoDeAfiliacao, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }    }
    
    public function buscaIpMaquina() 
    {
        
        $sql = "SELECT * FROM impressao WHERE ipMaquina = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id',  $this->ipMaquina, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function getUltimoId(){
        
        $sql = "SELECT MAX(`idImpressao`) as `maxId` from impressao";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listarTodasImpressao($discurso) {
        
        $sql = "SELECT * FROM `impressao` INNER JOIN `impressao_discurso`";
        $sql .= " ON `impressao`.`idImpressao` = `impressao_discurso`.`fk_idImpressao`";
        
        if($discurso) {
            $sql .= " JOIN `discurso` ON `discurso`.`idDiscurso` = `impressao_discurso`.`fk_idDiscurso`";
        }
        //echo "<h3>$sql</h3>";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}  

