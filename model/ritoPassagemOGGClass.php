<?php

require_once ("conexaoClass.php");

class ritoPassagemOGG {

    private $idRitoPassagemOGG;
    private $fk_idOrganismoAfiliado;
    private $rito;
    private $data;
    private $hora;
    private $comentario;
    private $seqCadastMestre;
    private $nomeMestre;
    private $usuario;
    private $ultimoAtualizar;
    
    function getIdRitoPassagemOGG() {
        return $this->idRitoPassagemOGG;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getRito() {
        return $this->rito;
    }

    function getData() {
        return $this->data;
    }

    function getHora() {
        return $this->hora;
    }

    function getComentario() {
        return $this->comentario;
    }

    function getSeqCadastMestre() {
        return $this->seqCadastMestre;
    }

    function getNomeMestre() {
        return $this->nomeMestre;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdRitoPassagemOGG($idRitoPassagemOGG) {
        $this->idRitoPassagemOGG = $idRitoPassagemOGG;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setRito($rito) {
        $this->rito = $rito;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setHora($hora) {
        $this->hora = $hora;
    }

    function setComentario($comentario) {
        $this->comentario = $comentario;
    }

    function setSeqCadastMestre($seqCadastMestre) {
        $this->seqCadastMestre = $seqCadastMestre;
    }

    function setNomeMestre($nomeMestre) {
        $this->nomeMestre = $nomeMestre;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

        
    public function cadastro(){

        $sql = "INSERT INTO ritoPassagemOGG 
            (fk_idOrganismoAfiliado, rito, data, hora, comentario, seqCadastMestre, nomeMestre, usuario,dataCadastro) 
        VALUES (
        :fk_idOrganismoAfiliado,
        :rito,
        :data,
        :hora,
        :comentario,
        :seqCadastMestre,
        :nomeMestre,
        :usuario,
        now()
        )";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':rito', $this->rito, PDO::PARAM_INT);
        $sth->bindParam(':data', $this->data, PDO::PARAM_STR);
        $sth->bindParam(':hora', $this->hora, PDO::PARAM_STR);
        $sth->bindParam(':comentario', $this->comentario, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastMestre', $this->seqCadastMestre, PDO::PARAM_INT);
        $sth->bindParam(':nomeMestre', $this->nomeMestre, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            $ultimo_id = $this->selecionaUltimoIdCadastrado();
            return $ultimo_id;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'ritoPassagemOGG'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoIdCadastrado(){

        $sql= "SELECT `idRitoPassagemOGG` as id FROM `ritoPassagemOGG` ORDER BY idRitoPassagemOGG DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['id'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }
        
    # Seleciona Evento
    public function lista($id=null,$idOrganismoAfiliado=null,$mes=null,$ano=null,$rito=null,$menosRito=null) 
    {
        $sql = "SELECT * FROM ritoPassagemOGG where 1=1 ";
        if($id!=null)
        {
                $sql .= " and idRitoPassagemOGG=:id";
        }
        if($idOrganismoAfiliado!=null)
        {
                $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }
        if($mes!=null)
        {
                $sql .= " and MONTH(data)=:mes";
        }
        if($ano!=null)
        {
                $sql .= " and YEAR(data)=:ano";
        }
        if($rito!=null)
        {
                $sql .= " and rito=:rito";
        }
        if($menosRito!=null)
        {
                $sql .= " and rito<>:menosRito";
        }
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
        {
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($mes!=null)
        {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }
        if($ano!=null)
        {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($rito!=null)
        {
            $sth->bindParam(':rito', $rito, PDO::PARAM_INT);
        }
        if($menosRito!=null)
        {
            $sth->bindParam(':menosRito', $menosRito, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
                return $resultado;
        }else{
                return false;
        }
    }
    
    public function remove($id=null){

        $sql= "DELETE FROM ritoPassagemOGG";

    	if($id!=null) {
    		$sql .= " where idRitoPassagemOGG=:fk_idRitoPassagemOGG";
    	}
    	
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($id!=null){
            $sth->bindParam(':fk_idRitoPassagemOGG', $id, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function altera(){

        $sql = "UPDATE ritoPassagemOGG SET
            fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado, 
            rito=:rito,
            data=:data,
            hora= :hora,
            comentario=:comentario,
            seqCadastMestre= :seqCadastMestre,
            nomeMestre= :nomeMestre,
            ultimoAtualizar=:alteradoPorUsuario
            where
            idRitoPassagemOGG = :id
        ";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':rito', $this->rito, PDO::PARAM_INT);
        $sth->bindParam(':data', $this->data, PDO::PARAM_STR);
        $sth->bindParam(':hora', $this->hora, PDO::PARAM_STR);
        $sth->bindParam(':comentario', $this->comentario, PDO::PARAM_STR);
        $sth->bindParam(':seqCadastMestre', $this->seqCadastMestre, PDO::PARAM_INT);
        $sth->bindParam(':nomeMestre', $this->nomeMestre, PDO::PARAM_STR);
        $sth->bindParam(':alteradoPorUsuario', $this->ultimoAtualizar, PDO::PARAM_STR);
        $sth->bindParam(':id', $this->idRitoPassagemOGG, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>