<?php

require_once ("conexaoClass.php");

class auxiliarWebAssinado{

    private $idAuxiliarWebAssinado;
    private $fk_idAuxiliarWeb;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdAuxiliarWebAssinado() {
        return $this->idAuxiliarWebAssinado;
    }

    function getFkIdAuxiliarWeb() {
        return $this->fk_idAuxiliarWeb;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdAuxiliarWebAssinado($idAuxiliarWebAssinado) {
        $this->idAuxiliarWebAssinado = $idAuxiliarWebAssinado;
    }

    function setFkIdAuxiliarWeb($fk_idAuxiliarWeb) {
        $this->fk_idAuxiliarWeb = $fk_idAuxiliarWeb;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    //--------------------------------------------------------------------------

    public function cadastroAuxiliarWebAssinado(){

        $sql="INSERT INTO auxiliarWebAssinado  
                                 (
                                  fk_idAuxiliarWeb,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idAuxiliarWeb,
                                    :caminho,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFkIdAuxiliarWeb       = $this->getFkIdAuxiliarWeb();
        $getCaminho                     = $this->getCaminho();

        $sth->bindParam(':idAuxiliarWeb', $getFkIdAuxiliarWeb, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $getCaminho, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'auxiliarWebAssinado'";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                    $proximo_id = $vetor['Auto_increment']; 
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaAuxiliarWebAssinado($id=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM auxiliarWebAssinado
    	inner join auxiliarWeb on fk_idAuxiliarWeb = idAuxiliarWeb
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idAuxiliarWeb = :id";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
    	//echo "<br><br>".$sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeAuxiliarWebAssinado($idAuxiliarWebAssinado){
        
        $sql="DELETE FROM auxiliarWebAssinado WHERE idAuxiliarWebAssinado = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idAuxiliarWebAssinado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
