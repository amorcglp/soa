<?php

require_once ("conexaoClass.php");

class planoAcaoRegiaoMeta{

    private $idPlanoAcaoRegiaoMeta;
    private $fk_idPlanoAcaoRegiao;
    private $seqCadast;
    private $statusMeta;
    private $tituloMeta;
    private $inicio;
    private $fim;
    private $oque;
    private $porque;
    private $quem;
    private $onde;
    private $quando;
    private $como;
    private $quanto;
    private $ordenacao;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoAcaoRegiaoMeta() {
        return $this->idPlanoAcaoRegiaoMeta;
    }

    function getFk_idPlanoAcaoRegiao() {
        return $this->fk_idPlanoAcaoRegiao;
    }
    
	function getSeqCadast() {
        return $this->seqCadast;
    }
    
	function getStatusMeta() {
        return $this->statusMeta;
    }

    function getTituloMeta() {
        return $this->tituloMeta;
    } 
    
    function getInicio() {
        return $this->inicio;
    } 
    
    function getFim() {
        return $this->fim;
    } 
    
    function getOque() {
        return $this->oque;
    } 
    
    function getPorque() {
        return $this->porque;
    } 
    
    function getQuem() {
        return $this->quem;
    } 
    
    function getOnde() {
        return $this->onde;
    } 
    
    function getQuando() {
        return $this->quando;
    } 
    
    function getComo() {
        return $this->como;
    } 
    
    function getQuanto() {
        return $this->quanto;
    } 
    
    function getOrdenacao()
    {
    	return $this->ordenacao;
    }

    function setIdPlanoAcaoRegiaoMeta($idPlanoAcaoRegiaoMeta) {
        $this->idPlanoAcaoRegiaoMeta = $idPlanoAcaoRegiaoMeta;
    }
  
	function setFk_idPlanoAcaoRegiao($fk_idPlanoAcaoRegiao) {
        $this->fk_idPlanoAcaoRegiao = $fk_idPlanoAcaoRegiao;
    }
    
	function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }
    
	function setStatusMeta($statusMeta) {
        $this->statusMeta = $statusMeta;
    }
    
	function setTituloMeta($tituloMeta) {
        $this->tituloMeta = $tituloMeta;
    }
    
	function setInicio($inicio) {
        $this->inicio = $inicio;
    }
    
	function setFim($fim) {
        $this->fim = $fim;
    }
    
	function setOque($oque) {
        $this->oque = $oque;
    }
    
	function setPorque($porque) {
        $this->porque = $porque;
    }
    
	function setQuem($quem) {
        $this->quem = $quem;
    }
    
	function setOnde($onde) {
        $this->onde = $onde;
    }
    
	function setQuando($quando) {
        $this->quando = $quando;
    }
    
	function setComo($como) {
        $this->como = $como;
    }
    
	function setQuanto($quanto) {
        $this->quanto = $quanto;
    }
    
	function setOrdenacao($ordenacao) {
        $this->ordenacao = $ordenacao;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoAcaoRegiaoMeta(){

        $sql="INSERT INTO planoAcaoRegiao_meta  
                                 (
                                  fk_idPlanoAcaoRegiao,
                                  seqCadast,
                                  statusMeta,
                                  tituloMeta,
                                  inicio,
                                  fim,
                                  oque,
                                  porque,
                                  quem,
                                  onde,
                                  quando,
                                  como,
                                  quanto,
                                  ordenacao,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idPlanoAcaoRegiao,
                                    :seqCadast,
                                    :status,
                                    :titulo,
                                    :inicio,
                                    :fim,
                                    :oque,
                                    :porque,
                                    :quem,
                                    :onde,
                                    :quando,
                                    :como,
                                    :quanto,
                                    :ordenacao,
                                    now()
                                    )";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoRegiao',$this->fk_idPlanoAcaoRegiao , PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':status',  $this->statusMeta, PDO::PARAM_INT);
        $sth->bindParam(':titulo', $this->tituloMeta, PDO::PARAM_STR);
        $sth->bindParam(':inicio', $this->inicio, PDO::PARAM_STR);
        $sth->bindParam(':fim',  $this->fim, PDO::PARAM_STR);
        $sth->bindParam(':oque', $this->oque, PDO::PARAM_STR);
        $sth->bindParam(':porque', $this->porque, PDO::PARAM_STR);
        $sth->bindParam(':quem', $this->quem, PDO::PARAM_STR);
        $sth->bindParam(':onde',  $this->onde, PDO::PARAM_STR);
        $sth->bindParam(':quando', $this->quando, PDO::PARAM_STR);
        $sth->bindParam(':como', $this->como, PDO::PARAM_STR);
        $sth->bindParam(':quanto',  $this->quanto, PDO::PARAM_STR);
        $sth->bindParam(':ordenacao', $this->ordenacao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusMeta() {
        
	$sql = "UPDATE planoAcaoRegiao_meta SET `statusMeta` = :status
                               WHERE `idPlanoAcaoRegiaoMeta` = :id";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status',$this->statusMeta, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idPlanoAcaoRegiaoMeta, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusMeta();
            return $arr;
        } else {
            return false;
        }
    }
   
    
    public function listaMeta($idPlanoAcaoRegiao=null,$statusMeta=null,$id=null){

    	$sql = "SELECT * FROM planoAcaoRegiao_meta
    	where 1=1 
    	 ";
    	if($id!=null)
    	{
    		$sql .= " and idPlanoAcaoRegiaoMeta=:id";
    	}   
    	if($idPlanoAcaoRegiao!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoRegiao=:idPlanoAcaoRegiao";
    	}
    	if($statusMeta!=null)
    	{
    		$sql .= " and statusMeta=:status";
    	}	 
    	$sql .= " order by ordenacao asc";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($idPlanoAcaoRegiao!=null)
    	{
            $sth->bindParam(':idPlanoAcaoRegiao', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        }
        if($statusMeta!=null)
    	{
            $sth->bindParam(':status', $statusMeta, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaMetaRegiao($regiao=null,$statusMeta=0,$ano=null,$statusNaoMeta=0){
        
		//echo "statusmeta=>".$statusMeta."<br><br>";
    	$sql = "SELECT * FROM planoAcaoRegiao_meta
    	left join planoAcaoRegiao on fk_idPlanoAcaoRegiao = idPlanoAcaoRegiao 
    	
    	where 1=1 
    	 "; 
    	if($regiao!=null)
    	{
    		$sql .= " and regiao=:regiao";
    	}
    	if($statusMeta!=null)
    	{
    		$sql .= " and statusMeta=:status";
    	}else{
			if($statusNaoMeta!=null)
	    	{
	    		$sql .= " and statusMeta='0'";
	    	}
    	}
	if($ano!=null)
    	{
    		$sql .= " and YEAR(inicio)=:ano";
    	}	 
    	$sql .= " and statusPlano='1' group by idPlanoAcaoRegiaoMeta";
    	//echo $sql."<br><br>";
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($regiao!=null)
    	{
            $sth->bindParam(':regiao', $regiao, PDO::PARAM_INT);
        }
        if($statusMeta!=null)
    	{
            $sth->bindParam(':status', $statusMeta, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaUltimaMeta($idPlanoAcaoRegiao){

    	$sql = "SELECT * FROM planoAcaoRegiao_meta
    	where 1=1 
    	 ";  
        
    	if($idPlanoAcaoRegiao!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoRegiao=:id";
    	}	 
    	$sql .= " order by dataCadastro desc limit 1";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idPlanoAcaoRegiao!=null)
    	{
            $sth->bindParam(':id', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
        	foreach($resultado as $vetor)
        	{
        		$ultimaMeta = $vetor['dataCadastro'];
        	}
            return $ultimaMeta;
        }else{
            return "";
        }
    }

    public function removeMeta(){
        $sql="DELETE FROM planoAcaoRegiao_meta WHERE idPlanoAcaoRegiaoMeta = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idPlanoAcaoRegiaoMeta, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    } 

    public function removeMetas($idPlanoAcaoRegiao){
        
        $sql="DELETE FROM planoAcaoRegiao_meta WHERE fk_idPlanoAcaoRegiao = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
