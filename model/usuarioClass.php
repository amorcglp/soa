<?php
require_once ("conexaoClass.php");

class Usuario {

    public $seqCadast;
    public $nomeUsuario;
    public $emailUsuario;
    public $telefoneResidencialUsuario;
    public $telefoneComercialUsuario;
    public $celularUsuario;
    public $ramalUsuario;
    public $loginUsuario;
    public $senhaUsuario;
    public $dataCadastroUsuario;
    public $codigoDeAfiliacaoUsuario;
    public $statusUsuario;
    public $siglaOA;
    public $fk_idDepartamento;
    public $fk_idRegiaoRosacruz;
    public $tokenFacebook;

    /* Funções GET e SET */

    function getSeqCadast() {
        return $this->seqCadast;
    }

    function getNomeUsuario() {
        return $this->nomeUsuario;
    }

    function getEmailUsuario() {
        return $this->emailUsuario;
    }

    function getTelefoneResidencialUsuario() {
        return $this->telefoneResidencialUsuario;
    }
    
	function getTelefoneComercialUsuario() {
        return $this->telefoneComercialUsuario;
    }

    function getCelularUsuario() {
        return $this->celularUsuario;
    }

    function getRamalUsuario() {
        return $this->ramalUsuario;
    }

    function getLoginUsuario() {
        return $this->loginUsuario;
    }

    function getSenhaUsuario() {
        return $this->senhaUsuario;
    }

    function getDataCadastroUsuario() {
        return $this->dataCadastroUsuario;
    }

    function getCodigoDeAfiliacaoUsuario() {
        return $this->codigoDeAfiliacaoUsuario;
    }

    function getStatusUsuario() {
        return $this->statusUsuario;
    }

	function getSiglaOA() {
        return $this->siglaOA;
    }
    
    function getFk_idDepartamento() {
        return $this->fk_idDepartamento;
    }

    function getFk_idRegiaoRosacruz() {
        return $this->fk_idRegiaoRosacruz;
    }
    
    function getTokenFacebook() {
        return $this->tokenFacebook;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    function setNomeUsuario($nomeUsuario) {
        $this->nomeUsuario = $nomeUsuario;
    }

    function setEmailUsuario($emailUsuario) {
        $this->emailUsuario = $emailUsuario;
    }

    function setTelefoneResidencialUsuario($telefoneResidencialUsuario) {
        $this->telefoneResidencialUsuario = $telefoneResidencialUsuario;
    }
    
	function setTelefoneComercialUsuario($telefoneComercialUsuario) {
        $this->telefoneComercialUsuario = $telefoneComercialUsuario;
    }

    function setCelularUsuario($celularUsuario) {
        $this->celularUsuario = $celularUsuario;
    }

    function setRamalUsuario($ramalUsuario) {
        $this->ramalUsuario = $ramalUsuario;
    }

    function setLoginUsuario($loginUsuario) {
        $this->loginUsuario = $loginUsuario;
    }

    function setSenhaUsuario($senhaUsuario) {
        $this->senhaUsuario = $senhaUsuario;
    }

    function setDataCadastroUsuario($dataCadastroUsuario) {
        $this->dataCadastroUsuario = $dataCadastroUsuario;
    }

    function setCodigoDeAfiliacaoUsuario($codigoDeAfiliacaoUsuario) {
        $this->codigoDeAfiliacaoUsuario = $codigoDeAfiliacaoUsuario;
    }

    function setStatusUsuario($statusUsuario) {
        $this->statusUsuario = $statusUsuario;
    }
    
	function setSiglaOA($siglaOA) {
        $this->siglaOA = $siglaOA;
    }

    function setFk_idDepartamento($fk_idDepartamento) {
        $this->fk_idDepartamento = $fk_idDepartamento;
    }

    function setFk_idRegiaoRosacruz($fk_idRegiaoRosacruz) {
        $this->fk_idRegiaoRosacruz = $fk_idRegiaoRosacruz;
    }
    
    function setTokenFacebook($tokenFacebook) {
        $this->tokenFacebook = $tokenFacebook;
    }

    public function cadastraUsuario() {
        
    	$sql = "INSERT INTO usuario (
                                    seqCadast, 
                                    nomeUsuario, 
                                    emailUsuario, 
                                    telefoneResidencialUsuario, 
                                    telefoneComercialUsuario, 
                                    celularUsuario, 
                                    ramalUsuario, 
                                    loginUsuario, 
                                    senhaUsuario, 
                                    dataCadastroUsuario, 
                                    codigoDeAfiliacao, 
                                    statusUsuario, 
                                    fk_idDepartamento,
                                    fk_idRegiaoRosacruz, 
                                    siglaOA)
                                    VALUES (
                                           :seqCadast,
                                           :nomeUsuario,
                                           :emailUsuario,
                                           :telefoneResidencialUsuario,
                                           :telefoneComercialUsuario,
                                           :celularUsuario,
                                           :ramalUsuario,
                                           :loginUsuario,
                                           :senhaUsuario,
                                           now(),
                                           :codigoDeAfiliacao,
                                           :statusUsuario,
                                           :fk_idDepartamento,
                                           :fk_idRegiaoRosacruz,
                                           :siglaOA
                                            )";
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getSeqCadast                       = $this->getSeqCadast();
        $getNomeUsuario                     = $this->getNomeUsuario();
        $getEmailUsuario                    = $this->getEmailUsuario();
        $getTelefoneResidencialUsuario      = $this->getTelefoneResidencialUsuario();
        $getTelefoneComercialUsuario        = $this->getTelefoneComercialUsuario();
        $getCelularUsuario                  = $this->getCelularUsuario();
        $getRamalUsuario                    = $this->getRamalUsuario();
        $getLoginUsuario                    = $this->getLoginUsuario();
        $getSenhaUsuario                    = $this->getSenhaUsuario();
        $getCodigoDeAfiliacaoUsuario        = $this->getCodigoDeAfiliacaoUsuario();
        $getStatusUsuario                   = $this->getStatusUsuario();
        $getFk_idDepartamento               = $this->getFk_idDepartamento();
        $getFk_idRegiaoRosacruz             = $this->getFk_idRegiaoRosacruz();
        $getSiglaOA                         = $this->getSiglaOA();

        $sth->bindParam(':seqCadast', $getSeqCadast, PDO::PARAM_INT);
        $sth->bindParam(':nomeUsuario', $getNomeUsuario, PDO::PARAM_STR);
        $sth->bindParam(':emailUsuario', $getEmailUsuario, PDO::PARAM_STR);
        $sth->bindParam(':telefoneResidencialUsuario', $getTelefoneResidencialUsuario, PDO::PARAM_STR);
        $sth->bindParam(':telefoneComercialUsuario', $getTelefoneComercialUsuario, PDO::PARAM_STR);
        $sth->bindParam(':celularUsuario', $getCelularUsuario, PDO::PARAM_STR);
        $sth->bindParam(':ramalUsuario', $getRamalUsuario, PDO::PARAM_INT);
        $sth->bindParam(':loginUsuario', $getLoginUsuario, PDO::PARAM_STR);
        $sth->bindParam(':senhaUsuario', $getSenhaUsuario, PDO::PARAM_STR);
        $sth->bindParam(':codigoDeAfiliacao', $getCodigoDeAfiliacaoUsuario, PDO::PARAM_INT);
        $sth->bindParam(':statusUsuario', $getStatusUsuario, PDO::PARAM_INT);
        $sth->bindParam(':fk_idDepartamento', $getFk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':fk_idRegiaoRosacruz', $getFk_idRegiaoRosacruz, PDO::PARAM_INT);
        $sth->bindParam(':siglaOA', $getSiglaOA, PDO::PARAM_STR);

        /*
        $data = array(
            'seqCadast'=>$getSeqCadast,
            'nomeUsuario'=>$getNomeUsuario,
            'emailUsuario'=>$getEmailUsuario,
            'telefoneResidencialUsuario'=>$getTelefoneResidencialUsuario,
            'telefoneComercialUsuario'=>$getTelefoneComercialUsuario,
            'celularUsuario'=>$getCelularUsuario,
            'ramalUsuario'=>$getRamalUsuario,
            'loginUsuario'=>$getLoginUsuario,
            'senhaUsuario'=>$getSenhaUsuario,
            'codigoDeAfiliacao'=>$getCodigoDeAfiliacaoUsuario,
            'statusUsuario'=>$getStatusUsuario,
            'fk_idDepartamento'=>$getFk_idDepartamento,
            'fk_idRegiaoRosacruz'=>$getFk_idRegiaoRosacruz,
            'siglaOA'=>$getSiglaOA
        );

        echo $this->parms("cadastraUsuario",$sql,$data);
        */
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function alteraUsuario($id) {

	$sql = "UPDATE usuario SET `nomeUsuario`=:nomeUsuario,
                                                 emailUsuario = :emailUsuario,
                                                 telefoneResidencialUsuario = :telefoneResidencialUsuario,
                                                 telefoneComercialUsuario = :telefoneComercialUsuario,
                                                 celularUsuario = :celularUsuario,
                                                 loginUsuario = :loginUsuario,
                                                 senhaUsuario = :senhaUsuario,
                                                 fk_idDepartamento = :fk_idDepartamento
                            WHERE seqCadast = :seqCadast";
                
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getNomeUsuario                     = $this->getNomeUsuario();
        $getEmailUsuario                    = $this->getEmailUsuario();
        $getTelefoneResidencialUsuario      = $this->getTelefoneResidencialUsuario();
        $getTelefoneComercialUsuario        = $this->getTelefoneComercialUsuario();
        $getCelularUsuario                  = $this->getCelularUsuario();
        $getLoginUsuario                    = $this->getLoginUsuario();
        $getSenhaUsuario                    = $this->getSenhaUsuario();
        $getFk_idDepartamento               = $this->getFk_idDepartamento();

        $sth->bindParam(':nomeUsuario', $getNomeUsuario, PDO::PARAM_STR);
        $sth->bindParam(':emailUsuario', $getEmailUsuario, PDO::PARAM_STR);
        $sth->bindParam(':telefoneResidencialUsuario', $getTelefoneResidencialUsuario, PDO::PARAM_STR);
        $sth->bindParam(':telefoneComercialUsuario', $getTelefoneComercialUsuario, PDO::PARAM_STR);
        $sth->bindParam(':celularUsuario', $getCelularUsuario, PDO::PARAM_STR);
        $sth->bindParam(':loginUsuario', $getLoginUsuario, PDO::PARAM_STR);
        $sth->bindParam(':senhaUsuario', $getSenhaUsuario, PDO::PARAM_STR);
        $sth->bindParam(':fk_idDepartamento', $getFk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $id, PDO::PARAM_INT);

        if ($c->run($sth,true)){

            return true;
        } else {

            return false;
        }
    }

    public function listaUsuario($siglaOrganismo=null,$departamentos=null,$funcoesVinculosExternos=null,$idRegiaoRosacruz=null,$groupBy=null,$orderBy=null,$ativo=null,$search=null,$idRegiaoRosacruzOrganismo=null,$senhaNaoCriptografada=null,$start=null,$limit=null,$menosEstesUsuarios=null,$oaAtivo=null,$letraLogin=null,$letraNome=null) {
                                                                                
        
        
    	$sql = "SELECT *,u.seqCadast as seqCadast,u.seqCadast as idUsuario, u.dataCadastroUsuario as dataUsuarioCad FROM usuario as u
    	left join departamento as d on fk_idDepartamento = idDepartamento
    	left join organismoAfiliado as o on siglaOA = siglaOrganismoAfiliado ";
        
    	if ($funcoesVinculosExternos != null) {
    		$sql .= " inner join funcao_usuario as fu on fk_seq_cadast = u.seqCadast
    		inner join funcao as f on fk_idFuncao = idFuncao";
    	}
        
    	$sql .= " WHERE 1=1 ";

        if ($letraNome != null) {
            $sql .= " and u.nomeUsuario like :letraNome";
        }

        if ($letraLogin != null) {
            $sql .= " and u.loginUsuario like :letraLogin";
        }
    	
    	if ($siglaOrganismo != null) {
			$sql .= " and u.siglaOA = :siglaOrganismo";
		}
		
    	if ($idRegiaoRosacruz != null) {
			$sql .= " and u.fk_idRegiaoRosacruz = :idRegiaoRosacruz";
		}
	if ($idRegiaoRosacruzOrganismo != null) {
			$sql .= " and o.fk_idRegiaoOrdemRosacruz = :idRegiaoRosacruzOrganismo";
		}	
    	if ($departamentos != null) {
            $ids = explode(",", $departamentos);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {    
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and fk_idDepartamento IN(".$inQuery.")";
	}
		
        if ($funcoesVinculosExternos != null) {
            $ids2 = explode(",", $funcoesVinculosExternos);
            for($i=0;$i<count($ids2);$i++)
            {
                if($i==0)
                {    
                    $inQuery2 = ":".$i;
                }else{
                    $inQuery2 .= ",:".$i;
                }
            }
            $sql .= " and f.vinculoExterno IN(".$inQuery2.")";
        }
        
        if ($menosEstesUsuarios != null) {
            
            $ids3 = explode(",", $menosEstesUsuarios);
            //echo "<pre>";print_r($ids3);
            for($i=0;$i<count($ids3);$i++)
            {
                if($i==0)
                {    
                    $inQuery3 = ":".$i;
                }else{
                    $inQuery3 .= ",:".$i;
                }
            }
            $sql .= " and u.seqCadast NOT IN(".$inQuery3.")";
	}
		
    	if ($ativo != null) {
			$sql .= " and u.statusUsuario='0'";
		}
		
    	if ($search != null) {
			$sql .= " and (
				u.nomeUsuario like :searchUm 
				or o.nomeOrganismoAfiliado like :searchDois
				) ";
		}
        if ($senhaNaoCriptografada != null) {
			$sql .= " and u.senhaCriptografada = '0'";
		}
	if($oaAtivo!=null)
    	{
    		$sql .= " and o.statusOrganismoAfiliado != 1 ";
    	}
                
		
        if ($groupBy != null) {
                    $sql .= " group by ".$groupBy;
            }
		
    	if ($orderBy != null) {
			$sql .= " order by ".$orderBy;
		}
        if ($limit != null) {
			$sql .= " limit :start, :limit";
		}
        /*     
        echo "<br>====================================";        
        echo "<br>siglaOrganismo:".$siglaOrganismo;
        echo "<br>departamentos:".$departamentos;
        echo "<br>funcoesVinculosExternos:".$funcoesVinculosExternos;
        echo "<br>idRegiaoRosacruz:".$idRegiaoRosacruz;
        echo "<br>groupBy:".$groupBy;
        echo "<br>orderBy:".$orderBy;
        echo "<br>ativo:".$ativo;
        echo "<br>search:".$search;
        echo "<br>idRegiaoRosacruzOrganismo:".$idRegiaoRosacruzOrganismo; 
        echo "<br>senhaNaoCriptografada:".$senhaNaoCriptografada;
        echo "<br>start:".$start;
        echo "<br>limit:".$limit;
        echo "<br>menosEstesUsuarios:".$menosEstesUsuarios;
        echo "<br>====================================";
        */
        //echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($siglaOrganismo != null) {
            //echo "1";
            $sth->bindParam(':siglaOrganismo', $siglaOrganismo, PDO::PARAM_STR);
        }
        if ($idRegiaoRosacruz != null) {
            //echo "2";
            $sth->bindParam(':idRegiaoRosacruz', $idRegiaoRosacruz, PDO::PARAM_INT);
        }
        if ($idRegiaoRosacruzOrganismo != null) {
            //echo "3";
            $sth->bindParam(':idRegiaoRosacruzOrganismo', $idRegiaoRosacruzOrganismo, PDO::PARAM_INT);
        }
        if($departamentos!=null)
    	{
            //echo "4";
            foreach ($ids as $u => $id)
            {    
                 $sth->bindValue(":".$u, $id);
            }
        }
        if($funcoesVinculosExternos!=null)
    	{
            //echo "5";
            foreach ($ids2 as $k => $id)
            {    
                $sth->bindValue(":".$k, $id);
            }
        }
        if($menosEstesUsuarios!=null)
    	{
            //echo "6";
            foreach ($ids3 as $m => $id)
            {    
                $sth->bindValue(":".$m, $id);
            }
        }
        
        if ($search != null) {
            $search = '%'.$search.'%';
            //echo "7";
            $sth->bindParam(':searchUm', $search, PDO::PARAM_STR);
            $sth->bindParam(':searchDois', $search, PDO::PARAM_STR);
        }
        /*
        if ($groupBy != null) {
            //echo "8";
            $sth->bindValue(':groupBy', (string) $groupBy, PDO::PARAM_STR);
        }
        if ($orderBy != null) {
            //echo "9";
            $sth->bindValue(':orderBy', (string) $orderBy, PDO::PARAM_STR);
        }
        */
        if ($start != null) {
            //echo "10";
            $sth->bindValue(':start', (int) trim($start), PDO::PARAM_INT);
        }
         
        if ($limit != null) {
            //echo "11";
            $sth->bindValue(':limit', (int) trim($limit), PDO::PARAM_INT);
        }

        if ($letraLogin != null) {
            //echo "1";
            $l=$letraLogin."%";
            $sth->bindParam(':letraLogin', $l, PDO::PARAM_STR);
        }
        if ($letraNome != null) {
            //echo "1";
            $ln=$letraNome."%";
            $sth->bindParam(':letraNome', $ln, PDO::PARAM_STR);
        }
        //echo '==>'.$inQuery2;
        $data = array(
            'idRegiaoRosacruz'=>$idRegiaoRosacruz,
            'funcoesVinculosExternos'=>$funcoesVinculosExternos,
            'limit'=>$limit,
            'start'=>$start   
        );
        if($funcoesVinculosExternos!=null)
            {
                //echo "5";
                foreach ($ids2 as $k => $id)
                {    
                    $data[$k] = $id;
                }
            }
        //echo "QUERY: " . $this->parms("listaUsuario",$sql,$data) . "\n";
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
         
    }
    
    public function listaUsuarioAux() {
        
        
    	$sql = "SELECT *,u.seqCadast as seqCadast,u.seqCadast as idUsuario FROM usuario_aux as u
    	inner join departamento as d on fk_idDepartamento = idDepartamento
    	inner join organismoAfiliado as o on siglaOA = siglaOrganismoAfiliado
        limit 1400,200";	
	
        //echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
         
    }
    
    public function listaUsuarioForChangePassword($siglaOrganismo=null,$departamentos=null,$funcoesVinculosExternos=null,$idRegiaoRosacruz=null,$groupBy=null,$orderBy=null,$ativo=null,$search=null,$idRegiaoRosacruzOrganismo=null,$senhaNaoCriptografada=null) {
        
        
    	$sql = "SELECT *,u.seqCadast as seqCadast,u.seqCadast as idUsuario FROM usuario as u";
        
    	if ($funcoesVinculosExternos != null) {
    		$sql .= " inner join funcao_usuario as fu on fk_seq_cadast = u.seqCadast
    		inner join funcao as f on fk_idFuncao = idFuncao";
    	}
        
    	$sql .= " WHERE 1=1 ";
    	
    	if ($siglaOrganismo != null) {
			$sql .= " and u.siglaOA = :siglaOrganismo";
		}
		
    	if ($idRegiaoRosacruz != null) {
			$sql .= " and u.fk_idRegiaoRosacruz = :idRegiaoRosacruz";
		}
	if ($idRegiaoRosacruzOrganismo != null) {
			$sql .= " and o.fk_idRegiaoOrdemRosacruz = :idRegiaoRosacruzOrganismo";
		}	
    	if ($departamentos != null) {
            $ids = explode(",", $departamentos);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {    
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and fk_idDepartamento IN(".$inQuery.")";
	}
		
        if ($funcoesVinculosExternos != null) {
            $ids2 = explode(",", $funcoesVinculosExternos);
            for($i=0;$i<count($ids2);$i++)
            {
                if($i==0)
                {    
                    $inQuery2 = ":".$i;
                }else{
                    $inQuery2 .= ",:".$i;
                }
            }
            $sql .= " and f.vinculoExterno IN(".$inQuery2.")";
        }
		
    	if ($ativo != null) {
			$sql .= " and u.statusUsuario=0";
		}
		
    	if ($search != null) {
			$sql .= " and (
				u.nomeUsuario like :searchUm 
				or o.nomeOrganismoAfiliado like :searchDois
				) ";
		}
        if ($senhaNaoCriptografada != null) {
			$sql .= " and u.senhaCriptografada = '0'";
		}
	
                
		
        if ($groupBy != null) {
                    $sql .= " group by :groupBy";
            }
		
    	if ($orderBy != null) {
			$sql .= " order by :orderBy";
		}
	/*
        echo "<br>====================================";        
        echo "<br>siglaOrganismo:".$siglaOrganismo;
        echo "<br>departamentos:".$departamentos;
        echo "<br>funcoesVinculosExternos:".$funcoesVinculosExternos;
        echo "<br>idRegiaoRosacruz:".$idRegiaoRosacruz;
        echo "<br>groupBy:".$groupBy;
        echo "<br>orderBy:".$orderBy;
        echo "<br>ativo:".$ativo;
        echo "<br>search:".$search;
        echo "<br>idRegiaoRosacruzOrganismo:".$idRegiaoRosacruzOrganismo;        
        echo "<br>====================================";
        */
        //echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($siglaOrganismo != null) {
            //echo "1";
            $sth->bindParam(':siglaOrganismo', $siglaOrganismo, PDO::PARAM_STR);
        }
        if ($idRegiaoRosacruz != null) {
            //echo "2";
            $sth->bindParam(':idRegiaoRosacruz', $idRegiaoRosacruz, PDO::PARAM_INT);
        }
        if ($idRegiaoRosacruzOrganismo != null) {
            //echo "3";
            $sth->bindParam(':idRegiaoRosacruzOrganismo', $idRegiaoRosacruzOrganismo, PDO::PARAM_INT);
        }
        if($departamentos!=null)
    	{
            foreach ($ids as $u => $id)
            {    
                 $sth->bindValue(":".$u, $id);
            }
        }
        if($funcoesVinculosExternos!=null)
    	{
            foreach ($ids2 as $k => $id)
            {    
                $sth->bindValue(":".$k, $id);
            }
        }
        
        if ($search != null) {
            $search = '%'.$search.'%';
            //echo "4";
            $sth->bindParam(':searchUm', $search, PDO::PARAM_STR);
            $sth->bindParam(':searchDois', $search, PDO::PARAM_STR);
        }
        if ($groupBy != null) {
            //echo "5";
            $sth->bindParam(':groupBy', $groupBy, PDO::PARAM_STR);
        }
        if ($orderBy != null) {
            //echo "6";
            $sth->bindParam(':orderBy', $orderBy, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
         
    }
    
    public function listaUsuarioVinculoFuncao($siglaOrganismo=null,$departamentos=null,$funcoesVinculosExternos=null,$idRegiaoRosacruz=null,$groupBy=null,$orderBy=null,$ativo=null,$search=null) 
    {
        
    	$sql = "SELECT *,u.seqCadast as seqCadast,u.seqCadast as idUsuario FROM usuario as u
        inner join funcao_usuario as fu on fk_seq_cadast = u.seqCadast
    	inner join funcao as f on fk_idFuncao = idFuncao    
    	inner join departamento as d on u.fk_idDepartamento = d.idDepartamento
    	inner join organismoAfiliado as o on o.siglaOrganismoAfiliado = fu.siglaOA
        
    	";
    	$sql .= " WHERE 1=1 ";
    	
    	if ($siglaOrganismo != null) {
			$sql .= " and fu.siglaOA = :siglaOrganismo";
		}
		
    	if ($idRegiaoRosacruz != null) {
			$sql .= " and u.fk_idRegiaoRosacruz = :idRegiaoRosacruz";
		}
		
    	if ($departamentos != null) {
            $ids = explode(",", $departamentos);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {    
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and fk_idDepartamento IN(".$inQuery.")";
	}
		
        if ($funcoesVinculosExternos != null) {
            $ids2 = explode(",", $funcoesVinculosExternos);
            for($i=0;$i<count($ids2);$i++)
            {
                if($i==0)
                {    
                    $inQuery2 = ":".$i;
                }else{
                    $inQuery2 .= ",:".$i;
                }
            }
            $sql .= " and f.vinculoExterno IN(".$inQuery2.")";
        }
		
    	if ($ativo != null) {
			$sql .= " and u.statusUsuario=0";
		}
		
    	if ($search != null) {
			$sql .= " and (
				u.nomeUsuario like %:searchUm% 
				or o.nomeOrganismoAfiliado like %:searchDois%
				) ";
		}
		
 	    if ($groupBy != null) {
			$sql .= " group by :groupBy";
		}
		
    	if ($orderBy != null) {
			$sql .= " order by :orderBy";
		}
	
        //echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($siglaOrganismo != null) {
            $sth->bindParam(':siglaOrganismo', $siglaOrganismo, PDO::PARAM_STR);
        }
        if ($idRegiaoRosacruz != null) {
            $sth->bindParam(':idRegiaoRosacruz', $idRegiaoRosacruz, PDO::PARAM_INT);
        }
        if($departamentos!=null)
    	{
            foreach ($ids as $k => $id)
            {    
                $sth->bindValue(":".$k, $id);
            }
        }
        if($funcoesVinculosExternos!=null)
    	{
            foreach ($ids2 as $k => $id)
            {    
                $sth->bindValue(":".$k, $id);
            }
        }
        
        if ($search != null) {
            $search = '%'.$search.'%';
            $sth->bindParam(':searchUm', $search, PDO::PARAM_STR);
            $sth->bindParam(':searchDois', $search, PDO::PARAM_STR);
        }
        if ($groupBy != null) {
            $sth->bindParam(':groupBy', $groupBy, PDO::PARAM_STR);
        }
        if ($orderBy != null) {
            $sth->bindParam(':orderBy', $orderBy, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
        
    }
    
    public function buscaUsuario($seqCadast) {
        
        $sql = "SELECT * FROM usuario
                                 WHERE `seqCadast` = :seqCadast";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function temUsuario($seqCadast) {

        $sql = "SELECT * FROM usuario WHERE `seqCadast` = :seqCadast";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function listaUsuarioMensagensChatNovas($seqCadastProprio=null,$groupby=null,$paraMim=null,$from=null,$nova=null,$nome=null,$status=null) 
    {
        //echo "SEQCADASTPROPRIO:".$seqCadastProprio;
	$sql = "SELECT * FROM usuario as u 
            left join chat as c on u.seqCadast = c.para 
            where 1=1 ";
		if($seqCadastProprio!=null)
		{
			$sql .= " and u.seqCadast <> :seqCadastProprio";
		}  
		if($paraMim!=null)
		{
			$sql .= " and para = :praMim";
		}
		if($from!=null)
		{
			$sql .= " and de = :from ";
		}
		//if($nova==1)
		//{
			$sql .= " and nova=1 ";
		//}
		if($nome!=null)
		{
			$sql .= " and nomeUsuario like :nome";
		}
		if($status!=null)
		{
			$sql .= " and statusUsuario=0";
		}
    	$sql .= " group by ";
    	if($groupby==null)
    	{
    		$sql .= " seqCadast ";
    	}else{
    		$sql .= " :groupBy";
    	}
    	$sql .= " ORDER BY  idChat DESC";
    	if($nova==1)
    	{
    		$sql .= ", nova DESC";
    	}
    	$sql .= ", u.nomeUsuario ASC";
	//echo $sql;
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seqCadastProprio!=null)
	{
            $sth->bindParam(':seqCadastProprio', $seqCadastProprio, PDO::PARAM_INT);
        }
        if($paraMim!=null)
	{
            $sth->bindParam(':praMim', $paraMim, PDO::PARAM_INT);
        }
        if($from!=null)
	{
            $sth->bindParam(':from', $from, PDO::PARAM_STR);
        }
        if($nome!=null)
	{
            $nome = "%".$nome."%";
            $sth->bindParam(':nome', $nome , PDO::PARAM_STR);
        }
        if($groupby!=null)
    	{
            $sth->bindParam(':groupBy', $groupby, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
         
    }
    
    public function verificaSeMandouMensagemNova($paraMim,$from) 
    {
  
	$sql = "select * from chat as c
            where 1=1 
		and para = :praMim
		and de = :from 
                and nova=1  
                group by c.de ";
    	
	//echo $sql;
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        
        $sth->bindParam(':praMim', $paraMim, PDO::PARAM_INT);
        $sth->bindParam(':from', $from, PDO::PARAM_STR);
                
        $resultado = $c->run($sth);
        //echo "<pre>";print_r($resultado);
        if (count($resultado)>0){
            return true;
        } else {
            return false;
        }
         
    }
    
    public function totalSeMandouMensagemNova($paraMim) 
    {
  
	$sql = "select * from chat as c
            where 1=1 
		and para = :praMim
                and nova=1  
                group by c.de ";
    	
	//echo $sql;
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        
        $sth->bindParam(':praMim', $paraMim, PDO::PARAM_INT);
                
        $resultado = $c->run($sth,false,true);
        if ($resultado){
            return $resultado;
        } else {
            return 0;
        }
         
    }
    
    public function listaUsuarioMensagensChatUltimas($seqCadastProprio=null,$groupby=null,$paraMim=null,$from=null,$nova=null,$nome=null,$status=null) {
    	
	$sql = "SELECT * FROM usuario as u 
            left join chat as c on u.seqCadast = c.de 
            where 1=1 ";
		if($seqCadastProprio!=null)
		{
			$sql .= " and u.seqCadast <> :seqCadastProprioUm";
		}  
		
		$sql .= " and (para=:seqCadastProprioDois";
		$sql .= " or de=:seqCadastProprioTres)";
		
		if($nova==1)
		{
			$sql .= " and nova=1 ";
		}
		if($nome!=null)
		{
			$sql .= " and nomeUsuario like %:nome%";
		}
		if($status!=null)
		{
			$sql .= " and statusUsuario=0";
		}
    	$sql .= " group by ";
    	if($groupby==null)
    	{
    		$sql .= " idChat ";
    	}else{
    		$sql .= " :groupBy";
    	}
    	$sql .= " ORDER BY c.data desc limit 0,5";
		//echo $sql;
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($seqCadastProprio!=null)
	{
            $sth->bindParam(':seqCadastProprioUm', $seqCadastProprio, PDO::PARAM_INT);
        }
        $sth->bindParam(':seqCadastProprioDois', $seqCadastProprio, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastProprioTres', $seqCadastProprio, PDO::PARAM_INT);
        if($nome!=null)
	{
            $sth->bindParam(':nome', $nome, PDO::PARAM_STR);
        }
        if($groupby!=null)
    	{
            $sth->bindParam(':groupBy', $groupby, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
        
         
    }
    
    public function listaUsuarioMensagensEmailNovas($seqCadast) {
    	
	$sql = "SELECT * FROM usuario as u 
            left join email as e on u.seqCadast = e.de
            inner join emailDePara as edp on e.idEmail = edp.fk_idEmail 
            where 1=1 
            and para=:seqCadast and nova=1 
            group by idEmail order by dataCadastro DESC";
    	
		//echo $sql;
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
        
         
    }

    public function removeUsuario($seqCadast) {
        
        $sql = "DELETE FROM usuario WHERE `seqCadast` = :seqCadast";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function buscaNomeUsuario() {
        
        $sql = "SELECT * FROM usuario
                                 WHERE `nomeUsuario` LIKE :nomeUsuario
                              ORDER BY `nomeUsuario` ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $bindNomeUsuario = '%'.$this->nomeUsuario.'%';
        $sth->bindParam(':nomeUsuario', $bindNomeUsuario, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function buscaLoginUsuario() {
        
        $sql = "SELECT * FROM usuario
                                 WHERE `loginUsuario` = :loginUsuario
                              ORDER BY `nomeUsuario` ASC";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':loginUsuario', $this->loginUsuario, PDO::PARAM_STR);

        if ($c->run($sth,null,null,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function buscaUsuarioPorLogin() {
        
        $sql = "SELECT * FROM usuario
                                 WHERE `loginUsuario` LIKE :loginUsuario
                              ";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':loginUsuario', $this->loginUsuario, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function recuperaSenha() 
    {
		
        $sql = "SELECT * FROM usuario
                                 WHERE  
                                 		`emailUsuario` = :emailUsuario
                                                and statusUsuario=0
                              ORDER BY `nomeUsuario` ASC limit 0,1";
        //echo $sql;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $emailUsuario = $this->getEmailUsuario();
        //echo "----------->Email:".$this->getEmailUsuario();
        
        $sth->bindParam(':emailUsuario', $emailUsuario, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        //echo "<pre>";print_r($resultado);
        $array = array();
        $array['achou'] = 0;
        
        foreach ($resultado as $retorno) {
            $array['seqCadast']		= $retorno['seqCadast'];
            $array['nomeUsuario'] 	= $retorno['nomeUsuario'];
            $array['emailUsuario'] 	= $retorno['emailUsuario'];
            $array['loginUsuario']     	= $retorno['loginUsuario'];
            $array['achou'] 		= 1;
        }
        
		if($array['achou']==1)//Achou o usu�rio ent�o gerar token
		{
			$token= "";
                        $timelife=time();
			$valor = "0123456789";
			srand((double)microtime()*1000000);
			for ($i=0; $i<10; $i++){
				$token.= $valor[rand()%strlen($valor)];
			}
			
			while ($this->verificaTokenRecuperacaoSenha($token))//enquanto o token j� existir gerar novamente
			{
				$token= "";
				$valor = "0123456789";
				srand((double)microtime()*1000000);
				for ($i=0; $i<10; $i++){
					$token.= $valor[rand()%strlen($valor)];
				}
			}
			
			$sql2 = "INSERT INTO recuperaSenha (data,fk_seqCadast,emailUsuario,token,timelife) values 
			(now(),:seqCadast,:emailUsuario,:token,:timelife)";
                        
                        $con = $c->openSOA();
                        $sth = $con->prepare($sql2);

                        $sth->bindParam(':seqCadast', $array['seqCadast'], PDO::PARAM_INT);
                        $sth->bindParam(':emailUsuario', $array['emailUsuario'], PDO::PARAM_STR);
                        $sth->bindParam(':token', $token, PDO::PARAM_STR);
                        $sth->bindParam(':timelife', $timelife, PDO::PARAM_STR);

                        if ($c->run($sth,true)) {
        	
				$array['token'] = $token;
			}else{
				$array['token'] = "";
			}
		}
                
	//echo "<pre>";print_r($array);exit();	
        
        if ($array) {
            return $array;
        } else {
            return false;
        }
    }
    
    public function verificaTokenRecuperacaoSenha($token)
    {
        $sql = "SELECT * from recuperaSenha where token =:token";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);

        if ($c->run($sth,false,false,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function retornaToken($token)
    {
        $sql = "SELECT * from recuperaSenha where token =:token ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function retornaTokenIntegridade($token=null,$idFuncionalidade=null)
    {
        $sql = "SELECT * from integridadeTokenUsuario"
                . " inner join usuario on usuarioToken = loginUsuario "
                . " where 1=1 ";
        if($token!=null)
        {       
                $sql .= " and token =:token ";
        }
        if($idFuncionalidade!=null)
        {
            $sql .= " and idFuncionalidade=:idFuncionalidade";
        }
        if($token!=null)
        { 
            $sql .= " limit 0,1";
        }
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($token!=null)
        { 
            $sth->bindParam(':token', $token, PDO::PARAM_STR);
        }
        
        if($idFuncionalidade!=null)
        {
            $sth->bindParam(':idFuncionalidade', $idFuncionalidade, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function retornaTokenPorLogin($login,$hoje,$idFuncionalidade)
    {

        $sql = "SELECT * from integridadeTokenUsuario"
            . " inner join usuario on usuarioToken = loginUsuario "
            . " where 1=1 "
            . " and usuarioToken =:login 
                and dataFinal >= :hoje
                and dataInicial <= :hoje
                and idFuncionalidade = :idFuncionalidade";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':login', $login, PDO::PARAM_STR);
        $sth->bindParam(':hoje', $hoje, PDO::PARAM_STR);
        $sth->bindParam(':idFuncionalidade', $idFuncionalidade, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
	
    public function alterarSenha($t,$novaSenha) {
		
        $array = array();
        $array['achou']=0;

        $sql = "SELECT * from recuperaSenha 
        where token=:t limit 0,1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':t', $t, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
		

        foreach ($resultado as $retorno) {
            $array['seqCadast']		= $retorno['fk_seqCadast'];
            $array['achou'] 		= 1;
        }
		
        if($array['achou']==1)//Achou o token ent�o...
        {
            
                $sql2 = "UPDATE usuario set senhaUsuario = :senhaUsuario 
                where seqCadast=:seqCadast";

                $con = $c->openSOA();
                $sth = $con->prepare($sql2);

                $sth->bindParam(':seqCadast', $array['seqCadast'], PDO::PARAM_INT);
                $sth->bindParam(':senhaUsuario', $novaSenha, PDO::PARAM_STR);

                if ($c->run($sth,true)) {
                        return true;
                }else{
                        return false;
                }

                return true;
        }
        return false;
        
    }
	
    public function enviouEmailRecuperaSenha($token) {

        $sql = "UPDATE recuperaSenha SET envioEmail='1'
		WHERE token = :token";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function erroEmailRecuperaSenha($token) {

        $sql = "UPDATE recuperaSenha SET erroEmail='1'
		WHERE token = :token";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function enviouEmailIntegridadeTokenUsuario($token) {

        $sql = "UPDATE integridadeTokenUsuario 
                SET envioEmail='1'
		WHERE token = :token";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function erroEmailIntegridadeTokenUsuario($token) {

        $sql = "UPDATE integridadeTokenUsuario SET erroEmail='1'
		WHERE token = :token";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function buscaSeqCadastUsuario() {
        $sql = "SELECT * FROM usuario
                                 WHERE `seqCadast` = :seqCadast
                              ORDER BY `nomeUsuario` ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,false,false,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function buscarIdUsuario($seqCadast) {
        
        if (isset($seqCadast)) {
            $sql = "SELECT * FROM usuario"
                    . "                 WHERE `seqCadast` = :seqCadast";
        } else {
            $sql = "SELECT * FROM usuario 
                                 WHERE `seqCadast` = :id";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if (isset($seqCadast)) {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        if(isset($_POST['id']))
        {    
            if(is_numeric($_POST['id'])&&!isset($seqCadast))
            {    
                $sth->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
            }
        }
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function alteraStatusUsuario() {
        
    	$sql = "UPDATE usuario SET statusUsuario = :statusUsuario WHERE seqCadast = :seqCadast";
        //echo "<br>Query:".$sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        //echo "<br>Status:".$this->statusUsuario;
        //echo "<br>SeqCadast:".$this->seqCadast;
        
        $sth->bindParam(':statusUsuario', $this->statusUsuario, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            $arr = array();
            $arr['status'] = $this->statusUsuario;
            return $arr;
        } else {
            return false;
        }
    }

    public function verificaEmailExistente($emailUsuario) {
        
        $sql = "SELECT * FROM usuario WHERE 1=1";

        if ($emailUsuario) {
            $sql .= "AND emailUsuario = :emailUsuario";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($emailUsuario) {
            $sth->bindParam(':emailUsuario', trim($emailUsuario), PDO::PARAM_STR);
        }

        $resultado = $c->run($sth);

        if (count($resultado['seqCadast']) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public function verificaLoginExistente($login) {

        $sql = "SELECT * FROM usuario WHERE 1=1 AND loginUsuario = :login";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':login', $login, PDO::PARAM_STR);

        if ($c->run($sth,null,null,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function verificaSeJaCienteDaAssinaturaEletronica($login) {

        $sql = "SELECT * FROM usuario WHERE 1=1 AND cienteAssinaturaDigital=1 AND loginUsuario = :login";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':login', $login, PDO::PARAM_STR);

        if ($c->run($sth,null,null,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function registroUltimoAcesso($seqCadast, $ip) {
        
        $sql = "INSERT INTO log (fk_seqCadast, dataHoraAcesso, ipComputador
                              VALUES (:seqCadast, now(), :ip)";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimaAcesso($seqCadast) {
        
        $sql = "SELECT * FROM log
                              WHERE fk_seqCadast = :seqCadast
                              ORDER BY dataHoraAcesso DESC
                              LIMIT 1,1";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function alteraLotacaoUsuario() {
        
    	$sql = "UPDATE usuario SET `siglaOA` = :siglaOA WHERE `seqCadast` = :seqCadast";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getSiglaOA             = $this->getSiglaOA();
        $getSeqCadast           = $this->getSeqCadast();

        $sth->bindParam(':siglaOA', $getSiglaOA, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $getSeqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function listaUsuarioPeloDepartamento($departamentos=null) {
        
        $sql = "SELECT seqCadast,nomeUsuario FROM usuario WHERE 1=1 and statusUsuario=0 ";
        $sql .= $departamentos != null ? " and fk_idDepartamento=:departamentos" : "";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($departamentos!=null)
        {    
            $sth->bindParam(':departamentos', $departamentos, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
    
    public function verificaTokenFacebook($token) {
        
        $sql = "SELECT seqCadast,nomeUsuario FROM usuario WHERE "
                . " tokenFacebook=:token ";
        //echo "---->".$token;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);
        
        if ($c->run($sth,null,null,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function vinculaIdFacebookContaSOA($idFacebook,$seqCadast) {
        
    	$sql = "UPDATE usuario SET `tokenFacebook` = :idFacebook WHERE `seqCadast` = :seqCadast";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idFacebook', $idFacebook, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function verificaVinculoFacebook($seq=null, $tokenFacebook=null, $semTokenFacebook=null,$avisoFacebook=null, $semAvisoFacebook=null ) {
        
        $sql = "SELECT seqCadast,nomeUsuario FROM usuario WHERE 1=1 ";
        
        if($seq!=null)
        {    
            $sql .= " and seqCadast=:seq ";
        }
        if($tokenFacebook!=null)
        {    
            $sql .= " and tokenFacebook != ''";
        }
        if($semTokenFacebook!=null)
        {    
            $sql .= " and tokenFacebook = ''";
        }
        if($avisoFacebook!=null)
        {    
            $sql .= " and avisoFacebook= 1 ";
        }
        if($semAvisoFacebook!=null)
        {    
            $sql .= " and avisoFacebook= 0 ";
        }
        //echo "---->".$sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seq', $seq, PDO::PARAM_INT);
        
        if ($c->run($sth,null,null,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function fechaAvisoFacebook($seqCadast) {

        $sql = "UPDATE usuario SET avisoFacebook='1'
		WHERE seqCadast = :seqCadast";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function desvincularFacebook($seqCadast) {

        $sql = "UPDATE usuario SET tokenFacebook=''
		WHERE seqCadast = :seqCadast";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function selecionaDadosUsuarioPeloToken($token=null) {
        
        $sql = "SELECT senhaUsuario,loginUsuario FROM usuario WHERE "
                . " tokenFacebook=:token ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':token', $token, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        
        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }
    
    public function changePassword($seqCadast,$hash)
    {
        $sql2 = "UPDATE usuario set senhaUsuario = :senhaUsuario,
                    senhaCriptografada='1'
                    where seqCadast=:seqCadast";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql2);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':senhaUsuario', $hash, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
                return true;
        }else{
                return false;
        }
    }
    
    public function atualizaDataCadastroUsuario($seqCadast,$dataCadastroUsuario)
    {
        $sql2 = "UPDATE usuario set dataCadastroUsuario = :dataCadastroUsuario
                    where seqCadast=:seqCadast";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql2);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':dataCadastroUsuario', $dataCadastroUsuario, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
                return true;
        }else{
                return false;
        }
    }
    
    public function cadastraIntegridadeTokenUsuario($token,$idFuncionalidade,$idItemFuncionalidade,$usuarioToken,$emailUsuario,$usuario,$timelife,$dataInicial,$dataFinal,$mes,$ano,$liberarEmMassa,$idOrganismoAfiliado) {
        
    	$sql = "INSERT INTO integridadeTokenUsuario (
                                    token, 
                                    timelife,
                                    dataInicial,
                                    dataFinal,
                                    idFuncionalidade, 
                                    idItemFuncionalidade, 
                                    usuarioToken, 
                                    emailUsuario, 
                                    usuario, 
                                    dataCadastro,
                                    mes,
                                    ano,
                                    liberarEmMassa,
                                    fk_idOrganismoAfiliado)
                                    VALUES (
                                           :token,
                                           :timelife,
                                           :dataInicial,
                                           :dataFinal,
                                           :idFuncionalidade,
                                           :idItemFuncionalidade,
                                           :usuarioToken,
                                           :emailUsuario,
                                           :usuario,
                                           now(),
                                           :mes,
                                           :ano,
                                           :liberarEmMassa,
                                           :idOrganismoAfiliado
                                            )";
        /*
        $data = array(
            'token'=>$token,
            'timelife'=>$timelife,
            'dataInicial'=>$dataInicial,
            'dataFinal'=>$dataFinal,
            'idFuncionalidade'=>$idFuncionalidade,
            'idItemFuncionalidade'=>$idItemFuncionalidade,
            'usuarioToken'=>$usuarioToken,
            'emailUsuario'=>$emailUsuario,
            'usuario'=>$usuario,
            'mes'=>$mes,
            'ano'=>$ano,
            'idOrganismoAfiliado'=>$idOrganismoAfiliado
        );
        */
        //echo $this->parms("cadastraUsuario",$sql,$data);
        
        
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':token', $token, PDO::PARAM_STR);
        $sth->bindParam(':timelife', $timelife, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $dataFinal, PDO::PARAM_STR);
        $sth->bindParam(':idFuncionalidade', $idFuncionalidade, PDO::PARAM_INT);
        $sth->bindParam(':idItemFuncionalidade', $idItemFuncionalidade, PDO::PARAM_INT);
        $sth->bindParam(':usuarioToken', $usuarioToken, PDO::PARAM_STR);
        $sth->bindParam(':emailUsuario', $emailUsuario, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $usuario, PDO::PARAM_INT);
        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':liberarEmMassa', $liberarEmMassa, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        
        
        if ($c->run($sth,true)){
            $ultimo_id = $this->selecionaUltimoIdCadastrado();
            return $ultimo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE integridadeTokenUsuario";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function selecionaUltimoIdCadastrado(){

        $sql= "SELECT `idIntegridadeTokenUsuario` as id FROM `integridadeTokenUsuario` ORDER BY idIntegridadeTokenUsuario DESC LIMIT 1";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['id'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }
    
    public function listaUsuarioLoginDuplicado($start=null,$limit=null) {
                                                                                
        
        
    	$sql = "SELECT * FROM usuario as u WHERE 1=1 
                    and loginUsuario IN (
                    SELECT B.loginUsuario
                    FROM usuario B
                    GROUP BY B.loginUsuario
                    HAVING COUNT(*) > 1)";
    	
        if ($limit != null) {
			$sql .= " limit :start, :limit";
		}
        //echo $sql."<br>";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($start != null) {
            //echo "10";
            $sth->bindValue(':start', (int) trim($start), PDO::PARAM_INT);
        }
         
        if ($limit != null) {
            //echo "11";
            $sth->bindValue(':limit', (int) trim($limit), PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
         
    }
    
    public function guardaLoginAntigoNovo($loginAntigo, $loginNovo, $seqCadast)
    {
        $sql2 = "UPDATE usuario set loginUsuarioAntigo = :loginAntigo,
                                    loginUsuario = :loginNovo
                    where seqCadast=:seqCadast";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql2);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':loginAntigo', $loginAntigo, PDO::PARAM_STR);
        $sth->bindParam(':loginNovo', $loginNovo, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
                return true;
        }else{
                return false;
        }
    }

    public function concordoAssinatura($seqCadast,$codigo) {

        $sql = "UPDATE usuario SET codigoTermoAdesao=:codigo
		WHERE seqCadast = :seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_STR);
        $sth->bindParam(':codigo', $codigo, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function cienteAssinatura($seqCadast) {

        $sql = "UPDATE usuario SET cienteAssinaturaDigital='1',dataAssinaturaDigital=now()
		WHERE seqCadast = :seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function atualizaNotificacaoSMSAdesaoAssinaturaEletronica($seqCadast,$status) {

        $sql = "UPDATE usuario SET smsAdesaoAssinaturaEletronica=:status
		WHERE seqCadast = :seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':status', $status, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function enviouEmailAssinatura($seqCadast) {

        $sql = "UPDATE usuario SET envioEmailAssinatura='1'
		WHERE seqCadast = :seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function erroEmailAssinatura($seqCadast) {

        $sql = "UPDATE usuario SET envioEmailAssinatura='0'
		WHERE seqCadast = :seqCadast";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function cienteAssinaturaPorLogin($login) {

        $sql = "UPDATE usuario SET cienteAssinaturaDigital='1',dataAssinaturaDigital=now()
		WHERE loginUsuario = :login";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':login', $login, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function atualizaCodigoDeAfiliacaoUsuario($seqCadast,$codigoDeAfiliacao)
    {
        $sql2 = "UPDATE usuario set codigoDeAfiliacao = :codigoDeAfiliacao
                    where seqCadast=:seqCadast";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql2);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':codigoDeAfiliacao', $codigoDeAfiliacao, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }

}


?>
