<?php
require_once ("conexaoClass.php");

class LogSistema {

    public $idLogSistema;
    public $data;
    public $ip;
    public $host;
    public $pagina;
    public $browser;

    /* Funções GET e SET */

    function getIdLogSistema() {
        return $this->idLogSistema;
    }

    function getData() {
        return $this->data;
    }

    function getIp() {
        return $this->ip;
    }

    function getHost() {
        return $this->host;
    }

    function getPagina() {
        return $this->pagina;
    }

    function getBrowser() {
        return $this->browser;
    }

    function setIdLogSistema($idLogSistema) {
        $this->idLogSistema = $idLogSistema;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setIp($ip) {
        $this->ip = $ip;
    }

    function setHost($host) {
        $this->host = $host;
    }

    function setPagina($pagina) {
        $this->pagina = $pagina;
    }

    function setBrowser($browser) {
        $this->browser = $browser;
    }

    public function cadastraLogSistema() {
        
        $sql = "DROP TRIGGER IF EXISTS `entrada`.`log_entrada`//
                              CREATE TRIGGER `log` AFTER INSERT ON `usuario`
                              FOR EACH ROW BEGIN
                              INSERT INTO logsistema SET
                              data = NEW.data,
                              ip = NEW.ip,                          
                              host = NEW.host,
                              pagina = NEW.pagina,
                              browser = NEW.browser,
                              acao = 'insert',
                              END";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}
?>​
