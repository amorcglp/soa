<?php

require_once ("conexaoClass.php");

class ColumbaRelatorioTrimestral{

    private $idColumbaRelatorioTrimestral;    
    private $fk_idOrganismoAfiliado;
    private $codigoAfiliacao;
    private $seqCadastMembro;
    private $dataPrevistaInstalacao;
    private $nomeColumba;
    private $tipoColumba;
    private $usuario;

    //METODO SET/GET -----------------------------------------------------------

    function getIdColumbaRelatorioTrimestral() {
        return $this->idColumbaRelatorioTrimestral;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }

    function getSeqCadastMembro() {
        return $this->seqCadastMembro;
    }

    function getDataPrevistaInstalacao() {
        return $this->dataPrevistaInstalacao;
    }

    function getNomeColumba() {
        return $this->nomeColumba;
    }

    function getTipoColumba() {
        return $this->tipoColumba;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setIdColumbaRelatorioTrimestral($idColumbaRelatorioTrimestral) {
        $this->idColumbaRelatorioTrimestral = $idColumbaRelatorioTrimestral;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }

    function setSeqCadastMembro($seqCadastMembro) {
        $this->seqCadastMembro = $seqCadastMembro;
    }

    function setDataPrevistaInstalacao($dataPrevistaInstalacao) {
        $this->dataPrevistaInstalacao = $dataPrevistaInstalacao;
    }

    function setNomeColumba($nomeColumba) {
        $this->nomeColumba = $nomeColumba;
    }

    function setTipoColumba($tipoColumba) {
        $this->tipoColumba = $tipoColumba;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

        
    //--------------------------------------------------------------------------

    public function cadastroColumba(){

        $sql = "INSERT INTO columbaRelatorioTrimestral  
                                 (fk_idOrganismoAfiliado,
                                  codigoAfiliacao,
                                  nomeColumba,
                                  seqCadastMembro,
                                  dataPrevistaInstalacao,
                                  tipoColumba,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                            		:idOrganismoAfiliado,
                            		:codigoAfiliacao,
                            		:nome,
                            		:seqCadast,
                            		:dataPrevistaInstalacao,
                                        :tipo,    
                            		:usuario,
                                    now()
                                    )";
        //echo $sql; exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nomeColumba, PDO::PARAM_STR);
        $sth->bindParam(':seqCadast', $this->seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':dataPrevistaInstalacao', $this->dataPrevistaInstalacao, PDO::PARAM_STR);
        $sth->bindParam(':tipo', $this->tipoColumba, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }
    
    public function atualizaOrganismoColumba(){

        $sql="UPDATE columbaRelatorioTrimestral SET  
                                  fk_idOrganismoAfiliado=:idOrganismoAfiliado
                                  WHERE seqCadastMembro=:seqCadast
                                  ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadastMembro, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idColumba) as lastid FROM columbaRelatorioTrimestral";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	
    
    public function listaColumba($idOrganismoAfiliado=null,$tipoColumba=null){

    	$sql = "SELECT * FROM columbaRelatorioTrimestral
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1 ";
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
        if($tipoColumba!=null)
    	{
    		$sql .= " and tipoColumba = :tipo";
    	}
    	$sql .= " order by codigoAfiliacao";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($tipoColumba!=null)
    	{
            $sth->bindParam(':tipo', $tipoColumba, PDO::PARAM_INT);
        }
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($seqCadastMembro,$idOrganismoAfiliado){

    	$sql = "SELECT * FROM columbaRelatorioTrimestral 
    			where seqCadastMembro=:seqCadast"
                . " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if($c->run($sth,false,false,true))
        {
	    return true;    
        }else{
            return false;
        }
    }

    public function buscaColumbaPeloSeqCadast($seqCadast){

        $sql = "SELECT * FROM  columbaRelatorioTrimestral WHERE   seqCadastMembro = :seqCadast";

        //echo $sql."<br>";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function instalarColumba($id,$dataInstalacao){

        $sql="UPDATE columbaRelatorioTrimestral SET  
                                  tipoColumba='2', 
                                  dataInstalacao=:dataInstalacao
                                  WHERE idColumbaRelatorioTrimestral=:id
                                  ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':dataInstalacao', $dataInstalacao, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }
    
    public function desinstalarColumba($id,$dataDesinstalacao){

        $sql = "UPDATE columbaRelatorioTrimestral SET  
                                  tipoColumba='3', dataDesinstalacao=:dataInstalacao
                                  WHERE idColumbaRelatorioTrimestral=:id
                                  ";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':dataInstalacao', $dataInstalacao, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }
    
}

?>
