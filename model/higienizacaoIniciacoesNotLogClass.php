<?php

require_once ("conexaoClass.php");

class higienizacaoIniciacoesNotLog {

    public $idHigienizacaoIniciacoesNotLog;
    public $fk_idAtividadeIniciatica;
    public $seqCadast;
    public $seqTipoObrigacao;
    public $localObrigacao;
    public $dataObrigacao;
    public $regiao;
    public $siglaOA;

    /**
     * @return mixed
     */
    public function getIdHigienizacaoIniciacoesNotLog()
    {
        return $this->idHigienizacaoIniciacoesNotLog;
    }

    /**
     * @param mixed $idHigienizacaoIniciacoesNotLog
     */
    public function setIdHigienizacaoIniciacoesNotLog($idHigienizacaoIniciacoesNotLog)
    {
        $this->idHigienizacaoIniciacoesNotLog = $idHigienizacaoIniciacoesNotLog;
    }

    /**
     * @return mixed
     */
    public function getFkIdAtividadeIniciatica()
    {
        return $this->fk_idAtividadeIniciatica;
    }

    /**
     * @param mixed $fk_idAtividadeIniciatica
     */
    public function setFkIdAtividadeIniciatica($fk_idAtividadeIniciatica)
    {
        $this->fk_idAtividadeIniciatica = $fk_idAtividadeIniciatica;
    }

    /**
     * @return mixed
     */
    public function getSeqCadast()
    {
        return $this->seqCadast;
    }

    /**
     * @param mixed $seqCadast
     */
    public function setSeqCadast($seqCadast)
    {
        $this->seqCadast = $seqCadast;
    }

    /**
     * @return mixed
     */
    public function getSeqTipoObrigacao()
    {
        return $this->seqTipoObrigacao;
    }

    /**
     * @param mixed $seqTipoObrigacao
     */
    public function setSeqTipoObrigacao($seqTipoObrigacao)
    {
        $this->seqTipoObrigacao = $seqTipoObrigacao;
    }

    /**
     * @return mixed
     */
    public function getLocalObrigacao()
    {
        return $this->localObrigacao;
    }

    /**
     * @param mixed $localObrigacao
     */
    public function setLocalObrigacao($localObrigacao)
    {
        $this->localObrigacao = $localObrigacao;
    }

    /**
     * @return mixed
     */
    public function getDataObrigacao()
    {
        return $this->dataObrigacao;
    }

    /**
     * @param mixed $dataObrigacao
     */
    public function setDataObrigacao($dataObrigacao)
    {
        $this->dataObrigacao = $dataObrigacao;
    }

    /**
     * @return mixed
     */
    public function getRegiao()
    {
        return $this->regiao;
    }

    /**
     * @param mixed $regiao
     */
    public function setRegiao($regiao)
    {
        $this->regiao = $regiao;
    }

    /**
     * @return mixed
     */
    public function getSiglaOA()
    {
        return $this->siglaOA;
    }

    /**
     * @param mixed $siglaOA
     */
    public function setSiglaOA($siglaOA)
    {
        $this->siglaOA = $siglaOA;
    }



    public function cadastraHigienizacaoIniciacoesNotLog() {
        
        $sql = "INSERT INTO higienizacaoIniciacoesNotLog 
                                  (
                                  fk_idAtividadeIniciatica,
                                  seqCadast,
                                  seqTipoObrigacao,
                                  localObrigacao,
                                  dataObrigacao,
                                  regiao, 
                                  siglaOA, 
                                  dataCadastro)
                                    VALUES (
                                            :fk_idAtividadeIniciatica,
                                            :seqCadast,
                                            :seqTipoObrigacao,
                                            :localObrigacao,
                                            :dataObrigacao,
                                            :regiao,
                                            :siglaOA,
                                            now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeIniciatica', $this->fk_idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':seqTipoObrigacao', $this->seqTipoObrigacao, PDO::PARAM_INT);
        $sth->bindParam(':localObrigacao', $this->localObrigacao, PDO::PARAM_STR);
        $sth->bindParam(':dataObrigacao', $this->dataObrigacao, PDO::PARAM_STR);
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_STR);
        $sth->bindParam(':siglaOA', $this->siglaOA, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    
    public function listaHigienizacaoIniciacoesNotLog($start,$limit) {

        $sql =" SELECT * FROM higienizacaoIniciacoesNotLog as b ";
        $sql .= " limit :start, :limit";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($limit != null) {
            $sth->bindValue(':start', (int) trim($start), PDO::PARAM_INT);
            //echo "11";
            $sth->bindValue(':limit', (int) trim($limit), PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function apagaRegistro($id) {

        $sql = "DELETE FROM higienizacaoIniciacoesNotLog WHERE idHigienizacaoIniciacoesNotLog = '".$id."'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($c->run($sth,null,null,true)) {
            return true;
        } else {
            return false;
        }
    }

    public function limpaTabelaNotLog() {
        
        $sql = "TRUNCATE higienizacaoIniciacoesNotLog";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

}

?>