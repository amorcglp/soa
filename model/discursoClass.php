<?php

require_once 'conexaoClass.php';

class Discurso {
    
    private $idDiscurso;
    private $numeroMensagem;
    private $autorMensagem;
    private $tituloMensagem;
    private $dataCriacao;
    private $temExperimento;
    private $qtdeImpressao;
    private $fk_seqCadast;
    private $fk_idDocumento;
    private $tipo;
    
    /*
     * SET/GET INÍCIO
     */
    
    public function getIdDiscurso() {
        return $this->idDiscurso;
    }

    public function getNumeroMensagem() {
        return $this->numeroMensagem;
    }

    public function getAutorMensagem() {
        return $this->autorMensagem;
    }

    public function getTituloMensagem() {
        return $this->tituloMensagem;
    }
    
    public function getDataCriacao() {
        return $this->dataCriacao;
    }
    
    public function getTemExperimento() {
        return $this->temExperimento;
    }

    public function getQtdeImpressao() {
        return $this->qtdeImpressao;
    }

    public function getFkSeqCadast() {
        return $this->fk_seqCadast;
    }
    
    public function getFkIdDocumento() {
        return $this->fk_idDocumento;
    }
    
    public function getTipo() {
        return $this->tipo;
    }
    
    public function setIdDiscurso($idDiscurso) {
        $this->idDiscurso = $idDiscurso;
    }

    public function setNumeroMensagem($numeroMensagem) {
        $this->numeroMensagem = $numeroMensagem;
    }

    public function setAutorMensagem($autorMensagem) {
        $this->autorMensagem = $autorMensagem;
    }

    public function setTituloMensagem($tituloMensagem) {
        $this->tituloMensagem = $tituloMensagem;
    }
    
    public function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }
    
    public function setTemExperimento($temExperimento) {
        $this->temExperimento = $temExperimento;
    }

    public function setQtdeImpressao($qtdeImpressao) {
        $this->qtdeImpressao = $qtdeImpressao;
    }

    public function setFkSeqCadast($fk_seqCadast) {
        $this->fk_seqCadast = $fk_seqCadast;
    }
    
    public function setFkIdDocumento($fk_idDocumento) {
        $this->fk_idDocumento = $fk_idDocumento;
    }
    
    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }
    
    /*
     * SET/GET FIM
     */
    
    public function cadastraDiscurso() {
        
        $sql = "INSERT INTO `discurso` (";
        $sql .= "`numeroMensagem`, ";
        $sql .= "`autorMensagem`, ";
        $sql .= "`tituloMensagem`, ";
        $sql .= "`dataCriacao`, ";
        $sql .= "`temExperimento`, ";
        $sql .= "`qtdeImpressao`, ";
        $sql .= "`fk_seqCadast`, ";
        $sql .= "`fk_idDocumento`,";
        $sql .= "`tipo`";
        $sql .= ") VALUES (";
        $sql .=  ":numero, ";
        $sql .= ":autor, ";
        $sql .= ":titulo, ";
        $sql .= ":dataCriacao, ";
        $sql .= ":temExperimento, ";
        $sql .= ":qtdeImpressao, ";
        $sql .= ":seqCadast, ";
        $sql .= ":idDocumento,";
                $sql .= ":tipo"
                . ")";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':numero', $this->numeroMensagem, PDO::PARAM_STR);
        $sth->bindParam(':autor', $this->autorMensagem, PDO::PARAM_STR);
        $sth->bindParam(':titulo', $this->tituloMensagem, PDO::PARAM_STR);
        $sth->bindParam(':dataCriacao', $this->dataCriacao, PDO::PARAM_STR);
        $sth->bindParam(':temExperimento', $this->temExperimento, PDO::PARAM_INT);
        $sth->bindParam(':qtdeImpressao', $this->qtdeImpressao, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->fk_seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':idDocumento', $this->fk_idDocumento, PDO::PARAM_INT);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraDiscurso() {
        
        $sql = "UPDATE `discurso` SET "
                . " `numeroMensagem` = :numero, "
                . " `autorMensagem` = :autor, "
                . " `tituloMensagem` = :titulo,  "
                . " `dataCriacao` = :dataCriacao, "
                . " `temExperimento` = :temExperimento, "
                . " `qtdeImpressao` = :qtdeImpressao, "
                . " `fk_idDocumento` = :idDocumento, "
                . " `tipo` = :tipo "
                . " WHERE `idDiscurso` = :idDiscurso";
        
        //echo "<h1>".$sql."</h1>";exit();
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':numero', $this->numeroMensagem, PDO::PARAM_STR);
        $sth->bindParam(':autor', $this->autorMensagem, PDO::PARAM_STR);
        $sth->bindParam(':titulo', $this->tituloMensagem, PDO::PARAM_STR);
        $sth->bindParam(':dataCriacao', $this->dataCriacao, PDO::PARAM_STR);
        $sth->bindParam(':temExperimento', $this->temExperimento, PDO::PARAM_INT);
        $sth->bindParam(':qtdeImpressao', $this->qtdeImpressao, PDO::PARAM_INT);
        $sth->bindParam(':idDocumento', $this->fk_idDocumento, PDO::PARAM_INT);
        $sth->bindParam(':idDiscurso', $this->idDiscurso, PDO::PARAM_INT);
        $sth->bindParam(':tipo', $this->tipo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function listarDiscurso($fk_seqCadast) {
        $sql = ($fk_seqCadast == null) ?
                "SELECT * FROM `discurso` " :
                "SELECT * FROM `discurso` WHERE `fk_seqCadast` = :seqCadast";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':seqCadast', $fk_seqCadast, PDO::PARAM_INT);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listarDiscursoId($id, $fk_seqCadast) {
        
        $sql = "SELECT * FROM `discurso` WHERE idDiscurso = :id";
        
        if($fk_seqCadast!=null) {
            $sql .= " and fk_seqCadast = :seqCadast";
        }
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);   
        
        if($fk_seqCadast!=null) {
            $sth->bindParam(':seqCadast', $fk_seqCadast, PDO::PARAM_INT);        
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function cadastraIdDocumento($idDiscurso) {
        
        $sql = "UPDATE `discurso` SET `fk_idDocumento` = :idDocumento";
        $sql .= " WHERE `idDiscurso` = :idDiscurso";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDocumento', $this->fk_idDocumento, PDO::PARAM_INT);
        $sth->bindParam(':idDiscurso', $idDiscurso, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function removeDocumento($idDiscurso) {
        
        $sql = "UPDATE `discurso` SET ";
        $sql .= "`fk_idDocumento` = NULL ";
        $sql .= "WHERE `idDiscurso` = :idDiscurso";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDiscurso', $idDiscurso, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function excluiDiscurso() {
        
        $sql = "DELETE FROM `discurso` WHERE `idDiscurso` = :idDiscurso";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDiscurso', $this->idDiscurso, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}
