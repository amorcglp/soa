<?php
require_once ("conexaoClass.php");

class QuitacaoDivida {

    public $idQuitacaoDivida;
    public $fk_idDivida;
    public $dataPagamento;
    public $valorPagamento;
    public $observacao;
    public $usuario;
    public $ultimoAtualizar;

    /* Funções GET e SET */

    function getIdQuitacaoDivida() {
        return $this->idQuitacaoDivida;
    }

    function getFkIdDivida() {
        return $this->fk_idDivida;
    }

    function getDataPagamento() {
        return $this->dataPagamento;
    }

    function getValorPagamento() {
        return $this->valorPagamento;
    }
    
    function getObservacao() {
        return $this->observacao;
    }
       
    function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdQuitacaoDivida($idQuitacaoDivida) {
        $this->idQuitacaoDivida = $idQuitacaoDivida;
    }

    function setFkIdDivida($fk_idDivida) {
        $this->fk_idDivida = $fk_idDivida;
    }

    function setDataPagamento($dataPagamento) {
        $this->dataPagamento = $dataPagamento;
    }

    function setValorPagamento($valorPagamento) {
        $this->valorPagamento = $valorPagamento;
    }
    
    function setObservacao($observacao) {
        $this->observacao = $observacao;
    }
    
    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    public function cadastraQuitacaoDivida() {
        
    	$sql = "INSERT INTO quitacaoDivida 
                                    (`fk_idDivida`, 
                                    `dataPagamento`, 
                                    `valorPagamento`,
                                    `observacao`,
                                    `usuario`,
                                    `dataCadastro`
                                    )
                                    VALUES (
                                            :idDivida,
                                            :dataPagamento,
                                            :valor,
                                            :observacao,
                                            :usuario,
                                            now()
                                            )";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idDivida', $this->fk_idDivida, PDO::PARAM_INT);
        $sth->bindParam(':dataPagamento', $this->dataPagamento, PDO::PARAM_STR);
        $sth->bindParam(':valor', $this->valorPagamento, PDO::PARAM_STR);
        $sth->bindParam(':observacao', $this->observacao, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraQuitacaoDivida($id) {

        $sql = "UPDATE quitacaoDivida SET 
        					 `fk_idDivida` = :idDivida,
                                                 `dataPagamento` = :dataPagamento,
                                                 `valorPagamento` = :valorPagamento,
                                                 `observacao` = :observacao,
                                                 `ultimoAtualizar` = :ultimoAtualizar    
                            WHERE `idQuitacaoDivida` = :idQuitacaoDivida";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDivida', $this->fk_idDivida, PDO::PARAM_INT);
        $sth->bindParam(':dataPagamento', $this->dataPagamento, PDO::PARAM_STR);
        $sth->bindParam(':valorPagamento', $this->valorPagamento, PDO::PARAM_STR);
        $sth->bindParam(':observacao', $this->observacao, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $this->usuario, PDO::PARAM_INT);
        $sth->bindParam(':idQuitacaoDivida', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'quitacaoDivida'";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELEC * FROM quitacaoDivida ORDER BY idQuitacaoDivida DESC LIMIT 1";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaQuitacaoDivida($fk_idOrganismoAfiliado=null, $mesAtual=null,$anoAtual=null,$categoriasGLP=null,$categoriasOutros=null, $idDivida=null, $anoPagamento=null, $mesPagamento=null) {
        
    	$sql = "SELECT * FROM quitacaoDivida "
                . "left join  divida on fk_idDivida = idDivida "
                . "where 1=1";

        if($mesAtual!=null)
        {
            $sql .= " and MONTH(inicioDivida)<=:mesAtual and MONTH(fimDivida)>=:mesAtual";
        }
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(inicioDivida)<=:anoAtual and YEAR(fimDivida)>=:anoAtual";
    	}
        if($anoPagamento != null) {
            $sql .= " and YEAR(dataPagamento) = :anoPagamento";
        }
        if($mesPagamento != null)
        {
            $sql .= " and MONTH(dataPagamento)=:mesPagamento";
        }
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        if($categoriasGLP!=null)
    	{
    		$sql .= " and categoriaDivida in (15,18,19,20) ";
    	}
        if($categoriasOutros!=null)
    	{
    		$sql .= " and categoriaDivida in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,21,22,23) ";
    	}
        if($idDivida!=null)
        {
            $sql .= " and idDivida=:idDivida ";
        }

        $sql .= " order by idDivida asc,idQuitacaoDivida asc";
    	// echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        // echo $c->parms('quitacaoDividaClass', $sql, [
        //     'idDivida' => $idDivida,
        //     'mesAtual' => $mesPagamento,
        //     'somenteAno' => $anoPagamento,
        //     'idOrganismoAfiliado' => $fk_idOrganismoAfiliado
        // ]);

        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($anoPagamento!=null)
    	{
            $sth->bindParam(':anoPagamento', $anoPagamento, PDO::PARAM_INT);
        }
        if($mesPagamento!=null)
        {
            $sth->bindValue(':mesPagamento', $mesPagamento, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($idDivida!=null)
        {
            $sth->bindParam(':idDivida', $idDivida, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    /*
     * #SOAM-340 
     * Método para listar todas as dividas quitadas parcialmente
     * para o cálculo mensal trazer o "valor restante" correto no relatório
     */

    public function retornaQuitacaoDividaPorDivida($fk_idOrganismoAfiliado, $fk_idDivida, $anoAtual, $mesAtual) {
        $sql = "SELECT * FROM quitacaoDivida left join divida on fk_idDivida = idDivida"
             . " where fk_idDivida = :idDivida"
             . " and YEAR(dataPagamento) <= :anoAtual and MONTH(dataPagamento) <= :mesAtual"
             . " and fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado"
             . " order by idDivida asc, idQuitacaoDivida asc";
    
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idDivida', $fk_idDivida, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaQuitacaoDivida($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT SUM(REPLACE( REPLACE(valorPagamento, '.' ,'' ), ',', '.' )) as total FROM quitacaoDivida "
                . "inner join divida on fk_idDivida = idDivida "
                . "where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(inicioDivida)<=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(inicioDivida)<=:anoAtual";
    	}
		if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(fimDivida)>=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(fimDivida)>=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        
        $total=0;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $vetor)
        	{
        		$total = $vetor['total'];
        	}
            return $total;
        } else {
            return false;
        }
    }
    
    public function removeQuitacaoDivida($id) {
        
        $sql = "DELETE FROM quitacaoDivida WHERE `idQuitacaoDivida` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdQuitacaoDivida($id) {
        
        $sql = "SELECT * FROM quitacaoDivida 
                                 WHERE `idQuitacaoDivida` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $retorno = $c->run($sth);
        if ($retorno){
            return $retorno;
        }else{
            return false;
        }
    }
}
?>