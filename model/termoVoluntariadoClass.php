<?php

require_once ("conexaoClass.php");

class termoVoluntariado {

    private $idTermoVoluntariado;
    private $fk_idOrganismoAfiliado;
    private $dataTermoVoluntariado;
    private $codigoAfiliacao;
    private $nomeVoluntario;
    private $cpf;
    private $rg;
    private $profissao;
    private $nacionalidade;
    private $email;
    private $logradouro;
    private $numero;
    private $complemento;
    private $bairro;
    private $cep;
    private $cidade;
    private $uf;
    private $telefoneResidencial;
    private $telefoneComercial;
    private $celular;
    private $areaTrabalhoVoluntario;
    private $tarefaEspecifica;
    private $diasSemanaVoluntariado;
    private $anoInicial;
    private $anoFinal;
    private $usuario;

    //METODO SET/GET -----------------------------------------------------------

    function getIdTermoVoluntariado() {
        return $this->idTermoVoluntariado;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getDataTermoVoluntariado() {
        return $this->dataTermoVoluntariado;
    }

    function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }

    function getNomeVoluntario() {
        return $this->nomeVoluntario;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getRg() {
        return $this->rg;
    }

    function getProfissao() {
        return $this->profissao;
    }

    function getNacionalidade() {
        return $this->nacionalidade;
    }

    function getEmail() {
        return $this->email;
    }

    function getLogradouro() {
        return $this->logradouro;
    }

    function getNumero() {
        return $this->numero;
    }

    function getComplemento() {
        return $this->complemento;
    }

    function getBairro() {
        return $this->bairro;
    }

    function getCep() {
        return $this->cep;
    }

    function getCidade() {
        return $this->cidade;
    }

    function getUf() {
        return $this->uf;
    }

    function getTelefoneResidencial() {
        return $this->telefoneResidencial;
    }

    function getTelefoneComercial() {
        return $this->telefoneComercial;
    }

    function getCelular() {
        return $this->celular;
    }

    function getAreaTrabalhoVoluntario() {
        return $this->areaTrabalhoVoluntario;
    }

    function getTarefaEspecifica() {
        return $this->tarefaEspecifica;
    }

    function getDiasSemanaVoluntariado() {
        return $this->diasSemanaVoluntariado;
    }

    function getAnoInicial() {
        return $this->anoInicial;
    }

    function getAnoFinal() {
        return $this->anoFinal;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setIdTermoVoluntariado($idTermoVoluntariado) {
        $this->idTermoVoluntariado = $idTermoVoluntariado;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setDataTermoVoluntariado($dataTermoVoluntariado) {
        $this->dataTermoVoluntariado = $dataTermoVoluntariado;
    }

    function setNomeVoluntario($nomeVoluntario) {
        $this->nomeVoluntario = $nomeVoluntario;
    }

    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setRg($rg) {
        $this->rg = $rg;
    }

    function setProfissao($profissao) {
        $this->profissao = $profissao;
    }

    function setNacionalidade($nacionalidade) {
        $this->nacionalidade = $nacionalidade;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    function setNumero($numero) {
        $this->numero = $numero;
    }

    function setComplemento($complemento) {
        $this->complemento = $complemento;
    }

    function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    function setCep($cep) {
        $this->cep = $cep;
    }

    function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    function setUf($uf) {
        $this->uf = $uf;
    }

    function setTelefoneResidencial($telefoneResidencial) {
        $this->telefoneResidencial = $telefoneResidencial;
    }

    function setTelefoneComercial($telefoneComercial) {
        $this->telefoneComercial = $telefoneComercial;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setAreaTrabalhoVoluntario($areaTrabalhoVoluntario) {
        $this->areaTrabalhoVoluntario = $areaTrabalhoVoluntario;
    }

    function setTarefaEspecifica($tarefaEspecifica) {
        $this->tarefaEspecifica = $tarefaEspecifica;
    }

    function setDiasSemanaVoluntariado($diasSemanaVoluntariado) {
        $this->diasSemanaVoluntariado = $diasSemanaVoluntariado;
    }

    function setAnoInicial($anoInicial) {
        $this->anoInicial = $anoInicial;
    }

    function setAnoFinal($anoFinal) {
        $this->anoFinal = $anoFinal;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    //--------------------------------------------------------------------------

    public function cadastroTermoVoluntariado() {

        $sql = "INSERT INTO termoVoluntariado  
                                 (
                                  fk_idOrganismoAfiliado,
                                  dataTermoVoluntariado,
                                  nomeVoluntario,
                                  codigoAfiliacao,
                                  cpf,
                                  rg,
                                  profissao,
                                  nacionalidade,
                                  email,
                                  logradouro,
                                  numero,
                                  complemento,
                                  bairro,
                                  cep,
                                  cidade,
                                  uf,
                                  telefoneResidencial,
                                  telefoneComercial,
                                  celular,
                                  areaTrabalhoVoluntario,
                                  tarefaEspecifica,
                                  diasSemanaVoluntariado,
                                  anoInicial,
                                  anoFinal,
                                  usuario,
                                  dataCadastro
                                  )
                                   
                            VALUES (
                                    :fk_idOrganismoAfiliado,
                                    :dataTermoVoluntariado,    
                                    :nomeVoluntario,
                                    :codigoAfiliacao,
                                    :cpf,
                                    :rg,
                                    :profissao,
                                    :nacionalidade,
                                    :email,
                                    :logradouro,
                                    :numero,
                                    :complemento,
                                    :bairro,
                                    :cep,
                                    :cidade,
                                    :uf,
                                    :telefoneResidencial,
                                    :telefoneComercial,
                                    :celular,
                                    :areaTrabalhoVoluntario,
                                    :tarefaEspecifica,
                                    :diasSemanaVoluntariado,
                                    :anoInicial,
                                    :anoFinal,
                                    :usuario,
                                    now())";
        //echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFk_idOrganismoAfiliado      = $this->getFk_idOrganismoAfiliado();
        $getDataTermoVoluntariado       = $this->getDataTermoVoluntariado();
        $getNomeVoluntario              = $this->getNomeVoluntario();
        $getCodigoAfiliacao             = $this->getCodigoAfiliacao();
        $getCpf                         = $this->getCpf();
        $getRg                          = $this->getRg();
        $getProfissao                   = $this->getProfissao();
        $getNacionalidade               = $this->getNacionalidade();
        $getEmail                       = $this->getEmail();
        $getLogradouro                  = $this->getLogradouro();
        $getNumero                      = $this->getNumero();
        $getComplemento                 = $this->getComplemento();
        $getBairro                      = $this->getBairro();
        $getCep                         = $this->getCep();
        $getCidade                      = $this->getCidade();
        $getUf                          = $this->getUf();
        $getTelefoneResidencial         = $this->getTelefoneResidencial();
        $getTelefoneComercial           = $this->getTelefoneComercial();
        $getCelular                     = $this->getCelular();
        $getAreaTrabalhoVoluntario      = $this->getAreaTrabalhoVoluntario();
        $getTarefaEspecifica            = $this->getTarefaEspecifica();
        $getDiasSemanaVoluntariado      = $this->getDiasSemanaVoluntariado();
        $getAnoInicial                  = $this->getAnoInicial();
        $getAnoFinal                    = $this->getAnoFinal();
        $getUsuario                     = $this->getUsuario();

        $sth->bindParam(':fk_idOrganismoAfiliado', $getFk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataTermoVoluntariado', $getDataTermoVoluntariado, PDO::PARAM_STR);
        $sth->bindParam(':nomeVoluntario', $getNomeVoluntario, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $getCodigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':cpf', $getCpf, PDO::PARAM_STR);
        $sth->bindParam(':rg', $getRg, PDO::PARAM_STR);
        $sth->bindParam(':profissao', $getProfissao, PDO::PARAM_STR);
        $sth->bindParam(':nacionalidade', $getNacionalidade, PDO::PARAM_STR);
        $sth->bindParam(':email', $getEmail, PDO::PARAM_STR);
        $sth->bindParam(':logradouro', $getLogradouro, PDO::PARAM_STR);
        $sth->bindParam(':numero', $getNumero, PDO::PARAM_INT);
        $sth->bindParam(':complemento', $getComplemento, PDO::PARAM_STR);
        $sth->bindParam(':bairro', $getBairro, PDO::PARAM_STR);
        $sth->bindParam(':cep', $getCep, PDO::PARAM_STR);
        $sth->bindParam(':cidade', $getCidade, PDO::PARAM_STR);
        $sth->bindParam(':uf', $getUf, PDO::PARAM_STR);
        $sth->bindParam(':telefoneResidencial', $getTelefoneResidencial, PDO::PARAM_STR);
        $sth->bindParam(':telefoneComercial', $getTelefoneComercial, PDO::PARAM_STR);
        $sth->bindParam(':celular', $getCelular, PDO::PARAM_STR);
        $sth->bindParam(':areaTrabalhoVoluntario', $getAreaTrabalhoVoluntario, PDO::PARAM_STR);
        $sth->bindParam(':tarefaEspecifica', $getTarefaEspecifica, PDO::PARAM_STR);
        $sth->bindParam(':diasSemanaVoluntariado', $getDiasSemanaVoluntariado, PDO::PARAM_STR);
        $sth->bindParam(':anoInicial', $getAnoInicial, PDO::PARAM_INT);
        $sth->bindParam(':anoFinal', $getAnoFinal, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $getUsuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql = "SELECT MAX(idTermoVoluntariado) as lastid FROM termoVoluntariado";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                    $lastid = $vetor['lastid']; 
            }
            return $lastid;
        }else{
            return false;
        }
    }

    public function alteraTermoVoluntariado($idTermoVoluntariado) {

        $sql = "UPDATE termoVoluntariado SET  
                                    dataTermoVoluntariado = :dataTermoVoluntariado,    
                                    nomeVoluntario = :nomeVoluntario,
                                    codigoAfiliacao = :codigoAfiliacao,
                                    cpf = :cpf,
                                    rg = :rg,
                                    profissao = :profissao,
                                    nacionalidade = :nacionalidade,
                                    email = :email,
                                    logradouro = :logradouro,
                                    numero = :numero,
                                    complemento = :complemento,
                                    bairro = :bairro,
                                    cep = :cep,
                                    cidade = :cidade,
                                    uf = :uf,
                                    telefoneResidencial = :telefoneResidencial,
                                    telefoneComercial = :telefoneComercial,
                                    celular = :celular,
                                    areaTrabalhoVoluntario = :areaTrabalhoVoluntario,
                                    tarefaEspecifica =:tarefaEspecifica,
                                    diasSemanaVoluntariado = :diasSemanaVoluntariado,
                                    anoInicial = :anoInicial,
                                    anoFinal = :anoFinal
                                  WHERE idTermoVoluntariado = :idTermoVoluntariado";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getDataTermoVoluntariado       = $this->getDataTermoVoluntariado();
        $getNomeVoluntario              = $this->getNomeVoluntario();
        $getCodigoAfiliacao             = $this->getCodigoAfiliacao();
        $getCpf                         = $this->getCpf();
        $getRg                          = $this->getRg();
        $getProfissao                   = $this->getProfissao();
        $getNacionalidade               = $this->getNacionalidade();
        $getEmail                       = $this->getEmail();
        $getLogradouro                  = $this->getLogradouro();
        $getNumero                      = $this->getNumero();
        $getComplemento                 = $this->getComplemento();
        $getBairro                      = $this->getBairro();
        $getCep                         = $this->getCep();
        $getCidade                      = $this->getCidade();
        $getUf                          = $this->getUf();
        $getTelefoneResidencial         = $this->getTelefoneResidencial();
        $getTelefoneComercial           = $this->getTelefoneComercial();
        $getCelular                     = $this->getCelular();
        $getAreaTrabalhoVoluntario      = $this->getAreaTrabalhoVoluntario();
        $getTarefaEspecifica            = $this->getTarefaEspecifica();
        $getDiasSemanaVoluntariado      = $this->getDiasSemanaVoluntariado();
        $getAnoInicial                  = $this->getAnoInicial();
        $getAnoFinal                    = $this->getAnoFinal();

        $sth->bindParam(':dataTermoVoluntariado', $getDataTermoVoluntariado, PDO::PARAM_STR);
        $sth->bindParam(':nomeVoluntario', $getNomeVoluntario, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $getCodigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':cpf', $getCpf, PDO::PARAM_STR);
        $sth->bindParam(':rg', $getRg, PDO::PARAM_STR);
        $sth->bindParam(':profissao', $getProfissao, PDO::PARAM_STR);
        $sth->bindParam(':nacionalidade', $getNacionalidade, PDO::PARAM_STR);
        $sth->bindParam(':email', $getEmail, PDO::PARAM_STR);
        $sth->bindParam(':logradouro', $getLogradouro, PDO::PARAM_STR);
        $sth->bindParam(':numero', $getNumero, PDO::PARAM_INT);
        $sth->bindParam(':complemento', $getComplemento, PDO::PARAM_STR);
        $sth->bindParam(':bairro', $getBairro, PDO::PARAM_STR);
        $sth->bindParam(':cep', $getCep, PDO::PARAM_STR);
        $sth->bindParam(':cidade', $getCidade, PDO::PARAM_STR);
        $sth->bindParam(':uf', $getUf, PDO::PARAM_STR);
        $sth->bindParam(':telefoneResidencial', $getTelefoneResidencial, PDO::PARAM_STR);
        $sth->bindParam(':telefoneComercial', $getTelefoneComercial, PDO::PARAM_STR);
        $sth->bindParam(':celular', $getCelular, PDO::PARAM_STR);
        $sth->bindParam(':areaTrabalhoVoluntario', $getAreaTrabalhoVoluntario, PDO::PARAM_STR);
        $sth->bindParam(':tarefaEspecifica', $getTarefaEspecifica, PDO::PARAM_STR);
        $sth->bindParam(':diasSemanaVoluntariado', $getDiasSemanaVoluntariado, PDO::PARAM_STR);
        $sth->bindParam(':anoInicial', $getAnoInicial, PDO::PARAM_INT);
        $sth->bindParam(':anoFinal', $getAnoFinal, PDO::PARAM_INT);
        $sth->bindParam(':idTermoVoluntariado', $idTermoVoluntariado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {

            return false;
        };
    }

    public function listaTermoVoluntariado() {

        $sql = "SELECT * FROM termoVoluntariado
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function verificaSeJaExiste() {

        $sql = "SELECT * FROM termoVoluntariado where 1=1 
    			and nomeVoluntario = :nomeVolunario
                        and fk_idOrganismoAfiliado =  :idOrganismoAfiliado
                        and anoInicial = :anoInicial
			and anoFinal = :anoFinal
                ";
        
        //echo $sql;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getNomeVoluntario                    = $this->getNomeVoluntario();
        $getFk_idOrganismoAfiliado            = $this->getFk_idOrganismoAfiliado();
        $getAnoInicial                        = $this->getAnoInicial();
        $getAnoFinal                          = $this->getAnoFinal();

        $sth->bindParam(':nomeVolunario', $getNomeVoluntario, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $getFk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':anoInicial', $getAnoInicial, PDO::PARAM_INT);
        $sth->bindParam(':anoFinal', $getAnoFinal, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdTermoVoluntariado($idTermoVoluntariado) {
        
        $sql = "SELECT * FROM  termoVoluntariado as a
        	inner join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                WHERE   idTermoVoluntariado = :id";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idTermoVoluntariado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeTermoVoluntariado($idTermoVoluntariado) {
        
        $sql = "DELETE FROM termoVoluntariado WHERE idTermoVoluntariado = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idTermoVoluntariado, PDO::PARAM_INT);

        $resultado = $c->run($sth,true);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>
