<?php

require_once ("conexaoClass.php");

class planoAcaoRegiaoParticipante{

    private $idPlanoAcaoRegiaoParticipante;
    private $fk_idPlanoAcaoRegiao;
    private $seqCadast;
    
    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoAcaoRegiaoParticipante() {
        return $this->idPlanoAcaoRegiaoParticipante;
    }

    function getFk_idPlanoAcaoRegiao() {
        return $this->fk_idPlanoAcaoRegiao;
    }
    
	function getSeqCadast() {
        return $this->seqCadast;
    }

    function setIdPlanoAcaoRegiaoParticipante($idPlanoAcaoRegiaoParticipante) {
        $this->idPlanoAcaoRegiaoParticipante = $idPlanoAcaoRegiaoParticipante;
    }
  
	function setFk_idPlanoAcaoRegiao($fk_idPlanoAcaoRegiao) {
        $this->fk_idPlanoAcaoRegiao = $fk_idPlanoAcaoRegiao;
    }
    
	function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoAcaoRegiaoParticipante(){

        $sql="INSERT INTO planoAcaoRegiao_participante  
                                 (
                                  fk_idPlanoAcaoRegiao,
                                  seqCadast
                                  )
                                   
                            VALUES (:idPlanoAcaoRegiao,
                                    :seqCadast)";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoRegiao', $this->fk_idPlanoAcaoRegiao, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function listaParticipantes($idPlanoAcaoRegiao=null,$seqCadast=null){

    	$sql = "SELECT * FROM planoAcaoRegiao_participante
    	where 1=1 
    	 ";   
    	if($idPlanoAcaoRegiao!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoRegiao=:idPlanoAcaoRegiao";
    	}
    	if($seqCadast!=null)
    	{
    		$sql .= " and seqCadast=:seqCadast";
    	}	 
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoRegiao!=null)
    	{
            $sth->bindParam(':idPlanoAcaoRegiao', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        }
        if($seqCadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeParticipantes($idPlanoAcaoRegiao=null,$id=null)
    {
        
    	$sql = "DELETE FROM planoAcaoRegiao_participante WHERE 1=1";
        
    	if($idPlanoAcaoRegiao!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoRegiao = :idPlanoAcaoRegiao";
    	}
    	if($id!=null)
    	{
    		$sql .= " and idPlanoAcaoRegiaoParticipante = :id";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idPlanoAcaoRegiao!=null)
    	{
            $sth->bindParam(':idPlanoAcaoRegiao', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        }
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }   
}

?>
