<?php
require_once ("consultaClass.php");

class Departamento {

    public $idDepartamento;
    public $nomeDepartamento;
    public $descricaoDepartamento;
    public $vinculoExterno;
    public $statusDepartamento;

    /* Funções GET e SET */

    function getIdDepartamento() {
        return $this->idDepartamento;
    }

    function getNomeDepartamento() {
        return $this->nomeDepartamento;
    }

    function getDescricaoDepartamento() {
        return $this->descricaoDepartamento;
    }

    function getVinculoExterno() {
        return $this->vinculoExterno;
    }
    
	function getStatusDepartamento() {
        return $this->statusDepartamento;
    }

    function setIdDepartamento($idDepartamento) {
        $this->idDepartamento = $idDepartamento;
    }

    function setNomeDepartamento($nomeDepartamento) {
        $this->nomeDepartamento = $nomeDepartamento;
    }

    function setDescricaoDepartamento($descricaoDepartamento) {
        $this->descricaoDepartamento = $descricaoDepartamento;
    }

    function setVinculoExterno($vinculoExterno) {
        $this->vinculoExterno = $vinculoExterno;
    }
    
	function setStatusDepartamento($statusDepartamento) {
        $this->statusDepartamento = $statusDepartamento;
    }

    public function cadastraDepartamento() {
        
        $sql = "INSERT INTO departamento (`idDepartamento`, `nomeDepartamento`, `descricaoDepartamento`, `vinculoExterno`)
                                    VALUES (:idDepartamento,
                                            :nome,
                                            :descricao,
                                            :vinculoExterno)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nomeDepartamento, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':vinculoExterno', $this->vinculoExterno, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraDepartamento($id) {

        $sql ="UPDATE departamento SET `nomeDepartamento` = :nome,
                                                 `descricaoDepartamento` = :descricao,
                                                 `vinculoExterno` = :vinculoExterno
                            WHERE `idDepartamento` = :idDepartamento";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nome', $this->nomeDepartamento, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':vinculoExterno', $this->vinculoExterno, PDO::PARAM_INT);
        $sth->bindParam(':idDepartamento', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusDepartamento() {
        
	$sql = "UPDATE departamento SET `statusDepartamento` = :status
                               WHERE `idDepartamento` = :idDepartamento";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idDepartamento', $this->idDepartamento, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->statusDepartamento;
            return $arr;
        } else {
            return false;
        }
    }

    public function listaDepartamento($ativo=null) {
        
    	$sql = "SELECT * FROM departamento where 1=1 ";
    	if($ativo==1)
    	{
    		$sql .= " and statusDepartamento = 0 ";
    	}
    	$sql .= " ORDER BY `nomeDepartamento` ASC";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeDepartamento($idDepartamento) {
        
        $sql = "DELETE FROM departamento WHERE `nomeDepartamento` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idDepartamento, PDO::PARAM_INT);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }

    public function buscaNomeDepartamento() {
        
        $sql = "SELECT * FROM departamento
                                 WHERE `nomeDepartamento` LIKE :nome
                              ORDER BY `nomeDepartamento` ASC";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $nome = '%'.$this->nomeDepartamento.'%';
        $sth->bindParam(':nome', $nome, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdDepartamento() {
        
        $sql = "SELECT * FROM departamento 
                                 WHERE `idDepartamento` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {    
            $sth->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}
?>