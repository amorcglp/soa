<?php

require_once ("conexaoClass.php");

class Chat {

    private $idChat;
    private $fromChat;
    private $paraChat;
    private $mensagemChat;
    private $novaChat;
    
    function getIdChat() {
        return $this->idChat;
    }

    function getFromChat() {
        return $this->fromChat;
    }
    
    function getParaChat() {
        return $this->paraChat;
    }
    
	function getMensagemChat() {
        return $this->mensagemChat;
    }
    
	function getNovaChat() {
        return $this->novaChat;
    }
    
	function setIdChat($idChat) {
        $this->idChat = $idChat;
    }

    function setFromChat($fromChat) {
        $this->fromChat = $fromChat;
    }
    
	function setParaChat($paraChat) {
        $this->paraChat = $paraChat;
    }
    
	function setMensagemChat($mensagemChat) {
        $this->mensagemChat = $mensagemChat;
    }

    # Seleciona Evento
    public function selecionaChat() 
    {	
        $msgs=array();
        $sql = "SELECT * FROM chat WHERE 
                                (de = :from and para =:para
                                or
                                 de = :para and para =:from)
                        order by data asc;";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':from', $this->fromChat, PDO::PARAM_INT);
        $sth->bindParam(':para', $this->paraChat, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        //echo $sql;
        if ($resultado) {
                $contagem = count($resultado);
                //selecionar novamente agora trazendo as ultimas 6 mensagens
                $inicio = $contagem-4;
                if($inicio<0)
                {
                        $inicio=0;
                }
                $sql2 = "SELECT * FROM chat WHERE 
                        (de = :from and para =:para
                         or
                         de = :para and para =:from)
                order by idChat desc limit :limit,4";
                
                $c2 = new conexaoSOA(); 
                $con2 = $c2->openSOA();
                $sth2 = $con2->prepare($sql2);

                $sth2->bindParam(':from', $this->fromChat, PDO::PARAM_INT);
                $sth2->bindParam(':para', $this->paraChat, PDO::PARAM_INT);
                $sth2->bindParam(':limit', $inicio, PDO::PARAM_INT);
                
                $resultado2 = $c->run($sth2);
                if ($resultado2) {
                        foreach ($resultado as $vetor) {
                                        $msgs['idChat'][] 		= $vetor['idChat'];
                                        $msgs['from'][] 		= $vetor['de'];
                                        $msgs['para'][] 		= $vetor['para'];
                                        $msgs['mensagem'][]             = $vetor['mensagem'];
                                        $msgs['data'][] 		= substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)."/".substr($vetor['data'],0,4)." ".substr($vetor['data'],10,6);			
                        }
                }
        }
        return $msgs;	
    }

    function getNomeDestino()
    {
        $nome="";
        $sql = "SELECT * FROM usuario 
                                WHERE 
                                seqCadast = :para";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':para', $this->paraChat, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado) {
                foreach ($resultado as $vetor) {
                        $arrNome 		= explode(" ",$vetor['nomeUsuario']);
                        //$nome 			= $arrNome[0];//primeiro nome
                        $nome 			= $vetor['nomeUsuario'];//primeiro nome
                }
        }
        return $nome;
    }

    function apagarNovas()
    {

        $sql = "UPDATE chat SET nova='0' WHERE de=:para and para = :from";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':para', $this->paraChat, PDO::PARAM_INT);
        $sth->bindParam(':from',$this->fromChat , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    function cadastraMensagem()
    {
        $sql = "INSERT INTO chat (de,para,mensagem,data,nova)
                                VALUES 		(:from,
                                                        :para,
                                                        :msg,
                                                        now(),
                                                        1
                                                        )";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':para', $this->paraChat, PDO::PARAM_INT);
        $sth->bindParam(':from',$this->fromChat , PDO::PARAM_INT);
        $sth->bindParam(':msg',$this->mensagemChat , PDO::PARAM_STR);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    function getTotalNovas()
    {
        $contagem=0;
        $sql = "SELECT * FROM chat 
                                WHERE 
                                nova=1
                                and de=:para 
                                and para = :from
                                ";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':para', $this->paraChat, PDO::PARAM_INT);
        $sth->bindParam(':from',$this->fromChat , PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado) {
                $contagem = count($resultado['idChat']);
        }
        return $contagem;
    }
}

?>