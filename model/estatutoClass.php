<?php

require_once ("conexaoClass.php");

class estatuto{

    private $idEstatuto;
    
    private $fk_idOrganismoAfiliado;
    
    private $numeroOficio;
    private $cidadeOficio;
    private $ufOficio;
    private $nomeAdvogado;
    private $numeroOAB;
    
    private $status;
    private $motivoStatus;
    
    private $cartorioPronaos;
    private $comarcaPronaos;
    private $numeroPronaos;
    private $numeroAverbacaoPronaos;
    private $numeroLeiMunicipalPronaos;
    private $dataLeiMunicipalPronaos;
    private $numeroLeiEstadualPronaos;
    private $dataLeiEstadualPronaos;
    
    private $cartorioCapitulo;
    private $comarcaCapitulo;
    private $numeroCapitulo;
    private $numeroAverbacaoCapitulo;
    private $numeroLeiMunicipalCapitulo;
    private $dataLeiMunicipalCapitulo;
    private $numeroLeiEstadualCapitulo;
    private $dataLeiEstadualCapitulo;
    
    private $cartorioLoja;
    private $comarcaLoja;
    private $numeroLoja;
    private $numeroAverbacaoLoja;
    private $numeroLeiMunicipalLoja;
    private $dataLeiMunicipalLoja;
    private $numeroLeiEstadualLoja;
    private $dataLeiEstadualLoja;
    
    private $dataAssembleiaGeral;
    private $numeroPresentes;
    
    private $usuario;
    private $ultimoAtualizar;

    /**
     * @return mixed
     */
    public function getIdEstatuto()
    {
        return $this->idEstatuto;
    }

    /**
     * @param mixed $idEstatuto
     */
    public function setIdEstatuto($idEstatuto)
    {
        $this->idEstatuto = $idEstatuto;
    }

    /**
     * @return mixed
     */
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }

    /**
     * @param mixed $fk_idOrganismoAfiliado
     */
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getNumeroOficio()
    {
        return $this->numeroOficio;
    }

    /**
     * @param mixed $numeroOficio
     */
    public function setNumeroOficio($numeroOficio)
    {
        $this->numeroOficio = $numeroOficio;
    }

    /**
     * @return mixed
     */
    public function getCidadeOficio()
    {
        return $this->cidadeOficio;
    }

    /**
     * @param mixed $cidadeOficio
     */
    public function setCidadeOficio($cidadeOficio)
    {
        $this->cidadeOficio = $cidadeOficio;
    }

    /**
     * @return mixed
     */
    public function getUfOficio()
    {
        return $this->ufOficio;
    }

    /**
     * @param mixed $ufOficio
     */
    public function setUfOficio($ufOficio)
    {
        $this->ufOficio = $ufOficio;
    }

    /**
     * @return mixed
     */
    public function getNomeAdvogado()
    {
        return $this->nomeAdvogado;
    }

    /**
     * @param mixed $nomeAdvogado
     */
    public function setNomeAdvogado($nomeAdvogado)
    {
        $this->nomeAdvogado = $nomeAdvogado;
    }

    /**
     * @return mixed
     */
    public function getNumeroOAB()
    {
        return $this->numeroOAB;
    }

    /**
     * @param mixed $numeroOAB
     */
    public function setNumeroOAB($numeroOAB)
    {
        $this->numeroOAB = $numeroOAB;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMotivoStatus()
    {
        return $this->motivoStatus;
    }

    /**
     * @param mixed $motivoStatus
     */
    public function setMotivoStatus($motivoStatus)
    {
        $this->motivoStatus = $motivoStatus;
    }

    /**
     * @return mixed
     */
    public function getCartorioPronaos()
    {
        return $this->cartorioPronaos;
    }

    /**
     * @param mixed $cartorioPronaos
     */
    public function setCartorioPronaos($cartorioPronaos)
    {
        $this->cartorioPronaos = $cartorioPronaos;
    }

    /**
     * @return mixed
     */
    public function getComarcaPronaos()
    {
        return $this->comarcaPronaos;
    }

    /**
     * @param mixed $comarcaPronaos
     */
    public function setComarcaPronaos($comarcaPronaos)
    {
        $this->comarcaPronaos = $comarcaPronaos;
    }

    /**
     * @return mixed
     */
    public function getNumeroPronaos()
    {
        return $this->numeroPronaos;
    }

    /**
     * @param mixed $numeroPronaos
     */
    public function setNumeroPronaos($numeroPronaos)
    {
        $this->numeroPronaos = $numeroPronaos;
    }

    /**
     * @return mixed
     */
    public function getNumeroAverbacaoPronaos()
    {
        return $this->numeroAverbacaoPronaos;
    }

    /**
     * @param mixed $numeroAverbacaoPronaos
     */
    public function setNumeroAverbacaoPronaos($numeroAverbacaoPronaos)
    {
        $this->numeroAverbacaoPronaos = $numeroAverbacaoPronaos;
    }

    /**
     * @return mixed
     */
    public function getNumeroLeiMunicipalPronaos()
    {
        return $this->numeroLeiMunicipalPronaos;
    }

    /**
     * @param mixed $numeroLeiMunicipalPronaos
     */
    public function setNumeroLeiMunicipalPronaos($numeroLeiMunicipalPronaos)
    {
        $this->numeroLeiMunicipalPronaos = $numeroLeiMunicipalPronaos;
    }

    /**
     * @return mixed
     */
    public function getDataLeiMunicipalPronaos()
    {
        return $this->dataLeiMunicipalPronaos;
    }

    /**
     * @param mixed $dataLeiMunicipalPronaos
     */
    public function setDataLeiMunicipalPronaos($dataLeiMunicipalPronaos)
    {
        $this->dataLeiMunicipalPronaos = $dataLeiMunicipalPronaos;
    }

    /**
     * @return mixed
     */
    public function getNumeroLeiEstadualPronaos()
    {
        return $this->numeroLeiEstadualPronaos;
    }

    /**
     * @param mixed $numeroLeiEstadualPronaos
     */
    public function setNumeroLeiEstadualPronaos($numeroLeiEstadualPronaos)
    {
        $this->numeroLeiEstadualPronaos = $numeroLeiEstadualPronaos;
    }

    /**
     * @return mixed
     */
    public function getDataLeiEstadualPronaos()
    {
        return $this->dataLeiEstadualPronaos;
    }

    /**
     * @param mixed $dataLeiEstadualPronaos
     */
    public function setDataLeiEstadualPronaos($dataLeiEstadualPronaos)
    {
        $this->dataLeiEstadualPronaos = $dataLeiEstadualPronaos;
    }

    /**
     * @return mixed
     */
    public function getCartorioCapitulo()
    {
        return $this->cartorioCapitulo;
    }

    /**
     * @param mixed $cartorioCapitulo
     */
    public function setCartorioCapitulo($cartorioCapitulo)
    {
        $this->cartorioCapitulo = $cartorioCapitulo;
    }

    /**
     * @return mixed
     */
    public function getComarcaCapitulo()
    {
        return $this->comarcaCapitulo;
    }

    /**
     * @param mixed $comarcaCapitulo
     */
    public function setComarcaCapitulo($comarcaCapitulo)
    {
        $this->comarcaCapitulo = $comarcaCapitulo;
    }

    /**
     * @return mixed
     */
    public function getNumeroCapitulo()
    {
        return $this->numeroCapitulo;
    }

    /**
     * @param mixed $numeroCapitulo
     */
    public function setNumeroCapitulo($numeroCapitulo)
    {
        $this->numeroCapitulo = $numeroCapitulo;
    }

    /**
     * @return mixed
     */
    public function getNumeroAverbacaoCapitulo()
    {
        return $this->numeroAverbacaoCapitulo;
    }

    /**
     * @param mixed $numeroAverbacaoCapitulo
     */
    public function setNumeroAverbacaoCapitulo($numeroAverbacaoCapitulo)
    {
        $this->numeroAverbacaoCapitulo = $numeroAverbacaoCapitulo;
    }

    /**
     * @return mixed
     */
    public function getNumeroLeiMunicipalCapitulo()
    {
        return $this->numeroLeiMunicipalCapitulo;
    }

    /**
     * @param mixed $numeroLeiMunicipalCapitulo
     */
    public function setNumeroLeiMunicipalCapitulo($numeroLeiMunicipalCapitulo)
    {
        $this->numeroLeiMunicipalCapitulo = $numeroLeiMunicipalCapitulo;
    }

    /**
     * @return mixed
     */
    public function getDataLeiMunicipalCapitulo()
    {
        return $this->dataLeiMunicipalCapitulo;
    }

    /**
     * @param mixed $dataLeiMunicipalCapitulo
     */
    public function setDataLeiMunicipalCapitulo($dataLeiMunicipalCapitulo)
    {
        $this->dataLeiMunicipalCapitulo = $dataLeiMunicipalCapitulo;
    }

    /**
     * @return mixed
     */
    public function getNumeroLeiEstadualCapitulo()
    {
        return $this->numeroLeiEstadualCapitulo;
    }

    /**
     * @param mixed $numeroLeiEstadualCapitulo
     */
    public function setNumeroLeiEstadualCapitulo($numeroLeiEstadualCapitulo)
    {
        $this->numeroLeiEstadualCapitulo = $numeroLeiEstadualCapitulo;
    }

    /**
     * @return mixed
     */
    public function getDataLeiEstadualCapitulo()
    {
        return $this->dataLeiEstadualCapitulo;
    }

    /**
     * @param mixed $dataLeiEstadualCapitulo
     */
    public function setDataLeiEstadualCapitulo($dataLeiEstadualCapitulo)
    {
        $this->dataLeiEstadualCapitulo = $dataLeiEstadualCapitulo;
    }

    /**
     * @return mixed
     */
    public function getCartorioLoja()
    {
        return $this->cartorioLoja;
    }

    /**
     * @param mixed $cartorioLoja
     */
    public function setCartorioLoja($cartorioLoja)
    {
        $this->cartorioLoja = $cartorioLoja;
    }

    /**
     * @return mixed
     */
    public function getComarcaLoja()
    {
        return $this->comarcaLoja;
    }

    /**
     * @param mixed $comarcaLoja
     */
    public function setComarcaLoja($comarcaLoja)
    {
        $this->comarcaLoja = $comarcaLoja;
    }

    /**
     * @return mixed
     */
    public function getNumeroLoja()
    {
        return $this->numeroLoja;
    }

    /**
     * @param mixed $numeroLoja
     */
    public function setNumeroLoja($numeroLoja)
    {
        $this->numeroLoja = $numeroLoja;
    }

    /**
     * @return mixed
     */
    public function getNumeroAverbacaoLoja()
    {
        return $this->numeroAverbacaoLoja;
    }

    /**
     * @param mixed $numeroAverbacaoLoja
     */
    public function setNumeroAverbacaoLoja($numeroAverbacaoLoja)
    {
        $this->numeroAverbacaoLoja = $numeroAverbacaoLoja;
    }

    /**
     * @return mixed
     */
    public function getNumeroLeiMunicipalLoja()
    {
        return $this->numeroLeiMunicipalLoja;
    }

    /**
     * @param mixed $numeroLeiMunicipalLoja
     */
    public function setNumeroLeiMunicipalLoja($numeroLeiMunicipalLoja)
    {
        $this->numeroLeiMunicipalLoja = $numeroLeiMunicipalLoja;
    }

    /**
     * @return mixed
     */
    public function getDataLeiMunicipalLoja()
    {
        return $this->dataLeiMunicipalLoja;
    }

    /**
     * @param mixed $dataLeiMunicipalLoja
     */
    public function setDataLeiMunicipalLoja($dataLeiMunicipalLoja)
    {
        $this->dataLeiMunicipalLoja = $dataLeiMunicipalLoja;
    }

    /**
     * @return mixed
     */
    public function getNumeroLeiEstadualLoja()
    {
        return $this->numeroLeiEstadualLoja;
    }

    /**
     * @param mixed $numeroLeiEstadualLoja
     */
    public function setNumeroLeiEstadualLoja($numeroLeiEstadualLoja)
    {
        $this->numeroLeiEstadualLoja = $numeroLeiEstadualLoja;
    }

    /**
     * @return mixed
     */
    public function getDataLeiEstadualLoja()
    {
        return $this->dataLeiEstadualLoja;
    }

    /**
     * @param mixed $dataLeiEstadualLoja
     */
    public function setDataLeiEstadualLoja($dataLeiEstadualLoja)
    {
        $this->dataLeiEstadualLoja = $dataLeiEstadualLoja;
    }

    /**
     * @return mixed
     */
    public function getDataAssembleiaGeral()
    {
        return $this->dataAssembleiaGeral;
    }

    /**
     * @param mixed $dataAssembleiaGeral
     */
    public function setDataAssembleiaGeral($dataAssembleiaGeral)
    {
        $this->dataAssembleiaGeral = $dataAssembleiaGeral;
    }

    /**
     * @return mixed
     */
    public function getNumeroPresentes()
    {
        return $this->numeroPresentes;
    }

    /**
     * @param mixed $numeroPresentes
     */
    public function setNumeroPresentes($numeroPresentes)
    {
        $this->numeroPresentes = $numeroPresentes;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getUltimoAtualizar()
    {
        return $this->ultimoAtualizar;
    }

    /**
     * @param mixed $ultimoAtualizar
     */
    public function setUltimoAtualizar($ultimoAtualizar)
    {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }


    



            
    //--------------------------------------------------------------------------

    /**
     * [Realiza cadastro]
     * @return [boolean] [Em caso de sucesso no retorno do PDO retorna TRUE]
     */
    public function cadastroEstatuto($arrTipos){

        $sql = "INSERT INTO estatuto 
        (
        fk_idOrganismoAfiliado,
        numeroOficio,
        cidadeOficio,
        ufOficio,";

        if(in_array("P",$arrTipos)) {
            $sql .= "cartorioPronaos,
            comarcaPronaos,
            numeroPronaos,
            numeroAverbacaoPronaos,
            numeroLeiMunicipalPronaos,
            dataLeiMunicipalPronaos,
            numeroLeiEstadualPronaos,
            dataLeiEstadualPronaos,";
        }

        if(in_array("C",$arrTipos)) {
            $sql .= "cartorioCapitulo,
            comarcaCapitulo,
            numeroCapitulo,
            numeroAverbacaoCapitulo,
            numeroLeiMunicipalCapitulo,
            dataLeiMunicipalCapitulo,
            numeroLeiEstadualCapitulo,
            dataLeiEstadualCapitulo,";
        }

        if(in_array("L",$arrTipos)) {
            $sql .= "cartorioLoja,
            comarcaLoja,
            numeroLoja,
            numeroAverbacaoLoja,
            numeroLeiMunicipalLoja,
            dataLeiMunicipalLoja,
            numeroLeiEstadualLoja,
            dataLeiEstadualLoja,";
        }

        $sql .= "dataAssembleiaGeral,
        numeroPresentes,
        nomeAdvogado,
        numeroOAB,
        usuario,
        dataCadastro) 
        VALUES (
        :fk_idOrganismoAfiliado,
        :numeroOficio,
        :cidadeOficio,
        :ufOficio,";

        if(in_array("P",$arrTipos)) {
            $sql .= ":cartorioPronaos,
        :comarcaPronaos,
        :numeroPronaos,
        :numeroAverbacaoPronaos,
        :numeroLeiMunicipalPronaos,
        :dataLeiMunicipalPronaos,
        :numeroLeiEstadualPronaos,
        :dataLeiEstadualPronaos,";
        }

        if(in_array("C",$arrTipos)) {
        $sql .= ":cartorioCapitulo,
        :comarcaCapitulo,
        :numeroCapitulo,
        :numeroAverbacaoCapitulo,
        :numeroLeiMunicipalCapitulo,
        :dataLeiMunicipalCapitulo,
        :numeroLeiEstadualCapitulo,
        :dataLeiEstadualCapitulo,";
        }

        if(in_array("L",$arrTipos)) {
            $sql .= ":cartorioLoja,
            :comarcaLoja,
            :numeroLoja,
            :numeroAverbacaoLoja,
            :numeroLeiMunicipalLoja,
            :dataLeiMunicipalLoja,
            :numeroLeiEstadualLoja,
            :dataLeiEstadualLoja,";
        }

        $sql .= ":dataAssembleiaGeral,
        :numeroPresentes,
        :nomeAdvogado,
        :numeroOAB,
        :usuario,
        now()
        )";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':numeroOficio', $this->numeroOficio, PDO::PARAM_INT);
        $sth->bindParam(':cidadeOficio', $this->cidadeOficio, PDO::PARAM_STR);
        $sth->bindParam(':ufOficio', $this->ufOficio, PDO::PARAM_STR);
        if(in_array("P",$arrTipos)) {
            $sth->bindParam(':cartorioPronaos', $this->cartorioPronaos, PDO::PARAM_STR);
            $sth->bindParam(':comarcaPronaos', $this->comarcaPronaos, PDO::PARAM_STR);
            $sth->bindParam(':numeroPronaos', $this->numeroPronaos, PDO::PARAM_INT);
            $sth->bindParam(':numeroAverbacaoPronaos', $this->numeroAverbacaoPronaos, PDO::PARAM_INT);
            $sth->bindParam(':numeroLeiMunicipalPronaos', $this->numeroLeiMunicipalPronaos, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiMunicipalPronaos', $this->dataLeiMunicipalPronaos, PDO::PARAM_STR);
            $sth->bindParam(':numeroLeiEstadualPronaos', $this->numeroLeiEstadualPronaos, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiEstadualPronaos', $this->dataLeiEstadualPronaos, PDO::PARAM_STR);
        }
        if(in_array("C",$arrTipos)) {
            $sth->bindParam(':cartorioCapitulo', $this->cartorioCapitulo, PDO::PARAM_STR);
            $sth->bindParam(':comarcaCapitulo', $this->comarcaCapitulo, PDO::PARAM_STR);
            $sth->bindParam(':numeroCapitulo', $this->numeroCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':numeroAverbacaoCapitulo', $this->numeroAverbacaoCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':numeroLeiMunicipalCapitulo', $this->numeroLeiMunicipalCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiMunicipalCapitulo', $this->dataLeiMunicipalCapitulo, PDO::PARAM_STR);
            $sth->bindParam(':numeroLeiEstadualCapitulo', $this->numeroLeiEstadualCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiEstadualCapitulo', $this->dataLeiEstadualCapitulo, PDO::PARAM_STR);
        }
        if(in_array("L",$arrTipos)) {
            $sth->bindParam(':cartorioLoja', $this->cartorioLoja, PDO::PARAM_STR);
            $sth->bindParam(':comarcaLoja', $this->comarcaLoja, PDO::PARAM_STR);
            $sth->bindParam(':numeroLoja', $this->numeroLoja, PDO::PARAM_INT);
            $sth->bindParam(':numeroAverbacaoLoja', $this->numeroAverbacaoLoja, PDO::PARAM_INT);
            $sth->bindParam(':numeroLeiMunicipalLoja', $this->numeroLeiMunicipalLoja, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiMunicipalLoja', $this->dataLeiMunicipalLoja, PDO::PARAM_STR);
            $sth->bindParam(':numeroLeiEstadualLoja', $this->numeroLeiEstadualLoja, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiEstadualLoja', $this->dataLeiEstadualLoja, PDO::PARAM_STR);
        }
        $sth->bindParam(':dataAssembleiaGeral', $this->dataAssembleiaGeral, PDO::PARAM_STR);
        $sth->bindParam(':numeroPresentes', $this->numeroPresentes, PDO::PARAM_INT);
        $sth->bindParam(':nomeAdvogado', $this->nomeAdvogado, PDO::PARAM_STR);
        $sth->bindParam(':numeroOAB', $this->numeroOAB, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function listaEstatuto(){

        $sql = "SELECT * FROM estatuto "
                . " inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdEstatuto($idEstatuto){
        $sql = "SELECT * FROM  estatuto "
                . " inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado "
                . " WHERE   idEstatuto = :idEstatuto";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idEstatuto', $idEstatuto, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function atualizaEstatuto($idEstatuto,$arrTipos)
    {
        $sql = "UPDATE estatuto SET "
        . "
        numeroOficio = :numeroOficio,
        cidadeOficio = :cidadeOficio,
        ufOficio = :ufOficio,";

        if(in_array("P",$arrTipos)) {
            $sql .= "cartorioPronaos = :cartorioPronaos,
                comarcaPronaos = :comarcaPronaos,
                numeroPronaos = :numeroPronaos,
                numeroAverbacaoPronaos = :numeroAverbacaoPronaos,
                numeroLeiMunicipalPronaos = :numeroLeiMunicipalPronaos,
                dataLeiMunicipalPronaos = :dataLeiMunicipalPronaos,
                numeroLeiEstadualPronaos = :numeroLeiEstadualPronaos,
                dataLeiEstadualPronaos = :dataLeiEstadualPronaos,";
        }

        if(in_array("P",$arrTipos)) {
            $sql .= "cartorioCapitulo = :cartorioCapitulo,
                comarcaCapitulo = :comarcaCapitulo,
                numeroCapitulo = :numeroCapitulo,
                numeroAverbacaoCapitulo = :numeroAverbacaoCapitulo,
                numeroLeiMunicipalCapitulo = :numeroLeiMunicipalCapitulo,
                dataLeiMunicipalCapitulo = :dataLeiMunicipalCapitulo,
                numeroLeiEstadualCapitulo = :numeroLeiEstadualCapitulo,
                dataLeiEstadualCapitulo = :dataLeiEstadualCapitulo,";
        }

        if(in_array("P",$arrTipos)) {
            $sql .= "cartorioLoja = :cartorioLoja,
                comarcaLoja = :comarcaLoja,
                numeroLoja = :numeroLoja,
                numeroAverbacaoLoja = :numeroAverbacaoLoja, 
                numeroLeiMunicipalLoja = :numeroLeiMunicipalLoja,
                dataLeiMunicipalLoja = :dataLeiMunicipalLoja,
                numeroLeiEstadualLoja = :numeroLeiEstadualLoja,
                dataLeiEstadualLoja = :dataLeiEstadualLoja,";
        }

        $sql .= "dataAssembleiaGeral = :dataAssembleiaGeral,
            numeroPresentes = :numeroPresentes,
            nomeAdvogado = :nomeAdvogado,
            numeroOAB = :numeroOAB,
            ultimoAtualizar = :ultimoAtualizar
            WHERE idEstatuto= :idEstatuto
        ";

        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idEstatuto', $idEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':numeroOficio', $this->numeroOficio, PDO::PARAM_INT);
        $sth->bindParam(':cidadeOficio', $this->cidadeOficio, PDO::PARAM_STR);
        $sth->bindParam(':ufOficio', $this->ufOficio, PDO::PARAM_STR);

        if(in_array("P",$arrTipos)) {
            $sth->bindParam(':cartorioPronaos', $this->cartorioPronaos, PDO::PARAM_STR);
            $sth->bindParam(':comarcaPronaos', $this->comarcaPronaos, PDO::PARAM_STR);
            $sth->bindParam(':numeroPronaos', $this->numeroPronaos, PDO::PARAM_INT);
            $sth->bindParam(':numeroAverbacaoPronaos', $this->numeroAverbacaoPronaos, PDO::PARAM_INT);
            $sth->bindParam(':numeroLeiMunicipalPronaos', $this->numeroLeiMunicipalPronaos, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiMunicipalPronaos', $this->dataLeiMunicipalPronaos, PDO::PARAM_STR);
            $sth->bindParam(':numeroLeiEstadualPronaos', $this->numeroLeiEstadualPronaos, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiEstadualPronaos', $this->dataLeiEstadualPronaos, PDO::PARAM_STR);
        }

        if(in_array("C",$arrTipos)) {
            $sth->bindParam(':cartorioCapitulo', $this->cartorioCapitulo, PDO::PARAM_STR);
            $sth->bindParam(':comarcaCapitulo', $this->comarcaCapitulo, PDO::PARAM_STR);
            $sth->bindParam(':numeroCapitulo', $this->numeroCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':numeroAverbacaoCapitulo', $this->numeroAverbacaoCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':numeroLeiMunicipalCapitulo', $this->numeroLeiMunicipalCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiMunicipalCapitulo', $this->dataLeiMunicipalCapitulo, PDO::PARAM_STR);
            $sth->bindParam(':numeroLeiEstadualCapitulo', $this->numeroLeiEstadualCapitulo, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiEstadualCapitulo', $this->dataLeiEstadualCapitulo, PDO::PARAM_STR);
        }

        if(in_array("L",$arrTipos)) {
            $sth->bindParam(':cartorioLoja', $this->cartorioLoja, PDO::PARAM_STR);
            $sth->bindParam(':comarcaLoja', $this->comarcaLoja, PDO::PARAM_STR);
            $sth->bindParam(':numeroLoja', $this->numeroLoja, PDO::PARAM_INT);
            $sth->bindParam(':numeroAverbacaoLoja', $this->numeroAverbacaoLoja, PDO::PARAM_INT);
            $sth->bindParam(':numeroLeiMunicipalLoja', $this->numeroLeiMunicipalLoja, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiMunicipalLoja', $this->dataLeiMunicipalLoja, PDO::PARAM_STR);
            $sth->bindParam(':numeroLeiEstadualLoja', $this->numeroLeiEstadualLoja, PDO::PARAM_INT);
            $sth->bindParam(':dataLeiEstadualLoja', $this->dataLeiEstadualLoja, PDO::PARAM_STR);
        }

        $sth->bindParam(':dataAssembleiaGeral', $this->dataAssembleiaGeral, PDO::PARAM_STR);
        $sth->bindParam(':numeroPresentes', $this->numeroPresentes, PDO::PARAM_INT);
        $sth->bindParam(':nomeAdvogado', $this->nomeAdvogado, PDO::PARAM_STR);
        $sth->bindParam(':numeroOAB', $this->numeroOAB, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    } 
    
    public function verificaSeJaExiste() {

        $sql = "SELECT * FROM estatuto where 1=1
        and fk_idOrganismoAfiliado =  :fk_idOrganismoAfiliado";
        
        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function alteraStatusEstatuto() {
        
        $sql = "UPDATE estatuto SET `status` = :status, motivoStatus = :motivoStatus"
                . "                WHERE `idEstatuto` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
      
        $sth->bindParam(':status', $this->getStatus(), PDO::PARAM_INT);
        $sth->bindParam(':motivoStatus', $this->getMotivoStatus(), PDO::PARAM_INT);
        $sth->bindParam(':id', $this->getIdEstatuto(), PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

}

?>
