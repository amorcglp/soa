<?php

require_once ("conexaoClass.php");
require_once ("recebimentoClass.php");
require_once ("despesaClass.php");
require_once ("valoresDoMesClass.php");

class saldoFinanceiro{

    private $idSaldoFinanceiro;
    private $mes;
    private $ano;
    private $dataSaldoFinanceiro;
    private $fk_idOrganismoAfiliado;
    private $saldoAnterior;
    private $totalEntradas;
    private $totalSaidas;
    private $saldoMes;
    private $valoresDoMes;
    private $recebimento;
    private $despesa;

    function __construct() {

        $this->valoresDoMes = new valoresDoMes();
        $this->recebimento = new Recebimento();
        $this->despesa = new Despesa();
    }

    /**
     * @return mixed
     */
    public function getIdSaldoFinanceiro()
    {
        return $this->idSaldoFinanceiro;
    }

    /**
     * @param mixed $idSaldoFinanceiro
     */
    public function setIdSaldoFinanceiro($idSaldoFinanceiro)
    {
        $this->idSaldoFinanceiro = $idSaldoFinanceiro;
    }

    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @param mixed $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getDataSaldoFinanceiro()
    {
        return $this->dataSaldoFinanceiro;
    }

    /**
     * @param mixed $dataSaldoFinanceiro
     */
    public function setDataSaldoFinanceiro($dataSaldoFinanceiro)
    {
        $this->dataSaldoFinanceiro = $dataSaldoFinanceiro;
    }

    /**
     * @return mixed
     */
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }

    /**
     * @param mixed $fk_idOrganismoAfiliado
     */
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getSaldoAnterior()
    {
        return $this->saldoAnterior;
    }

    /**
     * @param mixed $saldoAnterior
     */
    public function setSaldoAnterior($saldoAnterior)
    {
        $this->saldoAnterior = $saldoAnterior;
    }

    /**
     * @return mixed
     */
    public function getTotalEntradas()
    {
        return $this->totalEntradas;
    }

    /**
     * @param mixed $totalEntradas
     */
    public function setTotalEntradas($totalEntradas)
    {
        $this->totalEntradas = $totalEntradas;
    }

    /**
     * @return mixed
     */
    public function getTotalSaidas()
    {
        return $this->totalSaidas;
    }

    /**
     * @param mixed $totalSaidas
     */
    public function setTotalSaidas($totalSaidas)
    {
        $this->totalSaidas = $totalSaidas;
    }

    /**
     * @return mixed
     */
    public function getSaldoMes()
    {
        return $this->saldoMes;
    }

    /**
     * @param mixed $saldoMes
     */
    public function setSaldoMes($saldoMes)
    {
        $this->saldoMes = $saldoMes;
    }



    public function cadastroSaldoFinanceiro(){

    	$sql = "INSERT INTO saldoFinanceiro  
                                 (mes,
                                  ano,
                                  dataSaldoFinanceiro,
                                  fk_idOrganismoAfiliado,
                                  saldoAnterior,
                                  totalEntradas,
                                  totalSaidas,
                                  saldoMes,
                                  dataCadastro
                                  )
                            VALUES (
                                    :mes,
                                    :ano,
                                    :dataSaldoFinanceiro,
                            		:idOrganismoAfiliado,
                            		:saldoAnterior,
                            		:totalEntradas,
                            		:totalSaidas,
                            		:saldoMes,
                                    now()
                                    )";
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':dataSaldoFinanceiro', $this->dataSaldoFinanceiro, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':saldoAnterior', $this->saldoAnterior, PDO::PARAM_STR);
        $sth->bindParam(':totalEntradas', $this->totalEntradas, PDO::PARAM_STR);
        $sth->bindParam(':totalSaidas', $this->totalSaidas, PDO::PARAM_STR);
        $sth->bindParam(':saldoMes', $this->saldoMes, PDO::PARAM_STR);

        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function atualizaSaldoFinanceiro($mes,$ano,$idOrganismoAfiliado)
    {


        $saldoAnterior=0;
        $totalEntradas=0;
        $totalSaidas=0;

        $resultado = $this->listaSaldoInicial($mes, $ano, $idOrganismoAfiliado);
        if ($resultado) {
            foreach ($resultado as $v) {
                $saldoAnterior = floatval($v['saldoAnterior']);
            }
        }

        $resultado = $this->recebimento->listaRecebimento($mes, $ano, $idOrganismoAfiliado);
        if ($resultado) {
            foreach ($resultado as $v) {
                $totalEntradas += $v['valorRecebimento'];
            }
        }

        $resultado = $this->despesa->listaDespesa($mes, $ano, $idOrganismoAfiliado);
        if ($resultado) {
            foreach ($resultado as $v) {
                $totalSaidas += $v['valorDespesa'];
            }
        }

        $saldoMes = ($saldoAnterior+$totalEntradas)-$totalSaidas;

        //inserir histórico até o mês anterior
        $this->setMes($mes);
        $this->setAno($ano);
        $dataSaldoFinanceiro = $ano . "-" . $mes . "-01";
        $this->setDataSaldoFinanceiro($dataSaldoFinanceiro);
        $this->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
        $this->setSaldoAnterior($saldoAnterior);
        $this->setTotalEntradas($totalEntradas);
        $this->setTotalSaidas($totalSaidas);
        $this->setSaldoMes($saldoMes);
        $response = $this->updateSaldoFinanceiro();

        if($response)
        {
            $arr = array();
            $arr['totalEntradas'] = $totalEntradas;
            $arr['totalSaidas'] = $totalSaidas;
            $arr['saldoMes'] = $saldoMes;
            return $arr;
        }else{
            return false;
        }

    }

    public function updateSaldoFinanceiro()
    {
        $sql="UPDATE saldoFinanceiro SET  
                                  totalEntradas = :totalEntradas,
                                  totalSaidas = :totalSaidas,
                                  saldoMes = :saldoMes,
                                  dataCadastro = now()    
                                  WHERE mes = :mes and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado
                            ";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':totalEntradas', $this->totalEntradas, PDO::PARAM_STR);
        $sth->bindParam(':totalSaidas', $this->totalSaidas, PDO::PARAM_STR);
        $sth->bindParam(':saldoMes', $this->saldoMes, PDO::PARAM_STR);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }

    }


    public function updateSaldoFinanceiroCompleto()
    {
        $sql="UPDATE saldoFinanceiro SET  
                                  saldoAnterior = :saldoAnterior,
                                  totalEntradas = :totalEntradas,
                                  totalSaidas = :totalSaidas,
                                  saldoMes = :saldoMes,
                                  dataCadastro = now()    
                                  WHERE mes = :mes and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado
                            ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':saldoAnterior', $this->saldoAnterior, PDO::PARAM_STR);
        $sth->bindParam(':totalEntradas', $this->totalEntradas, PDO::PARAM_STR);
        $sth->bindParam(':totalSaidas', $this->totalSaidas, PDO::PARAM_STR);
        $sth->bindParam(':saldoMes', $this->saldoMes, PDO::PARAM_STR);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }

    }

    public function listaSaldoInicial($mes=null,$ano=null,$idOrganismoAfiliado=null,$ultimoRegistro=null){

    	$sql = "SELECT CAST(saldoAnterior AS DECIMAL(12,2)) as saldoAnterior, 
                      CAST(totalEntradas  AS DECIMAL(12,2)) as totalEntradas, 
                      CAST(totalSaidas  AS DECIMAL(12,2)) as totalSaidas, 
                      CAST(saldoMes  AS DECIMAL(12,2)) as saldoMes
                      FROM saldoFinanceiro
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";

        if($mes!=null)
        {
            $sql .= " and mes=:mes";
        }

        if($ano!=null)
        {
            $sql .= " and ano=:ano";
        }

 	    if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}

        if($ultimoRegistro!=null)
        {
            $sql .= " order by ano,mes DESC LIMIT 0,1";
        }


        
    	
    	//echo $sql;
    	$c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mes!=null)
        {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }

        if($ano!=null)
        {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function limparSaldoFinanceiro($mes=null,$ano=null,$idOrganismoAfiliado=null){

    	$sql = "DELETE FROM saldoInicial
    	 where mes=:mes and ano=:ano and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function retornaOrganismosCadastrados(){

        $sql = "SELECT fk_idOrganismoAfiliado FROM saldoFinanceiro
    	 group by fk_idOrganismoAfiliado";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function limpaTabelas(){

        $sql = "TRUNCATE saldoFinanceiro";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $resultado = $c->run($sth);

        $sql2 = "TRUNCATE saldosAnterioresDoMes";
        $c2 = new conexaoSOA();
        $con2 = $c2->openSOA();
        $sth2 = $con2->prepare($sql2);
        $resultado2 = $c2->run($sth2);

        $sql3 = "TRUNCATE valoresDoMes";
        $c3 = new conexaoSOA();
        $con3 = $c3->openSOA();
        $sth3 = $con3->prepare($sql3);
        $resultado3 = $c3->run($sth3);

        $sql4 = "TRUNCATE valoresDoMesDespesas";
        $c4 = new conexaoSOA();
        $con4 = $c4->openSOA();
        $sth4 = $con4->prepare($sql4);
        $resultado4 = $c4->run($sth4);

        $sql5 = "TRUNCATE valoresDoMesEntradas";
        $c5 = new conexaoSOA();
        $con5 = $c5->openSOA();
        $sth5 = $con5->prepare($sql5);
        $resultado5 = $c5->run($sth5);


        return true;

    }

    public function limpaTabelasPorId($id){

        $sql = "DELETE FROM saldoFinanceiro WHERE fk_idOrganismoAfiliado = :id";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $resultado = $c->run($sth);

        $sql2 = "DELETE FROM saldosAnterioresDoMes WHERE fk_idOrganismoAfiliado = :id";
        $c2 = new conexaoSOA();
        $con2 = $c2->openSOA();
        $sth2 = $con2->prepare($sql2);
        $sth2->bindParam(':id', $id, PDO::PARAM_INT);
        $resultado2 = $c2->run($sth2);

        $sql3 = "DELETE FROM valoresDoMes  WHERE fk_idOrganismoAfiliado = :id";
        $c3 = new conexaoSOA();
        $con3 = $c3->openSOA();
        $sth3 = $con3->prepare($sql3);
        $sth3->bindParam(':id', $id, PDO::PARAM_INT);
        $resultado3 = $c3->run($sth3);

        $sql4 = "DELETE FROM valoresDoMesDespesas  WHERE fk_idOrganismoAfiliado = :id";
        $c4 = new conexaoSOA();
        $con4 = $c4->openSOA();
        $sth4 = $con4->prepare($sql4);
        $sth4->bindParam(':id', $id, PDO::PARAM_INT);
        $resultado4 = $c4->run($sth4);

        $sql5 = "DELETE FROM valoresDoMesEntradas  WHERE fk_idOrganismoAfiliado = :id";
        $c5 = new conexaoSOA();
        $con5 = $c5->openSOA();
        $sth5 = $con5->prepare($sql5);
        $sth5->bindParam(':id', $id, PDO::PARAM_INT);
        $resultado5 = $c5->run($sth5);

        return true;

    }


    public function limpar($idOrganismoAfiliado=null){

        $sql = "DELETE FROM saldoFinanceiro
    	 where fk_idOrganismoAfiliado=:id";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
