<?php
require_once ("conexaoClass.php");

class MembrosRosacruzesAtivos {

    public $idMembrosRosacruzesAtivos;
    public $fk_idOrganismoAfiliado;
    public $dataVerificacao;
    public $novasAfiliacoes;
    public $desligamentos;
    public $reintegrados;
    public $numeroAtualMembrosAtivos;
    public $usuario;
    public $ultimoAtualizar;
    public $dataAtualizacao;

    /* Funções GET e SET */

    function getIdMembrosRosacruzesAtivos() {
        return $this->idMembrosRosacruzesAtivos;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    
	function getDataVerificacao() {
        return $this->dataVerificacao;
    }

    function getNovasAfiliacoes() {
        return $this->novasAfiliacoes;
    }

    function getDesligamentos() {
        return $this->desligamentos;
    }
    
	function getReintegrados() {
        return $this->reintegrados;
    }
        
	function getNumeroAtualMembrosAtivos() {
        return $this->numeroAtualMembrosAtivos;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }
    
    function getDataAtualizacao() {
        return $this->dataAtualizacao;
    }

    function setIdMembrosRosacruzesAtivos($idMembrosRosacruzesAtivos) {
        $this->idMembrosRosacruzesAtivos = $idMembrosRosacruzesAtivos;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }
    
	function setDataVerificacao($dataVerificacao) {
        $this->dataVerificacao = $dataVerificacao;
    }

	function setNovasAfiliacoes($novasAfiliacoes) {
        $this->novasAfiliacoes = $novasAfiliacoes;
    }
    
	function setDesligamentos($desligamentos) {
        $this->desligamentos = $desligamentos;
    }

    function setReintegrados($reintegrados) {
        $this->reintegrados = $reintegrados;
    }
    
	function setNumeroAtualMembrosAtivos($numeroAtualMembrosAtivos) {
        $this->numeroAtualMembrosAtivos = $numeroAtualMembrosAtivos;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }
    
    function setDataAtualizacao($dataAtualizacao) {
        $this->dataAtualizacao = $dataAtualizacao;
    }

    public function cadastraMembrosRosacruzesAtivos() {
        
    	$sql = "INSERT INTO membrosRosacruzesAtivos 
        							(`fk_idOrganismoAfiliado`,
        							`dataVerificacao`, 
        							`novasAfiliacoes`, 
        							`desligamentos`,
        							`reintegrados`,
        							`numeroAtualMembrosAtivos`,
        							`usuario`,
        							`dataCadastro`
        							)
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :dataVerificacao,
                                            :novasAfiliacoes,
                                            :desligamento,
                                            :reintegrado,
                                            :numeroAtual,
                                            :usuario,
                                            now()
                                            )";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado',$this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataVerificacao', $this->dataVerificacao, PDO::PARAM_STR);
        $sth->bindParam(':novasAfiliacoes', $this->novasAfiliacoes, PDO::PARAM_INT);
        $sth->bindParam(':desligamento',$this->desligamentos, PDO::PARAM_INT);
        $sth->bindParam(':reintegrado',$this->reintegrados, PDO::PARAM_INT);
        $sth->bindParam(':numeroAtual',$this->numeroAtualMembrosAtivos, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraMembrosRosacruzesAtivos($id) {

        $sql = "UPDATE membrosRosacruzesAtivos SET
        					`dataVerificacao` = :dataVerificacao, 
        					`novasAfiliacoes` = :novasAfiliacoes,
                                                 `desligamentos` = :desligamento,
                                                 `reintegrados` = :reintegrado,
                                                 `numeroAtualMembrosAtivos` = :numeroAtual,
                                                 `ultimoAtualizar` = :ultimoAtualizar,
                                                 `dataAtualizacao` = now()
                            WHERE `idMembrosRosacruzesAtivos` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':dataVerificacao', $this->dataVerificacao, PDO::PARAM_STR);
        $sth->bindParam(':novasAfiliacoes', $this->novasAfiliacoes, PDO::PARAM_INT);
        $sth->bindParam(':desligamento',$this->desligamentos, PDO::PARAM_INT);
        $sth->bindParam(':reintegrado',$this->reintegrados, PDO::PARAM_INT);
        $sth->bindParam(':numeroAtual',$this->numeroAtualMembrosAtivos, PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'membrosRosacruzesAtivos'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM membrosRosacruzesAtivos ORDER BY idMembrosRosacruzesAtivos DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaMembrosRosacruzesAtivos($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM membrosRosacruzesAtivos where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataVerificacao)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataVerificacao)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	$sql .= " order by dataVerificacao ASC";
        
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_STR);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaMembrosRosacruzesAtivos($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT DISTINCT idMembrosRosacruzesAtivos,dataVerificacao, novasAfiliacoes, desligamentos, reintegrados, numeroAtualMembrosAtivos 
          FROM membrosRosacruzesAtivos where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataVerificacao)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataVerificacao)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	$sql .= " order by dataVerificacao ASC";
        
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_STR);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        $arr = array();
        $arr['idMembrosRosacruzesAtivos']="";
        $arr['novasAfiliacoes']=0;
        $arr['desligamentos']=0;
        $arr['reintegrados']=0;
        $arr['numeroAtualMembrosAtivos']=0;
        
        if ($resultado) 
        {
            foreach($resultado as $vetor)
            {
        		$arr['idMembrosRosacruzesAtivos'] 	= $vetor['idMembrosRosacruzesAtivos'];//Não mexer
        		$arr['novasAfiliacoes'] 		= $vetor['novasAfiliacoes'];
        		$arr['desligamentos'] 			= $vetor['desligamentos'];
        		$arr['reintegrados'] 			= $vetor['reintegrados'];
        		$arr['numeroAtualMembrosAtivos'] 	= $vetor['numeroAtualMembrosAtivos'];
            }
        } 
        return $arr;
    }
    
    public function retornaMembrosRosacruzesAtivosMesesAnteriores($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT 
    				 SUM(novasAfiliacoes) AS totalnovasafiliacoes, 
    				 SUM(desligamentos) AS totaldesligamentos,
    				 SUM(reintegrados) AS totalreintegrados
    				 FROM membrosRosacruzesAtivos where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataVerificacao)<:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataVerificacao)<=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	$sql .= " order by dataVerificacao ASC";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_STR);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        $arr = array();
        $arr['novasAfiliacoes']=0;
        $arr['desligamentos']=0;
        $arr['reintegrados']=0;
        $arr['numeroAtualMembrosAtivos']=0;
        
        if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $arr['novasAfiliacoes'] 			= $vetor['totalnovasafiliacoes'];
                    $arr['desligamentos'] 				= $vetor['totaldesligamentos'];
                    $arr['reintegrados'] 				= $vetor['totalreintegrados'];
                    $total					 	= ($vetor['totalnovasafiliacoes']+$vetor['totalreintegrados'])-$vetor['totaldesligamentos'];
            }
        } 
        return $total;
    }
    
    public function retornaMediaMembrosRosacruzesAtivos($anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT SUM(numeroAtualMembrosAtivos) as total FROM membrosRosacruzesAtivos where 1=1 ";
        
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataVerificacao)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	$sql .= " order by dataVerificacao ASC";
        
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        $total=0;
        
        if ($resultado) {
            foreach($resultado as $vetor)
        	{
        		$total 	= $vetor['total'];
        	}
        } 
        return  round($total/12);
    }

    public function removeMembrosRosacruzesAtivos($idMembrosRosacruzesAtivos) 
    {
        
        $sql = "DELETE FROM membrosRosacruzesAtivos WHERE `idMembrosRosacruzesAtivos` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idMembrosRosacruzesAtivos, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdMembrosRosacruzesAtivos($idMembrosRosacruzesAtivos) 
    {
        $sql = "SELECT * FROM membrosRosacruzesAtivos 
                                 WHERE `idMembrosRosacruzesAtivos` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idMembrosRosacruzesAtivos, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function validaMembrosRosacruzesAtivos($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM membrosRosacruzesAtivos where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataVerificacao)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataVerificacao)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	$sql .= " order by dataVerificacao ASC";
        
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_STR);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,null,null,true)){
            return true;
        }else{
            return false;
        }

    }

    public function validaDataMembrosRosacruzesAtivos($dataVerificacao,$fk_idOrganismoAfiliado) {

        $sql = "SELECT * FROM membrosRosacruzesAtivos where 1=1 ";
        $sql .= " and dataVerificacao=:dataVerificacao";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':dataVerificacao', $dataVerificacao, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);


        $resultado  = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return true;
            }

        }
        return false;

    }

    public function retornaNroMembrosRosacruzesAtivos($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null) {
        
        $sql = "SELECT numeroAtualMembrosAtivos FROM membrosRosacruzesAtivos where 1=1 ";
        if($mesAtual!=null)
        {
            $sql .= " and MONTH(dataVerificacao)=:mesAtual";
        }
        if($anoAtual!=null)
        {
            $sql .= " and YEAR(dataVerificacao)=:anoAtual";
        }
        if($fk_idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }
        $sql .= " order by dataVerificacao ASC";
        
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mesAtual!=null)
        {
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null)
        {
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        $numeroAtualMembrosAtivos=0;

        $data = array(
            'mesAtual'=>$mesAtual,
            'anoAtual'=>$anoAtual,
            'idOrganismoAfiliado'=>$fk_idOrganismoAfiliado
        );
        //echo "QUERY: " . $this->parms("retornaNroMembrosRosacruzesAtivos",$sql,$data);
        
        if ($resultado) 
        {
            foreach($resultado as $vetor)
            {
                $numeroAtualMembrosAtivos    = $vetor['numeroAtualMembrosAtivos'];
            }
        } 
        return $numeroAtualMembrosAtivos;
    }

    public function dataCriacao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM membrosRosacruzesAtivos where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataVerificacao)=:mes";
        $sql .= " and YEAR(dataVerificacao)=:ano";
        $sql .= " order by dataCadastro ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function dataUltimaAlteracao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM membrosRosacruzesAtivos where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataVerificacao)=:mes";
        $sql .= " and YEAR(dataVerificacao)=:ano";
        $sql .= " order by dataCadastro DESC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function responsaveis($mes,$ano, $idOrganismoAfiliado,$ultimaAlteracao=null) {

        if($ultimaAlteracao==null)
        {
            $sql = "SELECT * FROM membrosRosacruzesAtivos as m left join usuario on seqCadast = usuario where 1=1 ";
        }else{
            $sql = "SELECT * FROM membrosRosacruzesAtivos as m left join usuario on seqCadast = ultimoAtualizar where 1=1 ";
        }

        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataVerificacao)=:mes";
        $sql .= " and YEAR(dataVerificacao)=:ano and seqCadast <>0 order by m.dataCadastro ";

        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $arr=array();

        $i=0;
        if($resultado)
        {
            foreach($resultado as $v)
            {
                $arr['data'][$i] = $v['dataCadastro'];
                $arr['seq'][$i] = $v['seqCadast'];
                $arr['nome'][$i] = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
                $i++;
            }

        }
        return $arr;
    }


    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}
?>