<?php

require_once ("conexaoClass.php");

class secaoMenu {

    public $idSecaoMenu;
    public $nomeSecaoMenu;
    public $descricaoSecaoMenu;
    public $iconeSecaoMenu;
    public $arquivoSecaoMenu;
    public $prioridadeSecaoMenu;
    public $statusSecaoMenu;

    //Getter e Setter
    function getIdSecaoMenu() {
        return $this->idSecaoMenu;
    }

	function getNomeSecaoMenu() {
        return $this->nomeSecaoMenu;
    }
    
	function getDescricaoSecaoMenu() {
        return $this->descricaoSecaoMenu;
    }
    
	function getIconeSecaoMenu() {
        return $this->iconeSecaoMenu;
    }
    
	function getArquivoSecaoMenu() {
        return $this->arquivoSecaoMenu;
    }
    
	function getPrioridadeSecaoMenu() {
        return $this->prioridadeSecaoMenu;
    }
    
	function getStatusSecaoMenu() {
        return $this->statusSecaoMenu;
    }
    
    function setIdSecaoMenu($idSecaoMenu) {
        $this->idSecaoMenu = $idSecaoMenu;
    }

	function setNomeSecaoMenu($nomeSecaoMenu) {
        $this->nomeSecaoMenu = $nomeSecaoMenu;
    }
    
    function setDescricaoSecaoMenu($descricaoSecaoMenu) {
        $this->descricaoSecaoMenu = $descricaoSecaoMenu;
    }
    
	function setIconeSecaoMenu($iconeSecaoMenu) {
        $this->iconeSecaoMenu = $iconeSecaoMenu;
    }
    
	function setArquivoSecaoMenu($arquivoSecaoMenu) {
        $this->arquivoSecaoMenu = $arquivoSecaoMenu;
    }
    
	function setPrioridadeSecaoMenu($prioridadeSecaoMenu) {
        $this->prioridadeSecaoMenu = $prioridadeSecaoMenu;
    }
    
	function setStatusSecaoMenu($statusSecaoMenu) {
        $this->statusSecaoMenu = $statusSecaoMenu;
    }

    public function cadastraSecaoMenu() {
        
        $sql = "INSERT INTO secaoMenu 
                    (
                    nomeSecaoMenu, 
                    descricaoSecaoMenu, 
                    iconeSecaoMenu, 
                    arquivoSecaoMenu, 
                    prioridadeSecaoMenu)
                                    VALUES (
                                            :nome,
                                            :descricao,
                                            :icone,
                                            :arquivo,
                                            :prioridade)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getNomeSecaoMenu                   = $this->getNomeSecaoMenu();
        $getDescricaoSecaoMenu              = $this->getDescricaoSecaoMenu();
        $getIconeSecaoMenu                  = $this->getIconeSecaoMenu();
        $getArquivoSecaoMenu                = $this->getArquivoSecaoMenu();
        $getPrioridadeSecaoMenu             = $this->getPrioridadeSecaoMenu();

        $sth->bindParam(':nome', $getNomeSecaoMenu, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $getDescricaoSecaoMenu , PDO::PARAM_STR);
        $sth->bindParam(':icone', $getIconeSecaoMenu, PDO::PARAM_STR);
        $sth->bindParam(':arquivo', $getArquivoSecaoMenu, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $getPrioridadeSecaoMenu, PDO::PARAM_INT);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraSecaoMenu($id) {
        
        $sql = "UPDATE secaoMenu 
                            SET 
                                `nomeSecaoMenu` = :nome,
                                `descricaoSecaoMenu` = :descricao,
                                `iconeSecaoMenu` = :icone,
                                `arquivoSecaoMenu` = :arquivo,
                                `prioridadeSecaoMenu` = :prioridade
                            WHERE idSecaoMenu = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getNomeSecaoMenu                   = $this->getNomeSecaoMenu();
        $getDescricaoSecaoMenu              = $this->getDescricaoSecaoMenu();
        $getIconeSecaoMenu                  = $this->getIconeSecaoMenu();
        $getArquivoSecaoMenu                = $this->getArquivoSecaoMenu();
        $getPrioridadeSecaoMenu             = $this->getPrioridadeSecaoMenu();

        $sth->bindParam(':nome', $getNomeSecaoMenu, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $getDescricaoSecaoMenu , PDO::PARAM_STR);
        $sth->bindParam(':icone', $getIconeSecaoMenu, PDO::PARAM_STR);
        $sth->bindParam(':arquivo', $getArquivoSecaoMenu, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $getPrioridadeSecaoMenu, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusSecaoMenu() {
        
        $sql = "UPDATE secaoMenu SET `statusSecaoMenu` = :status"
                . " WHERE `idSecaoMenu` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getStatusSecaoMenu             = $this->getStatusSecaoMenu();
        $getIdSecaoMenu                 = $this->getIdSecaoMenu();

        $sth->bindParam(':status', $getStatusSecaoMenu, PDO::PARAM_INT);
        $sth->bindParam(':id', $getIdSecaoMenu, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusSecaoMenu();
            return $arr;
        } else {
            return false;
        }
    }

    public function listaSecaoMenu() {

        $sql = "SELECT * FROM secaoMenu
                              ORDER BY nomeSecaoMenu";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeSecaoMenu($idSecaoMenu) {
        
        $sql = "DELETE FROM secaoMenu WHERE idSecaoMenu = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idSecaoMenu, PDO::PARAM_INT);

        $resultado = $c->run($sth,true);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaSecaoMenu($status=0) {
        
    	$sql ="SELECT * FROM secaoMenu
                WHERE `nomeSecaoMenu` LIKE :nome
                and statusSecaoMenu = :status
                ORDER BY nomeSecaoMenu ASC";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $nome = '%'.$this->getNomeSecaoMenu().'%';
        
        $sth->bindParam(':nome', $nome, PDO::PARAM_STR);
        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdSecaoMenu() {
        
        $sql = "SELECT * FROM secaoMenu 
                              WHERE idSecaoMenu = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        if(is_numeric($_GET['id']))
        {
            $sth->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
        }    
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>