/* QUERY QUE RETORNA A CONTAGEM DO PRIMEIRO NOME DO MEMBRO NO ORCZ */
SELECT substring(nom_client,1, 
          case when charindex(' ',nom_client,1)=0 then len(nom_client) else charindex(' ',nom_client,1) end) as nome, DATALENGTH(substring(nom_client,1, 
          case when charindex(' ',nom_client,1)=0 then len(nom_client) else charindex(' ',nom_client,1) end)) AS total
FROM cadastro 
GROUP BY (nom_client)
HAVING DATALENGTH(substring(nom_client,1, 
          case when charindex(' ',nom_client,1)=0 then len(nom_client) else charindex(' ',nom_client,1) end)) > 11

/* QUERY QUE RETORNA OS ORGANISMOS ONDE AO MENOS UM OFICIAL EST� CADASTRADO*/
SELECT nomeOrganismoAfiliado, siglaOrganismoAfiliado
FROM  organismoAfiliado 
WHERE siglaOrganismoAfiliado != 
ALL (

SELECT siglaOA
FROM usuario
)
AND tipoOrganismoAfiliado =1
AND nomeOrganismoAfiliado !=  ''
ORDER BY nomeOrganismoAfiliado

/* QUERY QUE RETORNA LISTA DE E-MAILS DOS OFICIAIS ATIVOS */

SELECT CONCAT (emailUsuario,',') FROM `usuario` WHERE statusUsuario = 0 AND `senhaCriptografada` = 1 AND `fk_idRegiaoRosacruz` <> 'PR113' AND `codigoDeAfiliacao` <> '' AND `codigoDeAfiliacao` <> '0' AND `fk_idDepartamento` <> '2'