<?php
require_once ("conexaoClass.php");

class FuncaoSubMenu {

    public $fk_idFuncao;
    public $fk_idSubMenu;
    
    /* Funções GET e SET */

    function getFkIdFuncao() {
        return $this->fk_idFuncao;
    }

    function getFkIdSubMenu() {
        return $this->fk_idSubMenu;
    }

    function setFkIdFuncao($fk_idFuncao) {
        $this->fk_idFuncao = $fk_idFuncao;
    }

    function setFkIdSubMenu($fk_idSubMenu) {
        $this->fk_idSubMenu = $fk_idSubMenu;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO funcao_subMenu (`fk_idFuncao`, `fk_idSubMenu`)
                                    VALUES (:idFuncao,
                                            :idSubMenu)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':idSubMenu', $this->fk_idSubMenu, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM funcao_subMenu 
                            WHERE fk_idFuncao = :idFuncao
                            and fk_idSubMenu = :idSubMenu";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':idSubMenu', $this->fk_idSubMenu, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaPorFuncao($funcoesUsuarioString=null) 
    {
        
    	$sql = "SELECT * FROM funcao_subMenu
                   WHERE 1=1 "; 
    	if($funcoesUsuarioString!=null)
    	{
            $ids = explode(",", $funcoesUsuarioString);
            $inQuery = implode(",", array_fill(0, count($ids), "?"));
            $sql .= " and fk_idFuncao IN (".$inQuery.")"; 
    	}else{
        	$sql .= "  and `fk_idFuncao` = :idFuncao";
    	}
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($funcoesUsuarioString!=null)
    	{
            foreach ($ids as $k => $id)
            {    
                $sth->bindValue(($k+1), $id);
            }
        }else{ 
            $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
}
?>