<?php

require_once ("conexaoClass.php");

class atividadeIniciaticaColumba {

    private $idAtividadeIniciaticaColumba;
    private $seqCadastAtividadeIniciaticaColumba;
    private $codAfiliacaoAtividadeIniciaticaColumba;
    private $nomeAtividadeIniciaticaColumba;
    private $descricaoAtividadeIniciaticaColumba;
    private $cadastradoEmAtividadeIniciaticaColumba;
    private $atualizadoEmAtividadeIniciaticaColumba;
    private $statusAtividadeIniciaticaColumba;
    //private $anoAtividadeIniciaticaColumba;
    private $enderecoAtividadeIniciaticaColumba;
    private $cepAtividadeIniciaticaColumba;
    private $naturalidadeAtividadeIniciaticaColumba;
    private $dataNascimentoAtividadeIniciaticaColumba;
    private $escolaAtividadeIniciaticaColumba;
    private $emailAtividadeIniciaticaColumba;
    private $redeSocialAtividadeIniciaticaColumba;
    private $dataAdmissaoAtividadeIniciaticaColumba;
    private $dataInstalacaoAtividadeIniciaticaColumba;
    private $dataAtivaAteAtividadeIniciaticaColumba;
    private $codAfiliacaoTutorAtividadeIniciaticaColumba;
    private $nomeTutorAtividadeIniciaticaColumba;
    private $companheiroTutorAtividadeIniciaticaColumba;
    private $codAfiliacaoTutoraAtividadeIniciaticaColumba;
    private $nomeTutoraAtividadeIniciaticaColumba;
    private $companheiroTutoraAtividadeIniciaticaColumba;
    private $responsavelPlenoAtividadeIniciaticaColumba;
    private $seqCadastResponsavelPlenoAtividadeIniciaticaColumba;
    private $emailResponsavelAtividadeIniciaticaColumba;
    private $RgAnexoAtividadeIniciaticaColumba;
    private $certidaoNascimentoAnexoAtividadeIniciaticaColumba;
    private $fotoCorpoInteiroAnexoAtividadeIniciaticaColumba;
    private $cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba;
    private $fk_idOrganismoAfiliado;
    private $fk_seqCadastAtualizadoPor;

    //--- GETERS -----------------------------------------------------
    public function getIdAtividadeIniciaticaColumba() {
        return $this->idAtividadeIniciaticaColumba;
    }
    public function getSeqCadastAtividadeIniciaticaColumba() {
        return $this->seqCadastAtividadeIniciaticaColumba;
    }
    public function getCodAfiliacaoAtividadeIniciaticaColumba(){
        return $this->codAfiliacaoAtividadeIniciaticaColumba;
    }
    public function getNomeAtividadeIniciaticaColumba() {
        return $this->nomeAtividadeIniciaticaColumba;
    }
    public function getDescricaoAtividadeIniciaticaColumba() {
        return $this->descricaoAtividadeIniciaticaColumba;
    }
    public function getCadastradoEmAtividadeIniciaticaColumba() {
        return $this->cadastradoEmAtividadeIniciaticaColumba;
    }
    public function getAtualizadoEmAtividadeIniciaticaColumba() {
        return $this->atualizadoEmAtividadeIniciaticaColumba;
    }
    public function getStatusAtividadeIniciaticaColumba() {
        return $this->statusAtividadeIniciaticaColumba;
    }
    //public function getAnoAtividadeIniciaticaColumba() {
    //    return $this->anoAtividadeIniciaticaColumba;
    //}
    public function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }
    public function getFkSeqCadastAtualizadoPor() {
        return $this->fk_seqCadastAtualizadoPor;
    }
    public function getEnderecoAtividadeIniciaticaColumba() {
        return $this->enderecoAtividadeIniciaticaColumba;
    }
    public function getCepAtividadeIniciaticaColumba() {
        return $this->cepAtividadeIniciaticaColumba;
    }
    public function getNaturalidadeAtividadeIniciaticaColumba() {
        return $this->naturalidadeAtividadeIniciaticaColumba;
    }
    public function getDataNascimentoAtividadeIniciaticaColumba() {
        return $this->dataNascimentoAtividadeIniciaticaColumba;
    }
    public function getEscolaAtividadeIniciaticaColumba() {
        return $this->escolaAtividadeIniciaticaColumba;
    }
    public function getEmailAtividadeIniciaticaColumba() {
        return $this->emailAtividadeIniciaticaColumba;
    }
    public function getRedeSocialAtividadeIniciaticaColumba() {
        return $this->redeSocialAtividadeIniciaticaColumba;
    }
    public function getDataAdmissaoAtividadeIniciaticaColumba() {
        return $this->dataAdmissaoAtividadeIniciaticaColumba;
    }
    public function getDataInstalacaoAtividadeIniciaticaColumba() {
        return $this->dataInstalacaoAtividadeIniciaticaColumba;
    }
    public function getDataAtivaAteAtividadeIniciaticaColumba() {
        return $this->dataAtivaAteAtividadeIniciaticaColumba;
    }
    public function getCodAfiliacaoTutorAtividadeIniciaticaColumba() {
        return $this->codAfiliacaoTutorAtividadeIniciaticaColumba;
    }
    public function getNomeTutorAtividadeIniciaticaColumba() {
        return $this->nomeTutorAtividadeIniciaticaColumba;
    }
    public function getCompanheiroTutorAtividadeIniciaticaColumba() {
        return $this->companheiroTutorAtividadeIniciaticaColumba;
    }
    public function getCodAfiliacaoTutoraAtividadeIniciaticaColumba() {
        return $this->codAfiliacaoTutoraAtividadeIniciaticaColumba;
    }
    public function getNomeTutoraAtividadeIniciaticaColumba() {
        return $this->nomeTutoraAtividadeIniciaticaColumba;
    }
    public function getCompanheiroTutoraAtividadeIniciaticaColumba() {
        return $this->companheiroTutoraAtividadeIniciaticaColumba;
    }
    public function getResponsavelPlenoAtividadeIniciaticaColumba() {
        return $this->responsavelPlenoAtividadeIniciaticaColumba;
    }
    public function getSeqCadastResponsavelPlenoAtividadeIniciaticaColumba() {
        return $this->seqCadastResponsavelPlenoAtividadeIniciaticaColumba;
    }
    public function getEmailResponsavelAtividadeIniciaticaColumba() {
        return $this->emailResponsavelAtividadeIniciaticaColumba;
    }
    public function getRgAnexoAtividadeIniciaticaColumba() {
        return $this->RgAnexoAtividadeIniciaticaColumba;
    }
    public function getCertidaoNascimentoAnexoAtividadeIniciaticaColumba() {
        return $this->certidaoNascimentoAnexoAtividadeIniciaticaColumba;
    }
    public function getFotoCorpoInteiroAnexoAtividadeIniciaticaColumba() {
        return $this->fotoCorpoInteiroAnexoAtividadeIniciaticaColumba;
    }
    public function getCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba() {
        return $this->cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba;
    }


    //--- SETERS -----------------------------------------------------
    public function setIdAtividadeIniciaticaColumba($idAtividadeIniciaticaColumba) {
        $this->idAtividadeIniciaticaColumba = $idAtividadeIniciaticaColumba;
    }
    public function setSeqCadastAtividadeIniciaticaColumba($seqCadastAtividadeIniciaticaColumba) {
        $this->seqCadastAtividadeIniciaticaColumba = $seqCadastAtividadeIniciaticaColumba;
    }
    public function setCodAfiliacaoAtividadeIniciaticaColumba($codAfiliacaoAtividadeIniciaticaColumba) {
        $this->codAfiliacaoAtividadeIniciaticaColumba = $codAfiliacaoAtividadeIniciaticaColumba;
    }
    public function setNomeAtividadeIniciaticaColumba($nomeAtividadeIniciaticaColumba) {
        $this->nomeAtividadeIniciaticaColumba = $nomeAtividadeIniciaticaColumba;
    }
    public function setDescricaoAtividadeIniciaticaColumba($descricaoAtividadeIniciaticaColumba) {
        $this->descricaoAtividadeIniciaticaColumba = $descricaoAtividadeIniciaticaColumba;
    }
    public function setCadastradoEmAtividadeIniciaticaColumba($cadastradoEmAtividadeIniciaticaColumba) {
        $this->cadastradoEmAtividadeIniciaticaColumba = $cadastradoEmAtividadeIniciaticaColumba;
    }
    public function setAtualizadoEmAtividadeIniciaticaColumba($atualizadoEmAtividadeIniciaticaColumba) {
        $this->atualizadoEmAtividadeIniciaticaColumba = $atualizadoEmAtividadeIniciaticaColumba;
    }
    public function setStatusAtividadeIniciaticaColumba($statusAtividadeIniciaticaColumba) {
        $this->statusAtividadeIniciaticaColumba = $statusAtividadeIniciaticaColumba;
    }
    //public function setAnoAtividadeIniciaticaColumba($anoAtividadeIniciaticaColumba) {
    //    $this->anoAtividadeIniciaticaColumba = $anoAtividadeIniciaticaColumba;
    //}
    public function setEnderecoAtividadeIniciaticaColumba($enderecoAtividadeIniciaticaColumba) {
        $this->enderecoAtividadeIniciaticaColumba = $enderecoAtividadeIniciaticaColumba;
    }
    public function setCepAtividadeIniciaticaColumba($cepAtividadeIniciaticaColumba) {
        $this->cepAtividadeIniciaticaColumba = $cepAtividadeIniciaticaColumba;
    }
    public function setNaturalidadeAtividadeIniciaticaColumba($naturalidadeAtividadeIniciaticaColumba) {
        $this->naturalidadeAtividadeIniciaticaColumba = $naturalidadeAtividadeIniciaticaColumba;
    }
    public function setDataNascimentoAtividadeIniciaticaColumba($dataNascimentoAtividadeIniciaticaColumba) {
        $this->dataNascimentoAtividadeIniciaticaColumba = $dataNascimentoAtividadeIniciaticaColumba;
    }
    public function setEscolaAtividadeIniciaticaColumba($escolaAtividadeIniciaticaColumba) {
        $this->escolaAtividadeIniciaticaColumba = $escolaAtividadeIniciaticaColumba;
    }
    public function setEmailAtividadeIniciaticaColumba($emailAtividadeIniciaticaColumba) {
        $this->emailAtividadeIniciaticaColumba = $emailAtividadeIniciaticaColumba;
    }
    public function setRedeSocialAtividadeIniciaticaColumba($redeSocialAtividadeIniciaticaColumba) {
        $this->redeSocialAtividadeIniciaticaColumba = $redeSocialAtividadeIniciaticaColumba;
    }
    public function setDataAdmissaoAtividadeIniciaticaColumba($dataAdmissaoAtividadeIniciaticaColumba) {
        $this->dataAdmissaoAtividadeIniciaticaColumba = $dataAdmissaoAtividadeIniciaticaColumba;
    }
    public function setDataInstalacaoAtividadeIniciaticaColumba($dataInstalacaoAtividadeIniciaticaColumba) {
        $this->dataInstalacaoAtividadeIniciaticaColumba = $dataInstalacaoAtividadeIniciaticaColumba;
    }
    public function setDataAtivaAteAtividadeIniciaticaColumba($dataAtivaAteAtividadeIniciaticaColumba) {
        $this->dataAtivaAteAtividadeIniciaticaColumba = $dataAtivaAteAtividadeIniciaticaColumba;
    }
    public function setCodAfiliacaoTutorAtividadeIniciaticaColumba($codAfiliacaoTutorAtividadeIniciaticaColumba) {
        $this->codAfiliacaoTutorAtividadeIniciaticaColumba = $codAfiliacaoTutorAtividadeIniciaticaColumba;
    }
    public function setNomeTutorAtividadeIniciaticaColumba($nomeTutorAtividadeIniciaticaColumba) {
        $this->nomeTutorAtividadeIniciaticaColumba = $nomeTutorAtividadeIniciaticaColumba;
    }
    public function setCompanheiroTutorAtividadeIniciaticaColumba($companheiroTutorAtividadeIniciaticaColumba) {
        $this->companheiroTutorAtividadeIniciaticaColumba = $companheiroTutorAtividadeIniciaticaColumba;
    }
    public function setCodAfiliacaoTutoraAtividadeIniciaticaColumba($codAfiliacaoTutoraAtividadeIniciaticaColumba) {
        $this->codAfiliacaoTutoraAtividadeIniciaticaColumba = $codAfiliacaoTutoraAtividadeIniciaticaColumba;
    }
    public function setNomeTutoraAtividadeIniciaticaColumba($nomeTutoraAtividadeIniciaticaColumba) {
        $this->nomeTutoraAtividadeIniciaticaColumba = $nomeTutoraAtividadeIniciaticaColumba;
    }
    public function setCompanheiroTutoraAtividadeIniciaticaColumba($companheiroTutoraAtividadeIniciaticaColumba) {
        $this->companheiroTutoraAtividadeIniciaticaColumba = $companheiroTutoraAtividadeIniciaticaColumba;
    }
    public function setResponsavelPlenoAtividadeIniciaticaColumba($responsavelPlenoAtividadeIniciaticaColumba) {
        $this->responsavelPlenoAtividadeIniciaticaColumba = $responsavelPlenoAtividadeIniciaticaColumba;
    }
    public function setSeqCadastResponsavelPlenoAtividadeIniciaticaColumba($seqCadastResponsavelPlenoAtividadeIniciaticaColumba) {
        $this->seqCadastResponsavelPlenoAtividadeIniciaticaColumba = $seqCadastResponsavelPlenoAtividadeIniciaticaColumba;
    }
    public function setEmailResponsavelAtividadeIniciaticaColumba($emailResponsavelAtividadeIniciaticaColumba) {
        $this->emailResponsavelAtividadeIniciaticaColumba = $emailResponsavelAtividadeIniciaticaColumba;
    }
    public function setRgAnexoAtividadeIniciaticaColumba($RgAnexoAtividadeIniciaticaColumba) {
        $this->RgAnexoAtividadeIniciaticaColumba = $RgAnexoAtividadeIniciaticaColumba;
    }
    public function setCertidaoNascimentoAnexoAtividadeIniciaticaColumba($certidaoNascimentoAnexoAtividadeIniciaticaColumba) {
        $this->certidaoNascimentoAnexoAtividadeIniciaticaColumba = $certidaoNascimentoAnexoAtividadeIniciaticaColumba;
    }
    public function setFotoCorpoInteiroAnexoAtividadeIniciaticaColumba($fotoCorpoInteiroAnexoAtividadeIniciaticaColumba) {
        $this->fotoCorpoInteiroAnexoAtividadeIniciaticaColumba = $fotoCorpoInteiroAnexoAtividadeIniciaticaColumba;
    }
    public function setCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba($cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba) {
        $this->cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba = $cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba;
    }
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }
    public function setFkSeqCadastAtualizadoPor($fk_seqCadastAtualizadoPor) {
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }

    public function cadastroAtividadeIniciaticaColumba(){
/*
        $query = "INSERT INTO atividadeIniciaticaColumba
                                 (seqCadastAtividadeIniciaticaColumba,
                                  codAfiliacaoAtividadeIniciaticaColumba,
                                  nomeAtividadeIniciaticaColumba,
                                  descricaoAtividadeIniciaticaColumba,
                                  cadastradoEmAtividadeIniciaticaColumba,
                                  enderecoAtividadeIniciaticaColumba,
                                  cepAtividadeIniciaticaColumba,
                                  naturalidadeAtividadeIniciaticaColumba,
                                  dataNascimentoAtividadeIniciaticaColumba,
                                  escolaAtividadeIniciaticaColumba,
                                  emailAtividadeIniciaticaColumba,
                                  redeSocialAtividadeIniciaticaColumba,
                                  dataAdmissaoAtividadeIniciaticaColumba,
                                  dataInstalacaoAtividadeIniciaticaColumba,
                                  dataAtivaAteAtividadeIniciaticaColumba,
                                  codAfiliacaoTutorAtividadeIniciaticaColumba,
                                  nomeTutorAtividadeIniciaticaColumba,
                                  codAfiliacaoTutoraAtividadeIniciaticaColumba,
                                  nomeTutoraAtividadeIniciaticaColumba,
                                  responsavelPlenoAtividadeIniciaticaColumba,
                                  seqCadastResponsavelPlenoAtividadeIniciaticaColumba,
                                  emailResponsavelAtividadeIniciaticaColumba,
                                  RgAnexoAtividadeIniciaticaColumba,
                                  certidaoNascimentoAnexoAtividadeIniciaticaColumba,
                                  fotoCorpoInteiroAnexoAtividadeIniciaticaColumba,
                                  cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba,
                                  fk_idOrganismoAfiliado,
                                  fk_seqCadastAtualizadoPor)
                            VALUES ('".$this->getSeqCadastAtividadeIniciaticaColumba()."',
                                    '".$this->getCodAfiliacaoAtividadeIniciaticaColumba()."',
                                    '".$this->getNomeAtividadeIniciaticaColumba()."',
                                    '".$this->getDescricaoAtividadeIniciaticaColumba()."',
                                    NOW(),
                                    '".$this->getEnderecoAtividadeIniciaticaColumba()."',
                                    '".$this->getCepAtividadeIniciaticaColumba()."',
                                    '".$this->getNaturalidadeAtividadeIniciaticaColumba()."',
                                    '".$this->getDataNascimentoAtividadeIniciaticaColumba()."',
                                    '".$this->getEscolaAtividadeIniciaticaColumba()."',
                                    '".$this->getEmailAtividadeIniciaticaColumba()."',
                                    '".$this->getRedeSocialAtividadeIniciaticaColumba()."',
                                    '".$this->getDataAdmissaoAtividadeIniciaticaColumba()."',
                                    '".$this->getDataInstalacaoAtividadeIniciaticaColumba()."',
                                    '".$this->getDataAtivaAteAtividadeIniciaticaColumba()."',
                                    '".$this->getCodAfiliacaoTutorAtividadeIniciaticaColumba()."',
                                    '".$this->getNomeTutorAtividadeIniciaticaColumba()."',
                                    '".$this->getCodAfiliacaoTutoraAtividadeIniciaticaColumba()."',
                                    '".$this->getNomeTutoraAtividadeIniciaticaColumba()."',
                                    '".$this->getResponsavelPlenoAtividadeIniciaticaColumba()."',
                                    '".$this->getSeqCadastResponsavelPlenoAtividadeIniciaticaColumba()."',
                                    '".$this->getEmailResponsavelAtividadeIniciaticaColumba()."',
                                    '".$this->getRgAnexoAtividadeIniciaticaColumba()."',
                                    '".$this->getCertidaoNascimentoAnexoAtividadeIniciaticaColumba()."',
                                    '".$this->getFotoCorpoInteiroAnexoAtividadeIniciaticaColumba()."',
                                    '".$this->getCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba()."',
                                    '".$this->getFkIdOrganismoAfiliado()."',
                                    '".$this->getFkSeqCadastAtualizadoPor()."')";
*/

        $sql = "INSERT INTO atividadeIniciaticaColumba
                                 (seqCadastAtividadeIniciaticaColumba,
                                  codAfiliacaoAtividadeIniciaticaColumba,
                                  nomeAtividadeIniciaticaColumba,
                                  descricaoAtividadeIniciaticaColumba,
                                  cadastradoEmAtividadeIniciaticaColumba,
                                  fk_idOrganismoAfiliado,
                                  fk_seqCadastAtualizadoPor)
                            VALUES (:seqCadastAtividadeIniciaticaColumba,
                                    :codAfiliacaoAtividadeIniciaticaColumba,
                                    :nomeAtividadeIniciaticaColumba,
                                    :descricaoAtividadeIniciaticaColumba,
                                    NOW(),
                                    :fk_idOrganismoAfiliado,
                                    :fk_seqCadastAtualizadoPor)";

        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadastAtividadeIniciaticaColumba', $this->seqCadastAtividadeIniciaticaColumba, PDO::PARAM_INT);
        $sth->bindParam(':codAfiliacaoAtividadeIniciaticaColumba', $this->codAfiliacaoAtividadeIniciaticaColumba, PDO::PARAM_STR);
        $sth->bindParam(':nomeAtividadeIniciaticaColumba', $this->nomeAtividadeIniciaticaColumba, PDO::PARAM_STR);
        $sth->bindParam(':descricaoAtividadeIniciaticaColumba', $this->descricaoAtividadeIniciaticaColumba, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraAtividadeIniciaticaColumba($idAtividadeIniciaticaColumba) {

/*
        $query = "UPDATE atividadeIniciaticaColumba SET
                                  `descricaoAtividadeIniciaticaColumba` = '" . $this->getDescricaoAtividadeIniciaticaColumba() . "',
                                  `enderecoAtividadeIniciaticaColumba` = '" . $this->getEnderecoAtividadeIniciaticaColumba() . "',
                                  `cepAtividadeIniciaticaColumba` = '" . $this->getCepAtividadeIniciaticaColumba() . "',
                                  `naturalidadeAtividadeIniciaticaColumba` = '" . $this->getNaturalidadeAtividadeIniciaticaColumba() . "',
                                  `dataNascimentoAtividadeIniciaticaColumba` = '" . $this->getDataNascimentoAtividadeIniciaticaColumba() . "',
                                  `escolaAtividadeIniciaticaColumba` = '" . $this->getEscolaAtividadeIniciaticaColumba() . "',
                                  `emailAtividadeIniciaticaColumba` = '" . $this->getEmailAtividadeIniciaticaColumba() . "',
                                  `redeSocialAtividadeIniciaticaColumba` = '" . $this->getRedeSocialAtividadeIniciaticaColumba() . "',
                                  `dataAdmissaoAtividadeIniciaticaColumba` = '" . $this->getDataAdmissaoAtividadeIniciaticaColumba() . "',
                                  `dataInstalacaoAtividadeIniciaticaColumba` = '" . $this->getDataInstalacaoAtividadeIniciaticaColumba() . "',
                                  `dataAtivaAteAtividadeIniciaticaColumba` = '" . $this->getDataAtivaAteAtividadeIniciaticaColumba() . "',
                                  `codAfiliacaoTutorAtividadeIniciaticaColumba` = '" . $this->getCodAfiliacaoTutorAtividadeIniciaticaColumba() . "',
                                  `nomeTutorAtividadeIniciaticaColumba` = '" . $this->getNomeTutorAtividadeIniciaticaColumba() . "',
                                  `codAfiliacaoTutoraAtividadeIniciaticaColumba` = '" . $this->getCodAfiliacaoTutoraAtividadeIniciaticaColumba() . "',
                                  `nomeTutoraAtividadeIniciaticaColumba` = '" . $this->getNomeTutoraAtividadeIniciaticaColumba() . "',
                                  `responsavelPlenoAtividadeIniciaticaColumba` = '" . $this->getResponsavelPlenoAtividadeIniciaticaColumba() . "',
                                  `seqCadastResponsavelPlenoAtividadeIniciaticaColumba` = '" . $this->getSeqCadastResponsavelPlenoAtividadeIniciaticaColumba() . "',
                                  `emailResponsavelAtividadeIniciaticaColumba` = '" . $this->getEmailResponsavelAtividadeIniciaticaColumba() . "'";
        if ($this->getRgAnexoAtividadeIniciaticaColumba() != "") {
            $query .= ", `RgAnexoAtividadeIniciaticaColumba` = '" . $this->getRgAnexoAtividadeIniciaticaColumba() . "'";
        }
        if ($this->getCertidaoNascimentoAnexoAtividadeIniciaticaColumba() != "") {
            $query .= ", `certidaoNascimentoAnexoAtividadeIniciaticaColumba` = '" . $this->getCertidaoNascimentoAnexoAtividadeIniciaticaColumba() . "'";
        }
        if ($this->getFotoCorpoInteiroAnexoAtividadeIniciaticaColumba() != "") {
            $query .= ", `fotoCorpoInteiroAnexoAtividadeIniciaticaColumba` = '" . $this->getFotoCorpoInteiroAnexoAtividadeIniciaticaColumba() . "'";
        }
        if ($this->getCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba() != "") {
            $query .= ", `cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba` = '" . $this->getCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba() . "'";
        }
        $query .= ", `atualizadoEmAtividadeIniciaticaColumba` =  NOW(),
                                  `fk_seqCadastAtualizadoPor` =  '" . $this->getFkSeqCadastAtualizadoPor() . "'
                                  WHERE idAtividadeIniciaticaColumba = ".$idAtividadeIniciaticaColumba."";
*/

        $sql = "UPDATE atividadeIniciaticaColumba SET
                                  `descricaoAtividadeIniciaticaColumba` = :descricaoAtividadeIniciaticaColumba";

                      $sql .= ", `atualizadoEmAtividadeIniciaticaColumba` =  NOW(),
                                  `fk_seqCadastAtualizadoPor` =  :fk_seqCadastAtualizadoPor
                                  WHERE idAtividadeIniciaticaColumba = :idAtividadeIniciaticaColumba";

        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':descricaoAtividadeIniciaticaColumba', $this->descricaoAtividadeIniciaticaColumba, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeIniciaticaColumba', $idAtividadeIniciaticaColumba, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeIniciaticaColumba($idOrganismoAfiliado){

        $sql = "SELECT * FROM atividadeIniciaticaColumba";
        if ($idOrganismoAfiliado != "") {
            $sql .= " WHERE fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado";
        }
        $sql .= " ORDER BY idAtividadeIniciaticaColumba ASC";

       // echo " || " . $query . " || ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($idOrganismoAfiliado != "") {
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscAtividadeIniciaticaColumbaPorId($idAtividadeIniciaticaColumba) {

        $sql = "SELECT * FROM  atividadeIniciaticaColumba
                WHERE idAtividadeIniciaticaColumba = :idAtividadeIniciaticaColumba";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeIniciaticaColumba', $idAtividadeIniciaticaColumba, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function verificaColumbaJaCadastrada($seqCadast, $fk_idOrganismoAfiliado){

        $sql = "SELECT * FROM atividadeIniciaticaColumba
                                WHERE seqCadastAtividadeIniciaticaColumba = :seqCadast
                                and fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado";
        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,null,null,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraStatusAtividadeIniciaticaColumba() {

        $sql = "UPDATE atividadeIniciaticaColumba SET statusAtividadeIniciaticaColumba = ':statusAtividadeIniciaticaColumba' "
                . "WHERE idAtividadeIniciaticaColumba = ':idAtividadeIniciaticaColumba'";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusAtividadeIniciaticaColumba', $this->statusAtividadeIniciaticaColumba, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeIniciaticaColumba', $this->idAtividadeIniciaticaColumba, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusAtividadeIniciaticaColumba();
            return $arr;
        } else {
            return false;
        }
    }

}