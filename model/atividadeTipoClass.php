<?php

require_once ("conexaoClass.php");

class atividadeTipo{

    private $idAtividadeTipo;
    private $nomeAtividadeTipo;
    private $descricaoAtividadeTipo;
    private $lojaAtividadeTipo;
    private $pronaosAtividadeTipo;
    private $capituloAtividadeTipo;
    private $qntLojaAtividadeTipo;
    private $qntPronaosAtividadeTipo;
    private $qntCapituloAtividadeTipo;
    private $statusAtividadeTipo;
    
    //--- GETERS -----------------------------------------------------
    function getIdAtividadeTipo() {
        return $this->idAtividadeTipo;
    }

    function getNomeAtividadeTipo() {
        return $this->nomeAtividadeTipo;
    }

    function getDescricaoAtividadeTipo() {
        return $this->descricaoAtividadeTipo;
    }

    function getLojaAtividadeTipo() {
        return $this->lojaAtividadeTipo;
    }

    function getPronaosAtividadeTipo() {
        return $this->pronaosAtividadeTipo;
    }

    function getCapituloAtividadeTipo() {
        return $this->capituloAtividadeTipo;
    }

    function getQntLojaAtividadeTipo() {
        return $this->qntLojaAtividadeTipo;
    }

    function getQntPronaosAtividadeTipo() {
        return $this->qntPronaosAtividadeTipo;
    }

    function getQntCapituloAtividadeTipo() {
        return $this->qntCapituloAtividadeTipo;
    }

    function getStatusAtividadeTipo() {
        return $this->statusAtividadeTipo;
    }    
    
    //--- SETERS -----------------------------------------------------
	function setIdAtividadeTipo($idAtividadeTipo) {
        $this->idAtividadeTipo = $idAtividadeTipo;
    }

    function setNomeAtividadeTipo($nomeAtividadeTipo) {
        $this->nomeAtividadeTipo = $nomeAtividadeTipo;
    }

    function setDescricaoAtividadeTipo($descricaoAtividadeTipo) {
        $this->descricaoAtividadeTipo = $descricaoAtividadeTipo;
    }

    function setLojaAtividadeTipo($lojaAtividadeTipo) {
        $this->lojaAtividadeTipo = $lojaAtividadeTipo;
    }

    function setPronaosAtividadeTipo($pronaosAtividadeTipo) {
        $this->pronaosAtividadeTipo = $pronaosAtividadeTipo;
    }

    function setCapituloAtividadeTipo($capituloAtividadeTipo) {
        $this->capituloAtividadeTipo = $capituloAtividadeTipo;
    }

    function setQntLojaAtividadeTipo($qntLojaAtividadeTipo) {
        $this->qntLojaAtividadeTipo = $qntLojaAtividadeTipo;
    }

    function setQntPronaosAtividadeTipo($qntPronaosAtividadeTipo) {
        $this->qntPronaosAtividadeTipo = $qntPronaosAtividadeTipo;
    }

    function setQntCapituloAtividadeTipo($qntCapituloAtividadeTipo) {
        $this->qntCapituloAtividadeTipo = $qntCapituloAtividadeTipo;
    }

    function setStatusAtividadeTipo($statusAtividadeTipo) {
        $this->statusAtividadeTipo = $statusAtividadeTipo;
    }
    
    //--------------------------------------------------------------------------

	public function cadastroAtividadeTipo(){

        $sql = "INSERT INTO atividadeTipo
                                 (nomeAtividadeTipo,
                                  descricaoAtividadeTipo,
                                  lojaAtividadeTipo,
                                  pronaosAtividadeTipo,
                                  capituloAtividadeTipo,
                                  qntLojaAtividadeTipo,
                                  qntPronaosAtividadeTipo,
                                  qntCapituloAtividadeTipo)
                            VALUES (:nomeAtividadeTipo,
                                    :descricaoAtividadeTipo,
                                    :lojaAtividadeTipo,
                                    :pronaosAtividadeTipo,
                                    :capituloAtividadeTipo,
                                    :qntLojaAtividadeTipo,
                                    :qntPronaosAtividadeTipo,
                                    :qntCapituloAtividadeTipo)";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nomeAtividadeTipo', $this->nomeAtividadeTipo, PDO::PARAM_STR);
        $sth->bindParam(':descricaoAtividadeTipo', $this->descricaoAtividadeTipo, PDO::PARAM_STR);
        $sth->bindParam(':lojaAtividadeTipo', $this->lojaAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':pronaosAtividadeTipo', $this->pronaosAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':capituloAtividadeTipo', $this->capituloAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':qntLojaAtividadeTipo', $this->qntLojaAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':qntPronaosAtividadeTipo', $this->qntPronaosAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':qntCapituloAtividadeTipo', $this->qntCapituloAtividadeTipo, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraAtividadeTipo($idAtividadeTipo){
        
        $sql = "UPDATE atividadeTipo SET
                                  nomeAtividadeTipo = :nomeAtividadeTipo,
                                  descricaoAtividadeTipo =  :descricaoAtividadeTipo,
				  lojaAtividadeTipo = :lojaAtividadeTipo,
                                  pronaosAtividadeTipo = :pronaosAtividadeTipo,
                                  capituloAtividadeTipo = :capituloAtividadeTipo,
                                  qntLojaAtividadeTipo = :qntLojaAtividadeTipo,
                                  qntPronaosAtividadeTipo = :qntPronaosAtividadeTipo,
                                  qntCapituloAtividadeTipo = :qntCapituloAtividadeTipo
                                  WHERE idAtividadeTipo = :idAtividadeTipo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nomeAtividadeTipo', $this->nomeAtividadeTipo, PDO::PARAM_STR);
        $sth->bindParam(':descricaoAtividadeTipo', $this->descricaoAtividadeTipo, PDO::PARAM_STR);
        $sth->bindParam(':lojaAtividadeTipo', $this->lojaAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':pronaosAtividadeTipo', $this->pronaosAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':capituloAtividadeTipo', $this->capituloAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':qntLojaAtividadeTipo', $this->qntLojaAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':qntPronaosAtividadeTipo', $this->qntPronaosAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':qntCapituloAtividadeTipo', $this->qntCapituloAtividadeTipo, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeTipo', $idAtividadeTipo, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeTipo(){

        $query = "SELECT * FROM atividadeTipo ORDER BY idAtividadeTipo ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($query);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

}

?>
