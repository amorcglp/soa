<?php

require_once ("conexaoClass.php");

class acesso{

    private $idAcesso;
    private $seqCadast;
    private $ip;
    private $siglaOA;
    private $login;
    private $nome;
    private $dataCadastro;

    function getIdAcesso() {
        return $this->idAcesso;
    }

    function getSeqCadast() {
        return $this->seqCadast;
    }

    function getIp() {
        return $this->ip;
    }

    function getSiglaOA() {
        return $this->siglaOA;
    }

    function getLogin() {
        return $this->login;
    }

    function getNome() {
        return $this->nome;
    }

    function getDataCadastro() {
        return $this->dataCadastro;
    }

    function setIdAcesso($idAcesso) {
        $this->idAcesso = $idAcesso;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    function setIp($ip) {
        $this->ip = $ip;
    }

    function setSiglaOA($siglaOA) {
        $this->siglaOA = $siglaOA;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

    
    
    //--------------------------------------------------------------------------

    /**
     * [Realiza cadastro de ]
     * @return [boolean] [Em caso de sucesso no retorno do PDO retorna TRUE]
     */
    public function cadastroAcesso(){

        $sql = "INSERT INTO acesso (seqCadast, ip, siglaOA, login, nome, dataCadastro) 
        VALUES (
        :seqCadast,
        :ip,
        :siglaOA,
        :login,
        :nome,
        now()
        )";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':ip', $this->ip, PDO::PARAM_STR);
        $sth->bindParam(':siglaOA', $this->siglaOA, PDO::PARAM_STR);
        $sth->bindParam(':login', $this->login, PDO::PARAM_STR);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function listaAcesso(){

        $sql = "SELECT * FROM acesso ORDER BY dataCadastro ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaIdAcesso($idAcesso){
        $sql = "SELECT * FROM  acesso WHERE   idAcesso = :idAcesso";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAcesso', $idAcesso, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaExisteUsuarioComEsteIp($ip,$loginUsuario){
        $sql = "SELECT * FROM usersonline as us "
                . " inner join usuario as u on loginUsuario = us.login "
                . " WHERE us.ip=:ip and us.login=:login ";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':ip', $ip, PDO::PARAM_INT);
        $sth->bindParam(':login', $loginUsuario, PDO::PARAM_STR);

        if ($c->run($sth,null,null,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaExisteUsuarioComEsteIpRetornaDados($ip,$loginUsuario){
        $sql = "SELECT * FROM usersonline as us "
                . " inner join usuario as u on loginUsuario = us.login "
                . " WHERE us.ip=:ip and us.login=:login ";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':ip', $ip, PDO::PARAM_INT);
        $sth->bindParam(':login', $loginUsuario, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeAcesso($idAcesso){
        $sql = "DELETE FROM acesso WHERE idAcesso = :idAcesso";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAcesso', $idAcesso, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }


}

?>
