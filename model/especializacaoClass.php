<?php

require_once ("conexaoClass.php");

class especializacao {

    public $SeqEspeci;
    public $DesEspeci;
    
    function getSeqEspeci() {
        return $this->SeqEspeci;
    }

    function getDesEspeci() {
        return $this->DesEspeci;
    }

    function setSeqEspeci($SeqEspeci) {
        $this->SeqEspeci = $SeqEspeci;
    }

    function setDesEspeci($DesEspeci) {
        $this->DesEspeci = $DesEspeci;
    }

    public function lista() {
        
        $sql = "SELECT * FROM especializacao";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>