<?php
require_once ("conexaoClass.php");

class atividadeEstatutoMensal {

    public $idAtividadeEstatutoMensal;
    public $fk_idOrganismoAfiliado;
    public $mes;
    public $ano;
    public $entregue;
    public $dataEntrega;
    public $quemEntregou;

    /**
     * @return mixed
     */
    public function getIdAtividadeEstatutoMensal()
    {
        return $this->idAtividadeEstatutoMensal;
    }

    /**
     * @param mixed $idAtividadeEstatutoMensal
     */
    public function setIdAtividadeEstatutoMensal($idAtividadeEstatutoMensal)
    {
        $this->idAtividadeEstatutoMensal = $idAtividadeEstatutoMensal;
    }

    /**
     * @return mixed
     */
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }

    /**
     * @param mixed $fk_idOrganismoAfiliado
     */
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @param mixed $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getEntregue()
    {
        return $this->entregue;
    }

    /**
     * @param mixed $entregue
     */
    public function setEntregue($entregue)
    {
        $this->entregue = $entregue;
    }

    /**
     * @return mixed
     */
    public function getDataEntrega()
    {
        return $this->dataEntrega;
    }

    /**
     * @param mixed $dataEntrega
     */
    public function setDataEntrega($dataEntrega)
    {
        $this->dataEntrega = $dataEntrega;
    }

    /**
     * @return mixed
     */
    public function getQuemEntregou()
    {
        return $this->quemEntregou;
    }

    /**
     * @param mixed $quemEntregou
     */
    public function setQuemEntregou($quemEntregou)
    {
        $this->quemEntregou = $quemEntregou;
    }




    public function cadastraAtividadeMensal() {
        
        $sql = "INSERT INTO atividadeEstatutoMensal 
                                    (
                                    fk_idOrganismoAfiliado, 
                                    mes, 
                                    ano,
                                    entregue,
                                    dataEntrega,
                                    quemEntregou
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :mes,
                                            :ano,
                                            :entregue,
                                            now(),
                                            :quemEntregou
                                            )";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_STR);
        $sth->bindParam(':entregue', $this->entregue, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregou', $this->quemEntregou, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function entregaAtividadeMensal() {

        $sql = "UPDATE atividadeEstatutoMensal SET 
        			 entregue = 1,
                            WHERE mes = :mes and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':mes', $this->mes, PDO::PARAM_STR);
        $sth->bindParam(':ano',  $this->ano, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeMensal($mes=null,$ano=null,$fk_idOrganismoAfiliado=null,$entregueCompletamente=false) {
        
    	$sql = "SELECT * FROM atividadeEstatutoMensal as fm
    		inner join organismoAfiliado as oa on fm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on fm.quemEntregou = u.seqCadast
    	where 1=1 ";
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        if($entregueCompletamente)
        {
            $sql .= " and entregueCompletamente=1";
        }
    	$sql .= " order by ano, mes ";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_STR);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaEntregue($mes=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioAtividadeMensal where 1=1 ";
    	if($mes!=null)
    	{
    		$sql .= " and mes=:mes";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($mes!=null)
    	{
            $sth->bindParam(':mes', $mes, PDO::PARAM_STR);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,null,null,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscarIdAtividadeMensal($id) {
        
        $sql = "SELECT * FROM atividadeEstatutoMensal 
                                 WHERE idAtividadeEstatutoMensal = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeAtividadeMensal($id){
        
        $sql="DELETE FROM atividadeEstatutoMensal WHERE idAtividadeEstatutoMensal = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaNumeroAssinatura($mesAtual, $anoAtual,$idOrganismoAfiliado,$numeroAssinatura)
    {

        $sql = "UPDATE atividadeEstatutoMensal SET
        						  numeroAssinatura =  :numeroAssinatura
                                  WHERE mes = :mesAtual and ano = :anoAtual and fk_idOrganismoAfiliado = :idOrganismoAfiliado ";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':numeroAssinatura', $numeroAssinatura, PDO::PARAM_STR);
        $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function assinaturaEletronicaAtividadeMensal($seqCadast,$ip,$idOrganismoAfiliado,$funcao,$mes,$ano,$codigoAssinaturaIndividual){

        //echo "idAta=>".$idAta;
        $sql = "INSERT INTO atividadeEstatutoMensal_assinatura_eletronica
                                 (ano,
                                  mes,
                                  fk_idOrganismoAfiliado,
                                  codigoAssinaturaIndividual,
                                  seqCadast,
                                  fk_idFuncao,
                                  ip,
                                  data
                                  )
                            VALUES (:ano,
                                    :mes,
                                    :fk_idOrganismoAfiliado,
                                    :codigoAssinaturaIndividual,
                                    :seqCadast,
                                    :fk_idFuncao,
                                    :ip,
                                    now())";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth->bindParam(':fk_idFuncao', $funcao, PDO::PARAM_INT);
        $sth->bindParam(':codigoAssinaturaIndividual', $codigoAssinaturaIndividual, PDO::PARAM_INT);



        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarAssinaturasEmAtividadeMensal($seqCadast=null,$idOrganismoAfiliado=null,$codigoIndividual=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  atividadeEstatutoMensal_assinatura_eletronica as a
                left join atividadeEstatutoMensal as fm on fm.fk_idOrganismoAfiliado = a.fk_idOrganismoAfiliado and fm.mes = a.mes and fm.ano = a.ano
                left join usuario as u on u.seqCadast = a.seqCadast
                left join organismoAfiliado as o on a.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                where 1=1 ";

        if($seqCadast!=null)
        {
            $sql .= " and a.seqCadast = :seqCadast";
        }

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and a.fk_idOrganismoAfiliado = :idOrganismoAfiliado group by idAtividadeEstatutoMensalAssinaturaEletronica";
        }

        if($codigoIndividual!=null)
        {
            $sql .= " and a.codigoAssinaturaIndividual = :codigoIndividual";
        }


        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null) {
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        if($codigoIndividual!=null)
        {
            $sth->bindParam(':codigoIndividual', $codigoIndividual, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);

        if($codigoIndividual!=null)
        {
            return $resultado;
        }else {
            if ($idOrganismoAfiliado == null) {
                if ($resultado) {
                    $arr = array();
                    foreach ($resultado as $v) {
                        $arr[] = $v['mes'] . "@@" . $v['ano'] . "@@" . $v['fk_idOrganismoAfiliado'];
                    }
                    return $arr;
                } else {
                    return false;
                }
            } else {
                return $resultado;
            }
        }
    }

    public function buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,$idFuncao=null)
    {
        //echo "idOa=>".$idOrganismoAfiliado;
        $sql = "SELECT * FROM  atividadeEstatutoMensal_assinatura_eletronica as a
                left join usuario as u on u.seqCadast = a.seqCadast
                left join funcao as f on a.fk_idFuncao = f.idFuncao
                WHERE   mes = :mesAtual and ano = :anoAtual and fk_idOrganismoAfiliado = :idOrganismoAfiliado ";
        if($idFuncao!=null)
        {
            $sql .= " and fk_idFuncao = :idFuncao";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        if($idFuncao!=null)
        {
            $sth->bindParam(':idFuncao', $idFuncao, PDO::PARAM_INT);
        }

        if($idFuncao==null)
        {
            $resultado = $c->run($sth);
            if ($resultado){
                return $resultado;
            } else {
                return false;
            }
        }else{
            $resultado = $c->run($sth,null,null,true);
            if ($resultado){
                return true;
            } else {
                return false;
            }
        }

    }

    public function entregarCompletamente($mesAtual,$anoAtual,$idOrganismoAfiliado,$quemEntregouCompletamente)
    {
        $sql = "UPDATE atividadeEstatutoMensal SET
        						  entregueCompletamente =  1,
        						  quemEntregouCompletamente = :quemEntregouCompletamente,
        						  dataEntregouCompletamente = now()
                                  WHERE mes = :mesAtual
                                  and ano = :anoAtual
                                  and fk_idOrganismoAfiliado = :idOrganismoAfiliado ";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':quemEntregouCompletamente', $quemEntregouCompletamente, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinatura($codigo){

        $sql = "SELECT * FROM  atividadeEstatutoMensal as a
                WHERE   numeroAssinatura = :codigo";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAssinaturaIndividual($codigo,$codigoIndividual){

        $sql = "SELECT * FROM  atividadeEstatutoMensal as f
                inner join atividadeEstatutoMensal_assinatura_eletronica as fmae on fmae.ano = f.ano and fmae.mes = f.mes and fmae.fk_idOrganismoAfiliado =f.fk_idOrganismoAfiliado 
                WHERE   numeroAssinatura = :codigo and codigoAssinaturaIndividual = :codigoIndividual";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':codigo', $codigo , PDO::PARAM_INT);
        $sth->bindParam(':codigoIndividual', $codigoIndividual , PDO::PARAM_INT);

        $resultado = $c->run($sth,false,false,true);
        if ($resultado){
            return true;
        } else {
            return false;
        }
    }




}
?>