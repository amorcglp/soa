<?php
require_once ("conexaoClass.php");

class Despesa {

    public $idDespesa;
    public $fk_idOrganismoAfiliado;
    public $dataDespesa;
    public $descricaoDespesa;
    public $pagoA;
    public $valorDespesa;
    public $categoriaDespesa;
    public $usuario;
    public $ultimoAtualizar;
    public $idRefDocumentoFirebase;

    /* Funções GET e SET */

    function getIdDespesa() {
        return $this->idDespesa;
    }

    function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getDataDespesa() {
        return $this->dataDespesa;
    }

    function getDescricaoDespesa() {
        return $this->descricaoDespesa;
    }
    
	function getPagoA() {
        return $this->pagoA;
    }
        
	function getValorDespesa() {
        return $this->valorDespesa;
    }
    
	function getCategoriaDespesa() {
        return $this->categoriaDespesa;
    }
    
	function getUsuario() {
        return $this->usuario;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function setIdDespesa($idDespesa) {
        $this->idDespesa = $idDespesa;
    }

    function getIdRefDocumentoFirebase() {
        return $this->idRefDocumentoFirebase;
    }

    function setFkIdOrganismoAfiliado($fk_idOrganismoAfilidado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfilidado;
    }

	function setDataDespesa($dataDespesa) {
        $this->dataDespesa = $dataDespesa;
    }
    
	function setDescricaoDespesa($descricaoDespesa) {
        $this->descricaoDespesa = $descricaoDespesa;
    }

    function setPagoA($pagoA) {
        $this->pagoA = $pagoA;
    }
    
	function setValorDespesa($valorDespesa) {
        $this->valorDespesa = $valorDespesa;
    }
    
	function setCategoriaDespesa($categoriaDespesa) {
        $this->categoriaDespesa = $categoriaDespesa;
    }
    
	function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    function setIdRefDocumentoFirebase($idRefDocumentoFirebase) {
        $this->idRefDocumentoFirebase = $idRefDocumentoFirebase;
    }

    public function cadastraDespesa() {
        
    	$sql = "INSERT INTO despesa 
                        (`fk_idOrganismoAfiliado`, 
                        `dataDespesa`, 
                        `descricaoDespesa`,
                        `pagoA`,
                        `valorDespesa`,
                        `categoriaDespesa`,
                        `usuario`,
                        `dataCadastro`,
                        `idRefDocumentoFirebase`
                        )
                        VALUES (
                                :idOrganismoAfiliado,
                                :data,
                                :descricao,
                                :pagoA,
                                :valor,
                                :categoria,
                                :usuario,
                                now(),
                                :idRefDocumentoFirebase
                                )";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':data', $this->dataDespesa, PDO::PARAM_STR);
        $sth->bindParam(':descricao',  $this->descricaoDespesa, PDO::PARAM_STR);
        $sth->bindParam(':pagoA', $this->pagoA, PDO::PARAM_STR);
        $sth->bindParam(':valor', $this->valorDespesa, PDO::PARAM_STR);
        $sth->bindParam(':categoria',  $this->categoriaDespesa, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        $sth->bindParam(':idRefDocumentoFirebase', $this->idRefDocumentoFirebase, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraDespesa($id) {

        $sql = "UPDATE despesa SET 
                        `dataDespesa` = :data,
                        `descricaoDespesa` = :descricao,
                        `pagoA` = :pagoA,
                        `valorDespesa` = :valor,
                        `categoriaDespesa` = :categoria,
                        `ultimoAtualizar` = :ultimoAtualizar,
                        `idRefDocumentoFirebase` = :idRefDocumentoFirebase 
                            WHERE `idDespesa` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
     
        $sth->bindParam(':data', $this->dataDespesa, PDO::PARAM_STR);
        $sth->bindParam(':descricao',  $this->descricaoDespesa, PDO::PARAM_STR);
        $sth->bindParam(':pagoA', $this->pagoA, PDO::PARAM_STR);
        $sth->bindParam(':valor', $this->valorDespesa, PDO::PARAM_STR);
        $sth->bindParam(':categoria',  $this->categoriaDespesa, PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':idRefDocumentoFirebase', $this->idRefDocumentoFirebase, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizaIdRefDocumentoFirebase($id,$idRefDocumentoFirebase) {

        $sql = "UPDATE despesa SET 
                                       `idRefDocumentoFirebase` = :idRefDocumentoFirebase   
                            WHERE `idDespesa` = :idDespesa";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idDespesa', $id, PDO::PARAM_INT);
        $sth->bindParam(':idRefDocumentoFirebase', $idRefDocumentoFirebase, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'despesa'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM despesa ORDER BY idDespesa DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaDespesa($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null,$ultimoRegistro=null) {
        
    	$sql = "SELECT * FROM despesa where 1=1 ";
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataDespesa)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataDespesa)=:anoAtual";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        if($ultimoRegistro!=null)
        {
            $sql .= " order by dataDespesa DESC limit 0,1";
        }else {
            $sql .= " order by dataDespesa ASC";
        }
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual!=null) {
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null) {
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }

        if($fk_idOrganismoAfiliado!=null) {
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function retornaSaida($mesAtual=null,$anoAtual=null,$fk_idOrganismoAfiliado=null,$categoriaDespesa=null,$diaAtual=null,$idIgnorar=null)
    {
        
    	$sql = "SELECT SUM(REPLACE( REPLACE(valorDespesa, '.' ,'' ), ',', '.' )) as total FROM despesa where 1=1 ";
        
	if($diaAtual!=null)
    	{
    		$sql .= " and DAY(dataDespesa)=:diaAtual";
    	}
    	if($mesAtual!=null)
    	{
    		$sql .= " and MONTH(dataDespesa)=:mesAtual";
    	}
    	if($anoAtual!=null)
    	{
    		$sql .= " and YEAR(dataDespesa)=:anoAtual";
    	}
        if($categoriaDespesa!=null)
    	{
    		$sql .= " and categoriaDespesa=:categoria";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
        if($idIgnorar!=null)
        {
            $sql .= " and idDespesa <> :idIgnorar";
        }
        
    	$sql .= " order by dataDespesa ASC";
    	//echo "<br><br>".$sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($diaAtual!=null)
    	{
            $sth->bindParam(':diaAtual', $diaAtual, PDO::PARAM_INT);
        }
        if($mesAtual!=null)
    	{
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null)
    	{
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($categoriaDespesa!=null)
    	{
            $sth->bindParam(':categoria', $categoriaDespesa, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($idIgnorar!=null)
        {
            $sth->bindParam(':idIgnorar', $idIgnorar, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            foreach($resultado as $vetor)
        	{
        		$total = floatval($vetor['total']);
        	}
            return $total;
        } else {
            return false;
        }
    }

    public function removeDespesa($idDespesa) 
    {
        
        $sql = "DELETE FROM despesa WHERE `idDespesa` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idDespesa, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscarIdDespesa($idDespesa) 
    {
        
        $sql = "SELECT * FROM despesa 
                                 WHERE `idDespesa` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idDespesa, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function dataCriacao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM despesa where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataDespesa)=:mes";
        $sql .= " and YEAR(dataDespesa)=:ano";
        $sql .= " order by dataCadastro ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function dataUltimaAlteracao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM despesa where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataDespesa)=:mes";
        $sql .= " and YEAR(dataDespesa)=:ano";
        $sql .= " order by dataCadastro DESC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function responsaveis($mes,$ano, $idOrganismoAfiliado,$ultimaAlteracao=null) {

        if($ultimaAlteracao==null)
        {
            $sql = "SELECT * FROM despesa as d left join usuario on seqCadast = usuario where 1=1 ";
        }else{
            $sql = "SELECT * FROM despesa as d  left join usuario on seqCadast = ultimoAtualizar where 1=1 ";
        }

        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataDespesa)=:mes";
        $sql .= " and YEAR(dataDespesa)=:ano and seqCadast <>0  order by d.dataCadastro";

        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $arr=array();

        $i=0;
        if($resultado)
        {
            foreach($resultado as $v)
            {
                $arr['data'][$i] = $v['dataCadastro'];
                $arr['seq'][$i] = $v['seqCadast'];
                $arr['nome'][$i] = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
                $i++;
            }

        }
        return $arr;
    }

    public function getTotalRegistros($fk_idOrganismoAfiliado) {

        $sql = "SELECT COUNT(idDespesa) AS total FROM despesa WHERE fk_idOrganismoAfiliado=:id";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $fk_idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $total=0;

        if ($resultado){
            foreach ($resultado as $v)
            {
                $total = $v['total'];
            }
            return $total;
        }else{
            return false;
        }
    }

    public function totalDespesasAteMesAnterior($fk_idOrganismoAfiliado, $dataInicial, $ano, $mes) {

        $sql = "SELECT SUM(CAST(REPLACE(REPLACE(valorDespesa, '.', ''), ',', '.') AS DOUBLE)) FROM despesa where 1=1";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and Date(dataDespesa) >= :dataInicial";
        $sql .= " and Date(dataDespesa) <= :dataFinal";

        $ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));
        $dataFinal = $ano . "-" . $mes . "-" . $ultimo_dia;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':dataInicial', $dataInicial, PDO::PARAM_STR);
        $sth->bindParam(':dataFinal', $dataFinal, PDO::PARAM_STR);

        $resultado = $c->run($sth);
        return $resultado[0][0];
    }

}
?>