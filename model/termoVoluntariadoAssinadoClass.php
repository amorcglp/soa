<?php

require_once ("conexaoClass.php");

class termoVoluntariadoAssinado{

    private $idTermoVoluntariadoAssinado;
    private $fk_idTermoVoluntariado;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdTermoVoluntariadoAssinado() {
        return $this->idTermoVoluntariadoAssinado;
    }

    function getFkIdTermoVoluntariado() {
        return $this->fk_idTermoVoluntariado;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdTermoVoluntariadoAssinado($idTermoVoluntariadoAssinado) {
        $this->idTermoVoluntariadoAssinado = $idTermoVoluntariadoAssinado;
    }

    function setFkIdTermoVoluntariado($fk_idTermoVoluntariado) {
        $this->fk_idTermoVoluntariado = $fk_idTermoVoluntariado;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    //--------------------------------------------------------------------------

    public function cadastroTermoVoluntariadoAssinado(){

        $sql="INSERT INTO termoVoluntariado_Assinado  
                                 (
                                  fk_idTermoVoluntariado,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idTermoVoluntariado,
                                    :caminho,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFkIdTermoVoluntariado       = $this->getFkIdTermoVoluntariado();
        $getCaminho                     = $this->getCaminho();

        $sth->bindParam(':idTermoVoluntariado', $getFkIdTermoVoluntariado, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $getCaminho, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'termoVoluntariado_Assinado'";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            foreach ($resultado as $vetor)
            {
                    $proximo_id = $vetor['Auto_increment']; 
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaTermoVoluntariadoAssinado($id=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM termoVoluntariado_Assinado
    	inner join termoVoluntariado on fk_idTermoVoluntariado = idTermoVoluntariado
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idTermoVoluntariado = :id";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
    	//echo "<br><br>".$sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeTermoVoluntariadoAssinado($idTermoVoluntariadoAssinado){
        
        $sql="DELETE FROM termoVoluntariado_Assinado WHERE idTermoVoluntariadoAssinado = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idTermoVoluntariadoAssinado, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
