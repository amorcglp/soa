<?php

require_once ("conexaoClass.php");

class emailAnexo{

    private $idEmailAnexo;
    private $fk_idEmail;
    private $caminho;
    private $nomeArquivo;
    private $extensao;

    //METODO SET/GET -----------------------------------------------------------

    function getIdEmailAnexo() {
        return $this->idEmailAnexo;
    }

    function getFkIdEmail() {
        return $this->fk_idEmail;
    }

    function getCaminho() {
        return $this->caminho;
    }
    
	function getNomeArquivo() {
        return $this->nomeArquivo;
    }
    
	function getExtensao() {
        return $this->extensao;
    }

    function setIdEmailAnexo($idEmailAnexo) {
        $this->idEmailAnexo = $idEmailAnexo;
    }

    function setFkIdEmail($fk_idEmail) {
        $this->fk_idEmail = $fk_idEmail;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }
    
	function setNomeArquivo($nomeArquivo) {
        $this->nomeArquivo = $nomeArquivo;
    }
    
	function setExtensao($extensao) {
        $this->extensao = $extensao;
    }

    //--------------------------------------------------------------------------

    public function cadastroEmailAnexo(){

        $sql="INSERT INTO emailAnexo  
                                 (
                                  fk_idEmail,
                                  caminho,
                                  nome_arquivo,
                                  extensao,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idEmail,
                                    :caminho,
                                    :nomeArquivo,
                                    :extensao,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idEmail', $this->fk_idEmail, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        $sth->bindParam(':nomeArquivo', $this->nomeArquivo, PDO::PARAM_STR);
        $sth->bindParam(':extensao', $this->extensao, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'emailAnexo'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado) {
	        foreach ($resultado as $vetor)
	    	{
	    		$proximo_id = $vetor['Auto_increment']; 
	    	}
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaEmailAnexo($id=null){

    	$sql = "SELECT * FROM emailAnexo
    	inner join email on fk_idEmail = idEmail
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idEmail = :id";
    	}
    	
    	//echo "<br><br>".$sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);        
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeEmailAnexo($idEmailAnexo){
        
        $sql = "DELETE FROM emailAnexo WHERE idEmailAnexo = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idEmailAnexo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    
}

?>
