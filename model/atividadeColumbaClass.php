<?php

require_once ("conexaoClass.php");

class atividadeColumba{

    private $idAtividadeColumba;
    private $fk_idOrganismoAfiliado;
    private $trimestre;
    private $ano;
    private $seqCadastMembro;
    private $codigoAfiliacao;
    private $nomeColumba;
    private $paisAtivosOA;
    private $reunioes;
    private $ensaios;
    private $iniciacoesLojaCapitulo;
    private $aposicaoNome;
    private $casamento;
    private $convocacoesRitualisticas;
    private $iniciacoesGraus;
    private $instalacao;
    private $avaliacao;
    private $comentarios;
    private $ultimoAtualizar;
    private $usuario;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtividadeColumba() {
        return $this->idAtividadeColumba;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getTrimestre() {
        return $this->trimestre;
    }

    function getAno() {
        return $this->ano;
    }

    function getSeqCadastMembro() {
        return $this->seqCadastMembro;
    }

    function getCodigoAfiliacao() {
        return $this->codigoAfiliacao;
    }

    function getNomeColumba() {
        return $this->nomeColumba;
    }

    function getPaisAtivosOA() {
        return $this->paisAtivosOA;
    }

    function getReunioes() {
        return $this->reunioes;
    }

    function getEnsaios() {
        return $this->ensaios;
    }

    function getIniciacoesLojaCapitulo() {
        return $this->iniciacoesLojaCapitulo;
    }

    function getAposicaoNome() {
        return $this->aposicaoNome;
    }

    function getCasamento() {
        return $this->casamento;
    }

    function getConvocacoesRitualisticas() {
        return $this->convocacoesRitualisticas;
    }

    function getIniciacoesGraus() {
        return $this->iniciacoesGraus;
    }

    function getInstalacao() {
        return $this->instalacao;
    }

    function getAvaliacao() {
        return $this->avaliacao;
    }

    function getComentarios() {
        return $this->comentarios;
    }

    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setIdAtividadeColumba($idAtividadeColumba) {
        $this->idAtividadeColumba = $idAtividadeColumba;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setTrimestre($trimestre) {
        $this->trimestre = $trimestre;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setSeqCadastMembro($seqCadastMembro) {
        $this->seqCadastMembro = $seqCadastMembro;
    }

    function setCodigoAfiliacao($codigoAfiliacao) {
        $this->codigoAfiliacao = $codigoAfiliacao;
    }

    function setNomeColumba($nomeColumba) {
        $this->nomeColumba = $nomeColumba;
    }

    function setPaisAtivosOA($paisAtivosOA) {
        $this->paisAtivosOA = $paisAtivosOA;
    }

    function setReunioes($reunioes) {
        $this->reunioes = $reunioes;
    }

    function setEnsaios($ensaios) {
        $this->ensaios = $ensaios;
    }

    function setIniciacoesLojaCapitulo($iniciacoesLojaCapitulo) {
        $this->iniciacoesLojaCapitulo = $iniciacoesLojaCapitulo;
    }

    function setAposicaoNome($aposicaoNome) {
        $this->aposicaoNome = $aposicaoNome;
    }

    function setCasamento($casamento) {
        $this->casamento = $casamento;
    }

    function setConvocacoesRitualisticas($convocacoesRitualisticas) {
        $this->convocacoesRitualisticas = $convocacoesRitualisticas;
    }

    function setIniciacoesGraus($iniciacoesGraus) {
        $this->iniciacoesGraus = $iniciacoesGraus;
    }

    function setInstalacao($instalacao) {
        $this->instalacao = $instalacao;
    }

    function setAvaliacao($avaliacao) {
        $this->avaliacao = $avaliacao;
    }

    function setComentarios($comentarios) {
        $this->comentarios = $comentarios;
    }

    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    


    //--------------------------------------------------------------------------

    public function cadastroAtividadeColumba(){

        $sql = "INSERT INTO atividadeColumba
                                 (fk_idOrganismoAfiliado,
                                  trimestre,
                                  ano,
                                  seqCadastMembro,
                                  codigoAfiliacao,
                                  nomeColumba,
                                  paisAtivosOA,
                                  reunioes,
                                  ensaios,
                                  iniciacoesLojaCapitulo,
                                  aposicaoNome,
                                  casamento,
                                  convocacoesRitualisticas,
                                  iniciacoesGraus,
                                  instalacao,
                                  avaliacao,
                                  comentarios,
                                  usuario,
                                  dataCadastro
                                  )
                            VALUES (
                            		:fk_idOrganismoAfiliado,
                            		:trimestre,
                            		:ano,
                            		:seqCadastMembro,
                                        :codigoAfiliacao,
                                        :nomeColumba,
                            		:paisAtivosOA,
                                    :reunioes,
                                    :ensaios,
                                    :iniciacoesLojaCapitulo,
                                    :aposicaoNome,
                                    :casamento,
                                    :convocacoesRitualisticas,
                                    :iniciacoesGraus,
                                    :instalacao,
                                    :avaliacao,
                                    :comentarios,
                            		:usuario,
                                    now()
                                    )";
        //echo $sql; exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $this->seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':nomeColumba', $this->nomeColumba, PDO::PARAM_STR);
        $sth->bindParam(':paisAtivosOA', $this->paisAtivosOA, PDO::PARAM_INT);
        $sth->bindParam(':reunioes', $this->reunioes, PDO::PARAM_INT);
        $sth->bindParam(':ensaios', $this->ensaios, PDO::PARAM_INT);
        $sth->bindParam(':iniciacoesLojaCapitulo', $this->iniciacoesLojaCapitulo, PDO::PARAM_INT);
        $sth->bindParam(':aposicaoNome', $this->aposicaoNome, PDO::PARAM_INT);
        $sth->bindParam(':casamento', $this->casamento, PDO::PARAM_INT);
        $sth->bindParam(':convocacoesRitualisticas', $this->convocacoesRitualisticas, PDO::PARAM_INT);
        $sth->bindParam(':iniciacoesGraus', $this->iniciacoesGraus, PDO::PARAM_INT);
        $sth->bindParam(':instalacao', $this->instalacao, PDO::PARAM_INT);
        $sth->bindParam(':avaliacao', $this->avaliacao, PDO::PARAM_INT);
        $sth->bindParam(':comentarios', $this->comentarios, PDO::PARAM_STR);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE 'atividadeColumba'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }

    public function listaAtividadeColumba($idOrganismoAfiliado=null,$tipoColumba=null,$trimestre=null,$ano=null){

    	$sql = "SELECT * FROM atividadeColumba as ac
    	inner join organismoAfiliado as o on ac.fk_idOrganismoAfiliado = o.idOrganismoAfiliado
    	where 1=1 ";

    	if($idOrganismoAfiliado!=null){
    		$sql .= " and ac.fk_idOrganismoAfiliado = :fk_idOrganismoAfiliado";
    	}
        if($tipoColumba!=null){
    		$sql .= " and tipoColumba = :tipoColumba";
    	}
        if($trimestre!=null){
    		$sql .= " and trimestre = :trimestre";
    	}
        if($ano!=null){
    		$sql .= " and ano = :ano";
    	}
    	//$sql .= " order by codigoAfiliacao";
    	//echo $sql;exit();

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idOrganismoAfiliado!=null){
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($tipoColumba!=null){
            $sth->bindParam(':tipoColumba', $tipoColumba, PDO::PARAM_INT);
        }
        if($trimestre!=null){
            $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        }
        if($ano!=null){
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

	public function verificaSeJaCadastrado($seqCadastMembro,$idOrganismoAfiliado,$trimestre,$ano){

    	$sql = "SELECT * FROM atividadeColumba
    			where seqCadastMembro=:seqCadastMembro"
                . " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado"
                . " and trimestre=:trimestre"
                . " and ano=:ano";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaAtividadeColumbaPorId($id){

        $sql = "SELECT * FROM atividadeColumba WHERE idAtividadeColumba=:idAtividadeColumba";
        //echo $sql."<br>";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeColumba', $id, PDO::PARAM_INT);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function alteraAtividadeColumba($idAtividadeColumba) {

        $sql = "UPDATE atividadeColumba SET
                                  trimestre = :trimestre,
                                  ano = :ano,
                                  seqCadastMembro = :seqCadastMembro,
                                  codigoAfiliacao = :codigoAfiliacao,
                                  nomeColumba = :nomeColumba,
                                  paisAtivosOA = :paisAtivosOA,
                                  reunioes = :reunioes,
                                  ensaios = :ensaios,
			          iniciacoesLojaCapitulo = :iniciacoesLojaCapitulo,
                                  aposicaoNome = :aposicaoNome,
                                  casamento = :casamento,
                                  convocacoesRitualisticas = :convocacoesRitualisticas,
                                  iniciacoesGraus = :iniciacoesGraus,
                                  instalacao = :instalacao,
                                  avaliacao = :avaliacao,
                                  comentarios = :comentarios,
                                  ultimoAtualizar = :ultimoAtualizar
                                  WHERE idAtividadeColumba = :idAtividadeColumba";
        //echo $sql;exit();
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastMembro', $this->seqCadastMembro, PDO::PARAM_INT);
        $sth->bindParam(':codigoAfiliacao', $this->codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':nomeColumba', $this->nomeColumba, PDO::PARAM_STR);
        $sth->bindParam(':paisAtivosOA', $this->paisAtivosOA, PDO::PARAM_INT);
        $sth->bindParam(':reunioes', $this->reunioes, PDO::PARAM_INT);
        $sth->bindParam(':ensaios', $this->ensaios, PDO::PARAM_INT);
        $sth->bindParam(':iniciacoesLojaCapitulo', $this->iniciacoesLojaCapitulo, PDO::PARAM_INT);
        $sth->bindParam(':aposicaoNome', $this->aposicaoNome, PDO::PARAM_INT);
        $sth->bindParam(':casamento', $this->casamento, PDO::PARAM_INT);
        $sth->bindParam(':convocacoesRitualisticas', $this->convocacoesRitualisticas, PDO::PARAM_INT);
        $sth->bindParam(':iniciacoesGraus', $this->iniciacoesGraus, PDO::PARAM_INT);
        $sth->bindParam(':instalacao', $this->instalacao, PDO::PARAM_INT);
        $sth->bindParam(':avaliacao', $this->avaliacao, PDO::PARAM_INT);
        $sth->bindParam(':comentarios', $this->comentarios, PDO::PARAM_STR);
        $sth->bindParam(':ultimoAtualizar', $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeColumba', $idAtividadeColumba, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function dataCriacao($trimestre,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM atividadeColumba where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and trimestre=:trimestre";
        $sql .= " and ano=:ano";
        $sql .= " order by dataCadastro ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function dataUltimaAlteracao($trimestre,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM atividadeColumba where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and trimestre=:trimestre";
        $sql .= " and ano=:ano";
        $sql .= " order by dataCadastro DESC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastro'];
            }

        }
        return false;
    }

    public function responsaveis($trimestre,$ano, $idOrganismoAfiliado,$ultimaAlteracao=null) {

        $sql = "SELECT * FROM atividadeColumba  as r  left join usuario on seqCadast = ultimoAtualizar where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and trimestre=:trimestre";
        $sql .= " and ano=:ano and seqCadast <>0 order by r.dataCadastro";

        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $arr=array();

        $i=0;
        if($resultado)
        {
            foreach($resultado as $v)
            {
                $arr['data'][$i] = $v['dataCadastro'];
                $arr['seq'][$i] = $v['seqCadast'];
                $arr['nome'][$i] = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
                $i++;
            }

        }
        return $arr;
    }

}

?>
