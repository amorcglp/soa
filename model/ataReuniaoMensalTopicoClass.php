<?php

require_once ("conexaoClass.php");

class ataReuniaoMensalTopico{

    private $idAtaReuniaoMensalTopico;
    private $fk_idAtaReuniaoMensal;
    private $topico_um;
    private $topico_dois;
    private $topico_tres;
    private $topico_quatro;
    private $topico_cinco;
    private $topico_seis;
    private $topico_sete;
    private $topico_oito;
    private $topico_nove;
    private $topico_dez;

    //METODO SET/GET -----------------------------------------------------------

    function getIdAtaReuniaoMensalTopico() {
        return $this->idAtaReuniaoMensalTopico;
    }

    function getFk_idAtaReuniaoMensal() {
        return $this->fk_idAtaReuniaoMensal;
    }

	function getTopicoUm() {
        return $this->topico_um;
    }

	function getTopicoDois() {
        return $this->topico_dois;
    }

	function getTopicoTres() {
        return $this->topico_tres;
    }

	function getTopicoQuatro() {
        return $this->topico_quatro;
    }

	function getTopicoCinco() {
        return $this->topico_cinco;
    }

	function getTopicoSeis() {
        return $this->topico_seis;
    }

	function getTopicoSete() {
        return $this->topico_sete;
    }

	function getTopicoOito() {
        return $this->topico_oito;
    }

	function getTopicoNove() {
        return $this->topico_nove;
    }

	function getTopicoDez() {
        return $this->topico_dez;
    }

    function setIdAtaReuniaoMensalTopico($idAtaReuniaoMensalTopico) {
        $this->idAtaReuniaoMensalTopico = $idAtaReuniaoMensalTopico;
    }

	function setFk_idAtaReuniaoMensal($fk_idAtaReuniaoMensal) {
        $this->fk_idAtaReuniaoMensal = $fk_idAtaReuniaoMensal;
    }

	function setTopicoUm($topico_um) {
        $this->topico_um = $topico_um;
    }

	function setTopicoDois($topico_dois) {
        $this->topico_dois = $topico_dois;
    }

	function setTopicoTres($topico_tres) {
        $this->topico_tres = $topico_tres;
    }

	function setTopicoQuatro($topico_quatro) {
        $this->topico_quatro = $topico_quatro;
    }

	function setTopicoCinco($topico_cinco) {
        $this->topico_cinco = $topico_cinco;
    }

	function setTopicoSeis($topico_seis) {
        $this->topico_seis = $topico_seis;
    }

	function setTopicoSete($topico_sete) {
        $this->topico_sete = $topico_sete;
    }

	function setTopicoOito($topico_oito) {
        $this->topico_oito = $topico_oito;
    }

	function setTopicoNove($topico_nove) {
        $this->topico_nove = $topico_nove;
    }

	function setTopicoDez($topico_dez) {
        $this->topico_dez = $topico_dez;
    }

    //--------------------------------------------------------------------------

    public function cadastroAtaReuniaoMensalTopico(){

        $sql = "INSERT INTO ataReuniaoMensal_topico
                                 (
                                  fk_idAtaReuniaoMensal,
                                  topico_um,
                                  topico_dois,
                                  topico_tres,
                                  topico_quatro,
                                  topico_cinco,
                                  topico_seis,
                                  topico_sete,
                                  topico_oito,
                                  topico_nove,
                                  topico_dez
                                  )
                            VALUES (:fk_idAtaReuniaoMensal,
                                    :topico_um,
                                    :topico_dois,
                                    :topico_tres,
                                    :topico_quatro,
                                    :topico_cinco,
                                    :topico_seis,
                                    :topico_sete,
                                    :topico_oito,
                                    :topico_nove,
                                    :topico_dez
                                    )";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtaReuniaoMensal', $this->fk_idAtaReuniaoMensal, PDO::PARAM_INT);
        $sth->bindParam(':topico_um', $this->topico_um, PDO::PARAM_STR);
        $sth->bindParam(':topico_dois', $this->topico_dois, PDO::PARAM_STR);
        $sth->bindParam(':topico_tres', $this->topico_tres, PDO::PARAM_STR);
        $sth->bindParam(':topico_quatro', $this->topico_quatro, PDO::PARAM_STR);
        $sth->bindParam(':topico_cinco', $this->topico_cinco, PDO::PARAM_STR);
        $sth->bindParam(':topico_seis', $this->topico_seis, PDO::PARAM_STR);
        $sth->bindParam(':topico_sete', $this->topico_sete, PDO::PARAM_STR);
        $sth->bindParam(':topico_oito', $this->topico_oito, PDO::PARAM_STR);
        $sth->bindParam(':topico_nove', $this->topico_nove, PDO::PARAM_STR);
        $sth->bindParam(':topico_dez', $this->topico_dez, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

	public function listaTopicos($idAtaReuniaoMensal){

    	$sql = "SELECT * FROM ataReuniaoMensal_topico where 1=1";

    	if($idAtaReuniaoMensal!=null) {
    		$sql .= " and fk_idAtaReuniaoMensal=:fk_idAtaReuniaoMensal";
    	}
    	//echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idAtaReuniaoMensal!=null) {
            $sth->bindParam(':fk_idAtaReuniaoMensal', $idAtaReuniaoMensal, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function removeTopicos($idAtaReuniaoMensal){
        $sql = "DELETE FROM ataReuniaoMensal_topico WHERE fk_idAtaReuniaoMensal=:fk_idAtaReuniaoMensal";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtaReuniaoMensal', $idAtaReuniaoMensal, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    
}

?>
