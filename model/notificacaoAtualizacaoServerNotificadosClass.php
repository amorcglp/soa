<?php

require_once ("conexaoClass.php");

class NotificacaoServerNotificados {

    public $idNotificacaoNotificados;
    public $fk_idNotificacao;
    public $seqCadast;
    public $data;

    /* Funções GET e SET */

    function getIdNotificacaoNotificados() {
        return $this->idNotificacaoNotificados;
    }

    function getFk_idNotificacao() {
        return $this->fk_idNotificacao;
    }

    function getSeqCadast() {
        return $this->seqCadast;
    }

    function getData() {
        return $this->data;
    }

    function setIdNotificacaoNotificados($idNotificacaoNotificados) {
        $this->idNotificacaoNotificados = $idNotificacaoNotificados;
    }

    function setFk_idNotificacao($fk_idNotificacao) {
        $this->fk_idNotificacao = $fk_idNotificacao;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    function setData($data) {
        $this->data = $data;
    }

    
    public function cadastra() {
        
        $sql = "INSERT INTO notificacoesServerNotificados (
                                `fk_idNotificacao`, 
        			`seqCadast`,`data`)
                                    VALUES (
                                            :fk_idNotificacao,
                                            :seqCadast,
                                            now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':fk_idNotificacao', $this->fk_idNotificacao, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast , PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function lista($idNotificacao=null,$data=null,$igualDataNotificacao=null) {

    	$sql = "SELECT * FROM notificacoesServerNotificados  "
                . " inner join notificacoesServer on idNotificacao = fk_idNotificacao where 1=1";
    	if($idNotificacao!=null)
    	{
    		$sql .= " and fk_idNotificacao = :id ";
    	}
        if($data!=null)
    	{
    		$sql .= " and DATE(data) = :data";
    	}
        if($igualDataNotificacao!=null)
    	{
    		$sql .= " and DATE(dataNotificacao) = :igualDataNotificacao";
                $sql .= " group by seqCadast ";
    	}
        
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idNotificacao!=null)
    	{
            $sth->bindParam(':id', $idNotificacao, PDO::PARAM_STR);
        }
        if($data!=null)
    	{
            $sth->bindParam(':data', $data, PDO::PARAM_STR);
        }
        if($igualDataNotificacao!=null)
    	{
            $sth->bindParam(':igualDataNotificacao', $data, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>