<?php

require_once ("conexaoClass.php");

class higienizacaoIniciacoes {

    public $idHigienizacaoIniciacoes;
    public $fk_idAtividadeIniciaticaMembro;
    public $seqCadast;
    public $fk_idAtividadeIniciatica;
    public $regiao;
    public $siglaOA;

    /**
     * @return mixed
     */
    public function getIdHigienizacaoIniciacoes()
    {
        return $this->idHigienizacaoIniciacoes;
    }

    /**
     * @param mixed $idHigienizacaoIniciacoes
     */
    public function setIdHigienizacaoIniciacoes($idHigienizacaoIniciacoes)
    {
        $this->idHigienizacaoIniciacoes = $idHigienizacaoIniciacoes;
    }

    /**
     * @return mixed
     */
    public function getFkIdAtividadeIniciaticaMembro()
    {
        return $this->fk_idAtividadeIniciaticaMembro;
    }

    /**
     * @param mixed $fk_idAtividadeIniciaticaMembro
     */
    public function setFkIdAtividadeIniciaticaMembro($fk_idAtividadeIniciaticaMembro)
    {
        $this->fk_idAtividadeIniciaticaMembro = $fk_idAtividadeIniciaticaMembro;
    }

    /**
     * @return mixed
     */
    public function getSeqCadast()
    {
        return $this->seqCadast;
    }

    /**
     * @param mixed $seqCadast
     */
    public function setSeqCadast($seqCadast)
    {
        $this->seqCadast = $seqCadast;
    }

    /**
     * @return mixed
     */
    public function getFkIdAtividadeIniciatica()
    {
        return $this->fk_idAtividadeIniciatica;
    }

    /**
     * @param mixed $fk_idAtividadeIniciatica
     */
    public function setFkIdAtividadeIniciatica($fk_idAtividadeIniciatica)
    {
        $this->fk_idAtividadeIniciatica = $fk_idAtividadeIniciatica;
    }

    /**
     * @return mixed
     */
    public function getRegiao()
    {
        return $this->regiao;
    }

    /**
     * @param mixed $regiao
     */
    public function setRegiao($regiao)
    {
        $this->regiao = $regiao;
    }

    /**
     * @return mixed
     */
    public function getSiglaOA()
    {
        return $this->siglaOA;
    }

    /**
     * @param mixed $siglaOA
     */
    public function setSiglaOA($siglaOA)
    {
        $this->siglaOA = $siglaOA;
    }



    
    public function cadastraHigienizacaoIniciacoes() {
        
        $sql = "INSERT INTO higienizacaoIniciacoes (fk_idAtividadeIniciaticaMembro,seqCadast,fk_idAtividadeIniciatica,regiao, siglaOA, dataCadastro)
                                    VALUES (:fk_idAtividadeIniciaticaMembro,
                                            :seqCadast,
                                            :fk_idAtividadeIniciatica,
                                            :regiao,
                                            :siglaOA,
                                            now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        echo "<br>===================";
        echo "<br>fk_idAtividadeIniciaticaMembro:".$this->fk_idAtividadeIniciaticaMembro;
        echo "<br>seqCadast:".$this->seqCadast;
        echo "<br>fk_idAtividadeIniciatica:".$this->fk_idAtividadeIniciatica;
        echo "<br>regiao:".$this->regiao;
        echo "<br>siglaOA:".$this->siglaOA;

        $sth->bindParam(':fk_idAtividadeIniciaticaMembro', $this->fk_idAtividadeIniciaticaMembro, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeIniciatica', $this->fk_idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_STR);
        $sth->bindParam(':siglaOA', $this->siglaOA, PDO::PARAM_STR);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

    
    public function listaHigienizacaoIniciacoes() {

        $sql =" SELECT * FROM higienizacaoIniciacoes as b "
               . " inner join organismoAfiliado as o on o.siglaOrganismoAfiliado = b.siglaOA "
               . " where 1=1 ";
        $sql .=" ORDER BY o.siglaOrganismoAfiliado";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaHigienizacaoIniciacoesHoje() {

       $sql =" SELECT * FROM higienizacaoIniciacoes as b "
               . " inner join organismoAfiliado as o on o.siglaOrganismoAfiliado = b.siglaOA "
               . " where 1=1 and (DATE(b.dataCadastro)='2018-12-12' 
                                    or DATE(b.dataCadastro)='2018-12-13'
                                    or DATE(b.dataCadastro)='2018-12-14'
                                    or DATE(b.dataCadastro)='2018-12-15'
                                    or DATE(b.dataCadastro)='2018-12-16'
                                    or DATE(b.dataCadastro)='2018-12-17')";

       
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
       

        $resultado = $c->run($sth);

        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function limpaTabela() {
        
        $sql = "TRUNCATE higienizacaoIniciacoes";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($c->run($sth,true)) {
            return true;
        } else {
            return false;
        }
    }

}

?>