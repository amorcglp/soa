<?php

require_once ("conexaoClass.php");

class formacao {

    public $SeqFormac;
    public $DesFormac;
    
    function getSeqFormac() {
        return $this->SeqFormac;
    }

    function getDesFormac() {
        return $this->DesFormac;
    }

    function setSeqFormac($SeqFormac) {
        $this->SeqFormac = $SeqFormac;
    }

    function setDesFormac($DesFormac) {
        $this->DesFormac = $DesFormac;
    }

    
    public function lista($cod=null) {
        
        $sql = "SELECT * FROM formacao where 1=1 ";
        if($cod!=null)
        {
            $sql .= " and SeqFormac=:cod";
        }  
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($cod!=null)
        {
            $sth->bindParam(':cod', $cod, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>