<?php

require_once ("consultaClass.php");
require_once ("conexaoClass.php");

class listaMembrosAtividadeIniciaticaAssinada{

    private $idListaMembrosAtividadeIniciaticaAssinada;
    private $fk_idAtividadeIniciatica;
    private $caminho;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdListaMembrosAtividadeIniciaticaAssinada() {
        return $this->idListaMembrosAtividadeIniciaticaAssinada;
    }

    function getFk_idAtividadeIniciatica() {
        return $this->fk_idAtividadeIniciatica;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function setIdListaMembrosAtividadeIniciaticaAssinada($idListaMembrosAtividadeIniciaticaAssinada) {
        $this->idListaMembrosAtividadeIniciaticaAssinada = $idListaMembrosAtividadeIniciaticaAssinada;
    }

    function setFk_idAtividadeIniciatica($fk_idAtividadeIniciatica) {
        $this->fk_idAtividadeIniciatica = $fk_idAtividadeIniciatica;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    
    //--------------------------------------------------------------------------

    public function cadastroListaMembrosAssinada(){

        $sql = "INSERT INTO listaMembrosAtividadeIniciatica_Assinada  
                                 (
                                  fk_idAtividadeIniciatica,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:id,
                                    :caminho,
                                    now())";
        //echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->fk_idAtividadeIniciatica, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'listaMembrosAtividadeIniciatica_Assinada'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
	        foreach ($resultado as $vetor)
	    	{
	    		$proximo_id = $vetor['Auto_increment']; 
	    	}
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaMembrosAssinada($id=null,$idOrganismoAfiliado=null){

    	$sql = "SELECT * FROM listaMembrosAtividadeIniciatica_Assinada
    	inner join atividadeIniciatica on fk_idAtividadeIniciatica = idAtividadeIniciatica
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idAtividadeIniciatica = :id";
    	}
    	if($idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado = :idOrganismoAfiliado";
    	}
    	//echo "<br><br>==>".$sql."<br>";
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);        
        }
        if($idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeListaMembrosAssinada($id){
        
        $sql = "DELETE FROM listaMembrosAtividadeIniciatica_Assinada "
                . " WHERE idListaMembrosAtividadeIniciaticaAssinada = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
