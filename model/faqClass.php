<?php

require_once ("conexaoClass.php");

class Faq {

    public $idFaq;
    public $seq_cadast;
    public $nomeUsuarioFaq;
    public $codigoDeAfiliacaoUsuarioFaq;
    public $emailUsuarioFaq;
    public $fk_idOrganismoAfiliado;
    public $tituloPerguntaFaq;
    public $perguntaFaq;
    public $dataFaq;
    public $statusFaq;

    /* Funções GET e SET */

    function getIdFaq() {
        return $this->idFaq;
    }

    function getSeq_cadast() {
        return $this->seq_cadast;
    }

    function getNomeUsuarioFaq() {
        return $this->nomeUsuarioFaq;
    }

    function getCodigoDeAfiliacaoUsuarioFaq() {
        return $this->codigoDeAfiliacaoUsuarioFaq;
    }

    function getEmailUsuarioFaq() {
        return $this->emailUsuarioFaq;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getTituloPerguntaFaq() {
        return $this->tituloPerguntaFaq;
    }

    function getPerguntaFaq() {
        return $this->perguntaFaq;
    }

    function getDataFaq() {
        return $this->dataFaq;
    }

    function setIdFaq($idFaq) {
        $this->idFaq = $idFaq;
    }

    function setSeq_cadast($seq_cadast) {
        $this->seq_cadast = $seq_cadast;
    }

    function setNomeUsuarioFaq($nomeUsuarioFaq) {
        $this->nomeUsuarioFaq = $nomeUsuarioFaq;
    }

    function setCodigoDeAfiliacaoUsuarioFaq($codigoDeAfiliacaoUsuarioFaq) {
        $this->codigoDeAfiliacaoUsuarioFaq = $codigoDeAfiliacaoUsuarioFaq;
    }

    function setEmailUsuarioFaq($emailUsuarioFaq) {
        $this->emailUsuarioFaq = $emailUsuarioFaq;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setTituloPerguntaFaq($tituloPerguntaFaq) {
        $this->tituloPerguntaFaq = $tituloPerguntaFaq;
    }

    function setPerguntaFaq($perguntaFaq) {
        $this->perguntaFaq = $perguntaFaq;
    }

    function setDataFaq($dataFaq) {
        $this->dataFaq = $dataFaq;
    }

    function getStatusFaq() {
        return $this->statusFaq;
    }

    function setStatusFaq($statusFaq) {
        $this->statusFaq = $statusFaq;
    }

    public function cadastraFaq() {
        
        $sql = "INSERT INTO faq (`idFaq`, `seq_cadast`, `nomeUsuarioFaq`, `codigoDeAfiliacaoUsuarioFaq`, `emailUsuarioFaq`, `fk_idOrganismoAfiliado`, `tituloPerguntaFaq`, `perguntaFaq`, `dataFaq`)
                                    VALUES (:id,
                                            :seqCadast,
                                            :nomeUsuario,
                                            :codigoAfiliacao,
                                            :emailUsuario,
                                            :idOrganismoAfiliado,
                                            :titulo,
                                            :pergunta,
                                            now())";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idFaq, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $this->seq_cadast, PDO::PARAM_INT);
        $sth->bindParam(':nomeUsuario', $this->nomeUsuarioFaq, PDO::PARAM_STR);
        $sth->bindParam(':codigoAfiliacao', $this->codigoDeAfiliacaoUsuarioFaq, PDO::PARAM_INT);
        $sth->bindParam(':emailUsuario', $this->emailUsuarioFaq, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':titulo', $this->tituloPerguntaFaq, PDO::PARAM_STR);
        $sth->bindParam(':pergunta', $this->perguntaFaq, PDO::PARAM_STR);
        
        
        if ($c->run($sth,true)){
            return true;
        }else{

            return false;
        }
    }

    public function listaFaq() {

        $sql = "SELECT * FROM faq";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdFaq() {
        
        $sql = "SELECT * FROM faq 
                                 WHERE `idFaq` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {    
            $sth->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function alteraStatusFaq() {

        $sql = "UPDATE faq SET `statusFaq` = :status"
                . " WHERE `idFaq` = :idFaq";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusFaq, PDO::PARAM_INT);
        $sth->bindParam(':idFaq', $this->idFaq, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
		
        if ($resultado) {
            $arr = array();
            $arr['status'] = $this->getStatusFaq();
            return $arr;
        } else {
            return false;
        }
    }

}

?>