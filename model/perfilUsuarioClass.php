<?php

require_once ("conexaoClass.php");

class perfilUsuario{

    private $avatarUsuario;
    private $seqCadast;
    private $nomeUsuario;
    private $emailUsuario;
    private $telefoneResidencialUsuario;
    private $telefoneComercialUsuario;
    private $celularUsuario;
    private $loginUsuario;
    private $senhaUsuario;
    private $codigoDeAfiliacao;
    private $fk_idDepartamento;
    private $cienteAssinaturaDigital;
    private $dataAssinaturaDigital;
    
    // Avatar do Usuário
	function getAvatarUsuario() {
        return $this->avatarUsuario;
    }
    function setAvatarUsuario($avatarUsuario) {
        $this->avatarUsuario = $avatarUsuario;
    }
    
    // Dados de Usuário
	function getSeqCadast() {
        return $this->seqCadast;
    }
	function getNomeUsuario() {
        return $this->nomeUsuario;
    }
	function getEmailUsuario() {
        return $this->emailUsuario;
    }
	function getTelefoneResidencialUsuario() {
        return $this->telefoneResidencialUsuario;
    }
	function getTelefoneComercialUsuario() {
        return $this->telefoneComercialUsuario;
    }
	function getCelularUsuario() {
        return $this->celularUsuario;
    }
	function getLoginUsuario() {
        return $this->loginUsuario;
    }
	function getSenhaUsuario() {
        return $this->senhaUsuario;
    }
	function getCodigoDeAfiliacao() {
        return $this->codigoDeAfiliacao;
    }
    public function getFkIdDepartamento(){
        return $this->fk_idDepartamento;
    }
    public function getCienteAssinaturaDigital(){
        return $this->cienteAssinaturaDigital;
    }

    public function getDataAssinaturaDigital(){
        return $this->dataAssinaturaDigital;
    }

    function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }
	function setNomeUsuario($nomeUsuario) {
        $this->nomeUsuario = $nomeUsuario;
    }
	function setEmailUsuario($emailUsuario) {
        $this->emailUsuario = $emailUsuario;
    }
	function setTelefoneResidencialUsuario($telefoneResidencialUsuario) {
        $this->telefoneResidencialUsuario = $telefoneResidencialUsuario;
    }
	function setTelefoneComercialUsuario($telefoneComercialUsuario) {
        $this->telefoneComercialUsuario = $telefoneComercialUsuario;
    }
	function setCelularUsuario($celularUsuario) {
        $this->celularUsuario = $celularUsuario;
    }
	function setLoginUsuario($loginUsuario) {
        $this->loginUsuario = $loginUsuario;
    }
	function setSenhaUsuario($senhaUsuario) {
        $this->senhaUsuario = $senhaUsuario;
    }
	function setCodigoDeAfiliacao($codigoDeAfiliacao) {
        $this->codigoDeAfiliacao = $codigoDeAfiliacao;
    }
    public function setFkIdDepartamento($fk_idDepartamento){
        $this->fk_idDepartamento = $fk_idDepartamento;
    }
    public function setCienteAssinaturaDigital($cienteAssinaturaDigital){
        $this->cienteAssinaturaDigital = $cienteAssinaturaDigital;
    }
    public function setDataAssinaturaDigital($dataAssinaturaDigital){
        $this->dataAssinaturaDigital = $dataAssinaturaDigital;
    }


    //--------------------------------------------------------------------------
    
    public function alteraUsuario($seqCadast){
        
        $sql = "UPDATE usuario SET  
                    `nomeUsuario` = :nome,
                    `emailUsuario` = :email,
                    `telefoneResidencialUsuario` = :telefoneResidencial,
                    `telefoneComercialUsuario` = :telefoneComercial,
                    `celularUsuario` = :celular,
                    `loginUsuario` = :login,
                    `senhaUsuario` = :senha";
		if ($this->getAvatarUsuario() != "") {
        	$sql .= ", `avatarUsuario` = :avatarUsuario";
		}
        $sql .= " WHERE seqCadast = :seqCadast";
        
        //echo $query;
	    $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':nome', $this->nomeUsuario, PDO::PARAM_INT);
        $sth->bindParam(':email', $this->emailUsuario , PDO::PARAM_INT);
        $sth->bindParam(':telefoneResidencial', $this->telefoneResidencialUsuario, PDO::PARAM_INT);
        $sth->bindParam(':telefoneComercial', $this->telefoneComercialUsuario , PDO::PARAM_INT);
        $sth->bindParam(':celular', $this->celularUsuario, PDO::PARAM_INT);
        $sth->bindParam(':login', $this->loginUsuario , PDO::PARAM_INT);
        $sth->bindParam(':senha', $this->senhaUsuario, PDO::PARAM_STR);
        if ($this->getAvatarUsuario() != "") {
            $sth->bindParam(':avatarUsuario',  $this->avatarUsuario, PDO::PARAM_INT);
        }
        $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;  
        } else {
            return false;
            
        }
        
    }

    public function listaAvatarUsuario($id){

        $sql="SELECT avatarUsuario,nomeUsuario,seqCadast FROM usuario where seqCadast=:id";
	    $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function buscaIdUsuario($idUsuario){
        
        $sql = "SELECT * FROM  usuario
                                WHERE   seqCadast = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idUsuario, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaContato($pesquisa,$notEqual){
        
        $sql="SELECT * FROM usuario WHERE nomeUsuario LIKE :pesquisa and seqCadast <> :notEqual and statusUsuario=0";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $pesquisa='%'.$pesquisa.'%';
        
        $sth->bindParam(':pesquisa', $pesquisa, PDO::PARAM_STR);
        $sth->bindParam(':notEqual', $notEqual, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaContatoInicioMaximo($inicio,$maximo,$pesquisa,$notEqual){
        
        $sql = "SELECT seqCadast,
                                avatarUsuario,
                                nomeUsuario,
                                emailUsuario,
                                telefoneResidencialUsuario,
                                telefoneComercialUsuario,
                                celularUsuario,
                                siglaOA,
                                codigoDeAfiliacao
                        FROM usuario WHERE nomeUsuario LIKE :pesquisa
                        and seqCadast <> :notEqual 
                        and statusUsuario=0 
                        ORDER BY seqCadast LIMIT :inicio,:maximo";

        //echo $query;

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $pesquisa = '%'.$pesquisa.'%';
        $inicio = (int) $inicio;
        $maximo = (int) $maximo;
        
        $sth->bindParam(':pesquisa', $pesquisa, PDO::PARAM_STR);
        $sth->bindParam(':notEqual', $notEqual, PDO::PARAM_INT);
        $sth->bindParam(':inicio', $inicio, PDO::PARAM_INT);
        $sth->bindParam(':maximo', $maximo, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
}

?>
