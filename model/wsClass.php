<?php

set_time_limit(1000);

require_once __DIR__ . '/../loadEnv.php';

class Ws {

    private $url = 'http://192.1.1.{server}:{porta}/datasnap/rest/TServerMethods1/';
    //private $url = 'http://192.1.1.135:18085/datasnap/rest/TServerMethods1/';
    private $server_default;

    public function __construct($server_cron=null) {
        date_default_timezone_set('America/Sao_Paulo');
        srand((double) microtime() * 1000000);
        $this->server_cron      = $server_cron;
        $this->server_default   = getenv('WS_ACTUMPLUS_IP');
    }

    public function callMethod($methodName, $params, $code, $isUser = true) {

        $params['DatHoraProces'] = date('Y-m-d\TH:i:s');
        $params['NumProces'] = $this->getRequisitionValidationCode($code, $isUser);
        //echo "<pre>";print_r($params);
        //exit();
        $url = $this->url . $methodName;

        foreach ($params as $key => $value) {
            $url .= '/' . $value;
        }
        for ($i = 0; $i < 10; $i++) {
            try {
                $server = $this->server_cron == null ? $this->server_default : $this->server_cron;
                /*
                108 -> Desenvolvimento
                135 -> Produção
                138 -> Produção - CRON
                */
                
                //if (($i % 2) == 0) { $server = 135; } else { $server = 108; }
                //$urlWs = str_replace("{porta}", rand(18085, 18094), $url);

                $urlWs = str_replace("{porta}", rand(18085, 18094), $url);
                $urlWs = str_replace("{server}", $server, $urlWs);
                //echo "url: ".$this->url."<br>";
                //echo "urlWs: ".$urlWs."<br>";
                //echo "Posicao: ".$i."<br>";

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $urlWs);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);

                $output = curl_exec($ch);
                //echo "urlencode(ch): ".urlencode($ch)."<br>";
                $obj = json_decode(json_encode(json_decode($output)), true);
                //echo $ch;
                if (empty($obj["result"])) {
                    //echo "Deu erro e não deu break!<br>";
                } elseif (!empty($obj["fDesMensag"])) {
                    if (substr($obj["fDesMensag"], 0, 5) == 'Erro!') {
                        echo "Deu erro e não deu break!<br>" . $obj["fDesMensag"] . " -> " . $ch;
                    } else {
                        break;
                    }
                } else {
                    break;
                }

                // echo "sai do try";
            } catch (Exception $e) {
                echo 'Exceção capturada: ', $e->getMessage(), "\n";
            }
        }
        curl_close($ch);
        return json_decode($output);
    }

    private function getRequisitionValidationCode($code, $isUser) {

        if (!is_numeric($code) && !($isUser)) {
            return false;
        } else if (($isUser) && !is_numeric($code)) {
            $code = $this->getUserKeyCode($code);
        } else if (($isUser) && is_numeric($code)) {
            return false;
        }

        $date = date('dmYHi') + 0;
        //echo $date; exit;

        $codMult = $code * 17;

        $rest = floor(fmod($date, $codMult));


        $i = 9;
        $sum = 0;
        for ($j = strlen($rest); $j > 0; $j--) {
            $sum += (substr($rest, $j - 1, 1) * $i);
            $i--;

            if ($i == 0)
                $i = 9;
        }
        $mod = $sum % 11;

        if ($mod > 9)
            $dv = 0;
        else
            $dv = $mod;

        return $sum . $dv;
    }

    private function getUserKeyCode($user) {

        $sum = 0;
        for ($i = 0; $i < strlen($user); $i++) {
            $sum += ord(substr($user, $i, 1)) * ($i + 1);
        }
        $sum = $sum * $i;

        return $sum;
    }

}

?>