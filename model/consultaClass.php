<?php

require_once("conexaoClass.php");

class consultaSOA {

    private $query;

    public function __construct() {
        
        $c = new conexaoSOA();

        $con = $c->openSOA();
        //$c->checkStatus();
        //echo "<pre>";print_r($con);
        return $con;
    }

    public function getQuery() {

        return $this->query;
    }

    public function setQuery($query) {

        $this->query = $query;
    }
   
    public function executaConsulta() {

        try {

            echo "<h3>ERRO! Esta sendo usando um modo nao seguro de fazer as consultas a base de dados!</h3>";

            return true;
        } catch (PDOException $e) {

            echo "<h3>ERRO! " . $e->getMessage() . "</h3>";
        }
    }

}
?>