<?php

require_once ("consultaClass.php");

class Ticket{

    private $idTicket;
    private $tituloTicket;
    private $descricaoTicket;
    private $criadoPorTicket;
    private $criadoPorDepartTicket;
    private $atribuidoTicket;
    private $atribuidoDepartTicket;
    private $statusTicket;
    private $dataCriacaoTicket;
    private $dataAtualizacaoTicket;
    private $prioridadeTicket;
    private $tagTicket;
    private $identificadorTicket;
    private $fk_idFuncionalidade;
    private $fk_idTicketFinalidade;
    private $fk_idOrganismoAfiliado;

    private $idFuncionalidade;
    private $nomeFuncionalidade;
    private $corpoFuncionalidade;
    private $referenciaIdFuncionalidade;

    //METODO SET/GET -----------------------------------------------------------

    public function getIdTicket() {
        return $this->idTicket;
    }
    public function getTituloTicket() {
        return $this->tituloTicket;
    }
    public function getDescricaoTicket() {
        return $this->descricaoTicket;
    }
    public function getCriadoPorTicket() {
        return $this->criadoPorTicket;
    }
    public function getCriadoPorDepartTicket() {
        return $this->criadoPorDepartTicket;
    }
    public function getAtribuidoTicket() {
        return $this->atribuidoTicket;
    }
    public function getAtribuidoDepartTicket() {
        return $this->atribuidoDepartTicket;
    }
    public function getStatusTicket() {
        return $this->statusTicket;
    }
    public function getDataCriacaoTicket() {
        return $this->dataCriacaoTicket;
    }
    public function getDataAtualizacaoTicket() {
        return $this->dataAtualizacaoTicket;
    }
    public function getPrioridadeTicket() {
        return $this->prioridadeTicket;
    }
    public function getTagTicket() {
        return $this->tagTicket;
    }
    public function getIdentificadorTicket() {
        return $this->identificadorTicket;
    }
    public function getFkIdFuncionalidade() {
        return $this->fk_idFuncionalidade;
    }
    public function getFkIdTicketFinalidade() {
        return $this->fk_idTicketFinalidade;
    }
    public function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    public function getIdFuncionalidade() {
        return $this->idFuncionalidade;
    }
    public function getNomeFuncionalidade() {
        return $this->nomeFuncionalidade;
    }
    public function getCorpoFuncionalidade() {
        return $this->corpoFuncionalidade;
    }
    public function getReferenciaIdFuncionalidade() {
        return $this->referenciaIdFuncionalidade;
    }



    public function setIdTicket($idTicket) {
        $this->idTicket = $idTicket;
    }
    public function setTituloTicket($tituloTicket) {
        $this->tituloTicket = $tituloTicket;
    }
    public function setDescricaoTicket($descricaoTicket) {
        $this->descricaoTicket = $descricaoTicket;
    }
    public function setCriadoPorTicket($criadoPorTicket) {
        $this->criadoPorTicket = $criadoPorTicket;
    }
    public function setCriadoPorDepartTicket($criadoPorDepartTicket) {
        $this->criadoPorDepartTicket = $criadoPorDepartTicket;
    }
    public function setAtribuidoTicket($atribuidoTicket) {
        $this->atribuidoTicket = $atribuidoTicket;
    }
    public function setAtribuidoDepartTicket($atribuidoDepartTicket) {
        $this->atribuidoDepartTicket = $atribuidoDepartTicket;
    }
    public function setStatusTicket($statusTicket) {
        $this->statusTicket = $statusTicket;
    }
    public function setDataCriacaoTicket($dataCriacaoTicket) {
        $this->dataCriacaoTicket = $dataCriacaoTicket;
    }
    public function setDataAtualizacaoTicket($dataAtualizacaoTicket) {
        $this->dataAtualizacaoTicket = $dataAtualizacaoTicket;
    }
    public function setPrioridadeTicket($prioridadeTicket) {
        $this->prioridadeTicket = $prioridadeTicket;
    }
    public function setTagTicket($tagTicket) {
        $this->tagTicket = $tagTicket;
    }
    public function setIdentificadorTicket($identificadorTicket) {
        $this->identificadorTicket = $identificadorTicket;
    }
    public function setFkIdFuncionalidade($fk_idFuncionalidade) {
        $this->fk_idFuncionalidade = $fk_idFuncionalidade;
    }
    public function setFkIdTicketFinalidade($fk_idTicketFinalidade) {
        $this->fk_idTicketFinalidade = $fk_idTicketFinalidade;
    }
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    public function setIdFuncionalidade($idFuncionalidade) {
        $this->idFuncionalidade = $idFuncionalidade;
    }
    public function setNomeFuncionalidade($nomeFuncionalidade) {
        $this->nomeFuncionalidade = $nomeFuncionalidade;
    }
    public function setCorpoFuncionalidade($corpoFuncionalidade) {
        $this->corpoFuncionalidade = $corpoFuncionalidade;
    }
    public function setReferenciaIdFuncionalidade($referenciaIdFuncionalidade) {
        $this->referenciaIdFuncionalidade = $referenciaIdFuncionalidade;
    }




    //--------------------------------------------------------------------------

    public function cadastroTicket(){

        $sql = "INSERT INTO ticket
                                 (tituloTicket,
                                  descricaoTicket,
                                  criadoPorTicket,
                                  criadoPorDepartTicket,
                                  atribuidoDepartTicket,
                                  dataCriacaoTicket,
                                  prioridadeTicket,
                                  tagTicket,
                                  identificadorTicket,
                                  fk_idFuncionalidade,
                                  fk_idTicketFinalidade,
                                  fk_idOrganismoAfiliado)
                            VALUES (:tituloTicket,
		                            :descricaoTicket,
		                            :criadoPorTicket,
		                            :criadoPorDepartTicket,
		                            2,
		                            NOW(),
		                            :prioridadeTicket,
		                            :tagTicket,
		                            :identificadorTicket,
		                            :fk_idFuncionalidade,
                                    :fk_idTicketFinalidade,
		                            :fk_idOrganismoAfiliado)";
        //echo "| SQL: ".$sql."| <br>";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getTituloTicket            = $this->getTituloTicket();
        $getDescricaoTicket         = $this->getDescricaoTicket();
        $getCriadoPorTicket         = $this->getCriadoPorTicket();
        $getCriadoPorDepartTicket   = $this->getCriadoPorDepartTicket();
        $getPrioridadeTicket        = $this->getPrioridadeTicket();
        $getTagTicket               = trim($this->getTagTicket());
        $getIdentificadorTicket     = $this->getIdentificadorTicket();
        $getFkIdFuncionalidade      = $this->getFkIdFuncionalidade();
        $getFkIdTicketFinalidade    = $this->getFkIdTicketFinalidade();
        $getFkIdOrganismoAfiliado   = $this->getFkIdOrganismoAfiliado();

        $sth->bindParam(':tituloTicket', $getTituloTicket, PDO::PARAM_STR);
        $sth->bindParam(':descricaoTicket', $getDescricaoTicket, PDO::PARAM_STR);
        $sth->bindParam(':criadoPorTicket', $getCriadoPorTicket, PDO::PARAM_INT);
        $sth->bindParam(':criadoPorDepartTicket', $getCriadoPorDepartTicket, PDO::PARAM_INT);
        $sth->bindParam(':prioridadeTicket', $getPrioridadeTicket, PDO::PARAM_INT);
        $sth->bindParam(':tagTicket', $getTagTicket, PDO::PARAM_STR);
        $sth->bindParam(':identificadorTicket', $getIdentificadorTicket, PDO::PARAM_INT);
        $sth->bindParam(':fk_idFuncionalidade', $getFkIdFuncionalidade, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicketFinalidade', $getFkIdTicketFinalidade, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $getFkIdOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function selecionaUltimoId() {

        $sql= "SHOW TABLE STATUS LIKE 'ticket'";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);;
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoIdInserido(){

        $sql= "SELECT MAX(idTicket) as lastid FROM ticket";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function listaFinalidade(){

        $sql = "SELECT * FROM ticketFinalidade WHERE 1=1 and statusTicketFinalidade=1";
        //echo "| SQL: ".$sql."| <br>";
        
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function listaTicket($idOrganismo,$pagina,$status,$organismo,$funcionalidade,$atribuicao,$finalidade){

        $paginaInicial = ($pagina-1)*10;

    	$sql = "SELECT * FROM ticket WHERE 1=1";
        $sql .= $idOrganismo!=null ? " and fk_idOrganismoAfiliado=:idOrganismo" : "";
        $sql .= $status!=null ? " and statusTicket=:status" : "";
        $sql .= $organismo!=null ? " and fk_idOrganismoAfiliado=:organismo" : "";
        $sql .= $funcionalidade!=null ? " and fk_idFuncionalidade=:funcionalidade" : "";
        $sql .= $atribuicao!=null ? " and atribuidoTicket=:atribuicao" : "";
        $sql .= $finalidade!=null ? " and fk_idTicketFinalidade=:fk_idTicketFinalidade" : "";
        $sql .= ($pagina != null) ? " ORDER BY idTicket DESC LIMIT :pagina,10" : " ORDER BY idTicket DESC LIMIT 0,10";
    	//echo "| SQL: ".$sql."| <br>";
        //
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($idOrganismo!=null)
            $sth->bindParam(':idOrganismo', $idOrganismo, PDO::PARAM_INT);
        if ($status!=null)
            $sth->bindParam(':status', $status, PDO::PARAM_INT);
        if ($organismo!=null)
            $sth->bindParam(':organismo', $organismo, PDO::PARAM_INT);
        if ($funcionalidade!=null)
            $sth->bindParam(':funcionalidade', $funcionalidade, PDO::PARAM_INT);
        if ($atribuicao!=null)
            $sth->bindParam(':atribuicao', $atribuicao, PDO::PARAM_INT);
        if ($finalidade!=null)
            $sth->bindParam(':fk_idTicketFinalidade', $finalidade, PDO::PARAM_INT);
        if ($pagina!=null)
            $sth->bindParam(':pagina', $paginaInicial, PDO::PARAM_INT);

        $data = array();
        if ($idOrganismo!=null)
            $data['idOrganismo'] = $idOrganismo;
        if ($status!=null)
            $data['status'] = $status;
        if ($organismo!=null)
            $data['organismo'] = $organismo;
        if ($funcionalidade!=null)
            $data['funcionalidade'] = $funcionalidade;
        if ($atribuicao!=null)
            $data['atribuicao'] = $atribuicao;
        if ($finalidade!=null)
            $data['fk_idTicketFinalidade'] = $finalidade;
        if ($pagina!=null)
            $data['pagina'] = $paginaInicial;
        //echo "QUERY: " . $this->parms("listaTicket",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function retornaQuantidadeTicket($idOrganismo,$status,$organismo,$funcionalidade,$atribuicao,$finalidade){

        $sql = "SELECT COUNT(*) as total FROM ticket WHERE 1=1";
        $sql .= $idOrganismo!=null ? " and fk_idOrganismoAfiliado=:idOrganismo" : "";
        $sql .= $status!=null ? " and statusTicket=:status" : "";
        $sql .= $organismo!=null ? " and fk_idOrganismoAfiliado=:organismo" : "";
        $sql .= $funcionalidade!=null ? " and fk_idFuncionalidade=:funcionalidade" : "";
        $sql .= $finalidade!=null ? " and fk_idTicketFinalidade=:fk_idTicketFinalidade" : "";
        $sql .= $atribuicao!=null ? " and atribuidoTicket=:atribuicao" : "";
        //echo "| SQL: ".$sql."| <br>";
        //
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if ($idOrganismo!=null)
            $sth->bindParam(':idOrganismo', $idOrganismo, PDO::PARAM_INT);
        if ($status!=null)
            $sth->bindParam(':status', $status, PDO::PARAM_INT);
        if ($organismo!=null)
            $sth->bindParam(':organismo', $organismo, PDO::PARAM_INT);
        if ($funcionalidade!=null)
            $sth->bindParam(':funcionalidade', $funcionalidade, PDO::PARAM_INT);
        if ($finalidade!=null)
            $sth->bindParam(':fk_idTicketFinalidade', $finalidade, PDO::PARAM_INT);
        if ($atribuicao!=null)
            $sth->bindParam(':atribuicao', $atribuicao, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function retornaQntTicket(){

        $sql = "SELECT COUNT(*) as total FROM ticket WHERE 1=1";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

	public function alteraStatusTicket($idTicket,$status) {
        $sql = "UPDATE ticket SET `statusTicket` = :statusTicket WHERE `idTicket` = :idTicket";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusTicket', $status, PDO::PARAM_INT);
        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaFuncionalidade($id=null){

        $sql = "SELECT * FROM funcionalidade WHERE 1=1 ";
        
        if($id!=null)
        {
            $sql .= " and idFuncionalidade=:id ";
        }    
        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaTicket($idTicket){

        $sql = "SELECT * FROM ticket where idTicket=:idTicket";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaFuncionalidade($idFuncionalidade){

        $sql = "SELECT * FROM funcionalidade where idFuncionalidade=:idFuncionalidade";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idFuncionalidade', $idFuncionalidade, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function renovarDataAtualizacaoTicket($idTicket){
        $sql = "UPDATE ticket SET dataAtualizacaoTicket=NOW() WHERE idTicket = :idTicket";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAnexo($fk_idTicket){

        $sql = "SELECT * FROM ticketAnexo
                  WHERE excluido = '1'
                  and fk_idTicket = :fk_idTicket
                  ORDER BY idTicketAnexo DESC";

        //echo $query;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function excluiAnexo($idAnexo){

        $sql = "UPDATE ticketAnexo SET excluido='0' WHERE idTicketAnexo = :idAnexo";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAnexo', $idAnexo, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function cadastroAnexo($nome_original,$nome_arquivo,$ext,$full_path,$data,$excluido,$fk_idTicket){

        $sql = "INSERT INTO ticketAnexo (nome_original,nome_arquivo,ext,full_path,data_cadastro,excluido,fk_idTicket)
                            VALUES (:nome_original,
                                    :nome_arquivo,
                                    :ext,
                                    :full_path,
                                    :data_cadastro,
                                    :excluido,
                                    :fk_idTicket)";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nome_original', $nome_original, PDO::PARAM_STR);
        $sth->bindParam(':nome_arquivo', $nome_arquivo, PDO::PARAM_STR);
        $sth->bindParam(':ext', $ext, PDO::PARAM_STR);
        $sth->bindParam(':full_path', $full_path, PDO::PARAM_STR);
        $sth->bindParam(':data_cadastro', $data, PDO::PARAM_STR);
        $sth->bindParam(':excluido', $excluido, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraStatusTicketUsuario($status, $fk_idTicket, $seqCadast=null) {

        $sql = "UPDATE ticket_usuario
                  SET `statusTicketUsuario` = :status
                  WHERE `fk_idTicket` = :fk_idTicket";
        $sql .= $seqCadast!=null ? " and `fk_seqCadast` = :seqCadast" : "";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':status', $status, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        if($seqCadast!=null)
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function hasAuthorization($seqCadast, $fk_idTicket) {

        $sql = "SELECT * FROM ticket_usuario
                  WHERE fk_seqCadast = :fk_seqCadast
                  and fk_idTicket = :fk_idTicket";
        //echo "| SQL: ".$query."| <br>";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaEnvolvidos($fk_idTicket,$fk_idDepartamento) {

        $sql = "SELECT tu.fk_seqCadast,u.nomeUsuario,u.avatarUsuario FROM ticket_usuario as tu INNER JOIN usuario as u
                  ON u.seqCadast=tu.fk_seqCadast
                  WHERE 1=1 and u.statusUsuario=0
                  and tu.fk_idTicket = :fk_idTicket
                  and tu.fk_idDepartamento = :fk_idDepartamento";
        //echo $query;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        $sth->bindParam(':fk_idDepartamento', $fk_idDepartamento, PDO::PARAM_INT);

        $data = array(
            'fk_idTicket'=>$fk_idTicket,
            'fk_idDepartamento'=>$fk_idDepartamento
        );
        //echo "QUERY: " . $this->parms("listaEnvolvidos",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function cadastroEnvolvidos($fk_idTicket,$fk_seqCadast,$fk_idDepartamento,$statusTicketUsuario){

        $sql = "INSERT INTO ticket_usuario
                                 (fk_idTicket,
                                  fk_seqCadast,
                                  fk_idDepartamento,
                                  statusTicketUsuario)
                            VALUES (:fk_idTicket,
		                            :fk_seqCadast,
		                            :fk_idDepartamento,
		                            :statusTicketUsuario)";
        //echo $sql."<br>";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadast', $fk_seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':fk_idDepartamento', $fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':statusTicketUsuario', $statusTicketUsuario, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaTicketDropdown($ativo=null,$seqCadast=null) {

        $sql = "SELECT t.idTicket,t.tituloTicket,t.statusTicket,t.descricaoTicket,t.dataAtualizacaoTicket,t.dataAtualizacaoTicket FROM ticket as t
                  left join ticket_usuario as tu on tu.fk_idTicket=t.idTicket where 1=1";
        if($ativo!=null) {
            $sql .= " and tu.statusTicketUsuario=0";
        }
        if($seqCadast!=null) {
            $sql .= " and tu.fk_seqCadast=:seqCadast";
        }
        //$sql .= " order by t.dataAtualizacaoTicket desc,t.dataCriacaoTicket desc"; // LIMIT 4
        $sql .= " order by IF(t.dataAtualizacaoTicket <> '0000-00-00 00:00:00',t.dataAtualizacaoTicket,t.dataCriacaoTicket) DESC";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seqCadast!=null)
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $data = array(
            'seqCadast'=>$seqCadast
        );
        //echo "QUERY: " . $this->parms("listaTicketDropdown",$sql,$data);

        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function lista($seq_cadast=null,$fk_idTicket=null){

        $sql = "SELECT COUNT(*) as total FROM ticket_usuario where 1=1";
        if($seq_cadast!=null) {
            $sql .= " and fk_seqCadast=:seqCadast";
        }
        if($fk_idTicket!=null) {
            $sql .= " and fk_idTicket=:fk_idTicket";
        }
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($seq_cadast!=null)
        {    
            $sth->bindParam(':seqCadast', $seq_cadast, PDO::PARAM_INT);
        }
        if($fk_idTicket!=null)
        {    
            $sth->bindParam(':fk_idTicket', $fk_idTicket, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        //echo "<pre>";print_r($resultado);
        if ($resultado){
            return $resultado[0]['total'];
        } else {
            return false;
        }
    }

    public function alteraAtribuicaoTicket($idTicket,$seqCadastAtribuido,$seqCadastAtribuidor) {
        $sql = "UPDATE ticket SET
                    `atribuidoTicket` = :seqCadastAtribuido,
                    `atribuidorTicket` = :seqCadastAtribuidor
                    WHERE `idTicket` = :idTicket";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':seqCadastAtribuido', $seqCadastAtribuido, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastAtribuidor', $seqCadastAtribuidor, PDO::PARAM_INT);
        $sth->bindParam(':idTicket', $idTicket, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }
}
?>
