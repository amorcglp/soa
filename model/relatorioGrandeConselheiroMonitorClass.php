<?php

require_once ("conexaoClass.php");

class relatorioGrandeConselheiroMonitor{

    private $fk_idRelatorioGrandeConselheiro;
    private $seqCadast;
    
    //METODO SET/GET -----------------------------------------------------------

    function getFk_idRelatorioGrandeConselheiro() {
        return $this->fk_idRelatorioGrandeConselheiro;
    }
    
	function getSeqCadast() {
        return $this->seqCadast;
	}
  
	function setFk_idRelatorioGrandeConselheiro($fk_idRelatorioGrandeConselheiro) {
        $this->fk_idRelatorioGrandeConselheiro = $fk_idRelatorioGrandeConselheiro;
    }
    
	function setSeqCadast($seqCadast) {
        $this->seqCadast = $seqCadast;
    }

    //--------------------------------------------------------------------------

    public function cadastroRelatorioGrandeConselheiroMonitor(){

        $sql="INSERT INTO relatorioGrandeConselheiro_monitor  
                            (
                             fk_idRelatorioGrandeConselheiro,
                             seqCadast
                             )
                            VALUES (:fkIdRelatorioGrandeConselheiro,
                                    :seqCadast)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFk_idRelatorioGrandeConselheiro             = $this->getFk_idRelatorioGrandeConselheiro();
        $getSeqCadast                                     = $this->getSeqCadast();
        
        $sth->bindParam(':fkIdRelatorioGrandeConselheiro', $getFk_idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        $sth->bindParam(':seqCadast', $getSeqCadast, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
   
    
    public function listaMonitores($idRelatorioGrandeConselheiro=null,$seqCadast=null){

    	$sql = "SELECT * FROM relatorioGrandeConselheiro_monitor
    	where 1=1 
    	 ";   
    	if($idRelatorioGrandeConselheiro!=null)
    	{
    		$sql .= " and fk_idRelatorioGrandeConselheiro=:fkIdRelatorioGrandeConselheiro";
    	}
    	if($seqCadast!=null)
    	{
    		$sql .= " and seqCadast=:seqCadast";
    	}	 
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idRelatorioGrandeConselheiro!=null)
    	{
            $sth->bindParam(':fkIdRelatorioGrandeConselheiro', $idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        }
        if($seqCadast!=null)
    	{
            $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function removeMonitores($idRelatorioGrandeConselheiro=null){
        
    	$sql = "DELETE FROM relatorioGrandeConselheiro_monitor WHERE 1=1";
    	if($idRelatorioGrandeConselheiro!=null)
    	{
    		$sql .= " and fk_idRelatorioGrandeConselheiro = :id";
    	}
        
    	//echo $sql;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($idRelatorioGrandeConselheiro!=null)
    	{
            $sth->bindParam(':id', $idRelatorioGrandeConselheiro, PDO::PARAM_INT);
        }    
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }   
}

?>
