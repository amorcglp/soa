<?php

require_once ("conexaoClass.php");

class PlanoAcaoRegiao {

    private $idPlanoAcaoRegiao;
    private $tituloPlano;
    private $statusPlano;
    private $criadoPor;
    private $regiao;
    private $dataCriacao;
    private $descricaoPlano;
    private $prioridade;
    private $palavraChave;
    private $ultimoAtualizar;
    
	function getIdPlanoAcaoRegiao() {
        return $this->idPlanoAcaoRegiao;
    }
    
	function getTituloPlano() {
        return $this->tituloPlano;
    }

    function getStatusPlano() {
        return $this->statusPlano;
    }
    
	function getCriadoPor() {
        return $this->criadoPor;
    }
    
	function getRegiao() {
        return $this->regiao;
    }
    
	function getDataCriacao() {
        return $this->dataCriacao;
    }
    
	function getDescricaoPlano() {
        return $this->descricaoPlano;
    }
    
	function getPrioridade() {
        return $this->prioridade;
    }
    
    function getPalavraChave() {
        return $this->palavraChave;
    }
    
    function getUltimoAtualizar() {
        return $this->ultimoAtualizar;
    }
    
    function setIdPlanoAcaoRegiao($idPlanoAcaoRegiao) {
        $this->idPlanoAcaoRegiao = $idPlanoAcaoRegiao;
    }
    
	function setTituloPlano($tituloPlano) {
        $this->tituloPlano = $tituloPlano;
    }

    function setStatusPlano($statusPlano) {
        $this->statusPlano = $statusPlano;
    }
    
	function setCriadoPor($criadoPor) {
        $this->criadoPor = $criadoPor;
    }
    
	function setRegiao($regiao) {
        $this->regiao = $regiao;
    }

	function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }
    
	function setDescricaoPlano($descricaoPlano) {
        $this->descricaoPlano = $descricaoPlano;
    }
    
	function setPrioridade($prioridade) {
        $this->prioridade = $prioridade;
    }
    
	function setPalavraChave($palavraChave) {
        $this->palavraChave = $palavraChave;
    }
    
    function setUltimoAtualizar($ultimoAtualizar) {
        $this->ultimoAtualizar = $ultimoAtualizar;
    }
    
//--------------------------------------------------------------------------

    public function cadastroPlanoAcaoRegiao(){

    	$sql = "INSERT INTO planoAcaoRegiao  
                                 (tituloPlano,
                                  statusPlano,
                                  criadoPor,
                                  regiao,
                                  dataCriacao,
                                  descricaoPlano,
                                  prioridade,
                                  palavrasChave
                                  )                    
                            VALUES (
                                    :titulo,
                                    :status,
                                    :criadoPor,
                                    :regiao,
                                    now(),
                                    :descricao,
                                    :prioridade,
                                    :palavraChave
                                    )";
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->tituloPlano, PDO::PARAM_STR);
        $sth->bindParam(':status', $this->statusPlano, PDO::PARAM_INT);
        $sth->bindParam(':criadoPor',  $this->criadoPor, PDO::PARAM_INT);
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_INT);
        $sth->bindParam(':descricao', $this->descricaoPlano, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $this->prioridade , PDO::PARAM_INT);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
        	$ultimo_id=0;
        	$resultado = $this->selecionaUltimoId();
        	if($resultado)
        	{
        		foreach ($resultado as $vetor)
        		{
        			$ultimo_id = $vetor['lastid']; 
        		}
        	}
        	return $ultimo_id;
   
        }else{
            return false;
        }
    }
    
    public function alteraStatusPlano() {
		$sql = "UPDATE planoAcaoRegiao SET `statusPlano` = :status
                               WHERE `idPlanoAcaoRegiao` = :idPlanoAcaoRegiao";
		
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusPlano, PDO::PARAM_INT);
        $sth->bindParam(':idPlanoAcaoRegiao', $this->idPlanoAcaoRegiao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusPlano();
            return $arr;
        } else {
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SELECT MAX(idPlanoAcaoRegiao) as lastid FROM planoAcaoRegiao";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }	

    public function alteraPlanoAcaoRegiao($idPlanoAcaoRegiao){
        
    	$sql = "UPDATE planoAcaoRegiao SET
                                tituloPlano = :titulo,	  
                                descricaoPlano = :descricao,
                                palavrasChave = :palavraChave,
                                prioridade = :prioridade,
                                ultimoAtualizar = :ultimoAtualizar
                                  WHERE idPlanoAcaoRegiao = :id";
        //echo $sql;
        //exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->tituloPlano, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoPlano, PDO::PARAM_STR);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $this->prioridade , PDO::PARAM_INT);
        $sth->bindParam(':ultimoAtualizar',  $this->ultimoAtualizar, PDO::PARAM_INT);
        $sth->bindParam(':id',  $idPlanoAcaoRegiao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        };
    }
    
	# Seleciona 
    public function listaPlanoAcaoRegiao($regiao=null,$criadoPor=null,$pesquisar=null,$linha_inicial=null, $qtdRegistros=null) 
    {
        $sql = "SELECT * FROM planoAcaoRegiao as par 
        inner join regiaoRosacruz as rr on par.regiao = rr.idRegiaoRosacruz
        inner join usuario as u on u.seqCadast = par.criadoPor
        where 1=1 ";
        if($pesquisar!=null)
        {
            $sql .= " and ((regiaoRosacruz like :p)"
                    . " or(tituloPlano like :p)"
                    . " or(nomeUsuario like :p)"
                    . " or(palavrasChave like :p))";
        }    
        if($regiao!=null)
        {
                $sql .= " and regiao=:regiao";
        }
        if($criadoPor!=null)
        {
                $sql .= " and criadoPor=:criadoPor";
        }
        if($qtdRegistros>0&&$qtdRegistros!=null)
        {    
            $sql .= "LIMIT ".$linha_inicial.", " . $qtdRegistros;
        }
   
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($regiao!=null)
        {
            $sth->bindParam(':regiao', $regiao, PDO::PARAM_INT);
        }
        if($criadoPor!=null)
        {
            $sth->bindParam(':criadoPor', $criadoPor, PDO::PARAM_INT);
        }
        if($pesquisar!=null)
        {
            $pesq = "%".$pesquisar."%";
            $sth->bindParam(':p', $pesq, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
	
    public function verificaSeJaExiste(){

    	$sql = "SELECT * FROM planoAcaoRegiao where 1=1 
                    and tituloPlano = :titulo
                    and regiao =  :regiao
                    and criadoPor = :criadoPor
                    and palavrasChave = :palavraChave
                ";
    	//echo $sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':titulo', $this->tituloPlano, PDO::PARAM_STR);
        $sth->bindParam(':regiao', $this->regiao, PDO::PARAM_INT);
        $sth->bindParam(':criadoPor', $this->criadoPor, PDO::PARAM_INT);
        $sth->bindParam(':palavraChave', $this->palavraChave, PDO::PARAM_STR);
        
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaTituloPlanoAcaoRegiao(){
        
    	$sql = "SELECT *   FROM    planoAcaoRegiao
                                    WHERE  tituloPlano LIKE :titulo
                                    ORDER BY tituloPlano ASC";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getTituloPlano = '%'.str_replace("  "," ",trim($this->getTituloPlano())).'%';
        $sth->bindParam(':titulo', $getTituloPlano, PDO::PARAM_STR);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscarIdPlanoAcaoRegiao($idPlanoAcaoRegiao){
        
    	$sql="SELECT * FROM  planoAcaoRegiao as a
    				inner join usuario as u on criadoPor = seqCadast
    				inner join regiaoRosacruz as rr on a.regiao = rr.idRegiaoRosacruz
                                WHERE   idPlanoAcaoRegiao = :id";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':id', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function excluirPlanoAcaoRegiao($idPlanoAcaoRegiao){
        
    	$sql="DELETE FROM  planoAcaoRegiao 
                                WHERE   idPlanoAcaoRegiao = :id";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idPlanoAcaoRegiao, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>