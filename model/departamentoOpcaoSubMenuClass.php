<?php
require_once ("conexaoClass.php");

class DepartamentoOpcaoSubMenu {

    public $fk_idDepartamento;
    public $fk_idOpcaoSubMenu;
    
    /* Funções GET e SET */

    function getFkIdDepartamento() {
        return $this->fk_idDepartamento;
    }

    function getFkIdOpcaoSubMenu() {
        return $this->fk_idOpcaoSubMenu;
    }

    function setFkIdDepartamento($fk_idDepartamento) {
        $this->fk_idDepartamento = $fk_idDepartamento;
    }

    function setFkIdOpcaoSubMenu($fk_idOpcaoSubMenu) {
        $this->fk_idOpcaoSubMenu = $fk_idOpcaoSubMenu;
    }

    public function cadastra() {

        $sql = "INSERT INTO departamento_opcaoSubMenu (`fk_idDepartamento`, `fk_idOpcaoSubMenu`)
                                    VALUES (:idDepartamento,
                                            :idOpcaoSubMenu)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idOpcaoSubMenu',$this->fk_idOpcaoSubMenu , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM departamento_opcaoSubMenu 
        						WHERE fk_idDepartamento = :idDepartamento
        						and fk_idOpcaoSubMenu = :idOpcaoSubMenu";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idOpcaoSubMenu',$this->fk_idOpcaoSubMenu , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaPorDepartamento() {
        
        $sql = "SELECT * FROM departamento_opcaoSubMenu
                                 WHERE `fk_idDepartamento` = :idDepartamento
                              ";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
}
?>