<?php

require_once ("conexaoClass.php");

class EmailDePara {

    private $fk_idEmail;
    private $para;
    private $pastaInterna;
    private $nova;
    private $enviado;
    private $vinculoEmailEnviado;
    
    function getFkIdEmail() {
        return $this->fk_idEmail;
    }

    function getPara() {
        return $this->para;
    }
    
	function getPastaInterna() {
        return $this->pastaInterna;
    }
    
	function getNova() {
        return $this->nova;
    }
    
	function getEnviado() {
        return $this->enviado;
    }
    
	function getVinculoEmailEnviado() {
        return $this->vinculoEmailEnviado;
    }
    
	function setFkIdEmail($fk_idEmail) {
        $this->fk_idEmail = $fk_idEmail;
    }

    function setPara($para) {
        $this->para = $para;
    }
    
	function setPastaInterna($pastaInterna) {
        $this->pastaInterna = $pastaInterna;
    }
    
	function setNova($nova) {
        $this->nova = $nova;
    }
    
	function setEnviado($enviado) {
        $this->enviado = $enviado;
    }
    
	function setVinculoEmailEnviado($vinculoEmailEnviado) {
        $this->vinculoEmailEnviado = $vinculoEmailEnviado;
    }
    
	# 
	public function cadastraEmailDePara() 
	{
            $sql="INSERT INTO emailDePara  
                             (
                              fk_idEmail,	
                              para,
                              pastaInterna,
                              nova,
                              enviado,
                              vinculoEmailenviado
                              )   
                        VALUES (
                                    :idEmail,
                                    :para,
                                    :pastaInterna,
                                    :nova,
                                    :enviado,
                                    :vinculo
                                )";

            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql);

            $sth->bindParam(':idEmail', $this->fk_idEmail, PDO::PARAM_INT);
            $sth->bindParam(':para', $this->para, PDO::PARAM_INT);
            $sth->bindParam(':pastaInterna', $this->pastaInterna, PDO::PARAM_INT);
            $sth->bindParam(':nova', $this->nova, PDO::PARAM_INT);
            $sth->bindParam(':enviado', $this->enviado, PDO::PARAM_INT);
            $sth->bindParam(':vinculo', $this->vinculoEmailEnviado, PDO::PARAM_INT);

            if ($c->run($sth,true)){
                return true;
            }else{
                return false;
            }
	}
	
	public function selecionarDestinatarios($idEmail)
	{
            $sql = "SELECT * FROM emailDePara 
                            WHERE fk_idEmail=:id";
            //echo $sql;exit();
            $c = new conexaoSOA(); 
            $con = $c->openSOA();
            $sth = $con->prepare($sql);

            $sth->bindParam(':id',  $idEmail, PDO::PARAM_INT);

            $resultado = $c->run($sth);
            if ($resultado){
                return $resultado;
            }else{
                return false;
            }
	}
	
	public function marcarComoLida($idEmail,$seqCadast)
	{
		$sql = "UPDATE emailDePara SET 
					nova='0'
				WHERE fk_idEmail=:id and para=:seqCadast";
		//echo $sql;
		$c = new conexaoSOA(); 
                $con = $c->openSOA();
                $sth = $con->prepare($sql);

                $sth->bindParam(':id',  $idEmail, PDO::PARAM_INT);
                $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);

		if ($c->run($sth, true)) {
                    return true;
		}else{
                    return false;
                }
	}
	
	public function marcarComoImportante($idEmail,$seqCadast)
	{
		$sql = "UPDATE emailDePara SET 
					pastaInterna='3'
				WHERE fk_idEmail=:id "
                        . " and para=:seqCadast;";
		//echo $sql;
		$c = new conexaoSOA(); 
                $con = $c->openSOA();
                $sth = $con->prepare($sql);

                $sth->bindParam(':id',  $idEmail, PDO::PARAM_INT);
                $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);

		if ($c->run($sth, true)) {
                    return true;
		}else{
                    return false;
                }
	}
	
	public function moverParaLixeira($idEmail,$seqCadast=null)
	{
		$sql = "UPDATE emailDePara SET 
					pastaInterna=:id
				WHERE fk_idEmail=:seqCadast";
		if($seqCadast!=null)
		{
			$sql .= " and para=:seqCadast";
		}
		//echo $sql;
		$c = new conexaoSOA(); 
                $con = $c->openSOA();
                $sth = $con->prepare($sql);

                $sth->bindParam(':id',  $idEmail, PDO::PARAM_INT);
                $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);

		if ($c->run($sth, true)) {
                    return true;
		}else{
                    return false;
                }
	}
	
	public function excluirEmailDefinitivamente($idEmail,$seqCadast)
	{
		$sql = ("UPDATE emailDePara SET 
					pastaInterna='5'
				WHERE fk_idEmail=:id
                                and (para=:seqCadast or para='0');");
		//echo $sql;
		$c = new conexaoSOA(); 
                $con = $c->openSOA();
                $sth = $con->prepare($sql);

                $sth->bindParam(':id',  $idEmail, PDO::PARAM_INT);
                $sth->bindParam(':seqCadast',  $seqCadast, PDO::PARAM_INT);

		if ($c->run($sth, true)) {
                    return true;
		}else{
                    return false;
                }
	}
}

?>