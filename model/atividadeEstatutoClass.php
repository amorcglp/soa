<?php

require_once ("conexaoClass.php");

class AtividadeEstatuto{

    private $idAtividadeEstatuto;
    private $complementoNomeAtividadeEstatuto;
    private $descricaoAtividadeEstatuto;
    private $localAtividadeEstatuto;
    private $nroParticipanteAtividadeEstatuto;
    private $dataAtividadeEstatuto;
    private $horaAtividadeEstatuto;
    private $tituloDiscursoAtividadeEstatuto;
    private $nomeOradorDiscursoAtividadeEstatuto;
    private $dataCadastradoAtividadeEstatuto;
    private $dataAtualizadoAtividadeEstatuto;
    private $statusAtividadeEstatuto;
    private $fk_idTipoAtividadeEstatuto;
    private $fk_seqCadastAtualizadoPor;
    private $fk_idOrganismoAfiliado;

    //--- GETERS -----------------------------------------------------
    function getIdAtividadeEstatuto() {
        return $this->idAtividadeEstatuto;
    }
    public function getComplementoNomeAtividadeEstatuto() {
        return $this->complementoNomeAtividadeEstatuto;
    }
    public function getDescricaoAtividadeEstatuto() {
        return $this->descricaoAtividadeEstatuto;
    }
    public function getLocalAtividadeEstatuto() {
        return $this->localAtividadeEstatuto;
    }
    public function getNroParticipanteAtividadeEstatuto() {
        return $this->nroParticipanteAtividadeEstatuto;
    }
    public function getDataAtividadeEstatuto() {
        return $this->dataAtividadeEstatuto;
    }
    public function getHoraAtividadeEstatuto() {
        return $this->horaAtividadeEstatuto;
    }
    public function getTituloDiscursoAtividadeEstatuto() {
        return $this->tituloDiscursoAtividadeEstatuto;
    }
    public function getNomeOradorDiscursoAtividadeEstatuto(){
        return $this->nomeOradorDiscursoAtividadeEstatuto;
    }
    public function getDataCadastradoAtividadeEstatuto() {
        return $this->dataCadastradoAtividadeEstatuto;
    }
    public function getDataAtualizadoAtividadeEstatuto() {
        return $this->dataAtualizadoAtividadeEstatuto;
    }
    public function getStatusAtividadeEstatuto() {
        return $this->statusAtividadeEstatuto;
    }
    public function getFkIdTipoAtividadeEstatuto() {
        return $this->fk_idTipoAtividadeEstatuto;
    }
    public function getFkSeqCadastAtualizadoPor() {
        return $this->fk_seqCadastAtualizadoPor;
    }
    public function getFkIdOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    //--- SETERS -----------------------------------------------------
	function setIdAtividadeEstatuto($idAtividadeEstatuto) {
        $this->idAtividadeEstatuto = $idAtividadeEstatuto;
    }
    public function setComplementoNomeAtividadeEstatuto($complementoNomeAtividadeEstatuto) {
        $this->complementoNomeAtividadeEstatuto = $complementoNomeAtividadeEstatuto;
    }
    public function setDescricaoAtividadeEstatuto($descricaoAtividadeEstatuto) {
        $this->descricaoAtividadeEstatuto = $descricaoAtividadeEstatuto;
    }
    public function setLocalAtividadeEstatuto($localAtividadeEstatuto) {
        $this->localAtividadeEstatuto = $localAtividadeEstatuto;
    }
    public function setNroParticipanteAtividadeEstatuto($nroParticipanteAtividadeEstatuto) {
        $this->nroParticipanteAtividadeEstatuto = $nroParticipanteAtividadeEstatuto;
    }
    public function setDataAtividadeEstatuto($dataAtividadeEstatuto) {
        $this->dataAtividadeEstatuto = $dataAtividadeEstatuto;
    }
    public function setHoraAtividadeEstatuto($horaAtividadeEstatuto) {
        $this->horaAtividadeEstatuto = $horaAtividadeEstatuto;
    }
    public function setTituloDiscursoAtividadeEstatuto($tituloDiscursoAtividadeEstatuto){
        $this->tituloDiscursoAtividadeEstatuto = $tituloDiscursoAtividadeEstatuto;
    }
    public function setNomeOradorDiscursoAtividadeEstatuto($nomeOradorDiscursoAtividadeEstatuto){
        $this->nomeOradorDiscursoAtividadeEstatuto = $nomeOradorDiscursoAtividadeEstatuto;
    }
    public function setDataCadastradoAtividadeEstatuto($dataCadastradoAtividadeEstatuto) {
        $this->dataCadastradoAtividadeEstatuto = $dataCadastradoAtividadeEstatuto;
    }
    public function setDataAtualizadoAtividadeEstatuto($dataAtualizadoAtividadeEstatuto) {
        $this->dataAtualizadoAtividadeEstatuto = $dataAtualizadoAtividadeEstatuto;
    }
    public function setStatusAtividadeEstatuto($statusAtividadeEstatuto) {
        $this->statusAtividadeEstatuto = $statusAtividadeEstatuto;
    }
    public function setFkIdTipoAtividadeEstatuto($fk_idTipoAtividadeEstatuto) {
        $this->fk_idTipoAtividadeEstatuto = $fk_idTipoAtividadeEstatuto;
    }
    public function setFkSeqCadastAtualizadoPor($fk_seqCadastAtualizadoPor) {
        $this->fk_seqCadastAtualizadoPor = $fk_seqCadastAtualizadoPor;
    }
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    //--------------------------------------------------------------------------

	public function cadastroAtividadeEstatuto(){

        $sql = "INSERT INTO atividadeEstatuto
                                 (complementoNomeAtividadeEstatuto,
                                  descricaoAtividadeEstatuto,
                                  localAtividadeEstatuto,
                                  dataAtividadeEstatuto,
                                  horaAtividadeEstatuto,
                                  tituloDiscursoAtividadeEstatuto,
                                  nomeOradorDiscursoAtividadeEstatuto,
                                  dataCadastradoAtividadeEstatuto,
                                  fk_idTipoAtividadeEstatuto,
                                  fk_seqCadastAtualizadoPor,
                                  fk_idOrganismoAfiliado)
                            VALUES (:complementoNomeAtividadeEstatuto,
                                    :descricaoAtividadeEstatuto,
                                    :localAtividadeEstatuto,
                                    :dataAtividadeEstatuto,
                                    :horaAtividadeEstatuto,
                                    :tituloDiscursoAtividadeEstatuto,
                                    :nomeOradorDiscursoAtividadeEstatuto,
                                    NOW(),
                                    :fk_idTipoAtividadeEstatuto,
                                    :fk_seqCadastAtualizadoPor,
                                    :fk_idOrganismoAfiliado)";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':complementoNomeAtividadeEstatuto', $this->complementoNomeAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':descricaoAtividadeEstatuto', $this->descricaoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':localAtividadeEstatuto', $this->localAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':dataAtividadeEstatuto', $this->dataAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':horaAtividadeEstatuto', $this->horaAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':tituloDiscursoAtividadeEstatuto', $this->tituloDiscursoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':nomeOradorDiscursoAtividadeEstatuto', $this->nomeOradorDiscursoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':fk_idTipoAtividadeEstatuto', $this->fk_idTipoAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':fk_idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraAtividadeEstatuto($idAtividadeEstatuto){

        $sql = "UPDATE atividadeEstatuto SET
                                  complementoNomeAtividadeEstatuto = :complementoNomeAtividadeEstatuto,
                                  descricaoAtividadeEstatuto = :descricaoAtividadeEstatuto,
                                  localAtividadeEstatuto = :localAtividadeEstatuto,
                                  dataAtividadeEstatuto =  :dataAtividadeEstatuto,
                                  horaAtividadeEstatuto =  :horaAtividadeEstatuto,
                                  tituloDiscursoAtividadeEstatuto =  :tituloDiscursoAtividadeEstatuto,
                                  nomeOradorDiscursoAtividadeEstatuto =  :nomeOradorDiscursoAtividadeEstatuto,
				  				  dataAtualizadoAtividadeEstatuto = NOW(),
                                  fk_seqCadastAtualizadoPor = :fk_seqCadastAtualizadoPor
                                  WHERE idAtividadeEstatuto = :idAtividadeEstatuto";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':complementoNomeAtividadeEstatuto', $this->complementoNomeAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':descricaoAtividadeEstatuto', $this->descricaoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':localAtividadeEstatuto', $this->localAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':dataAtividadeEstatuto', $this->dataAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':horaAtividadeEstatuto', $this->horaAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':tituloDiscursoAtividadeEstatuto', $this->tituloDiscursoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':nomeOradorDiscursoAtividadeEstatuto', $this->nomeOradorDiscursoAtividadeEstatuto, PDO::PARAM_STR);
        $sth->bindParam(':fk_seqCadastAtualizadoPor', $this->fk_seqCadastAtualizadoPor, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeEstatuto', $idAtividadeEstatuto, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function listaAtividadeEstatuto($idOrganismo=null,$mesAtual=null,$anoAtual=null,$status=null,$tipos=null){

        $sql = "SELECT ae.*, aet.nomeTipoAtividadeEstatuto, u.nomeUsuario, aet.* FROM atividadeEstatuto as ae"
                . " inner join atividadeEstatuto_tipo as aet on fk_idTipoAtividadeEstatuto = idTipoAtividadeEstatuto "
                . " inner join usuario as u on ae.fk_seqCadastAtualizadoPor = u.seqCadast"
                . " where 1=1 ";

        if($mesAtual!=null){
            $sql .= " and MONTH(dataAtividadeEstatuto)=:mesAtual";
        }
        if($anoAtual!=null){
            $sql .= " and YEAR(dataAtividadeEstatuto)=:anoAtual";
        }
        if($idOrganismo!=null){
            $sql .= " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado";
        }
        if($status!=null){
            $sql .= " and statusAtividadeEstatuto=:status";
        }

        if($tipos!=null){

            $ids = explode(",", $tipos);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
            $sql .= " and fk_idTipoAtividadeEstatuto IN (" . $inQuery . ") ";
        }

        $sql .= " order by dataAtividadeEstatuto ASC";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual!=null){
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null){
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($idOrganismo!=null){
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismo, PDO::PARAM_INT);
        }
        if($status!=null){
            $sth->bindParam(':status', $status, PDO::PARAM_INT);
        }
        if($tipos!=null)
        {
            foreach ($ids as $u => $id)
            {
                 $sth->bindValue(":".$u, $id);
            }
        }

        $data = array(
            'mesAtual'=>$mesAtual,
            'anoAtual'=>$anoAtual,
            'fk_idOrganismoAfiliado'=>$idOrganismo,
            'status'=>$status,
        );
        //echo $this->parms("listaAtividadeEstatuto",$sql,$data);

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function listaAtividadeEstatutoParaRelatorio($idOrganismo=null,$mesAtual=null,$anoAtual=null,$tipoAtividade=null){

        $sql = "SELECT * FROM atividadeEstatuto where 1=1 and statusAtividadeEstatuto=2";
        $sql .= $mesAtual != null ? " and MONTH(dataAtividadeEstatuto)=:mesAtual" : "";
        $sql .= $anoAtual != null ? " and YEAR(dataAtividadeEstatuto)=:anoAtual" : "";
        $sql .= $idOrganismo != null ? " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado" : "";
        $sql .= $tipoAtividade != null ? " and fk_idTipoAtividadeEstatuto=:fk_idTipoAtividadeEstatuto" : "";
        $sql .= " order by dataAtividadeEstatuto ASC";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual!=null){
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null){
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($idOrganismo!=null){
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismo, PDO::PARAM_INT);
        }
        if($tipoAtividade!=null){
            $sth->bindParam(':fk_idTipoAtividadeEstatuto', $tipoAtividade, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);

        $data = array(
            'mesAtual' => $mesAtual,
            'anoAtual' => $anoAtual,
            'fk_idOrganismoAfiliado' => $idOrganismo,
            'fk_idTipoAtividadeEstatuto' => $tipoAtividade
        );
        //echo $this->parms("listaAtividadeEstatutoTipoPelaClassificacaoOa",$sql,$data);

        if ($resultado){
            return count($resultado); //buscar rowCount()
        } else {
            return 0;
        }
    }
    
    public function listaAtividadeEstatutoParaRelatorioGetClassificacao($idOrganismo=null,$mesAtual=null,$anoAtual=null){

        $sql = "SELECT * FROM atividadeEstatuto ae "
                . " inner join atividadeEstatuto_tipo as tae on ae.fk_idTipoAtividadeEstatuto=tae.idTipoAtividadeEstatuto"
                . " where 1=1 and statusAtividadeEstatuto=2 ";
        $sql .= $mesAtual != null ? " and MONTH(ae.dataAtividadeEstatuto)=:mesAtual" : "";
        $sql .= $anoAtual != null ? " and YEAR(ae.dataAtividadeEstatuto)=:anoAtual" : "";
        $sql .= $idOrganismo != null ? " and ae.fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado" : "";
        $sql .= " order by ae.dataAtividadeEstatuto ASC";

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual!=null){
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null){
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($idOrganismo!=null){
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismo, PDO::PARAM_INT);
        }
   
        $resultado = $c->run($sth);

        $classificacao = 2;
        foreach($resultado as $v)
        {
            $classificacao = $v['classiOaTipoAtividadeEstatuto'];
        }    

        return $classificacao;
    }

    public function retornaNumeroDeParticipantes($idOrganismo=null,$mesAtual=null,$anoAtual=null,$tipoAtividade=null){

        $sql = "SELECT (SUM(nroParticipanteAtividadeEstatuto) + SUM(nroFrequentadorAtividadeEstatuto)) as nroParticipantes FROM atividadeEstatuto where 1=1 and statusAtividadeEstatuto=2";
        $sql .= $mesAtual != null ? " and MONTH(dataAtividadeEstatuto)=:mesAtual" : "";
        $sql .= $anoAtual != null ? " and YEAR(dataAtividadeEstatuto)=:anoAtual" : "";
        $sql .= $idOrganismo != null ? " and fk_idOrganismoAfiliado=:fk_idOrganismoAfiliado" : "";
        $sql .= $tipoAtividade != null ? " and fk_idTipoAtividadeEstatuto=:fk_idTipoAtividadeEstatuto" : "";
        //$sql .= " order by dataAtividadeEstatuto ASC";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mesAtual!=null){
            $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        }
        if($anoAtual!=null){
            $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);
        }
        if($idOrganismo!=null){
            $sth->bindParam(':fk_idOrganismoAfiliado', $idOrganismo, PDO::PARAM_INT);
        }
        if($tipoAtividade!=null){
            $sth->bindParam(':fk_idTipoAtividadeEstatuto', $tipoAtividade, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function listaAtividadeEstatutoOficiais($idEstatudo=null){

        $sql = "SELECT seqCadastOficial, codAfiliacaoOficial, nomeOficial, statusOficial FROM atividadeEstatutoOficial where 1=1";
        if($idEstatudo!=null)
        {
            $sql .= " and fk_idAtividadeEstatuto=:fk_idAtividadeEstatuto";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($idEstatudo!=null){
            $sth->bindParam(':fk_idAtividadeEstatuto', $idEstatudo, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacaoOa,$estatuto=null,$mes=null){

        $sql = "SELECT * FROM atividadeEstatuto_tipo where statusTipoAtividadeEstatuto=0 and classiOaTipoAtividadeEstatuto=:classiOaTipoAtividadeEstatuto";
        $sql .= $estatuto =! null ? " and estatutoTipoAtividadeEstatuto=1" : "";

        if($mes != null){
            $sql .= $mes == 1 ? " and janeiroTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 2 ? " and fevereiroTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 3 ? " and marcoTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 4 ? " and abrilTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 5 ? " and maioTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 6 ? " and junhoTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 7 ? " and julhoTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 8 ? " and agostoTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 9 ? " and setembroTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 10 ? " and outubroTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 11 ? " and novembroTipoAtividadeEstatuto=1" : "";
            $sql .= $mes == 12 ? " and dezembroTipoAtividadeEstatuto=1" : "";
        }

        $sql .= " ORDER BY ordemTipoAtividadeEstatuto ASC";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':classiOaTipoAtividadeEstatuto', $classificacaoOa, PDO::PARAM_INT);

        $resultado = $c->run($sth);;

        $data = array(
            'classiOaTipoAtividadeEstatuto'=>$classificacaoOa
        );
        //echo $this->parms("listaAtividadeEstatutoTipoPelaClassificacaoOa",$sql,$data);

        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdAtividadeEstatuto($idAtividadeEstatuto){

        $sql = "SELECT * FROM  atividadeEstatuto WHERE   idAtividadeEstatuto=:idAtividadeEstatuto";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeEstatuto', $idAtividadeEstatuto, PDO::PARAM_INT);

        $resultado = $c->run($sth);;
        if ($resultado){
            return $resultado;
        } else {
            return false;
        }
    }

    public function alteraStatusAtividadeEstatuto() {

        $sql = "UPDATE atividadeEstatuto SET statusAtividadeEstatuto = :statusAtividadeEstatuto "
                . "WHERE idAtividadeEstatuto = :idAtividadeEstatuto";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusAtividadeEstatuto', $this->statusAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeEstatuto', $this->idAtividadeEstatuto, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusAtividadeEstatuto();
            return $arr;
        } else {
            return false;
        }
    }

    public function alteraNroParticipanteAtividadeEstatuto($idAtividade,$nroParticipante) {

        $sql = "UPDATE atividadeEstatuto SET nroParticipanteAtividadeEstatuto = :nroParticipanteAtividadeEstatuto WHERE idAtividadeEstatuto = :idAtividadeEstatuto";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nroParticipanteAtividadeEstatuto', $nroParticipante, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeEstatuto', $idAtividade, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraNroFrequentadoresAtividadeEstatuto($idAtividade,$nroFrequentadores) {

        $sql = "UPDATE atividadeEstatuto SET nroFrequentadorAtividadeEstatuto = :nroFrequentadorAtividadeEstatuto WHERE idAtividadeEstatuto = :idAtividadeEstatuto";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nroFrequentadorAtividadeEstatuto', $nroFrequentadores, PDO::PARAM_INT);
        $sth->bindParam(':idAtividadeEstatuto', $idAtividade, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraStatusOficialResponsavel($idAtividadeEstatuto,$idOficialResponsavel,$statusOficialResponsavel) {

        $sql = "UPDATE atividadeEstatutoOficial SET statusOficial = :statusOficial  WHERE fk_idAtividadeEstatuto = :fk_idAtividadeEstatuto and seqCadastOficial =  :seqCadastOficial";

        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':statusOficial', $statusOficialResponsavel, PDO::PARAM_INT);
        $sth->bindParam(':fk_idAtividadeEstatuto', $idAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastOficial', $idOficialResponsavel, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function incluiOficialResponsavelAtividade($idAtividadeEstatuto, $codigoAfiliacao, $nomeMembro, $seqCadast){

        $sql = "INSERT INTO atividadeEstatutoOficial
                                 (fk_idAtividadeEstatuto,
                                  seqCadastOficial,
                                  codAfiliacaoOficial,
                                  nomeOficial)
                            VALUES (:fk_idAtividadeEstatuto,
                                    :seqCadastOficial,
                                    :codAfiliacaoOficial,
                                    :nomeOficial)";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeEstatuto', $idAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastOficial', $seqCadast, PDO::PARAM_INT);
        $sth->bindParam(':codAfiliacaoOficial', $codigoAfiliacao, PDO::PARAM_INT);
        $sth->bindParam(':nomeOficial', $nomeMembro, PDO::PARAM_STR);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function verificaOficialResponsavelAtividade($idAtividadeEstatuto, $seqCadast){

        $sql = "SELECT * FROM  atividadeEstatutoOficial WHERE fk_idAtividadeEstatuto=:fk_idAtividadeEstatuto and seqCadastOficial=:seqCadastOficial";
        //echo $sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':fk_idAtividadeEstatuto', $idAtividadeEstatuto, PDO::PARAM_INT);
        $sth->bindParam(':seqCadastOficial', $seqCadast, PDO::PARAM_INT);

        if ($c->run($sth,false,false,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraStatusAtividadeEstatutoRealizada($idAtividadeEstatuto=null) {

        $sql = "UPDATE atividadeEstatuto SET `statusAtividadeEstatuto` = 2 WHERE `idAtividadeEstatuto`=:idAtividadeEstatuto";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeEstatuto', $idAtividadeEstatuto, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function alteraStatusAtividadeCancelada($idAtividade=null) {

        $sql = "UPDATE atividadeEstatuto SET `statusAtividadeEstatuto` = 0 "
                . " WHERE `idAtividadeEstatuto` = :idAtividadeEstatuto";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeEstatuto', $idAtividade, PDO::PARAM_INT);

        $data = array('idAtividadeEstatuto'=>$idAtividade);
        //echo "QUERY: " . $this->parms("alteraStatusAtividadeIniciaticaCancelada",$sql,$data);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function excluirAtividade($idAtividade) {

        $sql = "DELETE FROM atividadeEstatuto "
                . " WHERE `idAtividadeEstatuto` = :idAtividadeEstatuto";
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idAtividadeEstatuto', $idAtividade, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function excluirTodasAtividadesDoMes($idOrganismoAfiliado,$mesAtual,$anoAtual) {

        $sql = "DELETE FROM atividadeEstatuto "
                . " WHERE `fk_idOrganismoAfiliado` = :idOrganismoAfiliado"
                . " and MONTH(dataAtividadeEstatuto) = :mesAtual "
                . " and YEAR(dataAtividadeEstatuto) = :anoAtual ";
        
        //$data = array('idOrganismoAfiliado'=>$idOrganismoAfiliado,'mesAtual'=>$mesAtual,'anoAtual'=>$anoAtual);
        //echo "QUERY: " . $this->parms("excluirTodasAtividadesDoMes",$sql,$data);
        //
        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':mesAtual', $mesAtual, PDO::PARAM_INT);
        $sth->bindParam(':anoAtual', $anoAtual, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function dataCriacao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM atividadeEstatuto where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataAtividadeEstatuto)=:mes";
        $sql .= " and YEAR(dataAtividadeEstatuto)=:ano";
        $sql .= " order by dataCadastradoAtividadeEstatuto ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataCadastradoAtividadeEstatuto'];
            }

        }
        return false;
    }

    public function dataUltimaAlteracao($mes,$ano, $idOrganismoAfiliado) {

        $sql = "SELECT * FROM atividadeEstatuto where 1=1 ";
        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataAtividadeEstatuto)=:mes";
        $sql .= " and YEAR(dataAtividadeEstatuto)=:ano";
        $sql .= " order by dataCadastradoAtividadeEstatuto ASC limit 1";
        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);



        $resultado = $c->run($sth);

        if($resultado)
        {
            foreach($resultado as $v)
            {
                return $v['dataAtualizadoAtividadeEstatuto'];
            }

        }
        return false;
    }

    public function responsaveis($mes,$ano, $idOrganismoAfiliado,$ultimaAlteracao=null) {

        if($ultimaAlteracao==null)
        {
            $sql = "SELECT * FROM atividadeEstatuto  as r  left join usuario on seqCadast = fk_seqCadastAtualizadoPor where 1=1 ";
        }else{
            $sql = "SELECT * FROM atividadeEstatuto  as r left join usuario on seqCadast = fk_seqCadastAtualizadoPor where 1=1 ";
        }

        $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        $sql .= " and MONTH(dataAtividadeEstatuto)=:mes";
        $sql .= " and YEAR(dataAtividadeEstatuto)=:ano and seqCadast <>0 order by r.dataCadastradoAtividadeEstatuto";

        //echo "<br>".$sql;

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);

        $resultado = $c->run($sth);

        $arr=array();

        $i=0;
        if($resultado)
        {
            foreach($resultado as $v)
            {
                $arr['data'][$i] = $v['dataCadastro'];
                $arr['seq'][$i] = $v['seqCadast'];
                $arr['nome'][$i] = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
                $i++;
            }

        }
        return $arr;
    }

    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}

?>
