<?php
require_once ("conexaoClass.php");

class DepartamentoObjetoDashboard {

    public $fk_idDepartamento;
    public $fk_idObjetoDashboard;
    
    /* Funções GET e SET */

    function getFkIdDepartamento() {
        return $this->fk_idDepartamento;
    }

    function getFkIdObjetoDashboard() {
        return $this->fk_idObjetoDashboard;
    }

    function setFkIdDepartamento($fk_idDepartamento) {
        $this->fk_idDepartamento = $fk_idDepartamento;
    }

    function setFkIdObjetoDashboard($fk_idObjetoDashboard) {
        $this->fk_idObjetoDashboard = $fk_idObjetoDashboard;
    }

    public function cadastra() {
        
    	$sql = "INSERT INTO departamento_objetoDashboard (`fk_idDepartamento`, `fk_idObjetoDashboard`)
                                    VALUES (:idDepartamento,
                                            :idObjetoDashboard)";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idObjetoDashboard',$this->fk_idObjetoDashboard , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM departamento_objetoDashboard 
        						WHERE fk_idDepartamento = :idDepartamento
        						and fk_idObjetoDashboard = :idObjetoDashboard";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idObjetoDashboard',$this->fk_idObjetoDashboard , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaPorDepartamento() {
        
        $sql = "SELECT * FROM departamento_objetoDashboard
                        WHERE `fk_idDepartamento` = :idDepartamento
                              ";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
}
?>