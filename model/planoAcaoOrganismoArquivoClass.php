<?php

require_once ("conexaoClass.php");

class planoAcaoOrganismoArquivo{

    private $idPlanoOrganismoArquivo;
    private $fk_idPlanoAcaoOrganismo;
    private $caminhoPlanoOrganismoArquivo;
    

    //METODO SET/GET -----------------------------------------------------------

    function getIdPlanoOrganismoArquivo() {
        return $this->idPlanoOrganismoArquivo;
    }

    function getFkIdPlanoAcaoOrganismo() {
        return $this->fk_idPlanoAcaoOrganismo;
    }

    function getCaminhoPlanoOrganismoArquivo() {
        return $this->caminhoPlanoOrganismoArquivo;
    }

    function setIdPlanoOrganismoArquivo($idPlanoOrganismoArquivo) {
        $this->idPlanoOrganismoArquivo = $idPlanoOrganismoArquivo;
    }

    function setFkIdPlanoAcaoOrganismo($fk_idPlanoAcaoOrganismo) {
        $this->fk_idPlanoAcaoOrganismo = $fk_idPlanoAcaoOrganismo;
    }

    function setCaminhoPlanoOrganismoArquivo($caminhoPlanoOrganismoArquivo) {
        $this->caminhoPlanoOrganismoArquivo = $caminhoPlanoOrganismoArquivo;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlanoOrganismoArquivo(){

    	$sql = "INSERT INTO planoAcaoOrganismo_arquivo  
                                 (
                                  fk_idPlanoAcaoOrganismo,
                                  caminho,
                                  dataCadastro
                                  )
                                   
                            VALUES (:idPlanoAcaoOrganismo,
                                    :caminho,
                                    now())";
    	//echo $sql; exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idPlanoAcaoOrganismo', $this->fk_idPlanoAcaoOrganismo, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminhoPlanoOrganismoArquivo, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'planoAcaoOrganismo_arquivo'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
	        foreach ($resultado as $vetor)
	    	{
	    		$proximo_id = $vetor['Auto_increment']; 
	    	}
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function listaPlanoOrganismoArquivo($id=null){

    	$sql = "SELECT * FROM planoAcaoOrganismo_arquivo
    	inner join planoAcaoOrganismo on fk_idPlanoAcaoOrganismo = idPlanoAcaoOrganismo
    	where 1=1";
    	if($id!=null)
    	{
    		$sql .= " and fk_idPlanoAcaoOrganismo = :id";
    	}
    	
    	//echo "<br><br>".$sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);        
        }
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removePlanoAcaoOrganismoArquivo($idPlanoAcaoOrganismoArquivo)
    {
        
    	$sql = "DELETE FROM planoAcaoOrganismo_arquivo WHERE idPlanoAcaoOrganismoArquivo = :id";
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idPlanoAcaoOrganismoArquivo, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
