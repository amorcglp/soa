<?php
require_once ("conexaoClass.php");

class DepartamentoNivelDePermissao {

    public $fk_idDepartamento;
    public $fk_idNivelDePermissao;
    
    /* Funções GET e SET */

    function getFkIdDepartamento() {
        return $this->fk_idDepartamento;
    }

    function getFkIdNivelDePermissao() {
        return $this->fk_idNivelDePermissao;
    }

    function setFkIdDepartamento($fk_idDepartamento) {
        $this->fk_idDepartamento = $fk_idDepartamento;
    }

    function setFkIdNivelDePermissao($fk_idNivelDePermissao) {
        $this->fk_idNivelDePermissao = $fk_idNivelDePermissao;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO departamento_nivelDePermissao (`fk_idDepartamento`, `fk_idNivelDePermissao`)
                                    VALUES (:idDepartamento,
                                            :idNivelDePermissao)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idNivelDePermissao',$this->fk_idNivelDePermissao , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM departamento_nivelDePermissao 
                                WHERE fk_idDepartamento = :idDepartamento
                                and fk_idNivelDePermissao = :idNivelDePermissao";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        $sth->bindParam(':idNivelDePermissao',$this->fk_idNivelDePermissao , PDO::PARAM_INT);
   
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function buscaPorDepartamento($retornaString=null) {
        
        $sql = "SELECT * FROM departamento_nivelDePermissao
                                 WHERE `fk_idDepartamento` = :idDepartamento
                              ";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idDepartamento', $this->fk_idDepartamento, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if($retornaString==1)
        {
	        $i=0;
	        $string="0";
	        
	        foreach($resultado as $vetor)
	        {
	        	if($i==0)
	        	{
	        		$string = $vetor['fk_idNivelDePermissao'];
	        	}else{
	        		$string .= ",".$vetor['fk_idNivelDePermissao'];
	        	}
	        	$i++;
	        }
	        
	        return $string;
        }else{
        	if($resultado)
        	{
        		return $resultado;
        	}else{
        		return false;
        	}
        }
    }
}
?>​