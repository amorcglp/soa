<?php

require_once ("conexaoClass.php");

class valoresDoMesEntradas{

    private $mes;
    private $ano;
    private $fk_idOrganismoAfiliado;
    private $valorEntrada;

    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @param mixed $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @return mixed
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * @param mixed $ano
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    }

    /**
     * @return mixed
     */
    public function getFkIdOrganismoAfiliado()
    {
        return $this->fk_idOrganismoAfiliado;
    }

    /**
     * @param mixed $fk_idOrganismoAfiliado
     */
    public function setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado)
    {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    /**
     * @return mixed
     */
    public function getValorEntrada()
    {
        return $this->valorEntrada;
    }

    /**
     * @param mixed $valorEntrada
     */
    public function setValorEntrada($valorEntrada)
    {
        $this->valorEntrada = $valorEntrada;
    }




    public function cadastro(){

    	$sql = "INSERT INTO valoresDoMesEntradas 
                                 (mes,
                                  ano,
                                  fk_idOrganismoAfiliado,
                                  valorEntrada,
                                  dataCadastro
                                  )
                            VALUES (
                                    :mes,
                                    :ano,
                            		:idOrganismoAfiliado,
                            		:valorEntrada,
                                    now()
                                    )";
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':mes', $this->mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':valorEntrada', $this->valorEntrada, PDO::PARAM_STR);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);

        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function lista($mes=null,$ano=null,$idOrganismoAfiliado=null,$ultimoRegistro=null){

        $sql = "SELECT 
                      CAST(valorEntrada  AS DECIMAL(12,2)) as valorEntrada
                      FROM valoresDoMesEntrada
    	inner join organismoAfiliado on fk_idOrganismoAfiliado = idOrganismoAfiliado
    	where 1=1
    	";

        if($mes!=null)
        {
            $sql .= " and mes=:mes";
        }

        if($ano!=null)
        {
            $sql .= " and ano=:ano";
        }

        if($idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }

        if($ultimoRegistro!=null)
        {
            $sql .= " order by ano,mes DESC LIMIT 0,1";
        }

        //echo $sql;
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        if($mes!=null)
        {
            $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        }

        if($ano!=null)
        {
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }

        if($idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);
        }

        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function atualizaValoresDoMesEntradas($mes,$ano,$idOrganismoAfiliado,$totalEntradas){


        $sql="UPDATE valoresDoMesEntradas SET  
                                  valorEntrada = :totalEntradasDoMes,
                                  dataCadastro = now()    
                                  WHERE mes = :mes and ano = :ano and fk_idOrganismoAfiliado = :idOrganismoAfiliado
                            ";

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);


        $sth->bindParam(':totalEntradasDoMes', $totalEntradas, PDO::PARAM_STR);
        $sth->bindParam(':mes', $mes, PDO::PARAM_INT);
        $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $idOrganismoAfiliado, PDO::PARAM_INT);


        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
}

?>
