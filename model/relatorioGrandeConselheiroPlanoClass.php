<?php

require_once ("conexaoClass.php");

class relatorioGrandeConselheiroPlano{

    private $idRelatorioGrandeConselheiroPlano;
    private $fk_idRelatorioGrandeConselheiro;
    private $caminho;
    private $nomeArquivo;
    private $extensao;

    //METODO SET/GET -----------------------------------------------------------

    function getIdRelatorioGrandeConselheiroPlano() {
        return $this->idRelatorioGrandeConselheiroPlano;
    }

    function getFkIdRelatorioGrandeConselheiro() {
        return $this->fk_idRelatorioGrandeConselheiro;
    }

    function getCaminho() {
        return $this->caminho;
    }
    
	function getNomeArquivo() {
        return $this->nomeArquivo;
    }
    
	function getExtensao() {
        return $this->extensao;
    }

    function setIdRelatorioGrandeConselheiroPlano($idRelatorioGrandeConselheiroPlano) {
        $this->idRelatorioGrandeConselheiroPlano = $idRelatorioGrandeConselheiroPlano;
    }

    function setFkIdRelatorioGrandeConselheiro($fk_idRelatorioGrandeConselheiro) {
        $this->fk_idRelatorioGrandeConselheiro = $fk_idRelatorioGrandeConselheiro;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }
    
	function setNomeArquivo($nomeArquivo) {
        $this->nomeArquivo = $nomeArquivo;
    }
    
	function setExtensao($extensao) {
        $this->extensao = $extensao;
    }

    //--------------------------------------------------------------------------

    public function cadastroPlano(){

        $sql="INSERT INTO relatorioGrandeConselheiro_plano  
                                 (
                                  fk_idRelatorioGrandeConselheiro,
                                  caminho,
                                  nome_arquivo,
                                  extensao,
                                  dataCadastro
                                  )     
                            VALUES (:fkIdRelatorioGrandeConselheiro,
                                    :caminho,
                                    :arquivo,
                                    :extensao,
                                    now())";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getFkIdRelatorioGrandeConselheiro              = $this->getFkIdRelatorioGrandeConselheiro();
        $getCaminho                                     = $this->getCaminho();
        $getNomeArquivo                                 = $this->getNomeArquivo();
        $getExtensao                                    = $this->getExtensao();

        $sth->bindParam(':fkIdRelatorioGrandeConselheiro', $getFkIdRelatorioGrandeConselheiro, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $getCaminho, PDO::PARAM_STR);
        $sth->bindParam(':arquivo', $getNomeArquivo, PDO::PARAM_STR);
        $sth->bindParam(':extensao', $getExtensao, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioGrandeConselheiro_plano'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRelatorioGrandeConselheiroPlano($id=null){

    	$sql = "SELECT * FROM relatorioGrandeConselheiro_plano
    	inner join relatorioGrandeConselheiro on fk_idRelatorioGrandeConselheiro = idRelatorioGrandeConselheiro
    	where 1=1";
        
    	if($id!=null)
    	{
    		$sql .= " and fk_idRelatorioGrandeConselheiro = :id";
    	}
    	
    	//echo "<br><br>".$sql;
    	
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($id!=null)
    	{
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioGrandeConselheiroPlano($id){
        
        $sql="DELETE FROM relatorioGrandeConselheiro_plano WHERE idRelatorioGrandeConselheiroPlano = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    
}

?>
