<?php

require_once ("conexaoClass.php");

class objetoDashboard {

    public $idObjetoDashboard;
    public $nomeObjetoDashboard;
    public $descricaoObjetoDashboard;
    public $prioridadeObjetoDashboard;
    public $arquivoObjetoDashboard;
    public $fk_idTipoObjetoDashboard;
    public $statusObjetoDashboard;

    //Getter e Setter
    function getIdObjetoDashboard() {
        return $this->idObjetoDashboard;
    }

    function getNomeObjetoDashboard() {
        return $this->nomeObjetoDashboard;
    }
    
    function getDescricaoObjetoDashboard() {
        return $this->descricaoObjetoDashboard;
    }
    
    function getArquivoObjetoDashboard() {
        return $this->arquivoObjetoDashboard;
    }
    
    function getPrioridadeObjetoDashboard() {
        return $this->prioridadeObjetoDashboard;
    }
    
    function getFkIdTipoObjetoDashboard() {
        return $this->fk_idTipoObjetoDashboard;
    }
    
    function getStatusObjetoDashboard() {
        return $this->statusObjetoDashboard;
    }
    
    function setIdObjetoDashboard($idObjetoDashboard) {
        $this->idObjetoDashboard = $idObjetoDashboard;
    }

    function setNomeObjetoDashboard($nomeObjetoDashboard) {
        $this->nomeObjetoDashboard = $nomeObjetoDashboard;
    }
    
    function setDescricaoObjetoDashboard($descricaoObjetoDashboard) {
        $this->descricaoObjetoDashboard = $descricaoObjetoDashboard;
    }
    
    function setArquivoObjetoDashboard($arquivoObjetoDashboard) {
        $this->arquivoObjetoDashboard = $arquivoObjetoDashboard;
    }
    
    function setPrioridadeObjetoDashboard($prioridadeObjetoDashboard) {
        $this->prioridadeObjetoDashboard = $prioridadeObjetoDashboard;
    }
    
    function setFkIdTipoObjetoDashboard($fk_idTipoObjetoDashboard) {
        $this->fk_idTipoObjetoDashboard = $fk_idTipoObjetoDashboard;
    }
    
    function setStatusObjetoDashboard($statusObjetoDashboard) {
        $this->statusObjetoDashboard = $statusObjetoDashboard;
    }

    public function cadastraObjetoDashboard() 
    {    
        $sql = "INSERT INTO objetoDashboard (idObjetoDashboard, nomeObjetoDashboard, descricaoObjetoDashboard, arquivoObjetoDashboard, prioridadeObjetoDashboard, fk_idTipoObjetoDashboard)
                                    VALUES (:id,
                                            :nome,
                                            :descricao,
                                            :arquivo,
                                            :prioridade,
                                            :idTipoObjetoDashboard
                                            )";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nomeObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':arquivo', $this->arquivoObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $this->prioridadeObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':idTipoObjetoDashboard', $this->fk_idTipoObjetoDashboard, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function alteraObjetoDashboard($id) 
    {
        $sql = "UPDATE objetoDashboard 
                            SET 
                                `nomeObjetoDashboard` = :nome,
                                `descricaoObjetoDashboard` = :descricao,
                                `arquivoObjetoDashboard` = :arquivo,
                                `prioridadeObjetoDashboard` = :prioridade,
                                `fk_idTipoObjetoDashboard` = :idTipoObjetoDashboard
                            WHERE idObjetoDashboard = :id";
        
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':nome', $this->nomeObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':descricao', $this->descricaoObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':arquivo', $this->arquivoObjetoDashboard, PDO::PARAM_STR);
        $sth->bindParam(':prioridade', $this->prioridadeObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':idTipoObjetoDashboard', $this->fk_idTipoObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function alteraStatusObjetoDashboard() {
        
        $sql = "UPDATE objetoDashboard SET `statusObjetoDashboard` = :status  "
                . " WHERE `idObjetoDashboard` = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':status', $this->statusObjetoDashboard, PDO::PARAM_INT);
        $sth->bindParam(':id', $this->idObjetoDashboard, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $arr = array();
            $arr['status'] = $this->getStatusObjetoDashboard();
            return $arr;
        } else {
            return false;
        }
    }

    public function listaObjetoDashboard() {

    	$sql = "SELECT * FROM objetoDashboard
                inner join tipoObjetoDashboard on fk_idTipoObjetoDashboard = idTipoObjetoDashboard
                where 1=1 ORDER BY nomeObjetoDashboard";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function listaObjetoDashboardInicio($tipoObjetoDashboard=null,$arrPermissaoDepartamento=null,$arrPermissaoFuncao=null) {

    	$sql = "SELECT * FROM objetoDashboard
                inner join tipoObjetoDashboard on fk_idTipoObjetoDashboard = idTipoObjetoDashboard
                where 1=1
                and statusObjetoDashboard=0 ";
    	if($tipoObjetoDashboard!=null)
        {
                $sql .= " and tipoObjetoDashboard=:tipo";
        }
        $sql .= "  ORDER BY prioridadeObjetoDashboard";
        //echo $sql."<br>";
        
        $objetos_dashboard=array();
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($tipoObjetoDashboard!=null)
        {
            $sth->bindParam(':tipo', $tipoObjetoDashboard, PDO::PARAM_STR);
        }
        
        $resultado = $c->run($sth);

        if ($resultado) {
        
   			foreach ($resultado as $vetor) {
   			if(
					//pegar todos os dashboards
					(count($arrPermissaoDepartamento)==null&&count($arrPermissaoFuncao)==null)
					||
					//Ou �pegar apenas os dashboards permitidos para o grupo ou subgrupo
					(
					(count($arrPermissaoDepartamento)&&in_array($vetor['idObjetoDashboard'],$arrPermissaoDepartamento['fk_idObjetoDashboard']))
					|| 
					(count($arrPermissaoFuncao)>0&&in_array($vetor['idObjetoDashboard'],$arrPermissaoFuncao['fk_idObjetoDashboard']))
					)
				)
				{
					$objetos_dashboard['idobjeto_dashboard'][]		= $vetor['idObjetoDashboard'];
					$objetos_dashboard['objeto_dashboard'][]		= $vetor['nomeObjetoDashboard'];
					$objetos_dashboard['fktipo_objeto_dashboard'][]	= $vetor['fk_idTipoObjetoDashboard'];
					$objetos_dashboard['tipo_objeto_dashboard'][]	= $vetor['tipoObjetoDashboard'];
					$objetos_dashboard['descricao'][]				= $vetor['descricaoObjetoDashboard'];
					$objetos_dashboard['arquivo'][]					= $vetor['arquivoObjetoDashboard'];
					$objetos_dashboard['prioridade'][]				= $vetor['prioridadeObjetoDashboard'];
					$objetos_dashboard['proporcao'][]				= $vetor['porLinhaObjetoDashboard'];
				}
   			}
            return $objetos_dashboard;
        } else {
            return false;
        }
    }

    public function removeObjetoDashboard($idObjetoDashboard) {
        
        $sql = "DELETE FROM objetoDashboard WHERE idObjetoDashboard = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idObjetoDashboard, PDO::PARAM_INT);
        $resultado = $c->run($sth,true);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function buscaObjetoDashboard() {
        $sql = "SELECT * FROM objetoDashboard
                                 ORDER BY nomeObjetoDashboard ASC";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

    public function buscaIdObjetoDashboard() 
    {
        
        $sql = "SELECT * FROM objetoDashboard 
                              WHERE idObjetoDashboard = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if(is_numeric($_GET['id']))
        {
            $id = $_GET['id'];
            $sth->bindParam(':id', $id, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }

}

?>