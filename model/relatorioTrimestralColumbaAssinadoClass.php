<?php
require_once ("conexaoClass.php");

class RelatorioTrimestralColumbaAssinado {

    public $idRelatorioTrimestralColumbaAssinado;
    public $fk_idOrganismoAfiliado;
    public $trimestre;
    public $ano;
    public $caminho;
    public $usuario;

    /* Funções GET e SET */

    function getIdRelatorioTrimestralColumbaAssinado() {
        return $this->idRelatorioTrimestralColumbaAssinado;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getTrimestre() {
        return $this->trimestre;
    }

    function getAno() {
        return $this->ano;
    }

    function getCaminho() {
        return $this->caminho;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function setIdRelatorioTrimestralColumbaAssinado($idRelatorioTrimestralColumbaAssinado) {
        $this->idRelatorioTrimestralColumbaAssinado = $idRelatorioTrimestralColumbaAssinado;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setTrimestre($trimestre) {
        $this->trimestre = $trimestre;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setCaminho($caminho) {
        $this->caminho = $caminho;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    
    public function cadastraRelatorioTrimestralColumbaAssinado() {
        
        $sql = "INSERT INTO relatorioTrimestralColumbaAssinado 
                                    (`fk_idOrganismoAfiliado`, 
                                    `trimestre`, 
                                    `ano`,
                                    `caminho`,
                                    `usuario`,
                                    `dataCadastro`
                                    )
                                    VALUES (
                                            :idOrganismoAfiliado,
                                            :trimestre,
                                            :ano,
                                            '',
                                            :usuario,
                                            now()
                                            )";
        //echo $sql;exit();
        $id=1;
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':usuario', $this->usuario, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            $resultado2 = $this->selecionaUltimoIdInserido();
            if($resultado2)
            {
                foreach ($resultado2 as $vetor)
                {
                    $id = $vetor['idRelatorioTrimestralColumbaAssinado'];
                }
            }
        }
        return $id;
    }

    public function alteraRelatorioTrimestralColumbaAssinado($id) {

        $sql = "UPDATE relatorioTrimestralColumbaAssinado SET 
        					trimestre = :trimestre,
                                                ano = :ano,
                                                caminho = :caminho
                            WHERE `idRelatorioTrimestralColumbaAssinado` = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':trimestre', $this->trimestre, PDO::PARAM_INT);
        $sth->bindParam(':ano', $this->ano, PDO::PARAM_INT);
        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function atualizaCaminhoRelatorioTrimestralColumbaAssinado($id) {

        $sql = "UPDATE relatorioTrimestralColumbaAssinado SET 
                            caminho = :caminho
                            WHERE idRelatorioTrimestralColumbaAssinado = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $sth->bindParam(':caminho', $this->caminho, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function selecionaUltimoId(){

        $sql= "SHOW TABLE STATUS LIKE 'relatorioTrimestralColumbaAssinado'";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
            return $proximo_id;
        }else{
            return false;
        }
    }

    public function selecionaUltimoIdInserido(){

        $sql= "SELECT * FROM relatorioTrimestralColumbaAssinado ORDER BY idRelatorioTrimestralColumbaAssinado DESC LIMIT 1";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function listaRelatorioTrimestralColumbaAssinado($trimestre=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioTrimestralColumbaAssinado as rfm
    		inner join organismoAfiliado as oa on rfm.fk_idOrganismoAfiliado = oa.idOrganismoAfiliado
    		inner join usuario as u on rfm.usuario = u.seqCadast
    	where 1=1 ";
    	if($trimestre!=null)
    	{
    		$sql .= " and trimestre=:trimestre";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($trimestre!=null)
    	{
            $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaCadastrado($trimestre=null,$ano=null,$fk_idOrganismoAfiliado=null) {
        
    	$sql = "SELECT * FROM relatorioTrimestralColumbaAssinado where 1=1 ";
    	if($trimestre!=null)
    	{
    		$sql .= " and trimestre=:trimestre";
    	}
    	if($ano!=null)
    	{
    		$sql .= " and ano=:ano";
    	}
    	if($fk_idOrganismoAfiliado!=null)
    	{
    		$sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
    	}
    	//echo $sql;
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($trimestre!=null)
    	{
            $sth->bindParam(':trimestre', $trimestre, PDO::PARAM_INT);
        }
        if($ano!=null)
    	{
            $sth->bindParam(':ano', $ano, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
    	{
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

    public function retornaIdRelatorioTrimestralColumbaAssinado() {
    	
    	$sql = "SELECT * FROM relatorioTrimestralColumbaAssinado 
                                 WHERE `trimestre` = :trimestre
                                 and `ano` = :ano
                                 and `fk_idOrganismoAfiliado` =  :idOrganismoAfiliado
                                 ";
    	//echo $sql;
   	    $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);

        $getTrimestre                           = $this->getTrimestre();
        $getAno                                 = $this->getAno();
        $getFkIdOrganismoAfiliado               = $this->getFkIdOrganismoAfiliado();

        $sth->bindParam(':trimestre', $getTrimestre, PDO::PARAM_STR);
        $sth->bindParam(':ano', $getAno, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $getFkIdOrganismoAfiliado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
		
        $id=0;
        if ($resultado) {
        	foreach ($resultado as $vetor)
        	{
        		$id = $vetor['idRelatorioTrimestralColumbaAssinado'];
        	}
        }
        return $id;
    }

    public function buscarIdRelatorioTrimestralColumbaAssinado($idRelatorioTrimestralColumbaAssinado) {
        
        $sql = "SELECT * FROM relatorioTrimestralColumbaAssinado 
                                 WHERE `idRelatorioTrimestralColumbaAssinado` = :id";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $idRelatorioTrimestralColumbaAssinado, PDO::PARAM_INT);
        
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }
    
    public function removeRelatorioTrimestralColumbaAssinado($id){
        
        $sql="DELETE FROM relatorioTrimestralColumbaAssinado WHERE idRelatorioTrimestralColumbaAssinado = :id";
        
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }

}
?>