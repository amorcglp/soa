<?php
require_once ("conexaoClass.php");

class FuncaoNivelDePermissao {

    public $fk_idFuncao;
    public $fk_idNivelDePermissao;
    
    /* Funções GET e SET */

    function getFkIdFuncao() {
        return $this->fk_idFuncao;
    }

    function getFkIdNivelDePermissao() {
        return $this->fk_idNivelDePermissao;
    }

    function setFkIdFuncao($fk_idFuncao) {
        $this->fk_idFuncao = $fk_idFuncao;
    }

    function setFkIdNivelDePermissao($fk_idNivelDePermissao) {
        $this->fk_idNivelDePermissao = $fk_idNivelDePermissao;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO funcao_nivelDePermissao (`fk_idFuncao`, `fk_idNivelDePermissao`)
                                    VALUES (:idFuncao,
                                            :idNivel)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':idNivel', $this->fk_idNivelDePermissao, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM funcao_nivelDePermissao 
                WHERE fk_idFuncao = :idFuncao
                and fk_idNivelDePermissao = :idNivel";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->fk_idFuncao, PDO::PARAM_INT);
        $sth->bindParam(':idNivel', $this->fk_idNivelDePermissao, PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaPorFuncao($fk_idFuncao=null,$string=null) {
        
    	$sql = "SELECT * FROM funcao_nivelDePermissao
                                 WHERE 1=1";
        if($string!=null)
    	{
            //echo $string;echo "/";
            $ids = explode("','", $string);
            for($i=0;$i<count($ids);$i++)
            {
                if($i==0)
                {    
                    $inQuery = ":".$i;
                }else{
                    $inQuery .= ",:".$i;
                }
            }
    		$sql .= " 
                              and   `fk_idFuncao` IN (".$inQuery.")
                              ";
    	}
    	if($fk_idFuncao!=null)
    	{
    		$sql .= " 
                              and   `fk_idFuncao` = :idFuncao
                              ";
    	}
    	
    	//echo $sql;exit();
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($string!=null)
    	{
            foreach ($ids as $k => $id)
            {    
                $sth->bindValue(":".$k, $id);
            }
        }
        if($fk_idFuncao!=null)
    	{
            $sth->bindParam(':idFuncao', $fk_idFuncao, PDO::PARAM_INT);
        }
        
        $resultado = $c->run($sth);
        //print_r($resultado);
        //exit();
        if($string!=null)
        {
        	$i=0;
	        $string="0";
	        
	        foreach($resultado as $vetor)
	        {
	        	if($i==0)
	        	{
	        		$string = $vetor['fk_idNivelDePermissao'];
	        	}else{
	        		$string .= ",".$vetor['fk_idNivelDePermissao'];
	        	}
	        	$i++;
	        }
	        //echo "stringggggggggg".$string;exit();
	        return $string;
        }else{
        	if($resultado)
        	{
        		return $resultado;
        	}else{
        		return false;
        	}
        }
    }
}
?>​