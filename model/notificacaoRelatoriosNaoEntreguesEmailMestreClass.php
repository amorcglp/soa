<?php

require_once ("conexaoClass.php");

class notificacaoRelatoriosNaoEntreguesEmailMestre {

    public $idNotificacaoRelatoriosNaoEntreguesEmailMestre;
    public $fk_idUsuario;
    public $fk_idOrganismoAfiliado;
    public $nome;
    public $email;
    public $data;

    //Getter e Setter
    function getIdNotificacaoRelatoriosNaoEntreguesEmailMestre() {
        return $this->idNotificacaoRelatoriosNaoEntreguesEmailMestre;
    }

    function getFk_idUsuario() {
        return $this->fk_idUsuario;
    }

    function getFk_idOrganismoAfiliado() {
        return $this->fk_idOrganismoAfiliado;
    }

    function getNome() {
        return $this->nome;
    }

    function getEmail() {
        return $this->email;
    }

    function getData() {
        return $this->data;
    }

    function setIdNotificacaoRelatoriosNaoEntreguesEmailMestre($idNotificacaoRelatoriosNaoEntreguesEmailMestre) {
        $this->idNotificacaoRelatoriosNaoEntreguesEmailMestre = $idNotificacaoRelatoriosNaoEntreguesEmailMestre;
    }

    function setFk_idUsuario($fk_idUsuario) {
        $this->fk_idUsuario = $fk_idUsuario;
    }

    function setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado) {
        $this->fk_idOrganismoAfiliado = $fk_idOrganismoAfiliado;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setData($data) {
        $this->data = $data;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO notificacao_relatorios_nao_entregues_email_mestre 
                    (idNotificacaoRelatoriosNaoEntreguesEmailMestre, 
                    fk_idUsuario, 
                    fk_idOrganismoAfiliado, 
                    nome, 
                    email, 
                    data)
                        VALUES (:id,
                                :idUsuario,
                                :idOrganismoAfiliado,
                                :nome,
                                :email,
                                now())";
        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $this->idNotificacaoRelatoriosNaoEntreguesEmailMestre, PDO::PARAM_INT);
        $sth->bindParam(':idUsuario', $this->fk_idUsuario, PDO::PARAM_INT);
        $sth->bindParam(':idOrganismoAfiliado', $this->fk_idOrganismoAfiliado, PDO::PARAM_INT);
        $sth->bindParam(':nome', $this->nome, PDO::PARAM_STR);
        $sth->bindParam(':email', $this->email, PDO::PARAM_STR);
        
        if ($c->run($sth,true)){
            return true;
        }else{
            return false;
        }
    }
    
    public function verificaSeJaExiste($fk_idUsuario=null,$fk_idOrganismoAfiliado=null,$data=null) {

        $sql = "SELECT * FROM notificacao_relatorios_nao_entregues_email_mestre where 1=1 ";
        if($fk_idUsuario!=null)
        {
            $sql .= " and fk_idUsuario=:idUsuario";
        }
        if($fk_idOrganismoAfiliado!=null)
        {
            $sql .= " and fk_idOrganismoAfiliado=:idOrganismoAfiliado";
        }
        if($data!=null)
        {
            $sql .= " and MONTH(data)=:data";
        }
            
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        if($fk_idUsuario!=null)
        {
            $sth->bindParam(':idUsuario', $fk_idUsuario, PDO::PARAM_INT);
        }
        if($fk_idOrganismoAfiliado!=null)
        {
            $sth->bindParam(':idOrganismoAfiliado', $fk_idOrganismoAfiliado, PDO::PARAM_INT);
        }
        if($data!=null)
        {
            $sth->bindParam(':data', substr($data,5,2), PDO::PARAM_STR);
        }
       
        if ($c->run($sth,false,false,true)){
            return true;
        }else{
            return false;
        }
    }

    public function lista() {

        $sql = "SELECT * FROM notificacao_relatorios_nao_entregues_email_mestre";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
       
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

    public function remove($id) 
    {
        
        $sql = "DELETE FROM notificacao_relatorios_nao_entregues_email_mestre "
                . "WHERE idNotificacaoRelatoriosNaoEntreguesEmailMestre = :id";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
       
        $resultado = $c->run($sth);
        if ($resultado){
            return $resultado;
        }else{
            return false;
        }
    }

}

?>