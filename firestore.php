<?php

require_once 'vendor/autoload.php';

echo "<br>Teste Firestore (GET Colections e Subcolections)";


use Google\Cloud\Firestore\FirestoreClient;

/**
 * Initialize Cloud Firestore with default project ID.
 * ```
 * initialize();
 * ```
 */
function initialize()
{
    $projectId="sistemadeorganismosafiliados";

    // Create the Cloud Firestore client
    $db = new FirestoreClient([
        'projectId' => $projectId
    ]);

    $arrProjecaoCompleto = array();
    $i=0;
    $idDocumentoAlteracao=0;
    $idBalances=0;
    $mesAlteracao=2;
    $anoAlteracao=2016;

    # [START fs_get_document]
    $b = $db->collection('balances');
    $query = $b->documents();

    //echo "<pre>";print_r($query);

    foreach ($query as $document) {
        if ($document->exists()) {
            //printf('Document data for document %s:' . PHP_EOL, $document->id());
            //print_r($document->data());
            //printf(PHP_EOL);
            $arr = $document->data();
            //echo "<pre>";print_r($arr);

            $idBalances = $document->id();

            //Chamar subcoleção
            $projecao = $db->collection('balances/'.$document->id().'/balance');
            $queryProjecao = $projecao->documents();

            //echo "<pre>";print_r($query);

            foreach ($queryProjecao as $documentProjecao) {
                if ($documentProjecao->exists()) {
                    //printf('Document data for document %s:' . PHP_EOL, $document->id());
                    //print_r($document->data());
                    //printf(PHP_EOL);
                    $arrProjecao = $documentProjecao->data();

                    //echo "<pre>";print_r($arrProjecao);

                    $arrProjecaoCompleto[$i] = $arrProjecao['month'];
                    $i++;
                    $arrProjecaoCompleto[$i] = $arrProjecao['year'];
                    $i++;
                    $arrProjecaoCompleto[$i] = $arrProjecao['previousBalance'];
                    $i++;
                    $arrProjecaoCompleto[$i] = $arrProjecao['totalEntries'];
                    $i++;
                    $arrProjecaoCompleto[$i] = $arrProjecao['totalOutputs'];
                    $i++;
                    $arrProjecaoCompleto[$i] = $arrProjecao['monthBalance'];
                    $i++;

                    //Guardar posição do id que deve ser alterado
                    if ($arrProjecao['month'] == $mesAlteracao && $arrProjecao['year'] == $anoAlteracao) {
                        $idDocumentoAlteracao = $documentProjecao->id();
                    }

                }
            }

        } else {
            printf('Document %s does not exist!' . PHP_EOL, $document->id());
        }
    }
    # [END fs_get_document]

    $arr2 = array();
    $arr2['idBalances']=$idBalances;
    $arr2['idDocumentoAlteracao']=$idDocumentoAlteracao;
    $arr2['db']=$db;

    return $arr2;
}

$arrAlteracao = initialize();

echo "<br>Teste Firestore (UPDATE Colections e Subcolections)";

//Atualizar projecao conforme id

//Encontrar posição a ser alterada

echo "<br>Array Completo:";
//echo "<pre>";print_r($arrAlteracao);

# [START fs_set_document]
$dataArray =
    [
    'month'=> 2,
    'year'=> 2016,
    'previousBalance' => 77.77,
    'totalEntries' => 66.33,
    'totalOutputs' => 55.44,
    'monthBalance' => 22.11
    ];

//Atualizando Firebase
echo "<br>Iniciando Update:";

$projectId="sistemadeorganismosafiliados";

// Create the Cloud Firestore client
$db2 = new FirestoreClient([
    'projectId' => $projectId
]);

$docRef = $db2->collection("balances/".$arrAlteracao['idBalances']."/balance")->document($arrAlteracao['idDocumentoAlteracao']);
$docRef->set($dataArray);

printf('<br><br>Added data to the aturing document in the users collection.' . PHP_EOL);

/*

$db->collection('balances/'.$arrAlteracao['idBalances'].'/balance')->document($arrAlteracao['idDocumentoAlteracao'])->set($data);
# [END fs_set_document]
printf('Set data for the oa document in the balance collection.' . PHP_EOL);

*/

?>