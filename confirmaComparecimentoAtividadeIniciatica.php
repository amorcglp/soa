<?php
if(!$_SERVER['HTTPS']) {
    header( "Location: "."https://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
}
$t = isset($_REQUEST['t'])?$_REQUEST['t']:null;
$a = isset($_REQUEST['a'])?$_REQUEST['a']:null;

$resposta=false;

require_once 'model/atividadeIniciaticaClass.php';
require_once 'model/enviaEmailMembroAtividadeIniciaticaClass.php';

$atividade = new atividadeIniciatica();
$e = new enviaEmailMembroAtividadeIniciatica();

if($e->verificaToken($t))
{ 
    //echo "verificou o token";
    //Pegar as informações vinculadas ao token
    $resultado = $e->selecionaToken($t);
    if($resultado)
    {    
        foreach($resultado as $vetor)
        {
            $statusAtividade = $atividade->retornaStatusAtividadeIniciatica($vetor['fk_idAtividadeIniciatica']);
            //echo "statusAtividade: " . $statusAtividade;

            if(($statusAtividade==0) || ($statusAtividade==1)){

                //Se não foi confirmado então confirmar ou cancelar
                if($a==1)
                {
                    //confirmar
                    $resposta = $atividade->confirmaAgendamentoMembroAtividadeIniciatica($vetor['seqCadastMembro'], $vetor['fk_idAtividadeIniciatica']);
                    $titulo = "Confirmação de Comparecimento";
                    $mesagem = "Você foi confirmado com sucesso para participar da Iniciação. Aguardamos anciosamente sua presença!";
                    $class="alert-info";
                }
                if($a==2)
                {
                    //cancelar
                    $resposta = $atividade->cancelaAgendamentoMembroAtividadeIniciatica($vetor['seqCadastMembro'], $vetor['fk_idAtividadeIniciatica']);
                    $titulo = "Cancelamento de Comparecimento";
                    $mesagem = "Você foi cancelado com sucesso e não precisará participar da Iniciação!";
                    $class="alert-danger";
                }

            } elseif($statusAtividade==2) {

                $resposta = true;
                $titulo = "Iniciação já realizada";
                $mesagem = "Iniciação já realizada. Não é mais possível confirmar ou cancelar participação nesta atividade.";
                $class="alert-info";

            } elseif($statusAtividade==3) {

                $resposta = true;
                $titulo = "Iniciação cancelada";
                $mesagem = "Iniciação cancelada. Não é mais possível confirmar ou cancelar participação nesta atividade.";
                $class="alert-info";

            }
        }   
        
        if($resposta == true)
        {    
                ?>
                <!DOCTYPE html>
                <html>
                    <head>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">

                        <title>SOA | <?php echo $titulo;?></title>
                        <link href="img/icon_page.png" rel="shortcut icon">
                        <link href="css/bootstrap.min.css" rel="stylesheet">
                        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

                        <!-- Toastr style -->
                        <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

                        <!-- Sweet Alert -->
                        <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

                        <link href="css/animate.css" rel="stylesheet">
                        <link href="css/style.css" rel="stylesheet">

                        <!-- Loading Ajax -->
                        <link href="css/loadingajax.css" rel="stylesheet">

                    </head>

                    <body class="gray-bg">

                        <div class="text-center loginscreen  animated fadeInDown">
                            <div>
                                <br>
                                <div>
                                    <img src="img/solado.png">
                                    <!--<h1 class="logo-name">SOA</h1>-->
                                </div>
                                <br>
                                <h3>Atividades Iniciáticas</h3>
                                <p>Confirmação de Presença ou Ausência</p>
                                <div class="alert <?php echo $class;?>">
                                    <?php echo $mesagem;?>
                                </div>
                                <p class="m-t"> <small>Sistema de Organismos Afiliados AMORC-GLP &copy; <?php echo date('Y');?></small> </p>
                            </div>
                        </div>
                    </body>

                </html>
        <?php
        }
    }
}
?>