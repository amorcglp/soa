<?php
@include_once('https.php');

if((!isset($_COOKIE['lg'])) || (!isset($_COOKIE['nm'])) || (!isset($_COOKIE['av']))){
    echo "<script type='text/javascript'>window.location = './login.php';</script>";
}

$corpo      = (isset($_COOKIE['corpo'])) ? $_COOKIE['corpo'] : "";

$incorreto = isset($_REQUEST['incorreto'])?$_REQUEST['incorreto']:null;
if($incorreto==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Login ou senha Incorretos!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php
$inativo = isset($_REQUEST['inativo'])?$_REQUEST['inativo']:null;
if($inativo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "O usuário está inativo e não pode acessar o sistema! Caso precise acessar o sistema mesmo assim entre em contato com o setor de Organismos Afiliados pelo telefone +55 41 33513031",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!DOCTYPE html>
<?php
require_once("model/criaSessaoClass.php");
$sessao = new criaSessao();
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Tela de Bloqueio</title>
        <link href="img/icon_page.png" rel="shortcut icon">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="css/animate.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <!-- Sweet Alert -->
    	<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    </head>
    <body class="gray-bg">
        <!--<span class="first-word"><img style="max-width: 600px; margin-top: 5%; margin-left: 15%;" alt="logo_sgi" src="img/triangulo_com_rosa.png"> </span>-->
        <div class="middle-box text-center lockscreen animated fadeInDown">
            <div>
                <div class="m-b-md">
                    <img alt="image" style="max-width: 142px" class="img-circle circle-border" src="<?php echo $_COOKIE['av']; ?>">
                </div>
                <h3><?php echo utf8_encode(ucwords(strtolower($_COOKIE['nm']))); ?></h3>
                <p>Você está em uma tela de bloqueio. Digite novamente sua senha para acessar o sistema. Esta medida visa a segurança da sua conta.</p>
                <p>
                <form class="m-t" role="form" action="acoes/acaoLogin.php" method="post">
                    <div class="form-group">
                        <input type="hidden" name="loginUsuario" id="loginUsuario" class="form-control" placeholder="Login" value="<?php echo $_COOKIE['lg']; ?>">
                        <input type="password" name="senhaUsuario" id="senhaUsuario" class="form-control" placeholder="Senha" required="">
                    </div>
                    <input type="hidden" name="corpo" id="corpo" class="form-control" value="<?php echo $corpo; ?>">
                    <button type="submit" class="btn btn-primary block full-width">Desbloquear</button>
                </form>
                </p>
                <p><a href="./esqueciSenha.php" align="left"><i class="fa fa-key"></i> Esqueci a senha</a></p>
                <p><a href="./login.php" align="left"><i class="fa fa-share"></i> Não é minha conta</a></p>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="js/jquery-2.1.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Sweet alert -->
    	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
    </body>
</html>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
