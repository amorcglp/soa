<?php
$textoImpressao = "
Saudações em todas as pontas do nosso Sagrado Triângulo!

A partir deste momento a sua assinatura passará a ser eletrônica. O processo de adesão funciona através de um método de autenticação de dois fatores que garantirão que você é realmente o dono deste login.

A primeira etapa é a utilização de sua senha pessoal de acesso ao SOA e a segunda etapa é a utilização de um código de ativação que será enviado para o seu número de celular ou e-mail cadastrado no sistema de membros. O processo de ativação é individual e intransferível.

As assinaturas de documentos eletrônicos herdam as mesmas responsabilidades dos documentos antes assinados de “próprio punho”, portanto, concordando com este termo você está ciente que precisa manter essas informações em sigilo preservando sua identidade eletrônica enquanto membro oficial da AMORC-GLP e também se responsabiliza pelas informações registadas nos relatórios assinados por você.

Todo documento assinado por você será guardado em histórico para o seu acompanhamento.

Este presente termo será encaminhado para seu e-mail.

Na certeza de que saberá valorizar este Sagrado Legado,

Confio-lhe!

Antiga e Mística Ordem Rosacruz – Grande Loja da Jurisdição de Língua Portuguesa";
?>
    <!DOCTYPE html>
    <html>

    <head>

        <script type="text/javascript">
            window.smartlook||(function(d) {
                var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
                var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
                c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
            })(document);
            smartlook('init', '42b9199de0b0553dc89578ebab6f3ea5d65240fe');
        </script>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Sistema dos Organismos Afiliados</title>

        <link href="../img/icon_page.png" rel="shortcut icon" >

        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <!--<link href="font-awesome/../css/font-awesome.css" rel="stylesheet">-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link href="../css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

        <!-- Summernote -->
        <link href="../css/plugins/summernote/summernote.css" rel="stylesheet">
        <link href="../css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

        <!-- Data Tables -->
        <!--
        <link href="../css/plugins/dataTables/dataTables.bootstrap.css"
              rel="stylesheet">
        <link href="../css/plugins/dataTables/dataTables.responsive.css"
              rel="stylesheet">
        <link href="../css/plugins/dataTables/dataTables.tableTools.min.css"
              rel="stylesheet">-->
        <link href="../css/plugins/dataTables/datatables.min.css"
              rel="stylesheet">

        <link href="../css/animate.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">

        <link href="../css/plugins/datapicker/datepicker3.css" rel="stylesheet">
        <link href="../css/plugins/chosen/chosen.css" rel="stylesheet">
        <link href="../css/plugins/iCheck/custom.css" rel="stylesheet">

        <!-- Uploadify -->
        <link href='../css/uploadify.css' rel='stylesheet'>

        <!--Steps-->
        <link href="../css/plugins/steps/jquery.steps.css" rel="stylesheet">

        <!-- Clock Picker -->
        <link href="../css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <!-- Sweet Alert -->
        <link href="../css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <!-- Loading Ajax -->
        <link href="../css/loadingajax.css" rel="stylesheet">

        <!-- FooTable -->
        <link href="../css/plugins/footable/footable.core.css" rel="stylesheet">

        <link href="../css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
        <link href="../css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>

        <link href="../css/plugins/select2/select2.min.css" rel="stylesheet">

        <link href="../css/uploadifive.css" rel="stylesheet">

    </head>
    <style type="text/css" media="screen and (max-width: 750px)">
        #titulo_soa{
            font-size: 20px!important;
        }
        li.side-menu {
            margin:0 0 10px 0;
        }
    </style>

    <body>
    <center><a href="../login.php" class="btn btn-primary">Voltar para o Login</a> </center>
    <div class="modal inmodal" id="mySeeAssinatura" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 50%">
            <div class="modal-content animated bounceInUp">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <form id="imp" name="imp" class="form-horizontal" method="post" action="../painelDeControle.php" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <h1>Assinatura Eletrônica</h1>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div id="validarAdesao">
                                    <textarea name="textoTermo" id="textoTermo" class="form-control" style="width: 100%; background-color: white; font-family: cursive"
                                      rows="20" readonly=""><?php echo $textoImpressao; ?></textarea><br>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <!--<center><span id="load">(PRESSIONE SHIFT+F5 PARA ATUALIZAR A PÁGINA ANTES DE CONCORDAR)</span></center>-->
                        <span id="botao">
                            <input type="submit" class="btn btn-warning" name="ok" id="ok" value="Concordo" onclick="return validarAdesaoAssinaturaEletronica('<?php echo $_SESSION['seqCadast'];?>')">
                        </span>
                        <button type="button" class="btn btn-white" onclick="return location.href='../login.php'">Não Concordo</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
    <!-- Mainly scripts -->
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../js/plugins/jeditable/jquery.jeditable.js"></script>

    <!-- Flot -->

    <script src="../js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="../js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="../js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="../js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="../js/plugins/flot/curvedLines.js"></script>

    <!-- Peity -->
    <script src="../js/plugins/peity/jquery.peity.min.js"></script>
    <script src="../js/demo/peity-demo.js"></script>
    <!-- Custom and plugin javascript -->
    <script src="../js/inspinia.js"></script>
    <script src="../js/plugins/pace/pace.min.js"></script>
    <!-- jQuery UI -->
    <script src="../js/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Jvectormap -->

    <!-- Data Tables -->
    <script src="../js/plugins/dataTables/datatables.min.js"></script>
    <!--
    <script src="../js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="../js/plugins/dataTables/dataTables.responsive.js"></script>
    <script src="../js/plugins/dataTables/dataTables.tableTools.min.js"></script>->

    <!--<script src="../js/jquery.uploadifive.min.js"></script>-->
    <script src="lib/uploadifive-v1.2.2/jquery.uploadifive.js"></script>

    <!-- Nestable List -->
    <script src="../js/plugins/nestable/jquery.nestable.js"></script>

    <script src="../js/functions.js"></script>
    <script src="../js/functions3.js"></script>
    <script src="../js/functions4.js"></script>

    <!-- Chosen -->
    <script src="../js/plugins/chosen/chosen.jquery.js"></script>

    <!-- SummerNote -->
    <script src="../js/plugins/summernote/summernote.min.js"></script>



    <!-- Image cropper -->
    <script src="../js/plugins/cropper/cropper.min.js"></script>

    <!-- FooTable -->
    <script src="../js/plugins/footable/footable.all.min.js"></script>



    <!-- Toastr script -->
    <script src="../js/plugins/toastr/toastr.min.js"></script>

    <script src="../js/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="../js/jquery.maskMoney.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/paginadorPerfilUsuarioContatos.js"></script>

    <!-- iCheck -->
    <script src="../js/plugins/iCheck/icheck.min.js"></script>

    <!-- Full Calendar -->
    <script src="../js/plugins/fullcalendar/moment.min.js"></script>
    <script src="../js/plugins/fullcalendar/fullcalendar.min.js"></script>

    <!-- blueimp gallery -->
    <script src="../js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>

    <!-- Multiple file upload plugin -->
    <script src="../js/jquery.uploadify.min.js"></script>

    <!-- Flot Charts -->
    <script src="../js/plugins/flot/jquery.flot.js"></script>
    <script src="../js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../js/plugins/flot/jquery.flot.resize.js"></script>


    <!-- aqui -->



    <script src="../js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="../js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- Sparkline -->
    <script src="../js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Sparkline demo data  -->
    <script src="../js/demo/sparkline-demo.js"></script>
    <!-- ChartJS-->
    <script src="../js/plugins/chart../js/Chart.min.js"></script>

    <!-- Clock Picker -->
    <script src="../js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Sweet alert -->
    <script src="../js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Select2 -->
    <script src="../js/plugins/select2/select2.full.min.js"></script>



    <!-- Steps -->
    <script src="../js/plugins/steps/jquery.steps.min.js"></script>

    <script type="text/javascript" src="../js/functions2.js"></script>

    <script type="text/javascript" src="../js/functions5.js"></script>



    <!-- Data picker -->
    <script src="../js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <script>
        $('#mySeeAssinatura').modal({});
    </script>
<?php
exit();
?>