<?php

require_once 'loadEnv.php';

if(getenv('ENVIRONMENT') !== 'development') {
  if (!$_SERVER['HTTPS'])
    header( "Location: https://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
}