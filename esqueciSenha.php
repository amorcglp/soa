<!DOCTYPE html>
<?php
if(!$_SERVER['HTTPS']) { header( "Location: "."https://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
}

session_start();
$arrData = array();
$arrData['key'] = 777;
$logged=true;
require_once ('sec/make.php');
$_SESSION['tk']= $token;
$_SESSION['vk']= $arrData['key'];
?>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Esqueci a senha</title>
        <link href="img/icon_page.png" rel="shortcut icon">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <!-- Sweet Alert -->
    	<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <link href="css/animate.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        
        <!-- Loading Ajax -->
    	<link href="css/loadingajax.css" rel="stylesheet">

    </head>

    <body class="gray-bg">

        <div class="middle-box text-center loginscreen  animated fadeInDown">
            <div>
                <br>
                <div>
                    <img src="img/solado.png">
                    <!--<h1 class="logo-name">SOA</h1>-->
                </div>
                <br>
                <h3>SOA - Sistema de Organismos Afiliados</h3>
                <p>Recuperação/Alteração da Senha ou Login</p>

                <div id="carregando" class="container" style="display:none;margin-left:-80px;">
                    <svg id="svg" class="machine" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 645 526">
                    <defs/>
                    <g>
                    <path  x="-173,694" y="-173,694" class="large-shadow" d="M645 194v-21l-29-4c-1-10-3-19-6-28l25-14 -8-19 -28 7c-5-8-10-16-16-24L602 68l-15-15 -23 17c-7-6-15-11-24-16l7-28 -19-8 -14 25c-9-3-18-5-28-6L482 10h-21l-4 29c-10 1-19 3-28 6l-14-25 -19 8 7 28c-8 5-16 10-24 16l-23-17L341 68l17 23c-6 7-11 15-16 24l-28-7 -8 19 25 14c-3 9-5 18-6 28l-29 4v21l29 4c1 10 3 19 6 28l-25 14 8 19 28-7c5 8 10 16 16 24l-17 23 15 15 23-17c7 6 15 11 24 16l-7 28 19 8 14-25c9 3 18 5 28 6l4 29h21l4-29c10-1 19-3 28-6l14 25 19-8 -7-28c8-5 16-10 24-16l23 17 15-15 -17-23c6-7 11-15 16-24l28 7 8-19 -25-14c3-9 5-18 6-28L645 194zM471 294c-61 0-110-49-110-110S411 74 471 74s110 49 110 110S532 294 471 294z"/>
                    </g>
                    <g>
                    <path x="-136,996" y="-136,996" class="medium-shadow" d="M402 400v-21l-28-4c-1-10-4-19-7-28l23-17 -11-18L352 323c-6-8-13-14-20-20l11-26 -18-11 -17 23c-9-4-18-6-28-7l-4-28h-21l-4 28c-10 1-19 4-28 7l-17-23 -18 11 11 26c-8 6-14 13-20 20l-26-11 -11 18 23 17c-4 9-6 18-7 28l-28 4v21l28 4c1 10 4 19 7 28l-23 17 11 18 26-11c6 8 13 14 20 20l-11 26 18 11 17-23c9 4 18 6 28 7l4 28h21l4-28c10-1 19-4 28-7l17 23 18-11 -11-26c8-6 14-13 20-20l26 11 11-18 -23-17c4-9 6-18 7-28L402 400zM265 463c-41 0-74-33-74-74 0-41 33-74 74-74 41 0 74 33 74 74C338 430 305 463 265 463z"/>
                    </g>
                    <g >
                    <path x="-100,136" y="-100,136" class="small-shadow" d="M210 246v-21l-29-4c-2-10-6-18-11-26l18-23 -15-15 -23 18c-8-5-17-9-26-11l-4-29H100l-4 29c-10 2-18 6-26 11l-23-18 -15 15 18 23c-5 8-9 17-11 26L10 225v21l29 4c2 10 6 18 11 26l-18 23 15 15 23-18c8 5 17 9 26 11l4 29h21l4-29c10-2 18-6 26-11l23 18 15-15 -18-23c5-8 9-17 11-26L210 246zM110 272c-20 0-37-17-37-37s17-37 37-37c20 0 37 17 37 37S131 272 110 272z"/>
                    </g>
                    <g>
                    <path x="-100,136" y="-100,136" class="small" d="M200 236v-21l-29-4c-2-10-6-18-11-26l18-23 -15-15 -23 18c-8-5-17-9-26-11l-4-29H90l-4 29c-10 2-18 6-26 11l-23-18 -15 15 18 23c-5 8-9 17-11 26L0 215v21l29 4c2 10 6 18 11 26l-18 23 15 15 23-18c8 5 17 9 26 11l4 29h21l4-29c10-2 18-6 26-11l23 18 15-15 -18-23c5-8 9-17 11-26L200 236zM100 262c-20 0-37-17-37-37s17-37 37-37c20 0 37 17 37 37S121 262 100 262z"/>
                    </g>
                    <g>
                    <path x="-173,694" y="-173,694" class="large" d="M635 184v-21l-29-4c-1-10-3-19-6-28l25-14 -8-19 -28 7c-5-8-10-16-16-24L592 58l-15-15 -23 17c-7-6-15-11-24-16l7-28 -19-8 -14 25c-9-3-18-5-28-6L472 0h-21l-4 29c-10 1-19 3-28 6L405 9l-19 8 7 28c-8 5-16 10-24 16l-23-17L331 58l17 23c-6 7-11 15-16 24l-28-7 -8 19 25 14c-3 9-5 18-6 28l-29 4v21l29 4c1 10 3 19 6 28l-25 14 8 19 28-7c5 8 10 16 16 24l-17 23 15 15 23-17c7 6 15 11 24 16l-7 28 19 8 14-25c9 3 18 5 28 6l4 29h21l4-29c10-1 19-3 28-6l14 25 19-8 -7-28c8-5 16-10 24-16l23 17 15-15 -17-23c6-7 11-15 16-24l28 7 8-19 -25-14c3-9 5-18 6-28L635 184zM461 284c-61 0-110-49-110-110S401 64 461 64s110 49 110 110S522 284 461 284z"/>
                    </g>
                    <g>
                    <path x="-136,996" y="-136,996" class="medium" d="M392 390v-21l-28-4c-1-10-4-19-7-28l23-17 -11-18L342 313c-6-8-13-14-20-20l11-26 -18-11 -17 23c-9-4-18-6-28-7l-4-28h-21l-4 28c-10 1-19 4-28 7l-17-23 -18 11 11 26c-8 6-14 13-20 20l-26-11 -11 18 23 17c-4 9-6 18-7 28l-28 4v21l28 4c1 10 4 19 7 28l-23 17 11 18 26-11c6 8 13 14 20 20l-11 26 18 11 17-23c9 4 18 6 28 7l4 28h21l4-28c10-1 19-4 28-7l17 23 18-11 -11-26c8-6 14-13 20-20l26 11 11-18 -23-17c4-9 6-18 7-28L392 390zM255 453c-41 0-74-33-74-74 0-41 33-74 74-74 41 0 74 33 74 74C328 420 295 453 255 453z"/>
                    </g>
                    </svg>
                </div>
                <form class="m-t" role="form" action="" method="post">
                    <!--
                    <div class="form-group">
                        <input type="text" name="loginUsuario" id="loginUsuario" class="form-control" placeholder="BUSCAR POR LOGIN" required="" onChange="this.value = this.value.toLowerCase()">
                    </div>
                    -->
                    <div class="form-group">
                        <input type="text" name="emailUsuario" id="emailUsuario" class="form-control" placeholder="BUSCAR POR E-MAIL" required="" onChange="this.value = this.value.toLowerCase()">
                    </div>
                    <button type="button" id="pesquisar" class="btn btn-primary block full-width m-b">Pesquisar</button>
                </form>
                <a class="btn btn-sm btn-white btn-block" href="login.php">Login</a>
                <p class="m-t"> <small>Sistema de Organismos Afiliados AMORC-GLP &copy; 2015</small> </p>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="js/jquery-2.1.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="js/inspinia.js"></script>
        <script src="js/plugins/pace/pace.min.js"></script>

        <script src="js/functions.js"></script>

        <!-- Toastr script -->
        <script src="js/plugins/toastr/toastr.min.js"></script>

        <script src="js/jquery.maskedinput.js" type="text/javascript"></script>

        <script src="js/velocity.min.js"></script>
        
        <!-- Sweet alert -->
    	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>

        <script type="text/javascript">
                            $(function () {
                                //Carregando
                                $('.small, .small-shadow').velocity({
                                    rotateZ: [0, -360]}, {
                                    loop: true,
                                    duration: 2000
                                });
                                $('.medium, .medium-shadow').velocity({
                                    rotateZ: -240}, {
                                    loop: true,
                                    duration: 2000
                                });
                                $('.large, .large-shadow').velocity({
                                    rotateZ: 180}, {
                                    loop: true,
                                    duration: 2000
                                });

                                //Máscaras
                                $("#telefoneResidencialUsuario").mask("(999) 9999-9999");
                                $("#telefoneComercialUsuario").mask("(999) 9999-9999");
                                $("#celularUsuario").mask("(999) 99999-9999");

                                var i = -1;
                                var toastCount = 0;
                                var $toastlast;
                                var getMessage = function () {
                                    var msg = 'Hi, welcome to Inspinia. This is example of Toastr notification box.';
                                    return msg;
                                };

                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "progressBar": false,
                                    "positionClass": "toast-top-center",
                                    "onclick": null,
                                    "showDuration": "400",
                                    "hideDuration": "1000000",
                                    "timeOut": "7000000",
                                    "extendedTimeOut": "1000000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                }

                                $('#pesquisar').click(function () {
                                    //Loading

                                    if ($('#emailUsuario').val() == "")
                                    {
                                        toastr.warning('Preencha o campo login ou o campo e-mail para pesquisar', 'Aviso!');
                                    } else {
                                        //Chamar esqueci a senha
                                        esqueciSenha();

                                        // Limpar campos
                                        //$('#loginUsuario').val("");
                                        $('#emailUsuario').val("");
                                    }
                                });
                                function getLastToast() {
                                    return $toastlast;
                                }
                                $('#clearlasttoast').click(function () {
                                    toastr.clear(getLastToast());
                                });
                                $('#cleartoasts').click(function () {
                                    toastr.clear();
                                });
                            })
        </script>
    </body>

</html>

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>