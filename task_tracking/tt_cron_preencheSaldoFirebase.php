<?php

//Para usar esse recurso passar: idOrganismoAfiliado e idSOA do Recebimento

include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

function UrlAtual(){
    $dominio= $_SERVER['HTTP_HOST'];
    $url = $dominio;
    return $url;
}

echo "<b>Cron: Verificação de Saldo no Firebase (mes/ano)</b><br><br>";

echo "<br>Tempo 1 - Início da Execução da Cron: ".date("d/m/Y H:i:s");

require_once '../vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;

//Produção
if(UrlAtual()=="soa.amorc.org.br") {

    $projectId = "sistemadeorganismosafiliados";

}

//Teste
if(UrlAtual()=="treinamento.amorc.org.br") {
    $projectId = "soadesenvolvimento";
}

echo "<br>ProjectId no Firebase- ".$projectId;

// Create the Cloud Firestore client
$db = new FirestoreClient([
    'projectId' => $projectId
]);

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']:null;//Organismo alvo: 160
$mesString = isset($_REQUEST['mes']) ? strval($_REQUEST['mes']):null;//04
$anoString = isset($_REQUEST['ano']) ? strval($_REQUEST['ano']):null;//2019

if(isset($idOrganismoAfiliado))
{
    $o = new organismo();

    //Verificar se id existe
    $arrExiste = $o->buscaIdOrganismo($idOrganismoAfiliado);
    //echo "<pre>";print_r($arrExiste);

    if(isset($arrExiste[0]['idOrganismoAfiliado']))
    {
        $arr = $o->retornaSiglaOa($idOrganismoAfiliado);
        $idOrganismoAfiliado = $arr['id'];
        echo "<br>Id do Oa Atualizado:" . $arr['id'];
        echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
        echo "<br>Sigla:" . $arr['sigla'];

        /*
         * Testes para ver se existe perda de informação para PR101
         */

        //Busca pelo id do Documento no Firebase

        $b = $db->collection("balances/" . $idOrganismoAfiliado . "/balance");

        $queryWhereB = $b
            ->where('month', '=', $mesString)
            ->where('year', '=', $anoString)
        ;

        $queryB = $queryWhereB->documents();

        $dataArray=array();

        //Percorrer recebimentos
        foreach ($queryB as $documentB) {
            if ($documentB->exists()) {
                $idRefDocumentoFirebase = $documentB->id();
                $dataArray = $documentB->data();
            }
        }

        echo "<br>===================================================";
        echo "<br>Firebase:";
        echo "<br>Id no Firebase: ".$idRefDocumentoFirebase;
        echo "<br>Informações do documento no Firebase: <pre>"; print_r($dataArray);

        echo "<br>===================================================";
        echo "<br>Verificando a quantidade de arquivos (5 anos = 60 documentos)";

        $b2 = $db->collection("balances/" . $idOrganismoAfiliado . "/balance");
        $queryB2 = $b2->documents();

        $totalDocumentosSaldo=0;
        //Percorrer recebimentos
        foreach ($queryB2 as $documentB2) {
            if ($documentB2->exists()) {
               $totalDocumentosSaldo++;
            }
        }

        echo "<br>Total de Arquivos de Saldo para o Organismo: ".$totalDocumentosSaldo. " <br>(Verificar se o Saldo Inicial do Organismo começa em Jan/2016, se for o firebase deve ter <b>60 documentos</b>)";



    }else{
        echo "<br>===================================================";
        echo "<br><b>Avaliação: Id do oa passado como parâmetro não foi encontrado na base do soa!</b>";
    }


}else{
    echo "<br>===================================================";
    echo "<br><b>É preciso passar o id do organismo para acesso ao relatório de execução da cron!</b>";
}

echo "<br>===================================================";
echo "<br>Fim do script!";

echo "<br>Tempo- Fim da Execução: ".date("d/m/Y H:i:s");






