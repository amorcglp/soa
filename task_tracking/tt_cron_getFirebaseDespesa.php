<?php

//Para usar esse recurso passar: idOrganismoAfiliado e idSOA do Recebimento

include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

function UrlAtual(){
    $dominio= $_SERVER['HTTP_HOST'];
    $url = $dominio;
    return $url;
}

echo "<b>Cron: Verificação de Despesa no Firebase</b><br><br>";

echo "<br>Tempo 1 - Início da Execução da Cron: ".date("d/m/Y H:i:s");

require_once '../vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;

//Produção
if(UrlAtual()=="soa.amorc.org.br") {

    $projectId = "sistemadeorganismosafiliados";

}

//Teste
if(UrlAtual()=="treinamento.amorc.org.br") {
    $projectId = "soadesenvolvimento";
}

echo "<br>ProjectId no Firebase- ".$projectId;

// Create the Cloud Firestore client
$db = new FirestoreClient([
    'projectId' => $projectId
]);

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']:null;//Organismo alvo: 160

if(isset($idOrganismoAfiliado))
{
    $o = new organismo();

    //Verificar se id existe
    $arrExiste = $o->buscaIdOrganismo($idOrganismoAfiliado);
    //echo "<pre>";print_r($arrExiste);

    if(isset($arrExiste[0]['idOrganismoAfiliado']))
    {
        $arr = $o->retornaSiglaOa($idOrganismoAfiliado);
        $idOrganismoAfiliado = $arr['id'];
        echo "<br>Id do Oa Atualizado:" . $arr['id'];
        echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
        echo "<br>Sigla:" . $arr['sigla'];

        /*
         * Testes para ver se existe perda de informação para PR101
         */

        //Busca pelo id do Documento no Firebase

        $despesas = $db->collection("balances/" . $idOrganismoAfiliado . "/despesas");

        $queryWhereD = $despesas
            ->where('idDespesaSOA', '=', $_REQUEST['idSOA']);

        $queryDespesas = $queryWhereD->documents();

        $dataArray=array();

        //Percorrer recebimentos
        foreach ($queryDespesas as $documentDes) {
            if ($documentDes->exists()) {
                $idRefDocumentoFirebase = $documentDes->id();
                $dataArray = $documentDes->data();
            }
        }

        echo "<br>===================================================";
        echo "<br>Firebase:";
        echo "<br>Id no Firebase: ".$idRefDocumentoFirebase;
        echo "<br>Informações do documento no Firebase: <pre>"; print_r($dataArray);

    }else{
        echo "<br>===================================================";
        echo "<br><b>Avaliação: Id do oa passado como parâmetro não foi encontrado na base do soa!</b>";
    }


}else{
    echo "<br>===================================================";
    echo "<br><b>É preciso passar o id do organismo para acesso ao relatório de execução da cron!</b>";
}

echo "<br>===================================================";
echo "<br>Fim do script!";

echo "<br>Tempo- Fim da Execução: ".date("d/m/Y H:i:s");






