<?php



//echo "<br>Forçando 1 execução";
//exit;


//echo "<b>Cron: Envio de Recebimentos e Despesas para o Firebase</b><br><br>";

echo "<br>Tempo 1 - Início da Execução da Cron: ".date("d/m/Y H:i:s");

require_once '../vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;

$browser = isset($_REQUEST['browser'])? $_REQUEST['browser']: false;

if(!$browser)
{
    echo "<br>Rodando cron no server do Jupiter, não no browser";

    $dirName = dirname(__FILE__);
    $arrDirName = explode("/",$dirName);
    echo "Dir:<pre>";print_r($arrDirName);

    $play="localhost";
    if(in_array('treinamento',$arrDirName))
    {
        echo "<br>Rodando cron em Treinamento";
        $play="treinamento";
    }else{
        if(in_array('soa',$arrDirName)) {
            echo "<br>Rodando cron em Produção";
            $play="producao";
        }else{
            echo "<br>Rodando em Localhost...não é permitido rodar essa cron! Encerrando o script!";
            exit;
        }
    }

    $projectId = (strcmp(UrlAtual(), "producao")==0) ? "sistemadeorganismosafiliados" : "";
    $projectId = (strcmp(UrlAtual(), "treinamento")==0 or strcmp(UrlAtual(), "local")==0) ? "soadesenvolvimento" : "";

} else {

    echo "<br>Rodando cron no browser";

    function UrlAtual(){
        return $_SERVER['HTTP_HOST'];
    }

    $projectId = UrlAtual()=="soa.amorc.org.br" ? "sistemadeorganismosafiliados" : "";
    $projectId = (strcmp(UrlAtual(), "treinamento.amorc.org.br")==0 or strcmp(UrlAtual(), "soa.local")==0) ? "soadesenvolvimento" : "";
}


echo "<br>ProjectId no Firebase: " . $projectId;

//Create the Cloud Firestore client
$db = new FirestoreClient([
    'projectId' => $projectId
]);

echo "teste1";


include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : null;//Organismo alvo: 160

if(isset($idOrganismoAfiliado))
{
    echo "teste2";

    $o = new organismo();

    //Verificar se id existe
    $arrExiste = $o->buscaIdOrganismo($idOrganismoAfiliado);
    //echo "<pre>";print_r($arrExiste);

    if(isset($arrExiste[0]['idOrganismoAfiliado']))
    {
        $arr = $o->retornaSiglaOa($idOrganismoAfiliado);
        $idOrganismoAfiliado = $arr['id'];
        echo "<br>Id do Oa Atualizado:" . $arr['id'];
        echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
        echo "<br>Sigla:" . $arr['sigla'];

        /*
         * Testes para ver se existe perda de informação para PR101
         */

        $docRef7 = $db->collection("balances/".$idOrganismoAfiliado."/despesas");
        $query= $docRef7->documents();
        $totalDocumentosDespesas=0;

        foreach ($query as $d)
        {
            if ($d->exists()) {
                $totalDocumentosDespesas++;
            }
        }

        $docRef8 = $db->collection("balances/".$idOrganismoAfiliado."/recebimentos");
        $queryRec= $docRef8->documents();
        $totalDocumentosRecebimentos=0;

        foreach ($queryRec as $d2)
        {
            if ($d2->exists()) {
                $totalDocumentosRecebimentos++;
            }
        }

        echo "<br>===================================================";
        echo "<br>Firebase:";
        echo "<br>Total de documentos de despesas no Firebase: ".$totalDocumentosDespesas;
        echo "<br>Total de documentos de recebimentos no Firebase: ".$totalDocumentosRecebimentos;

        echo "<br>===================================================";
        echo "<br>SOA:";
        $r2 = new Recebimento();
        $d2 = new Despesa();
        $totalDespesas = $d2->getTotalRegistros($idOrganismoAfiliado);
        $totalRecebimentos = $r2->getTotalRegistros($idOrganismoAfiliado);
        echo "<br>Total de ids de despesas no SOA: ".$totalDespesas;
        echo "<br>Total de ids de recebimentos no SOA: ".$totalRecebimentos;

        //Teste
        if($totalDocumentosDespesas==$totalDespesas)
        {
            echo "<br>===================================================";
            echo "<br>Avaliação: Despesas <b>100% enviadas</b> para o Firebase!";
        }else{
            echo "<br>===================================================";
            echo "<br>Avaliação: Despesas <b>não enviadas corretamente</b> para o Firebase!";
        }
        if($totalDocumentosRecebimentos==$totalRecebimentos)
        {
            echo "<br>===================================================";
            echo "<br>Avaliação: Recebimentos <b>100% enviados</b> para o Firebase!";
        }else{
            echo "<br>===================================================";
            echo "<br>Avaliação: Recebimentos <b>não enviados corretamente</b> para o Firebase!";
        }
    }else{
        echo "<br>===================================================";
        echo "<br><b>Avaliação: Id do oa passado como parâmetro não foi encontrado na base do soa!</b>";
    }


}else{
    echo "<br>===================================================";
    echo "<br><b>É preciso passar o id do organismo para acesso ao relatório de execução da cron!</b>";
}

echo "<br>===================================================";
echo "<br>Fim do script!";

echo "<br>Tempo- Fim da Execução: ".date("d/m/Y H:i:s");






