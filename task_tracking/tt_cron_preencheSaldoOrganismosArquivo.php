<?php

/**
 * Created by PhpStorm.
 * User: braz
 * Date: 01/03/19
 * Time: 09:14
 * Script de Acompanhamento da execução do arquivo cron/preencheSaldoOrganismosArquivo.php
 * (extrato basico das operações da cron e relatório conforme o id passado do organismo)
 *
 * A idéia inicial desse script é mostrar quais arquivos txt foram criados na pasta 'saldo' do projeto
 * e qual a projeção e os saldos do organismo (passando por parâmetro o id do oa) para verificação no soa produção
 * se essas informações são reais.
 */

//Extrato básico das operações da cron - Lista de Arquivos Criados bem como Saldo Final

include_once('../lib/functions.php');

date_default_timezone_set('America/Sao_Paulo');

$path = "../saldo/";
$diretorio = dir($path);

echo "Lista de Arquivos do diretório '<strong>".$path."</strong>':<br /><br />";

$arr=array();

while($arquivo = $diretorio -> read()){

    $infoFile = pathinfo($arquivo);

    //echo "<pre>";print_r($infoFile);

    //Limpando a lista...
    if($infoFile['basename'] != "."&&$infoFile['basename'] != ".."&&$infoFile['basename'] != ".txt")
    {
        $filename = "../saldo/".$infoFile['basename'];

        if (file_exists($filename)) {
            $data = "- Data da Última Modificação do Arquivo: ". date ("d/m/Y H:i:s.", fileatime($filename));
        }else{
            echo "arquivo não existe:".$filename."<br>";
        }

        //Pegar saldo para mês e ano atual
        $arquivo = "../saldo/" . $infoFile['filename'] . ".txt";

        //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
        $fp = fopen($arquivo, "r");

        //Lê o conteúdo do arquivo aberto.
        while (!feof($fp)) {
            $valor = fgets($fp, 4096);

            $arr2 = explode("@", $valor);
        }

        //Percorrer array

        $saldoParaExibir="";
        $mesAtual = date('m');
        $anoAtual = date('Y');

        for ($i = 0; $i < count($arr2); $i++) {
            $mes = $arr2[$i];
            //echo $mes."<br>";
            $i++;
            $ano = $arr2[$i];
            //echo $ano."<br>";
            $i++;
            $aux = $i;
            $saldoAnterior = $arr2[$i];
            //echo $saldoAnterior."<br>";
            $i++;
            $totalEntradas = $arr2[$i];
            //echo $totalEntradas."<br>";
            $i++;
            $totalSaidas = $arr2[$i];
            //echo $totalSaidas."<br>";
            $i++;
            $saldoMes = $arr2[$i];
            //echo $saldoMes."<br>";

            if ($mes == $mesAtual && $ano == $anoAtual) {

                $saldoParaExibir = $saldoMes;

            }

        }

        $arr[] = "Arquivo: <a href='".$path.$arquivo."'>".$arquivo."</a> - ".retornaNomeCompletoOrganismoAfiliado($infoFile['filename'])." ".$data." - Saldo do mês e ano atual no arquivo: ".$saldoParaExibir."<br />";
    }
}

//Verificar se arr é vazio e ler array
if(count($arr)>0)
{
    //Ordernar array usando a Classificação da "ordem natural"
    natsort($arr);
    //Listar
    foreach ($arr as $k => $v)
    {
      echo $v;
    }
}else{
    echo "Pasta vazia";
}

$diretorio -> close();

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']:null;




