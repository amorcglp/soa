<?php
class Functions {
	
	private $errosGerais = array();
	
	// Pega todos os dados do site
	// Retorna um array apenas com os membros ativos
	public function getMembro($cpf) {
		$sql = "
			SELECT nom_client,des_email FROM CADASTRO as c
			inner join CADASTRO_FINANCEIRO as cf on cf.seq_cadast = c.seq_cadast
			inner join ROSACRUZ as r on r.seq_cadast_rosacr = c.seq_cadast
			where cf.dat_quitac_remess_atual is NULL
			and CONVERT(DATETIME,CONVERT(VARCHAR, r.dat_admiss ,112))>= '05/08/2014'
			and CONVERT(DATETIME,CONVERT(VARCHAR, r.dat_admiss ,112))<= '13/08/2014'
			and c.cod_cpf='".$cpf."'";
		//echo $sql."<br>";
		$arr = array();
		$arr['nome']=null;
		$arr['email']=null;

		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
			
				while ($dados = mssql_fetch_assoc($query)) {
					
					$arr['nome']	= $dados['nom_client'];
					$arr['email'] 	= $dados['des_email'];
				}
			}
		}
		return $arr;
	}
	

	
	// Insere um novo atendimento "CAMARA EXTERNA" para o cadastro do não membro
	private function insertOcorrenciaOgg($seqCadast) {
		// Busca o sequencial de atendimento	
		$sql = ('select Max(seq_atendi) as sequencia from Historico_Atend where num_ano_atendi = 2014');
		$query = mssql_query($sql);
		if ($query) {
			$dados = mssql_fetch_assoc($query);
			$sequencial = $dados['sequencia'] + 1;
		}
		else {
			$errosGerais[] = 'Ocorreu algum problema ao pegar o sequencial de atendimento do membro '.$seqCadast;
			return false;
		}
		
		$sql = ("insert into historico_atend (dat_hora_atendi, num_ano_atendi, seq_atendi, cod_usuari_atendi,dat_hora_atuali_atendi, ide_aberto, seq_cadast, seq_tipo_atendi)
				values (getdate(), '2014', '".$sequencial."', 'lucianob', getdate(), 'S', '".$seqCadast."', '7')");
		$query = mssql_query($sql);
		if ($query) {
			$sql = ("insert into historico_atend_ocorrencia 
					(num_ano_atendi, 
					seq_atendi, 
					seq_tipo_ocorre, 
					des_ocorre, 
					ide_tipo_membro, 
   															
					cod_setor, 
					sig_corres_padrao, 
					cod_usuari_atendi, 
					seq_lote, des_soluca, 
   															
					dat_emissa_corres, 
					ide_envio_caixa_postal, 
					dat_emissa_lote, 
					num_ano_movime_financ, 
   															
					num_movime_financ,
					
					ide_penden, 
					ide_urgent, 
					dat_encami, 
					dat_hora_atendi_ocorre
					)
					values  ('2014', 
					'".$sequencial."', 
					'79', 
					'Acerto migração lotes novos OGG', 
					'2', 
					'1', 
					'ces', 
					NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 'S', 'N', NULL, NULL)");
			$query = mssql_query($sql);
			if ($query) {
				return true;
			}
			else {
				$errosGerais[] = 'Ocorreu algum problema ao cadastrar o atendimento do membro '.$seqCadast;
				return false;
			}
		}
		else {
			$errosGerais[] = 'Ocorreu algum problema ao cadastrar o atendimento do membro '.$seqCadast;
			return false;
		}
	}
	
	// Insere atend de ogg
	public function cadastrarOcorrenciaOgg($seq_cadast) {
		$this->insertOcorrenciaOgg($seq_cadast);
	}
	
	public function print_relatorios() {
		echo '<h1>Erros gerais</h1>';
		echo '<pre>';	
			print_r($this->errosGerais);
		echo '</pre>';
	}
	
}
?>