-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 07/02/2018 às 16:23
-- Versão do servidor: 10.0.33-MariaDB-1~xenial
-- Versão do PHP: 5.6.32-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `soa`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tabelaContribuicao`
--

CREATE TABLE `tabelaContribuicao` (
  `idTabelaContribuicao` int(10) NOT NULL,
  `tipo` int(10) NOT NULL,
  `modalidadeMensalIndividual` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeTrimestralIndividualReal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeTrimestralIndividualDolar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeTrimestralIndividualEuro` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeAnualIndividualReal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeAnualIndividualCFD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeMensalDual` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeTrimestralDualReal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeTrimestralDualDolar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeTrimestralDualEuro` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeAnualDualReal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modalidadeAnualDualCFD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `taxaInscricaoReal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `taxaInscricaoDolar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `taxaInscricaoEuro` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` int(10) NOT NULL,
  `usuarioAlteracao` int(10) NOT NULL,
  `data` datetime NOT NULL,
  `dataAlteracao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `tabelaContribuicao`
--

INSERT INTO `tabelaContribuicao` (`idTabelaContribuicao`, `tipo`, `modalidadeMensalIndividual`, `modalidadeTrimestralIndividualReal`, `modalidadeTrimestralIndividualDolar`, `modalidadeTrimestralIndividualEuro`, `modalidadeAnualIndividualReal`, `modalidadeAnualIndividualCFD`, `modalidadeMensalDual`, `modalidadeTrimestralDualReal`, `modalidadeTrimestralDualDolar`, `modalidadeTrimestralDualEuro`, `modalidadeAnualDualReal`, `modalidadeAnualDualCFD`, `taxaInscricaoReal`, `taxaInscricaoDolar`, `taxaInscricaoEuro`, `usuario`, `usuarioAlteracao`, `data`, `dataAlteracao`) VALUES
(1, 1, '51', '153', '60', '45', '530', '0', '60', '180', '69', '51', '625', '0', '40', '15', '15', 550110, 550110, '2018-02-06 18:52:33', '2018-02-06 19:20:19'),
(2, 2, '41', '123', '48', '36', '435', '0', '43', '129', '51', '39', '446', '0', '0', '0', '0', 550110, 0, '2018-02-06 19:21:46', '0000-00-00 00:00:00'),
(3, 3, '58', '174', '72', '54', '615', '0', '64', '192', '81', '57', '670', '0', '40', '15', '15', 550110, 0, '2018-02-06 19:23:05', '0000-00-00 00:00:00'),
(4, 4, '39', '117', '45', '36', '400', '53', '47', '141', '54', '42', '485', '85', '35', '18', '14', 550110, 0, '2018-02-06 19:24:57', '0000-00-00 00:00:00'),
(5, 5, '13', '39', '16', '13', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 550110, 0, '2018-02-06 19:26:23', '0000-00-00 00:00:00'),
(6, 6, '11', '33', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 550110, 550110, '2018-02-07 10:17:29', '2018-02-07 10:18:02'),
(7, 7, '16', '48', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 550110, 0, '2018-02-07 10:18:51', '0000-00-00 00:00:00');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `tabelaContribuicao`
--
ALTER TABLE `tabelaContribuicao`
  ADD PRIMARY KEY (`idTabelaContribuicao`),
  ADD UNIQUE KEY `idTabelaContribuicao` (`idTabelaContribuicao`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `tabelaContribuicao`
--
ALTER TABLE `tabelaContribuicao`
  MODIFY `idTabelaContribuicao` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
