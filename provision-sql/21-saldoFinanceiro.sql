-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 28-Set-2018 às 16:31
-- Versão do servidor: 10.0.33-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soa_treinamento`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `saldoFinanceiro`
--

CREATE TABLE IF NOT EXISTS `saldoFinanceiro` (
  `idSaldoFinanceiro` int(10) NOT NULL,
  `mes` int(10) NOT NULL,
  `ano` int(10) NOT NULL,
  `fk_idOrganismoAfiliado` int(10) NOT NULL,
  `saldoAnterior` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `totalEntradas` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `totalSaidas` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `saldoMes` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `saldoFinanceiro`
--
ALTER TABLE `saldoFinanceiro`
  ADD PRIMARY KEY (`idSaldoFinanceiro`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `saldoFinanceiro`
--
ALTER TABLE `saldoFinanceiro`
  MODIFY `idSaldoFinanceiro` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
