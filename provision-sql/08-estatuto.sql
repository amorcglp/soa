-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 08/11/2017 às 11:05
-- Versão do servidor: 10.0.31-MariaDB-1~xenial
-- Versão do PHP: 5.6.31-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `soa`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `estatuto`
--

CREATE TABLE `estatuto` (
  `idEstatuto` int(10) NOT NULL,
  `fk_idOrganismoAfiliado` int(10) NOT NULL,
  `numeroOficio` int(10) NOT NULL,
  `cidadeOficio` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ufOficio` int(2) NOT NULL,
  `cartorioPronaos` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `comarcaPronaos` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `numeroPronaos` int(10) NOT NULL,
  `numeroLeiMunicipalPronaos` int(10) NOT NULL,
  `dataLeiMunicipalPronaos` date NOT NULL,
  `numeroLeiEstadualPronaos` int(10) NOT NULL,
  `dataLeiEstadualPronaos` date NOT NULL,
  `cartorioCapitulo` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `comarcaCapitulo` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `numeroCapitulo` int(10) NOT NULL,
  `numeroLeiMunicipalCapitulo` int(10) NOT NULL,
  `dataLeiMunicipalCapitulo` date NOT NULL,
  `numeroLeiEstadualCapitulo` int(10) NOT NULL,
  `dataLeiEstadualCapitulo` date NOT NULL,
  `cartorioLoja` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `comarcaLoja` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `numeroLoja` int(10) NOT NULL,
  `numeroLeiMunicipalLoja` int(10) NOT NULL,
  `dataLeiMunicipalLoja` date NOT NULL,
  `numeroLeiEstadualLoja` int(10) NOT NULL,
  `dataLeiEstadualLoja` date NOT NULL,
  `dataAssembleiaGeral` date NOT NULL,
  `usuario` int(10) NOT NULL,
  `ultimoAtualizar` int(10) NOT NULL,
  `dataCadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `estatuto`
--
ALTER TABLE `estatuto`
  ADD PRIMARY KEY (`idEstatuto`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `estatuto`
--
ALTER TABLE `estatuto`
  MODIFY `idEstatuto` int(10) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
