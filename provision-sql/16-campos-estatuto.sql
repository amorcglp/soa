ALTER TABLE `estatuto` ADD `numeroAverbacaoPronaos` VARCHAR(10) NOT NULL AFTER `numeroPronaos`;

ALTER TABLE `estatuto` ADD `numeroAverbacaoCapitulo` VARCHAR(10) NOT NULL AFTER `numeroCapitulo`;

ALTER TABLE `estatuto` ADD `numeroAverbacaoLoja` VARCHAR(10) NOT NULL AFTER `numeroLoja`;
