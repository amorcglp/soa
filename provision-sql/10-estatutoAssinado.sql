-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 14/11/2017 às 11:29
-- Versão do servidor: 10.0.31-MariaDB-1~xenial
-- Versão do PHP: 5.6.31-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `soa`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `estatutoAssinado`
--

CREATE TABLE `estatutoAssinado` (
  `idEstatutoAssinado` int(10) NOT NULL,
  `fk_idEstatuto` int(10) NOT NULL,
  `caminho` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dataCadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `estatutoAssinado`
--
ALTER TABLE `estatutoAssinado`
  ADD PRIMARY KEY (`idEstatutoAssinado`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `estatutoAssinado`
--
ALTER TABLE `estatutoAssinado`
  MODIFY `idEstatutoAssinado` int(10) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
