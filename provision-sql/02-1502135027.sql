ALTER TABLE `usuario` CHANGE `senhaCriptografada` `senhaCriptografada` INT(1) NULL DEFAULT NULL;

ALTER TABLE `usuario` CHANGE `emailUsuario` `emailUsuario` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `usuario` CHANGE `telefoneResidencialUsuario` `telefoneResidencialUsuario` VARCHAR(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `usuario` CHANGE `telefoneComercialUsuario` `telefoneComercialUsuario` VARCHAR(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `usuario` CHANGE `celularUsuario` `celularUsuario` VARCHAR(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
