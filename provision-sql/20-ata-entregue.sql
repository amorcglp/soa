ALTER TABLE `ataReuniaoMensal` ADD `entregue` INT(1) NOT NULL AFTER `numeroAssinatura`;
ALTER TABLE `ataReuniaoMensal` CHANGE `entregue` `entregue` INT(1) NOT NULL DEFAULT '0';
ALTER TABLE `ataReuniaoMensal` ADD `dataEntrega` DATETIME NOT NULL AFTER `entregue`;
ALTER TABLE `ataReuniaoMensal` ADD `quemEntregou` INT(10) NOT NULL AFTER `dataEntrega`;
