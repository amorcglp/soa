-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 11/08/2017 às 13:47
-- Versão do servidor: 10.0.31-MariaDB-1~xenial
-- Versão do PHP: 5.6.31-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `soa`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `auxiliarWeb`
--

CREATE TABLE `auxiliarWeb` (
  `idAuxiliarWeb` int(10) NOT NULL,
  `fk_idOrganismoAfiliado` int(10) NOT NULL,
  `dataTermoCompromisso` date NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigoAfiliacao` int(10) NOT NULL,
  `cpf` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `logradouro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numero` int(10) NOT NULL,
  `complemento` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `pais` int(1) NOT NULL,
  `telefoneResidencial` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `telefoneComercial` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `celular` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` int(10) NOT NULL,
  `ultimoAtualizar` int(10) NOT NULL,
  `dataCadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `auxiliarWeb`
--
ALTER TABLE `auxiliarWeb`
  ADD PRIMARY KEY (`idAuxiliarWeb`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `auxiliarWeb`
--
ALTER TABLE `auxiliarWeb`
  MODIFY `idAuxiliarWeb` int(10) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
