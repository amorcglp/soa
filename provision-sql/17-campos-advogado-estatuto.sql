ALTER TABLE `estatuto` ADD `nomeAdvogado` VARCHAR(255) NOT NULL AFTER `numeroPresentes`, ADD `numeroOAB` VARCHAR(20) NOT NULL AFTER `nomeAdvogado`;

ALTER TABLE `estatuto` CHANGE `ufOficio` `ufOficio` VARCHAR(2) NOT NULL;
