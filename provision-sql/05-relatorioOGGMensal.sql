-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 26-Jul-2017 às 16:50
-- Versão do servidor: 10.2.6-MariaDB-10.2.6+maria~xenial-log
-- PHP Version: 5.6.30-12~ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tom_soa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `relatorioOGGMensal`
--

CREATE TABLE `relatorioOGGMensal` (
  `idRelatorioOGGMensal` int(10) NOT NULL,
  `fk_idOrganismoAfiliado` int(10) NOT NULL,
  `mes` int(10) NOT NULL,
  `ano` int(10) NOT NULL,
  `caminhoRelatorioOGGMensal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` int(10) NOT NULL,
  `dataCadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `relatorioOGGMensal`
--
ALTER TABLE `relatorioOGGMensal`
  ADD PRIMARY KEY (`idRelatorioOGGMensal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `relatorioOGGMensal`
--
ALTER TABLE `relatorioOGGMensal`
  MODIFY `idRelatorioOGGMensal` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
