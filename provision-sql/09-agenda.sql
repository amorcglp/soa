ALTER TABLE `agendaAtividade` ADD `statusAgenda` INT(1) NOT NULL AFTER `fk_idPublico`;

ALTER TABLE `agendaAtividade` ADD `ultimoAtualizar` INT(10) NOT NULL AFTER `statusAgenda`;
