-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 08-Mar-2019 às 16:09
-- Versão do servidor: 10.0.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soa_treinamento`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `emailLog`
--

CREATE TABLE IF NOT EXISTS `emailLog` (
  `idEmailLog` int(10) NOT NULL,
  `domainOrigin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailFrom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailTo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` int(255) NOT NULL,
  `html` longtext COLLATE utf8_unicode_ci NOT NULL,
  `domainEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seqCadast` int(10) NOT NULL,
  `codigoRetorno` int(10) NOT NULL,
  `msgRetorno` longtext COLLATE utf8_unicode_ci NOT NULL,
  `tipoEvento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricaoEvento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jsonEvento` longtext COLLATE utf8_unicode_ci NOT NULL,
  `funcionalidadeNoSistema` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `api` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dataHora` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emailLog`
--
ALTER TABLE `emailLog`
  ADD PRIMARY KEY (`idEmailLog`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emailLog`
--
ALTER TABLE `emailLog`
  MODIFY `idEmailLog` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
