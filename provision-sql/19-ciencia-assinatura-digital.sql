ALTER TABLE `usuario` ADD `cienteAssinaturaDigital` INT(1) NOT NULL AFTER `avisoFacebook`;

ALTER TABLE `usuario` ADD `envioEmailAssinatura` INT(1) NOT NULL AFTER `cienteAssinaturaDigital`;

ALTER TABLE `ataReuniaoMensal` ADD `numeroAssinatura` VARCHAR(15) NOT NULL AFTER `data`;
