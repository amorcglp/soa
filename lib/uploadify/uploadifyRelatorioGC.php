<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
include_once('../connect/connect.php');
$idPergunta 	= isset($_POST['idPergunta']) ? addslashes($_POST['idPergunta']) : '';
$fkusuario		= isset($_POST['fkusuario']) ? addslashes($_POST['fkusuario']) : '';
$fkrelatorio	= isset($_POST['fkrelatorio']) ? addslashes($_POST['fkrelatorio']) : '';

// Define a destination
$targetFolder = 'soa/controle_anexo/relatorio_gc/'; // Relative to the root
if (!empty($_FILES)) {

	// Validate the file type
	$fileTypes = array('pdf','jpg','jpeg','png','gif'); // File extensions
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	$fileParts['extension'] = strtolower($fileParts['extension']);

	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
	$newName = md5(uniqid(time())) . "." . $fileParts['extension'];
	$targetFile = rtrim($targetPath,'/') . '/' . $newName;
	$pathBd = 'soa/controle_anexo/relatorio_gc/' . $newName;
	
	
	if (in_array($fileParts['extension'],$fileTypes)) {
		if (move_uploaded_file($tempFile,$targetFile)) {
			$sql = ("INSERT INTO relatorio_gc_anexo (fkrelatorio_gc,
				fkrelatorio_gc_pergunta,
				fkusuario,
				descricao, 
				nome_original, 
				nome_arquivo, 
				ext, 
				full_path, 
				data,
				excluido) 
			VALUES (
				'".$fkrelatorio."',
				'".$idPergunta."',
				'".$fkusuario."',
				'', 
				'".$_FILES['Filedata']['name']."', 
				'".$newName."',
				'".$fileParts['extension']."', 
				'".$pathBd."', 
				NOW(),
				'0');");
			//echo $sql;
			$query = mysql_query($sql);
			if ($query) {
				echo '1';		
			}
			else {
				unlink($targetFile);
				echo 'Ocorreu algum erro ao inserir o anexo. Tente novamente ou entre em contato com o setor de TI';
			}
			
		};
	} else {
		echo 'Tipos de arquivo suportados (PDF, JPG, JPEG, PNG OU GIF). Em caso de duvidas entre em contato com o setor de TI';
	}
	
}
else {
	echo 'O arquivo que você está tentando enviar é muito grande ou o servidor não possuí mais espaço disponível. Entre em contato com o setor de TI.';	
}

?>