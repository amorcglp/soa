<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
*/
include_once('../connect/connect.php');

$fkDocumento 		= isset($_POST['idDocumento']) ? addslashes($_POST['idDocumento']) : '';
$fkgestao_imovel	= isset($_POST['idControle']) ? addslashes($_POST['idControle']) : '';
$fkTurno			= isset($_POST['fkturno']) ? addslashes($_POST['fkturno']) : '';
$fkOa				= isset($_POST['fkOa']) ? addslashes($_POST['fkOa']) : '';
$fkImovel			= isset($_POST['fkImovel']) ? addslashes($_POST['fkImovel']) : '';
$titulo				= isset($_POST['titulo']) ? addslashes($_POST['titulo']) : '';
$descricao			= isset($_POST['descricao']) ? addslashes($_POST['descricao']) : '';

//echo $fkDocumento."<br>";

// Define a destination
$targetFolder = 'soa/controle_anexo/gestao_imovel_anexo/'; // Relative to the root
if (!empty($_FILES)) {

	// Validate the file type
	$fileTypes = array('pdf','jpg','jpeg','png','gif'); // File extensions
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	$fileParts['extension'] = strtolower($fileParts['extension']);

	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
	$newName = md5(uniqid(time())) . "." . $fileParts['extension'];
	$targetFile = rtrim($targetPath,'/') . '/' . $newName;
	$pathBd = 'soa/controle_anexo/gestao_imovel_anexo/' . $newName;
	
	
	if (in_array($fileParts['extension'],$fileTypes)) {
		if (move_uploaded_file($tempFile,$targetFile)) {
			$sql = ("INSERT INTO gestao_imovel_anexo (fkgestao_imovel_controle,
				fkdocumento,
				fkoa,
				fkimovel,
				fkturno,
				titulo,
				descricao, 
				nome_original, 
				nome_arquivo, 
				ext, 
				full_path, 
				data,
				excluido) 
			VALUES (
				'".$fkgestao_imovel."',
				'".$fkDocumento."',
				'".$fkOa."',
				'".$fkImovel."',
				'".$fkTurno."',
				'".$titulo."',
				'".$descricao."',
				'".$_FILES['Filedata']['name']."', 
				'".$newName."',
				'".$fileParts['extension']."', 
				'".$pathBd."', 
				NOW(),
				'1');");
			//echo $sql."<br>";
			$query = mysql_query($sql);
			if ($query) {
				echo '1';		
			}
			else {
				unlink($targetFile);
				echo 'Ocorreu algum erro ao inserir o anexo. Tente novamente ou entre em contato com o setor de TI';
			}
			
		};
	} else {
		echo 'Tipos de arquivo suportados (PDF, JPG, JPEG, PNG OU GIF). Em caso de duvidas entre em contato com o setor de TI';
	}
	
}
else {
	echo 'O arquivo que você está tentando enviar é muito grande ou o servidor não possuí mais espaço disponível. Entre em contato com o setor de TI.';	
}

?>