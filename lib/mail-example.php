<?php
# Include the Autoloader (see "Libraries" for install instructions)
require '../vendor/autoload.php';
use Mailgun\Mailgun;



# Instantiate the client.
$mgClient = new Mailgun('key-24e5c1eb843300db6922fee30623bcf0');
/** Official */
//$domain = "mg.amorc.org.br";
/** Sandbox */
$domain = "sandbox622f068a94064201a9d4021ad9bd9042.mailgun.org";

# Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
   		'from'    => 'TI <ti@amorc.org.br>',
		'to'      => 'TI <ti@amorc.org.br>',
		'subject' => 'Hello TI',
		'text'    => 'Congratulations TI, you just sent an email with Mailgun!  You are truly awesome!  You can see a record of this email in your logs: https://mailgun.com/cp/log .  You can send up to 300 emails/day from this sandbox server.  Next, you should add your own domain so you can send 10,000 emails/month for free.'));