<?php

if(realpath('../../model/wsClass.php')){
	require_once '../../model/wsClass.php';
}else{
	if(realpath('../model/wsClass.php')){
		require_once '../model/wsClass.php';	
	}else{
		require_once './model/wsClass.php';
	}
}

function aleatorio(){
	$novo_valor= "";
	$valor = "0123456789";
	srand((double)microtime()*1000000);
	for ($i=0; $i<6; $i++){
        $novo_valor.= $valor[rand()%strlen($valor)];
	}
	return $novo_valor;
}

function inverte_data($data) {
	if ($data != '') {
		$data2 = explode('/', $data);
		if (count($data2) == 3) {
			$newData = $data2[2].'/'.$data2[1].'/'.$data2[0];
			return $newData;
		}
	}
	else {
		return '0000-00-00';
	}
};

/*
function UrlAtual(){
 $dominio= $_SERVER['HTTP_HOST'];
 $url = $dominio;
 return $url;
}
*/

function validaCPF($cpf = null) {

    // Verifica se um n?mero foi informado
    if(empty($cpf)) {
        return false;
    }

    // Elimina possivel mascara
    $cpf = preg_replace('/[^0-9]/', '', $cpf);
    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

    // Verifica se o numero de digitos informados ? igual a 11
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se nenhuma das sequ?ncias invalidas abaixo
    // foi digitada. Caso afirmativo, retorna falso
    else if ($cpf == '00000000000' ||
        $cpf == '11111111111' ||
        $cpf == '22222222222' ||
        $cpf == '33333333333' ||
        $cpf == '44444444444' ||
        $cpf == '55555555555' ||
        $cpf == '66666666666' ||
        $cpf == '77777777777' ||
        $cpf == '88888888888' ||
        $cpf == '99999999999') {
        return false;
        // Calcula os digitos verificadores para verificar se o
        // CPF ? v?lido
    } else {

        for ($t = 9; $t < 11; $t++) {

            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }
}

function retornaDDD($ddd1,$ddd2,$ddd3)
{
	if($ddd1!="000")
		return $ddd1;
	if($ddd2!="000")
		return $ddd2;
	if($ddd3!="000")
		return $ddd3;
}

function retornaTel($tel1,$tel2,$tel3)
{
	if($tel1!="000000000")
		return $tel1;
	if($tel2!="000000000")
		return $tel2;
	if($tel3!="000000000")
		return $tel3;
}

function retornaTotalTel($tel1,$tel2,$tel3)
{
	if($tel1!="000000000")
		return $tel1;
	if($tel2!="000000000")
		return $tel2;
	if($tel3!="000000000")
		return $tel3;
}

function perfilAmorc($perfil)
{
	switch ($perfil)
	{
		case 1:
			return "Rosacruz";
			break;
		case 2:
			return "TOM";
			break;
		case 3:
			return "Amigo da Amorc";
			break;
		case 4:
			return "Não membro";
			break;
	}
}


function tiraacento($txt) {

	$r=iconv("UTF-8", "ASCII//TRANSLIT", $txt);

	$r=str_replace(array('"',"'",'~','^','&','?','$','.',','),'',$r);

    return $r;

}

function efetuouPagamento($cod)
{
	if($cod==0)
	{
		return "Não";
	}else{
		return "Sim";
	}
}

function formaPagamento($cod,$fkpais)
{
	if($cod==1)
	{
		return "Depósito Bancário/Cheque/Espécie";
	}
	if($cod==2)
	{
		return "Pagseguro";
	}
	if($fkpais!=88)
	{
		return "Paypall";
	}
}

function situacaoInscricao($cod)
{
	if($cod==0)
	{
		return "Não confirmado";
	}else{
		if($cod==1)
		{
			return "Confirmado";
		}else{
			if($cod==2)
			{
				return "Cancelado";
			}
		}
	}
}

function simNao($cod,$maiusculo=null)
{
	if($maiusculo==null){
		if($cod==0)
		{
			return "Não";
		}else{
			return "Sim";
		}
	}
	if($maiusculo){
		if($cod==0)
		{
			return "NÃO";
		}else{
			return "SIM";
		}
	}
}

function comoConheceuAmorc($cod)
{
	switch ($cod)
	{
		case 1:
			return "Pesquisa na Internet";
			break;
		case 2:
			return "Facebook";
			break;
		case 3:
			return "Indicação de um amigo ou familiar";
			break;
		case 4:
			return "Revistas, jornais";
			break;
		case 5:
			return "Outro";
			break;
	}
}

function estarEmSilencio($cod)
{
	switch ($cod)
	{
		case 1:
			return "Tranquilo";
			break;
		case 2:
			return "Preocupado";
			break;
		case 3:
			return "Não vou conseguir";
			break;
		case 4:
			return "Não sei";
			break;
	}
}

function ideiaFazOrdem($cod)
{
    switch ($cod)
	{
		case 1:
			return "Fraternidade";
			break;
		case 2:
			return "Um tipo de religião";
			break;
		case 3:
			return "Uma escola esotérica";
			break;
		case 4:
			return "Seita";
			break;
		case 5:
			return "Semelhante a maçonaria";
			break;
		case 6:
			return "Filosofia";
			break;
		case 7:
			return "Semelhante ao espiritismo";
			break;
	}
}

function meioTransporte($meio)
{
	switch ($meio)
	{
		case 1:
			return "Carro próprio";
			break;
		case 2:
			return "Avião";
			break;
		case 3:
			return "Ônibus";
			break;
	}
}

function tipoDual($cod)
{
	switch ($cod)
	{
		case 0:
			return "Não informado";
			break;
		case 1:
			return "Titular";
			break;
		case 2:
			return "Co-titular";
			break;
	}
}

function sexo($cod)
{
	switch ($cod)
	{
		case 0:
			return "Não informado";
			break;
		case 1:
			return "Masculino";
			break;
		case 2:
			return "Feminino";
			break;
	}
}

function geraTimestamp($data) {

	$partes = explode('-', $data);

	return mktime(0, 0, 0, $partes[1], $partes[2], $partes[0]);

}

function getDiferencaDatas($data_inicial,$data_final)
{
	// Usa a função criada e pega o timestamp das duas datas:
	$time_inicial = geraTimestamp($data_inicial);
	$time_final = geraTimestamp($data_final);

	// Calcula a diferença de segundos entre as duas datas:
	$diferenca = $time_final - $time_inicial; // 19522800 segundos

	// Calcula a diferença de dias
	$dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias

	return $dias;
}

function sexoReduzido($cod)
{
	switch ($cod)
	{
		case 0:
			return "*";
			break;
		case 1:
			return "M";
			break;
		case 2:
			return "F";
			break;
	}
}


function getIdade($aniversario, $curr = 'now', $desc=null) {

	$complemento = "";

	if($aniversario=="0000-00-00")
	{
		return "Não informada";
	}

	if($desc==null)
	{
		$complemento = " anos";
	}

    $year_curr = date("Y", strtotime($curr));

    $days = !($year_curr % 4) || !($year_curr % 400) & ($year_curr % 100) ? 366: 355;

    list($y, $m, $d ) = explode('-', $aniversario);

    return floor(((strtotime($curr) - mktime(0, 0, 0, $m, $d, $y)) / 86400) / $days).$complemento;

}

function modalidadeCurso($cod)
{
	switch ($cod)
	{
		case 1:
			return "Presencial";
			break;
		case 2:
			return "Semi-presencial";
			break;
		case 3:
			return "À distância";
			break;
		default:
			return "Presencial";
			break;
	}
}

function removeAcentos($var) {
    
    if(is_string($var))
    {    
        $string = strtolower($var);

        if ( !preg_match('/[\x80-\xff]/', $string) )
            return $string;

        if (seems_utf8($string)) {
            $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
            chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
            chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
            chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
            chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
            chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
            chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
            chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
            chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
            chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
            chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
            chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
            chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
            chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
            chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
            chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
            chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
            chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
            chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
            chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
            chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
            chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
            chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
            chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
            chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
            chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
            chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
            chr(195).chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
            chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
            chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
            chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
            chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
            chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
            chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
            chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
            chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
            chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
            chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
            chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
            chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
            chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
            chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
            chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
            chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
            chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
            chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
            chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
            chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
            chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
            chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
            chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
            chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
            chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
            chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
            chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
            chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
            chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
            chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
            chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
            chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
            chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
            chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
            chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
            chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
            chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
            chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
            chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
            chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
            chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
            chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
            chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
            chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
            chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
            chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
            chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
            chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
            chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
            chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
            chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
            chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
            chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
            chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
            chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
            chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
            chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
            chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
            chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
            chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
            chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
            chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
            chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
            // Euro Sign
            chr(226).chr(130).chr(172) => 'E',
            // GBP (Pound) Sign
            chr(194).chr(163) => '');

            $string = strtr($string, $chars);
        } else {
            // Assume ISO-8859-1 if not UTF-8
            $chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
                    .chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
                    .chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
                    .chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
                    .chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
                    .chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
                    .chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
                    .chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
                    .chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
                    .chr(252).chr(253).chr(255);

            $chars['out'] = "EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy";

            $string = strtr($string, $chars['in'], $chars['out']);
            $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254));
            $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th');
            $string = str_replace($double_chars['in'], $double_chars['out'], $string);
        }
    }else{
        $string = $var;
    }    
    return $string;
}

function seems_utf8($str) {
    $length = strlen($str);
    for ($i=0; $i < $length; $i++) {
        $c = ord($str[$i]);
        if ($c < 0x80) $n = 0; # 0bbbbbbb
        elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
        elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
        elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
        elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
        elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
        else return false; # Does not match any model
        for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
            if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                return false;
        }
    }
    return true;
}
function utf8_uri_encode( $utf8_string, $length = 0 ) {
    $unicode = '';
    $values = array();
    $num_octets = 1;
    $unicode_length = 0;

    $string_length = strlen( $utf8_string );
    for ($i = 0; $i < $string_length; $i++ ) {

        $value = ord( $utf8_string[ $i ] );

        if ( $value < 128 ) {
            if ( $length && ( $unicode_length >= $length ) )
                break;
            $unicode .= chr($value);
            $unicode_length++;
        } else {
            if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

            $values[] = $value;

            if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
                break;
            if ( count( $values ) == $num_octets ) {
                if ($num_octets == 3) {
                    $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
                    $unicode_length += 9;
                } else {
                    $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
                    $unicode_length += 6;
                }

                $values = array();
                $num_octets = 1;
            }
        }
    }

    return $unicode;
}
function sanitize_title_with_dashes($title) {
    $title = strip_tags($title);
    // Preserve escaped octets.
    $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
    // Remove percent signs that are not part of an octet.
    $title = str_replace('%', '', $title);
    // Restore octets.
    $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

    $title = remove_accents($title);
    if (seems_utf8($title)) {
        if (function_exists('mb_strtolower')) {
            $title = mb_strtolower($title, 'UTF-8');
        }
        $title = utf8_uri_encode($title, 200);
    }

    $title = strtolower($title);
    $title = preg_replace('/&.+?;/', '', $title); // kill entities
    $title = str_replace('.', '-', $title);
    $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
    $title = preg_replace('/\s+/', '-', $title);
    $title = preg_replace('|-+|', '-', $title);
    $title = trim($title, '-');

    return $title;
}

function retornaImagemAnexo($ext)
{
	$img = '';

	switch($ext) {

        case 'jpg':
            $img = 'img/icon_anexo/jpg.png';
			break;

        case 'jpeg':
            $img = 'img/icon_anexo/jpg.png';
			break;

        case 'png':
            $img = 'img/icon_anexo/png.png';
			break;

        case 'pdf':
            $img = 'img/icon_anexo/pdf.png';
			break;

        default:
            $img = 'img/icon_anexo/archive.png';
			break;
	}

	return $img;
}

function periodo($periodo)
{
	$retorno = '';

	switch($periodo) {

        case 1:
            $retorno = 'Semanal';
			break;

        case 2:
            $retorno = 'Quinzenal';
			break;

        case 3:
            $retorno = 'Mensal';
			break;

        case 4:
            $retorno = 'Bimestral';
			break;

        case 5:
            $retorno = 'Trimestral';
			break;

        case 6:
            $retorno = 'Quadrimestral';
			break;

        case 7:
            $retorno = 'Semestral';
			break;

        case 8:
            $retorno = 'Anual';
			break;

        case 9:
            $retorno = 'Bienal';
			break;

        case 10:
            $retorno = 'Trienal';
			break;

        case 11:
            $retorno = 'Quadrienal';
			break;

        case 12:
            $retorno = 'Quinquenal';
			break;

        default:
            $retorno = 'Semanal';
			break;
	}

	return $retorno;
}

function periodoDias($periodo)
{
	$retorno = '';

	switch($periodo) {

        case 1:
            $retorno = 5;
            break;


        case 2:
            $retorno = 10;
            break;


        case 3:
            $retorno = 20;
            break;


        case 4:
            $retorno = 40;
            break;


        case 5:
            $retorno = 60;
            break;


        case 6:
            $retorno = 80;
            break;


        case 7:
            $retorno = 120;
            break;


        case 8:
            $retorno = 240;
            break;


        case 9:
            $retorno = 480;
            break;


        case 10:
            $retorno = 720;
            break;


        case 11:
            $retorno = 960;
            break;


        case 12:
            $retorno = 1200;
            break;

        default:
            $retorno = 5;
			break;
	}

	return $retorno;
}

/**
 * Fun??o para calcular o pr?ximo dia ?til de uma data
 * Formato de entrada da $data: AAAA-MM-DD
 */

function proximoDiaUtil($data, $saida = 'd/m/Y') {

	// Converte $data em um UNIX TIMESTAMP
	$timestamp = strtotime($data);

	// Calcula qual o dia da semana de $data
	// O resultado ser? um valor num?rico:
	// 1 -> Segunda ... 7 -> Domingo
	$dia = date('N', $timestamp);

	// Se for s?bado (6) ou domingo (7), calcula a pr?xima segunda-feira
	if ($dia >= 6) {
		$timestamp_final = $timestamp + ((8 - $dia) * 3600 * 24);
	} else {
        // N?o ? s?bado nem domingo, mant?m a data de entrada mais um dia
		$timestamp_final = $timestamp + (3600 * 24);
	}

	return date($saida, $timestamp_final);

}

/**
 * Fun??o para calcular o ultimo dia ?til de uma data
 * Formato de entrada da $data: AAAA-MM-DD
 */

function ultimoDiaUtil($data, $saida = 'd/m/Y') {

	// Converte $data em um UNIX TIMESTAMP
	$timestamp = strtotime($data);

	// Calcula qual o dia da semana de $data
	// O resultado ser? um valor num?rico:
	// 1 -> Segunda ... 7 -> Domingo
	$dia = date('N', $timestamp);

	// Se for s?bado (6) ou domingo (7), calcula a pr?xima segunda-feira
	if ($dia >= 6) {
		if($dia==6)
		{
			$timestamp_final = $timestamp - (1 * 3600 * 24);
		}
		if($dia==7)
		{
			$timestamp_final = $timestamp - (2 * 3600 * 24);
		}
	} else {
		// N?o ? s?bado nem domingo, mant?m a data de entrada menos um dia
		$timestamp_final = $timestamp - (3600 * 24);
	}

	return date($saida, $timestamp_final);

}

/**
 * Fun??o para calcular o dia a uma semana atr?s
 * Formato de entrada da $data: AAAA-MM-DD
 */

function semanaPassada($data, $saida = 'd/m/Y') {

	// Converte $data em um UNIX TIMESTAMP
	$timestamp = strtotime($data);

	$timestamp_final = $timestamp - (7 * 3600 * 24);

	return date($saida, $timestamp_final);

}

function mesExtensoPortugues($mes)
{
	switch ($mes)
	{
		case 1:
			return "Janeiro";
			break;
		case 2:
			return "Fevereiro";
			break;
		case 3:
			return "Março";
			break;
		case 4:
			return "Abril";
			break;
		case 5:
			return "Maio";
			break;
		case 6:
			return "Junho";
			break;
		case 7:
			return "Julho";
			break;
		case 8:
			return "Agosto";
			break;
		case 9:
			return "Setembro";
			break;
		case 10:
			return "Outubro";
			break;
		case 11:
			return "Novembro";
			break;
		case 12:
			return "Dezembro";
			break;
		default:
			return "Informe o valor numerico do mês";
			break;
	}
}

function retornaMesb($mesb)
{
	$mes= '';

	switch($mesb) {

        case 'Jan':
            $mes = '01';
			break;

        case 'Feb':
            $mes = '02';
			break;

        case 'Mar':
            $mes = '03';
			break;

        case 'Apr':
            $mes = '04';
			break;

        case 'May':
            $mes = '05';
			break;

        case 'Jun':
            $mes = '06';
			break;

        case 'Jul':
            $mes = '07';
			break;

        case 'Aug':
            $mes = '08';
			break;

        case 'Sep':
            $mes = '09';
			break;

        case 'Oct':
            $mes = '10';
			break;

        case 'Nov':
            $mes = '11';
			break;

        case 'Dec':
            $mes = '12';
			break;

        default:
            $mes = 'Erro';
			break;
	}

	return $mes;
}

function valorPorExtenso($valor=0) {
	$rt="";
	$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
	$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");

	$c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
	$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
	$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
	$u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");

	$z=0;

	$valor = number_format($valor, 2, ".", ".");
	$inteiro = explode(".", $valor);
	for($i=0;$i<count($inteiro);$i++)
		for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
			$inteiro[$i] = "0".$inteiro[$i];

	// $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
	$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
	for ($i=0;$i<count($inteiro);$i++) {
		$valor = $inteiro[$i];
		$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
		$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
		$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

		$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
		$t = count($inteiro)-1-$i;
		$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
		if ($valor == "000")$z++; elseif ($z > 0) $z--;
		if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
		if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
	}

	return($rt ? $rt : "zero");
}

function retornaCategoriaRecebimento($cod)
{
	switch ($cod)
	{
		case 1:
			return "Mensalidade do Organismo";
			break;
		case 2:
			return "Donativos e Lei de AMRA";
			break;
		case 3:
			return "Bazar";
			break;
		case 4:
			return "Comissões";
			break;
		case 5:
			return "Atividades Sociais";
			break;
		case 6:
			return "Recebimentos Diversos";
			break;
		case 7:
			return "Construção";
			break;
		case 8:
			return "Convenções";
			break;
		case 9:
			return "Receitas Financeiras";
			break;
		case 10:
			return "Jornadas";
			break;
		case 11:
			return "Suprimentos do Organismo";
			break;
		case 12:
			return "GLP - Trimestralidades";
			break;
		case 13:
			return "GLP - Suprimentos";
			break;
		case 14:
			return "GLP - Outros valores";
			break;
		case 15:
			return "Mensalidade Atrium Martinista";
			break;
        case 16:
			return "Região";
			break;
	}
}

function retornaCategoriaDivida($cod)
{
	switch ($cod)
	{
		case 1:
			return "Aluguel";
			break;
		case 2:
			return "Comissões";
			break;
		case 3:
			return "Luz";
			break;
		case 4:
			return "Água";
			break;
		case 5:
			return "Telefone";
			break;
		case 6:
			return "Tarifas";
			break;
		case 7:
			return "Manutenção";
			break;
		case 8:
			return "Beneficiência Social";
			break;
		case 9:
			return "Boletim";
			break;
		case 10:
			return "Convenções";
			break;
		case 11:
			return "Jornadas";
			break;
		case 12:
			return "Reuniões Sociais";
			break;
		case 13:
			return "Despesas de Correio";
			break;
		case 14:
			return "Anúncios";
			break;
		case 15:
			return "Carta Constitutiva (GLP)";
			break;
		case 16:
			return "Cantina";
			break;
		case 17:
			return "Despesas Gerais";
			break;
		case 18:
			return "GLP - Remessa Trimestralidade";
			break;
		case 19:
			return "GLP - Pagamentos Suprimentos";
			break;
		case 20:
			return "GLP - Outros";
			break;
		case 21:
			return "Investimentos";
			break;
		case 22:
			return "Impostos";
			break;
        case 23:
			return "Região";
			break;
	}
}

function retornaCategoriaDespesa($cod)
{
	switch ($cod)
	{
		case 1:
			return "Aluguel";
			break;
		case 2:
			return "Comissões";
			break;
		case 3:
			return "Luz";
			break;
		case 4:
			return "Água";
			break;
		case 5:
			return "Telefone";
			break;
		case 6:
			return "Tarifas";
			break;
		case 7:
			return "Manutenção";
			break;
		case 8:
			return "Beneficiência Social";
			break;
		case 9:
			return "Boletim";
			break;
		case 10:
			return "Convenções";
			break;
		case 11:
			return "Jornadas";
			break;
		case 12:
			return "Reuniões Sociais";
			break;
		case 13:
			return "Despesas de Correio";
			break;
		case 14:
			return "Anúncios";
			break;
		case 15:
			return "Carta Constitutiva (GLP)";
			break;
		case 16:
			return "Cantina";
			break;
		case 17:
			return "Despesas Gerais";
			break;
		case 18:
			return "GLP - Remessa Trimestralidade";
			break;
		case 19:
			return "GLP - Pagamentos Suprimentos";
			break;
		case 20:
			return "GLP - Outros";
			break;
		case 21:
			return "Investimentos";
			break;
		case 22:
			return "Impostos";
			break;
        case 23:
			return "Região";
			break;
	}
}

function retornaCategoriaRendimento($cod)
{
	switch ($cod)
	{
		case 1:
			return "Dinheiro em Caixa";
			break;
		case 2:
			return "Bancos";
			break;
		case 3:
			return "Atrium Martinista";
			break;
		case 4:
			return "Aplicação Poupança";
			break;
		case 5:
			return "Aplicação CDB";
			break;
		case 6:
			return "Aplicação RDB";
			break;
		case 7:
			return "Aplicação Outros";
			break;
	}
}

function retornaAtribuidoARendimento($cod)
{
	switch ($cod)
	{
		case 1:
			return "Organismo Afiliado";
			break;
		case 2:
			return "Região";
			break;
	}
}

function retornaSaidas($r,$d,$mesAtual,$anoAtual,$idOrganismoAfiliado)
{
	/**
     * Cálculo Entradas
     */
	$mensalidades 			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,1);
	$mensalidadesAtriumMartinista 	= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,15);
	$totalEntradaMensalidades	= $mensalidades+$mensalidadesAtriumMartinista;
	$comissoesEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,4);
	$construcao			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,7);
	$donativos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,2);
	$atividadesSociais		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,5);
	$convencoesEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,8);
	$jornadasEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,10);
    $regiaoEntrada  		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,16);
	$totalEntradaConvencoesJornadas = $convencoesEntrada+$jornadasEntrada+$regiaoEntrada;
	$receitasFinanceiras		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,9);
	$bazar				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,3);
	$suprimentos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,11);
	$totalEntradaBazarSuprimentos	= $bazar+$suprimentos;
	$recebimentosDiversos		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,6);
	//Subtotal
	$subtotalEntrada		= $totalEntradaMensalidades+$comissoesEntrada+$construcao+$donativos
										+$atividadesSociais+$totalEntradaConvencoesJornadas
										+$receitasFinanceiras+$totalEntradaBazarSuprimentos+$recebimentosDiversos;
	$glpTrimestralidades		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,12);
	$glpOutros			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,14);
	$totalEntradaTrimesOutros	= $glpTrimestralidades+$glpOutros;
	$glpSuprimentos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,13);
	//Soma das Entradas
	$somaEntradas			= $subtotalEntrada+$totalEntradaTrimesOutros+$glpSuprimentos;

	/**
     * Cálculo Saídas
     */
	$alugueis 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,1);
	$comissoes 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,2);
	$luz	 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,3);
	$agua	 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,4);
	$telefone 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,5);
	$totalSaidaLuzAguaTelefone	= $luz+$agua+$telefone;
	$tarifas 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,6);
	$impostos			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,22);
	$totalSaidaTarifasImpostos	= $tarifas+$impostos;
	$manutencao			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,7);
	$beneficienciaSocial		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,8);
	$boletim 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,9);
	$convencoesSaida		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,10);
	$jornadasSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,11);
    $regiaoSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,23);
	$totalSaidaConvencoesJornadas 	= $convencoesSaida+$jornadasSaida+$regiaoSaida;
	$reunioesSociais		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,12);
	$despesaCorreio			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,13);
	$anuncios 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,14);
	$cartaConstitutivaGLP		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,15);
	$cantina 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,16);
	$despesasGerais			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,17);


	//Subtotal Saída
	$subtotalSaida			= $alugueis+$comissoes+$totalSaidaLuzAguaTelefone+$tarifas+$impostos+$manutencao
									+$beneficienciaSocial+$boletim+$totalSaidaConvencoesJornadas+$reunioesSociais
									+$despesaCorreio+$anuncios+$cartaConstitutivaGLP+$cantina+$despesasGerais;

	$glpRemessaTrimestralidade		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,18);
	$glpOutrosSaida				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,20);
	$totalGlpRemessTrimesOutros		= $glpRemessaTrimestralidade+$glpOutrosSaida;
	$glpPagamentosSuprimentos		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,19);
	$investimentos				= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,21);

	//Total Saída
	$totalSaidas				= $subtotalSaida+$totalGlpRemessTrimesOutros+$glpPagamentosSuprimentos+$investimentos;

	return $totalSaidas;
}

function retornaSaldo($r,$d,$mesAtual=null,$anoAtual,$idOrganismoAfiliado,$saldoMesesAnteriores)
{
	/**
     * Cálculo Entradas
     */
	$mensalidades 			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,1);
	$mensalidadesAtriumMartinista 	= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,15);
	$totalEntradaMensalidades	= $mensalidades+$mensalidadesAtriumMartinista;
	$comissoesEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,4);
	$construcao			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,7);
	$donativos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,2);
	$atividadesSociais		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,5);
	$convencoesEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,8);
	$jornadasEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,10);
    $regiaoEntrada                  = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,16);
	$totalEntradaConvencoesJornadas = $convencoesEntrada+$jornadasEntrada+$regiaoEntrada;
	$receitasFinanceiras		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,9);
	$bazar				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,3);
	$suprimentos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,11);
	$totalEntradaBazarSuprimentos	= $bazar+$suprimentos;
	$recebimentosDiversos		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,6);
	//Subtotal
	$subtotalEntrada		= $totalEntradaMensalidades+$comissoesEntrada+$construcao+$donativos
										+$atividadesSociais+$totalEntradaConvencoesJornadas
										+$receitasFinanceiras+$totalEntradaBazarSuprimentos+$recebimentosDiversos;
	$glpTrimestralidades		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,12);
	$glpOutros			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,14);
	$totalEntradaTrimesOutros	= $glpTrimestralidades+$glpOutros;
	$glpSuprimentos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,13);
	//Soma das Entradas
	$somaEntradas			= $subtotalEntrada+$totalEntradaTrimesOutros+$glpSuprimentos;

	//Total das Entradas

	$totalEntradas = $somaEntradas+$saldoMesesAnteriores;
	//echo "totalEntradas:".$totalEntradas."<br>";
	/**
     * Cálculo Saídas
     */
	$alugueis 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,1);
	$comissoes 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,2);
	$luz	 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,3);
	$agua	 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,4);
	$telefone 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,5);
	$totalSaidaLuzAguaTelefone	= $luz+$agua+$telefone;
	$tarifas 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,6);
	$impostos			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,22);
	$totalSaidaTarifasImpostos	= $tarifas+$impostos;
	$manutencao			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,7);
	$beneficienciaSocial		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,8);
	$boletim 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,9);
	$convencoesSaida		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,10);
	$jornadasSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,11);
    $regiaoSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,23);
	$totalSaidaConvencoesJornadas 	= $convencoesSaida+$jornadasSaida+$regiaoSaida;
	$reunioesSociais		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,12);
	$despesaCorreio			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,13);
	$anuncios 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,14);
	$cartaConstitutivaGLP		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,15);
	$cantina 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,16);
	$despesasGerais			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,17);

	//Subtotal Saída
	$subtotalSaida			= $alugueis+$comissoes+$totalSaidaLuzAguaTelefone+$tarifas+$impostos+$manutencao
									+$beneficienciaSocial+$boletim+$totalSaidaConvencoesJornadas+$reunioesSociais
									+$despesaCorreio+$anuncios+$cartaConstitutivaGLP+$cantina+$despesasGerais;

	$glpRemessaTrimestralidade	= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,18);
	$glpOutrosSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,20);
	$totalGlpRemessTrimesOutros	= $glpRemessaTrimestralidade+$glpOutrosSaida;
	$glpPagamentosSuprimentos	= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,19);
	$investimentos			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,21);

	//Total Saída
	$totalSaidas			= $subtotalSaida+$totalGlpRemessTrimesOutros+$glpPagamentosSuprimentos+$investimentos;
	//echo "totalSaidas:".$totalSaidas."<br>";
	//SALDO DO MÊS
	$saldoMes 			= $totalEntradas-$totalSaidas;
        //echo "totalSaldo:".$saldoMes."<br>";
	return $saldoMes;
}

function retornaSaldoRetornoArray($r,$d,$mesAtual=null,$anoAtual,$idOrganismoAfiliado,$saldoMesesAnteriores,$idIgnorarRecebimento=null,$valorRecebimento=null,$idIgnorarDespesa=null,$valorDespesa=null)
{
    /**
     * Cálculo Entradas
     */
    $mensalidades 			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,1,null,$idIgnorarRecebimento);
    $mensalidadesAtriumMartinista 	= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,15,null,$idIgnorarRecebimento);
    $totalEntradaMensalidades	= $mensalidades+$mensalidadesAtriumMartinista;
    $comissoesEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,4,null,$idIgnorarRecebimento);
    $construcao			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,7,null,$idIgnorarRecebimento);
    $donativos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,2,null,$idIgnorarRecebimento);
    $atividadesSociais		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,5,null,$idIgnorarRecebimento);
    $convencoesEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,8,null,$idIgnorarRecebimento);
    $jornadasEntrada		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,10,null,$idIgnorarRecebimento);
    $regiaoEntrada                  = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,16,null,$idIgnorarRecebimento);
    $totalEntradaConvencoesJornadas = $convencoesEntrada+$jornadasEntrada+$regiaoEntrada;
    $receitasFinanceiras		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,9,null,$idIgnorarRecebimento);
    $bazar				= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,3,null,$idIgnorarRecebimento);
    $suprimentos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,11,null,$idIgnorarRecebimento);
    $totalEntradaBazarSuprimentos	= $bazar+$suprimentos;
    $recebimentosDiversos		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,6,null,$idIgnorarRecebimento);
    //Subtotal
    $subtotalEntrada		= $totalEntradaMensalidades+$comissoesEntrada+$construcao+$donativos
        +$atividadesSociais+$totalEntradaConvencoesJornadas
        +$receitasFinanceiras+$totalEntradaBazarSuprimentos+$recebimentosDiversos;
    $glpTrimestralidades		= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,12,null,$idIgnorarRecebimento);
    $glpOutros			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,14,null,$idIgnorarRecebimento);
    $totalEntradaTrimesOutros	= $glpTrimestralidades+$glpOutros;
    $glpSuprimentos			= $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,13,null,$idIgnorarRecebimento);
    //Soma das Entradas
    $somaEntradas			= $subtotalEntrada+$totalEntradaTrimesOutros+$glpSuprimentos+$valorRecebimento;

    //$somaEntradas			= $valorRecebimento;

    //Total das Entradas

    $totalEntradas = $somaEntradas+$saldoMesesAnteriores;
    //$totalEntradas = $somaEntradas;
    //echo "totalEntradas:".$totalEntradas."<br>";
    /**
     * Cálculo Saídas
     */
    $alugueis 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,1,null,$idIgnorarDespesa);
    $comissoes 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,2,null,$idIgnorarDespesa);
    $luz	 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,3,null,$idIgnorarDespesa);
    $agua	 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,4,null,$idIgnorarDespesa);
    $telefone 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,5,null,$idIgnorarDespesa);
    $totalSaidaLuzAguaTelefone	= $luz+$agua+$telefone;
    $tarifas 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,6,null,$idIgnorarDespesa);
    $impostos			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,22,null,$idIgnorarDespesa);
    $totalSaidaTarifasImpostos	= $tarifas+$impostos;
    $manutencao			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,7,null,$idIgnorarDespesa);
    $beneficienciaSocial		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,8,null,$idIgnorarDespesa);
    $boletim 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,9,null,$idIgnorarDespesa);
    $convencoesSaida		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,10,null,$idIgnorarDespesa);
    $jornadasSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,11,null,$idIgnorarDespesa);
    $regiaoSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,23,null,$idIgnorarDespesa);
    $totalSaidaConvencoesJornadas 	= $convencoesSaida+$jornadasSaida+$regiaoSaida;
    $reunioesSociais		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,12,null,$idIgnorarDespesa);
    $despesaCorreio			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,13,null,$idIgnorarDespesa);
    $anuncios 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,14,null,$idIgnorarDespesa);
    $cartaConstitutivaGLP		= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,15,null,$idIgnorarDespesa);
    $cantina 			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,16,null,$idIgnorarDespesa);
    $despesasGerais			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,17,null,$idIgnorarDespesa);

    //Subtotal Saída
    $subtotalSaida			= $alugueis+$comissoes+$totalSaidaLuzAguaTelefone+$tarifas+$impostos+$manutencao
        +$beneficienciaSocial+$boletim+$totalSaidaConvencoesJornadas+$reunioesSociais
        +$despesaCorreio+$anuncios+$cartaConstitutivaGLP+$cantina+$despesasGerais;

    $glpRemessaTrimestralidade	= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,18,null,$idIgnorarDespesa);
    $glpOutrosSaida			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,20,null,$idIgnorarDespesa);
    $totalGlpRemessTrimesOutros	= $glpRemessaTrimestralidade+$glpOutrosSaida;
    $glpPagamentosSuprimentos	= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,19,null,$idIgnorarDespesa);
    $investimentos			= $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,21,null,$idIgnorarDespesa);

    //Total Saída
    $totalSaidas			= $subtotalSaida+$totalGlpRemessTrimesOutros+$glpPagamentosSuprimentos+$investimentos;
    $totalSaidas 			= $totalSaidas+$valorDespesa;
    //echo "<br>totalSaidas:".$totalSaidas."<br>";
    //SALDO DO MÊS
    $saldoMes 			= $totalEntradas-$totalSaidas;

    $arr=array();
    $arr['saldoAnterior']=$saldoMesesAnteriores;
    $arr['totalEntradas']=$totalEntradas;
    $arr['totalSaidas']=$totalSaidas;
    $arr['saldoMes']=$saldoMes;

    $arr['totalEntradasDoMes']=$somaEntradas;
    $arr['totalSaidasDoMes']=$totalSaidas;
    //echo "totalSaldo:".$saldoMes."<br>";
    return $arr;
}

function retornaSaldoRetornoArrayFirebase($queryRecebimentos,$queryDespesas,$mesAtual=null,$anoAtual,$saldoMesesAnteriores, $migracaoDB=null)
{
    //Iniciando variaveis Recebimentos
    $mensalidades=0;
    $mensalidadesAtriumMartinista=0;
    $comissoesEntrada=0;
    $construcao=0;
    $donativos=0;
    $atividadesSociais=0;
    $convencoesEntrada=0;
    $jornadasEntrada=0;
    $regiaoEntrada=0;
    $receitasFinanceiras=0;
    $bazar=0;
    $suprimentos=0;
    $recebimentosDiversos=0;
    $glpTrimestralidades=0;
    $glpOutros=0;
    $glpSuprimentos=0;

    //Iniciando variaveis Despesas
    $alugueis=0;
    $comissoes=0;
    $luz=0;
    $agua=0;
    $telefone=0;
    $tarifas=0;
    $impostos=0;
    $manutencao=0;
    $beneficienciaSocial=0;
    $boletim=0;
    $convencoesSaida=0;
    $jornadasSaida=0;
    $regiaoSaida=0;
    $reunioesSociais=0;
    $despesaCorreio=0;
    $anuncios=0;
    $cartaConstitutivaGLP=0;
    $cantina=0;
    $despesasGerais=0;
    $glpRemessaTrimestralidade=0;
    $glpOutrosSaida=0;
    $glpPagamentosSuprimentos=0;
    $investimentos=0;

    $arrDocumentosRecebimentosMesAno = array();
    $arrDocumentosDespesasMesAno = array();


    //Somar Recebimentos
	if(count($queryRecebimentos->rows()))
	{
		foreach ($queryRecebimentos as $documentRecebimentos) {

            if ($documentRecebimentos->exists()) {

                $arrRecebimentos = $documentRecebimentos->data();

                //echo "<br>ArrayRecebimentos vindo do Firebase:<pre>";print_r($arrRecebimentos);
                $mesR = substr($arrRecebimentos['dataRecebimento'],3,2);
                $anoR = substr($arrRecebimentos['dataRecebimento'],6,4);

                //if($arrRecebimentos['idDespesaSOA']!=$idIgnorarRecebimento)
                //{
                    if ($mesR == $mesAtual && $anoR == $anoAtual) {

                        $arrDocumentosRecebimentosMesAno[] = $documentRecebimentos->id();

                        //Cálculo

                        /**
                         * Cálculo Entradas
                         */

                        if ($migracaoDB) {
							$arrRecebimentos['valorRecebimento'] = str_replace('.', '', $arrRecebimentos['valorRecebimento']);
							$arrRecebimentos['valorRecebimento'] = str_replace(',', '.', $arrRecebimentos['valorRecebimento']);
						}
                        switch($arrRecebimentos['categoriaRecebimento'])
                        {
                            case 1:
                                $mensalidades += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 15:
                                $mensalidadesAtriumMartinista += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 4:
                                $comissoesEntrada += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 7:
                                $construcao += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 2:
                                $donativos += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 5:
                                $atividadesSociais += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 8:
                                $convencoesEntrada += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 10:
                                $jornadasEntrada += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 16:
                                $regiaoEntrada += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 9:
                                $receitasFinanceiras += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 3:
                                $bazar += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 11:
                                $suprimentos += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 6:
                                $recebimentosDiversos += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 12:
                                $glpTrimestralidades += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 14:
                                $glpOutros += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                            case 13:
                                $glpSuprimentos += floatval($arrRecebimentos['valorRecebimento']);
                                break;
                        }
                    }
                //}
            }
        }
    }


	//Somar Despesas

	if(count($queryDespesas->rows())) {
        foreach ($queryDespesas as $documentDespesas) {

            if ($documentDespesas->exists()) {

                $arrDespesas = $documentDespesas->data();
                $mesD = substr($arrDespesas['dataDespesa'], 3, 2);
                $anoD = substr($arrDespesas['dataDespesa'], 6, 4);

                //if ($arrDespesas['idDespesaSOA'] != $idIgnorarDespesa) {

                    if ($mesD == $mesAtual && $anoD == $anoAtual) {

                        $arrDocumentosDespesasMesAno[] = $documentDespesas->id();

                        //Cálculo

                        /**
                         * Cálculo Saidas
                         */
						if ($migracaoDB) {
							$arrDespesas['valorDespesa'] = str_replace('.', '', $arrDespesas['valorDespesa']);
							$arrDespesas['valorDespesa'] = str_replace(',', '.', $arrDespesas['valorDespesa']);
						}
                        switch ($arrDespesas['categoriaDespesa']) {
                            case 1:
                                $alugueis += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 2:
                                $comissoes += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 3:
                                $luz += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 4:
                                $agua += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 5:
                                $telefone += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 6:
                                $tarifas += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 22:
                                $impostos += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 7:
                                $manutencao += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 8:
                                $beneficienciaSocial += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 9:
                                $boletim += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 10:
                                $convencoesSaida += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 11:
                                $jornadasSaida += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 23:
                                $regiaoSaida += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 12:
                                $reunioesSociais += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 13:
                                $despesaCorreio += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 14:
                                $anuncios += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 15:
                                $cartaConstitutivaGLP += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 16:
                                $cantina += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 17:
                                $despesasGerais += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 18:
                                $glpRemessaTrimestralidade += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 20:
                                $glpOutrosSaida += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 19:
                                $glpPagamentosSuprimentos += floatval($arrDespesas['valorDespesa']);
                                break;
                            case 21:
                                $investimentos += floatval($arrDespesas['valorDespesa']);
                                break;
                        }
                    }
                //}
            }
        }
    }

    /*

    echo "<br>mes atual: ".$mesAtual;
    echo "<br>ano atual: ".$anoAtual;
    echo "<br>mensalidades: ".$mensalidades;

    */

    /*
     * Cálculo dos subtotais Entradas
     */
    $totalEntradaMensalidades		= $mensalidades + $mensalidadesAtriumMartinista;
    $totalEntradaConvencoesJornadas = $convencoesEntrada + $jornadasEntrada + $regiaoEntrada;
    $totalEntradaBazarSuprimentos	= $bazar + $suprimentos;

    //Subtotal
    $subtotalEntrada				= $totalEntradaMensalidades + $comissoesEntrada + $construcao + $donativos + $atividadesSociais + $totalEntradaConvencoesJornadas + $receitasFinanceiras + $totalEntradaBazarSuprimentos + $recebimentosDiversos;

    $totalEntradaTrimesOutros		= $glpTrimestralidades + $glpOutros;

    //Soma das Entradas
    $somaEntradas					= $subtotalEntrada + $totalEntradaTrimesOutros + $glpSuprimentos;

    //echo "<br>Soma das Entradas no functions:".$somaEntradas;

    //Total das Entradas
    $totalEntradas 					= $somaEntradas + $saldoMesesAnteriores;

    /*
     * Cálculo dos subtotais Saídas
     */
    $totalSaidaLuzAguaTelefone		= $luz+$agua+$telefone;
    $totalSaidaTarifasImpostos		= $tarifas+$impostos;
    $totalSaidaConvencoesJornadas 	= $convencoesSaida+$jornadasSaida+$regiaoSaida;
    $subtotalSaida 					= $alugueis + $comissoes + $totalSaidaLuzAguaTelefone + $tarifas + $impostos + $manutencao + $beneficienciaSocial + $boletim + $totalSaidaConvencoesJornadas + $reunioesSociais + $despesaCorreio + $anuncios + $cartaConstitutivaGLP + $cantina + $despesasGerais;
    $totalGlpRemessTrimesOutros		= $glpRemessaTrimestralidade + $glpOutrosSaida;

    //Total Saída
    $totalSaidas					= $subtotalSaida + $totalGlpRemessTrimesOutros + $glpPagamentosSuprimentos + $investimentos;

    //echo "<br>totalSaidas:".$totalSaidas."<br>";
    //SALDO DO MÊS
    $saldoMes 						= $totalEntradas - $totalSaidas;

    $arr = array();
    $arr['previousBalance']	= $saldoMesesAnteriores;
    $arr['totalEntries']	= $somaEntradas;//Essa informação não considera o saldo anterior como entrada
    $arr['totalOutputs']	= $totalSaidas;
    $arr['monthBalance']	= $saldoMes;

    return $arr;
}

function estadoCivil($cod)
{
	switch ($cod)
	{
		case 1:
			return "Solteiro(a)";
			break;
		case 2:
			return "Casado(a)";
			break;
		case 3:
			return "Desquitado(a)";
			break;
		case 4:
			return "Divorciado(a)";
			break;
		case 5:
			return "União Estável";
			break;
		case 6:
			return "Viúvo(a)";
			break;
	}
}

function prioridade($cod)
{
	switch ($cod)
	{
		case 1:
			return "Alta prioridade";
			break;
		case 2:
			return "média";
			break;
		case 3:
			return "Baixa prioridade";
			break;
	}
}

function organismoAfiliadoNomeCompleto($classificacao,$tipo,$nomeOa,$sigla=null){
	$nomeCompletoOa = '';
	switch ($classificacao) {
		case 1: $nomeCompletoOa = "Loja "; break;
		case 2: $nomeCompletoOa = "Pronaos "; break;
		case 3: $nomeCompletoOa = "Capítulo "; break;
		case 4: $nomeCompletoOa = "Heptada "; break;
		case 5: $nomeCompletoOa = "Atrium "; break;
	}
	switch ($tipo) {
		case 1: $nomeCompletoOa .= "R+C "; break;
		case 2: $nomeCompletoOa .= "TOM "; break;
	}
	$nomeCompletoOa .= $nomeOa;
	if($sigla!=null) {
		$nomeCompletoOa .= " - " . $sigla;
	}
	return $nomeCompletoOa;
}

function retornaNomeCompletoOrganismoAfiliado($idOrganismo){

    if(realpath('../../controller/organismoController.php')){
        require_once '../../controller/organismoController.php';
    }else{
        if(realpath('controller/organismoController.php')){
            require_once 'controller/organismoController.php';
        }else{
            require_once '../controller/organismoController.php';
        }
    }
	$oc = new organismoController();
	$dadosOrg = $oc->buscaOrganismo($idOrganismo);

	$nomeCompletoOa = '';
	switch ($dadosOrg->getClassificacaoOrganismoAfiliado()) {
		case 1: $nomeCompletoOa = "Loja "; break;
		case 2: $nomeCompletoOa = "Pronaos "; break;
		case 3: $nomeCompletoOa = "Capítulo "; break;
		case 4: $nomeCompletoOa = "Heptada "; break;
		case 5: $nomeCompletoOa = "Atrium "; break;
	}
	switch ($dadosOrg->getTipoOrganismoAfiliado()) {
		case 1: $nomeCompletoOa .= "R+C "; break;
		case 2: $nomeCompletoOa .= "TOM "; break;
	}
	$nomeCompletoOa .= $dadosOrg->getNomeOrganismoAfiliado();
	if($dadosOrg->getSiglaOrganismoAfiliado()!=null) {
		$nomeCompletoOa .= " - " . $dadosOrg->getSiglaOrganismoAfiliado();
	}
	return $nomeCompletoOa;
}

function retornaGrauRC($cod)
{
	if($cod==3)
	{
		return 1;
	}
	if($cod==5)
	{
		return 2;
	}
	if($cod==7)
	{
		return 3;
	}
	if($cod==9)
	{
		return 4;
	}
	if($cod==11)
	{
		return 5;
	}
	if($cod==13)
	{
		return 6;
	}
	if($cod==15)
	{
		return 7;
	}
	if($cod==17)
	{
		return 8;
	}
	if($cod==19)
	{
		return 9;
	}
	if($cod==32)
	{
		return 10;
	}
	if($cod==33)
	{
		return 11;
	}
	if($cod==34)
	{
		return 12;
	}
    if($cod==35)
    {
        return "Discurso de Orientação";
    }
    if($cod==1)
    {
        return "Iniciacao a Loja";
    }
	return 13;
}

function converteGrauSOAParaOrcz($cod)
{
    if($cod==1)
    {
        return 3;
    }
    if($cod==2)
    {
        return 5;
    }
    if($cod==3)
    {
        return 7;
    }
    if($cod==4)
    {
        return 9;
    }
    if($cod==5)
    {
        return 11;
    }
    if($cod==6)
    {
        return 13;
    }
    if($cod==7)
    {
        return 15;
    }
    if($cod==8)
    {
        return 17;
    }
    if($cod==9)
    {
        return 19;
    }
    if($cod==10)
    {
        return 32;
    }
    if($cod==11)
    {
        return 33;
    }
    if($cod==12)
    {
        return 34;
    }
    if($cod=="Discurso de Orientação")
    {
        return 35;
    }
    if($cod=="Iniciacao a Loja")
    {
        return 1;
    }
    return 13;
}


function retornaGrauRCSOA($cod)
{
    if($cod==1)
    {
        return 1;
    }
    if($cod==2)
    {
        return 2;
    }
    if($cod==3)
    {
        return 3;
    }
    if($cod==4)
    {
        return 4;
    }
    if($cod==5)
    {
        return 5;
    }
    if($cod==6)
    {
        return 6;
    }
    if($cod==7)
    {
        return 7;
    }
    if($cod==8)
    {
        return 8;
    }
    if($cod==9)
    {
        return 9;
    }
    if($cod==10)
    {
        return 10;
    }
    if($cod==11)
    {
        return 11;
    }
    if($cod==12)
    {
        return 12;
    }
    if($cod==13)
    {
        return "Iniciacao a Loja";
    }
    if($cod==15)
    {
        return "Discurso de Orientação";
    }
    return 0;
}

function retornaGrauRCConformeLote($cod)
{
    if ($cod > 0 && $cod <= 5)
    {
        return 0;
    }
    if ($cod > 5 && $cod < 7)
    {
        return 1;
    }
    if ($cod == 7)
    {
        return 2;
    }
    if ($cod == 8)
    {
        return 3;
    }
    if ($cod == 9)
    {
        return 4;
    }
    if ($cod == 10)
    {
        return 5;
    }
    if ($cod >= 11 && $cod <= 12)
    {
        return 6;
    }
    if ($cod >= 13 && $cod <= 15)
    {
        return 7;
    }
    if ($cod >= 16 && $cod <= 18)
    {
        return 8;
    }
    if ($cod >= 19 && $cod <= 22)
    {
        return 9;
    }
    if ($cod >= 23 && $cod <= 31 )
    {
        return 10;
    }
    if ($cod >= 32 && $cod <= 44)
    {
        return 11;
    }
    if ($cod >= 45)
    {
        return 12;
    }
    return 0;
}

function anoRC($ano,$mes,$dia)
{
	$mes = (int) $mes;
	$dia = (int) $dia;
        //echo "mes".$mes;
        //echo "dia".$dia;
	if($mes>3)
	{
		return $ano+1353;
	}else{
            if($mes==3&&$dia>=21)
            {
                return $ano+1353;
            }else{    
		return $ano+1352;
            }
	}
}

function comparaIniciacoesDeGrau($cod)
{
	if($cod==3)
	{
		return true;
	}
	if($cod==5)
	{
		return true;
	}
	if($cod==7)
	{
		return true;
	}
	if($cod==9)
	{
		return true;
	}
	if($cod==11)
	{
		return true;
	}
	if($cod==13)
	{
		return true;
	}
	if($cod==15)
	{
		return true;
	}
	if($cod==17)
	{
		return true;
	}
	if($cod==19)
	{
		return true;
	}
	if($cod==32)
	{
		return true;
	}
	if($cod==33)
	{
		return true;
	}
	if($cod==34)
	{
		return true;
	}
	return false;
}

function getRequisitionValidationCode($code='lucianob', $isUser=true) {

    if (!is_numeric($code) && !($isUser)) {
        return false;
    }
    else if (($isUser) && !is_numeric($code)) {
        $code = getUserKeyCode($code);
    }
    else if (($isUser) && is_numeric($code)) {
        return false;
    }

    $date = date('dmYHi')+0;
    //echo $date; exit;

    $codMult = $code * 17;

    $rest = floor(fmod($date, $codMult));


    $i = 9;
    $sum = 0;
    for ($j = strlen($rest); $j > 0; $j--) {
        $sum += (substr($rest, $j-1, 1) * $i);
        $i--;

        if ($i == 0)
            $i = 9;
    }
    $mod = $sum % 11;

    if ($mod > 9)
        $dv = 0;
    else
        $dv = $mod;

    return $sum.$dv;
}

function getUserKeyCode($user) {

    $sum = 0;
    for ($i = 0; $i < strlen($user); $i++) {
        $sum += ord(substr($user, $i, 1)) * ($i+1);
    }
    $sum = $sum * $i;

    return $sum;
}

function converteEncoding($item)
{
	return mb_convert_encoding($item, "UTF-8", "ISO-8859-1");
}

function retornaDadosOficialIniciaticoPeloSeqCadast($id){

	include_once("controller/membroOAController.php");
	$moam 	= new membroOA();
	$retorno        = $moam->buscaMembroOaPeloSeqCadast($id);

	if($retorno){
		foreach($retorno as $vetor){
			$dados					= '['.$vetor['codigoAfiliacao'].'] '.ucwords(mb_strtolower($vetor['nomeMembroOa'],'UTF-8'));
		}
	} else {
		$dados     = 'Ainda Não Cadastrado!';
	}
	return $dados;
}

function retornaDadosOficialIniciaticoPeloSeqCadastMembroOa($id){

	include_once("controller/membroOAController.php");
	$moam 	= new membroOA();
	$retorno        = $moam->buscaMembroOaPeloSeqCadast($id);

	if($retorno){
		foreach($retorno as $vetor){
			$dados					= '['.$vetor['codigoAfiliacao'].'] '.ucwords(mb_strtolower($vetor['nomeMembroOa'],'UTF-8'));
		}
	} else {
		$retorno        = $moam->buscaMembroOaPeloSeqCadast($id);
		$dados     = 'Ainda Não Cadastrado!';
	}
	return $dados;
}

function imprimeDadosMembroPeloSeqCadast($seqCadastMembro){

	$seqCadast = $seqCadastMembro;
	$ocultar_json=1;

	include 'js/ajax/retornaDadosMembroPorSeqCadast.php';
	$obj2 = json_decode(json_encode($return),true);

	$codAfiliacao   = $obj2['result'][0]['fields']['fSeqCadast'] != 0 ? "[" . $obj2['result'][0]['fields']['fCodRosacruz'] . "] " : "Não informado!";
	$nome           = $obj2['result'][0]['fields']['fSeqCadast'] != 0 ? ucwords(mb_strtolower($obj2['result'][0]['fields']['fNomCliente'])) : "";

	echo $codAfiliacao . $nome;
}

function situacaoCadastral($cod)
{
	switch($cod)
	{
        case 1:
            return "<span class=\"badge badge-primary\">Ativo</span>";
            break;
        case 2:
            return "<span class=\"badge badge-danger\">Inativo</span>";
            break;
        case 3:
            return "<span class=\"badge badge-warning\">Estudos paralisados</span>";
            break;
        case 7:
            return "<span class=\"badge badge-warning\">Estudos paralisados</span>";
            break;
        case 9:
            return "<span class=\"badge badge-plain\">Vitalício</span>";
            break;
        default:
            return "<span class=\"badge badge-danger\">Não encontrado. Avisar T.I</span>";
            break;
	}
}

function situacaoRemessa($cod)
{
	switch($cod)
	{
        case 1:
            return "<span class=\"badge badge-primary\">Envio de Lote - Normal</span>";
            break;
        case 6:
            return "<span class=\"badge badge-warning\">Estudos paralisados</span>";
            break;
        case 7:
            return "<span class=\"badge badge-warning\">Estudos paralisados</span>";
            break;
        default:
            return "<span class=\"badge badge-danger\">Não encontrado</span>";
            break;
	}
}

function situacaoOA($cod)
{
	switch($cod)
	{
        case 1:
            return "Recesso";
            break;
        case 2:
            return "Fechou";
            break;
        case 3:
            return "Aberto";
            break;
        default:
            return "Aberto";
            break;
	}
}

function retornaOperadoraCelularTexto($cod)
{
	switch($cod)
	{
        case 1:
            return "TIM";
            break;
        case 2:
            return "Vivo";
            break;
        case 3:
            return "Claro";
            break;
        case 4:
            return "Oi";
            break;
        default:
            return "";
            break;
	}
}

function retornaLetraClassificacaoOa($cod)
{
	switch($cod)
	{
        case 1:
            return "L";
            break;
        case 2:
            return "P";
            break;
        case 3:
            return "C";
            break;
        case 4:
            return "H";
            break;
        case 5:
            return "A";
            break;
        default:
            return "";
            break;
	}
}

function retornaSiglaPais($cod)
{
	switch($cod)
	{
        case 1:
            return "BR";
            break;
        case 2:
            return "PT";
            break;
        case 3:
            return "AO";
            break;
        default:
            return "";
            break;
	}
}

//FORMATA COMO TIMESTAMP
function dataToTimestamp($data){
    $ano = substr($data, 6,4);
    $mes = substr($data, 3,2);
    $dia = substr($data, 0,2);
    return mktime(0, 0, 0, $mes, $dia, $ano);
}

function Soma1dia($data){
    $ano = substr($data, 6,4);
    $mes = substr($data, 3,2);
    $dia = substr($data, 0,2);
    return date("d/m/Y", mktime(0, 0, 0, $mes, $dia+1, $ano));
}

//LISTA DE FERIADOS NO ANO
function Feriados($ano,$posicao){
    $dia = 86400;
    $datas = array();
    $datas['pascoa'] = easter_date($ano);
    $datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
    $datas['carnaval'] = $datas['pascoa'] - (47 * $dia);
    $datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
    $feriados = array (
       '01/01',
       '02/02', // Navegantes
       date('d/m',$datas['carnaval']),
       date('d/m',$datas['sexta_santa']),
       date('d/m',$datas['pascoa']),
       '21/04',
       '01/05',
       date('d/m',$datas['corpus_cristi']),
       '20/09', // Revolução Farroupilha \m/
       '12/10',
       '02/11',
       '15/11',
       '25/12',
    );

    return $feriados[$posicao]."/".$ano;
}


function SomaDiasUteis($xDataInicial,$xSomarDias){
    for($ii=1; $ii<=$xSomarDias; $ii++){

        $xDataInicial=Soma1dia($xDataInicial); //SOMA DIA NORMAL

        //VERIFICANDO SE EH DIA DE TRABALHO
        if(date("w", dataToTimestamp($xDataInicial))=="0"){
            //SE DIA FOR DOMINGO OU FERIADO, SOMA +1
            $xDataInicial=Soma1dia($xDataInicial);

        }else if(date("w", dataToTimestamp($xDataInicial))=="6"){
            //SE DIA FOR SABADO, SOMA +2
            $xDataInicial=Soma1dia($xDataInicial);
            $xDataInicial=Soma1dia($xDataInicial);

        }else{
            //senaum vemos se este dia eh FERIADO
            for($i=0; $i<=12; $i++){
                if($xDataInicial==Feriados(date("Y"),$i)){
                    $xDataInicial=Soma1dia($xDataInicial);
                }
            }
        }
    }
    return $xDataInicial;
}

function modalNotificacao($seqCadast){

	$modal = <<<EOD
<div class="modal inmodal" id="mySeeNotificacao" name="mySeeNotificacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-envelope modal-icon"></i>
                <h4 class="modal-title">Notificar Responsáveis</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                <div id="conteudoModal"></div>
                </div>
                <div class="row" style="display: none;" id="blocoTipo">
                    <div class="form-group" id="tipoNotificacao">
                        <br>
                        <label>Mensagem do Tipo: </label>
                        <div>
                            <select class="form-control col-sm-3" name="tipoNotificacaoGlp" id="tipoNotificacaoGlp" required="required">
                                <option value="0">Selecione...</option>
                                <option value="1">Não Entregue</option>
                                <option value="2">Falta Documento</option>
                                <option value="3">Erro de Documentação</option>
                                <option value="4">Deve Atualizar</option>
                                <option value="5">Outros</option>
                            </select>
                        </div>
                        <br>
                    </div>
                    <br>
                </div>
                <div class="row" style="" id="blocoTitulo">
                    <div class="form-group" style="margin-top: 20px;" id="tituloNotificacao">
                        <label>Título:</label>
                        <div>
                            <input class="form-control" id="tituloNotificacaoGlp" name="tituloNotificacaoGlp" type="text" value="" style="min-width: 320px" required="required">
                        </div>
                    </div>
                </div>
                <div class="row" style="" id="blocoMensagem">
                    <div class="form-group" style="margin-top: 10px;" id="mensagemNotificacao">
                        <label>Mensagem:</label>
                        <div style="border: #ccc solid 1px">
                            <div class="summernote" id="mensagemNotificacaoGlp" name="mensagemNotificacaoGlp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="fk_seqCadastRemetente" value="$seqCadast" />
                <div id="enviarNotificacaoButton"></div>
            </div>
        </div>
    </div>
</div>
EOD;
	echo $modal;
}


function modalIniciacaoDisponivel($seqCadast){

	$modal = <<<EOD
<div class="modal inmodal" id="mySeeNotificacao" name="mySeeNotificacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-envelope modal-icon"></i>
                <h4 class="modal-title">Notificar Responsáveis</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                <div id="conteudoModal"></div>
                </div>
                <div class="row" style="display: none;" id="blocoTipo">
                    <div class="form-group" id="tipoNotificacao">
                        <br>
                        <label>Mensagem do Tipo: </label>
                        <div>
                            <select class="form-control col-sm-3" name="tipoNotificacaoGlp" id="tipoNotificacaoGlp" required="required">
                                <option value="0">Selecione...</option>
                                <option value="1">Não Entregue</option>
                                <option value="2">Falta Documento</option>
                                <option value="3">Erro de Documentação</option>
                                <option value="4">Deve Atualizar</option>
                                <option value="5">Outros</option>
                            </select>
                        </div>
                        <br>
                    </div>
                    <br>
                </div>
                <div class="row" style="" id="blocoTitulo">
                    <div class="form-group" style="margin-top: 20px;" id="tituloNotificacao">
                        <label>Título:</label>
                        <div>
                            <input class="form-control" id="tituloNotificacaoGlp" name="tituloNotificacaoGlp" type="text" value="" style="min-width: 320px" required="required">
                        </div>
                    </div>
                </div>
                <div class="row" style="" id="blocoMensagem">
                    <div class="form-group" style="margin-top: 10px;" id="mensagemNotificacao">
                        <label>Mensagem:</label>
                        <div style="border: #ccc solid 1px">
                            <div class="summernote" id="mensagemNotificacaoGlp" name="mensagemNotificacaoGlp"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="fk_seqCadastRemetente" value="$seqCadast" />
                <div id="enviarNotificacaoButton"></div>
            </div>
        </div>
    </div>
</div>
EOD;
	echo $modal;
}

function imprimeDadosMembroSemIniciacoes(){

	$txt = <<<EOD
<td> 1º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 2º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 3º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 4º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 5º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 6º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 7º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 8º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 9º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 10º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 11º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
<td>------------------------------------------</td>
<td> 12º Grau de Templo (Não Realizado/Informado)</td>
<td><span class="pie"> -- </span></td>
<td> -- </td>
EOD;
	echo $txt;
}

function ordenaOficialAtuanteRetirante($x,$y,$dataX,$dataY,$codX=null,$codY=null)//Passar data no formato americano Ex. 2006-01-01
{
    $arr=array();
    $dataX = substr($dataX,0,4).substr($dataX,5,2).substr($dataX,8,2);
    $dataY = substr($dataY,0,4).substr($dataY,5,2).substr($dataY,8,2);
    if($dataX<$dataY)//ou seja 01/02/2016 < 31/12/2016
    {
        $arr[0]=$y;
        $arr[1]=$x;
        if(isset($codX))
        {
            $arr['cod'][0]=$codY;
            $arr['cod'][1]=$codX;
        }

    }
    if($dataX>$dataY)//ou seja 01/02/2016 > 31/12/2015
    {
        $arr[0]=$x;
        $arr[1]=$y;
        if(isset($codX))
        {
            $arr['cod'][0]=$codX;
            $arr['cod'][1]=$codY;
        }
    }
    if($dataX==$dataY)//ou seja 01/02/2016 > 31/12/2015
    {
        $arr[0]=$x;
        $arr[1]=$y;
        if(isset($codX))
        {
            $arr['cod'][0]=$codY;
            $arr['cod'][1]=$codX;
        }
    }

    return $arr;
}

function imprimeLink($link, $idAnexo, $idImovel){

    $arrLink = explode('.', $link);
    $arrLinkNome	= $arrLink[count($arrLink)-2];
    $arrLinkExt		= $arrLink[count($arrLink)-1];

    echo "<a target='_BLANK' href='" . $link . "'>". $arrLinkNome . "." . $arrLinkExt . "</a><a style='margin-right: 25px' onclick='excluiImovelAnexo(" . $idAnexo . "," . $idImovel . ");'> <i class='fa fa-trash'></i></a><br>";
}

function getTipoAtividadeDaAgenda($cod)
{
    switch($cod)
	{
        case 1:
            return "Convocação de Templo";
            break;
        case 2:
            return "Convocação de Pronaos";
            break;
        case 3:
            return "Convocação da Classe de Artesãos";
            break;
        case 4:
            return "Convocação da Ordem dos Guias do Graal";
            break;
        case 5:
            return "Meditação: Luz, Vida e Amor";
            break;
        case 6:
            return "Meditação: Elevação ao Sanctum Celestial";
            break;
        case 7:
            return "Meditação Aberta para Visitantes";
            break;
        case 8:
            return "Plantão";
            break;
        case 9:
            return "Atividades Sociais";
            break;
        case 10:
            return "Outros";
            break;
        default:
            return "Não encontrado. Avisar TI";
            break;
	}
}

function timeAgo($date){

	date_default_timezone_set('America/Sao_Paulo');

    if(empty($date)) {
        return "Sem data prevista";
    }

    $periods         = array("segundo", "minuto", "hora", "dia", "semana", "mês", "ano", "década");
    $lengths         = array("60","60","24","7","4.35","12","10");

    $now             = time();
    $unix_date         = strtotime($date);

    // check validity of date
    if(empty($unix_date)) {
        return "Data inválida";
    }

    // is it future date or past date
    if($now > $unix_date) {
        $difference     = $now - $unix_date;
        $tense         = "atrás";

    } else {
        $difference     = $unix_date - $now;
        $tense         = "a partir de agora";
    }

    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    if($difference != 1) {
    	if($periods[$j] == "mês"){
    		$periods[$j] = "meses";
    	} else {
        	$periods[$j].= "s";
    	}
    }
    return "$difference $periods[$j] {$tense}";
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function retornaIconeAnexo($ext){
    switch($ext) {
        case 'doc':
            $icone = '<i class="fa fa-file-word-o"></i>';
            break;
        case 'docx':
            $icone = '<i class="fa fa-file-word-o"></i>';
            break;
        case 'odt':
            $icone = '<i class="fa fa-file-word-o"></i>';
            break;
        case 'zip':
            $icone = '<i class="fa fa-file-zip-o"></i>';
            break;
        case 'rar':
            $icone = '<i class="fa fa-file-zip-o"></i>';
            break;
        case 'pdf':
            $icone = '<i class="fa fa-file-pdf-o"></i>';
            break;
        case 'jpg':
            $icone = '<i class="fa fa-file-picture-o"></i>';
            break;
        case 'jpeg':
            $icone = '<i class="fa fa-file-picture-o"></i>';
            break;
        case 'png':
            $icone = '<i class="fa fa-file-picture-o"></i>';
            break;
        default:
            $icone = '<i class="fa fa-file-o"></i>';
            break;
    }
    return $icone;
}

function retornaTipoAtividadeParaExibicao($tipoAtividadeIniciatica){
    $tipoParaExibicao = "";
    switch($tipoAtividadeIniciatica){
        case 0: $tipoParaExibicao = "ERRO"; break;
        case 1: $tipoParaExibicao = "1º Grau de Templo"; break;
        case 2: $tipoParaExibicao = "2º Grau de Templo"; break;
        case 3: $tipoParaExibicao = "3º Grau de Templo"; break;
        case 4: $tipoParaExibicao = "4º Grau de Templo"; break;
        case 5: $tipoParaExibicao = "5º Grau de Templo"; break;
        case 6: $tipoParaExibicao = "6º Grau de Templo"; break;
        case 7: $tipoParaExibicao = "7º Grau de Templo"; break;
        case 8: $tipoParaExibicao = "8º Grau de Templo"; break;
        case 9: $tipoParaExibicao = "9º Grau de Templo"; break;
        case 10: $tipoParaExibicao = "10º Grau de Templo"; break;
        case 11: $tipoParaExibicao = "11º Grau de Templo"; break;
        case 12: $tipoParaExibicao = "12º Grau de Templo"; break;
        case 13: $tipoParaExibicao = "Iniciação a Loja"; break;
        case 14: $tipoParaExibicao = "Iniciação ao Pronaos"; break;
        case 15: $tipoParaExibicao = "Discurso de Orientação"; break;
        case 16: $tipoParaExibicao = "Iniciação ao Capitulo"; break;
    }
    return $tipoParaExibicao;
}

function retornaStatusAtividadeSpan($statusAtividadeIniciatica){
    $statusAtividadeSpan = "";
    switch ($statusAtividadeIniciatica) {
        case 0:
            $statusAtividadeSpan = "<span class='badge badge-primary'>Ativo</span>";
            break;
        case 1:
            $statusAtividadeSpan = "<span class='badge badge-danger'>Inativo</span>";
            break;
        case 2:
            $statusAtividadeSpan = "<span class='badge badge-success'>Realizada</span>";
            break;
        case 3:
            $statusAtividadeSpan = "<span class='badge badge-danger'>Cancelado Posteriormente</span>";
            break;
    }
    return $statusAtividadeSpan;
}
function retornaNomeFormatado($nomeUsuario, $qnt=null){

    $arrNome				= explode(" ", $nomeUsuario);
    $nomeRetorno            = "";

    if ($qnt == null) {
        $nomeRetorno    = ucfirst(mb_strtolower($arrNome[0],'UTF-8'));
        $nomeRetorno    .= array_key_exists(1,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[1],'UTF-8')) : "";
        $nomeRetorno    .= array_key_exists(2,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[2],'UTF-8')) : "";
        $nomeRetorno    .= array_key_exists(3,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[3],'UTF-8')) : "";
        $nomeRetorno    .= array_key_exists(4,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[4],'UTF-8')) : "";
        $nomeRetorno    .= array_key_exists(5,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[5],'UTF-8')) : "";
    } else {
        if($qnt >= 1)
            $nomeRetorno    = ucfirst(mb_strtolower($arrNome[0],'UTF-8'));
        if($qnt >= 2)
            $nomeRetorno    .= array_key_exists(1,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[1],'UTF-8')) : "";
        if($qnt >= 3)
            $nomeRetorno    .= array_key_exists(2,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[2],'UTF-8')) : "";
        if($qnt >= 4)
            $nomeRetorno    .= array_key_exists(3,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[3],'UTF-8')) : "";
        if($qnt >= 5)
            $nomeRetorno    .= array_key_exists(4,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[4],'UTF-8')) : "";
        if($qnt == 6)
            $nomeRetorno    .= array_key_exists(5,$arrNome) ? " ".ucfirst(mb_strtolower($arrNome[5],'UTF-8')) : "";
    }
    return $nomeRetorno;
}

function validaAvatarUsuario($avatarUsuario){
    if ($avatarUsuario != "") {
        $avatarUsuario = $avatarUsuario;
    } else {
        $avatarUsuario = "img/default-user.png";
    }
    return $avatarUsuario;
}

function serverStatus(){
    $ip = '192.1.1.52';
    $port = 80;

    @$fp = fsockopen($ip, $port, $errno, $errstr, 1);
    if(!$fp){
        echo '<h2 class="text-danger">
                <i class="fa fa-play fa-rotate-90"></i>Offline
              </h2>';
    }
    else{
        echo '<h2 class="text-navy">
                <i class="fa fa-play fa-rotate-270"></i> Online
              </h2>';
    }
}

function verificaSeUsuarioEstaOnline($seqCadast)
{
    //verificar se existem registros na tabela usersonline
    $sql2 = 'SELECT * FROM usersonline WHERE seqCadast=:seqCadast';

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth2 = $con->prepare($sql2);

    $sth2->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

    $result = $c->run($sth2,false,false,true);
    if($result)
    {
        return true;
    }else{
        return false;
    }    
}

function usuarioOnline()
{
    ini_set("display_errors", 0 );
    require_once ('model/conexaoClass.php');

    //Config:
    /*
    $local ="localhost";
    $user ="root"; //Usuário do DataBase
    $senha="@w2xpglp"; //Senha do DataBase
    $db ="tom_soa_homologacao"; //DataBase

    //Código:

    $res = mysql_connect("$local", "$user", "$senha") or die ("Erro de conexão"); //conecta com o DB
    mysql_select_db($db,$res); //seleciona o DB
    */

    $tempmins = 3; //minutos para inatividade de um usuário
    $ip=$_SERVER['REMOTE_ADDR']; //pega o IP do visitante
    include_once ("model/criaSessaoClass.php");
    $sessao = new criaSessao();
    $login=$sessao->getValue("loginUsuario");//login do usuário
    $seqCadast=$sessao->getValue("seqCadast");//seqCadast do usuário
    $nome=$sessao->getValue("nomeUsuario");//nome do usuário
    //echo $login;exit();
    $time = time();

    $sql = "SELECT * FROM usersonline WHERE ip=:ip";

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':ip', $ip, PDO::PARAM_STR);

    $total = $c->run($sth,false,true);

    if($total>0) { //verifica se o ip ja esta no DB

        //ja que ele está é necessario dar um update no time para que ele não seja deletado rapdamente
        $sql2 = 'UPDATE usersonline SET time=:time WHERE ip=:ip';

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth2 = $con->prepare($sql2);

        $sth2->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth2->bindParam(':time', $time, PDO::PARAM_STR);

        $result = $c->run($sth2,true);

    } else {

        //ele não está no DB, então prescisamos inseri-lo
        $sql3 = 'INSERT INTO usersonline (ip,seqCadast,login,nome,time) VALUES (:ip,:seqCadast,:login,:nome,:time)';

        $c = new conexaoSOA();
        $con = $c->openSOA();
        $sth3 = $con->prepare($sql3);

        $sth3->bindParam(':ip', $ip, PDO::PARAM_STR);
        $sth3->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
        $sth3->bindParam(':time', $time, PDO::PARAM_STR);
        $sth3->bindParam(':login', $login, PDO::PARAM_STR);
        $sth3->bindParam(':nome', $nome, PDO::PARAM_STR);

        $result = $c->run($sth3,true);

    }

    $t = (time()-($tempmins*60));
    $sql4 = 'DELETE FROM usersonline WHERE time<:t'; //deleta os ips com mais de 5 minutos

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth4 = $con->prepare($sql4);

    $sth4->bindParam(':t', $t, PDO::PARAM_STR);

    $result = $c->run($sth4,true);

    $sql5 = "SELECT * FROM usersonline";

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth5 = $con->prepare($sql5);

    $totalUsers = $c->run($sth5,false,true);

    return $totalUsers.' usuários online'; //Mostra na pagina os usuarios online
}

function ipLogado(){
    $ip = 0;
    require_once ('model/conexaoClass.php');

    $sql = "SELECT ip FROM usersonline WHERE ip NOT LIKE '%192.1.1.%' ORDER BY ip DESC LIMIT 1";

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $resultado = $c->run($sth);
    if ($resultado){
        //count($resultado);
        foreach($resultado as $vetor){
            $ip = $vetor['ip'];
        }
    }


    require_once 'lib/IP2Location.php';

    $db = new \IP2Location\Database('lib/IP2LOCATION-LITE-DB11.BIN', \IP2Location\Database::FILE_IO);
    $records = $db->lookup($ip, \IP2Location\Database::ALL);
    echo '<pre>';

    echo 'Endereço do IP        : ' . $records['ipAddress'] . "\n";
    echo 'Código do País        : ' . $records['countryCode'] . "\n";
    echo 'Nome do País          : ' . $records['countryName'] . "\n";
    echo 'Região                : ' . $records['regionName'] . "\n";
    echo 'Cidade                : ' . $records['cityName'] . "\n";
    echo 'Latitude              : ' . $records['latitude'] . "\n";
    echo 'Longitude             : ' . $records['longitude'] . "\n";
    echo 'Fuso Horário          : ' . $records['timeZone'] . "\n";
    echo 'CEP                   : ' . $records['zipCode'] . "\n";
    echo '</pre>';
}

function parms($class,$string,$data) {
	$indexed=$data==array_values($data);
	foreach($data as $k=>$v) {
		if(is_string($v)) $v="$v";
		if($indexed) $string=preg_replace('/\?/',$v,$string,1);
		else $string=str_replace(":$k",$v,$string);
	}
	return "[".$class.": " . $string."]<br>";
}

function ip_visitor_country($ip=null)
{
    $country  = "Pais Desconhecido";

    if($ip==null)
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=".$ip);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $ip_data_in = curl_exec($ch); // string
    curl_close($ch);

    $ip_data = json_decode($ip_data_in,true);
    $ip_data = str_replace('&quot;', '"', $ip_data); // for PHP 5.2 see stackoverflow.com/questions/3110487/

    if($ip_data && $ip_data['geoplugin_countryCode'] != null) {
        $country = $ip_data['geoplugin_countryCode'];
    }

    return $country;
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function i18n($pais)
{

    switch ($pais)
    {
        case "PT":
            $arquivo = 'lang/pt_pt.php';
            break;
        case "AO":
            $arquivo = 'lang/pt_ao.php';
            break;
        default :
            $arquivo = 'lang/pt_br.php';
            break;
    }

    //Reconhecimento do caminho real do arquivo
    if(realpath("../".$arquivo)){
        $arquivo = "../".$arquivo;
    }else{
        if(realpath("../../".$arquivo)){
            $arquivo = "../../".$arquivo;
        }
    }

    require_once $arquivo;
}

function titleCase($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("de", "da", "dos", "das", "do", "I", "II", "III", "IV", "V", "VI"))
{
    /*
     * Exceptions in lower case are words you don't want converted
     * Exceptions all in upper case are any words you don't want converted to title case
     *   but should be converted to upper case, e.g.:
     *   king henry viii or king henry Viii should be King Henry VIII
     */
    $string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
    foreach ($delimiters as $dlnr => $delimiter) {
        $words = explode($delimiter, $string);
        $newwords = array();
        foreach ($words as $wordnr => $word) {
            if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtoupper($word, "UTF-8");
            } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtolower($word, "UTF-8");
            } elseif (!in_array($word, $exceptions)) {
                // convert to uppercase (non-utf8 only)
                $word = ucfirst($word);
            }
            array_push($newwords, $word);
        }
        $string = join($delimiter, $newwords);
    }//foreach
    return $string;
}

function retornaPaisMembroPeloSeqCadast($seqCadastMembro){

	$seqCadast = $seqCadastMembro;
        
        $server = 135;

        $ws = new Ws($server);

        // Nome do Método que deseja chamar
        $method = 'RetornaDadosMembroPorSeqCadast';

        // Parametros que serão enviados à chamada
        $params = array('CodUsuario' => 'lucianob',
                                        'SeqCadast' => $seqCadast);

        // Chamada do método
        $return = $ws->callMethod($method, $params, 'lucianob');
    
	$obj2 = json_decode(json_encode($return),true);

	$pais   = $obj2['result'][0]['fields']['fSeqCadast'] != 0 ? $obj2['result'][0]['fields']['fSigPaisEndereco'] : "BR";

	return $pais;
}

function diferencaMeses($data_ini,$data_end)
{
    $date = new DateTime($data_ini); // Data de Nascimento
    $idade_acompanhamento = $date->diff(new DateTime($data_end)); // Data do Acompanhamento
    $idade_acompanhamento_mostra_anos = $idade_acompanhamento->format('%Y')*12;
    $idade_acompanhamento_mostra_meses = $idade_acompanhamento->format('%m');


    $total_meses = $idade_acompanhamento_mostra_anos+$idade_acompanhamento_mostra_meses;
    
    return $total_meses;
} 

function date_diffe($date1, $date2) 
{
    $DataInicial = getdate(strtotime($date1));
    $DataFinal = getdate(strtotime($date2));
    $Dif = ($DataFinal[0] - $DataInicial[0]) / 86400;
    $meses = round($Dif/30);
    
    return $meses;
}

function dataAmanha($data)
{
   //### INICIO ## SABER DATA DE AMANHA ###
   $timestamp=  strtotime($data);
   $timestamp += 86400;
   $tomorrow= date('Y-m-d', $timestamp); 
   
   return $tomorrow;
  //### FIM ## SABER DATA DE AMANHA ###
}

function verificaPrazo($dt_atual,$dt_expira)
{
    $timestamp_dt_atual 	= strtotime($dt_atual); // converte para timestamp Unix
    $timestamp_dt_expira	= strtotime($dt_expira); // converte para timestamp Unix
 
    // data atual é maior que a data de expiração
    if ($timestamp_dt_atual > $timestamp_dt_expira) // true
    {    
      return true;
    }
    else // false
    {    
      return false;
    }
}   
function calcular_tempo_trasnc($hora1,$hora2){ 
    $separar[1]=explode(':',$hora1); 
    $separar[2]=explode(':',$hora2); 

$total_minutos_trasncorridos[1] = ($separar[1][0]*60)+$separar[1][1]; 
$total_minutos_trasncorridos[2] = ($separar[2][0]*60)+$separar[2][1]; 
$total_minutos_trasncorridos = $total_minutos_trasncorridos[1]-$total_minutos_trasncorridos[2]; 
if($total_minutos_trasncorridos<=59) return($total_minutos_trasncorridos.' Minutos'); 
elseif($total_minutos_trasncorridos>59){ 
$HORA_TRANSCORRIDA = round($total_minutos_trasncorridos/60); 
if($HORA_TRANSCORRIDA<=9) $HORA_TRANSCORRIDA='0'.$HORA_TRANSCORRIDA; 
$MINUTOS_TRANSCORRIDOS = $total_minutos_trasncorridos%60; 
if($MINUTOS_TRANSCORRIDOS<=9) $MINUTOS_TRANSCORRIDOS='0'.$MINUTOS_TRANSCORRIDOS; 
return ($HORA_TRANSCORRIDA.':'.$MINUTOS_TRANSCORRIDOS.' Horas'); 

} } 

/**
 * URL Exists
 *
 * Verifica se o caminho URL existe.
 * Isso é útil para verificar se um arquivo de imagem num 
 * servidor remoto antes de definir um link para o mesmo.
 *
 * @param string $url           O URL a verificar.
 *
 * @return boolean
 */
function url_exists($url) {

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return ($code == 200); // verifica se recebe "status OK"
}

function retornaLogin($seqCadast)
{
    $sql2 = 'SELECT * FROM usuario WHERE seqCadast=:seqCadast';

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth2 = $con->prepare($sql2);

    $sth2->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);

    $result = $c->run($sth2);
    if($result)
    {
        $login="";
        foreach($result as $v)
        {
            $login = $v['loginUsuario'];
        }    
        return $login;
    }else{
        return "--";
    }
}

function retornaTrimestre($mes)
{
    if($mes==1||$mes==2||$mes==3)
    {
        $trimestre=1;
    }
    if($mes==4||$mes==5||$mes==6)
    {
        $trimestre=2;
    }
    if($mes==7||$mes==8||$mes==9)
    {
        $trimestre=3;
    }
    if($mes==10||$mes==11||$mes==12)
    {
        $trimestre=4;
    }
    return $trimestre;
}

function retornaEstado($uf)
{
    switch ($uf)
    {
        case 'AC':
            $estado = "Acre";
            break;
        case 'AL':
            $estado = "Alagoas";
            break;
        case 'AM':
            $estado = "Amazonas";
            break;
        case 'BA':
            $estado = "Bahia";
            break;
        case 'CE':
            $estado = "Ceará";
            break;
        case 'DF':
            $estado = "Distrito Federal";
            break;
        case 'ES':
            $estado = "Espírito Santo";
            break;
        case 'GO':
            $estado = "Goiás";
            break;
        case 'MA':
            $estado = "Maranhão";
            break;
        case 'MT':
            $estado = "Mato Grosso";
            break;
        case 'MS':
            $estado = "Mato Grosso do Sul";
            break;
        case 'MG':
            $estado = "Minas Gerais";
            break;
        case 'PA':
            $estado = "Pará";
            break;
        case 'PB':
            $estado = "Paraíba";
            break;
        case 'PR':
            $estado = "Paraná";
            break;
        case 'PE':
            $estado = "Pernambuco";
            break;
        case 'PI':
            $estado = "Piauí";
            break;
        case 'RJ':
            $estado = "Rio de Janeiro";
            break;
        case 'RN':
            $estado = "Rio Grande do Norte";
            break;
        case 'RS':
            $estado = "Rio Grande do Sul";
            break;
        case 'RO':
            $estado = "Rondônia";
            break;
        case 'RR':
            $estado = "Roraima";
            break;
        case 'SC':
            $estado = "Santa Catarina";
            break;
        case 'SP':
            $estado = "São Paulo";
            break;
        case 'SE':
            $estado = "Sergipe";
            break;
        case 'TO':
            $estado = "Tocantins";
            break;
        default:
            $estado = "";
            break;
    }
    return $estado;
}

function retornaIdTipoObrigacaoORCZ($tipo){
        switch($tipo){
            case "1": return 3; break;
            case "2": return 5; break;
            case "3": return 7; break;
            case "4": return 9; break;
            case "5": return 11; break;
            case "6": return 13; break;
            case "7": return 15; break;
            case "8": return 17; break;
            case "9": return 19; break;
            case "10": return 32; break;
            case "11": return 33; break;
            case "12": return 34; break;
            case "13": return 1; break;
            case "14": return 36; break;
            case "15": return 35; break;
            case "16": return 24; break;
            default: return 0;
        }
}

function arraySort($array, $on, $order=SORT_ASC){
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

function atualizaFuncao($codigoFuncao,$tipo)
{
	$retorno = "";

	//Conversão de funções de capítulo
	if($tipo=="Capítulo")
	{
		switch ($codigoFuncao) {
            case '201':
                $retorno = 240;
                break;
            case '203':
                $retorno = 242;
                break;
            case '207':
                $retorno = 244;
                break;
            case '209':
                $retorno = 246;
                break;
            case '211':
                $retorno = 248;
                break;
            case '213':
                $retorno = 250;
                break;
            case '230':
                $retorno = 252;
                break;
        }
    }

	//Conversão de funções de pronaos
	if($tipo=="Pronaos")
	{
		switch ($codigoFuncao) {
            case '201':
                $retorno = 260;
                break;
            case '203':
                $retorno = 262;
                break;
            case '207':
                $retorno = 264;
                break;
            case '230':
                $retorno = 266;
                break;
        }
    }

	//Conversão de funções de capítulo
	if($tipo=="Loja")
	{
		switch ($codigoFuncao) {
            case '201':
                $retorno = 280;
                break;
            case '203':
                $retorno = 282;
                break;
            case '207':
                $retorno = 284;
                break;
            case '209':
                $retorno = 286;
                break;
            case '211':
                $retorno = 288;
                break;
            case '213':
                $retorno = 290;
                break;
            case '230':
                $retorno = 292;
                break;
        }
    }

    if($retorno=="") {
        $retorno = $codigoFuncao;
    }

    return $retorno;

}

function validaData($valorData)
{
	if(substr($valorData,6,4)>=1900) {
        $data = DateTime::createFromFormat('d/m/Y', $valorData);
        if ($data && $data->format('d/m/Y') === $valorData) {
            return true;
        } else {
            return false;
        }
    }else{
		return false;
	}
}

function aleatorioAssinatura(){
    $novo_valor= "";
    $valor = "0123456789";
    srand((double)microtime()*1000000);
    for ($i=0; $i<15; $i++){
        $novo_valor.= $valor[rand()%strlen($valor)];
    }
    return $novo_valor;
}

function aleatorioTermo(){
    $novo_valor= "";
    $valor = "0123456789";
    srand((double)microtime()*1000000);
    for ($i=0; $i<4; $i++){
        $novo_valor.= $valor[rand()%strlen($valor)];
    }
    return $novo_valor;
}

function unique_multidim_array_vals($array) {

    $temp_array = array();
    $key_array = array();

   	//echo "<pre>";print_r($array);
    //exit();
    foreach($array as $k => $k2) {
        foreach ($k2 as $k3 => $v3) {
            if (!in_array($v3, $key_array)) {
                $key_array[]=$v3;
                $temp_array[$k][$k3] = $v3;
            }
        }

    }

    //echo "<pre>";print_r($temp_array);
    return $temp_array;
}

function verificaSeGestaoAnterior($anoGestaoAtual,$anoCompetencia)
{
	if($anoCompetencia<$anoGestaoAtual)
	{
		return true;
	}else{
		return false;
	}
}

// Ordem crescente
function cmp($a, $b) {
    return $a['nome'] > $b['nome'];
}

/**
 * Formats a JSON string for pretty printing
 *
 * @param string $json The JSON to make pretty
 * @param bool $html Insert nonbreaking spaces and <br />s for tabs and linebreaks
 * @return string The prettified output
 * @author Jay Roberts
 */
function _format_json($json, $html = false) {
    $tabcount = 0;
    $result = '';
    $inquote = false;
    $ignorenext = false;
    if ($html) {
        $tab = "&nbsp;&nbsp;&nbsp;";
        $newline = "<br/>";
    } else {
        $tab = "\t";
        $newline = "\n";
    }
    for($i = 0; $i < strlen($json); $i++) {
        $char = $json[$i];
        if ($ignorenext) {
            $result .= $char;
            $ignorenext = false;
        } else {
            switch($char) {
                case '{':
                    $tabcount++;
                    $result .= $char . $newline . str_repeat($tab, $tabcount);
                    break;
                case '}':
                    $tabcount--;
                    $result = trim($result) . $newline . str_repeat($tab, $tabcount) . $char;
                    break;
                case ',':
                    $result .= $char . $newline . str_repeat($tab, $tabcount);
                    break;
                case '"':
                    $inquote = !$inquote;
                    $result .= $char;
                    break;
                case '\\':
                    if ($inquote) $ignorenext = true;
                    $result .= $char;
                    break;
                default:
                    $result .= $char;
            }
        }
    }
    return $result;
}
?>
