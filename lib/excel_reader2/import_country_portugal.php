<?php
header('Content-Type: text/html; charset=utf-8');

error_reporting(E_ALL ^ E_NOTICE);
require_once("../../model/conexaoClass.php");
require_once 'excel_reader2.php';

$data = new Spreadsheet_Excel_Reader("divisao_territorial.xls");

function titleCase($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("de", "da", "dos", "das", "do", "I", "II", "III", "IV", "V", "VI"))
{
    /*
     * Exceptions in lower case are words you don't want converted
     * Exceptions all in upper case are any words you don't want converted to title case
     *   but should be converted to upper case, e.g.:
     *   king henry viii or king henry Viii should be King Henry VIII
     */
    $string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
    foreach ($delimiters as $dlnr => $delimiter) {
        $words = explode($delimiter, $string);
        $newwords = array();
        foreach ($words as $wordnr => $word) {
            if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtoupper($word, "UTF-8");
            } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
                // check exceptions list for any words that should be in upper case
                $word = mb_strtolower($word, "UTF-8");
            } elseif (!in_array($word, $exceptions)) {
                // convert to uppercase (non-utf8 only)
                $word = ucfirst($word);
            }
            array_push($newwords, $word);
        }
        $string = join($delimiter, $newwords);
    }//foreach
    return $string;
}

function insertCity($estado,$uf,$nome){

    $sql = "INSERT INTO cidade (estado, uf, nome) VALUES (:estado, :uf, :nome)";

    //echo $sql;

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':estado', $estado, PDO::PARAM_INT);
    $sth->bindParam(':uf', $uf, PDO::PARAM_STR);
    $sth->bindParam(':nome', $nome, PDO::PARAM_STR);

    if ($c->run($sth,true)){
        return true;
    }else{
        return false;
    }
}

function insertState($pais,$uf,$nome){

    $sql = "INSERT INTO estado (pais, uf, nome) VALUES (:pais, :uf, :nome)";

    //echo $sql;

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':pais', $pais, PDO::PARAM_INT);
    $sth->bindParam(':uf', $uf, PDO::PARAM_STR);
    $sth->bindParam(':nome', $nome, PDO::PARAM_STR);

    if ($c->run($sth,true)){
        return true;
    }else{
        return false;
    }

}

function insertFreguesia($cidade, $nome){

    $sql = "INSERT INTO freguesia (cidade, nome) VALUES (:cidade, :nome)";

    //echo $sql;

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':cidade', $cidade, PDO::PARAM_INT);
    $sth->bindParam(':nome', $nome, PDO::PARAM_STR);

    if ($c->run($sth,true)){
        return true;
    }else{
        return false;
    }
}

function selectState($nameState){

    $sql = "SELECT id FROM  estado WHERE nome=:nome";

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':nome',  $nameState, PDO::PARAM_STR);

    $resultado = $c->run($sth);
    if ($resultado){
        foreach ($resultado as $vetor){
            $idState = $vetor['id'];
        }
        return $idState;
    }else{
        return false;
    }
}

function selectCity($nameCity){

    $sql = "SELECT id FROM  cidade WHERE nome=:name";

    //echo "sql city: " . $sql." + " . $nameCity . "<br>";

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':name',  $nameCity, PDO::PARAM_STR);

    $resultado = $c->run($sth);
    if ($resultado){
        foreach ($resultado as $vetor){
            $idState = $vetor['id'];
        }
        return $idState;
    }else{
        return false;
    }
}

function importState($data){
    $pote=0;
    $estado="";
    for($i=2; $i <= $data->rowcount($sheet_index=0); $i++ ){

        if($estado != $data->val($i, 2)){

            if(insertState(2,"", trim(titleCase($data->val($i, 2))))){
                echo "Estado: " . titleCase($data->val($i, 2)) . "<br>";
            }

            $estado = $data->val($i, 2);
            $pote++;
        }

    }
    echo "Total de registros populados:".$pote;
}

function importCity($data){
    $pote=0;
    $cidade="";
    for($i=2; $i <= $data->rowcount($sheet_index=0); $i++ ){

        if($cidade !=  trim(titleCase(utf8_encode($data->val($i, 4))))) {

            $idState = selectState(trim(titleCase(utf8_encode($data->val($i, 2)))));

            if (insertCity($idState, "", trim(titleCase(utf8_encode($data->val($i, 4)))))) {
                echo "Cidade: [". $idState . "] " . trim(titleCase(utf8_encode($data->val($i, 4)))) . "<br>";
            }

            $cidade = trim(titleCase(utf8_encode($data->val($i, 4))));
            $pote++;
        }

    }
    echo "Total de registros populados:".$pote;
}

function importFreguesia($data){
    $pote=0;
    $cidade="";
    for($i=2; $i <= $data->rowcount($sheet_index=0); $i++ ){

        if($cidade != trim(titleCase(utf8_encode($data->val($i, 4))))) {
            $idCity = selectCity(trim(titleCase(utf8_encode($data->val($i, 4)))));
        }

        if (insertFreguesia($idCity, trim(titleCase(utf8_encode($data->val($i, 6)))))) {
            echo "Freguesia: [". $idCity . "] " . trim(titleCase(utf8_encode($data->val($i, 6)))) . "<br>";
        }

        $cidade = trim(titleCase(utf8_encode($data->val($i, 4))));
        $pote++;

    }
    echo "Total de registros populados:".$pote;
}

//importState($data);
//importCity($data);
//importFreguesia($data);
echo "Import off.";