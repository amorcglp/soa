<?php
header('Content-Type: text/html; charset=utf-8');

require_once("../model/conexaoClass.php");

function insertCity($estado,$uf,$nome){

    $sql = "INSERT INTO cidade (estado, uf, nome) VALUES (:estado, :uf, :nome)";

    //echo $sql;

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':estado', $estado, PDO::PARAM_INT);
    $sth->bindParam(':uf', $uf, PDO::PARAM_STR);
    $sth->bindParam(':nome', $nome, PDO::PARAM_STR);

    if ($c->run($sth,true)){
        return true;
    }else{
        return false;
    }
}

function selectState($nomeState){

    $sql = "SELECT id,nome FROM  estado WHERE nome=:nome";

    $c = new conexaoSOA();
    $con = $c->openSOA();
    $sth = $con->prepare($sql);

    $sth->bindParam(':nome',  $nomeState, PDO::PARAM_STR);

    $resultado = $c->run($sth);
    if ($resultado){
        foreach ($resultado as $vetor){
            $idState = $vetor['id'];
        }
        return $idState;
    }else{
        return false;
    }
}

error_reporting(E_ALL ^ E_NOTICE);
require_once 'excel_reader2.php';
$data = new Spreadsheet_Excel_Reader("divisao_territorial.xls");
$pote=0;
$estado="";
$cidade="";
for($i=2; $i <= $data->rowcount($sheet_index=0); $i++ ){

    if($cidade != $data->val($i, 4)) {

        $idState = selectState($data->val($i, 2));

        if (insertCity($idState, "", utf8_encode($data->val($i, 4)))) {
            echo "Cidade: [". $idState . "] " . utf8_encode($data->val($i, 4)) . "<br>";
        }

        $cidade = $data->val($i, 4);
        $pote++;
    }

}

echo "Total de registros populados:".$pote;
?>