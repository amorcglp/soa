<?php

ini_set('memory_limit', '-1');

if(realpath('../../model/wsClass.php')){
	require_once '../../model/wsClass.php';
}else{
	if(realpath('../model/wsClass.php')){
		require_once '../model/wsClass.php';	
	}else{
		require_once './model/wsClass.php';
	}
}

function retornaCodigoAfiliacao($seqCadast,$tipoDeCodigo=null) {

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaDadosMembroPorSeqCadast';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
        'SeqCadast' => $seqCadast
    );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');
    //echo "<pre>";print_r($return);
    $codigoAfiliacao = "0";
    if (count($return->result) > 0) {
           
        $codigoAfiliacao = $return->result[0]->fields->fCodRosacruz;   
        if($tipoDeCodigo==2)
        {
            $codigoAfiliacao = $return->result[0]->fields->fCodOgg;
        }    
    }
    
    return $codigoAfiliacao;
}

function retornaNomeCompleto($seqCadast) {

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaDadosMembroPorSeqCadast';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
        'SeqCadast' => $seqCadast
    );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    $nome = "";
    if (count($return->result) > 0) {
        
        $nome  = $return->result[0]->fields->fNomCliente;
      
    }
    return $nome;
}

function retornaDataNascimento($seqCadast) {

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaDadosMembroPorSeqCadast';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
        'SeqCadast' => $seqCadast
    );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    $dataNascimento = "0";
    if (count($return->result) > 0) {
        $dataNascimento = $return->result[0]->fields->fDatNascimento;  
    }
    return $dataNascimento;
}

?>