<?php
require_once 'phpmailer/class.phpmailer.php';
require_once 'functions.php';
/**
 * Verifica se a variável é um html sem scripts
 * maliciosos
 * @param type $t
 * @return boolean
 */
function cleanHtml($h)
{
    $h = str_replace("<script>","",$h);
    $h = str_replace("</script>","",$h);
 
    return $h;
}
/**
 * Verifica se o input da aplicação está sujo
 * @param type $dirty
 * @return boolean
 */
function validaSegurancaInput($dirty)
{
    $v=false;
    $y=0;
    for($i=0;$i<count($dirty);$i++)
    {
        $arrY=$dirty[$i];
        
        $valor = isset($arrY[0])? str_replace(' ', '',$arrY[0]):null;
        $tipo  = isset($arrY[1])?$arrY[1]:null;
        
        //UTF-8
        if($valor!=""&&$valor!=null)
        {    
            if(mb_detect_encoding($valor.'x', 'UTF-8, ISO-8859-1'))
            {        
                if(is_string($valor))
                {    
                    $valor =  utf8_decode($valor);
                }
            }
        }
        $valor = removeAcentos($valor);
        
        //Desconsiderar : 
        $valor = str_replace("/","",$valor);
        $valor = str_replace(":","",$valor);
        $valor = str_replace(".","",$valor);
        $valor = str_replace("-","",$valor);
        $valor = str_replace("?","",$valor);
        $valor = str_replace("#","",$valor);
        $valor = str_replace("&","",$valor);
        $valor = str_replace("=","",$valor);
        $valor = str_replace("+","",$valor);
        $valor = str_replace("_","",$valor);
        $valor = str_replace(",","",$valor);
        $valor = str_replace("<","",$valor);
        $valor = str_replace(">","",$valor);
        $valor = str_replace("$","",$valor);
        $valor = str_replace("%","",$valor);
        $valor = str_replace("(","",$valor);
        $valor = str_replace(")","",$valor);
        $valor = str_replace(";","",$valor);
        $valor = str_replace("|","",$valor);
        $valor = str_replace("@","",$valor);
        $valor = str_replace("*","",$valor);
        $valor = str_replace("[","",$valor);
        $valor = str_replace("]","",$valor);
        $valor = str_replace("\\","",$valor);
        
        if(is_string($valor))
        {    
        
            if(trim($valor)=="")
            {
                //Pular validação desse campo
            }else{    

                if(!isset($valor)||!isset($tipo))
                {
                    echo "<script>alert('Valores e tipos sao null! ".print_r($dirty)."');</script>";
                    $v=true;
                }    
                if($tipo=="texto")
                {
                    if(!ctype_alpha($valor))
                    {
                        echo "<script>alert('Para os campos de texto e necessario informar apenas texto! E voce informou ".$valor."');</script>";
                        $v=true;
                    }    
                }
                if($tipo=="textoNumero")
                {
                    if(!ctype_alnum(utf8_decode($valor)))
                    {
                        echo "<script>alert('Para os campos de texto ou numero e necessario informar apenas texto ou numero! E voce informou ".$valor."');</script>";
                        $v=true;
                    }    
                }
                if($tipo=="inteiro")
                {
                    if(!is_numeric($valor))
                    {
                        echo "<script>alert('Para os campos numerais e necessario informar apenas numero! E voce informou ".$valor."');</script>";
                        $v=true;
                    }    
                }
            }   
        }
        $i++;
    }
    return $v;
}
/**
 * Filtra output da aplicação
 * @param type $dirty
 * @return output
 */
function validaSegurancaOutput($dirty)
{
    $output = htmlentities($dirty, ENT_QUOTES, 'UTF-8');
    return $output;
}

/**
 * Envia E-mail avisando violação
 * @param type $ip
 * @return boolean
 */
function enviaEmailViolacaoSeguranca($ip)
{
    //Enviar email 
    $texto = '<div>
    <div dir="ltr">
            <table class="ecxbody-wrap"
                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                    <tbody>
                            <tr
                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                    <td
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                            valign="top"></td>
                                    <td class="ecxcontainer" width="600"
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                            valign="top">
                                            <div class="ecxcontent"
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                    <table class="ecxmain" width="100%" cellpadding="0"
                                                            cellspacing="0"
                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                            <tbody>
                                                                    <tr
                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <td class="ecxcontent-wrap"
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                    valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                    style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                    alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                            <br>
                                                                            <br>
                                                                            <br>
                                                                                    <div style="text-align: left;"></div>
                                                                                    <table width="100%" cellpadding="0" cellspacing="0"
                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                            <tbody>
                                                                                                    <tr
                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                            <td class="ecxcontent-block"
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                    valign="top">Equipe de TI essa é uma mensagem automática,</td>
                                                                                                    </tr>
                                                                                                    <tr
                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                            <td class="ecxcontent-block"
                                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                    valign="top">Houve uma violação de segurança no sistema! </td>
                                                                                                    </tr>';
                                                                                                    
                                                            $texto .= '<tr
                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                            <td class="ecxcontent-block"
                                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                    valign="top">';
                                                            $texto .="IP:".$ip;
                                                            $texto .= '        </td>
                                                                                                    </tr>';
                                                            $texto .= '     			<tr
                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                            <td class="ecxcontent-block"
                                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                    valign="top"><font color="red">Essa é uma mensagem automática, não há necessidade de resposta!</font></td>
                                                                                                    </tr>
                                                                                                    <tr
                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                            <td class="ecxcontent-block"
                                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                    valign="top">Em caso de dúvida, por favor entre em contato: atendimentoportal@amorc.org.br</td>
                                                                                                    </tr>
                                                                                                    <tr
                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                            <td class="ecxcontent-block"
                                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                    valign="top"> 
                                                                                                                    Ordem Rosacruz, AMORC – Grande Loja da Jurisdição de Língua Portuguesa<br>
                                                                                                    Rua Nicarágua, 2620, Curitiba – Paraná<br>
                                                                                                    Telefone: (+55) 041- 3351-3000</td>
                                                                                                    </tr>
                                                                                            </tbody>
                                                                                    </table></td>
                                                                    </tr>
                                                            </tbody>
                                                    </table>
                                                    <div class="footer"
                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                            <div style="text-align: left;"></div>
                                                            <table width="100%"
                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                    <tbody>
                                                                            <tr
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                    <td class="ecxaligncenter ecxcontent-block"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                            align="center" valign="top">AMORC-GLP '.date('Y').'</td>
                                                                            </tr>
                                                                    </tbody>
                                                            </table>
                                                    </div>
                                            </div>
                                            <div style="text-align: left;"></div></td>
                                    <td
                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                            valign="top"></td>
                            </tr>
                    </tbody>
            </table>
            </div>
    </div>';
    //echo $texto;
   

    $servidor="smtp.office365.com";
    $porta="587";
    $usuario="noresponseglp@amorc.org.br";
    $senha="@w2xpglp";

    //echo $senha;
    #Disparar email
    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPSecure = "tls";
    $mail->SMTPAuth = true;
    $mail->Host = $servidor;	// SMTP utilizado
    $mail->Port = $porta;
    $mail->Username = $usuario;
    $mail->Password = $senha;
    $mail->From = 'noresponseglp@amorc.org.br';
    $mail->AddReplyTo("atendimento@amorc.org.br","Antiga e Mística Ordem Rosacruz - AMORC");
    $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
    //$mail->AddAddress("luis.fernando@amorc.org.br");
    //$mail->AddAddress("tatiane.jesus@amorc.org.br");
    //$mail->AddAddress("lucianob@amorc.org.br");
    //$mail->AddAddress("samufcaldas@hotmail.com.br");
    //$mail->AddAddress("samuel.caldas@hotmail.com.br");
    //$mail->AddAddress("luciano.bastos@gmail.com");
    $mail->AddAddress("braz@amorc.org.br");
    //$mail->AddAddress("amigoeterno10@hotmail.com");
    //$mail->AddAddress("ti@amorc.org.br");
    //$mail->AddAddress($vetorUsu['emailUsuario']);
    $mail->isHTML(true);
    $mail->CharSet = 'utf-8';
    //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
    $mail->Subject = 'Violação de Segurança - SOA';
    $mail->Body = $texto;
    $mail->AltBody = strip_tags($texto); 

    //Enviar e-mail
    $enviado = $mail->Send();
}
/**
 * Verifica se existe tentativa de 
 * falsificação de variáveis
 * @param type $arr
 * @return boolean
 */
function validaSegurancaRequest($arr)
{
    //echo "<pre>";print_r($arr);
    for($i=0;$i<count($arr);$i++)
    {
        if(array_key_exists($arr[$i],$_GET))
        {
            echo "GET";
            return true;
        }
        if(array_key_exists($arr[$i],$_COOKIE))
        {
            echo "COOKIE";
            return true;
        }
        if(array_key_exists($arr[$i],$_FILES))
        {
            echo "FILES";
            return true;
        }
        if(array_key_exists($arr[$i],$_REQUEST)&&!array_key_exists($arr[$i],$_POST))
        {
            echo "REQUEST";
            return true;
        }
    }
    return false;
}        
?>