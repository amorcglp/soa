<?php
// Funções Ruan

@include_once('model/geradorPdfClass.php');
@include_once('../model/geradorPdfClass.php');

function adicionarMarcaDaAguaPdf($arquivo, $usuario, $pagina, $opcoes) {
    $pdf = new GeradorPdf();
    $topo = $meio = $rodape = false;
    $adicionaMarca = false; // necessita configurar os parâmetros em $opcoes
    
    $numeroPaginas = $pdf->setSourceFile($arquivo);
    
    // Opções para adicionar marca
    if($opcoes != null) {
        $adicionaMarca = true;
        $opcoes= explode("&", $opcoes);
        for($i = 0; $i < count($opcoes); $i++) {
            switch($opcoes[$i]) {
                case "topo": $topo= true; break;
                case "meio": $meio= true; break;
                case "rodape": $rodape= true; break;
            }
        }
    }
    
    if($numeroPaginas>=1) {
        $ordemDeImpressao = $dado = array();
        $pagina = ($pagina==null) ? "1-".$numeroPaginas : $pagina;
        if(preg_match_all("/^(\d+)-(\d+)$/", $pagina, $dado, PREG_SET_ORDER)) {
            
            $inicio = (int) $dado[0][1];
            $fim = (int) $dado[0][2];
            $sequencia = $inicio <= $fim ? true : false;
            
            if($sequencia) {
                // Ordem crescente
                for($i = $inicio, $j = 0; $i <= $fim; $i++, $j++) {
                    $ordemDeImpressao[$j] = $i;
                }
            }
            else {
                // Ordem decrescente
                for($i = $inicio, $j = 0; $i >= $fim; $i--, $j++) {
                    $ordemDeImpressao[$j] = $i;
                }
            }
        }
        else {
            $dado = explode(',', $pagina);
            // Ordem por seleção
            for($i = 0; $i < count($dado); $i++) {
                $ordemDeImpressao[$i] = $dado[$i];
            }
        }
        // Configura a(s) página(s)
        for($i = 0; $i < count($ordemDeImpressao); $i++) {
            if($adicionaMarca)
                configurarPaginaPdf($pdf, $usuario, $ordemDeImpressao[$i],true, $topo, $meio, $rodape);
            else
                configurarPaginaPdf($pdf, $usuario, $ordemDeImpressao[$i],false);
        }
        
        $token = bin2hex(openssl_random_pseudo_bytes(8));
        // Gera o pdf de saída
        $pdf->Output("output/".$token.".pdf");
        
        return $token;
    }
    // Erro
    return null;
}

function configurarPaginaPdf($pdf,$usuario,$pagina,$addMarca,
                             $addMarcaTopo=true,$addMarcaMeio=true,$addMarcaRodape=true) {
    $tplIdx = $pdf->importPage($pagina);
    $specs = $pdf->getTemplateSize($tplIdx);
    $tipoPagina = $specs['h'] > $specs['w'] ? 'P' : 'L';
    
    $pdf->addPage($tipoPagina);
    $pdf->useTemplate($tplIdx, null, null, $specs['w'], $specs['h'], true);
    
    if($addMarca) {
        $x1 = $y1 = $x2 = $y2 = array();
        $pdf->SetFont("Courier", "B", 14);
        
        // Orientação para inserção de texto
        if($tipoPagina == 'P') {
            $x1 = array(50,50,50,50);
            $y1 = array(150,160,170,180);
            $x2 = array(8,8);
            $y2 = array(285,290);
        }
        else {
            $x1 = array(100,100,100,100);
            $y1 = array(130,140,150,160);
            $x2 = array(8,8);
            $y2 = array(200,205);
        }
        
        // Topo
        if($addMarcaTopo) {
            //  $pdf->SetTextColor(255,2,2);
            //  $pdf->SetAlpha(0.3);
            $pdf->SetFont("Courier", "", 10);
            //$pdf->Text(4,5, utf8_decode("Autenticação Digital - SOA Web"));
            $pdf->Text(4,9, utf8_decode($usuario->getNomeUsuario()));
            $pdf->Text(4,5,  "Impresso em ".date("d/m/Y, ")
                .utf8_decode("às ").date("H:i:s - ")."Documento de uso da AMORC-GLP - SOA Web");
            $pdf->SetFont("Courier", "B", 14);
            //  $pdf->SetTextColor(0,0,0);
            //  $pdf->SetAlpha(1);
        }
        
        // Meio
        if($addMarcaMeio) {
            $pdf->Rotate(10);
            $pdf->SetAlpha(0.3);
            $pdf->SetFont("Courier", "", 14);
            $pdf->Text($x1[0],$y1[0], utf8_decode("Usuário: ").$usuario->getNomeUsuario());
            $pdf->Text($x1[1],$y1[1], "Email: ".$usuario->getEmailUsuario());
            $pdf->Text($x1[2],$y1[2], utf8_decode("Código de Afiliação: ").$usuario->getCodigoDeAfiliacaoUsuario());
            //$pdf->Text($x1[3],$y1[3], "IP:".$_SERVER['REMOTE_ADDR']);
            $pdf->Rotate(0);
            $pdf->SetAlpha(1);
        }
        
        // Rodapé
        if($addMarcaRodape) {
            $pdf->SetAlpha(0.3);
            $pdf->SetFont("Courier", "", 10);
            $pdf->Text($x2[0],$y2[0], $usuario->getNomeUsuario());
            //$pdf->Text($x2[1],$y2[1], date("d/m/Y - H:i:s - ", time())."Documento de uso da AMORC-GLP: SOA Web");
            $pdf->Text($x2[1],$y2[1], "Impresso em ".date("d/m/Y, ")
                .utf8_decode("às ").date("H:i:s - ")."Documento de uso da AMORC-GLP");
            $pdf->SetFont("Courier", "", 14);
            $pdf->SetAlpha(1);
        }
    }
}

/*
 *  doc:        nome do documento a ser impresso;
 *  impressao:  objeto da classe ImpressaoController;
 *  documento:  objeto da classe DocumentoController;
 *  usuario:    objeto da classe UsuarioController;
 *  id:         ID (SeqCadast) do usuário.
 */

function impPdf($doc, $impressao, $documento, $usuario, $id, $opt=null, $caminho="") {
    if($impressao->cadastroImpressao()) {
        // O arquivo gerado para impressão é armazenado nessa pasta
        // /impress/ManualAdministrativo/
        $doc = "impress/" . $caminho . $doc . "." . $documento->getDocumentoExtensao();
        
        if($documento->getDocumentoExtensao() == "pdf") {
            return adicionarMarcaDaAguaPdf(
                $doc, $usuario->buscaUsuario($id), $documento->getPaginaImpressao(), $opt);
        }
        // Erro (não é pdf?)
        return null;
    }
}

/*
 *  doc:        nome do documento a ser impresso;
 *  impressao:  objeto da classe ImpressaoController;
 *  documento:  objeto da classe DocumentoController;
 *  usuario:    objeto da classe UsuarioController;
 *  id:         ID (SeqCadast) do usuário.
 */

function impPdfSemCadastro($t,$nomeOa, $pdfCopia,$opt=null) {

        return adicionarMarcaDaAguaPdf2($t,$pdfCopia, $nomeOa,null,$opt);
}

function adicionarMarcaDaAguaPdf2($t,$arquivo, $nomeOa,$pagina, $opcoes) {

    $pdf = new GeradorPdf();

    $topo = $meio = $rodape = false;
    $adicionaMarca = false; // necessita configurar os parâmetros em $opcoes

    $numeroPaginas = $pdf->setSourceFile($arquivo);

    // Opções para adicionar marca
    if($opcoes != null) {
        $adicionaMarca = true;
        $opcoes= explode("&", $opcoes);
        for($i = 0; $i < count($opcoes); $i++) {
            switch($opcoes[$i]) {
                case "topo": $topo= true; break;
                case "meio": $meio= true; break;
                case "rodape": $rodape= true; break;
            }
        }
    }

    if($numeroPaginas>=1) {
        $ordemDeImpressao = $dado = array();
        $pagina = ($pagina==null) ? "1-".$numeroPaginas : $pagina;
        if(preg_match_all("/^(\d+)-(\d+)$/", $pagina, $dado, PREG_SET_ORDER)) {

            $inicio = (int) $dado[0][1];
            $fim = (int) $dado[0][2];
            $sequencia = $inicio <= $fim ? true : false;

            if($sequencia) {
                // Ordem crescente
                for($i = $inicio, $j = 0; $i <= $fim; $i++, $j++) {
                    $ordemDeImpressao[$j] = $i;
                }
            }
            else {
                // Ordem decrescente
                for($i = $inicio, $j = 0; $i >= $fim; $i--, $j++) {
                    $ordemDeImpressao[$j] = $i;
                }
            }
        }
        else {
            $dado = explode(',', $pagina);
            // Ordem por seleção
            for($i = 0; $i < count($dado); $i++) {
                $ordemDeImpressao[$i] = $dado[$i];
            }
        }
        // Configura a(s) página(s)
        for($i = 0; $i < count($ordemDeImpressao); $i++) {
            if($adicionaMarca)
                configurarPaginaPdf2($pdf, $nomeOa,$ordemDeImpressao[$i],true, $topo, $meio, $rodape);
            else
                configurarPaginaPdf2($pdf, $nomeOa,$ordemDeImpressao[$i],false);
        }


        // Gera o pdf de saída
        //echo "<br>testes/".$t.".pdf";
        $filePath="";
        $file_name = $t. '.pdf';
/*
        if(file_exists($file_name))
        {
            //echo "arquivo existe";
        }else {
            //echo "arquivo nao existe";
        }
        exit();
//echo $file_name;
*/
            if ($pdf->Output($file_name, 'D')) {
                return true;
            }else {
                return false;
            }


    }
    // Erro
    return null;
}

function configurarPaginaPdf2($pdf,$nomeOa,$pagina,$addMarca,
                             $addMarcaTopo=true,$addMarcaMeio=true,$addMarcaRodape=true) {
    $tplIdx = $pdf->importPage($pagina);
    $specs = $pdf->getTemplateSize($tplIdx);
    $tipoPagina = $specs['h'] > $specs['w'] ? 'P' : 'L';

    $pdf->addPage($tipoPagina);
    $pdf->useTemplate($tplIdx, null, null, $specs['w'], $specs['h'], true);

    if($addMarca) {
        $x1 = $y1 = $x2 = $y2 = array();
        $pdf->SetFont("Courier", "B", 14);

        // Orientação para inserção de texto
        if($tipoPagina == 'P') {
            //$x1 = array(50,50,50,50);
            $x1 = array(5,5,5,5);
            $y1 = array(150,160,170,180);
            $x2 = array(8,8);
            $y2 = array(285,290);
        }
        else {
            $x1 = array(100,100,100,100);
            //$x1 = array(100,100,100,100);
            $y1 = array(130,140,150,160);
            $x2 = array(8,8);
            $y2 = array(200,205);
        }

        // Topo
        if($addMarcaTopo) {
            //  $pdf->SetTextColor(255,2,2);
            //  $pdf->SetAlpha(0.3);
            $pdf->SetFont("Courier", "", 10);
            //$pdf->Text(4,5, utf8_decode("Autenticação Digital - SOA Web"));
            $pdf->Text(4,9, utf8_decode($nomeOa));
            $pdf->Text(4,5,  "Impresso em ".date("d/m/Y, ")
                .utf8_decode("às ").date("H:i:s - ")."Documento de uso confidencial");
            $pdf->SetFont("Courier", "B", 14);
            //  $pdf->SetTextColor(0,0,0);
            //  $pdf->SetAlpha(1);
        }


        /*
        $quebra=false;
        if(strlen($nomeOa)>20)
        {
            $quebra=true;
            $arr = explode(" ",$nomeOa);
            for($i=0;$i<(count($arr)-1);$i++) {
                if ($i==0)
                {
                    $nomeOa = $arr[$i];
                }else{
                    $nomeOa .= " ".$arr[$i];
                }
            }

            $ultimoNome = $arr[count($arr)-1];

        }
        */

        // Meio
        if($addMarcaMeio) {
            $pdf->Rotate(10);
            $pdf->SetAlpha(0.3);
            $pdf->SetFont("Courier", "", 20);
            $pdf->Text($x1[0],$y1[0], utf8_decode($nomeOa));

            //if(!$quebra) {
                $pdf->Text($x1[1], $y1[1], "Documento de uso confidencial");
                $pdf->Text($x1[2], $y1[2], "");
            /*
            }else{
                $pdf->Text($x1[1], $y1[1], "             ".utf8_decode($ultimoNome));
                $pdf->Text($x1[2], $y1[2], "Documento de uso confidencial");
            }*/
            //$pdf->Text($x1[3],$y1[3], "IP:".$_SERVER['REMOTE_ADDR']);
            $pdf->Rotate(0);
            $pdf->SetAlpha(1);
        }

        // Rodapé
        if($addMarcaRodape) {
            $pdf->SetAlpha(0.3);
            $pdf->SetFont("Courier", "", 10);
            $pdf->Text($x2[0],$y2[0], $nomeOa);
            //$pdf->Text($x2[1],$y2[1], date("d/m/Y - H:i:s - ", time())."Documento de uso da AMORC-GLP: SOA Web");
            $pdf->Text($x2[1],$y2[1], "Impresso em ".date("d/m/Y, ")
                .utf8_decode("às ").date("H:i:s - ")."Documento de uso confidencial");
            $pdf->SetFont("Courier", "", 14);
            $pdf->SetAlpha(1);
        }
    }
}