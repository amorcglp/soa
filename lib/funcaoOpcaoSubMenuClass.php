<?php
require_once ("conexaoClass.php");

class FuncaoOpcaoSubMenu {

    public $fk_idFuncao;
    public $fk_idOpcaoSubMenu;
    
    /* Funções GET e SET */

    function getFkIdFuncao() {
        return $this->fk_idFuncao;
    }

    function getFkIdOpcaoSubMenu() {
        return $this->fk_idOpcaoSubMenu;
    }

    function setFkIdFuncao($fk_idFuncao) {
        $this->fk_idFuncao = $fk_idFuncao;
    }

    function setFkIdOpcaoSubMenu($fk_idOpcaoSubMenu) {
        $this->fk_idOpcaoSubMenu = $fk_idOpcaoSubMenu;
    }

    public function cadastra() {
        
        $sql = "INSERT INTO funcao_opcaoSubMenu (`fk_idFuncao`, `fk_idOpcaoSubMenu`)
                                    VALUES (:idFuncao,
                                            :idOpcaoSubMenu)";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->getFkIdFuncao(), PDO::PARAM_INT);
        $sth->bindParam(':idOpcaoSubMenu', $this->getFkIdOpcaoSubMenu(), PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }
    
    public function remove() {
        
        $sql = "DELETE FROM funcao_opcaoSubMenu 
                        WHERE fk_idFuncao = :idFuncao
                        and fk_idOpcaoSubMenu = :idOpcaoSubMenu";

        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->fk_idFuncao(), PDO::PARAM_INT);
        $sth->bindParam(':idOpcaoSubMenu', $this->fk_idOpcaoSubMenu(), PDO::PARAM_INT);

        if ($c->run($sth,true)){
            return true;
        } else {
            return false;
        }
    }

    public function buscaPorFuncao() {
        
        $sql = "SELECT * FROM funcao_opcaoSubMenu
                                 WHERE `fk_idFuncao` = :idFuncao
                              ";
        $c = new conexaoSOA(); 
        $con = $c->openSOA();
        $sth = $con->prepare($sql);
        
        $sth->bindParam(':idFuncao', $this->getFkIdFuncao(), PDO::PARAM_INT);
        
        $resultado = $c->run($sth);

        if ($resultado) {
            return $resultado;
        } else {
            return false;
        }
    }
}
?>​