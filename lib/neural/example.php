<?php

require_once 'class_neuralnetwork.php';

// Create a new neural network with 3 input neurons,
// 4 hidden neurons, and 1 output neuron
$n = new NeuralNetwork(4, 4, 1);
$n->setVerbose(false);

// Add test-data to the network. In this case,
// we want the network to learn the 'XOR'-function
$n->addTestData(array (-1, -1, 1, 1), array (-1));
$n->addTestData(array (-1,  1, 1, 1), array ( 1));
$n->addTestData(array ( 1, -1, 1, 1), array ( 1));
$n->addTestData(array ( 1,  1, 1, 1), array (-1));

// we try training the network for at most $max times
$i=0;
$max = 3;

// train the network in max 1000 epochs, with a max squared error of 0.01
while (!($success = $n->train(1000, 0.01)) && ++$i<$max) {
    echo "Rodada $i: Nenhum sucesso...<hr />";
}

// print a message if the network was succesfully trained
if ($success) {
     $epochs = $n->getEpoch();
     echo "Sucesso em $epochs rodadas de treino!<hr />";
}

// in any case, we print the output of the neural network
echo "<h2>Resultado final</h2>";
for ($i = 0; $i < count($n->trainInputs); $i ++) {
    $output = $n->calculate($n->trainInputs[$i]);
     echo "<br />Grupo de Teste $i; ";
     echo "saída esperada = (".implode(", ", $n->trainOutput[$i]).") ";
     echo "saída da rede neural = (".implode(", ", $output).")\n";
}
?>