<?php

$autoload = 'vendor/autoload.php';

if(realpath($autoload)){
    require $autoload;
}else{
    if(realpath('../../vendor/autoload.php')){
        require '../../vendor/autoload.php';
    }else{
        require '../vendor/autoload.php';
    }
}

use Mailgun\Mailgun;
# Instantiate the client.
//$mgClient = new Mailgun('key-24e5c1eb843300db6922fee30623bcf0');
$mgClient = new Mailgun('key-24e5c1eb843300db6922fee30623bcf0');
/** Official */
//$domain = "mg.amorc.org.br";
$domain = "amorc.com.br";
/** Sandbox */
//$domain = "sandbox622f068a94064201a9d4021ad9bd9042.mailgun.org";
?>
