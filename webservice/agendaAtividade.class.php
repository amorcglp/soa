<?php
require_once '../model/conexaoClass.php';
require_once '../lib/functions.php';
/**
 * @author Braz Alves
 * @category classe
 * @package void
 * @version 1.0.0
 */
  class agendaAtividade{

  	public $cod_rosacr; /* Key Field */
	public $nom_client;
	private $ws;
	
	public function get($prop){
		return $this->$prop;
	}

	public function set($prop,$val){
		$this->$prop = $val;
	}

	public function __construct($cod_rosacr = null){
		//$this->ws = new Ws();	
		//$bd = mysql_select_db('test');
		if($cod_rosacr != null){
			$ler = array();
			$sql = "select * from CADASTRO where cod_rosacr='".$cod_rosacr."'";
			$query = mssql_query($sql);
			if ($query) {
				$contagem = mssql_num_rows($query);
				if ($contagem > 0) {
					while ($dados = mssql_fetch_array($query)) {
						$ler[] = $dados['cod_rosacr'];
						$ler[] = $dados['nom_client'];
					}
					$this->__populate($ler);
				}
				
			}
		}
	}

	private function __populate($line){
		$properties = '';
		$properties = get_class_vars(get_class($this));
		if(count($properties) > 0 ){
			$i=0;
			foreach($properties as $props=>$values){
				$this->$props = $line[$i];
				$i++;
			}
		}
	}

	public function emails($cod_rosacr){
		$emails = array();
		$sql = "select * from CADASTRO where cod_rosacr='".$cod_rosacr."'";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$emails[] = $dados['des_email'];
				}
			}
		}
		
		return implode('*',$emails);

	}
	
  	
	
        public function pesquisaAgendaAtividade($chave,$siglaOrganismoAfiliado=null,$anoCompetencia=null,$idPublico=null){
 
            $string="";
            
        
                if($chave=="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9@eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9@TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ@")
                {    
                    $sql = "SELECT 
                            a.*,
                            aet.nomeTipoAtividadeEstatuto,
                            o.siglaOrganismoAfiliado,
                            o.paisOrganismoAfiliado
                            FROM  agendaAtividade as a
                            inner join atividadeEstatuto_tipo as aet on idTipoAtividadeEstatuto = a.tipoAtividade
                            inner join organismoAfiliado as o on fk_idOrganismoAfiliado = idOrganismoAfiliado
                            WHERE 1=1 
                            and a.statusAgenda = 0
                            and (a.dataFinal >='".date("Y-m-d")."' or a.dataFinal = '0000-00-00') 
                            ";
                    
                            if($siglaOrganismoAfiliado!=null&&$siglaOrganismoAfiliado!="")
                            {                                
                                $sql .= " and o.siglaOrganismoAfiliado = :siglaOrganismoAfiliado";
                            }
                            if($anoCompetencia!=null&&$anoCompetencia!="")
                            {
                                $sql .= " and a.anoCompetencia = :anoCompetencia";
                            }
                            if($idPublico!=null&&$idPublico!=""&&$idPublico!=3)
                            {
                                $sql .= " and a.fk_idPublico = :idPublico";
                            }
                            /*
                            if($nomeTipoAtividadeEstatuto!=null&&$nomeTipoAtividadeEstatuto!="")
                            {
                                $sql .= " and aet.nomeTipoAtividadeEstatuto like :nomeTipoAtividadeEstatuto";
                            }
                            if($local!=null&&$local!="")
                            {
                                $sql .= " and a.local like :local";
                            }
                            if($diasHorarios!=null&&$diasHorarios!="")
                            {
                                $sql .= " and a.diasHorarios like :diasHorarios";
                            }
                            if($dataInicial!=null&&$dataInicial!="")
                            {
                                $sql .= " and a.dataInicial like :dataInicial";
                            }
                            if($dataFinal!=null&&$dataFinal!="")
                            {
                                $sql .= " and a.dataFinal like :dataFinal";
                            }
                            if($horaInicial!=null&&$horaInicial!="")
                            {
                                $sql .= " and a.horaInicial like :horaInicial";
                            }
                            if($horaFinal!=null&&$horaFinal!="")
                            {
                                $sql .= " and a.horaFinal like :horaFinal";
                            }
                            if($tag!=null&&$tag!="")
                            {
                                $sql .= " and a.tag like :tag";
                            }
                             */
                            
                             /* 
                            if($observacoes!=null&&$observacoes!="")
                            {
                                $sql .= " and a.observacoes like :observacoes ";  
                            }
                            if($tipoAtividade!=null&&$tipoAtividade!="")
                            {
                                $sql .= " and aet.nomeTipoAtividadeEstatuto like :tipoAtividade ";  
                            }
                            */
                    
                    $sql .= " group by siglaOrganismoAfiliado,idAgendaAtividade "
                            . " order by tipoAtividade,anoCompetencia ";

                    //echo $sql;

                    $c = new conexaoSOA(); 
                    $con = $c->openSOA();
                    $sth = $con->prepare($sql);

                    if($siglaOrganismoAfiliado!=null&&$siglaOrganismoAfiliado!="")
                    {
                        $sth->bindParam("siglaOrganismoAfiliado", $siglaOrganismoAfiliado, PDO::PARAM_STR);
                    }
                    if($anoCompetencia!=null&&$anoCompetencia!="")
                    {
                        $sth->bindParam("anoCompetencia", $anoCompetencia, PDO::PARAM_STR);
                    }
                    if($idPublico!=null&&$idPublico!="")
                    {
                        //$anoCompetencia = '%'.$anoCompetencia.'%';
                        $sth->bindParam("idPublico", $idPublico, PDO::PARAM_INT);
                    }
                    /*
                    if($local!=null&&$local!="")
                    {
                        $local = '%'.$local.'%';
                        $sth->bindParam("local", $local, PDO::PARAM_STR);
                    }
                    if($diasHorarios!=null&&$diasHorarios!="")
                    {
                        $diasHorarios = '%'.$diasHorarios.'%';
                        $sth->bindParam("diasHorarios", $diasHorarios, PDO::PARAM_STR);
                    }
                    if($dataInicial!=null&&$dataInicial!="")
                    {
                        $dataInicial = '%'.$dataInicial.'%';
                        $sth->bindParam("dataInicial", $dataInicial, PDO::PARAM_STR);
                    }
                    if($dataFinal!=null&&$dataFinal!="")
                    {
                        $dataFinal = '%'.$dataFinal.'%';
                        $sth->bindParam("dataFinal", $dataFinal, PDO::PARAM_STR);
                    }
                    if($horaInicial!=null&&$horaInicial!="")
                    {
                        $horaInicial = '%'.$horaInicial.'%';
                        $sth->bindParam("horaInicial", $horaInicial, PDO::PARAM_STR);
                    }
                    if($horaFinal!=null&&$horaFinal!="")
                    {
                        $horaFinal = '%'.$horaFinal.'%';
                        $sth->bindParam("horaFinal", $horaFinal, PDO::PARAM_STR);
                    }
                    if($tag!=null&&$tag!="")
                    {
                        $tag = '%'.$tag.'%';
                        $sth->bindParam("tag", $tag, PDO::PARAM_STR);
                    }
                    
                    if($observacoes!=null&&$observacoes!="")
                    {
                        $observacoes = '%'.$observacoes.'%';
                        $sth->bindParam("observacoes", $observacoes, PDO::PARAM_STR);
                    }
                    if($tipoAtividade!=null&&$tipoAtividade!="")
                    {
                        $tipoAtividade = '%'.$tipoAtividade.'%';
                        $sth->bindParam("tipoAtividade", $tipoAtividade, PDO::PARAM_STR);
                    }
                     */
                    //exit();
                    $resultado = $c->run($sth);
                    //echo "total=>".count($resultado);
                    $i=0;
                    if (count($resultado)){
                        if($resultado)
                        {
                            /*
                            if(realpath('../../model/wsClass.php')){
                                require_once '../../model/wsClass.php';
                            }else{
                                if(realpath('../model/wsClass.php')){
                                        require_once '../model/wsClass.php';	
                                }else{
                                        require_once './model/wsClass.php';
                                }
                            }
                            */
                            $resultado['totalRegistros']=count($resultado);
                            $decoded = json_decode(strip_tags(json_encode(str_replace("<\/p>","",$resultado), true)));
                            /*
                            //echo "<pre>";print_r($decoded);
                            $y=0;
                            foreach($decoded as $i)
                            {
                                switch ($i->paisOrganismoAfiliado)
                                {
                                    case 1:
                                        $siglaPais = "BR";
                                        break;
                                    case 2:
                                        $siglaPais = "PT";
                                        break;
                                    case 3:
                                        $siglaPais = "AO";
                                        break;
                                    case 4:
                                        $siglaPais = "MZ";
                                        break;
                                    default :
                                        $siglaPais = "BR";
                                        break;
                                        
                                }
                                // Instancia a classe
                                $ws = new Ws();

                                // Nome do Método que deseja chamar
                                $method = 'RetornaRelacaoOA';
                                
                                //echo "sigla=>".$i->siglaOrganismoAfiliado."<br>";

                                // Parametros que serão enviados à chamada
                                $params = array('CodUsuario' => 'lucianob',
                                                                'IdeTipoOrganismo' => '',
                                                                'SigOrganismoAfiliado' => $i->siglaOrganismoAfiliado,
                                                                'IdeDescartarOaPorSituacao' => '',
                                                                'SigPaisOrganismo' => $siglaPais,
                                                                'SigRegiaoBrasil' => '',
                                                                'SigAgrupamentoRegiao' => ''
                                        );

                                // Chamada do método
                                $return = $ws->callMethod($method, $params, 'lucianob');
                                $obj = json_decode(json_encode($return),true);
                                $arrayOA = (array) $obj['result'][0]['fields']['fArrayOA'];
                                //echo "<pre>";print_r($arrayOA);
                                $id = (int) $i->idAgendaAtividade;
                                
                                $decoded = (array) $decoded;
                                
                                //echo $id;
                                
                                $fSigOrgafi = $arrayOA[0]['fields']['fSigOrgafi'];
                                $fNomClient = $arrayOA[0]['fields']['fNomClient'];
                                $fDesLograd = $arrayOA[0]['fields']['fDesLograd'];
                                $fNumEndere = $arrayOA[0]['fields']['fNumEndere'];
                                $fDesCompleLograd = $arrayOA[0]['fields']['fDesCompleLograd'];
                                $fNomBairro = $arrayOA[0]['fields']['fNomBairro'];
                                $fNomLocali = $arrayOA[0]['fields']['fNomLocali'];
                                $fSigUf = $arrayOA[0]['fields']['fSigUf'];
                                $fCodCep = $arrayOA[0]['fields']['fCodCep'];
                                $fDesEmail = $arrayOA[0]['fields']['fDesEmail'];
                                $fSigPaisEndere = $arrayOA[0]['fields']['fSigPaisEndere'];

                                $decoded['orcz']["fSigOrgafi"][$y] = trim($fSigOrgafi);    
                                $decoded['orcz']["fNomClient"][$y] = trim($fNomClient);
                                $decoded['orcz']["fDesLograd"][$y] = trim($fDesLograd);
                                $decoded['orcz']["fNumEndere"][$y] = trim($fNumEndere);
                                $decoded['orcz']["fDesCompleLograd"][$y] = trim($fDesCompleLograd);
                                $decoded['orcz']["fNomBairro"][$y] = trim($fNomBairro);
                                $decoded['orcz']["fNomLocali"][$y] = trim($fNomLocali);
                                $decoded['orcz']["fSigUf"][$y] = trim($fSigUf);
                                $decoded['orcz']["fCodCep"][$y] = trim($fCodCep);
                                $decoded['orcz']["fDesEmail"][$y] = trim($fDesEmail);
                                $decoded['orcz']["fSigPaisEndere"][$y] = trim($fSigPaisEndere);
                                $y++;
                                
                            } 
                             */
                            //echo "<pre>";print_r($decoded);
                            //echo json_encode($decoded);
                            return json_encode($decoded);
                        }    
                        return $string;
                    }else{
                        return "{\"totalRegistros\":\"0\"}";
                    }
                }else{
                    return "{\"msg\":\"Acesso negado\"}";
                }
        }
        
  /* METODOS DE INTEGRACAO COM O WEBSERVICE */
    static function str2Obj($str,$class){
	 	$ex = explode('*',$str);

	 	if(count($ex) > 0 ){
	 		foreach($ex as $ind=>$values){
			 $x = explode('=',$values);
			 if(count($x)> 1){
			 $class->set($x[0],$x[1]);
			 }
	 		}
	 	}
		return $class;
	 }
	 public function obj2Str($class) {

		$properties = get_class_vars(get_class($class));

		if(count($properties) > 0 ){
			$str = '';
			$i=0;
			foreach($properties as $props=>$values){
				$str.=$props.'='.$this->$props.'*';
			}
			return substr($str,0,strlen($str)-1);
		}
    }
  }

?>