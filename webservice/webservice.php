<?php

include('lib/nusoap.php');
include('agendaAtividade.class.php');
//include('cliente.class.php');
//include('EstouAqui.class.php');
//require '../connectOrcz.php';
// cria��o de uma inst�ncia do servidor
$server = new soap_server;
// inicializa o suporte a WSDL
$server->configureWSDL('Registros do WSDL','urn:server.clientes');
$server->wsdl->schemaTargetNamespace = 'urn:server.clientes';

/**
 * M�TODOS ORCZ
 */

$server->register('pesquisaAgendaAtividade', //nome do m�todo
array(
	'chave'=>'xsd:string',
        'siglaOrganismoAfiliado'=>'xsd:string',
        'anoCompetencia'=>'xsd:string',
        'idPublico'=>'xsd:string'
        /*
        'cepOrganismoAfiliado'=>'xsd:string',
        'enderecoOrganismoAfiliado'=>'xsd:string',
        'estado'=>'xsd:string',
        'cidade'=>'xsd:string',
        'bairroOrganismoAfiliado'=>'xsd:string',
        'local'=>'xsd:string',
        'diasHorarios'=>'xsd:string',
        'dataInicial'=>'xsd:string',
        'dataFinal'=>'xsd:string',
        'horaInicial'=>'xsd:string',
        'horaFinal'=>'xsd:string',
        'tag'=>'xsd:string',
        ,
        'observacoes'=>'xsd:string',
        'tipoAtividade'=>'xsd:string'*/
), //par�metros de entrada
array('return' => 'xsd:string'), //par�metros de sa�da
'urn:server.pesquisaAgendaAtividade', //namespace
'urn:server.pesquisaAgendaAtividade#pesquisaAgendaAtividade', //soapaction
'rpc', //style
'encoded', //use
'Pesquisa na agenda de atividades' //documenta��o do servi�o
);
/*
$server->wsdl->addComplexType('arr', 'complexType', 'struct', 'all','',
array(
  'nome'=>array('name'=>'nome','type'=>'xsd:string')
  ,'cod_rosacr'=>array('name'=>'cod_rosacr','type'=>'xsd:string')
  ,'data_inicial'=>array('name'=>'data_inicial','type'=>'xsd:string')
  ,'data_final'=>array('name'=>'data_final','type'=>'xsd:string')
 )
);
*/
	
	function pesquisaAgendaAtividade($chave,$siglaOrganismoAfiliado,$anoCompetencia,$idPublico) {
		$cliente = new AgendaAtividade();
		return $cliente->pesquisaAgendaAtividade($chave,$siglaOrganismoAfiliado,$anoCompetencia,$idPublico);
	}
	
	

// requisi��o para uso do servi�o
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>