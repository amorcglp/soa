<?php
require_once '../model/conexaoClass.php';
require_once '../lib/functions.php';
/**
 * @author Braz Alves
 * @category classe
 * @package void
 * @version 1.0.0
 */
  class atividade{

  	public $cod_rosacr; /* Key Field */
	public $nom_client;
	private $ws;
	
	public function get($prop){
		return $this->$prop;
	}

	public function set($prop,$val){
		$this->$prop = $val;
	}

	public function __construct($cod_rosacr = null){
		//$this->ws = new Ws();	
		//$bd = mysql_select_db('test');
		if($cod_rosacr != null){
			$ler = array();
			$sql = "select * from CADASTRO where cod_rosacr='".$cod_rosacr."'";
			$query = mssql_query($sql);
			if ($query) {
				$contagem = mssql_num_rows($query);
				if ($contagem > 0) {
					while ($dados = mssql_fetch_array($query)) {
						$ler[] = $dados['cod_rosacr'];
						$ler[] = $dados['nom_client'];
					}
					$this->__populate($ler);
				}
				
			}
		}
	}

	private function __populate($line){
		$properties = '';
		$properties = get_class_vars(get_class($this));
		if(count($properties) > 0 ){
			$i=0;
			foreach($properties as $props=>$values){
				$this->$props = $line[$i];
				$i++;
			}
		}
	}

	public function emails($cod_rosacr){
		$emails = array();
		$sql = "select * from CADASTRO where cod_rosacr='".$cod_rosacr."'";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$emails[] = $dados['des_email'];
				}
			}
		}
		
		return implode('*',$emails);

	}
	
  	
	
        public function pesquisaAtividadeIniciatica($pesquisa=null,$chave){
            $string="";
        
                if($chave=="ea5c6781f6566006ca864e4d30b92c56")
                {    
                    $sql = "SELECT *, c.nome as cidade, e.nome as estado FROM  atividadeIniciatica as a
                            inner join organismoAfiliado as o on fk_idOrganismoAfiliado = idOrganismoAfiliado
                            left join estado as e on e.id = o.fk_idEstado
                            inner join cidade as c on c.uf = e.uf and o.fk_idCidade = c.id";
                    if($pesquisa!=null&&$pesquisa!="")
                    {    
                    $sql .= "               WHERE 1=1 and  
                                            (o.nomeOrganismoAfiliado like :pesquisa"
                                            . " or o.siglaOrganismoAfiliado like :pesquisa"
                                            . " or o.cepOrganismoAfiliado like :pesquisa"
                                            . " or o.enderecoOrganismoAfiliado like :pesquisa"
                                            . " or e.nome like :pesquisa"
                                            . " or c.nome like :pesquisa"
                                            . " or o.bairroOrganismoAfiliado like :pesquisa"
                                            . " or a.tipoAtividadeIniciatica like :pesquisa"
                                            . " or a.localAtividadeIniciatica like :pesquisa"
                                            . " or DAY(a.dataRealizadaAtividadeIniciatica) like :pesquisa"
                                            . " or MONTH(a.dataRealizadaAtividadeIniciatica) like :pesquisa"
                                            . " or YEAR(a.dataRealizadaAtividadeIniciatica) like :pesquisa"
                                            . " or a.horaRealizadaAtividadeIniciatica like :pesquisa"
                                            . " or a.anotacoesAtividadeIniciatica like :pesquisa)";
                    }
                    $sql .= " group by idOrganismoAfiliado,idAtividadeIniciatica "
                            . " order by tipoAtividadeIniciatica,dataRealizadaAtividadeIniciatica,horaRealizadaAtividadeIniciatica ";

                    //echo $sql;

                    $c = new conexaoSOA(); 
                    $con = $c->openSOA();
                    $sth = $con->prepare($sql);
                    
                    if($pesquisa!=null&&$pesquisa!="")
                    {    
                        $pesquisa = '%".$pesquisa."%';
                        $sth->bindParam(':pesquisa', $pesquisa, PDO::PARAM_INT);
                    }

                    $resultado = $c->run($sth);

                    $i=0;
                    if (count($resultado)){
                        if($resultado)
                        {
                            $string .= "{\"totalRegistros\":".count($resultado)."";
                            if(count($resultado)>0)
                            {    
                                    $string .= ",\"arrayAtividadeIniciatica\":[";
                                    foreach($resultado as $vetor)
                                    {
                                        if($i==0)
                                        {    
                                            $string .= "{"
                                                . "\"idAtividadeIniciatica\":\"".$vetor['idAtividadeIniciatica']."\","
                                                . "\"grauAtividadeIniciatica\":\"".$vetor['tipoAtividadeIniciatica']."\","
                                                . "\"localAtividadeIniciatica\":\"".$vetor['localAtividadeIniciatica']."\","
                                                . "\"dataRealizadaAtividadeIniciatica\":\"".$vetor['dataRealizadaAtividadeIniciatica']."\","
                                                . "\"horaRealizadaAtividadeIniciatica\":\"".$vetor['horaRealizadaAtividadeIniciatica']."\","
                                                . "\"cidade\":\"".$vetor['cidade']."\","
                                                . "\"estado\":\"".$vetor['estado']."\","
                                                . "\"nomeOrganismoAfiliado\":\"".retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado'])."\","
                                                . "\"enderecoOrganismoAfiliado\":\"".$vetor['enderecoOrganismoAfiliado']."\","
                                                . "\"numeroOrganismoAfiliado\":\"".$vetor['numeroOrganismoAfiliado']."\","
                                                . "\"bairroOrganismoAfiliado\":\"".$vetor['bairroOrganismoAfiliado']."\","
                                                . "\"cepOrganismoAfiliado\":\"".$vetor['cepOrganismoAfiliado']."\","
                                                . "\"celularOrganismoAfiliado\":\"".$vetor['celularOrganismoAfiliado']."\","
                                                . "\"telefoneFixoOrganismoAfiliado\":\"".$vetor['telefoneFixoOrganismoAfiliado']."\","
                                                . "\"outroTelefoneOrganismoAfiliado\":\"".$vetor['outroTelefoneOrganismoAfiliado']."\""    
                                                . "}";
                                        }else{
                                            $string .= ",{"
                                                . "\"idAtividadeIniciatica\":\"".$vetor['idAtividadeIniciatica']."\","
                                                . "\"grauAtividadeIniciatica\":\"".$vetor['tipoAtividadeIniciatica']."\","
                                                . "\"localAtividadeIniciatica\":\"".$vetor['localAtividadeIniciatica']."\","
                                                . "\"dataRealizadaAtividadeIniciatica\":\"".$vetor['dataRealizadaAtividadeIniciatica']."\","
                                                . "\"horaRealizadaAtividadeIniciatica\":\"".$vetor['horaRealizadaAtividadeIniciatica']."\","
                                                . "\"cidade\":\"".$vetor['cidade']."\","
                                                . "\"estado\":\"".$vetor['estado']."\","
                                                . "\"nomeOrganismoAfiliado\":\"".retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado'])."\","
                                                . "\"enderecoOrganismoAfiliado\":\"".$vetor['enderecoOrganismoAfiliado']."\","
                                                . "\"numeroOrganismoAfiliado\":\"".$vetor['numeroOrganismoAfiliado']."\","
                                                . "\"bairroOrganismoAfiliado\":\"".$vetor['bairroOrganismoAfiliado']."\","
                                                . "\"cepOrganismoAfiliado\":\"".$vetor['cepOrganismoAfiliado']."\","
                                                . "\"celularOrganismoAfiliado\":\"".$vetor['celularOrganismoAfiliado']."\","
                                                . "\"telefoneFixoOrganismoAfiliado\":\"".$vetor['telefoneFixoOrganismoAfiliado']."\","
                                                . "\"outroTelefoneOrganismoAfiliado\":\"".$vetor['outroTelefoneOrganismoAfiliado']."\""
                                                . "}";
                                        }
                                        $i++;
                                    }
                                    $string .= "]";
                            }
                            $string .= "}";
                        }    
                        return $string;
                    }else{
                        return "{\"msg\":\"Nenhuma atividade encontrada\"}";
                    }
                }else{
                    return "{\"msg\":\"Acesso negado\"}";
                }
        }
        
  /* METODOS DE INTEGRACAO COM O WEBSERVICE */
    static function str2Obj($str,$class){
	 	$ex = explode('*',$str);

	 	if(count($ex) > 0 ){
	 		foreach($ex as $ind=>$values){
			 $x = explode('=',$values);
			 if(count($x)> 1){
			 $class->set($x[0],$x[1]);
			 }
	 		}
	 	}
		return $class;
	 }
	 public function obj2Str($class) {

		$properties = get_class_vars(get_class($class));

		if(count($properties) > 0 ){
			$str = '';
			$i=0;
			foreach($properties as $props=>$values){
				$str.=$props.'='.$this->$props.'*';
			}
			return substr($str,0,strlen($str)-1);
		}
    }
  }

?>