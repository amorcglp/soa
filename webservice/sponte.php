<?php

if(realpath('../vendor/autoload.php')) {
    require '../vendor/autoload.php';
}elseif(realpath('../../vendor/autoload.php'))
{
    require '../../vendor/autoload.php';
}elseif(realpath('./vendor/autoload.php')){

    require './vendor/autoload.php';
}elseif(realpath('vendor/autoload.php')){
    require 'vendor/autoload.php';
}else{
    require '../../../vendor/autoload.php';
}

use GuzzleHttp\Client;

$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'http://api.sponteeducacional.net.br/',
]);

try {

    //Parâmetros
    $nCodigoCliente="64061";
    $sToken="E2YEsXUf2S3B";
    $sNome="BRAZ DA TI - TESTE";
    $sMidia="Google Forms";
    $dDataNascimento="1985-02-03";
    $sCidade="Curitiba";
    $sBairro="Boa Vista";
    $sCEP="82560580";
    $sEndereco="Rua Desembargador Antonio Leopoldo dos Santos";
    $nNumeroEndereco="92";
    $sEmail="braz@amorc.org.br";
    $sTelefone="4133513092";
    $sCPF="04325385932";
    $sRG="90258990";
    $sCelular="41997296119";
    $sObservacao="Bloco 13 - Apto 14";
    $sSexo="M";
    $sProfissao="Analista de Sistemas";
    $sCidadeNatal="";

    //Confirmar com a Jiu
    $sRa="";
    $sNumeroMatricula="";
    $sSituacao="3";
    $sCursoInteresse="Curso de Esoterismo Ocidental";

    $method="InsertAlunos";

    $vars = array('nCodigoCliente' => $nCodigoCliente
    , 'sToken' => $sToken
    , 'sNome' => $sNome
    , 'sMidia' => $sMidia
    , 'dDataNascimento' => $dDataNascimento
    , 'sCidade' => $sCidade
    , 'sBairro' => $sBairro
    , 'sCEP' => $sCEP
    , 'sEndereco' => $sEndereco
    , 'nNumeroEndereco' => $nNumeroEndereco
    , 'sEmail' => $sEmail
    , 'sTelefone' => $sTelefone
    , 'sCPF' => $sCPF
    , 'sRG' => $sRG
    , 'sCelular' => $sCelular
    , 'sObservacao' => $sObservacao
    , 'sSexo' => $sSexo
    , 'sProfissao' => $sProfissao
    , 'sCidadeNatal' => $sCidadeNatal
    , 'sRa' => $sRa
    , 'sNumeroMatricula' => $sNumeroMatricula
    , 'sSituacao' => $sSituacao
    , 'sCursoInteresse' => $sCursoInteresse
    );

    echo "<pre>";print_r($vars);

    $query = http_build_query($vars, null, '&');

    @$response = $client->request('POST', $method, $vars);

    $content = $response->getBody()->getContents();
    $obj = \GuzzleHttp\json_decode($content, false);

    echo "<pre>";print_r($obj);

}catch (\Exception $e){
    echo "ERROR - Houve uma exceção na linha: ".$e->getLine()." - Código do Erro: ".$e->getCode()." - Mensagem: ".$e->getMessage();
}

?>