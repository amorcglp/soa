<?php

foreach ([
    [
        '../vendor/autoload.php', '../loadEnv.php'
    ],
    [
        '../../vendor/autoload.php', '../../loadEnv.php'
    ],
    [
        './vendor/autoload.php', './loadEnv.php'
    ],
    [
        'vendor/autoload.php', 'loadEnv.php'
    ],
    [
        '../../../vendor/autoload.php', '../../../loadEnv.php'
    ]
] as $includePath) {
    list($autoload, $env) = $includePath; 
    if(realpath($autoload) && realpath($env)) {
        require_once $autoload;
        require_once $env;
        break;
    }
}

use GuzzleHttp\Client;

function montaCredenciais()
{
    return base64_encode(getenv('API_USERNAME').':'.getenv('API_PASSWORD'));
}

function restAmorc($method, $vars = array(), $req = null) {

    if($req==null)
        $req = 'GET';

    $client = new Client([
        // Base URI is used with relative requests
        'base_uri' => getenv('API_BASE_URL') . getenv('API_URL'),
    ]);
    try {

        $query = http_build_query($vars, null, '&');

        if($req == 'GET') {

            $credentials = montaCredenciais();

            $params = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $query);

            // die("Query: $query<br />Params: $params");

            $response = $client->request('GET', $method,
                [
                    'verify' => false,
                    'headers' => ['Authorization' => 'Basic ' . $credentials],
                    'query' => $params
                ]);

        }
        if($req == 'PUT') {

            $credentials = montaCredenciais();

            $params = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $query);
            @$response = $client->request('PUT', $method,
                [
                    'verify' => false,
                    'headers' => ['Authorization' => 'Basic ' . $credentials],
                    'query' => $params
                ]);
        }
        if($req == 'POST') {
            //echo $method;exit();
            @$response = $client->request('POST', $method, $vars);

        }

        $content = $response->getBody()->getContents();
        $obj = \GuzzleHttp\json_decode($content, false);
        return $obj;
    }catch (\Exception $e){
        error_log("ERROR - Houve uma exceção na linha: ".$e->getLine()." - Código do Erro: ".$e->getCode()." - Mensagem: ".$e->getMessage());
    }
}