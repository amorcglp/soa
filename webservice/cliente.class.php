<?php
/**
 * @author Braz Alves
 * @category classe
 * @package void
 * @version 1.0.0
 */
  class cliente{

  	public $cod_rosacr; /* Key Field */
	public $nom_client;

	public function get($prop){
		return $this->$prop;
	}

	public function set($prop,$val){
		$this->$prop = $val;
	}

	public function __construct($cod_rosacr = null){
		//$bd = mysql_select_db('test');
		if($cod_rosacr != null){
			$ler = array();
			$sql = "select * from CADASTRO where cod_rosacr='".$cod_rosacr."'";
			$query = mssql_query($sql);
			if ($query) {
				$contagem = mssql_num_rows($query);
				if ($contagem > 0) {
					while ($dados = mssql_fetch_array($query)) {
						$ler[] = $dados['cod_rosacr'];
						$ler[] = $dados['nom_client'];
					}
					$this->__populate($ler);
				}
				
			}
		}
	}

	private function __populate($line){
		$properties = '';
		$properties = get_class_vars(get_class($this));
		if(count($properties) > 0 ){
			$i=0;
			foreach($properties as $props=>$values){
				$this->$props = $line[$i];
				$i++;
			}
		}
	}

	public function emails($cod_rosacr){
		$emails = array();
		$sql = "select * from CADASTRO where cod_rosacr='".$cod_rosacr."'";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$emails[] = $dados['des_email'];
				}
			}
		}
		
		return implode('*',$emails);

	}
	
  	public function consultaFaturaEvento($cod_rosacr,$data_inicial,$data_final,$dual,$pesquisar,$nome,$seq_tipo_evento,$ano_evento=null){
  		
  		$situacao = 0;//A principio n�o existem registros de faturamento para o membro
		
  		//Pegar ano do evento para ter como refer�ncia
  		if($ano_evento==null)
  		{
  			$ano = substr($data_inicial, 6,4);
  		}else{
  			$ano = $ano_evento;
  		}
  		//Buscar seqcadast do membro
		$seq_cadast = $this->retornaSeqCadast($cod_rosacr,$nome);
		//Buscar seqcadast do evento
		$seq_evento = $this->retornaSeqCadastEvento($data_inicial,$data_final,$pesquisar);
		//Verificar se está no evento
  		$situacao = $this->verificaLancamentoEvento($seq_cadast,$seq_evento,$dual,$ano,$seq_tipo_evento);
		
		return $situacao;

	}
	
	public function verificaLancamentoEvento($seq_cadast=0,$seq_evento=0,$dual,$ano,$seq_tipo_evento)
	{
		$situacao = 0;//Se permanecer zero at� o final dessa fun��o quer dizer que a pessoa n�o pagou
		
		$sql = "select
				  *
				from
				  EVENTO_INSCR ei
				inner join CADASTRO as c on ei.seq_cadast = c.seq_cadast
				where
				  ei.seq_evento = '".$seq_evento."'
				 AND ei.seq_cadast= '".$seq_cadast."'
				 and ei.num_ano_evento='".$ano."'
				 and ei.seq_tipo_evento='".$seq_tipo_evento."'
				 and ide_isento = 'N'
				 and dat_cancel is null";
  		//echo $sql."<br>";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				
				$situacao=1;//Isso quer dizer que o financeiro fez um faturamento para este ERIN para esta pessoa
				
			}
		}
		
		return $situacao;
	}
	
	public function retornaSeqCadast($cod_rosacr,$nome)
	{
		$seq_cadast = 0;
		
		$sql = "select * from CADASTRO where cod_rosacr='".$cod_rosacr."' and nom_client like '".$nome."%' ";
		//echo $sql."<br>";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$seq_cadast = $dados['seq_cadast'];
				}
			}
		}
		return $seq_cadast;
	}
	
	public function retornaSeqCadastEvento($data_inicial,$data_final,$pesquisar)
	{
		$seq_evento = 0;

		
		$sql = "select * from EVENTO 
					WHERE CONVERT(CHAR(10),dat_hora_inicio_evento , 103) = '".$data_inicial."'
					and CONVERT(CHAR(10),dat_hora_final_evento , 103) = '".$data_final."'
					and des_evento like '%".$pesquisar."%'";
		//echo $sql."<br>";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$seq_evento = $dados['seq_evento'];
					//echo "seq do evento:".$seq_evento."<br>";
				}
			}
		}
		
		return $seq_evento;
	}
	
  	public function consultaCidadeUfUsuario($cod_rosacr,$nome)
	{
		$arr=array();
		$seq_cadast = $this->retornaSeqCadast($cod_rosacr,$nome);
		
		$sql = "select sig_uf,nom_locali from CADASTRO where seq_cadast=".$seq_cadast;
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$arr['cidade'] 	= $dados['nom_locali'];
					$arr['uf'] 		= $dados['sig_uf'];
				}
			}
		}
		return implode('*',$arr);
	}
	
  	public function consultaOficial($cod_rosacr,$nome)
  	{
  		$situacao=0;
		$seq_cadast = $this->retornaSeqCadast($cod_rosacr,$nome);
		
		$sql = ("SELECT CONVERT(CHAR(10),dat_entrad, 103) as dat_entrad, CONVERT(CHAR(10),dat_saida, 103) as dat_saida, oaf.seq_tipo_funcao_oficial, tfo.des_tipo_funcao, oaf.seq_funcao, fo.des_funcao, oaf.seq_cadast_oa, oa.sig_orgafi, oaf.ide_tipo_motivo_saida
			FROM
			  OA_OFICIAL oaf, CADASTRO oa,
			  OA_TIPO_FUNCAO_OFICIAL tfo, OA_FUNCAO_OFICIAL fo
			WHERE
			  oaf.seq_cadast_membro ='".$seq_cadast."' and
			  oa.seq_cadast = oaf.seq_cadast_oa and
			  tfo.seq_tipo_funcao_oficial = oaf.seq_tipo_funcao_oficial and
			  fo.seq_tipo_funcao_oficial = tfo.seq_tipo_funcao_oficial and
			  fo.seq_funcao = oaf.seq_funcao
			ORDER BY
			  oaf.dat_entrad desc");
		//echo $sql;
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				$situacao=1;//Isso quer dizer que a pessoa � ou foi oficial
			}else{
				$situacao=0;
			}
		}
		
		return $situacao;
		
	}
	
 	public function consultaMonitoresOas($cod_rosacr=null,$regiao=null,$sigla=null)
  	{
		$monitores = array();
		$sql = "SELECT oaf.SEQ_CADAST_OA,cad.seq_cadast,cad.nom_client, cad.cod_rosacr, cad.des_email, fo.des_funcao
			, 
			oaf.dat_entrad, oaf.dat_saida, oaf.dat_termin_mandat
			, 
			(SELECT b.nom_client FROM CADASTRO b where b.seq_cadast = oaf.SEQ_CADAST_OA) as nome_oa,
			(SELECT c.sig_orgafi FROM CADASTRO c where c.seq_cadast = oaf.SEQ_CADAST_OA) as sigla_oa
				FROM 
			  OA_OFICIAL oaf, CADASTRO cad,
			  OA_FUNCAO_OFICIAL fo
			WHERE
			  oaf.SEQ_CADAST_OA IN (SELECT d.seq_cadast FROM CADASTRO d where 1=1 "; 
		if($regiao!=null&&$regiao!="0")
		{
			$sql .= " and LEFT(d.sig_orgafi,3)='".$regiao."'";//Filtrar por regi�o
		}
  		if($sigla!=null&&$sigla!="0")
		{
			$sql .= " and d.sig_orgafi='".$sigla."'";//Filtrar por sigla do OA
		}
			$sql .=  ") 
			and fo.seq_funcao IN ('155','157','159','161','163','165','167','169','171','173','179','181','183','185') 
			and oaf.seq_cadast_membro = cad.seq_cadast 
			and fo.seq_funcao = oaf.seq_funcao
			  and cad.cod_rosacr is not null
			  and cad.ide_tipo_pessoa <> 'J'
			  ";
			
		if($cod_rosacr!=null&&$cod_rosacr!=0)
		{
			$sql .= " and cad.cod_rosacr='".$cod_rosacr."'";
		}
		$sql .= " ORDER BY
			  oaf.dat_entrad desc";
		//echo $sql."<br>";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$monitores[] = $dados['SEQ_CADAST_OA'];
					$monitores[] = $dados['seq_cadast'];
					$monitores[] = utf8_encode($dados['nom_client']);
					$monitores[] = $dados['cod_rosacr'];
					$monitores[] = ($dados['des_email']!=null) ? $dados['des_email'] : 0;
					$monitores[] = utf8_encode($dados['des_funcao']);
					$monitores[] = ($dados['dat_entrad']!=null) ? substr($dados['dat_entrad'], 0,11) : 0;
					$monitores[] = ($dados['dat_saida']!=null) ? substr($dados['dat_saida'], 0,11) : 0;
					$monitores[] = ($dados['dat_termin_mandat']!=null) ? substr($dados['dat_termin_mandat'], 0,11) : 0;
					$nome_oa_tratado = str_replace("*", "", $dados['nome_oa']);
					$nome_oa_tratado = str_replace("FECHOU", "", $nome_oa_tratado);
					$monitores[] = $nome_oa_tratado;
					$monitores[] = $dados['sigla_oa'];
				}
			}else{
				$monitores[0]=0;
			}
		}
		
		return implode('*',$monitores);

	}
	
  	public function consultaOficiaisUsuariosSOA($cod_rosacr=null,$regiao=null,$sigla=null,$funcao=null)
  	{
		$oficiaisSOA = array();
		$hoje = date("Y");
		$sql = "SELECT oaf.SEQ_CADAST_OA,cad.seq_cadast,cad.nom_client, cad.cod_rosacr, cad.num_telefo,cad.des_email, fo.seq_funcao,fo.des_funcao
			, 
			oaf.dat_entrad, oaf.dat_saida, oaf.dat_termin_mandat
			, 
			(SELECT b.nom_client FROM CADASTRO b where b.seq_cadast = oaf.SEQ_CADAST_OA) as nome_oa,
			(SELECT c.sig_orgafi FROM CADASTRO c where c.seq_cadast = oaf.SEQ_CADAST_OA) as sigla_oa
				FROM 
			  OA_OFICIAL oaf, CADASTRO cad,
			  OA_FUNCAO_OFICIAL fo
			WHERE
			  oaf.SEQ_CADAST_OA IN (SELECT d.seq_cadast FROM CADASTRO d where 1=1 "; 
		if($regiao!=null&&$regiao!="0")
		{
			$sql .= " and LEFT(d.sig_orgafi,3)='".$regiao."'";//Filtrar por regi�o
		}
  		if($sigla!=null&&$sigla!="0")
		{
			$sql .= " and d.sig_orgafi='".$sigla."'";//Filtrar por sigla do OA
		}
			$sql .=  ") 
			and fo.seq_funcao IN ('155','201','203','205','207','209','211','213','605','609','611') 
			and oaf.seq_cadast_membro = cad.seq_cadast 
			and fo.seq_funcao = oaf.seq_funcao
			  and cad.cod_rosacr is not null
			  and cad.ide_tipo_pessoa <> 'J'
			  and YEAR(oaf.dat_entrad) >= '".$hoje."'
			  ";
			
		if($cod_rosacr!=null&&$cod_rosacr!=0)
		{
			$sql .= " and cad.cod_rosacr='".$cod_rosacr."'";
		}
		$sql .= " ORDER BY
			  oaf.dat_entrad desc";
		//echo $sql."<br>";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$oficiaisSOA[] = $dados['SEQ_CADAST_OA'];
					$oficiaisSOA[] = $dados['seq_cadast'];
					$oficiaisSOA[] = utf8_encode($dados['nom_client']);
					$oficiaisSOA[] = $dados['cod_rosacr'];
					$oficiaisSOA[] = ($dados['des_email']!=null) ? $dados['des_email'] : 0;
					$oficiaisSOA[] = utf8_encode($dados['seq_funcao']);
					$oficiaisSOA[] = utf8_encode($dados['des_funcao']);
					$oficiaisSOA[] = ($dados['dat_entrad']!=null) ? substr($dados['dat_entrad'], 0,11) : 0;
					$oficiaisSOA[] = ($dados['dat_saida']!=null) ? substr($dados['dat_saida'], 0,11) : 0;
					$oficiaisSOA[] = ($dados['dat_termin_mandat']!=null) ? substr($dados['dat_termin_mandat'], 0,11) : 0;
					$nome_oa_tratado = str_replace("*", "", $dados['nome_oa']);
					$nome_oa_tratado = str_replace("FECHOU", "", $nome_oa_tratado);
					$oficiaisSOA[] = $nome_oa_tratado;
					$oficiaisSOA[] = $dados['sigla_oa'];
					$oficiaisSOA[] = $dados['num_telefo'];
				}
			}else{
				$oficiaisSOA[0]=0;
			}
		}
		
		return implode('*',$oficiaisSOA);

	}
	
  	public function consultaOficiaisOas($cod_rosacr=null,$regiao=null,$sigla=null,$seq_funcao=null,$seqOficiaisComLogin=null)
  	{
		$oficiaisSOA = array();
		$sql = "SELECT oaf.SEQ_CADAST_OA,cad.seq_cadast,cad.nom_client, cad.cod_rosacr,cad.des_email, 
			fo.des_funcao, cad.num_telefo,cad.des_email, fo.seq_funcao
			, 
			oaf.dat_entrad, oaf.dat_saida, oaf.dat_termin_mandat
			, 
			(SELECT b.nom_client FROM CADASTRO b where b.seq_cadast = oaf.SEQ_CADAST_OA) as nome_oa,
			(SELECT c.sig_orgafi FROM CADASTRO c where c.seq_cadast = oaf.SEQ_CADAST_OA) as sigla_oa
				FROM 
			  OA_OFICIAL oaf, CADASTRO cad,
			  OA_FUNCAO_OFICIAL fo
			WHERE
			  oaf.SEQ_CADAST_OA IN (SELECT d.seq_cadast FROM CADASTRO d where 1=1 "; 
		if($regiao!=null&&$regiao!="0")
		{
			$sql .= " and LEFT(d.sig_orgafi,3)='".$regiao."'";//Filtrar por regi�o
		}
  		if($sigla!=null&&$sigla!="0")
		{
			$sql .= " and d.sig_orgafi='".$sigla."'";//Filtrar por sigla do OA
		}
			$sql .=  ") ";
		if($seq_funcao!=null&&$seq_funcao!="0")
		{	
			$sql .=  "  and fo.seq_funcao IN (".$seq_funcao.") ";
		} 
  		
			$sql .=  "  
			  and oaf.seq_cadast_membro = cad.seq_cadast 
			  and fo.seq_funcao = oaf.seq_funcao
			  and cad.cod_rosacr is not null
			  and cad.ide_tipo_pessoa <> 'J'
			  and cad.des_email is not null
			  and cad.des_email <> ''
			  ";
			
		if($cod_rosacr!=null&&$cod_rosacr!=0)
		{
			$sql .= " and cad.cod_rosacr='".$cod_rosacr."'";
		}
		
  		if($seqOficiaisComLogin!=null&&$seqOficiaisComLogin!="0")
		{	
			$sql .=  "  and cad.seq_cadast NOT IN (".$seqOficiaisComLogin.") ";
		}
		$sql .= " ORDER BY
			  oaf.dat_entrad desc";
		//echo $sql."<br>";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					$oficiaisSOA[] 			= ($dados['SEQ_CADAST_OA']!=null) ?$dados['SEQ_CADAST_OA']:0;
					$oficiaisSOA[] 			= ($dados['seq_cadast']!=null) ?$dados['seq_cadast']:0;
					$oficiaisSOA[] 			= (utf8_encode($dados['nom_client'])!=null) ?utf8_encode($dados['nom_client']):0;
					$oficiaisSOA[] 			= ($dados['cod_rosacr']!=null) ?$dados['cod_rosacr']:0;
					$arrEmail				= explode("  ",$dados['des_email']);
					$email_sem_telefone		= $arrEmail[0];
					$email					= ($email_sem_telefone!=null) ? $email_sem_telefone : 0;
					$oficiaisSOA[]			= strtolower($email);			
					$oficiaisSOA[] 			= ($dados['seq_funcao']!=null) ?$dados['seq_funcao']:0;
					$oficiaisSOA[] 			= (utf8_encode($dados['des_funcao'])!=null) ?utf8_encode($dados['des_funcao']):0;
					$oficiaisSOA[] 			= ($dados['dat_entrad']!=null) ? substr($dados['dat_entrad'], 0,11) : 0;
					$oficiaisSOA[] 			= ($dados['dat_saida']!=null) ? substr($dados['dat_saida'], 0,11) : 0;
					$oficiaisSOA[] 			= ($dados['dat_termin_mandat']!=null) ? substr($dados['dat_termin_mandat'], 0,11) : 0;
					$nome_oa_tratado 		= str_replace("*", "", $dados['nome_oa']);
					$nome_oa_tratado 		= str_replace("FECHOU", "", $nome_oa_tratado);
					$oficiaisSOA[] 			= ($nome_oa_tratado!=null) ?$nome_oa_tratado:0;
					$oficiaisSOA[] 			= ($dados['sigla_oa']!=null) ?$dados['sigla_oa']:0;
					$oficiaisSOA[] 			= ($dados['num_telefo']!=null) ?str_replace("*","",$dados['num_telefo']):0;
					//Defini��o da regi�o do Oficial
					$regiao_rosacruz		= trim(substr($dados['sigla_oa'], 0,3));
					$oficiaisSOA[] 			= ($regiao_rosacruz!=null) ? $regiao_rosacruz:0;
				}
			}else{
				$oficiaisSOA[0]=0;
			}
		}
		
		return implode('*',$oficiaisSOA);
	}
	
  	public function consultaGCsUsuariosSOA($cod_rosacr=null,$regiao=null)
  	{
  		/*
  		 * Verificar se o m�todo n�o est� apontando para o desenvolvimento 3, 
  		 * se tiver a query ir� retornar um n�mero menor de Grandes Conselheiros
  		 */
		$oficiaisSOA = array();
		$sql = "
			SELECT oaf.SEQ_CADAST_OA,cad.seq_cadast,cad.nom_client, cad.cod_rosacr, 
			cad.num_telefo,cad.des_email, fo.seq_funcao,fo.des_funcao , 
			oaf.dat_entrad, oaf.dat_saida, oaf.dat_termin_mandat , 
			(SELECT b.nom_client FROM CADASTRO b where b.seq_cadast = oaf.SEQ_CADAST_OA) as nome_oa, 
			(SELECT c.sig_orgafi FROM CADASTRO c where c.seq_cadast = oaf.SEQ_CADAST_OA) as sigla_oa 
			FROM OA_OFICIAL oaf, CADASTRO cad, OA_FUNCAO_OFICIAL fo 
			WHERE 1=1 ";
			if($regiao!=null&&$regiao!="0")
			{
				$sql .= "	 
				and 
				oaf.SEQ_CADAST_OA IN 
				(
				 SELECT d.seq_cadast FROM CADASTRO d where 1=1 and LEFT(d.sig_orgafi,3)='".$regiao."' 
				)";//Filtrar por regi�o
			}
		$sql .= "
			and fo.seq_funcao IN ('151') 
			and oaf.seq_cadast_membro = cad.seq_cadast 
			and fo.seq_funcao = oaf.seq_funcao 
			and cad.cod_rosacr is not null 
			and cad.ide_tipo_pessoa <> 'J' 
			";
  		if($cod_rosacr!=null&&$cod_rosacr!="0")
		{
			$sql .= " and cad.cod_rosacr='".$cod_rosacr."'";
		}
		$sql .= " order by cad.cod_rosacr ";
		//echo $sql."<br>";
		$query = mssql_query($sql);
		if ($query) {
			$contagem = mssql_num_rows($query);
			//echo "total de gcs:".$contagem;
			if ($contagem > 0) {
				while ($dados = mssql_fetch_array($query)) {
					
					$oficiaisSOA[] 			= ($dados['SEQ_CADAST_OA']!=null) ?$dados['SEQ_CADAST_OA']:0;
					$oficiaisSOA[] 			= ($dados['seq_cadast']!=null) ?$dados['seq_cadast']:0;
					$oficiaisSOA[] 			= (utf8_encode($dados['nom_client'])!=null) ?utf8_encode($dados['nom_client']):0;
					$oficiaisSOA[] 			= ($dados['cod_rosacr']!=null) ?$dados['cod_rosacr']:0;
					$oficiaisSOA[] 			= ($dados['des_email']!=null) ? $dados['des_email'] : 0;
					$oficiaisSOA[] 			= ($dados['seq_funcao']!=null) ?$dados['seq_funcao']:0;
					$oficiaisSOA[] 			= (utf8_encode($dados['des_funcao'])!=null) ?utf8_encode($dados['des_funcao']):0;
					$oficiaisSOA[] 			= ($dados['dat_entrad']!=null) ? substr($dados['dat_entrad'], 0,11) : 0;
					$oficiaisSOA[] 			= ($dados['dat_saida']!=null) ? substr($dados['dat_saida'], 0,11) : 0;
					$oficiaisSOA[] 			= ($dados['dat_termin_mandat']!=null) ? substr($dados['dat_termin_mandat'], 0,11) : 0;
					$nome_oa_tratado 		= str_replace("*", "", $dados['nome_oa']);
					$nome_oa_tratado 		= str_replace("FECHOU", "", $nome_oa_tratado);
					$oficiaisSOA[] 			= ($nome_oa_tratado!=null) ?$nome_oa_tratado:0;
					$oficiaisSOA[] 			= ($dados['sigla_oa']!=null) ?$dados['sigla_oa']:0;
					$oficiaisSOA[] 			= ($dados['num_telefo']!=null) ?$dados['num_telefo']:0;
					//Defini��o da regi�o do Grande Conselheiro
					$regiao_rosacruz		= trim(substr($dados['sigla_oa'], 0,3));
					$oficiaisSOA[] 			= ($regiao_rosacruz!=null) ? $regiao_rosacruz:0;
				}
			}else{
				$oficiaisSOA[0]=0;
			}
		}
		
		return implode('*',$oficiaisSOA);

	}
	
  /* METODOS DE INTEGRACAO COM O WEBSERVICE */
    static function str2Obj($str,$class){
	 	$ex = explode('*',$str);

	 	if(count($ex) > 0 ){
	 		foreach($ex as $ind=>$values){
			 $x = explode('=',$values);
			 if(count($x)> 1){
			 $class->set($x[0],$x[1]);
			 }
	 		}
	 	}
		return $class;
	 }
	 public function obj2Str($class) {

		$properties = get_class_vars(get_class($class));

		if(count($properties) > 0 ){
			$str = '';
			$i=0;
			foreach($properties as $props=>$values){
				$str.=$props.'='.$this->$props.'*';
			}
			return substr($str,0,strlen($str)-1);
		}
    }
  }

?>