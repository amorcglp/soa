<?php 

require_once '../model/organismoClass.php';
require_once '../model/regiaoRosacruzClass.php';
require_once '../model/trimestralidadeClass.php';
include_once('../model/membrosRosacruzesAtivosClass.php');
require_once '../lib/functions.php';

?>
<style>
table , body{
font-family: Calibri;
font-size: 12px!important;
color: rgb(85, 85, 85);
border-collapse:collapse;
border-color:#000;
max-width: 100%;
border-spacing: 0;
}
.cinza{
	background-color:#F5F5F5;
}
.centro{
	text-align:center;
}

/* Table Base */

.table { 
  width: 100%;
  margin-bottom: 20px;
}

.table th,
.table td {
  font-weight: normal;
  font-size: 12px;
  padding: 8px 15px;
  line-height: 20px;
  text-align: left;
  vertical-align: middle;
  border-top: 1px solid #dddddd;
}
.table thead th {
  vertical-align: bottom;
  font-weight:bold;
}      
.table .t-small {
  width: 5%;
}
.table .t-medium {
  width: 10%;
}
.table .t-nome {
  width: 30%;
}
.table .t-status {
  font-weight: bold;
}
.table .t-active {
  color: #46a546;
}
.table .t-inactive {
  color: #e00300;
}
.table .t-draft {
  color: #f89406;
}
.table .t-scheduled {
  color: #049cdb;
}


</style>

<?php 
$html = '';
$organismo 		= new organismo();
$regiaoRosacruz = new regiaoRosacruz();
$t 				= new trimestralidade();
$mra			= new MembrosRosacruzesAtivos();

$resultado = $regiaoRosacruz->listaRegiaoRosacruz();

$resultadoTri = $t->listaTrimestralidade();
$valorTrimestralidade=0;
if($resultadoTri)
{
	foreach($resultadoTri as $vetorTri)
	{
		$valorTrimestralidade = (float) str_replace(",",".",str_replace(".","",$vetorTri['valorTrimestralidade']));
	}
}
$metadeTrimestralidade = $valorTrimestralidade/2;

$cor ="";
$anoProximo = isset($_REQUEST['ano'])?$_REQUEST['ano']:null;
//$anoAtual = date('Y')-1;
$anoAtual = isset($_REQUEST['ano'])?$_REQUEST['ano']:null;
$totalGeral=0;

$arquivo = 'CARTA CONSTITUTIVA ORG. AFIL '.$anoAtual.'-'.$anoProximo.'.xls';

header ("Content-type: application/x-msexcel");
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
header ("Content-Description: PHP Generated Data" );

$linha=9;
$arrTotalGeral=array();
$o=0;
?>
<style>
</style>
<center>
<?php if($resultado){

?>

<table class="table table-action">
	<thead>
	<tr >
	<!--<th rowspan="100"><font color="white">**</font></th>-->
	
	</tr>
	</thead>
	<tr style="color:#000;height:50px" colspan="5">
		<td align="center" style="border:1px solid black;">CARTA CONSTITUTIVA <?php echo $anoAtual;?>-<?php echo $anoProximo;?></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
	</tr>
	<tr style="color:#000;" colspan="5">
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
	</tr>
	<tr style="color:#000;" colspan="5">
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;">Calculado por:</td>
		<td align="center" style="border:1px solid black;">=TEXTO(<?php echo number_format($metadeTrimestralidade, 2, ',', '.');?>;"R$#.##0,00")</td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
	</tr>
	<tr style="color:#000;" colspan="5">
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;">50% do trimestre:</td>
		<td align="center" style="border:1px solid black;width:100px;">=TEXTO(<?php echo number_format($valorTrimestralidade, 2, ',', '.');?>;"R$#.##0,00")</td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
	</tr>
	<tbody>
	<?php foreach($resultado as $vetor){
		$totalRegiao=0;
                $arrSubtotal=array();
                $resultado2 = $organismo->listaOrganismo(null,null,null,$vetor['regiaoRosacruz'],null,null,null,1);
                if($resultado2)
                {    
	?>
	<tr style="color:#000;" colspan="5">
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                
	</tr>
	<tr style="color:#000;height:50px" colspan="5">
		<td align="center" style="border:1px solid black;"> REGI&Atilde;O <?php echo $vetor['regiaoRosacruz'];?></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
                <td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
		<td align="center" style="border:1px solid black;"></td>
	</tr>
	<?php 
                }
		
		if($resultado2)
		{
                    
			?>
			<th style="border:1px solid black;background-color:#00688B;color:#fff">SIGLA</th>
			<th style="border:1px solid black;background-color:#00688B;color:#fff">NOME</th>
			<th style="border:1px solid black;background-color:#00688B;color:#fff">STATUS</th>
			<th style="border:1px solid black;background-color:#00688B;color:#fff">C&Oacute;DIGO</th>	
			
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">JANEIRO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">FEVEREIRO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">MAR&Ccedil;O</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">ABRIL</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">MAIO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">JUNHO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">JULHO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">AGOSTO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">SETEMBRO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">OUTUBRO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">NOVEMBRO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">DEZEMBRO</th>
                        <th style="border:1px solid black;background-color:#00688B;color:#fff">ENTRADAS</th>
			<th style="border:1px solid black;background-color:#00688B;color:#fff">N&#186; DE MEMBROS</th>
			<th style="border:1px solid black;background-color:#00688B;color:#fff;width:200px">TOTAL</th>
			<?php 
			foreach($resultado2 as $vetor2)
			{
                            $arrSubtotal[]=$linha;
                            
				$total=0;
				$nomeOrganismo = organismoAfiliadoNomeCompleto($vetor2['classificacaoOrganismoAfiliado'],$vetor2['tipoOrganismoAfiliado'],$vetor2['nomeOrganismoAfiliado']);
				$idOrganismoAfiliado = $vetor2['idOrganismoAfiliado'];
				
				$mJan						= $mra->retornaMembrosRosacruzesAtivos(1,$anoAtual,$idOrganismoAfiliado);
				$mJan						= $mJan['numeroAtualMembrosAtivos'];
				
				$mFev						= $mra->retornaMembrosRosacruzesAtivos(2,$anoAtual,$idOrganismoAfiliado);
				$mFev						= $mFev['numeroAtualMembrosAtivos'];
				
				$mMar						= $mra->retornaMembrosRosacruzesAtivos(3,$anoAtual,$idOrganismoAfiliado);
				$mMar						= $mMar['numeroAtualMembrosAtivos'];
				
				$mAbr						= $mra->retornaMembrosRosacruzesAtivos(4,$anoAtual,$idOrganismoAfiliado);
				$mAbr						= $mAbr['numeroAtualMembrosAtivos'];
				
				$mMai						= $mra->retornaMembrosRosacruzesAtivos(5,$anoAtual,$idOrganismoAfiliado);
				$mMai						= $mMai['numeroAtualMembrosAtivos'];
				
				$mJun						= $mra->retornaMembrosRosacruzesAtivos(6,$anoAtual,$idOrganismoAfiliado);
				$mJun						= $mJun['numeroAtualMembrosAtivos'];
				
				$mJul						= $mra->retornaMembrosRosacruzesAtivos(7,$anoAtual,$idOrganismoAfiliado);
				$mJul						= $mJul['numeroAtualMembrosAtivos'];
				
				$mAgo						= $mra->retornaMembrosRosacruzesAtivos(8,$anoAtual,$idOrganismoAfiliado);
				$mAgo						= $mAgo['numeroAtualMembrosAtivos'];
				
				$mSet						= $mra->retornaMembrosRosacruzesAtivos(9,$anoAtual,$idOrganismoAfiliado);
				$mSet						= $mSet['numeroAtualMembrosAtivos'];
				
				$mOut						= $mra->retornaMembrosRosacruzesAtivos(10,$anoAtual,$idOrganismoAfiliado);
				$mOut						= $mOut['numeroAtualMembrosAtivos'];
				
				$mNov						= $mra->retornaMembrosRosacruzesAtivos(11,$anoAtual,$idOrganismoAfiliado);
				$mNov						= $mNov['numeroAtualMembrosAtivos'];
				
				$mDez						= $mra->retornaMembrosRosacruzesAtivos(12,$anoAtual,$idOrganismoAfiliado);
				$mDez						= $mDez['numeroAtualMembrosAtivos'];
				
				$mediaMembrosAtivos = round(($mJan+$mFev+$mMar+$mAbr+$mMai+$mJun+$mJul+$mAgo+$mSet+$mOut+$mNov+$mDez)/12);

				$total = $mediaMembrosAtivos*$metadeTrimestralidade;
				
				$totalRegiao += $total;
				
				$totalGeral += $total;
				?>
				<tr>
					<td align="center" style="border:1px solid black;"><?php echo strtoupper($vetor2['siglaOrganismoAfiliado']);?></td>
                                        <td align="left" style="border:1px solid black;"><?php echo utf8_decode($nomeOrganismo);?></td>
					<td style="border:1px solid black;"><?php echo situacaoOA($vetor2['situacaoOrganismoAfiliado']);?></td>
					<td style="border:1px solid black;"><?php echo $vetor2['codigoAfiliacaoOrganismoAfiliado'];?></td>
                                        <td style="border:1px solid black;"><?php echo $mJan;?></td>
                                        <td style="border:1px solid black;"><?php echo $mFev;?></td>
                                        <td style="border:1px solid black;"><?php echo $mMar;?></td>
                                        <td style="border:1px solid black;"><?php echo $mAbr;?></td>
                                        <td style="border:1px solid black;"><?php echo $mMai;?></td>
                                        <td style="border:1px solid black;"><?php echo $mJun;?></td>
                                        <td style="border:1px solid black;"><?php echo $mJul;?></td>
                                        <td style="border:1px solid black;"><?php echo $mAgo;?></td>
                                        <td style="border:1px solid black;"><?php echo $mSet;?></td>
                                        <td style="border:1px solid black;"><?php echo $mOut;?></td>
                                        <td style="border:1px solid black;"><?php echo $mNov;?></td>
                                        <td style="border:1px solid black;"><?php echo $mDez;?></td>
                                        <td style="border:1px solid black;width:120">=SES(E<?php echo $linha;?>=0;"INCONSISTENTE";F<?php echo $linha;?>=0;"INCONSISTENTE";G<?php echo $linha;?>=0;"INCONSISTENTE";H<?php echo $linha;?>=0;"INCONSISTENTE";I<?php echo $linha;?>=0;"INCONSISTENTE";J<?php echo $linha;?>=0;"INCONSISTENTE";K<?php echo $linha;?>=0;"INCONSISTENTE";L<?php echo $linha;?>=0;"INCONSISTENTE";M<?php echo $linha;?>=0;"INCONSISTENTE";N<?php echo $linha;?>=0;"INCONSISTENTE";O<?php echo $linha;?>=0;"INCONSISTENTE";P<?php echo $linha;?>=0;"INCONSISTENTE";VERDADEIRO;"CONSISTENTE")</td>
					<td style="border:1px solid black;"><?php echo $mediaMembrosAtivos;?></td>
					<td align="center" style="border:1px solid black;">=TEXTO((R<?php echo $linha;?>*G4);"R$#.##0,00")</td>
				</tr>
			<?php 
                            $linha++;
                            $arrTotalGeral[$o]=$linha;
                             
			}
                        $linha = $linha+4;
		}
                if($resultado2)
                {    
			?>
			<tr>
				<td align="center" style="border:1px solid black;"></td>
				<td align="right" style="border:1px solid black;"></td>
				<td align="center" style="border:1px solid black;"></td>
				<td align="center" style="border:1px solid black;"></td>
				<td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
                                <td style="border:1px solid black;"></td>
				<td style="border:1px solid black;">SUB TOTAL</td>
				<td align="center" style="border:1px solid black;">=TEXTO((
                                    <?php 
                                    $c=0;
                                    $ultimaLinha=0;
                                        foreach($arrSubtotal as $v)
                                        {
                                            $ultimaLinha=$v;
                                            if($c==0)
                                            {    
                                                echo "S".$v;
                                            }else{
                                                echo "+S".$v;
                                            }
                                            $c++;
                                        }
                                        ?>);"R$#.##0,00")
                                </td>
			</tr>
<?php 
                    $o++;
                    
                }
}?>
			<tr>
				<td align="center" style="border:1px solid black;background-color:#00688B;color:#fff"></td>
				<td align="right" style="border:1px solid black;background-color:#00688B;color:#fff"></td>
				<td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
				<td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
				<td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff;width:100">CONSISTENTES</td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff">=CONT.SE(Q9:Q<?php echo $ultimaLinha+1;?>;"CONSISTENTE")</td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff;width:120">INCONSISTENTES</td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff">=CONT.SE(Q9:Q<?php echo $ultimaLinha+1;?>;"INCONSISTENTE")</td>
                                <td style="border:1px solid black;background-color:#00688B;color:#fff"></td>
				<td style="border:1px solid black;background-color:#00688B;color:#fff">TOTAL GERAL</td>
				<td align="center" style="border:1px solid black;background-color:#00688B;color:#fff">
                                    =TEXTO((
                                    <?php
                                    $c=0;
                                        foreach($arrTotalGeral as $v)
                                        {
                                            if($c==0)
                                            {    
                                                echo "S".$v;
                                            }else{
                                                echo "+S".$v;
                                            }
                                            $c++;
                                        }
                                        ?>);"R$#.##0,00")
                                    </td>
			</tr>
	</tbody>
</table>
<?php }else{
	echo "Nenhuma informação a mostrar";
}?>
</center>
<?php 
 
// Envia o conte�do do arquivo
echo $html;

exit;
?>