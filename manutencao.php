<?php
if(!$_SERVER['HTTPS']) {
    header( "Location: "."https://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SOA | Manutenção</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body class="gray-bg">
        <div class="middle-box text-center animated fadeInDown">
            <br><br>
            <img src="img/solado.png" />
            <br><br>
            <h2 class="font-bold">Manutenção</h2>
            <br><br>
            <div class="error-desc">
                Estamos retirando o sistema de operação por motivos de manutenção.<br><br>
                Previsão de retorno dia 09/08/2017 – 17:00h, em caso de necessidade de extensão deste prazo, informaremos aqui!<br>
                <br>
                Gratos pela compreensão<br>
                Equipe de T.I
            </div>
        </div>
        <!-- Mainly scripts -->
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
