<?php

header('Content-Type: text/html; charset=utf-8');
include_once('../model/organismoClass.php');

ini_set('max_execution_time', 999999);
ini_set('memory_limit', '-1');

//Ler EXCEL
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
require_once 'excel_reader2.php';

$o	 	= new organismo();

//Selecionar todos as listas
$resultado = $o->listaOrganismo();

$ultimo_id = $o->selecionaUltimoIdCadastradoPlanilhaSenhasAtualizadas();
						
$data = new Spreadsheet_Excel_Reader("planilha/".$ultimo_id.".xls");
$y=0;
for( $i=1; $i <= $data->rowcount($sheet_index=0); $i++ ){

	//echo utf8_encode($data->val($i, 1));
	
	//Se achar o webmail atualizar a senha
	$email = $data->val($i, 1);
	$senha = $data->val($i, 2);
	$resultado = $o->buscaIdOrganismo(null,trim($email));
	
	if($resultado)
	{
		foreach($resultado as $vetor)
		{
			$org = new organismo();
			$resposta = $org->alteraSenhaEmailOrganismo($vetor['idOrganismoAfiliado'],$senha);
			
			if ($resposta)
			{
				$y++;
				echo $y."-Senha ".utf8_encode($data->val($i, 2))." importado para o e-mail ".$data->val($i, 1)."<br>";
			}else{
				echo "-<font color=red>Não foi possível importar a senha ".utf8_encode($data->val($i, 2))." para o e-mail ".$data->val($i, 1)."</font><br>";
			}
		}	
	}	
}

echo "<br><b>Importação finalizada!</b>";
				
?>