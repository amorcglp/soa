function enviardados() {

    var campos = new Array();
    var mensagem;

    mensagem = "Preencha os seguintes campos: \n";

    if (document.dados.nomeAdmin.value == "" || document.dados.nomeAdmin.value.length < 5) {
        campos.push("Nome");
        document.dados.nomeAdmin.focus();
    }

    if (document.dados.telefoneAdmin.value == "" || document.dados.telefoneAdmin.value.length < 8) {
        campos.push("Telefone");
        document.dados.telefoneAdmin.focus();
    }

    if (document.dados.emailAdmin.value == "" || document.dados.emailAdmin.value.indexOf('@') == -1 || document.dados.emailAdmin.value.indexOf('.') == -1) {
        campos.push("E-mail")
        document.dados.emailAdmin.focus();

    }

    if (document.dados.senhaAdmin.value == "" || document.dados.senhaAdmin.value.length < 6) {
        campos.push("Senha com no mínimo 6 dígitos");
        document.dados.senhaAdmin.focus();
    }

    for (var i = 0; i < campos.length; i++) {

        mensagem += campos[i];
        if (i != campos.length - 1)
            mensagem += "\n ";
    }

    mensagem += ".";
    if (campos.length > 0) {
        alert(mensagem);
        return false;
    }
}

