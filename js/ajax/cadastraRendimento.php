<?php

function retornaCategoriaRendimento($cod) {
    switch ($cod) {
        case 1:
            return "Dinheiro em Caixa";
            break;
        case 2:
            return "Bancos";
            break;
        case 3:
            return "Atrium Martinista";
            break;
        case 4:
            return "Aplicação Poupança";
            break;
        case 5:
            return "Aplicação CDB";
            break;
        case 6:
            return "Aplicação RDB";
            break;
        case 7:
            return "Aplicação Outros";
            break;
    }
}

function retornaAtribuidoARendimento($cod)
{
	switch ($cod)
	{
		case 1:
			return "Organismo Afiliado";
			break;
		case 2:
			return "Região";
			break;
	}
}

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $dataVerificacao = isset($_REQUEST['dataVerificacao']) ? $_REQUEST['dataVerificacao'] : '';
    $descricaoRendimento = isset($_REQUEST['descricaoRendimento']) ? $_REQUEST['descricaoRendimento'] : '';
    $valorRendimento = isset($_REQUEST['valorRendimento']) ? $_REQUEST['valorRendimento'] : '';
    $categoriaRendimento = isset($_REQUEST['categoriaRendimento']) ? $_REQUEST['categoriaRendimento'] : '';
    $atribuidoA = isset($_REQUEST['atribuidoA']) ? $_REQUEST['atribuidoA'] : '';
    $usuario = isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $fk_idOrganismoAfiliado = isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';

    //Tratar data
    $dataVerificacao = substr($dataVerificacao, 6, 4) . "-" . substr($dataVerificacao, 3, 2) . "-" . substr($dataVerificacao, 0, 2);

    $arr = array();
    $arr['status'] = 0;

    include_once('../../model/rendimentoClass.php');
    //include_once('../../lib/functions.php');
    $r = new Rendimento();
    $r->setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado);
    $r->setDataVerificacao($dataVerificacao);
    $r->setDescricaoRendimento($descricaoRendimento);
    $r->setValorRendimento($valorRendimento);
    $r->setCategoriaRendimento($categoriaRendimento);
    $r->setAtribuidoA($atribuidoA);
    $r->setUsuario($usuario);
    $retorno = $r->cadastraRendimento();

    if ($retorno) {
        $ultimo_id = 0;
        $resultado = $r->selecionaUltimoId();
        if ($resultado) {
            foreach ($resultado as $vetor) {
                $proximo_id = $vetor['Auto_increment'];
            }
        }
        $arr['id'] = $proximo_id;
        $resultadoUltimoId = $r->selecionaUltimoIdInserido();
        if($resultadoUltimoId)
        {
            foreach ($resultadoUltimoId as $vetor)
            {
                $ultimo_id = $vetor['idRendimento'];
            }    
        }    
        $arr['ultimoId'] = $ultimo_id;
        $arr['categoriaRendimento'] = retornaCategoriaRendimento($categoriaRendimento);
        $arr['atribuidoA'] = retornaAtribuidoARendimento($atribuidoA);
        $arr['status'] = 1;
    }

    echo json_encode($arr);
}
?>