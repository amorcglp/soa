<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    include_once('../../model/recebimentoClass.php');

    $r = new Recebimento();

    $retorno = array();
    $total = $r->retornaEntrada($_REQUEST['mesAtual'],$_REQUEST['anoAtual'],$_REQUEST['idOrganismoAfiliado']);
    $retorno['totalGeral'] = number_format($total, 2, ',', '.');

    echo json_encode($retorno);
}
?>