<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    
    include_once("../../controller/imovelControleController.php");
    require_once ("../../model/imovelControleClass.php");

    include_once("../../controller/imovelController.php");
    require_once ("../../model/imovelClass.php");

    include_once("../../controller/organismoController.php");
    require_once ("../../model/organismoClass.php");

    $idImovelControle	= isset($_REQUEST['idImovelControle']) ? json_decode($_REQUEST['idImovelControle']) : '';

    $icc = new imovelControleController();
    $dados = $icc->buscaImovelControle($idImovelControle);

    $ic = new imovelController();
    $dadosI = $ic->buscaImovel($dados->getFk_idImovel());

    $oc = new organismoController();
    $dadosOa = $oc->buscaOrganismo($dados->getFk_idOrganismoAfiliado());

    switch ($dadosOa->getClassificacaoOrganismoAfiliado()) {
            case 1:
                    $classificacaoOrganismoAfiliado = "Loja";
                    break;
            case 2:
                    $classificacaoOrganismoAfiliado = "Pronaos";
                    break;
            case 3:
                    $classificacaoOrganismoAfiliado = "Capítulo";
                    break;
            case 4:
                    $classificacaoOrganismoAfiliado = "Heptada";
                    break;
            case 5:
                    $classificacaoOrganismoAfiliado = "Atrium";
                    break;
    }
    switch ($dadosOa->getTipoOrganismoAfiliado()) {
            case 1:
                    $tipoOrganismoAfiliado = "R+C";
                    break;
            case 2:
                    $tipoOrganismoAfiliado = "TOM";
                    break;
    }
    switch($dados->getStatusImovelControle()){
            case 0:
                    $statusImovelControle = '<span class="badge badge-primary">Ativo</span>';
                    break;
            case 1:
                    $statusImovelControle = '<span class="badge badge-danger">Inativo</span>';
                    break;
    }
    switch($dados->getPropriedadeIptuImovelControle()){
            case 0:
                    $propriedadeIptuImovelControle = "Pago";
                    break;
            case 1:
                    $propriedadeIptuImovelControle = "Em debito";
                    break;
            case 2:
                    $propriedadeIptuImovelControle = "Isento";
                    break;
            case 3:
                    $propriedadeIptuImovelControle = "Imune";
                    break;
            case 4:
                    $propriedadeIptuImovelControle = "Outro";
                    break;
    }


    $html = '
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Sistema dos Organismos Afiliados</title>

        <link href="../../css/impressao.css" rel="stylesheet">
    </head>
    <body>
                    <div class="gray-bg" style="min-height: 815px;">
                <div class="wrapper wrapper-content  animated fadeInRight article">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="text-center article-title">
                                        <h1>'.$classificacaoOrganismoAfiliado.' '.$tipoOrganismoAfiliado.' '.$dadosOa->getNomeOrganismoAfiliado().' - '.$dadosOa->getSiglaOrganismoAfiliado().'</h1>
                                    </div>
                                    <hr>
                                            <div class="row">					
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Organismo Afiliado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dadosI->getEnderecoImovel()." - ".$dadosI->getNumeroImovel()." - ".$dadosI->getComplementoImovel().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Status: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <div class="statusTarget'. $dados->getStatusImovelControle().'">
                                                                                                            '.$statusImovelControle.'
                                                                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Mainly scripts 
        <script src="../../js/jquery-2.1.1.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="../../js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
            -->
        <!-- Custom and plugin javascript 
        <script src="../../js/inspinia.js"></script>
        <script src="../../js/plugins/pace/pace.min.js"></script>
        -->
    </body>
    </html>
    ';

    //echo $html;
    //==============================================================
    //==============================================================
    //==============================================================

    include("../../lib/MPDF57/mpdf.php");

    $mpdf=new mPDF('c','A4'); 

    $mpdf->SetDisplayMode('fullpage');

    // LOAD a stylesheet
    $stylesheet_bootstrap		 = file_get_contents('../../css/bootstrap.min.css');
    $stylesheet_font	 		 = file_get_contents('../../font-awesome/css/font-awesome.css');
    $stylesheet_animate			 = file_get_contents('../../css/animate.css');
    $stylesheet_style			 = file_get_contents('../../css/style.css');
    $style_jquery			 	 = file_get_contents('../../js/jquery-2.1.1.js');
    $style_bootstrap			 = file_get_contents('../../js/bootstrap.min.js');
    $style_metisMenu			 = file_get_contents('../../js/plugins/metisMenu/jquery.metisMenu.js');
    $style_slimscroll			 = file_get_contents('../../js/plugins/slimscroll/jquery.slimscroll.min.js');
    $style_inspinia			 	 = file_get_contents('../../js/inspinia.js');
    $style_pace			 		 = file_get_contents('../../js/plugins/pace/pace.min.js');

    /*
    $mpdf->WriteHTML($stylesheet_bootstrap,1);	// The parameter 1 tells that this is css/style only and no body/html/text
    $mpdf->WriteHTML($stylesheet_font,1);
    $mpdf->WriteHTML($stylesheet_animate,1);
    $mpdf->WriteHTML($stylesheet_style,1);
    */
    $mpdf->WriteHTML($style_jquery,1);
    $mpdf->WriteHTML($style_bootstrap,1);
    $mpdf->WriteHTML($style_metisMenu,1);
    $mpdf->WriteHTML($style_slimscroll,1);
    $mpdf->WriteHTML($style_inspinia,1);
    $mpdf->WriteHTML($style_pace,1);

    $mpdf->WriteHTML($html,0);

    $nome = date("y-m-d").'-detalhe-controle-de-documentos-de-imoveis.pdf';

    $mpdf->Output();

    exit;

    //==============================================================
    //==============================================================
    //==============================================================

}