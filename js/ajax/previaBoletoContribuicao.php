<?php 
session_start();
function getRequisitionValidationCode($code='lucianob', $isUser=true) {

		if (!is_numeric($code) && !($isUser)) {
			return false;
		}
		else if (($isUser) && !is_numeric($code)) {
			$code = getUserKeyCode($code);
		}
		else if (($isUser) && is_numeric($code)) {
			return false;
		}

		$date = date('dmYHi')+0;
		//echo $date; exit;

		$codMult = $code * 17;
		
		$rest = floor(fmod($date, $codMult));
		
		
		$i = 9;
		$sum = 0;
		for ($j = strlen($rest); $j > 0; $j--) {
			$sum += (substr($rest, $j-1, 1) * $i); 
			$i--;

			if ($i == 0)
				$i = 9;
		}
		$mod = $sum % 11;

		if ($mod > 9)
			$dv = 0;
		else
			$dv = $mod;

		return $sum.$dv;
}

function getUserKeyCode($user) {
		
		$sum = 0;
		for ($i = 0; $i < strlen($user); $i++) {
			$sum += ord(substr($user, $i, 1)) * ($i+1);
		}
		$sum = $sum * $i;
		
		return $sum;
}

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    ini_set("soap.wsdl_cache_enabled", "0"); 
    date_default_timezone_set('America/Sao_Paulo');
    include_once '../../model/boletoContribuicaoGlpClass.php';
    if(isset($_REQUEST['codigoAfiliacao']))
    {
        $response=array("teste"=>1);
            /**
             * Webservice SOAP
             */

            $client 	= new SoapClient("http://192.1.1.135:8085/wsdl/IAmorcSSDM");
            $client->soap_defencoding = 'UTF-8';
            $client->decode_utf8 = true;
            $DatHoraProces = date('Y-m-d\TH:i:s');
            $NumProces = getRequisitionValidationCode($_REQUEST['codigoAfiliacao'],false);

            $response 		= $client->PreviaBoletoContribuicao(
                                                            $_REQUEST['codigoAfiliacao'],
                                                            $_REQUEST['tipoMembro'],
                                                            $_REQUEST['quantidade'],
                                                            $DatHoraProces,$NumProces);



            $token = md5($DatHoraProces);
            $_SESSION['tokenBoleto']=$token;
            /*
            $bcg = new boletoContribuicaoGlp();

            $bcg->setToken($token);
            $bcg->setCodigoAfiliacao($_REQUEST['codigoAfiliacao']);
            $bcg->setTipoMembro($_REQUEST['tipoMembro']);
            $bcg->setSeqCadast($_REQUEST['seqCadast']);
            $bcg->setCodigoVeiculo(0);
            $bcg->setCodigoContaContribuicao($response->CodContaContribuicao);
            $valorBoleto = (float) $response->ValContribuicao+$response->ValTaxaAfiliacao;
            $bcg->setValorBoleto($valorBoleto);
            $bcg->setQuantidade($_REQUEST['quantidade']);
            $bcg->setNome($_REQUEST['nome']);
            $bcg->setEndereco($_REQUEST['endereco']);
            $bcg->setNumero($_REQUEST['numero']);
            $bcg->setCidade($_REQUEST['cidade']);
            $bcg->setEstado($_REQUEST['estado']);
            $bcg->setCep($_REQUEST['cep']);
            $bcg->setNumeroBoleto(0);
            $bcg->setUsuario($_REQUEST['usuario']);
            $bcg->cadastroBoletoContribuicaoGlp();
            */
            $response->token=$token;

            echo json_encode($response);
    }
}
?>