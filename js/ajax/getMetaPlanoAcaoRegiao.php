<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/planoAcaoRegiaoMetaClass.php');

    $parm = new planoAcaoRegiaoMeta();

    $resultado = $parm->listaMeta(null,null,$_REQUEST['id']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
            $retorno['tituloMeta']		= $vetor['tituloMeta'];
            $retorno['inicio']			= substr($vetor['inicio'],8,2)."/".substr($vetor['inicio'],5,2)."/".substr($vetor['inicio'],0,4);
            $retorno['fim']				= substr($vetor['fim'],8,2)."/".substr($vetor['fim'],5,2)."/".substr($vetor['fim'],0,4);
            $retorno['oque']			= $vetor['oque'];
            $retorno['porque']			= $vetor['porque'];
            $retorno['quem']			= $vetor['quem'];
            $retorno['onde']			= $vetor['onde'];
            $retorno['quando']			= $vetor['quando'];
            $retorno['como']			= $vetor['como'];
            $retorno['quanto']			= $vetor['quanto'];
            $retorno['ordenacao']		= $vetor['ordenacao'];
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>