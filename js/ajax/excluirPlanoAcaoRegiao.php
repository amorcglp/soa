<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/planoAcaoRegiaoClass.php');
    include_once('../../model/planoAcaoRegiaoMetaClass.php');
    include_once('../../model/planoAcaoRegiaoAtualizacaoClass.php');
    include_once('../../model/planoAcaoRegiaoParticipanteClass.php');

    $p = new PlanoAcaoRegiao();

    $m = new PlanoAcaoRegiaoMeta();

    $a = new planoAcaoRegiaoAtualizacao();

    $part = new planoAcaoRegiaoParticipante();

    //remover o plano
    $resultado = $p->excluirPlanoAcaoRegiao($_REQUEST['id']);

    //remover as metas
    $resultado2 = $m->removeMetas($_REQUEST['id']);

    //remover os atualização
    $resultado3 = $a->removeAtualizacoes($_REQUEST['id']);

    //remover participantes
    $resultado4 = $part->removeParticipantes($_REQUEST['id']);

    $arr = array();
    if ($resultado) {
            $arr['status']=1;
    } else {
        $arr['status']=0;
    }
    echo json_encode($arr);
}
?>