<?php
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

require_once '../../loadEnv.php';
/*
 * Token
 */
require_once '../../sec/token.php';

include '../../mailgun.php';

    include_once('../../lib/phpmailer/class.phpmailer.php');

    $REDIRECT_URI =  (getenv('ENVIRONMENT') === 'production' || getenv('ENVIRONMENT') === 'testing') ? 'https://' : 'http://';
    $REDIRECT_URI .= $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'];

    //$loginUsuario = isset($_REQUEST['loginUsuario'])?$_REQUEST['loginUsuario']:null;
    $idFuncionalidade = isset($_REQUEST['idFuncionalidade']) ? $_REQUEST['idFuncionalidade'] : null;
    $idItemFuncionalidade = isset($_REQUEST['idItemFuncionalidade']) ? $_REQUEST['idItemFuncionalidade'] : null;
    $usuarioToken = isset($_REQUEST['usuarioToken']) ? $_REQUEST['usuarioToken'] : null;
    $dataInicial = isset($_REQUEST['dataInicial']) ? $_REQUEST['dataInicial'] : null;
    $dataFinal = isset($_REQUEST['dataFinal']) ? $_REQUEST['dataFinal'] : null;
    $mes = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : 0;       
    $ano = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : 0; 
    $liberarEmMassa = isset($_REQUEST['liberarEmMassa']) ? $_REQUEST['liberarEmMassa'] : 0;
    $idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : 0;

    $dataInicial = substr($dataInicial,6,4)."-".substr($dataInicial,3,2)."-".substr($dataInicial,0,2);
    $dataFinal = substr($dataFinal,6,4)."-".substr($dataFinal,3,2)."-".substr($dataFinal,0,2);

    include_once('../../model/usuarioClass.php');
    $u = new Usuario();
    $arr = array();

    //$u->setLoginUsuario($loginUsuario);
    $u->setLoginUsuario($usuarioToken);

    $arr = $u->buscaUsuarioPorLogin();
    
    include_once('../../model/ticketClass.php');
    $t = new Ticket();
    $arrFuncionalidade = array();
    
    $arrFuncionalidade = $t->listaFuncionalidade($idFuncionalidade);
    
    if($mes=="")
    {
        $mes=0;
    }
    if($ano=="")
    {
        $ano=0;
    }

    include_once('../../controller/organismoController.php');
    $oc = new organismoController();
    $dadosOrg = $oc->buscaOrganismo($idOrganismoAfiliado);
    $nomeCompletoOa = '';
    switch ($dadosOrg->getClassificacaoOrganismoAfiliado()) {
        case 1: $nomeCompletoOa = "Loja "; break;
        case 2: $nomeCompletoOa = "Pronaos "; break;
        case 3: $nomeCompletoOa = "Capítulo "; break;
        case 4: $nomeCompletoOa = "Heptada "; break;
        case 5: $nomeCompletoOa = "Atrium "; break;
    }
    switch ($dadosOrg->getTipoOrganismoAfiliado()) {
        case 1: $nomeCompletoOa .= "R+C "; break;
        case 2: $nomeCompletoOa .= "TOM "; break;
    }
    $nomeCompletoOa .= $dadosOrg->getNomeOrganismoAfiliado();
    if($dadosOrg->getSiglaOrganismoAfiliado()!=null) {
        $nomeCompletoOa .= " - " . $dadosOrg->getSiglaOrganismoAfiliado();
    }


    //echo "<pre>";print_r($arrFuncionalidade);exit();

    if (count($arr)>0) {
        
        /*
         * Gerar Token
         */
        $token= "";
        $timelife=time();
        $valor = "0123456789";
        srand((double)microtime()*1000000);
        for ($i=0; $i<10; $i++){
                $token.= $valor[rand()%strlen($valor)];
        }

        while ($u->retornaTokenIntegridade($token))//enquanto o token j� existir gerar novamente
        {
                $token= "";
                $valor = "0123456789";
                srand((double)microtime()*1000000);
                for ($i=0; $i<10; $i++){
                        $token.= $valor[rand()%strlen($valor)];
                }
        }
        
        $idLiberacao = $u->cadastraIntegridadeTokenUsuario($token,$idFuncionalidade,$idItemFuncionalidade,$usuarioToken,$arr[0]['emailUsuario'],$_SESSION['seqCadast'],time(), $dataInicial, $dataFinal,$mes,$ano,$liberarEmMassa,$idOrganismoAfiliado);

        //Quebrar o nome
        $arrNome = explode(" ", $arr[0]['nomeUsuario']);

        if($liberarEmMassa){
            $modalidade = "Em Massa - isso significa que foram liberados para alteração todos os registros de todos os anos dessa funcionalidade";
        } else {
            $modalidade = 'Mês: '.$mes.' Ano: '.$ano;
        }

        //Enviar email com o token
        $texto = '<div>
            <div dir="ltr">
                    <table class="ecxbody-wrap"
                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                            <tbody>
                                    <tr
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                            <td
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                            <td class="ecxcontainer" width="600"
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                    valign="top">
                                                    <div class="ecxcontent"
                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                            <table class="ecxmain" width="100%" cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                    <tbody>
                                                                            <tr
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                    <td class="ecxcontent-wrap"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                            valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                            style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                            alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                    <br>
                                                                                    <br>
                                                                                    <br>
                                                                                            <div style="text-align: left;"></div>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                    <tbody>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top">' . strtoupper($arrNome[0]) . ',</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top"><b>ID do Protocolo de Liberação: ' . $idLiberacao . '</b></td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Você solicitou um token para alterar a funcionalidade: ' . $arrFuncionalidade[0]['nomeFuncionalidade'] .'.</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Organismo Afiliado: ' . $nomeCompletoOa .'.</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Liberação: ' . $modalidade .'.</td>
                                                                                                            </tr>
                                                                                                            
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top">O período para você alterar a funcionalidade é: <b>' . $_REQUEST['dataInicial'] . ' à '.$_REQUEST['dataFinal'].'</b></td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Acesse a opção abaixo e faça seu login para modificar este item. </td>
                                                                                                            </tr>
                                                                                                            
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top"><a href="'. $REDIRECT_URI .'login.php?liberarTokenUsuario=' . $token . '" class="ecxbtn-primary" style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background: #348eda; border-color: #348eda; border-style: solid; border-width: 10px 20px;" target="_blank" alt="Alterar sua senha no SOA"> Clique aqui para acessar o SOA com o TOKEN</a>
                                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">AMORC - Ordem Rosacruz</td>
                                                                                                            </tr>
                                                                                                    </tbody>
                                                                                            </table></td>
                                                                            </tr>
                                                                    </tbody>
                                                            </table>
                                                            <div class="footer"
                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                    <div style="text-align: left;"></div>
                                                                    <table width="100%"
                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <tbody>
                                                                                    <tr
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                            <td class="ecxaligncenter ecxcontent-block"
                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                    align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2015</td>
                                                                                    </tr>
                                                                            </tbody>
                                                                    </table>
                                                            </div>
                                                    </div>
                                                    <div style="text-align: left;"></div></td>
                                            <td
                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                    </tr>
                            </tbody>
                    </table>
                    </div>
            </div>';

        /*
        $servidor = "smtp.office365.com";
        $porta = "587";
        $usuario = "noresponseglp@amorc.org.br";
        $senha = "@w2xpglp";
        */
        /*
        $servidor = "smtp.outlook.com";
        $porta = "465";
        $usuario = "samufcaldas@hotmail.com.br";
        $senha = "safc3092";
        */
        /*
        $servidor = "smtp.gmail.com";
        $porta = "465";
        $usuario = "cpdglp@gmail.com";
        $senha = "@w2xpglp";
        
        
        #Disparar email
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPSecure = "ssl";
        $mail->SMTPAuth = true;
        $mail->Host = $servidor; // SMTP utilizado
        $mail->Port = $porta;
        $mail->Username = $usuario;
        $mail->Password = $senha;
        $mail->From = 'cpdglp@gmail.com';
        $mail->AddReplyTo("atendimento@amorc.org.br", "Antiga e Mística Ordem Rosacruz - AMORC");
        $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
        //$mail->AddAddress("samufcaldas@hotmail.com.br");
        //$mail->AddAddress("tatiane.jesus@amorc.org.br");
        //$mail->AddAddress("lucianob@amorc.org.br");
        //$mail->AddAddress("braz@amorc.org.br");
        $mail->AddAddress($arr[0]['emailUsuario']);
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
        $mail->Subject = 'Token de Acesso - '.$arrFuncionalidade[0]['nomeFuncionalidade'].$stringCabecalho.' - SOA';
        $mail->Body = $texto;
        $mail->AltBody = strip_tags($texto);
        $enviado = $mail->Send();
        */

        $enviado = $mgClient->sendMessage($domain, array(
                'from'    => 'Atendimento <atendimento@amorc.org.br>',
                'to'      => strtoupper($arrNome[0]).' <'.$arr[0]['emailUsuario'].'>',
                'subject' => 'Token de Acesso - '.$arrFuncionalidade[0]['nomeFuncionalidade'].' - SOA',
                'text'    => 'Seu e-mail não suporta HTML',
                'html'    => $texto));
        if ($enviado) {
            $r = $u->enviouEmailIntegridadeTokenUsuario($token);
            $retorno['status'] = '1';
        } else {
            $r = $u->erroEmailIntegridadeTokenUsuario($token);
            $retorno['status'] = '2';
        }
    } else {
        $retorno['status'] = '0';
    }

    echo json_encode($retorno);
   
?>