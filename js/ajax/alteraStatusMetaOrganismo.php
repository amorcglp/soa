<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $novo_status = ($_REQUEST['status']==1) ? 0 : 1;


    include_once('../../model/planoAcaoOrganismoMetaClass.php');
    $parm = new planoAcaoOrganismoMeta();
    $parm->setIdPlanoAcaoOrganismoMeta($_REQUEST['id']);
    $parm->setStatusMeta($novo_status);
    $arr = $parm->alteraStatusMeta();


    echo json_encode($arr);
}	
?>