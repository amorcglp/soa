<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    session_start();
    include_once("../../model/usuarioClass.php");
    $u = new Usuario();
    $resultado = $u->listaUsuarioMensagensChatNovas($_SESSION['seqCadast'],null,null,null,null,$_REQUEST['nomeUsuario'],1);

    if ($resultado) {
            foreach ($resultado as $vetor) {
                    if ($_SESSION['seqCadast'] != $vetor['seqCadast']) {
                            ?>
    <div class="chat-user">
            <img class="chat-avatar"
                    src="<?php if ($vetor['avatarUsuario'] == "") { ?>img/default-user-small.png<?php } else {
                                                        echo $vetor['avatarUsuario'];
                                                    } ?>"
                    alt="">
            <div class="chat-user-name">
                    <a href="?corpo=chat&para=<?php echo $vetor['seqCadast']; ?>">
                    <?php echo $vetor['nomeUsuario']; ?> 
                    </a>
                    <?php
                    //Verficação se tem mensagem nova para mim
                    $u2 = new Usuario();
                    $resultado2 = $u2->listaUsuarioMensagensChatNovas($_SESSION['seqCadast'], "idChat", 1, $vetor['seqCadast'], 1);
                    $total = count($resultado2);
                    if ($total > 0) {
                            ?>
                    <i class="fa fa-comments"></i>
                    <?php
                    }
                    ?>
            </div>
    </div>
                    <?php
                    }
            }
    }
}
?>