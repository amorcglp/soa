<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $seqCadast 	                = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast']  : '';
    $idAtividadeIniciatica 	    = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica']  : '';

    $retorno=array();

    include_once('../../model/atividadeIniciaticaClass.php');
    include_once('../../model/usuarioClass.php');
    include_once('../../model/organismoClass.php');

    $ai     = new atividadeIniciatica();
    $u      = new Usuario();
    $o      = new organismo();

    function retornaIdTipoObrigacaoORCZ($tipo){
        switch($tipo){
            case "1": return 3; break;
            case "2": return 5; break;
            case "3": return 7; break;
            case "4": return 9; break;
            case "5": return 11; break;
            case "6": return 13; break;
            case "7": return 15; break;
            case "8": return 17; break;
            case "9": return 19; break;
            case "10": return 32; break;
            case "11": return 33; break;
            case "12": return 34; break;
            case "13": return 1; break;
            case "14": return 36; break;
            case "15": return 35; break;
            case "16": return 24; break;
            default: return 0;
        }
    }
    
    /*Retorna Dados Membro por SeqCadast*/
    $seqCadast = $seqCadastMembro;
    $ocultar_json=1;

    include 'retornaDadosMembroPorSeqCadast.php';
    $obj2 = json_decode(json_encode($return),true);
    if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz']==$seqCadast)
    {
        $companheiro="S";
    }else{
        $companheiro="";
    }    

    $retorno['encontrado'] = false;
    $dadosAtividade = $ai->buscaIdAtividadeIniciatica($idAtividadeIniciatica);
    if ($dadosAtividade) {
        foreach($dadosAtividade as $vetorAtividade){
            $retorno['seqCadast']               = $seqCadast;
            $retorno['codigoAfiliacao']         = $obj2['result'][0]['fields']['fCodRosacruz'];
            $retorno['tipoMembro']              = 1;
            $retorno['tipoObrigacao']           = retornaIdTipoObrigacaoORCZ($vetorAtividade['tipoAtividadeIniciatica']);
            $retorno['dataObrigacao']           = $vetorAtividade['dataRealizadaAtividadeIniciatica']."T".$vetorAtividade['horaRealizadaAtividadeIniciatica'];
            $retorno['descricaoLocal']          = $vetorAtividade['localAtividadeIniciatica'];
            $retorno['companheiro']             = $companheiro;
            $retorno['fk_idOrganismoAfiliado']  = $vetorAtividade['fk_idOrganismoAfiliado'];

            $dadosUsuario = $u->buscaUsuario($vetorAtividade['fk_seqCadastAtualizadoPor']);
            if($dadosUsuario){
                foreach($dadosUsuario as $vetorUsuario){
                    $retorno['loginAtualizadoPor']      = $vetorUsuario['loginUsuario'];
                }
            }

            $dadosOrganismo = $o->buscaIdOrganismo($vetorAtividade['fk_idOrganismoAfiliado']);
            if($dadosOrganismo){
                foreach($dadosOrganismo as $vetorOrganismo){
                    $retorno['siglaOA']                 = $vetorOrganismo['siglaOrganismoAfiliado'];
                }
            }
            $retorno['encontrado'] = true;
        }
    }

    echo json_encode($retorno);
}