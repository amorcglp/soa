<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/cidadeClass.php');

    $c = new Cidade();

    $resultado = $c->retornaIdCidade($_REQUEST['cidade'],$_REQUEST['uf']);

    $retorno = array();

    $retorno['idCidade'] = $resultado;
    
    echo json_encode($retorno);
}
?>