<?php
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    

    $idAtividadeIniciatica      = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';
    $tipoOficialSuplente        = isset($_REQUEST['tipoOficialSuplente']) ? $_REQUEST['tipoOficialSuplente'] : '';
    $seqCadastSuplente          = isset($_REQUEST['seqCadastSuplente']) ? $_REQUEST['seqCadastSuplente'] : '';

    include_once('../../model/atividadeIniciaticaClass.php');

    $retorno = Array();

    if(($idAtividadeIniciatica != '') || ($tipoOficialSuplente != '') || ($seqCadastSuplente != '')){

        $aim = new atividadeIniciatica();
        $dados = $aim->verificaExistenciaRegistroAtividadeSuplente($idAtividadeIniciatica);

        if($dados){
            $resultado = $aim->alteraRegistroAtividadeSuplente($idAtividadeIniciatica, $tipoOficialSuplente, $seqCadastSuplente);
            if($resultado){
                $retorno['resposta'] = true;
            } else {
                $retorno['resposta'] = false;
            }
        } else {
            $resultado = $aim->cadastraRegistroAtividadeSuplente($idAtividadeIniciatica, $tipoOficialSuplente, $seqCadastSuplente);
            if($resultado){
                $retorno['resposta'] = true;
            } else {
                $retorno['resposta'] = false;
            }
        }

    } else {
        $retorno['resposta'] = false;
    }
    echo json_encode($retorno);

    exit;
}