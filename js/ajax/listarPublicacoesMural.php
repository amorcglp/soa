<?php

require_once('../../lib/functions.php');
require_once ('../../model/perfilUsuarioClass.php');
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

	//$exibicaoMural		= isset($_POST['exibicaoMural']) ? addslashes($_POST['exibicaoMural']) : '';
	$fk_idRegiaoRosacruz		= isset($_REQUEST['fk_idRegiaoRosacruz']) ? addslashes($_REQUEST['fk_idRegiaoRosacruz']) : '';
	$fk_idOrganismoAfiliado		= isset($_REQUEST['fk_idOrganismoAfiliado']) ? addslashes($_REQUEST['fk_idOrganismoAfiliado']) : '';
	$seqCadast					= isset($_REQUEST['seqCadast']) ? addslashes($_REQUEST['seqCadast']) : '';
    $avatarUsuarioPrincipal     = isset($_REQUEST['avatarUsuario']) ? addslashes($_REQUEST['avatarUsuario']) : '';
	$status		                = isset($_REQUEST['status']) ? addslashes($_REQUEST['status']) : '';

    $pagAtual                   = isset($_REQUEST['pagAtual']) ? addslashes($_REQUEST['pagAtual']) : '';
    $qntItens                   = isset($_REQUEST['qntItens']) ? addslashes($_REQUEST['qntItens']) : '';

    //echo "qntItens=>".$qntItens;

    $filtroFavorito             = isset($_POST['filtroFavorito']) ? addslashes($_REQUEST['filtroFavorito']) : '';
    $filtroPropriasPublicacoes  = isset($_POST['filtroPropriasPublicacoes']) ? addslashes($_REQUEST['filtroPropriasPublicacoes']) : '';

    $filtroCategorias   = isset($_REQUEST['filtroCategoriaSelecionada']) ? json_decode($_REQUEST['filtroCategoriaSelecionada']) : '';
    $filtroPublicos   = isset($_REQUEST['filtroPublicoSelecionado']) ? json_decode($_REQUEST['filtroPublicoSelecionado']) : '';
    $filtroOrganismos   = isset($_REQUEST['filtroOrganismosSelecionados']) ? json_decode($_REQUEST['filtroOrganismosSelecionados']) : '';


    $inicio = ($pagAtual * $qntItens) - $qntItens;


    include_once("../../model/muralClass.php");
    $mural = new Mural();

    $retorno=array();
    $retorno['vetor'] = array();
    $retorno['qtdVetor'] = 0;

    $i=0;

    //echo "<pre>";print_r($filtroCategorias);echo "</pre>";

    $publicacoes      = $mural->listaMural($fk_idRegiaoRosacruz,
                                           $fk_idOrganismoAfiliado,
                                           $status,
                                           $inicio,
                                           $qntItens,
                                           $filtroFavorito,
                                           $filtroPropriasPublicacoes,
                                           $seqCadast,
                                           $filtroCategorias,
                                           $filtroPublicos,
                                           $filtroOrganismos);

    if ($publicacoes) {
        foreach ($publicacoes as $vetorPublicacoes) {

            $retorno['vetor'][$i]['id'] = $vetorPublicacoes['idMural'];

        	$idPublicacao				= $vetorPublicacoes['idMural'];
        	$mensagemMural 				= $vetorPublicacoes['mensagemMural'];


        	$dtCadastroMural 			= substr($vetorPublicacoes["dtCadastroMural"],8,2)."/".
                                          substr($vetorPublicacoes["dtCadastroMural"],5,2)."/".
                                          substr($vetorPublicacoes["dtCadastroMural"],0,4) . " às " . 
                                          substr($vetorPublicacoes["dtCadastroMural"],10,6);

            $dtCadastroMuralTimeAgo     = timeAgo($vetorPublicacoes["dtCadastroMural"]);

        	$dtAtualizacaoMural 			= substr($vetorPublicacoes["dtAtualizacaoMural"],8,2)."/".substr($vetorPublicacoes["dtAtualizacaoMural"],5,2)."/".substr($vetorPublicacoes["dtAtualizacaoMural"],0,4) . " às " . substr($vetorPublicacoes["dtAtualizacaoMural"],10,6);
        	$dtAtualizacaoMuralTimeAgo 	= timeAgo($vetorPublicacoes["dtAtualizacaoMural"]);
        	$nomeUsuario 				= retornaNomeFormatado($vetorPublicacoes['nomeUsuario'],6);
            $avatarUsuario              = $vetorPublicacoes['avatarUsuario'];
            $seqCadast                  = $vetorPublicacoes['seqCadast'];

            $exibicaoMural              = $vetorPublicacoes['exibicaoMural'];
            $fk_idMuralCategoria        = $vetorPublicacoes['fk_idMuralCategoria'];
            $fk_idMuralPublico        = $vetorPublicacoes['fk_idMuralPublico'];
        	$fk_idMuralImagem 	        = $vetorPublicacoes['fk_idMuralImagem'];

            $classificacaoOrganismoAfiliado = $vetorPublicacoes['classificacaoOrganismoAfiliado'];
            $nomeOrganismoAfiliado      = $vetorPublicacoes['nomeOrganismoAfiliado'];
            $siglaOrganismoAfiliado     = $vetorPublicacoes['siglaOrganismoAfiliado'];
            $fk_idOrganismoAfiliado     = $vetorPublicacoes['fk_idOrganismoAfiliado'];

            $favorito                   = $vetorPublicacoes['favorito'];
            $curtido                    = $vetorPublicacoes['curtido'];
            $qnt_curtido                = $vetorPublicacoes['qnt_curtido'];

        	$retorno['vetor'][$i]['socialFooter'] = "";


            if($vetorPublicacoes['fk_seqCadastAtualizadoPor'] == $seqCadast){
                if($sessao->getValue("seqCadast")==431328 //EMANUELLE SPACK - Assessoria de Comunicação -AMORC
                    ||in_array("329",$arrFuncoes) //V. Ext. 151
                    ||in_array("365",$arrFuncoes) //V. Ext. 201
                    ||in_array("367",$arrFuncoes) //V. Ext. 203
                    ||in_array("369",$arrFuncoes) //V. Ext. 205
                    ||in_array("341",$arrFuncoes) //V. Ext. 163
                    //||in_array("155",$arrFuncoes) //V. Ext. 155 Monitor de Organismos Afiliado
                    ||($sessao->getValue("fk_idDepartamento")==2)
                    || ($sessao->getValue("fk_idDepartamento")==3)) {
                    $retorno['vetor'][$i]['socialAction'] = <<<EOT
                        <button data-toggle="dropdown" class="dropdown-toggle btn-white">
                            <i class="fa fa-angle-down"></i> 
                        </button>
                        <ul class="dropdown-menu m-t-xs">
                            <li><a onClick="liberarEdicaoPublicacaoMural($idPublicacao);">Editar</a></li>
                            <li><a onClick="alterarStatusPublicacaoMural($idPublicacao,0);">Excluir</a></li>
                        </ul>
EOT;
                }
            } else {
            $retorno['vetor'][$i]['socialAction'] = "";
            }
            
           /*
            $p = new perfilUsuario();
            $resultadoPerfil = $p->listaAvatarUsuario($seqCadast);
            $src = "a";
            foreach ($resultadoPerfil as $v) {
                $src = $v['avatarUsuario'];
                //$nome = $avatar['nomeUsuario'];
            }
          
            if(!file_exists("./".$src)){
                
               $src = "arquivo existe!";
            }else{
                 $src="./img/default-user.png";
                
            }
            */
            
            
            
            $avatarUsuario =   '../../'.$avatarUsuario;
            if(!file_exists($avatarUsuario)){
                
               $avatarUsuario="../../img/default-user.png";
            }
            
            //$avatarUsuario = str_replace("../../","",$avatarUsuario);
            

            $retorno['vetor'][$i]['socialAvatar'] =$avatarUsuario;
            if(file_exists($avatarUsuario)){
        	$retorno['vetor'][$i]['socialAvatar'] = <<<EOT
        	<a class="pull-left">
                <img alt="image" class="img-circle" src="$avatarUsuario">
            </a>
            <div class="media-body">
                <a>$nomeUsuario</a>
                <small class="text-muted">
                       $dtCadastroMuralTimeAgo - $dtCadastroMural
                </small>
            </div>
EOT;
            }
            if($vetorPublicacoes['curtido'] == 0){
                $curtirBtn = <<<EOT
                <button id="btnCurtir-$idPublicacao-$seqCadast"
                        name="btnCurtir-$idPublicacao-$seqCadast"
                        class="btn btn-white btn-xs"
                        onClick="curtirPublicacao($idPublicacao,$seqCadast);" >
                    <i class="fa fa-thumbs-up"></i> Curtir!
                </button>
EOT;
            } else {
                $curtirBtn = <<<EOT
                <button id="btnCurtir-$idPublicacao-$seqCadast"
                        name="btnCurtir-$idPublicacao-$seqCadast"
                        class="btn btn-primary btn-xs"
                        onClick="descurtirPublicacao($idPublicacao,$seqCadast);" >
                    <i class="fa fa-thumbs-up"></i> Curtir!
                </button>
EOT;
            }

            if($vetorPublicacoes['favorito'] == 0){
                $favoritoBtn = <<<EOT
                <button id="btnFavorito-$idPublicacao-$seqCadast" 
                        name="btnFavorito-$idPublicacao-$seqCadast" 
                        class="btn btn-white btn-xs" 
                        onClick="favoritarPublicacao($idPublicacao,$seqCadast);" >
                    <i class="fa fa-star"></i> Favoritar!
                </button>
EOT;
            } else {
                $favoritoBtn = <<<EOT
                <button id="btnFavorito-$idPublicacao-$seqCadast"
                        name="btnFavorito-$idPublicacao-$seqCadast" 
                        class="btn btn-primary btn-xs" 
                        onClick="desfavoritarPublicacao($idPublicacao,$seqCadast);" >
                    <i class="fa fa-star"></i> Favoritar!
                </button>
EOT;
            }

            if($qnt_curtido == 0){
                $fraseCurtidas = "";
            } elseif($qnt_curtido == 1){
                $fraseCurtidas = $qnt_curtido . " pessoa já curtiu essa publicação!";
            } else {
                $fraseCurtidas = $qnt_curtido . " pessoas já curtiram essa publicação!";
            }

            if ($exibicaoMural == 1) {
                $exibicaoPara = "todos";
            } elseif ($exibicaoMural == 2) {
                $exibicaoPara = "a região";
            } else {
                $exibicaoPara = "o organismo";
            }

            if($fk_idMuralCategoria == 1){
                $categoria = "Atividades";
            } elseif($fk_idMuralCategoria == 2){
                $categoria = "Eventos";
            } elseif($fk_idMuralCategoria == 3){
                $categoria = "Notícias";
            }

            if($fk_idMuralPublico == 1){
                $publico = "Rosacruz";
            } elseif($fk_idMuralPublico == 2){
                $publico = "Martinista";
            } elseif($fk_idMuralPublico == 3){
                $publico = "OGG";
            }

            if($fk_idMuralImagem == 0){
                $imagemPublicacao = "";
            } else {
                $imagemPublicacao = $mural->retornaFullPathImagemPeloId($fk_idMuralImagem);
                $imagemPublicacao = '<br><center><img src="'.$imagemPublicacao.'" class="img-responsive" style="width: 75%"></center>';
                //$imagemPublicacao = '<img src="img/mural/11.jpg" class="img-responsive">';

            }

            switch ($classificacaoOrganismoAfiliado) {
                case 1: $nomeCompletoOa = "Loja "; break;
                case 2: $nomeCompletoOa = "Pronaos "; break;
                case 3: $nomeCompletoOa = "Capítulo "; break;
                case 4: $nomeCompletoOa = "Heptada "; break;
                case 5: $nomeCompletoOa = "Atrium "; break;
            }
            $nomeCompletoOa .= $nomeOrganismoAfiliado . " - " . $siglaOrganismoAfiliado;

        	$retorno['vetor'][$i]['socialBody'] = <<<EOT
            <ul id="tag-list-$idPublicacao" name="tag-list-$idPublicacao" class="tag-list" style="padding: 0; width: 100%; height: 35px">
                <li id="tag-list-exibicao-$idPublicacao" name="tag-list-exibicao-$idPublicacao"><a><i class="fa fa-tag"></i> Exibido para $exibicaoPara</a></li>
                <li id="tag-list-categoria-$idPublicacao" name="tag-list-categoria-$idPublicacao"><a><i class="fa fa-tag"></i> $categoria</a></li>
                <li id="tag-list-publico-$idPublicacao" name="tag-list-publico-$idPublicacao"><a><i class="fa fa-tag"></i> $publico</a></li>
                <li><a><i class="fa fa-tag"></i> $nomeCompletoOa</a></li>

                <input type="hidden" value="$exibicaoMural" id="exibicaoMural-$idPublicacao" name="exibicaoMural-$idPublicacao">
                <input type="hidden" value="$fk_idMuralCategoria" id="fk_idMuralCategoria-$idPublicacao" name="fk_idMuralCategoria-$idPublicacao">
                <input type="hidden" value="$fk_idMuralPublico" id="fk_idMuralPublico-$idPublicacao" name="fk_idMuralPublico-$idPublicacao">
            </ul>
        	<p id="msgMural-$idPublicacao" name="msgMural-$idPublicacao" style="margin-top: 16px">
                $mensagemMural
            </p>
            <input type="hidden" value="$mensagemMural" id="msgMuralExibir-$idPublicacao" name="msgMuralExibir-$idPublicacao">
            <div id="imagemPublicacao-$idPublicacao" name="imagemPublicacao-$idPublicacao">
                $imagemPublicacao
            </div>
            <div class="btn-group" style="width: 100%; margin-top: 16px">
                $favoritoBtn
                $curtirBtn
                <span id="areaQntCurtidas-$idPublicacao-$seqCadast">
                    &nbsp&nbsp
                    $fraseCurtidas
                    <input type="hidden" value="$qnt_curtido" id="qntCurtidas-$idPublicacao-$seqCadast" name="qntCurtidas-$idPublicacao-$seqCadast">
                </span>
            </div>
EOT;

			$comentarios      = $mural->listaComentarioPelaPublicacao($idPublicacao,false);
			if($comentarios){
				foreach($comentarios as $comentarioVetor){

					$idComentario 					= $comentarioVetor['idMuralComentario'];
					$mensagemMuralComentario 		= $comentarioVetor['mensagemMuralComentario'];
					$dtCadastroComentario 			= substr($comentarioVetor['dtCadastroMuralComentario'],8,2)."/".substr($comentarioVetor['dtCadastroMuralComentario'],5,2)."/".substr($comentarioVetor['dtCadastroMuralComentario'],0,4) . " às " . substr($comentarioVetor['dtCadastroMuralComentario'],10,6);
					$dtCadastroComentarioTimeAgo 	= timeAgo($comentarioVetor['dtCadastroMuralComentario']);
					$dtAtualizacaoComentario 		= substr($comentarioVetor['dtAtualizacaoMuralComentario'],8,2)."/".substr($comentarioVetor['dtAtualizacaoMuralComentario'],5,2)."/".substr($comentarioVetor['dtAtualizacaoMuralComentario'],0,4) . " às " . substr($comentarioVetor['dtAtualizacaoMuralComentario'],10,6);
					$dtAtualizacaoComentarioTimeAgo = timeAgo($comentarioVetor['dtAtualizacaoMuralComentario']);
					$nomeUsuarioComentario 			= retornaNomeFormatado($comentarioVetor['nomeUsuario'],6);
        			$avatarUsuarioComentario 		= $comentarioVetor['avatarUsuario'];

					$retorno['vetor'][$i]['socialFooter'] .= <<<EOT
			        	<div id="social-comment$idPublicacao$idComentario" class="social-comment">
			                <a class="pull-left">
			                	<img src="$avatarUsuarioComentario" style="width: 32px;margin-right: 10px;">
			                </a>
			                <div class="media-body">
			                    <a>$nomeUsuarioComentario</a>
			                    $mensagemMuralComentario
			                    <br/>
                                <!--
			                    <a class="small"><i class="fa fa-thumbs-up"></i> 26 Like this! X FALTA X</a> - 
                                -->
			                    <small class="text-muted"> à $dtCadastroComentarioTimeAgo - $dtCadastroComentario</small>
			                </div>
			            </div>
EOT;
				}
			}

        	$retorno['vetor'][$i]['socialFooter'] .= <<<EOT
            <div class="social-comment">
                <a class="pull-left">
                	<img src="$avatarUsuarioPrincipal">
                </a>
                <div class="media-body">
                    <textarea style="width: 700px; height: 65px" id="comentario-publicacao-$idPublicacao" onkeyup="diminuirCaracteresMensagemMural('comentario-publicacao-$idPublicacao','caracteres-comentario-publicacao-$idPublicacao',250)" class="form-control" maxlength="250" placeholder="Escreva um comentário..."></textarea>
                    <div id="caracteres-comentario-publicacao-$idPublicacao">250 caracteres restantes!</div>
                    <button onClick="cadastraComentarioPublicacaoMural($idPublicacao,$seqCadast,'$avatarUsuarioPrincipal');" class="btn btn-white btn-xs"><i class="fa fa-comments"></i> Comentar</button>
                </div>
            </div>
EOT;
			$i++;
        }
    }
    $retorno['qtdVetor'] = $i;

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}
