<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $nomeMembro             = isset($_REQUEST['nomeMembro'])?$_REQUEST['nomeMembro']:NULL;
    //echo $nomeMembro;
    $codigoAfiliacao        = isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    $codigoAfiliacao        = ($codigoAfiliacao=="")? 0 : $codigoAfiliacao;
    //echo $codigoAfiliacao;
    $campoCod               = isset($_REQUEST['campoCod'])?$_REQUEST['campoCod']:NULL;
    $campoNome              = isset($_REQUEST['campoNome'])?$_REQUEST['campoNome']:NULL;
    $campoHiddenSeq         = isset($_REQUEST['campoHiddenSeq'])?$_REQUEST['campoHiddenSeq']:NULL;
    $campoHiddenNome        = isset($_REQUEST['campoHiddenNome'])?$_REQUEST['campoHiddenNome']:NULL;
    $idModal                = isset($_REQUEST['idModal'])?$_REQUEST['idModal']:NULL;
    $tipoMembro             = isset($_REQUEST['tipo'])?$_REQUEST['tipo']:1;
    $chamaFuncao            = isset($_REQUEST['chamaFuncao'])?$_REQUEST['chamaFuncao']:'N';
    $ocultar_json=1;
    include_once('retornaCadastrosFonetica.php');
    $obj = json_decode(json_encode($return),true);
    $arr = $obj['result'][0]['fields']['fArrayMembros'];
    $total = count($arr);
    //echo "<pre>";print_r($arr);echo "</pre>";
    ?>
    <script language="javascript">
    //document.onmousedown=click
    //document.oncontextmenu = new Function("return false;")
    </script>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Pesquisa</h4>
        <small class="font-bold">
            <div id="informacoesPesquisa">
                <?php
                    $str="";
                    if(isset($_REQUEST['codigoAfiliacao']))
                    {    
                        if($_REQUEST['codigoAfiliacao']!="")
                        {
                            $str .= $_REQUEST['codigoAfiliacao']." ";
                        }
                    }
                    if(isset($_REQUEST['nomeMembro']))
                    {    
                        if($_REQUEST['nomeMembro']!="")
                        {
                            $str .= $_REQUEST['nomeMembro'];
                        }
                    }
                    if($total>50)
                    {
                        echo "Sua pesquisa retornou mais de 50 resultados. Por favor refine sua pesquisa!";
                    }else{
                        echo $total." resultado(s) para \"".trim($str)."\"";
                    }   
                ?>
            </div>
        </small>
    </div>
    <?php
    if(count($arr)>0)
    {
            echo "<table class=\"table table-bordered\" oncopy=\"return false\" oncut=\"return false\" onpaste=\"return false\">
                <thead>
                    <tr>
                        <th>C.I</th>
                        <th>Nome</th>
                        <th>Data de Nascimento</th>
                        <th>Cidade</th>
                        <th>UF</th>
                        <th>R+C</th>
                        <th>TOM</th>
                        <th>OGG</th>
                    </tr>
                </thead>
                <tbody>";
            foreach($arr as $vetor){
                if($vetor['fields']['fIdeTipoFiliacRosacruz'] == "I"){//Se for individual
                    $hasTitular = 0;
                }else{
                    $hasTitular = $vetor['fields']['fSeqCadast'] == $vetor['fields']['fSeqCadastPrincipalRosacruz'] ? '0' : '2';
                }

                $dataTratada = substr($vetor['fields']['fDatNascimento'], 8,2)."/".substr($vetor['fields']['fDatNascimento'], 5,2);

    ?>
                <tr>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php echo $vetor['fields']['fSeqCadast'];?></a></td>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php echo $vetor['fields']['fNomCliente'];?></a></td>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php echo $dataTratada;?></a></td>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php echo $vetor['fields']['fNomCidade'];?></a></td>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php echo $vetor['fields']['fSigUf'];?></a></td>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){echo $vetor['fields']['fCodRosacruz'];}else{echo "--";}?></a></td>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php if($vetor['fields']['fIdeTipoTom']!="N"&&$vetor['fields']['fIdeTipoTom']!=""){echo "SIM";}else{echo "NÃO";}?></a></td>
                    <td><a href="#" onclick="carregaCamposPesquisaFonetica('<?php echo $campoHiddenSeq;?>','<?php echo $campoHiddenNome;?>','<?php echo $vetor['fields']['fSeqCadast'];?>','<?php echo $vetor['fields']['fNomCliente'];?>','<?php echo $campoCod;?>','<?php if($vetor['fields']['fCodRosacruz']!="0"&&$vetor['fields']['fCodRosacruz']!=""){ echo $vetor['fields']['fCodRosacruz'];}else{ echo $vetor['fields']['fCodOgg']; }?>','<?php echo $campoNome;?>','<?php echo $idModal;?>','<?php echo $hasTitular;?>');<?php if($chamaFuncao!="N"){ echo $chamaFuncao."();";}?>"><?php if($vetor['fields']['fCodOgg']!="0"&&$vetor['fields']['fCodOgg']!=""){echo $vetor['fields']['fCodOgg'];}else{echo "--";}?></a></td>
                </tr>
                <?php
            }
            echo "</tbody>
            </table>";

    } else {
        echo "Nenhum Resultado Encontrado!";
    }
}
?>