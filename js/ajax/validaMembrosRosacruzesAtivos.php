<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $dataVerificacao			= isset($_REQUEST['dataVerificacao']) ? $_REQUEST['dataVerificacao'] : '';
    $fk_idOrganismoAfiliado		= isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';

    //Tratar data
    $dataVerificacao = substr($dataVerificacao,6,4)."-".substr($dataVerificacao,3,2)."-".substr($dataVerificacao,0,2);
    $mesAtual =substr($dataVerificacao,3,2);
    $anoAtual=substr($dataVerificacao,6,4);        

    $arr=array();
    $arr['status']=0;

    include_once('../../model/membrosRosacruzesAtivosClass.php');

    $r = new MembrosRosacruzesAtivos();
    $r->setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado);

    if($r->validaDataMembrosRosacruzesAtivos($dataVerificacao,$fk_idOrganismoAfiliado))
    {
        $arr['status']=true;
    }else{
        $arr['status']=false;
    }
    echo json_encode($arr);
}	
?>