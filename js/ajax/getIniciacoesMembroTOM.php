<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $seqCadast=isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;
    $codigoAfiliacao=0;
    $ideCompanheiro='N';
    $_REQUEST['tipoMembro']=6;
    $ocultar_json=1;

    include '../../js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayObrigacoes']);
    $arrIniciacoes=array();
    if(isset($obj['result']))
    {
            $arrIniciacoes = $obj['result'][0]['fields']['fArrayObrigacoes'];
    }
    if(count($arrIniciacoes)>0)
    {
            ?>
            <table class="table">
                      <thead>
                              <tr>
                                      <th>Iniciação</th>
                                      <th>Data</th>
                                      <th>Organismo</th>                                        
                              </tr>
                      </thead>   
                      <tbody>
                    <?php 
                            foreach($arrIniciacoes as $vetor)
                            {
                    ?>
                    <tr>
                            <td>
                                    <?php 
                                            $ideCompanheiro='N';
                                            $ideTipoMembro=6;
                                            $seqTipoObriga=$vetor['fields']['fSeqTipoObriga'];
                                            include '../../js/ajax/retornaTipoObrigacaoRitualistica.php';
                                            $obj2 = json_decode(json_encode($return),true);
                                            //echo "<pre>".print_r($obj2['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga']);
                                            echo $obj2['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga'];
                                    ?>
                            </td>
                            <td><?php echo substr($vetor['fields']['fDatObriga'],8,2)."/".substr($vetor['fields']['fDatObriga'],5,2)."/".substr($vetor['fields']['fDatObriga'],0,4);?></td>
                            <td><?php echo $vetor['fields']['fSigOrgafi'];?></td>
                    </tr>
                    <?php 
                            }
                    ?>
            </tbody>
       </table>
            <?php 	
    }else{
            echo "Não possui";
    }
}