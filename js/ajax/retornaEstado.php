<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/estadoClass.php');
    $e = new Estado();

    $resultado = $e->listaEstado($_REQUEST['uf']);
    $arr=array();
    $arr['idEstado'] = 0;
    if ($resultado) {
                    foreach ($resultado as $vetor) {
                            $arr['idEstado'] = $vetor['id'];
                    }
    }

    echo json_encode($arr);
}	
?>