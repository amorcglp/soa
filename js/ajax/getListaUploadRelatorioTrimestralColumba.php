<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    
    session_start();
    $arrNivelUsuario = isset($_SESSION['niveis'])?explode(",",$_SESSION['niveis']):NULL;
    include_once('../../model/relatorioTrimestralColumbaAssinadoClass.php');

    $rfm = new RelatorioTrimestralColumbaAssinado();

    $resultado = $rfm->listaRelatorioTrimestralColumbaAssinado($_REQUEST['trimestreAtual'],$_REQUEST['anoAtual'],$_REQUEST['idOrganismoAfiliado']);
    $i=1;
    if($resultado)
    {
            if (count($resultado)>0) {
                    echo "<ul>";
                    foreach($resultado as $vetor)
                    {
                            ?>
                            <li> 
                                
                                    <a href="<?php echo $vetor['caminho'];?>" target="_blank">Relatório Trimestral de Columba <?php if(count($resultado)>1){?>- Parte <?php echo $i;}?></a>
                                    <?php if(in_array("3",$arrNivelUsuario)){?>
                                    <a href="#" onclick="excluirUploadRelatorioTrimestralColumbaAssinado('<?php echo $vetor['idRelatorioTrimestralColumbaAssinado'];?>','<?php echo $_REQUEST['trimestreAtual'];?>','<?php echo $_REQUEST['anoAtual'];?>','<?php echo $_REQUEST['idOrganismoAfiliado'];?>');"><i class="fa fa-trash"></i></a>
                                    <?php }?>
                            </li>
                            <?php
                            $i++; 
                    }
                    echo "</ul>";
            }
    } else {
        echo "Nenhum Relatório Trimestral de Columba Enviado!";
    }
}
?>