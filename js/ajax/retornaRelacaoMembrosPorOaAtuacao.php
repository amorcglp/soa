<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 

    if(!isset($siglaOA))
    {
            $siglaOA 	= isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:0;
    }

    $ocultar_json 	= isset($ocultar_json)?$ocultar_json:0;

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaRelacaoMembrosPorOaAtuacao';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SigOrgafi' => $siglaOA
    );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    // Imprime o retorno
    if($ocultar_json==0)
    {
            echo json_encode($return);
    }
}
?>