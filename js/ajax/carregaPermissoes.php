<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $departamento	= isset($_REQUEST['departamento']) ? json_decode($_REQUEST['departamento']) : '';
    $funcao 		= isset($_REQUEST['funcao']) ? json_decode($_REQUEST['funcao']) : '';

    //Pegar as opções marcadas para os grupos
    if($departamento!=""&&$departamento!="0")
    {
            include_once('../../model/departamentoSubMenuClass.php');
            $dsm = new DepartamentoSubMenu();
            $dsm->setFkIdDepartamento($departamento);
            $resOpcoesMarcadas = $dsm->buscaPorDepartamento();
            $arrayOpcoesMarcadas = array();
            $arrayOpcoesMarcadas['fk_idSubMenu'][]="";
            if ($resOpcoesMarcadas) {
            foreach ($resOpcoesMarcadas as $vetorOpcoesMarcadas) {
                    $arrayOpcoesMarcadas['fk_idDepartamento'][] = $vetorOpcoesMarcadas['fk_idDepartamento'];
                    $arrayOpcoesMarcadas['fk_idSubMenu'][] = $vetorOpcoesMarcadas['fk_idSubMenu']; 		
            }
            }	

            include_once('../../model/departamentoOpcaoSubMenuClass.php');
            $dosm = new DepartamentoOpcaoSubMenu();
            $dosm->setFkIdDepartamento($departamento);
            $resSubOpcoesMarcadas = $dosm->buscaPorDepartamento();
            $arraySubOpcoesMarcadas = array();
            $arraySubOpcoesMarcadas['fk_idOpcaoSubMenu'][]="";
            if ($resSubOpcoesMarcadas) {
            foreach ($resSubOpcoesMarcadas as $vetorOpcoesMarcadas) {
                    $arraySubOpcoesMarcadas['fk_idDepartamento'][] = $vetorOpcoesMarcadas['fk_idDepartamento'];
                    $arraySubOpcoesMarcadas['fk_idOpcaoSubMenu'][] = $vetorOpcoesMarcadas['fk_idOpcaoSubMenu']; 		
            }
            }

            //Listar

            include_once('../../model/secaoMenuClass.php');
            $secao = new secaoMenu();
            $secoes = $secao->buscaSecaoMenu(0);

            if ($secoes) {
            foreach ($secoes as $vetorSecoes) {
                            echo "<br><b>".$vetorSecoes['nomeSecaoMenu']."</b><br>";

                            include_once('../../model/subMenuClass.php');
                            $submenu = new subMenu();
                            $submenus = $submenu->listaSubMenu($vetorSecoes['idSecaoMenu'],null);

                            if ($submenus) {
                            foreach ($submenus as $vetorSubmenus) {

                                            include_once('../../model/opcaoSubMenuClass.php');
                                            $opcaoSubmenu = new opcaoSubMenu();
                                            $opcoesSubmenu = $opcaoSubmenu->buscaOpcaoSubMenu($vetorSubmenus['idSubMenu'],0);

                                            ?>
                                                    <input icheck type="checkbox" name="opt[]" id="opt<?php echo $vetorSubmenus['idSubMenu'];?>" value="<?php echo $vetorSubmenus['idSubMenu'];?>" onChange="concederPermissao('<?php echo $vetorSubmenus['idSubMenu'];?>');"
                                                            <?php if(in_array($vetorSubmenus['idSubMenu'],$arrayOpcoesMarcadas['fk_idSubMenu'])){?>
                                                                    checked="checked"
                                                            <?php }?>

                                                    >

                                            <?php 
                                            echo $vetorSubmenus['nomeSubMenu']."<br>";

                                            //mostrar subopções
                                            if ($opcoesSubmenu) {
                                            foreach ($opcoesSubmenu as $vetorOpcoesSubmenu) {
                                                            ?>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                    <input icheck type="checkbox" name="optSub[]" id="optSub<?php echo $vetorOpcoesSubmenu['idOpcaoSubMenu'];?>" value="<?php echo $vetorOpcoesSubmenu['idOpcaoSubMenu'];?>" onChange="concederPermissaoSubopcao('<?php echo $vetorOpcoesSubmenu['idOpcaoSubMenu'];?>');"
                                                                            <?php if(in_array($vetorOpcoesSubmenu['idOpcaoSubMenu'],$arraySubOpcoesMarcadas['fk_idOpcaoSubMenu'])){?>
                                                                                    checked="checked"
                                                                            <?php }?>
                                                                    >

                                                            <?php 
                                                            echo $vetorOpcoesSubmenu['nomeOpcaoSubMenu']."<br>";
                                                    }
                                            }
                                    }
                            }

                    }
            }


            //Listar Objetos Dashboard

            include_once('../../model/departamentoObjetoDashboardClass.php');
            $dod = new DepartamentoObjetoDashboard();
            $dod->setFkIdDepartamento($departamento);
            $resOpcoesMarcadasDash = $dod->buscaPorDepartamento();
            $arrayOpcoesMarcadasDash = array();
            $arrayOpcoesMarcadasDash['fk_idObjetoDashboard'][]="";
            if ($resOpcoesMarcadasDash) {
            foreach ($resOpcoesMarcadasDash as $vetorOpcoesMarcadasDash) {
                    $arrayOpcoesMarcadasDash['fk_idDepartamento'][] 	= $vetorOpcoesMarcadasDash['fk_idDepartamento'];
                    $arrayOpcoesMarcadasDash['fk_idObjetoDashboard'][] 	= $vetorOpcoesMarcadasDash['fk_idObjetoDashboard']; 		
            }
            }

            echo "<br><b>Objeto Dashboard</b><br>";

            include_once('../../model/objetoDashboardClass.php');
            $objDash = new objetoDashboard();
            $objetosDash = $objDash->buscaObjetoDashboard();

            if ($objetosDash) {
            foreach ($objetosDash as $vetorObjetosDashboard) {

                            ?>
                            <input type="checkbox" name="optObjetoDashboard[]" id="optObjetoDashboard<?php echo $vetorObjetosDashboard['idObjetoDashboard'];?>" value="<?php echo $vetorObjetosDashboard['idObjetoDashboard'];?>" onChange="concederPermissaoObjetoDashboard('<?php echo $vetorObjetosDashboard['idObjetoDashboard'];?>');"
                                    <?php if(in_array($vetorObjetosDashboard['idObjetoDashboard'],$arrayOpcoesMarcadasDash['fk_idObjetoDashboard'])){?>
                                            checked="checked"
                                    <?php }?>

                            >
                            <?php 
                            echo $vetorObjetosDashboard['nomeObjetoDashboard']."<br>";
                    }
            }

            //Listar Niveis

            include_once('../../model/departamentoNivelDePermissaoClass.php');
            $dndp = new DepartamentoNivelDePermissao();
            $dndp->setFkIdDepartamento($departamento);
            $resOpcoesMarcadasNivel = $dndp->buscaPorDepartamento();
            $arrayOpcoesMarcadasNivel = array();
            $arrayOpcoesMarcadasNivel['fk_idNivelDePermissao'][]="";
            if ($resOpcoesMarcadasNivel) {
            foreach ($resOpcoesMarcadasNivel as $vetorOpcoesMarcadasNivel) {
                    $arrayOpcoesMarcadasNivel['fk_idDepartamento'][] 	= $vetorOpcoesMarcadasNivel['fk_idDepartamento'];
                    $arrayOpcoesMarcadasNivel['fk_idNivelDePermissao'][] = $vetorOpcoesMarcadasNivel['fk_idNivelDePermissao']; 		
            }
            }

            echo "<br><b>Nível</b><br>";
            include_once('../../model/nivelDePermissaoClass.php');
            $nivel = new nivelDePermissao();
            $niveis = $nivel->buscaNivelDePermissao();

            if ($niveis) {
            foreach ($niveis as $vetorNiveis) {
                            ?>
                            <input type="checkbox" name="optNivel[]" id="optNivel<?php echo $vetorNiveis['idNivelDePermissao'];?>" value="<?php echo $vetorNiveis['idNivelDePermissao'];?>" onChange="concederPermissaoNivel('<?php echo $vetorNiveis['idNivelDePermissao'];?>');"
                                    <?php if(in_array($vetorNiveis['idNivelDePermissao'],$arrayOpcoesMarcadasNivel['fk_idNivelDePermissao'])){?>
                                            checked="checked"
                                    <?php }?>

                            >
                            <?php 
                            echo $vetorNiveis['nivelDePermissao']."<br>";
                    }
            }

    }	



    //Pegar as opções marcadas para os subgrupos
    if($funcao!=""&&$funcao!="0")
    {
            include_once('../../model/funcaoSubMenuClass.php');
            $fsm = new FuncaoSubMenu();
            $fsm->setFkIdFuncao($funcao);
            $resOpcoesMarcadasFuncoes = $fsm->buscaPorFuncao();
            $arrayOpcoesMarcadasFuncoes = array();
            $arrayOpcoesMarcadasFuncoes['fk_idSubMenu'][]="";
            if ($resOpcoesMarcadasFuncoes) {
            foreach ($resOpcoesMarcadasFuncoes as $vetorOpcoesMarcadasFuncoes) {
                    $arrayOpcoesMarcadasFuncoes['fk_idFuncao'][] = $vetorOpcoesMarcadasFuncoes['fk_idFuncao'];
                    $arrayOpcoesMarcadasFuncoes['fk_idSubMenu'][] = $vetorOpcoesMarcadasFuncoes['fk_idSubMenu']; 		
            }
            }	

            include_once('../../model/funcaoOpcaoSubMenuClass.php');
            $fosm = new FuncaoOpcaoSubMenu();
            $fosm->setFkIdFuncao($funcao);
            $resSubOpcoesMarcadasFuncoes = $fosm->buscaPorFuncao();
            $arraySubOpcoesMarcadasFuncoes = array();
            $arraySubOpcoesMarcadasFuncoes['fk_idOpcaoSubMenu'][]="";
            if ($resSubOpcoesMarcadasFuncoes) {
            foreach ($resSubOpcoesMarcadasFuncoes as $vetorOpcoesMarcadasFuncoes) {
                    $arraySubOpcoesMarcadasFuncoes['fk_idFuncao'][] = $vetorOpcoesMarcadasFuncoes['fk_idFuncao'];
                    $arraySubOpcoesMarcadasFuncoes['fk_idOpcaoSubMenu'][] = $vetorOpcoesMarcadasFuncoes['fk_idOpcaoSubMenu']; 		
            }
            }

            //Listar

            include_once('../../model/secaoMenuClass.php');
            $secao = new secaoMenu();
            $secoes = $secao->buscaSecaoMenu();

            if ($secoes) {
            foreach ($secoes as $vetorSecoes) {
                            echo "<br><b>".$vetorSecoes['nomeSecaoMenu']."</b><br>";

                            include_once('../../model/subMenuClass.php');
                            $submenu = new subMenu();
                            $submenus = $submenu->listaSubMenu($vetorSecoes['idSecaoMenu']);

                            if ($submenus) {
                            foreach ($submenus as $vetorSubmenus) {

                                            include_once('../../model/opcaoSubMenuClass.php');
                                            $opcaoSubmenu = new opcaoSubMenu();
                                            $opcoesSubmenu = $opcaoSubmenu->buscaOpcaoSubMenu($vetorSubmenus['idSubMenu']);

                                            ?>
                                                    <input icheck type="checkbox" name="opt[]" id="opt<?php echo $vetorSubmenus['idSubMenu'];?>" value="<?php echo $vetorSubmenus['idSubMenu'];?>" onChange="concederPermissaoFuncao('<?php echo $vetorSubmenus['idSubMenu'];?>');"
                                                            <?php if(in_array($vetorSubmenus['idSubMenu'],$arrayOpcoesMarcadasFuncoes['fk_idSubMenu'])){?>
                                                                    checked="checked"
                                                            <?php }?>

                                                    >

                                            <?php 
                                            echo $vetorSubmenus['nomeSubMenu']."<br>";

                                            //mostrar subopções
                                            if ($opcoesSubmenu) {
                                            foreach ($opcoesSubmenu as $vetorOpcoesSubmenu) {
                                                            ?>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                    <input icheck type="checkbox" name="optSub[]" id="optSub<?php echo $vetorOpcoesSubmenu['idOpcaoSubMenu'];?>" value="<?php echo $vetorOpcoesSubmenu['idOpcaoSubMenu'];?>" onChange="concederPermissaoSubopcaoFuncao('<?php echo $vetorOpcoesSubmenu['idOpcaoSubMenu'];?>');"
                                                                            <?php if(in_array($vetorOpcoesSubmenu['idOpcaoSubMenu'],$arraySubOpcoesMarcadasFuncoes['fk_idOpcaoSubMenu'])){?>
                                                                                    checked="checked"
                                                                            <?php }?>
                                                                    >

                                                            <?php 
                                                            echo $vetorOpcoesSubmenu['nomeOpcaoSubMenu']."<br>";
                                                    }
                                            }
                                    }
                            }

                    }
            }


            //Listar Objetos Dashboard

            include_once('../../model/funcaoObjetoDashboardClass.php');
            $fod = new FuncaoObjetoDashboard();
            $fod->setFkIdFuncao($funcao);
            $resOpcoesMarcadasDashFuncoes = $fod->buscaPorFuncao();
            $arrayOpcoesMarcadasDashFuncoes = array();
            $arrayOpcoesMarcadasDashFuncoes['fk_idObjetoDashboard'][]="";
            if ($resOpcoesMarcadasDashFuncoes) {
            foreach ($resOpcoesMarcadasDashFuncoes as $vetorOpcoesMarcadasDashFuncoes) {
                    $arrayOpcoesMarcadasDashFuncoes['fk_idFuncao'][] 			= $vetorOpcoesMarcadasDashFuncoes['fk_idFuncao'];
                    $arrayOpcoesMarcadasDashFuncoes['fk_idObjetoDashboard'][] 	= $vetorOpcoesMarcadasDashFuncoes['fk_idObjetoDashboard']; 		
            }
            }

            echo "<br><b>Objeto Dashboard</b><br>";

            include_once('../../model/objetoDashboardClass.php');
            $objDash = new objetoDashboard();
            $objetosDash = $objDash->buscaObjetoDashboard();

            if ($objetosDash) {
            foreach ($objetosDash as $vetorObjetosDashboard) {

                            ?>
                            <input type="checkbox" name="optObjetoDashboard[]" id="optObjetoDashboard<?php echo $vetorObjetosDashboard['idObjetoDashboard'];?>" value="<?php echo $vetorObjetosDashboard['idObjetoDashboard'];?>" onChange="concederPermissaoObjetoDashboardFuncao('<?php echo $vetorObjetosDashboard['idObjetoDashboard'];?>');"
                                    <?php if(in_array($vetorObjetosDashboard['idObjetoDashboard'],$arrayOpcoesMarcadasDashFuncoes['fk_idObjetoDashboard'])){?>
                                            checked="checked"
                                    <?php }?>

                            >
                            <?php 
                            echo $vetorObjetosDashboard['nomeObjetoDashboard']."<br>";
                    }
            }

            //Listar Niveis

            include_once('../../model/funcaoNivelDePermissaoClass.php');
            $fndp = new FuncaoNivelDePermissao();
            //$fndp->setFkIdFuncao();
            $resOpcoesMarcadasNivelFuncoes = $fndp->buscaPorFuncao($funcao);
            $arrayOpcoesMarcadasNivelFuncoes = array();
            $arrayOpcoesMarcadasNivelFuncoes['fk_idNivelDePermissao'][]="";
            if ($resOpcoesMarcadasNivelFuncoes) {
            foreach ($resOpcoesMarcadasNivelFuncoes as $vetorOpcoesMarcadasNivelFuncoes) {
                    $arrayOpcoesMarcadasNivelFuncoes['fk_idFuncao'][] 	= $vetorOpcoesMarcadasNivelFuncoes['fk_idFuncao'];
                    $arrayOpcoesMarcadasNivelFuncoes['fk_idNivelDePermissao'][] = $vetorOpcoesMarcadasNivelFuncoes['fk_idNivelDePermissao']; 		
            }
            }

            echo "<br><b>Nível</b><br>";
            include_once('../../model/nivelDePermissaoClass.php');
            $nivel = new nivelDePermissao();
            $niveis = $nivel->buscaNivelDePermissao();

            if ($niveis) {
            foreach ($niveis as $vetorNiveis) {
                            ?>
                            <input type="checkbox" name="optNivel[]" id="optNivel<?php echo $vetorNiveis['idNivelDePermissao'];?>" value="<?php echo $vetorNiveis['idNivelDePermissao'];?>" onChange="concederPermissaoNivelFuncao('<?php echo $vetorNiveis['idNivelDePermissao'];?>');"
                                    <?php if(in_array($vetorNiveis['idNivelDePermissao'],$arrayOpcoesMarcadasNivelFuncoes['fk_idNivelDePermissao'])){?>
                                            checked="checked"
                                    <?php }?>

                            >
                            <?php 
                            echo $vetorNiveis['nivelDePermissao']."<br>";
                    }
            }


    }


    exit;
}
?>