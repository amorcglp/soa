<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idAtividadeIniciatica					= isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : null;

    //echo "idAtividadeIniciatica: ".$idAtividadeIniciatica;

    include '../../model/atividadeIniciaticaClass.php';
    $aim = new atividadeIniciatica();

    $retorno=array();
    $resultado = $aim->buscaIdAtividadeIniciatica($idAtividadeIniciatica);
    $retorno['qnt'] = 0;
    if($resultado){
            foreach($resultado as $vetor){

                    $retorno['qnt'] = 1;

                    $retorno['tipo'] = $vetor['tipoAtividadeIniciatica'];

                    $retorno['anotacoes'] = $vetor['anotacoesAtividadeIniciatica'];

                    $retorno['local'] = $vetor['localAtividadeIniciatica'];

                    $retorno['data'] = substr($vetor['dataRealizadaAtividadeIniciatica'],8,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],5,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],0,4).' às '.substr($vetor['horaRealizadaAtividadeIniciatica'],0,5).' horas ';

            switch ($vetor['statusAtividadeIniciatica']) {
                case 0:
                    $status = "<span class='badge badge-primary'>Ativo</span>";
                    break;
                case 1:
                    $status = "<span class='badge badge-danger'>Inativo</span>";
                    break;
                case 2:
                    $status = "<span class='badge badge-success'>Realizada</span>";
                    break;
                case 3:
                    $status = "<span class='badge badge-danger'>Cancelado Posteriormente</span>";
                    break;
            }
            $retorno['status'] = $status;

            }
    }

    echo json_encode($retorno);
}