<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require '../sec/token.php';	
	}else{
		require './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    if(!class_exists("Ws"))
    {
        if(realpath('../../model/wsClass.php')){
                require '../../model/wsClass.php';
        }else{
                if(realpath('../model/wsClass.php')){
                        require '../model/wsClass.php';	
                }else{
                        require './model/wsClass.php';
                }
        } 
    }
    
    $seqCadast 	= isset($seqCadast)?$seqCadast:0;

    if($seqCadast==0)
    {
            $seqCadast 	= isset($_REQUEST['seq_cadast'])?$_REQUEST['seq_cadast']:0;
    }
    $ocultar_json 	= isset($ocultar_json)?$ocultar_json:0;
    $siglaOA	 	= isset($siglaOA)?$siglaOA:'';
    $atuantes		= isset($atuantes)?$atuantes:'S';
    $naoAtuantes	= isset($naoAtuantes)?$naoAtuantes:'N';
    $seqFuncao		= isset($seqFuncao)?$seqFuncao:0;
    $tipoMembro		= isset($tipoMembro)?$tipoMembro:'';
    $nomeParcialFuncao = isset($nomeParcialFuncao)?$nomeParcialFuncao:'';
    if(!isset($siglaPais))
    {
            $siglaPais		= isset($_REQUEST['siglaPais'])?$_REQUEST['siglaPais']:'BR';
    }
    if(!isset($filtro))
    {
            $filtro				= isset($_REQUEST['filtro'])?$_REQUEST['filtro']:null;
    }else{
            $filtro                         = null;
    }
    $server = isset($server) ? $server : null;
    // Instancia a classe
    $ws = new Ws($server);
    // Nome do Método que deseja chamar
    $method = 'RetornaRelacaoOficiais';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SeqCadast' => $seqCadast,
                                    'SeqFuncao' => $seqFuncao,
                                    'NomParcialFuncao' => $nomeParcialFuncao,
                                    'NomLocaliOficial' => '',
                                    'SigRegiaoBrasilOficial' => '',
                                    'SigPaisOficial' => $siglaPais,
                                    'SigOrganismoAfiliado' => $siglaOA,
                                    'SigAgrupamentoRegiao' => '',
                                    'IdeListarAtuantes' => $atuantes,
                                    'IdeListarNaoAtuantes' => $naoAtuantes,
                    'IdeTipoMembro' => $tipoMembro 
                            );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    if($ocultar_json==0)
    {
            // Imprime o retorno
            if($filtro==null)
            {    
                echo json_encode($return);
            }else{
                if($filtro=="externo")
                {
                    $var = $return->result[0];
                    foreach($var->fields->fArrayOficiais as $vetor)
                    {
                        unset($vetor->fields->fSeqCadastOa);
                        unset($vetor->fields->fNomeOa);
                        //unset($vetor->fields->fSigOrgafi);
                        unset($vetor->fields->fSeqCadast);
                        unset($vetor->fields->fNomClient);
                        unset($vetor->fields->fCodMembro);
                        unset($vetor->fields->fDesEmail);
                        unset($vetor->fields->fNumTelefo);
                        unset($vetor->fields->fNumTelefoFixo);
                        unset($vetor->fields->fNumCelula);
                        unset($vetor->fields->fNumOutroTelefo);
                        unset($vetor->fields->fDesOutroTelefo);
                        //$vetor->fields->fCodTipoFuncao;
                        //unset($vetor->fields->fDesTipoFuncao);
                        //$vetor->fields->fCodFuncao;
                        //unset($vetor->fields->fDesFuncao);
                        /*
                        $vetor->fields->fDatEntrad;
                        $vetor->fields->fDatSaida;
                        $vetor->fields->fDatTerminMandat;
                        $vetor->fields->fIdeTipoMotivoSaida;
                         */
                        unset($vetor->fields->fNomLocali);
                        unset($vetor->fields->fSigUf);
                        unset($vetor->fields->fSigPaisEndere);
                        unset($vetor->fields->fSigAgrupamentoRegiao);
                    }    
                }
                echo json_encode($var);
            }
    }
}
?>