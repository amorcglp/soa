<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $codigoAfiliacao=isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:null;
    $ideCompanheiro=isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:'N';
    $numGrauApto=isset($_REQUEST['numGrau'])?$_REQUEST['numGrau']:null;
    $seqCadastMembro=isset($_REQUEST['seqCadastMembro'])?$_REQUEST['seqCadastMembro']:null;

    //echo 'Codigo de Afiliacao: '.$codigoAfiliacao."<br>";
    //echo 'Companheiro: '.$ideCompanheiro."<br>";
    //echo 'Numero de GRAU disponivel pelo LOTE: '.$numGrau."<br>";

    $ocultar_json=1;

    include '../../js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
    include '../../lib/functions.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayObrigacoes']);

    $arrIniciacoes = $obj['result'][0]['fields']['fArrayObrigacoes'];

    //echo "<pre>";print_r($obj['result'][0]['fields']['fArrayObrigacoes']);echo "</pre>";

    $grauWS=0;
    if(count($arrIniciacoes)>0)
    {
                            foreach($arrIniciacoes as $vetor)
                            {
                                    //echo "<pre>";print_r($vetor['fields']['fSeqTipoObriga']);echo "</pre>";

                                    switch($vetor['fields']['fSeqTipoObriga']){
                                            case 1: $grauWS=0; break;
                                            case 3: $grauWS=1; break;
                                            case 5: $grauWS=2; break;
                                            case 7: $grauWS=3; break;
                                            case 9: $grauWS=4; break;
                                            case 11: $grauWS=5; break;
                                            case 13: $grauWS=6; break;
                                            case 15: $grauWS=7; break;
                                            case 17: $grauWS=8; break;
                                            case 19: $grauWS=9; break;
                                            case 32: $grauWS=10; break;
                                            case 33: $grauWS=11; break;
                                            case 34: $grauWS=12; break;
                                            //default: $grauWS=99;
                                    }
                                            //$grauWS=retornaGrauRC($vetor['fields']['fSeqTipoObriga']);
                                            //echo "/grauWS:".$vetor['fields']['fSeqTipoObriga']."/numGrau:".$numGrau."/";
                            }

                            //echo 'Grau encontrado: '.$grauWS.'<br>';

                            if($grauWS==$numGrauApto) {
                                    //echo 'Grau encontrado['.$grauWS.'] == ['.$numGrau.']Grau de Lote'.'<br>';
                                    if($grauWS==12){
                                            echo "Membro já no 12º Grau de Templo";
                                            echo "<input type='hidden' id='proximoGrauDisponivel$seqCadastMembro' name='proximoGrauDisponivel$seqCadastMembro' value='99'>";
                                    } else {
                                            echo "Nenhuma Iniciação Disponível";
                                    }
                                    //echo '<br>';
                            } else {

                                    //echo 'Grau encontrado != Grau Disponivel de Lote'.'<br>';
                                    //echo '$numGrau: '.$numGrau.'<br>';

                                    if($grauWS<$numGrauApto) {

                                            $proximoGrau =$grauWS+1;
                                            $y=0; 
                                            for($i=$proximoGrau;$i<=$numGrauApto;$i++) {

                                                    if($y==0)
                                                    {
                                                            echo $i."º GRAU DE TEMPLO".'<br>';
                                                            echo "<input type='hidden' id='proximoGrauDisponivel$seqCadastMembro' name='proximoGrauDisponivel$seqCadastMembro' value='$i'>";
                                                    }
                                                    $y++;

                                            }

                                    } else {

                                            //echo 'Grau encontrado < Grau Disponivel de Lote'.'<br>';
                                            echo "Ainda não Apto devido ao lote.";
                                            //echo '<br>';
                                    }

                            }
    }else{

            if($grauWS<$numGrauApto) {

                    $proximoGrau =$grauWS+1;
                    $y=0;

                    for($i=$proximoGrau;$i<=$numGrauApto;$i++) {

                            if($y==0)
                            {
                                    echo $i."º GRAU DE TEMPLO".'<br>';
                                    echo "<input type='hidden' id='proximoGrauDisponivel.$seqCadastMembro.' name='proximoGrauDisponivel.$seqCadastMembro.' value='.$i.'>";
                            }
                            $y++;

                    }

            } else {

                    //echo 'Grau encontrado < Grau Disponivel de Lote'.'<br>';
                    echo "Não fez nenhuma iniciação. Mas ainda não Apto devido ao lote";
                    //echo '<br>';
            }
    }
}