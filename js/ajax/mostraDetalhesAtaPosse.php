<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/ataPosseClass.php');
    include_once('../../model/usuarioClass.php');

    $ap = new ataPosse();
    $um = new usuario();
    //echo $_REQUEST['idAta'];
    $resultado = $ap->buscarIdAtaPosse($_REQUEST['idAta']);

    $retorno = array();

    if ($resultado) {
            if (count($resultado)>0) {
                    foreach ($resultado as $vetor) {
                            $data = date_create($vetor['dataPosse']);
                            $retorno['dataPosse'] = date_format($data, 'd/m/Y');
                            $retorno['horaInicioPosse'] = $vetor['horaInicioPosse'];
                            $retorno['enderecoPosse'] = $vetor['enderecoPosse'];
                            $retorno['numeroPosse'] = $vetor['numeroPosse'];
                            $retorno['bairroPosse'] = $vetor['bairroPosse'];
                            $retorno['cidadePosse'] = $vetor['cidadePosse'];
                            $retorno['mestreRetirante'] = $vetor['mestreRetirante'];
                            $retorno['secretarioRetirante'] = $vetor['secretarioRetirante'];
                            $retorno['presidenteJuntaDepositariaRetirante'] = $vetor['presidenteJuntaDepositariaRetirante'];
                            $retorno['detalhesAdicionaisPosse'] = $vetor['detalhesAdicionaisPosse'];

                            $retorno['cadastrado_por'] = "--";
                            if($vetor['fk_idUsuario'] != 0){
                                $resultadoAtualizadoPor = $um->buscaUsuario($vetor['fk_idUsuario']);
                                if ($resultadoAtualizadoPor) {
                                    foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                        $retorno['cadastrado_por']           = $vetorAtualizadoPor['nomeUsuario'];
                                    }
                                }
                            }
                            $retorno['atualizado_por'] = "--";
                            if($vetor['ultimo_atualizar'] != 0){
                                $resultadoAtualizadoPor = $um->buscaUsuario($vetor['ultimo_atualizar']);
                                if ($resultadoAtualizadoPor) {
                                    foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                        $retorno['atualizado_por']           = $vetorAtualizadoPor['nomeUsuario'];
                                    }
                                }
                            }                
                            $retorno['data'] = $vetor['data']!='0000-00-00' ?  substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)."/".substr($vetor['data'],0,4)." ".substr($vetor['data'],11,5) : 'Não Informado';
                            }
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao cadastrar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>