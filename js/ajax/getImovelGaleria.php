<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idImovel 	= isset($_POST['idImovel']) ? addslashes($_POST['idImovel']) : '';

    include_once("../../model/imovelClass.php");
    $ic = new imovel();

    $resultado = $ic->listaImovelGaleria($idImovel);

    $retorno['success'] = true;
    $retorno['sucesso'] = 'Imagem carregada com sucesso!';	
    $retorno['foto'] = array();
    if ($resultado) {
            $retorno['temFoto'] = 1;
            foreach ($resultado as $fotos) {

                    //echo $fotos['full_path'];

                    switch($fotos['ext']) {

                            case 'pdf':
                                    $img = 'img/icon_foto/pdf.png';
                            break;

                            case 'jpg':
                                    $img = 'img/icon_foto/jpg.png';
                            break;	

                            case 'jpeg':
                                    $img = 'img/icon_foto/jpg.png';
                            break;	

                            case 'png':
                                    $img = 'img/icon_foto/png.png';
                            break;	

                            default:
                                    $img = 'img/icon_foto/archive.png';
                            break;
                    }
                    $retorno['foto'][]['id'] = $fotos['idImovelGaleria'];
                    $retorno['foto'][]['img'] = $img;
                    $retorno['foto'][]['nome_original'] = $fotos['nome_original'];
                    $retorno['foto'][]['full_path'] = $fotos['full_path'];
                    $retorno['foto'][]['nome_arquivo'] = $fotos['nome_arquivo'];
                    $retorno['foto'][]['data_envio'] = $fotos['data'];
                    $path=$fotos['full_path'];
                    //echo "path:".$fotos[$i]['full_path'];
                    $retorno['foto'][]['galeria'] = '<a style="margin: 5px" href="http://soa.amorc.org.br/'.$fotos['full_path'].'" title="Image from Unsplash" data-gallery=""><img  style="width: 150px; margin-bottom: 20px" src="img/imovel/galeria/'.$fotos['nome_arquivo'].'"><a style="margin-right: 25px" onclick="excluiImovelGaleria('.$fotos['idImovelGaleria'].','.$idImovel.');"><i class="fa fa-trash"></i></a></a>';
            }

    }else {
            $retorno['temFoto'] = 0;//nenhuma foto
            $retorno['sucess'] = false;
            $retorno['erro'] = 'Erro ao buscar as fotos';	
    }

    //echo "<pre>";print_r($retorno);
    echo json_encode($retorno);
}
?>