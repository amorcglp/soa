<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idTicket 	    = isset($_REQUEST['idTicket']) ? $_REQUEST['idTicket']  : '';
    $statusNovo 	= isset($_REQUEST['statusNovo']) ? $_REQUEST['statusNovo']  : '';

    $arr=array();

    include_once('../../model/ticketClass.php');
    $t = new Ticket();
    $retorno = $t->alteraStatusTicket($idTicket,$statusNovo);

    $arr['alterado'] = false;

    function retornaBotaoNovo($statusNovo,$idTicket){
        switch($statusNovo){
            case 0:
                $novoBotao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-default btn-xs dropdown-toggle'>Aberto <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$idTicket.",1);'>Em andamento</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",3);'>Indeferido!</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",2);'>Fechado</a></li></ul></div>";
                break;
            case 1:
                $novoBotao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-warning btn-xs dropdown-toggle'>Em andamento <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$idTicket.",0);'>Aberto</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",3);'>Indeferido!</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",2);'>Fechado</a></li></ul></div>";
                break;
            case 2:
                $novoBotao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-primary btn-xs dropdown-toggle'>Fechado <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$idTicket.",0);'>Aberto</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",1);'>Em andamento</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",3);'>Indeferido!</a></li></ul></div>";
                break;
            case 3:
                $novoBotao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-danger btn-xs dropdown-toggle'>Indeferido! <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$idTicket.",0);'>Aberto</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",1);'>Em andamento</a></li><li><a onClick='mudaStatusTicket(".$idTicket.",2);'>Fechado</a></li></ul></div>";
                break;
            default:
                $novoBotao = "<div class='btn-group'><button class='btn btn-danger btn-xs dropdown-toggle'>Erro! Atualize a página. </button></div>";
        }
        return $novoBotao;
    }

    if ($retorno) {
        $arr['alterado'] = true;
        $arr['novoBotao'] = retornaBotaoNovo($statusNovo,$idTicket);;
    }

    echo json_encode($arr);
}	
?>