<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    $Campos 		= isset($_REQUEST['Campos'])?$_REQUEST['Campos']:null;
    $seqCadast 		= isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;
    $pastaInterna	= isset($_REQUEST['pastaInterna'])?$_REQUEST['pastaInterna']:null;

    $retorno=array();
    $retorno['status']=0;

    include_once('../../model/emailDeParaClass.php');
    $arr = $Campos['emails'];

    for($i=0;$i<count($arr);$i++)
    {
            $edp = new EmailDePara();
            $resultado = $edp->excluirEmailDefinitivamente($arr[$i], $seqCadast);
    }
    if($resultado)
    {
            $retorno['status']=1;
    }

    echo json_encode($retorno);
}	
?>