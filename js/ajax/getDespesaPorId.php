<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/despesaClass.php');

    $d = new Despesa();

    $resultado = $d->buscarIdDespesa($_REQUEST['id']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $data 										= date_create($vetor['dataDespesa']);
            $retorno['dataDespesa'] 					= date_format($data, 'd/m/Y');
            $retorno['descricaoDespesa'] 				= $vetor['descricaoDespesa'];
            $retorno['pagoA']							= $vetor['pagoA'];
            $retorno['valorDespesa']					= $vetor['valorDespesa'];
            $retorno['categoriaDespesa'] 				= $vetor['categoriaDespesa'];
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>