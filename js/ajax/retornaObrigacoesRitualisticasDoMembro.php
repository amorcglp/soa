<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    }

    $codigoAfiliacao                = isset($codigoAfiliacao)?$codigoAfiliacao:0;
    $seqCadast                = isset($seqCadast)?$seqCadast:0;

    if($codigoAfiliacao==0)
    {
            $codigoAfiliacao 	= isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    }
    if($seqCadast==0)
    {
            $seqCadast 	= isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:0;
    }
    $tipoMembro			= isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"1";

    $ocultar_json 			= isset($ocultar_json)?$ocultar_json:0;
    $ideCompanheiro 		= isset($ideCompanheiro)?$ideCompanheiro:'N';

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaObrigacoesRitualisticasDoMembro';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SeqCadast' => $seqCadast,
                                    'CodMembro' => $codigoAfiliacao,
                                    'IdeTipoMembro' => $tipoMembro,
                                    'SeqTipoObriga' => '0',
                                    'IdeDescarteOaSituacao' => 'N',
                                    'SigOrgafi' => '',
                                    'SigPaisOa' => '',
                                    'SigUfOa' => '',
                                    'NumSubRegiao' => '0',
                                    'SigAgrupamentoRegiao' => '',
                                    'IdeTipoOa' => '',
                                    'IdeCompanheiro' => $ideCompanheiro
                                    );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');
    if($ocultar_json==0)
    {
            // Imprime o retorno
            echo json_encode($return);
    }
}
?>