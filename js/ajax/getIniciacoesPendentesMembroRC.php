<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
    require_once '../../sec/token.php';
}else{
    if(realpath('../sec/token.php')){
        require_once '../sec/token.php';
    }else{
        require_once './sec/token.php';
    }
}

function grauTempoCasa($meses)
{
    $grauTempoCasa="";
    switch ($meses)
    {
        case $meses >= 135:
            $grauTempoCasa=12;
            break;
        case $meses >= 96:
            $grauTempoCasa=11;
            break;
        case $meses >= 69:
            $grauTempoCasa=10;
            break;
        case $meses >= 57:
            $grauTempoCasa=9;
            break;
        case $meses >= 48:
            $grauTempoCasa=8;
            break;
        case $meses >= 39:
            $grauTempoCasa=7;
            break;
        case $meses >= 33:
            $grauTempoCasa=6;
            break;
        case $meses >= 30:
            $grauTempoCasa=5;
            break;
        case $meses >= 27:
            $grauTempoCasa=4;
            break;
        case $meses >= 24:
            $grauTempoCasa=3;
            break;
        case $meses >= 21:
            $grauTempoCasa=2;
            break;
        case $meses >= 18:
            $grauTempoCasa=1;
            break;
        case $meses < 18:
            $grauTempoCasa=0;
            break;
        default:
            $grauTempoCasa=0;
            break;
    }
    return $grauTempoCasa;
}

if($tokenLiberado)
{
    $companheiro = isset($_REQUEST['companheiro'])? $_REQUEST['companheiro']:null;
    $dataAdmissao = isset($_REQUEST['dataAdmissao'])? substr($_REQUEST['dataAdmissao'],0,10):null;
    //echo $dataAdmissao;
    $hoje = date('Y-m-d');

    $diferenca = strtotime($hoje) - strtotime($dataAdmissao);

    $meses = floor($diferenca / (60 * 60 * 24 * 30));

    //echo "meses:".$meses;
    $grauTempoCasa = grauTempoCasa($meses);

    //echo "G=>".$grauTempoCasa."<br>";

    $seqCadast=isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;
    //echo "Seq:".$seqCadast;
    $codigoAfiliacao=0;
    //$ideCompanheiro=isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:'N';
    $numGrau=isset($_REQUEST['numGrau'])?$_REQUEST['numGrau']:null;
    //echo "numGrau====>".$numGrau;
    $lote=isset($_REQUEST['lote'])?$_REQUEST['lote']:null;

    $ocultar_json=1;

    include '../../js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
    include '../../lib/functions.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayObrigacoes']);

    $arrIniciacoes = $obj['result'][0]['fields']['fArrayObrigacoes'];
    $grauWS=0;
    if($lote<6)
    {
        $grauWS=0;
    }else{
        $grauWS=1;
    }
    $o=0;
    $arrGraus=array();
    $arrDescricao=array();
    $concluiu=false;
    //echo "<pre>";print_r($arrIniciacoes);
    if(isset($arrIniciacoes))
    {
        foreach($arrIniciacoes as $vetor)
        {
            $seqTipoObriga=$vetor['fields']['fSeqTipoObriga'];
            $ideTipoMembro=1;
            include '../../js/ajax/retornaTipoObrigacaoRitualistica.php';
            $obj = json_decode(json_encode($return),true);
            $descricao = $obj['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga'];
            $arrDescricao = explode(".",$descricao);
            if(isset($arrDescricao[1]))
            {
                if(substr(trim($arrDescricao[1]),0,4)=="Grau"||substr(trim($arrDescricao[1]),0,4)=="GRAU")
                {
                    $arrGraus[] = $arrDescricao[0];
                    if($arrDescricao[0]==12)
                    {
                        $concluiu=true;
                    }
                }
            }
        }
        //echo "<pre>";print_r($arrGraus);
        if(count($arrGraus)>0)
        {
            $grauWS =  max($arrGraus);
        }
        $pote=0;
        //echo "numGrau:".$numGrau."/";
        $arrAux = explode("º", $numGrau);
        $numGrau=$arrAux[0];
        if($grauWS<=$numGrau)
        {
            //$proximoGrau =$grauWS+1;
            $y=0;
            for($i=1;$i<=$numGrau;$i++)
            {
                if(!in_array($i,$arrGraus))
                {
                    //echo "entrou aqui companheiro:".$companheiro;
                    if($companheiro=="S")
                    {
                        if($i<=$grauTempoCasa) {//Apenas para companheiro
                            if ($y == 0) {
                                echo $i . "º GRAU DE TEMPLO";
                            } else {
                                echo ", " . $i . "º GRAU DE TEMPLO";
                            }
                            $pote++;
                            $y++;
                        }
                    }else{
                        if ($y == 0) {
                            echo $i . "º GRAU DE TEMPLO";
                        } else {
                            echo ", " . $i . "º GRAU DE TEMPLO";
                        }
                        $pote++;
                        $y++;
                    }

                }
            }
        }
        if($concluiu)
        {
            echo "Concluiu todas as iniciações";
        }else{
            if($pote==0)
            {
                echo "Nenhuma";
            }
        }





    }else{
        for($i=1;$i<=$numGrau;$i++)
        {

            if($i==1)
            {
                echo $i."º GRAU DE TEMPLO";
            }else{
                echo ", ".$i."º GRAU DE TEMPLO";
            }
        }
        //echo "Não fez nenhuma iniciação";
    }
}