<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idImovel 	                     = isset($_REQUEST['idImovel']) ? $_REQUEST['idImovel']  : '';
    $numeroImovelMatricula 	         = isset($_REQUEST['numeroImovelMatricula']) ? $_REQUEST['numeroImovelMatricula']  : '';

    //echo "numeroImovelMatricula: " . $numeroImovelMatricula;

    $arr=array();

    include_once('../../model/imovelClass.php');

    $i              = new imovel();

    $arr['cadastrado']      = false;
    $arr['msg']             = 'Erro!';


    if($i->verificaJaExisteImovelMatricula($idImovel,$numeroImovelMatricula)){
        $arr['msg']             = 'Número de matrícula já cadastrada para este imóvel!';
    } else {
        $retorno        = $i->cadastroImovelMatricula($idImovel,$numeroImovelMatricula);
        if ($retorno) {
            $arr['cadastrado']      = true;
            $arr['msg']             = '';
        }
    }

    echo json_encode($arr);
}	
?>