<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $from = isset($_REQUEST['from']) ? $_REQUEST['from'] : '';
    $para = isset($_REQUEST['para']) ? $_REQUEST['para'] : '';

    $arr = array();
    $arr['status'] = 0;

    include_once("../../model/perfilUsuarioClass.php");
    $perfilUsuario = new perfilUsuario();

    include_once('../../model/chatClass.php');
    $c = new chat();
    $c->setFromChat($from);
    $c->setParaChat($para);
    $msgs = $c->selecionaChat();
    $nome = $c->getNomeDestino();

    if (isset($msgs) && count($msgs) > 0) {
        for ($i = 0; $i < count($msgs['idChat']); $i++) {
            if ($msgs['from'][$i] == $para && $msgs['para'][$i] == $from) {
                    $resultado = $perfilUsuario->listaAvatarUsuario($para);
                $avatar="";
                foreach ($resultado as $vetor)
                {
                    $avatar = $vetor['avatarUsuario'];
                }
                $avatar =   '../../'.$avatar;
                ?>

                <div class="chat-message left">
                                        <img class="message-avatar" src="<?php if ($avatar == ""|| !file_exists($avatar)) { ?>img/default-user-small.png<?php } else { str_replace("../../","",$avatar); echo $avatar;} ?>" alt="" >
                                        <div class="message">
                                            <a class="message-author" href="#"> <?php echo $nome; ?> </a>
						<span class="message-date"> <?php echo $msgs['data'][$i]; ?> </span>
                                            <span class="message-content">
						<?php echo $msgs['mensagem'][$i]; ?>
                                            </span>
                                        </div>
                                    </div>
                <?php
            }
            if ($msgs['from'][$i] == $from && $msgs['para'][$i] == $para) {
                    $resultado = $perfilUsuario->listaAvatarUsuario($from);
                $avatar="";
                foreach ($resultado as $vetor)
                {
                    $avatar = $vetor['avatarUsuario'];
                }
                $avatar =   '../../'.$avatar;
                ?>
<div class="chat-message right">
                                        <img class="message-avatar" src="<?php if ($avatar == ""|| !file_exists($avatar)) { ?>img/default-user-small.png<?php } else { str_replace("../../","",$avatar);echo $avatar;} ?>" alt="" >
                                        <div class="message">
                                            <a class="message-author" href="#"> Eu </a>
                                            <span class="message-date">  <?php echo $msgs['data'][$i]; ?> </span>
                                            <span class="message-content">
						<?php echo $msgs['mensagem'][$i]; ?>
                                            </span>
                                        </div>
                                    </div>
                
                <?php
            }
        }
    }
}
?>