<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $status 	                = isset($_REQUEST['status']) ? $_REQUEST['status']  : '';
    $seqCadast 	                = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast']  : '';
    $camposGeraisData 	        = isset($_REQUEST['camposGeraisData']) ? json_decode($_REQUEST['camposGeraisData']) : 0;

    $arr=array();

    include_once('../../model/notificacaoGlpClass.php');
    $ngm = new notificacaoGlp();

    $arr['ok'] = true;

    for($i=0; $i < count($camposGeraisData); $i++) {
        $retorno = $ngm->alteraStatusNotificacao($status, $camposGeraisData[$i],$seqCadast);
    }
    echo json_encode($arr);
}	
?>