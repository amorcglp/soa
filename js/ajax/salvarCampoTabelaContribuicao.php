<?php

$tipo      = isset($_REQUEST['tipo']) ? $_REQUEST['tipo'] : '';
$campoDB   = isset($_REQUEST['campoDB']) ? $_REQUEST['campoDB'] : '';
$valor     = isset($_REQUEST['valor']) ? $_REQUEST['valor'] : '';

include_once('../../model/tabelaContribuicaoClass.php');

$t = new tabelaContribuicao();

$retorno = array();

$resultado = $t->editarCampoTabelaContribuicao($tipo,$campoDB, $valor);

if ($resultado) {
    $retorno['sucesso'] = true;
    $retorno['success'] = 'Tabela salva com sucesso!';

} else {
    $retorno['sucesso'] = false;
    $retorno['erro'] = 'Ocorreu algum erro ao salvar a tabela. Tente novamente ou entre em contato com o setor de Organismos Afiliados';
}


echo json_encode($retorno);

?>