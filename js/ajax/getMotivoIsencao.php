<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    include_once('../../model/isencaoOAClass.php');

    $i = new isencaoOA();

    $resultado = $i->buscaIsencaoOaPeloId($_REQUEST['id']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $dataInicio								= date_create($vetor['dataInicial']);
            $retorno['dataInicial']					= date_format($dataInicio, 'd/m/Y');
            $dataFim								= date_create($vetor['dataFinal']);
            $retorno['dataFinal']					= date_format($dataFim, 'd/m/Y');
            $retorno['motivo'] 						= $vetor['motivo'];
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>