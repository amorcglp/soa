<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    session_start();
    $arrNivelUsuario = isset($_SESSION['niveis'])?explode(",",$_SESSION['niveis']):NULL;
    include_once('../../model/relatorioFinanceiroMensalClass.php');

    $rfm = new RelatorioFinanceiroMensal();

    $resultado = $rfm->listaRelatorioFinanceiroMensal($_REQUEST['mesAtual'],$_REQUEST['anoAtual'],$_REQUEST['idOrganismoAfiliado']);
    $i=1;
    if($resultado)
    {
            if (count($resultado)>0) {
                    echo "<ul>";
                    foreach($resultado as $vetor)
                    {
                            ?>
                            <li>
                                    <a href="<?php echo $vetor['caminhoRelatorioFinanceiroMensal'];?>" target="_blank">Relatório Financeiro Mensal <?php if(count($resultado)>1){?>- Parte <?php echo $i;}?></a>
                                    <?php if(in_array("3",$arrNivelUsuario)){?>
                                    <a href="#" onclick="excluirUploadRelatorioFinanceiroMensal('<?php echo $vetor['idRelatorioFinanceiroMensal'];?>','<?php echo $_REQUEST['mesAtual'];?>','<?php echo $_REQUEST['anoAtual'];?>','<?php echo $_REQUEST['idOrganismoAfiliado'];?>');"><i class="fa fa-trash"></i></a>
                                    <?php }?>
                            </li>
                            <?php
                            $i++; 
                    }
                    echo "</ul>";
            }
    } else {
        echo "Nenhum Relatório Financeiro enviado através do formato de entrega anterior (assinatura por digitalização de papel)!";
    }
}
?>