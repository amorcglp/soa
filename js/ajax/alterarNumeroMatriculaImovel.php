<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idImovel 	                     = isset($_REQUEST['idImovel']) ? $_REQUEST['idImovel']  : '';
    $numeroImovelMatricula 	         = isset($_REQUEST['numeroImovelMatricula']) ? $_REQUEST['numeroImovelMatricula']  : '';
    $numeroImovelMatriculaNovo 	     = isset($_REQUEST['numeroImovelMatriculaNovo']) ? $_REQUEST['numeroImovelMatriculaNovo']  : '';

    //echo "numeroImovelMatricula: " . $numeroImovelMatricula;

    $arr=array();

    include_once('../../model/imovelClass.php');
    $i              = new imovel();
    
    $arr['alterado'] = false;
    $arr['msg']             = 'Erro!';

    if($i->verificaJaExisteImovelMatricula($idImovel,$numeroImovelMatriculaNovo)){
        $arr['msg']             = 'Número de matrícula já cadastrada para este imóvel!';
    } else {
        $retorno        = $i->alteraImovelMatricula($idImovel,$numeroImovelMatricula,$numeroImovelMatriculaNovo);
        if ($retorno) {
            $arr['alterado']        = true;
            $arr['msg']             = '';
        }
    }

    echo json_encode($arr);
}	
?>