<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idPublicacao      = isset($_REQUEST['idPublicacao']) ? $_REQUEST['idPublicacao'] : '';
    $idNovaImagem      = isset($_REQUEST['idNovaImagem']) ? $_REQUEST['idNovaImagem'] : '';

    include_once('../../model/muralClass.php');

    $m = new Mural();

    $retorno = array();

    $resultado = $m->alteraImagemPublicacaoMural($idPublicacao,$idNovaImagem);
    $full_path = $m->retornaFullPathImagemPeloId($idNovaImagem);
    $retorno['full_path'] = $full_path;

    if ($resultado) {
        $retorno['full_path'] = '<br><img src="'.$full_path.'" class="img-responsive" style="width: 100%">';
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Imagem da publicação alterada com sucesso!';

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu um erro, tente novamente mais tarde!';
    }


    echo json_encode($retorno);
}
?>