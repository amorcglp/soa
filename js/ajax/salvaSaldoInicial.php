<?php

$db = include('../../firebase.php');

/*
 * Token
 */

$includePath = [
    '../../sec/token.php',
    '../sec/token.php',
    './sec/token.php'
];

foreach($includePath as $path) {
    if(realpath($path)) {
        require_once $path;
        break;
    }
}

if($tokenLiberado)
{

    include_once('../../lib/functions.php');
    include_once('../../model/relatorioFinanceiroMensalClass.php');
    include_once('../../model/usuarioClass.php');
    include_once('../../model/organismoClass.php');
    include_once('../../model/recebimentoClass.php');
    include_once('../../model/despesaClass.php');
    include_once('../../model/saldoInicialClass.php');

    $dataSaldoInicialJS		= isset($_REQUEST['dataSaldoInicial']) ? $_REQUEST['dataSaldoInicial'] : '';
    $saldoInicial			= isset($_REQUEST['saldoInicial']) ? $_REQUEST['saldoInicial'] : '';
    $usuario				= isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $fk_idOrganismoAfiliado	= isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';
    $salvar					= isset($_REQUEST['salvar']) ? $_REQUEST['salvar'] : '';
    $idSaldoInicial			= isset($_REQUEST['idSaldoInicial']) ? $_REQUEST['idSaldoInicial'] : '';


    $arr=array();
    $arr['status']=0;


    //Tratar data para mysql
    $dataSaldoInicial = substr($dataSaldoInicialJS,6,4)."-".substr($dataSaldoInicialJS,3,2)."-".substr($dataSaldoInicialJS,0,2);

    $mesSaldoInicial = substr($dataSaldoInicialJS,3,2);
    $anoSaldoInicial = substr($dataSaldoInicialJS,6,4);

    $mesString = strval($mesSaldoInicial);
    $anoString = strval($anoSaldoInicial);

    $si = new saldoInicial();
    $r = new Recebimento();
    $d = new Despesa();

    $mes = substr($dataSaldoInicialJS,5,2);
    $ano = substr($dataSaldoInicialJS,0,4);

    //Montar "start" projeção
    $mesInt = intval($mes);
    $delimitadorProjecao = "@".$mesInt."@".$ano;

    $saldoAnterior = floatval(str_replace(",",".",str_replace(".","",$saldoInicial)));

    $valorSaldoInicialString = strval(str_replace(",",".",str_replace(".","",$saldoInicial)));

    $arr = retornaSaldoRetornoArray($r,$d,$mes,$ano,$fk_idOrganismoAfiliado,$saldoAnterior, null,0,null,0);

    //Salva Saldo Inicial

    $si = new saldoInicial();
    $si->setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado);
    $si->setDataSaldoInicial($dataSaldoInicial);
    $si->setSaldoInicial($saldoInicial);
    $si->setUsuario($usuario);


    if($salvar==0)
    {
        $retorno = $si->cadastroSaldoInicial();
    }else{
        $si->setUltimoAtualizar($usuario);
        $retorno = $si->atualizaSaldoInicial($idSaldoInicial);
    }

    /**
     * Firebase
     */

//    //Pegar informações do saldo no firebase usuário que cadastrou o si e data que foi cadastrado
//    $docRef = $db->collection('balances')->document($fk_idOrganismoAfiliado);
//    $snapshot = $docRef->snapshot();
//
//    /*
//     * Imprimir variáveis para ver se tem todos os parâmetros necessários
//     */
//
//    //Percorrer recebimentos
//    if ($snapshot->exists()) {
//
//        $docRef->update([
//            ['path' => 'dateOpeningBalance', 'value' => $dataSaldoInicialJS." 00:00:00"],
//            ['path' => 'lastUpdatedOpeningBalance', 'value' => strval($usuario)],
//            ['path' => 'monthOpeningBalance', 'value' => $mesString],
//            ['path' => 'yearOpeningBalance', 'value' => $anoString],
//            ['path' => 'updateData', 'value' => date('d/m/Y H:i:s')],
//            ['path' => 'valueOpeningBalance', 'value' => $valorSaldoInicialString],
//            ['path' => 'balance', 'value' => 0],
//            ['path' => 'status', 'value' => true]
//        ]);
//
//    } else {
//
//        $docRef->set([
//                'balance' => 0,
//                'dateOpeningBalance' => $dataSaldoInicialJS." 00:00:00",
//                'lastUpdatedOpeningBalance' => strval($usuario),
//                'monthOpeningBalance' => $mesString,
//                'status' => true,
//                'updateData' => date('d/m/Y H:i:s'),
//                'userOpeningBalance' => strval($usuario),
//                'valueOpeningBalance' => $valorSaldoInicialString,
//                'yearOpeningBalance' => $anoString
//            ]);
//    }

    echo json_encode($retorno);
}	
?>