<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $seqCadast = isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;
    $idVideoAula = isset($_REQUEST['idVideoAula'])?$_REQUEST['idVideoAula']:null;
    
    include_once('../../model/videoAulaVisualizacaoClass.php');

    $v = new videoAulaVisualizacao();
    $v->setSeqCadast($seqCadast);
    $v->setFk_idVideoAula($idVideoAula);
    //echo "<pre>";print_r($v);
    $resultado = $v->cadastroVideoAulaVisualizacao();

    $arr=array();
    if($resultado)
    {
       $arr['status'] = "1";
    } else {
       $arr['status'] = "0";
    }

    echo json_encode($arr);
}
?>