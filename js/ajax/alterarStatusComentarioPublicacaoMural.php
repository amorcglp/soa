<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idComentario      = isset($_REQUEST['idComentario']) ? $_REQUEST['idComentario'] : '';
    $status      = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';

    include_once('../../model/muralClass.php');

    $m = new Mural();

    $retorno = array();

    $resultado = $m->alterarStatusComentarioPublicacaoMural($idComentario,$status);

    if ($resultado) {
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Comentário inativado com sucesso!';

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao inativar o Comentário. Tente novamente ou entre em contato com o setor de Organismos Afiliados';
    }


    echo json_encode($retorno);
}
?>