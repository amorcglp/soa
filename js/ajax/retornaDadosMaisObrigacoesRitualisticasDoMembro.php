<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    } else {
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';
            } else {
                    require_once './model/wsClass.php';
            }
    }

    $codigoAfiliacao                = isset($codigoAfiliacao)?$codigoAfiliacao:0;
    $seqCadast                			= isset($seqCadast)?$seqCadast:0;

    if($codigoAfiliacao==0) {
	    $codigoAfiliacao 	= isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    }
    if($seqCadast==0) {
    	$seqCadast 	= isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:0;
    }
    $tipoMembro			= isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"1";
    $ocultar_json 			= isset($ocultar_json)?$ocultar_json:0;
    $ideCompanheiro 		= isset($ideCompanheiro)?$ideCompanheiro:'N';

		$retorno = Array();

    $ws = new Ws();
    $method = 'RetornaObrigacoesRitualisticasDoMembro';
    $params = array('CodUsuario' => 'lucianob',
                    'SeqCadast' => $seqCadast,
                    'CodMembro' => $codigoAfiliacao,
                    'IdeTipoMembro' => $tipoMembro,
                    'SeqTipoObriga' => '0',
                    'IdeDescarteOaSituacao' => 'N',
                    'SigOrgafi' => '',
                    'SigPaisOa' => '',
                    'SigUfOa' => '',
                    'NumSubRegiao' => '0',
                    'SigAgrupamentoRegiao' => '',
                    'IdeTipoOa' => '',
                    'IdeCompanheiro' => $ideCompanheiro
                    );
    $return = $ws->callMethod($method, $params, 'lucianob');

    $ws2 = new Ws();
    $method2 = 'RetornaDadosMembroPorSeqCadast';
    $params2 = array('CodUsuario' => 'lucianob',
                     'SeqCadast' => $seqCadast
									  );
    $return2 = $ws2->callMethod($method2, $params2, 'lucianob');

    //print_r($return2->result[0]);

		$retorno['fNomCliente'] 								= $return2->result[0]->fields->fNomCliente;
		$retorno['fDesEmail'] 									= $return2->result[0]->fields->fDesEmail;
		$retorno['fNumTelefoFixo'] 							= $return2->result[0]->fields->fNumTelefoFixo;
		$retorno['fNumCelula'] 									= $return2->result[0]->fields->fNumCelula;
		$retorno['fNumOutroTelefo'] 						= $return2->result[0]->fields->fNumOutroTelefo;
		$retorno['fCodRosacruz'] 								= $return2->result[0]->fields->fCodRosacruz;
		$retorno['fNumLoteAtualRosacr'] 				= $return2->result[0]->fields->fNumLoteAtualRosacr;

        if ($return2->result[0]->fields->fSeqCadast == $return2->result[0]->fields->fSeqCadastPrincipalRosacruz) {
    		$retorno['fDatAdmissRosacr'] 						= $return2->result[0]->fields->fDatAdmissRosacr;
        } else {
            $retorno['fDatAdmissRosacr']                        = $return2->result[0]->fields->fDatAdmissCompanRosacr;
        }

		$retorno['fIdeTipoSituacMembroRosacr'] 	= $return2->result[0]->fields->fIdeTipoSituacMembroRosacr;
		$retorno['fields'] 											= $return->result[0]->fields;

    if($ocultar_json==0) {
			// Imprime o retorno
			echo json_encode($retorno);
    }
}
?>
