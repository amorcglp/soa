<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $funcao						= isset($_REQUEST['funcao']) ? json_decode($_REQUEST['funcao']) : '';
    $objeto_dashboard 			= isset($_REQUEST['objeto_dashboard']) ? json_decode($_REQUEST['objeto_dashboard']) : '';
    $salvar 					= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/funcaoObjetoDashboardClass.php');
    $fod = new FuncaoObjetoDashboard();
    $fod->setFkIdFuncao($funcao);
    $fod->setFkIdObjetoDashboard($objeto_dashboard);

    if($salvar==0)
    {
            $retorno = $fod->remove();
    }
    if($salvar==1)
    {
            $retorno = $fod->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>