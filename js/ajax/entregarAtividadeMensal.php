<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $mesAtual           = isset($_REQUEST['mesAtual']) ? $_REQUEST['mesAtual']  : '';
    $anoAtual           = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual']  : '';
    $idOrganismoAfiliado= isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']  : '';
    $quemEntregou 	    = isset($_REQUEST['quemEntregou']) ? $_REQUEST['quemEntregou']  : '';

    $arr=array();

    include_once('../../lib/functions.php');
    include_once('../../model/atividadeEstatutoMensalClass.php');
    $a = new atividadeEstatutoMensal();
    $resultado = $a->listaAtividadeMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
    if(!$resultado)
    {
            $a = new atividadeEstatutoMensal();
            $a->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $a->setMes($mesAtual);
            $a->setAno($anoAtual);
            $a->setQuemEntregou($quemEntregou);
            $a->setEntregue(1);
            $a->cadastraAtividadeMensal();


        $a = new atividadeEstatutoMensal();
        $resultado = $a->listaAtividadeMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
    }

    $arr['status'] = 0;
    $arr['dataEntrega'] = "";

    if ($resultado) {

        $arr['status'] = 1;
        foreach ($resultado as $v)
        {
            $id = $v['idAtividadeEstatutoMensal'];
            if(trim($v['numeroAssinatura'])!="")
            {
                $numeroAssinatura = $v['numeroAssinatura'];
                $temNumeroAssinatura=true;
            }else{
                $numeroAssinatura = aleatorioAssinatura();
                $a->setIdAtividadeEstatutoMensal($id);
                $a->atualizaNumeroAssinatura($mesAtual, $anoAtual,$idOrganismoAfiliado,$numeroAssinatura);
            }

            $dataEntrega = substr($v['dataEntrega'],0,10);
            $arr['dataEntrega'] = substr($dataEntrega,8,2)."/".substr($dataEntrega,5,2)."/".substr($dataEntrega,0,4);
            $arr['quemEntregou'] = $v['nomeUsuario'];
            $arr['numeroAssinatura'] = $numeroAssinatura;
        }
    }

    echo json_encode($arr);
}	
?>