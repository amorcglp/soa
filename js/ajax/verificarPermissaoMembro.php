<?php


/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            require_once './model/wsClass.php';
    } 

    include_once('../../model/organismoClass.php');

    $o = new organismo();

    $seqCadast 	= isset($_REQUEST['seq_cadast'])?$_REQUEST['seq_cadast']:0;
    $siglaPais 	= isset($_REQUEST['siglaPais'])?$_REQUEST['siglaPais']:'BR';

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaRelacaoOficiais';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SeqCadast' => $seqCadast,
                                    'SeqFuncao' => 0,
                                    'NomParcialFuncao' => '',
                                    'NomLocaliOficial' => '',
                                    'SigRegiaoBrasilOficial' => '',
                                    'SigPaisOficial' => $siglaPais,
                                    'SigOrganismoAfiliado' => '',
                                    'SigAgrupamentoRegiao' => '',
                                    'IdeListarAtuantes' => 'S',
                                    'IdeListarNaoAtuantes' => 'N', 
                                    'IdeTipoMembro' => '1'
                            );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    $arrOrganismosRosacruzes=array();
    $resultadoOA = $o->listaOrganismo(null, null, null, null, null, null, null, 1);
    if($resultadoOA)
    {
        foreach($resultadoOA as $vetor)
        {
            $arrOrganismosRosacruzes[] = $vetor['siglaOrganismoAfiliado'];
        }    
    }    

    $resposta = array();
    $resposta['total']=0;
    $resposta['siglaOA']="PR113";
    $descFuncao=array();
    foreach($return->result[0]->fields->fArrayOficiais as $vetor)
    {
            if($vetor->fields->fDatSaida==0)
            {
                    $descFuncao[]=$vetor->fields->fDesFuncao;
                    if(in_array($vetor->fields->fSigOrgafi,$arrOrganismosRosacruzes))
                    {        
                        $resposta['siglaOA']=$vetor->fields->fSigOrgafi;
                    }
            }
    }
    $resposta['total']=count($descFuncao);

    echo json_encode($resposta);
}
?>