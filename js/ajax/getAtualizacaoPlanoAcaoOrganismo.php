<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    include_once('../../model/planoAcaoOrganismoAtualizacaoClass.php');

    $paoa = new planoAcaoOrganismoAtualizacao();

    $resultado = $paoa->listaAtualizacao(null,$_REQUEST['id']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
            $retorno['mensagem'] = $vetor['mensagem'];
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>