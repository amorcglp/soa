<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $selected = '';

    include_once("../../model/imovelClass.php");
    $imodel = new imovel();

    $resultado 			= $imodel->listaTipoEscrituraImovel();

    $id                 = $imodel->selecionaUltimoIdTipoEscritura();

    $retorno=array();
    $retorno['escrituras'] = array();

    if ($resultado) {
        $retorno['qtdTipoEscritura'] = 0;

        foreach ($resultado as $vetor) {

            $retorno['temTipoEscritura'] = 1;
            $retorno['qtdTipoEscritura'] ++;
            if ($vetor['idTipoEscrituraImovel'] == $id) {
                    $retorno['escrituras']['options'][] = '<option value="' . $vetor['idTipoEscrituraImovel'] . '" " selected >' . $vetor["tipoEscrituraImovel"] . '</option>';
            } else {
                    $retorno['escrituras']['options'][] = '<option value="' . $vetor['idTipoEscrituraImovel'] . '" ">' . $vetor["tipoEscrituraImovel"] . '</option>';
            }
        }

    } else {
        $retorno['temTipoEscritura'] = 0;
    }

    echo json_encode($retorno);
}