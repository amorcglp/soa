<?php
require_once('../../lib/functions.php');

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idPublicacao           = isset($_POST['idPublicacao']) ? addslashes($_POST['idPublicacao']) : '';
    $seqCadast              = isset($_POST['seqCadast']) ? addslashes($_POST['seqCadast']) : '';
	$avatarUsuario		    = isset($_POST['avatarUsuario']) ? addslashes($_POST['avatarUsuario']) : '';

    include_once("../../model/muralClass.php");
    $mural = new Mural();

    $retorno=array();
    $retorno['vetor'] = array();
    $retorno['qtdVetor'] = 0;

    $i=0;

    $retorno['socialFooter'] = "";

	$comentarios      = $mural->listaComentarioPelaPublicacao($idPublicacao, false);
	if($comentarios){
		foreach($comentarios as $comentarioVetor){

            $retorno['vetor'][$i]['id'] = $comentarioVetor['idMuralComentario'];
			$idComentario 					= $comentarioVetor['idMuralComentario'];
			$mensagemMuralComentario 		= $comentarioVetor['mensagemMuralComentario'];
			$dtCadastroComentario 			= substr($comentarioVetor['dtCadastroMuralComentario'],8,2)."/".substr($comentarioVetor['dtCadastroMuralComentario'],5,2)."/".substr($comentarioVetor['dtCadastroMuralComentario'],0,4) . " às " . substr($comentarioVetor['dtCadastroMuralComentario'],10,6);
			$dtCadastroComentarioTimeAgo 	= timeAgo($comentarioVetor['dtCadastroMuralComentario']);
			$dtAtualizacaoComentario 		= substr($comentarioVetor['dtAtualizacaoMuralComentario'],8,2)."/".substr($comentarioVetor['dtAtualizacaoMuralComentario'],5,2)."/".substr($comentarioVetor['dtAtualizacaoMuralComentario'],0,4) . " às " . substr($comentarioVetor['dtAtualizacaoMuralComentario'],10,6);
			$dtAtualizacaoComentarioTimeAgo = timeAgo($comentarioVetor['dtAtualizacaoMuralComentario']);
			$nomeUsuarioComentario 			= retornaNomeFormatado($comentarioVetor['nomeUsuario'],6);
			$avatarUsuarioComentario 		= $comentarioVetor['avatarUsuario'];

			$retorno['socialFooter'] .= <<<EOT
	        	<div id="social-comment$idPublicacao$idComentario" class="social-comment">
	                <a class="pull-left">
	                	<img src="$avatarUsuarioComentario" style="width: 32px;margin-right: 10px;">
	                </a>
	                <div class="media-body">
	                    <a>$nomeUsuarioComentario</a>
	                    $mensagemMuralComentario
	                    <br/>
                        <!--
	                    <a class="small"><i class="fa fa-thumbs-up"></i> 26 Like this! X FALTA X</a> - 
                        -->
	                    <small class="text-muted"> à $dtCadastroComentarioTimeAgo - $dtCadastroComentario</small>
	                </div>
	            </div>
EOT;
		}
	}
	$retorno['socialFooter'] .= <<<EOT
    <div class="social-comment">
        <a class="pull-left">
        	<img src="$avatarUsuario">
        </a>
        <div class="media-body">
            <textarea style="width: 700px; height: 65px" id="comentario-publicacao-$idPublicacao" onkeyup="diminuirCaracteresMensagemMural('comentario-publicacao-$idPublicacao','caracteres-comentario-publicacao-$idPublicacao',250)" class="form-control" maxlength="250" placeholder="Escreva um comentário..."></textarea>
                    <div id="caracteres-comentario-publicacao-$idPublicacao">250 caracteres restantes!</div>
            <button onClick="cadastraComentarioPublicacaoMural($idPublicacao,$seqCadast,'$avatarUsuario');" class="btn btn-white btn-xs"><i class="fa fa-comments"></i> Comentar</button>
        </div>
    </div>
EOT;
    $retorno['qtdVetor'] = $i;

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}
