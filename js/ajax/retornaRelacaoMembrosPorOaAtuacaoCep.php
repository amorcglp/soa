<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';
            }else{
                    require_once './model/wsClass.php';
            }
    } 

    if(!isset($siglaOA))
    {
            $siglaOA 	= isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:0;
    }

    if(!isset($saldoRemessaRosacruz))
    {
            $saldoRemessaRosacruz 	= isset($_REQUEST['saldoRemessaRosacruz'])?$_REQUEST['saldoRemessaRosacruz']:'';
    }

    if(!isset($situacaoRemessaRosacruz))
    {
            $situacaoRemessaRosacruz 	= isset($_REQUEST['situacaoRemessaRosacruz'])?$_REQUEST['situacaoRemessaRosacruz']:0;
    }

    if(!isset($situacaoCadastralRosacruz))
    {
            $situacaoCadastralRosacruz 	= isset($_REQUEST['situacaoCadastralRosacruz'])?$_REQUEST['situacaoCadastralRosacruz']:0;
    }

    $ocultar_json 	= isset($ocultar_json)?$ocultar_json:0;

    $server = isset($server) ? $server : null;

    // Instancia a classe
    $ws = new Ws($server);

    // Nome do Método que deseja chamar
    $method = 'RetornaRelacaoMembrosPorOaAtuacaoCep';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SigOrgafi' => $siglaOA,
                                    'SaldoRemessaRosacruz' => $saldoRemessaRosacruz,
                                    'SituacaoRemessaRosacruz' => $situacaoRemessaRosacruz,
                                    'SituacaoCadastralRosacruz' => $situacaoCadastralRosacruz
    );
    //echo "<pre>";print_r($params);
    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    // Imprime o retorno
    if($ocultar_json==0)
    {
            echo json_encode($return);
    }
}
?>