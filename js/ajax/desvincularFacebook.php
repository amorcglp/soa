<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $seqCadast	 	= isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;

    include_once('../../model/usuarioClass.php');
    $u = new Usuario();
    if($u->desvincularFacebook($seqCadast))
    {
            $retorno['status'] = '1';	
    }else{
            $retorno['status'] = '0';
    }	

    echo json_encode($retorno);
}	
?>