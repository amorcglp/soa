<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idTicket 	            = isset($_POST['idTicket']) ? addslashes($_POST['idTicket']) : '';

    include_once("../../model/ticketClass.php");
    $tm = new Ticket();

    $resultado = $tm->listaAnexo($idTicket);

    $retorno['success'] = true;
    $retorno['sucesso'] = 'Anexo carregado com sucesso!';
    $retorno['arquivo'] = array();
    if ($resultado) {
            $retorno['temAnexo'] = 1;
            foreach ($resultado as $arquivos) {
                    switch($arquivos['ext']) {
                case 'doc': $icone = '<i class="fa fa-file-word-o"></i>';break;
                case 'docx': $icone = '<i class="fa fa-file-word-o"></i>';break;
                case 'odt': $icone = '<i class="fa fa-file-word-o"></i>';break;
                case 'zip': $icone = '<i class="fa fa-file-zip-o"></i>';break;
                case 'rar': $icone = '<i class="fa fa-file-zip-o"></i>';break;
                            case 'pdf': $icone = '<i class="fa fa-file-pdf-o"></i>';break;
                            case 'jpg': $icone = '<i class="fa fa-file-picture-o"></i>';break;
                            case 'jpeg': $icone = '<i class="fa fa-file-picture-o"></i>';break;
                            case 'png': $icone = '<i class="fa fa-file-picture-o"></i>';break;
                            default: $icone = '<i class="fa fa-file-o"></i>';break;
                    }
                    $retorno['arquivo'][]['id'] = $arquivos['idTicketAnexo'];
                    $retorno['arquivo'][]['icone'] = $icone;
                    $retorno['arquivo'][]['nome_original'] = $arquivos['nome_original'];
                    $retorno['arquivo'][]['full_path'] = $arquivos['full_path'];
                    $retorno['arquivo'][]['nome_arquivo'] = $arquivos['nome_arquivo'];
                    $retorno['arquivo'][]['data_envio'] = $arquivos['data_cadastro'];
                    $path=$arquivos['full_path'];

            $nameFile = str_split($arquivos['nome_original'],30);
            $nameAnexo = strlen($nameFile[0]) == 30 ? $nameFile[0]."..." : $nameFile[0];

            $retorno['arquivo'][]['anexo'] = "<a target='_BLANK' title='".$arquivos['nome_original']."' href='" . $arquivos['full_path'] . "'>". $icone . " " . $nameAnexo . "</a><a style='margin-right: 25px' onclick='excluiTicketAnexo(" . $arquivos['idTicketAnexo'] . "," . $idTicket . ");'> <i class='fa fa-trash'></i></a><br>";
        }
    } else {
            $retorno['temAnexo'] = 0;//nenhuma arquivo
            $retorno['sucess'] = false;
            $retorno['erro'] = 'Erro ao buscar os anexos';
    }
    echo json_encode($retorno);
}
?>