<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $p 	= isset($_REQUEST['pesquisa']) ? $_REQUEST['pesquisa']  : '';

    $arr=array();

    include_once('../../model/organismoClass.php');
    $o = new organismo();
    $retorno = $o->listaOrganismo($p);

    if ($retorno) {
            foreach($retorno as $vetor)
            {
                    $arr['idOrganismoAfiliado'][]	=$vetor['idOrganismoAfiliado'];
                    $arr['nomeOrganismoAfiliado'][]	=$vetor['nomeOrganismoAfiliado'];
                    $arr['siglaOrganismoAfiliado'][]=$vetor['siglaOrganismoAfiliado'];
            }
    }

    echo json_encode($arr);
}
	
?>