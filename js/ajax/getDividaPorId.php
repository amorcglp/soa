<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/dividaClass.php');

    $d = new Divida();

    $resultado = $d->buscarIdDivida($_REQUEST['id']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $dataInicio								= date_create($vetor['inicioDivida']);
                    $retorno['inicioDivida']				= date_format($dataInicio, 'd/m/Y');
                    $dataFim								= date_create($vetor['fimDivida']);
                    $retorno['fimDivida']					= date_format($dataFim, 'd/m/Y');
                    $retorno['descricaoDivida'] 			= $vetor['descricaoDivida'];
                    $retorno['valorDivida']					= $vetor['valorDivida'];
                    $retorno['observacaoDivida'] 			= $vetor['observacaoDivida'];
                    $retorno['categoriaDivida'] 			= $vetor['categoriaDivida'];
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>