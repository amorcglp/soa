<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    include_once('../../model/despesaClass.php');

    $d = new Despesa();

    $retorno = array();
    $total = $d->retornaSaida($_REQUEST['mesAtual'],$_REQUEST['anoAtual'],$_REQUEST['idOrganismoAfiliado']);
    $retorno['totalGeral'] = number_format($total, 2, ',', '.');

    echo json_encode($retorno);
}
?>