<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idOrganismo 		= isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';
    $seqCadast 	= isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';

    include_once("../../model/atividadeIniciaticaOficialClass.php");
    $aiomodel = new atividadeIniciaticaOficial();

    $resultado 	= $aiomodel->retornaOficiaisOrganismo($idOrganismo,$seqCadast);

    //echo $resultado;

    $retorno['oficial']=array();

    $total=0;

    if ($resultado) {
            foreach($resultado as $vetor){
                    $total++;
            }
            $retorno['oficial'][] = $total;
    } else {
            $retorno['oficial'][] = 'error';
    }

    echo json_encode($retorno);
}