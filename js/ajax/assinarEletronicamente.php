<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once('../../lib/functions.php');

    date_default_timezone_set('America/Sao_Paulo');

    if(isset($_REQUEST['checkboxAta'])&&$_REQUEST['checkboxAta']!="")
    {
        $strAta = substr($_REQUEST['checkboxAta'],0,strlen($_REQUEST['checkboxAta'])-1);
        $arrCheckboxAta = explode(",",$strAta);
    }else{
        $arrCheckboxAta = array();
    }
    //echo "<pre>";print_r($arrCheckboxAta);

    if(isset($_REQUEST['checkboxFinanceiroMensal'])&&$_REQUEST['checkboxFinanceiroMensal']!="")
    {
        $strFinanceiro = substr($_REQUEST['checkboxFinanceiroMensal'],0,strlen($_REQUEST['checkboxFinanceiroMensal'])-1);
        $arrCheckboxFinanceiro = explode(",",$strFinanceiro);
    }else{
        $arrCheckboxFinanceiro = array();
    }

    if(isset($_REQUEST['checkboxAtividadeMensal'])&&$_REQUEST['checkboxAtividadeMensal']!="")
    {
        $strAtividade = substr($_REQUEST['checkboxAtividadeMensal'],0,strlen($_REQUEST['checkboxAtividadeMensal'])-1);
        $arrCheckboxAtividade = explode(",",$strAtividade);
    }else{
        $arrCheckboxAtividade = array();
    }

    if(isset($_REQUEST['checkboxOggMensal'])&&$_REQUEST['checkboxOggMensal']!="")
    {
        $strOgg = substr($_REQUEST['checkboxOggMensal'],0,strlen($_REQUEST['checkboxOggMensal'])-1);
        $arrCheckboxOgg = explode(",",$strOgg);
    }else{
        $arrCheckboxOgg = array();
    }

    if(isset($_REQUEST['checkboxColumbaTrimestral'])&&$_REQUEST['checkboxColumbaTrimestral']!="")
    {
        $strColumba = substr($_REQUEST['checkboxColumbaTrimestral'],0,strlen($_REQUEST['checkboxColumbaTrimestral'])-1);
        $arrCheckboxColumba = explode(",",$strColumba);
    }else{
        $arrCheckboxColumba = array();
    }

    if(isset($_REQUEST['checkboxRelatorioClasseArtesaos'])&&$_REQUEST['checkboxRelatorioClasseArtesaos']!="")
    {
        $strRelatorioClasseArtesaos = substr($_REQUEST['checkboxRelatorioClasseArtesaos'],0,strlen($_REQUEST['checkboxRelatorioClasseArtesaos'])-1);
        $arrCheckboxRelatorioClasseArtesaos = explode(",",$strRelatorioClasseArtesaos);
    }else{
        $arrCheckboxRelatorioClasseArtesaos = array();
    }

    if(isset($_REQUEST['checkboxRelatorioGrandeConselheiro'])&&$_REQUEST['checkboxRelatorioGrandeConselheiro']!="")
    {
        $strRelatorioGrandeConselheiro = substr($_REQUEST['checkboxRelatorioGrandeConselheiro'],0,strlen($_REQUEST['checkboxRelatorioGrandeConselheiro'])-1);
        $arrCheckboxRelatorioGrandeConselheiro = explode(",",$strRelatorioGrandeConselheiro);
    }else{
        $arrCheckboxRelatorioGrandeConselheiro = array();
    }

    $seqCadast 	                            = isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;
    $ip     	                            = isset($_REQUEST['ip'])?$_REQUEST['ip']:null;
    $classificacao                          = isset($_REQUEST['classificacao'])?$_REQUEST['classificacao']:null;
    $idOrganismoAfiliado     	            = isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:null;
    $regiao                  	            = isset($_REQUEST['regiao'])?$_REQUEST['regiao']:null;
    $arrFuncoesImportantesParaAssinatura    = isset($_REQUEST['funcoesImportantesParaAssinatura'])?explode(",",$_REQUEST['funcoesImportantesParaAssinatura']):array();

    //echo "<pre>";print_r($arrCheckboxAta);

    include_once('../../model/ataReuniaoMensalClass.php');
    $at = new ataReuniaoMensal();

    //Cadastrar assinaturas de Atas
    if(count($arrCheckboxAta)>0)
    {
        foreach($arrCheckboxAta as $ata => $idAta)
        {
            //echo "v:".$ata." =>".$idAta;
            if(count($arrFuncoesImportantesParaAssinatura)>0)
            {
                foreach ($arrFuncoesImportantesParaAssinatura as $fun=> $f)
                {
                    //$at = new ataReuniaoMensal();
                    $codigoAssinaturaIndividual = aleatorioAssinatura();

                    $resultadoAta = $at->assinaturaEletronicaAtaReuniaoMensal($seqCadast,$ip,$idOrganismoAfiliado,$f,$idAta,$codigoAssinaturaIndividual);
                    //echo "aqui teste do braz resultado=>".$resultadoAta;
                    //Verificar se existe alguma pessoa que falta assinar nesse documento, se não existe então dar ela como completa
                    $at2 = new ataReuniaoMensal();
                    //echo "idAta".$idAta;
                    $mestre = $at2->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($idAta,365);
                    $secretario = $at2->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($idAta,367);
                    //$tjd = $at2->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($idAta,375);

                    //echo "mestre:".$mestre;
                    //echo "secretario:".$secretario;

                    if(($mestre&&$secretario&&($classificacao==3||$classificacao==1))//Para Loja e Capítulo
                        ||($mestre&&$secretario&&($classificacao==2)))//Para Pronaos
                    {
                        //echo "Vai entregar completamente";
                        $respostaVerificacaoAta = $at2->entregarCompletamente($idAta,$seqCadast);
                        if($respostaVerificacaoAta)
                        {
                            //echo "entregou".$idAta;
                        }
                    }
                }
            }
        }


    }

    include_once('../../model/financeiroMensalClass.php');
    $fm = new financeiroMensal();

    //Cadastrar assinaturas de Atas
    if(count($arrCheckboxFinanceiro)>0)
    {
        foreach($arrCheckboxFinanceiro as $fin => $idFinanceiro)
        {
            //echo $idFinanceiro;
            //$arr = array();
            $arr = explode("@@",$idFinanceiro);


            if(isset($arr[0]))
            {
                $mes = $arr[0];
            }else{
                $mes = 0;
            }


            if(isset($arr[1]))
            {
                $ano = $arr[1];
            }else{
                $ano = 0;
            }

            //echo "mes=>".$mes;
            //echo "ano=>".$ano;
            //$idOrganismoAfiliado = $arr[2];
            //echo "v:".$fin." =>".$idFinanceiro;
            if(count($arrFuncoesImportantesParaAssinatura)>0)
            {
                foreach ($arrFuncoesImportantesParaAssinatura as $fun=> $f)
                {
                    $codigoAssinaturaIndividual = aleatorioAssinatura();

                    $fm = new financeiroMensal();
                    $resultadoFinanceiro = $fm->assinaturaEletronicaFinanceiroMensal($seqCadast,$ip,$idOrganismoAfiliado,$f,$mes,$ano,$codigoAssinaturaIndividual);

                    //Verificar se existe alguma pessoa que falta assinar nesse documento, se não existe então dar ela como completa
                    $fm2 = new financeiroMensal();
                    $mestre = $fm2->buscarAssinaturasEmFinanceiroMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,365);
                    $secretario = $fm2->buscarAssinaturasEmFinanceiroMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,367);
                    $tjd = $fm2->buscarAssinaturasEmFinanceiroMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,375);
                    $vdc = $fm2->buscarAssinaturasEmFinanceiroMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,533);

                    /*
                    $pjd = $fm2->buscarAssinaturasEmFinanceiroMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,371);
                    $sjd = $fm2->buscarAssinaturasEmFinanceiroMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,373);
                    $ma = $fm2->buscarAssinaturasEmFinanceiroMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,377);*/

                    if(($mestre&&$secretario&&$tjd&&$vdc&&($classificacao==3||$classificacao==1))//Para Loja e Capítulo
                        ||($mestre&&$secretario&&$vdc&&($classificacao==2)))//Para Pronaos
                    {
                        $respostaVerificacao = $fm2->entregarCompletamente($mes,$ano,$idOrganismoAfiliado,$seqCadast);
                    }
                }
            }
        }
    }

    include_once('../../model/atividadeEstatutoMensalClass.php');
    $aem = new atividadeEstatutoMensal();

    //Cadastrar assinaturas de Atas
    if(count($arrCheckboxAtividade)>0)
    {
        foreach($arrCheckboxAtividade as $ati => $idAtividade)
        {
            //echo $idFinanceiro;
            //$arr = array();
            $arr = explode("@@",$idAtividade);


            if(isset($arr[0]))
            {
                $mes = $arr[0];
            }else{
                $mes = 0;
            }


            if(isset($arr[1]))
            {
                $ano = $arr[1];
            }else{
                $ano = 0;
            }

            //echo "mes=>".$mes;
            //echo "ano=>".$ano;
            //$idOrganismoAfiliado = $arr[2];
            //echo "v:".$fin." =>".$idFinanceiro;
            if(count($arrFuncoesImportantesParaAssinatura)>0)
            {
                foreach ($arrFuncoesImportantesParaAssinatura as $fun=> $f)
                {
                    $codigoAssinaturaIndividual = aleatorioAssinatura();

                    $aem = new atividadeEstatutoMensal();
                    $resultadoAtividade = $aem->assinaturaEletronicaAtividadeMensal($seqCadast,$ip,$idOrganismoAfiliado,$f,$mes,$ano,$codigoAssinaturaIndividual);

                    //Verificar se existe alguma pessoa que falta assinar nesse documento, se não existe então dar ela como completa
                    $aem2 = new atividadeEstatutoMensal();
                    $mestre = $aem2->buscarAssinaturasEmAtividadeMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,365);
                    $secretario = $aem2->buscarAssinaturasEmAtividadeMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,367);
                    //$tjd = $aem2->buscarAssinaturasEmAtividadeMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,375);

                    //$pjd = $aem2->buscarAssinaturasEmAtividadeMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,371);
                    //$sjd = $aem2->buscarAssinaturasEmAtividadeMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,373);
                    //$ma = $aem2->buscarAssinaturasEmAtividadeMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,377);

                    if(($mestre&&$secretario&&($classificacao==3||$classificacao==1))//Para Loja e Capítulo
                        ||($mestre&&$secretario&&($classificacao==2)))//Para Pronaos
                    {
                        $respostaVerificacao = $aem2->entregarCompletamente($mes,$ano,$idOrganismoAfiliado,$seqCadast);
                    }
                }
            }
        }
    }

    include_once('../../model/oggMensalClass.php');
    $om = new oggMensal();

    //Cadastrar assinaturas de Atas
    if(count($arrCheckboxOgg)>0)
    {
        foreach($arrCheckboxOgg as $ogg => $idOgg)
        {
            //echo $idFinanceiro;
            //$arr = array();
            $arr = explode("@@",$idOgg);


            if(isset($arr[0]))
            {
                $mes = $arr[0];
            }else{
                $mes = 0;
            }


            if(isset($arr[1]))
            {
                $ano = $arr[1];
            }else{
                $ano = 0;
            }

            //echo "mes=>".$mes;
            //echo "ano=>".$ano;
            //$idOrganismoAfiliado = $arr[2];
            //echo "v:".$fin." =>".$idFinanceiro;
            if(count($arrFuncoesImportantesParaAssinatura)>0)
            {
                foreach ($arrFuncoesImportantesParaAssinatura as $fun=> $f)
                {
                    $codigoAssinaturaIndividual = aleatorioAssinatura();

                    $om = new oggMensal();
                    $resultadoOgg = $om->assinaturaEletronicaOggMensal($seqCadast,$ip,$idOrganismoAfiliado,$f,$mes,$ano,$codigoAssinaturaIndividual);

                    //Verificar se existe alguma pessoa que falta assinar nesse documento, se não existe então dar ela como completa
                    $om2 = new oggMensal();
                    $mestre = $om2->buscarAssinaturasEmOggMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,365);
                    $secretario = $om2->buscarAssinaturasEmOggMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,367);
                    //$tjd = $om2->buscarAssinaturasEmOggMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,375);

                    //$pjd = $om2->buscarAssinaturasEmOggMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,371);
                    //$sjd = $om2->buscarAssinaturasEmOggMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,373);
                    //$ma = $om2->buscarAssinaturasEmOggMensalPorDocumento($mes,$ano,$idOrganismoAfiliado,377);

                    if(($mestre&&$secretario&&($classificacao==3||$classificacao==1))//Para Loja e Capítulo
                        ||($mestre&&$secretario&&($classificacao==2)))//Para Pronaos
                    {
                        $respostaVerificacao = $om2->entregarCompletamente($mes,$ano,$idOrganismoAfiliado,$seqCadast);
                    }
                }
            }
        }
    }

    include_once('../../model/columbaTrimestralClass.php');
    $ct = new columbaTrimestral();

    //Cadastrar assinaturas de Atas
    if(count($arrCheckboxColumba)>0)
    {
        foreach($arrCheckboxColumba as $col => $idColumba)
        {
            //echo $idFinanceiro;
            //$arr = array();
            $arr = explode("@@",$idColumba);


            if(isset($arr[0]))
            {
                $trimestre = $arr[0];
            }else{
                $trimestre = 0;
            }


            if(isset($arr[1]))
            {
                $ano = $arr[1];
            }else{
                $ano = 0;
            }

            //echo "mes=>".$mes;
            //echo "ano=>".$ano;
            //$idOrganismoAfiliado = $arr[2];
            //echo "v:".$fin." =>".$idFinanceiro;
            if(count($arrFuncoesImportantesParaAssinatura)>0)
            {
                foreach ($arrFuncoesImportantesParaAssinatura as $fun=> $f)
                {
                    $codigoAssinaturaIndividual = aleatorioAssinatura();

                    $ct = new columbaTrimestral();
                    $resultadoColumba = $ct->assinaturaEletronicaColumbaTrimestral($seqCadast,$ip,$idOrganismoAfiliado,$f,$trimestre,$ano,$codigoAssinaturaIndividual);

                    //Verificar se existe alguma pessoa que falta assinar nesse documento, se não existe então dar ela como completa
                    $ct2 = new columbaTrimestral();
                    $mestre = $ct2->buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestre,$ano,$idOrganismoAfiliado,365);
                    $secretario = $ct2->buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestre,$ano,$idOrganismoAfiliado,367);
                    //$tjd = $ct2->buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestre,$ano,$idOrganismoAfiliado,375);

                    //$pjd = $ct2->buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestre,$ano,$idOrganismoAfiliado,371);
                    //$sjd = $ct2->buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestre,$ano,$idOrganismoAfiliado,373);
                    //$ma = $ct2->buscarAssinaturasEmColumbaTrimestralPorDocumento($trimestre,$ano,$idOrganismoAfiliado,377);

                    if(($mestre&&$secretario&&($classificacao==3||$classificacao==1))//Para Loja e Capítulo
                        ||($mestre&&$secretario&&($classificacao==2)))//Para Pronaos
                    {
                        $respostaVerificacao = $ct2->entregarCompletamente($trimestre,$ano,$idOrganismoAfiliado,$seqCadast);
                    }
                }
            }
        }
    }

    include_once('../../model/relatorioClasseArtesaosClass.php');
    $rca = new relatorioClasseArtesaos();

    //Cadastrar assinaturas de Atas
    if(count($arrCheckboxRelatorioClasseArtesaos)>0)
    {
        foreach($arrCheckboxRelatorioClasseArtesaos as $classeArtesaos => $idClasseArtesaos)
        {
            //echo "v:".$ata." =>".$idAta;
            if(count($arrFuncoesImportantesParaAssinatura)>0)
            {
                foreach ($arrFuncoesImportantesParaAssinatura as $fun=> $f)
                {
                    //$at = new ataReuniaoMensal();
                    $codigoAssinaturaIndividual = aleatorioAssinatura();

                    $resultadoRelatorioClasseArtesaos = $rca->assinaturaEletronicaRelatorioClasseArtesaos($seqCadast,$ip,$idOrganismoAfiliado,$f,$idClasseArtesaos,$codigoAssinaturaIndividual);
                    //echo "aqui teste do braz resultado=>".$resultadoAta;
                    //Verificar se existe alguma pessoa que falta assinar nesse documento, se não existe então dar ela como completa
                    $rca2 = new relatorioClasseArtesaos();
                    //echo "idAta".$idAta;
                    $mestre = $rca2->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($idClasseArtesaos,365);
                    $secretario = $rca2->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($idClasseArtesaos,367);
                    //$tjd = $rca2->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($idClasseArtesaos,375);

                    //echo "mestre:".$mestre;
                    //echo "secretario:".$secretario;

                    if(($mestre&&$secretario&&($classificacao==3||$classificacao==1))//Para Loja e Capítulo
                        ||($mestre&&$secretario&&($classificacao==2)))//Para Pronaos
                    {
                        //echo "Vai entregar completamente";
                        $respostaVerificacaoClasseArtesao = $rca2->entregarCompletamente($idClasseArtesaos,$seqCadast);
                        if($respostaVerificacaoClasseArtesao)
                        {
                            //echo "entregou".$idAta;
                        }
                    }
                }
            }
        }


    }

    include_once('../../model/relatorioGrandeConselheiroClass.php');
    $rgc = new relatorioGrandeConselheiro();



    //Cadastrar assinaturas de Atas
    if(count($arrCheckboxRelatorioGrandeConselheiro)>0)
    {
        foreach($arrCheckboxRelatorioGrandeConselheiro as $grandeConselheiro => $idGrandeConselheiro)
        {
            //echo "v:".$ata." =>".$idAta;
            if(count($arrFuncoesImportantesParaAssinatura)>0)
            {
                foreach ($arrFuncoesImportantesParaAssinatura as $fun=> $f)
                {
                    //$at = new ataReuniaoMensal();
                    $codigoAssinaturaIndividual = aleatorioAssinatura();

                    $resultadoRelatorioGrandeConselheiro = $rgc->assinaturaEletronicaRelatorioGrandeConselheiro($seqCadast,$ip,$regiao,$f,$idGrandeConselheiro,$codigoAssinaturaIndividual);
                    //echo "aqui teste do braz resultado=>".$resultadoAta;
                    //Verificar se existe alguma pessoa que falta assinar nesse documento, se não existe então dar ela como completa
                    $rgc2 = new relatorioGrandeConselheiro();
                    //echo "idAta".$idAta;
                    $gc = $rgc2->buscarAssinaturasEmRelatorioGrandeConselheiroPorDocumento($idGrandeConselheiro,329);

                    if($gc)//Para Pronaos
                    {
                        //echo "Vai entregar completamente";
                        $respostaVerificacaoGrandeConselheiro = $rgc2->entregarCompletamente($idGrandeConselheiro,$seqCadast);
                        if($respostaVerificacaoGrandeConselheiro)
                        {
                            //echo "entregou".$idAta;
                        }
                    }
                }
            }
        }


    }

    $retorno = array();

    if ($resultadoAta==true
            ||$resultadoFinanceiro==true
            ||$resultadoAtividade==true
            ||$resultadoOgg==true
            ||$resultadoColumba==true
            ||$resultadoRelatorioClasseArtesaos==true
            ||$resultadoRelatorioGrandeConselheiro==true
    ) {
        $retorno['status'] = 1;
    } else {
        $retorno['status'] = 0;
    }


    echo json_encode($retorno);
}
?>