<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $seqCadastAtribuido 	= isset($_REQUEST['seqCadastAtribuido']) ? $_REQUEST['seqCadastAtribuido']  : '';
    $seqCadastAtribuidor 	= isset($_REQUEST['seqCadastAtribuidor']) ? $_REQUEST['seqCadastAtribuidor']  : '';
    $idTicket 	        = isset($_REQUEST['idTicket']) ? $_REQUEST['idTicket']  : '';

    $arr=array();

    include_once('../../model/ticketClass.php');
    $t = new Ticket();
    $retorno = $t->alteraAtribuicaoTicket($idTicket,$seqCadastAtribuido,$seqCadastAtribuidor);

    $arr['alterado'] = false;

    if ($retorno) {
        $arr['alterado'] = true;
    }

    echo json_encode($arr);
}	
?>