<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    include_once('../../model/dividaClass.php');

    $d = new Divida();

    $retorno = array();
    $total = $d->retornaDivida(null,null,$_REQUEST['idOrganismoAfiliado']);
    $retorno['totalGeral'] = number_format($total, 2, ',', '.');

    echo json_encode($retorno);
}
?>