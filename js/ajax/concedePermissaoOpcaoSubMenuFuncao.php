<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    
    $funcao						= isset($_REQUEST['funcao']) ? json_decode($_REQUEST['funcao']) : '';
    $subopcao			 		= isset($_REQUEST['subopcao']) ? json_decode($_REQUEST['subopcao']) : '';
    $salvar 					= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/funcaoOpcaoSubMenuClass.php');
    $fosm = new FuncaoOpcaoSubMenu();
    $fosm->setFkIdFuncao($funcao);
    $fosm->setFkIdOpcaoSubMenu($subopcao);

    if($salvar==0)
    {
            $retorno = $fosm->remove();
    }
    if($salvar==1)
    {
            $retorno = $fosm->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>