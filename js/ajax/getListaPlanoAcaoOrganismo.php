<?php
include("../../lib/functions.php");
//@usuarioOnline();

include_once("../../lib/webservice/retornaInformacoesMembro.php");
include_once("../../model/perfilUsuarioClass.php");
include_once("../../controller/planoAcaoOrganismoController.php");
include_once("../../model/planoAcaoOrganismoParticipanteClass.php");
include_once("../../model/organismoClass.php");

$parc = new planoAcaoOrganismoController();
?>
<?php 

$pesquisar = isset($_REQUEST['pesquisar'])?$_REQUEST['pesquisar']:null;

/*PAGINACAO*/

define('QTDE_REGISTROS', 5);   
define('RANGE_PAGINAS', 3); 

/* Recebe o número da página via parâmetro na URL */  
 $pagina_atual = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;   
   
 /* Calcula a linha inicial da consulta */  
 $linha_inicial = ($pagina_atual -1) * QTDE_REGISTROS; 
 
 /* Conta quantos registos existem na tabela */  
 $total_registros = $resultado = $parc->totalPlanoAcaoOrganismo($pesquisar);
 
 /* Idêntifica a primeira página */  
 $primeira_pagina = 1;   
   
 /* Cálcula qual será a última página */  
 $ultima_pagina  = ceil($total_registros / QTDE_REGISTROS);   
   
 /* Cálcula qual será a página anterior em relação a página atual em exibição */   
 $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual -1 : 0 ;   
   
 /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */   
 $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual +1 : 0 ;  
   
 /* Cálcula qual será a página inicial do nosso range */    
 $range_inicial  = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1 ;   
   
 /* Cálcula qual será a página final do nosso range */    
 $range_final   = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina ) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina ;   
   
 /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */   
 $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? 'mostrar' : 'esconder'; 
   
 /* Verifica se vai exibir o botão "Anterior" e "Último" */   
 $exibir_botao_final = ($range_final > $pagina_atual) ? 'mostrar' : 'esconder';  
?>

<table class="table table-hover">
<tbody>
<?php

$sessao = new criaSessao();
$organismo = new organismo();
$o = new organismo();

$funcoesUsuarioString = $sessao->getValue("funcoes");
$arrFuncoes = explode(",", $funcoesUsuarioString);

$arrNivelUsuario = explode(",", $sessao->getValue("niveis"));

$resultado = $organismo->listaOrganismo(null,$sessao->getValue("siglaOA"));

$naoCobrarDashboard=0;
$idOrganismoAfiliado=1;
if($resultado)
{
    foreach($resultado as $vetor)
    {
        $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];
        $idSiglaPais = $vetor['paisOrganismoAfiliado'];
        $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
        $naoCobrarDashboard = $vetor['naoCobrarDashboard'];
    }
}

            
            $resultado = $parc->listaPlanoAcaoOrganismo($pesquisar,$linha_inicial,QTDE_REGISTROS);

            include_once("../../model/planoAcaoOrganismoMetaClass.php");


            if ($resultado) {
                foreach ($resultado as $vetor) {

                        if(($vetor['fk_idOrganismoAfiliado']==$idOrganismoAfiliado)||($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2))
			{
                                $parm = new planoAcaoOrganismoMeta();

                                $resultadoMeta = $parm->listaMeta($vetor['idPlanoAcaoOrganismo']);

                                                                if($resultadoMeta)
                                                                {
                                                                        $totalMeta = count($resultadoMeta);
                                                                }else{
                                                                        $totalMeta = 0;
                                                                }

                                                                $resultadoMetaConcluida = $parm->listaMeta($vetor['idPlanoAcaoOrganismo'],1);

                                                                if($resultadoMetaConcluida)
                                                                {
                                                                        $totalMetaConcluida = count($resultadoMetaConcluida);
                                                                }else{
                                                                        $totalMetaConcluida = 0;
                                                                }

                                /*
                                                                 * Cálculo da percentagem concluída
                                                                 */
                                                                if($totalMetaConcluida>0)
                                                                {
                                                                        $percentual = round(($totalMetaConcluida*100)/$totalMeta);
                                                                }else{
                                                                        $percentual = 0;
                                                                }
                                    	?>
	                                    <tr>
	                                        <td class="project-status">
	                                            
	                                            <?php 
	                                            if($vetor['statusPlano']==1)
	                                            {
	                                            	echo "<div id=\"statusPlano".$vetor['idPlanoAcaoOrganismo']."\"><a href=\"#\" ";
                                                        if(is_array($arrNivelUsuario))
                                                        {    
                                                            if(in_array("2",$arrNivelUsuario)){
                                                                    echo "onclick=\"mudaStatusPlanoAcaoOrganismo('".$vetor['idPlanoAcaoOrganismo']."','1')\"";
                                                            }
                                                        }
	                                            	echo "><span class=\"label label-primary\">Ativo</span></a></div>";
	                                            }else{
	                                            	echo "<div id=\"statusPlano".$vetor['idPlanoAcaoOrganismo']."\"><a href=\"#\""; 
                                                        if(is_array($arrNivelUsuario))
                                                        {
                                                            if(in_array("2",$arrNivelUsuario)){
                                                                    echo "onclick=\"mudaStatusPlanoAcaoOrganismo('".$vetor['idPlanoAcaoOrganismo']."','0')\"";
                                                            }
                                                        }
	                                            	echo "><span class=\"label label-default\">Inativo</span></a></div>";
	                                            }
	                                            ?>
	                                            
	                                        </td>
	                                        <td class="project-title">
	                                            <a href="?corpo=buscaPlanoAcaoOrganismoDetalhe&idPlanoAcaoOrganismo=<?php echo $vetor['idPlanoAcaoOrganismo'];?>"><?php echo $vetor['tituloPlano'];?></a>
	                                            <br/>
	                                            <small>Palavras-chave: <?php echo $vetor['palavrasChave'];?></small>
	                                        </td>
	                                        <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2) ){
		                                       		$resultado2 = $o->listaOrganismo(null,null,null,null,null,$vetor['fk_idOrganismoAfiliado']);
													if($resultado2)
													{
														foreach($resultado2 as $vetor2)
														{
															switch ($vetor2['classificacaoOrganismoAfiliado']) {
																case 1:
																	$classificacao = "Loja";
																	break;
																case 2:
																	$classificacao = "Pronaos";
																	break;
																case 3:
																	$classificacao = "Capítulo";
																	break;
																case 4:
																	$classificacao = "Heptada";
																	break;
																case 5:
																	$classificacao = "Atrium";
																	break;
															}
															switch ($vetor2['tipoOrganismoAfiliado']) {
																case 1:
																	$tipo = "R+C";
																	break;
																case 2:
																	$tipo = "TOM";
																	break;
															}
								
															$nomeOrganismo2 = $vetor2["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor2["nomeOrganismoAfiliado"];
															
														}
													}
	                                        	?>
		                                        <td>
		                                        	<?php echo $nomeOrganismo2;?>
		                                        </td>
	                                        <?php }?>
	                                        <td class="project-completion">
	                                                <small>Progresso: <?php echo $percentual;?>%</small>
	                                                <div class="progress progress-mini">
	                                                    <div style="width: <?php echo $percentual;?>%;" class="progress-bar"></div>
	                                                </div>
	                                        </td>
	                                        <?php 
	                                        $parp = new planoAcaoOrganismoParticipante();
	                                        $resultado2 = $parp->listaParticipantes($vetor['idPlanoAcaoOrganismo']);
	                                        ?>
	                                        <td class="project-people">
	                                        	<?php 
	                                        	if($resultado2)
	                                        	{
	                                        		foreach($resultado2 as $vetor2)
	                                        		{
                                                                    if(isset($vetor2['seqCadast']))
                                                                    {    
	                                        			$pu = new perfilUsuario();
	                                        			$resultado3 = $pu->listaAvatarUsuario($vetor2['seqCadast']);
	                                        			if($resultado3)
	                                        			{
		                                        			foreach($resultado3 as $vetor3)
		                                        			{
		                                        				
			                                        			if($vetor3['avatarUsuario']!="")
			                                        			{
			                                        			
			                                        	?>
			                                            			<a href="#" title="<?php echo $vetor3['nomeUsuario'];?>"><img alt="image" class="img-circle" src="<?php echo $vetor3['avatarUsuario'];?>"></a>
			                                            <?php 
			                                        			}else{
			                                        			?>
			                                        				<a href="#" title="<?php echo retornaNomeCompleto($vetor3['seqCadast']);?>"><img alt="image" class="img-circle" src="img/default-user.png"></a>
			                                        			<?php 	
			                                        			}
		                                        			}	
	                                        			}
                                                                    }
	                                        		}
	                                        	}
	                                            ?>	
	                                        </td>
	                                        <td class="project-actions">
	                                            <a href="?corpo=buscaPlanoAcaoOrganismoDetalhe&idPlanoAcaoOrganismo=<?php echo $vetor['idPlanoAcaoOrganismo'];?>" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> Visualizar </a>
	                                            <?php
                                                    if(is_array($arrNivelUsuario))
                                                    {
                                                        if(in_array("2",$arrNivelUsuario)){?>
                                                            <a href="?corpo=alteraPlanoAcaoOrganismo&idPlanoAcaoOrganismo=<?php echo $vetor['idPlanoAcaoOrganismo'];?>" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Editar </a>
                                                            <?php 
                                                        }
                                                    }
                                                    ?>
	                                            <a href="#" onclick="imprimirPlanoAcaoOrganismo('<?php echo $vetor['idPlanoAcaoOrganismo'];?>','<?php echo $idOrganismoAfiliado;?>');" class="btn btn-white btn-sm"><i class="fa fa-print"></i> Imprimir </a>
	                                            <a href="#" onclick="excluirPlanoAcaoOrganismo('<?php echo $vetor['idPlanoAcaoOrganismo'];?>');" class="btn btn-white btn-sm"><i class="fa fa-trash"></i> Excluir </a>
	                                        </td>
	                                    </tr>
	                                    <?php 
			                                	}
			                                }
			                            }
	                                    ?>
    </tbody>
</table>
                        <?php if ($resultado) { ?>  
                                <div style="text-align:right">
                                    <ul class="pagination">
                                        <?php if($exibir_botao_inicio=='mostrar'){ ?>
                                        <li class="paginate_button previous">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$primeira_pagina?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">
                                                Primeira
                                            </a>
                                        </li>
                                        <li class="paginate_button previous">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$pagina_anterior?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">
                                                Anterior
                                            </a>
                                        </li>
                                        <?php  
                                        }
                                        
                                            /* Loop para montar a páginação central com os números */   
                                            $r=2;
                                            for ($k=$range_inicial; $k <= $range_final; $k++):   
                                              $destaque = ($k == $pagina_atual) ? 'destaque' : '' ;  
                                              ?>
                                                    <li class="paginate_button <?php if($destaque=='destaque'){ echo "active";}?>">
                                                        <?php if($destaque=='destaque'){ ?>
                                                        <a href="#">
                                                        <?=$k?>
                                                        </a>    
                                                        <?php }else{ ?>    
                                                        <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$k?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="<?=$r?>" tabindex="0">
                                                            <?=$k?>
                                                        </a>
                                                        <?php }?>    
                                                            
                                                    </li>
                                            <?php 
                                            $r++;
                                            endfor; ?>
                                        <?php if($exibir_botao_final=='mostrar'){?>            
                                        <li class="paginate_button next">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$proxima_pagina?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="<?=$r++?>" tabindex="0">
                                                Próxima
                                            </a>
                                        </li>
                                        <li class="paginate_button next">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$ultima_pagina?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="<?=$r++?>" tabindex="0">
                                                Última
                                            </a>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>
                                <?php } ?> 