<?php

/*
 * Token
 */

$includePath = [
    '../../sec/token.php',
    '../sec/token.php',
    './sec/token.php'
];

foreach($includePath as $path) {
    if(realpath($path)) {
        require_once $path;
        break;
    }
}

$db = include('../../firebase.php');

if($tokenLiberado)
{
//    include_once('../../model/saldoFinanceiroClass.php');
    include_once('../../model/valoresDoMesClass.php');
    include_once('../../model/valoresDoMesDespesasClass.php');
    include_once('../../model/saldosAnterioresDoMesClass.php');
    include_once('../../model/saldoInicialClass.php');

    $fk_idOrganismoAfiliado	= isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';
    $usuario				= isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';

    //Limpar tabelas auxiliares
    $vm = new valoresDoMes();
    $retorno1 = $vm->limpar($fk_idOrganismoAfiliado);

    $vmd = new valoresDoMesDespesas();
    $retorno2 = $vmd->limpar($fk_idOrganismoAfiliado);

//    $s = new saldoFinanceiro();
//    $retorno3 = $s->limpar($fk_idOrganismoAfiliado);

    $sa = new saldosAnterioresDoMes();
    $retorno4 = $sa->limpar($fk_idOrganismoAfiliado);

    //Limpar Saldo Inicial
    $arr=array();
    $arr['status']=0;

    $si = new saldoInicial();
    $retorno = $si->limparSaldoInicial($fk_idOrganismoAfiliado);

    if ($retorno) {
        $arr['status']=1;
    }

    /** FIREBASE */
//    $saldoInicialReferencia = $db->collection('balances')->document($fk_idOrganismoAfiliado);
//    $snapshot = $saldoInicialReferencia->snapshot();
//    if ($snapshot->exists())
//    {
//        $saldoInicialReferencia->update([
//            ['path' => 'lastUpdatedOpeningBalance', 'value' => strval($usuario)],
//            ['path' => 'updateData', 'value' => date('d/m/Y H:i:s')],
//            ['path' => 'valueOpeningBalance', 'value' => "0"],
//            ['path' => 'balance', 'value' => 0],
//            ['path' => 'status', 'value' => false]
//        ]);
//    }
//
//    $b2 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/balance");
//    $queryPro = $b2->documents();
//
//    foreach ($queryPro as $documentProjecao)
//    {
//        if ($documentProjecao->exists()) {
//
//            $mesProjecaoReferencia = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/balance")->document($documentProjecao->id());
//
//            $mesProjecaoReferencia->update([
//                ['path' => 'monthBalance', 'value' => "0.00"],
//                ['path' => 'previousBalance', 'value' => "0.00"],
//                ['path' => 'updateData', 'value' => date('d/m/Y H:i:s')]
//            ]);
//
//        }
//    }

    echo json_encode($arr);
}	
?>