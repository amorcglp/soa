<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    
    $id				= isset($_POST['id']) ? addslashes($_POST['id']) : '';
    $idImovelControle		= isset($_POST['idImovelControle']) ? addslashes($_POST['idImovelControle']) : '';

    include_once("../../model/imovelControleClass.php");
    $icc = new imovelControle();

    $resultado = $icc->excluiImovelControleAnexo($id);
    //echo $resultado."<br>";

    $retorno=array();

    if ($resultado>0) {
            $retorno['sucess'] = true;
    }
    else {
            $retorno['sucess'] = false;
            $retorno['erro'] = 'Erro ao excluir o Anexo';
    }

    $retorno['idImovel'] = $idImovelControle;
    //echo "<pre>";print_r($retorno);

    echo json_encode($retorno);
}
?>