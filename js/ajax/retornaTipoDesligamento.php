<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            require_once './model/wsClass.php';
    } 

    $codigoDesligamento = isset($_REQUEST['codigoDesligamento'])?$_REQUEST['codigoDesligamento']:0;

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaTipoDesligamento';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'CodTipoDeslig' => $codigoDesligamento);

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    // Imprime o retorno
    echo json_encode($return);
}
?>