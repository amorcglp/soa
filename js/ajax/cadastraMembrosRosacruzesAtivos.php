<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $dataVerificacao			= isset($_REQUEST['dataVerificacao']) ? $_REQUEST['dataVerificacao'] : '';
    $novasAfiliacoes			= isset($_REQUEST['novasAfiliacoes']) ? $_REQUEST['novasAfiliacoes'] : '';
    $desligamentos				= isset($_REQUEST['desligamentos']) ? $_REQUEST['desligamentos'] : '';
    $reintegrados				= isset($_REQUEST['reintegrados']) ? $_REQUEST['reintegrados'] : '';
    $numeroAtualMembrosAtivos	= isset($_REQUEST['numeroAtualMembrosAtivos']) ? $_REQUEST['numeroAtualMembrosAtivos'] : '';
    $usuario					= isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $fk_idOrganismoAfiliado		= isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';

    //Tratar data
    $dataVerificacao = substr($dataVerificacao,6,4)."-".substr($dataVerificacao,3,2)."-".substr($dataVerificacao,0,2);
    $mesAtual =substr($dataVerificacao,3,2);
    $anoAtual=substr($dataVerificacao,6,4);        

    $arr=array();
    $arr['status']=0;

    include_once('../../model/membrosRosacruzesAtivosClass.php');
    //include_once('../../lib/functions.php');
    $r = new MembrosRosacruzesAtivos();
    $r->setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado);
    $r->setDataVerificacao($dataVerificacao);
    $r->setNovasAfiliacoes($novasAfiliacoes);
    $r->setDesligamentos($desligamentos);
    $r->setReintegrados($reintegrados);
    $r->setNumeroAtualMembrosAtivos($numeroAtualMembrosAtivos);
    $r->setUsuario($usuario);
    if(!$r->validaMembrosRosacruzesAtivos($mesAtual,$anoAtual,$fk_idOrganismoAfiliado))
    {    
        $retorno = $r->cadastraMembrosRosacruzesAtivos();
    

        if ($retorno) {
            $ultimo_id=0;
            $resultado = $r->selecionaUltimoId();
            if(count($resultado))
            {
                foreach ($resultado as $vetor)
                {
                        $proximo_id = $vetor['Auto_increment']; 
                }
            }
            $resultadoUltimoId = $r->selecionaUltimoIdInserido();
            if($resultadoUltimoId)
            {
                foreach ($resultadoUltimoId as $vetor)
                {
                    $ultimo_id = $vetor['idMembrosRosacruzesAtivos'];
                }    
            }    
            $arr['ultimoId'] = $ultimo_id;
            $arr['id']=$proximo_id;
            $arr['status']=1;
        }
    }
    echo json_encode($arr);
}	
?>