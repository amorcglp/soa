<?php

//$db = include('../../firebase.php');

function retornaCategoriaRecebimento2($cod) {
    switch ($cod) {
        case 1:
            return "Mensalidade do Organismo";
            break;
        case 2:
            return "Donativos e Lei de AMRA";
            break;
        case 3:
            return "Bazar";
            break;
        case 4:
            return "Comissões";
            break;
        case 5:
            return "Atividades Sociais";
            break;
        case 6:
            return "Recebimentos Diversos";
            break;
        case 7:
            return "Construção";
            break;
        case 8:
            return "Convenções";
            break;
        case 9:
            return "Receitas Financeiras";
            break;
        case 10:
            return "Jornadas";
            break;
        case 11:
            return "Suprimentos do Organismo";
            break;
        case 12:
            return "GLP - Trimestralidades";
            break;
        case 13:
            return "GLP - Suprimentos";
            break;
        case 14:
            return "GLP - Outros valores";
            break;
        case 15:
            return "Mensalidade Atrium Martinista";
            break;
        case 16:
            return "Região";
            break;
        default:
            return "Não identificada";
            break;
    }
}

/*
 * Token - Para teste será comentado essa parte do código
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    $descricaoRecebimento = isset($_REQUEST['descricaoRecebimento']) ? $_REQUEST['descricaoRecebimento'] : '';
    $recebemosDe = isset($_REQUEST['recebemosDe']) ? $_REQUEST['recebemosDe'] : '';
    $codigoAfiliacao = isset($_REQUEST['codigoAfiliacao']) ? $_REQUEST['codigoAfiliacao'] : '';
    $valorRecebimento = isset($_REQUEST['valorRecebimento']) ? $_REQUEST['valorRecebimento'] : '';
    $categoriaRecebimento = isset($_REQUEST['categoriaRecebimento']) ? $_REQUEST['categoriaRecebimento'] : '';
    $usuario = isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $fk_idOrganismoAfiliado = isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';

    $arr=array();
    $arr['status'] = 0;

    include_once('../../lib/functions.php');
    include_once('../../model/saldoInicialClass.php');
    include_once('../../model/recebimentoClass.php');
    include_once('../../model/despesaClass.php');

    $si = new saldoInicial();
    $r = new Recebimento();
    $d = new Despesa();

    //Tratamentos da Data e do Id do Oa para o Firebase

    $dataRecebimentoJS = isset($_REQUEST['dataRecebimento']) ? $_REQUEST['dataRecebimento'] : '';

    //Tratar data
    $dataRecebimento = substr($dataRecebimentoJS, 6, 4) . "-" . substr($dataRecebimentoJS, 3, 2) . "-" . substr($dataRecebimentoJS, 0, 2);

    $mes = intval(substr($dataRecebimento,5,2));
    $ano = intval(substr($dataRecebimento,0,4));

    $mesString = strval(substr($dataRecebimento,5,2));
    $anoString = strval(substr($dataRecebimento,0,4));

    $fk_idOrganismoAfiliadoString = strval($fk_idOrganismoAfiliado);

    $mesInt = intval(substr($dataRecebimento,5,2));
    $anoInt = intval(substr($dataRecebimento,0,4));

    /*
     * Firebase
     */

    $resultado = $r->selecionaUltimoId();
    if (count($resultado)) {
        foreach ($resultado as $vetor) {
            $proximo_id = $vetor['Auto_increment'];
        }
    }

//    //Tratamento do valor do recebimento para enviar para o firebase
//    $valorRecebimentoFloat = floatval(str_replace(",",".",str_replace(".","",$valorRecebimento)));
//    $valorRecebimentoString = strval($valorRecebimentoFloat);
//
//    # [START fs_set_document]
//
//    /*
//     * Modelo Recebimentos Firebase
//     *
//     * $dataArrayRecebimento =
//                    [
//                        'fk_idOrganismoAfiliado' => $idOrganismoAfiliado,
//                        'mes' => $mes,
//                        'ano' => $ano,
//                        'idRecebimentoSOA' => $v['idRecebimento'],
//                        'categoriaRecebimento' => $v['categoriaRecebimento'],
//                        'codigoAfiliacao' => $v['codigoAfiliacao'],
//                        'dataRecebimento' => $v['dataRecebimento'] . " 00:00:00",
//                        'descricaoRecebimento' => $v['descricaoRecebimento'],
//                        'recebemosDe' => $v['recebemosDe'],
//                        'ultimoAtualizar' => $v['ultimoAtualizar'],
//                        'valorRecebimento' => $v['valorRecebimento'],//Fazer conversão para float
//                        'userRecebimentos' => $v['usuario'],
//                        'dataCadastro' => $v['dataCadastro'],
//                        'balance' => 1,
//                        'updateData' => date('d/m/Y H:i:s')
//
//                    ];
//     */
//
//    $dataArray =
//        [
//            'fk_idOrganismoAfiliado'   => $fk_idOrganismoAfiliadoString,
//            'mes'   => $mesString,
//            'ano'   => $anoString,
//            'idRecebimentoSOA' => $proximo_id,
//            'categoriaRecebimento' => $categoriaRecebimento,
//            'codigoAfiliacao' => $codigoAfiliacao,
//            'dataRecebimento' => $dataRecebimentoJS." 00:00:00",
//            'descricaoRecebimento' => $descricaoRecebimento,
//            'recebemosDe' => $recebemosDe,
//            'ultimoAtualizar' => 0,
//            'valorRecebimento' => $valorRecebimentoString,
//            'userRecebimentos' => $usuario,
//            'dataCadastro' => date('d/m/Y H:i:s'),
//            'balance' => 0, //Int
//            'updateData' => ''
//        ];
//
//    //echo "<br><br>Array que será atualizado no Firebase:<pre>";
//    //print_r($dataArray);
//
//    $docRef2 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/recebimentos")->newDocument();
//    $docRef2->set($dataArray);
//
//    $arr['idRefDocumentoFirebase']=$docRef2->id();

    //echo "<br>Tempo 4 - Iniciando registro de entrada no SOA (devido a geração de id): ".date("d/m/Y H:i:s");

    $r = new Recebimento();
    $r->setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado);
    $r->setDataRecebimento($dataRecebimento);
    $r->setDescricaoRecebimento($descricaoRecebimento);
    $r->setRecebemosDe($recebemosDe);
    $r->setCodigoAfiliacao($codigoAfiliacao);
    $r->setValorRecebimento($valorRecebimento);
    $r->setCategoriaRecebimento($categoriaRecebimento);
    $r->setUsuario($usuario);
    $r->setUltimoAtualizar(0);
    $r->setIdRefDocumentoFirebase("");
    $retorno = $r->cadastraRecebimento();

    if ($retorno) {
        $ultimo_id = 0;
        $arr['id'] = $proximo_id;
        $resultadoUltimoId = $r->selecionaUltimoIdInserido();
        if($resultadoUltimoId)
        {
            foreach ($resultadoUltimoId as $vetor)
            {
                $ultimo_id = $vetor['idRecebimento'];
            }
        }

        $arr['ultimoId'] = $ultimo_id;
        $arr['categoriaRecebimento'] = retornaCategoriaRecebimento2($categoriaRecebimento);
        $arr['status'] = 1;

    }

    echo json_encode($arr);

    //echo "<br><br>Tempo 9 - Fim do looping e da aplicação: ".date("d/m/Y H:i:s");

}
?>