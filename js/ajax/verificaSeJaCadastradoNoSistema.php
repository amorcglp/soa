<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/usuarioClass.php');
    $u = new Usuario();

    $u->setSeqCadast($_REQUEST['seq_cadast']);

    if($u->buscaSeqCadastUsuario())
    {
            $retorno['status'] = '1';
    }else{
            $retorno['status'] = '0';
    }	

    echo json_encode($retorno);
}	
?>