<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';
            }else{
                    require_once './model/wsClass.php';
            }
    }
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

    include '../../mailgun.php';

    require_once '../../model/atividadeIniciaticaClass.php';
    require_once '../../model/enviaEmailMembroAtividadeIniciaticaClass.php';

    $CodUsuario					    = 'lucianob';
    $seqCadastMembro				= isset($_REQUEST['seqCadastMembro'])?$_REQUEST['seqCadastMembro']:'';
    $idAtividadeIniciatica			= isset($_REQUEST['idAtividadeIniciatica'])?$_REQUEST['idAtividadeIniciatica']:'';
    $nomeMembro                     = isset($_REQUEST['nomeMembro'])?$_REQUEST['nomeMembro']:'';

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaDadosMembroPorSeqCadast';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => $CodUsuario,
                                    'SeqCadast' => $seqCadastMembro,
                            );
    // Chamada do método
    $return = $ws->callMethod($method, $params, $CodUsuario);

    $email = $return->result[0]->fields->fDesEmail;

    if($email!="")
    {
        //Iniciar variáveis
        $tipoAtividadeIniciatica="";
        $dataAtividadeIniciatica="";
        $horaAtividadeIniciatica="";
        $localAtividadeIniciatica="";

        //Pegar dados da atividade Iniciatica
        $atividade = new atividadeIniciatica();
        $resultado = $atividade->buscaIdAtividadeIniciatica($idAtividadeIniciatica);
        if($resultado)
        {
            foreach($resultado as $vetor)
            {
                switch($vetor['tipoAtividadeIniciatica']){
                    case 0: $tipoAtividadeIniciatica = "[ERRO! Entrar em contato com o organismo!]"; break;
                    case 1: $tipoAtividadeIniciatica = "na Iniciação do 1º Grau de Templo"; break;
                    case 2: $tipoAtividadeIniciatica = "na Iniciação do 2º Grau de Templo"; break;
                    case 3: $tipoAtividadeIniciatica = "na Iniciação do 3º Grau de Templo"; break;
                    case 4: $tipoAtividadeIniciatica = "na Iniciação do 4º Grau de Templo"; break;
                    case 5: $tipoAtividadeIniciatica = "na Iniciação do 5º Grau de Templo"; break;
                    case 6: $tipoAtividadeIniciatica = "na Iniciação do 6º Grau de Templo"; break;
                    case 7: $tipoAtividadeIniciatica = "na Iniciação do 7º Grau de Templo"; break;
                    case 8: $tipoAtividadeIniciatica = "na Iniciação do 8º Grau de Templo"; break;
                    case 9: $tipoAtividadeIniciatica = "na Iniciação do 9º Grau de Templo"; break;
                    case 10: $tipoAtividadeIniciatica = "na Iniciação do 10º Grau de Templo"; break;
                    case 11: $tipoAtividadeIniciatica = "na Iniciação do 11º Grau de Templo"; break;
                    case 12: $tipoAtividadeIniciatica = "na Iniciação do 12º Grau de Templo"; break;
                    case 13: $tipoAtividadeIniciatica = "na Iniciação a Loja"; break;
                    case 14: $tipoAtividadeIniciatica = "na Iniciação ao Pronaos"; break;
                    case 15: $tipoAtividadeIniciatica = "no Discurso de Orientação"; break;
                    case 16: $tipoAtividadeIniciatica = "na Iniciação ao Capitulo"; break;
                }
                $dataAtividadeIniciatica=substr($vetor['dataRealizadaAtividadeIniciatica'],8,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],5,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],0,4);
                $horaAtividadeIniciatica=substr($vetor['horaRealizadaAtividadeIniciatica'],0,5);
                $localAtividadeIniciatica=$vetor['localAtividadeIniciatica'];
            }
        }

        //Gerar Token
        $token= "";
        $valor = "0123456789";
        srand((double)microtime()*1000000);
        for ($i=0; $i<40; $i++){
                $token.= $valor[rand()%strlen($valor)];
        }

        $e = new enviaEmailMembroAtividadeIniciatica();
        while ($e->verificaToken($token))//enquanto o token j� existir gerar novamente
        {
                $token= "";
                $valor = "0123456789";
                srand((double)microtime()*1000000);
                for ($i=0; $i<40; $i++){
                        $token.= $valor[rand()%strlen($valor)];
                }
        }

        //Inserir token
        $e->insereToken($seqCadastMembro, $idAtividadeIniciatica, $email, $token);


            //Enviar email com o token
            $texto = '<div>
            <div dir="ltr">
                    <table class="ecxbody-wrap"
                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                            <tbody>
                                    <tr
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                            <td
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                            <td class="ecxcontainer" width="600"
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                    valign="top">
                                                    <div class="ecxcontent"
                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                            <table class="ecxmain" width="100%" cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                    <tbody>
                                                                            <tr
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                    <td class="ecxcontent-wrap"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                            valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                            style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                            alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                    <br>
                                                                                    <br>
                                                                                    <br>
                                                                                            <div style="text-align: left;"></div>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                    <tbody>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top">Prezado(a) <b>'.strtoupper($nomeMembro).'</b>,</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">
                                                                                                                                Contamos com sua presença  '.$tipoAtividadeIniciatica.' que acontecerá em '.$dataAtividadeIniciatica.'
                                                                                                                                às '.$horaAtividadeIniciatica.'. Local: '.$localAtividadeIniciatica.'
                                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                <td class="ecxcontent-block"
                                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                    valign="top">
                                                                                                                        <b>
                                                                                                                            Para confirmar ou cancelar sua participação, basta clicar em um dos botões abaixo. Não necessitando responder a este e-mail.
                                                                                                                        </b>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top"><a href="https://soa.amorc.org.br/confirmaComparecimentoAtividadeIniciatica.php?t='.$token.'&a=1"
                                                                                                                            class="ecxbtn-primary"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background: #348eda; border-color: #348eda; border-style: solid; border-width: 10px 20px;"
                                                                                                                            target="_blank" alt="Alterar sua senha no SOA"> Confirmar minha presença na Iniciação </a></td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top"><a href="https://soa.amorc.org.br/confirmaComparecimentoAtividadeIniciatica.php?t='.$token.'&a=2"
                                                                                                                            class="ecxbtn-primary"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background: #348eda; border-color: #348eda; border-style: solid; border-width: 10px 20px;"
                                                                                                                            target="_blank" alt="Alterar sua senha no SOA"> Cancelar minha presença na Iniciação </a></td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">AMORC - Ordem Rosacruz</td>
                                                                                                            </tr>
                                                                                                    </tbody>
                                                                                            </table></td>
                                                                            </tr>
                                                                    </tbody>
                                                            </table>
                                                            <div class="footer"
                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                    <div style="text-align: left;"></div>
                                                                    <table width="100%"
                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <tbody>
                                                                                    <tr
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                            <td class="ecxaligncenter ecxcontent-block"
                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                    align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2017</td>
                                                                                    </tr>
                                                                            </tbody>
                                                                    </table>
                                                            </div>
                                                    </div>
                                                    <div style="text-align: left;"></div></td>
                                            <td
                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                    </tr>
                            </tbody>
                    </table>
                    </div>
            </div>';



            $enviado = $mgClient->sendMessage($domain, array(
                'from'    => 'Sistema de Organismos Afiliados <atendimentoportal@amorc.org.br>',
                'to'      => strtoupper($nomeMembro).' <'.$email.'>',
                'subject' => 'Agendamento de Iniciação - Ordem Rosacruz',
                'text'    => 'Seu e-mail não suporta HTML',
                'html'    => $texto));


            if($enviado)
            {
                    $r = $e->enviouEmail($token);
                    echo "{\"status\":\"1\"}";
            }else{
                //echo "<br>Token:".$token."<br><br>";
                    $r = $e->erroEmail($token);
                    echo "{\"status\":\"0\"}";
            }
    }else{
        echo "{\"status\":\"0\"}";
    }
}
?>
