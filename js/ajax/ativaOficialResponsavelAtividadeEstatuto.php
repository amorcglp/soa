<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    $idAtividadeEstatuto		= isset($_POST['idAtividadeEstatuto']) ? addslashes($_POST['idAtividadeEstatuto']) : '';
    $idOficialResponsavel		= isset($_POST['idOficialResponsavel']) ? addslashes($_POST['idOficialResponsavel']) : '';

    include_once("../../model/atividadeEstatutoClass.php");
    $aec = new atividadeEstatuto();

    $resultado = $aec->alteraStatusOficialResponsavel($idAtividadeEstatuto, $idOficialResponsavel, 0);
    //echo $resultado."<br>";

    $retorno=array();

    if ($resultado>0) {
            $retorno['sucesso'] = true;
    }
    else {
            $retorno['sucesso'] = false;
            $retorno['erro'] = 'Erro ao inativar o oficial';
    }

    $retorno['idOficialResponsavel'] = $idOficialResponsavel;
    //echo "<pre>";print_r($retorno);

    echo json_encode($retorno);
}
?>