<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $siglaOA=isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:null;

    include '../../model/organismoClass.php';
    include '../../lib/functions.php';

    $organismo = new organismo();
    $resultado = $organismo->listaOrganismo(null,$siglaOA);

    if($resultado)
    {
            foreach($resultado as $vetor)
            {
                    echo organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'],$vetor['tipoOrganismoAfiliado'],$vetor['nomeOrganismoAfiliado'],$siglaOA);
            }
    }else{
            echo "Organismo não encontrado";
    }
}
?>