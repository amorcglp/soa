<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idMural      = isset($_REQUEST['idPublicacao']) ? $_REQUEST['idPublicacao'] : '';
    $status      = isset($_REQUEST['status']) ? $_REQUEST['status'] : '';

    include_once('../../model/muralClass.php');

    $m = new Mural();

    $retorno = array();

    $resultado = $m->alterarStatusPublicacaoMural($idMural,$status);

    if ($resultado) {
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Publicação inativada com sucesso!';

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao inativar a publicação. Tente novamente ou entre em contato com o setor de Organismos Afiliados';
    }


    echo json_encode($retorno);
}
?>