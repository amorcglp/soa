<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idOrganismo = isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';
    //$ano = 0;

    include_once("../../model/atividadeIniciaticaOficialClass.php");
    $aiomodel = new atividadeIniciaticaOficial();

    $resultadoAnos 	= $aiomodel->retornaAnosJaCadastrados($idOrganismo);

    $ano['anoAtividadeIniciaticaOficial']=array();
    if ($resultadoAnos) {
            foreach ($resultadoAnos as $vetorAno) {
                    $ano['anoAtividadeIniciaticaOficial'][] = $vetorAno['anoAtividadeIniciaticaOficial'];
            }
    }
    //echo "<pre>";print_r($ano);

    $retorno=array();
    $retorno['atividade'] = array();

    $maxSearch = array();

    for ($i = date("Y")+1353; $i >= 3365; $i--) {

            $maxSearch = array_count_values($ano['anoAtividadeIniciaticaOficial']);
            $max = 0;
            if(isset($maxSearch[$i])){
                    $max = $maxSearch[$i];
            }

            if($max < 3){
                    $retorno['atividade']['anos'][] = '<option value="'.$i.'">'.$i.'</option>';
            }
    }


    /*
    for ($i = date("Y")+1353; $i >= 3360; $i--) {
            if(!in_array($i,$ano['anoAtividadeIniciaticaOficial'])){
                    $retorno['atividade']['anos'][] = '<option value="'.$i.'">'.$i.'</option>';
            }
    }
    */

    echo json_encode($retorno);
}