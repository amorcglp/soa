<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $departamento	= isset($_REQUEST['departamento']) ? json_decode($_REQUEST['departamento']) : '';
    $opcao 			= isset($_REQUEST['opcao']) ? json_decode($_REQUEST['opcao']) : '';
    $salvar 		= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/departamentoSubMenuClass.php');
    $dsm = new DepartamentoSubMenu();
    $dsm->setFkIdDepartamento($departamento);
    $dsm->setFkIdSubMenu($opcao);

    if($salvar==0)
    {
            $retorno = $dsm->remove();
    }
    if($salvar==1)
    {
            $retorno = $dsm->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>