<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $oa = isset($_REQUEST['oa']) ? $_REQUEST['oa'] : '';
    $ic = isset($_REQUEST['ic']) ? $_REQUEST['ic'] : '';
    $imovel = isset($_REQUEST['imovel']) ? $_REQUEST['imovel'] : '';

    $selected = $imovel;

    include_once("../../model/imovelControleClass.php");
    $icmodel = new imovelControle();

    $resultado 			= $icmodel->listaImovelPeloOA($oa);
    $resultadoImovel 	= $icmodel->buscaIdImovelControle($ic);

    if ($resultadoImovel) {
            foreach ($resultadoImovel as $vetorImovel) {
            echo "<pre>";print_r($vetorImovel);echo "</pre>";
                    $selected = $vetorImovel['fk_idImovel'];
            }
    }

    $retorno=array();
    $retorno['imoveis'] = array();

    if ($resultado) {
        $retorno['qtdImoveis'] = 0;

        foreach ($resultado as $vetor) {

            $retorno['temImoveis'] = 1;
            $retorno['qtdImoveis'] ++;

            $complemento='';
            if($vetor["complementoImovel"]!=''){
                $complemento = ' - '.$vetor["complementoImovel"];
            }

            if ($vetor['idImovel'] == $selected) {
                    $retorno['imoveis']['options'][] = '<option value="' . $vetor['idImovel'] . '" " selected >' . $vetor["enderecoImovel"] . ' - ' . $vetor["numeroImovel"] . $complemento . '</option>';
            } else {
                    $retorno['imoveis']['options'][] = '<option value="' . $vetor['idImovel'] . '" ">' . $vetor["enderecoImovel"] . ' - ' . $vetor["numeroImovel"] . $complemento . '</option>';
            }
        }

    } else {
        $retorno['temImoveis'] = 0;
    }

    echo json_encode($retorno);
}