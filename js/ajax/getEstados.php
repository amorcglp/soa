<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idPais         = isset($_REQUEST['idPais']) ? $_REQUEST['idPais'] : '';
    $idImovel       = isset($_REQUEST['idImovel']) ? $_REQUEST['idImovel'] : '';

    include_once("../../model/imovelClass.php");
    $imodel = new imovel();

    $resultado = $imodel->listaEstados($idPais);

    $selected = "";
    if ($idImovel != null) {
        $resultadoImovel = $imodel->buscaIdImovel($idImovel);

        if ($resultadoImovel) {
            foreach ($resultadoImovel as $vetorImovel) {
                $selected = $vetorImovel['fk_idEstado'];
            }
        }
    }

    $retorno = array();
    $retorno['imoveis'] = array();

    if ($resultado) {
        $retorno['qtdImoveis'] = 0;

        foreach ($resultado as $vetorEstado) {

            $retorno['temImoveis'] = 1;
            $retorno['qtdImoveis'] ++;
            if ($vetorEstado['id'] == $selected) {
                $retorno['imoveis']['options'][] = '<option value="' . $vetorEstado['id'] . '" " selected >' . $vetorEstado["nome"] . '</option>';
            } else {
                $retorno['imoveis']['options'][] = '<option value="' . $vetorEstado['id'] . '" ">' . $vetorEstado["nome"] . '</option>';
            }
        }
    } else {
        $retorno['temImoveis'] = 0;
    }

    echo json_encode($retorno);
}