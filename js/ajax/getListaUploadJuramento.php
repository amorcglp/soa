<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    //ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    //ini_set("display_errors", 1 );
    //session_start();
    $arrNivelUsuario = isset($_SESSION['niveis'])?explode(",",$_SESSION['niveis']):NULL;
    include_once('../../model/termoVoluntariadoJuramentoClass.php');

    $tva = new termoVoluntariadoJuramento();

    $resultado = $tva->listaTermoVoluntariadoJuramento($_REQUEST['id']);
    $i=1;
    if($resultado)
    {
            if (count($resultado)>0) {
                    echo "<ul>";
                    foreach($resultado as $vetor)
                    {
                            ?>
                            <li>
                                    <a href="<?php echo $vetor['caminho'];?>" target="_blank">Juramento <?php if(count($resultado)>1){?>- Parte <?php echo $i;}?></a>
                                    <?php if(in_array("3",$arrNivelUsuario)){?>
                                    <a href="#" onclick="excluirUploadJuramento('<?php echo $vetor['idTermoVoluntariadoJuramento'];?>','<?php echo $_REQUEST['id'];?>');"><i class="fa fa-trash"></i></a>
                                    <?php }?>
                            </li>
                            <?php
                            $i++; 
                    }
                    echo "</ul>";
            }
    } else {
        echo "Nenhum Juramento foi enviado!";
    }
}
?>