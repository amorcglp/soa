<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    header("Content-Type: text/html;  charset=UTF-8",true);
    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 

    $CodUsuario					= isset($_REQUEST['CodUsuario'])?$_REQUEST['CodUsuario']:'';
    $SeqCadast					= isset($_REQUEST['SeqCadast'])?$_REQUEST['SeqCadast']:'';
    $CodMembro					= isset($_REQUEST['CodMembro'])?$_REQUEST['CodMembro']:'';
    $TipoMembro					= isset($_REQUEST['TipoMembro'])?$_REQUEST['TipoMembro']:'';

    $NumTelefoFixo					= isset($_REQUEST['NumTelefoFixo'])?$_REQUEST['NumTelefoFixo']:'';
    $NumCelula					= isset($_REQUEST['NumCelula'])?$_REQUEST['NumCelula']:'';
    $NumOutroTelefo					= isset($_REQUEST['NumOutroTelefo'])?$_REQUEST['NumOutroTelefo']:'';
    //$DesOutroTelefo				= isset($_REQUEST['DesOutroTelefo'])?$_REQUEST['DesOutroTelefo']:'';

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'atualizarDadosContato';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => $CodUsuario,
                                    'SeqCadast' => $SeqCadast,
                                    'CodMembro' => $CodMembro,
                                    'TipoMembro' => $TipoMembro,
                                    'DesEmail' => $DesEmail, 
                                    'NumTelefoFixo' => $NumTelefoFixo,
                                    'NumCelula' => $NumCelula,
                                    'NumOutroTelefo' => $NumOutroTelefo
                            );

    // Chamada do método
    $return = $ws->callMethod($method, $params, $CodUsuario);

    // Imprime o retorno
    //echo json_encode($return2);
    echo "{\"status\":\"1\"}";

}
?>