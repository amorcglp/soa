<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $camposGerais 	= isset($_REQUEST['camposGerais']) ? json_decode($_REQUEST['camposGerais']) : '';

    include_once('../../model/notificacaoGlpClass.php');
    $u = new NotificacaoGlp();

    $arrOficiais=array();
    $retorno = array();

    //$qntOficiais = $camposGerais->qntOficiais;

    if(count($camposGerais->oficial)>0){

            for ($i=0; $i<count($camposGerais->oficial); $i++){
                    $arrOficiais[$i] = $camposGerais->oficial[$i];
            }
            //print_r($arrOficiais);
            $tipoNotificacaoGlp 		= $camposGerais->tipoNotificacaoGlp;
            $tituloNotificacaoGlp 		= $camposGerais->tituloNotificacaoGlp;
            $fk_seqCadastRemetente 		= $camposGerais->fk_seqCadastRemetente;
            $mensagemNotificacaoGlp 	= $camposGerais->mensagemNotificacaoGlp;

        //echo $tipoNotificacaoGlp;

            if ($tipoNotificacaoGlp!=0){
                    if ($tituloNotificacaoGlp!=''){
                            $resultado = $u->cadastroNotificacao($tituloNotificacaoGlp,$mensagemNotificacaoGlp,$tipoNotificacaoGlp,$fk_seqCadastRemetente);

                            if ($resultado==true) {
                                    $retorno['sucesso'] = true;
                                    $retorno['success'] = 'Notificação enviada com sucesso!';
                                    for ($i=0; $i<count($camposGerais->oficial); $i++){
                                            $ultimoId = $u->selecionaUltimoId();
                                            $resultadoOficiais = $u->cadastroNotificacaoGlp($ultimoId,$arrOficiais[$i]);
                                    }
                            }
                            else {
                                    $retorno['sucesso'] = false;
                                    $retorno['focus'] = 'qntOficiais';
                                    $retorno['erro'] = 'Ocorreu algum erro ao enviar a notificação. Tente novamente ou entre em contato com o setor de TI';	
                            }
                    } else {
                    $retorno['sucesso'] = false;
                    $retorno['focus'] = 'tituloNotificacaoGlp';
                    $retorno['erro'] = 'A notificação está sem título!';	
            }
            } else {
                    $retorno['sucesso'] = false;
                    $retorno['focus'] = 'tipoNotificacaoGlp';
                    $retorno['erro'] = 'Informe o Tipo de Notificação!';	
            }
    } else {
            $retorno['sucesso'] = false;
            $retorno['focus'] = 'qntOficiais';
            $retorno['erro'] = 'Informe pelo menos um Oficial do Organismo!';	
    }

    echo json_encode($retorno);
}
?>