<?php
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    $msg 			= isset($_REQUEST['msg']) ? $_REQUEST['msg'] : '';
    $from 			= isset($_REQUEST['from']) ? $_REQUEST['from'] : '';
    $para			= isset($_REQUEST['para']) ? $_REQUEST['para'] : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/chatClass.php');
    $c = new chat();
    $c->setMensagemChat($msg);
    $c->setFromChat($from);
    $c->setParaChat($para);
    $retorno = $c->cadastraMensagem();

    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);
}	
?>