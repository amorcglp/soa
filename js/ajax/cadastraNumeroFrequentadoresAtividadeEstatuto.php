<?php
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    $idAtividadeEstatuto 	= isset($_REQUEST['idAtividadeEstatuto']) ? json_decode($_REQUEST['idAtividadeEstatuto']) : '';
    $nroNovo 	            = isset($_REQUEST['nroNovo']) ? json_decode($_REQUEST['nroNovo']) : '';

    include_once('../../model/atividadeEstatutoClass.php');
    $aem = new AtividadeEstatuto();

    $retorno = array();

    if ($nroNovo!==''){

        $resultado = $aem->alteraNroFrequentadoresAtividadeEstatuto($idAtividadeEstatuto, $nroNovo);

        if ($resultado==true) {
            $retorno['sucesso'] = true;
            $retorno['success'] = 'Número novo de frequentadores salvo com sucesso!';
        } else {
            $retorno['sucesso'] = false;
            $retorno['erro'] = 'Ocorreu algum erro ao salvar. Tente novamente mais tarde!';
        }

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Preencha o campo com o novo número de frequentadores!';
    }

    echo json_encode($retorno);
}
?>