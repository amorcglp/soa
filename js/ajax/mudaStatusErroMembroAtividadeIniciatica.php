<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    $seqCadast 	                = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast']  : '';
    $idAtividadeIniciatica 	    = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica']  : '';
		$msgRetornoWebService 	    = isset($_REQUEST['msgRetornoWebService']) ? $_REQUEST['msgRetornoWebService']  : '';

    $arr=array();

    include_once('../../model/atividadeIniciaticaClass.php');
    $ai = new atividadeIniciatica();
    $retorno = $ai->mudaStatusErroMembroAtividadeIniciatica($seqCadast,$idAtividadeIniciatica,$msgRetornoWebService);

    $arr['alterado'] = false;

    if ($retorno) {
        $arr['alterado'] = true;
    }

    echo json_encode($arr);
}
?>
