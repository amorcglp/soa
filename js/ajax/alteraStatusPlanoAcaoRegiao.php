<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    $novo_status = ($_REQUEST['status']==1) ? 0 : 1;


    include_once('../../model/planoAcaoRegiaoClass.php');
    $par = new planoAcaoRegiao();
    $par->setIdPlanoAcaoRegiao($_REQUEST['id']);
    $par->setStatusPlano($novo_status);
    $arr = $par->alteraStatusPlano();


    echo json_encode($arr);
}	
?>