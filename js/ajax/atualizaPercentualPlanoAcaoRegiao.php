<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    /**
     * Atualizar percentual do Plano de Ação
     */
    include_once("../../model/planoAcaoRegiaoMetaClass.php");
    $parm = new planoAcaoRegiaoMeta();

    $resultadoMeta = $parm->listaMeta($_REQUEST['idPlanoAcaoRegiao']);	

    if($resultadoMeta)
    {
            $totalMeta = count($resultadoMeta);
    }else{
            $totalMeta = 0;
    }

    $resultadoMetaConcluida = $parm->listaMeta($_REQUEST['idPlanoAcaoRegiao'],1);	

    if($resultadoMetaConcluida)
    {
            $totalMetaConcluida = count($resultadoMetaConcluida);
    }else{
            $totalMetaConcluida = 0;
    }


    /*
     * Cálculo da percentagem concluída
     */
    if($totalMetaConcluida>0)
    {
            $percentual = ($totalMetaConcluida*100)/$totalMeta;
    }else{
            $percentual = 0;
    }

    $arr=array();
    $arr['percentual']=round($percentual);


    echo json_encode($arr);
}	
?>