<?php

require_once('../../lib/functions.php');

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

	//$exibicaoMural		= isset($_POST['exibicaoMural']) ? addslashes($_POST['exibicaoMural']) : '';
	//$fk_idRegiaoRosacruz		= isset($_POST['fk_idRegiaoRosacruz']) ? addslashes($_POST['fk_idRegiaoRosacruz']) : '';
	//$fk_idOrganismoAfiliado		= isset($_POST['fk_idOrganismoAfiliado']) ? addslashes($_POST['fk_idOrganismoAfiliado']) : '';
	$seqCadast					= isset($_POST['seqCadast']) ? addslashes($_POST['seqCadast']) : '';

    $pagAtual                   = isset($_POST['pagAtual']) ? addslashes($_POST['pagAtual']) : '';
    $qntItens                   = isset($_POST['qntItens']) ? addslashes($_POST['qntItens']) : '';
    $filtroRegioes              = isset($_REQUEST['filtroRegioesSelecionados']) ? json_decode($_REQUEST['filtroRegioesSelecionados']) : '';
    $filtroOrganismos           = isset($_REQUEST['filtroOrganismosSelecionados']) ? json_decode($_REQUEST['filtroOrganismosSelecionados']) : '';
    $filtroCategorias           = isset($_REQUEST['filtroCategoriaSelecionada']) ? json_decode($_REQUEST['filtroCategoriaSelecionada']) : '';
    $filtroPublicos             = isset($_REQUEST['filtroPublicoSelecionado']) ? json_decode($_REQUEST['filtroPublicoSelecionado']) : '';
    $filtroFavorito             = isset($_POST['filtroFavorito']) ? addslashes($_POST['filtroFavorito']) : '';


    $inicio = ($pagAtual * $qntItens) - $qntItens;


    include_once("../../model/muralClass.php");
    $mural = new Mural();

    $retorno=array();
    $retorno['vetor'] = array();
    $retorno['qtdVetor'] = 0;

    $i=0;

    //echo "<pre>";print_r($filtroCategorias);echo "</pre>";

    $publicacoes      = $mural->listaMuralAdmin($filtroRegioes,
                                                   $filtroOrganismos,
                                                   $filtroCategorias,
                                                   $filtroPublicos,
                                                   $filtroFavorito,
                                                   $seqCadast,
                                                   $inicio,
                                                   $qntItens);
    
    if ($publicacoes) {
        foreach ($publicacoes as $vetorPublicacoes) {

            $idPublicacao                       = $vetorPublicacoes['idMural'];
            $fk_idMuralCategoria                = $vetorPublicacoes['fk_idMuralCategoria'];
            $fk_idMuralPublico                  = $vetorPublicacoes['fk_idMuralPublico'];
            $classificacaoOrganismoAfiliado     = $vetorPublicacoes['classificacaoOrganismoAfiliado'];
            $nomeOrganismoAfiliado              = $vetorPublicacoes['nomeOrganismoAfiliado'];
            $siglaOrganismoAfiliado             = $vetorPublicacoes['siglaOrganismoAfiliado'];
            $fk_idOrganismoAfiliado             = $vetorPublicacoes['fk_idOrganismoAfiliado'];
            $statusMural                        = $vetorPublicacoes['status'];
            $visualizado                        = $vetorPublicacoes['visualizado'];
            $favorito                           = $vetorPublicacoes['favorito'];

            $dtCadastroMural                    = substr($vetorPublicacoes["dtCadastroMural"],8,2)."/".
                                                  substr($vetorPublicacoes["dtCadastroMural"],5,2)."/".
                                                  substr($vetorPublicacoes["dtCadastroMural"],0,4) . " às " . 
                                                  substr($vetorPublicacoes["dtCadastroMural"],10,6);

            $dtCadastroMuralTimeAgo             = timeAgo($vetorPublicacoes["dtCadastroMural"]);

            $dtAtualizacaoMural                 = substr($vetorPublicacoes["dtAtualizacaoMural"],8,2)."/".
                                                  substr($vetorPublicacoes["dtAtualizacaoMural"],5,2)."/".
                                                  substr($vetorPublicacoes["dtAtualizacaoMural"],0,4) . " às " . 
                                                  substr($vetorPublicacoes["dtAtualizacaoMural"],10,6);
            $dtAtualizacaoMuralTimeAgo          = timeAgo($vetorPublicacoes["dtAtualizacaoMural"]);

            if($statusMural == 0){
                $status = "<span class='label'>Inativa</span>";
            } else {
                $status = $visualizado == 1 ? "<span class='label label-primary'>Lida</span>" : "<span class='label label-danger'>Não Lida</span>";
            }

            if($fk_idMuralCategoria == 1){
                $categoria = "Atividades";
            } elseif($fk_idMuralCategoria == 2){
                $categoria = "Eventos";
            } elseif($fk_idMuralCategoria == 3){
                $categoria = "Notícias";
            }

            if($fk_idMuralPublico == 1){
                $publico = "Rosacruz";
            } elseif($fk_idMuralPublico == 2){
                $publico = "Martinista";
            } elseif($fk_idMuralPublico == 3){
                $publico = "OGG";
            }

            switch ($classificacaoOrganismoAfiliado) {
                case 1: $nomeCompletoOa = "Loja "; break;
                case 2: $nomeCompletoOa = "Pronaos "; break;
                case 3: $nomeCompletoOa = "Capítulo "; break;
                case 4: $nomeCompletoOa = "Heptada "; break;
                case 5: $nomeCompletoOa = "Atrium "; break;
            }
            $nomeCompletoOa .= $nomeOrganismoAfiliado . " - " . $siglaOrganismoAfiliado;

            if($favorito == 1){
                $btnFavorito = <<<EOT
                <button id="btnFavorito-$idPublicacao" name="dtcadastro-$idPublicacao"
                        onClick="desfavoritarPublicacaoAdmin($idPublicacao,$seqCadast);" 
                        class="btn btn-danger btn-circle" 
                        type="button">
                    <i class="fa fa-heart"></i>
                </button>
EOT;
            } else {
                $btnFavorito = <<<EOT
                <button id="btnFavorito-$idPublicacao" name="dtcadastro-$idPublicacao"
                        onClick="favoritarPublicacaoAdmin($idPublicacao,$seqCadast);" 
                        class="btn btn-default btn-circle" 
                        type="button">
                    <i class="fa fa-heart"></i>
                </button>
EOT;
            }

            if($statusMural == 1){
                $btnExclusao = <<<EOT
                <button id="btnExclusao-$idPublicacao" name="btnExclusao-$idPublicacao"
                        onClick="alterarStatusPublicacaoMuralAdmin($idPublicacao,0);" 
                        class="btn btn-info btn-circle" 
                        type="button">
                    <i class="fa fa-check"></i>
                </button>
EOT;
            } else {
                $btnExclusao = <<<EOT
                <button id="btnExclusao-$idPublicacao" name="btnExclusao-$idPublicacao"
                        onClick="alterarStatusPublicacaoMuralAdmin($idPublicacao,1);" 
                        class="btn btn-default btn-circle" 
                        type="button">
                    <i class="fa fa-check"></i>
                </button>
EOT;
            }


            $retorno['vetor'][$i]['line'] = <<<EOT
                <tr>
                    <td id="status-$idPublicacao" name="status-$idPublicacao">$status</td>
                    <td id="categoria-$idPublicacao" name="categoria-$idPublicacao">$categoria</td>
                    <td id="publico-$idPublicacao" name="publico-$idPublicacao">$publico</td>
                    <td><span class="pie" id="nomeoa-$idPublicacao" name="nomeoa-$idPublicacao">$nomeCompletoOa</span></td>
                    <td id="dtcadastro-$idPublicacao" name="dtcadastro-$idPublicacao">$dtCadastroMural</td>
                    <td>
                        <button onclick='abrirModalPublicacaoMural($idPublicacao);'
                                type="button" class="btn btn-w-m btn-info" 
                                data-toggle="modal">
                            Ver Mais
                        </button>
                    </td>
                    <td>
                        $btnFavorito
                        $btnExclusao
                    </td>
                </tr>
EOT;



            $i++; 
        }
    }
    $retorno['qtdVetor'] = $i;

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}
