<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $regiao			= isset($_REQUEST['regiao']) ? $_REQUEST['regiao'] : '';

    $arr=array();
    $arr['regiao']=0;

    include_once('../../model/regiaoRosacruzClass.php');
    $rr = new regiaoRosacruz();
    $rr->setRegiaoRosacruz($regiao);
    $retorno = $rr->buscaRegiaoRosacruzEquals();

    if ($retorno) {
            foreach ($retorno as $vetor)
            {
                    $arr['regiao']=$vetor['idRegiaoRosacruz'];	
            }
    }else{
            $arr['regiao']=0;
    }

    echo json_encode($arr);
}	
?>