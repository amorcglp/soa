<?php


    if(realpath('../../webservice/wsInovad.php')){
        require_once '../../webservice/wsInovad.php';
    }else{
        if(realpath('../webservice/wsInovad.php')){
            require_once '../webservice/wsInovad.php';
        }else{
            require_once './webservice/wsInovad.php';
        }
    }

    if(realpath('../../lib/functions.php')){
        require_once '../../lib/functions.php';
    }else{
        if(realpath('../lib/functions.php')){
            require_once '../lib/functions.php';
        }else{
            require_once './lib/functions.php';
        }
    }

    $loginAtualizadoPor 	    = isset($_REQUEST['loginAtualizadoPor'])?$_REQUEST['loginAtualizadoPor']:0;
    $codigoAfiliacao 		    = isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    $tipoMembro				    = isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"";
    $tipoObrigacao			    = isset($_REQUEST['tipoObrigacao'])?$_REQUEST['tipoObrigacao']:"";
    $dataObrigacao			    = isset($_REQUEST['dataObrigacao'])?$_REQUEST['dataObrigacao']:"";
    $siglaOA				    = isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:"";
    $descricaoLocal			    = isset($_REQUEST['descricaoLocal'])?$_REQUEST['descricaoLocal']:"";
    $companheiro			    = isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:"";
    $membros 	                = isset($_REQUEST['membros']) ? $_REQUEST['membros'] : null;


    $arrMembros = explode("@@",$membros);

    //echo "<pre>";print_r($arrMembros);
    //$return = $ws->callMethod($method, $params, 'lucianob');

    $credentials = montaCredenciais();

    $ano = substr($dataObrigacao,0,4);
    $mes = substr($dataObrigacao,5,2);
    $dia = substr($dataObrigacao,8,2);
    $hora = substr($dataObrigacao,11,8);

    $dataObrigacao = $ano."-".$mes."-".$dia." ".$hora;

    //echo "data=>".$dataObrigacao;

    $arrDatasOrcz = array();
    $arrDatasOrcz[] = "0000-00-00";


    //Montar string para fazer o get de todas iniciações da relação de membros
    $seqCadasts = implode(",",$arrMembros);
    //echo $seqCadasts;


    //Buscar Nome do Usuário vindo do ORCZ
    $vars = array('seq_cadast' => $teste);
    $resposta = json_decode(json_encode(restAmorc("membros/[" . $seqCadasts . "]/ritualistico/1", $vars)), true);
    $obj2 = json_decode(json_encode($resposta), true);
    //echo "<pre>";print_r($obj2);

    $arrDatasOrcz=array();

    if (isset($obj2['data'])) {

        foreach($arrMembros as $m){
            foreach ($obj2['data'][$m] as $vetor2) {

                $dataOrcz = substr($vetor2['dat_obriga'],0,10);
                $seq = $vetor2['seq_cadast'];
                //echo "==>GrauOrcz:".$grauOrcz;

                if (!in_array($dataOrcz, $arrDatasOrcz)) {
                    $arrDatasOrcz[$seq][] = $dataOrcz;
                }
            }
        }


    }

    //echo "<pre>";print_r($arrDatasOrcz);

    //$grauSOA = retornaGrauRCSOA($tipoObrigacao);

//Separar os seqs que podem fazer iniciação
    $dataSOA = substr($dataObrigacao,0,10);
    $seqCadastsPost="";
    $t=0;
    if (isset($arrMembros)) {

        foreach($arrMembros as $m => $k)
        {
            if (!in_array($dataSOA, $arrDatasOrcz[$k])) {//Se a data da obrigação não está nas Datas de Iniciação do Membro
                if($t==0)
                {
                    $seqCadastsPost .= $k;
                }else{
                    $seqCadastsPost .= ",".$k;
                }
                $t++;
            }
        }
    }

//echo 'seqsPost=>'.$seqCadastsPost;

    $vars = Array('form_params' =>
        Array('seq_tipo_obriga' => $tipoObrigacao,
            'sig_orgafi' => $siglaOA,
            'des_local_obriga' => str_replace(",", "", utf8_encode($descricaoLocal)),
            'dat_obriga' => $dataObrigacao,//Substituir por $dataSOA depois que o John tirar o horário da iniciação no cadastro
            'cod_usuari' => 'lucianob'),
        'headers' => Array('Authorization' => 'Basic ' . $credentials),
        'verify' => false);

//print_r($vars);exit();

    $resposta = json_decode(json_encode(restAmorc("membros/[" . $seqCadastsPost . "]/ritualistico/1", $vars, 'POST')), true);
    $obj2 = json_decode(json_encode($resposta), true);
//echo "<pre>";
//print_r($obj2);
//exit();

    $retorno = array();
    if ($obj2['success'] == true) {
        $retorno['status'] = 1;
    } else {
        $retorno['status'] = 2;
    }

    // Imprime o retorno
    echo json_encode($retorno);

?>