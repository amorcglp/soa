<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $seqCadast 		= isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';
    $endereco		= isset($_REQUEST['endereco']) ? $_REQUEST['endereco'] : '';
    $usuario		= isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/inicianteMostrarEnderecoClass.php');
    $i = new inicianteMostrarEndereco();
    $i->setSeqCadast($seqCadast);
    $i->setEndereco($endereco);
    $i->setUsuario($usuario);
    $retorno = $i->cadastro();

    if ($retorno) {
        $arr['status']=1;
    }

    echo json_encode($arr);
}	
?>