<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/rendimentoClass.php');

    $r = new Rendimento();

    $resultado2 = $r->selecionaUltimoId();
    if($resultado2)
    {
            foreach ($resultado2 as $vetor2)
            {
                    $proximo_id = $vetor2['Auto_increment'];
            }
    }


    $resultado = $r->removeRendimento($_REQUEST['id']);

    $retorno = array();
    $retorno['status']=0;

    if ($resultado) {
            $retorno['status']=1;
            $retorno['proximo_id']=$proximo_id;
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>