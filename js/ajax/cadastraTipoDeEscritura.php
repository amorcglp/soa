<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $tipoEscritura 	= $_REQUEST['tipoEscritura'];

    include_once('../../model/imovelClass.php');
    $im = new imovel();

    $retorno = array();

    if ($tipoEscritura!=''){
        $resultado = $im->cadastroTipoDeEscritura($tipoEscritura);

        if ($resultado==true) {
            $retorno['sucesso'] = true;
            $retorno['success'] = 'Tipo de escritura cadastrado com sucesso!';
            $retorno['tipoEscrituraCadastrado'] = $tipoEscritura;
        } else {
            $retorno['sucesso'] = false;
            $retorno['focus'] = 'tipoEscritura';
            $retorno['erro'] = 'Ocorreu algum erro ao enviar a notificação. Tente novamente ou entre em contato com o setor de TI';
        }
    } else {
        $retorno['sucesso'] = false;
        $retorno['focus'] = 'tipoEscritura';
        $retorno['erro'] = 'Preencha o campo: Tipo de Escritura!';
    }

    echo json_encode($retorno);
}
?>