<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $mensagemMural          = isset($_REQUEST['mensagemMural']) ? $_REQUEST['mensagemMural'] : '';
    $exibicaoMural          = isset($_POST['exibicaoMural']) ? addslashes($_POST['exibicaoMural']) : '';
    $fk_idMuralCategoria    = isset($_POST['fk_idMuralCategoria']) ? addslashes($_POST['fk_idMuralCategoria']) : '';
    $fk_idMuralPublico      = isset($_POST['fk_idMuralPublico']) ? addslashes($_POST['fk_idMuralPublico']) : '';
    $fk_idRegiaoOa          = isset($_REQUEST['fk_idRegiaoOa']) ? $_REQUEST['fk_idRegiaoOa'] : '';
    $fk_idOA                = isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';
    $seqCadast              = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';
    $fk_idMuralImagem       = isset($_REQUEST['fk_idMuralImagem']) ? $_REQUEST['fk_idMuralImagem'] : '';

    include_once('../../model/muralClass.php');

    $m = new Mural();

    $retorno = array();

    //echo "fk_idMuralImagem: " . $fk_idMuralImagem . "<br>";

    $resultado = $m->cadastroPublicacaoMural($mensagemMural,$exibicaoMural,$fk_idMuralCategoria,$fk_idRegiaoOa,$fk_idOA,$seqCadast,$fk_idMuralImagem,$fk_idMuralPublico);
    
    if ($resultado) {
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Publicação cadastrada com sucesso!';

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao cadastrar a publicação. Tente novamente ou entre em contato com o setor de TI';
    }


    echo json_encode($retorno);
}
?>