<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $fk_seqCadastRemetente 	        = isset($_REQUEST['fk_seqCadastRemetente']) ? $_REQUEST['fk_seqCadastRemetente'] : '';
    $fk_idTicket 	                = isset($_REQUEST['fk_idTicket']) ? $_REQUEST['fk_idTicket'] : '';
    $mensagemNotificacao 	        = isset($_REQUEST['mensagemNotificacao']) ? $_REQUEST['mensagemNotificacao'] : '';

    include_once('../../model/notificacaoClass.php');
    include_once('../../model/ticketClass.php');

    $n = new Notificacao();
    $t = new Ticket();

    $retorno = array();

    $resultado = $n->cadastroNotificacao($mensagemNotificacao,$fk_seqCadastRemetente,$fk_idTicket);

    if ($resultado) {
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Notificação enviada com sucesso!';

        $t->renovarDataAtualizacaoTicket($fk_idTicket);

        $buscaTicket = $t->buscaTicket($fk_idTicket);
        if($buscaTicket) {
            foreach($buscaTicket as $vetorTicket){
                $atribuidoTicket                  = $vetorTicket['atribuidoTicket'];
                $criadoPorTicket                  = $vetorTicket['criadoPorTicket'];
            }
            if($atribuidoTicket && $criadoPorTicket){
                $t->alteraStatusTicketUsuario(0,$fk_idTicket,$atribuidoTicket);
                $t->alteraStatusTicketUsuario(0,$fk_idTicket,$criadoPorTicket);
            } else {
                $t->alteraStatusTicketUsuario(0,$fk_idTicket);
            }
        }
        $t->alteraStatusTicketUsuario(1,$fk_idTicket,$fk_seqCadastRemetente);

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao enviar a notificação. Tente novamente ou entre em contato com o setor de TI';
    }


    echo json_encode($retorno);
}
?>