<?php

/*
 * Firebase
 */

$db = null;
$includePath = [
    '../../firebase.php',
    '../firebase.php',
    './firebase.php'
];

foreach($includePath as $path) {
    if(realpath($path)) {
        $db = include($path);
        break;
    }
}

/*
 * Token
 */

$includePath = [
    '../../sec/token.php',
    '../sec/token.php',
    './sec/token.php'
];

foreach($includePath as $path) {
    if(realpath($path)) {
        require_once $path;
        break;
    }
}

if($tokenLiberado)
{

    function retornaCategoriaDespesa2($cod) {
        switch ($cod) {
            case 1:
                return "Aluguel";
                break;
            case 2:
                return "Comissões";
                break;
            case 3:
                return "Luz";
                break;
            case 4:
                return "Água";
                break;
            case 5:
                return "Telefone";
                break;
            case 6:
                return "Tarifas";
                break;
            case 7:
                return "Manutenção";
                break;
            case 8:
                return "Beneficiência Social";
                break;
            case 9:
                return "Boletim";
                break;
            case 10:
                return "Convenções";
                break;
            case 11:
                return "Jornadas";
                break;
            case 12:
                return "Reuniões Sociais";
                break;
            case 13:
                return "Despesas de Correio";
                break;
            case 14:
                return "Anúncios";
                break;
            case 15:
                return "Carta Constitutiva (GLP)";
                break;
            case 16:
                return "Cantina";
                break;
            case 17:
                return "Despesas Gerais";
                break;
            case 18:
                return "GLP - Remessa Trimestralidade";
                break;
            case 19:
                return "GLP - Pagamentos Suprimentos";
                break;
            case 20:
                return "GLP - Outros";
                break;
            case 21:
                return "Investimentos";
                break;
            case 22:
                return "Impostos";
                break;
            case 23:
                return "Região";
                break;
            default:
                return "Não identificada";
                break;
        }
    }

    $descricaoDespesa = isset($_REQUEST['descricaoDespesa']) ? $_REQUEST['descricaoDespesa'] : '';
    $pagoA = isset($_REQUEST['pagoA']) ? $_REQUEST['pagoA'] : '';
    $valorDespesa = isset($_REQUEST['valorDespesa']) ? $_REQUEST['valorDespesa'] : '';
    $categoriaDespesa = isset($_REQUEST['categoriaDespesa']) ? $_REQUEST['categoriaDespesa'] : '';
    $usuario = isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $fk_idOrganismoAfiliado = isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';

    //Tratar data
    $dataDespesaJS = isset($_REQUEST['dataDespesa']) ? $_REQUEST['dataDespesa'] : '';
    $dataDespesa = substr($dataDespesaJS, 6, 4) . "-" . substr($dataDespesaJS, 3, 2) . "-" . substr($dataDespesaJS, 0, 2);

    $arr = array();
    $arr['status'] = 0;

    include_once('../../lib/functions.php');
    include_once('../../model/saldoInicialClass.php');
    include_once('../../model/recebimentoClass.php');
    include_once('../../model/despesaClass.php');

    $si = new saldoInicial();
    $r = new Recebimento();
    $d = new Despesa();

    $mes = substr($dataDespesa,5,2);
    $ano = substr($dataDespesa,0,4);

    $mesString = strval(substr($dataDespesa,5,2));
    $anoString = strval(substr($dataDespesa,0,4));

    $fk_idOrganismoAfiliadoString = strval($fk_idOrganismoAfiliado);

    $mesInt = intval(substr($dataDespesa,5,2));
    $anoInt = intval(substr($dataDespesa,0,4));

    $resultado = $d->selecionaUltimoId();
    if (count($resultado)) {
        foreach ($resultado as $vetor) {
            $proximo_id = $vetor['Auto_increment'];
        }
    }

    /*
     * Firebase
     */

//    //Tratamento do valor do recebimento para enviar para o firebase
//    $valorDespesaFloat = floatval(str_replace(",",".",str_replace(".","",$valorDespesa)));
//    $valorDespesaString = strval($valorDespesaFloat);
//
//    # [START fs_set_document]
//    /*
//     * Modelo Despesa Firebase
//     *
//     * 'fk_idOrganismoAfiliado' => $idOrganismoAfiliado,
//                        'mes' => $mes,
//                        'ano' => $ano,
//                        'idDespesaSOA' => $v['idDespesa'],
//                        'categoriaDespesa' => $v['categoriaDespesa'],
//                        'dataDespesa' => $v['dataDespesa'] . " 00:00:00",
//                        'descricaoDespesa' => $v['descricaoDespesa'],
//                        'pagoA' => $v['pagoA'],
//                        'ultimoAtualizar' => $v['ultimoAtualizar'],
//                        'valorDespesa' => $v['valorDespesa'],//Fazer conversão para float
//                        'userDespesas' => $v['usuario'],
//                        'dataCadastro' => $v['dataCadastro'],
//                        'balance' => 1,
//                        'updateData' => date('d/m/Y H:i:s')
//     */
//
//    $dataArray =
//        [
//            'fk_idOrganismoAfiliado'   => $fk_idOrganismoAfiliadoString,
//            'mes'   => $mesString,
//            'ano'   => $anoString,
//            'idDespesaSOA' => $proximo_id,
//            'categoriaDespesa' => $categoriaDespesa,
//            'dataDespesa' => $dataDespesaJS." 00:00:00",
//            'descricaoDespesa' => $descricaoDespesa,
//            'pagoA' => $pagoA,
//            'ultimoAtualizar' => 0,
//            'valorDespesa' => $valorDespesaString,
//            'userDespesas' => $usuario,
//            'dataCadastro' => date('d/m/Y H:i:s'),
//            'balance' => 0, //Int
//            'updateData' => ''
//        ];
//
//    /*
//
//    echo "<br><br>Array que será atualizado no Firebase:<pre>";
//    print_r($dataArray);
//
//    */
//
//    $docRef2 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/despesas")->newDocument();
//    $docRef2->set($dataArray);
//
//    $arr['idRefDocumentoFirebase']=$docRef2->id();

    //Cadastrar a despesa

    $d->setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado);
    $d->setDataDespesa($dataDespesa);
    $d->setDescricaoDespesa($descricaoDespesa);
    $d->setPagoA($pagoA);
    $d->setValorDespesa($valorDespesa);
    $d->setCategoriaDespesa($categoriaDespesa);
    $d->setUsuario($usuario);
    $d->setUltimoAtualizar(0);
    $d->setIdRefDocumentoFirebase("");

    //echo "<pre>";print_r($d);
    $retorno = $d->cadastraDespesa();

    if ($retorno) {
        $ultimo_id = 0;
        $arr['id'] = $proximo_id;
        $resultadoUltimoId = $d->selecionaUltimoIdInserido();
        if($resultadoUltimoId)
        {
            foreach ($resultadoUltimoId as $vetor)
            {
                $ultimo_id = $vetor['idDespesa'];
            }    
        }    
        $arr['ultimoId'] = $ultimo_id;
        $arr['categoriaDespesa'] = retornaCategoriaDespesa2($categoriaDespesa);
        $arr['status'] = 1;
    }
    
    echo json_encode($arr);
}
?>