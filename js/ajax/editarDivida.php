<?php

function retornaCategoriaDivida($cod) {
    switch ($cod) {
        case 1:
            return "Aluguel";
            break;
        case 2:
            return "Comissões";
            break;
        case 3:
            return "Luz";
            break;
        case 4:
            return "Água";
            break;
        case 5:
            return "Telefone";
            break;
        case 6:
            return "Tarifas";
            break;
        case 7:
            return "Manutenção";
            break;
        case 8:
            return "Beneficiência Social";
            break;
        case 9:
            return "Boletim";
            break;
        case 10:
            return "Convenções";
            break;
        case 11:
            return "Jornadas";
            break;
        case 12:
            return "Reuniões Sociais";
            break;
        case 13:
            return "Despesas de Correio";
            break;
        case 14:
            return "Anúncios";
            break;
        case 15:
            return "Carta Constitutiva (GLP)";
            break;
        case 16:
            return "Cantina";
            break;
        case 17:
            return "Despesas Gerais";
            break;
        case 18:
            return "GLP - Remessa Trimestralidade";
            break;
        case 19:
            return "GLP - Pagamentos Suprimentos";
            break;
        case 20:
            return "GLP - Outros";
            break;
        case 21:
            return "Investimentos";
            break;
        case 22:
            return "Impostos";
            break;
        case 23:
            return "Região";
            break;
    }
}

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $id				 		= isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $inicioDivida 			= isset($_REQUEST['inicioDivida']) ? $_REQUEST['inicioDivida'] : '';
    $fimDivida				= isset($_REQUEST['fimDivida']) ? $_REQUEST['fimDivida'] : '';
    $descricaoDivida		= isset($_REQUEST['descricaoDivida']) ? $_REQUEST['descricaoDivida'] : '';
    $valorDivida			= isset($_REQUEST['valorDivida']) ? $_REQUEST['valorDivida'] : '';
    $observacaoDivida		= isset($_REQUEST['observacaoDivida']) ? $_REQUEST['observacaoDivida'] : '';
    $categoriaDivida		= isset($_REQUEST['categoriaDivida']) ? $_REQUEST['categoriaDivida'] : '';
    $usuario                        = isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';

    //Tratar data
    $inicioDivida 	= substr($inicioDivida,6,4)."-".substr($inicioDivida,3,2)."-".substr($inicioDivida,0,2);
    $fimDivida 	= substr($fimDivida,6,4)."-".substr($fimDivida,3,2)."-".substr($fimDivida,0,2);

    $arr=array();
    $arr['status']=0;

    include_once('../../model/dividaClass.php');
    //include_once('../../lib/functions.php');
    $d = new Divida();
    $d->setInicioDivida($inicioDivida);
    $d->setFimDivida($fimDivida);
    $d->setDescricaoDivida($descricaoDivida);
    $d->setValorDivida($valorDivida);
    $d->setObservacaoDivida($observacaoDivida);
    $d->setCategoriaDivida($categoriaDivida);
    $d->setUltimoAtualizar($usuario);
    $retorno = $d->alteraDivida($id);

    if ($retorno) {
            $arr['categoriaDivida']=retornaCategoriaDivida($categoriaDivida);
            $arr['status']=1;
    }

    echo json_encode($arr);
}	
?>