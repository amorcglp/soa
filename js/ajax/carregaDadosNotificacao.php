<?php
function organismoAfiliadoNomeCompleto($classificacao,$tipo,$nomeOa,$sigla=null){
	$nomeCompletoOa = '';
	switch ($classificacao) {
		case 1: $nomeCompletoOa = "Loja "; break;
		case 2: $nomeCompletoOa = "Pronaos "; break;
		case 3: $nomeCompletoOa = "Capítulo "; break;
		case 4: $nomeCompletoOa = "Heptada "; break;
		case 5: $nomeCompletoOa = "Atrium "; break;
	}
	switch ($tipo) {
		case 1: $nomeCompletoOa .= "R+C "; break;
		case 2: $nomeCompletoOa .= "TOM "; break;
	}
	$nomeCompletoOa .= $nomeOa;
	if($sigla!=null) {
		$nomeCompletoOa .= " - " . $sigla;
	}
	return $nomeCompletoOa;
}

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idOrganismoAfiliado 	    = isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:'';

    //echo "ID do Organismo: ".$idOrganismoAfiliado;

    include_once('../../model/organismoClass.php');
    include_once('../../model/usuarioClass.php');
    //include_once('../../lib/functions.php');

    $om = new organismo();
    $um = new Usuario();

    $sigla = $om->buscaIdOrganismo($idOrganismoAfiliado);
    if($sigla){
        foreach($sigla as $vetorSigla){
            $siglaOa                            = $vetorSigla['siglaOrganismoAfiliado'];
            $classificacaoOrganismoAfiliado     = $vetorSigla['classificacaoOrganismoAfiliado'];
            $tipoOrganismoAfiliado              = $vetorSigla['tipoOrganismoAfiliado'];
            $nomeOrganismoAfiliado              = $vetorSigla['nomeOrganismoAfiliado'];
        }
    }
    //echo "Sigla do Organismo: ".$siglaOa;
    //exit();

    $ocultar_json=1;
    $siglaOA=strtoupper($siglaOa);
    $atuantes='S';
    $naoAtuantes='N';

    include 'retornaFuncaoMembro.php';
    $oficial = json_decode(json_encode($return),true);
    $a=0;
    $b=0;
    $retorno=array();
    foreach ($oficial['result'][0]['fields']['fArrayOficiais'] as $vetorOficial) {

        if(($vetorOficial['fields']['fDesTipoFuncao']=='ADMINISTRATIVOS') || ($vetorOficial['fields']['fDesTipoFuncao']=='DIGNITÁRIOS')){
            if($vetorOficial['fields']['fCodMembro']!=0){

                $seqCadastOficial = $vetorOficial['fields']['fSeqCadast'];

                $funcaoOficial = ucfirst(mb_strtolower($vetorOficial['fields']['fDesFuncao'],'UTF-8')).": ";

                $arrNome				= explode(" ",$vetorOficial['fields']['fNomClient']);
                $arrNomeDecrescente		= $arrNome[count($arrNome)-1];
                $nomeOficial = ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNomeDecrescente,'UTF-8'));


                $temCadastro = $um->temUsuario($seqCadastOficial);
                if($temCadastro){
                    $retorno['comRegistro'][] = "<div class='form-group form-inline'><label class='col-sm-6 col-sm-offset-1 control-label' style='padding: 7px 0px 7px 0px'><div class='checkbox i-checks'><input type='checkbox' id='oficial".$a."' name='oficial".$a."[]' value='".$seqCadastOficial."' style='margin-top: 10px'></div>&nbsp;<span>".$funcaoOficial."</span></label><div class='col-sm-5'><p class='form-control-static' style='margin-left: 15px; padding-top: 7px'>".$nomeOficial."</p></div></div><br><br>";
                    $a++;
                } else {
                    $retorno['semRegistro'][] = "<li>".$funcaoOficial.$nomeOficial."</li>";
                    $b++;
                }

            }
        }


    }
    $retorno['nomeCompletoOrganismo'] = "<h4><center>".organismoAfiliadoNomeCompleto($classificacaoOrganismoAfiliado,$tipoOrganismoAfiliado,$nomeOrganismoAfiliado,$siglaOa)."</center></h4><br>";
    $retorno['comRegistroTotal'] = $a;
    $retorno['semRegistroTotal'] = $b;

    echo json_encode($retorno);
}
?>
