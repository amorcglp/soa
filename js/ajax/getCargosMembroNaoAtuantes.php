<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado) {

    $seqCadast = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : null;

    $ocultar_json=1;

    $naoAtuantes='S';
    $atuantes='S';
    include '../../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
//    echo "<pre>"; print_r($obj['result'][0]['fields']['fArrayOficiais']); echo "</pre>";
    $arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];

    if(count($arrFuncoes)>0) { ?>
    <table class="table">
        <thead>
            <tr>
                <th>Cargos</th>
                <th>Ano</th>
                <th>Organismo</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($arrFuncoes as $vetor) {
            if ($vetor['fields']['fDatSaida'] != '') { ?>
                <tr>
                    <td><?php echo $vetor['fields']['fDesFuncao']; ?></td>
                    <td><?php echo substr($vetor['fields']['fDatEntrad'], 0, 4); ?></td>
                    <td><?php echo $vetor['fields']['fNomeOa']; ?></td>
                </tr>
            <?php }
        }?>
        </tbody>
    </table>
    <?php
    } else {
        echo "Não possui";
    }
}
?>