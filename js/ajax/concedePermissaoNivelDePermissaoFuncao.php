<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $funcao						= isset($_REQUEST['funcao']) ? json_decode($_REQUEST['funcao']) : '';
    $nivel			 			= isset($_REQUEST['nivel']) ? json_decode($_REQUEST['nivel']) : '';
    $salvar 					= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/funcaoNivelDePermissaoClass.php');
    $fndp = new FuncaoNivelDePermissao();
    $fndp->setFkIdFuncao($funcao);
    $fndp->setFkIdNivelDePermissao($nivel);

    if($salvar==0)
    {
            $retorno = $fndp->remove();
    }
    if($salvar==1)
    {
            $retorno = $fndp->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>