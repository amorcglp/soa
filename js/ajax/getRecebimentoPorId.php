<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/recebimentoClass.php');

    $r = new Recebimento();

    $resultado = $r->buscarIdRecebimento($_REQUEST['id']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $data 											= date_create($vetor['dataRecebimento']);
            $retorno['dataRecebimento'] 					= date_format($data, 'd/m/Y');
            $retorno['descricaoRecebimento'] 				= $vetor['descricaoRecebimento'];
            $retorno['recebemosDe']							= $vetor['recebemosDe'];
            $retorno['codigoAfiliacao']						= $vetor['codigoAfiliacao'];
            $retorno['valorRecebimento']					= $vetor['valorRecebimento'];
            $retorno['categoriaRecebimento'] 				= $vetor['categoriaRecebimento'];
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>