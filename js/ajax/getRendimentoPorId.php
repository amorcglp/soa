<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/rendimentoClass.php');

    $d = new Rendimento();

    $resultado = $d->buscarIdRendimento($_REQUEST['id']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $data 										= date_create($vetor['dataVerificacao']);
            $retorno['dataVerificacao']					= date_format($data, 'd/m/Y');
            $retorno['descricaoRendimento']				= $vetor['descricaoRendimento'];
            $retorno['valorRendimento']					= $vetor['valorRendimento'];
            $retorno['categoriaRendimento']				= $vetor['categoriaRendimento'];
            $retorno['atribuidoA']			 			= $vetor['atribuidoA'];
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>