<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idTipoAtividade = isset($_REQUEST['idTipoAtividade'])?$_REQUEST['idTipoAtividade']:null;

    include '../../model/atividadeEstatutoTipoClass.php';
    include '../../lib/functions.php';

    $aet = new AtividadeEstatutoTipo();

    $arr = [];
    $retorno = $aet->buscaIdTipoAtividadeEstatuto($idTipoAtividade);
    if($retorno)
    {
        foreach($retorno as $vetor)
        {
            $arr['janeiro']        = $vetor['janeiroTipoAtividadeEstatuto'];
            $arr['fevereiro']      = $vetor['fevereiroTipoAtividadeEstatuto'];
            $arr['marco']          = $vetor['marcoTipoAtividadeEstatuto'];
            $arr['abril']          = $vetor['abrilTipoAtividadeEstatuto'];
            $arr['maio']           = $vetor['maioTipoAtividadeEstatuto'];
            $arr['junho']          = $vetor['junhoTipoAtividadeEstatuto'];
            $arr['julho']          = $vetor['julhoTipoAtividadeEstatuto'];
            $arr['agosto']         = $vetor['agostoTipoAtividadeEstatuto'];
            $arr['setembro']       = $vetor['setembroTipoAtividadeEstatuto'];
            $arr['outubro']        = $vetor['outubroTipoAtividadeEstatuto'];
            $arr['novembro']       = $vetor['novembroTipoAtividadeEstatuto'];
            $arr['dezembro']       = $vetor['dezembroTipoAtividadeEstatuto'];
        }
    }

    echo json_encode($arr);
}