<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    $seqCadast=isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;
    $codigoAfiliacao=0;
    //$ideCompanheiro=isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:'N';
    $numGrau=isset($_REQUEST['numGrau'])?$_REQUEST['numGrau']:null;
    //echo "numGrau====>".$numGrau;
    $lote=isset($_REQUEST['lote'])?$_REQUEST['lote']:null;

    $ocultar_json=1;

    include '../../js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
    include '../../lib/functions.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayObrigacoes']);

    $arrIniciacoes = $obj['result'][0]['fields']['fArrayObrigacoes'];
    $grauWS=0;
    $o=0;
    $arrGraus=array();
    $arrDescricao=array();
    $concluiu=false;
    if(isset($arrIniciacoes))
    {
                            foreach($arrIniciacoes as $vetor)
                            { 
                                $seqTipoObriga=$vetor['fields']['fSeqTipoObriga'];
                                $ideTipoMembro=1;
                                include '../../js/ajax/retornaTipoObrigacaoRitualistica.php';
                                $obj = json_decode(json_encode($return),true);
                                $descricao = $obj['result'][0]['fields']['fArrayTiposObrigacoes'][0]['fields']['fDesTipoObriga'];
                                $arrDescricao = explode(".",$descricao);
                                if(isset($arrDescricao[1]))
                                {    
                                    if(substr(trim($arrDescricao[1]),0,4)=="Grau"||substr(trim($arrDescricao[1]),0,4)=="GRAU")
                                    {
                                        $arrGraus[] = $arrDescricao[0];
                                        if($arrDescricao[0]==12)
                                        {
                                            $concluiu=true;
                                        }    
                                    }
                                }
                            }
                            //echo "<pre>";print_r($arrGraus);
                            if(count($arrGraus)>0)
                            {    
                                $grauWS =  max($arrGraus);
                            }
                            if($grauWS!=0)
                            {    
                                echo $grauWS. "º GRAU";
                            }else{
                                if($lote<6){
                                    echo "Neófito";
                                }else{
                                    echo "1ª GRAU";
                                }
                            }
    }else{
        echo 'Neófito';
        //echo "Não fez nenhuma iniciação";
    }
}