<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idOrganismo = isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';
    $idAtividade = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';
    $ano = date("Y")+1353;

    include_once("../../model/atividadeIniciaticaOficialClass.php");
    include_once("../../model/atividadeIniciaticaClass.php");
    $aiomodel = new atividadeIniciaticaOficial();
    $aimodel = new atividadeIniciatica();

    $resultadoEquipeAtividade 	= $aimodel->buscaIdAtividadeIniciatica($idAtividade);

    $resultadoEquipes 	= $aiomodel->listaAtividadeIniciaticaOficial($idOrganismo,$ano,($ano-1));

    $selected=0;
    if($resultadoEquipeAtividade){
            foreach($resultadoEquipeAtividade as $vetorEquipeAtividade){
                    $selected = $vetorEquipeAtividade['fk_idAtividadeIniciaticaOficial'];
            }
    }


    $retorno=array();

    $retorno['atividade']['total'] = 0;

    if ($resultadoEquipes) {
            foreach ($resultadoEquipes as $vetorEquipes) {

                    $retorno['atividade']['total']++;

                    if($vetorEquipes['idAtividadeIniciaticaOficial']==$selected){
                            $retorno['atividade']['equipes'][] = '<option value="'.$vetorEquipes['idAtividadeIniciaticaOficial'].'" selected>Ano '.$vetorEquipes['anoAtividadeIniciaticaOficial'].' / Equipe Inici&aacute;tica '.$vetorEquipes['tipoAtividadeIniciaticaOficial'].'</option>';
                    } else {
                            $retorno['atividade']['equipes'][] = '<option value="'.$vetorEquipes['idAtividadeIniciaticaOficial'].'">Ano '.$vetorEquipes['anoAtividadeIniciaticaOficial'].' / Equipe Inici&aacute;tica '.$vetorEquipes['tipoAtividadeIniciaticaOficial'].'</option>';
                    }

            }
    }

    /*
    echo "<pre>";
    print_r($retorno);
    echo "</pre>";
    */

    echo json_encode($retorno);
}