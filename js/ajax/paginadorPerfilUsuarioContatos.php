<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

	include_once('../../model/perfilUsuarioClass.php');
	require_once("../../model/criaSessaoClass.php");
	$pu = new perfilUsuario();	
	
   	$tipo=$_GET['tipo'];
   	   	
   	if($tipo=='listagem'){
   		
   		$pag=$_GET['pag'];
   		$maximo=$_GET['maximo'];
		$inicio = ($pag * $maximo) - $maximo;
		$pesquisa=$_GET['pesquisa'];
		$sessao = new criaSessao();
                $notEqual = $sessao->getValue("seqCadast");
		$resultado = $pu->listaContatoInicioMaximo($inicio,$maximo,$pesquisa,$notEqual);
   		if ($resultado) {

			foreach ($resultado as $vetor) {
				if ($vetor['seqCadast'] != $sessao->getValue("seqCadast")){
?>
					<div class="col-lg-6">
		                <div class="contact-box">
		                    <!--<a href="?corpo=perfilUsuario=<?php // echo $vetor['seqCadast'];?>">-->
			                    <div class="col-sm-4">
			                        <div class="text-center">
                                        <center>
			                                <img alt="image" style="max-height: 82.234px; max-width: 87px;" class="img-circle m-t-xs img-responsive" src="<?php if($vetor['avatarUsuario'] != ""){echo $vetor['avatarUsuario'];}else{echo "img/default-user.png";}?>">
                                        </center>
                                        <div class="m-t-xs font-bold">
                                            <?php
                                            require_once ("../../model/funcaoUsuarioClass.php");
                                            require_once ("../../model/funcaoClass.php");
                                            $fum = new FuncaoUsuario();
                                            $fm = new funcao();

                                            $funcao = $fum->buscaPorFuncao($vetor['seqCadast']);
                                            if ($funcao) {
                                                foreach ($funcao as $vetorFuncao) {
                                                    $vFuncao = $vetorFuncao['fk_idFuncao'];
                                                }
                                            }
                                            if(isset($vFuncao)){

                                                if($fum->verificaSeJaExistePorParametro($vetor['seqCadast'],$vFuncao)){
                                                    $funcaoNome = $fm->buscarIdFuncao($vFuncao);
                                                    if ($funcaoNome) {
                                                        foreach ($funcaoNome as $vetorFuncaoNome) {
                                                            $vFuncaoNome = $vetorFuncaoNome['nomeFuncao'];
                                                        }
                                                        if($vFuncaoNome=='GLP'){
                                                            echo $vFuncaoNome;
                                                        } else {
                                                            echo ucfirst(mb_strtolower($vFuncaoNome,'UTF-8'));
                                                        }
                                                    }
                                                }else {
                                                    echo "Erro ao identificar a função";
                                                }
                                            } else {
                                                echo "Erro ao identificar a função";
                                            }
                                            ?>
                                        </div>
			                        </div>
			                    </div>
			                    <div class="col-sm-8">
			                    <?php 
			                    	$arrNome				= explode(" ",$vetor['nomeUsuario']);
			                    	$arrNomeDecrescente		= $arrNome[count($arrNome)-1];
			                    ?>
                                    <h3><strong> <?php echo ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNomeDecrescente,'UTF-8'));?></strong></h3>
			                        <address style="margin-top: 10px;">
			                            <i style="width: 16px" class="fa fa-institution"></i>
                                        <?php
                                        include_once("../../model/organismoClass.php");
                                        $o = new organismo();
                                        $nomeOrganismo = " LOJA R+C CURITIBA - PR101";//Default
                                        if($sessao->getValue("siglaOA")!="") {
                                            $resultadoOA = $o->listaOrganismo(null,$vetor['siglaOA']);
                                            if ($resultadoOA) {
                                                foreach ($resultadoOA as $vetorOA) {
                                                    $sessao->setValue("idOrganismoAfiliado", $vetorOA["idOrganismoAfiliado"]);//Guardar Id do Organismo na Sessão
                                                    switch($vetorOA['classificacaoOrganismoAfiliado']){
                                                        case 1: $classificacao =  "Loja"; break;
                                                        case 2: $classificacao =  "Pronaos"; break;
                                                        case 3: $classificacao =  "Capítulo"; break;
                                                        case 4: $classificacao =  "Heptada"; break;
                                                        case 5: $classificacao =  "Atrium"; break;
                                                    }
                                                    switch($vetorOA['tipoOrganismoAfiliado']){
                                                        case 1: $tipo = "R+C"; break;
                                                        case 2: $tipo = "TOM"; break;
                                                    }
                                                    $nomeOrganismo = $classificacao . " " . $tipo . " " .$vetorOA["nomeOrganismoAfiliado"]. " - ".$vetorOA["siglaOrganismoAfiliado"];
                                                }
                                            }
                                        }
                                        echo $nomeOrganismo;
                                        ?>
                                        <br>
                                        <br>
			                        <i style="width: 16px" class="fa fa-credit-card"></i> 
                                                <?php echo strtolower($vetor['codigoDeAfiliacao']);?>
                                                <!--<br>
			                        <?php //if ($vetor['emailUsuario'] != "") {?>
                                                <a data-container="body" data-toggle="popover" data-placement="left" data-content="<?php //echo strtolower($vetor['emailUsuario']);?>">
                                                <i style="width: 16px" class="fa fa-envelope"></i> <?php //echo strtolower($vetor['emailUsuario']);?>
                                                <?php
                                                /*
                                                $arrEmail				= str_split(strtolower($vetor['emailUsuario']));
                                                echo $arrEmail[0].$arrEmail[1].$arrEmail[2].$arrEmail[3].$arrEmail[4].$arrEmail[5].$arrEmail[6].$arrEmail[7].$arrEmail[8].$arrEmail[9].$arrEmail[10].$arrEmail[11]." ... [visualize]";
                                                */
                                                ?>
                                            </a>-->
                                            <br>
			                            <?php //}?>

			                            <?php //if ($vetor['telefoneResidencialUsuario'] != "") { ?>
			                            	<!--<i style="width: 16px" class="fa fa-phone"></i> <?php //echo $vetor['telefoneResidencialUsuario'];?><br>
			                            	
			                            <?php //} else { ?>
			                            
			                            	<?php //if ($vetor['celularUsuario'] != "") { ?>
			                            	
			                            		<i style="width: 16px" class="fa fa-mobile-phone"></i> <?php //echo $vetor['celularUsuario'];?>
			                            		
			                            	<?php //} else { ?>
			                            	
				                            	<?php //if ($vetor['telefoneComercialUsuario'] != "") { ?>
				                            	
				                            		<i style="width: 16px" class="fa fa-phone"></i> <?php //echo $vetor['telefoneComercialUsuario'];?>
				                            		
				                            	<?php //} else { ?>
				                            	
				                            		<i style="width: 16px" class="fa fa-phone"></i> Nenhum Telefone Registrado
				                            		
				                            	<?php //} ?>
				                            	
			                            	<?php //} ?>
			                            	
			                            <?php //} ?>
			                            -->
			                        </address>
			                    </div>
			                    <div class="clearfix"></div>
		                    <!--</a>-->
		                </div>
		            </div>
<?php 
				}
			}
			
		} else {
			return false;
		}
	} else if($tipo=='contador') {
		
		$pesquisa = $_GET['pesquisa'];

        $sessao = new criaSessao();
        $notEqual = $sessao->getValue("seqCadast");
		$resultado = $pu->listaContato($pesquisa,$notEqual);
		
		if ($resultado != false) {
			$contador = count($resultado);
			echo $contador;
		}
	} else {
		echo "Solicitação inválida";
	}
}        
?>
