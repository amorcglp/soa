<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $mensagemMuralComentario    = isset($_REQUEST['mensagemMuralComentario']) ? $_REQUEST['mensagemMuralComentario'] : '';
    $fk_idMural               = isset($_REQUEST['fk_idMural']) ? $_REQUEST['fk_idMural'] : '';
    $seqCadast                  = isset($_SESSION['seqCadast']) ? $_SESSION['seqCadast'] : '';

    include_once('../../model/muralClass.php');

    $m = new Mural();

    $retorno = array();

    $resultado = $m->cadastroComentarioPublicacaoMural($mensagemMuralComentario,$fk_idMural,$seqCadast);

    if ($resultado) {
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Publicação cadastrada com sucesso!';

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao cadastrar a publicação. Tente novamente ou entre em contato com o setor de TI';
    }


    echo json_encode($retorno);
}
?>