<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idAtividadeIniciatica 			        = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';

    /* ALGORITMO */
    include_once('../../model/atividadeIniciaticaClass.php');

    $aim    = new atividadeIniciatica();

    $aim->deletaMembroAtividadeIniciatica($idAtividadeIniciatica);
    $aim->deletaAtividadeIniciatica($idAtividadeIniciatica);

    $retorno = array();

    if(deletaAtividadeIniciatica){
        $retorno = true;
    }

    echo json_encode($retorno);
}
?>