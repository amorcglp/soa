<?php 
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
 
require_once '../../lib/functions.php';
?>
    <option>Selecione...</option>
    <?php

    $idCampoOpcao 	= isset($_REQUEST['idCampoOpcao'])?$_REQUEST['idCampoOpcao']:null;
    $parametro		= isset($_REQUEST['parametro'])?$_REQUEST['parametro']:null;
    $selecionaOpcao	= isset($_REQUEST['selecionaOpcao'])?$_REQUEST['selecionaOpcao']:null;

    if($idCampoOpcao=="fk_idSubMenu")
    {
            include_once('../../model/subMenuClass.php');
            $smc = new subMenu();
            $resultado = $smc->listaSubMenu($parametro);

            if ($resultado) {
                    foreach ($resultado as $vetor) {
                            if ($selecionaOpcao != 0 && $selecionaOpcao == $vetor["idSubMenu"]) 
                            {
                                    $selecionado = 'selected = selected';
                            } else {
                                    $selecionado = '';
                            }
                            echo '<option value="' . $vetor['idSubMenu'] . '" ' . $selecionado . '>' . $vetor["nomeSubMenu"] . '</option>';
                    }
            }
    }

    if($idCampoOpcao=="fk_idFuncao")
    {
            include_once('../../model/funcaoClass.php');
            $f = new funcao();
            $resultado = $f->listaFuncao($parametro);

            if ($resultado) {
                    foreach ($resultado as $vetor) {
                            if ($selecionaOpcao != 0 && $selecionaOpcao == $vetor["idSubMenu"]) 
                            {
                                    $selecionado = 'selected = selected';
                            } else {
                                    $selecionado = '';
                            }
                            echo '<option value="' . $vetor['idFuncao'] . '" ' . $selecionado . '>' . $vetor["nomeFuncao"] . '</option>';
                    }
            }
    }

    if($idCampoOpcao=="fk_idCidade")
    {
            include_once('../../model/cidadeClass.php');
            $c = new Cidade();
            $resultado = $c->listaCidade($parametro);

            if ($resultado) {
                    foreach ($resultado as $vetor) {
                            if ($selecionaOpcao != 0 && $selecionaOpcao == $vetor["id"]) 
                            {
                                    $selecionado = 'selected = selected';
                            } else {
                                    $selecionado = '';
                            }
                            echo '<option value="' . $vetor['id'] . '" ' . $selecionado . '>' . $vetor["nome"] . '</option>';
                    }
            }
    }
    
    if($idCampoOpcao=="fk_idOrganismoAfiliado")
    {
            include_once('../../model/organismoClass.php');
            $o = new organismo();
            $resultado = $o->listaOrganismo(null,null,null,null,$parametro);

            if ($resultado) {
                    foreach ($resultado as $vetor) {
                            if ($selecionaOpcao != 0 && $selecionaOpcao == $vetor["idOrganismoAfiliado"]) 
                            {
                                    $selecionado = 'selected = selected';
                            } else {
                                    $selecionado = '';
                            }
                            echo '<option value="' . $vetor['idOrganismoAfiliado'] . '" ' . $selecionado . '>' . retornaNomeCompletoOrganismoAfiliado($vetor["idOrganismoAfiliado"]) . '</option>';
                    }
            }
    }

}
	
?>