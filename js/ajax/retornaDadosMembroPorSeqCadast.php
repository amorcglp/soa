<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}
if(isset($_COOKIE['tL']))
{
    $tokenLiberado = $_COOKIE['tL'];
}    
if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 
    if(!isset($seqCadast))
    {
            $seqCadast 		= isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:0;
    }
    if(!isset($tipoMembro))
    {
            $tipoMembro				= isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"0";
    }
    $seqCadast 			= isset($seqCadast)?$seqCadast:0;
    $seqCadast 			= isset($seqCadast2)?$seqCadast2:$seqCadast;
    $ocultar_json 			= isset($ocultar_json)?$ocultar_json:0;
    $server = isset($server) ? $server : null;
    //echo "seqCadast: ".$seqCadast;

    // Instancia a classe
    $ws = new Ws($server);

    // Nome do Método que deseja chamar
    $method = 'RetornaDadosMembroPorSeqCadast';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SeqCadast' => $seqCadast);

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    if($ocultar_json==0)
    {
            // Imprime o retorno
            echo json_encode($return);
    }
}
?>