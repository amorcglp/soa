<?php

/*
 * Token
 */
if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

	//$exibicaoMural		= isset($_POST['exibicaoMural']) ? addslashes($_POST['exibicaoMural']) : '';
	$idPublicacao		= isset($_POST['idPublicacao']) ? addslashes($_POST['idPublicacao']) : '';
    $seqCadast     		= isset($_POST['seqCadast']) ? addslashes($_POST['seqCadast']) : '';

    include_once("../../model/muralClass.php");
    $mural = new Mural();

    $retorno=array();
    $retorno['sucesso'] = false;

    $resultado 		= $mural->jaCurtido($idPublicacao,$seqCadast);
    if($resultado > 0){
		$resultado2 = $mural->descurtirPublicacaoMural($idPublicacao,$seqCadast);
    	if($resultado2){
    		$retorno['sucesso'] = true;
    	}
    }
    
    //echo "<pre>";print_r($retorno);echo "</pre>";
    echo json_encode($retorno);
}
