<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once('../../model/organismoClass.php');

    $o = new organismo();

    $resultado = $o->listaOrganismo($_REQUEST['search'],null,"siglaOrganismoAfiliado",$_REQUEST['regiao'],null,null,null,1,null,null,null,1);

    if ($resultado) {
            foreach ($resultado as $vetor) {

                    switch($vetor['classificacaoOrganismoAfiliado']){
                            case 1:
                                    $classificacao =  "Loja";
                                    break;
                            case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                            case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                            case 4:
                                    $classificacao =  "Heptada";
                                    break;
                            case 5:
                                    $classificacao =  "Atrium";
                                    break;
                    }
                    switch($vetor['tipoOrganismoAfiliado']){
                            case 1:
                                    $tipo = "R+C";
                                    break;
                            case 2:
                                    $tipo = "TOM";
                                    break;
                    }

                    $nomeOrganismo = $vetor["siglaOrganismoAfiliado"]." - ".$classificacao . " " . $tipo . " " .$vetor["nomeOrganismoAfiliado"];
                    ?>
                            <a
                                    href="alteraLotacao.php?corpo=<?php echo $_REQUEST['corpo'];?>&sigla=<?php echo $vetor["siglaOrganismoAfiliado"];?>"><i
                                    class="fa fa-globe"></i> <?php echo $nomeOrganismo;?> </a>
                            <br>
                            <br>
                    <?php
            }
    }
}    
?>