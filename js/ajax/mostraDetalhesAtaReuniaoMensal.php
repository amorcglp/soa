<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/ataReuniaoMensalClass.php');
    include_once '../../model/ataReuniaoMensalOficialClass.php';
    include_once '../../model/ataReuniaoMensalTopicoClass.php';
    include_once("../../lib/webservice/retornaInformacoesMembro.php");
    include_once('../../model/usuarioClass.php');
    $arm = new ataReuniaoMensal();
    $um = new usuario();

    $resultado = $arm->buscarIdAtaReuniaoMensal($_REQUEST['idAta']);

    $retorno = array();

    if ($resultado) {
            foreach($resultado as $vetor)
            {
                    $data = date_create($vetor['dataAtaReuniaoMensal']);
                    $retorno['dataAtaReuniaoMensal'] = date_format($data, 'd/m/Y');
                    $retorno['horaInicialAtaReuniaoMensal'] = $vetor['horaInicialAtaReuniaoMensal'];
                    $retorno['horaFinalAtaReuniaoMensal'] = $vetor['horaFinalAtaReuniaoMensal'];
                    $retorno['numeroMembrosAtaReuniaoMensal'] = $vetor['numeroMembrosAtaReuniaoMensal'];
                    $retorno['nomePresidenteAtaReuniaoMensal'] = $vetor['nomePresidenteAtaReuniaoMensal'];
                    
                    $resultadoAtualizadoPor = $um->buscaUsuario($vetor['fk_seq_cadast']);
                    if ($resultadoAtualizadoPor) {
                        foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                            $retorno['cadastrado_por']           = $vetorAtualizadoPor['nomeUsuario'];
                        }
                    }
                    $retorno['atualizado_por'] = "--";
                    if($vetor['ultimo_atualizar'] != 0){
                        $resultadoAtualizadoPor = $um->buscaUsuario($vetor['ultimo_atualizar']);
                        if ($resultadoAtualizadoPor) {
                            foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                $retorno['atualizado_por']           = $vetorAtualizadoPor['nomeUsuario'];
                            }
                        }
                    }
                    $retorno['entregue'] =$vetor['entregue'];
                    $retorno['data'] = $vetor['data']!='0000-00-00' ?  substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)."/".substr($vetor['data'],0,4)." ".substr($vetor['data'],11,5) : 'Não Informado';
                    $retorno['dataEntrega'] = substr($vetor['dataEntrega'],0,10)!='0000-00-00' ?  substr($vetor['dataEntrega'],8,2)."/".substr($vetor['dataEntrega'],5,2)."/".substr($vetor['dataEntrega'],0,4)." ".substr($vetor['dataEntrega'],11,5) : 'Não Informado';
                    $retorno['dataEntregouCompletamente'] = substr($vetor['dataEntregouCompletamente'],0,10)!='0000-00-00' ?  substr($vetor['dataEntregouCompletamente'],8,2)."/".substr($vetor['dataEntregouCompletamente'],5,2)."/".substr($vetor['dataEntregouCompletamente'],0,4)." ".substr($vetor['dataEntregouCompletamente'],11,5) : 'Não Informado';
                    $retorno['numeroAssinatura'] =$vetor['numeroAssinatura'];
                    if($vetor['entregue']==1)
                    {
                        $retorno['quemEntregou'] =$vetor['quemEntregou'];
                    }else{
                        $retorno['quemEntregou'] ="Não informado";
                    }




            }

            //Oficiais que assinaram o documento
            $resultadoOficiaisQueAssinaram = $arm->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($_REQUEST['idAta']);
            $u=1;
            if ($resultadoOficiaisQueAssinaram) {
                $retorno['oficiaisQueAssinaram']="";
                foreach ($resultadoOficiaisQueAssinaram as $v) {
                    $retorno['oficiaisQueAssinaram'] .= $u.". ".$v['nomeUsuario']." - ".$v['nomeFuncao']."<br>";
                    $u++;
                }
            }else{
                $retorno['oficiaisQueAssinaram']="Ninguém assinou eletronicamente esse documento";
            }

            //Falta assinar
            $mestre         = $arm->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($_REQUEST['idAta'],365);
            $secretario     = $arm->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($_REQUEST['idAta'],367);
            $retorno['faltaAssinar']="";
            if (!$mestre||!$secretario) {
                if(!$mestre)
                {
                    $retorno['faltaAssinar'] .= "MESTRE DO ORGANISMO AFILIADO<br>";
                }
                if(!$secretario)
                {
                    $retorno['faltaAssinar'] .= "SECRETÁRIO DO ORGANISMO AFILIADO<br>";
                }
            }else{
                $retorno['faltaAssinar']="Documento 100% entregue";
            }

            $armo = new ataReuniaoMensalOficial();
            $resultado2 = $armo->listaOficiaisAdministrativos($_REQUEST['idAta']);
            $i=0;
            if($resultado2)
            {
                    foreach($resultado2 as $vetor2)
                    {
                            if($i==0)
                            {
                                    $retorno['oficiaisAdministrativos'] = "[".retornaCodigoAfiliacao($vetor2['fk_seq_cadastMembro'])."] ".retornaNomeCompleto($vetor2['fk_seq_cadastMembro']);
                            }else{
                                    $retorno['oficiaisAdministrativos'] .= "<br>[".retornaCodigoAfiliacao($vetor2['fk_seq_cadastMembro'])."] ".retornaNomeCompleto($vetor2['fk_seq_cadastMembro']);
                            }
                            $i++;
                    }
            }
            $armt = new ataReuniaoMensalTopico();
            $resultado3 = $armt->listaTopicos($_REQUEST['idAta']);
            if($resultado3)
            {
                    foreach($resultado3 as $vetor3)
                    {
                            $retorno['topico_um']		=$vetor3['topico_um'];
                            $retorno['topico_dois']		=$vetor3['topico_dois'];
                            $retorno['topico_tres']		=$vetor3['topico_tres'];
                            $retorno['topico_quatro']	=$vetor3['topico_quatro'];
                            $retorno['topico_cinco']	=$vetor3['topico_cinco'];
                            $retorno['topico_seis']		=$vetor3['topico_seis'];
                            $retorno['topico_sete']		=$vetor3['topico_sete'];
                            $retorno['topico_oito']		=$vetor3['topico_oito'];
                            $retorno['topico_nove']		=$vetor3['topico_nove'];
                            $retorno['topico_dez']		=$vetor3['topico_dez'];
                    }	
            }
    } else {
        $retorno['erro'][] = 'Ocorreu algum erro ao cadastrar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    echo json_encode($retorno);
}
?>