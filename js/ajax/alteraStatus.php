<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $novo_status = ($_REQUEST['status']==1) ? 0 : 1;

    if($_REQUEST['classe']=="regiaoRosacruz")
    {
            include_once('../../model/regiaoRosacruzClass.php');
            $rr = new regiaoRosacruz();
            $rr->setIdRegiaoRosacruz($_REQUEST['id']);
            $rr->setStatusRegiaoRosacruz($novo_status);
            $arr = $rr->alteraStatusRegiaoRosacruz();
    }else if($_REQUEST['classe']=="secaoMenu")
    {
            include_once('../../model/secaoMenuClass.php');
            $sm = new secaoMenu();
            $sm->setIdSecaoMenu($_REQUEST['id']);
            $sm->setStatusSecaoMenu($novo_status);
            $arr = $sm->alteraStatusSecaoMenu();
    }else if($_REQUEST['classe']=="subMenu")
    {
            include_once('../../model/subMenuClass.php');
            $sm = new subMenu();
            $sm->setIdSubMenu($_REQUEST['id']);
            $sm->setStatusSubMenu($novo_status);
            $arr = $sm->alteraStatusSubMenu();
    }else if($_REQUEST['classe']=="opcaoSubMenu")
    {
            include_once('../../model/opcaoSubMenuClass.php');
            $osm = new opcaoSubMenu();
            $osm->setIdOpcaoSubMenu($_REQUEST['id']);
            $osm->setStatusOpcaoSubMenu($novo_status);
            $arr = $osm->alteraStatusOpcaoSubMenu();
    }else if($_REQUEST['classe']=="nivelDePermissao")
    {
            include_once('../../model/nivelDePermissaoClass.php');
            $np = new nivelDePermissao();
            $np->setIdNivelDePermissao($_REQUEST['id']);
            $np->setStatusNivelDePermissao($novo_status);
            $arr = $np->alteraStatusNivelDePermissao();
    }else if($_REQUEST['classe']=="tipoObjetoDashboard")
    {
            include_once('../../model/tipoObjetoDashboardClass.php');
            $tod = new tipoObjetoDashboard();
            $tod->setIdTipoObjetoDashboard($_REQUEST['id']);
            $tod->setStatusTipoObjetoDashboard($novo_status);
            $arr = $tod->alteraStatusTipoObjetoDashboard();
    }else if($_REQUEST['classe']=="objetoDashboard")
    {
            include_once('../../model/objetoDashboardClass.php');
            $od = new objetoDashboard();
            $od->setIdObjetoDashboard($_REQUEST['id']);
            $od->setStatusObjetoDashboard($novo_status);
            $arr = $od->alteraStatusObjetoDashboard();
    }else if($_REQUEST['classe']=="funcao")
    {
            include_once('../../model/funcaoClass.php');
            $f = new funcao();
            $f->setIdFuncao($_REQUEST['id']);
            $f->setStatusFuncao($novo_status);
            $arr = $f->alteraStatusFuncao();
    }else if($_REQUEST['classe']=="departamento")
    {
            include_once('../../model/departamentoClass.php');
            $d = new departamento();
            $d->setIdDepartamento($_REQUEST['id']);
            $d->setStatusDepartamento($novo_status);
            $arr = $d->alteraStatusDepartamento();
    }else if($_REQUEST['classe']=="usuario")
    {
            include_once('../../model/usuarioClass.php');
            $u = new Usuario();
            $u->setSeqCadast($_REQUEST['id']);
            $u->setStatusUsuario($novo_status);
            $arr = $u->alteraStatusUsuario();
    }else if($_REQUEST['classe']=="organismo")
    {
            include_once('../../model/organismoClass.php');
            $o = new organismo();
            $o->setIdOrganismoAfiliado($_REQUEST['id']);
            $o->setStatusOrganismoAfiliado($novo_status);
            $arr = $o->alteraStatusOrganismo();
    }else if($_REQUEST['classe']=="imovel")
    {
            include_once('../../model/imovelClass.php');
            $i = new imovel();
            $i->setIdImovel($_REQUEST['id']);
            $i->setStatusImovel($novo_status);
            $arr = $i->alteraStatusImovel();
    }else if($_REQUEST['classe']=="imovelControle")
    {
            include_once('../../model/imovelControleClass.php');
            $ic = new imovelControle();
            $ic->setIdImovelControle($_REQUEST['id']);
            $ic->setStatusImovelControle($novo_status);
            $arr = $ic->alteraStatusImovelControle();
    }
    else if($_REQUEST['classe']=="notificacaoServer")
    {
            include_once('../../model/notificacaoAtualizacaoServerClass.php');
            $n = new NotificacaoServer();
            $n->setIdNotificacao($_REQUEST['id']);
            $n->setStatusNotificacao($novo_status);
            $arr = $n->alteraStatusNotificacaoServer();
    }
    else if($_REQUEST['classe']=="atividadeIniciatica")
    {
            include_once('../../model/atividadeIniciaticaClass.php');
            $ai = new atividadeIniciatica();
            $ai->setIdAtividadeIniciatica($_REQUEST['id']);
            $ai->setStatusAtividadeIniciatica($novo_status);
            $arr = $ai->alteraStatusAtividadeIniciatica();
    }
    else if($_REQUEST['classe']=="faq")
    {
            include_once('../../model/faqClass.php');
            $fc = new Faq();
            $fc->setIdFaq($_REQUEST['id']);
            $fc->setStatusFaq($novo_status);
            $arr = $fc->alteraStatusFaq();
    }
    else if($_REQUEST['classe']=="atividadeEstatuto")
    {
            include_once('../../model/atividadeEstatutoClass.php');
            $aec = new AtividadeEstatuto();
            $aec->setIdAtividadeEstatuto($_REQUEST['id']);
            $aec->setStatusAtividadeEstatuto($novo_status);
            $arr = $aec->alteraStatusAtividadeEstatuto();
    }
    else if($_REQUEST['classe']=="atividadeEstatutoTipo")
    {
        include_once('../../model/atividadeEstatutoTipoClass.php');
        $aet = new AtividadeEstatutoTipo();
        $aet->setIdTipoAtividadeEstatuto($_REQUEST['id']);
        $aet->setStatusTipoAtividadeEstatuto($novo_status);
        $arr = $aet->alteraStatusTipoAtividadeEstatuto();
    }
    else if($_REQUEST['classe']=="atividadeIniciaticaColumba")
    {
        include_once('../../model/atividadeIniciaticaColumbaClass.php');
        $aicm = new atividadeIniciaticaColumba();
        $aicm->setIdAtividadeIniciaticaColumba($_REQUEST['id']);
        $aicm->setStatusAtividadeIniciaticaColumba($novo_status);
        $arr = $aicm->alteraStatusAtividadeIniciaticaColumba();
    }
    else if($_REQUEST['classe']=="imovelAnexoTipo")
    {
        include_once('../../model/imovelAnexoTipoClass.php');
        $iatm = new ImovelAnexoTipo();
        $iatm->setIdImovelAnexoTipo($_REQUEST['id']);
        $iatm->setStatusImovelAnexoTipo($novo_status);
        $arr = $iatm->alteraStatusImovelAnexoTipo();
    }
    else if($_REQUEST['classe']=="atividadeAgendaTipo")
    {
        include_once('../../model/atividadeAgendaTipoClass.php');
        $a = new AtividadeAgendaTipo();
        $a->setIdAtividadeAgendaTipo($_REQUEST['id']);
        $a->setStatusTipoAtividadeAgenda($novo_status);
        $arr = $a->alteraStatusTipoAtividadeAgenda();
    }
    else if($_REQUEST['classe']=="videoAula")
    {
        include_once('../../model/videoAulaClass.php');
        $v = new videoAula();
        $v->setIdVideoAula($_REQUEST['id']);
        $v->setStatus($novo_status);
        $arr = $v->alteraStatusVideoAula();
    }
    else if($_REQUEST['classe']=="historicoOA")
    {
        include_once('../../model/organismoHistoricoClass.php');
        $o = new organismoHistorico();
        $o->setIdOrganismoAfiliadoHistorico($_REQUEST['id']);
        $o->setStatusHistorico($novo_status);
        $arr = $o->alteraStatusHistorico();
    }
    else if($_REQUEST['classe']=="agendaAtividade")
    {
        include_once('../../model/agendaAtividadeClass.php');
        $a = new AgendaAtividade();
        $a->setIdAgendaAtividade($_REQUEST['id']);
        $a->setStatusAgenda($novo_status);
        $arr = $a->alteraStatusAgendaAtividade();
    }
    
    echo json_encode($arr);
}	
?>