<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/estadoClass.php');

    $e = new Estado();

    $resultado = $e->retornaIdUf($_REQUEST['uf']);

    $retorno = array();

    $retorno['idEstado'] = $resultado;
    
    echo json_encode($retorno);
}
?>