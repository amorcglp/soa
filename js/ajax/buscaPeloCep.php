<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    include_once('../../lib/correios/Correios.php');
    include_once('../../lib/functions.php');

    $cep = isset($_REQUEST['cep'])?$_REQUEST['cep']:null;
    $cep = str_replace(".","",str_replace("-","",trim($cep)));

    $correios = new Correios;
    $correios->retornaInformacoesCep($cep);
    //echo $correios->informacoesCorreios->getBairro();

    $retorno = array();

    $retorno['logradouro'] 		= converteEncoding($correios->informacoesCorreios->getLogradouro());
    $retorno['bairro']			= converteEncoding($correios->informacoesCorreios->getBairro());
    $retorno['cidade']			= converteEncoding($correios->informacoesCorreios->getLocalidade());
    $retorno['uf'] 				= $correios->informacoesCorreios->getUf();

    //echo "<pre>";print_r($retorno);
    echo json_encode($retorno);
}
?>