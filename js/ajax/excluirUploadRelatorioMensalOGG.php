<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/relatorioOGGMensalClass.php');

    $rfm = new RelatorioOGGMensal();
    
    $resultado = $rfm->removeRelatorioOGGMensal($_REQUEST['id']);

    $arr = array();
    if ($resultado) {
            $arr['status']=1;
    } else {
        $arr['status']=0;
    }
    echo json_encode($arr);
}
?>