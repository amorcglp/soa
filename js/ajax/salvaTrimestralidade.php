<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/trimestralidadeClass.php');

    $dataTrimestralidade	= isset($_REQUEST['dataTrimestralidade']) ? $_REQUEST['dataTrimestralidade'] : '';
    $valorTrimestralidade	= isset($_REQUEST['valorTrimestralidade']) ? $_REQUEST['valorTrimestralidade'] : '';
    $usuario				= isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $salvar					= isset($_REQUEST['salvar']) ? $_REQUEST['salvar'] : '';
    $idTrimestralidade		= isset($_REQUEST['idTrimestralidade']) ? $_REQUEST['idTrimestralidade'] : '';


    $arr=array();
    $arr['status']=0;


    //Tratar data
    $dataTrimestralidade = substr($dataTrimestralidade,6,4)."-".substr($dataTrimestralidade,3,2)."-".substr($dataTrimestralidade,0,2);

    $t = new trimestralidade();
    $t->setDataTrimestralidade($dataTrimestralidade);
    $t->setValorTrimestralidade($valorTrimestralidade);
    $t->setUsuario($usuario);

    if($salvar==0)
    {
            $retorno = $t->cadastroTrimestralidade();
    }else{
            $t->setUltimoAtualizar($usuario);
            $retorno = $t->atualizaTrimestralidade($idTrimestralidade);
    }

    if ($retorno) {
            $arr['status']=1;
    }


    echo json_encode($arr);
}	
?>