<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    include_once('../../model/ataReuniaoMensalAssinadaClass.php');

    $arma = new ataReuniaoMensalAssinada();

    $resultado = $arma->removeAtaReuniaoMensalAssinada($_REQUEST['id']);

    $arr = array();
    if ($resultado) {
            $arr['status']=1;
    } else {
        $arr['status']=0;
    }
    echo json_encode($arr);
}
?>