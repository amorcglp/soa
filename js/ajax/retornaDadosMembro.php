<?php


/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require '../sec/token.php';	
	}else{
		require './sec/token.php';
	}
}

if($tokenLiberado)
{  
    if(!class_exists("Ws"))
    {
        //require_once '../../lib/functions.php';
        if(realpath('../../model/wsClass.php')){
                require '../../model/wsClass.php';
        }else{
                if(realpath('../model/wsClass.php')){
                        require '../model/wsClass.php';	
                }else{
                        require './model/wsClass.php';
                }
        }
    }
    //xxxxxxxxxx
    if(!isset($codigoAfiliacao))
    {
            $codigoAfiliacao 		= isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    }
    if(!isset($nomeMembro))
    {
            $nomeMembro			= isset($_REQUEST['nomeMembro'])?$_REQUEST['nomeMembro']:"";
    }
    if(!isset($tipoMembro))
    {
            $tipoMembro			= isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"0";
    }
    if(!isset($filtro))
    {
            $filtro				= isset($_REQUEST['filtro'])?$_REQUEST['filtro']:null;
    }else{
            $filtro                         = null;
    }
    $ocultar_json 			= isset($ocultar_json)?$ocultar_json:0;
    $arrNome				= explode(" ",$nomeMembro);


    $server = isset($server) ? $server : null;

    // Instancia a classe
    $ws = new Ws($server);

    // Nome do Método que deseja chamar
    $method = 'RetornaDadosMembro';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'CodMembro' => $codigoAfiliacao,
                                    'TipoMembro' => $tipoMembro,
                                    'Nome' => urlencode($arrNome[0]));

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');
    /*
    //Verificar novamente agora passando o primeiro sobrenome
    if(isset($return)&&!isset($return->error))
    {
            if(($return->result['0']->fields->fSeqCadast==0)
                    &&(count($arrNome)>1))
            {
                    // Parametros que serão enviados à chamada
                    $params = array('CodUsuario' => 'lucianob',
                                                    'CodMembro' => $codigoAfiliacao,
                                                    'TipoMembro' => $tipoMembro,
                                                    'Nome' => $arrNome[1]);

                    // Chamada do método
                    $return = $ws->callMethod($method, $params, 'lucianob');
            }

            //Verificar novamente passando o segundo sobrenome
            if($return->result['0']->fields->fSeqCadast==0
                    &&count($arrNome)>2)
            {
                    // Parametros que serão enviados à chamada
                    $params = array('CodUsuario' => 'lucianob',
                                                    'CodMembro' => $codigoAfiliacao,
                                                    'TipoMembro' => $tipoMembro,
                                                    'Nome' => $arrNome[2]);

                    // Chamada do método
                    $return = $ws->callMethod($method, $params, 'lucianob');
            }
    }*/
    if($ocultar_json==0)
    {
            // Imprime o retorno
            if($filtro==null)
            {    
                echo json_encode($return);
            }else{
                if($filtro=="externo")
                {
                    $var = $return->result[0];
                    //unset($var->fields->fSeqCadast);
                    unset($var->fields->fIdeTipoPessoa);
                    //unset($var->fields->fNomCliente);
                    unset($var->fields->fNomConjuge);
                    unset($var->fields->fIdeSexo);
                    unset($var->fields->fDatNascimento);
                    unset($var->fields->fCodCpf);
                    unset($var->fields->fCodRg);
                    unset($var->fields->fDesOrgaoEmissao);
                    unset($var->fields->fCodCnpj);
                    unset($var->fields->fCodInscricaoEstadual);
                    unset($var->fields->fCodEstadoCivil);
                    unset($var->fields->fCodGrauInstrucao);
                    unset($var->fields->fSeqOcupacao);
                    unset($var->fields->fSeqEspecializacao);
                    unset($var->fields->fSeqFormacao);
                    unset($var->fields->fCodCliente);
                    unset($var->fields->fCodNaoMembro);
                    unset($var->fields->fCodRosacruz);
                    unset($var->fields->fCodOgg);
                    unset($var->fields->fCodAma);
                    unset($var->fields->fCodAssinanteRosacruz);
                    unset($var->fields->fCodAssinanteAmorcCultural);
                    unset($var->fields->fCodAssinanteOrdemJuvenil);
                    unset($var->fields->fCodArtista);
                    unset($var->fields->fCodPalestrante);
                    unset($var->fields->fCodLeitor);
                    unset($var->fields->fSigOA);
                    unset($var->fields->fIdeTipoOa);
                    unset($var->fields->fIdeTipoTom);
                    unset($var->fields->fNomLogradouro);
                    unset($var->fields->fNumEndereco);
                    unset($var->fields->fDesComplementoEndereco);
                    unset($var->fields->fNomBairro);
                    unset($var->fields->fNomCidade);
                    unset($var->fields->fSigUf);
                    unset($var->fields->fCodCepEndereco);
                    //unset($var->fields->fSigPaisEndereco);
                    unset($var->fields->fNumCaixaPostal);
                    unset($var->fields->fCodCepCaixaPostal);
                    unset($var->fields->fIdeEnderecoCorrespondencia);
                    unset($var->fields->fNumTelefone);
                    unset($var->fields->fNumFax);
                    unset($var->fields->fNumTelefoFixo);
                    unset($var->fields->fNumCelula);
                    unset($var->fields->fNumOutroTelefo);
                    unset($var->fields->fDesOutroTelefo);
                    //unset($var->fields->fDesEmail);
                    unset($var->fields->fDatImplantacaoCadastro);
                    unset($var->fields->fDatCadastro);
                    unset($var->fields->fCodUsuarioCadastro);
                    unset($var->fields->fIdeTipoFiliacRosacruz);
                    unset($var->fields->fSeqCadastPrincipalRosacruz);
                    unset($var->fields->fSeqCadastCompanheiroRosacruz);
                    unset($var->fields->fIdeTipoFiliacTom);
                    unset($var->fields->fSeqCadastPrincipalTom);
                    unset($var->fields->fSeqCadastCompanheiroTom);
                    unset($var->fields->fIdeContribuicaoCarne);
                    unset($var->fields->fCodRosacruzCompanheiro);
                    unset($var->fields->fIdeTipoMoeda);
                    unset($var->fields->fIdeModoFaturamento);
                    unset($var->fields->fIdeDependenciaAdministrativa);
                    unset($var->fields->fDesObservacao);
                    unset($var->fields->fIdeTipoSituacMembroRosacr);
                    unset($var->fields->fIdeTipoSituacMembroOjp);
                    unset($var->fields->fIdeTipoSituacMembroTom);
                    unset($var->fields->fIdeTipoSituacMembroAma);
                    unset($var->fields->fCodDesligRosacr);
                    unset($var->fields->fCodDesligOjp);
                    unset($var->fields->fCodDesligTom);
                    unset($var->fields->fCodDesligAma);
                    unset($var->fields->fDatDesligRosacr);
                    unset($var->fields->fDatDesligOjp);
                    unset($var->fields->fDatDesligTom);
                    unset($var->fields->fDatDesligAma);
                    unset($var->fields->fIdeTipoSituacRemessRosacr);
                    unset($var->fields->fIdeTipoSituacRemessOjp);
                    unset($var->fields->fIdeTipoSituacRemessTom);
                    unset($var->fields->fIdeTipoSituacRemessAma);
                    unset($var->fields->fDatQuitacRosacr);
                    unset($var->fields->fDatQuitacOjp);
                    unset($var->fields->fDatQuitacTom);
                    unset($var->fields->fDatQuitacAma);
                    unset($var->fields->fNumLoteAtualRosacr);
                    unset($var->fields->fNumLoteAtualOjp);
                    unset($var->fields->fNumLoteAtualTom);
                    unset($var->fields->fNumLoteAtualAma);
                    unset($var->fields->fNumLoteLimiteRosacr);
                    unset($var->fields->fNumLoteLimiteTom);
                    unset($var->fields->fSeqCadastOaAtuacaoRosacr);
                    unset($var->fields->fSigOaAtuacaoRosacr);
                    unset($var->fields->fSeqCadastOaAtuacaoTom);
                    unset($var->fields->fSigOaAtuacaoTom);
                    unset($var->fields->fSeqCadastOaAtuacaoRosacrCEP);
                    unset($var->fields->fSigOaAtuacaoRosacrCEP);
                    unset($var->fields->fSeqCadastOaAtuacaoTomCEP);
                    unset($var->fields->fSigOaAtuacaoTomCEP);
                    unset($var->fields->fIdeAceitoContato);
                    unset($var->fields->fIdePeriodoContato);
                    unset($var->fields->fIdeFormaContatoIdeal);
                    unset($var->fields->fCodTipoMoeda);
                    unset($var->fields->fIdePossuiComputador);
                    unset($var->fields->fIdePossuiAcessoInternet);
                    unset($var->fields->fDesejaColaborarComOANasEquipes);
                    unset($var->fields->fExperienciaProfissionalResumida);
                    unset($var->fields->fIdeMembroURCI);
                    unset($var->fields->fDatAdmissRosacr);
                    unset($var->fields->fDatAdmissOjp);
                    unset($var->fields->fDatAdmissTom);
                    unset($var->fields->fDatAdmissAma);
                    unset($var->fields->fDatAdmissCompanRosacr);
                    unset($var->fields->fDatAdmissCompanTom);
                    unset($var->fields->fIdeAmorcgDigita);
                    unset($var->fields->fIdeForumrDigita);
                    unset($var->fields->fIdeOrosacDigita);
                    unset($var->fields->fIdeOpentaDigita);
                    unset($var->fields->fIdeOGGDigita);

                    unset($var->fields->fIdeOpcaoRemessRosacr);
                    unset($var->fields->fIdeOpcaoRemessOjp);
                    unset($var->fields->fIdeOpcaoRemessTom);
                    unset($var->fields->fIdeOpcaoRemessAma);
                    //unset($var->fields->fDesMensag);
                }    
                echo json_encode($var);
            }
    }
    
}
?>