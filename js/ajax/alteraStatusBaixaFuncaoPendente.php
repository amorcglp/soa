<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $novo_status = ($_REQUEST['status']==1) ? 0 : 1;

    $arr=array();

    include_once('../../model/baixaAutomaticaFuncoesUsuarioClass.php');
    $parm = new baixaAutomaticaFuncoesUsuario();
    if($parm->atualizaBaixaFuncaoPendente($_REQUEST['seqCadast'],$novo_status,$_REQUEST['funcao'],$_REQUEST['siglaOA'],$_REQUEST['dataEntrada'],$_REQUEST['dataTerminoMandato'])){
        $arr["status"]=1;
    }else{
        $arr["status"]=0;
    }

    echo json_encode($arr);
}	
?>