<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/planoAcaoRegiaoMetaClass.php');

    $parm = new planoAcaoRegiaoMeta();
    //echo "id=>".$_REQUEST['id'];
    $resultado = $parm->listaMeta(null,null,$_REQUEST['id']);

    $arr=array();
    if($resultado)
    {
            if (count($resultado)>0) {

                    foreach($resultado as $vetor)
                    {
                            $arr['tituloMeta'] = $vetor['tituloMeta'];
                            $arr['oque'] = $vetor['oque'];
                            $arr['porque'] = $vetor['porque'];
                            $arr['quem'] = $vetor['quem'];
                            $arr['onde'] = $vetor['onde'];
                            $arr['quando'] = $vetor['quando'];
                            $arr['como'] = $vetor['como'];
                            $arr['quanto'] = $vetor['quanto'];
                    }

            }
    } else {
       $arr['erro'] = "Erro!";
    }

    echo json_encode($arr);
}
?>