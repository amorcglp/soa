<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idAtividadeEstatuto		= isset($_POST['idAtividadeEstatuto']) ? addslashes($_POST['idAtividadeEstatuto']) : '';
    $codigoAfiliacao			= isset($_POST['codigoAfiliacao']) ? addslashes($_POST['codigoAfiliacao']) : '';
    $nomeMembro					= isset($_POST['nomeMembro']) ? addslashes($_POST['nomeMembro']) : '';
    $seqCadast					= isset($_POST['seqCadast']) ? addslashes($_POST['seqCadast']) : '';

    include_once("../../model/atividadeEstatutoClass.php");
    $aec = new atividadeEstatuto();

    $retorno=array();

    $verificado = $aec->verificaOficialResponsavelAtividade($idAtividadeEstatuto, $seqCadast);

    if(!$verificado){

            $resultado = $aec->incluiOficialResponsavelAtividade($idAtividadeEstatuto, $codigoAfiliacao, $nomeMembro, $seqCadast);

            if ($resultado>0) {
                    $retorno['sucesso'] = true;
            }
            else {
                    $retorno['sucesso'] = false;
                    $retorno['erro'] = 'Erro ao incluir o oficial';
            }
    } else {
            $retorno['sucesso'] = false;
            $retorno['erro'] = 'Oficial já cadastrado como responsável dessa atividade!';
    }
    $retorno['idOficialResponsavel'] = $seqCadast;

    echo json_encode($retorno);
}
?>