<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idEmail 		= isset($_REQUEST['idEmail'])?$_REQUEST['idEmail']:null;
    $seqCadast 		= isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;

    $retorno=array();
    $retorno['status']=0;

    include_once('../../model/emailDeParaClass.php');

    $edp = new EmailDePara();
    $resultado = $edp->moverParaLixeira($idEmail, $seqCadast);

    if($resultado)
    {
            $retorno['status']=1;
    }

    echo json_encode($retorno);
}
	
?>