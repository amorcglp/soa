<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $seqCadastMembro 	        = isset($_REQUEST['seqCadastMembro'])?$_REQUEST['seqCadastMembro']:'';
    $idAtividadeIniciatica 	    = isset($_REQUEST['idAtividadeIniciatica'])?$_REQUEST['idAtividadeIniciatica']:'';
    $nomeMembro 	            = isset($_REQUEST['nomeMembro'])?$_REQUEST['nomeMembro']:'';

    include_once('../../model/atividadeIniciaticaClass.php');
    $aim = new atividadeIniciatica();

    $retorno = array();

    if ($idAtividadeIniciatica!=''){

        $vagas = $aim->retornaQntVagasDisponiveis($idAtividadeIniciatica);
        $vagasDisponiveis = 12 - $vagas;

        if($vagasDisponiveis > 0){
            $resultadoEncontrado = $aim->verificaMembroNaAtividadeIniciatica($seqCadastMembro,$idAtividadeIniciatica);

            if ($resultadoEncontrado==true) {
                $resultado = $aim->alteraMembroAtividadeIniciatica($seqCadastMembro, $idAtividadeIniciatica);
            } else {
                $resultado = $aim->cadastraMembroAtividadeIniciatica($seqCadastMembro, $idAtividadeIniciatica, $nomeMembro);
            }

            if ($resultado==true) {
                $retorno['sucesso'] = true;
            } else {
                $retorno['sucesso'] = false;
                $retorno['erro'] = 'Ocorreu algum erro ao cadastrar o membro. Tente novamente ou entre em contato com o setor de TI';
            }
        } else {
            $retorno['sucesso'] = false;
            $retorno['erro'] = 'Essa atividade está com o número máximo de pessoas agendadas!';
        }

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Marque alguma das datas para realizar as Iniciações!';
    }

    echo json_encode($retorno);
}
?>