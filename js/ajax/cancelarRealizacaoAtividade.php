<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idAtividade 	= isset($_REQUEST['idAtividade']) ? $_REQUEST['idAtividade']  : '';

    $arr=array();

    include_once('../../model/atividadeEstatutoClass.php');
    $am = new AtividadeEstatuto();
    $retorno = $am->alteraStatusAtividadeCancelada($idAtividade);

    $arr['sucesso'] = $retorno;

    echo json_encode($arr);
}
?>
