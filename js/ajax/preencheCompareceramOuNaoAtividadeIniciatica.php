<?php
@include_once("../../lib/functions.php");

include_once("../../model/atividadeIniciaticaClass.php");
$aim = new atividadeIniciatica();

$idAtividadeIniciatica				= isset($_REQUEST['idAtividadeIniciatica'])?$_REQUEST['idAtividadeIniciatica']:"";

$resultadoMembros = $aim->listaAtividadeIniciaticaMembros($idAtividadeIniciatica);

$arr = array();
$arr['status']   = 0;

if ($resultadoMembros) {
    foreach ($resultadoMembros as $vetorMembros) {
        $statusMembro = $vetorMembros['statusAtividadeIniciaticaMembro'];
        $arr['status']=1;
        switch($statusMembro){
            case 0: $arr['convidados'][]=$vetorMembros['seqCadastAtividadeIniciaticaMembro']; break;
            case 1: $arr['confirmados'][]=$vetorMembros['seqCadastAtividadeIniciaticaMembro']; break;
            case 2: $arr['compareceu'][]=$vetorMembros['seqCadastAtividadeIniciaticaMembro']; break;
            case 3: $arr['naoCompareceu'][]=$vetorMembros['seqCadastAtividadeIniciaticaMembro']; break;
            default: $arr['outros'][]=$vetorMembros['seqCadastAtividadeIniciaticaMembro']; break;
        }


    }
}
echo json_encode($arr);
?>

