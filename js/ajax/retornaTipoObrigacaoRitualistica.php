<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    }

    $ocultar_json  = isset($ocultar_json)?$ocultar_json:0;
    $seqTipoObriga = isset($seqTipoObriga)?$seqTipoObriga:0;
    $ideTipoMembro = isset($ideTipoMembro)?$ideTipoMembro:0;

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaTiposObrigacoesRitualisticas';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SeqTipoObriga' => $seqTipoObriga,
                                    'IdeTipoMembro' => $ideTipoMembro);

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');
    if($ocultar_json==0)
    {
        // Imprime o retorno
        echo json_encode($return);
    }
}
?>