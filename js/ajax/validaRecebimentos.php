<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $dataRecebimento			= isset($_REQUEST['dataRecebimento']) ? $_REQUEST['dataRecebimento'] : '';
    $fk_idOrganismoAfiliado		= isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';
    $recebemosDe		        = isset($_REQUEST['recebemosDe']) ? $_REQUEST['recebemosDe'] : '';
    $valorRecebimento		    = isset($_REQUEST['valorRecebimento']) ? $_REQUEST['valorRecebimento'] : '';

    //Tratar data
    $dataVerificacao = substr($dataRecebimento,6,4)."-".substr($dataRecebimento,3,2)."-".substr($dataRecebimento,0,2);
    $mesAtual =substr($dataRecebimento,3,2);
    $anoAtual=substr($dataRecebimento,6,4);

    $arr=array();
    $arr['status']=0;

    include_once('../../model/recebimentoClass.php');

    $r = new Recebimento();
    $r->setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado);

    if($r->validaRecebimentos($dataRecebimento,$fk_idOrganismoAfiliado,$recebemosDe,$valorRecebimento))
    {
        $arr['status']=true;
    }else{
        $arr['status']=false;
    }
    echo json_encode($arr);
}	
?>