<?php

$db = include('../../firebase.php');

/*
 * Token
 */

$includePath = [
    '../../sec/token.php',
    '../sec/token.php',
    './sec/token.php'
];

foreach($includePath as $path) {
    if(realpath($path)) {
        require_once $path;
        break;
    }
}

if($tokenLiberado)
{
    $idString		 			= isset($_REQUEST['id']) ? strval($_REQUEST['id']) : '';

    //Pegar a data e o id do organismo
    include_once('../../model/despesaClass.php');
    $d = new Despesa();
    $resultadoInformacoesSaldo = $d->buscarIdDespesa($_REQUEST['id']);
    if($resultadoInformacoesSaldo)
    {
        foreach ($resultadoInformacoesSaldo as $is)
        {
            $dataDespesa = $is['dataDespesa'];
            $fk_idOrganismoAfiliado = $is['fk_idOrganismoAfiliado'];
        }
    }

    include_once('../../lib/functions.php');
    include_once('../../model/saldoInicialClass.php');
    include_once('../../model/recebimentoClass.php');
    include_once('../../model/despesaClass.php');

    $si = new saldoInicial();
    $r = new Recebimento();
    $d = new Despesa();

    $mes = substr($dataDespesa,5,2);
    $ano = substr($dataDespesa,0,4);

    //Exclusão da Despesa

    $resultado2 = $d->selecionaUltimoId();
    if($resultado2)
    {
            foreach ($resultado2 as $vetor2)
            {
                    $proximo_id = $vetor2['Auto_increment'];
            }
    }

    $valorDespesa=0;
    $resultado = $d->buscarIdDespesa($_REQUEST['id']);
    if ($resultado) {
        foreach ($resultado as $v) {
            $valorDespesa = floatval($v['valorDespesa']);
            $dataDespesa = $v['dataDespesa'];
            $fk_idOrganismoAfiliado = $v['fk_idOrganismoAfiliado'];
        }
    }

    $resultado = $d->removeDespesa($_REQUEST['id']);

    $arr['status']=0;

    if ($resultado) {
        $arr['status']=1;
        $arr['proximo_id']=$proximo_id;
    } else {
        $arr['erro'][] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    /*
     * Firebase
     */

    //Busca pelo id do Documento no Firebase

//    $despesa = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/despesas");
//
//    $queryWhereD = $despesa
//        ->where('idDespesaSOA', '=', $idString);
//
//    $queryDespesa = $queryWhereD->documents();
//
//    //Percorrer recebimentos
//    foreach ($queryDespesa as $documentDes) {
//        if ($documentDes->exists()) {
//
//            $idRefDocumentoFirebase = $documentDes->id();
//            $db->collection("balances/" . $fk_idOrganismoAfiliado . "/despesas")->document($idRefDocumentoFirebase)->delete();
//
//            $arr['idRefDocumentoFirebase'] = $idRefDocumentoFirebase;
//        }
//    }


    echo json_encode($arr);
}
?>