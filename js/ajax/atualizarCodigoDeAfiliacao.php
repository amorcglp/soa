<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $codigoDeAfiliacao = isset($_REQUEST['codigoDeAfiliacaoAtual'])?$_REQUEST['codigoDeAfiliacaoAtual']:null;
    $seqCadast = isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;

    include_once('../../model/usuarioClass.php');
    require_once '../../webservice/wsInovad.php';

    $u = new Usuario();

    //Popular funções do usuário
    $array = [0 => $seqCadast];
    $teste = '';
    foreach ($array as $key => $value) {
        if ($teste != '') {
            $teste = $teste . ', ' . $value;
        } else {
            $teste = $value;
        }
    }

    //Buscar Funções
    $vars = array('seq_cadast' => '[' . $teste . ']');
    $resposta20 = json_decode(json_encode(restAmorc("membros/oficiais", $vars)), true);
    $obj = json_decode(json_encode($resposta20), true);
    //echo "<pre>";print_r($obj);exit();

    $retorno=array();
    $retorno['status']=0;
    $retorno['status']=0;

    if (isset($obj['data'][0])) {

        //Atualizar Código de Afiliacao se preciso
        if ($codigoDeAfiliacao != $obj['data'][0]['cod_rosacr']) {
            $u = new Usuario();
            $u->atualizaCodigoDeAfiliacaoUsuario($seqCadast, $obj['data'][0]['cod_rosacr']);
            $retorno['status']=1;
            $retorno['codigoDeAfiliacao']=$obj['data'][0]['cod_rosacr']."<br><br>
                                                            <a href=# class=\"btn btn-sm btn-info\" onclick=\"atualizarCodigoDeAfiliacao('$seqCadast',".$obj['data'][0]['cod_rosacr'].");\">Atualizar</a>";
        }else{
            $retorno['status']=2;
        }
    }

    echo json_encode($retorno);
}
?>