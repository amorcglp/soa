<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $id 	        = isset($_REQUEST['id']) ? $_REQUEST['id']  : '';
    $quemEntregou 	= isset($_REQUEST['quemEntregou']) ? $_REQUEST['quemEntregou']  : '';

    $arr=array();

    include_once('../../lib/functions.php');
    include_once('../../model/relatorioGrandeConselheiroClass.php');
    $rgc = new relatorioGrandeConselheiro();
    $resultado = $rgc->buscarIdRelatorioGrandeConselheiro($id);
    if($resultado)
    {
        foreach ($resultado as $vetor)
        {
            if(trim($vetor['numeroAssinatura'])!="")
            {
                $numeroAssinatura = $vetor['numeroAssinatura'];
                $temNumeroAssinatura=true;
            }else{
                $numeroAssinatura = aleatorioAssinatura();
                $rgc = new relatorioGrandeConselheiro;
                $rgc->setIdRelatorioGrandeConselheiro($id);
                $rgc->atualizaNumeroAssinatura($numeroAssinatura);
            }

        }
    }

    $retorno = $rgc->entregar($id,$quemEntregou);

    $arr['status'] = 0;
    $arr['dataEntrega'] = "";

    if ($retorno) {

        $arr['status'] = 1;
        //Puxar data da entrega
        $retornoData = $rgc->buscarIdRelatorioGrandeConselheiro($id);
        if($retornoData)
        {
            foreach ($retornoData as $v)
            {
                $dataEntrega = substr($v['dataEntrega'],0,10);
                $arr['dataEntrega'] = substr($dataEntrega,8,2)."/".substr($dataEntrega,5,2)."/".substr($dataEntrega,0,4);
            }
        }

    }

    echo json_encode($arr);
}	
?>