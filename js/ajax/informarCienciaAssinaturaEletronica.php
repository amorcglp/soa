<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $login 	= isset($_REQUEST['login']) ? $_REQUEST['login']  : '';

    $arr=array();

    include_once('../../model/usuarioClass.php');
    $u = new Usuario();
    if($u->verificaLoginExistente($login)){

        if(!$u->verificaSeJaCienteDaAssinaturaEletronica($login)) {

            //echo "verificou que existe o login:".$login;
            if ($u->cienteAssinaturaPorLogin($login)) {
                //echo "tornou o login ciente:".$login;
                $arr['status'] = 1;
            } else {
                $arr['status'] = 4;
            }
        }else{
            $arr['status'] = 3;
        }

    }else{
        $arr['status'] = 2;
    }



    echo json_encode($arr);
}	
?>