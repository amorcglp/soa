<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            require_once './model/wsClass.php';
    } 

    $seqCadast 	= isset($_REQUEST['seq_cadast'])?$_REQUEST['seq_cadast']:0;

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaRelacaoOficiais';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'SeqCadast' => $seqCadast,
                                    'SeqFuncao' => 0,
                                    'NomParcialFuncao' => '',
                                    'NomLocaliOficial' => '',
                                    'SigRegiaoBrasilOficial' => '',
                                    'SigPaisOficial' => 'BR',
                                    'SigOrganismoAfiliado' => '',
                                    'SigAgrupamentoRegiao' => '',
                                    'IdeListarAtuantes' => 'S',
                                    'IdeListarNaoAtuantes' => 'N',
                                    'IdeTipoMembro' => '1'
                            );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    $resposta = array();
    $h=0;
    foreach($return->result[0]->fields->fArrayOficiais as $vetor)
    {
            if($vetor->fields->fDatSaida==0)
            {
                    $descFuncao[$h]['codigoFuncao']=$vetor->fields->fCodFuncao;
                    $descFuncao[$h]['dataInicioMandato']=$vetor->fields->fDatEntrad;
                    $descFuncao[$h]['dataFimMandato']=$vetor->fields->fDatTerminMandat;
                    $descFuncao[$h]['siglaOA']=$vetor->fields->fSigOrgafi;
                    $h++;
            }
    }

    // Pegar id da(s) Função(oes) atual(is) e inserir na tabela funcao_usuario
    include_once('../../model/funcaoClass.php');
    $f = new funcao();
    include_once('../../model/funcaoUsuarioClass.php');
    //echo "<pre>";print_r($descFuncao);
    if($descFuncao){
            foreach ($descFuncao as $vetor)
            {
                    $f->setVinculoExterno($vetor['codigoFuncao']);
                    $retorno = $f->buscaVinculoExterno();
                    foreach ($retorno as $vetor2)
                    {
                            //echo "<br>idfuncao:".$vetor2['idFuncao'];
                            $fu = new funcaoUsuario();
                            $fu->setFk_idFuncao($vetor2['idFuncao']);
                            $fu->setFk_seq_cadast($seqCadast);
                            $fu->setDataInicioMandato($vetor['dataInicioMandato']);
                            $fu->setDataFimMandato($vetor['dataFimMandato']);
                            $fu->setSiglaOA($vetor['siglaOA']);
                            if(!$fu->verificaSeJaExiste())
                            {
                                    $fu->cadastra();
                            }
                            $resposta['status']=1;
                    }

            }
    } else {
            $resposta['status']=0;
    }

    echo json_encode($resposta);
}
?>