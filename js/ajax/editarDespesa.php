<?php

//$db = include('../../vendor/autoload.php');

function retornaCategoriaDespesa2($cod) {
    switch ($cod) {
        case 1:
            return "Aluguel";
            break;
        case 2:
            return "Comissões";
            break;
        case 3:
            return "Luz";
            break;
        case 4:
            return "Água";
            break;
        case 5:
            return "Telefone";
            break;
        case 6:
            return "Tarifas";
            break;
        case 7:
            return "Manutenção";
            break;
        case 8:
            return "Beneficiência Social";
            break;
        case 9:
            return "Boletim";
            break;
        case 10:
            return "Convenções";
            break;
        case 11:
            return "Jornadas";
            break;
        case 12:
            return "Reuniões Sociais";
            break;
        case 13:
            return "Despesas de Correio";
            break;
        case 14:
            return "Anúncios";
            break;
        case 15:
            return "Carta Constitutiva (GLP)";
            break;
        case 16:
            return "Cantina";
            break;
        case 17:
            return "Despesas Gerais";
            break;
        case 18:
            return "GLP - Remessa Trimestralidade";
            break;
        case 19:
            return "GLP - Pagamentos Suprimentos";
            break;
        case 20:
            return "GLP - Outros";
            break;
        case 21:
            return "Investimentos";
            break;
        case 22:
            return "Impostos";
            break;
        case 23:
            return "Região";
            break;
        default:
            return "Não identificada";
            break;
    }
}

/*
 * Token
 */

$includePath = [
    '../../sec/token.php',
    '../sec/token.php',
    './sec/token.php'
];

foreach($includePath as $path) {
    if(realpath($path)) {
        require_once $path;
        break;
    }
}

if($tokenLiberado)
{ 

    $id				 	    	= isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
//    $idString		 			= isset($_REQUEST['id']) ? strval($_REQUEST['id']) : '';
    $dataDespesaJS 		    	= isset($_REQUEST['dataDespesa']) ? $_REQUEST['dataDespesa'] : '';
    $descricaoDespesa		    = isset($_REQUEST['descricaoDespesa']) ? $_REQUEST['descricaoDespesa'] : '';
    $pagoA					    = isset($_REQUEST['pagoA']) ? $_REQUEST['pagoA'] : '';
    $valorDespesa			    = isset($_REQUEST['valorDespesa']) ? $_REQUEST['valorDespesa'] : '';
    $categoriaDespesa		    = isset($_REQUEST['categoriaDespesa']) ? $_REQUEST['categoriaDespesa'] : '';
    $usuario                    = isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $fk_idOrganismoAfiliado     = isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';

    //Tratar data
    $dataDespesa = substr($dataDespesaJS,6,4)."-".substr($dataDespesaJS,3,2)."-".substr($dataDespesaJS,0,2);

    $arr=array();
    $arr['status']=0;

    include_once('../../lib/functions.php');
//    include_once('../../model/saldoInicialClass.php');
//    include_once('../../model/recebimentoClass.php');
    include_once('../../model/despesaClass.php');

//    $si = new saldoInicial();
//    $r = new Recebimento();
//    $d = new Despesa();

    //Tratamentos da Data e do Id do Oa para o Firebase

//    $mes = intval(substr($dataDespesa,5,2));
//    $ano = intval(substr($dataDespesa,0,4));
//
//    $mesString = strval(substr($dataDespesa,5,2));
//    $anoString = strval(substr($dataDespesa,0,4));
//
//    $fk_idOrganismoAfiliadoString = strval($fk_idOrganismoAfiliado);
//
//    $mesInt = intval(substr($dataDespesa,5,2));
//    $anoInt = intval(substr($dataDespesa,0,4));

    //Tratamento do valor do recebimento para enviar para o firebase
//    $valorDespesaFloat = floatval(str_replace(",", ".", str_replace(".", "", $valorDespesa)));
//    $valorDespesaString = strval($valorDespesaFloat);

    /*
     * Firebase
     */

//    //Busca pelo id do Documento no Firebase
//
//    $despesas = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/despesas");
//
//    $queryWhereD = $despesas
//        ->where('idDespesaSOA', '=', $_REQUEST['id']);
//
//    $queryDespesas = $queryWhereD->documents();
//
//    $arrDes=array();
//
//    //Percorrer recebimentos
//    foreach ($queryDespesas as $documentDes) {
//        if ($documentDes->exists()) {
//
//            $idRefDocumentoFirebase = $documentDes->id();
//
//            /*
//             * Modelo Despesa Firebase
//             *
//             * 'fk_idOrganismoAfiliado' => $idOrganismoAfiliado,
//                                'mes' => $mes,
//                                'ano' => $ano,
//                                'idDespesaSOA' => $v['idDespesa'],
//                                'categoriaDespesa' => $v['categoriaDespesa'],
//                                'dataDespesa' => $v['dataDespesa'] . " 00:00:00",
//                                'descricaoDespesa' => $v['descricaoDespesa'],
//                                'pagoA' => $v['pagoA'],
//                                'ultimoAtualizar' => $v['ultimoAtualizar'],
//                                'valorDespesa' => $v['valorDespesa'],//Fazer conversão para float
//                                'userDespesas' => $v['usuario'],
//                                'dataCadastro' => $v['dataCadastro'],
//                                'balance' => 1,
//                                'updateData' => date('d/m/Y H:i:s')
//             */
//
//            $arrDes = $documentDes->data();
//            $arrDes['categoriaDespesa'] = $categoriaDespesa;
//            $arrDes['dataDespesa'] = $dataDespesaJS." 00:00:00";
//            $arrDes['descricaoDespesa'] = $descricaoDespesa;
//            $arrDes['ultimoAtualizar'] = $usuario;
//            $arrDes['valorDespesa'] = $valorDespesaString;
//            $arrDes['pagoA'] = $pagoA;
//            $arrDes['balance'] = 0;
//            $arrDes['mes'] = $mesString;
//            $arrDes['ano'] = $anoString;
//            $arrDes['updateData'] = date('d/m/Y H:i:s');
//
//            //echo "<pre>";print_r($arrDes);
//
//            $docRef2 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/despesas")->document($idRefDocumentoFirebase);
//            $docRef2->set($arrDes);
//
//
//
//        }
//    }
//
//    $arr['idRefDocumentoFirebase'] = $idRefDocumentoFirebase;

    //Edição da Despesa

    $d = new Despesa();
    $d->setDataDespesa($dataDespesa);
    $d->setDescricaoDespesa($descricaoDespesa);
    $d->setPagoA($pagoA);
    $d->setValorDespesa($valorDespesa);
    $d->setCategoriaDespesa($categoriaDespesa);
    $d->setUltimoAtualizar($usuario);
    $d->setIdRefDocumentoFirebase("");
    $retorno = $d->alteraDespesa($id);

    if ($retorno) {
            $arr['categoriaDespesa']=retornaCategoriaDespesa2($categoriaDespesa);
            $arr['status'] = 1;
    }

    echo json_encode($arr);
}	
?>