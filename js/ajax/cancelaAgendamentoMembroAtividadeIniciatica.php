<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $seqCadastMembro 	        = isset($_REQUEST['seqCadastMembro'])?$_REQUEST['seqCadastMembro']:'';
    $idAtividadeIniciatica 	    = isset($_REQUEST['idAtividadeIniciatica'])?$_REQUEST['idAtividadeIniciatica']:'';

    include_once('../../model/atividadeIniciaticaClass.php');
    $aim = new atividadeIniciatica();

    $retorno = array();
    $resultado = $aim->cancelaAgendamentoMembroAtividadeIniciatica($seqCadastMembro,$idAtividadeIniciatica);

    if ($resultado==true) {
        $retorno['sucesso'] = true;
    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao confirmar o membro. Tente novamente ou entre em contato com o setor de TI';
    }

    echo json_encode($retorno);
}
?>