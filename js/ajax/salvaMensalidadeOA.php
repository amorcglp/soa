<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/mensalidadeOAClass.php');

    $camposGerais 			= isset($_REQUEST['camposGerais']) ? json_decode($_REQUEST['camposGerais']) : '';
    $seqCadast				= isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';
    $semestre				= isset($_REQUEST['semestre']) ? $_REQUEST['semestre'] : '';
    $ano					= isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual'] : '';
    $usuario				= isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    $fk_idOrganismoAfiliado	= isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : '';


    $arr=array();
    $arr['status']=0;

    $arrDataPagamento= array();
    for ($i=0;$i<count($camposGerais->datas);$i++)
    {
            $arrDataPagamento[] = $camposGerais->datas[$i];
    }
    $k=0;
    if($semestre==1)
    {
            $inicio=1;
            $fim=6;
    }else{
            $inicio=7;
            $fim=12;
    }
    for($i=$inicio;$i<=$fim;$i++)
    {
            //Tratar data
            $dataPagamento = substr($arrDataPagamento[$k],6,4)."-".substr($arrDataPagamento[$k],3,2)."-".substr($arrDataPagamento[$k],0,2);

            $m = new mensalidadeOA();
            $m->setFk_idOrganismoAfiliado($fk_idOrganismoAfiliado);
            $m->setSeqCadastMembroOa($seqCadast);
            $m->setDataPagamento($dataPagamento);
            $m->setMes($i);
            $m->setAno($ano);
            $m->setSemestre($semestre);
            $m->setUsuario($usuario);
            if(!$m->verificaSeJaCadastrado($seqCadast, $i, $semestre, $ano, $fk_idOrganismoAfiliado))
            {
                    $retorno = $m->cadastroMensalidadeOa();
            }else {
            $m->setUltimoAtualizar($usuario);
                    $retorno = $m->atualizaMensalidadeOa();
            }

            if ($retorno) {
                    $arr['status']=1;
            }
            $k++;
    }

    echo json_encode($arr);
}	
?>