<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idImovel 	            = isset($_POST['idImovel']) ? addslashes($_POST['idImovel']) : '';
    $idImovelAnexoTipo 	    = isset($_POST['idImovelAnexoTipo']) ? addslashes($_POST['idImovelAnexoTipo']) : '';

    include_once("../../model/imovelClass.php");
    $ic = new imovel();

    $resultado = $ic->listaImovelAnexo($idImovel,$idImovelAnexoTipo);

    $retorno['success'] = true;
    $retorno['sucesso'] = 'Anexo carregado com sucesso!';
    $retorno['arquivo'] = array();
    if ($resultado) {
            $retorno['temAnexo'] = 1;
            foreach ($resultado as $arquivos) {

                    //echo $arquivos['full_path'];

                    switch($arquivos['ext']) {

                            case 'pdf':
                                    $img = 'img/icon_foto/pdf.png';
                            break;

                            case 'jpg':
                                    $img = 'img/icon_foto/jpg.png';
                            break;	

                            case 'jpeg':
                                    $img = 'img/icon_foto/jpg.png';
                            break;	

                            case 'png':
                                    $img = 'img/icon_foto/png.png';
                            break;	

                            default:
                                    $img = 'img/icon_foto/archive.png';
                            break;
                    }
                    $retorno['arquivo'][]['id'] = $arquivos['idImovelAnexo'];
                    $retorno['arquivo'][]['img'] = $img;
                    $retorno['arquivo'][]['nome_original'] = $arquivos['nome_original'];
                    $retorno['arquivo'][]['full_path'] = $arquivos['full_path'];
                    $retorno['arquivo'][]['nome_arquivo'] = $arquivos['nome_arquivo'];
                    $retorno['arquivo'][]['data_envio'] = $arquivos['data'];
                    $path=$arquivos['full_path'];
                    //echo "path:".$arquivos[$i]['full_path'];
                    //$retorno['arquivo'][]['anexo'] = '<a style="margin: 5px" href="http://soa.amorc.org.br/'.$arquivos['full_path'].'" title="Image from Unsplash" data-gallery=""><img  style="width: 150px; margin-bottom: 20px" src="img/imovel/anexo/'.$arquivos['nome_arquivo'].'"><a style="margin-right: 25px" onclick="excluiImovelAnexo('.$arquivos['idImovelAnexo'].','.$idImovel.');"><i class="fa fa-trash"></i></a></a>';
            $retorno['arquivo'][]['anexo'] = "<a target='_BLANK' href='" . $arquivos['full_path'] . "'>". $arquivos['nome_original'] . "</a><a style='margin-right: 25px' onclick='excluiImovelAnexo(" . $arquivos['idImovelAnexo'] . "," . $idImovel . "," . $arquivos['fk_idImovelAnexoTipo'] . ");'> <i class='fa fa-trash'></i></a><br>";

        }

    }else {
            $retorno['temAnexo'] = 0;//nenhuma arquivo
            $retorno['sucess'] = false;
            $retorno['erro'] = 'Erro ao buscar os anexos';
    }

    //echo "<pre>";print_r($retorno);
    echo json_encode($retorno);
}
?>