<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $id				 						= isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
    $dataVerificacao						= isset($_REQUEST['dataVerificacao']) ? $_REQUEST['dataVerificacao'] : '';
    $novasAfiliacoes						= isset($_REQUEST['novasAfiliacoes']) ? $_REQUEST['novasAfiliacoes'] : '';
    $desligamentos							= isset($_REQUEST['desligamentos']) ? $_REQUEST['desligamentos'] : '';
    $reintegrados							= isset($_REQUEST['reintegrados']) ? $_REQUEST['reintegrados'] : '';
    $numeroAtualMembrosAtivos				= isset($_REQUEST['numeroAtualMembrosAtivos']) ? $_REQUEST['numeroAtualMembrosAtivos'] : '';
    $idOrganismoAfiliado					= isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : '';
    $usuario                        = isset($_REQUEST['usuario']) ? $_REQUEST['usuario'] : '';
    //Tratar data
    $dataVerificacao = substr($dataVerificacao,6,4)."-".substr($dataVerificacao,3,2)."-".substr($dataVerificacao,0,2);

    $arr=array();
    $arr['status']=0;

    include_once('../../model/membrosRosacruzesAtivosClass.php');
    //include_once('../../lib/functions.php');
    $r = new MembrosRosacruzesAtivos();
    $r->setDataVerificacao($dataVerificacao);
    $r->setNovasAfiliacoes($novasAfiliacoes);
    $r->setDesligamentos($desligamentos);
    $r->setReintegrados($reintegrados);
    $r->setNumeroAtualMembrosAtivos($numeroAtualMembrosAtivos);
    $r->setUltimoAtualizar($usuario);
    $retorno = $r->alteraMembrosRosacruzesAtivos($id);

    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);
}	
?>