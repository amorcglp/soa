<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    include_once("../../controller/imovelControleController.php");
    require_once ("../../model/imovelControleClass.php");

    include_once("../../controller/imovelController.php");
    require_once ("../../model/imovelClass.php");

    include_once("../../controller/organismoController.php");
    require_once ("../../model/organismoClass.php");

    $idImovelControle	= isset($_REQUEST['idImovelControle']) ? json_decode($_REQUEST['idImovelControle']) : '';

    $icc = new imovelControleController();
    $dados = $icc->buscaImovelControle($idImovelControle);

    $ic = new imovelController();
    $dadosI = $ic->buscaImovel($dados->getFk_idImovel());

    $oc = new organismoController();
    $dadosOa = $oc->buscaOrganismo($dados->getFk_idOrganismoAfiliado());

    switch ($dadosOa->getClassificacaoOrganismoAfiliado()) {
            case 1:
                    $classificacaoOrganismoAfiliado = "Loja";
                    break;
            case 2:
                    $classificacaoOrganismoAfiliado = "Pronaos";
                    break;
            case 3:
                    $classificacaoOrganismoAfiliado = "Capítulo";
                    break;
            case 4:
                    $classificacaoOrganismoAfiliado = "Heptada";
                    break;
            case 5:
                    $classificacaoOrganismoAfiliado = "Atrium";
                    break;
    }
    switch ($dadosOa->getTipoOrganismoAfiliado()) {
            case 1:
                    $tipoOrganismoAfiliado = "R+C";
                    break;
            case 2:
                    $tipoOrganismoAfiliado = "TOM";
                    break;
    }
    switch($dados->getStatusImovelControle()){
            case 0:
                    $statusImovelControle = '<span class="badge badge-primary">Ativo</span>';
                    break;
            case 1:
                    $statusImovelControle = '<span class="badge badge-danger">Inativo</span>';
                    break;
    }
    switch($dados->getPropriedadeTipoImovelControle()){
            case 0:
                    $propriedadeTipoImovelControle = "Casa";
                    break;
            case 1:
                    $propriedadeTipoImovelControle = "Sobrado";
                    break;
            case 2:
                    $propriedadeTipoImovelControle = "Prédio";
                    break;
            case 3:
                    $propriedadeTipoImovelControle = "Outros";
                    break;
    }
    switch($dados->getPropriedadeStatusImovelControle()){
            case 0:
                    $propriedadeStatusImovelControle = "Própria";
                    break;
            case 1:
                    $propriedadeStatusImovelControle = "Alugada";
                    break;
            case 2:
                    $propriedadeStatusImovelControle = "Cedida";
                    break;
            case 3:
                    $propriedadeStatusImovelControle = "Comodato";
                    break;
            case 4:
                    $propriedadeStatusImovelControle = "Outros";
                    break;
    }
    switch($dados->getPropriedadeEmNomeImovelControle()){
            case 0:
                    $propriedadeEmNomeImovelControle = "GLB";
                    break;
            case 1:
                    $propriedadeEmNomeImovelControle = "GLP";
                    break;
            case 2:
                    $propriedadeEmNomeImovelControle = "O.A.";
                    break;
            case 3:
                    $propriedadeEmNomeImovelControle = "Outros";
                    break;
    }
    switch($dados->getPropriedadeIptuImovelControle()){
            case 0:
                    $propriedadeIptuImovelControle = "Pago";
                    break;
            case 1:
                    $propriedadeIptuImovelControle = "Em debito";
                    break;
            case 2:
                    $propriedadeIptuImovelControle = "Isento";
                    break;
            case 3:
                    $propriedadeIptuImovelControle = "Imune";
                    break;
            case 4:
                    $propriedadeIptuImovelControle = "Outro";
                    break;
    }

    function yesNo($binario){
            switch($binario){
                    case 0:
                            $resposta = "Sim";
                            break;
                    case 1:
                            $resposta = "Não";
                            break;
            }
            return $resposta;
    }

    function periodicidade($binario){
            switch($binario){
                    case 0:
                            $resposta = "Anual";
                            break;
                    case 1:
                            $resposta = "Bienal";
                            break;
                    case 2:
                            $resposta = "Trienal";
                            break;
                    case 3:
                            $resposta = "Quadrienal";
                            break;
            }
            return $resposta;
    }

    $html = '
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Sistema dos Organismos Afiliados</title>

        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link href="../../font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="../../css/animate.css" rel="stylesheet">
        <link href="../../css/style.css" rel="stylesheet">

    </head>
    <body>
                    <div id="wrapper">
                <div class="wrapper wrapper-content animated fadeInRight article">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="text-center article-title">
                                        <h1>'.$classificacaoOrganismoAfiliado.' '.$tipoOrganismoAfiliado.' '.$dadosOa->getNomeOrganismoAfiliado().' - '.$dadosOa->getSiglaOrganismoAfiliado().'</h1>
                                    </div>
                                    <hr>
                                            <div class="row">					
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Organismo Afiliado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dadosI->getEnderecoImovel()." - ".$dadosI->getNumeroImovel()." - ".$dadosI->getComplementoImovel().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Status: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            <div class="statusTarget'. $vetor["idImovelControle"].'">
                                                                                                            '.$statusImovelControle.'
                                                                                                            </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Referente ao Ano de: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getReferenteAnoImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Tipo de Propriedade: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$propriedadeTipoImovelControle.'	
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Status da Propriedade: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$propriedadeStatusImovelControle.'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Propriedade em nome: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$propriedadeEmNomeImovelControle.'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Data da Escritura: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getPropriedadeEscrituraDataImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">IPTU: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$propriedadeIptuImovelControle.'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            if($dados->getPropriedadeIptuImovelControle()==3 || $dados->getPropriedadeIptuImovelControle()==4){
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Observações IPTU: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getPropriedadeIptuObsImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            }
                                            if($dados->getPropriedadeIptuAnexoImovelControle() != ''){
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">IPTU Anexado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">';
                                                                                    $arrLink = explode(".", $dados->getPropriedadeIptuAnexoImovelControle());
                                                                                                                    $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                                                                                    $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                                            $html .='<a target="_BLANK" href="../'.$dados->getPropriedadeIptuAnexoImovelControle().'">'.$arrLinkNome.'.'.$arrLinkExt.'</a>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            }
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Nº Registro de Imóvel: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getRegistroImovelNumeroImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Data do Registro de Imóvel: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '. $dados->getRegistroImovelDataImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Possui Habite-Se: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.yesNo($dados->getHabiteSeImovelControle()).'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            if($dados->getHabiteSeAnexoImovelControle() != ''){
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Habite-Se Anexado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">';
                                                                                    $arrLink = explode(".", $dados->getHabiteSeAnexoImovelControle());
                                                                                                                    $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                                                                                    $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                                            $html .='<a target="_BLANK" href="../'.$dados->getHabiteSeAnexoImovelControle().'">'.$arrLinkNome.'.'.$arrLinkExt.'</a>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            }
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Possui Vistoria do Corpo de Bombeiros: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.yesNo($dados->getVistoriaBombeiroImovelControle()).'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            if($dados->getVistoriaBombeiroAnexoImovelControle() != ''){
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Vistoria Anexada: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">';
                                                                                    $arrLink = explode(".", $dados->getVistoriaBombeiroAnexoImovelControle());
                                                                                                                    $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                                                                                    $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                                            $html .='<a target="_BLANK" href="../'.$dados->getVistoriaBombeiroAnexoImovelControle().'">'.$arrLinkNome.'.'.$arrLinkExt.'</a>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            }
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Imóvel Avaliado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.yesNo($dados->getAvaliacaoImovelControle()).'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            if($dados->getAvaliacaoImovelControle() == 0){
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Valor Atual Aprox. da Propriedade: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            R$ '.$dados->getAvaliacaoValorImovelControle().',00
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Nome do Avaliador: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getAvaliacaoNomeImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Creci do Avaliador: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getAvaliacaoCreciImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Data da Avaliação: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getAvaliacaoDataImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            }
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Possui Seguro: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.yesNo($dados->getSeguroImovelControle()).'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            if($dados->getSeguroAnexoImovelControle() != ''){
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Seguro Anexado: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">				                                
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">';
                                                                                    $arrLink = explode(".", $dados->getSeguroAnexoImovelControle());
                                                                                                                    $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                                                                                    $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                                            $html .='<a target="_BLANK" href="../'.$dados->getSeguroAnexoImovelControle().'">'.$arrLinkNome.'.'.$arrLinkExt.'</a>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            }
                                            $html .='<hr>
                                            <div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Houve Construção? </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.yesNo($dados->getConstrucaoImovelControle()).'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            if($dados->getConstrucaoImovelControle() == 1){
                                            $html .='<hr>';
                                            }
                                            if($dados->getConstrucaoImovelControle() == 0){
                                                    $html .='<hr>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Possui Alvará de Construção: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.yesNo($dados->getConstrucaoAlvaraImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    if($dados->getConstrucaoAlvaraAnexoImovelControle() != ''){
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Vistoria Anexado: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">';
                                                                                    $arrLink = explode(".", $dados->getConstrucaoAlvaraAnexoImovelControle());
                                                                                                                    $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                                                                                    $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                                                    $html .='<a target="_BLANK" href="../'.$dados->getConstrucaoAlvaraAnexoImovelControle().'">'.$arrLinkNome.'.'.$arrLinkExt.'</a>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    }
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Restrições na Construção: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.yesNo($dados->getConstrucaoRestricoesImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    if($dados->getConstrucaoRestricoesAnotacoesImovelControle() != ''){
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">São elas: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.$dados->getConstrucaoRestricoesAnotacoesImovelControle().'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    }
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Construção Concluída: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.yesNo($dados->getConstrucaoConcluidaImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    if($dados->getConstrucaoTerminoDataImovelControle() != ''){
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Data/Prazo de Término da Construção: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.$dados->getConstrucaoTerminoDataImovelControle().'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    }
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Possui INSS da Construção: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.yesNo($dados->getConstrucaoInssImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    if($dados->getConstrucaoInssAnexoImovelControle() != ''){
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">INSS Anexado: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">					                                
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">';
                                                                                    $arrLink = explode(".", $dados->getConstrucaoInssAnexoImovelControle());
                                                                                                                    $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                                                                                    $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                                                    $html .='<a target="_BLANK" href="../'.$dados->getConstrucaoInssAnexoImovelControle().'">'.$arrLinkNome.'.'.$arrLinkExt.'</a>
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    }
                                                    $html .='<hr>';
                                            }
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Houve Manutenção? </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.yesNo($dados->getManutencaoImovelControle()).'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>';
                                            if($dados->getManutencaoImovelControle() == 1){
                                            $html .='<hr>';
                                            }
                                            if($dados->getManutencaoImovelControle() == 0){
                                                    $html .='<hr>
                                                    <div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Pintura: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.yesNo($dados->getManutencaoPinturaImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    if($dados->getManutencaoPinturaImovelControle() == 0){
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.periodicidade($dados->getManutencaoPinturaTipoImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    }
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Reparo de Hidráulica: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.yesNo($dados->getManutencaoHidraulicaImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    if($dados->getManutencaoHidraulicaImovelControle() == 0){
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.periodicidade($dados->getManutencaoHidraulicaTipoImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    }
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Reparo de Elétrica: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.yesNo($dados->getManutencaoEletricaImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    if($dados->getManutencaoEletricaImovelControle() == 0){
                                                    $html .='<div class="row">
                                                            <div class="form-group">
                                                                <label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
                                                                <div class="col-sm-8">
                                                                    <div class="col-sm-12" style="max-width: 320px">
                                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                                    '.periodicidade($dados->getManutencaoEletricaTipoImovelControle()).'
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                                                    }
                                                    $html .='<hr>';
                                            }
                                            $html .='<div class="row">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label" style="text-align: right">Anotações Complementares: </label>
                                                        <div class="col-sm-8">
                                                            <div class="col-sm-12" style="max-width: 320px">
                                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                            '.$dados->getAnotacoesImovelControle().'
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>

            <!-- Mainly scripts -->
        <script src="../../js/jquery-2.1.1.js"></script>
        <script src="../../js/bootstrap.min.js"></script>
        <script src="../../js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="../../js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="../../js/inspinia.js"></script>
        <script src="../../js/plugins/pace/pace.min.js"></script>
    </body>
    </html>

    ';

    //echo $html;

    //==============================================================
    //==============================================================
    //==============================================================

    include("../../lib/MPDF57/mpdf.php");

    $mpdf=new mPDF('c','A4'); 

    $mpdf->SetDisplayMode('fullpage');

    // LOAD a stylesheet
    $stylesheet_bootstrap		 = file_get_contents('../../css/bootstrap.min.css');
    $stylesheet_font	 		 = file_get_contents('../../font-awesome/css/font-awesome.css');
    $stylesheet_animate			 = file_get_contents('../../css/animate.css');
    $stylesheet_style			 = file_get_contents('../../css/style.css');

    $style_jquery			 	 = file_get_contents('../../js/jquery-2.1.1.js');
    $style_bootstrap			 = file_get_contents('../../js/bootstrap.min.js');
    $style_metisMenu			 = file_get_contents('../../js/plugins/metisMenu/jquery.metisMenu.js');
    $style_slimscroll			 = file_get_contents('../../js/plugins/slimscroll/jquery.slimscroll.min.js');
    $style_inspinia			 	 = file_get_contents('../../js/inspinia.js');
    $style_pace			 		 = file_get_contents('../../js/plugins/pace/pace.min.js');

    //$mpdf->WriteHTML($stylesheet_bootstrap,1);	// The parameter 1 tells that this is css/style only and no body/html/text
    //$mpdf->WriteHTML($stylesheet_font,1);
    //$mpdf->WriteHTML($stylesheet_animate,1);
    //$mpdf->WriteHTML($stylesheet_style,1);

    //$mpdf->WriteHTML($style_jquery,1);
    //$mpdf->WriteHTML($style_bootstrap,1);
    //$mpdf->WriteHTML($style_metisMenu,1);
    //$mpdf->WriteHTML($style_slimscroll,1);
    //$mpdf->WriteHTML($style_inspinia,1);
    //$mpdf->WriteHTML($style_pace,1);


    $mpdf->WriteHTML($html);

    $nome = date("y-m-d").'-detalhe-controle-de-documentos-de-imoveis.pdf';

    $mpdf->Output();

    exit;

    //==============================================================
    //==============================================================
    //==============================================================

}