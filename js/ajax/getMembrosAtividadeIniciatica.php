<?php
@include_once("../../lib/functions.php");

include_once("../../model/atividadeIniciaticaClass.php");
$aim = new atividadeIniciatica();

$idAtividadeIniciatica				= isset($_REQUEST['idAtividadeIniciatica'])?$_REQUEST['idAtividadeIniciatica']:"";

$resultadoMembros = $aim->listaAtividadeIniciaticaMembros($idAtividadeIniciatica);
if ($resultadoMembros) {
    foreach ($resultadoMembros as $vetorMembros) {
        $statusMembro = $vetorMembros['statusAtividadeIniciaticaMembro'];
        //$statusMembro = 2;
        ?>
        <tr>
            <td>
                <?php echo retornaNomeFormatado($vetorMembros['nomeAtividadeIniciaticaMembro'],4); ?>
            </td>
            <td style="min-width: 200px">
                <center>
                    <div id="statusMembroAtividade<?php echo $vetorMembros['seqCadastAtividadeIniciaticaMembro'].$vetorMembros['idAtividadeIniciatica']; ?>">
                        <?php
                        switch($statusMembro){
                            case 0: echo "<span class='badge badge-warning'>Convidado</span>"; break;
                            case 1: echo "<span class='badge badge-success'>Confirmado</span>"; break;
                            case 2: echo "<span class='badge badge-primary compareceu'>Compareceu</span>"; break;
                            case 3: echo "<span class='badge badge-danger'>Cancelado</span>"; break;
                            case 4: echo "<span class='badge badge-danger' style='max-width: 200px'>".$vetorMembros['mensagemAtividadeIniciaticaMembro']."</span>"; break;
                        }
                        ?>
                    </div>
                </center>
            </td>
            <td style="min-width: 150px">
                <center>
                    <div id="acoesMembroAtividade<?php echo $vetorMembros['seqCadastAtividadeIniciaticaMembro'].$vetorMembros['idAtividadeIniciatica']; ?>">
                        <?php
                        if ($vetorMembros['statusAtividadeIniciatica'] != 2 || $statusMembro == 4) {
                            if ($statusMembro == 0) {
                                echo "<a class='btn-xs btn-success' onclick='confirmaAgendamentoMembroAtividadeIniciatica(" . $vetorMembros['seqCadastAtividadeIniciaticaMembro'] . "," . $vetorMembros['idAtividadeIniciatica'] . ")'>Confirmar</a>
                                &nbsp
                                <a class='btn-xs btn-danger' onclick='cancelaAgendamentoMembroAtividadeIniciatica(" . $vetorMembros['seqCadastAtividadeIniciaticaMembro'] . "," . $vetorMembros['idAtividadeIniciatica'] . ")'>Cancelar</a>";
                            } elseif ($statusMembro == 1) {
                                echo "<a class='btn-xs btn-danger' onclick='cancelaAgendamentoMembroAtividadeIniciatica(" . $vetorMembros['seqCadastAtividadeIniciaticaMembro'] . "," . $vetorMembros['idAtividadeIniciatica'] . ")'>Cancelar</a>
                                &nbsp
                                <a class='btn-xs btn-primary' onclick='confirmaParticipacaoMembroAtividadeIniciatica(" . $vetorMembros['seqCadastAtividadeIniciaticaMembro'] . "," . $vetorMembros['idAtividadeIniciatica'] . ")'>Presente</a>";
                            } elseif ($statusMembro == 2) {
                                echo "--";
                            } elseif ($statusMembro == 3) {
                                echo "--";
                            } elseif ($statusMembro == 4) {
                            if ($vetorMembros['mensagemAtividadeIniciaticaMembro'] === "Iniciação já cadastrada!"){
                                echo "--";
                            } else {
                                echo "<a class='btn-xs btn-primary' onclick='atualizarObrigacaoRitualisticaMembroNovamente(".$vetorMembros['seqCadastAtividadeIniciaticaMembro'].",".$vetorMembros['idAtividadeIniciatica'].")'>Tentar Novamente</a>";
                            }
                            } else {
                                echo "--";
                            }
                        } else {
                            echo "--";
                        }
                        ?>
                    </div>
                </center>
            </td>
        </tr>
        <input type="text" class="<?php echo $vetorMembros['idAtividadeIniciatica'].$vetorMembros['seqCadastAtividadeIniciaticaMembro'];?>" id="<?php echo $vetorMembros['seqCadastAtividadeIniciaticaMembro'];?>" name="membros<?php echo $vetorMembros['idAtividadeIniciatica'];?>[]" value="<?php echo $statusMembro; ?>">
<?php
    }
} else {
?>
    <tr>
        <td colspan="3">
            <div class="alert alert-danger">
                <center>Ainda não há nenhum membro cadastrado nessa atividade!</center>
            </div>
        </td>
    </tr>
<?php
}
?>

