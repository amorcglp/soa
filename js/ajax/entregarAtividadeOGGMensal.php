<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $mesAtual           = isset($_REQUEST['mesAtual']) ? $_REQUEST['mesAtual']  : '';
    $anoAtual           = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual']  : '';
    $idOrganismoAfiliado= isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']  : '';
    $quemEntregou 	    = isset($_REQUEST['quemEntregou']) ? $_REQUEST['quemEntregou']  : '';

    $arr=array();

    include_once('../../lib/functions.php');
    include_once('../../model/oggMensalClass.php');
    $rom = new oggMensal();
    $resultado = $rom->listaOggMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
    if(!$resultado)
    {
            $rom = new oggMensal();
            $rom->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $rom->setMes($mesAtual);
            $rom->setAno($anoAtual);
            $rom->setQuemEntregou($quemEntregou);
            $rom->setEntregue(1);
            $rom->cadastraOggMensal();


        $rom = new oggMensal();
        $resultado = $rom->listaOggMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
    }

    $arr['status'] = 0;
    $arr['dataEntrega'] = "";

    if ($resultado) {

        $arr['status'] = 1;
        foreach ($resultado as $v)
        {
            if(trim($v['numeroAssinatura'])!="")
            {
                $numeroAssinatura = $v['numeroAssinatura'];
                $temNumeroAssinatura=true;
            }else{
                $numeroAssinatura = aleatorioAssinatura();
                $rom->atualizaNumeroAssinatura($mesAtual, $anoAtual,$idOrganismoAfiliado,$numeroAssinatura);
            }

            $dataEntrega = substr($v['dataEntrega'],0,10);
            $arr['dataEntrega'] = substr($dataEntrega,8,2)."/".substr($dataEntrega,5,2)."/".substr($dataEntrega,0,4);
            $arr['quemEntregou'] = $v['nomeUsuario'];
            $arr['numeroAssinatura'] = $numeroAssinatura;
        }
    }

    echo json_encode($arr);
}	
?>