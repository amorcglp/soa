<?php

/*
 * Token
 */
if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

	//$exibicaoMural		= isset($_POST['exibicaoMural']) ? addslashes($_POST['exibicaoMural']) : '';
	$fk_idRegiaoRosacruz		= isset($_REQUEST['fk_idRegiaoRosacruz']) ? addslashes($_REQUEST['fk_idRegiaoRosacruz']) : '';
    $fk_idOrganismoAfiliado     = isset($_REQUEST['fk_idOrganismoAfiliado']) ? addslashes($_REQUEST['fk_idOrganismoAfiliado']) : '';
	$status		                = isset($_REQUEST['status']) ? addslashes($_REQUEST['status']) : '';
	$seqCadast		            = isset($_REQUEST['seqCadast']) ? addslashes($_REQUEST['seqCadast']) : '';
	$filtroFavorito		        = isset($_REQUEST['filtroFavorito']) ? addslashes($_REQUEST['filtroFavorito']) : '';
	$filtroPropriasPublicacoes	= isset($_REQUEST['filtroPropriasPublicacoes']) ? addslashes($_REQUEST['filtroPropriasPublicacoes']) : '';

	$filtroCategorias   = isset($_REQUEST['filtroCategoriaSelecionada']) ? json_decode($_REQUEST['filtroCategoriaSelecionada']) : '';
    $filtroPublicos   = isset($_REQUEST['filtroPublicoSelecionado']) ? json_decode($_REQUEST['filtroPublicoSelecionado']) : '';
    $filtroOrganismos   = isset($_REQUEST['filtroOrganismosSelecionados']) ? json_decode($_REQUEST['filtroOrganismosSelecionados']) : '';

    include_once("../../model/muralClass.php");
    $mural = new Mural();

    //echo "<pre>";print_r($filtroOrganismos);echo "</pre>";

    $retorno=array();
    $retorno['qntTotal'] = 0;
    $retorno['qntTotal'] = $mural->retornaQuantidadePublicacaoMural($fk_idRegiaoRosacruz,$fk_idOrganismoAfiliado,$status,$filtroFavorito,$filtroPropriasPublicacoes,$seqCadast,$filtroCategorias,$filtroOrganismos,$filtroPublicos);

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}
