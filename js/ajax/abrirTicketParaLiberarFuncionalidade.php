<?php
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

include '../../mailgun.php';

function UrlAtual(){
 $dominio= $_SERVER['HTTP_HOST'];
 $url = $dominio;
 return $url;
}
/*
 * Token
 */
require_once '../../sec/token.php';

if($tokenLiberado)
{

    //include_once('../../lib/phpmailer/class.phpmailer.php');
    
    $REDIRECT_URI  ="";
    if(UrlAtual()=="treinamento.amorc.org.br")
    {    
        /*Treinamento*/
        $REDIRECT_URI  = 'https://treinamento.amorc.org.br/';
    }

    if(UrlAtual()=="soa.amorc.org.br")
    {
        /*Produção*/
        $REDIRECT_URI  = 'https://soa.amorc.org.br/';
    }
    if(UrlAtual()=="localhost")
    {
        /*Localhost*/
        $REDIRECT_URI  = 'https://localhost/soa_producao_st/';
    }

    $idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : null;
    $idFuncionalidade = isset($_REQUEST['idFuncionalidade']) ? $_REQUEST['idFuncionalidade'] : null;
    $idItemFuncionalidade = isset($_REQUEST['idItemFuncionalidade']) ? $_REQUEST['idItemFuncionalidade'] : null;
    $seqCadast = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : null;
    $motivo = isset($_REQUEST['motivo']) ? $_REQUEST['motivo'] : null;
    $mes = isset($_REQUEST['mes']) ? $_REQUEST['mes'] : null;
    $ano = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : null;
    
    if($idItemFuncionalidade!=0)
    {    
        $stringCabecalho = ' Cód. '.$idItemFuncionalidade;
    }else{
        $stringCabecalho = ' Mês: '.$mes.' Ano: '.$ano;
    }

    include_once('../../model/usuarioClass.php');
    $u = new Usuario();
    $arr = array();

    $arr = $u->buscaUsuario($seqCadast);
    
    include_once('../../model/ticketItemFuncionalidadeClass.php');
    $tif = new TicketItemFuncionalidade();
    
    include_once('../../model/ticketClass.php');
    $t = new Ticket();
    $arrFuncionalidade = array();
    
    $arrFuncionalidade = $t->listaFuncionalidade($idFuncionalidade);
    
    //echo "<pre>";print_r($arrFuncionalidade);exit();

    if (count($arr)>0) {
        
        //Abrir Ticket
        include_once('../../controller/ticketController.php');
        $tc = new ticketController();
        $remetente              = addslashes($seqCadast);
        $descricao              = "Requisição de token para ".$arr[0]['loginUsuario'];
        /*
        echo "<br>idFuncionalidade:".$idFuncionalidade;
        echo "<br>descricao:".$descricao;
        echo "<br>remetente:".$remetente;
        echo "<br>idOrganismoAfiliado:".$idOrganismoAfiliado;
        echo "<br>idItemFuncionalidade:".$idItemFuncionalidade;
         */
        $arrTicket=array(); 
        $arrTicket = $tc->cadastroTicketAltomatico($idFuncionalidade,$descricao,$remetente,$idOrganismoAfiliado,$idItemFuncionalidade,3);
        if(count($arrTicket)>0)
        {     
            $idTicket = $arrTicket[0]['lastid'];
            //Cadastrar Motivo como primeira notificação
            include_once('../../model/notificacaoClass.php');
            $n = new Notificacao();
            $resultadoNotificacao = $n->cadastroNotificacao($motivo,$seqCadast,$idTicket);
            
            $motivo = str_replace("\"","",$motivo);//retirar aspas duplas
            $motivo = str_replace("\'","",$motivo);//retirar aspas simples
            
            //Cadastrar vinculo do item com o ticket
            $tif->setIdOrganismoAfiliado($idOrganismoAfiliado);
            $tif->setFk_idFuncionalidade($idFuncionalidade);
            $tif->setFk_idItemFuncionalidade($idItemFuncionalidade);
            $tif->setSeqCadast($seqCadast);
            $tif->setMotivo(mysql_escape_string($motivo));
            $tif->setFk_idFinalidade(3);
            $tif->setFk_idTicket($idTicket);
            $tif->setMes($mes);
            $tif->setAno($ano);
            $r = $tif->cadastroVinculoItemTicket();
                    
            //Quebrar o nome
            $arrNome = explode(" ", $arr[0]['nomeUsuario']);

            //Enviar email com o token
            $texto = '<div>
                <div dir="ltr">
                        <table class="ecxbody-wrap"
                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                                <tbody>
                                        <tr
                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                <td
                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                        valign="top"></td>
                                                <td class="ecxcontainer" width="600"
                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                        valign="top">
                                                        <div class="ecxcontent"
                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                                <table class="ecxmain" width="100%" cellpadding="0"
                                                                        cellspacing="0"
                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                        <tbody>
                                                                                <tr
                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                        <td class="ecxcontent-wrap"
                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                                valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                                style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                                alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                        <br>
                                                                                        <br>
                                                                                        <br>
                                                                                                <div style="text-align: left;"></div>
                                                                                                <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                        <tbody>
                                                                                                                <tr
                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                        <td class="ecxcontent-block"
                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                                valign="top">' . strtoupper($arrNome[0]) . ',</td>
                                                                                                                </tr>
                                                                                                                <tr
                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                        <td class="ecxcontent-block"
                                                                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                valign="top">Você solicitou um ticket para alterar a funcionalidade: ' . $arrFuncionalidade[0]['nomeFuncionalidade'] . $stringCabecalho.'.</td>
                                                                                                                </tr>
                                                                                                                <tr
                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                        <td class="ecxcontent-block"
                                                                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                valign="top">Motivo: ' . $motivo . '.</td>
                                                                                                                </tr>
                                                                                                                <tr
                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                        <td class="ecxcontent-block"
                                                                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                valign="top">Em breve a GLP entrará em contato dando um parecer. </td>
                                                                                                                </tr>
                                                                                                                <tr
                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                        <td class="ecxcontent-block"
                                                                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                valign="top">AMORC - Ordem Rosacruz</td>
                                                                                                                </tr>
                                                                                                        </tbody>
                                                                                                </table></td>
                                                                                </tr>
                                                                        </tbody>
                                                                </table>
                                                                <div class="footer"
                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                        <div style="text-align: left;"></div>
                                                                        <table width="100%"
                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                <tbody>
                                                                                        <tr
                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                <td class="ecxaligncenter ecxcontent-block"
                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                        align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2015</td>
                                                                                        </tr>
                                                                                </tbody>
                                                                        </table>
                                                                </div>
                                                        </div>
                                                        <div style="text-align: left;"></div></td>
                                                <td
                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                        valign="top"></td>
                                        </tr>
                                </tbody>
                        </table>
                        </div>
                </div>';

            /*
            $servidor = "smtp.office365.com";
            $porta = "587";
            $usuario = "noresponseglp@amorc.org.br";
            $senha = "@w2xpglp";
            */
            /*
            $servidor = "smtp.outlook.com";
            $porta = "465";
            $usuario = "samufcaldas@hotmail.com.br";
            $senha = "safc3092";
            */
            /*
            $servidor = "smtp.gmail.com";
            $porta = "465";
            $usuario = "cpdglp@gmail.com";
            $senha = "@w2xpglp";


            #Disparar email
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->SMTPAuth = true;
            $mail->Host = $servidor; // SMTP utilizado
            $mail->Port = $porta;
            $mail->Username = $usuario;
            $mail->Password = $senha;
            $mail->From = 'cpdglp@gmail.com';
            $mail->AddReplyTo("atendimento@amorc.org.br", "Antiga e Mística Ordem Rosacruz - AMORC");
            $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
            //$mail->AddAddress("samufcaldas@hotmail.com.br");
            //$mail->AddAddress("tatiane.jesus@amorc.org.br");
            //$mail->AddAddress("lucianob@amorc.org.br");
            //$mail->AddAddress("braz@amorc.org.br");
            $mail->AddAddress($arr[0]['emailUsuario']);
            $mail->isHTML(true);
            $mail->CharSet = 'utf-8';
            //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
            $mail->Subject = 'Novo Ticket - '.$arrFuncionalidade[0]['nomeFuncionalidade'].' - Cod. '.$idItemFuncionalidade.' - SOA';
            $mail->Body = $texto;
            $mail->AltBody = strip_tags($texto);
            $enviado = $mail->Send();
             */
            $enviado = $mgClient->sendMessage($domain, array(
                'from'    => 'Atendimento <atendimento@amorc.org.br>',
                'to'      => strtoupper($arrNome[0]).' <'.$arr[0]['emailUsuario'].'>',
                'subject' => 'Novo Ticket - '.$arrFuncionalidade[0]['nomeFuncionalidade'].' - Cod. '.$idItemFuncionalidade.' - SOA',
                'text'    => 'Seu e-mail não suporta HTML',
                'html'    => $texto));
														 
            if ($enviado) {
                //$r = $u->enviouEmailIntegridadeTokenUsuario($token);
                $retorno['status'] = '1';
            } else {
                //$r = $u->erroEmailIntegridadeTokenUsuario($token);
                $retorno['status'] = '2';
            }
            
            # Try running this locally.
            # Include the Autoloader (see "Libraries" for install instructions)
            /*
            require 'vendor/autoload.php';
            use Mailgun\Mailgun;

            # Instantiate the client.
            $mgClient = new Mailgun('key-3ax6xnjp29jd6fds4gc373sgvjxteol0');
            $domain = "mg.amorc.org.br";

            # Make the call to the client.
            $result = $mgClient->sendMessage("$domain",
              array('from'    => 'Atendimento - Ordem Rosacruz, AMORC - GLP <cpdglp@gmail.com>',
                    'to'      => strtoupper($arrNome[0]).'<'.$arr[0]['emailUsuario'].'>',
                    'subject' => 'Novo Ticket - '.$arrFuncionalidade[0]['nomeFuncionalidade'].' - Cod. '.$idItemFuncionalidade.' - SOA',
                    'text'    => $texto));
             */
        }else{
            $retorno['status'] = '3';
        }
    } else {
        $retorno['status'] = '0';
    }

    echo json_encode($retorno);
}    
?>