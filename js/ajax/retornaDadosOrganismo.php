<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 

    if(!isset($siglaOA))
    {    
        $siglaOA 	= isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:0;
    }

    if(!isset($pais))
    {
        $pais	= isset($_REQUEST['pais'])?$_REQUEST['pais']:'BR';
    }
    if(!isset($cidade))
    {
        $cidade	= isset($_REQUEST['cidade'])?$_REQUEST['cidade']:'';
    }

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaRelacaoOA';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'IdeTipoOrganismo' => '',
                                    'SigOrganismoAfilidado' => $siglaOA,
                                    'IdeDescartarOaPorSituacao' => '',
                                    'SigPaisOrganismo' => $pais,
                                    'SigRegiaoBrasil' => '',
                                    'SigAgrupamentoRegiao' => '',
                                    'NomLocali' => $cidade,
                                    'NomOrgaFil' => ''    
    );

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    // Imprime o retorno
    if(!isset($ocultar_json))
    {    
        echo json_encode($return);
    }
}
?>