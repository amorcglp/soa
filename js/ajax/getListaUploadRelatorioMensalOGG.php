<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    //session_start();
    $arrNivelUsuario = isset($_SESSION['niveis'])?explode(",",$_SESSION['niveis']):NULL;
    include_once('../../model/relatorioOGGMensalClass.php');

    $rfm = new RelatorioOGGMensal();

    $resultado = $rfm->listaRelatorioOGGMensal($_REQUEST['mesAtual'],$_REQUEST['anoAtual'],$_REQUEST['idOrganismoAfiliado']);
    $i=1;
    if($resultado)
    {
            if (count($resultado)>0) {
                    echo "<ul>";
                    foreach($resultado as $vetor)
                    {
                            ?>
                            <li>
                                    <a href="<?php echo $vetor['caminhoRelatorioOGGMensal'];?>" target="_blank">Relatório Mensal OGG <?php if(count($resultado)>1){?>- Parte <?php echo $i;}?></a>
                                    <?php if(in_array("3",$arrNivelUsuario)){?>
                                    <a href="#" onclick="excluirUploadRelatorioMensalOGG('<?php echo $vetor['idRelatorioOGGMensal'];?>','<?php echo $_REQUEST['mesAtual'];?>','<?php echo $_REQUEST['anoAtual'];?>','<?php echo $_REQUEST['idOrganismoAfiliado'];?>');"><i class="fa fa-trash"></i></a>
                                    <?php }?>
                            </li>
                            <?php
                            $i++; 
                    }
                    echo "</ul>";
            }
    } else {
        echo "Nenhum Relatório Mensal Assinado Enviado!";
    }
}
?>