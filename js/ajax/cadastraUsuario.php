<?php

include_once '../../mailgun.php';

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

//include_once('../../lib/phpmailer/class.phpmailer.php');
include_once('../../model/usuarioClass.php');

$u = new Usuario();
/*
echo '<br>===========================';
echo '<br>=>seq_cadast:'.$_REQUEST['seq_cadast'];
echo '<br>=>nomeUsuario:'.$_REQUEST['nomeUsuario'];
echo '<br>=>emailUsuario:'.$_REQUEST['emailUsuario'];
echo '<br>=>telefoneResidencialUsuario:'.$_REQUEST['telefoneResidencialUsuario'];
echo '<br>=>telefoneComercialUsuario:'.$_REQUEST['telefoneComercialUsuario'];
echo '<br>=>celularUsuario:'.$_REQUEST['celularUsuario'];
echo '<br>=>RAMAL:0';
echo '<br>=>loginUsuario:'.$_REQUEST['loginUsuario'];
echo '<br>=>senhaUsuario:'.$_REQUEST['senhaUsuario'];
echo '<br>=>codigoAfiliacaoUsuario:'.$_REQUEST['codigoAfiliacaoUsuario'];
echo '<br>=>inativo:'.$_REQUEST['inativo'];
echo '<br>=>siglaOA:'.$_REQUEST['siglaOA'];
echo '<br>=>fk_idDepartamento:'.$_REQUEST['fk_idDepartamento'];
*/

$u->setSeqCadast($_REQUEST['seq_cadast']);
$u->setNomeUsuario($_REQUEST['nomeUsuario']);
$u->setEmailUsuario($_REQUEST['emailUsuario']);
$u->setTelefoneResidencialUsuario($_REQUEST['telefoneResidencialUsuario']);
$u->setTelefoneComercialUsuario($_REQUEST['telefoneComercialUsuario']);
$u->setCelularUsuario($_REQUEST['celularUsuario']);
$u->setRamalUsuario(0);
$u->setLoginUsuario($_REQUEST['loginUsuario']);
$u->setSenhaUsuario(password_hash($_REQUEST['senhaUsuario'], PASSWORD_DEFAULT));
$u->setCodigoDeAfiliacaoUsuario($_REQUEST['codigoAfiliacaoUsuario']);
$u->setStatusUsuario($_REQUEST['inativo']);
$u->setSiglaOA($_REQUEST['siglaOA']); //Sigla do OA de atuação
$u->setFk_idDepartamento($_REQUEST['fk_idDepartamento']); //Cadastrar usuário no departamento Organismos Afiliados

if($_REQUEST['inativo']==0)
{
	$u->setFk_idRegiaoRosacruz($_REQUEST['regiaoUsuario']);//Cadastrar usuário na primeira região encontrada no retorno das funções dele
}else{
	$u->setFk_idRegiaoRosacruz(18);//
}

if ($u->cadastraUsuario()) {
    
    $retorno['sucesso'] = 'Usuario cadastrado com sucesso!';
    
    //Setar variáveis
    $nomeUsuario    = isset($_REQUEST['nomeUsuario'])?$_REQUEST['nomeUsuario']:null;
    $loginUsuario   = isset($_REQUEST['loginUsuario'])?$_REQUEST['loginUsuario']:null;
    $senhaUsuario   = isset($_REQUEST['senhaUsuario'])?$_REQUEST['senhaUsuario']:null;
    $emailUsuario   = isset($_REQUEST['emailUsuario'])?$_REQUEST['emailUsuario']:null;

    //Quebrar o nome
    $arrNome = explode(" ", $nomeUsuario);
    
    //Enviar email com o token
	$texto = '<div>
	<div dir="ltr">
		<table class="ecxbody-wrap"
			style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
			<tbody>
				<tr
					style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
					<td
						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
						valign="top"></td>
					<td class="ecxcontainer" width="600"
						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
						valign="top">
						<div class="ecxcontent"
							style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
							<table class="ecxmain" width="100%" cellpadding="0"
								cellspacing="0"
								style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
								<tbody>
									<tr
										style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
										<td class="ecxcontent-wrap"
											style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
											valign="top"><img src="http://i.imgur.com/FKg7aai.png"
											style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
											alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
										<br>
										<br>
										<br>
											<div style="text-align: left;"></div>
											<table width="100%" cellpadding="0" cellspacing="0"
												style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
												<tbody>
													<tr
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
														<td class="ecxcontent-block"
															style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
															valign="top">'.strtoupper($arrNome[0]).',</td>
													</tr>
                                                                                                        <tr
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
														<td class="ecxcontent-block"
															style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
															valign="top">Seu login é: '.$loginUsuario.' </td>
													</tr>
                                                                                                        <tr
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
														<td class="ecxcontent-block"
															style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
															valign="top">Sua senha é: '.$senhaUsuario.' </td>
													</tr>
													<tr
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
														<td class="ecxcontent-block"
															style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
															valign="top"><a href="http://soa.amorc.com.br"
															class="ecxbtn-primary"
															style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background: #348eda; border-color: #348eda; border-style: solid; border-width: 10px 20px;"
															target="_blank" alt="SOA"> Clique aqui para acessar o sistema do SOA </a></td>
													</tr>
													<tr
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
														<td class="ecxcontent-block"
															style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
															valign="top">AMORC - Ordem Rosacruz</td>
													</tr>
												</tbody>
											</table></td>
									</tr>
								</tbody>
							</table>
							<div class="footer"
								style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
								<div style="text-align: left;"></div>
								<table width="100%"
									style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
									<tbody>
										<tr
											style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
											<td class="ecxaligncenter ecxcontent-block"
												style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
												align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2015</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div style="text-align: left;"></div></td>
					<td
						style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
						valign="top"></td>
				</tr>
			</tbody>
		</table>
		</div>
	</div>';
	/*				
	$servidor="smtp.office365.com";
        $porta="587";
        $usuario="noresponseglp@amorc.org.br";
        $senha="@w2xpglp";

        //echo $texto;
        #Disparar email
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPSecure = "tls";
        $mail->SMTPAuth = true;
        $mail->Host = $servidor;	// SMTP utilizado
        $mail->Port = $porta;
        $mail->Username = $usuario;
        $mail->Password = $senha;
        $mail->From = 'noresponseglp@amorc.org.br';
        $mail->AddReplyTo("soa@amorc.org.br","Antiga e Mística Ordem Rosacruz - AMORC");
        $mail->FromName = 'SOA - Ordem Rosacruz, AMORC - GLP';
	//$mail->AddAddress("luis.fernando@amorc.org.br");
	//$mail->AddAddress("tatiane.jesus@amorc.org.br");
	//$mail->AddAddress("lucianob@amorc.org.br");
	//$mail->AddAddress("ewerton@amorc.org.br");
        //$mail->AddAddress("braz@amorc.org.br");
	$mail->AddAddress($emailUsuario);
	$mail->isHTML(true);
	$mail->CharSet = 'utf-8';
	//$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
	$mail->Subject = 'Login e Senha - SOA';
	$mail->Body = $texto;
	$mail->AltBody = strip_tags($texto); 
	$enviado = $mail->Send();
         */
        $enviado = $mgClient->sendMessage($domain, array(
                    'from'    => 'Atendimento <atendimento@amorc.org.br>',
                    'to'      => $arrNome[0].' <'.$emailUsuario.'>',
                    'subject' => 'Login e Senha - SOA - AMORC',
                    'text'    => 'Seu e-mail não suporta HTML',
                    'html'    => $texto));
	if($enviado)
	{
		$retorno['status'] = '1';
	}else{
		$retorno['status'] = '0';
	}
} else {
    $retorno['erro'][] = 'Ocorreu algum erro ao cadastrar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    $retorno['status'] = 0;
}

echo json_encode($retorno);
}
?>