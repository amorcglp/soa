<?php
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

    //include_once('../../lib/phpmailer/class.phpmailer.php');

    $novaSenha	 	= isset($_REQUEST['novaSenha'])?$_REQUEST['novaSenha']:null;
    $token		= isset($_REQUEST['token'])?$_REQUEST['token']:null;

    include_once('../../model/usuarioClass.php');
    $u = new Usuario();
    if($u->alterarSenha($token,password_hash($novaSenha, PASSWORD_DEFAULT)))
    {
        /*
        require_once ("../../controller/loginController.php");
        //include_once ("../../model/criaSessaoClass.php");

        $cl = new loginController();

        $cl->efetuaLogout();
        */
        $retorno['status'] = '1';	
    }else{
        $retorno['status'] = '0';
    }	

    echo json_encode($retorno);
}	
?>