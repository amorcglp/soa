<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $fk_idOrganismoAfiliado 			        = isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado'] : '';
    $fk_idAtividadeIniciaticaOficial 		    = isset($_REQUEST['fk_idAtividadeIniciaticaOficial']) ? $_REQUEST['fk_idAtividadeIniciaticaOficial'] : '';
    $fk_idAtividadeIniciaticaColumba 			= isset($_REQUEST['fk_idAtividadeIniciaticaColumba']) ? $_REQUEST['fk_idAtividadeIniciaticaColumba'] : '';
    $codAfiliacaoRecepcao 			            = isset($_REQUEST['codAfiliacaoRecepcao']) ? $_REQUEST['codAfiliacaoRecepcao'] : '';
    $nomeRecepcao 			                    = isset($_REQUEST['nomeRecepcao']) ? $_REQUEST['nomeRecepcao'] : '';
    $recepcaoAtividadeIniciaticaOficial 		= isset($_REQUEST['recepcaoAtividadeIniciaticaOficial']) ? $_REQUEST['recepcaoAtividadeIniciaticaOficial'] : '';
    $tipoAtividadeIniciatica 			        = isset($_REQUEST['tipoAtividadeIniciatica']) ? $_REQUEST['tipoAtividadeIniciatica'] : '';
    $dataRealizadaAtividadeIniciatica 			= isset($_REQUEST['dataRealizadaAtividadeIniciatica']) ? $_REQUEST['dataRealizadaAtividadeIniciatica'] : '';
    $horaRealizadaAtividadeIniciatica 			= isset($_REQUEST['horaRealizadaAtividadeIniciatica']) ? $_REQUEST['horaRealizadaAtividadeIniciatica'] : '';
    $localAtividadeIniciatica        			= isset($_REQUEST['localAtividadeIniciatica']) ? $_REQUEST['localAtividadeIniciatica'] : '';
    $anotacoesAtividadeIniciatica        		= isset($_REQUEST['anotacoesAtividadeIniciatica']) ? $_REQUEST['anotacoesAtividadeIniciatica'] : '';
    $fk_seqCadastAtualizadoPor                  = isset($_REQUEST['fk_seqCadastAtualizadoPor']) ? $_REQUEST['fk_seqCadastAtualizadoPor'] : '';
    $camposGeraisSeqCadast 	                    = isset($_REQUEST['camposGeraisSeqCadast']) ? json_decode($_REQUEST['camposGeraisSeqCadast']) : '';
    $camposGeraisTexto 	                        = isset($_REQUEST['camposGeraisTexto']) ? json_decode($_REQUEST['camposGeraisTexto']) : '';
    $camposGeraisCompanheiro 	                = isset($_REQUEST['camposGeraisCompanheiro']) ? json_decode($_REQUEST['camposGeraisCompanheiro']) : '';
    $loginUsuario 	                            = isset($_REQUEST['loginAtualizadoPor']) ? json_decode($_REQUEST['loginAtualizadoPor']) : '';

    /* FUNCTIONS */
    function converteIdeTipoObrigacao($tipo){
        switch($tipo){
            case 1: return 3; break;
            case 2: return 5; break;
            case 3: return 7; break;
            case 4: return 9; break;
            case 5: return 11; break;
            case 6: return 13; break;
            case 7: return 15; break;
            case 8: return 17; break;
            case 9: return 19; break;
            case 10: return 32; break;
            case 11: return 33; break;
            case 12: return 34; break;
        }
    }

    function retornaNomeExtraido($textoSelect){
        $texto              = $textoSelect;
        $arrayTexto         = explode(" ", $texto);
        $textoParaEliminar  = $arrayTexto[0] . " ";
        $nome               = substr($texto, strlen($textoParaEliminar));
        return $nome;
    }

    function retornaCodAfiliacaoExtraido($textoSelect){
        $arrayTexto         = explode(" ", $textoSelect);
        $codAfiliacao       = str_replace("]","",str_replace("[","",$arrayTexto[0]));
        return $codAfiliacao;
    }

    /* ALGORITMO */
    include_once('../../model/atividadeIniciaticaClass.php');

    $aim    = new atividadeIniciatica();

    $dataRealizada           = substr($dataRealizadaAtividadeIniciatica,6,4)."-".substr($dataRealizadaAtividadeIniciatica,3,2)."-".substr($dataRealizadaAtividadeIniciatica,0,2);

    $retorno = array();
    if ($fk_idOrganismoAfiliado!='')
    {

        /* CADASTRA ATIVIDADE INICIÁTICA */
        $resultado = $aim->cadastroAtividadeIniciaticaAgil($fk_idOrganismoAfiliado,$fk_idAtividadeIniciaticaOficial,$fk_idAtividadeIniciaticaColumba,
            $codAfiliacaoRecepcao, $nomeRecepcao, $recepcaoAtividadeIniciaticaOficial, $tipoAtividadeIniciatica, $dataRealizada,
            $horaRealizadaAtividadeIniciatica, $localAtividadeIniciatica, $anotacoesAtividadeIniciatica, $fk_seqCadastAtualizadoPor);

        if ($resultado==true)
        {
            /* SELECIONA ID DA ATIVIDADE CRIADA */
            $ultimoId = $aim->selecionaUltimoIdInserido();
            foreach($ultimoId as $ultimoIdVetor)
            {
                $ultimoIdAtividade = $ultimoIdVetor['idAtividadeIniciatica'];
            }

            $aim->alteraStatusAtividadeIniciaticaRealizada($ultimoIdAtividade);
            /* CADASTRO DE CADA MEMBRO NA ATIVIDADE INICIÁTICA */
            $cadastSuccess=0;
            for($i=0; $i < count($camposGeraisSeqCadast); $i++)
            {
                $seqCadast          = $camposGeraisSeqCadast[$i];
                $nome               = retornaNomeExtraido($camposGeraisTexto[$i]);

                if($aim->cadastraMembroAtividadeIniciatica($seqCadast, $ultimoIdAtividade, $nome))
                {
                    $cadastSuccess++;
                    $aim->confirmaParticipacaoMembroAtividadeIniciatica($seqCadast,$ultimoIdAtividade);
                }
            }

            /* SE INSUCESSO EM TODOS OS CADASTROS DE MEMBRO CANCELA TODA A OPERAÇÃO */
            if($cadastSuccess != count($camposGeraisSeqCadast))
            {
                $aim->deletaMembroAtividadeIniciatica($ultimoIdAtividade);
                $aim->deletaAtividadeIniciatica($ultimoIdAtividade);
                $retorno['sucesso'] = false;
                $retorno['erro'] = 'Ocorreu algum erro ao realizar o cadastro. Tente novamente mais tarde!1';
            }
            /* SE SUCESSO EM TODOS OS CADASTROS DE MEMBRO ENVIA OBRIGAÇÕES INICIÁTICAS DE CADA MEMBRO PARA O ORCZ */
            else
            {
                $retorno['sucesso'] = true;
                $retorno['success'] = 'Cadastrado realizado com sucesso!';
            }
        } else
        {
            $retorno['sucesso'] = false;
            $retorno['erro'] = 'Ocorreu algum erro ao realizar o cadastro. Tente novamente mais tarde!2';
        }
    } else
    {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Insira o Organismo Afiliado!';
    }
    $retorno['idAtividade'] = $ultimoIdAtividade;
    echo json_encode($retorno);
}
?>