<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idPublicacao           = isset($_REQUEST['idPublicacao']) ? $_REQUEST['idPublicacao'] : '';
    $mensagemMural          = isset($_REQUEST['mensagemMural']) ? $_REQUEST['mensagemMural'] : '';
    $exibicaoMural          = isset($_POST['exibicaoMural']) ? addslashes($_POST['exibicaoMural']) : '';
    $fk_idMuralCategoria    = isset($_POST['fk_idMuralCategoria']) ? addslashes($_POST['fk_idMuralCategoria']) : '';
    $fk_idMuralPublico      = isset($_POST['fk_idMuralPublico']) ? addslashes($_POST['fk_idMuralPublico']) : '';

    include_once('../../model/muralClass.php');

    $m = new Mural();

    $retorno = array();

    $resultado = $m->alteraPublicacaoMural($mensagemMural,$exibicaoMural,$fk_idMuralCategoria,$idPublicacao,$fk_idMuralPublico);

    if ($resultado) {
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Publicação alterada com sucesso!';

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao cadastrar a publicação. Tente novamente ou entre em contato com o setor de TI';
    }


    echo json_encode($retorno);
}
?>