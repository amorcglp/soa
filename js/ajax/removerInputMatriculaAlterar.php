<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idImovel 	                     = isset($_REQUEST['idImovel']) ? $_REQUEST['idImovel']  : '';
    $numeroImovelMatricula 	         = isset($_REQUEST['numeroImovelMatricula']) ? $_REQUEST['numeroImovelMatricula']  : '';

    $arr=array();

    include_once('../../model/imovelClass.php');
    $i              = new imovel();
    $retorno        = $i->deletaImovelMatricula($idImovel,$numeroImovelMatricula);

    $arr['alterado'] = false;

    if ($retorno) {
        $arr['alterado'] = true;
    }

    echo json_encode($arr);
}	
?>