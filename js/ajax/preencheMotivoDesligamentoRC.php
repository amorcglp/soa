<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 

    $cod            = isset($_REQUEST['cod'])?$_REQUEST['cod']:null;
    $ocultar_json   = isset($ocultar_json)?$ocultar_json:0;
    $server         = isset($server) ? $server : null;
    //echo "seqCadast: ".$seqCadast;

    // Instancia a classe
    $ws = new Ws($server);

    // Nome do Método que deseja chamar
    $method = 'RetornaTipoDesligamento';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'CodTipoDeslig' => $cod);

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    if($ocultar_json==0)
    {
            // Imprime o retorno
            echo json_encode($return);
    }
}
?>