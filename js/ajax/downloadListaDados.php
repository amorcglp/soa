<?php

date_default_timezone_set('America/Sao_Paulo');

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require '../sec/token.php';	
	}else{
		require './sec/token.php';
	}
}

if($tokenLiberado)
{  
    if(!class_exists("Ws"))
    {
        //require_once '../../lib/functions.php';
        if(realpath('../../model/wsClass.php')){
                require '../../model/wsClass.php';
        }else{
                if(realpath('../model/wsClass.php')){
                        require '../model/wsClass.php';	
                }else{
                        require './model/wsClass.php';
                }
        }
        
        $siglaOA = isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:null;
        
        //Gerar Excel
        $arquivo = 'LISTA-'.$siglaOA.'-'.date('d-m-Y-H:i:s').'.xls';

        header ("Cache-Control: no-cache, must-revalidate");
        header ("Pragma: no-cache");
        header ("Content-type: application/x-msexcel;");
        header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
        header ("Content-Description: PHP Generated Data" );

        $linhaZero = isset($_REQUEST['linhaZero'])?$_REQUEST['linhaZero']:null;
        $linha1 = isset($_REQUEST['linha1'])?$_REQUEST['linha1']:null;
        $linha2 = isset($_REQUEST['linha2'])?$_REQUEST['linha2']:null;
        $linha3 = isset($_REQUEST['linha3'])?$_REQUEST['linha3']:null;
        $linha4 = isset($_REQUEST['linha4'])?$_REQUEST['linha4']:null;
        $linha5 = isset($_REQUEST['linha5'])?$_REQUEST['linha5']:null;
        $linha6 = isset($_REQUEST['linha6'])?$_REQUEST['linha6']:null;
        $linha7 = isset($_REQUEST['linha7'])?$_REQUEST['linha7']:null;
        $linha8 = isset($_REQUEST['linha8'])?$_REQUEST['linha8']:null;
        $linha9 = isset($_REQUEST['linha9'])?$_REQUEST['linha9']:null;
        $linha10 = isset($_REQUEST['linha10'])?$_REQUEST['linha10']:null;
        $linha11 = isset($_REQUEST['linha11'])?$_REQUEST['linha11']:null;
        $linha12 = isset($_REQUEST['linha12'])?$_REQUEST['linha12']:null;
        $linha13 = isset($_REQUEST['linha13'])?$_REQUEST['linha13']:null;
        $linha14 = isset($_REQUEST['linha14'])?$_REQUEST['linha14']:null;
        
        include_once '../../lib/functions.php';
        
        $ocultar_json = 1;
        $saldoRemessaRosacruz = "P";
        $situacaoRemessaRosacruz = 1;
        $situacaoCadastralRosacruz = 1;

        $server = 108;
        include 'retornaRelacaoMembrosPorOaAtuacaoCep.php';
        $arrMembros = $return->result[0]->fields->fArrayMembros;
        //echo "<pre>";
        //print_r($arrMembros);
        $i=1;
        if (count($arrMembros) > 0) {
            ?>
            <table>
                <thead>
                <tr style="height:50px">
                <!--<th rowspan="100"><font color="white">**</font></th>-->
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Item</th>
                <?php if($linhaZero==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Codigo Afiliacao</th>
                <?php }?>
                <?php if($linha1==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Nome</th>
                <?php }?>
                <?php if($linha2==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Telefone Residencial</th>	
                <?php }?>
                <?php if($linha3==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Telefone Comercial</th>
                <?php }?>
                <?php if($linha4==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Celular</th>
                <?php }?>
                <?php if($linha5==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">E-mail</th>
                <?php }?>
                <?php if($linha6==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Rua</th>
                <?php }?>
                <?php if($linha7==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Cidade</th>
                <?php }?>
                <?php if($linha8==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Estado</th>
                <?php }?>
                <?php if($linha9==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff;">Pais</th>
                <?php }?>
                <?php if($linha10==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">CEP</th>
                <?php }?>
                <?php if($linha11==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Grau</th>
                <?php }?>
                <?php if($linha12==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Lote</th>
                <?php }?>
                <?php if($linha13==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Sexo</th>
                <?php }?>
                <?php if($linha14==1){?>
                <th style="border:1px solid black;background-color:#00688B;color:#fff">Data de Nascimento</th>
                <?php }?>
                </tr>
                </thead>
                <tbody>
            <?php
            foreach ($arrMembros as $seq) {

                //Puxar outras informações para guardar na classe do membro em potencial
                // Instancia a classe
                $ws2 = new Ws($server);

                // Nome do Método que deseja chamar
                $method2 = 'RetornaDadosMembroPorSeqCadast';

                // Parametros que serão enviados à chamada
                $params2 = array('CodUsuario' => 'lucianob',
                    'SeqCadast' => $seq);

                // Chamada do método
                $return2 = $ws2->callMethod($method2, $params2, 'lucianob');

                $arrDadosPessoais = $return2->result[0]->fields;

                //echo "->array completo:<pre>".$arrDadosPessoais."<br>";exit();
                $nome                       = utf8_decode($arrDadosPessoais->fNomCliente);
                $sexo                       = utf8_decode($arrDadosPessoais->fIdeSexo);
                $dataNascimento             = utf8_decode($arrDadosPessoais->fDatNascimento);
                $codigoAfiliacao            = utf8_decode($arrDadosPessoais->fCodRosacruz);
                $rua                        = utf8_decode($arrDadosPessoais->fNomLogradouro);
                $numero                     = utf8_decode($arrDadosPessoais->fNumEndereco);
                $complemento                = utf8_decode($arrDadosPessoais->fDesComplementoEndereco);
                $bairro                     = utf8_decode($arrDadosPessoais->fNomBairro);
                $cidade                     = utf8_decode($arrDadosPessoais->fNomCidade);
                $uf                         = utf8_decode($arrDadosPessoais->fSigUf);
                $cep                        = utf8_decode($arrDadosPessoais->fCodCepEndereco);
                $pais                       = utf8_decode($arrDadosPessoais->fSigPaisEndereco);
                $telefone                   = utf8_decode($arrDadosPessoais->fNumTelefone);
                $fax                        = utf8_decode($arrDadosPessoais->fNumFax);
                $telefoneFixo               = utf8_decode($arrDadosPessoais->fNumTelefoFixo);
                $celular                    = utf8_decode($arrDadosPessoais->fNumCelula);
                $outroTelefone              = utf8_decode($arrDadosPessoais->fNumOutroTelefo);
                $descricaoOutroTelefone     = utf8_decode($arrDadosPessoais->fDesOutroTelefo);
                $email                      = utf8_decode($arrDadosPessoais->fDesEmail);
                $lote                       = utf8_decode($arrDadosPessoais->fNumLoteAtualRosacr);
                $grau                       = utf8_decode(retornaGrauRCConformeLote($lote));
                
                ?>
                <tr style="color:#000;">
                        <td align="center" style="border:1px solid black;"><?php echo $i;?></td>
                        <?php if($linhaZero==1){?>
                        <td align="left" style="border:1px solid black;"><?php echo $codigoAfiliacao;?></td>
                        <?php }?>
                        <?php if($linha1==1){?>
                        <td align="left" style="border:1px solid black;"><?php echo $nome;?></td>
                        <?php }?>
                        <?php if($linha2==1){?>
                        <td align="center" style="border:1px solid black;"><?php if(trim($telefoneFixo)!=""){echo $telefoneFixo;}else{ echo $telefone;}?></td>
                        <?php }?>
                        <?php if($linha3==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $outroTelefone;?></td>
                        <?php }?>
                        <?php if($linha4==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $celular;?></td>
                        <?php }?>
                        <?php if($linha5==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $email;?></td>
                        <?php }?>
                        <?php if($linha6==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $rua;?>, <?php echo $numero;?></td>
                        <?php }?>
                        <?php if($linha7==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $cidade;?></td>
                        <?php }?>
                        <?php if($linha8==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $uf;?></td>
                        <?php }?>
                        <?php if($linha9==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $pais;?></td>
                        <?php }?>
                        <?php if($linha10==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $cep;?></td>
                        <?php }?>
                        <?php if($linha11==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $grau;?></td>
                        <?php }?>
                        <?php if($linha12==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $lote;?></td>
                        <?php }?>
                        <?php if($linha13==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo $sexo;?></td>
                        <?php }?>
                        <?php if($linha14==1){?>
                        <td align="center" style="border:1px solid black;"><?php echo substr($dataNascimento,8,2)."/".substr($dataNascimento,5,2)."/".substr($dataNascimento,0,4);?></td>
                        <?php }?>
                </tr>
                <?php
                $i++;
             }?>
	</tbody>
</table>
<?php }
    }
}
    
?>