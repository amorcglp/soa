<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
    require_once '../../sec/token.php';
}else{
    if(realpath('../sec/token.php')){
        require_once '../sec/token.php';
    }else{
        require_once './sec/token.php';
    }
}

if($tokenLiberado)
{
    $dataAdmissao = isset($_REQUEST['dataAdmissao'])? substr($_REQUEST['dataAdmissao'],0,10):null;
    //echo $dataAdmissao;
    $hoje = date('Y-m-d');

    $data1 = new DateTime( $dataAdmissao );
    $data2 = new DateTime( $hoje );

    $intervalo = $data1->diff( $data2 );
    if($intervalo->y > 0)
    {
        echo " {$intervalo->y} anos, {$intervalo->m} meses e {$intervalo->d} dias";
    }else{
        if($intervalo->m > 0)
        {
            echo " {$intervalo->m} meses e {$intervalo->d} dias";
        }else{
            echo " {$intervalo->d} dias";
        }

    }



}