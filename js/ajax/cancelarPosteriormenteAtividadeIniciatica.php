<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idAtividadeIniciatica 	= isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica']  : '';

    $arr=array();

    include_once('../../model/atividadeIniciaticaClass.php');
    $ai = new atividadeIniciatica();
    $retorno = $ai->alteraStatusAtividadeIniciaticaCancelada($idAtividadeIniciatica);

    $arr['sucesso'] = false;
    if ($retorno) {
        $arr['sucesso'] = true;
        $resultado = $ai->cancelaAgendamentoMembroAtividadeIniciatica(null,$idAtividadeIniciatica);
    }

    echo json_encode($arr);
}
?>
