<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    
    $mensagemMural      = isset($_REQUEST['mensagemMural']) ? $_REQUEST['mensagemMural'] : '';
    $seqCadast          = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';
    $fk_idPublicacao 	        = isset($_REQUEST['fk_idPublicacao']) ? $_REQUEST['fk_idPublicacao'] : '';


    include_once('../../model/muralClass.php');

    $m = new Mural();

    $retorno = array();

    $resultado = $m->cadastroComentarioMural($mensagemMural,$seqCadast,$fk_idPublicacao);

    if ($resultado) {
        $retorno['sucesso'] = true;
        $retorno['success'] = 'Comentário cadastrado com sucesso!';

        $t->renovarDataAtualizacaoPublicacao($fk_idPublicacao);

        $buscaPublicacao = $t->buscaPublicacao($fk_idPublicacao);
        if($buscaPublicacao) {
            foreach($buscaPublicacao as $vetorPublicacao){
                $atribuidoPublicacao                  = $vetorPublicacao['atribuidoPublicacao'];
                $criadoPorPublicacao                  = $vetorPublicacao['criadoPorPublicacao'];
            }
            if($atribuidoPublicacao && $criadoPorPublicacao){
                $t->alteraStatusPublicacao(0,$fk_idPublicacao,$atribuidoPublicacao);
                $t->alteraStatusPublicacao(0,$fk_idPublicacao,$criadoPorPublicacao);
            } else {
                $t->alteraStatusPublicacao(0,$fk_idPublicacao);
            }
        }
        $t->alteraStatusPublicacao(1,$fk_idPublicacao,$fk_seqCadastRemetente);

    } else {
        $retorno['sucesso'] = false;
        $retorno['erro'] = 'Ocorreu algum erro ao enviar a notificação. Tente novamente ou entre em contato com o setor de TI';
    }


    echo json_encode($retorno);
}
?>