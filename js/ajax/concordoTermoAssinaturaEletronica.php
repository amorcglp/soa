<?php

$retorno=array();

$textoImpressao="

Saudações em todas as pontas do nosso Sagrado Triângulo!<br><br>

A partir deste momento a sua assinatura passará a ser eletrônica. O processo de adesão funciona através de um método de autenticação de dois fatores que garantirão que você é realmente o dono deste login.<br><br>

A primeira etapa é a utilização de sua senha pessoal de acesso ao SOA e a segunda etapa é a utilização de um código de ativação que será enviado para o seu número de celular ou e-mail cadastrado no sistema de membros. O processo de ativação é individual e intransferível.<br><br>

As assinaturas de documentos eletrônicos herdam as mesmas responsabilidades dos documentos antes assinados de “próprio punho”, portanto, concordando com este termo você está ciente que precisa manter essas informações em sigilo preservando sua identidade eletrônica enquanto membro oficial da AMORC-GLP e também se responsabiliza pelas informações registadas nos relatórios assinados por você.<br><br>

Todo documento assinado por você será guardado em histórico para o seu acompanhamento.<br><br>

Este presente termo será encaminhado para seu e-mail.<br><br>

Na certeza de que saberá valorizar este Sagrado Legado,<br><br>

Confio-lhe!<br><br>

Antiga e Mística Ordem Rosacruz – Grande Loja da Jurisdição de Língua Portuguesa

";

$seqCadast 	= isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;

include_once('../../lib/functions.php');
include_once('../../model/usuarioClass.php');
require_once '../../webservice/wsInovad.php';

$u = new Usuario();
$codigo= aleatorioTermo();
if($u->concordoAssinatura($seqCadast,$codigo))
{
    $resultadoUsuario = $u->buscaUsuario($seqCadast);
    $emailUsuario="";
    $celular="";

    $email="";
    $celular="";

    if($resultadoUsuario)
    {
        foreach ($resultadoUsuario as $v)
        {
            $arrNome        = explode(" ",$v['nomeUsuario']);
            $emailUsuario   = $v['emailUsuario'];
            $celularUsuario = $v['celularUsuario'];
        }
    }

    $array = [0 => $seqCadast];
    $teste = '';
    foreach ($array as $key => $value){
        if($teste != ''){
            $teste = $teste.', '.$value;
        }else{
            $teste = $value;
        }
    }

//Buscar Nome do Usuário vindo do ORCZ
    $vars = array('seq_cadast' => $teste);
    $resposta3 = json_decode(json_encode(restAmorc("membros",$vars)),true);;
    $obj3 = json_decode(json_encode($resposta3),true);
    //echo "<pre>";print_r($obj3['data'][0]);
    $status=0;

    if(isset($obj3['data'][0]['nom_client']))
    {
        $status=1;
        $email = trim($obj3['data'][0]['des_email']);
        $celular=$obj3['data'][0]['num_celula'];
    }

    if($email==""){
        $email = $emailUsuario;
    }

    if($celular==""){
        $celular = $celularUsuarioUsuario;
    }

    if( substr($email, -1)==".")
    {
        $size = strlen($email);

        $email = substr($email,0, $size-1);
    }

    /*
    Enviar E-mail para o usuário dizendo que ele acaba de aderir a Assinatura
    */

    //echo "teste aqui-".$celular."-".$email."/";


    if($celular!="") {

        //ENDEREÇO IP DA PLATAFORMA QUE VAI FAZER O LOGIN DO CLIENTE
        $PLATAFORMA = "corporativo.allcancesms.com.br";

        $telefone = str_replace(" ", "", str_replace("-", "", str_replace(")", "", str_replace("(", "", $celular)))); //NÚMERO DO DESTINO

        if (strlen($telefone) == 10)//Se tiver 10 caracteres acrescentar 9 depois do ddd
        {
            $telefone = substr($telefone, 0, 2) . "9" . substr($telefone, 2, 8);
        }

        //$telefone 			 = '41997055545'; //Cleu
        //$arrNome[0]          = "LUCIANO JEDAI";//Nome da msg
        //$telefone 			 = '4196516900'; //Luciano
        $usuario = 'AMORC'; //USUARIO PLATAFORMA
        $senha = '9611'; //SENHA PLATAFORMA
        $textoSms = $arrNome[0] . ", seu código de Adesão para Assinatura Eletronica no SOA é:" . $codigo;
        //echo "telefone:".$telefone;

        if ($telefone != 0 && $telefone != "") {


            $mensagem = $textoSms;

            //CODIFICAR A MENSAGEM

            $mensagem = urlencode($mensagem);
            //echo "<br>msg=>" . $mensagem . "<br>";
            //ENVIA MENSAGEM

            $url = "http://$PLATAFORMA/app/modulo/api/index.php?action=sendsms&lgn=$usuario&pwd=$senha&numbers=$telefone&msg=$mensagem";

            $cURL = curl_init('$url');
            curl_setopt($cURL, CURLOPT_URL, $url);
            curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
            $resultado = curl_exec($cURL);
            curl_close($cURL);

            $resJson = json_decode($resultado, false);
            $status = $resJson->status;

            if ($status == '0') {
                $r = $u->atualizaNotificacaoSMSAdesaoAssinaturaEletronica($seqCadast, 0);
            } else {
                $r = $u->atualizaNotificacaoSMSAdesaoAssinaturaEletronica($seqCadast, 1);
            }

        }
    }

    if($email!="") {

        include '../../mailgun.php';


        $texto = '<div>
            <div dir="ltr">
                    <table class="ecxbody-wrap"
                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                            <tbody>
                                    <tr
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                            <td
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                            <td class="ecxcontainer" width="600"
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                    valign="top">
                                                    <div class="ecxcontent"
                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                            <table class="ecxmain" width="100%" cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                    <tbody>
                                                                            <tr
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                    <td class="ecxcontent-wrap"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                            valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                            style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                            alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                    <br>
                                                                                    <br>
                                                                                    <br>
                                                                                            <div style="text-align: left;"></div>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                    <tbody>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top"><b>' . $arrNome[0] . '</b><br><br><hr>
                                                                                                                            <b>Seu código para aderir Assinatura Eletrônica é: ' . $codigo . '</b>
                                                                                                                            <hr><br>' . $textoImpressao . '</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top"><!--AMORC - Ordem Rosacruz--></td>
                                                                                                            </tr>
                                                                                                    </tbody>
                                                                                            </table></td>
                                                                            </tr>
                                                                    </tbody>
                                                            </table>
                                                            <div class="footer"
                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                    <div style="text-align: left;"></div>
                                                                    <table width="100%"
                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <tbody>
                                                                                    <tr
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                            <td class="ecxaligncenter ecxcontent-block"
                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                    align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2015</td>
                                                                                    </tr>
                                                                            </tbody>
                                                                    </table>
                                                            </div>
                                                    </div>
                                                    <div style="text-align: left;"></div></td>
                                            <td
                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                    </tr>
                            </tbody>
                    </table>
                    </div>
            </div>';

        /*
        $servidor = "smtp.office365.com";
        $porta = "587";
        $usuario = "noresponseglp@amorc.org.br";
        $senha = "@w2xpglp";
        */
        /*
        $servidor = "smtp.outlook.com";
        $porta = "465";
        $usuario = "samufcaldas@hotmail.com.br";
        $senha = "safc3092";
        */
        /*
        $servidor = "smtp.gmail.com";
        $porta = "465";
        $usuario = "cpdglp@gmail.com";
        $senha = "@w2xpglp";


        #Disparar email
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPSecure = "ssl";
        $mail->SMTPAuth = true;
        $mail->Host = $servidor; // SMTP utilizado
        $mail->Port = $porta;
        $mail->Username = $usuario;
        $mail->Password = $senha;
        $mail->From = 'cpdglp@gmail.com';
        $mail->AddReplyTo("atendimento@amorc.org.br", "Antiga e Mística Ordem Rosacruz - AMORC");
        $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
        //$mail->AddAddress("samufcaldas@hotmail.com.br");
        //$mail->AddAddress("tatiane.jesus@amorc.org.br");
        //$mail->AddAddress("lucianob@amorc.org.br");
        //$mail->AddAddress("braz@amorc.org.br");
        $mail->AddAddress($arr[0]['emailUsuario']);
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
        $mail->Subject = 'Token de Acesso - '.$arrFuncionalidade[0]['nomeFuncionalidade'].$stringCabecalho.' - SOA';
        $mail->Body = $texto;
        $mail->AltBody = strip_tags($texto);
        $enviado = $mail->Send();
        */

        $emailFrom = "Atendimento <atendimento@amorc.org.br>";
        $subject = "Assinatura Eletrônica - SOA";

        $enviado = $mgClient->sendMessage($domain, array(
            'from' => $emailFrom,
            'to' => strtoupper($arrNome[0]) . ' <' . $email . '>',
            'subject' => $subject,
            'text' => 'Seu e-mail não suporta HTML',
            'html' => $texto));

        //echo "<pre>";print_r($enviado);

        if ($enviado) {
            $r = $u->enviouEmailAssinatura($seqCadast);
        } else {
            $r = $u->erroEmailAssinatura($seqCadast);
        }

        //Gerar log

        $emailTo = $email;
        $html = $texto;

        date_default_timezone_set('America/Sao_Paulo');
        $dataHorarioEnvio           = date("D, d M Y G:i:s")." GMT"; ; //Formato: 'Thu, 13 Oct 2011 18:02:00 GMT' (Formato: RFC 2822)
        $funcionalidadeNoSistema    = "concordoTermoAssinaturaEletronica";//Normalmente apontar para o arquivo para facilitar a identificação

        include '../../email_events/create_log.php'; // Arquivo que gera o log do email

    }



    //Montar Meio email e celular
    $arrEmail = explode("@",$email);
    $retorno['meioEmail'] = substr($email, 0, 4)."*******".substr($arrEmail[1],2,100);
    $retorno['meioCelular'] = substr($celular, 0, 8)."***";

    $retorno['status'] = '1';
}else{
    $retorno['status'] = '0';
    $retorno['meioEmail'] ='';
    $retorno['meioCelular'] ='';

}

echo json_encode($retorno);


?>