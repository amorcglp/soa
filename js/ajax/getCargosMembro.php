<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado) {

    $seqCadast = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : null;

    /**
     * Código legado: busca feita no WebService da Actumplus, o qual não identifica novas regras de negócio referentes ao status de atuação do oficial

    $ocultar_json=1;
    $naoAtuantes='N';
    $atuantes='S';

    include '../../js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return),true);
    //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayOficiais']);
    $arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];

    <td><?php echo $vetor['fields']['fDesFuncao'];?></td>
    <td><?php echo substr($vetor['fields']['fDatEntrad'],0,4);?></td>
    <td><?php echo $vetor['fields']['fNomeOa'];?></td>
     */

    include_once("../../model/funcaoUsuarioClass.php");
    $fu = new funcaoUsuario();
    $arrFuncoes = $fu->buscaPorMaisDeUmaFuncao($seqCadast);

    if($arrFuncoes) { ?>
    <table class="table">
        <thead>
            <tr>
                <th>Cargos</th>
                <th>Ano</th>
                <th>Organismo</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($arrFuncoes as $vetor) { ?>
            <tr>
                <td><?php echo $vetor['nomeFuncao'];?></td>
                <td><?php echo substr($vetor['dataInicioMandato'],0,4);?></td>
                <td><?php echo $vetor['siglaOA'];?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php
    } else {
        echo "Não possui";
    }
}
?>