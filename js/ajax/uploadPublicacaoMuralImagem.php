<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    /*
    Uploadify
    Copyright (c) 2012 Reactive Apps, Ronnie Garcia
    Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
    */

    include_once("../../model/muralClass.php");
    $mural = new Mural();
    //echo $fkDocumento."<br>";

    // Define a destination
    //$targetFolder = '/img/mural/temp/'; // Relative to the root
    $targetFolder = '/soa/img/imovel/anexo/'; // Relative to the root
    
    if (!empty($_FILES)) {

        // Validate the file type
        $fileTypes                  = array('jpg','jpeg','png'); // File extensions
        $fileParts                  = pathinfo($_FILES['Filedata']['name']);
        $fileParts['extension']     = strtolower($fileParts['extension']);
        $tempFile                   = $_FILES['Filedata']['tmp_name'];
        $targetPath                 = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
        $newName                    = md5(uniqid(time())) . "." . $fileParts['extension'];
        $targetFile                 = rtrim($targetPath,'/') . '/' . $newName;
        $pathBd                     = '/img/mural/temp/' . $newName;
        //$pathBd                     = '/soa/img/imovel/anexo/' . $newName;
        $data                       = 'NOW()';
        //print_r($_FILES);

        //echo $fileParts['extension'];

        if (in_array($fileParts['extension'],$fileTypes)) {
            if (move_uploaded_file($tempFile,$targetFile)) {
                    /*
                    $retorno = $mural->cadastroPublicacaoImagem($_FILES['Filedata']['name'],$newName,$fileParts['extension'],$pathBd,$data,'0');
                    //echo $retorno;
                    if ($retorno!=0) {
                            echo 1;
                    }
                    else {
                            unlink($targetFile);
                            echo 'Ocorreu algum erro ao inserir o anexo. Tente novamente mais tarde!';
                    }
                    */
                    echo 1;
            }
            else {
                echo 'Ocorreu algum erro ao inserir o anexo. Tente novamente mais tarde!';
            }
        } else {
            echo 'Tipos de arquivo suportados (PDF, JPG, JPEG, PNG OU GIF). Em caso de duvidas entre em contato com o setor de TI';
        }

    }
    else {
            echo 'O arquivo que você está tentando enviar é muito grande ou o servidor não possuí mais espaço disponível. Entre em contato com o setor de TI.';	
    }
}
?>