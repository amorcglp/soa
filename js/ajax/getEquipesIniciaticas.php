<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idOrganismo = isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';
    $ano         = isset($_REQUEST['ano']) ? $_REQUEST['ano'] : '';

    //echo "Ano: ".$ano;
    //echo "ID Organismo: ".$idOrganismo;

    include_once("../../model/atividadeIniciaticaOficialClass.php");
    $aiomodel = new atividadeIniciaticaOficial();

    $equipe['tipoAtividadeIniciaticaOficial']=array();

    $resultadoEquipes 	= $aiomodel->retornaEquipesJaCadastrados($idOrganismo,$ano);
    if ($resultadoEquipes) {
            foreach ($resultadoEquipes as $vetorEquipes) {
            $equipe['tipoAtividadeIniciaticaOficial'][] = $vetorEquipes['tipoAtividadeIniciaticaOficial'];
            }
    }
    /*
    echo "<pre>";
    print_r($equipe['tipoAtividadeIniciaticaOficial']);
    echo "</pre>";
    */

    $retorno=array();
    $retorno['atividade'] = array();

    for ($i = 1; $i <= 3; $i++) {

            if(!in_array($i,$equipe['tipoAtividadeIniciaticaOficial'])){
            $retorno['atividade']['equipes'][] = '<option value="'.$i.'">'.$i.'</option>';
            }

    }

    echo json_encode($retorno);
}