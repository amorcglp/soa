<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 

    if(!isset($nomeMembro))
    {
            $nomeMembro				= isset($_REQUEST['nomeMembro'])?$_REQUEST['nomeMembro']:"";
    }

    if(!isset($codigoAfiliacao))
    {
            $codigoAfiliacao			= isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    }

    if(!isset($tipoMembro))
    {
            $tipoMembro				= isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:0;
    }

    //Tratar nome antes de enviar para o webservice
    $nomeMembro=urlencode($nomeMembro);


    $ocultar_json 			= isset($ocultar_json)?$ocultar_json:0;

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaCadastrosFonetica';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'TipoMembro' => $tipoMembro,
                                    'CodMembro' => $codigoAfiliacao,
                                    'NomeMembro' => $nomeMembro);

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');
    if($return)
    {
        if($return->result[0]->fields->fQtdMembros>0)
        {    
            unset($return->result[0]->fields->fArrayMembros[0]->fields->fNomLogradouro);
            unset($return->result[0]->fields->fArrayMembros[0]->fields->fNumEndereco);
            unset($return->result[0]->fields->fArrayMembros[0]->fields->fDesComplementoEndereco);
            unset($return->result[0]->fields->fArrayMembros[0]->fields->fCodCepEndereco);
        }
    }
    if($ocultar_json==0)
    {
            // Imprime o retorno
            echo json_encode($return);
    }
}
?>