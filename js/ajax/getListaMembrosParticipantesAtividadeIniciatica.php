<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $idAtividadeIniciatica		= isset($_POST['idAtividadeIniciatica']) ? addslashes($_POST['idAtividadeIniciatica']) : '';

    include_once("../../model/atividadeIniciaticaClass.php");
    $aim = new atividadeIniciatica();

    $retorno=array();
    $retorno['membros'] = array();
    $retorno['qtdMembros'] = 0;

    $listaAtividadeIniciaticaMembros      = $aim->listaAtividadeIniciaticaMembros($idAtividadeIniciatica);
    if ($listaAtividadeIniciaticaMembros) {
        foreach ($listaAtividadeIniciaticaMembros as $vetorAtividadeIniciaticaMembros) {
            $status = $vetorAtividadeIniciaticaMembros['statusAtividadeIniciaticaMembro'];

            switch ($status) {
                case 0: $spamStatus = "<span class='badge badge-warning'>Convidado</span>"; break;
                case 1: $spamStatus = "<span class='badge badge-success'>Confirmado</span>"; break;
                case 2: $spamStatus = "<span class='badge badge-primary compareceu'>Compareceu</span>"; break;
                case 3: $spamStatus = "<span class='badge badge-danger'>Cancelado</span>"; break;
								case 4: $spamStatus = "<span class='badge badge-danger' style='max-width: 200px'>".$vetorMembros['mensagemAtividadeIniciaticaMembro']."</span>"; break;
            }

            if($status == 0){
                $botao = "<a class='btn-xs btn-success' onclick='confirmaAgendamentoMembroAtividadeIniciatica(".$vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro']."," . $idAtividadeIniciatica . ")'>Confirmar</a>&nbsp<a class='btn-xs btn-danger' onclick='cancelaAgendamentoMembroAtividadeIniciatica(".$vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro']."," . $idAtividadeIniciatica . ")'>Cancelar</a>";
            }
            elseif ($status == 1){
                $botao = "<a class='btn-xs btn-danger' onclick='cancelaAgendamentoMembroAtividadeIniciatica(".$vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro'] . "," . $idAtividadeIniciatica . ")'>Cancelar</a>&nbsp<a class='btn-xs btn-primary' onclick='confirmaParticipacaoMembroAtividadeIniciatica(".$vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro'].",". $idAtividadeIniciatica .")'>Presente</a>";
            }
            elseif ($status == 2){
                $botao = "--";
            }
            else {
                $botao = "--";
            }

            $arrNome				= explode(" ", $vetorAtividadeIniciaticaMembros['nomeAtividadeIniciaticaMembro']);
            $arrNomeDecrescente		= $arrNome[count($arrNome)-1];
            $nome = ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNome[1],'UTF-8'));
            if(isset($arrNome[2])){
                $nome .= " ".ucfirst(mb_strtolower($arrNome[2],'UTF-8'));
            }
            if(isset($arrNome[3])){
                $nome .= " ".ucfirst(mb_strtolower($arrNome[3],'UTF-8'));
            }

            $retorno['membros'][] =    "<tr><td>" . $nome . "</td><td><center><div id='statusMembroAtividade" . $vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro'] . $idAtividadeIniciatica ."'>" . $spamStatus . "</div></center></td><td><center><div id='acoesMembroAtividade" . $vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro'] . $idAtividadeIniciatica . "'>" . $botao . "</div></center></td></tr><input type='hidden' class='" . $idAtividadeIniciatica . $vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro'] . "' id='acoesMembroAtividade" . $vetorAtividadeIniciaticaMembros['seqCadastAtividadeIniciaticaMembro'] . "' name='membros" . $idAtividadeIniciatica . "[]' value='" . $status . "'>";
            $retorno['qtdMembros'] ++;
        }
    } else {
        $retorno['qtdMembros'] = 0;
    }

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}
