<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            require_once './model/wsClass.php';
    }
    
    if(realpath('../../model/criaSessaoClass.php')){
            require_once '../../model/criaSessaoClass.php';
    }else{
            require_once './model/criaSessaoClass.php';
    }
    
    $sessao = new criaSessao();

    $siglaOrganismoAfiliado			= isset($_REQUEST['siglaOrganismoAfiliado'])?$_REQUEST['siglaOrganismoAfiliado']:'';
    $codAfiliacaoMembroOficial		= isset($_REQUEST['codAfiliacaoMembroOficial'])?$_REQUEST['codAfiliacaoMembroOficial']:0;
    $tipoMembro						= isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:0;
    $tipoCargo						= isset($_REQUEST['tipoCargo'])?$_REQUEST['tipoCargo']:0;
    $funcao							= isset($_REQUEST['funcao'])?$_REQUEST['funcao']:0;
    $dataEntrada					= isset($_REQUEST['dataEntrada'])?$_REQUEST['dataEntrada']:'';
    $dataSaida						= isset($_REQUEST['dataSaida'])?$_REQUEST['dataSaida']:'';
    $dataTerminoMandato				= isset($_REQUEST['dataTerminoMandato'])?$_REQUEST['dataTerminoMandato']:'';
    $companheiro					= isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:'';
    $usuario                            = $sessao->getValue("seqCadast");
    /*****
     * Formatar Datas para o formato ISO
     *****/
    if($dataEntrada!="")
    {
            $dataEntrada = substr($dataEntrada, 6,4)."-".substr($dataEntrada, 3,2)."-".substr($dataEntrada, 0,2)."T".str_pad(rand(1, 23), 2, "0", STR_PAD_LEFT).":".str_pad(rand(1, 59), 2, "0", STR_PAD_LEFT).":".str_pad(rand(1, 59), 2, "0", STR_PAD_LEFT);
    }
    if($dataSaida!="")
    {
            $dataSaida = substr($dataSaida, 6,4)."-".substr($dataSaida, 3,2)."-".substr($dataSaida, 0,2)."T00:00:00";
    }
    if($dataTerminoMandato!="")
    {
            $dataTerminoMandato = substr($dataTerminoMandato, 6,4)."-".substr($dataTerminoMandato, 3,2)."-".substr($dataTerminoMandato, 0,2)."T00:00:00";
    }
    
    //Gerar log do registro da função para o oficial
    if($dataEntrada!=""&&$dataTerminoMandato!="")
    {
        if(realpath('../../model/logRegistroOficialClass.php')){
                require_once '../../model/logRegistroOficialClass.php';
        }else{
                require_once './model/logRegistroOficialClass.php';
        }
        //echo $companheiro;
        $log = new logRegistroOficial();
        $log->setSiglaOrganismoAfiliado($siglaOrganismoAfiliado);
        $log->setCodAfiliacao($codAfiliacaoMembroOficial);
        $log->setTipoMembro($tipoMembro);
        $log->setTipoCargo($tipoCargo);
        $log->setFuncao($funcao);
        $log->setDataEntrada(substr($dataEntrada,0,10));
        $log->setDataTermino(substr($dataTerminoMandato,0,10));
        $log->setCompanheiro($companheiro);
        $log->setUsuario($usuario);
        if($log->cadastro())
        {    

            // Instancia a classe
            $ws = new Ws();

            // Nome do Método que deseja chamar
            $method = 'AtualizarOficial';

            // Parametros que serão enviados à chamada
            $params = array('CodUsuario' => 'lucianob',
                                            'CodMembro' => $codAfiliacaoMembroOficial,
                                            'IdeTipoMembro' => $tipoMembro,
                                            'IdeCompanheiro' => $companheiro,
                                            'SigOrgafi' => $siglaOrganismoAfiliado,
                                            'SeqTipoFuncaoOficial' => $tipoCargo,
                                            'SeqFuncao' => $funcao,
                                            'DatHoraEntrada' => $dataEntrada,
                                            'DatSaida' => $dataSaida,
                                            'DatTerminMandato' => $dataTerminoMandato,
                                            'IdeTipoMotivoSaida' => '' 
                                    );

            // Chamada do método
            $return = $ws->callMethod($method, $params, 'lucianob');

            // Imprime o retorno
            echo json_encode($return);
        }    
    }
}
?>