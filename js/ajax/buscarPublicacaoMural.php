<?php

require_once('../../lib/functions.php');

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    $idPublicacao       = isset($_POST['idPublicacao']) ? addslashes($_POST['idPublicacao']) : '';
	$seqCadastUsuarioLogado		= isset($_POST['seqCadastUsuarioLogado']) ? addslashes($_POST['seqCadastUsuarioLogado']) : '';


    include_once("../../model/muralClass.php");
    $mural = new Mural();

    $retorno=array();
    $retorno['qtdVetor'] = 0;

    $i=0;

    //echo "<pre>";print_r($filtroCategorias);echo "</pre>";
    
    if($mural->jaVisualizado($idPublicacao, $seqCadastUsuarioLogado) == 0){
        $mural->visualizarPublicacao($idPublicacao, $seqCadastUsuarioLogado);
    }

    $publicacao      = $mural->buscarPublicacaoMural($idPublicacao, $seqCadastUsuarioLogado);
    if ($publicacao) {
        foreach ($publicacao as $vetorPublicacao) {
            $mensagemMural                  = $vetorPublicacao['mensagemMural'];
            $dtCadastroMural                = substr($vetorPublicacao["dtCadastroMural"],8,2)."/".
                                                substr($vetorPublicacao["dtCadastroMural"],5,2)."/".
                                                substr($vetorPublicacao["dtCadastroMural"],0,4) . " às " . 
                                                substr($vetorPublicacao["dtCadastroMural"],10,6);
            $dtAtualizacaoMural             = $vetorPublicacao['dtAtualizacaoMural'];
            $exibicaoMural                  = $vetorPublicacao['exibicaoMural'];
            $status                         = $vetorPublicacao['status'];
            $fk_idMuralImagem               = $vetorPublicacao['fk_idMuralImagem'];
            $fk_idMuralCategoria            = $vetorPublicacao['fk_idMuralCategoria'];
            $fk_idMuralPublico              = $vetorPublicacao['fk_idMuralPublico'];
            $fk_idRegiaoRosacruz            = $vetorPublicacao['fk_idRegiaoRosacruz'];
            $fk_idOrganismoAfiliado         = $vetorPublicacao['fk_idOrganismoAfiliado'];
            $fk_seqCadastAtualizadoPor      = $vetorPublicacao['fk_seqCadastAtualizadoPor'];

            $classificacaoOrganismoAfiliado = $vetorPublicacao['classificacaoOrganismoAfiliado'];
            $nomeOrganismoAfiliado          = $vetorPublicacao['nomeOrganismoAfiliado'];
            $siglaOrganismoAfiliado         = $vetorPublicacao['siglaOrganismoAfiliado'];

            $avatarUsuarioPublicacao        = $vetorPublicacao['avatarUsuario'];
            $nomeUsuarioPublicacao          = retornaNomeFormatado($vetorPublicacao['nomeUsuario'],3);

            $retorno['favorito']            = $vetorPublicacao['favorito'];


            switch ($classificacaoOrganismoAfiliado) {
                case 1: $nomeCompletoOa = "Loja "; break;
                case 2: $nomeCompletoOa = "Pronaos "; break;
                case 3: $nomeCompletoOa = "Capítulo "; break;
                case 4: $nomeCompletoOa = "Heptada "; break;
                case 5: $nomeCompletoOa = "Atrium "; break;
            }
            $nomeCompletoOa .= $nomeOrganismoAfiliado . " - " . $siglaOrganismoAfiliado;            

            if($fk_idMuralCategoria == 1){
                $categoria = "Atividades";
            } elseif($fk_idMuralCategoria == 2){
                $categoria = "Eventos";
            } else{
                $categoria = "Notícias";
            }

            if($fk_idMuralPublico == 1){
                $publico = "Rosacruz";
            } elseif($fk_idMuralPublico == 2){
                $publico = "Martinista";
            } else{
                $publico = "OGG";
            }

            if($exibicaoMural == 4)
            {
                $exibicaoPara = "a GLP";
            }elseif($exibicaoMural == 1){
                $exibicaoPara = "todos";
            } elseif($exibicaoMural == 2){
                $exibicaoPara = "a região";
            } else{
                $exibicaoPara = "o organismo";
            }

            if($fk_idMuralImagem == 0){
                $imagemPublicacao = "";
            } else {
                $imagemPublicacao = $mural->retornaFullPathImagemPeloId($fk_idMuralImagem);
                $imagemPublicacao = '<br><center><img src="'.$imagemPublicacao.'" class="img-responsive" style="width: 60%"><center>';
                //$imagemPublicacao = '<img src="img/mural/11.jpg" class="img-responsive">';

            }
            

            $retorno['headerTitulo']            = $nomeCompletoOa . " | " . $dtCadastroMural;
            $retorno['headerDados']             = "Tipo da Postagem: " . $categoria . "| Público:".$publico."| Exibido para " . $exibicaoPara;


            $retorno['publicacaoAvatar']        = '<img alt="'.$nomeUsuarioPublicacao.'" src="'.$avatarUsuarioPublicacao.'" class="img-circle">';
            $retorno['publicacaoNome']          = $nomeUsuarioPublicacao;
            $retorno['publicacaoData']          = $dtCadastroMural;
            $retorno['publicacaoMensagem']      = $mensagemMural;
            $retorno['publicacaoImagem']        = $imagemPublicacao;

            if($vetorPublicacao['favorito'] == 1){
                $retorno['btnFavorito'] = <<<EOT
                <button type="button" class="btn btn-outline btn-warning" id="btnFavoritoModal" onClick="desfavoritarPublicacaoAdminModal()">Favoritar</button>
EOT;
            } else {
                $retorno['btnFavorito'] = <<<EOT
                <button type="button" class="btn btn-outline btn-warning" id="btnFavoritoModal" onClick="favoritarPublicacaoAdminModal()">Favoritar</button>
EOT;
            }

            if($status == 1){
                $retorno['btnExclusao'] = <<<EOT
                <button type="button" class="btn btn-outline btn-warning" id="btnExclusaoModal" onClick="alterarStatusPublicacaoMuralAdminModal(0)">Inativar</button>
EOT;
            } else {
                $retorno['btnExclusao'] = <<<EOT
                <button type="button" class="btn btn-outline btn-warning" id="btnExclusaoModal" onClick="alterarStatusPublicacaoMuralAdminModal(1)">Ativar</button>
EOT;
            }

            $retorno['comentario']              = "";

            $comentarios      = $mural->listaComentarioPelaPublicacao($idPublicacao,true);
            if($comentarios){
                foreach($comentarios as $comentarioVetor){

                    $idComentario                   = $comentarioVetor['idMuralComentario'];
                    $mensagemMuralComentario        = $comentarioVetor['mensagemMuralComentario'];
                    $dtCadastroComentario           = substr($comentarioVetor['dtCadastroMuralComentario'],8,2)."/".substr($comentarioVetor['dtCadastroMuralComentario'],5,2)."/".substr($comentarioVetor['dtCadastroMuralComentario'],0,4) . " às " . substr($comentarioVetor['dtCadastroMuralComentario'],10,6);
                    $dtCadastroComentarioTimeAgo    = timeAgo($comentarioVetor['dtCadastroMuralComentario']);
                    $dtAtualizacaoComentario        = substr($comentarioVetor['dtAtualizacaoMuralComentario'],8,2)."/".substr($comentarioVetor['dtAtualizacaoMuralComentario'],5,2)."/".substr($comentarioVetor['dtAtualizacaoMuralComentario'],0,4) . " às " . substr($comentarioVetor['dtAtualizacaoMuralComentario'],10,6);
                    $dtAtualizacaoComentarioTimeAgo = timeAgo($comentarioVetor['dtAtualizacaoMuralComentario']);
                    $nomeUsuarioComentario          = retornaNomeFormatado($comentarioVetor['nomeUsuario'],3);
                    $avatarUsuarioComentario        = $comentarioVetor['avatarUsuario'];
                    $statusComentario               = $comentarioVetor['status'];

                    if($statusComentario == 1){
                        $btnExclusaoComentario = <<<EOT
                            <button type="button" 
                                id="btnExclusaoComentario-$idComentario"
                                onClick="alterarStatusComentarioPublicacaoMuralAdmin($idComentario, 0)" 
                                class="btn btn-danger btn-xs">
                                Inativar
                            </button>
EOT;
                        $linethrough = "";
                    } else {
                        $btnExclusaoComentario = <<<EOT
                            <button type="button" 
                                id="btnExclusaoComentario-$idComentario"
                                onClick="alterarStatusComentarioPublicacaoMuralAdmin($idComentario, 1)" 
                                class="btn btn-primary btn-xs">
                                Ativar
                            </button>
EOT;
                        $linethrough = "line-through";
                    }

                    $retorno['comentario'] .= <<<EOT
                        <div class="feed-element" id="comentario-$idComentario">
                            <a class="pull-left">
                                <img alt="$nomeUsuarioComentario" class="img-circle" src="$avatarUsuarioComentario">
                            </a>
                            <div class="media-body ">
                                <small class="pull-right">
                                    $btnExclusaoComentario
                                </small>
                                <strong>$nomeUsuarioComentario</strong>
                                <small class="text-muted">$dtCadastroComentario</small>
                                <div class="well" id="msg-comentario-$idComentario" style="text-decoration: $linethrough;">
                                    $mensagemMuralComentario
                                </div>
                            </div>
                        </div>
EOT;
                }
            } else {
                $retorno['comentario'] = <<<EOT
                    Nenhum comentário cadastrado.
EOT;
            }

            
			$i++;
        }
    }
    $retorno['qtdVetor'] = $i;

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}
