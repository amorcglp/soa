<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $departamento				= isset($_REQUEST['departamento']) ? json_decode($_REQUEST['departamento']) : '';
    $nivel			 			= isset($_REQUEST['nivel']) ? json_decode($_REQUEST['nivel']) : '';
    $salvar 					= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/departamentoNivelDePermissaoClass.php');
    $dndp = new DepartamentoNivelDePermissao();
    $dndp->setFkIdDepartamento($departamento);
    $dndp->setFkIdNivelDePermissao($nivel);

    if($salvar==0)
    {
            $retorno = $dndp->remove();
    }
    if($salvar==1)
    {
            $retorno = $dndp->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>