<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idImovel 		= isset($_REQUEST['idImovel']) ? $_REQUEST['idImovel'] : '';
    $anoCadastrar 	= isset($_REQUEST['ano']) ? $_REQUEST['ano'] : '';
    //$ano = 0;

    include_once("../../model/imovelControleClass.php");
    $icmodel = new imovelControle();

    $resultadoAnos 	= $icmodel->retornaAnosJaCadastrados($idImovel);

    $ano['referenteAnoImovelControle']=array();
    if ($resultadoAnos) {
            foreach ($resultadoAnos as $vetorAno) {
                    $ano['referenteAnoImovelControle'][] = $vetorAno['referenteAnoImovelControle'];
            }
    }
    //echo "<pre>";print_r($ano);

    $retorno=array();
    $retorno['imoveis'] = array();


    for ($i = date("Y"); $i >= 2010; $i--) {

            if(!in_array($i,$ano['referenteAnoImovelControle'])){
                    if($anoCadastrar == $i){
                            $retorno['imoveis']['anos'][] = '<option value="'.$i.'" selected>'.$i.'</option>';
                    } else {
                            $retorno['imoveis']['anos'][] = '<option value="'.$i.'">'.$i.'</option>';
                    }
            }

    }

    //echo "<pre>";print_r($retorno['imoveis']['anos']);

    echo json_encode($retorno);
}    