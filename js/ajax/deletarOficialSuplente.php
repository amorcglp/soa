<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $seqCadast                  = isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';
    $tipoSuplente               = isset($_REQUEST['tipoSuplente']) ? $_REQUEST['tipoSuplente'] : '';
    $idAtividadeIniciatica      = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';

    include_once('../../model/atividadeIniciaticaClass.php');

    $retorno = Array();
    $retorno['resposta'] = false;

    if($idAtividadeIniciatica != ''){
        $aim = new atividadeIniciatica();

        switch($tipoSuplente){
            case 'mestre': $tipoSuplente = 1; break;
            case 'mestreAux': $tipoSuplente = 2; break;
            case 'arquivista': $tipoSuplente = 3; break;
            case 'capelao': $tipoSuplente = 4; break;
            case 'matre': $tipoSuplente = 5; break;
            case 'grandeSacerdotisa': $tipoSuplente = 6; break;
            case 'guia': $tipoSuplente = 7; break;
            case 'guardiaoInterno': $tipoSuplente = 8; break;
            case 'guardiaoExterno': $tipoSuplente = 9; break;
            case 'archote': $tipoSuplente = 10; break;
            case 'medalhista': $tipoSuplente = 11; break;
            case 'arauto': $tipoSuplente = 12; break;
            case 'adjutor': $tipoSuplente = 13; break;
            case 'sonoplasta': $tipoSuplente = 14; break;
            case 'columba': $tipoSuplente = 15; break;
            default: $tipoSuplente = 0;
        }
        if($tipoSuplente != 0){
            $resultadoVerificacao = $aim->deletarOficialAdjunto($idAtividadeIniciatica, $tipoSuplente);
            if($resultadoVerificacao){
                $retorno['resposta'] = true;
            }
        }
    }

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);

    exit;
}
?>