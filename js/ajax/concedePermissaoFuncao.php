<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $funcao			= isset($_REQUEST['funcao']) ? json_decode($_REQUEST['funcao']) : '';
    $opcao 			= isset($_REQUEST['opcao']) ? json_decode($_REQUEST['opcao']) : '';
    $salvar 		= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/funcaoSubMenuClass.php');
    $fsm = new FuncaoSubMenu();
    $fsm->setFkIdFuncao($funcao);
    $fsm->setFkIdSubMenu($opcao);

    if($salvar==0)
    {
            $retorno = $fsm->remove();
    }
    if($salvar==1)
    {
            $retorno = $fsm->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>