<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

	// Strip document extension
    $path = storage_path($path . $imagem->name);

    // Convert this document
    // Each page to single image
    $img = new Imagick($path);

    // Set background color and flatten
    // Prevents black background on objects with transparency
    $img->setImageBackgroundColor('white');

    // Set image resolution
    $img->setResolution(530,353);

    // Determine num of pages
    $num_pages = $img->getNumberImages();

    // Compress Image Quality
    $img->setImageCompressionQuality(100);

    // Convert PDF pages to images
    for($i = 0;$i < $num_pages && $i < 1; $i++) {

        // Set iterator postion
        $img->setIteratorIndex($i);

        // Set image format
        $img->setImageFormat('jpeg');

        $imageName = $name . substr(md5(rand(10000,1000000)),0, 13) . '.jpeg';

        // Write Images to temp 'upload' folder
        $img->writeImage(storage_path($imagePath . $imageName));
    }

    $img->destroy();

/*
    $im = new imagick("c:\\temp\\523764_169105429888246_1540489537_n.jpg");
    $imageprops = $im->getImageGeometry();
    $width = $imageprops['width'];
    $height = $imageprops['height'];
    if($width > $height) {
        $newHeight = 80;
        $newWidth = (80 / $height) * $width;
    } else {
        $newWidth = 80;
        $newHeight = (80 / $width) * $height;
    }
    $im->resizeImage($newWidth,$newHeight, imagick::FILTER_LANCZOS, 0.9, true);
    $im->cropImage (80,80,0,0);
    $im->writeImage( "D:\\xampp\\htdocs\\th_80x80_test.jpg" );
    echo '<img src="th_80x80_test.jpg">';
*/
}        
?>
