<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $departamento				= isset($_REQUEST['departamento']) ? json_decode($_REQUEST['departamento']) : '';
    $subopcao			 		= isset($_REQUEST['subopcao']) ? json_decode($_REQUEST['subopcao']) : '';
    $salvar 					= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/departamentoOpcaoSubMenuClass.php');
    $dosm = new DepartamentoOpcaoSubMenu();
    $dosm->setFkIdDepartamento($departamento);
    $dosm->setFkIdOpcaoSubMenu($subopcao);

    if($salvar==0)
    {
            $retorno = $dosm->remove();
    }
    if($salvar==1)
    {
            $retorno = $dosm->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>