<?php
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    
    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    }

    $loginAtualizadoPor 	    = isset($_REQUEST['loginAtualizadoPor'])?$_REQUEST['loginAtualizadoPor']:0;
    $codigoAfiliacao 		    = isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    $tipoMembro				    = isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"";
    $tipoObrigacao			    = isset($_REQUEST['tipoObrigacao'])?$_REQUEST['tipoObrigacao']:"";
    $dataObrigacao			    = isset($_REQUEST['dataObrigacao'])?$_REQUEST['dataObrigacao']:"";
    $siglaOA				    = isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:"";
    $descricaoLocal			    = isset($_REQUEST['descricaoLocal'])?$_REQUEST['descricaoLocal']:"";
    $companheiro			    = isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:"";
    $camposGeraisSeqCadast 	    = isset($_REQUEST['camposGeraisSeqCadast']) ? json_decode($_REQUEST['camposGeraisSeqCadast']) : 0;

    $retorno = array();

    for($i=0; $i < count($camposGeraisSeqCadast); $i++) {
        $ws = new Ws();
        $method = 'atualizarObrigacaoRitualisticaMembro';
        $params = array('CodUsuario' => $loginAtualizadoPor,
            'SeqCadast' => $camposGeraisSeqCadast[$i],
            'CodMembro' => $codigoAfiliacao,
            'IdeTipoMembro' => $tipoMembro,
            'SeqTipoObriga' => $tipoObrigacao,
            'DatObriga' => $dataObrigacao,
            'DatCadastObriga' => date('Y-m-d\TH:i:s'),
            'SigOrgafi' => $siglaOA,
            'DesLocal' => $descricaoLocal,
            'IdeCompanheiro' => $companheiro
        );
        $return = $ws->callMethod($method, $params, $loginAtualizadoPor);
        $retorno[$i] = $return;
    }

    // Imprime o retorno
    echo json_encode($retorno);
}
?>