<?php

/*
 * Token
 */

foreach([
        ['../../sec/token.php', '../../loadEnv.php'],
        ['../sec/token.php', '../loadEnv.php'],
        ['./sec/token.php', './loadEnv.php']
       ] as $paths) {
               if(realpath($paths[0]) && realpath($paths[1])) {
                       require_once $paths[0];
                       require_once $paths[1];
                       break;
               }
}

if($tokenLiberado)
{ 
    /*
    Uploadify
    Copyright (c) 2012 Reactive Apps, Ronnie Garcia
    Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
    */

    $idImovelControle			= isset($_REQUEST['idImovelControle']) ? addslashes($_REQUEST['idImovelControle']) : '';
    $idImovelAnexoTipo			= isset($_REQUEST['idImovelAnexoTipo']) ? addslashes($_REQUEST['idImovelAnexoTipo']) : '';

    include_once("../../model/imovelControleClass.php");
    $icm = new imovelControle();
    //echo $fkDocumento."<br>";

    // Define a destination
    $targetFolder = getenv('ENVIRONMENT') == 'testing' ? '/soa/img/imovel/anexo/' : '/img/imovel/anexo/'; // Relative to the root
    //$targetFolder = '/soa/img/imovel/anexo/'; // Relative to the root
    if (!empty($_FILES)) {

            // Validate the file type
            $fileTypes = array('jpg','jpeg','png','gif','pdf'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            $fileParts['extension'] = strtolower($fileParts['extension']);

            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            $newName = md5(uniqid(time())) . "." . $fileParts['extension'];
            $targetFile = rtrim($targetPath,'/') . '/' . $newName;
            $pathBd = '/img/imovel/anexo/' . $newName;
        //$pathBd = '/soa/img/imovel/anexo/' . $newName;
            $data = 'NOW()';
            //print_r($_FILES);

            //echo $fileParts['extension'];

            if (in_array($fileParts['extension'],$fileTypes)) {
                    if (move_uploaded_file($tempFile,$targetFile)) {

                            $retorno = $icm->cadastroImovelControleAnexo($_FILES['Filedata']['name'],$newName,$fileParts['extension'],$pathBd,$data,'1',$idImovelAnexoTipo,$idImovelControle);

                            //echo $retorno;
                            if ($retorno==true) {
                                    echo '1';		
                            }
                            else {
                                    unlink($targetFile);
                                    echo 'Ocorreu algum erro ao inserir o anexo. Tente novamente mais tarde!';
                            }
                    }
            else {
                echo 'Ocorreu algum erro ao inserir o anexo. Tente novamente mais tarde!';
            }
            } else {
                    echo 'Tipos de arquivo suportados (PDF, JPG, JPEG, PNG OU GIF). Em caso de duvidas entre em contato com o setor de TI';
            }

    }
    else {
            echo 'O arquivo que você está tentando enviar é muito grande ou o servidor não possuí mais espaço disponível. Entre em contato com o setor de TI.';	
    }
}
?>