<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 

    if(!isset($codigoAfiliacao))
    {
            $codigoAfiliacao		= isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:'';
    }
    if(!isset($classificacaoOA))
    {
            $classificacaoOA		= isset($_REQUEST['classificacaoOA'])?$_REQUEST['classificacaoOA']:'';
    }
    if(!isset($siglaOA))
    {
            $siglaOA				= isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:'';
    }
    if(!isset($nomeOA))
    {
            $nomeOA					= isset($_REQUEST['nomeOA'])?$_REQUEST['nomeOA']:'';
    }
    //echo "cod:".$codigoAfiliacao;exit();
    //echo "<br>--->nome:".$nomeOA;
    //echo "<br>--->urlencode: ".urlencode($nomeOA);exit();
    if(!isset($pais))
    {
            $pais					= isset($_REQUEST['pais'])?$_REQUEST['pais']:'';
    }
    if(!isset($logradouro))
    {
            $logradouro				= isset($_REQUEST['logradouro'])?$_REQUEST['logradouro']:'';
    }
    if(!isset($numero))
    {
            $numero					= isset($_REQUEST['numero'])?$_REQUEST['numero']:'';
    }
    if(!isset($complemento))
    {
            $complemento			= isset($_REQUEST['complemento'])?$_REQUEST['complemento']:'';
    }
    if(!isset($bairro))
    {
            $bairro					= isset($_REQUEST['bairro'])?$_REQUEST['bairro']:'';
    }
    if(!isset($cidade))
    {
            $cidade					= isset($_REQUEST['cidade'])?$_REQUEST['cidade']:'';
    }
    if(!isset($uf))
    {
            $uf						= isset($_REQUEST['uf'])?$_REQUEST['uf']:'';
    }
    if(!isset($cep))
    {
            $cep					= isset($_REQUEST['cep'])?$_REQUEST['cep']:'';
    }
    if(!isset($email))
    {
            $email					= isset($_REQUEST['email'])?$_REQUEST['email']:'';
    }
    if(!isset($telefoneFixo))
    {
            $telefoneFixo			= isset($_REQUEST['telefoneFixo'])?$_REQUEST['telefoneFixo']:'';
    }
    if(!isset($celular))
    {
            $celular				= isset($_REQUEST['celular'])?$_REQUEST['celular']:'';
    }
    if(!isset($outroTelefone))
    {
            $outroTelefone			= isset($_REQUEST['outroTelefone'])?$_REQUEST['outroTelefone']:'';
    }
    if(!isset($descricaoOutroTelefone))
    {
            $descricaoOutroTelefone	= isset($_REQUEST['descricaoOutroTelefone'])?$_REQUEST['descricaoOutroTelefone']:'';
    }
    if(!isset($cnpj))
    {
            $cnpj					= isset($_REQUEST['cnpj'])?$_REQUEST['cnpj']:'';
    }
    if(!isset($fax))
    {
            $fax					= isset($_REQUEST['fax'])?$_REQUEST['fax']:'';
    }
    if(!isset($caixaPostal))
    {
            $caixaPostal			= isset($_REQUEST['caixaPostal'])?$_REQUEST['caixaPostal']:'';
    }
    if(!isset($cepCaixaPostal))
    {
            $cepCaixaPostal			= isset($_REQUEST['cepCaixaPostal'])?$_REQUEST['cepCaixaPostal']:'';
    }
    if(!isset($enderecoCorrespondencia))
    {
            $enderecoCorrespondencia= isset($_REQUEST['enderecoCorrespondencia'])?$_REQUEST['enderecoCorrespondencia']:'';
    }
    //echo "<br>Código ORCZ:".$CodUsuario;exit();
    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'atualizarOA';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => $CodUsuario,
				'CodRosacr' => $codigoAfiliacao,
				'IdeTipoOa' => $classificacaoOA,
				'SigOrgafi' => $siglaOA,
				'NomClient' => str_replace("R C", "R+C",str_replace("+", " ",urlencode($nomeOA))),
				'SigPaisEndere' => $pais,
				'DesLograd' => str_replace("+", " ", urlencode($logradouro)),
				'NumEndere' => $numero,
				'DesCompleLograd' => str_replace("+", " ", urlencode($complemento)),
				'NomBairro' => str_replace("+", " ", urlencode($bairro)),
				'NomLocali' => str_replace("+", " ", urlencode($cidade)),
				'SigUf' => $uf,
				'CodCep' => $cep,
				'DesEmail' => $email,
				'NumTelefoFixo' => $telefoneFixo,
				'NumCelula' => $celular,
				'NumOutroTelefo' => $outroTelefone,
				'DesOutroTelefo' => str_replace("+", " ", urlencode($descricaoOutroTelefone)),
				'NumCnpj' => $cnpj,
				'NumFax' => $fax,
				'NumCaixaPostal' => $caixaPostal,
				'CodCepCaixaPostal' => $cepCaixaPostal,
				'IdeEndereCorres' => $enderecoCorrespondencia
			);

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    // Imprime o retorno
    if($ocultar_json==0)
    {
            echo json_encode($return);
    }
}
?>