<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $grauDisponivel					=isset($_REQUEST['grauDisponivel']) ? $_REQUEST['grauDisponivel'] : null;
    $idOrganismo					=isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : null;

    include '../../model/atividadeIniciaticaClass.php';
    $aim = new atividadeIniciatica();

    $retorno=array();
    $resultado = $aim->listaAtividadeIniciatica($idOrganismo,$grauDisponivel,0,0);
    $retorno['iniciacoes']=array();
    if($resultado){
            foreach($resultado as $vetor){
            $vagas = $aim->retornaQntVagasDisponiveis($vetor['idAtividadeIniciatica']);
            $vagasDisponiveis = 14 - $vagas;
            $vagasDisponiveis = $vagasDisponiveis <= 0 ? 0 : $vagasDisponiveis;

                    $data = substr($vetor['dataRealizadaAtividadeIniciatica'],8,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],5,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],0,4).' às '.substr($vetor['horaRealizadaAtividadeIniciatica'],0,5).' horas <b>['.$vagasDisponiveis.' vagas]</b>';
                    $local = '<br> Local: '.$vetor['localAtividadeIniciatica'].'<br>';

                    $retorno['iniciacoes'][] = '<div class="radio radio-success"><input type="radio" name="iniciacoes" id="iniciacao'.$vetor['idAtividadeIniciatica'].'" value="'.$vetor['idAtividadeIniciatica'].'"> <label>'.$data.$local.'</label></div><hr>';
            }
    }

    echo json_encode($retorno);
}