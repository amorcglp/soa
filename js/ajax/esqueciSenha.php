<?php
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

function UrlAtual(){
 $dominio= $_SERVER['HTTP_HOST'];
 $url = $dominio;
 return $url;
}
/*
 * Token
 */
require_once '../../sec/token.php';

if($tokenLiberado)
{
    include '../../mailgun.php';

    include_once('../../lib/phpmailer/class.phpmailer.php');
    
    $REDIRECT_URI  ="";
    if(UrlAtual()=="treinamento.amorc.org.br")
    {    
        /*Treinamento*/
        $REDIRECT_URI  = 'https://treinamento.amorc.org.br/';
    }

    if(UrlAtual()=="soa.amorc.org.br")
    {
        /*Produção*/
        $REDIRECT_URI  = 'https://soa.amorc.org.br/';
    }
    if(UrlAtual()=="localhost")
    {
        /*Localhost*/
        $REDIRECT_URI  = 'https://localhost/soa_producao_st/';
    }

    //$loginUsuario = isset($_REQUEST['loginUsuario'])?$_REQUEST['loginUsuario']:null;
    $emailUsuario = isset($_REQUEST['emailUsuario']) ? $_REQUEST['emailUsuario'] : null;

    include_once('../../model/usuarioClass.php');
    $u = new Usuario();
    $arr = array();

    //$u->setLoginUsuario($loginUsuario);
    $u->setEmailUsuario($emailUsuario);

    $arr = $u->recuperaSenha();

    if ($arr['achou'] == 1) {
        //Quebrar o nome
        $arrNome = explode(" ", $arr['nomeUsuario']);

        //Enviar email com o token
        $texto = '<div>
            <div dir="ltr">
                    <table class="ecxbody-wrap"
                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                            <tbody>
                                    <tr
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                            <td
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                            <td class="ecxcontainer" width="600"
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                    valign="top">
                                                    <div class="ecxcontent"
                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                            <table class="ecxmain" width="100%" cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                    <tbody>
                                                                            <tr
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                    <td class="ecxcontent-wrap"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                            valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                            style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                            alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                    <br>
                                                                                    <br>
                                                                                    <br>
                                                                                            <div style="text-align: left;"></div>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                    <tbody>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top">' . strtoupper($arrNome[0]) . ',</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Seu login é: ' . $arr['loginUsuario'] . ' </td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Acesse a opção abaixo e siga as instruções na tela para alterar sua senha. </td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top"><a href="'.$REDIRECT_URI.'alteraSenha.php?tk=' . $arr['token'] . '"
                                                                                                                            class="ecxbtn-primary"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background: #348eda; border-color: #348eda; border-style: solid; border-width: 10px 20px;"
                                                                                                                            target="_blank" alt="Alterar sua senha no SOA"> Alterar sua senha no SOA </a></td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">AMORC - Ordem Rosacruz</td>
                                                                                                            </tr>
                                                                                                    </tbody>
                                                                                            </table></td>
                                                                            </tr>
                                                                    </tbody>
                                                            </table>
                                                            <div class="footer"
                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                    <div style="text-align: left;"></div>
                                                                    <table width="100%"
                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <tbody>
                                                                                    <tr
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                            <td class="ecxaligncenter ecxcontent-block"
                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                    align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2015</td>
                                                                                    </tr>
                                                                            </tbody>
                                                                    </table>
                                                            </div>
                                                    </div>
                                                    <div style="text-align: left;"></div></td>
                                            <td
                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                    </tr>
                            </tbody>
                    </table>
                    </div>
            </div>';

        /*
        $servidor = "smtp.office365.com";
        $porta = "587";
        $usuario = "noresponseglp@amorc.org.br";
        $senha = "@w2xpglp";
        */
        /*
        $servidor = "smtp.outlook.com";
        $porta = "465";
        $usuario = "samufcaldas@hotmail.com.br";
        $senha = "safc3092";
        */
        /*
        $servidor = "smtp.gmail.com";
        $porta = "465";
        $usuario = "cpdglp@gmail.com";
        $senha = "@w2xpglp";
        
        
        #Disparar email
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPSecure = "ssl";
        $mail->SMTPAuth = true;
        $mail->Host = $servidor; // SMTP utilizado
        $mail->Port = $porta;
        $mail->Username = $usuario;
        $mail->Password = $senha;
        $mail->From = 'cpdglp@gmail.com';
        $mail->AddReplyTo("atendimento@amorc.org.br", "Antiga e Mística Ordem Rosacruz - AMORC");
        $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
        //$mail->AddAddress("samufcaldas@hotmail.com.br");
        //$mail->AddAddress("tatiane.jesus@amorc.org.br");
        //$mail->AddAddress("lucianob@amorc.org.br");
        //$mail->AddAddress("braz@amorc.org.br");
        $mail->AddAddress($arr['emailUsuario']);
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
        $mail->Subject = 'Alteração/Recuperação de Senha - SOA';
        $mail->Body = $texto;
        $mail->AltBody = strip_tags($texto);
        $enviado = $mail->Send();*/

        $enviado = $mgClient->sendMessage($domain, array(
            'from'    => 'Atendimento <atendimento@amorc.org.br>',
            'to'      => strtoupper($arrNome[0]).' <'.$arr['emailUsuario'].'>',
            'subject' => 'Alteração/Recuperação de Senha - SOA',
            'text'    => 'Seu e-mail não suporta HTML',
            'html'    => $texto));

        if ($enviado) {
            $r = $u->enviouEmailRecuperaSenha($arr['token']);
        } else {
            $r = $u->erroEmailRecuperaSenha($arr['token']);
        }
        $retorno['status'] = '1';
    } else {
        $retorno['status'] = '0';
    }

    echo json_encode($retorno);
}    
?>