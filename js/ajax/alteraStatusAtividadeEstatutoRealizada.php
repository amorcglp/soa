<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $idAtividadeEstatuto 	= isset($_REQUEST['idAtividadeEstatuto']) ? $_REQUEST['idAtividadeEstatuto']  : '';

    $arr=array();

    include_once('../../model/atividadeEstatutoClass.php');
    $ae = new atividadeEstatuto();
    $retorno = $ae->alteraStatusAtividadeEstatutoRealizada($idAtividadeEstatuto);

    $arr['alterado'] = false;

    if ($retorno) {
        $arr['alterado'] = true;
    }

    echo json_encode($arr);
}	
?>