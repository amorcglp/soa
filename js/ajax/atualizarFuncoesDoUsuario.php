<?php 

function UrlAtual(){
    $dominio= $_SERVER['HTTP_HOST'];
    $url = $dominio;
    return $url;
}
 
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  
    include_once('../../lib/functions.php');
    include_once('../../lib/phpmailer/class.phpmailer.php');
    include_once('../../model/organismoClass.php');
    include_once('../../model/usuarioClass.php');
    include_once('../../model/funcaoUsuarioClass.php');
    include_once('../../model/funcaoClass.php');
    include_once('../../model/funcaoUsuarioClass.php');
    
    if(UrlAtual()=="treinamento.amorc.org.br"||UrlAtual()=="soa.amorc.org.br")
    {
        $server=135;
    }else{
        $server=108;
    }

    $seqCadast = isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;

    //error_reporting(E_ALL);
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
    ini_set("display_errors", 1 );

    $hoje = date("Y-m-d")."T00:00:00";

    

    if($seqCadast!=null)
    {  
        //Buscar Pais do Usuário vindo do ORCZ
        $siglaPais = retornaPaisMembroPeloSeqCadast($seqCadast);

        //Limpar funções do usuário
        $funcaoUsuario = new FuncaoUsuario();
        $funcaoUsuario->setFk_seq_cadast($seqCadast);
        $funcaoUsuario->removePorUsuario();

        //Recuperar dados da Função
        $naoAtuantes='N';
        $atuantes='S';
        $ocultar_json=1;

        //$seqCadast=$seqCadast;
        $tipoMembro=1;//Apenas fun��es R+C
        include 'retornaFuncaoMembro.php';
        $obj = json_decode(json_encode($return),true);
        //echo "<pre>";print_r($obj);exit();
        $arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];
        //echo "<pre>";print_r($arrFuncoes);exit();

        /**
         * Priorização de lotação para funções administrativas
         */
        //Varrer funções priorizando lotação para funções administrativas
        $siglaOrganismoLotacao="";

        foreach($arrFuncoes as $vetor)
        {
            if($vetor['fields']['fCodTipoFuncao']==100)//Se tiver dignitária priorizar lotação
            {
                $siglaOrganismoLotacao = $vetor['fields']['fSigOrgafi'];
            }    
        }
        //Se não conseguiu encontrar uma lotação para função dignitária pegar administrativa
        if($siglaOrganismoLotacao=="")
        {
            foreach($arrFuncoes as $vetor)
            {
                if($vetor['fields']['fCodTipoFuncao']==200)//Se tiver administrativa priorizar lotação
                {
                    $siglaOrganismoLotacao = $vetor['fields']['fSigOrgafi'];
                }    
            }
        }
        //Se não conseguiu encontrar uma lotação para função administrativa então pegar de qualquer outro tipo
        if($siglaOrganismoLotacao=="")
        {
            foreach($arrFuncoes as $vetor)
            {
                $siglaOrganismoLotacao = $vetor['fields']['fSigOrgafi'];
            }
        }

        //echo "<br>Sigla atualizada:".$siglaOrganismoLotacao;exit();

        foreach($arrFuncoes as $vetor)
        {
            $data_saida = strtotime("0 days",strtotime(substr($vetor['fields']['fDatSaida'],0,10)));
            $data_saida = strtotime(date("Y-m-d"));
                        
                if($vetor['fields']['fDatSaida']==0||$data_saida>=date("Y-m-d"))//Verificar apenas campos que a data de saida não foi cadastrada
                {
                        $dataTerminoMandato = $vetor['fields']['fDatTerminMandat'];
                        $codFuncao = $vetor['fields']['fCodFuncao'];
                        $tipoCargo = $vetor['fields']['fCodTipoFuncao'];
                        $dataEntrada = $vetor['fields']['fDatEntrad'];
                        $desFuncao = substr($vetor['fields']['fDesFuncao'],0,3);
                        $codFuncaoEx = $vetor['fields']['fCodFuncao'];
                        $siglaOrganismoWS = $vetor['fields']['fSigOrgafi'];
                        $codFuncaoEx++;
                        $data1_inteiro = strtotime("+40 days",strtotime(substr($dataTerminoMandato,0,10)));
                        //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                        $data2_inteiro = strtotime(date("Y-m-d"));
                        


                        if($desFuncao!="EX-")
                        {                                                        
                            if($data1_inteiro>=$data2_inteiro)
                            {	
                                //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                if($data_saida>=date("Y-m-d"))
                                {
                                    //echo "codigoFuncao:".$codFuncao;
                                    $f = new funcao();								
                                    $f->setVinculoExterno($codFuncao);
                                    $retorno = $f->buscaVinculoExterno();
                                    //echo "<pre>";print_r($retorno);
                                    foreach ($retorno as $vetor2)
                                    {
                                        $fu = new funcaoUsuario();
                                        $fu->setFk_seq_cadast($seqCadast);
                                        $fu->setFk_idFuncao($vetor2['idFuncao']);
                                        $fu->setDataInicioMandato(substr($dataEntrada,0,10));
                                        $fu->setDataFimMandato(substr($dataTerminoMandato,0,10));
                                        $fu->setSiglaOA($siglaOrganismoWS);
                                        if(!$fu->verificaSeJaExiste())
                                        {
                                            if($fu->cadastra())
                                            {    
                                                //echo "<br>Funcao ".$vetor2['nomeFuncao']." foi adicionada para o usuário do SOA"; 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                }
        }

        //Atualizado a lotação do usuario
        if($siglaOrganismoLotacao!="")
        {    
            $usua = new Usuario();
            $usua->setSeqCadast($seqCadast);
            $usua->setSiglaOA($siglaOrganismoLotacao);
            $resp = $usua->alteraLotacaoUsuario();
        }
        echo "{\"status\":\"1\"}";
    }else{
        echo "{\"status\":\"0\"}";
    }	
}    
?>	