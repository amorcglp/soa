<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idEstado = isset($_REQUEST['idEstado']) ? $_REQUEST['idEstado'] : '';
    $idImovel = isset($_REQUEST['idImovel']) ? $_REQUEST['idImovel'] : '';

    include_once("../../model/imovelClass.php");
    $imodel = new imovel();

    $resultado = $imodel->listaCidades($idEstado);

    $selected = "";
    if ($idImovel != null) {
        $resultadoImovel = $imodel->buscaIdImovel($idImovel);

        if ($resultadoImovel) {
            foreach ($resultadoImovel as $vetorImovel) {
                $selected = $vetorImovel['fk_idCidade'];
            }
        }
    }

    $retorno = array();
    $retorno['imoveis'] = array();

    if ($resultado) {
        $retorno['qtdImoveis'] = 0;

        foreach ($resultado as $vetorCidade) {

            $retorno['temImoveis'] = 1;
            $retorno['qtdImoveis'] ++;
            if ($vetorCidade['id'] == $selected) {
                $retorno['imoveis']['options'][] = '<option value="' . $vetorCidade['id'] . '" " selected >' . $vetorCidade["nome"] . '</option>';
            } else {
                $retorno['imoveis']['options'][] = '<option value="' . $vetorCidade['id'] . '" ">' . $vetorCidade["nome"] . '</option>';
            }
        }
    } else {
        $retorno['temImoveis'] = 0;
    }

    echo json_encode($retorno);
}