<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $seqCadast 			        		= isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';
    $tipoAtividadeIniciatica 		    = isset($_REQUEST['tipoAtividadeIniciatica']) ? $_REQUEST['tipoAtividadeIniciatica'] : '';

    include_once('../../model/atividadeIniciaticaClass.php');
    $aim = new atividadeIniciatica();

    $resultado = $aim->verificaMembroJaAgendado($seqCadast, $tipoAtividadeIniciatica);

    if($resultado){
            $retorno['status'] = '0';
    }else{
            $retorno['status'] = '1';
    }

    echo json_encode($retorno);
}	
?>