<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $trimestreAtual     = isset($_REQUEST['trimestreAtual']) ? $_REQUEST['trimestreAtual']  : '';
    $anoAtual           = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual']  : '';
    $idOrganismoAfiliado= isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']  : '';
    $quemEntregou 	    = isset($_REQUEST['quemEntregou']) ? $_REQUEST['quemEntregou']  : '';

    $arr=array();

    include_once('../../lib/functions.php');
    include_once('../../model/columbaTrimestralClass.php');
    $ct = new columbaTrimestral();
    $resultado = $ct->listaColumbaTrimestral($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
    if(!$resultado)
    {
            $ct = new columbaTrimestral();
            $ct->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $ct->setTrimestre($trimestreAtual);
            $ct->setAno($anoAtual);
            $ct->setQuemEntregou($quemEntregou);
            $ct->setEntregue(1);
            $ct->cadastraColumbaTrimestral();


        $ct = new columbaTrimestral();
        $resultado = $ct->listaColumbaTrimestral($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
    }

    $arr['status'] = 0;
    $arr['dataEntrega'] = "";

    if ($resultado) {

        $arr['status'] = 1;
        foreach ($resultado as $v)
        {
            if(trim($v['numeroAssinatura'])!="")
            {
                $numeroAssinatura = $v['numeroAssinatura'];
                $temNumeroAssinatura=true;
            }else{
                $numeroAssinatura = aleatorioAssinatura();
                $ct->atualizaNumeroAssinatura($trimestreAtual, $anoAtual,$idOrganismoAfiliado,$numeroAssinatura);
            }

            $dataEntrega = substr($v['dataEntrega'],0,10);
            $arr['dataEntrega'] = substr($dataEntrega,8,2)."/".substr($dataEntrega,5,2)."/".substr($dataEntrega,0,4);
            $arr['quemEntregou'] = $v['nomeUsuario'];
            $arr['numeroAssinatura'] = $numeroAssinatura;
        }
    }

    echo json_encode($arr);
}	
?>