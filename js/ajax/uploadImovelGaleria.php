<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    /*
    Uploadify
    Copyright (c) 2012 Reactive Apps, Ronnie Garcia
    Released under the MIT License <http://www.opensource.org/licenses/mit-license.php> 
    */

    $idImovel			= isset($_POST['idImovel']) ? addslashes($_POST['idImovel']) : '';

    include_once("../../model/imovelClass.php");
    $ic = new imovel();
    //echo $fkDocumento."<br>";

    // Define a destination
    $targetFolder = '/img/imovel/galeria/'; // Relative to the root
    if (!empty($_FILES)) {

            // Validate the file type
            $fileTypes = array('jpg','jpeg','png','gif'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);
            $fileParts['extension'] = strtolower($fileParts['extension']);

            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            $newName = md5(uniqid(time())) . "." . $fileParts['extension'];
            $targetFile = rtrim($targetPath,'/') . '/' . $newName;
            $pathBd = '/img/imovel/galeria/' . $newName;
            $data = 'NOW()';
            //print_r($_FILES);

            //echo $fileParts['extension'];

            if (in_array($fileParts['extension'],$fileTypes)) {
                    if (move_uploaded_file($tempFile,$targetFile)) {

                            $retorno = $ic->cadastroImovelGaleriaImagens($_FILES['Filedata']['name'],$newName,$fileParts['extension'],$pathBd,$data,'1',$idImovel);

                            //echo $retorno;

                            if ($retorno==true) {
                                    echo '1';		
                            }
                            else {
                                    unlink($targetFile);
                                    echo 'Ocorreu algum erro ao inserir o anexo. Tente novamente ou entre em contato com o setor de TI';
                            }

                    }
            } else {
                    echo 'Tipos de arquivo suportados (PDF, JPG, JPEG, PNG OU GIF). Em caso de duvidas entre em contato com o setor de TI';
            }

    }
    else {
            echo 'O arquivo que você está tentando enviar é muito grande ou o servidor não possuí mais espaço disponível. Entre em contato com o setor de TI.';	
    }
}
?>