<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idOrganismo = isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';
    $idAtividade = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';
    $ano = date("Y")+1353;

    include_once("../../model/atividadeIniciaticaColumbaClass.php");
    include_once("../../model/atividadeIniciaticaClass.php");
    $aicmodel 	= new atividadeIniciaticaColumba();
    $aimodel 	= new atividadeIniciatica();

    $resultadoColumbas 				= $aicmodel->listaAtividadeIniciaticaColumba($idOrganismo);

    $selected=0;

    if($idAtividade != ''){
            $resultadoColumbaAtividade 		= $aimodel->buscaIdAtividadeIniciatica($idAtividade);
            if($resultadoColumbaAtividade){
                    foreach($resultadoColumbaAtividade as $vetorColumbaAtividade){
                            $selected = $vetorColumbaAtividade['fk_idAtividadeIniciaticaColumba'];
                    }
            }
    }

    $retorno=array();

    $retorno['atividade']['total'] = 0;

    if ($resultadoColumbas) {
            foreach ($resultadoColumbas as $vetorColumbas) {
                    if($vetorColumbas['statusAtividadeIniciaticaColumba'] == 0){
                            $retorno['atividade']['total']++;

                            $arrNome				= explode(" ", $vetorColumbas['nomeAtividadeIniciaticaColumba']);
                            $nome = "[".$vetorColumbas['codAfiliacaoAtividadeIniciaticaColumba'] . "] " .
                                            ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".
                                            ucfirst(mb_strtolower($arrNome[1],'UTF-8'));
                            if(array_key_exists(2, $arrNome)){
                                    $nome .= " " . ucfirst(mb_strtolower($arrNome[2],'UTF-8'));
                            }
                            if(array_key_exists(3, $arrNome)){
                                    $nome .= " " . ucfirst(mb_strtolower($arrNome[3],'UTF-8'));
                            }

                            if($vetorColumbas['idAtividadeIniciaticaColumba']==$selected){
                                    $retorno['atividade']['columbas'][] = '<option value="'.$vetorColumbas['idAtividadeIniciaticaColumba'].'" selected>' . $nome . '</option>';
                            } else {
                                    $retorno['atividade']['columbas'][] = '<option value="'.$vetorColumbas['idAtividadeIniciaticaColumba'].'">' . $nome . '</option>';
                            }
                    }
            }
    }

    /*
    echo "<pre>";
    print_r($retorno);
    echo "</pre>";
    */

    echo json_encode($retorno);
}