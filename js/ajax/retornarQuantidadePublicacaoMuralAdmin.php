<?php

/*
 * Token
 */
if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $filtroRegioes              = isset($_REQUEST['filtroRegioesSelecionados']) ? json_decode($_REQUEST['filtroRegioesSelecionados']) : '';
    $filtroOrganismos           = isset($_REQUEST['filtroOrganismosSelecionados']) ? json_decode($_REQUEST['filtroOrganismosSelecionados']) : '';
    $filtroCategorias           = isset($_REQUEST['filtroCategoriaSelecionada']) ? json_decode($_REQUEST['filtroCategoriaSelecionada']) : '';
    $filtroPublicos             = isset($_REQUEST['filtroPublicoSelecionado']) ? json_decode($_REQUEST['filtroPublicoSelecionado']) : '';
    $filtroFavorito             = isset($_POST['filtroFavorito']) ? addslashes($_POST['filtroFavorito']) : '';

    include_once("../../model/muralClass.php");
    $mural = new Mural();

    //echo "<pre>";print_r($filtroOrganismos);echo "</pre>";

    $retorno=array();
    $retorno['qntTotal'] = 0;
    $retorno['qntTotal'] = $mural->retornaQuantidadePublicacaoMuralAdmin($filtroRegioes,
                                                                         $filtroOrganismos,
                                                                         $filtroCategorias,
                                                                         $filtroFavorito,
                                                                         $filtroPublicos);

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}
