<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $departamento				= isset($_REQUEST['departamento']) ? json_decode($_REQUEST['departamento']) : '';
    $objeto_dashboard 			= isset($_REQUEST['objeto_dashboard']) ? json_decode($_REQUEST['objeto_dashboard']) : '';
    $salvar 					= isset($_REQUEST['salvar']) ? json_decode($_REQUEST['salvar']) : '';

    $arr=array();
    $arr['status']=0;

    include_once('../../model/departamentoObjetoDashboardClass.php');
    $dod = new DepartamentoObjetoDashboard();
    $dod->setFkIdDepartamento($departamento);
    $dod->setFkIdObjetoDashboard($objeto_dashboard);

    if($salvar==0)
    {
            $retorno = $dod->remove();
    }
    if($salvar==1)
    {
            $retorno = $dod->cadastra();
    }
    if ($retorno) {
            $arr['status']=1;
    }

    echo json_encode($arr);

    exit;
}
?>