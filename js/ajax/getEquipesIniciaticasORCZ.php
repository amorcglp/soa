<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once("../../model/organismoClass.php");
    $om = new organismo();

    $tipoEquipe         			= isset($_REQUEST['tipoEquipe']) ? $_REQUEST['tipoEquipe'] : '';
    $idOrganismoAfiliado         	= isset($_REQUEST['idOrganismo']) ? $_REQUEST['idOrganismo'] : '';

    $resultado = $om->buscaIdOrganismo($idOrganismoAfiliado);
    if($resultado){
            foreach($resultado as $vetor){
                    $siglaOA = strtoupper($vetor['siglaOrganismoAfiliado']);
            }
    }

    //$siglaOA='PR101';
    $ocultar_json=1;
    $atuantes='S';
    $naoAtuantes='N';

    include 'retornaFuncaoMembro.php';
    $oficial = json_decode(json_encode($return),true);
    $retorno=array();
    $retorno['total']=0;
    foreach ($oficial['result'][0]['fields']['fArrayOficiais'] as $vetorOficial) {

            //echo "<pre>";print_r($vetorOficial);echo "</pre>";

            switch($tipoEquipe){
                    case 1:
                            $codFuncaoMestre 				= 401;
                            $codFuncaoMestreAux 			= 403;
                            $codFuncaoArquivista 			= 415;
                            $codFuncaoCapelao 				= 405;
                            $codFuncaoMatre 				= 417;
                            $codFuncaoGrandeSacerdotisa 	= 419;
                            $codFuncaoGuia 					= 413;
                            $codFuncaoGuardiaoInterno 		= 407;
                            $codFuncaoGuardiaoExterno 		= 409;
                            $codFuncaoArchote 				= 421;
                            $codFuncaoMedalhista 			= 423;
                            $codFuncaoArauto 				= 411;
                            $codFuncaoAdjutor 				= 425;
                            $codFuncaoSonoplasta 			= 427;
                            break;
                    case 2:
                            $codFuncaoMestre 				= 501;
                            $codFuncaoMestreAux 			= 503;
                            $codFuncaoArquivista 			= 511;
                            $codFuncaoCapelao 				= 505;
                            $codFuncaoMatre 				= 507;
                            $codFuncaoGrandeSacerdotisa 	= 509;
                            $codFuncaoGuia 					= 515;
                            $codFuncaoGuardiaoInterno 		= 527;
                            $codFuncaoGuardiaoExterno 		= 529;
                            $codFuncaoArchote 				= 517;
                            $codFuncaoMedalhista 			= 519;
                            $codFuncaoArauto 				= 513;
                            $codFuncaoAdjutor 				= 531;
                            $codFuncaoSonoplasta 			= 525;
                            break;
                    case 3:
                            $codFuncaoMestre 				= 521;
                            $codFuncaoMestreAux 			= 523;
                            $codFuncaoArquivista 			= 543;
                            $codFuncaoCapelao 				= 533;
                            $codFuncaoMatre 				= 545;
                            $codFuncaoGrandeSacerdotisa 	= 547;
                            $codFuncaoGuia 					= 541;
                            $codFuncaoGuardiaoInterno 		= 535;
                            $codFuncaoGuardiaoExterno 		= 537;
                            $codFuncaoArchote 				= 549;
                            $codFuncaoMedalhista 			= 551;
                            $codFuncaoArauto 				= 539;
                            $codFuncaoAdjutor 				= 553;
                            $codFuncaoSonoplasta 			= 555;
                            break;
            }

            if($tipoEquipe){
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoMestre){
                            $seqCadastMestre 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoMestre 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeMestre 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoMestreAux){
                            $seqCadastMestreAux 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoMestreAux 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeMestreAux 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoArquivista){
                            $seqCadastArquivista 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoArquivista 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeArquivista 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoCapelao){
                            $seqCadastCapelao 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoCapelao 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeCapelao 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoMatre){
                            $seqCadastMatre 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoMatre 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeMatre 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoGrandeSacerdotisa){
                            $seqCadastGrandeSacerdotisa 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoGrandeSacerdotisa 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeGrandeSacerdotisa 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoGuia){
                            $seqCadastGuia 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoGuia 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeGuia 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoGuardiaoInterno){
                            $seqCadastGuardiaoInterno 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoGuardiaoInterno 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeGuardiaoInterno 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoGuardiaoExterno){
                            $seqCadastGuardiaoExterno 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoGuardiaoExterno 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeGuardiaoExterno 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            //$retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoArchote){
                            $seqCadastArchote 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoArchote 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeArchote 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoMedalhista){
                            $seqCadastMedalhista 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoMedalhista 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeMedalhista 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoArauto){
                            $seqCadastArauto 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoArauto 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeArauto 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoAdjutor){
                            $seqCadastAdjutor 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoAdjutor 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeAdjutor 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
                    if($vetorOficial['fields']['fCodFuncao']==$codFuncaoSonoplasta){
                            $seqCadastSonoplasta 		= $vetorOficial['fields']['fSeqCadast'];

                            $codAfiliacaoSonoplasta 			= $vetorOficial['fields']['fCodMembro'];
                            $nomeSonoplasta 			= ucwords(mb_strtolower($vetorOficial['fields']['fNomClient'],'UTF-8'));

                            $retorno['total']++;
                    }
            }
    }

    if(isset($seqCadastMestre)){
            $retorno['mestre'] 					= $seqCadastMestre;
            $retorno['mestreCodAfiliacao'] 		= '['.$codAfiliacaoMestre.'] ';
            $retorno['mestreNome'] 				= $nomeMestre;
    } else {
            $retorno['mestre'] 					= 0;
            $retorno['mestreCodAfiliacao'] 		= '';
            $retorno['mestreNome'] 				= ' -- ';
    }
    if(isset($seqCadastMestreAux)){
            $retorno['mestreAux'] 					= $seqCadastMestreAux;
            $retorno['mestreAuxCodAfiliacao'] 		= '['.$codAfiliacaoMestreAux.'] ';
            $retorno['mestreAuxNome'] 				= $nomeMestreAux;
    } else {
            $retorno['mestreAux'] 					= 0;
            $retorno['mestreAuxCodAfiliacao'] 		= '';
            $retorno['mestreAuxNome'] 				= ' -- ';
    }
    if(isset($seqCadastArquivista)){
            $retorno['arquivista'] 					= $seqCadastArquivista;
            $retorno['arquivistaCodAfiliacao'] 		= '['.$codAfiliacaoArquivista.'] ';
            $retorno['arquivistaNome'] 				= $nomeArquivista;
    } else {
            $retorno['arquivista'] 					= 0;
            $retorno['arquivistaCodAfiliacao'] 		= '';
            $retorno['arquivistaNome'] 				= ' -- ';
    }
    if(isset($seqCadastCapelao)){
            $retorno['capelao'] 					= $seqCadastCapelao;
            $retorno['capelaoCodAfiliacao'] 		= '['.$codAfiliacaoCapelao.'] ';
            $retorno['capelaoNome'] 				= $nomeCapelao;
    } else {
            $retorno['capelao'] 					= 0;
            $retorno['capelaoCodAfiliacao'] 		= '';
            $retorno['capelaoNome'] 				= ' -- ';
    }
    if(isset($seqCadastMatre)){
            $retorno['matre'] 					= $seqCadastMatre;
            $retorno['matreCodAfiliacao'] 		= '['.$codAfiliacaoMatre.'] ';
            $retorno['matreNome'] 				= $nomeMatre;
    } else {
            $retorno['matre'] 					= 0;
            $retorno['matreCodAfiliacao'] 		= '';
            $retorno['matreNome'] 				= ' -- ';
    }
    if(isset($seqCadastGrandeSacerdotisa)){
            $retorno['grandeSacerdotisa'] 					= $seqCadastGrandeSacerdotisa;
            $retorno['grandeSacerdotisaCodAfiliacao'] 		= '['.$codAfiliacaoGrandeSacerdotisa.'] ';
            $retorno['grandeSacerdotisaNome'] 				= $nomeGrandeSacerdotisa;
    } else {
            $retorno['grandeSacerdotisa'] 					= 0;
            $retorno['grandeSacerdotisaCodAfiliacao'] 		= '';
            $retorno['grandeSacerdotisaNome'] 				= ' -- ';
    }
    if(isset($seqCadastGuia)){
            $retorno['guia'] 					= $seqCadastGuia;
            $retorno['guiaCodAfiliacao'] 		= '['.$codAfiliacaoGuia.'] ';
            $retorno['guiaNome'] 				= $nomeGuia;
    } else {
            $retorno['guia'] 					= 0;
            $retorno['guiaCodAfiliacao'] 		= '';
            $retorno['guiaNome'] 				= ' -- ';
    }
    if(isset($seqCadastGuardiaoInterno)){
            $retorno['guardiaoInterno'] 					= $seqCadastGuardiaoInterno;
            $retorno['guardiaoInternoCodAfiliacao'] 		= '['.$codAfiliacaoGuardiaoInterno.'] ';
            $retorno['guardiaoInternoNome'] 				= $nomeGuardiaoInterno;
    } else {
            $retorno['guardiaoInterno'] 					= 0;
            $retorno['guardiaoInternoCodAfiliacao'] 		= '';
            $retorno['guardiaoInternoNome'] 				= '  --  ';
    }
    if(isset($seqCadastGuardiaoExterno)){
            $retorno['guardiaoExterno'] 						= $seqCadastGuardiaoExterno;
            $retorno['guardiaoExternoCodAfiliacao'] 		= '['.$codAfiliacaoGuardiaoExterno.'] ';
            $retorno['guardiaoExternoNome'] 					= $nomeGuardiaoExterno;
    } else {
            $retorno['guardiaoExterno'] 						= 0;
            $retorno['guardiaoExternoCodAfiliacao'] 			= '';
            $retorno['guardiaoExternoNome'] 					= ' -- ';
    }
    if(isset($seqCadastArchote)){
            $retorno['archote'] 					= $seqCadastArchote;
            $retorno['archoteCodAfiliacao'] 		= '['.$codAfiliacaoArchote.'] ';
            $retorno['archoteNome'] 				= $nomeArchote;
    } else {
            $retorno['archote'] 					= 0;
            $retorno['archoteCodAfiliacao'] 		= '';
            $retorno['archoteNome'] 				= ' -- ';
    }
    if(isset($seqCadastMedalhista)){
            $retorno['medalhista'] 					= $seqCadastMedalhista;
            $retorno['medalhistaCodAfiliacao'] 		= '['.$codAfiliacaoMedalhista.'] ';
            $retorno['medalhistaNome'] 				= $nomeMedalhista;
    } else {
            $retorno['medalhista'] 					= 0;
            $retorno['medalhistaCodAfiliacao'] 		= '';
            $retorno['medalhistaNome'] 				= ' -- ';
    }
    if(isset($seqCadastArauto)){
            $retorno['arauto'] 					= $seqCadastArauto;
            $retorno['arautoCodAfiliacao'] 		= '['.$codAfiliacaoArauto.'] ';
            $retorno['arautoNome'] 				= $nomeArauto;
    } else {
            $retorno['arauto'] 					= 0;
            $retorno['arautoCodAfiliacao'] 		= '';
            $retorno['arautoNome'] 				= ' -- ';
    }
    if(isset($seqCadastAdjutor)){
            $retorno['adjutor'] 					= $seqCadastAdjutor;
            $retorno['adjutorCodAfiliacao'] 		= '['.$codAfiliacaoAdjutor.'] ';
            $retorno['adjutorNome'] 				= $nomeAdjutor;
    } else {
            $retorno['adjutor'] 					= 0;
            $retorno['adjutorCodAfiliacao'] 		= '';
            $retorno['adjutorNome'] 				= ' -- ';
    }
    if(isset($seqCadastSonoplasta)){
            $retorno['sonoplasta'] 					= $seqCadastSonoplasta;
            $retorno['sonoplastaCodAfiliacao'] 		= '['.$codAfiliacaoSonoplasta.'] ';
            $retorno['sonoplastaNome'] 				= $nomeSonoplasta;
    } else {
            $retorno['sonoplasta'] 					= 0;
            $retorno['sonoplastaCodAfiliacao'] 		= '';
            $retorno['sonoplastaNome'] 				= ' -- ';
    }
    $retorno['columba'] 					= 0;
    $retorno['columbaCodAfiliacao'] 		= '';
    $retorno['columbaNome'] 				= 'Conforme escala!';

    $retorno['recepcao'] 				= 0;
    $retorno['recepcaoCodAfiliacao'] 	= '';
    $retorno['recepcaoNome'] 			= 'Conforme escala!';


    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);
}