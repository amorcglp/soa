<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $id			= isset($_POST['id']) ? addslashes($_POST['id']) : '';
    $idTicket		= isset($_POST['idTicket']) ? addslashes($_POST['idTicket']) : '';

    include_once("../../model/ticketClass.php");
    $tm = new Ticket();

    $resultado = $tm->excluiAnexo($id);
    //echo $resultado."<br>";

    $retorno=array();

    if ($resultado>0) {
            $retorno['sucess'] = true;
    }
    else {
            $retorno['sucess'] = false;
            $retorno['erro'] = 'Erro ao excluir o Anexo';
    }

    $retorno['idTicket'] = $idTicket;
    //echo "<pre>";print_r($retorno);

    echo json_encode($retorno);
}
?>