<?php
/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    
    header("Content-Type: text/html;  charset=UTF-8",true);
    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';	
            }else{
                    require_once './model/wsClass.php';
            }
    } 
    $server                                         = 108;

    //Verificar se tem numeros na string

    //login (- a parte numerica)
    /*
    if(isset($_REQUEST['CodUsuario']))
    {    
        $str = $_REQUEST['CodUsuario'];
        $str = str_replace("0","",$str);
        $str = str_replace("1","",$str);
        $str = str_replace("2","",$str);
        $str = str_replace("3","",$str);
        $str = str_replace("4","",$str);
        $str = str_replace("5","",$str);
        $str = str_replace("6","",$str);
        $str = str_replace("7","",$str);
        $str = str_replace("8","",$str);
        $str = str_replace("9","",$str);
        $loginArrumado = "soa ".substr($str,0,6);
    }
     */
    $loginArrumado = "soa".$_REQUEST['SeqCadastAtendimento'];
    $CodUsuario					= isset($_REQUEST['CodUsuario'])? (string) $loginArrumado:'';
    $SeqCadast					= isset($_REQUEST['SeqCadast'])?$_REQUEST['SeqCadast']:'';
    $CodMembro					= isset($_REQUEST['CodMembro'])?$_REQUEST['CodMembro']:'';
    $TipoMembro					= isset($_REQUEST['TipoMembro'])?$_REQUEST['TipoMembro']:'';

    $IdeSexo					= isset($_REQUEST['IdeSexo'])?$_REQUEST['IdeSexo']:'';
    $NomMembro					= isset($_REQUEST['NomMembro'])?$_REQUEST['NomMembro']:'';
    $NomConjuge					= isset($_REQUEST['NomConjuge'])?$_REQUEST['NomConjuge']:'';
    $DesLograd					= isset($_REQUEST['DesLograd'])?$_REQUEST['DesLograd']:'';
    $NumEndere					= isset($_REQUEST['NumEndere'])?$_REQUEST['NumEndere']:'';
    $DesCompleLograd				= isset($_REQUEST['DesCompleLograd'])?$_REQUEST['DesCompleLograd']:'';
    $NomBairro					= isset($_REQUEST['NomBairro'])?$_REQUEST['NomBairro']:'';
    $NomLocali					= isset($_REQUEST['NomLocali'])?$_REQUEST['NomLocali']:'';
    $SigUf  					= isset($_REQUEST['SigUf'])?$_REQUEST['SigUf']:'';
    $CodCep 					= isset($_REQUEST['CodCep'])?$_REQUEST['CodCep']:'';
    $NumCaixaPostal					= isset($_REQUEST['NumCaixaPostal'])?$_REQUEST['NumCaixaPostal']:'';
    $SigPais					= isset($_REQUEST['SigPais'])?$_REQUEST['SigPais']:'';
    $DesEmail					= isset($_REQUEST['DesEmail'])?$_REQUEST['DesEmail']:'';
    $DatNascim					= isset($_REQUEST['DatNascim'])?$_REQUEST['DatNascim']:'';
    $IdeEstadoCivil					= isset($_REQUEST['IdeEstadoCivil'])?$_REQUEST['IdeEstadoCivil']:'';
    $CodRg  					= isset($_REQUEST['CodRg'])?$_REQUEST['CodRg']:'';
    $IdeEnderecoCorrespondencia			= isset($_REQUEST['IdeEnderecoCorrespondencia'])?$_REQUEST['IdeEnderecoCorrespondencia']:'';
    $CodCepCaixaPostal				= isset($_REQUEST['CodCepCaixaPostal'])?$_REQUEST['CodCepCaixaPostal']:'';
    $TipoGrauInstrucao				= isset($_REQUEST['TipoGrauInstrucao'])?$_REQUEST['TipoGrauInstrucao']:'';
    $CodFormacao					= isset($_REQUEST['fk_idFormacao'])?$_REQUEST['fk_idFormacao']:'';
    $CodOcupacao					= isset($_REQUEST['fk_idOcupacao'])?$_REQUEST['fk_idOcupacao']:'';
    $CodEspecializacao				= isset($_REQUEST['fk_idEspecializacao'])?$_REQUEST['fk_idEspecializacao']:'';
    $CodTipoMoeda					= isset($_REQUEST['fk_idTipoMoeda'])?$_REQUEST['fk_idTipoMoeda']:'';

    $NumTelefoFixo					= isset($_REQUEST['NumTelefoFixo'])?$_REQUEST['NumTelefoFixo']:'';
    $NumCelula					= isset($_REQUEST['NumCelula'])?$_REQUEST['NumCelula']:'';
    $NumOutroTelefo					= isset($_REQUEST['NumOutroTelefo'])?$_REQUEST['NumOutroTelefo']:'';
    $DesOutroTelefo					= isset($_REQUEST['DesOutroTelefo'])?$_REQUEST['DesOutroTelefo']:'';
    $IdeAceitoContat				= isset($_REQUEST['IdeAceitoContat'])?$_REQUEST['IdeAceitoContat']:'';
    $IdePeriodContat				= isset($_REQUEST['IdePeriodContat'])?$_REQUEST['IdePeriodContat']:'';
    $IdeFormaContatIdeal				= isset($_REQUEST['IdeFormaContatIdeal'])?$_REQUEST['IdeFormaContatIdeal']:'';
    $IdePossuiComputador				= isset($_REQUEST['possuiComputador'])?$_REQUEST['possuiComputador']:'';
    $IdePossuiAcessoInternet 			= isset($_REQUEST['possuiInternet'])?$_REQUEST['possuiInternet']:'';
    $DesejaColaborarComOANasEquipes 		= isset($_REQUEST['desejaColaborar'])?$_REQUEST['desejaColaborar']:'';
    $ExperienciaProfissionalResumida 		= isset($_REQUEST['experienciaProfissional'])?$_REQUEST['experienciaProfissional']:'';
    //echo "->experiencia: " . $ExperienciaProfissionalResumida;//ação 5151df///0000%%
    $IdeMembroURCI     				= isset($_REQUEST['membroURCI'])?$_REQUEST['membroURCI']:'';

    $IdeAmorcgDigita     				= isset($_REQUEST['IdeAmorcgDigita'])?$_REQUEST['IdeAmorcgDigita']:'';
    $IdeForumrDigita     				= isset($_REQUEST['IdeForumrDigita'])?$_REQUEST['IdeForumrDigita']:'';
    $IdeOrosacDigita      				= isset($_REQUEST['IdeOrosacDigita'])?$_REQUEST['IdeOrosacDigita']:'';
    $IdeOpentaDigita     				= isset($_REQUEST['IdeOpentaDigita'])?$_REQUEST['IdeOpentaDigita']:'';
    $IdeOGGDigita     				= isset($_REQUEST['IdeOGGDigita'])?$_REQUEST['IdeOGGDigita']:'';

    // Instancia a classe
    $ws = new Ws($server);

    // Nome do Método que deseja chamar
    $method = 'atualizarDadosBasicos';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => $CodUsuario,
                                    'SeqCadast' => $SeqCadast,
                                    'CodMembro' => $CodMembro,
                                    'TipoMembro' => $TipoMembro,
                                    'IdeSexo' => $IdeSexo,
                                    'NomMembro' => str_replace("+", " ", urlencode($NomMembro)),
                                    'NomConjuge' => str_replace("+", " ", urlencode($NomConjuge)),
                                    'DesLograd' => str_replace("+", " ", urlencode($DesLograd)),
                                    'NumEndere' => $NumEndere,
                                    'DesCompleLograd' => str_replace("+", " ", urlencode($DesCompleLograd)),
                                    'NomBairro' => str_replace("+", " ", urlencode($NomBairro)),
                                    'NomLocali' => str_replace("+", " ", urlencode($NomLocali)),
                                    'SigUf' => $SigUf,
                                    'CodCep' => $CodCep,
                                    'NumCaixaPostal' => $NumCaixaPostal,
                                    'SigPais' => $SigPais,
                                    'DesEmail' => $DesEmail,
                                    'DatNascim' => $DatNascim,
                                    'IdeEstadoCivil' => $IdeEstadoCivil,
                                    'CodRg' => $CodRg,
                                    'IdeEnderecoCorrespondencia' => $IdeEnderecoCorrespondencia,
                                    'CodCepCaixaPostal' => $CodCepCaixaPostal,
                                    'TipoGrauInstrucao' => $TipoGrauInstrucao,
                                    'CodFormacao' => $CodFormacao,
                                    'CodOcupacao' => $CodOcupacao,
                                    'CodEspecializacao' => $CodEspecializacao,
                                    'CodTipoMoeda' => $CodTipoMoeda
                            );

    // Chamada do método
    $return = $ws->callMethod($method, $params, $CodUsuario);

    // Instancia a classe
    $ws2 = new Ws($server);

    // Nome do Método que deseja chamar
    $method2 = 'atualizarDadoscomplementares';

    // Parametros que serão enviados à chamada
    $params2 = array('CodUsuario' => $CodUsuario,
                                    'SeqCadast' => $SeqCadast,
                                    'CodMembro' => $CodMembro,
                                    'TipoMembro' => $TipoMembro,
                                    'IdePossuiComputador' => $IdePossuiComputador,
                                    'IdePossuiAcessoInternet' => $IdePossuiAcessoInternet,
                                    'DesejaColaborarComOANasEquipes ' => $DesejaColaborarComOANasEquipes,
                                    'ExperienciaProfissionalResumida ' => str_replace("+", " ", urlencode($ExperienciaProfissionalResumida)),
                                    'IdeMembroURCI' => $IdeMembroURCI, 
                                    'NumTelefoFixo' => $NumTelefoFixo,
                                    'NumCelula' => $NumCelula,
                                    'NumOutroTelefo' => $NumOutroTelefo,
                                    'DesOutroTelefo' => str_replace("+", " ", urlencode($DesOutroTelefo)),
                                    'IdeAceitoContat' => $IdeAceitoContat,
                                    'IdePeriodContat' => $IdePeriodContat,
                                    'IdeFormaContatIdeal' => $IdeFormaContatIdeal,

                                    'IdeAmorcgDigita' => $IdeAmorcgDigita,
                                    'IdeForumrDigita' => $IdeForumrDigita,
                                    'IdeOrosacDigita' => $IdeOrosacDigita,
                                    'IdeOpentaDigita' => $IdeOpentaDigita,
                                    'IdeOGGDigita' => $IdeOGGDigita

                            );

    // Chamada do método
    $return2 = $ws2->callMethod($method2, $params2, $CodUsuario);

    // Imprime o retorno
    //echo json_encode($return2);
    echo "{\"status\":\"1\"}";

}
?>