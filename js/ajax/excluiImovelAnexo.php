<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
   
    $id			= isset($_POST['id']) ? addslashes($_POST['id']) : '';
    $idImovel		= isset($_POST['idImovel']) ? addslashes($_POST['idImovel']) : '';

    include_once("../../model/imovelClass.php");
    $ic = new imovel();

    $resultado = $ic->excluiImovelAnexo($id);
    //echo $resultado."<br>";

    $retorno=array();

    if ($resultado>0) {
            $retorno['sucess'] = true;
    }
    else {
            $retorno['sucess'] = false;
            $retorno['erro'] = 'Erro ao excluir o Anexo';
    }

    $retorno['idImovel'] = $idImovel;
    //echo "<pre>";print_r($retorno);

    echo json_encode($retorno);
}
?>