<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idAtividadeEstatuto		= isset($_POST['idAtividadeEstatuto']) ? addslashes($_POST['idAtividadeEstatuto']) : '';

    include_once("../../model/atividadeEstatutoClass.php");
    $aec = new atividadeEstatuto();

    $retorno=array();
    $retorno['oficiais'] = array();

    $resultadoOficiaisResponsaveis      = $aec->listaAtividadeEstatutoOficiais($idAtividadeEstatuto);
    if ($resultadoOficiaisResponsaveis) {
        $retorno['qtdOficiais'] = 0;
        $retorno['qtdOficiaisAtivos'] = 0;
        foreach ($resultadoOficiaisResponsaveis as $vetorOficiaisResponsaveis) {

            switch ($vetorOficiaisResponsaveis['statusOficial']) {
                case 0:
                    $status = "<span class='badge badge-primary'>Ativo</span>";
                    $retorno['qtdOficiaisAtivos'] ++;
                    break;
                case 1:
                    $status = "<span class='badge badge-danger'>Inativo</span>";
                    break;
            }

            switch ($vetorOficiaisResponsaveis['statusOficial']) {
                case 0:
                    $botao = "<a class='btn btn-xs btn-danger' onclick='inativaOficialResponsavel(" .  $idAtividadeEstatuto . " , " . $vetorOficiaisResponsaveis['seqCadastOficial'] . ")' data-rel='tooltip' title='Inativar'> Inativar </a>";
                    break;
                case 1:
                    $botao = "<a class='btn btn-xs btn-primary' onclick='ativaOficialResponsavel(" .  $idAtividadeEstatuto . " , " . $vetorOficiaisResponsaveis['seqCadastOficial'] . ")' data-rel='tooltip' title='Inativar'> Ativar </a>";
                    break;
            }

            $arrNome				= explode(" ",$vetorOficiaisResponsaveis['nomeOficial']);
            $arrNomeDecrescente		= $arrNome[count($arrNome)-1];

            $nome = ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNomeDecrescente,'UTF-8'));

            $retorno['qtdOficiais'] ++;
            $retorno['oficiais']['rows'][] = "<tr><td>". $vetorOficiaisResponsaveis['codAfiliacaoOficial'] . "</td><td>". $nome . "</td><td><center><div id='statusTarget" . $idAtividadeEstatuto . $vetorOficiaisResponsaveis['seqCadastOficial'] . "'>" . $status . "</div></center></td><td><center><div id='botaoOficialResponsavel" . $idAtividadeEstatuto . $vetorOficiaisResponsaveis['seqCadastOficial'] . "'  class='botaoOficialResponsavel" . $idAtividadeEstatuto . "'>" . $botao . "</div></center></td></tr>";
        }
    } else {
        $retorno['qtdOficiais'] = 0;
    }

    echo json_encode($retorno);
}