<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $idAtividadeIniciatica      = isset($_REQUEST['idAtividadeIniciatica']) ? $_REQUEST['idAtividadeIniciatica'] : '';

    include_once('../../model/atividadeIniciaticaClass.php');

    $retorno = Array();
    $retorno['oficiais'] = Array();
    $retorno['resposta'] = false;

    if($idAtividadeIniciatica != ''){

        $aim = new atividadeIniciatica();

        $resultadoVerificacao = $aim->verificaExistenciaRegistroAtividadeSuplente($idAtividadeIniciatica);

        if($resultadoVerificacao){

            $resultado = $aim->listaAtividadeSuplente($idAtividadeIniciatica);
            if($resultado){
                foreach($resultado as $vetor){

                    //echo "<pre>";print_r($vetor);echo "</pre>";

                    $retorno['oficiais']['0']               = $vetor['mestreAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['1']               = $vetor['mestreAuxAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['2']               = $vetor['arquivistaAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['3']               = $vetor['capelaoAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['4']               = $vetor['matreAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['5']               = $vetor['grandeSacerdotisaAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['6']               = $vetor['guiaAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['7']               = $vetor['guardiaoInternoAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['8']               = $vetor['guardiaoExternoAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['9']               = $vetor['archoteAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['10']              = $vetor['medalhistaAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['11']              = $vetor['arautoAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['12']              = $vetor['adjutorAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['13']              = $vetor['sonoplastaAtividadeIniciatica_suplente'];
                    $retorno['oficiais']['14']              = $vetor['columbaAtividadeIniciatica_suplente'];

                    $retorno['resposta'] = true;
                }
            }
        }
    }
    if($retorno['resposta'] == false){
        $retorno['oficiais']['0']               = '';
        $retorno['oficiais']['1']               = '';
        $retorno['oficiais']['2']               = '';
        $retorno['oficiais']['3']               = '';
        $retorno['oficiais']['4']               = '';
        $retorno['oficiais']['5']               = '';
        $retorno['oficiais']['6']               = '';
        $retorno['oficiais']['7']               = '';
        $retorno['oficiais']['8']               = '';
        $retorno['oficiais']['9']               = '';
        $retorno['oficiais']['10']              = '';
        $retorno['oficiais']['11']              = '';
        $retorno['oficiais']['12']              = '';
        $retorno['oficiais']['13']              = '';
        $retorno['oficiais']['14']              = '';
    }

    //echo "<pre>";print_r($retorno);echo "</pre>";

    echo json_encode($retorno);

    exit;
}