<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{
    $lista 	                    = isset($_REQUEST['lista']) ? json_decode($_REQUEST['lista']) : '';

    include_once('../../model/atividadeEstatutoTipoClass.php');

    $aet    = new atividadeEstatutoTipo();

    for($i=0; $i < count($lista); $i++)
    {
				$aet->alteraHierarquiaTipo($lista[$i]->id, ($i+1));
    }

		$retorno = true;

    echo json_encode($retorno);
}
?>
