<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 
    if(realpath('../../model/wsClass.php')){
            require_once '../../model/wsClass.php';
    }else{
            require_once './model/wsClass.php';
    } 

    $codigoAfiliacao 		= isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    $nome					= isset($_REQUEST['nomeMembro'])?$_REQUEST['nomeMembro']:"";
    $tipoMembro				= isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"0";
    $arrNome				= explode(" ",$nome);

    // Instancia a classe
    $ws = new Ws();

    // Nome do Método que deseja chamar
    $method = 'RetornaSeqCadast';

    // Parametros que serão enviados à chamada
    $params = array('CodUsuario' => 'lucianob',
                                    'CodMembro' => $codigoAfiliacao,
                                    'TipoMembro' => $tipoMembro,
                                    'Nome' => $arrNome[0]);

    // Chamada do método
    $return = $ws->callMethod($method, $params, 'lucianob');

    // Imprime o retorno
    echo json_encode($return);
}
?>