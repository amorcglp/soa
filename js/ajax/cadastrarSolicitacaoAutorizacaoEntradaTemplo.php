<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{  

    $seqCadast 				= isset($_REQUEST['seqCadast']) ? $_REQUEST['seqCadast'] : '';
    $idOrganismoAfiliado	= isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado'] : '';
    $usuario				= isset($_REQUEST['usuario'])?$_REQUEST['usuario']:null;

    $arr=array();
    $arr['status']=0;

    include_once('../../model/autorizacaoTemploClass.php');
    //include_once('../../lib/functions.php');
    $a = new autorizacaoTemplo();
    $a->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
    $a->setSeqCadastMembro($seqCadast);
    $a->setUsuario($usuario);

    $retorno = $a->cadastra();

    if ($retorno) {
            $total=0;
        $resultado = $a->selecionaTotalSolicitacoes();
        if($resultado)
        {
            $total = count($resultado);
        }
        $arr['numeroSolicitacoes']=$total;    	
            $arr['status']=1;
    }

    echo json_encode($arr);
}	
?>