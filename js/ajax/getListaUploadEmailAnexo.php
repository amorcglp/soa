<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    include_once('../../model/emailAnexoClass.php');

    $ea = new emailAnexo();

    $resultado = $ea->listaEmailAnexo($_REQUEST['id']);
    $i=1;
    if($resultado)
    {
            if (count($resultado)>0) {
                    echo "<ul>";
                    foreach($resultado as $vetor)
                    {
                            ?>
                            <li>
                                    <a href="<?php echo $vetor['caminho'];?>" target="_blank">Anexo <?php if($resultado->rowCount()>1){?>- Parte <?php echo $i;}?></a>
                                    <a href="#" onclick="excluirUploadEmailAnexo('<?php echo $vetor['idEmailAnexo'];?>','<?php echo $_REQUEST['id'];?>');"><i class="fa fa-trash"></i></a>
                            </li>
                            <?php
                            $i++; 
                    }
                    echo "</ul>";
            }
    } else {
        echo "Nenhuma Arquivo anexado!";
    }
}
?>