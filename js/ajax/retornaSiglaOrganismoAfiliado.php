<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{ 

    $fk_idOrganismoAfiliado 	= isset($_REQUEST['fk_idOrganismoAfiliado']) ? $_REQUEST['fk_idOrganismoAfiliado']  : '';

    $arr=array();

    include_once('../../model/organismoClass.php');
    $o = new organismo();
    $retorno = $o->buscaIdOrganismo($fk_idOrganismoAfiliado);

    $arr['encontrado'] = 0;

    if ($retorno) {
            foreach($retorno as $vetor)
            {
                    $arr['siglaOrganismoAfiliado']		= $vetor['siglaOrganismoAfiliado'];
                    $arr['encontrado']++;
            }
    }

    echo json_encode($arr);
}	
?>