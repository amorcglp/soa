<?php 

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';	
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    require_once '../../webservice/wsInovad.php';

    $seqCadast=isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:null;
    $codigoAfiliacao=0;
    $ideCompanheiro='';

    $ocultar_json=1;

    //Buscar Nome do Usuário vindo do ORCZ
    $vars = array('seq_cadast' => $seqCadast);
    $resposta = json_decode(json_encode(restAmorc("membros/[".$seqCadast."]/ritualistico/1",$vars)),true);
    $obj2 = json_decode(json_encode($resposta),true);
    //echo "<pre>";print_r($obj2);

    $u=0;
    $arrObriga=array();
    if (isset($obj2['data'][$seqCadast][0])) {
        foreach ($obj2['data'][$seqCadast] as $vetor2) {
            $arrObriga[$u]['seqTipoObriga'] = $vetor2['seq_tipo_obriga'];
            $arrObriga[$u]['dat_obriga'] = $vetor2['dat_obriga'];
            $arrObriga[$u]['sig_orgafi'] = $vetor2['sig_orgafi'];
            $u++;
        }
    }

    //echo "<pre>";print_r($arrObriga);

    if(count($arrObriga)>0)
    {
            ?>
            <table class="table">
                      <thead>
                              <tr>
                                      <th>Iniciação</th>
                                      <th>Data</th>
                                      <th>Organismo</th>                                        
                              </tr>
                      </thead>   
                      <tbody>
                    <?php 
                            foreach($arrObriga as $vetor)
                            {
                    ?>
                    <tr>
                            <td>
                                <?php
                                $vars = array('seq_tipo_obriga'=> "[".$vetor['seqTipoObriga']."]", 'ide_tipo_membro' =>1);
                                $resposta3 = json_decode(json_encode(restAmorc("ritualistico/tipos?seq_tipo_obriga=[".$vetor['seqTipoObriga']."]&ide_tipo_membro=1", $vars)),true);
                                $obj3 = json_decode(json_encode($resposta3),true);

                                if (isset($obj3['data'][0])) {
                                    foreach ($obj3['data'] as $vetor3) {
                                        echo $vetor3['des_tipo_obriga'];
                                    }
                                }
                                ?>
                            </td>
                            <td><?php echo substr($vetor['dat_obriga'],8,2)."/".substr($vetor['dat_obriga'],5,2)."/".substr($vetor['dat_obriga'],0,4);?></td>
                            <td><?php echo $vetor['sig_orgafi'];?></td>
                    </tr>
                    <?php 
                            }
                    ?>
            </tbody>
       </table>
            <?php 	
    }else{
            echo "Não possui";
    }
}