<?php

/*
 * Token
 */

if(realpath('../../sec/token.php')){
	require_once '../../sec/token.php';
}else{
	if(realpath('../sec/token.php')){
		require_once '../sec/token.php';
	}else{
		require_once './sec/token.php';
	}
}

if($tokenLiberado)
{

    $seqCadastMembro 	        		= isset($_REQUEST['seqCadastMembro']) ? $_REQUEST['seqCadastMembro'] : '';
    $idAtividade 	            		= isset($_REQUEST['idAtividade']) ? $_REQUEST['idAtividade'] : '';
    $iniciacoes 	            		= isset($_POST['iniciacoes']) ? json_decode($_POST['iniciacoes']) : '';
    $nomeMembro               		= isset($_REQUEST['nomeMembro']) ? $_REQUEST['nomeMembro'] : '';

    $fNumLoteAtualRosacr          = isset($_REQUEST['fNumLoteAtualRosacr']) ? $_REQUEST['fNumLoteAtualRosacr'] : '';
    $fDatAdmissRosacr             = isset($_REQUEST['fDatAdmissRosacr']) ? $_REQUEST['fDatAdmissRosacr'] : '';
    $fIdeTipoSituacMembroRosacr   = isset($_REQUEST['fIdeTipoSituacMembroRosacr']) ? $_REQUEST['fIdeTipoSituacMembroRosacr'] : '';

    //echo "seqCadastMembro: " . $seqCadastMembro;

		function retornaQntMesesMembroDesde($fDatAdmissRosacr){
				$membroDesde = $fDatAdmissRosacr;
				$anoEntrada = substr($membroDesde, 0, 4);
				$anoAtual = date('Y');
				$meses = (($anoAtual - $anoEntrada) - 1) * 12;
				$meses = $meses < 0 ? 0 : $meses;
				$mesEntrada = substr($membroDesde, 5, 2);
				$mesAtual = date('m');
				if($anoEntrada != $anoAtual) {
						$meses = $meses + ($mesAtual + (12 - ($mesEntrada - 1)));
				} else {
						$meses = $meses + $mesAtual;
				}
				//echo "mesesAdmissao".$meses;
				return $meses;
		}

    if($seqCadastMembro != ''){
        include_once('../../model/atividadeIniciaticaClass.php');
        $aim = new atividadeIniciatica();

        $dadosAtiv = $aim->buscaIdAtividadeIniciatica($idAtividade);
        $tipoAtividade = 0;
        if($dadosAtiv){
            foreach($dadosAtiv as $vetorAtiv){
                $tipoAtividade = $vetorAtiv['tipoAtividadeIniciatica'];
            }
        }
				if (($tipoAtividade==13) || ($tipoAtividade==14) || ($tipoAtividade==15) || ($tipoAtividade==16)){
					$tipoAtividade = 1;
				}

        $arrayMembro=array();

        if (isset($iniciacoes)) {
            $c = 0;
            foreach ($iniciacoes->fArrayObrigacoes as $vetorObrigacoes) {
                if(($vetorObrigacoes->fields->fSeqTipoObriga == 0) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 1) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 3) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 5) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 7) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 9) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 11) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 13) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 15) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 17) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 19) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 32) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 33) ||
                    ($vetorObrigacoes->fields->fSeqTipoObriga == 34))
                {
                    $arrayMembro['atividades'][] = $vetorObrigacoes->fields->fSeqTipoObriga;
                    $c++;
                }
            }
            if($c==0){
                $arrayMembro['atividades'][] = 0;
            }
        } else {
            $arrayMembro['atividades'][] = 0;
        }

        for($i=0; $i < count($arrayMembro); $i++){
            if(!empty($arrayMembro['atividades'])){
                if (!sort($arrayMembro['atividades'])) {
                    exit();
                }
            }
        }

        if(!empty($arrayMembro)) {
            /*
            if(isset($arrayMembro['tempoDeMembro'])) {
                $membroDesde = $arrayMembro['tempoDeMembro'];
                $anoEntrada = substr($membroDesde, 0, 4);
                $anoAtual = date('Y');
                $meses = (($anoAtual - $anoEntrada) - 1) * 12;
                $meses = $meses < 0 ? 0 : $meses;
                $mesEntrada = substr($membroDesde, 5, 2);
                $mesAtual = date('m');
                if($anoEntrada != $anoAtual) {
                    $meses = $meses + ($mesAtual + (12 - ($mesEntrada - 1)));
                } else {
                    $meses = $meses + $mesAtual;
                }
                $arrayMembro['tempoDeMembro'] = $meses;
            }
            */
            if (!empty($arrayMembro['atividades'])) {
                for ($z = 0; $z < count($arrayMembro['atividades']); $z++) {
                    if(!empty($arrayMembro['lote'])) {
                        if ($arrayMembro['lote'] >= 6) {
                            $arrayMembro['tempoDeMembro'];
                        }
                    }
                    $grauAtual = $arrayMembro['atividades'][$z];
                }
                switch($grauAtual){
                    case 0: $grau = 0; break;
                    case 1: $grau = 0; break;
                    case 3: $grau = 1; break;
                    case 5: $grau = 2; break;
                    case 7: $grau = 3; break;
                    case 9: $grau = 4; break;
                    case 11: $grau = 5; break;
                    case 13: $grau = 6; break;
                    case 15: $grau = 7; break;
                    case 17: $grau = 8; break;
                    case 19: $grau = 9; break;
                    case 32: $grau = 10; break;
                    case 33: $grau = 11; break;
                    case 34: $grau = 12; break;
                }

								/**
-----------------------------------------------------------------------------------------------------------
								*/

								if(isset($fDatAdmissRosacr)) {
											$mesesAdmissaoRC = retornaQntMesesMembroDesde($fDatAdmissRosacr);
								}

                                //echo "GrauMembro:".$grau."/mesesAdmissao:".$mesesAdmissaoRC."/lote:".$fNumLoteAtualRosacr;

								if(isset($mesesAdmissaoRC) && isset($grau) && isset($fNumLoteAtualRosacr)){
										//echo "grau: ".$grau." | ".$mesesAdmissaoRC." | ".$fNumLoteAtualRosacr."<br>";
										if(($grau == 0) && ($mesesAdmissaoRC >= 17) && ($fNumLoteAtualRosacr >= 6)){
												$grauDisponivel = 1;
										} else if(($grau == 1) && ($mesesAdmissaoRC >= 20) && ($fNumLoteAtualRosacr >= 7)){
												$grauDisponivel = 2;
										} else if(($grau == 2) && ($mesesAdmissaoRC >= 23) && ($fNumLoteAtualRosacr >= 8)){
												$grauDisponivel = 3;
										} else if(($grau == 3) && ($mesesAdmissaoRC >= 26) && ($fNumLoteAtualRosacr >= 9)){
												$grauDisponivel = 4;
										} else if(($grau == 4) && ($mesesAdmissaoRC >= 29) && ($fNumLoteAtualRosacr >= 10)){
												$grauDisponivel = 5;
										} else if(($grau == 5) && ($mesesAdmissaoRC >= 32) && ($fNumLoteAtualRosacr >= 11)){
												$grauDisponivel = 6;
										} else if(($grau == 6) && ($mesesAdmissaoRC >= 39) && ($fNumLoteAtualRosacr >= 13)){
												$grauDisponivel = 7;
										} else if(($grau == 7) && ($mesesAdmissaoRC >= 46) && ($fNumLoteAtualRosacr >= 16)){
												$grauDisponivel = 8;
										} else if(($grau == 8) && ($mesesAdmissaoRC >= 55) && ($fNumLoteAtualRosacr >= 19)){
												$grauDisponivel = 9;
										} else if(($grau == 9) && ($mesesAdmissaoRC >= 68) && ($fNumLoteAtualRosacr >= 23)){
												$grauDisponivel = 10;
										} else if(($grau == 10) && ($mesesAdmissaoRC >= 93) && ($fNumLoteAtualRosacr >= 32)){
												$grauDisponivel = 11;
										} else if(($grau == 11) && ($mesesAdmissaoRC >= 131) && ($fNumLoteAtualRosacr >= 45)){
												$grauDisponivel = 12;
										}  else if($grau == 12){
												$grauDisponivel = null;
										} else {
												$grauDisponivel = null;
										}
								}

								/**
                                    Parte crucial do código onde é concedido ou não a disponibilidade
                                 * Grau 1 / meses 89 / lote 10 => grau disponivel 2
								*/
                                //echo "tipoAtividade: ".$tipoAtividade." | grau:".$grau." | grauDisponibvel2: ".$grauDisponivel."<br>";
								if(($tipoAtividade <= $grau) || ($grau <= $grauDisponivel)){
                    $resultadoEncontrado = $aim->verificaMembroNaAtividadeIniciatica($seqCadastMembro,$idAtividade);
                    if ($resultadoEncontrado==true) {
												//echo "alteraMembroAtividadeIniciatica";
                        $resultado = $aim->alteraMembroAtividadeIniciatica($idAtividade, $seqCadastMembro);
                    } else {
												//echo "cadastraMembroAtividadeIniciatica";
                        $resultado = $aim->cadastraMembroAtividadeIniciatica($seqCadastMembro, $idAtividade, $nomeMembro);
                    }
                    if($resultado){
                        $retorno['disponibilidade'] = true;
                    } else {
                        $retorno['disponibilidade'] = false;
                    }
                } else {
                    $retorno['disponibilidade'] = false;
                }
            } else {
                $retorno['disponibilidade'] = false;
            }
        } else {
            $retorno['disponibilidade'] = false;
        }
    } else {
        $retorno['disponibilidade'] = false;
    }


    echo json_encode($retorno);
}
