<?php

$db = include('../../firebase.php');

/*
 * Token
 */

$includePath = [
    '../../sec/token.php',
    '../sec/token.php',
    './sec/token.php'
];

foreach($includePath as $path) {
    if(realpath($path)) {
        require_once $path;
        break;
    }
}

if($tokenLiberado)
{

    $idString		 			= isset($_REQUEST['id']) ? strval($_REQUEST['id']) : '';

    //Pegar a data e o id do organismo
    include_once('../../model/recebimentoClass.php');
    $r = new Recebimento();
    $resultadoInformacoesSaldo = $r->buscarIdRecebimento($_REQUEST['id']);
    if($resultadoInformacoesSaldo)
    {
        foreach ($resultadoInformacoesSaldo as $is)
        {
            $dataRecebimento = $is['dataRecebimento'];
            $fk_idOrganismoAfiliado = $is['fk_idOrganismoAfiliado'];
        }
    }

    //echo "<br><br>Saldo anterior:".$saldoAnterior;exit();

    //echo "<br>Tempo 4 -Iniciando busca nas Tabelas Recebimentos e Despesas do SOA: ".date("d/m/Y H:i:s");



    //Remover recebimento

    $resultado2 = $r->selecionaUltimoId();
    if($resultado2)
    {
            foreach ($resultado2 as $vetor2)
            {
                    $proximo_id = $vetor2['Auto_increment'];
            }
    }

    $valorRecebimento=0;
    $resultado = $r->buscarIdRecebimento($_REQUEST['id']);
    if ($resultado) {
        foreach ($resultado as $v) {
            $valorRecebimento = floatval($v['valorRecebimento']);
            $dataRecebimento = $v['dataRecebimento'];
            $fk_idOrganismoAfiliado = $v['fk_idOrganismoAfiliado'];
        }
    }

    $resultado = $r->removeRecebimento($_REQUEST['id']);

    $arr['status']=0;

    if ($resultado) {

        $arr['status'] = 1;
        $arr['proximo_id'] = $proximo_id;

    } else {
        $arr['erro'] = 'Ocorreu algum erro ao selecionar as informações no banco de dados.\nTente novamente ou entre em contato com o setor de TI.';
    }

    /*
     * Firebase
     */

    //Busca pelo id do Documento no Firebase

//    $recebimentos = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/recebimentos");
//
//    $queryWhereR = $recebimentos
//        ->where('idRecebimentoSOA', '=', $idString);
//
//    $queryRecebimentos = $queryWhereR->documents();
//
//    //Percorrer recebimentos
//    foreach ($queryRecebimentos as $documentRec) {
//        if ($documentRec->exists()) {
//            $idRefDocumentoFirebase = $documentRec->id();
//            $db->collection("balances/" . $fk_idOrganismoAfiliado . "/recebimentos")->document($idRefDocumentoFirebase)->delete();
//            $arr['idRefDocumentoFirebase'] = $idRefDocumentoFirebase;
//        }
//    }

    //echo "<br>";////////////////////////////////////////////////////////////////////////NÃO ESQUECER DE TIRAR
    echo json_encode($arr);
}    
?>