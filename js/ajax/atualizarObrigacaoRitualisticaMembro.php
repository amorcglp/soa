<?php

    if(realpath('../../model/atividadeIniciaticaClass.php')){
        require_once '../../model/atividadeIniciaticaClass.php';
    }else{
        if(realpath('../model/atividadeIniciaticaClass.php')){
            require_once '../model/atividadeIniciaticaClass.php';
        }else{
            require_once './model/atividadeIniciaticaClass.php';
        }
    }
//echo "1";
    $atividadeIniciatica = new atividadeIniciatica();
//echo "2";

    if(realpath('../../webservice/wsInovad.php')){
        require_once '../../webservice/wsInovad.php';
    }else{
        if(realpath('../webservice/wsInovad.php')){
            require_once '../webservice/wsInovad.php';
        }else{
            require_once './webservice/wsInovad.php';
        }
    }

    if(realpath('../../lib/functions.php')){
        require_once '../../lib/functions.php';
    }else{
        if(realpath('../lib/functions.php')){
            require_once '../lib/functions.php';
        }else{
            require_once './lib/functions.php';
        }
    }
//echo "3";
    $fk_idOrganismoAfiliado     = isset($_REQUEST['fk_idOrganismoAfiliado'])?$_REQUEST['fk_idOrganismoAfiliado']:null;
    $seqAtualizadoPor           = isset($_REQUEST['seqAtualizadoPor'])?$_REQUEST['seqAtualizadoPor']:0;
    $loginAtualizadoPor         = isset($_REQUEST['loginAtualizadoPor'])?$_REQUEST['loginAtualizadoPor']:0;
    $seqCadast                  = isset($_REQUEST['seqCadast'])?$_REQUEST['seqCadast']:0;
    $codigoAfiliacao            = isset($_REQUEST['codigoAfiliacao'])?$_REQUEST['codigoAfiliacao']:0;
    $tipoMembro				    = isset($_REQUEST['tipoMembro'])?$_REQUEST['tipoMembro']:"";
    $tipoObrigacao			    = isset($_REQUEST['tipoObrigacao'])?$_REQUEST['tipoObrigacao']:"";
    $dataObrigacao			    = isset($_REQUEST['dataObrigacao'])?$_REQUEST['dataObrigacao']:"";
    $siglaOA				    = isset($_REQUEST['siglaOA'])?$_REQUEST['siglaOA']:"";
    $descricaoLocal			    = isset($_REQUEST['descricaoLocal'])?$_REQUEST['descricaoLocal']:"";
    $companheiro			    = isset($_REQUEST['companheiro'])?$_REQUEST['companheiro']:"";
    $usuarioLogin			    = isset($_REQUEST['usuarioLogin'])?$_REQUEST['usuarioLogin']:"";


    //$return = $ws->callMethod($method, $params, 'lucianob');

    $credentials = montaCredenciais();

    $ano = substr($dataObrigacao,0,4);
    $mes = substr($dataObrigacao,5,2);
    $dia = substr($dataObrigacao,8,2);
    $hora = substr($dataObrigacao,11,8);

    $dataObrigacao = $ano."-".$mes."-".$dia." ".$hora;

    //echo "data=>".$dataObrigacao;

    $arrDatasOrcz = array();
    $arrDatasOrcz[] = "0000-00-00";

    $array = [0 => $seqCadast];
    $teste = '';
    foreach ($array as $key => $value) {
        if ($teste != '') {
            $teste = $teste . ', ' . $value;
        } else {
            $teste = $value;
        }
    }

    //Buscar Nome do Usuário vindo do ORCZ
    $vars = array('seq_cadast' => $teste);
    $resposta = json_decode(json_encode(restAmorc("membros/" . $seqCadast . "/ritualistico/1", $vars)), true);
    $obj2 = json_decode(json_encode($resposta), true);
    //echo "<pre>";print_r($obj2);

    if (isset($obj2['data'][0])) {
        foreach ($obj2['data'] as $vetor2) {

            $dataOrcz = substr($vetor2['dat_obriga'],0,10);
            //echo "==>GrauOrcz:".$grauOrcz;

            if (!in_array($dataOrcz, $arrDatasOrcz)) {

                $arrDatasOrcz[] = $dataOrcz;

            }
        }

    }
/*
    echo "<br>Iniciações do membro no ORCZ:";
    echo "<pre>";
    print_r($arrGrausOrcz);
*/
    //$grauSOA = retornaGrauRCSOA($tipoObrigacao);

    $dataSOA = substr($dataObrigacao,0,10);

    if ($dataSOA != 0) {
        if (!in_array($dataSOA, $arrDatasOrcz)) {

            $vars = Array('form_params' =>
                Array('seq_tipo_obriga' => $tipoObrigacao,
                    'sig_orgafi' => $siglaOA,
                    'des_local_obriga' => str_replace(",", "", utf8_encode($descricaoLocal)),
                    'dat_obriga' => $dataObrigacao,
                    'cod_usuari' => 'lucianob'),
                'headers' => Array('Authorization' => 'Basic ' . $credentials),
                'verify' => false);

            //print_r($vars);exit();

            $resposta = json_decode(json_encode(restAmorc("membros/" . $seqCadast . "/ritualistico/1", $vars, 'POST')), true);
            $obj2 = json_decode(json_encode($resposta), true);
            //echo "<pre>";
            //print_r($obj2);
            //exit();

            $return = array();
            if ($obj2['success'] == true) {
                $return['status'] = 1;
            } else {
                $return['status'] = 2;
            }

            // Imprime o retorno
            echo json_encode($return);
        }else{
            $return['status'] = 3;//já existe iniciação para esta data
        }
    }

?>