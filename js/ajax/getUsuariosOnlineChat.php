<?php
    include_once("../../model/usuarioClass.php");
    include_once("../../lib/functions.php");
    
    $para= isset($_REQUEST['para'])?$_REQUEST['para']:null;
    
    $u = new Usuario();
    $resultado = $u->listaUsuario();

    if ($resultado) {

        //Online primeiro

        foreach ($resultado as $vetor) {
                    if ($_REQUEST['seqCadast'] != $vetor['seqCadast']) {
                            $online=false;
                            if(verificaSeUsuarioEstaOnline($vetor['seqCadast']))
                            {
                                $online=true;
                                $vetor['avatarUsuario'] =   '../../'.$vetor['avatarUsuario'];
                                 ?>
                                <div class="chat-user" <?php if ($para == $vetor['seqCadast']) { ?>style="background-color: #BEBEBE;" <?php } ?>>
                                    <?php if($online){?><span class="pull-right label label-primary">Online</span><?php }else{?><span class="pull-right label label-warning-light">Offline</span><?php }?>
                                    <img class="chat-avatar" src="<?php if ($vetor['avatarUsuario'] == "../../"|| !file_exists($vetor['avatarUsuario'])) { ?>../../img/default-user-small.png<?php } else {str_replace("../../","",$vetor['avatarUsuario']);
                                            echo $vetor['avatarUsuario'];
                                        } ?>" alt="" >
                                    <div class="chat-user-name">
                                        <a
                                                href="?corpo=chat&para=<?php echo $vetor['seqCadast']; ?>"><?php if ($para == $vetor['seqCadast']) { ?><b><?php } ?>
                                                <?php echo retornaNomeFormatado($vetor['nomeUsuario']); ?> <?php if ($para == $vetor['seqCadast']) { ?>
                                        </b> <?php } ?> </a>
                                        <?php
                                        //Verficação se tem mensagem nova para mim
                                        $u2 = new Usuario();
                                        $resultado2 = $u2->verificaSeMandouMensagemNova($_SESSION['seqCadast'], $vetor['seqCadast']);
                                        if ($resultado2) {
                                                ?>
                                        <i class="fa fa-comments"></i>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            <?php
                            }
                    }
        }    

        //Offline por segundo

        foreach ($resultado as $vetor) {
                    if ($_REQUEST['seqCadast'] != $vetor['seqCadast']) {
                        $online=false;
                        if(!verificaSeUsuarioEstaOnline($vetor['seqCadast']))
                        {
                            $online=false;
                            
                            $vetor['avatarUsuario'] =   '../../'.$vetor['avatarUsuario'];
                            ?>
                            <div class="chat-user" <?php if ($para == $vetor['seqCadast']) { ?>style="background-color: #BEBEBE;" <?php } ?>>
                                <?php if($online){?><span class="pull-right label label-primary">Online</span><?php }else{?><span class="pull-right label label-warning-light">Offline</span><?php }?>
                                <img class="chat-avatar" src="<?php if ($vetor['avatarUsuario'] == "../../"|| !file_exists($vetor['avatarUsuario'])) { ?>../../img/default-user-small.png<?php } else {str_replace("../../","",$vetor['avatarUsuario']);
                                        echo $vetor['avatarUsuario'];
                                    } ?>" alt="" >
                                <div class="chat-user-name">
                                    <a
                                            href="?corpo=chat&para=<?php echo $vetor['seqCadast']; ?>"><?php if ($para == $vetor['seqCadast']) { ?><?php } ?>
                                            <?php echo retornaNomeFormatado($vetor['nomeUsuario']); ?> <?php if ($para == $vetor['seqCadast']) { ?>
                                     <?php } ?> </a>
                                    <?php
                                    //Verficação se tem mensagem nova para mim
                                    $u2 = new Usuario();
                                    $resultado2 = $u2->verificaSeMandouMensagemNova($_REQUEST['seqCadast'], $vetor['seqCadast']);
                                    if ($resultado2) {
                                            ?>
                                    <i class="fa fa-comments"></i>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                    }
                }    
        }
    }
                                        

?>