/** Funções específicas Braz */

$(document).ready(function () {
    //Class datapicker
    $('.datapicker .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });
});

$(document).ready(function () {

    $('#datapicker_ata .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });
});

$(document).ready(function () {

    $('#datapicker_data_entrada .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });

});

$(document).ready(function () {

    $('#datapicker_data_saida .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });

});

$(document).ready(function () {

    $('#datapicker_data_termino_mandato .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });

});

function retornaDadosMembroPresidenteECompanheiro()
{
    var codigoAfiliacao = $("#codAfiliacaoMembroPresidente").val();
    var nomeMembro = $("#nomeMembroPresidente").val();
    var tipoMembro = '1';

    //console.log("retornaDados() --- Cód. Afiliação: "+codigoAfiliacao+" Nome: "+nomeMembro+" Tipo: "+tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);

    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.result[0].fields.fNomCliente != "")
            {
                $("#h_seqCadastMembroPresidente").val(data.result[0].fields.fSeqCadast);
                $("#nomeMembroPresidente").val(data.result[0].fields.fNomCliente);
                $("#h_nomeMembroPresidente").val(data.result[0].fields.fNomCliente);
                $("#h_seqCadastCompanheiroMembroPresidente").val(data.result[0].fields.fSeqCadastCompanheiroRosacruz);
                $("#h_nomeCompanheiroMembroPresidente").val(data.result[0].fields.fNomConjuge);
            } else {
                swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
            }

        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

$(document).ready(function () {

    $('#companheiroMembroPresidente').change(function () {
        if ($(this).is(":checked")) {
            if ($("#h_nomeCompanheiroMembroPresidente").val() == "")
            {
                swal({title: "Aviso!", text: "Este membro não possui companheiro", type: "warning", confirmButtonColor: "#1ab394"});
                $(this).prop("checked", false);
            } else {
                $("#nomeMembroPresidente").val($("#h_nomeCompanheiroMembroPresidente").val());
            }
        } else {
            $("#nomeMembroPresidente").val($("#h_nomeMembroPresidente").val());
        }
    });

});

function retornaDadosMembroOficialECompanheiro()
{
    var codigoAfiliacao = $("#codAfiliacaoMembroOficial").val();
    var nomeMembro = $("#nomeMembroOficial").val();
    var tipoMembro = '1';

    //console.log("retornaDados() --- Cód. Afiliação: "+codigoAfiliacao+" Nome: "+nomeMembro+" Tipo: "+tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);

    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if (data.result[0].fields.fNomCliente != "")
            {
                //alert(data.result[0].fields.fDesMensag);
                $("#h_seqCadastMembroOficial").val(data.result[0].fields.fSeqCadast);
                $("#nomeMembroOficial").val(data.result[0].fields.fNomCliente);
                $("#h_nomeMembroOficial").val(data.result[0].fields.fNomCliente);
                $("#h_seqCadastCompanheiroMembroOficial").val(data.result[0].fields.fSeqCadastCompanheiroRosacruz);
                $("#h_nomeCompanheiroMembroOficial").val(data.result[0].fields.fNomConjuge);
                //alert("princ:"+data.result[0].fields.fSeqCadast+"-"+data.result[0].fields.fSeqCadastPrincipalRosacruz);
                if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalRosacruz)
                {
                    $("#principalCompanheiro").val(1);
                }
                if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroRosacruz)
                {
                    $("#principalCompanheiro").val(2);
                }
            } else {
                swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
            }

        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

$(document).ready(function () {

    $("#dataAtaReuniaoMensal").mask("99/99/9999");
    $("#horaInicialAtaReuniaoMensal").mask("99:99");
    $("#horaFinalAtaReuniaoMensal").mask("99:99");

});

$(document).ready(function () {

    $(".mascaraData").mask("99/99/9999");
});

$(document).on('ready', function () {
    $('[data-toggle="tooltip"]').tooltip();
});


function verificaSeJaExisteNaListaOficiais(seqCadast)
{

    var lb = document.getElementById("ata_oa_mensal_oficiais");

    for (var i = 0; i < lb.length; i++) {
        if (lb.options[i].value == seqCadast)
        {
            return true;
        }
    }

    return false;
}

function verificaSeJaExisteNaListaNaoOficiais(nome,tipo)
{

    var lb = document.getElementById("ata_oa_mensal_nao_oficiais");

    for (var i = 0; i < lb.length; i++) {
        if (lb.options[i].value == (nome+'@'+tipo))
        {
            return true;
        }
    }

    return false;
}

function verificaSeJaExisteNaListaMembros(seqCadast)
{

    var lb = document.getElementById("membros");

    for (var i = 0; i < lb.length; i++) {
        if (lb.options[i].value == seqCadast)
        {
            return true;
        }
    }

    return false;
}

function incluiOficiaisAtaReuniaoMensal()
{
    $(".chosen-choices").html('');
    $(".chosen-results").html('');

    var codigoAfiliacao = $("#codAfiliacaoMembroOficial").val();
    if ($('#companheiroMembroOficial').is(":checked")) {
        var seqCadast = $("#h_seqCadastCompanheiroMembroOficial").val();
    } else {
        var seqCadast = $("#h_seqCadastMembroOficial").val();
    }

    var nomeMembro = $("#nomeMembroOficial").val();

    if (verificaSeJaExisteNaListaOficiais(seqCadast))
    {
        swal({title: "Aviso!", text: "Este membro já consta na lista!", type: "warning", confirmButtonColor: "#1ab394"});
    } else {
        if (seqCadast == 0)
        {
            swal({title: "Aviso!", text: "Insira este membro novamente", type: "warning", confirmButtonColor: "#1ab394"});
        } else {
            $("#ata_oa_mensal_oficiais").append('<option value="' + seqCadast + '" id="">[' + codigoAfiliacao + ']&nbsp;' + nomeMembro + '</option>');
        }
    }
    $("#codAfiliacaoMembroOficial").val("");
    $("#nomeMembroOficial").val("");
}

function incluiMembrosConvocacaoRitualistica()
{
    $(".chosen-choices").html('');
    $(".chosen-results").html('');

    var codigoAfiliacao = $("#codAfiliacaoMembroOficial").val();
    var seqCadast = $("#h_seqCadastMembroOficial").val();
    var nomeMembro = $("#nomeMembroOficial").val();
    var tipoMembro = $("#tipo").val();
    var emailMembro = $("#emailMembro").val();
    
    //Montar JSON
    var json = "{'emailMembro' : '"+emailMembro+"', 'tipoMembro' : "+tipoMembro+", 'seqCadast': "+seqCadast+"}";

    if (verificaSeJaExisteNaListaOficiais(seqCadast))
    {
        swal({title: "Aviso!", text: "Este membro já consta na lista!", type: "warning", confirmButtonColor: "#1ab394"});
    } else {
        if (seqCadast == 0)
        {
            swal({title: "Aviso!", text: "Insira este membro novamente", type: "warning", confirmButtonColor: "#1ab394"});
        } else {
            $("#ata_oa_mensal_oficiais").append('<option value="' +  json +'" id="">[' + codigoAfiliacao + ']&nbsp;' + nomeMembro + '</option>');
        }
    }
    $("#codAfiliacaoMembroOficial").val("");
    $("#nomeMembroOficial").val("");
    $("#tipoMembro").val(0);
    $("#emailMembro").val("");
}

function incluiMembrosTestemunhas()
{
    $(".chosen-choices").html('');
    $(".chosen-results").html('');

    var codigoAfiliacao = $("#codAfiliacaoMembroOficialTestemunha").val();
    var seqCadast = $("#h_seqCadastMembroOficialTestemunha").val();
    var nomeMembro = $("#nomeMembroOficialTestemunha").val();
    var tipoMembro = $("#tipoTestemunha").val();
    var emailMembro = $("#emailMembroTestemunha").val();
    
    //Montar JSON
    var json = "{'emailMembro' : '"+emailMembro+"', 'tipoMembro' : "+tipoMembro+", 'seqCadast': "+seqCadast+"}";

    if (verificaSeJaExisteNaListaOficiais(seqCadast))
    {
        swal({title: "Aviso!", text: "Este membro já consta na lista!", type: "warning", confirmButtonColor: "#1ab394"});
    } else {
        if (seqCadast == 0)
        {
            swal({title: "Aviso!", text: "Insira este membro novamente", type: "warning", confirmButtonColor: "#1ab394"});
        } else {
            $("#ata_oa_mensal_oficiais_testemunhas").append('<option value="' +  json +'" id="">[' + codigoAfiliacao + ']&nbsp;' + nomeMembro + '</option>');
        }
    }
    $("#codAfiliacaoMembroOficialTestemunha").val("");
    $("#nomeMembroOficialTestemunha").val("");
    $("#tipoMembroTestemunha").val(0);
    $("#emailMembroTestemunha").val("");
}

function incluiNaoMembrosConvocacaoRitualistica()
{
    $(".chosen-choices").html('');
    $(".chosen-results").html('');

    var nomeNaoMembro = $("#nomeNaoMembro").val();
    var tipoNaoMembro = $("#tipoNaoMembro").val();
    var emailNaoMembro = $("#emailNaoMembro").val();
    
    //Montar JSON
    var json = "{'emailNaoMembro' : '"+emailNaoMembro+"', 'tipoNaoMembro' : "+tipoNaoMembro+", 'nomeNaoMembro': '"+nomeNaoMembro+"'}";

    if (verificaSeJaExisteNaListaNaoOficiais(nomeNaoMembro,tipoNaoMembro))
    {
        swal({title: "Aviso!", text: "Este nome já consta na lista!", type: "warning", confirmButtonColor: "#1ab394"});
    } else {
        if (nomeNaoMembro == "")
        {
            swal({title: "Aviso!", text: "Insira este membro novamente", type: "warning", confirmButtonColor: "#1ab394"});
        } else {
            $("#ata_oa_mensal_nao_oficiais").append('<option value="' + json+'" id="">' + nomeNaoMembro + '</option>');
        }
    }
    
    $("#nomeNaoMembro").val("");
    $("#tipoNaoMembro").val(0);
    $("#emailNaoMembro").val("");
}

function incluiMembrosRelatorioClasseArtesaos()
{
    $(".chosen-choices").html('');
    $(".chosen-results").html('');

    var codigoAfiliacao = $("#codAfiliacaoMembro").val();
    if ($('#companheiroMembroOficial').is(":checked")) {
        var seqCadast = $("#h_seqCadastCompanheiroMembro").val();
    } else {
        var seqCadast = $("#h_seqCadastMembro").val();
    }

    var nomeMembro = $("#nomeMembro").val();

    if (verificaSeJaExisteNaListaMembros(seqCadast))
    {
        swal({title: "Aviso!", text: "Este membro já consta na lista!", type: "warning", confirmButtonColor: "#1ab394"});
    } else {
        if (seqCadast == 0)
        {
            swal({title: "Aviso!", text: "Insira este membro novamente", type: "warning", confirmButtonColor: "#1ab394"});
        } else {
            $("#membros").append('<option value="' + seqCadast + '" id="">[' + codigoAfiliacao + ']&nbsp;' + nomeMembro + '</option>');
        }
    }
    $("#codAfiliacaoMembro").val("");
    $("#nomeMembro").val("");
}

function validaAtaReuniaoMensal()
{
    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }

    if ($("#dataAtaReuniaoMensal").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a data da reunião", type: "warning", confirmButtonColor: "#1ab394"});
        $("#dataAtaReuniaoMensal").focus();
        return false;
    }

    if ($("#horaInicialAtaReuniaoMensal").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a hora inicial da reunião", type: "warning", confirmButtonColor: "#1ab394"});
        $("#horaInicialAtaReuniaoMensal").focus();
        return false;
    }

    if ($("#horaFinalAtaReuniaoMensal").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a hora final da reunião", type: "warning", confirmButtonColor: "#1ab394"});
        $("#horaFinalAtaReuniaoMensal").focus();
        return false;
    }

    if ($("#numeroMembrosAtaReuniaoMensal").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o número de membros", type: "warning", confirmButtonColor: "#1ab394"});
        $("#numeroMembrosAtaReuniaoMensal").focus();
        return false;
    }

    if ($("#codAfiliacaoMembroPresidente").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o código de afiliação do presidente da reunião", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codAfiliacaoMembroPresidente").focus();
        return false;
    }

    if ($("#nomeMembroPresidente").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o primeiro nome do presidente da reunião e pressione TAB", type: "warning", confirmButtonColor: "#1ab394"});
        $("#nomeMembroPresidente").focus();
        return false;
    }

    var frm = document.forms['ataReuniaoMensal']['ata_oa_mensal_oficiais[]'].selectedIndex;

    if (frm == -1)
    {
        swal({title: "Aviso!", text: "Inclua pelo menos um oficial administrativo nesta ata", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    return true;
}

function mostraDetalhesAtaReuniaoMensal(idAta)
{
    $.ajax({
        type: "post",
        url: "js/ajax/mostraDetalhesAtaReuniaoMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {idAta: idAta}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#dataAtaReuniaoMensal").html(data.dataAtaReuniaoMensal);
            $("#horaInicialAtaReuniaoMensal").html(data.horaInicialAtaReuniaoMensal);
            $("#horaFinalAtaReuniaoMensal").html(data.horaFinalAtaReuniaoMensal);
            $("#numeroMembrosAtaReuniaoMensal").html(data.numeroMembrosAtaReuniaoMensal);
            $("#nomePresidenteAtaReuniaoMensal").html(data.nomePresidenteAtaReuniaoMensal);
            $("#oficiaisAdministrativos").html(data.oficiaisAdministrativos);
            $("#topico_um").html(data.topico_um);
            $("#topico_dois").html(data.topico_dois);
            $("#topico_tres").html(data.topico_tres);
            $("#topico_quatro").html(data.topico_quatro);
            $("#topico_cinco").html(data.topico_cinco);
            $("#topico_seis").html(data.topico_seis);
            $("#topico_sete").html(data.topico_sete);
            $("#topico_oito").html(data.topico_oito);
            $("#topico_nove").html(data.topico_nove);
            $("#topico_dez").html(data.topico_dez);
            $("#data").html(data.data);
            $("#cadastrado_por").html(data.cadastrado_por);
            $("#atualizado_por").html(data.atualizado_por);
            $("#data_entrega").html(data.dataEntrega);
            $("#quem_entregou").html(data.quemEntregou);
            $("#oficiais_que_assinaram").html(data.oficiaisQueAssinaram);
            $("#assinaturas").html(data.dataEntregouCompletamente);
            if(data.entregue==1)
            {
                $("#faltam_assinar").html(data.faltaAssinar);
            }else{
                $("#faltam_assinar").html("--");
            }

            if(data.numeroAssinatura!="")
            {
                $("#numero_assinatura").html(data.numeroAssinatura);
            }else{
                $("#numero_assinatura").html("--");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

$(document).ready(function () {

    $('.summernote').summernote({
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });

});

$(document).ready(function () {
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
});

$(function () {
    //Máscaras
    //$("#telefoneFixoOrganismoAfiliado").mask("(999) 9999-9999");
    //$("#faxOrganismoAfiliado").mask("(999) 9999-9999");
    //$("#outroTelefoneOrganismoAfiliado").mask("(999) 9999-9999");
    //$("#celularUsuario").mask("(999) 9999-9999");

    jQuery('input[type=tel]').mask("(999) 9999-9999?9").ready(function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 10) {
            element.mask("(999) 99999-999?9");
        } else {
            element.mask("(999) 9999-9999?9");
        }
    });
})

function retornaDadosOrganismo()
{
    var siglaOA = $("#siglaOrganismoAfiliado").val();
    var paisOrganismo = $("#paisOrganismoAfiliado").val();
    var pais = "BR";
    if (paisOrganismo == 2)
    {
        pais = "PT";
    }
    if (paisOrganismo == 3)
    {
        pais = "AO";
    }

    if (siglaOA != "")
    {
        $.ajax({
            type: "post",
            url: "js/ajax/retornaDadosOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {siglaOA: siglaOA, pais: pais}, // exemplo: {codigo:125, nome:"Joaozinho"}  
            success: function (data) {
                //alert(data);
                $("#seq_cadast").val(data.result[0].fields.fArrayOA[0].fields.fSeqCadast);
                $("#codigoAfiliacaoOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fCodRosacr);
                $("#bairroOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNomBairro.trim());
                $("#enderecoOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fDesLograd.trim());
                $("#numeroOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNumEndere);
                $("#complementoOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fDesCompleLograd);
                $("#cepOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fCodCep.trim());
                $("#emailOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fDesEmail.trim());
                if (data.result[0].fields.fArrayOA[0].fields.fSigPaisEndere == "BR")
                {
                    selecionaEstadoApartirUf(data.result[0].fields.fArrayOA[0].fields.fSigUf);
                }
                //alert(data.result[0].fields.fArrayOA[0].fields.fSituacaoOa);
                if (data.result[0].fields.fArrayOA[0].fields.fSituacaoOa == "R")
                {
                    $("#situacaoOrganismoAfiliado").val(1);
                }
                if (data.result[0].fields.fArrayOA[0].fields.fSituacaoOa == "F")
                {
                    $("#situacaoOrganismoAfiliado").val(2);
                }
                if (data.result[0].fields.fArrayOA[0].fields.fSituacaoOa == "A")
                {
                    $("#situacaoOrganismoAfiliado").val(3);
                }
                $("#cnpjOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNumCnpj.trim());
                $("#celularOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNumCelula.trim());
                $("#telefoneFixoOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNumTelefoFixo.trim());
                $("#outroTelefoneOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNumOutroTelefo.trim());
                $("#faxOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNumFax.trim());
                $("#caixaPostalOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fNumCaixaPostal.trim());
                $("#cepCaixaPostalOrganismoAfiliado").val(data.result[0].fields.fArrayOA[0].fields.fCodCepCaixaPostal.trim());
                if (data.result[0].fields.fArrayOA[0].fields.fIdeEndereCorres == "1")
                {
                    $("#EC_logradouro").attr("checked", true);
                } else {
                    $("#EC_caixaPostal").attr("checked", true);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert('erro');
            }
        });
    }
}

function selecionaEstadoApartirUf(uf)
{

    $.ajax({
        type: "post",
        url: "js/ajax/retornaEstado.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {uf: uf}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {

            $("#fk_idEstado").val(data.idEstado);
            carregaCombo(data.idEstado, 'fk_idCidade');
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

$(document).ready(function () {

    $('#companheiroMembroOficial').change(function () {
        if ($(this).is(":checked")) {

            if ($("#h_nomeCompanheiroMembroOficial").val() == "")
            {
                swal({title: "Aviso!", text: "Este membro não possui companheiro", type: "warning", confirmButtonColor: "#1ab394"});
                $(this).prop("checked", false);
            } else {
                $("#nomeMembroOficial").val($("#h_nomeCompanheiroMembroOficial").val());
            }
        } else {
            $("#nomeMembroOficial").val($("#h_nomeMembroOficial").val());
        }
    });

});

function retornaTipoCargo(funcao)
{
    if (funcao >= 100 && funcao < 200)
    {
        return 100;//Dignatário
    }
    if (funcao >= 200 && funcao < 300)
    {
        return 200;//Administrativo
    }
    if (funcao >= 300 && funcao < 400)
    {
        return 300;//Ritualistico
    }
    if (funcao >= 400 && funcao < 500)
    {
        return 400;//Iniciático Equipe I
    }
    if (funcao >= 500 && funcao < 600)
    {
        return 500;//Iniciático Equipe II e III
    }
    if (funcao >= 600 && funcao < 700)
    {
        return 600;//Outros cargos em comissões
    }
    if (funcao >= 700 && funcao < 800)
    {
        return 700;//Oficiais templários na GLP
    }
    if (funcao >= 800 && funcao < 900)
    {
        return 800;//Dignatários SUPREMA e GLP
    }
    if (funcao >= 900 && funcao < 1000)
    {
        return 900;//Moradeiro
    }
    return 0;
}

function atualizarOficial()
{
    var siglaOrganismoAfiliado = $("#siglaOrganismoAfiliado").val();
    //alert("sigla:"+siglaOrganismoAfiliado);
    var codAfiliacaoMembroOficial = $("#codAfiliacaoMembroOficial").val();
    //alert("codAfiliacao:"+codAfiliacaoMembroOficial);
    var companheiro = "";
    var tipoMembro = 1;
    var funcao = $("#funcao").val();
    //alert("funcao:"+funcao);
    var tipoCargo = retornaTipoCargo(parseInt(funcao));
    //alert("tipoCargo:"+tipoCargo);

    var dataEntrada = $("#dataEntrada").val();
    //alert("dtEntrada:"+dataEntrada);
    var dataSaida = '';//Não liberar para o pessoal do organismo esse campo
    var dataTerminoMandato = $("#dataTerminoMandato").val();
    //alert("dtTermino:"+dataTerminoMandato);

    if ($("#principalCompanheiro").val() == 2 || $("#companheiro").is(":checked"))
    {
        companheiro = 'S';
    } else {
        companheiro = '';
    }
    //alert("companheiro:"+companheiro);
    //Validação
    if (siglaOrganismoAfiliado == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo Afiliado", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if (codAfiliacaoMembroOficial == "")
    {
        swal({title: "Aviso!", text: "Preencha o código de afiliação do oficial", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if (funcao == 0)
    {
        swal({title: "Aviso!", text: "Selecione a função", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if (funcao == 652
            ||funcao == 653
            ||funcao == 615
            ||funcao == 616
            )
    {
        swal({title: "Aviso!", text: "Selecione outra função, essa função não pode ser selecionada!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if (dataEntrada == "")
    {
        swal({title: "Aviso!", text: "Preencha a data de entrada do oficial", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if (dataTerminoMandato == "")
    {
        swal({title: "Aviso!", text: "Preencha a data de término do mandato do oficial", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    $.ajax({
        type: "post",
        url: "js/ajax/atualizarOficial.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {siglaOrganismoAfiliado: siglaOrganismoAfiliado,
            codAfiliacaoMembroOficial: codAfiliacaoMembroOficial,
            tipoMembro: tipoMembro,
            tipoCargo: tipoCargo,
            funcao: funcao,
            dataEntrada: dataEntrada,
            dataSaida: dataSaida,
            dataTerminoMandato: dataTerminoMandato,
            companheiro: companheiro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            //alert("retorno:"+retorno);
            if (retorno.result[0].fields.fIdeInclusaoOk == "N")
            {
                location.href = "painelDeControle.php?corpo=buscaOficial&erroWebservice=" + retorno.result[0].fields.fDesMensag;
            } else {
                location.href = "painelDeControle.php?corpo=buscaOficial&salvo=1";
            }
            if(!retorno)
            {
                swal({title: "Aviso!", text: "Verifique se esse oficial atua nesse Organismo Afiliado! Se tiver dúvidas entre em contato com o Setor de Organismos Afiliados da GLP", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            }    
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function abrirPopUpAtaPosse(idAta)
{
    window.open('impressao/ataPosse.php?idAta=' + idAta, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

$(document).ready(function () {

    $("#dataPosse").mask("99/99/9999");
    $("#dataNascimento1").mask("99/99/9999");
    $("#dataNascimento2").mask("99/99/9999");
    $("#dataNascimento3").mask("99/99/9999");
    $("#dataNascimento4").mask("99/99/9999");
    $("#dataNascimento5").mask("99/99/9999");
    $("#dataNascimento6").mask("99/99/9999");
    $("#dataNascimento7").mask("99/99/9999");

    $("#inicioMandato1").mask("99/99/9999");
    $("#inicioMandato2").mask("99/99/9999");
    $("#inicioMandato3").mask("99/99/9999");
    $("#inicioMandato4").mask("99/99/9999");
    $("#inicioMandato5").mask("99/99/9999");
    $("#inicioMandato6").mask("99/99/9999");
    $("#inicioMandato7").mask("99/99/9999");

    $("#fimMandato1").mask("99/99/9999");
    $("#fimMandato2").mask("99/99/9999");
    $("#fimMandato3").mask("99/99/9999");
    $("#fimMandato4").mask("99/99/9999");
    $("#fimMandato5").mask("99/99/9999");
    $("#fimMandato6").mask("99/99/9999");
    $("#fimMandato7").mask("99/99/9999");

    $("#horaInicioPosse").mask("99:99");
    $("#cpf").mask("999.999.999-99");
    $("#cpf1").mask("999.999.999-99");
    $("#cpf2").mask("999.999.999-99");
    $("#cpf3").mask("999.999.999-99");
    $("#cpf4").mask("999.999.999-99");
    $("#cpf5").mask("999.999.999-99");
    $("#cpf6").mask("999.999.999-99");
    $("#cpf7").mask("999.999.999-99");

    $(".currency").maskMoney({symbol: 'R$ ',
        showSymbol: false, thousands: '.', decimal: ',', symbolStay: true});

    $(".data").mask("99/99/9999");

    $(".telefone").mask("(999) 9999-9999");

    $(".celular").mask("(999) 99999-9999");

    $('.clockpicker').clockpicker();
});

function mostraDetalhesAtaPosse(idAta)
{
    $.ajax({
        type: "post",
        url: "js/ajax/mostraDetalhesAtaPosse.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {idAta: idAta}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#dataPosse").html(data.dataPosse);
            $("#horaInicioPosse").html(data.horaInicioPosse);
            $("#enderecoPosse").html(data.enderecoPosse);
            $("#numeroPosse").html(data.numeroPosse);
            $("#bairroPosse").html(data.bairroPosse);
            $("#cidadePosse").html(data.cidadePosse);
            $("#mestreRetirante").html(data.mestreRetirante);
            $("#secretarioRetirante").html(data.secretarioRetirante);
            $("#presidenteJuntaDepositariaRetirante").html(data.presidenteJuntaDepositariaRetirante);
            $("#detalhesAdicionaisPosse").html(data.detalhesAdicionaisPosse);

            $("#data").html(data.data);
            $("#cadastrado_por").html(data.cadastrado_por);
            $("#atualizado_por").html(data.atualizado_por);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

/** Funções da área de Autorização de Entrada de Templo e Pronaos - SOA */
function abrirPopUpImprimiAutorizacaoEntradaTemploConvocacaoRitualistica(nome, codigoAfiliacao, idOrganismoAfiliado, numeroSolicitacoes)
{
    window.open('impressao/soa_autorizacao_entrada_templo.php?nome=' + nome + '&codigoAfiliacao=' + codigoAfiliacao + '&idOrganismoAfiliado=' + idOrganismoAfiliado + '&numeroSolicitacoes=' + numeroSolicitacoes, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpCarteirinhaMembro(codigoAfiliacao, companheiro)
{
    window.open('impressao/consulta_emissao_carteira_ritualistica.php?codigoAfiliacao=' + codigoAfiliacao + '&companheiro=' + companheiro, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpCarteirinhaMembroWeb(codigoAfiliacao, companheiro, nome, dataAdmissao)
{
    window.open('impressao/consulta_emissao_carteira_ritualistica_web.php?codigoAfiliacao=' + codigoAfiliacao + '&companheiro=' + companheiro + '&nome=' + nome + '&dataAdmissao=' + dataAdmissao, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpComprovanteQuitacao(codigoAfiliacao, companheiro, nome, dataAdmissao, dataQuitacaoRC, dataQuitacaoTOM, loteAtualRC, loteAtualTOM)
{
    window.open('impressao/consulta_comprovante_quitacao.php?codigoAfiliacao=' + codigoAfiliacao + '&companheiro=' + companheiro + '&nome=' + nome + '&dataAdmissao=' + dataAdmissao + '&dataQuitacaoRC=' + dataQuitacaoRC + '&dataQuitacaoTOM=' + dataQuitacaoTOM + '&loteAtualRC=' + loteAtualRC + '&loteAtualTOM=' + loteAtualTOM, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}


function abrirPopUpReciboRecebimento(id, idOrganismoAfiliado)
{
    window.open('impressao/reciboRecebimento.php?id=' + id + '&idOrganismoAfiliado=' + idOrganismoAfiliado, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpReciboDespesa(id, idOrganismoAfiliado)
{
    window.open('impressao/reciboDespesa.php?id=' + id + '&idOrganismoAfiliado=' + idOrganismoAfiliado, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpImprimirRelatorioGrandeConselheiro(idRelatorioGrandeConselheiro)
{
    window.open('impressao/relatorioGrandeConselheiro.php?idRelatorioGrandeConselheiro=' + idRelatorioGrandeConselheiro, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpTermoVoluntariado(id)
{
    window.open('impressao/termoVoluntariado.php?idTermoVoluntariado=' + id, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpAuxiliarWeb(id)
{
    window.open('impressao/auxiliarWeb.php?idAuxiliarWeb=' + id, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpRelatorioClasseArtesaos(id)
{
    window.open('impressao/relatorioClasseArtesaos.php?idRelatorioClasseArtesaos=' + id, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function validaCadastroMembroOA()
{
    if ($("#nomeMembroOa").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        $("#nomeMembroOa").focus();
        return false;
    }

    if ($("#codigoAfiliacao").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o código de afiliação", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codigoAfiliacao").focus();
        return false;
    }

    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }

    if ($("#seqCadastMembroOa").val() == "")
    {
        swal({title: "Aviso!", text: "O código interno não foi encontrado. Faça a busca novamente e tente salvar novamente", type: "warning", confirmButtonColor: "#1ab394"});
        $("#seqCadastMembroOa").focus();
        return false;
    }

    return true;
}

function abrirPopupRelatorioFinanceiroMensal(mesAtual, anoAtual, idOrganismoAfiliado, saldoMesAnterior, numeroMembrosAtivosAnterior)
{
    window.open('impressao/relatorioFinanceiroMensal.php?mesAtual=' + mesAtual + '&anoAtual=' + anoAtual + '&idOrganismoAfiliado=' + idOrganismoAfiliado + '&saldoMesesAnteriores=' + saldoMesAnterior + '&numeroMembrosAtivosAnterior=' + numeroMembrosAtivosAnterior, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopupRelatorioFinanceiroMensalPreview(mesAtual, anoAtual, idOrganismoAfiliado, saldoMesAnterior, numeroMembrosAtivosAnterior)
{
    window.open('impressao/relatorioFinanceiroMensalPreview.php?mesAtual=' + mesAtual + '&anoAtual=' + anoAtual + '&idOrganismoAfiliado=' + idOrganismoAfiliado + '&saldoMesesAnteriores=' + saldoMesAnterior + '&numeroMembrosAtivosAnterior=' + numeroMembrosAtivosAnterior, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopupRelatorioFinanceiroAnual(anoAtual, idOrganismoAfiliado, saldoAnosAnteriores)
{
    window.open('impressao/relatorioFinanceiroAnual.php?anoAtual=' + anoAtual + '&idOrganismoAfiliado=' + idOrganismoAfiliado + '&saldoAnosAnteriores=' + saldoAnosAnteriores, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopupExtratoFinanceiro(idOrganismoAfiliado, ano)
{
    if (validaExtratoFinanceiro())
    {
        window.open('impressao/extratoFinanceiro.php?ano=' + ano + '&idOrganismoAfiliado=' + idOrganismoAfiliado, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }
}
function validaExtratoFinanceiro()
{
    /*
    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal("Aviso", "Selecione o Organismo Afiliado!", "error");
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    */
    if ($("#ano").val() == "")
    {
        swal("Aviso", "Informe o ano!", "error");
        $("#ano").focus();
        return false;
    }
    return true;
}

function retornaDadosMembroNaAta(cod)
{
    var codigoAfiliacao = $("#codigoAfiliacao" + cod).val();
    var nomeMembro = $("#nome" + cod).val();
    var tipoMembro = '1';

    //console.log("retornaDados() --- Cód. Afiliação: "+codigoAfiliacao+" Nome: "+nomeMembro+" Tipo: "+tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);

    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            $("#nome" + cod).val(data.result[0].fields.fNomCliente.trim());
            $("#endereco" + cod).val(data.result[0].fields.fNomLogradouro.trim());
            $("#cidade" + cod).val(data.result[0].fields.fNomCidade.trim());
            $("#cep" + cod).val(data.result[0].fields.fCodCepEndereco.trim());
            $("#dataNascimento" + cod).val(
                    data.result[0].fields.fDatNascimento.substr(8, 2) + "/"
                    + data.result[0].fields.fDatNascimento.substr(5, 2) + "/"
                    + data.result[0].fields.fDatNascimento.substr(0, 4)
                    );
            $("#profissao" + cod).val(data.result[0].fields.fDesFormacao.trim());
            $("#rg" + cod).val(data.result[0].fields.fCodRg.trim());
            $("#cpf" + cod).val(data.result[0].fields.fCodCpf.trim());
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

function listaUploadAtaReuniaoMensal(id)
{
    $("#listaUpload").html();
    var tiraExcluir="N";
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadAtaReuniaoMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id,tiraExcluir:tiraExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function listaUploadAtaReuniaoMensalJaEntregue(id)
{
    $("#listaUploadJaEntregue").html("");
    var tiraExcluir="S";
    
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadAtaReuniaoMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id,tiraExcluir:tiraExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUploadJaEntregue").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadAtaReuniaoMensal(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadAtaReuniaoMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadAtaReuniaoMensal(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function listaUploadAtaPosse(id)
{
    $("#listaUpload").html("");
    var tirarExcluir="N";
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadAtaPosse.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id,tirarExcluir:tirarExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function listaUploadAtaPosseJaEntregue(id)
{
    $("#listaUploadJaEntregue").html("");
    var tirarExcluir="S";
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadAtaPosse.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id,tirarExcluir:tirarExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUploadJaEntregue").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadAtaPosse(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadAtaPosse.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadAtaPosse(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
    //}else{
    //	return false;
    //}
}

function listaUploadRelatorioFinanceiroMensal(mesAtual, anoAtual, idOrganismoAfiliado)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRelatorioFinanceiroMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {mesAtual: mesAtual, anoAtual: anoAtual, idOrganismoAfiliado: idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRelatorioFinanceiroMensal(idExcluir, mesAtual, anoAtual, idOrganismoAfiliado)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRelatorioFinanceiroMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRelatorioFinanceiroMensal(mesAtual, anoAtual, idOrganismoAfiliado);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
    //}else{
    //	return false;
    //}
}

function listaUploadRelatorioFinanceiroAnual(anoAtual, idOrganismoAfiliado)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRelatorioFinanceiroAnual.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {anoAtual: anoAtual, idOrganismoAfiliado: idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRelatorioFinanceiroAnual(idExcluir, anoAtual, idOrganismoAfiliado)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRelatorioFinanceiroAnual.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRelatorioFinanceiroAnual(anoAtual, idOrganismoAfiliado);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
    //}else{
    //	return false;
    //}
}

function salvarMensalidadeOA(seqCadast, semestre, anoAtual, idOrganismoAfiliado, usuario)
{
    //alert('chamou');
    //alert(seqCadast);
    //alert(semestre);
    //alert(anoAtual);
    //alert(idOrganismoAfiliado);
    //alert(usuario);

    var Campos = {};
    camposPreenchidos = new Array();
    if (semestre == 1)
    {
        for (i = 1; i <= 6; i++)
        {
            camposPreenchidos.push($("#" + seqCadast + "_" + i + "_" + anoAtual).val());
            //alert($("#"+seqCadast+"_"+i+"_"+anoAtual).val());
        }
    }
    if (semestre == 2)
    {
        for (i = 7; i <= 12; i++)
        {
            camposPreenchidos.push($("#" + seqCadast + "_" + i + "_" + anoAtual).val());
        }
    }

    Campos['datas'] = camposPreenchidos;

    var camposGeraisData = JSON.stringify(Campos);

    $.ajax({
        type: "post",
        url: "js/ajax/salvaMensalidadeOA.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
            camposGerais: camposGeraisData,
            seqCadast: seqCadast,
            semestre: semestre,
            anoAtual: anoAtual,
            idOrganismoAfiliado: idOrganismoAfiliado,
            usuario: usuario}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            if (data.status == 1)
            {
                swal({title: "Sucesso!", text: "Mensalidade(s) salva(s) com sucesso!", type: "success", confirmButtonColor: "#1ab394"});
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function lerCapituloLivroOrganismo(id, i)
{
    $.ajax({
        type: "post",
        url: "js/ajax/lerCapituloLivroOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#texto").html(data);
            $("#capituloLivro").html(i);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function listaUploadLivroOrganismo(id, i)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadLivroOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
            $("#capituloArquivo").html(i);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadLivroOrganismo(idExcluir, id)
{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadLivroOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadLivroOrganismo(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
}

function lerCapituloLivroRegiao(id, i)
{
    $.ajax({
        type: "post",
        url: "js/ajax/lerCapituloLivroRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#texto").html(data);
            $("#capituloLivro").html(i);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function listaUploadLivroRegiao(id, i)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadLivroRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
            $("#capituloArquivo").html(i);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadLivroRegiao(idExcluir, id)
{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadLivroRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadLivroOrganismo(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });

}

/**
 * Notificações Ata
 */
function getValueNotificacoesGLP(titulo, oficiaisMarcados, tipoNotificacao, remetente, mensagem) {

    var Values = {};

    Values['oficial'] = oficiaisMarcados;

    Values['tipoNotificacaoGlp'] = tipoNotificacao;

    Values['tituloNotificacaoGlp'] = titulo;

    Values['fk_seqCadastRemetente'] = remetente;

    Values['mensagemNotificacaoGlp'] = mensagem;

    //print_r(Values);
    return Values;
}

function enviaNotificacaoGLP(titulo, oficiaisMarcados, tipoNotificacao, remetente, mensagem)
{
    var values = getValueNotificacoesGLP(titulo, oficiaisMarcados, tipoNotificacao, remetente, mensagem);
    var camposGeraisData = JSON.stringify(values);

    $.ajax({
        type: "post",
        url: "../js/ajax/cadastraNotificacaoGlp.php",
        dataType: "json",
        data: {camposGerais: camposGeraisData},
        success: function (retorno) {
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                swal({title: "Aviso!", text: retorno.erro, type: "warning", confirmButtonColor: "#1ab394"});
                //alert(retorno.erro);
                $('#' + retorno.focus).focus();
            }
            else if (retorno.sucesso != undefined) {
                //alert(retorno.success);
                //$('#mySeeNotificacao'+idModal).modal('hidde');
                //setTimeout(fechaModal(idModal),50);
            }
            else {
                alert("Ocorreu algum erro. Entre em contato com o setor de TI.");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("erro");
        }
    });
}

/**
 * Método responsável pelo início do processo que
 * cadastra saldo inicial e projeção financeira
 */
function salvarSaldoInicial()
{
    $("#loadSI").html("<img src='img/loading_trans.gif' width='70'>");

    var dataSaldoInicial = $("#dataSaldoInicial").val();
    var saldoInicial = $("#saldoInicial").val();
    var usuario = $("#usuario").val();
    var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();

    // no momento funcionalidade não permite edição, logo default salvar = 0 e idSaldoInicial = null
    var salvar = 0;
    var idSaldoInicial = null;

    if (dataSaldoInicial == "")
    {
        swal({title: "Aviso!", text: "Preencha a data", type: "warning", confirmButtonColor: "#1ab394"});
        $("#loadSI").html("");
        return false;
    }
    if (saldoInicial == "")
    {
        swal({title: "Aviso!", text: "Preencha o saldo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#loadSI").html("");
        return false;
    }

    $.ajax({
        type: "post",
        url: "js/ajax/salvaSaldoInicial.php",
        dataType: "json",
        data: {
            dataSaldoInicial: dataSaldoInicial,
            saldoInicial: saldoInicial,
            usuario: usuario,
            fk_idOrganismoAfiliado: fk_idOrganismoAfiliado,
            salvar: salvar,
            idSaldoInicial: idSaldoInicial
        },
        success: function (retorno) {
            if (retorno)
            {
                swal({
                    title: "Sucesso!",
                    text: "Saldo Inicial salvo com sucesso!",
                    type: "success",
                    confirmButtonColor: "#1ab394"
                });
                document.getElementById('saldoInicial').readOnly = true;
                $("#loadSI").html("");

                var mes = dataSaldoInicial.substr(3, 2);
                var ano = dataSaldoInicial.substr(6, 4);

                // atualizaSaldoOrganismo(
                //     mes,
                //     ano,
                //     $("#fk_idOrganismoAfiliado").val(),
                //     null,
                //     null,
                //     null,
                //     null,
                //     usuario,
                //     null,
                //     'saldoInicial',
                //     null
                // );
            }
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function visualizar5w2h(idMeta)
{
    $("#v_tituloMeta").html("");
    $("#v_oque").html("");
    $("#v_porque").html("");
    $("#v_quem").html("");
    $("#v_onde").html("");
    $("#v_quando").html("");
    $("#v_como").html("");
    $("#v_quanto").html("");

    $.ajax({
        type: "post",
        url: "js/ajax/visualizar5w2h.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: idMeta}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);

            $("#v_tituloMeta").html(data.tituloMeta);
            $("#v_oque").html(data.oque);
            $("#v_porque").html(data.porque);
            $("#v_quem").html(data.quem);
            $("#v_onde").html(data.onde);
            $("#v_quando").html(data.quando);
            $("#v_como").html(data.como);
            $("#v_quanto").html(data.quanto);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

function mudaStatusMeta(id, status)
{
    $.ajax({
        type: "post",
        url: "js/ajax/alteraStatusMeta.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id, status: status}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if (data.status == 1)
            {
                $("#statusMeta" + id).html("<a href=\"#\" onclick=\"mudaStatusMeta('" + id + "','1')\"><span class=\"label label-primary\"><i class=\"fa fa-check\"></i> Completa</span></a>");

            } else {
                $("#statusMeta" + id).html("<a href=\"#\" onclick=\"mudaStatusMeta('" + id + "','0')\"><span class=\"label label-default\"><i class=\"fa fa-thumbs-o-down\"></i> Incompleta</span></a>");
            }
            atualizaPercentualPlanoAcaoRegiao();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function atualizaPercentualPlanoAcaoRegiao()
{
    var idPlanoAcaoRegiao = $("#idPlanoAcaoRegiaoModalMeta").val();
    $.ajax({
        type: "post",
        url: "js/ajax/atualizaPercentualPlanoAcaoRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {idPlanoAcaoRegiao: idPlanoAcaoRegiao}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#percentual").width(data.percentual + "%");
            $("#textoPercentual").html(data.percentual);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function mudaStatusPlanoAcaoRegiao(id, status)
{
    $.ajax({
        type: "post",
        url: "js/ajax/alteraStatusPlanoAcaoRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id, status: status}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if (data.status == 1)
            {
                $("#statusPlano" + id).html("<a href=\"#\" onclick=\"mudaStatusPlanoAcaoRegiao('" + id + "','1')\"><span class=\"label label-primary\">Ativo</span></a>");

            } else {
                $("#statusPlano" + id).html("<a href=\"#\" onclick=\"mudaStatusPlanoAcaoRegiao('" + id + "','0')\"><span class=\"label label-default\">Inativo</span></a>");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirArquivo(link)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            location.href = link;
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
    //}else{
    //	return false;
    //}
}

function cadastrarMetaModal()
{
    if ($("#inicio").val() == "00/00/0000")
    {
        swal({title: "Aviso!", text: "Preencha corretamente o campo data!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if ($("#fim").val() == "00/00/0000")
    {
        swal({title: "Aviso!", text: "Preencha corretamente o campo data!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    //alert('teste do braz aqui na validation da meta');
    $("#meta").submit();
}

function mudaStatusMetaOrganismo(id, status)
{
    $.ajax({
        type: "post",
        url: "js/ajax/alteraStatusMetaOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id, status: status}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {

            if (data.status == 1)
            {
                $("#statusMeta" + id).html("<a href=\"#\" onclick=\"mudaStatusMetaOrganismo('" + id + "','1')\"><span class=\"label label-primary\"><i class=\"fa fa-check\"></i> Completa</span></a>");

            } else {
                $("#statusMeta" + id).html("<a href=\"#\" onclick=\"mudaStatusMetaOrganismo('" + id + "','0')\"><span class=\"label label-default\"><i class=\"fa fa-thumbs-o-down\"></i> Incompleta</span></a>");
            }
            atualizaPercentualPlanoAcaoOrganismo();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function atualizaPercentualPlanoAcaoOrganismo()
{

    var idPlanoAcaoOrganismo = $("#idPlanoAcaoOrganismoModalMeta").val();


    $.ajax({
        type: "post",
        url: "js/ajax/atualizaPercentualPlanoAcaoOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {idPlanoAcaoOrganismo: idPlanoAcaoOrganismo}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#percentual").width(data.percentual + "%");
            $("#textoPercentual").html(data.percentual);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

function mudaStatusPlanoAcaoOrganismo(id, status)
{
    $.ajax({
        type: "post",
        url: "js/ajax/alteraStatusPlanoAcaoOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id, status: status}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if (data.status == 1)
            {
                $("#statusPlano" + id).html("<a href=\"#\" onclick=\"mudaStatusPlanoAcaoOrganismo('" + id + "','1')\"><span class=\"label label-primary\">Ativo</span></a>");

            } else {
                $("#statusPlano" + id).html("<a href=\"#\" onclick=\"mudaStatusPlanoAcaoOrganismo('" + id + "','0')\"><span class=\"label label-default\">Inativo</span></a>");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function visualizar5w2hOrganismo(idMeta)
{
    $("#v_tituloMeta").html("");
    $("#v_oque").html("");
    $("#v_porque").html("");
    $("#v_quem").html("");
    $("#v_onde").html("");
    $("#v_quando").html("");
    $("#v_como").html("");
    $("#v_quanto").html("");

    $.ajax({
        type: "post",
        url: "js/ajax/visualizar5w2hOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: idMeta}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);

            $("#v_tituloMeta").html(data.tituloMeta);
            $("#v_oque").html(data.oque);
            $("#v_porque").html(data.porque);
            $("#v_quem").html(data.quem);
            $("#v_onde").html(data.onde);
            $("#v_quando").html(data.quando);
            $("#v_como").html(data.como);
            $("#v_quanto").html(data.quanto);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

function excluirParticipantePlanoAcaoRegiao(idPlano)
{
    var arr = $("#ata_oa_mensal_oficiais").val();

    if (arr == null)
    {
        swal({title: "Aviso!", text: "Escolha um participante na lista para ser excluído", type: "error", confirmButtonColor: "#1ab394"});
    } else {
        swal({
            title: "Tem certeza que deseja excluir?",
            text: "Com essa ação você excluirá definitivamente esse participante do plano!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, exclua!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: false},
        function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: "post",
                    url: "js/ajax/excluirParticipantePlanoAcaoRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {arr: arr, idPlanoAcaoRegiao: idPlano}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                    success: function (data) {
                        //alert(data);
                        if (data.status == 1)
                        {
                            location.href = '?corpo=alteraPlanoAcaoRegiao&idPlanoAcaoRegiao=' + idPlano + '&excluido=1';
                        } else {
                            if (data.status == 0)
                            {
                                $('#ata_oa_mensal_oficiais option:selected').remove();
                                swal({title: "Sucesso!", text: "Participante(s) excluído(s) com sucesso da lista!", type: "success", confirmButtonColor: "#1ab394"});
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro ao excluir o participante.');
                    }
                });
            } else {
                swal({title: "Cancelado", text: "Nós não excluímos o participante :)", type: "error", confirmButtonColor: "#1ab394"});
            }
        });

    }
}

function excluirParticipantePlanoAcaoOrganismo(idPlano)
{
    var arr = $("#ata_oa_mensal_oficiais").val();

    if (arr == null)
    {
        swal({title: "Aviso!", text: "Escolha um participante na lista para ser excluído", type: "error", confirmButtonColor: "#1ab394"});
    } else {
        swal({
            title: "Tem certeza que deseja excluir?",
            text: "Com essa ação você excluirá definitivamente esse participante do plano!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, exclua!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: false},
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: "js/ajax/excluirParticipantePlanoAcaoOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {arr: arr, idPlanoAcaoOrganismo: idPlano}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                    success: function (data) {
                        //alert(data);
                        if (data.status == 1)
                        {
                            location.href = '?corpo=alteraPlanoAcaoOrganismo&idPlanoAcaoOrganismo=' + idPlano + '&excluido=1';
                        } else {
                            if (data.status == 0)
                            {
                                $('#ata_oa_mensal_oficiais option:selected').remove();
                                swal({title: "Sucesso!", text: "Participante(s) excluído(s) com sucesso da lista!", type: "success", confirmButtonColor: "#1ab394"});
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro ao excluir o participante.');
                    }
                });
            } else {
                swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
            }
        });
    }
}

function imprimirPlanoAcaoRegiao(idPlanoAcaoRegiao, idOrganismoAfiliado)
{
    window.open('impressao/planoAcaoRegiao.php?idPlanoAcaoRegiao=' + idPlanoAcaoRegiao + '&idOrganismoAfiliado=' + idOrganismoAfiliado, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function imprimirPlanoAcaoOrganismo(idPlanoAcaoOrganismo, idOrganismoAfiliado)
{
    window.open('impressao/planoAcaoOrganismo.php?idPlanoAcaoOrganismo=' + idPlanoAcaoOrganismo + '&idOrganismoAfiliado=' + idOrganismoAfiliado, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function excluirPlanoAcaoRegiao(idPlanoAcaoRegiao)
{
    swal({
        title: "Tem certeza que deseja excluir?",
        text: "Com essa ação você excluirá definitivamente esse plano de ação!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirPlanoAcaoRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idPlanoAcaoRegiao}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    location.href = '?corpo=buscaPlanoAcaoRegiao';
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o plano :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
}

function excluirPlanoAcaoOrganismo(idPlanoAcaoOrganismo)
{
    swal({
        title: "Tem certeza que deseja excluir?",
        text: "Com essa ação você excluirá definitivamente esse plano de ação!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirPlanoAcaoOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idPlanoAcaoOrganismo}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    location.href = '?corpo=buscaPlanoAcaoOrganismo';
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o plano :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
}

function filtrarListaChat(nomeUsuario)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUsuariosChat.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {nomeUsuario: nomeUsuario}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUsuarios").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function filtrarListaChatUltimasConversas(nomeUsuario)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUsuariosChat.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {nomeUsuario: nomeUsuario}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUsuariosUltimasConversas").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function visualizarAtualizacaoPlanoAcaoRegiao(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getAtualizacaoPlanoAcaoRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#mensagem").val(data.mensagem);
            $("#idPlanoAcaoRegiaoAtualizacao").val(id);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function visualizarAtualizacaoPlanoAcaoOrganismo(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getAtualizacaoPlanoAcaoOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#mensagem").val(data.mensagem);
            $("#idPlanoAcaoOrganismoAtualizacao").val(id);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function cadastraAtualizacao()
{
    $('#mensagem').val("");
}

function cadastrarMeta()
{
    $('#tituloMeta').val("");
    $('#inicio').val("");
    $('#fim').val("");
    $('#oque').val("");
    $('#porque').val("");
    $('#quem').val("");
    $('#onde').val("");
    $('#quando').val("");
    $('#como').val("");
    $('#quanto').val("");
}

function visualizarMetaPlanoAcaoRegiao(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getMetaPlanoAcaoRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {

            $("#tituloMeta").val(data.tituloMeta);
            $("#inicio").val(data.inicio);
            $("#fim").val(data.fim);
            $("#oque").val(data.oque);
            $("#porque").val(data.porque);
            $("#quem").val(data.quem);
            $("#onde").val(data.onde);
            $("#quando").val(data.quando);
            $("#como").val(data.como);
            $("#quanto").val(data.quanto);
            $("#ordenacao").val(data.ordenacao);
            $("#idPlanoAcaoRegiaoMeta").val(id);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function visualizarMetaPlanoAcaoOrganismo(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getMetaPlanoAcaoOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {

            $("#tituloMeta").val(data.tituloMeta);
            $("#inicio").val(data.inicio);
            $("#fim").val(data.fim);
            $("#oque").val(data.oque);
            $("#porque").val(data.porque);
            $("#quem").val(data.quem);
            $("#onde").val(data.onde);
            $("#quando").val(data.quando);
            $("#como").val(data.como);
            $("#quanto").val(data.quanto);
            $("#ordenacao").val(data.ordenacao);
            $("#idPlanoAcaoOrganismoMeta").val(id);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function validaTransferenciaMembro()
{
    if ($("#fk_idOrganismoAfiliadoDestino").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo de Destino", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliadoDestino").focus();
        return false;
    }

    if ($("#motivoTransferencia").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o motivo da transferência", type: "warning", confirmButtonColor: "#1ab394"});
        $("#dataAtaReuniaoMensal").focus();
        return false;
    }

    return true;
}

function imprimirDeclaracaoTransferencia(idTransferenciaMembro)
{
    window.open('impressao/declaracaoTransferencia.php?idTransferenciaMembro=' + idTransferenciaMembro, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function mostraDetalhesOrganismo(id, nome, sigla, idpais, paisExtenso)
{
    var siglaOA = sigla;
    var paisOrganismo = idpais;
    var pais = "BR";
    if (paisOrganismo == 2)
    {
        pais = "PT";
    }
    if (paisOrganismo == 3)
    {
        pais = "AO";
    }
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {siglaOA: siglaOA, pais: pais}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#nomeOrganismoAfiliado").html(nome);
            $("#siglaOrganismoAfiliado").html(sigla);
            $("#regiao").html(sigla.substr(0, 3));
            $("#pais").html(paisExtenso);
            $("#codigoAfiliacao").html(data.result[0].fields.fArrayOA[0].fields.fCodRosacr);
            $("#bairro").html(data.result[0].fields.fArrayOA[0].fields.fNomBairro.trim());
            $("#endereco").html(data.result[0].fields.fArrayOA[0].fields.fDesLograd.trim());
            $("#numero").html(data.result[0].fields.fArrayOA[0].fields.fNumEndere);
            $("#complemento").html(data.result[0].fields.fArrayOA[0].fields.fDesCompleLograd);
            $("#cep").html(data.result[0].fields.fArrayOA[0].fields.fCodCep.trim());
            $("#email").html(data.result[0].fields.fArrayOA[0].fields.fDesEmail.trim());
            $("#estado").html(data.result[0].fields.fArrayOA[0].fields.fSigUf);
            $("#cidade").html(data.result[0].fields.fArrayOA[0].fields.fNomLocali);
            //alert(data.result[0].fields.fArrayOA[0].fields.fSituacaoOa);
            if (data.result[0].fields.fArrayOA[0].fields.fSituacaoOa == "R")
            {
                $("#situacao").html("Recesso");
            }
            if (data.result[0].fields.fArrayOA[0].fields.fSituacaoOa == "F")
            {
                $("#situacao").html("Fechou");
            }
            if (data.result[0].fields.fArrayOA[0].fields.fSituacaoOa == "A")
            {
                $("#situacao").html("Aberto");
            }
            $("#cnpj").html(data.result[0].fields.fArrayOA[0].fields.fNumCnpj.trim());
            $("#celular").html(data.result[0].fields.fArrayOA[0].fields.fNumCelula.trim());
            $("#telefoneFixo").html(data.result[0].fields.fArrayOA[0].fields.fNumTelefoFixo.trim());
            $("#outroTelefone").html(data.result[0].fields.fArrayOA[0].fields.fNumOutroTelefo.trim());
            $("#fax").html(data.result[0].fields.fArrayOA[0].fields.fNumFax.trim());
            $("#caixaPostal").html(data.result[0].fields.fArrayOA[0].fields.fNumCaixaPostal.trim());
            $("#cepCaixaPostal").html(data.result[0].fields.fArrayOA[0].fields.fCodCepCaixaPostal.trim());
            if (data.result[0].fields.fArrayOA[0].fields.fIdeEndereCorres == "1")
            {
                $("#enderecoCorrespondencia").html("Logradouro");
            } else {
                $("#enderecoCorrespondencia").html("Caixa Postal");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}



function checkAll() {

    var ipts = $("#checkboxes").find("input");
    // CheckBox que ao ser clicado marca ou desmarca todos elementos
    var check = document.getElementById("checkboxCheckAll");

    // Testamos o CheckBox para ver se devemos marcar ou desmarcar
    check.checked ?
            jQuery.each(ipts, function () {
                // Se esta "checado" então marcamos todos elementos como checked=true
                this.checked = true;
            }) :
            jQuery.each(ipts, function () {
                // Se não esta "checado" então marcamos todos elementos como checked=false
                this.checked = false;
            });
}

function checkAllOA(sigla, check) {
    //alert(sigla);
    var ipts = $("#checkboxes" + sigla).find("input");
    // CheckBox que ao ser clicado marca ou desmarca todos elementos
    //var check = document.getElementById("checkboxCheckAll");

    // Testamos o CheckBox para ver se devemos marcar ou desmarcar
    check.checked ?
            jQuery.each(ipts, function () {
                // Se esta "checado" então marcamos todos elementos como checked=true
                this.checked = true;
            }) :
            jQuery.each(ipts, function () {
                // Se não esta "checado" então marcamos todos elementos como checked=false
                this.checked = false;
            });
}

function enviarEmail()
{
    $('#mensagemEmail').val($('#mensagem.summernote').code());
    document.email.submit();
}

function anexar()
{
    $('#mensagemAnexo').val($('#mensagem.summernote').code());
    $('#assuntoAnexo').val($('#assunto').val());
    document.arquivo.submit();
}

function marcarMensagemComoLida(seqCadast)
{
    var Campos = {};

    camposMarcados = new Array();
    $("input[type=checkbox][name='emails[]']:checked").each(function () {
        camposMarcados.push($(this).val());
        //alert(camposMarcados);
    });
    Campos['emails'] = camposMarcados;

    $.ajax({
        type: "post",
        url: "js/ajax/marcarMensagemComoLida.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {Campos: Campos, seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {

            if (retorno.status == 1)
            {
                location.href = "?corpo=buscaEmail";
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao marcar mensagem como lida');
        }
    });
}

function marcarMensagemComoImportante(seqCadast)
{
    var Campos = {};

    camposMarcados = new Array();
    $("input[type=checkbox][name='emails[]']:checked").each(function () {
        camposMarcados.push($(this).val());
        //alert(camposMarcados);
    });
    Campos['emails'] = camposMarcados;

    $.ajax({
        type: "post",
        url: "js/ajax/marcarMensagemComoImportante.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {Campos: Campos, seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {

            if (retorno.status == 1)
            {
                location.href = "?corpo=buscaEmail";
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao marcar mensagem como importante');
        }
    });
}

function moverMensagemParaLixeira(seqCadast, pastaInterna)
{
    var Campos = {};

    camposMarcados = new Array();
    $("input[type=checkbox][name='emails[]']:checked").each(function () {
        camposMarcados.push($(this).val());
        //alert(camposMarcados);
    });
    Campos['emails'] = camposMarcados;

    $.ajax({
        type: "post",
        url: "js/ajax/moverMensagemParaLixeira.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {Campos: Campos, seqCadast: seqCadast, pastaInterna: pastaInterna}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {

            if (retorno.status == 1)
            {
                location.href = "?corpo=buscaEmail&pastaInterna=" + pastaInterna;
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            swal({title: "Aviso!", text: "Não foi possível mover mensagem para a lixeira. Refaça o processo selecionando a mensagem.", type: "warning", confirmButtonColor: "#1ab394"});
        }
    });
}

function excluirEmailDefinitivamente(seqCadast, pastaInterna)
{
    var Campos = {};

    camposMarcados = new Array();
    $("input[type=checkbox][name='emails[]']:checked").each(function () {
        camposMarcados.push($(this).val());
        //alert(camposMarcados);
    });
    Campos['emails'] = camposMarcados;

    $.ajax({
        type: "post",
        url: "js/ajax/excluirEmailDefinitivamente.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {Campos: Campos, seqCadast: seqCadast, pastaInterna: pastaInterna}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {

            if (retorno.status == 1)
            {
                location.href = "?corpo=buscaEmail&pastaInterna=" + pastaInterna;
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            swal({title: "Aviso!", text: "Não foi possível mover mensagem para a lixeira. Refaça o processo selecionando a mensagem.", type: "warning", confirmButtonColor: "#1ab394"});
        }
    });
}

function moverMensagemParaLixeiraEmailUnico(seqCadast, idEmail)
{
    $.ajax({
        type: "post",
        url: "js/ajax/moverMensagemParaLixeiraEmailUnico.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {idEmail: idEmail, seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            if (retorno.status == 1)
            {
                location.href = "?corpo=buscaEmail";
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao mover email unico para lixeira');
        }
    });
}

function imprimirEmail(idEmail, vinculoEmailEnviado)
{
    window.open('impressao/email.php?idEmail=' + idEmail + '&vinculoEmailEnviado=' + vinculoEmailEnviado, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function pesquisarEmail()
{
    var search = $("#search").val();
    location.href = '?corpo=buscaEmail&search=' + search;
}

function descartarEmail()
{
    location.href = '?corpo=buscaEmail';
}

function alteraPara()
{
    var options = new Array();
    $('#paraView > option:selected').each(
            function (i) {
                options[i] = $(this).val();
            });


    $("#para").val("");//Limpar campo que realmente será enviado
    $("#paraAnexo").val("");//Limpar campo que guarda os destinatários
    var parte = "";
    var total = options.length;
    for (i = 0; i < total; i++)
    {
        if (i == 0)
        {
            parte = parte + options[i];
        } else {
            parte = parte + "," + options[i];
        }
    }
    $("#para").val(parte);
    $("#paraAnexo").val(parte);
}

function listaUploadEmailAnexo(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadEmailAnexo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadEmailAnexo(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadEmailAnexo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadEmailAnexo(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function pesquisarEmailAoDigitar(e)
{
    if (e.keyCode !== 13)
        return;
    // Pesquisar
    var s = $("#search").val();
    location.href = "?corpo=buscaEmail&search=" + s;
}

function situacaoCadastral(cod)
{
    switch (cod)
    {
        case 1:
            return "Ativo";
            break;
        case 2:
            return "Inativo";
            break;
        case 3:
            return "Estudos paralisados";
            break;
        case 7:
            return "Estudos paralisados";
            break;
        case 9:
            return "Vitalício";
            break;
        default:
            return "Não possui";
            break;
    }
}

function situacaoRemessa(cod)
{
    switch (cod)
    {
        case 1:
            return "Normal";
            break;
        case 2:
            return "Estudos paralisados";
            break;
        case 3:
            return "Estudos paralisados";
            break;
        case 6:
            return "Estudos paralisados";
            break;
        case 7:
            return "Estudos paralisados";
            break;
        default:
            return "Não possui";
            break;
    }
}

function numeroGrauRC(cod)
{
    if (cod > 0 && cod <= 5)
    {
        return 0;
    }
    if (cod == 6)
    {
        return 1;
    }
    if (cod == 7)
    {
        return 2;
    }
    if (cod == 8)
    {
        return 3;
    }
    if (cod == 9)
    {
        return 4;
    }
    if (cod == 10)
    {
        return 5;
    }
    if (cod >= 11 && cod <= 12)
    {
        return 6;
    }
    if (cod >= 13 && cod <= 15)
    {
        return 7;
    }
    if (cod >= 16 && cod <= 18)
    {
        return 8;
    }
    if (cod >= 19 && cod <= 22)
    {
        return 9;
    }
    if (cod >= 23 && cod <=31)
    {
        return 10;
    }
    if (cod >= 32 && cod <= 44)
    {
        return 11;
    }
    if (cod >= 45)
    {
        return 12;
    }
    
    return 0;
}

function retornaQntMesesMembroRC(fDatAdmissRosacr){
    membroDesde = fDatAdmissRosacr;
    anoEntrada = membroDesde.substr(0, 4);
    now = new Date;
    anoAtual = now.getFullYear();
    meses = ((anoAtual - anoEntrada) - 1) * 12;
    meses = meses < 0 ? 0 : meses;
    mesEntrada = membroDesde.substr(5, 2);
    mesAtual = now.getMonth();
    if(anoEntrada != anoAtual) {
            meses = meses + (mesAtual + (12 - (mesEntrada - 1)));
    } else {
            meses = meses + mesAtual;
    }
    return meses;
}

function grauRCDisponivel(loteAtual, mesesAdmissaoRC)
{
    //alert(loteAtual);alert(mesesAdmissaoRC);
    
    if ((loteAtual >= 45) && (mesesAdmissaoRC >= 135))
    {
        return "12º Grau";
    }
    else if ((loteAtual >= 32) && (mesesAdmissaoRC >= 96))
    {
        return "11º Grau";
    }
    else if ((loteAtual >= 23) && (mesesAdmissaoRC >= 69))
    {
        return "10º Grau";
    }
    else if ((loteAtual >= 19) && (mesesAdmissaoRC >= 57))
    {
        return "9º Grau";
    }
    else if ((loteAtual >= 16) && (mesesAdmissaoRC >= 48))
    {
        return "8º Grau";
    }
    else if ((loteAtual >= 13) && (mesesAdmissaoRC >= 39))
    {
        return "7º Grau";
    }
    else if ((loteAtual >= 11) && (mesesAdmissaoRC >= 33)) 
    {
        return "6º Grau";
    }
    else if ((loteAtual >= 10) && (mesesAdmissaoRC >= 30)) 
    {
        return "5º Grau";
    }
    else if ((loteAtual >= 9) && (mesesAdmissaoRC >= 27))
    {
        return "4º Grau";
    }
    else if ((loteAtual >= 8) && (mesesAdmissaoRC >= 24))
    {
        return "3º Grau";
    }
    else if ((loteAtual >= 7) && (mesesAdmissaoRC >= 21))
    {
        return "2º Grau";
    }
    else if ((loteAtual >= 6) && (mesesAdmissaoRC >= 18))
    {
        return "1º Grau";
    }else if (loteAtual <= 5)
    {
        return "Neófito";
    }
    return "Não possui";
}

function grauTOM(cod)
{
    if (cod > 0 && cod <= 5)
    {
        return "Neófito";
    }
    if (cod > 5 && cod <= 7)
    {
        return "1º Grau";
    }
    if (cod == 8)
    {
        return "2º Grau";
    }
    if (cod == 9)
    {
        return "3º Grau";
    }
    if (cod == 10)
    {
        return "4º Grau";
    }
    if (cod >= 11 && cod <= 12)
    {
        return "5º Grau";
    }
    if (cod >= 13 && cod <= 15)
    {
        return "6º Grau";
    }
    if (cod >= 16 && cod <= 18)
    {
        return "7º Grau";
    }
    if (cod >= 19 && cod <= 22)
    {
        return "8º Grau";
    }
    if (cod == 23)
    {
        return "9º Grau";
    }
    if (cod >= 24 && cod <= 32)
    {
        return "10º Grau";
    }
    if (cod >= 33 && cod <= 45)
    {
        return "11º Grau";
    }
    if (cod >= 46)
    {
        return "12º Grau";
    }
    return "Não possui";
}

function grauOGG(cod)
{
    if (cod > 0 && cod <= 8)
    {
        return "Guardião do Castelo";
    }
    if (cod > 8 && cod <= 20)
    {
        return "Escudeiro da Verdade";
    }
    if (cod >= 21 && cod <= 32)
    {
        return "Fiel Servidor";
    }
    if (cod >= 33 && cod <= 40)
    {
        return "Cavaleiro da Perseverança";
    }
    if (cod >= 41 && cod <= 48)
    {
        return "Guias do Graal";
    }
    if (cod == 49)
    {
        return "Encerramento";
    }
    return "Não possui";
}

function consultaMembro()
{


    var tipoMembro = $("#tipo").val();
    var nomeMembro = $("#nome").val();
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fNumLoteAtualRosacr);
            if (data.result[0].fields.fNomCliente.trim() != "")
            {
                //Preencher Iniciações
                var companheiro = "N";
                var dataAdmissao = data.result[0].fields.fDatAdmissRosacr;
                if (data.result[0].fields.fSeqCadastCompanheiroRosacruz == data.result[0].fields.fSeqCadast)
                {
                    companheiro = "S";
                    dataAdmissao = data.result[0].fields.fDatAdmissCompanRosacr;

                }

                var fSeqCadast              = data.result[0].fields.fSeqCadast;
                var fNumLoteAtualRosacr     = data.result[0].fields.fNumLoteAtualRosacr;

                if(fSeqCadast == data.result[0].fields.fSeqCadastPrincipalRosacruz)
                {
                    fDatAdmiss = data.result[0].fields.fDatAdmissRosacr;
                } 
                else {
                    fDatAdmiss = data.result[0].fields.fDatAdmissCompanRosacr;
                }

                var mesesAdmissaoRC         = retornaQntMesesMembroRC(fDatAdmiss);

                //console.log("retornaQntMesesMembroRC: " + mesesAdmissaoRC);

                var grau                    = numeroGrauRC(fNumLoteAtualRosacr, mesesAdmissaoRC);


                consultaIniciacoesMembro(codigoAfiliacao, companheiro, fSeqCadast, grau, fNumLoteAtualRosacr,dataAdmissao);

                consultaIniciacoesMembroTOM(codigoAfiliacao, companheiro, data.result[0].fields.fSeqCadast, grau, fNumLoteAtualRosacr);
                consultaIniciacoesMembroOGG(codigoAfiliacao, companheiro, data.result[0].fields.fSeqCadast, grau, fNumLoteAtualRosacr);
                preencheProfissao(data.result[0].fields.fSeqFormacao);
                preencheOcupacao(data.result[0].fields.fSeqOcupacao);
                //Preencher outros campos
                $("#m_nome").html(data.result[0].fields.fNomCliente.trim());
                $("#m_codigoAfiliacao").html(codigoAfiliacao);
                //$("#logradouro").html(data.result[0].fields.fNomLogradouro.trim());
                //$("#numero").html(data.result[0].fields.fNumEndereco);
                $("#cidade").html(data.result[0].fields.fNomCidade.trim());
                //$("#cep").html(data.result[0].fields.fCodCepEndereco.trim());
                if(tipoMembro==1)//Se for R+C ou TOM tirar a data de nascimento
                {    
                    $("#dataNascimento").html(
                        data.result[0].fields.fDatNascimento.substr(8, 2) + "/"
                        + data.result[0].fields.fDatNascimento.substr(5, 2)

                        );
                }
                if(tipoMembro==2)//Se for OGG mostrar data inteira
                {    
                    $("#dataNascimento").html(
                        data.result[0].fields.fDatNascimento.substr(8, 2) + "/"
                        + data.result[0].fields.fDatNascimento.substr(5, 2) + "/"
                        + data.result[0].fields.fDatNascimento.substr(0, 4)
                        );   
                } 
                if(data.result[0].fields.fIdeTipoTom=="S")
                {
                    $("#tom").html("SIM");
                }else{
                    $("#tom").html("NÃO");
                } 
                $("#tipoAfiliacaoTom").html("");
                $("#dualTom").html("");
                if(data.result[0].fields.fIdeTipoTom=="S")//Se for TOM
                {   
                    if(data.result[0].fields.fIdeTipoFiliacTom=="D"||data.result[0].fields.fIdeTipoFiliacTom=="C")
                    {    
                        $("#tipoAfiliacaoTom").html("- Dual");
                        if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalTom)
                        {    
                            $("#dualTom").html("- Principal");
                        }
                        if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroTom)
                        {    
                            $("#dualTom").html("- Companheiro");
                        }
                    }else{
                        $("#tipoAfiliacaoTom").html("- Individual");
                    }
                }
                //$("#profissao").html(data.result[0].fields.fDesFormacao);
                //$("#ocupacao").html(data.result[0].fields.fDesOcupacao);
                //$("#rg").html(data.result[0].fields.fCodRg.trim());			
                //$("#cpf").html(data.result[0].fields.fCodCpf.trim());
                
                //$("#telefone").html(data.result[0].fields.fNumTelefone.trim());
                //$("#email").html(data.result[0].fields.fDesEmail.trim());
                //alert(data.result[0].fields.fIdeTipoFiliacRosacruz);
                
                $("#tipoAfiliacao").html("NÃO");
                $("#dual").html("");
                
                if (data.result[0].fields.fIdeTipoFiliacRosacruz == "I")
                {
                    $("#tipoAfiliacao").html("Individual");
                    $("#admissao").html(
                        data.result[0].fields.fDatAdmissRosacr.substr(8, 2) + "/"
                        + data.result[0].fields.fDatAdmissRosacr.substr(5, 2) + "/"
                        + data.result[0].fields.fDatAdmissRosacr.substr(0, 4)
                        );
                }else{
                
                    if (data.result[0].fields.fIdeTipoFiliacRosacruz == "D"||data.result[0].fields.fIdeTipoFiliacRosacruz=="C")
                    {
                        $("#tipoAfiliacao").html("Dual");
                        

                        if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalRosacruz)
                        {
                            //Montar link para companheiro
                            var link1 = '<a href=\"#\" class=\"btn btn-primary\" onclick=\"montaLinkConsultaMembro('+
                                data.result[0].fields.fSeqCadastCompanheiroRosacruz+','+
                                codigoAfiliacao+
                                ');\"><i class=\"fa fa-mail-forward\"></i> Buscar companheiro</a>';

                            $("#dual").html('- Principal &nbsp;&nbsp;&nbsp;&nbsp;');
                            $("#botaoDual").html(link1);
                            $("#admissao").html(
                            data.result[0].fields.fDatAdmissRosacr.substr(8, 2) + "/"
                            + data.result[0].fields.fDatAdmissRosacr.substr(5, 2) + "/"
                            + data.result[0].fields.fDatAdmissRosacr.substr(0, 4)
                            );
                        }else{
                            if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroRosacruz)
                            {
                                //Montar link para companheiro
                                var link2 = '<a href=\"#\" class="btn btn-primary" onclick=\"montaLinkConsultaMembro('+
                                    data.result[0].fields.fSeqCadastPrincipalRosacruz+','+
                                    codigoAfiliacao+
                                    ');\"><i class="fa fa-mail-reply"></i> Buscar principal</a>';

                                $("#dual").html('- Companheiro &nbsp;&nbsp;&nbsp;&nbsp;');
                                $("#botaoDual").html(link2);
                                $("#admissao").html(
                                data.result[0].fields.fDatAdmissCompanRosacr.substr(8, 2) + "/"
                                + data.result[0].fields.fDatAdmissCompanRosacr.substr(5, 2) + "/"
                                + data.result[0].fields.fDatAdmissCompanRosacr.substr(0, 4)
                                );
                            }
                        }
                    } 
                }    
         
                //R+C
                $("#situacaoCadastralRC").html(situacaoCadastral(data.result[0].fields.fIdeTipoSituacMembroRosacr));
                
                if(data.result[0].fields.fCodDesligRosacr!=0)
                {
                    $("#tr_motivoDesligamentoRC").show();
                    preencheMotivoDesligamentoRC(data.result[0].fields.fCodDesligRosacr);
                }else{
                    $("#tr_motivoDesligamentoRC").hide();
                    $("#motivoDesligamentoRC").html("");
                }    
                if(data.result[0].fields.fCodDesligTom!=0)
                {
                    $("#tr_motivoDesligamentoTom").show();
                    preencheMotivoDesligamentoTom(data.result[0].fields.fCodDesligTom);
                }else{
                    $("#tr_motivoDesligamentoTom").hide();
                    $("#motivoDesligamentoTom").html("");
                }  
                if(data.result[0].fields.fCodDesligOjp!=0)
                {
                    $("#tr_motivoDesligamentoOjp").show();
                    preencheMotivoDesligamentoOjp(data.result[0].fields.fCodDesligOjp);
                }else{
                    $("#tr_motivoDesligamentoOjp").hide();
                    $("#motivoDesligamentoOjp").html("");
                }
                
                //if (data.result[0].fields.fIdeTipoFiliacRosacruz == "C")
                //{
                //    $("#situacaoRemessaRC").html("Não possui");
                //} else {
                    $("#situacaoRemessaRC").html(situacaoRemessa(data.result[0].fields.fIdeTipoSituacRemessRosacr));
                //}
                if (data.result[0].fields.fDatQuitacRosacr == "")
                {
                    $("#dataQuitacaoRC").html("Não possui");
                } else {
                    $("#dataQuitacaoRC").html(
                            data.result[0].fields.fDatQuitacRosacr.substr(8, 2) + "/"
                            + data.result[0].fields.fDatQuitacRosacr.substr(5, 2) + "/"
                            + data.result[0].fields.fDatQuitacRosacr.substr(0, 4)
                            );
                }
                if (data.result[0].fields.fNumLoteAtualRosacr == 0)
                {
                    $("#loteRC").html("Não possui");
                } else {
                    $("#loteRC").html(data.result[0].fields.fNumLoteAtualRosacr);
                }
                if (data.result[0].fields.fNumLoteLimiteRosacr != 0)
                {
                    $("#grauRC").html("Hierarquia da AMORC");
                } else {
                    if (data.result[0].fields.fNumLoteAtualRosacr == 0)
                    {
                        $("#grauRC").html("Não possui");
                    } else {
                        if(grau!=0) {
                            $("#grauRC").html(grau+"º");
                        }else{
                            $("#grauRC").html("Não possui");
                        }
                    }
                }
                if (data.result[0].fields.fNumLoteLimiteRosacr == 0)
                {
                    $("#reiniciouRC").html("Não");
                } else {
                    $("#reiniciouRC").html("Sim");
                }

                //TOM
                $("#situacaoCadastralTOM").html(situacaoCadastral(data.result[0].fields.fIdeTipoSituacMembroTom));
                if (data.result[0].fields.fIdeTipoFiliacRosacruz == "C")
                {
                    $("#situacaoRemessaTOM").html("Não possui");
                } else {
                    $("#situacaoRemessaTOM").html(situacaoRemessa(data.result[0].fields.fIdeTipoSituacRemessTom));
                }
                if (data.result[0].fields.fDatQuitacTom == "" || data.result[0].fields.fIdeTipoFiliacRosacruz == "C")
                {
                    $("#dataQuitacaoTOM").html("Não Possui");
                } else {
                    $("#dataQuitacaoTOM").html(
                            data.result[0].fields.fDatQuitacTom.substr(8, 2) + "/"
                            + data.result[0].fields.fDatQuitacTom.substr(5, 2) + "/"
                            + data.result[0].fields.fDatQuitacTom.substr(0, 4)
                            );
                }
                if (data.result[0].fields.fNumLoteAtualTom == 0)
                {
                    $("#loteTOM").html("Não possui");
                } else {
                    $("#loteTOM").html(data.result[0].fields.fNumLoteAtualTom);
                }
                if (data.result[0].fields.fNumLoteAtualTom == 0)
                {
                    $("#grauTOM").html("Não possui");
                } else {
                    $("#grauTOM").html(grauTOM(data.result[0].fields.fNumLoteAtualTom));
                }
                if (data.result[0].fields.fNumLoteLimiteTom == 0)
                {
                    $("#reiniciouTOM").html("Não");
                } else {
                    $("#reiniciouTOM").html("Sim");
                }

                //OGG
                $("#situacaoCadastralOGG").html(situacaoCadastral(data.result[0].fields.fIdeTipoSituacMembroOjp));
                $("#situacaoRemessaOGG").html(situacaoRemessa(data.result[0].fields.fIdeTipoSituacRemessOjp));
                if (data.result[0].fields.fDatQuitacOjp == "")
                {
                    $("#dataQuitacaoOGG").html("Não possui");
                } else {
                    $("#dataQuitacaoOGG").html(
                            data.result[0].fields.fDatQuitacOjp.substr(8, 2) + "/"
                            + data.result[0].fields.fDatQuitacOjp.substr(5, 2) + "/"
                            + data.result[0].fields.fDatQuitacOjp.substr(0, 4)
                            );
                }
                if (data.result[0].fields.fNumLoteAtualOjp == 0)
                {
                    $("#loteOGG").html("Não possui");
                } else {
                    $("#loteOGG").html(data.result[0].fields.fNumLoteAtualOjp);
                }
                if (data.result[0].fields.fNumLoteAtualOjp == 0)
                {
                    $("#grauOGG").html("Não possui");
                } else {
                    $("#grauOGG").html(grauOGG(data.result[0].fields.fNumLoteAtualOjp));
                }

                if (data.result[0].fields.fSigOaAtuacaoRosacr.trim() != "")
                {
                    retornaOrganismoAfiliado(data.result[0].fields.fSigOaAtuacaoRosacr.trim());
                } else {
                    $("#organismoAfiliado").html("O sistema não encontrou nenhuma informação");
                }
                
                if(data.result[0].fields.fIdeAmorcgDigita=="N"
                        ||data.result[0].fields.fIdeForumrDigita=="N"
                        ||data.result[0].fields.fIdeOrosacDigita=="N"
                        ||data.result[0].fields.fIdeOpentaDigita=="N"
                        ||data.result[0].fields.fIdeOGGDigita=="N"
                        )//Se for uma das revistas
                {
                    var s = "";
                    if(data.result[0].fields.fIdeAmorcgDigita=="N")
                    {
                        s += " [AMORC GLP]";
                    }
                    if(data.result[0].fields.fIdeForumrDigita=="N")
                    {
                        s += " [Fórum]";
                    }
                    if(data.result[0].fields.fIdeOrosacDigita=="N")
                    {
                        s += " [O Rosacruz]";
                    }
                    if(data.result[0].fields.fIdeOpentaDigita=="N")
                    {
                        s += " [O Pantáculo]";
                    }
                    /*
                    if(data.result[0].fields.fIdeOGGDigita=="N")
                    {
                        s += " [Ordem Guias do Graal]";
                    }
                    */
                    $("#revistas").html(s);
                    $("#tr_revistas").show();
                }else{
                    $("#tr_revistas").hide();
                }
                var abrirPesquisa = $('#abrirPesquisa').val();

                if(abrirPesquisa==1) {
                    $('#mySeeDetalhes').modal({});
                }
            } else {
                swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao consultar o membro');
        }
    });
 
}

function montaLinkConsultaMembro(seqCadast,codigoAfiliacao)
{
    $('#abrirPesquisa').val(0);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembroPorSeqCadast.php",
        dataType: "json",
        data: {seqCadast: seqCadast},
        success: function (data) {
            //alert(data.result[0].fields.fSeqFormacao);
            if (data.result[0].fields.fNomCliente.trim() != "") {

                var nome = data.result[0].fields.fNomCliente;
                carregaCamposPesquisaFonetica('', '', seqCadast, nome, 'codigoAfiliacao', codigoAfiliacao, 'nome', 'resultadoPesquisa', '1');
                $("#codigoAfiliacao").val(codigoAfiliacao);
                $("#nome").val(nome);
                consultaMembro();
                $('#abrirPesquisa').val(1);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao consultar o membro');
        }
    });
}

function fechaModalConsultaMembros()
{
    $('#mySeeDetalhes').modal('toggle');
    location.href='./painelDeControle.php?corpo=buscaMembros';
}



function preencheProfissao(cod)
{
    $.ajax({
        type: "post",
        url: "js/ajax/preencheProfissao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {cod: cod}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#profissao").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function preencheOcupacao(cod)
{
    $.ajax({
        type: "post",
        url: "js/ajax/preencheOcupacao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {cod: cod}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#ocupacao").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function retornaOrganismoAfiliado(siglaOA)
{
    $.ajax({
        type: "post",
        url: "js/ajax/retornaOrganismoAfiliado.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {siglaOA: siglaOA}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#organismoAfiliado").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function consultaIniciacoesMembro(codigoAfiliacao, companheiro, seqCadast, numGrau, lote,dataAdmissao)
{
    
    $.ajax({
        type: "post",
        url: "js/ajax/getIniciacoesMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast:seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            
            $("#iniciacoes").html(data);
            consultaCargosMembro(codigoAfiliacao, companheiro, seqCadast, numGrau, lote,dataAdmissao);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function consultaIniciacoesMembroTOM(codigoAfiliacao, companheiro, seqCadast, numGrau, lote)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getIniciacoesMembroTOM.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast:seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#iniciacoesTOM").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function consultaIniciacoesMembroOGG(codigoAfiliacao, companheiro, seqCadast, numGrau, lote)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getIniciacoesMembroOGG.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast:seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#iniciacoesOGG").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function consultaCargosMembro(codigoAfiliacao, companheiro, seqCadast, numGrau, lote,dataAdmissao)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getCargosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#cargos").html(data);
            consultaCargosMembroNaoAtuantes(codigoAfiliacao, companheiro, seqCadast, numGrau, lote,dataAdmissao);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function consultaCargosMembroNaoAtuantes(codigoAfiliacao, companheiro, seqCadast, numGrau, lote,dataAdmissao)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getCargosMembroNaoAtuantes.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#cargosNaoAtuantes").html(data);
            consultaIniciacoesPendentesMembroRC(codigoAfiliacao, companheiro, seqCadast, numGrau, lote,dataAdmissao);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function consultaIniciacoesPendentesMembroRC(codigoAfiliacao, companheiro, seqCadast, numGrau, lote,dataAdmissao)
{
    //alert(seqCadast);
    $("#iniciacoesPendentesRC").html("");
    $.ajax({
        type: "post",
        url: "js/ajax/getIniciacoesPendentesMembroRC.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {numGrau: numGrau, seqCadast:seqCadast, lote:lote, dataAdmissao: dataAdmissao, companheiro:companheiro}, // exemplo: {codigo:125, nome:"Joaozinho"}
        success: function (data) {
            $("#iniciacoesPendentesRC").html(data);

            $("#tempoCasaRC").html("");
            $.ajax({
                type: "post",
                url: "js/ajax/tempoCasaRC.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "html",
                data: {numGrau: numGrau, seqCadast:seqCadast, lote:lote, dataAdmissao: dataAdmissao}, // exemplo: {codigo:125, nome:"Joaozinho"}
                success: function (data) {
                    $("#tempoCasaRC").html(data);
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                },
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        },
    });
}

function autorizacaoTemplo()
{
    var usuario = $("#usuario").val();
    var nomeMembro = $("#nome").val();
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
    var tipoMembro = 1;

    if (nomeMembro == "")
    {
        swal({title: "Aviso!", text: "Digite o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    } else {
        if (codigoAfiliacao == "")
        {
            swal({title: "Aviso!", text: "Digite o código de afiliação do membro", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        } else {

            $.ajax({
                type: "post",
                url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.result[0].fields.fNomCliente.trim() != "")
                    {
                        //Cadastrar solicitação
                        cadastrarSolicitacaoAutorizacaoEntradaTemplo(usuario, data.result[0].fields.fSeqCadast, data.result[0].fields.fNomCliente.trim(), codigoAfiliacao, idOrganismoAfiliado);
                    } else {
                        swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                        return false;
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro ao consultar o membro');
                }
            });
        }
    }
}

function cadastrarSolicitacaoAutorizacaoEntradaTemplo(usuario, seqCadast, nome, codigoAfiliacao, idOrganismoAfiliado)
{
    $.ajax({
        type: "post",
        url: "js/ajax/cadastrarSolicitacaoAutorizacaoEntradaTemplo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {usuario: usuario, seqCadast: seqCadast, idOrganismoAfiliado: idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            if (data.status == 1)
            {
                abrirPopUpImprimiAutorizacaoEntradaTemploConvocacaoRitualistica(nome, codigoAfiliacao, idOrganismoAfiliado, data.numeroSolicitacoes);
            } else {
                alert('Erro ao cadastrar solicitacao de autorizacao');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao cadastrar a solicitacao de autorizacao do membro');
        }
    });
}

function carteiraRitualistica()
{
    var nomeMembro = $("#nome").val();
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var tipoMembro = 1;

    if (nomeMembro == "")
    {
        swal({title: "Aviso!", text: "Digite o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    } else {
        if (codigoAfiliacao == "")
        {
            swal({title: "Aviso!", text: "Digite o código de afiliação do membro", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        } else {
            //alert('testeaaa');
            $.ajax({
                type: "post",
                url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    //alert(data.result[0].fields.fNomCliente);

                    if (data.result[0].fields.fNomCliente.trim() != "")
                    {
                        var companheiro = "N"
                        if (data.result[0].fields.fSeqCadastCompanheiroRosacruz == data.result[0].fields.fSeqCadast)
                        {
                            companheiro = "S";
                        }
                        //alert(companheiro);
                        abrirPopUpCarteirinhaMembro(codigoAfiliacao, companheiro);
                    } else {
                        swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                        return false;
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro ao consultar o membro');
                }
            });

        }
    }
}

function carteiraRitualisticaWeb()
{
    var nomeMembro = $("#nome").val();
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var tipoMembro = 1;

    if (nomeMembro == "")
    {
        swal({title: "Aviso!", text: "Digite o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    } else {
        if (codigoAfiliacao == "")
        {
            swal({title: "Aviso!", text: "Digite o código de afiliação do membro", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        } else {
            //alert('testeaaa');
            $.ajax({
                type: "post",
                url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    //alert(data.result[0].fields.fNomCliente);

                    if (data.result[0].fields.fNomCliente.trim() != "")
                    {
                        var companheiro = "N"
                        if (data.result[0].fields.fSeqCadastCompanheiroRosacruz == data.result[0].fields.fSeqCadast)
                        {
                            companheiro = "S";
                        }
                        //alert(companheiro);
                        abrirPopUpCarteirinhaMembroWeb(codigoAfiliacao, companheiro, data.result[0].fields.fNomCliente.trim(),
                                data.result[0].fields.fDatImplantacaoCadastro.substr(8, 2) + "/"
                                + data.result[0].fields.fDatImplantacaoCadastro.substr(5, 2) + "/"
                                + data.result[0].fields.fDatImplantacaoCadastro.substr(0, 4)
                                );
                    } else {
                        swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                        return false;
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro ao consultar o membro');
                }
            });

        }
    }
}

function atualizaOaAtuacao(seqCadast, siglaOa)
{
    $.ajax({
        type: "post",
        url: "../js/ajax/atualizarOaAtuacao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: seqCadast, siglaOa: siglaOa}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            //alert('Oa atualizado com sucesso!');
            //alert(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //alert('Erro ao atualizar o oa de atuacao do membro - Avise o setor de TI!');
        }
    });
}

function filtraPessoasParaQuemVoceQuerMandarEmail(search)
{
    $.ajax({
        type: "post",
        url: "js/ajax/filtraPessoasParaQuemVoceQuerMandarEmail.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {search: search}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#pessoasParaQuemVoceQuerMandarEmail").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function comprovanteQuitacao()
{
    var nomeMembro = $("#nome").val();
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var tipoMembro = 1;

    if (nomeMembro == "")
    {
        swal({title: "Aviso!", text: "Digite o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    } else {
        if (codigoAfiliacao == "")
        {
            swal({title: "Aviso!", text: "Digite o código de afiliação do membro", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        } else {
            //alert('testeaaa');
            $.ajax({
                type: "post",
                url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    //alert(data.result[0].fields.fNomCliente);

                    if (data.result[0].fields.fNomCliente.trim() != "")
                    {
                        var companheiro = "N"
                        if (data.result[0].fields.fSeqCadastCompanheiroRosacruz == data.result[0].fields.fSeqCadast)
                        {
                            companheiro = "S";
                        }

                        var quitacaoRC = "--";
                        if (data.result[0].fields.fDatQuitacRosacr != "")
                        {
                            quitacaoRC = data.result[0].fields.fDatQuitacRosacr.substr(8, 2) + "/"
                                    + data.result[0].fields.fDatQuitacRosacr.substr(5, 2) + "/"
                                    + data.result[0].fields.fDatQuitacRosacr.substr(0, 4);
                        }

                        var quitacaoTOM = "--";
                        if (data.result[0].fields.fDatQuitacTom != "")
                        {
                            quitacaoTOM = data.result[0].fields.fDatQuitacTom.substr(8, 2) + "/"
                                    + data.result[0].fields.fDatQuitacTom.substr(5, 2) + "/"
                                    + data.result[0].fields.fDatQuitacTom.substr(0, 4);
                        }
                        //alert(companheiro);
                        abrirPopUpComprovanteQuitacao(codigoAfiliacao, companheiro, data.result[0].fields.fNomCliente.trim(),
                                data.result[0].fields.fDatImplantacaoCadastro.substr(8, 2) + "/"
                                + data.result[0].fields.fDatImplantacaoCadastro.substr(5, 2) + "/"
                                + data.result[0].fields.fDatImplantacaoCadastro.substr(0, 4),
                                quitacaoRC,
                                quitacaoTOM,
                                data.result[0].fields.fNumLoteAtualRosacr,
                                data.result[0].fields.fNumLoteAtualTom

                                );
                    } else {
                        swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                        return false;
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro ao consultar o membro');
                }
            });

        }
    }
}

function carregarCadastroMembroAMORC()
{
    var nomeMembro = $("#nome").val();
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var tipoMembro = 1;

    if (nomeMembro == "")
    {
        swal({title: "Aviso!", text: "Digite o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    } else {
        if (codigoAfiliacao == "")
        {
            swal({title: "Aviso!", text: "Digite o código de afiliação do membro", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        } else {
            //alert('testeaaa');
            $.ajax({
                type: "post",
                url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    //alert(data.result[0].fields.fNomCliente);

                    if (data.result[0].fields.fNomCliente.trim() != "")
                    {
                        /*
                         var dataNascimento = data.result[0].fields.fDatNascimento.substr(8, 2)+"/"
                         +data.result[0].fields.fDatNascimento.substr(5, 2)+"/"
                         +data.result[0].fields.fDatNascimento.substr(0, 4);
                         */
                        var dataNascimento = data.result[0].fields.fDatNascimento;
                        location.href = '?corpo=cadastroMembroAMORC&nome=' + data.result[0].fields.fNomCliente.trim() 
                                    + '&codigoAfiliacao=' + codigoAfiliacao 
                                    + '&tipoMembro=' + tipoMembro 
                                    + '&logradouro=' + data.result[0].fields.fNomLogradouro.trim() 
                                    + '&numero=' + data.result[0].fields.fNumEndereco 
                                    + '&complemento=' + data.result[0].fields.fDesComplementoEndereco.trim() 
                                    + '&bairro=' + data.result[0].fields.fNomBairro.trim() 
                                    + '&cidade=' + data.result[0].fields.fNomCidade.trim() 
                                    + '&uf=' + data.result[0].fields.fSigUf 
                                    + '&cep=' + data.result[0].fields.fCodCepEndereco.trim() 
                                    + '&numeroCaixaPostal=' + data.result[0].fields.fNumCaixaPostal 
                                    + '&codCepCaixaPostal=' + data.result[0].fields.fCodCepCaixaPostal
                                    + '&pais=' + data.result[0].fields.fSigPaisEndereco 
                                    + '&telefone=' + data.result[0].fields.fNumTelefone.trim() 
                                    + '&email=' + data.result[0].fields.fDesEmail.trim() 
                                    + '&dataNascimento=' + dataNascimento 
                                    + '&conjuge=' + data.result[0].fields.fNomConjuge.trim()
                                    +'&seqCadast='+data.result[0].fields.fSeqCadast
                                    +'&seqOcupacao='+data.result[0].fields.fSeqOcupacao
                                    +'&seqEspecializacao='+data.result[0].fields.fSeqEspecializacao
                                    +'&seqFormacao='+data.result[0].fields.fSeqFormacao
                                    +'&seqMoeda='+data.result[0].fields.fIdeTipoMoeda
                            
                                    +'&sexo='+data.result[0].fields.fIdeSexo
                                    +'&telefoneFixo='+data.result[0].fields.fNumTelefoFixo
                                    +'&celular='+data.result[0].fields.fNumCelula
                                    +'&outroTelefone='+data.result[0].fields.fNumOutroTelefo
                                    +'&descricaoOutroTelefone='+data.result[0].fields.fDesOutroTelefo
                            
                                    +'&aceitoContato='+data.result[0].fields.fIdeAceitoContato
            
                                    +'&periodoContato='+data.result[0].fields.fIdePeriodoContato
                                    +'&formaIdealContato='+data.result[0].fields.fIdeFormaContatoIdeal
                            
                                    +'&rg='+data.result[0].fields.fCodRg
                                    +'&enderecoCorrespondencia='+data.result[0].fields.fIdeEnderecoCorrespondencia
                                    +'&estadoCivil='+data.result[0].fields.fCodEstadoCivil
                                    +'&grauInstrucao='+data.result[0].fields.fCodGrauInstrucao
                                    
                                    +'&possuiComputador='+data.result[0].fields.fIdePossuiComputador
                                    +'&possuiInternet='+data.result[0].fields.fIdePossuiAcessoInternet                                                                   
                                    +'&desejaColaborar='+data.result[0].fields.fDesejaColaborarComOANasEquipes
                                    +'&experienciaProfissional='+data.result[0].fields.fExperienciaProfissionalResumida
                                    +'&membroURCI='+data.result[0].fields.fIdeMembroURCI
                            
                                    +'&IdeAmorcgDigita='+data.result[0].fields.fIdeAmorcgDigita
                                    +'&IdeForumrDigita='+data.result[0].fields.fIdeForumrDigita
                                    +'&IdeOrosacDigita='+data.result[0].fields.fIdeOrosacDigita
                                    +'&IdeOpentaDigita='+data.result[0].fields.fIdeOpentaDigita
                                    +'&IdeOGGDigita='+data.result[0].fields.fIdeOGGDigita
                                    ;
                    } else {
                        swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                        return false;
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro ao consultar o membro');
                }
            });

        }
    }
}

function getMotivoIsencao(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getMotivoIsencao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#motivo").html(data.motivo);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirIsencaoOa(id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente essa isenção!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirIsencaoOa.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        location.href = '?corpo=buscaIsencaoOA&excluido=1';
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o registro e o membro continua isento :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function consultaMembroOA(nomeMembro, codigoAfiliacao)
{
    var tipoMembro = 1;

    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.result[0].fields.fNomCliente.trim() != "")
            {
                //Preencher Iniciações
                var companheiro = "N"
                if (data.result[0].fields.fSeqCadastCompanheiroRosacruz == data.result[0].fields.fSeqCadast)
                {
                    companheiro = "S";
                }

                var fSeqCadast              = data.result[0].fields.fSeqCadast;
                var fNumLoteAtualRosacr     = data.result[0].fields.fNumLoteAtualRosacr;
                var numeroGrau              = numeroGrauRC(data.result[0].fields.fNumLoteAtualRosacr);
                
                consultaIniciacoesMembro(codigoAfiliacao, companheiro, fSeqCadast, numeroGrauRC(data.result[0].fields.fNumLoteAtualRosacr),fNumLoteAtualRosacr);
                consultaIniciacoesMembroTOM(codigoAfiliacao, companheiro, fSeqCadast, numeroGrauRC(data.result[0].fields.fNumLoteAtualRosacr),fNumLoteAtualRosacr);
                consultaIniciacoesMembroOGG(codigoAfiliacao, companheiro, fSeqCadast, numeroGrauRC(data.result[0].fields.fNumLoteAtualRosacr),fNumLoteAtualRosacr);
                preencheProfissao(data.result[0].fields.fSeqFormacao);
                preencheOcupacao(data.result[0].fields.fSeqOcupacao);
                
                //Preencher outros campos
                $("#m_nome").html(data.result[0].fields.fNomCliente.trim());
                $("#m_codigoAfiliacao").html(codigoAfiliacao);
                //$("#logradouro").html(data.result[0].fields.fNomLogradouro.trim());
                //$("#numero").html(data.result[0].fields.fNumEndereco);
                $("#cidade").html(data.result[0].fields.fNomCidade.trim());
                //$("#cep").html(data.result[0].fields.fCodCepEndereco.trim());
                $("#dataNascimento").html(
                        data.result[0].fields.fDatNascimento.substr(8, 2) + "/"
                        + data.result[0].fields.fDatNascimento.substr(5, 2) + "/"
                        + data.result[0].fields.fDatNascimento.substr(0, 4)
                        );
                //$("#profissao").html(data.result[0].fields.fDesFormacao.trim());
                //$("#ocupacao").html(data.result[0].fields.fDesOcupacao.trim());
                //$("#rg").html(data.result[0].fields.fCodRg.trim());			
                //$("#cpf").html(data.result[0].fields.fCodCpf.trim());
                $("#admissao").html(
                        data.result[0].fields.fDatImplantacaoCadastro.substr(8, 2) + "/"
                        + data.result[0].fields.fDatImplantacaoCadastro.substr(5, 2) + "/"
                        + data.result[0].fields.fDatImplantacaoCadastro.substr(0, 4)
                        );
                //$("#telefone").html(data.result[0].fields.fNumTelefone.trim());
                //$("#email").html(data.result[0].fields.fDesEmail.trim());
                $("#tipoAfiliacao").html("");
                $("#dual").html("");
                if (data.result[0].fields.fIdeTipoFiliacRosacruz == "I")
                {
                    $("#tipoAfiliacao").html("Individual");
                }else{
                
                    if (data.result[0].fields.fIdeTipoFiliacRosacruz == "D"||data.result[0].fields.fIdeTipoFiliacRosacruz=="C")
                    {
                        $("#tipoAfiliacao").html("Dual");

                        if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalRosacruz)
                        {
                            $("#dual").html("- Principal");
                        }else{
                            if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroRosacruz)
                            {    
                                $("#dual").html("- Companheiro");
                            }
                        }
                    } 
                }
                
                if(data.result[0].fields.fIdeTipoTom=="S")
                {
                    $("#tom").html("SIM");
                }else{
                    $("#tom").html("NÃO");
                }   
                $("#tipoAfiliacaoTom").html("");
                $("#dualTom").html("");
                if(data.result[0].fields.fIdeTipoTom=="S")//Se for TOM 
                {   
                    if(data.result[0].fields.fIdeTipoFiliacTom=="D"||data.result[0].fields.fIdeTipoFiliacTom=="C")
                    {    
                        $("#tipoAfiliacaoTom").html("- Dual");
                        if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalTom)
                        {    
                            $("#dualTom").html("- Principal");
                        }
                        if(data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroTom)
                        {    
                            $("#dualTom").html("- Companheiro");
                        }
                    }else{
                        $("#tipoAfiliacaoTom").html("- Individual");
                    }
                }

                //R+C
                $("#situacaoCadastralRC").html(situacaoCadastral(data.result[0].fields.fIdeTipoSituacMembroRosacr));
                if (data.result[0].fields.fIdeTipoFiliacRosacruz == "C")
                {
                    $("#situacaoRemessaRC").html("Não possui");
                } else {
                    $("#situacaoRemessaRC").html(situacaoRemessa(data.result[0].fields.fIdeTipoSituacRemessRosacr));
                }
                if (data.result[0].fields.fDatQuitacRosacr == "" || data.result[0].fields.fIdeTipoFiliacRosacruz == "C")
                {
                    $("#dataQuitacaoRC").html("Não possui");
                } else {
                    $("#dataQuitacaoRC").html(
                            data.result[0].fields.fDatQuitacRosacr.substr(8, 2) + "/"
                            + data.result[0].fields.fDatQuitacRosacr.substr(5, 2) + "/"
                            + data.result[0].fields.fDatQuitacRosacr.substr(0, 4)
                            );
                }
                if (data.result[0].fields.fNumLoteAtualRosacr == 0)
                {
                    $("#loteRC").html("Não possui");
                } else {
                    $("#loteRC").html(data.result[0].fields.fNumLoteAtualRosacr);
                }
                if (data.result[0].fields.fNumLoteAtualRosacr == 0)
                {
                    $("#grauRC").html("Não possui");
                } else {
                    $("#grauRC").html(grauRC(data.result[0].fields.fNumLoteAtualRosacr));
                }
                if (data.result[0].fields.fNumLoteLimiteRosacr == 0)
                {
                    $("#reiniciouRC").html("Não");
                } else {
                    $("#reiniciouRC").html("Sim");
                }

                //TOM
                $("#situacaoCadastralTOM").html(situacaoCadastral(data.result[0].fields.fIdeTipoSituacMembroTom));
                if (data.result[0].fields.fIdeTipoFiliacRosacruz == "C")
                {
                    $("#situacaoRemessaTOM").html("Não possui");
                } else {
                    $("#situacaoRemessaTOM").html(situacaoRemessa(data.result[0].fields.fIdeTipoSituacRemessTom));
                }
                if (data.result[0].fields.fDatQuitacTom == "" || data.result[0].fields.fIdeTipoFiliacRosacruz == "C")
                {
                    $("#dataQuitacaoTOM").html("Não Possui");
                } else {
                    $("#dataQuitacaoTOM").html(
                            data.result[0].fields.fDatQuitacTom.substr(8, 2) + "/"
                            + data.result[0].fields.fDatQuitacTom.substr(5, 2) + "/"
                            + data.result[0].fields.fDatQuitacTom.substr(0, 4)
                            );
                }
                if (data.result[0].fields.fNumLoteAtualTom == 0)
                {
                    $("#loteTOM").html("Não possui");
                } else {
                    $("#loteTOM").html(data.result[0].fields.fNumLoteAtualTom);
                }
                if (data.result[0].fields.fNumLoteAtualTom == 0)
                {
                    $("#grauTOM").html("Não possui");
                } else {
                    $("#grauTOM").html(grauTOM(data.result[0].fields.fNumLoteAtualTom));
                }
                if (data.result[0].fields.fNumLoteLimiteTom == 0)
                {
                    $("#reiniciouTOM").html("Não");
                } else {
                    $("#reiniciouTOM").html("Sim");
                }

                //OGG
                $("#situacaoCadastralOGG").html(situacaoCadastral(data.result[0].fields.fIdeTipoSituacMembroOjp));
                $("#situacaoRemessaOGG").html(situacaoRemessa(data.result[0].fields.fIdeTipoSituacRemessOjp));
                if (data.result[0].fields.fDatQuitacOjp == "")
                {
                    $("#dataQuitacaoOGG").html("Não possui");
                } else {
                    $("#dataQuitacaoOGG").html(
                            data.result[0].fields.fDatQuitacOjp.substr(8, 2) + "/"
                            + data.result[0].fields.fDatQuitacOjp.substr(5, 2) + "/"
                            + data.result[0].fields.fDatQuitacOjp.substr(0, 4)
                            );
                }
                if (data.result[0].fields.fNumLoteAtualOjp == 0)
                {
                    $("#loteOGG").html("Não possui");
                } else {
                    $("#loteOGG").html(data.result[0].fields.fNumLoteAtualOjp);
                }
                if (data.result[0].fields.fNumLoteAtualOjp == 0)
                {
                    $("#grauOGG").html("Não possui");
                } else {
                    $("#grauOGG").html(grauOGG(data.result[0].fields.fNumLoteAtualOjp));
                }

                $('#mySeeDetalhes').modal({
                });
            } else {
                swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao consultar o membro');
        }
    });

}

function salvarTrimestralidade()
{
    var dataTrimestralidade = $("#dataTrimestralidade").val();
    var valorTrimestralidade = $("#valorTrimestralidade").val();
    var usuario = $("#usuario").val();
    var salvar = 0;
    var idTrimestralidade = null;

    if (dataTrimestralidade == "")
    {
        swal({title: "Aviso!", text: "Preencha a data", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if (valorTrimestralidade == "")
    {
        swal({title: "Aviso!", text: "Preencha o valor", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if ($("#idTrimestralidade").val() == "")
    {
        salvar = 0;
    } else {
        salvar = 1;
        idTrimestralidade = $("#idTrimestralidade").val();
    }

    $.ajax({
        type: "post",
        url: "js/ajax/salvaTrimestralidade.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {dataTrimestralidade: dataTrimestralidade,
            valorTrimestralidade: valorTrimestralidade,
            idTrimestralidade: idTrimestralidade,
            salvar: salvar,
            usuario: usuario}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            if (retorno.status == 1)
            {
                location.href = "painelDeControle.php?corpo=buscaTrimestralidadeGLP&salvo=1";
            }
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
function previaBoletoContribuicao()
{
    var nomeMembro = $("#nome").val();
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var tipoMembro = $("#tipo").val();
    var quantidade = $("#quantidade").val();
    var contribuicao = $("input[name='contribuicao']:checked").val();
    
    if (nomeMembro == "")
    {
        swal({title: "Aviso!", text: "Digite o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    } else {
        if (codigoAfiliacao == "")
        {
            swal({title: "Aviso!", text: "Digite o código de afiliação do membro", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        } else {
            if (quantidade == 0)
            {
                swal({title: "Aviso!", text: "Escolha a contribuição e a quantidade", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            } else {
                if (quantidade > 12)
                {
                    swal({title: "Aviso!", text: "É possível gerar boletos apenas para um ano! Pois os valores podem mudar de um ano para outro!", type: "warning", confirmButtonColor: "#1ab394"});
                    return false;
                } else {
                    //teste
                    $.ajax({
                        type: "post",
                        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                        success: function (data) {
                            var nomeSelecionado =data.result[0].fields.fNomCliente.trim();
                            //alert(nomeSelecionado);
                            if(
                                    (tipoMembro==1&&data.result[0].fields.fSeqCadastPrincipalRosacruz==0&&data.result[0].fields.fSeqCadastCompanheiroRosacruz==0)
                                    || (tipoMembro==6&&data.result[0].fields.fSeqCadastPrincipalTom==0&&data.result[0].fields.fSeqCadastCompanheiroTom==0)
                            )
                            {
                                if(data.result[0].fields.fCodCpf.trim() !="")
                                {    
                                    if (data.result[0].fields.fNomCliente.trim() != "")
                                    {
                                        $("#m_nome").html(data.result[0].fields.fNomCliente.trim());
                                        $("#m_codigoAfiliacao").html(codigoAfiliacao);
                                        $("#nome").val(data.result[0].fields.fNomCliente.trim());
                                        $("#codigoAfiliacao").val(codigoAfiliacao);
                                        carregarPreviaBoletoContribuicao(
                                                codigoAfiliacao, 
                                                tipoMembro, 
                                                data.result[0].fields.fSeqCadast,
                                                escape(data.result[0].fields.fNomCliente.trim()),
                                                escape(data.result[0].fields.fNomLogradouro.trim()),
                                                data.result[0].fields.fNumEndereco,
                                                escape(data.result[0].fields.fNomCidade),
                                                data.result[0].fields.fSigUf,
                                                data.result[0].fields.fCodCepEndereco.trim(),
                                                data.result[0].fields.fNomCliente.trim(),
                                                data.result[0].fields.fNomLogradouro.trim(),
                                                data.result[0].fields.fNomCidade,
                                                quantidade,
                                                contribuicao,
                                                data.result[0].fields.fIdeOpcaoRemessRosacr,
                                                data.result[0].fields.fSeqCadastPrincipalRosacruz,
                                                data.result[0].fields.fNumLoteAtualTom,
                                                data.result[0].fields.fSeqCadastPrincipalRosacruz,
                                                nomeSelecionado
                                                );
                                    }else {
                                        swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                                        return false;
                                    }
                                }else{
                                    swal({title: "Aviso!", text: "Não é possível gerar boleto para membro que não informou o CPF! Favor entrar em contato com a GLP e informar o CPF do membro!", type: "warning", confirmButtonColor: "#1ab394"});
                                    return false;
                                }
                                  
                            }else{    
                            
                                $.ajax({
                                    type: "post",
                                    url: "js/ajax/retornaDadosMembroPorSeqCadast.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                                    dataType: "json",
                                    data: {seqCadast: data.result[0].fields.fSeqCadastPrincipalRosacruz}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                                    success: function (data2) {
                                                //alert(data);
                                                //alert(data.result[0].fields.fCodCpf.trim());
                                                if(data2.result[0].fields.fCodCpf.trim() !="")
                                                {    
                                                    if (data.result[0].fields.fNomCliente.trim() != "")
                                                    {
                                                        $("#m_nome").html(data.result[0].fields.fNomCliente.trim());
                                                        $("#m_codigoAfiliacao").html(codigoAfiliacao);
                                                        $("#nome").val(data.result[0].fields.fNomCliente.trim());
                                                        $("#codigoAfiliacao").val(codigoAfiliacao);
                                                        
                                                        carregarPreviaBoletoContribuicao(
                                                                codigoAfiliacao, 
                                                                tipoMembro, 
                                                                data.result[0].fields.fSeqCadast,
                                                                escape(data.result[0].fields.fNomCliente.trim()),
                                                                escape(data.result[0].fields.fNomLogradouro.trim()),
                                                                data.result[0].fields.fNumEndereco,
                                                                escape(data.result[0].fields.fNomCidade),
                                                                data.result[0].fields.fSigUf,
                                                                data.result[0].fields.fCodCepEndereco.trim(),
                                                                data.result[0].fields.fNomCliente.trim(),
                                                                data.result[0].fields.fNomLogradouro.trim(),
                                                                data.result[0].fields.fNomCidade,
                                                                quantidade,
                                                                contribuicao,
                                                                data.result[0].fields.fIdeOpcaoRemessRosacr,
                                                                data.result[0].fields.fSeqCadastPrincipalRosacruz,
                                                                data.result[0].fields.fNumLoteAtualTom,
                                                                data.result[0].fields.fSeqCadastPrincipalRosacruz,
                                                                nomeSelecionado
                                                                );
                                                    } else {
                                                        swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                                                        return false;
                                                    }
                                                }else{
                                                    swal({title: "Aviso!", text: "Não é possível gerar boleto para membro que não informou o CPF! Favor entrar em contato com a GLP e informar o CPF do membro!", type: "warning", confirmButtonColor: "#1ab394"});
                                                    return false;
                                                }
                                            },
                                    error: function (xhr, textStatus, errorThrown) {
                                        alert('erro ao consultar o membro');
                                    }
                                });   
                            }    
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('erro ao consultar o membro');
                        }
                    });
                }
            }
        }
    }
}

function carregarPreviaBoletoContribuicao(codigoAfiliacao, tipoMembro, seqCadast, nome, endereco, numero, cidade, estado, cep, nomeSemScape, enderecoSemScape, cidadeSemScape,quantidade,contribuicao, modalidade, principaltom,lote,principalrc,membroSelecionado)
{

    var quantidadeTrimestralidade = 1;
    var usuario = $("#usuario").val();
    
    $("#msg").html("");
    $("#tr_aviso").hide();
    $("#gerarBoletoDual").hide();
    
    //alert(quantidade);
    $.ajax({
        type: "post",
        url: "js/ajax/previaBoletoContribuicao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
            codigoAfiliacao: codigoAfiliacao,
            tipoMembro: tipoMembro,
            seqCadast: seqCadast,
            quantidade: quantidadeTrimestralidade,
            nome: nomeSemScape,
            endereco: enderecoSemScape,
            numero: numero,
            cidade: cidadeSemScape,
            estado: estado,
            cep: cep,
            usuario: usuario}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            
            //Pegar informações dinâmicas da tabela de contribuição
            
            var modalidadeAnualIndividualReal1 =parseFloat($("#modalidadeAnualIndividualReal1").val()); 
            var modalidadeAnualDualReal1= parseFloat($("#modalidadeAnualDualReal1").val());
            
            var modalidadeAnualIndividualReal2= parseFloat($("#modalidadeAnualIndividualReal2").val());
            var modalidadeAnualDualReal2= parseFloat($("#modalidadeAnualDualReal2").val());
            
            var modalidadeAnualIndividualReal3= parseFloat($("#modalidadeAnualIndividualReal3").val());
            var modalidadeAnualDualReal3= parseFloat($("#modalidadeAnualDualReal3").val());
            
            var modalidadeAnualIndividualReal4= parseFloat($("#modalidadeAnualIndividualReal4").val());
            var modalidadeAnualDualReal4= parseFloat($("#modalidadeAnualDualReal4").val());
            
            var modalidadeAnualIndividualCFD4= parseFloat($("#modalidadeAnualIndividualCFD4").val());
            var modalidadeAnualDualCFD4= parseFloat($("#modalidadeAnualDualCFD4").val());
            var modalidadeMensalIndividual4= parseFloat($("#modalidadeMensalIndividual4").val());
            var modalidadeTrimestralIndividualReal4= parseFloat($("#modalidadeTrimestralIndividualReal4").val());
            
            //alert(data);
            var valorImpresso=0;
            var valorDigital=0;
            var valorAmbos=0;
            
            if (data.CodMembro != "")
            {
                $("#gerarBoleto").show();
                if (data.IdetipoFiliacao == "D")
                {
                    valorImpresso=modalidadeAnualDualReal1;//Rosacruz - Modalidade 1 Anual Dual
                    valorDigital=modalidadeAnualDualReal2;//Rosacruz - Modalidade 2 Anual Dual
                    valorAmbos=modalidadeAnualDualReal3;//Rosacruz - Modalidade 3 Anual Dual
                    
                    if(seqCadast!=principaltom&&seqCadast!=principalrc)
                    {    
                        $("#tr_aviso").show();
                        $("#msg").html("<b>A geração de boletos é somente para o titular nos casos de duais!</b>");
                    }
                    
                    //alert(seqCadast);
                    //alert(principal);
                    $("#gerarBoleto").hide();
                    $("#gerarBoletoDual").show();
                    if(tipoMembro==6)
                    {
                        if(seqCadast!=principaltom)
                        {
                            $("#tipoAfiliacao").html("Dual (Companheiro)");

                            //alert(principal);
                            $("#seqCadast").val(principaltom);
                            preencheNomePrincipal(principaltom);
                        }else{
                            $("#tipoAfiliacao").html("Dual (Principal)");
                            $("#seqCadast").val(seqCadast);
                        }
                    }else{
                        if(seqCadast!=principalrc)
                        {
                            $("#tipoAfiliacao").html("Dual (Companheiro)");
                            //alert(principal);
                            $("#seqCadast").val(principalrc);
                            preencheNomePrincipal(principalrc);
                        }else{
                            $("#tipoAfiliacao").html("Dual (Principal)");
                            $("#seqCadast").val(seqCadast);
                        } 
                    }    
                    
                } else {
                    if(tipoMembro==1)
                    {    
                        valorImpresso=modalidadeAnualIndividualReal1;//Rosacruz - Modalidade 1 Anual Individual
                        valorDigital=modalidadeAnualIndividualReal2;//Rosacruz - Modalidade 2 Anual Individual
                        valorAmbos=modalidadeAnualIndividualReal3;//Rosacruz - Modalidade 3 Anual Individual
                    }
                    $("#tipoAfiliacao").html("Individual");
                    $("#seqCadast").val(seqCadast);
                }
                if (data.IdeStopDefinitivo == "S")
                {
                    $("#situacaoMembro").html("Sim");
                } else {
                    $("#situacaoMembro").html("Não");
                }
                if (data.IdeDebitoConta == "S")
                {
                    $("#debitoEmConta").html("Sim");

                } else {
                    $("#debitoEmConta").html("Não");

                }
                
                $("#valorTaxaAfiliacao").html("R$ " + data.ValTaxaAfiliacao.toFixed(2).replace(".", ","));
                
                if (data.ValAssinatura != 0)
                {
                    $("#valorAssinatura").html(data.ValAssinatura);
                    $("#tr_assinatura").show();
                } else {
                    $("#tr_assinatura").hide();
                }
                $("#totalTrimestralidades").html(quantidade);
                
                //Carregar campos escondidos
                $("#nome_h").val(nomeSemScape);
                $("#endereco").val(endereco);
                $("#numero").val(numero);
                $("#cidade").val(cidade);
                $("#estado").val(estado);
                $("#cep").val(cep);
                $("#codigoAfiliacao_h").val(codigoAfiliacao);
                $("#tipoMembro").val(tipoMembro);
                //$("#seqCadast").val(seqCadast);
                $("#codigoVeiculo").val(0);
                $("#codigoContaContribuicao").val(data.CodContaContribuicao);
                
                var vlrWs = parseFloat(data.ValContribuicao);
                var total = 0;
                var textoContribuicao='';
                var textoValor='';
                var modalidadeRemessa='É necessário informar a GLP';
                
                switch(modalidade)
                {
                    case "C":
                        total = valorImpresso;
                        modalidadeRemessa='Impressa';
                        break;
                    case "W":
                        total = valorDigital;
                        modalidadeRemessa='Digital';
                        break;    
                    case "A":
                        total = valorAmbos;
                        modalidadeRemessa='Digital+Impressa';
                        break;    
                }
                $("#modalidadeRemessa").html(modalidadeRemessa);
                switch(contribuicao)
                {
                    case "1":
                        textoContribuicao='Mensalidades';
                        textoValor='Mensalidade';
                        total =  vlrWs/3;
                        break;
                    case "2":
                        textoContribuicao='Trimestralidades';
                        textoValor='Trimestralidade';
                        total = (vlrWs);
                        break;    
                    case "3":
                        textoContribuicao='Anuidades';
                        textoValor='Anuidade';
                        break;    
                }
                
                
                //alert(vlrWs);
                //alert(total);
                
                var totalGeral = (total*quantidade);
                var valorBoleto =  totalGeral+ data.ValTaxaAfiliacao;                
                //alert(valorBoleto);
                var totalG = total.toFixed(2).replace(".", ",");

                $("#valor").html("R$ " +totalG.toString());
                
                $("#valorTexto").html(textoValor);
                $("#contribuicaoTrim").html(textoContribuicao);
                $("#quantidadeTrim").val(quantidade);
                $("#token").val(data.token);
                
                var valorBoletoG =valorBoleto.toFixed(2).replace(".", ",");
                $("#valorContribuicao").html("R$ " + valorBoletoG.toString() );
                $("#valorBoleto").val(valorBoleto.toFixed(2));
                
                if(contribuicao==1)
                {
                    $("#tr_aviso").show();
                    $("#msg").html("<b>Lembramos que a disponibilização das monografias, em qualquer formato, somente acontecerá mediante a quitação da terceira mensalidade!</b>");
                }
                
                if(data.ValTaxaAfiliacao==0&&data.ValContribuicao==0)
                {
                    
                    if(data.DesMensag!="Consulta prévia para boleto - Ok")
                    {
                        if(data.DesMensag=="Não foi possível obter valores de afiliação (1) !")
                        {
                            if(contribuicao==1)
                            {    
                                $("#valor").html("R$ "+modalidadeMensalIndividual4.toString()+",00");//Tom - Modalidade 4 Mensal Individual
                                $("#valorContribuicao").html("R$ "+modalidadeMensalIndividual4.toString()+",00");//Tom - Modalidade 4 Mensal Individual
                                $("#valorBoleto").val(modalidadeMensalIndividual4);//Tom - Modalidade 4 Mensal Individual
                            }
                            if(contribuicao==2)
                            {    
                                $("#valor").html("R$ "+modalidadeTrimestralIndividualReal4.toString()+",00");//Tom - Modalidade 4 Trimestral Individual Real
                                $("#valorContribuicao").html("R$ "+modalidadeTrimestralIndividualReal4.toString()+",00");//Tom - Modalidade 4 Trimestral Individual Real
                                $("#valorBoleto").val(modalidadeTrimestralIndividualReal4);//Tom - Modalidade 4 Trimestral Individual Real
                            }
                            
                            $("#gerarBoleto").show();
                            $("#gerarBoletoDual").hide();
                        }else{    
                            $("#msg").html("");
                            $("#tr_valor_taxa_afiliacao").hide();
                            $("#tr_valor_contribuicao").hide();
                            $("#gerarBoleto").hide();
                            $("#tr_aviso").show();
                            $("#msg").html(data.DesMensag);
                        }
                    }       
                }   
                //alert(valorBoleto);
                var total=0;
                if(lote==300&&contribuicao==3&&tipoMembro==6)
                {
                    if(data.IdetipoFiliacao != "D")
                    {    
                        total=modalidadeAnualIndividualCFD4;//Tom - Modalidade 4 Anual Individual CFD
                        //alert('entrou aqui vai cinqueta');
                        $("#valor").html("R$ "+modalidadeAnualIndividualCFD4.toString()+",00");//Tom - Modalidade 4 Anual Individual CFD
                        $("#valorContribuicao").html("R$ "+modalidadeAnualIndividualCFD4.toString()+",00");
                        $("#valorBoleto").val(modalidadeAnualIndividualCFD4);
                        $("#gerarBoleto").show();
                        $("#gerarBoletoDual").hide();
                    }else{
                        total=modalidadeAnualDualCFD4;//Tom - Modalidade 4 Anual Dual
                        $("#valor").html("R$ "+modalidadeAnualDualCFD4.toString()+",00");//Tom - Modalidade 4 Anual Dual CFD
                        $("#valorContribuicao").html("R$ "+modalidadeAnualDualCFD4.toString()+",00");
                        $("#valorBoleto").val(modalidadeAnualDualCFD4);
                        $("#gerarBoleto").hide();
                        $("#gerarBoletoDual").show();
                    }
                    
                }
                if(lote<300&&contribuicao==3&&tipoMembro==6)
                {
                    if(data.IdetipoFiliacao != "D")
                    {
                        total=modalidadeAnualIndividualReal4+valorBoleto;//Tom - Modalidade 4 Anual Individual Real
                        $("#valor").html("R$ "+modalidadeAnualIndividualReal4.toString()+",00");//Tom - Modalidade 4 Anual Individual Real
                        $("#valorContribuicao").html("R$ "+total.toString()+",00");
                        $("#valorBoleto").val(total);
                    }else{
   
                        total=modalidadeAnualDualReal4;//Tom - Modalidade 4 Anual Dual
                        
                        $("#valor").html("R$ "+modalidadeAnualDualReal4.toString()+",00");//Tom - Modalidade 4 Anual Dual
                        $("#valorContribuicao").html("R$ "+total.toString()+",00");
                        $("#valorBoleto").val(total);
                    }
                }
                if(lote==300&&tipoMembro==6&&(contribuicao==1||contribuicao==2))
                {
                    $("#valor").html("R$ 0,00");
                    $("#valorContribuicao").html("R$ 0,00");
                    $("#valorBoleto").val(0);
                    $("#gerarBoleto").hide();
                    $("#gerarBoletoDual").hide();
                    $("#tr_aviso").show();
                    $("#msg").html("<b>Identificamos que o seu cadastro tem o benefício da anuidade TOM, portanto selecione na tela anterior a opção de boleto (anuidade)!</b>");
                }    
                /*
                if (data.IdetipoFiliacao == "D")
                {
                    //alert(seqCadast);
                    //alert(principal);
                    if(seqCadast!=principal)
                    {
                        $("#seqCadast").val(principal);
                    } 
                }
                  */ 
                 //alert(membroSelecionado);
                $("#nome").val(membroSelecionado);
                $("#codigoAfiliacao").val(codigoAfiliacao); 
                
                $('#mySeeDetalhes').modal({
                });
            } else {
                swal({title: "Aviso!", text: "Não é possível Carregar os Dados da Prévia da Geração do Boleto!", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao cadastrar previa do boleto');
        }
    });
}

function gerarBoleto()
{
    //alert('teste');
    var token = $("#token").val();
    var nomeMembro = $("#nome_h").val();
    var codigoAfiliacao = $("#codigoAfiliacao_h").val();
    var tipoMembro = $("#tipo").val();
    var quantidade = $("#quantidade").val();
    var valorBoleto = $("#valorBoleto").val();
    var seqCadast = $("#seqCadast").val();
    //var codigoVeiculo = $("#codigoVeiculo").val();
    //var codigoContaContribuicao = $("#codigoContaContribuicao").val();
    
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fNomCliente.trim());
            if (data.result[0].fields.fNomCliente.trim() != "")
            {
                
                //var seqCadast = data.result[0].fields.fSeqCadastPrincipal;
                var endereco = data.result[0].fields.fNomLogradouro.trim();
                var numero = data.result[0].fields.fNumEndereco;
                var cidade = data.result[0].fields.fNomCidade;
                var estado = data.result[0].fields.fSigUf;
                var cep=data.result[0].fields.fCodCepEndereco.trim();
                
                window.open(encodeURI('impressao/gerarBoleto.php?token=' + token
                +'&codigoAfiliacao='+codigoAfiliacao
                +'&tipoMembro='+tipoMembro
                +'&seqCadast='+seqCadast
                +'&quantidade='+quantidade
                +'&nome='+nomeMembro
                +'&endereco='+endereco
                +'&numero='+numero
                +'&cidade='+cidade
                +'&estado='+estado
                +'&cep='+cep
                +'&valorBoleto='+valorBoleto)
                , '_blank'
                );
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao consultar o membro');
        }
    });
}

function validaRelatorioGrandeConselheiro()
{
    if ($("#regiao").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione uma Região", type: "warning", confirmButtonColor: "#1ab394"});
        $("#regiao").focus();
        return false;
    }

    if ($("#semestre").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione o semestre ao qual o relatório pertence", type: "warning", confirmButtonColor: "#1ab394"});
        $("#semestre").focus();
        return false;
    }

    if ($("#ano").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o ano do relatório", type: "warning", confirmButtonColor: "#1ab394"});
        $("#ano").focus();
        return false;
    }
/*
    if ($("#pergunta1").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 01", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    var checado = $('.supervisao').is(':checked');
    if (!checado) {
        swal({title: "Aviso!", text: "Marque pelo menos um Organismo Afiliado na pergunta 02", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta3").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 03", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta4").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 04", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta5").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 05", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta6").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 06", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta7").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 07", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta8").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 08", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta9").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 09", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta10").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 10", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta11").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 11", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta12").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 12", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    var checado2 = $('.monitores').is(':checked');
    if (!checado2) {
        swal({title: "Aviso!", text: "Marque pelo menos um Monitor na pergunta 13", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta14").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 14", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta15").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 15", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta16").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 16", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta17").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 17", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta18").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 18", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta19").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 19", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta20").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 20", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta21").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 21", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta22").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 22", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta23").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 23", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta24").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 24", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta25").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 25", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta26").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 26", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta27").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 27", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta28").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 28", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta29").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 29", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta30").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 30", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta31").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 31", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta32").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 32", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta33").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 33", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta34").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 34", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta35").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 35", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta36").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 36", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    if ($("#pergunta37").val() == 0)
    {
        swal({title: "Aviso!", text: "Preencha a resposta da pergunta 37", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
*/
    return true;
}

function fechaOutrasAbasRelatorioGrandeConselheiro(cod)
{
    for (var i = 1; i <= 37; i++)
    {
        if (i != cod)
        {
            document.getElementById("collapse" + i).classList.remove("in");
        }
    }
}

function listaUploadRelatorioGestao(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRelatorioGestao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRelatorioGestao(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRelatorioGestao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRelatorioGestao(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function abrirPopupRelatorioOficiais(idOrganismoAfiliado, tipo)
{
    if (validaRelatorioOficiais())
    {
        //Tipo Funcao
        var str = "";
        if($("#oficiaisAdministrativos").is(":checked"))
        {    
            str += ",200";
        }
        if($("#dignitarios").is(":checked"))
        {    
            str += ",100,800";
        }
        if($("#oficiaisRitualisticos").is(":checked"))
        {    
            str += ",300";
        }
        if($("#oficiaisComissao").is(":checked"))
        {    
            str += ",600";
        }
        if($("#oficiaisIniciaticos").is(":checked"))
        {    
            str += ",400,500";
        }
        
        //Função
        var strFuncao="";
        inputs = document.getElementsByName('funcao');///recebe todos os inputs do documento
        
        for(var x=0;x<inputs.length;x++){
             if(inputs[x].checked==true)
             {
                 strFuncao += ","+inputs[x].value;
             }    
        }
        
        window.open('impressao/relatorioOficiais.php?tipo=' + tipo 
                + '&idOrganismoAfiliado=' + idOrganismoAfiliado
                + '&str=' + str
                + '&strFuncao=' + strFuncao
                + '&terminoMandato=' + document.getElementById('terminoMandato').value
        , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }
}
function abrirPopupRelatorioOficiaisMailistInovad(idRegiao,idOrganismoAfiliado, tipo)
{
    if (validaRelatorioOficiaisMailist())
    {
        //Tipo Funcao
        var str = "";
        if($("#oficiaisAdministrativos").is(":checked"))
        {    
            str += ",200";
        }
        if($("#dignitarios").is(":checked"))
        {    
            str += ",100,800";
        }
        if($("#oficiaisRitualisticos").is(":checked"))
        {    
            str += ",300";
        }
        if($("#oficiaisComissao").is(":checked"))
        {    
            str += ",600";
        }
        if($("#oficiaisIniciaticos").is(":checked"))
        {    
            str += ",400,500";
        }
        
        //Função
        var strFuncao="";
        inputs = document.getElementsByName('funcao');///recebe todos os inputs do documento
        
        for(var x=0;x<inputs.length;x++){
             if(inputs[x].checked==true)
             {
                 strFuncao += ","+inputs[x].value;
             }    
        }
        
        window.open('impressao/relatorioOficiaisMailistInovad.php?tipo=' + tipo
                + '&idRegiao=' + idRegiao
                + '&idOrganismoAfiliado=' + idOrganismoAfiliado
                + '&str=' + str
                + '&strFuncao=' + strFuncao
                + '&terminoMandato=' + document.getElementById('terminoMandato').value
        , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }
}

function abrirPopupRelatorioOficiaisCertificadoRetrato(idRegiao,idOrganismoAfiliado, tipo)
{
    
    if (validaRelatorioOficiais())
    {
        //Tipo Funcao
        var str = "";
        if($("#oficiaisAdministrativos").is(":checked"))
        {    
            str += ",200";
        }
        if($("#dignitarios").is(":checked"))
        {    
            str += ",100,800";
        }
        if($("#oficiaisRitualisticos").is(":checked"))
        {    
            str += ",300";
        }
        if($("#oficiaisComissao").is(":checked"))
        {    
            str += ",600";
        }
        if($("#oficiaisIniciaticos").is(":checked"))
        {    
            str += ",400,500";
        }
        
        //Função
        var strFuncao="";
        inputs = document.getElementsByName('funcao');///recebe todos os inputs do documento
        
        for(var x=0;x<inputs.length;x++){
             if(inputs[x].checked==true)
             {
                 strFuncao += ","+inputs[x].value;
             }    
        }
        
        window.open('impressao/certificadoOficiaisRetrato.php?tipo=' + tipo 
                + '&idOrganismoAfiliado=' + idOrganismoAfiliado
                + '&idRegiao=' + idRegiao
                + '&str=' + str
                + '&strFuncao=' + strFuncao
                + '&terminoMandato=' + document.getElementById('terminoMandato').value
                + '&anoRosacruz=' + document.getElementById('anoRosacruz').value
                + '&dataEmissao=' + document.getElementById('dataEmissao').value
        , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }
}

function abrirPopupRelatorioOficiaisCertificadoPaisagem(idRegiao,idOrganismoAfiliado, tipo, anoRosacruz)
{
    if (validaRelatorioOficiais())
    {
        //Tipo Funcao
        var str = "";
        if($("#oficiaisAdministrativos").is(":checked"))
        {    
            str += ",200";
        }
        if($("#dignitarios").is(":checked"))
        {    
            str += ",100,800";
        }
        if($("#oficiaisRitualisticos").is(":checked"))
        {    
            str += ",300";
        }
        if($("#oficiaisComissao").is(":checked"))
        {    
            str += ",600";
        }
        if($("#oficiaisIniciaticos").is(":checked"))
        {    
            str += ",400,500";
        }
        
        //Função
        var strFuncao="";
        inputs = document.getElementsByName('funcao');///recebe todos os inputs do documento
        
        for(var x=0;x<inputs.length;x++){
             if(inputs[x].checked==true)
             {
                 strFuncao += ","+inputs[x].value;
             }    
        }
        
        window.open('impressao/certificadoOficiaisPaisagem2.php?tipo=' + tipo 
                + '&idOrganismoAfiliado=' + idOrganismoAfiliado
                + '&idRegiao=' + idRegiao
                + '&str=' + str
                + '&strFuncao=' + strFuncao
                + '&terminoMandato=' + document.getElementById('terminoMandato').value
                + '&anoRosacruz=' + document.getElementById('anoRosacruz').value
                + '&dataEmissao=' + document.getElementById('dataEmissao').value
        , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }
}

function abrirPopupRelatorioOficiaisEtiquetas(idRegiao,idOrganismoAfiliado, tipo)
{
    if (validaRelatorioOficiais())
    {
        //Tipo Funcao
        var str = "";
        if($("#oficiaisAdministrativos").is(":checked"))
        {    
            str += ",200";
        }
        if($("#dignitarios").is(":checked"))
        {    
            str += ",100,800";
        }
        if($("#oficiaisRitualisticos").is(":checked"))
        {    
            str += ",300";
        }
        if($("#oficiaisComissao").is(":checked"))
        {    
            str += ",600";
        }
        if($("#oficiaisIniciaticos").is(":checked"))
        {    
            str += ",400,500";
        }
        
        //Função
        var strFuncao="";
        inputs = document.getElementsByName('funcao');///recebe todos os inputs do documento
        
        for(var x=0;x<inputs.length;x++){
             if(inputs[x].checked==true)
             {
                 strFuncao += ","+inputs[x].value;
             }    
        }
        
        window.open('impressao/etiquetasOficiais.php?tipo=' + tipo 
                + '&idOrganismoAfiliado=' + idOrganismoAfiliado
                + '&idRegiao=' + idRegiao
                + '&str=' + str
                + '&strFuncao=' + strFuncao
                + '&terminoMandato=' + document.getElementById('terminoMandato').value
        , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }
}

function validaRelatorioOficiais()
{
    if ($("#fk_idRegiaoOrdemRosacruz").val() == 0)
    {
        swal("Aviso", "Selecione a Região!", "error");
        $("#fk_idRegiaoOrdemRosacruz").focus();
        return false;
    }
    
    if ($("#fk_idOrganismoAfiliado").val() == "Todos")
    {
        swal("Aviso", "Selecione o Organismo Afiliado!", "error");
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    
    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal("Aviso", "Selecione o Organismo Afiliado!", "error");
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }

    if ($("#tipo").val() == "0")
    {
        swal("Aviso", "Informe o tipo!", "error");
        $("#tipo").focus();
        return false;
    }
    
    //Função
    var checado=false;
    inputs = document.getElementsByName('funcao');///recebe todos os inputs do documento

    for(var x=0;x<inputs.length;x++){
         if(inputs[x].checked==true)
         {
             checado=true;
         }    
    }   
    if(!checado)
    {
        swal("Aviso", "Escolha um tipo de função e selecione as funções que aparecerão no relatório!", "error");
        return false;
    }    
    return true;
}


function validaRelatorioOficiaisMailist()
{
    /*
    if ($("#fk_idRegiaoOrdemRosacruz").val() == 0)
    {
        swal("Aviso", "Selecione a Região!", "error");
        $("#fk_idRegiaoOrdemRosacruz").focus();
        return false;
    }
    
    if ($("#fk_idOrganismoAfiliado").val() == "Selecione...")
    {
        swal("Aviso", "Selecione o Organismo Afiliado!", "error");
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    
    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal("Aviso", "Selecione o Organismo Afiliado!", "error");
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    */
    if ($("#tipo").val() == "0")
    {
        swal("Aviso", "Informe o tipo!", "error");
        $("#tipo").focus();
        return false;
    }
    
    //Função
    var checado=false;
    inputs = document.getElementsByName('funcao');///recebe todos os inputs do documento

    for(var x=0;x<inputs.length;x++){
         if(inputs[x].checked==true)
         {
             checado=true;
         }    
    }   
    if(!checado)
    {
        swal("Aviso", "Escolha um tipo de função e selecione as funções que aparecerão no relatório!", "error");
        return false;
    }    
    return true;
}
function excluirQuitacaoDivida(idExcluir)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse registro!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirQuitacaoDivida.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        location.href = '?corpo=buscaQuitacaoDivida&excluir=1';
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o registro de quitação :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function retornaDadosMembroNoTermoVoluntariado()
{
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var nomeMembro = $("#nomeVoluntario").val();
    var tipoMembro = '1';

    //console.log("retornaDados() --- Cód. Afiliação: "+codigoAfiliacao+" Nome: "+nomeMembro+" Tipo: "+tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);

    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#nomeVoluntario").val(data.result[0].fields.fNomCliente.trim());
            $("#logradouro").val(data.result[0].fields.fNomLogradouro.trim());
            $("#cidade").val(data.result[0].fields.fNomCidade.trim());
            $("#cep").val(data.result[0].fields.fCodCepEndereco.trim());
            $("#profissao").val(data.result[0].fields.fDesFormacao.trim());
            $("#rg").val(data.result[0].fields.fCodRg.trim());
            $("#cpf").val(data.result[0].fields.fCodCpf.trim());
            $("#email").val(data.result[0].fields.fDesEmail.trim());
            $("#numero").val(data.result[0].fields.fNumEndereco);
            $("#complemento").val(data.result[0].fields.fDesComplementoEndereco);
            $("#bairro").val(data.result[0].fields.fNomBairro.trim());
            $("#uf").val(data.result[0].fields.fSigUf.trim());
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

function listaUploadTermoVoluntariado(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadTermoVoluntariado.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadTermoVoluntariado(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadTermoVoluntariado.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadTermoVoluntariado(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function listaUploadJuramento(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadJuramento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUploadJuramento").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadJuramento(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadJuramento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadJuramento(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function validaDataZerada(campo)
{
    if (campo == "00/00/0000")
    {
        swal({title: "Aviso!", text: "Preencha corretamente o campo data!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    return true;
}

function atualizarCadastroMembroAMORC()
{
    var SeqCadastAtendimento = $('#SeqCadastAtendimento').val();
    var CodUsuario = $('#CodUsuario').val();
    var SeqCadast = $('#SeqCadast').val();
    var CodMembro = $('#CodigoAfiliacao').val();
    var TipoMembro = $('#TipoMembro').val();
    var IdeSexo = $("input[name='IdeSexo']:checked").val();
    var NomMembro = $('#NomMembro').val();
    var NomConjuge = $('#NomConjuge').val();
    var DesLograd = $('#DesLograd').val();
    var NumEndere = $('#NumEndere').val();
    var DesCompleLograd = $('#DesCompleLograd').val();
    var NomBairro = $('#NomBairro').val();
    var NomLocali = $('#NomLocali').val();
    var SigUf = $('#SigUf').val();
    var CodCep = $('#CodCep').val();
    var NumCaixaPostal = $('#NumCaixaPostal').val();
    var SigPais = $('#SigPais').val();
    var DesEmail = $('#DesEmail').val();
    var DatNascim = $('#DatNascim').val();
    var IdeEstadoCivil = $('#fk_idEstadoCivil').val();
    var CodRg = $('#CodRg').val();
    var IdeEnderecoCorrespondencia = $("input[name='IdeEnderecoCorrespondencia']:checked").val();
    var CodCepCaixaPostal = $('#CodCepCaixaPostal').val();
    var TipoGrauInstrucao = $('#fk_idGrauInstrucao').val();
    var fk_idFormacao = $("#fk_idFormacao").val();
    var fk_idOcupacao = $("#fk_idOcupacao").val();
    var fk_idEspecializacao = $("#fk_idEspecializacao").val();
    var fk_idTipoMoeda = $("#fk_idTipoMoeda").val();
    
    var NumTelefoFixo = $("#NumTelefoFixo").val();
    var NumCelula = $("#NumCelula").val();
    var NumOutroTelefo = $("#NumOutroTelefo").val();
    var DesOutroTelefo = $("#DesOutroTelefo").val();
    
    var str="";
    if($("#telefone").is(":checked"))
    {    
        str += "S";
    }else{
        str += "N";
    }
    if($("#carta").is(":checked"))
    {    
        str += "S";
    }else{
        str += "N";
    }
    if($("#email").is(":checked"))
    {    
        str += "S";
    }else{
        str += "N";
    }
    if($("#sms").is(":checked"))
    {    
        str += "S";
    }else{
        str += "N";
    }

    var AceitoContato = str;
    
    str="";
    if($("#manha").is(":checked"))
    {    
        str += "S";
    }else{
        str += "N";
    }
    if($("#tarde").is(":checked"))
    {    
        str += "S";
    }else{
        str += "N";
    }
    if($("#noite").is(":checked"))
    {    
        str += "S";
    }else{
        str += "N";
    }
    var IdePeriodContatIdeal = str; 
    
    var IdeFormaContatIdeal = $("input[name='IdeFormaContatIdeal']:checked").val();
    var possuiComputador = $("input[name='possuiComputador']:checked").val();
    var possuiInternet = $("input[name='possuiInternet']:checked").val();
    //alert("possui computador antes da chamada:"+possuiComputador);
    
    str="";
    if($("#administrativa").is(":checked"))
    {    
        str += " 1";
    }else{
        str += "";
    }
    if($("#ritualistica").is(":checked"))
    {    
        str += " 2";
    }else{
        str += "";
    }
    if($("#iniciatica").is(":checked"))
    {    
        str += " 3";
    }else{
        str += "";
    }
    if($("#cultural").is(":checked"))
    {    
        str += " 4";
    }else{
        str += "";
    }
    //alert(str);
    var desejaColaborar = str.trim();
    
    var experienciaProfissional = $("#experienciaProfissional").val();
   
    var membroURCI = $("input[name='membroURCI']:checked").val();
    
    //validação
    if(NomMembro=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo Nome", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    } 
    if(CodCep=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo CEP", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    if(DesLograd=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo Logradouro", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    if(NumEndere=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo Número do Endereço", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    if(NomBairro=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo Bairro", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    if(NomLocali=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo Cidade", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    if(SigUf=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo UF", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    if(SigPais=="")
    {
        swal({title: "Aviso!", text: "Preencha o campo Sigla do Pais", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    if(fk_idTipoMoeda=="0")
    {
        swal({title: "Aviso!", text: "Preencha o campo Sigla do Pais", type: "error", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    var IdeAmorcgDigita="";
    if($("#IdeAmorcgDigita").is(":checked"))
    {    
        IdeAmorcgDigita += "N";
    }else{
        IdeAmorcgDigita += "S";
    }
    
    var IdeForumrDigita="";
    if($("#IdeForumrDigita").is(":checked"))
    {    
        IdeForumrDigita += "N";
    }else{
        IdeForumrDigita += "S";
    }
    
    var IdeOrosacDigita="";
    if($("#IdeOrosacDigita").is(":checked"))
    {    
        IdeOrosacDigita += "N";
    }else{
        IdeOrosacDigita += "S";
    }
    
    var IdeOpentaDigita="";
    if($("#IdeOpentaDigita").is(":checked"))
    {    
        IdeOpentaDigita += "N";
    }else{
        IdeOpentaDigita += "S";
    }
    
    var IdeOGGDigita="";
    if($("#IdeOGGDigita").is(":checked"))
    {    
        IdeOGGDigita += "N";
    }else{
        IdeOGGDigita += "S";
    }
    
    $.ajax({
        type: "post",
        url: "js/ajax/atualizarDados.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {SeqCadastAtendimento:SeqCadastAtendimento,
            CodUsuario: CodUsuario,
            SeqCadast:SeqCadast,
            CodMembro: CodMembro,
            TipoMembro: TipoMembro,
            NomMembro: NomMembro,
            IdeSexo: IdeSexo,
            NomConjuge: NomConjuge,
            DesLograd: DesLograd,
            NumEndere: NumEndere,
            DesCompleLograd: DesCompleLograd,
            NomBairro: NomBairro,
            NomLocali: NomLocali,
            SigUf: SigUf,
            CodCep: CodCep,
            CodRg:CodRg,
            NumCaixaPostal: NumCaixaPostal,
            CodCepCaixaPostal:CodCepCaixaPostal,
            IdeEnderecoCorrespondencia:IdeEnderecoCorrespondencia,
            SigPais: SigPais,
            DesEmail: DesEmail,
            DatNascim: DatNascim,
            IdeEstadoCivil: IdeEstadoCivil,
            TipoGrauInstrucao: TipoGrauInstrucao,
            fk_idEspecializacao: fk_idEspecializacao,
            fk_idFormacao: fk_idFormacao,
            fk_idOcupacao: fk_idOcupacao,
            fk_idTipoMoeda: fk_idTipoMoeda,
            NumTelefoFixo:NumTelefoFixo,
            NumCelula:NumCelula,
            NumOutroTelefo:NumOutroTelefo,
            DesOutroTelefo:DesOutroTelefo,
            IdeAceitoContat:AceitoContato,
            IdePeriodContat:IdePeriodContatIdeal,
            IdeFormaContatIdeal:IdeFormaContatIdeal,
            possuiComputador:possuiComputador,
            possuiInternet:possuiInternet,
            desejaColaborar:desejaColaborar,
            experienciaProfissional:experienciaProfissional,
            membroURCI:membroURCI,
            IdeAmorcgDigita:IdeAmorcgDigita,
            IdeForumrDigita:IdeForumrDigita,
            IdeOrosacDigita:IdeOrosacDigita,
            IdeOpentaDigita:IdeOpentaDigita,
            IdeOGGDigita:IdeOGGDigita
        }, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            alert(data);
            if(data.status==1)
            {    
                location.href='?corpo=buscaMembroAMORC&salvo=1';
            }else{
                alert('Erro ao atualizar as informacoes do membro');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Erro. Contacte o Administrador de TI');
        }
    });
}

function mostrarLogradouro(i, seqCadast, endereco, usuario)
{
    $.ajax({
        type: "post",
        url: "js/ajax/mostrarLogradouro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: seqCadast, endereco: endereco, usuario: usuario}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            if (data.status == 1)
            {
                $('#linkLogradouro' + i).hide();
                $('#logradouroQuebrado' + i).hide();
                $('#logradouroInteiro' + i).show();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao mostrar logradouro');
        }
    });

}

function mostrarTelefone(i, seqCadast, telefone, usuario)
{
    $.ajax({
        type: "post",
        url: "js/ajax/mostrarTelefone.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: seqCadast, telefone: telefone, usuario: usuario}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            if (data.status == 1)
            {
                $('#linkTelefone' + i).hide();
                $('#telefoneQuebrado' + i).hide();
                $('#telefoneInteiro' + i).show();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao mostrar logradouro');
        }
    });
}

function excluirParticipanteAtaReuniaoMensal(idAta)
{
    //alert("idAta:"+idAta);
    var arr = $("#ata_oa_mensal_oficiais").val();

    if (arr == null)
    {
        swal({title: "Aviso!", text: "Escolha um participante na lista para ser excluído", type: "error", confirmButtonColor: "#1ab394"});
    } else {
        swal({
            title: "Tem certeza que deseja excluir?",
            text: "Com essa ação você excluirá definitivamente esse participante da ata!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, exclua!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: false},
        function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: "post",
                    url: "js/ajax/excluirParticipanteAtaReuniaoMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {arr: arr, idAtaReuniaoMensal: idAta}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                    success: function (data) {
                        //alert(data);
                        if (data.status == 1)
                        {
                            location.href = '?corpo=alteraAtaReuniaoMensal&idata_reuniao_mensal=' + idAta + '&excluido=1';
                        } else {
                            if (data.status == 0)
                            {
                                $('#ata_oa_mensal_oficiais option:selected').remove();
                                swal({title: "Sucesso!", text: "Participante(s) excluído(s) com sucesso da lista!", type: "success", confirmButtonColor: "#1ab394"});
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro ao excluir o participante.');
                    }
                });
            } else {
                swal({title: "Cancelado", text: "Nós não excluímos o participante :)", type: "error", confirmButtonColor: "#1ab394"});
            }
        });

    }
}

function abrirPopUpAtividadeIniciaticaMembrosParaAssinatura(idAtividadeIniciatica,idOrganismo)
{
    window.open('impressao/atividadeIniciaticaMembrosAssinatura.php?idAtividadeIniciatica='+idAtividadeIniciatica+'&idOrganismo='+idOrganismo,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function listaUploadMembrosAtividadeIniciatica(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadMembrosAtividadeIniciatica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadMembrosAtividadeIniciatica(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadMembrosAtividadeIniciatica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadMembrosAtividadeIniciatica(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function listaUploadOficiaisAtividadeIniciatica(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadOficiaisAtividadeIniciatica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {            
            $("#listaUploadOficiais").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadOficiaisAtividadeIniciatica(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadOficiaisAtividadeIniciatica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadOficiaisAtividadeIniciatica(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function listaUploadRelatorioClasseArtesaos(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRelatorioClasseArtesaos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRelatorioClasseArtesaos(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRelatorioClasseArtesaos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRelatorioClasseArtesaos(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function excluirMembroRelatorioClasseArtesaos(id)
{
    //alert("idAta:"+idAta);
    var arr = $("#membros").val();

    if (arr == null)
    {
        swal({title: "Aviso!", text: "Escolha um membro na lista para ser excluído", type: "error", confirmButtonColor: "#1ab394"});
    } else {
        swal({
            title: "Tem certeza que deseja excluir?",
            text: "Com essa ação você excluirá definitivamente esse membro do relatório!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, exclua!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: false},
        function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: "post",
                    url: "js/ajax/excluirMembroRelatorioClasseArtesaos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {arr: arr, idRelatorioClasseArtesaos: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                    success: function (data) {
                        //alert(data);
                        if (data.status == 1)
                        {
                            //location.href = '?corpo=alteraRelatorioClasseArtesaos&idrelatorio=' + id + '&excluido=1';
                            $('#membros option:selected').remove();
                            swal({title: "Sucesso!", text: "Membro(s) excluído(s) com sucesso da lista!", type: "success", confirmButtonColor: "#1ab394"});
                        } else {
                            if (data.status == 0)
                            {
                                $('#membros option:selected').remove();
                                swal({title: "Sucesso!", text: "Membro(s) excluído(s) com sucesso da lista!", type: "success", confirmButtonColor: "#1ab394"});
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro ao excluir o membro.');
                    }
                });
            } else {
                swal({title: "Cancelado", text: "Nós não excluímos o membro :)", type: "error", confirmButtonColor: "#1ab394"});
            }
        });

    }
}

function limparSaldoInicial(id)
{
    swal({
        title: "Tem certeza que deseja limpar o saldo?",
        text: "Com essa ação você excluirá definitivamente esse valor do relatório! Você possui o novo Saldo Inicial deste organismo?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, limpar pois possuo o novo saldo!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: true,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {

            $("#loadSI").html("<img src='img/loading_trans.gif' width='70'>");

            var usuario = $("#usuario").val();

            $.ajax({
                type: "post",
                async: true,
                url: "js/ajax/limparSaldoInicial.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {fk_idOrganismoAfiliado: id, usuario: usuario}, // exemplo: {codigo:125, nome:"Joaozinho"}
                success: function (data) {
                    if(data.status==1)
                    {
                        $("#loadSI").html("");
                        location.href = '?corpo=buscaSaldoInicial&limpou=1';
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('Erro ao limpar o saldo.');
                }
            });
        } else {
            swal({title: "Cancelado", text: "O saldo inicial não foi excluido!", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
}

function validaCadastroColumbaNaoInstalada()
{
    if ($("#nomeColumba").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o nome da Columba", type: "warning", confirmButtonColor: "#1ab394"});
        $("#nomeColumba").focus();
        return false;
    }

    if ($("#codigoAfiliacao").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o código de afiliação", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codigoAfiliacao").focus();
        return false;
    }

    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }

    if ($("#seqCadastMembro").val() == "")
    {
        swal({title: "Aviso!", text: "O código interno não foi encontrado. Faça a busca novamente e tente salvar novamente", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }

    return true;
}

function abrirPopupRelatorioTrimestralColumba(trimestreAtual, anoAtual, idOrganismoAfiliado)
{
    window.open('impressao/relatorioTrimestralColumba.php?trimestreAtual=' + trimestreAtual + '&anoAtual=' + anoAtual + '&idOrganismoAfiliado=' + idOrganismoAfiliado , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopupRelatorioAtividadeTrimestralColumba(trimestreAtual, anoAtual, idOrganismoAfiliado)
{
    window.open('impressao/relatorioAtividadeTrimestralColumba.php?trimestreAtual=' + trimestreAtual + '&anoAtual=' + anoAtual + '&idOrganismoAfiliado=' + idOrganismoAfiliado , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=1000,height=550');
}

function listaUploadRelatorioTrimestralColumba(trimestreAtual, anoAtual, idOrganismoAfiliado)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRelatorioTrimestralColumba.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {trimestreAtual: trimestreAtual, anoAtual: anoAtual, idOrganismoAfiliado: idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRelatorioTrimestralColumbaAssinado(idExcluir, trimestreAtual, anoAtual, idOrganismoAfiliado)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRelatorioTrimestralColumba.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRelatorioTrimestralColumba(trimestreAtual, anoAtual, idOrganismoAfiliado);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
    //}else{
    //	return false;
    //}
}

function listaUploadRelatorioAtividadeTrimestralColumba(trimestreAtual, anoAtual, idOrganismoAfiliado)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRelatorioAtividadeTrimestralColumba.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {trimestreAtual: trimestreAtual, anoAtual: anoAtual, idOrganismoAfiliado: idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRelatorioAtividadeTrimestralColumbaAssinado(idExcluir, trimestreAtual, anoAtual, idOrganismoAfiliado)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRelatorioAtividadeTrimestralColumba.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRelatorioAtividadeTrimestralColumba(trimestreAtual, anoAtual, idOrganismoAfiliado);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
    //}else{
    //	return false;
    //}
}

function validaCadastroAtividadeColumba()
{
    if ($("#codAfiliacaoColumba").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o código de afiliação da columba atuante", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codAfiliacaoColumba").focus();
        return false;
    }
    if ($("#nomeColumba").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o nome da columba atuante", type: "warning", confirmButtonColor: "#1ab394"});
        $("#nomeColumba").focus();
        return false;
    }

    if ($("#h_seqCadastColumba").val() == "")
    {
        swal({title: "Aviso!", text: "Selecione uma columba através da pesquisa pelo código de afiliação e nome", type: "warning", confirmButtonColor: "#1ab394"});
        $("#nomeColumba").focus();
        return false;
    }

    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    
    if ($("#seqCadastMembro").val() == "0")
    {
        swal({title: "Aviso!", text: "Selecione a Columba Atuante", type: "warning", confirmButtonColor: "#1ab394"});
        $("#seqCadastMembro").focus();
        return false;
    }

    if ($("#trimestre").val() == "0")
    {
        swal({title: "Aviso!", text: "Selecione o trimestre", type: "warning", confirmButtonColor: "#1ab394"});
        $("#seqCadastMembro").focus();
        return false;
    }
    
    if ($("#ano").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o Ano", type: "warning", confirmButtonColor: "#1ab394"});
        $("#seqCadastMembro").focus();
        return false;
    }
    
    if ($("#avaliacaoCoordenadora").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione a avaliação", type: "warning", confirmButtonColor: "#1ab394"});
        $("#avaliacaoCoordenadora").focus();
        return false;
    }
   
    return true;
}

function validaCadastroAgendaAtividade()
{
    
    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    
    if ($("#fk_idTipoAtividadeEstatuto").val() == "0")
    {
        swal({title: "Aviso!", text: "Selecione um Tipo de Atividade", type: "warning", confirmButtonColor: "#1ab394"});
        $("#tipoAtividade").focus();
        return false;
    }
    
    if ($("#fk_idPublico").val() == "0")
    {
        swal({title: "Aviso!", text: "Selecione o Público da Atividade", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idPublico").focus();
        return false;
    }

    if ($("#anoCompetencia").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o Ano de Competência", type: "warning", confirmButtonColor: "#1ab394"});
        $("#anoCompetencia").focus();
        return false;
    }
    
   
    
    if (document.formulario.recorrente[0].checked == false&&document.formulario.recorrente[1].checked == false)
    {
        swal({title: "Aviso!", text: "Marque se a atividade é recorrente", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }else{    
    
        if (document.formulario.recorrente[0].checked == false)
        {
            if ($("#diasHorarios").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha os dias e horários que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#diasHorarios").focus();
                return false;
            }
        }else{
            if ($("#dataInicial").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a data inicial que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#dataInicial").focus();
                return false;
            }
            if ($("#horaInicial").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a hora inicial que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#horaInicial").focus();
                return false;
            }
            if ($("#dataFinal").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a data final que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#dataFinal").focus();
                return false;
            }
            if ($("#horaFinal").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a hora final que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#horaFinal").focus();
                return false;
            }
        }
    }
    
    return true;
}

function validaEdicaoAgendaAtividade()
{
    if ($("#fk_idTipoAtividadeEstatuto").val() == "0")
    {
        swal({title: "Aviso!", text: "Selecione um Tipo de Atividade", type: "warning", confirmButtonColor: "#1ab394"});
        $("#tipoAtividade").focus();
        return false;
    }
    
    if ($("#fk_idPublico").val() == "0")
    {
        swal({title: "Aviso!", text: "Selecione o Público da Atividade", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idPublico").focus();
        return false;
    }

    if ($("#anoCompetencia").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o Ano de Competência", type: "warning", confirmButtonColor: "#1ab394"});
        $("#anoCompetencia").focus();
        return false;
    }
    
    
    
    if (document.formulario.recorrente[0].checked == false&&document.formulario.recorrente[1].checked == false)
    {
        swal({title: "Aviso!", text: "Marque se a atividade é recorrente", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }else{    
    
        if (document.formulario.recorrente[0].checked == false)
        {
            if ($("#diasHorarios").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha os dias e horários que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#diasHorarios").focus();
                return false;
            }
        }else{
            if ($("#dataInicial").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a data inicial que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#dataInicial").focus();
                return false;
            }
            if ($("#horaInicial").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a hora inicial que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#horaInicial").focus();
                return false;
            }
            if ($("#dataFinal").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a data final que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#dataFinal").focus();
                return false;
            }
            if ($("#horaFinal").val() == "")
            {
                swal({title: "Aviso!", text: "Preencha a hora final que a Atividade acontece", type: "warning", confirmButtonColor: "#1ab394"});
                $("#horaFinal").focus();
                return false;
            }
        }
    }
    
    return true;
    
}

function abrirPopUpAgendaAtividadeTodas()
{
    window.open('impressao/agendaAtividadeTodas.php', 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopUpAgendaAtividade(id)
{
    window.open('impressao/agendaAtividade.php?id=' + id, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function atualizarFuncoesDoUsuario(seqCadast,nome)
{
    //alert(seqCadast);
	$.ajax({
        type: "post",
        url: "js/ajax/atualizarFuncoesDoUsuario.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
        	seqCadast:seqCadast
                }, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            //alert(retorno);
            location.href='?corpo=buscaUsuario&funcoesAtualizadas=1&nome='+nome;    	
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function alteraStatusBaixaFuncaoPendente(seqCadast,status,funcao,siglaOA,dataEntrada,dataTerminoMandato)
{
    //alert(seqCadast);
    //alert(status);
	$.ajax({
        type: "post",
        url: "js/ajax/alteraStatusBaixaFuncaoPendente.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
        	seqCadast:seqCadast,
                status:status,
                funcao:funcao,
                siglaOA:siglaOA,
                dataEntrada:dataEntrada,
                dataTerminoMandato:dataTerminoMandato
                }, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            //alert(retorno);
            if(retorno.status=="1")
            {    
                location.href='?corpo=buscaBaixaFuncaoPendente&salvo=1';    	
            }
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function alteraStatusBaixaFuncaoPendenteParaNaoVerificado(seqCadast,status,funcao,siglaOA,dataEntrada,dataTerminoMandato)
{
    //alert(seqCadast);
    //alert(status);
	$.ajax({
        type: "post",
        url: "js/ajax/alteraStatusBaixaFuncaoPendente.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
        	seqCadast:seqCadast,
                status:status,
                funcao:funcao,
                siglaOA:siglaOA,
                dataEntrada:dataEntrada,
                dataTerminoMandato:dataTerminoMandato
                }, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            //alert(retorno);
            if(retorno.status=="1")
            {    
                location.href='?corpo=buscaBaixaFuncaoPendenteVerificados&salvo=1';    	
            }
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function alteraStatusBaixaFuncaoDuplicados(seqCadast,status,funcao,siglaOA,dataEntrada,dataTerminoMandato)
{
    //alert(seqCadast);
    //alert(status);
	$.ajax({
        type: "post",
        url: "js/ajax/alteraStatusBaixaDuplicados.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
        	seqCadast:seqCadast,
                status:status,
                funcao:funcao,
                siglaOA:siglaOA,
                dataEntrada:dataEntrada,
                dataTerminoMandato:dataTerminoMandato
                }, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            //alert(retorno);
            if(retorno.status=="1")
            {
                location.href='?corpo=buscaBaixaFuncaoDuplicados&salvo=1';    	
            }
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function alteraStatusBaixaFuncaoDuplicadosParaNaoVerificado(seqCadast,status,funcao,siglaOA,dataEntrada,dataTerminoMandato)
{
    //alert(seqCadast);
    //alert(status);
	$.ajax({
        type: "post",
        url: "js/ajax/alteraStatusBaixaDuplicados.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
        	seqCadast:seqCadast,
                status:status,
                funcao:funcao,
                siglaOA:siglaOA,
                dataEntrada:dataEntrada,
                dataTerminoMandato:dataTerminoMandato
                }, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            //alert(retorno);
            if(retorno.status=="1")
            {
                location.href='?corpo=buscaBaixaFuncaoDuplicadosVerificados&salvo=1';    	
            }
        },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function excluirMembrosIniciadosAtividadeIniciatica()
{
    //alert("idAta:"+idAta);
    var arr = $("#membros_atividade_iniciatica").val();

    if (arr == null)
    {
        swal({title: "Aviso!", text: "Escolha um membro na lista para ser excluído", type: "error", confirmButtonColor: "#1ab394"});
    } else {
        $('#membros_atividade_iniciatica :selected').remove();
    }
}

function abrirPopupRelatorioGerencialFinanceiroAnalitico()
{
    //alert('teste chamada');
    /*
     * Validação
     */
    
    var regiao = "";
    var oa ="";
    var filtro="";
    
    if($("#oaUsuario").val()=="")
    {
        if($("#regiao").val()=="0")
        {
            swal({title: "Aviso!", text: "Região não selecionada!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        regiao = $("#regiao").val();
        oa ="";
        filtro=1;
    }else{
        if($("#fk_idOrganismoAfiliado").val()=="0")
        {
            swal({title: "Aviso!", text: "Organismo não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        regiao ="";
        oa = $("#fk_idOrganismoAfiliado").val();
        filtro=2;
    }    
    /*
    if($("#mesInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    */
    if($("#anoInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    /*
    if($("#mesFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    */
    if($("#anoFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    var bool=false;
    
    for(i=1;i<=11;i++)
    {
        if ($("#linha"+i).is(":checked")) {
            bool=true;
        }
    } 
    if(bool==false)
    {
        swal({title: "Aviso!", text: "Marque pelo menos 1 item para ser mostrado no relatório!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }    
    var dataInicial="";
    dataInicial = "01/01/"+$("#anoInicial").val();
    
    var dataFinal="";
    dataFinal = "01/12/"+$("#anoInicial").val();
     
    var linha1 = 0;
    if ($("#linha1").is(":checked")) {
        linha1 = 1;
    }
    var linha2 = 0;
    if ($("#linha2").is(":checked")) {
        linha2 = 1;
    }
    var linha3 = 0;
    if ($("#linha3").is(":checked")) {
        linha3 = 1;
    }
    var linha4 = 0;
    if ($("#linha4").is(":checked")) {
        linha4 = 1;
    }
    var linha5 = 0;
    if ($("#linha5").is(":checked")) {
        linha5 = 1;
    }
    var linha6 = 0;
    if ($("#linha6").is(":checked")) {
        linha6 = 1;
    }
    var linha7 = 0;
    if ($("#linha7").is(":checked")) {
        linha7 = 1;
    }
    var linha8 = 0;
    if ($("#linha8").is(":checked")) {
        linha8 = 1;
    }
    var linha9 = 0;
    if ($("#linha9").is(":checked")) {
        linha9 = 1;
    }
    var linha10 = 0;
    if ($("#linha10").is(":checked")) {
        linha10 = 1;
    }
    var linha11 = 0;
    if ($("#linha11").is(":checked")) {
        linha11 = 1;
    }
    var linha12 = 0;
    if ($("#linha12").is(":checked")) {
        linha12 = 2;
    }
    var linha13 = 0;
    if ($("#linha13").is(":checked")) {
        linha13 = 1;
    }
    var linha14 = 0;
    if ($("#linha14").is(":checked")) {
        linha14 = 1;
    }
    var linha15 = 0;
    if ($("#linha15").is(":checked")) {
        linha15 = 1;
    }
    var linha16 = 0;
    if ($("#linha16").is(":checked")) {
        linha16 = 1;
    }
    var linha17 = 0;
    if ($("#linha17").is(":checked")) {
        linha17 = 1;
    }
    var linha18 = 0;
    if ($("#linha18").is(":checked")) {
        linha18 = 1;
    }
    var linha19 = 0;
    if ($("#linha19").is(":checked")) {
        linha19 = 1;
    }
    var linha20 = 0;
    if ($("#linha20").is(":checked")) {
        linha20 = 1;
    }
    
    var tipoRelatorio = $("input[name='tipoRelatorio']:checked").val();
    
    if(tipoRelatorio==1)
    {
        location.href= 'impressao/relatorioGerencialFinanceiroGCAnalitico.php?regiao=' + regiao
            + '&oa=' + oa
            + '&filtro=' + filtro
            + '&dataInicial=' + dataInicial
            + '&dataFinal=' + dataFinal
            + '&linha1=' + linha1
            + '&linha2=' + linha2
            + '&linha3=' + linha3
            + '&linha4=' + linha4
            + '&linha5=' + linha5
            + '&linha6=' + linha6
            + '&linha7=' + linha7
            + '&linha8=' + linha8
            + '&linha9=' + linha9
            + '&linha10=' + linha10
            + '&linha11=' + linha11
            + '&linha12=' + linha12
            + '&linha13=' + linha13
            + '&linha14=' + linha14
            + '&linha15=' + linha15
            + '&linha16=' + linha16
            + '&linha17=' + linha17
            + '&linha18=' + linha18
            + '&linha19=' + linha19
            + '&linha20=' + linha20
            ;
    }
    if(tipoRelatorio==2)
    {
        location.href ='impressao/relatorioGerencialFinanceiroGCAnaliticoOutrasReceitas.php?regiao=' + regiao
            + '&oa=' + oa
            + '&filtro=' + filtro
            + '&dataInicial=' + dataInicial
            + '&dataFinal=' + dataFinal;
    }
    if(tipoRelatorio==3)
    {
        location.href ='impressao/relatorioGerencialFinanceiroGCAnaliticoOutrasDespesas.php?regiao=' + regiao
            + '&oa=' + oa
            + '&filtro=' + filtro
            + '&dataInicial=' + dataInicial
            + '&dataFinal=' + dataFinal;
    }
}

function abrirPopupRelatorioGerencialFinanceiro()
{
    
    /*
     * Validação
     */
    
    var regiao = "";
    var oa ="";
    var filtro="";
    
    if($("#oaUsuario").val()=="")
    {
        if($("#regiao").val()=="0")
        {
            swal({title: "Aviso!", text: "Região não selecionada!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        regiao = $("#regiao").val();
        oa ="";
        filtro=1;
    }else{
        if($("#fk_idOrganismoAfiliado").val()=="0")
        {
            swal({title: "Aviso!", text: "Organismo não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        regiao ="";
        oa = $("#fk_idOrganismoAfiliado").val();
        filtro=2;
    }    
    
    if($("#mesInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if($("#anoInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if($("#mesFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if($("#anoFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    var bool=false;
    
    for(i=1;i<=11;i++)
    {
        if ($("#linha"+i).is(":checked")) {
            bool=true;
        }
    } 
    if(bool==false)
    {
        swal({title: "Aviso!", text: "Marque pelo menos 1 item para ser mostrado no relatório!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }    
    var dataInicial="";
    if($("#mesInicial").val()!=0)
    {    
       dataInicial= "01/"+$("#mesInicial").val()+"/"+$("#anoInicial").val();
    }
    var dataFinal="";
    if($("#mesInicial").val()!=0)
    {
        dataFinal= "01/"+$("#mesFinal").val()+"/"+$("#anoFinal").val();
    } 
    var linha1 = 0;
    if ($("#linha1").is(":checked")) {
        linha1 = 1;
    }
    var linha2 = 0;
    if ($("#linha2").is(":checked")) {
        linha2 = 1;
    }
    var linha3 = 0;
    if ($("#linha3").is(":checked")) {
        linha3 = 1;
    }
    var linha4 = 0;
    if ($("#linha4").is(":checked")) {
        linha4 = 1;
    }
    var linha5 = 0;
    if ($("#linha5").is(":checked")) {
        linha5 = 1;
    }
    var linha6 = 0;
    if ($("#linha6").is(":checked")) {
        linha6 = 1;
    }
    var linha7 = 0;
    if ($("#linha7").is(":checked")) {
        linha7 = 1;
    }
    var linha8 = 0;
    if ($("#linha8").is(":checked")) {
        linha8 = 1;
    }
    var linha9 = 0;
    if ($("#linha9").is(":checked")) {
        linha9 = 1;
    }
    var linha10 = 0;
    if ($("#linha10").is(":checked")) {
        linha10 = 1;
    }
    var linha11 = 0;
    if ($("#linha11").is(":checked")) {
        linha11 = 1;
    }
    
    window.open('impressao/relatorioGerencialFinanceiroGC.php?regiao=' + regiao
            + '&oa=' + oa
            + '&filtro=' + filtro
            + '&dataInicial=' + dataInicial
            + '&dataFinal=' + dataFinal
            + '&linha1=' + linha1
            + '&linha2=' + linha2
            + '&linha3=' + linha3
            + '&linha4=' + linha4
            + '&linha5=' + linha5
            + '&linha6=' + linha6
            + '&linha7=' + linha7
            + '&linha8=' + linha8
            + '&linha9=' + linha9
            + '&linha10=' + linha10
            + '&linha11=' + linha11
            , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function abrirPopupRelatorioGerencialAtividade()
{
    /*
     * Validação
     */

    if($("#oaUsuario").val()=="")
    {
        if($("#regiao").val()=="0")
        {
            swal({title: "Aviso!", text: "Região não selecionada!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        var regiao = $("#regiao").val();
        var oa ="";
        var filtro=1;
    }else{
        if($("#fk_idOrganismoAfiliado").val()=="0")
        {
            swal({title: "Aviso!", text: "Organismo não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        var regiao ="";
        var oa = $("#fk_idOrganismoAfiliado").val();
        var filtro=2;
    }  
    if($("#fk_idOrganismoAfiliado").val()!="TODOS"&&$("#fk_idOrganismoAfiliado").val()!="")
    {
        var regiao ="";
        var oa = $("#fk_idOrganismoAfiliado").val();
        var filtro=2;
    }
    
    if($("#mesInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if($("#anoInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if($("#mesFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if($("#anoFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    //Validação das atividades
    var str="";
    var checado=false;
    
    var aChk = document.getElementsByName("atividade"); 

    for (var i=0;i<aChk.length;i++)
    { 
        if (aChk[i].checked == true)
        { 
            checado=true;
            if(aChk[i].id!="linha8")
            {    
                str += ","+aChk[i].id;
            }
        } 
    }
    
    //tratar string
    str = str.substring(1, 100000000);
    
    
    var str2="";
    var checado2=false;
    
    var aChk2 = document.getElementsByName("iniciacao"); 

    for (var i=0;i<aChk2.length;i++)
    { 
        if (aChk2[i].checked == true)
        { 
            checado2=true;
            if(aChk2[i].id!="linha8")
            {    
                str2 += ","+aChk2[i].id;
            }
        } 
    }
    
    //tratar string
    str2 = str2.substring(1, 100000000);
    
    var str3="";
    var checado3=false;
    
    var aChk3 = document.getElementsByName("classificacaoOa"); 

    for (var i=0;i<aChk3.length;i++)
    { 
        if (aChk3[i].checked == true)
        { 
            checado3=true;
            if(aChk3[i].id!="linha8")
            {    
                str3 += ","+aChk3[i].id;
            }
        } 
    }
    
    //tratar string
    str3 = str3.substring(1, 100000000);
    /*
    if(checado==false)
    {
        swal({title: "Aviso!", text: "Marque pelo menos 1 atividade para ser mostrada no relatório!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }    
    */
    
    var dataInicial = "01/"+$("#mesInicial").val()+"/"+$("#anoInicial").val();
    var dataFinal = "01/"+$("#mesFinal").val()+"/"+$("#anoFinal").val();

    if ($("#linha8").is(":checked")) {
        var linha8 = 1;
    }else{
        var linha8 = 0;
    }
    
    window.open('impressao/relatorioGerencialAtividadeGC.php?regiao=' + regiao
            + '&oa=' + oa
            + '&filtro=' + filtro
            + '&dataInicial=' + dataInicial
            + '&dataFinal=' + dataFinal
            + '&str=' + str
            + '&str2=' + str2
            + '&str3=' + str3
            + '&linha8=' + linha8
            , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function getListaAtividadesEstatuto()
{
    var str3="";
    var checado3=false;
    
    var aChk3 = document.getElementsByName("classificacaoOa"); 

    for (var i=0;i<aChk3.length;i++)
    { 
        if (aChk3[i].checked == true)
        { 
            checado3=true;
            if(aChk3[i].id!="linha8")
            {    
                str3 += ","+aChk3[i].id;
            }
        } 
    }
    
    //tratar string
    str3 = str3.substring(1, 100000000);
    
    $.ajax({
        type: "post",
        url: "js/ajax/getListaAtividadesEstatuto.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {str3: str3}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaAtividades").html(data);
            $("#mostrar").show();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function abrirPopupRelatorioGerencialAtividadeAnalitico()
{
    /*
     * Validação
     */

    if($("#oaUsuario").val()=="")
    {
        if($("#regiao").val()=="0")
        {
            swal({title: "Aviso!", text: "Região não selecionada!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        var regiao = $("#regiao").val();
        var oa ="";
        var filtro=1;
    }else{
        if($("#fk_idOrganismoAfiliado").val()=="0")
        {
            swal({title: "Aviso!", text: "Organismo não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        var regiao ="";
        var oa = $("#fk_idOrganismoAfiliado").val();
        var filtro=2;
    }    
    
    if($("#mesInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if($("#anoInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if($("#mesFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    if($("#anoFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    //Validação das atividades
    var str="";
    var checado=false;
    
    var aChk = document.getElementsByName("atividade"); 

    for (var i=0;i<aChk.length;i++)
    { 
        if (aChk[i].checked == true)
        { 
            checado=true;
            if(aChk[i].id!="linha8")
            {    
                str += ","+aChk[i].id;
            }
        } 
    }
    
    //tratar string
    str = str.substring(1, 100000000);
    
    var str2="";
    var checado2=false;
    
    var aChk2 = document.getElementsByName("iniciacao"); 

    for (var i=0;i<aChk2.length;i++)
    { 
        if (aChk2[i].checked == true)
        { 
            checado2=true;
            if(aChk2[i].id!="linha8")
            {    
                str2 += ","+aChk2[i].id;
            }
        } 
    }
    
    //tratar string
    str2 = str2.substring(1, 100000000);
    
    var str3="";
    var checado3=false;
    
    var aChk3 = document.getElementsByName("classificacaoOa"); 

    for (var i=0;i<aChk3.length;i++)
    { 
        if (aChk3[i].checked == true)
        { 
            checado3=true;
            if(aChk3[i].id!="linha8")
            {    
                str3 += ","+aChk3[i].id;
            }
        } 
    }
    
    //tratar string
    str3 = str3.substring(1, 100000000);
    /*
    if(checado==false)
    {
        swal({title: "Aviso!", text: "Marque pelo menos 1 atividade para ser mostrada no relatório!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }    
    */
    
    var dataInicial = "01/"+$("#mesInicial").val()+"/"+$("#anoInicial").val();
    var dataFinal = "01/"+$("#mesFinal").val()+"/"+$("#anoFinal").val();
    
    var classificacaoOa = $('#classificacaoOa').val();

    if ($("#linha8").is(":checked")) {
        var linha8 = 1;
    }else{
        var linha8 = 0;
    }
    
    window.open('impressao/relatorioGerencialAtividadeGCAnalitico.php?regiao=' + regiao
            + '&oa=' + oa
            + '&filtro=' + filtro
            + '&dataInicial=' + dataInicial
            + '&dataFinal=' + dataFinal
            + '&str=' + str
            + '&str2=' + str2
            + '&str3=' + str3
            + '&linha8=' + linha8
            , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function fechaAvisoFacebook(seqCadast)
{
    $.ajax({
        type: "post",
        url: "js/ajax/fechaAvisoFacebook.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if(data.status==1)
            {    
                swal({title: "Aviso!", text: "Caso mude de idéia, você pode acessar o Painel de Usuário para vincular seu login ao Facebook!", type: "warning", confirmButtonColor: "#1ab394"});
            }else{
                alert('Erro ao fechar aviso');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function desvincularFacebook(seqCadast)
{
    $.ajax({
        type: "post",
        url: "js/ajax/desvincularFacebook.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#botaoDesvincularFacebook").hide();
            swal({title: "Aviso!", text: "Desvinculado com sucesso! Caso mude de idéia, você pode acessar o Painel de Usuário para vincular novamente seu login ao Facebook!", type: "warning", confirmButtonColor: "#1ab394"});
            
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function carregaOrganismo(parametro)
{
    var idCampoOpcao = "fk_idOrganismoAfiliado";
    
    $.ajax({
        type: "post",
        url: "js/ajax/carregaCombo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {parametro: parametro,idCampoOpcao: idCampoOpcao}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#fk_idOrganismoAfiliado").html(data);        
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
    
}

function carregaOrganismoMailist(parametro)
{
    //alert('kfjçladskjfçladksj');
    var idCampoOpcao = "fk_idOrganismoAfiliado";
    
    $.ajax({
        type: "post",
        url: "js/ajax/carregaCombo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {parametro: parametro,idCampoOpcao: idCampoOpcao}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            data = data.replace("Selecione...","TODOS");
            $("#fk_idOrganismoAfiliado").html(data);        
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
    
}

function carregaFuncoesCheckbox()
{
    
    var str = "";
    if($("#oficiaisAdministrativos").is(":checked"))
    {    
        str += ",2";
    }
    if($("#dignitarios").is(":checked"))
    {    
        str += ",1";
    }
    if($("#oficiaisRitualisticos").is(":checked"))
    {    
        str += ",3";
    }
    if($("#oficiaisComissao").is(":checked"))
    {    
        str += ",6";
    }
    if($("#oficiaisIniciaticos").is(":checked"))
    {    
        str += ",4,5";
    }
    var tipo=0;
    if($("#tipo").val()==1)
    {
        tipo=1;
    }
    if($("#tipo").val()==2)
    {
        tipo=2;
    }    
    //alert(str);    
    $.ajax({
        type: "post",
        url: "js/ajax/carregaFuncoes.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {str:str,tipo:tipo}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#linhaFuncao").show();        
            $("#funcoesSelecionadas").html(data); 
            $("#todos").prop('checked', true);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
    
}

function restricaoSair()
{
    swal({title: "Aviso!", text: "Por favor para sair, não feche a janela, mas sim clique em SAIR no canto direito da tela!", type: "warning", confirmButtonColor: "#1ab394"});
    return false;
}

function visualizarVideoAula(seqCadast,idVideoAula)
{
    $("#visualizacao"+idVideoAula).html("<span class=\"badge badge-success\">Visualizada</span>");
    
    $.ajax({
        type: "post",
        url: "js/ajax/visualizarVideoAula.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast: seqCadast, idVideoAula:idVideoAula}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

function listaUploadRecebimento(id)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRecebimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRecebimento(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRecebimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRecebimento(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function closeSA()
{
    var alert = document.querySelector(".sweet-alert"),
    okButton = alert.getElementsByTagName("button")[1];
    $(okButton).trigger("click");
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function zerarMesAno(){
    $("#mesToken").val(0);
    $("#anoToken").val(0);
}

function liberarTokenUsuario()
{
    
    var idFuncionalidade        = $("#idFuncionalidade").val();
    var idItemFuncionalidade    = $("#idItemFuncionalidade").val();
    var idOrganismoAfiliado     = $("#idOrganismoAfiliadoLiberarToken").val();
    var usuarioToken            = $("#usuarioToken").val();
    var dataInicial             = $("#dataInicial").val();
    var dataFinal               = $("#dataFinal").val();
    var mes                     = $("#mesToken").val();
    var ano                     = $("#anoToken").val();
    var liberarEmMassa          =0;
    if($("#liberarEmMassa").is(":checked"))
    {
        liberarEmMassa = 1;
    }
    
    //Valida se a data final e maior que a data inicial
    var data_1 = dataInicial;
    var data_2 = dataFinal;
    
    data_1 = parseInt(data_1.split("/")[2] + data_1.split("/")[1] + data_1.split("/")[0]);
    data_2 = parseInt(data_2.split("/")[2] + data_2.split("/")[1] + data_2.split("/")[0]);
    
    
    //alert(liberarEmMassa);
    if(usuarioToken=="")
    {
        swal({title: "Aviso!", text: "O token não foi enviado! Preencha o login antes de enviar o token!", type: "warning", confirmButtonColor: "#1ab394"});
    }else{    
        var n     = usuarioToken.length;
        if(n < 3){
            swal({title: "Aviso!", text: "O token não foi enviado! O login deve ter mais que 3 caracteres!", type: "warning", confirmButtonColor: "#1ab394"});
        }else{
            if(dataInicial=="")
            {
                swal({title: "Aviso!", text: "O token não foi enviado! A data inicial não foi preenchida!", type: "warning", confirmButtonColor: "#1ab394"});
            }else{
                //alert(dataFinal);
                if(dataFinal=="")
                {
                    swal({title: "Aviso!", text: "O token não foi enviado! A data final não foi preenchida!", type: "warning", confirmButtonColor: "#1ab394"});
                }else{
                    //alert(data_1);
                    //alert(data_2);
                    if (data_1 > data_2) {
                        swal({title: "Aviso!", text: "O token não foi enviado! A data final não pode ser menor que data inicial!", type: "warning", confirmButtonColor: "#1ab394"});
                        //return false;
                    }else{
                        if((mes==""||!isNumber(mes)))
                        {
                            swal({title: "Aviso!", text: "O token não foi enviado! O mês não foi corretamente preenchido!", type: "warning", confirmButtonColor: "#1ab394"});
                        }else{
                            if((ano==""||!isNumber(ano)))
                            {
                                swal({title: "Aviso!", text: "O token não foi enviado! O ano não foi corretamente preenchido!", type: "warning", confirmButtonColor: "#1ab394"});
                            }else{

                                swal({
                                  title: "Você tem certeza?", 
                                  text: "Com essa ação você liberará um token para o usuário: "+usuarioToken,
                                  type: "info",
                                  showCancelButton: true,
                                  closeOnConfirm: false,
                                  confirmButtonText: "Sim, libere!",
                                  confirmButtonColor: "#1ab394"
                                }, function() {
                                    $(".sa-button-container").html("<center><img src=\"img/load.gif\" id=\"loa\" width=\"50%\" height=\"50%\"></center>");
                                    $.ajax({
                                                    type: "post",
                                                    url: "js/ajax/liberarTokenUsuario.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                                                    dataType: "json",
                                                    data: {idFuncionalidade: idFuncionalidade, 
                                                        idItemFuncionalidade:idItemFuncionalidade, 
                                                        usuarioToken:usuarioToken,
                                                        dataInicial:dataInicial,
                                                        dataFinal:dataFinal,
                                                        idOrganismoAfiliado:idOrganismoAfiliado,
                                                        mes:mes,
                                                        ano:ano,
                                                        liberarEmMassa:liberarEmMassa
                                                    }, // exemplo: {codigo:125, nome:"Joaozinho"}  
                                                    success: function (data) {
                                                            //alert(data);
                                                    },
                                                    error: function (xhr, textStatus, errorThrown) {
                                                        alert('Erro no processo do envio do token. Favor avisar a equipe de TI');
                                                    }
                                                })
                                  .done(function(data) {
                                    $(".sa-button-container").html("<button class=\"confirm\" onclick=\"swal.close();\" tabindex=\"1\" style=\"display: inline-block; background-color: rgb(26, 179, 148); box-shadow: rgba(26, 179, 148, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.0470588) 0px 0px 0px 1px inset;\">OK</button>");  
                                    swal({title: "Sucesso!", text: "Token enviado com sucesso. Foi enviado um e-mail com o token para o usuário!", type: "success", confirmButtonColor: "#1ab394"});

                                    //$('#orders-history').load(document.URL +  ' #orders-history');
                                  })
                                  .error(function(data) {
                                    swal({title: "Aviso!", text: "Sistema não conseguiu finalizar o processo. Avise o Setor de TI!", type: "warning", confirmButtonColor: "#1ab394"});
                                  });
                                });
                            }
                        }
                    }
                }
            }
        }
    }
    
    
}

function abrirTicketParaLiberarFuncionalidade()
{
    //alert('teste braz');return false;
    var idFuncionalidadeTicket          = $("#idFuncionalidadeTicket").val();
    var idItemFuncionalidadeTicket      = $("#idItemFuncionalidadeTicket").val();
    var motivoTicket                    = $("#motivoTicket").val();
    var seqCadast                       = $("#seqCadastTicket").val();
    var idOrganismoAfiliado             = $("#idOrganismoAfiliado").val();
    var mes=0;
    var ano=0;
    
    if(idItemFuncionalidadeTicket==0)
    {
        mes = $("#mesTicket").val();
        ano = $("#anoTicket").val();
        idItemFuncionalidadeTicket = ano+mes+idOrganismoAfiliado;
    }    
    
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    if(motivoTicket=="")
    {
        swal({title: "Aviso!", text: "O ticket não foi aberto! Preencha o Motivo antes de abrir o Ticket!", type: "warning", confirmButtonColor: "#1ab394"});
    }else{    
        var n     = motivoTicket.length;
        if(n < 6){
            swal({title: "Aviso!", text: "O ticket não foi aberto! O motivo deve ter mais que 6 caracteres!", type: "warning", confirmButtonColor: "#1ab394"});
        }else{
            swal({
                title: "Você tem certeza?",
                text: "Com essa ação você criará um ticket para a GLP e receberá uma cópia em seu e-mail",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Sim, crie!",
                confirmButtonColor: "#1ab394"},
            function () {
                    $(".sa-button-container").html("<center><img src=\"img/load.gif\" id=\"loa\" width=\"50%\" height=\"50%\"></center>");
                    $.ajax({
                        type: "post",
                        url: "js/ajax/abrirTicketParaLiberarFuncionalidade.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {idFuncionalidade: idFuncionalidadeTicket, 
                            idItemFuncionalidade:idItemFuncionalidadeTicket, 
                            motivo:motivoTicket, 
                            seqCadast:seqCadast,
                            idOrganismoAfiliado:idOrganismoAfiliado,
                            mes:mes,
                            ano:ano
                        }, // exemplo: {codigo:125, nome:"Joaozinho"}  
                        success: function (data) {
                            //alert(data);
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('Erro no cadastro do ticket. Favor avisar a equipe dos Organismos afiliados pelo telefone +55 41 3351-3031');
                        }
                    })
                      .done(function(data) {
                          
                        $("#anexos"+idItemFuncionalidadeTicket).html("<button class=\"btn btn-sm btn-success\" type=\"button\" data-target=\"#mySeeIntegridadeTicketAberto\" data-toggle=\"modal\" data-placement=\"left\"><i class=\"fa fa-hourglass-start\"></i></button>");    
                        $("#editar"+idItemFuncionalidadeTicket).html("<button class=\"btn btn-sm btn-primary\" type=\"button\" data-target=\"#mySeeIntegridadeTicketAberto\" data-toggle=\"modal\" data-placement=\"left\"><i class=\"fa fa-hourglass-start\"></i> Editar </button>");  
                        $("#enviar"+idItemFuncionalidadeTicket).html("<button class=\"btn btn-sm btn-warning\" type=\"button\" data-target=\"#mySeeIntegridadeTicketAberto\" data-toggle=\"modal\" data-placement=\"left\"><i class=\"fa fa-hourglass-start\"></i> Enviar </button>");  
                        
                        $(".sa-button-container").html("<button class=\"confirm\" onclick=\"swal.close();\" tabindex=\"1\" style=\"display: inline-block; background-color: rgb(26, 179, 148); box-shadow: rgba(26, 179, 148, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.0470588) 0px 0px 0px 1px inset;\">OK</button>");  
                        swal({title: "Sucesso!", text: "Ticket aberto com sucesso. Foi enviado uma cópia do ticket para o seu e-mail!", type: "success", confirmButtonColor: "#1ab394"});

                        //$('#orders-history').load(document.URL +  ' #orders-history');
                      })
                      .error(function(data) {
                        swal({title: "Aviso!", text: "Sistema não conseguiu finalizar o processo e não abriu o ticket!! IMPORTANTE: Avise o Setor de Organismos Afiliados pelo telefone +55 41 3351-3034 ou pelo e-mail: soa@amorc.org.br!", type: "warning", confirmButtonColor: "#1ab394"});
                      });
            });
        }
    }
    //}else{
    //	return false;
    //}
}

$(document).ready(function() {
    $('#calendario').datepicker()
        .on("input change", function (e) {
        var dataSelecionada = $(this).val();
        var mes = dataSelecionada.substr(0,2);
        var ano = dataSelecionada.substr(6,4);
        var corpo = $("#paginaAtual").val();
        if(ano<2016)
        {    
             swal({title: "Aviso!", text: "Data inválida! Selecione uma data a partir de 2016, pois foi quando o sistema entrou no ar!", type: "warning", confirmButtonColor: "#1ab394"});
             return false;
        }
        location.href='?corpo='+corpo+'&mesAtual='+mes+'&anoAtual='+ano;
    });
});

function downloadListaDados()
{
    var siglaOA = $("#siglaOA").val();
    
    if ($("#linhaZero").is(":checked")) {
        var linhaZero = 1;
    }else{
        var linhaZero = 0;
    }
    
    if ($("#linha1").is(":checked")) {
        var linha1 = 1;
    }else{
        var linha1 = 0;
    }
    
    if ($("#linha2").is(":checked")) {
        var linha2 = 1;
    }else{
        var linha2 = 0;
    }
    
    if ($("#linha3").is(":checked")) {
        var linha3 = 1;
    }else{
        var linha3 = 0;
    }
    
    if ($("#linha4").is(":checked")) {
        var linha4 = 1;
    }else{
        var linha4 = 0;
    }
    
    if ($("#linha5").is(":checked")) {
        var linha5 = 1;
    }else{
        var linha5 = 0;
    }
    
    if ($("#linha6").is(":checked")) {
        var linha6 = 1;
    }else{
        var linha6 = 0;
    }
    
    if ($("#linha7").is(":checked")) {
        var linha7 = 1;
    }else{
        var linha7 = 0;
    }
    
    if ($("#linha8").is(":checked")) {
        var linha8 = 1;
    }else{
        var linha8 = 0;
    }
    
    if ($("#linha9").is(":checked")) {
        var linha9 = 1;
    }else{
        var linha9 = 0;
    }
    
    if ($("#linha10").is(":checked")) {
        var linha10 = 1;
    }else{
        var linha10 = 0;
    }
    
    if ($("#linha11").is(":checked")) {
        var linha11 = 1;
    }else{
        var linha11 = 0;
    }
    
    if ($("#linha12").is(":checked")) {
        var linha12 = 1;
    }else{
        var linha12 = 0;
    }
    
    if ($("#linha13").is(":checked")) {
        var linha13 = 1;
    }else{
        var linha13 = 0;
    }
    
    if ($("#linha14").is(":checked")) {
        var linha14 = 1;
    }else{
        var linha14 = 0;
    }
    
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    if(siglaOA=="")
    {
        swal({title: "Aviso!", text: "Não foi selecionado o OA!", type: "warning", confirmButtonColor: "#1ab394"});
    }else{    
        
            swal({
                title: "Você concorda com os Termos da AMORC?",
                text: "Com essa ação você aceita a responsabilidade de proteger esses dados",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Sim, crie!",
                confirmButtonColor: "#1ab394"
            },
            function () {
                
                    $(".sa-button-container").html("<center><img src=\"img/load.gif\" id=\"loa\" width=\"50%\" height=\"50%\"></center>");
                    
                    var link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
                    link.href = './js/ajax/downloadListaDados.php?siglaOA='+
                                        siglaOA
                                        +'&linhaZero='+linhaZero
                                        +'&linha1='+linha1
                                        +'&linha2='+linha2
                                        +'&linha3='+linha3
                                        +'&linha4='+linha4
                                        +'&linha5='+linha5
                                        +'&linha6='+linha6
                                        +'&linha7='+linha7
                                        +'&linha8='+linha8
                                        +'&linha9='+linha9
                                        +'&linha10='+linha10
                                        +'&linha11='+linha11
                                        +'&linha12='+linha12
                                        +'&linha13='+linha13
                                        +'&linha14='+linha14
                                        ;
                    link.target = '_blank';
                    var event = new MouseEvent('click', {
                        'view': window,
                        'bubbles': false,
                        'cancelable': true
                    });
                    link.dispatchEvent(event);
                    location.href='?corpo=buscaExtrairListaDados&gerouLista=1';

            });
        
    }
    //}else{
    //	return false;
    //}
}
$(document).ready(function () {
    $('#mostraSenha').change(function () {
        if ($(this).is(":checked")) {
            $('#senhaEmailOrganismoAfiliado').attr('type', 'text');
        }else{
            $('#senhaEmailOrganismoAfiliado').attr('type', 'password');
        }   
    });

});

function notificaNovaAtualizacao(id)
{
    $("#load"+id).html("<center><img src=\"img/load.gif\" id=\"loa\" width=\"50%\" height=\"50%\"></center>");
    $.ajax({
        type: "post",
        url: "js/ajax/notificaNovaAtualizacao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            if (data.status == 1)
            {
                $("#load"+id).html("<center><i class='fa fa-check'></i></center>");
                swal({title: "Sucesso!", text: "Enviado com sucesso!", type: "success", confirmButtonColor: "#1ab394"});
            } else {
                $("#load"+id).html("<center>Erro. Pressione F5</center>");
                swal({title: "Aviso!", text: "Sistema não conseguiu finalizar o processo!", type: "warning", confirmButtonColor: "#1ab394"});
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            swal({title: "Aviso!", text: "Sistema não conseguiu finalizar o processo. Avise o Setor de TI!", type: "warning", confirmButtonColor: "#1ab394"});
        }
    });

}

function preencheMotivoDesligamentoRC(cod)
{
    $.ajax({
        type: "post",
        url: "js/ajax/preencheMotivoDesligamentoRC.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {cod: cod}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#motivoDesligamentoRC").html(data.result[0].fields.fDesDeslig);
      },
        error: function (xhr, textStatus, errorThrown) {
            swal({title: "Aviso!", text: "Sistema não conseguiu finalizar o processo de preencher o motivo do desligamento rosacruz. Avise o Setor de TI!", type: "warning", confirmButtonColor: "#1ab394"});
        }
    });
}

function preencheMotivoDesligamentoTom(cod)
{
    $.ajax({
        type: "post",
        url: "js/ajax/preencheMotivoDesligamentoRC.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {cod: cod}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#motivoDesligamentoTom").html(data.result[0].fields.fDesDeslig);
      },
        error: function (xhr, textStatus, errorThrown) {
            swal({title: "Aviso!", text: "Sistema não conseguiu finalizar o processo de preencher o motivo do desligamento TOM. Avise o Setor de TI!", type: "warning", confirmButtonColor: "#1ab394"});
        }
    });
}

function preencheMotivoDesligamentoOjp(cod)
{
    $.ajax({
        type: "post",
        url: "js/ajax/preencheMotivoDesligamentoRC.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {cod: cod}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#motivoDesligamentoOjp").html(data.result[0].fields.fDesDeslig);
      },
        error: function (xhr, textStatus, errorThrown) {
            swal({title: "Aviso!", text: "Sistema não conseguiu finalizar o processo de preencher o motivo do desligamento OGG. Avise o Setor de TI!", type: "warning", confirmButtonColor: "#1ab394"});
        }
    });
}

function toggleRecorrente(cod)
{
    //alert(cod);
    if(cod==0)
    {
        $("#naoRecorrente").show();
        $("#atividadeRecorrente").hide();
    }else{
        $("#naoRecorrente").hide();
        $("#atividadeRecorrente").show();
    } 
    
}

function carregaUploadsDaFuncionalidade()
{
    var idFuncionalidade = $("#idFunc").val();
    var idIdentificadorTicket = $("#idIdentificadorTicket").val();
    var nomeFuncionalidade = $("#nomeFuncionalidade").val();
    
    $("#tituloModal").html(nomeFuncionalidade);
    $("#idDoItem").html(idIdentificadorTicket);
    
    if(idFuncionalidade==1)
    {
        $("#listaUploadJaEntregue").html("Em Desenvolvimento...");
    }    
    if(idFuncionalidade==2)
    {
        listaUploadAtaReuniaoMensalJaEntregue(idIdentificadorTicket);
    } 
    if(idFuncionalidade==3)
    {
        listaUploadAtaPosseJaEntregue(idIdentificadorTicket);
    }
    
}

function calculaQuantidadeMensalTrimestralAnual()
{
    var contribuicao = $("input[name='contribuicao']:checked").val();
    //alert(contribuicao);
    $.ajax({
        type: "post",
        url: "js/ajax/preencheComboContribuicao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {contribuicao: contribuicao}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#quantidade").html(data);
            $("#quantidade").val(1);
            
      },
        error: function (xhr, textStatus, errorThrown) {
            swal({title: "Aviso!", text: "Erro ao preencher a quantidade conforme a contribuição!", type: "warning", confirmButtonColor: "#1ab394"});
        }
    });
    
}

function colocaMesInicialFinal()
{
    $("#mesInicial_chosen").show();
    $("#mesFinal_chosen").show();
    $("#barra").show();
    $("#barra2").show();
    $("#txtMesInicial").show();
    $("#txtMesFinal").show();
    
    $("#mesInicial").val(0).trigger("chosen:updated");
    $("#mesFinal").val(0).trigger("chosen:updated");
    
}

function tiraMesInicialFinal()
{
    $("#mesInicial_chosen").hide();
    $("#mesFinal_chosen").hide();
    $("#barra").hide();
    $("#barra2").hide();
    $("#txtMesInicial").hide();
    $("#txtMesFinal").hide();
    
    $("#mesInicial").val(0).trigger("chosen:updated");
    $("#mesFinal").val(0).trigger("chosen:updated");
}

function mostrarColunas(mostra)
{
    //alert(mostra);
    if(mostra==1)
    {
        $("#mostrar").show();
    }else{
        $("#mostrar").hide();
    }    
}

function abrirPopupRelatoriosNaoEntregues()
{
    var regiao = "";
    var oa ="";
    var filtro="";
    var situacao = $("#situacao").val();
    
    if($("#oaUsuario").val()=="")
    {
        if($("#regiao").val()=="0")
        {
            swal({title: "Aviso!", text: "Região não selecionada!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        regiao = $("#regiao").val();
        oa ="";
        filtro=1;
    }else{
        if($("#fk_idOrganismoAfiliado").val()=="0")
        {
            swal({title: "Aviso!", text: "Organismo não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
        regiao ="";
        oa = $("#fk_idOrganismoAfiliado").val();
        filtro=2;
    }    
    
    if($("#mesInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if($("#anoInicial").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano inicial não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if($("#mesFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Mês final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if($("#anoFinal").val()=="0")
    {
        swal({title: "Aviso!", text: "Ano final não selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    var bool=false;
    
    for(i=1;i<=11;i++)
    {
        if ($("#linha"+i).is(":checked")) {
            bool=true;
        }
    } 
    if(bool==false)
    {
        swal({title: "Aviso!", text: "Marque pelo menos 1 item para ser mostrado no relatório!", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }    
    var dataInicial="";
    if($("#mesInicial").val()!=0)
    {    
       dataInicial= "01/"+$("#mesInicial").val()+"/"+$("#anoInicial").val();
    }
    var dataFinal="";
    if($("#mesInicial").val()!=0)
    {
        dataFinal= "01/"+$("#mesFinal").val()+"/"+$("#anoFinal").val();
    } 
    var linha1 = 0;
    if ($("#linha1").is(":checked")) {
        linha1 = 1;
    }
    var linha2 = 0;
    if ($("#linha2").is(":checked")) {
        linha2 = 1;
    }
    var linha3 = 0;
    if ($("#linha3").is(":checked")) {
        linha3 = 1;
    }
    var linha4 = 0;
    if ($("#linha4").is(":checked")) {
        linha4 = 1;
    }
    var linha5 = 0;
    if ($("#linha5").is(":checked")) {
        linha5 = 1;
    }
    var linha6 = 0;
    if ($("#linha6").is(":checked")) {
        linha6 = 1;
    }
    var linha7 = 0;
    if ($("#linha7").is(":checked")) {
        linha7 = 1;
    }
    var linha8 = 0;
    if ($("#linha8").is(":checked")) {
        linha8 = 1;
    }
    var linha9 = 0;
    if ($("#linha9").is(":checked")) {
        linha9 = 1;
    }
    var linha10 = 0;
    if ($("#linha10").is(":checked")) {
        linha10 = 1;
    }
                
    window.open('impressao/relatoriosNaoEntregues.php?regiao=' + regiao
            + '&oa=' + oa
            + '&filtro=' + filtro
            + '&dataInicial=' + dataInicial
            + '&dataFinal=' + dataFinal
            + '&linha1=' + linha1
            + '&linha2=' + linha2
            + '&linha3=' + linha3
            + '&linha4=' + linha4
            + '&linha5=' + linha5
            + '&linha6=' + linha6
            + '&linha7=' + linha7
            + '&linha8=' + linha8
            + '&linha9=' + linha9
            + '&linha10=' + linha10
            + '&situacao=' + situacao
            , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');        
}

function excelRecebimentos(mes,ano,id)
{
    location.href = 'impressao/excelRecebimentos.php?mesAtual=' + mes + '&anoAtual=' +ano+ '&idOrganismoAfiliado='+id;
}

function excelDespesas(mes,ano,id)
{
    location.href = 'impressao/excelDespesas.php?mesAtual=' + mes + '&anoAtual=' +ano+ '&idOrganismoAfiliado='+id;
}

function excelSaldo(mes,ano,id)
{
    location.href = 'impressao/excelSaldo.php?mesAtual=' + mes + '&anoAtual=' +ano+ '&idOrganismoAfiliado='+id;
}

function validaConvocacaoRitualisticaOGG()
{
    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }

    if ($("#dataConvocacao").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a data da convocação", type: "warning", confirmButtonColor: "#1ab394"});
        $("#dataConvocacao").focus();
        return false;
    }

    if ($("#horaConvocacao").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a hora inicial da convocação", type: "warning", confirmButtonColor: "#1ab394"});
        $("#horaConvocacao").focus();
        return false;
    }
    if ($("#tituloMensagem").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o título da mensagem", type: "warning", confirmButtonColor: "#1ab394"});
        $("#tituloMensagem").focus();
        return false;
    }
    if ($("#h_seqCadastMembroComendador").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o campo Comendador de forma apropriada. Encontrando um membro R+C pela pesquisa", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codAfiliacaoMembroComendador").focus();
        return false;
    }
    
    return true;
}

function getUltimoGrauRealizado(seqCadast, numGrau, lote)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getUltimaIniciacaoPendenteRC.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast:seqCadast,numGrau:numGrau, lote:lote}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#grauRC").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function validaRitoPassagemOGG()
{
    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    
    if ($("#rito").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um rito", type: "warning", confirmButtonColor: "#1ab394"});
        $("#rito").focus();
        return false;
    }

    if ($("#data").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a data do rito", type: "warning", confirmButtonColor: "#1ab394"});
        $("#data").focus();
        return false;
    }

    if ($("#hora").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a hora inicial do rito", type: "warning", confirmButtonColor: "#1ab394"});
        $("#hora").focus();
        return false;
    }
    if ($("#h_seqCadastMembroMestre").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o campo Mestre na Ocasião de forma apropriada. Encontrando um membro R+C pela pesquisa", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codAfiliacaoMembroMestre").focus();
        return false;
    }
    
    return true;
}

function abrirPopupRelatorioMensalOGG( idOrganismoAfiliado, mesAtual, anoAtual)
{
    window.open('impressao/relatorioMensalOGG.php?mesAtual=' + mesAtual + '&anoAtual=' + anoAtual + '&idOrganismoAfiliado=' + idOrganismoAfiliado , 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function listaUploadRelatorioMensalOGG(mesAtual, anoAtual, idOrganismoAfiliado)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadRelatorioMensalOGG.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {mesAtual: mesAtual, anoAtual: anoAtual, idOrganismoAfiliado: idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadRelatorioMensalOGG(idExcluir, mesAtual, anoAtual, idOrganismoAfiliado)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadRelatorioMensalOGG.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        listaUploadRelatorioMensalOGG(mesAtual, anoAtual, idOrganismoAfiliado);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
        }
    });
    //}else{
    //	return false;
    //}
}

function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        document.getElementById('enderecoOrganismoAfiliado').value=(conteudo.logradouro);
        document.getElementById('bairroOrganismoAfiliado').value=(conteudo.bairro);
        preencheLatitudeLongitudeOa();
        selecionaEstado(conteudo.localidade,conteudo.uf);
    } //end if.
    else {
        //CEP não Encontrado.
        //limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
}
        
function pesquisacep(valor) {

    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Cria um elemento javascript.
            var script = document.createElement('script');

            //Sincroniza com o callback.
            script.src = '//viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);

        } //end if.
        else {
            //cep é inválido.
            //limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};
 
function selecionaEstado(cidade,uf) 
{ 
    $.ajax({ 
        type: "post", 
        url: "js/ajax/selecionaEstado.php", // exemplo: "/meuProjeto/queroPegarOsDados.php" 
        dataType: "json", 
        data: {uf:uf}, // exemplo: {codigo:125, nome:"Joaozinho"}   
        success: function (data) { 
            //alert(data); 
            $("#fk_idEstado").val(data.idEstado); 
            carregaCombo(data.idEstado, 'fk_idCidade'); 
            window.setTimeout(function() { 
                selecionaCidade(cidade,uf); 
            }, 1000); 
             
        }, 
        error: function (xhr, textStatus, errorThrown) { 
            alert('erro ao selecionar o estado'); 
        } 
    }); 
} 
 
function selecionaCidade(cidade,uf) 
{ 
    $.ajax({ 
        type: "post", 
        url: "js/ajax/selecionaCidade.php", // exemplo: "/meuProjeto/queroPegarOsDados.php" 
        dataType: "json", 
        data: {cidade: cidade, uf:uf}, // exemplo: {codigo:125, nome:"Joaozinho"}   
        success: function (data) { 
            //alert(data); 
            $("#fk_idCidade").val(data.idCidade); 
            
        }, 
        error: function (xhr, textStatus, errorThrown) { 
            alert('erro ao selecionar a cidade'); 
        } 
    }); 
} 
 
function selecionaPais(id) 
{ 
    if(id==1) 
    { 
        $("#paisOa").val("BR"); 
    }   
    if(id==2) 
    { 
        $("#paisOa").val("PT"); 
    }  
    if(id==3) 
    { 
        $("#paisOa").val("RA"); 
    }  
} 
 
function preencheLatitudeLongitudeOa()
{
    
    var endereco = $("#enderecoOrganismoAfiliado").val();
    var numero = $("#numeroOrganismoAfiliado").val();
    var fkcidade = $("#fk_idCidade").val();
    var cep = $("#cepOrganismoAfiliado").val();
    var siglaPais = $("#paisOa").val();
        
    $.ajax({
        type: "GET",
        url: "cron/retorna_lat_long_oa_por_endereco.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {endereco: endereco, 
            numero:numero,
            fkcidade:fkcidade,
            cep:cep,
            siglaPais:siglaPais}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            
            $("#latitude").val(data.latitude);
            $("#logitude").val(data.longitude);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao preencher a latitude e logitude');
        }
    });
    
}

function validaAuxiliarWeb()
{

    if ($("#fk_idOrganismoAfiliado").val() == 0)
    {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    if ($("#dataTermoCompromisso").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a data do Termo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#dataTermoCompromisso").focus();
        return false;
    }
    if ($("#codigoAfiliacao").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o código de afiliação", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codigoAfiliacao").focus();
        return false;
    }
    if ($("#nome").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o nome", type: "warning", confirmButtonColor: "#1ab394"});
        $("#nomeMembroOa").focus();
        return false;
    }
    if ($("cpf").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o cpf", type: "warning", confirmButtonColor: "#1ab394"});
        $("#cpf").focus();
        return false;
    }
    if ($("email").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o email", type: "warning", confirmButtonColor: "#1ab394"});
        $("#email").focus();
        return false;
    }
    if ($("logradouro").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a rua", type: "warning", confirmButtonColor: "#1ab394"});
        $("#logradouro").focus();
        return false;
    }
    if ($("numero").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o número do logradouro", type: "warning", confirmButtonColor: "#1ab394"});
        $("#numero").focus();
        return false;
    }
    if ($("bairro").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o bairro", type: "warning", confirmButtonColor: "#1ab394"});
        $("#bairro").focus();
        return false;
    }
    if ($("cep").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o cep", type: "warning", confirmButtonColor: "#1ab394"});
        $("#cep").focus();
        return false;
    }
    if ($("cidade").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a cidade", type: "warning", confirmButtonColor: "#1ab394"});
        $("#cidade").focus();
        return false;
    }
    if ($("pais").val() == "0")
    {
        swal({title: "Aviso!", text: "Selecione o país", type: "warning", confirmButtonColor: "#1ab394"});
        $("#pais").focus();
        return false;
    }
    if (($("telefoneResidencial").val() == "")&&($("telefoneComercial").val() == "")&&($("celular").val() == ""))
    {
        swal({title: "Aviso!", text: "Preencha um telefone pelo menos!", type: "warning", confirmButtonColor: "#1ab394"});
        $("#bairro").focus();
        return false;
    }

    

    return true;
}
function listaUploadAuxiliarWeb(id)
{
    //alert(id+"teste");
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadAuxiliarWeb.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadAuxiliarWeb(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadAuxiliarWeb.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        //alert(id);
                        listaUploadAuxiliarWeb(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function preencheNomePrincipal(principal)
{
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembroPorSeqCadast.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: principal}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#nome_h").val(data.result[0].fields.fNomCliente.trim());
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao preencher nome do principal');
        }
    });    
}

function getUsuariosQueMandaramMensagem(seqCadast,para)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getUsuariosQueMandaramMensagem.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {seqCadast:seqCadast,para:para}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#listaUsuariosQueMandaramMensagem").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            //alert('erro ao preencher nome dos que mandaram mensagem');
        }
    });    
}

function escolheAnoCartaConstitutiva(ano)
{
    $("#botaoCalculaCartaConstitutiva").html("<a href=\"planilha/calcularCartaConstitutiva.php?ano="+ano+"\" class=\"btn btn-sm btn-success\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Calcular\" target=\"_blank\"> <i class=\"fa fa-calculator fa-white\"></i>&nbsp; Calcular Carta Constitutiva para o ano "+ano+" (Janeiro a Dezembro)</a>");
}

function listaUploadEstatuto(id)
{
    //alert(id+"teste");
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadEstatuto.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaUpload").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function excluirUploadEstatuto(idExcluir, id)
{
    //var r = confirm("Tem certeza que deseja excluir esse documento?");
    //if(r == true)
    //{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "post",
                url: "js/ajax/excluirUploadEstatuto.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",
                data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function (data) {
                    if (data.status == 1)
                    {
                        swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                        //alert(id);
                        listaUploadEstatuto(id);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert('erro');
                }
            });
        } else {
            swal("Cancelado", "Nós não excluímos o arquivo :)", "error");
        }
    });
    //}else{
    //	return false;
    //}
}

function abrirPopUpEstatuto(idEstatuto)
{
    window.open('impressao/estatuto1/index.php?idEstatuto=' + idEstatuto, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function excluirTodasAtividadesDoMes(idOrganismoAfiliado,mes,ano)
{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente todas as atividades desse mês!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false},
    function (isConfirm) {
        if (isConfirm) {
            location.href = '?corpo=buscaAtividadeEstatuto&excluirAtividadeEstatutoTodasDoMes='+idOrganismoAfiliado+'&mesAtual='+mes+'&anoAtual='+ano;
        } else {
            swal("Cancelado", "Nós não excluímos as atividades desse mês :)", "error");
        }
    });   
}

function listaUploadEstatutoMotivo(id)
{
    //alert(id+"teste");
    $.ajax({
        type: "post",
        url: "js/ajax/getListaUploadEstatutoMotivo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#listaMotivoEstatuto").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function validaEstatuto()
{
    if ($("#fk_idOrganismoAfiliado").val() == 0) {
        swal({title: "Aviso!", text: "Selecione um Organismo", type: "warning", confirmButtonColor: "#1ab394"});
        $("#fk_idOrganismoAfiliado").focus();
        return false;
    }
    
    if ($("#cidadeOficio").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o cidade do Ofício", type: "warning", confirmButtonColor: "#1ab394"});
        $("#cidadeOficio").focus();
        return false;
    }
    
    if ($("#ufOficio").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o número do Ofício", type: "warning", confirmButtonColor: "#1ab394"});
        $("#numeroOficio").focus();
        return false;
    }
    
    if ($("#dataAssembleiaGeral").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha a data da Assembléia Geral", type: "warning", confirmButtonColor: "#1ab394"});
        $("#dataAssembleiaGeral").focus();
        return false;
    }
    
    if ($("#numeroPresentes").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o número de presentes na Assembléia", type: "warning", confirmButtonColor: "#1ab394"});
        $("#numeroPresentes").focus();
        return false;
    }
    

    return true;
}

function escondeCamposTicket(id)
{
    if(id==1||id==2)
    {
        $("#tr_campos_mes").hide();
        $("#tr_campos_ano").hide();
        
    }else{
        $("#tr_campos_mes").show();
        $("#tr_campos_ano").show();
    }    
}

function pesquisarPlanoAcaoRegiao(pesquisar)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaPlanoAcaoRegiao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {pesquisar: pesquisar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#resultadoPesquisa").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function pesquisarPlanoAcaoOrganismo(pesquisar)
{
    $.ajax({
        type: "post",
        url: "js/ajax/getListaPlanoAcaoOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {pesquisar: pesquisar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            $("#resultadoPesquisa").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function navegaTabelaContribuicao(pag)
{
    location.href='?corpo=buscaTabelaContribuicao&pag='+pag;
}

function abrirCampoTabelaContribuicao(linha,tipo,valorDefault,campoDB)
{
    var campoDefault='';
    var nomeCampo='';
    //alert("linha=>"+linha);
    //Fechar todos os campos
    for(var i=0;i<15;i++)
    {

        //alert(campoDefault);
        if(i!=linha)
        {
            //alert("i=>"+i);
            campoDefault=$("#valorDefault"+i).val();
            nomeCampo=$("#nomeCampo"+i).val();
            //alert("campoDefault"+campoDefault);
            $("#"+i).html(campoDefault);
            $("#editar"+i).html('' +
                '<a href="#" class="btn btn-outline btn-primary" onclick="abrirCampoTabelaContribuicao(\''+i+'\',\''+tipo+'\',\''+campoDefault+'\',\''+nomeCampo+'\')">Editar</a>'
                +'<input type="hidden" id="valorDefault'+i+'" value="'+campoDefault+'">'
                +'<input type="hidden" id="nomeCampo'+i+'" value="'+nomeCampo+'">'
            );
        }
    }

    //Colocar valor no campo
    $("#"+linha).html('<input type="text" id="valor'+linha+'" value="'+valorDefault+'">');
    $("#editar"+linha).html("<a href='#' class='btn btn-outline btn-primary' onclick='salvarCampoTabelaContribuicao()'>Salvar</a>"
        +'<input type="hidden" id="valorDefault'+linha+'" value="'+valorDefault+'">'
        +'<input type="hidden" id="nomeCampo'+linha+'" value="'+campoDB+'">');

    //Guardar Campos no final da página
    $("#linha").val(linha);
    $("#tipo").val(tipo);
    $("#campoDB").val(campoDB);

}

function salvarCampoTabelaContribuicao()
{
    var linha = $("#linha").val();
    //alert(linha);
    var tipo = $("#tipo").val();
    //alert(tipo);
    var campoDB = $("#campoDB").val();
    //alert(campoDB);
    var valor = $("#valor"+linha).val();
    //alert(valor);
    $.ajax({
        type: "post",
        url: "js/ajax/salvarCampoTabelaContribuicao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {tipo: tipo, campoDB: campoDB, valor: valor}, // exemplo: {codigo:125, nome:"Joaozinho"}
        success: function (data) {
            //alert(data.sucesso);
            $("#"+linha).html(valor);
            $("#editar"+linha).html('' +
                '<a href="#" class="btn btn-outline btn-primary" onclick="abrirCampoTabelaContribuicao(\''+linha+'\',\''+tipo+'\',\''+valor+'\',\''+campoDB+'\')">Editar</a>'
                +'<input type="hidden" id="valorDefault'+linha+'" value="'+valor+'">'
                +'<input type="hidden" id="nomeCampo'+linha+'" value="'+campoDB+'">'
            );

        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function copiarTokenJquery(id)
{
    $("#link"+id).text($("#paraCopiar"+id).text());
}

function copy(element_id){
    var aux = document.createElement("div");
    aux.setAttribute("contentEditable", true);
    aux.innerHTML = document.getElementById(element_id).innerHTML;
    aux.setAttribute("onfocus", "document.execCommand('selectAll',false,null)");
    document.body.appendChild(aux);
    aux.focus();
    document.execCommand("copy");
    document.body.removeChild(aux);
}

function mascaraData(val) {
    var pass = val.value;
    var expr = /[0123456789]/;

    for (i = 0; i < pass.length; i++) {
        // charAt -> retorna o caractere posicionado no índice especificado
        var lchar = val.value.charAt(i);
        var nchar = val.value.charAt(i + 1);

        if (i == 0) {
            // search -> retorna um valor inteiro, indicando a posição do inicio da primeira
            // ocorrência de expReg dentro de instStr. Se nenhuma ocorrencia for encontrada o método retornara -1
            // instStr.search(expReg);
            if ((lchar.search(expr) != 0) || (lchar > 3)) {
                val.value = "";
            }

        } else if (i == 1) {

            if (lchar.search(expr) != 0) {
                // substring(indice1,indice2)
                // indice1, indice2 -> será usado para delimitar a string
                var tst1 = val.value.substring(0, (i));
                val.value = tst1;
                continue;
            }

            if ((nchar != '/') && (nchar != '')) {
                var tst1 = val.value.substring(0, (i) + 1);

                if (nchar.search(expr) != 0)
                    var tst2 = val.value.substring(i + 2, pass.length);
                else
                    var tst2 = val.value.substring(i + 1, pass.length);

                val.value = tst1 + '/' + tst2;
            }

        } else if (i == 4) {

            if (lchar.search(expr) != 0) {
                var tst1 = val.value.substring(0, (i));
                val.value = tst1;
                continue;
            }

            if ((nchar != '/') && (nchar != '')) {
                var tst1 = val.value.substring(0, (i) + 1);

                if (nchar.search(expr) != 0)
                    var tst2 = val.value.substring(i + 2, pass.length);
                else
                    var tst2 = val.value.substring(i + 1, pass.length);

                val.value = tst1 + '/' + tst2;
            }
        }

        if (i >= 6) {
            if (lchar.search(expr) != 0) {
                var tst1 = val.value.substring(0, (i));
                val.value = tst1;
            }
        }
    }

    if (pass.length > 10)
        val.value = val.value.substring(0, 10);
    return true;
}

function copiarInformacoesEstatuto(de,para)
{
    $("#cartorio"+para).val($("#cartorio"+de).val());
    $("#comarca"+para).val($("#comarca"+de).val());
    $("#numeroLeiMunicipal"+para).val($("#numeroLeiMunicipal"+de).val());
    $("#dataLeiMunicipal"+para).val($("#dataLeiMunicipal"+de).val());
    $("#numeroLeiEstadual"+para).val($("#numeroLeiEstadual"+de).val());
    $("#dataLeiEstadual"+para).val($("#dataLeiEstadual"+de).val());
}

function aumentarLetra(id,str)
{
    $("#"+id).val(str.toUpperCase());
}

function mascara_num(obj){
    valida_num(obj)
    if (obj.value.match("-")){
        mod = "-";
    }else{
        mod = "";
    }
    valor = obj.value.replace("-","");
    valor = valor.replace(",","");
    if (valor.length >= 3){
        valor = poe_ponto_num(valor.substring(0,valor.length-2))+","+valor.substring(valor.length-2, valor.length);
    }
    obj.value = mod+valor;
}
function poe_ponto_num(valor){
    valor = valor.replace(/\./g,"");
    if (valor.length > 3){
        valores = "";
        while (valor.length > 3){
            valores = "."+valor.substring(valor.length-3,valor.length)+""+valores;
            valor = valor.substring(0,valor.length-3);
        }
        return valor+""+valores;
    }else{
        return valor;
    }
}
function valida_num(obj){
    numeros = new RegExp("[0-9]");
    while (!obj.value.charAt(obj.value.length-1).match(numeros)){
        if(obj.value.length == 1 && obj.value == "-"){
            return true;
        }
        if(obj.value.length >= 1){
            obj.value = obj.value.substring(0,obj.value.length-1)
        }else{
            return false;
        }
    }
}

function abrirPopupRelatorioColumbasGLP()
{
    var id = $('#fk_idOrganismoAfiliado').val();

    if(id!=""&&id!=0) {
        window.open('impressao/relatorioColumbasGLP.php?idOrganismoAfiliado=' + id, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }else{
        swal({title: "Aviso!", text: "Selecione um Organismo primeiro!", type: "warning", confirmButtonColor: "#1ab394"});
    }
}

function validaCadastroFalecimento()
{
    if ($("#nomeMembroOa").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o nome do membro", type: "warning", confirmButtonColor: "#1ab394"});
        $("#nomeMembroOa").focus();
        return false;
    }

    if ($("#codigoAfiliacao").val() == "")
    {
        swal({title: "Aviso!", text: "Preencha o código de afiliação", type: "warning", confirmButtonColor: "#1ab394"});
        $("#codigoAfiliacao").focus();
        return false;
    }

    if ($("#seqCadastMembroOa").val() == "")
    {
        swal({title: "Aviso!", text: "O código interno não foi encontrado. Faça a busca novamente e tente salvar novamente", type: "warning", confirmButtonColor: "#1ab394"});
        $("#seqCadastMembroOa").focus();
        return false;
    }

    return true;
}

function concordoTermoAssinaturaEletronica(corpo,seqCadast)
{
    //alert(seqCadast);

    //alert('chamou concordo Assinatura');
    $("#load").html('<img src="../img/loading_trans.gif" width="70" height="70" alt="carregando">');
    $.ajax({
        type: "post",
        url: "../js/ajax/concordoTermoAssinaturaEletronica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: seqCadast}, // exemplo: {codigo:125, nome:"Joaozinho"}
        success: function (data) {
            //alert(data);


            var meioEmail = data.meioEmail;
            var meioCelular = data.meioCelular;

            if(data.status==1)
            {
                swal({title: "Aviso!", text: "É preciso validar a adesão à assinatura eletrônica! Se tudo estiver certo, foi enviado o código de adesão para o seu Celular ou para seu E-mail. Caso os dados estejam desatualizados, você deve clicar em Não Concordo abaixo, atualizar seus dados na Área do Afiliado e tentar um novo acesso ao SOA para aderir a Assinatura Eletrônica. Caso seja disparado, o sms chegará em até 5 minutos e pode ser que o e-mail esteja no lixo eletrônico da sua caixa de mensagens. Você deve copiar o código enviado e colocá-lo no campo Código da Adesão!", type: "warning", confirmButtonColor: "#1ab394"});
                $("#validarAdesao").html("<table><tr><td><p>É preciso validar a adesão à assinatura eletrônica! Se tudo estiver certo, foi enviado o código de adesão para o seu Celular ou para seu E-mail. Caso os dados estejam desatualizados, você deve clicar em Não Concordo abaixo, atualizar seus dados na Área do Afiliado e tentar um novo acesso ao SOA para aderir a Assinatura Eletrônica. Caso seja disparado, o sms chegará em até 5 minutos e pode ser que o e-mail esteja no lixo eletrônico da sua caixa de mensagens. Você deve copiar o código enviado e colocá-lo no campo Código da Adesão!</p></td></tr></table><hr><table>" +
                    "<tr><td><p><b>Ou celular:</b></p></td><td>&nbsp;"+meioCelular+"</td></tr>" +
                    "<tr><td><p><b>Ou e-mail:</b></p></td><td>&nbsp;"+meioEmail+"</td></tr>" +
                    "<tr><td><p><b>Código da Adesão:</b></p></td><td>&nbsp;<input onkeypress='valida_num(this)' type=text name='codigoAdesao' id='codigoAdesao'></td></tr></table>");
                $("#botao").html("<a class='btn btn-warning' href='#' onclick='validarAdesaoAssinaturaEletronica("+seqCadast+");'>Validar adesão</a>");

                $("#load").html('');

            }else{
                swal({title: "Aviso!", text: "Houve um problema ao concordar com a Assinatura Eletrônica. Avise o Setor de Organismos Afiliados!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Serviço em Manutenção!');
        }
    });
}


function validarAdesaoAssinaturaEletronica(seqCadast)
{
    //Validar assinatura sem codigo de adesão

    //var codigoAdesao = $("#codigoAdesao").val();

    var codigoAdesao = 0;

    //alert('chamou concordo Assinatura');
    $.ajax({
        type: "post",
        url: "../js/ajax/validarAdesaoAssinaturaEletronica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seqCadast: seqCadast,codigoAdesao:codigoAdesao}, // exemplo: {codigo:125, nome:"Joaozinho"}
        success: function (data) {
            //alert(data);
            if(data.status==1)
            {
                //swal({title: "Sucesso!", text: "Código validado com sucesso!", type: "success", confirmButtonColor: "#1ab394"});
                swal({title: "Sucesso!", text: "Foi registrado no sistema a data e a hora que foi aceito!", type: "success", confirmButtonColor: "#1ab394"});
                location.href='../painelDeControle.php?corpo=perfilUsuario';

            }
            /*
            else{
                swal({title: "Aviso!", text: "O código digitado não confere com o enviado!", type: "warning", confirmButtonColor: "#1ab394"});
            }*/
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Erro ao concordar com a Assinatura');
        }
    });
}


function checkAllCheck() {

    var ipts = $(".checkboxes");
    //alert(ipts);
    // CheckBox que ao ser clicado marca ou desmarca todos elementos
    var check = document.getElementById("checkboxCheckAll");
    //alert(check);
    // Testamos o CheckBox para ver se devemos marcar ou desmarcar
    check.checked ?
        jQuery.each(ipts, function () {
            // Se esta "checado" então marcamos todos elementos como checked=true
            this.checked = true;
        }) :
        jQuery.each(ipts, function () {
            // Se não esta "checado" então marcamos todos elementos como checked=false
            this.checked = false;
        });
}

function entregarAtaReuniaoMensal(id,quemEntregou)
{
    swal({
            title: "Você tem certeza?",
            text: "Com essa ação você entrega eletrônicamente e concorda que o documento está em sua ultima versão?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, entrego!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: true},
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: "js/ajax/entregarAtaReuniaoMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {id: id, quemEntregou:quemEntregou}, // exemplo: {codigo:125, nome:"Joaozinho"}
                    success: function (data) {

                        var funcoes ="";
                        var classificacao = $("#classificacaoOa").val();
                        if(classificacao==1||classificacao==3)
                        {
                            funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                        }else{
                            funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                        }
                        //alert(data);

                        if(data.status==1)
                        {
                            swal({title: "Sucesso!", text: "A ata ("+id+") foi entregue. Faltam as seguintes assinaturas: "+funcoes, type: "success", confirmButtonColor: "#1ab394"});
                            $("#faltaAssinar"+id).show();
                            $("#entregar"+id).hide();
                            $("#pronto"+id).html("<center><b>Sim</b><br><br>Em "+data.dataEntrega+"</center>");
                            return false;
                        }else{
                            swal({title: "Aviso!", text: "Houve um problema ao entregar esse documento da ata. Avise o Setor de Organismos Afiliados!", type: "warning", confirmButtonColor: "#1ab394"});
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro ao entregar o documento da ata');
                    }
                });
            }
        });
}

function entregarRelatorioClasseArtesaos(id,quemEntregou)
{
    swal({
            title: "Você tem certeza?",
            text: "Com essa ação você entrega eletrônicamente e concorda que o documento está em sua ultima versão?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, entrego!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: true},
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: "js/ajax/entregarRelatorioClasseArtesaos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {id: id, quemEntregou:quemEntregou}, // exemplo: {codigo:125, nome:"Joaozinho"}
                    success: function (data) {
                        //alert(data);
                        var funcoes ="";
                        var classificacao = $("#classificacaoOa").val();
                        if(classificacao==1||classificacao==3)
                        {
                            funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                        }else{
                            funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                        }
                        //alert(data);

                        if(data.status==1)
                        {
                            swal({title: "Sucesso!", text: "O Relatório da Classe de Artesãos ("+id+") foi entregue. Faltam as seguintes assinaturas: "+funcoes, type: "success", confirmButtonColor: "#1ab394"});
                            $("#faltaAssinar"+id).show();
                            $("#entregar"+id).hide();
                            $("#pronto"+id).html("<center><b>Sim</b><br><br>Em "+data.dataEntrega+"</center>");
                            return false;
                        }else{
                            swal({title: "Aviso!", text: "Houve um problema ao entregar esse documento do Relatório da Classe de Artesãos. Avise o Setor de Organismos Afiliados!", type: "warning", confirmButtonColor: "#1ab394"});
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro ao entregar o documento do Relatório da Classe de Artesãos');
                    }
                });
            }
        });
}

function entregarRelatorioGrandeConselheiro(id,quemEntregou)
{
    swal({
            title: "Você tem certeza?",
            text: "Com essa ação você entrega eletrônicamente e concorda que o documento está em sua ultima versão?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, entrego!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: true},
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "post",
                    url: "js/ajax/entregarRelatorioGrandeConselheiro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {id: id, quemEntregou:quemEntregou}, // exemplo: {codigo:125, nome:"Joaozinho"}
                    success: function (data) {
                        //alert(data);
                        var funcoes ="";
                        var classificacao = $("#classificacaoOa").val();

                        funcoes = "GRANDE CONSELHEIRO";

                        //alert(data);

                        if(data.status==1)
                        {
                            swal({title: "Sucesso!", text: "O Relatório do Grande Conselheiro (Cód. "+id+") foi entregue. Agora falta a assinatura do frater(soror)!", type: "success", confirmButtonColor: "#1ab394"});
                            $("#faltaAssinar"+id).show();
                            $("#entregar"+id).hide();
                            $("#pronto"+id).html("<center><b>Sim</b><br><br>Em "+data.dataEntrega+"</center>");
                            return false;
                        }else{
                            swal({title: "Aviso!", text: "Houve um problema ao entregar esse documento do Relatório de Grande Conselheiro. Avise o Setor de Organismos Afiliados!", type: "warning", confirmButtonColor: "#1ab394"});
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro ao entregar o documento do Relatório de Grandes Conselheiros');
                    }
                });
            }
        });
}

function assinarEletronicamente(seqCadast,ip,idOrganismoAfiliado)
{
    var regiao = $("#idRegiaoRosacruz").val();
    //alert(regiao);

    swal({
            title: "Você tem certeza?",
            text: "Com essa ação você assina eletrônicamente e concorda que o documento está em sua ultima versão?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, assino!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: true},
        function (isConfirm) {
            if (isConfirm) {


                //Pegar atas marcadas
                var funcoesImportantesParaAssinatura = $("#funcoesImportantesParaAssinatura").val();

                //Pegar atas marcadas
                var checkboxAta = "";
                if ($(".ataReuniaoMensal").length) {

                    $(".ataReuniaoMensal").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            checkboxAta += $(this).val() + ",";
                        }
                    });
                }

                //alert("ata:"+checkboxAta);

                var checkboxFinanceiroMensal = "";
                if ($(".financeiroMensal").length) {

                    $(".financeiroMensal").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            checkboxFinanceiroMensal += $(this).val() + ",";
                        }
                    });
                }

                var checkboxAtividadeMensal = "";
                if ($(".atividadeMensal").length) {

                    $(".atividadeMensal").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            checkboxAtividadeMensal += $(this).val() + ",";
                        }
                    });
                }

                var checkboxOggMensal = "";
                if ($(".oggMensal").length) {

                    $(".oggMensal").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            checkboxOggMensal += $(this).val() + ",";
                        }
                    });
                }

                var checkboxColumbaTrimestral = "";
                if ($(".columbaTrimestral").length) {

                    $(".columbaTrimestral").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            checkboxColumbaTrimestral += $(this).val() + ",";
                        }
                    });
                }

                //Pegar relatórios da classe de artesãos marcados
                var checkboxRelatorioClasseArtesaos = "";
                if ($(".relatorioClasseArtesaos").length) {

                    $(".relatorioClasseArtesaos").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            checkboxRelatorioClasseArtesaos += $(this).val() + ",";
                        }
                    });
                }

                //Pegar relatórios dos grandes conselheiros
                var checkboxRelatorioGrandeConselheiro = "";
                if ($(".relatorioGrandeConselheiro").length) {

                    $(".relatorioGrandeConselheiro").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            checkboxRelatorioGrandeConselheiro += $(this).val() + ",";
                        }
                    });
                }

                var classificacao = $("#classificacaoOa").val();
                //alert("financ:"+checkboxFinanceiroMensal);

                if(checkboxAta!=""
                        ||checkboxFinanceiroMensal!=""
                        ||checkboxAtividadeMensal!=""
                        ||checkboxOggMensal!=""
                        ||checkboxColumbaTrimestral!=""
                        ||checkboxRelatorioClasseArtesaos!=""
                        ||checkboxRelatorioGrandeConselheiro!=""
                )
                {
                    //alert(checkboxAta);
                    //alert(checkboxFinanceiroMensal);
                    $.ajax({
                        type: "post",
                        url: "js/ajax/assinarEletronicamente.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {
                            seqCadast: seqCadast,
                            ip: ip,
                            idOrganismoAfiliado: idOrganismoAfiliado,
                            regiao: regiao,
                            funcoesImportantesParaAssinatura: funcoesImportantesParaAssinatura,
                            checkboxAta: checkboxAta,
                            checkboxFinanceiroMensal: checkboxFinanceiroMensal,
                            checkboxAtividadeMensal: checkboxAtividadeMensal,
                            checkboxOggMensal: checkboxOggMensal,
                            checkboxColumbaTrimestral: checkboxColumbaTrimestral,
                            checkboxRelatorioClasseArtesaos: checkboxRelatorioClasseArtesaos,
                            checkboxRelatorioGrandeConselheiro: checkboxRelatorioGrandeConselheiro,
                            classificacao : classificacao
                        }, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (data) {
                            //alert(data);
                            if (data.status == 1) {
                                swal({
                                    title: "Sucesso!",
                                    text: "Documento(s) assinado(s) eletronicamente com sucesso!",
                                    type: "success",
                                    confirmButtonColor: "#1ab394"
                                });
                                location.href = '?corpo=painelDeControle.php';
                            } else {
                                swal({
                                    title: "Aviso!",
                                    text: "Houve um problema ao assinar. Avise o Setor de Organismos Afiliados!",
                                    type: "warning",
                                    confirmButtonColor: "#1ab394"
                                });
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('Erro ao assinar eletronicamente o(s) documento(s)');
                        }
                    });
                } else{
                    swal({
                        title: "Aviso!",
                        text: "Marque um documento para assinar!",
                        type: "warning",
                        confirmButtonColor: "#1ab394"
                    });
                }

            }
         });
}

function chamaModalAssinatura()
{
    $('#mySeeAssinatura').modal({});
}

function sucessoEntregaFinanceiro(classificacao,mesAtual,anoAtual)
{
    var funcoes ="";
    if(classificacao==1||classificacao==3)
    {
        funcoes = "MESTRE, SECRETÁRIO, TESOUREIRO DA JUNTA DO ORGANISMO AFILIADO e VERIFICAÇÃO DE CONTAS";
    }else{
        funcoes = "MESTRE, SECRETÁRIO DO ORGANISMO AFILIADO e VERIFICAÇÃO DE CONTAS";
    }
    swal({title: "Sucesso!", text: "O relatório financeiro ("+mesAtual+"/"+anoAtual+") foi entregue. Faltam as seguintes assinaturas: "+funcoes, type: "success", confirmButtonColor: "#1ab394"});
    return false;
}

function entregarFinanceiroMensal(mesAtual,anoAtual,idOrganismoAfiliado,quemEntregou)
{
    var criado = $("#criado").val();

    if(criado==1) {
        swal({
                title: "Você tem certeza?",
                text: "Com essa ação você entrega eletrônicamente e concorda que o documento está em sua ultima versão?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, entrego!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    /*
                    alert(mesAtual);
                    alert(anoAtual);
                    alert(idOrganismoAfiliado);
                    alert(quemEntregou);
                    */
                    $.ajax({
                        type: "post",
                        url: "js/ajax/entregarFinanceiroMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {
                            mesAtual: mesAtual,
                            anoAtual: anoAtual,
                            idOrganismoAfiliado: idOrganismoAfiliado,
                            quemEntregou: quemEntregou
                        }, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (data) {
                            //alert(data);
                            if (data.status == 1) {
                                var funcoes = "";
                                var classificacao = $("#classificacaoOa").val();
                                if (classificacao == 1 || classificacao == 3) {
                                    funcoes = "MESTRE, SECRETÁRIO, TESOUREIRO DA JUNTA DO ORGANISMO AFILIADO e VERIFICAÇÃO DE CONTAS";
                                } else {
                                    funcoes = "MESTRE, SECRETÁRIO e VERIFICAÇÃO DE CONTAS";
                                }
                                swal({
                                    title: "Sucesso!",
                                    text: "O relatório financeiro (" + mesAtual + "/" + anoAtual + ") foi entregue. Faltam as seguintes assinaturas: " + funcoes,
                                    type: "success",
                                    confirmButtonColor: "#1ab394"
                                });
                                $("#entregarFinanceiro").html("<a href='#' onclick='sucessoEntregaFinanceiro(" + classificacao + "," + mesAtual + "," + anoAtual + ")' data-container='body' data-toggle='popover' data-placement='top' aria-describedby='popover408083' id='faltaAssinar'  class='btn btn-info dim btn-large-dim btn-outline'><i class='fa fa-pencil'></i></a>");
                                $("#dataEntrega").html(data.dataEntrega);
                                $("#quemEntregou").html(data.quemEntregou);
                                $("#numeroAssinatura").html(data.numeroAssinatura);
                                $("#faltamAssinar").html(funcoes);
                                return false;
                            } else {
                                swal({
                                    title: "Aviso!",
                                    text: "Houve um problema ao entregar esse documento do relatório financeiro mensal. Avise o Setor de Organismos Afiliados!",
                                    type: "warning",
                                    confirmButtonColor: "#1ab394"
                                });
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('Erro ao entregar o documento do relatório financeiro mensal');
                        }
                    });
                    return false;
                }
            });
    }else{
        swal({
            title: "Aviso!",
            text: "O relatório financeiro deste mês selecionado não começou a ser criado e não pode ser entregue!",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
}

function sucessoEntregaAtividade(classificacao,mesAtual,anoAtual)
{
    var funcoes ="";
    if(classificacao==1||classificacao==3)
    {
        funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
    }else{
        funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
    }
    swal({title: "Sucesso!", text: "O relatório de atividades do mês ("+mesAtual+"/"+anoAtual+") foi entregue. Faltam as seguintes assinaturas: "+funcoes, type: "success", confirmButtonColor: "#1ab394"});
    return false;
}

function entregarAtividadeMensal(mesAtual,anoAtual,idOrganismoAfiliado,quemEntregou)
{
    /*
    alert(mesAtual);
    alert(anoAtual);
    alert(idOrganismoAfiliado);
    alert(quemEntregou);
    */
    var criado = $("#criado").val();

    if(criado==1) {
        swal({
                title: "Você tem certeza?",
                text: "Com essa ação você entrega eletrônicamente e concorda que o documento está em sua ultima versão?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, entrego!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    /*
                    alert(mesAtual);
                    alert(anoAtual);
                    alert(idOrganismoAfiliado);
                    alert(quemEntregou);
                    */
                    $.ajax({
                        type: "post",
                        url: "js/ajax/entregarAtividadeMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {
                            mesAtual: mesAtual,
                            anoAtual: anoAtual,
                            idOrganismoAfiliado: idOrganismoAfiliado,
                            quemEntregou: quemEntregou
                        }, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (data) {
                            //alert(data);
                            if (data.status == 1) {
                                var funcoes = "";
                                var classificacao = $("#classificacaoOa").val();
                                if (classificacao == 1 || classificacao == 3) {
                                    funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                                } else {
                                    funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                                }
                                swal({
                                    title: "Sucesso!",
                                    text: "O relatório mensal de atividades (" + mesAtual + "/" + anoAtual + ") foi entregue. Faltam as seguintes assinaturas: " + funcoes,
                                    type: "success",
                                    confirmButtonColor: "#1ab394"
                                });
                                $("#entregarX").html("<a href='#' onclick='sucessoEntregaAtividade(" + classificacao + "," + mesAtual + "," + anoAtual + ")' data-container='body' data-toggle='popover' data-placement='top' aria-describedby='popover408083' id='faltaAssinar'  class='btn btn-info dim btn-large-dim btn-outline'><i class='fa fa-pencil'></i></a>");
                                $("#dataEntrega").html(data.dataEntrega);
                                $("#quemEntregou").html(data.quemEntregou);
                                $("#numeroAssinatura").html(data.numeroAssinatura);
                                $("#faltamAssinar").html(funcoes);
                                return false;
                            } else {
                                swal({
                                    title: "Aviso!",
                                    text: "Houve um problema ao entregar esse documento do relatório mensal de atividades. Avise o Setor de Organismos Afiliados!",
                                    type: "warning",
                                    confirmButtonColor: "#1ab394"
                                });
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('Erro ao entregar o documento do relatório de atividades mensal');
                        }
                    });
                    return false;
                }
            });
    }else{
        swal({
            title: "Aviso!",
            text: "O relatório de atividades deste mês selecionado não começou a ser criado e não pode ser entregue!",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
}

function sucessoEntregaAtividadeOgg(classificacao,mesAtual,anoAtual)
{
    var funcoes ="";
    if(classificacao==1||classificacao==3)
    {
        funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
    }else{
        funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
    }
    swal({title: "Sucesso!", text: "O relatório de atividades OGG do mês ("+mesAtual+"/"+anoAtual+") foi entregue. Faltam as seguintes assinaturas: "+funcoes, type: "success", confirmButtonColor: "#1ab394"});
    return false;
}

function entregarOggMensal(mesAtual,anoAtual,idOrganismoAfiliado,quemEntregou)
{
    /*
    alert(mesAtual);
    alert(anoAtual);
    alert(idOrganismoAfiliado);
    alert(quemEntregou);
    */
    var criado = $("#criado").val();

    if(criado==1) {
        swal({
                title: "Você tem certeza?",
                text: "Com essa ação você entrega eletrônicamente e concorda que o documento está em sua ultima versão?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, entrego!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    /*
                    alert(mesAtual);
                    alert(anoAtual);
                    alert(idOrganismoAfiliado);
                    alert(quemEntregou);
                    */
                    $.ajax({
                        type: "post",
                        url: "js/ajax/entregarAtividadeOGGMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {
                            mesAtual: mesAtual,
                            anoAtual: anoAtual,
                            idOrganismoAfiliado: idOrganismoAfiliado,
                            quemEntregou: quemEntregou
                        }, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (data) {
                            //alert(data);
                            if (data.status == 1) {
                                var funcoes = "";
                                var classificacao = $("#classificacaoOa").val();
                                if (classificacao == 1 || classificacao == 3) {
                                    funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                                } else {
                                    funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                                }
                                swal({
                                    title: "Sucesso!",
                                    text: "O relatório mensal OGG de atividades (" + mesAtual + "/" + anoAtual + ") foi entregue. Faltam as seguintes assinaturas: " + funcoes,
                                    type: "success",
                                    confirmButtonColor: "#1ab394"
                                });
                                $("#entregarX").html("<a href='#' onclick='sucessoEntregaAtividadeOgg(" + classificacao + "," + mesAtual + "," + anoAtual + ")' data-container='body' data-toggle='popover' data-placement='top' aria-describedby='popover408083' id='faltaAssinar'  class='btn btn-info dim btn-large-dim btn-outline'><i class='fa fa-pencil'></i></a>");
                                $("#dataEntrega").html(data.dataEntrega);
                                $("#quemEntregou").html(data.quemEntregou);
                                $("#numeroAssinatura").html(data.numeroAssinatura);
                                $("#faltamAssinar").html(funcoes);
                                return false;
                            } else {
                                swal({
                                    title: "Aviso!",
                                    text: "Houve um problema ao entregar esse documento do relatório mensal OGG de atividades. Avise o Setor de Organismos Afiliados!",
                                    type: "warning",
                                    confirmButtonColor: "#1ab394"
                                });
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('Erro ao entregar o documento do relatório OGG de atividades mensal');
                        }
                    });
                    return false;
                }
            });
    }else{
        swal({
            title: "Aviso!",
            text: "O relatório de atividades OGG deste mês selecionado não começou a ser criado e não pode ser entregue!",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
}

function sucessoEntregaAtividadeColumba(classificacao,trimestreAtual,anoAtual)
{
    var funcoes ="";
    if(classificacao==1||classificacao==3)
    {
        funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
    }else{
        funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
    }
    swal({title: "Sucesso!", text: "O relatório de atividades das Columbas do trimestre ("+trimestreAtual+"/"+anoAtual+") foi entregue. Faltam as seguintes assinaturas: "+funcoes, type: "success", confirmButtonColor: "#1ab394"});
    return false;
}

function entregarColumbaTrimestral(trimestreAtual,anoAtual,idOrganismoAfiliado,quemEntregou)
{
    /*
    alert(mesAtual);
    alert(anoAtual);
    alert(idOrganismoAfiliado);
    alert(quemEntregou);
    */
    var criado = $("#criado").val();

    if(criado==1) {
        swal({
                title: "Você tem certeza?",
                text: "Com essa ação você entrega eletrônicamente e concorda que o documento está em sua ultima versão?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, entrego!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    /*
                    alert(mesAtual);
                    alert(anoAtual);
                    alert(idOrganismoAfiliado);
                    alert(quemEntregou);
                    */
                    $.ajax({
                        type: "post",
                        url: "js/ajax/entregarAtividadeColumbaTrimestral.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {
                            trimestreAtual: trimestreAtual,
                            anoAtual: anoAtual,
                            idOrganismoAfiliado: idOrganismoAfiliado,
                            quemEntregou: quemEntregou
                        }, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (data) {
                            //alert(data);
                            if (data.status == 1) {
                                var funcoes = "";
                                var classificacao = $("#classificacaoOa").val();
                                if (classificacao == 1 || classificacao == 3) {
                                    funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                                } else {
                                    funcoes = "MESTRE e SECRETÁRIO DO ORGANISMO AFILIADO";
                                }
                                swal({
                                    title: "Sucesso!",
                                    text: "O relatório trimestral de Columbas (" + trimestreAtual + "/" + anoAtual + ") foi entregue. Faltam as seguintes assinaturas: " + funcoes,
                                    type: "success",
                                    confirmButtonColor: "#1ab394"
                                });
                                $("#entregarX").html("<a href='#' onclick='sucessoEntregaAtividadeColumba(" + classificacao + "," + trimestreAtual + "," + anoAtual + ")' data-container='body' data-toggle='popover' data-placement='top' aria-describedby='popover408083' id='faltaAssinar'  class='btn btn-info dim btn-large-dim btn-outline'><i class='fa fa-pencil'></i></a>");
                                $("#dataEntrega").html(data.dataEntrega);
                                $("#quemEntregou").html(data.quemEntregou);
                                $("#numeroAssinatura").html(data.numeroAssinatura);
                                $("#faltamAssinar").html(funcoes);
                                return false;
                            } else {
                                swal({
                                    title: "Aviso!",
                                    text: "Houve um problema ao entregar esse documento do relatório trimestral de Columbas. Avise o Setor de Organismos Afiliados!",
                                    type: "warning",
                                    confirmButtonColor: "#1ab394"
                                });
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('Erro ao entregar o documento do relatório OGG de atividades mensal');
                        }
                    });
                    return false;
                }
            });
    }else{
        swal({
            title: "Aviso!",
            text: "O relatório de atividades de Columbas deste trimestre selecionado não começou a ser criado e não pode ser entregue!",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
}

function informarCienciaAssinaturaEletronica()
{
    var login = $("#login").val();

    swal({
            title: "Você tem certeza?",
            text: "Com essa ação você informa para o sistema que esse usuário está ciente da assinatura eletrônica",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim! Prossiga!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {

                $.ajax({
                    type: "post",
                    url: "js/ajax/informarCienciaAssinaturaEletronica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                    dataType: "json",
                    data: {
                        login: login
                    }, // exemplo: {codigo:125, nome:"Joaozinho"}
                    success: function (data) {
                        //salert(data);
                        if (data.status == 1) {

                            swal({
                                title: "Sucesso!",
                                text: "O login (" + login + ") já pode acessar o SOA normalmente!",
                                type: "success",
                                confirmButtonColor: "#1ab394"
                            });

                            return false;
                        } else {
                            if (data.status == 2) {
                                swal({
                                    title: "Aviso!",
                                    text: "Login informado não está na lista de usuários do SOA!",
                                    type: "warning",
                                    confirmButtonColor: "#1ab394"
                                });
                                return false;
                            }else{
                                if (data.status == 3) {
                                    swal({
                                        title: "Aviso!",
                                        text: "Essa pessoa já tinha confirmado que estava ciente da Assinatura Eletrônica!",
                                        type: "warning",
                                        confirmButtonColor: "#1ab394"
                                    });
                                    return false;
                                }else {
                                    swal({
                                        title: "Aviso!",
                                        text: "Houve algum problema ao informar que esse usuário está ciente da Assinatura Eletrônica! Por favor avise a TI!",
                                        type: "warning",
                                        confirmButtonColor: "#1ab394"
                                    });
                                    return false;
                                }
                            }
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Erro avise a TI');
                    }
                });
            }
        });
}

function tabelaTempoCasa()
{
    swal({
        title: "Para liberar iniciações para Companheiro",
        text: "1º Grau de Tempo: 1 ano e 6 meses de admissão\n" +
        "2º Grau de Tempo: 1 ano e 9 meses de admissão\n" +
        "3º Grau de Tempo: 2 anos de admissão\n" +
        "4º Grau de Tempo: 2 anos e 3 meses de admissão\n" +
        "5º Grau de Tempo: 2 anos e 6 meses de admissão\n" +
        "6º Grau de Tempo: 2 anos e 9 meses de admissão\n" +
        "7º Grau de Tempo: 3 anos e 3 meses de admissão\n" +
        "9º Grau de Tempo: 4 anos e 9 meses de admissão\n" +
        "10º Grau de Tempo: 5 anos e 9 meses de admissão\n" +
        "11º Grau de Tempo: 8 anos de admissão\n" +
        "12º Grau de Tempo: 11 anos e 3 meses de admissão\n"
        ,
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}

function alterarUsuario()
{
    var seqCadastUsuario;
    var letra;
    var letraNome;

    seqCadastUsuario= $("#seqCadastUsuario").val();
    letra= $("#letra").val();
    letraNome= $("#letraNome").val();

    location.href='?corpo=alteraUsuario&seqCadast='+seqCadastUsuario+'&letra='+letra+'&letraNome='+letraNome;
}

function atualizarCodigoDeAfiliacao(seqCadast,codigoDeAfiliacaoAtual)
{
    $.ajax({
        type: "post",
        url: "js/ajax/atualizarCodigoDeAfiliacao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
            seqCadast: seqCadast,
            codigoDeAfiliacaoAtual:codigoDeAfiliacaoAtual
        }, // exemplo: {codigo:125, nome:"Joaozinho"}
        success: function (data) {
            //alert(data.status);
            if (data.status == 1) {
                $("#codigoDeAfiliacao"+seqCadast).html(data.codigoDeAfiliacao);
                swal({
                    title: "Sucesso!",
                    text: "Código de Afiliação atualizado com sucesso!",
                    type: "success",
                    confirmButtonColor: "#1ab394"
                });

                return false;
            } else {
                if (data.status == 2) {
                    swal({
                        title: "Aviso!",
                        text: "O código de Afiliação permanece o mesmo! Não precisou ser atualizado. Nesse caso não houve dualização ou desdualização do cadastro do membro!",
                        type: "warning",
                        confirmButtonColor: "#1ab394"
                    });
                    return false;
                }else{
                    swal({
                        title: "Aviso!",
                        text: "Houve algum problema ao atualizar o código de afiliação! Por favor avise a TI!",
                        type: "warning",
                        confirmButtonColor: "#1ab394"
                    });
                    return false;
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Erro avise a TI');
        }
    });

}

function removerTodasAsAtividadesRecorrentesAgendaPortal(id)
{


    swal({
            title: "Você tem certeza?",
            text: "Com essa ação você excluirá todas as atividades recorrentes do Organismo.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim! Prossiga!",
            cancelButtonText: "Não, cancele!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                location.href='?corpo=buscaAgendaAtividade&removerAtividadesRecorrentes=true&idOa='+id;
            }
        }
        );
}