var numitens=6; //quantidade de itens a ser mostrado por página
var pagina=1;	//página inicial


$("#pesquisar").keyup(function() {
    var pesquisa = $(this).val();
    //console.log(pesquisa);
    getitens(1,numitens,pesquisa);
});


$(document).ready(function(){
	
	getitens(pagina,numitens);
	
})

function getitens(pag, maximo,pesquisa){
	
	if(pesquisa == undefined){
		var pesquisa = '';
	}
	//console.log("Conteúdo de pesquisa na função getitens(): "+pesquisa);
	
	pagina=pag; 
	$.ajax({
	type: 'GET',
	data: 'tipo=listagem&pag='+pag +'&maximo='+maximo+'&pesquisa='+pesquisa,
	url:'js/ajax/paginadorPerfilUsuarioContatos.php',
   	success: function(retorno){
			$('#conteudo').html(retorno);
			$('#linha_tracejada').addClass('hr-line-dashed');
			
	    	contador(pesquisa);
	    	
	    	if(retorno==false){
	    		$('#conteudo').html('<center>Nenhum contato encontrado!</center>');
	    		$('#linha_tracejada').removeAttr('class');
	    	}
	 	}
    })
}

function contador(pesquisa){
	//console.log("Conteúdo da pesquisa na função contador(): "+pesquisa);
	$.ajax({
      	type: 'GET',
      	data: 'tipo=contador&pesquisa='+pesquisa,
      	url:'js/ajax/paginadorPerfilUsuarioContatos.php',
   		success: function(retorno_pg){
			//console.log(retorno_pg);
        	paginador(retorno_pg,pesquisa);
      	}
    })
}

function paginador(cont,pesquisa){
	
	if(pesquisa == undefined){
		var pesquisa = '';
	}
	//console.log("Conteúdo de pesquisa na função paginador(): "+pesquisa);
	if(pesquisa!=''){
		var pesq = ", '"+pesquisa+"'";
	} else {
		var pesq = '';
	}
	//console.log("Conteúdo de pesquisa na função paginador(): "+pesq);
	
	if(cont<=numitens){
		
		$('#paginador').html('');
		$('#linha_tracejada').removeAttr('class');
		
	} else {
		
		$('#paginador').html('');

		var qtdpaginas=Math.ceil(cont/numitens);

		if((pagina-5) >= 1){
			$('#paginador').append('<a class="btn btn-white" onclick="getitens('+(1)+', '+numitens+pesq+');"><i class="fa fa-chevron-left"><i class="fa fa-chevron-left"> </a>');
		}
		if(pagina!=1){
			$('#paginador').append('<a class="btn btn-white" onclick="getitens('+(pagina-1)+', '+numitens+pesq+');"><i class="fa fa-chevron-left"> </a>');
		}
		for(var i=1;i<=qtdpaginas;i++){
			if(((i-5) < pagina) && (i+5) > pagina){
				if(pagina==i){
					$('#paginador').append('<a class="btn btn-white active" onclick="getitens('+i+', '+numitens+pesq+');">'+i+'</a>');
				} else {
					$('#paginador').append('<a class="btn btn-white" onclick="getitens('+i+', '+numitens+pesq+');">'+i+'</a>');
				}
			}
		}
		if(pagina!=qtdpaginas){
			$('#paginador').append('<a class="btn btn-white" onclick="getitens('+(pagina+1)+', '+numitens+pesq+');"><i class="fa fa-chevron-right"></i> </a>');
		}
		if((pagina+5) <= qtdpaginas){
			$('#paginador').append('<a class="btn btn-white" onclick="getitens('+(qtdpaginas)+', '+numitens+pesq+');"><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i> </a>');
		}
	}
}
			