/**
 * Criado em 17/07/2015.
 * Arquivo de Funcoes em JavaScript do Ruan
 */

$(document).ready(function() {
    $('#opcoesImp').change(function(){
        $('#outroMotivo').hide(300);
        if($(this).val()!=2){
            $('#outroMotivo').hide(300);
        } else {
            $('#outroMotivo').show(300);
        } 
    });
    
    if($('#opcoesImp').val() != 2){
        $('#outroMotivo').hide(300);
    }
    
});

function validaCampoMotivo() {

    var opcao = document.imp2.opcoesImp;
    var motivo = document.imp2.motivoImpressao.value;

    if(document.imp2.termo.value === '0') {
        swal("Termo de Compromisso", "Você precisa concordar com o Termo de Compromisso para imprimir este documento!", "info");
        document.getElementById("textoTermo").focus();
        return false;
    }
    
    if(opcao.value >= 0) {
        if(opcao.value == 2) {
            if(motivo.length <= 6) {
                swal("Motivo", "Especifique melhor o motivo. É necessário ter mais de 6 (seis) caracteres para o motivo ser válido.", "info");
                return false;
            }
        }
        document.getElementById("imp2").submit();
    }
    else {
        swal("...Motivo?", "Você esqueceu de especificar o motivo dessa impressão!", "error");
        opcao.focus();
        return false;
    }
}

function validaCampoExperimento() {
        if(document.getElementById("temExperimento").value === "") {
            alert("Preencha corretamente o campo Experimento!");
            document.getElementById("temExperimento").focus();
            return false;
        }
        
        return true;
}

function excluirUploadDiscurso(id)
{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse arquivo!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false },
    function (isConfirm) {
        if (isConfirm) {
            window.location = 'painelDeControle.php?corpo=buscaDiscurso&delfile='+id+'&excluido=1';
        } else {
            swal({title:"Cancelado",text:"Nós não excluímos o arquivo :)",type:"error",confirmButtonColor:"#1ab394"});
        }
    });	
}

$(document).ready(function(){
    
	$('#datapicker_discurso .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    });
    
});
/*
$(document).ready(function() {
    var btnImp = $('input[type=submit][name=okImprimir]');
    btnImp.prop('disabled', true);
    
   $('input[type=radio][name=termo]').change(function() {
      if($(this).val() === '0') {
        btnImp.prop('disabled', true);
      }
      else if ($(this).val() === '1') {
        btnImp.prop('disabled', false);
      }
   });
});
*/
function validaConcordanciaImpressao() {

    if(document.imp3.termo.value === '0') {
        swal("Termo de Compromisso", "Você precisa concordar com o Termo de Compromisso para imprimir este documento!", "info");
        document.getElementById("textoTermo").focus();
        return false;
    }

    document.getElementById("imp3").submit();

}

function excluirDiscurso(id)
{
    swal({
        title: "Você tem certeza?",
        text: "Com essa ação você excluirá definitivamente esse discurso!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false },
    function (isConfirm) {
        if (isConfirm) {
            window.location = 'painelDeControle.php?corpo=buscaDiscurso&delspeech='+id+'&excluido=2';
        } else {
            swal({title:"Cancelado",text:"Nós não excluímos o discurso :)",type:"error",confirmButtonColor:"#1ab394"});
        }
    });	
}