
    function isset () {
        // !No description available for isset. @php.js developers: Please update the function summary text file.
        //
        // version: 1103.1210
        // discuss at: http://phpjs.org/functions/isset
        // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: FremyCompany
        // +   improved by: Onno Marsman
        // +   improved by: Rafał Kukawski
        // *     example 1: isset( undefined, true);
        // *     returns 1: false
        // *     example 2: isset( 'Kevin van Zonneveld' );
        // *     returns 2: true
        var a = arguments,
            l = a.length,
            i = 0,
            undef;

        if (l === 0) {
            throw new Error('Empty isset');
        }

        while (i !== l) {
            if (a[i] === undef || a[i] === null) {
                return false;
            }
            i++;
        }
        return true;
    }

	/**
	 * Chosen select http://harvesthq.github.io/chosen/
	 */
	var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }


    function abrirModal() {
		$("#mySee").modal('show');
	}

	function abrirPopUp() {
		window.open('pasta/arquivo.php?codigo1='+codigo1+'&codigo2='+codigo2,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
	}

	function print_r(printthis, returnoutput) {
	    var output = '';

	    if($.isArray(printthis) || typeof(printthis) == 'object') {
	        for(var i in printthis) {
	            output += i + ' : ' + print_r(printthis[i], true) + '\n';
	        }
	    }else {
	        output += printthis;
	    }
	    if(returnoutput && returnoutput === true) {
	        return output;
	    }else {
	        console.log(output);
	    }
	}

	function getValue() {

		var Values = {};

		//Nos Input padrões
		Values['input']			= $("#input").val();

		//No Summernote
		var summernoteOne = $('#summernote_01.summernote').code();
		Values['summernote_01']		= summernoteOne;

		//Nos Checkboxs padrões
		if ($("input[type=checkbox][name='checkboxX[]").is(':checked')){
			var checkBoxVal = 1;
		} else {
			var checkBoxVal = 0;
		}
		Values['checkBoxVal']		= checkBoxVal;

		//print_r(Values);
		return Values;
	}

	function salvaRascunho()
	{
		// Pega os campos gerais
		var values = getValue();
		var camposGeraisData = JSON.stringify(values);

		// Starta item de 'Salvando rascunho'
		$("#rascunho").html('<img src="img/ajax-loaders/ajax-loader-1.gif" title="Salvando..."> Salvando rascunho');

		$.ajax({
			type:"post",
			url: "ajax/arquivo.php",
			dataType: "json",
			data: {camposGerais:camposGeraisData},
			success: function(retorno){
				//console.log(retorno);
				if (retorno.erro != undefined && retorno.erro.length > 0) {

					var erro = 'Ocorreram um ou mais erros:\n\n';
					for (var i =0; i < retorno.erro.length; i++) {
						erro += retorno.erro[i] + '\n';
					}
					alert(erro);
				}
				else if (retorno.sucesso != undefined) {
					$("#rascunho").html('');
					return false;
				}
				else {
					alert("Ocorreu algum erro. Entre em contato com o setor de TI.");
				}
			},

			error: function(xhr, textStatus, errorThrown){
				//console.log("error");
			}
		});
	}

	function cadastra()
	{
		//if(validaRelatorioGC())
		//{
			// Pega os campos gerais
			var camposAtaOaMensal = getCamposAtaOaMensal();
			var camposGeraisData = JSON.stringify(camposAtaOaMensal);

			$.ajax({
				type:"post",
				url: "ajax/ajax_cadastra_ata_oa_mensal.php",
				dataType: "json",
				data: {camposGerais:camposGeraisData},
				success: function(retorno){

					if (retorno.erro != undefined && retorno.erro.length > 0) {
						var erro = 'Ocorreram um ou mais erros:\n\n';
						for (var i =0; i < retorno.erro.length; i++) {
							erro += retorno.erro[i] + '\n';
						}
						alert(erro);
					}
					else if (retorno.sucesso != undefined) {
						alert(retorno.sucesso);
						location.href='?opcao=relatorio_gc.php';
					}
					else {
						alert("Ocorreu algum erro. Entre em contato com o setor de TI.");
					}
				},

				error: function(xhr, textStatus, errorThrown){

				}
			});
		//}
	}

	function insertDadosVindoDeInputCarregandoSelect(fk_da_tabela_onde_sera_trazidos_todos_os_dados)
	{
		// Pega os campos gerais
		var camposAtaOaMensalOficiais = getDadosAtaOaMensalOficiais();
		var camposGeraisData = JSON.stringify(camposAtaOaMensalOficiais);

		$.ajax({
			type:"post",
			url: "ajax/arquivo.php",
			dataType: "json",
			data: {camposGerais:camposGeraisData,fkata:fk_da_tabela_onde_sera_trazidos_todos_os_dados},
			success: function(retorno){
				//console.log(retorno);
				if (retorno.erro != undefined && retorno.erro.length > 0) {
					var erro = 'Ocorreram um ou mais erros:\n\n';
					for (var i =0; i < retorno.erro.length; i++) {
						erro += retorno.erro[i] + '\n';
					}
					alert(erro);
				}
				else if (retorno.sucesso != undefined) {
					alert(retorno.sucesso);
					//Inclui agora dentro de outra estrutura, no caso um Multi Select
					getVindoDeInputCarregandoSelect(fk_da_tabela_onde_sera_trazidos_todos_os_dados);
				}
				else {
					alert("Ocorreu algum erro. Entre em contato com o setor de TI.");
				}
			},

			error: function(xhr, textStatus, errorThrown){
				//console.log("error");
			}
		});
	}
	//Multi Select
	function getDadosVindoDeInputCarregandoSelect(fk_da_tabela_onde_sera_trazidos_todos_os_dados) {
		$.ajax({
			type:"post",
			url: "ajax/arquivo.php",
			dataType: "json",
			data: {fkata:fkata},
			success: function(data){
				//console.log("data retornou");

				//Itens que entram
				//Com ou sem 'selected=""'
				//<option selected="" value="1" id="ata_oa_mensal_oficial1">0&nbsp;[52293]&nbsp;OLIVEIRO ANGELO PANZERA</option>

				// Área de itens selecionados/ativados
				//<li class="search-choice"><span>0&nbsp;[52293]&nbsp;OLIVEIRO ANGELO PANZERA</span><a class="search-choice-close" data-option-array-index="0"></a></li>

				// Área de seleção com itens ocultos já selecionados e itens em evidência
				//Já selecionado
				//<li class="result-selected" data-option-array-index="0">0&nbsp;[52293]&nbsp;OLIVEIRO ANGELO PANZERA</li>
				//Esperando seleção
				//<li class="active-result" data-option-array-index="2">2&nbsp;[243395]&nbsp;VERA LUCIA SILVA MENDES</li>

				$("#ata_oa_mensal_oficiais").html('');
				$(".chosen-choices").html('');
				$(".chosen-results").html('');

				if(data.temOficial!=0)
				{
					for (var i = 0; i < data.oficial.length; i++) {

						$("#ata_oa_mensal_oficiais").append(data.oficial[i].option_oficial);
						$(".chosen-choices").append(data.oficial[i].option_select);
						$(".chosen-results").append(data.oficial[i].option_results);

					}
				}
				//$(".chosen-choices").append('<li class="search-field"><input type="text" value="Nenhum oficial selecionado..." class="" autocomplete="off" style="width: 25px;" tabindex="4"></li>');
			},

			error: function(xhr, textStatus, errorThrown){
				//console.log("error");
				//alert("error");
				/*
				$("#ata_oa_mensal_oficiais").html('');
				$(".chosen-choices").html('');
				$(".chosen-results").html('');
				*/

				//if(data.temOficial!=0)
				//{
					//alert("Entrou...temOficial!=0");
					//alert(data.temOficial.val());
					//alert(data.oficial.length);
					//alert(data.oficial);
					//print_rCode(data.oficial.temOficial)
					/*
					$("#ata_oa_mensal_oficiais").append('<option selected="" value="1" id="ata_oa_mensal_oficial1">[52293]&nbsp;OLIVEIRO ANGELO PANZERA</option>');
					$("#ata_oa_mensal_oficiais").append('<option selected="" value="2" id="ata_oa_mensal_oficial2">[168094]&nbsp;RAIMUNDO NONATO DE ARAUJO</option>');
					$("#ata_oa_mensal_oficiais").append('<option value="3" id="ata_oa_mensal_oficial3">[243395]&nbsp;VERA LUCIA SILVA MENDES</option>');
					$("#ata_oa_mensal_oficiais").append('<option value="4" id="ata_oa_mensal_oficial4">[88239]&nbsp;PEDRO PANHAN DA SILVA</option>');
					$("#ata_oa_mensal_oficiais").append('<option selected="" value="79" id="ata_oa_mensal_oficial79">[225010]&nbsp;EWERTON SCHAEDLER</option>');
					*/
					//alert("Passou...Popula SELECT");
					/*
					$(".chosen-choices").append('<li class="search-choice"><span>[52293]&nbsp;OLIVEIRO ANGELO PANZERA</span><a class="search-choice-close" data-option-array-index="0"></a></li>');
					$(".chosen-choices").append('<li class="search-choice"><span>[168094]&nbsp;RAIMUNDO NONATO DE ARAUJO</span><a class="search-choice-close" data-option-array-index="1"></a></li>');
					$(".chosen-choices").append('<li class="search-choice"><span>[225010]&nbsp;EWERTON SCHAEDLER</span><a class="search-choice-close" data-option-array-index="4"></a></li>');
					*/
					//alert("Passou...Popula CHOSEN");

					//$(".chosen-results").html('<li class="result-selected" data-option-array-index="0">[52293]&nbsp;OLIVEIRO ANGELO PANZERA</li><li class="result-selected" data-option-array-index="1">[168094]&nbsp;RAIMUNDO NONATO DE ARAUJO</li><li class="active-result" data-option-array-index="2">[243395]&nbsp;VERA LUCIA SILVA MENDES</li><li class="active-result" data-option-array-index="3">[88239]&nbsp;PEDRO PANHAN DA SILVA</li><li class="result-selected" data-option-array-index="4">[225010]&nbsp;EWERTON SCHAEDLER</li>');
					//alert("Passou...Popula RESULTS");
					/*
					for (var i = 0; i < data.oficial.length; i++) {
						//alert("popula na tela");
						alert("data.oficial[i]");
						$("#ata_oa_mensal_oficiais").append(data.oficial[i].option_oficial);
					}
					alert("Passou...");
					*/
				//}
			}
		});
	}
	/**
	 * Carrega tabela com anexo vindo do uploadify.
	 * @param Int 'posicao' valor vindo de um loop que diz quando a linha.
	 * @param Int 'fkTurno' quando usa turno/protocolo, que nunca se repete a cada carregamento.
	 * @param Int 'fkDocumento' ID do qual o anexo faz parte.
	 * @return {string} Conteúdo em HTML.
	*/
	function getAnexosEnviandoParaTabela(posicao,fkTurno,fkDocumento) {

		$.ajax({
			//method: "post",
			type:"post",
			url: "ajax/arquivo.php",
			//async: false,
			//sycr: false,
			dataType: "json",
			data: {id:idDocumento,posicao:posicao,fkTurno:fkTurno,fkDocumento:fkDocumento},
			success: function(data){
				//console.log(data);

				$("#list_anexos_gestao_imovel_controle").html('');

				if(data.temAnexo!=0)
				{
					for (var i = 0; i < data.anexo.length; i++) {

						$("#list_anexos_gestao_imovel_controle").append(data.anexo[i].nome_arquivo);
						//$("#list_anexos_gestao_imovel_controle").append('<tr><td>94</td><td><center>Escritura</center></td><td><center><a href="http://localhost/soa/controle_anexo/gestao_imovel_controle/4cff3d2b9af2d147a2aa3c343b7f8d47.pdf" target="_blank">asdasda...&nbsp;<i class="fa fa-share"></i></a></center></td><td><a style="cursor: pointer" class="btn btn-xs btn-danger" onclick="excluirAnexoControleImovel(94,0,2)"><i class="fa fa-trash-o fa-white"></i></a></td></tr><tr><td>94</td>');
					}
				}
			},
			error: function(xhr, textStatus, errorThrown){
				//console.log('error');

				$("#list_anexos_gestao_imovel_controle").html('');

				$("#list_anexos_gestao_imovel_controle").append('<tr><td colspan="4" style="color: #FA8072">Ocorreu um erro ao buscar os documentos anexados!</td></tr>');
			}

		});

	}

	function getEstados(idPais, idImovel) {
		//console.log("idPais: " + idPais);
		if (idPais != '' && idPais != undefined) {
			$.ajax({
				type:"post",
				url: "js/ajax/getEstados.php",
				dataType: "json",
				data: {idPais:idPais,idImovel:idImovel},
				success: function(data){
					//console.log(data);
					if(data.temImoveis!=0){
						//console.log('Quantidade de imóveis: '+data.qtdImoveis);
						$("#estadoImvl").html('');
						var funcao = "getCidades(this.value, '#cidadeImovel');";
						$("#estadoImvl").append('<label class="col-sm-2 control-label">Estado: </label><div class="col-sm-10"><select class="form-control col-sm-3" name="estadoImovel" id="estadoImovel" style="max-width: 400px" onchange="'+funcao+'" required="required"></select></div>');
						$("#estadoImovel").append('<option value="0">Selecione...</option>');
						$("#estadoImovel").append(data.imoveis.options)
					} else {
						$("#estadoImvl").html('');
						$("#estadoImvl").append('<label class="col-sm-2 control-label">Estado: </label><div class="col-sm-10"><p class="form-control-static" style="color: #245269"><b>Nenhum estado encontrada!</b></p></div>');
					}
				},
				error: function(xhr, textStatus, errorThrown){
					//console.log('error');
					$("#estadoImvl").html('');
					$("#estadoImvl").append('<label class="col-sm-2 control-label">Estado: </label><div class="col-sm-10"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar o estado! Atualize a página.</b></p></div>');
				}
			});
		}
		//if ($('#estadoImovel').val()==0) {
			$("#cidadeImvl").html('');
			$("#cidadeImvl").append('<label class="col-sm-2 control-label">Cidade: </label><div class="col-sm-10"><select class="form-control col-sm-3" name="cidadeImovel" id="cidadeImovel" style="max-width: 400px" required="required"></select></div>');
			$("#cidadeImovel").append('<option value="0" selected="selected">Selecione o estado.</option>');
		//}
	}
	$(document).ready(function() {
		if($('#paisImovel').val()!=0){
			getEstados($('#paisImovel').val(),$('#idAlteraImovel').val());
		}
	});
    function getCidades(idEstado, idImovel) {
        if (idEstado != '' && idEstado != undefined) {
            $.ajax({
                type:"post",
                url: "js/ajax/getCidades.php",
                dataType: "json",
                data: {idEstado:idEstado,idImovel:idImovel},
                success: function(data){
                    //console.log(data);
                    if(data.temImoveis!=0){
                        //console.log('Quantidade de imóveis: '+data.qtdImoveis);
                        $("#cidadeImvl").html('');
                        $("#cidadeImvl").append('<label class="col-sm-2 control-label">Cidade: </label><div class="col-sm-10"><select class="form-control col-sm-3" name="cidadeImovel" id="cidadeImovel" style="max-width: 400px" required="required"></select></div>');
                        $("#cidadeImovel").append('<option value="0">Selecione...</option>');
                        $("#cidadeImovel").append(data.imoveis.options)
                    } else {
                        $("#cidadeImvl").html('');
                        $("#cidadeImvl").append('<label class="col-sm-2 control-label">Cidade: </label><div class="col-sm-10"><p class="form-control-static" style="color: #245269"><b>Nenhuma cidade encontrada!</b></p></div>');
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    //console.log('error');
                    $("#cidadeImvl").html('');
                    $("#cidadeImvl").append('<label class="col-sm-2 control-label">Cidade: </label><div class="col-sm-10"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar a cidade! Atualize a página.</b></p></div>');
                }
            });
        }
    }
    $(document).ready(function() {
        if($('#estadoImovel').val()!=0){
            getCidades($('#estadoImovel').val(),$('#idAlteraImovel').val());
        }
    });

	/**
	 * Função responsável por carregar os imóveis referentes a um Organismo escolhido
	 * @param Int oa : ID do Organismo Afiliado sendo passado
	 * @param Int ic : ID do Controle de Documentos que está aberto (utilizado no carregamento do alteraImovelControle, onde já vem com um OA 'selected')
	 * Exibe o Select de Imóveis
	 */
    function getImoveis(oa,ic,imovel) {
        //console.log(oa);
        $.ajax({
            //method: "post",
            type:"post",
            url: "js/ajax/getImoveis.php",
            //async: false,
            //sycr: false,
            dataType: "json",
            data: {oa:oa,ic:ic,imovel:imovel},
            success: function(data){
                //console.log(data);
                if(data.temImoveis!=0){
                	//console.log(data);
                	$("#idImovel").html('');
                	$("#idImovel").append('<label class="col-sm-2 control-label">Imóvel: </label><div class="col-sm-10"><select class="form-control col-sm-3" onchange="mudaImovelGetAno(this.value);" name="fk_idImovel" id="fk_idImovel" style="max-width: 400px" required="required"></select></div>');
                    $("#fk_idImovel").append('<option value="">Selecione...</option>');
                	$("#fk_idImovel").append(data.imoveis.options)
                } else {
                	$("#idImovel").html('');
                	$("#idImovel").append('<label class="col-sm-2 control-label">Imóvel: </label><div class="col-sm-10"><p class="form-control-static" style="color: #245269"><b>Nenhum Imóvel cadastrado nesse Organismo Afiliado!</b></p></div>');
                	$('#referenteAno').hide(300);
                }
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $("#idImovel").html('');
                $("#idImovel").append('<label class="col-sm-2 control-label">Imóvel: </label><div class="col-sm-10"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar os imóveis! Atualize a página.</b></p></div>');
                $('#referenteAno').hide(300);
            }
        });
    }
    $(document).ready(function() {

        $('#fk_idOrganismoAfiliado').bind('change', function(){
            $('#idImovel').hide(300);
            if($(this).val()==0){
                $('#idImovel').hide(300);
            } else if($(this).val()!==0 && $(this).val()!==undefined) {
                $('#idImovel').show(300);
                oa = $(this).val();
                getImoveis(oa);
            }
        });
        if($('#fk_idOrganismoAfiliado').val()==0){
            $('#idImovel').hide(300);
        } else if($('#fk_idOrganismoAfiliado').val()!==0 && $('#fk_idOrganismoAfiliado').val()!==undefined){
            $('#idImovel').show(300);

            oa = $('#fk_idOrganismoAfiliado').val();
            ic = $('#idImovelControle').val();
			imovelCadastrar = $('#idImovelCadastrar').val();

            getImoveis(oa,ic,imovelCadastrar);
        }
    });

    $(document).ready(function() {
		//console.log($('#idImovelCadastrar').val());
		if($('#idImovelCadastrar').val()==''){
			$('#referenteAno').hide(300);
		} else if($('#idImovelCadastrar').val()!=='' && $('#idImovelCadastrar').val()!==undefined){
			$('#referenteAno').show(300);

			ano = $('#referenteAnoImovelControleCadastrar').val();

			getAno($('#idImovelCadastrar').val(),ano);
		}
    	//$('#referenteAno').hide(300);
    });

    function mudaImovelGetAno(idImovel) {
        //console.log("ID do Imóvel: "+idImovel);
        $('#referenteAno').hide(300);

        if(idImovel==0){
            $('#referenteAno').hide(300);
        } else if(idImovel!=0) {
            $('#referenteAno').show(300);
            getAno(idImovel);
        } else {
        	$('#referenteAno').hide(300);
        }
    }

    function getAno(idImovel,ano) {
        //console.log(idImovel);
        $.ajax({
            //method: "post",
            type:"post",
            url: "js/ajax/getAno.php",
            //async: false,
            //sycr: false,
            dataType: "json",
            data: {idImovel:idImovel,ano:ano},
            success: function(data){
                //console.log(data);
            	$("#referenteAno").html('');
            	$("#referenteAno").append('<label class="col-sm-2 control-label">Referente ao Ano: </label><div class="col-sm-10"><select class="form-control col-sm-3" name="referenteAnoImovelControle" id="referenteAnoImovelControle" style="max-width: 250px" required="required"></div>');
                $("#referenteAnoImovelControle").append('<option value="">Selecione...</option>');
            	$("#referenteAnoImovelControle").append(data.imoveis.anos)
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $("#referenteAno").html('');
                $("#referenteAno").append('<label class="col-sm-2 control-label">Referente ao Ano: </label><div class="col-sm-10"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar os anos para cadastro! Atualize a página.</b></p></div>');
            }
        });
    }

    /**
     *
     */
    function cadastraTipoDeEscritura() {

        var tipoEscritura;
        tipoEscritura = $("#tipoEscritura").val();

        $.ajax({
            type:"post",
            url: "js/ajax/cadastraTipoDeEscritura.php",
            dataType: "json",
            data: {tipoEscritura:tipoEscritura},
            success: function(retorno){

                if (retorno.erro != undefined && retorno.erro.length > 0) {
                    var erro = 'Ocorreram um ou mais erros:\n\n';
                    for (var i =0; i < retorno.erro.length; i++) {
                        erro += retorno.erro[i];
                    }
                    alert(erro);
                }
                else if (retorno.sucesso != undefined) {
                    alert("Tipo de Escritura registrado com sucesso!");
                    $("#tipoEscritura").val('');
                    $("#alertTipoDeEscritura").html('');
                    $("#alertTipoDeEscritura").append('<div class="alert alert-success" style="text-align: justify">Tipo de Escritura: <a class="alert-link">'+retorno.tipoEscrituraCadastrado+'</a> cadastrado com sucesso.Lembre-se de checar se o novo Tipo de Escritura está selecionado!</center></div>');
                    getTiposDeEscritura();
                }
                else {
                    alert("Ocorreu algum erro. Entre em contato com o setor de TI.");
                }
            },

            error: function(xhr, textStatus, errorThrown){

            }
        });
    }
    function getTiposDeEscritura() {

        $.ajax({
            type:"post",
            url: "js/ajax/getTiposDeEscritura.php",
            dataType: "json",
            data: {},
            success: function(data){
                //console.log(data);
                if(data.temTipoEscritura!=0) {
                    $("#tipoSelect").html('');
                    $("#tipoSelect").append('<label class="col-sm-2 control-label">Tipo de Escritura: </label><div class="col-sm-10"><select class="form-control col-sm-3" name="fk_idTipoEscrituraImovel" id="fk_idTipoEscrituraImovel" style="max-width: 450px"></select><a data-toggle="modal" data-target="#modalTipoDeEscritura" style="margin-left: 10px"><span class="badge badge-primary" style="margin-top: 7px"><i class="fa fa-plus"></i> Cadastrar</span></a></div>');
                    $("#fk_idTipoEscrituraImovel").append(data.escrituras.options)
                } else {
                    $("#tipoSelect").html('');
                    $("#tipoSelect").append('<label class="col-sm-2 control-label">Tipo de Escritura: </label><div class="col-sm-10"><p class="form-control-static" style="color: #245269"><b>Nenhum Tipo de Escritura cadastrado!</b></p><a data-toggle="modal" data-target="#modalTipoDeEscritura" style="margin-left: 10px"><span class="badge badge-primary" style="margin-top: 7px"><i class="fa fa-plus"></i> Cadastrar</span></a></div>');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $("#tipoSelect").html('');
                $("#tipoSelect").append('<label class="col-sm-2 control-label">Tipo de Escritura: </label><div class="col-sm-10"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar os Tipos de Escritura! Atualize a página.</b></p><a data-toggle="modal" data-target="#modalTipoDeEscritura" style="margin-left: 10px"><span class="badge badge-primary" style="margin-top: 7px"><i class="fa fa-plus"></i> Cadastrar</span></a></div>');
            }
        });
    }

    function trocaAlertTiposDeEscritura() {

        $("#alertTipoDeEscritura").html('');
        $("#alertTipoDeEscritura").append('<div class="alert alert-info" style="text-align: justify"><center>Contribua com um novo <a class="alert-link">Tipo de Escritura</a>. O qual será visível por todos!</center></div>');

    }

    function addInputMatriculaCadastrar(){
    	var idCampo = parseInt($("#nroCampo").val()) + 1;
    	var campo = '<p id="paragrafo'+idCampo+'"><input type="text" name="matricula'+idCampo+'" id="matricula'+idCampo+'" class="form-control col-sm-2" maxlength="14" style="max-width: 320px"><a onClick="removeInputMatriculaCadastrar(\''+idCampo+'\');" class="btn btn-xs btn-danger" style="color: white; margin-left: 15px; margin-top: 7px"><i class="fa fa-trash-o fa-white"></i></a></p>';

    	$("#campoMatriculas").append(campo);
    	$("#nroCampo").val(idCampo);
    }

    function removeInputMatriculaCadastrar(idCampo){
    	$("#paragrafo"+idCampo).html('');
    	$("#paragrafo"+idCampo).hide();
    }

    function limparImputMatriculaCadastrar(){
    	$("#matricula1").val('');
    }

    function addInputMatriculaAlterar(idAlteraImovel){

    	var idCampo = parseInt($("#nroCampo").val()) + 1;
    	var campo = '<p id="paragrafo'+idCampo+'"><input type="text" name="matricula'+idCampo+'" value="'+idCampo+'" id="matricula'+idCampo+'" class="form-control col-sm-2" maxlength="14" style="max-width: 320px"><a onClick="removeInputMatriculaCadastrar(\''+idCampo+'\');" class="btn btn-xs btn-danger" style="color: white; margin-left: 15px; margin-top: 7px"><i class="fa fa-trash-o fa-white"></i></a></p>';

		var campo = '<p id="paragrafo'+idCampo+'"> \
					    <input type="text" \
					    name="matricula'+idCampo+'" \
					    id="matricula'+idCampo+'" \
					    class="form-control col-sm-2" \
					    maxlength="14" \
					    onkeypress="exibirBotaoAlterarNumeroMatriculaImovel(\''+idCampo+'\');" \
					    style="max-width: 320px"> \
					    <a onClick="removerNumeroMatriculaImovel('+idAlteraImovel+','+idCampo+');" \
					       class="btn btn-xs btn-danger" \
					       style="color: white; margin-left: 15px; margin-top: 7px"> \
					        	<i class="fa fa-trash-o fa-white"></i> \
					    </a> \
					    <a onClick="alterarNumeroMatriculaImovel('+idAlteraImovel+',\'\',\''+idCampo+'\');" \
						   class="btn btn-xs btn-success" \
						   id="botaoAlterar'+idCampo+'" \
						   style="display: none; color: white; margin-left: 7px; margin-top: 7px"> \
					       		<i class="fa fa-save fa-white"></i> \
					    </a> \
					    <small id="msg'+idCampo+'" class="text-navy" style="margin-left: 7px"></small> \
					</p>'




    	$("#campoMatriculas").append(campo);
    	$("#nroCampo").val(idCampo);
    }

    function exibirBotaoAlterarNumeroMatriculaImovel(pos){
    	//console.log(pos);
    	$("#botaoAlterar"+pos).show();
    }

    function alterarNumeroMatriculaImovel(idImovel,numeroImovelMatricula,pos){

    	var numeroImovelMatriculaNovo = $("#matricula"+pos).val();

    	console.log("numeroImovelMatriculaNovo: " + numeroImovelMatriculaNovo);
    	console.log("idImovel: " + idImovel);
    	console.log("numeroImovelMatricula: " + numeroImovelMatricula);

    	if(numeroImovelMatricula != ''){
    		$.ajax({
	            type:"post",
	            url: "js/ajax/alterarNumeroMatriculaImovel.php",
	            dataType: "json",
	            data: {
		            idImovel:idImovel,
		            numeroImovelMatricula:numeroImovelMatricula,
		            numeroImovelMatriculaNovo:numeroImovelMatriculaNovo
	            },
	            success: function(data){
	                //console.log(data);
	                if(data.alterado == true){
						$("#botaoAlterar"+pos).hide();
	                	$("#msg"+pos).html(data.msg);

	                	var botaoAlterar = document.getElementById('botaoAlterar'+pos);
	                	botaoAlterar.setAttribute('onclick', 'alterarNumeroMatriculaImovel('+idImovel+','+numeroImovelMatriculaNovo+','+pos+')');
						//botaoAlterar.onclick =  alterarNumeroMatriculaImovel(idImovel,numeroImovelMatriculaNovo,pos);
	                } else {
	                	$("#matricula"+pos).val(numeroImovelMatricula);
	                	$("#matricula"+pos).focus();
	                	$("#msg"+pos).html(data.msg);
						$("#botaoAlterar"+pos).hide();
	                }
	            },
	            error: function(xhr, textStatus, errorThrown){
	                console.log('error');
	            }
	        });
    	} else {
    		$.ajax({
	            type:"post",
	            url: "js/ajax/cadastrarNumeroMatriculaImovel.php",
	            dataType: "json",
	            data: {
		            idImovel:idImovel,
		            numeroImovelMatricula:numeroImovelMatriculaNovo
	            },
	            success: function(data){
	                //console.log(data);
	                if(data.cadastrado == true){
						$("#botaoAlterar"+pos).hide();
	                	$("#msg"+pos).html(data.msg);

	                	var botaoAlterar = document.getElementById('botaoAlterar'+pos);
	                	botaoAlterar.setAttribute('onclick', 'alterarNumeroMatriculaImovel('+idImovel+','+numeroImovelMatriculaNovo+','+pos+')');
						//botaoAlterar.onclick =  alterarNumeroMatriculaImovel(idImovel,numeroImovelMatriculaNovo,pos);
	                } else {
	                	$("#matricula"+pos).val('');
	                	$("#matricula"+pos).focus();
	                	$("#msg"+pos).html(data.msg);
						$("#botaoAlterar"+pos).hide();
	                }
	            },
	            error: function(xhr, textStatus, errorThrown){
	                console.log('error');
	            }
	        });
    	}
    }

    function removerNumeroMatriculaImovel(idImovel,pos){

    	var numeroImovelMatricula = $("#matricula"+pos).val();
    	
    	$.ajax({
            type:"post",
            url: "js/ajax/removerNumeroMatriculaImovel.php",
            dataType: "json",
            data: {
	            idImovel:idImovel,
	            numeroImovelMatricula:numeroImovelMatricula
            },
            success: function(data){
                console.log(data);
                if(data.alterado == true){
					$("#paragrafo"+pos).html('');
    				$("#paragrafo"+pos).hide();
                } else {

                }
            },
            error: function(xhr, textStatus, errorThrown){
                console.log('error');
            }
        });
        
    }


    /*
        Atividade Iniciatica Oficial
     */
    $(document).ready(function() {
		if ($('#fk_idOrganismoAfiliado').val()==0) {
			$('#anoAtivInicOficial').hide(300);
			$('#tipoAtivInicOficial').hide(300);
		} else if($('#fk_idOrganismoAfiliado').val()!==0 && $('#fk_idOrganismoAfiliado').val()!==undefined) {
			$('#anoAtivInicOficial').show(300);
			$('#tipoAtivInicOficial').hide(300);
			getAnoEquipesIniciaticas($('#fk_idOrganismoAfiliado').val());
		} else {
			$('#anoAtivInicOficial').hide(300);
			$('#tipoAtivInicOficial').hide(300);
		}

    });
    function mudaOrganismoGetAno(idOrganismo) {
        $('#anoAtivInicOficial').hide(300);

        if(idOrganismo==0){
            $('#anoAtivInicOficial').hide(300);
        } else if(idOrganismo!=0) {
            $('#anoAtivInicOficial').show(300);
            getAnoEquipesIniciaticas(idOrganismo);
        } else {
            $('#anoAtivInicOficial').hide(300);
        }
    }
    function getAnoEquipesIniciaticas(idOrganismo) {
        $.ajax({
            type:"post",
            url: "js/ajax/getAnoEquipesIniciaticas.php",
            dataType: "json",
            data: {idOrganismo:idOrganismo},
            success: function(data){
                //console.log(data);
                $("#anoAtivInicOficial").html('');
                $("#anoAtivInicOficial").append('<label class="col-sm-3 control-label">Ano R+C: </label><div class="col-sm-9"><select class="form-control col-sm-3" onchange="mudaAnoGetEquipe(this.value);" name="anoAtividadeIniciaticaOficial" id="anoAtividadeIniciaticaOficial" style="max-width: 250px" required="required"></div>');
                $("#anoAtividadeIniciaticaOficial").append('<option value="0">Selecione...</option>');
                $("#anoAtividadeIniciaticaOficial").append(data.atividade.anos)
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $("#anoAtivInicOficial").html('');
                $("#anoAtivInicOficial").append('<label class="col-sm-3 control-label">Ano R+C: </label><div class="col-sm-9"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar os anos para cadastro! Atualize a página.</b></p></div>');
            }
        });
    }
    function mudaAnoGetEquipe(ano) {
        var idOrganismo;
        $('#tipoAtivInicOficial').hide(300);
        if(ano==0){
            $('#tipoAtivInicOficial').hide(300);
        } else if(ano!=0) {
            $('#tipoAtivInicOficial').show(300);
            idOrganismo = $('#fk_idOrganismoAfiliado').val();
			//console.log('ID do OA: '+idOrganismo);
            getEquipesIniciaticas(ano,idOrganismo);
        } else {
            $('#tipoAtivInicOficial').hide(300);
        }
    }
    function getEquipesIniciaticas(ano,idOrganismo) {
		//console.log('ID do OA: '+idOrganismo);
        $.ajax({
            type:"post",
            url: "js/ajax/getEquipesIniciaticas.php",
            dataType: "json",
            data: {ano:ano,idOrganismo:idOrganismo},
            success: function(data){
                //console.log(data);
                $("#tipoAtivInicOficial").html('');
                $("#tipoAtivInicOficial").append('<label class="col-sm-3 control-label">Equipe: </label><div class="col-sm-9"><select class="form-control col-sm-3" onchange="mudaTipoEquipeGetEquipe(this.value);" name="tipoAtividadeIniciaticaOficial" id="tipoAtividadeIniciaticaOficial" style="max-width: 250px" required="required"></div>');
                $("#tipoAtividadeIniciaticaOficial").append('<option value="0">Selecione...</option>');
                $("#tipoAtividadeIniciaticaOficial").append(data.atividade.equipes)
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $("#tipoAtivInicOficial").html('');
                $("#tipoAtivInicOficial").append('<label class="col-sm-3 control-label">Equipe: </label><div class="col-sm-9"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar as equipes para cadastro! Atualize a página.</b></p></div>');
            }
        });
    }

	$(document).ready(function() {
		$('#equipeJaCadastrada').hide(300);
	});
	function mudaTipoEquipeGetEquipe(tipoEquipe) {

		var idOrganismo;

		$('#equipeJaCadastrada').hide(300);

		console.log(tipoEquipe);
		if(tipoEquipe==0){
			$("#mestreAtividadeIniciaticaOficial").val('');
			$("#mestreAuxAtividadeIniciaticaOficial").val('');
			$("#arquivistaAtividadeIniciaticaOficial").val('');
			$("#capelaoAtividadeIniciaticaOficial").val('');
			$("#matreAtividadeIniciaticaOficial").val('');
			$("#grandeSacerdotisaAtividadeIniciaticaOficial").val('');
			$("#guiaAtividadeIniciaticaOficial").val('');
			$("#guardiaoInternoAtividadeIniciaticaOficial").val('');
			$("#guardiaoExternoAtividadeIniciaticaOficial").val('');
			$("#archoteAtividadeIniciaticaOficial").val('');
			$("#medalhistaAtividadeIniciaticaOficial").val('');
			$("#arautoAtividadeIniciaticaOficial").val('');
			$("#adjutorAtividadeIniciaticaOficial").val('');
			$("#sonoplastaAtividadeIniciaticaOficial").val('');
			$("#columbaAtividadeIniciaticaOficial").val('');
			$("#recepcaoAtividadeIniciaticaOficial").val('');
			$('#equipeJaCadastrada').hide(300);
		} else if(tipoEquipe!=0) {
			$('#equipeJaCadastrada').show(300);
			idOrganismo = $('#fk_idOrganismoAfiliado').val();
			//getEquipesIniciaticasORCZ(tipoEquipe,idOrganismo);
		} else {
			$('#equipeJaCadastrada').hide(300);
		}

	}
	function getEquipesIniciaticasORCZ(tipoEquipe,idOrganismo) {
		//console.log('ID do OA: '+idOrganismo);
		$.ajax({
			type:"post",
			url: "js/ajax/getEquipesIniciaticasORCZ.php",
			dataType: "json",
			data: {tipoEquipe:tipoEquipe,idOrganismo:idOrganismo},
			success: function(data){
				//console.log(data);
				$("#mestreAtividadeIniciaticaOficial").val('');
				$("#mestreAtividadeIniciaticaOficial").val(data.mestre);

				$("#mestreAuxAtividadeIniciaticaOficial").val('');
				$("#mestreAuxAtividadeIniciaticaOficial").val(data.mestreAux);

				$("#arquivistaAtividadeIniciaticaOficial").val('');
				$("#arquivistaAtividadeIniciaticaOficial").val(data.arquivista);

				$("#capelaoAtividadeIniciaticaOficial").val('');
				$("#capelaoAtividadeIniciaticaOficial").val(data.capelao);

				$("#matreAtividadeIniciaticaOficial").val('');
				$("#matreAtividadeIniciaticaOficial").val(data.matre);

				$("#grandeSacerdotisaAtividadeIniciaticaOficial").val('');
				$("#grandeSacerdotisaAtividadeIniciaticaOficial").val(data.grandeSacerdotisa);

				$("#guiaAtividadeIniciaticaOficial").val('');
				$("#guiaAtividadeIniciaticaOficial").val(data.guia);

				$("#guardiaoInternoAtividadeIniciaticaOficial").val('');
				$("#guardiaoInternoAtividadeIniciaticaOficial").val(data.guardiaoInterno);

				$("#guardiaoExternoAtividadeIniciaticaOficial").val('');
				$("#guardiaoExternoAtividadeIniciaticaOficial").val(data.guardiaoExterno);

				$("#archoteAtividadeIniciaticaOficial").val('');
				$("#archoteAtividadeIniciaticaOficial").val(data.archote);

				$("#medalhistaAtividadeIniciaticaOficial").val('');
				$("#medalhistaAtividadeIniciaticaOficial").val(data.medalhista);

				$("#arautoAtividadeIniciaticaOficial").val('');
				$("#arautoAtividadeIniciaticaOficial").val(data.arauto);

				$("#adjutorAtividadeIniciaticaOficial").val('');
				$("#adjutorAtividadeIniciaticaOficial").val(data.adjutor);

				$("#sonoplastaAtividadeIniciaticaOficial").val('');
				$("#sonoplastaAtividadeIniciaticaOficial").val(data.sonoplasta);

				$("#columbaAtividadeIniciaticaOficial").val('');
				$("#columbaAtividadeIniciaticaOficial").val(data.columba);

				$("#recepcaoAtividadeIniciaticaOficial").val('');
				$("#recepcaoAtividadeIniciaticaOficial").val(data.recepcao);


				$("#oficiaisDestaEquipe").html('');
				if(data.total>0){
					$("#oficiaisDestaEquipe").append('<li><strong>Mestre: </strong>'+data.mestreCodAfiliacao+data.mestreNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Mestre Auxiliar: </strong>'+data.mestreAuxCodAfiliacao+data.mestreAuxNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Arquivista: </strong>'+data.arquivistaCodAfiliacao+data.arquivistaNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Capelão: </strong>'+data.capelaoCodAfiliacao+data.capelaoNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Matre: </strong>'+data.matreCodAfiliacao+data.matreNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Grande Sacerdotisa: </strong>'+data.grandeSacerdotisaCodAfiliacao+data.grandeSacerdotisaNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Guia: </strong>'+data.guiaCodAfiliacao+data.guiaNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Guardião Interno: </strong>'+data.guardiaoInternoCodAfiliacao+data.guardiaoInternoNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Guardião Externo: </strong>'+data.guardiaoExternoCodAfiliacao+data.guardiaoExternoNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Portador do Archote: </strong>'+data.archoteCodAfiliacao+data.archoteNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Medalhista: </strong>'+data.medalhistaCodAfiliacao+data.medalhistaNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Arauto: </strong>'+data.arautoCodAfiliacao+data.arautoNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Adjutor: </strong>'+data.adjutorCodAfiliacao+data.adjutorNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Sonoplasta: </strong>'+data.sonoplastaCodAfiliacao+data.sonoplastaNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Columba: </strong>'+data.columbaCodAfiliacao+data.columbaNome+'</li>');
					$("#oficiaisDestaEquipe").append('<li><strong>Recepção: </strong>'+data.recepcaoCodAfiliacao+data.recepcaoNome+'</li>');
				}

			},
			error: function(xhr, textStatus, errorThrown){
				console.log('error');
			}
		});
	}

	function getAtualizaEquipeIniciaticasORCZ(tipoEquipe,idOrganismo,idEquipe) {
		//console.log('ID do OA: '+idOrganismo);
		$.ajax({
			type:"post",
			url: "js/ajax/getEquipesIniciaticasORCZ.php",
			dataType: "json",
			data: {tipoEquipe:tipoEquipe,idOrganismo:idOrganismo},
			success: function(data){
				//console.log(data);
				//console.log('Equipe: '+idEquipe);
				if(data.total>0){
					$("#mestreIniciatico"+idEquipe).html('');
					$("#mestreIniciatico"+idEquipe).html(data.mestreCodAfiliacao+data.mestreNome);

					$("#mestreAuxIniciatico"+idEquipe).html('');
					$("#mestreAuxIniciatico"+idEquipe).html(data.mestreAuxCodAfiliacao+data.mestreAuxNome);

					$("#arquivistaIniciatico"+idEquipe).html('');
					$("#arquivistaIniciatico"+idEquipe).html(data.arquivistaCodAfiliacao+data.arquivistaNome);

					$("#capelaoIniciatico"+idEquipe).html('');
					$("#capelaoIniciatico"+idEquipe).html(data.capelaoCodAfiliacao+data.capelaoNome);

					$("#matreIniciatico"+idEquipe).html('');
					$("#matreIniciatico"+idEquipe).html(data.matreCodAfiliacao+data.matreNome);

					$("#grandeSacerdotisaIniciatico"+idEquipe).html('');
					$("#grandeSacerdotisaIniciatico"+idEquipe).html(data.grandeSacerdotisaCodAfiliacao+data.grandeSacerdotisaNome);

					$("#guiaIniciatico"+idEquipe).html('');
					$("#guiaIniciatico"+idEquipe).html(data.guiaCodAfiliacao+data.guiaNome);

					$("#guardiaoInternoIniciatico"+idEquipe).html('');
					$("#guardiaoInternoIniciatico"+idEquipe).html(data.guardiaoInternoCodAfiliacao+data.guardiaoInternoNome);

					$("#guardiaoExternoIniciatico"+idEquipe).html('');
					$("#guardiaoExternoIniciatico"+idEquipe).html(data.guardiaoExternoCodAfiliacao+data.guardiaoExternoNome);

					$("#archoteIniciatico"+idEquipe).html('');
					$("#archoteIniciatico"+idEquipe).html(data.archoteCodAfiliacao+data.archoteNome);

					$("#medalhistaIniciatico"+idEquipe).html('');
					$("#medalhistaIniciatico"+idEquipe).html(data.medalhistaCodAfiliacao+data.medalhistaNome);

					$("#arautoIniciatico"+idEquipe).html('');
					$("#arautoIniciatico"+idEquipe).html(data.arautoCodAfiliacao+data.arautoNome);

					$("#adjutorIniciatico"+idEquipe).html('');
					$("#adjutorIniciatico"+idEquipe).html(data.adjutorCodAfiliacao+data.adjutorNome);

					$("#sonoplastaIniciatico"+idEquipe).html('');
					$("#sonoplastaIniciatico"+idEquipe).html(data.sonoplastaCodAfiliacao+data.sonoplastaNome);

					$("#columbaIniciatico"+idEquipe).html('');
					$("#columbaIniciatico"+idEquipe).html(data.columbaCodAfiliacao+data.columbaNome);

					$("#recepcaoIniciatico"+idEquipe).html('');
					$("#recepcaoIniciatico"+idEquipe).html(data.recepcaoCodAfiliacao+data.recepcaoNome);

					$("#buttonsDevelopment"+idEquipe).html('');
					$("#buttonsDevelopment"+idEquipe).html('<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button> <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar Equipe Atualizada">');

					//Inputs com values
					$("#mestreAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#mestreAtividadeIniciaticaOficial"+idEquipe).val(data.mestre);

					$("#mestreAuxAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#mestreAuxAtividadeIniciaticaOficial"+idEquipe).val(data.mestreAux);

					$("#arquivistaAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#arquivistaAtividadeIniciaticaOficial"+idEquipe).val(data.arquivista);

					$("#capelaoAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#capelaoAtividadeIniciaticaOficial"+idEquipe).val(data.capelao);

					$("#matreAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#matreAtividadeIniciaticaOficial"+idEquipe).val(data.matre);

					$("#grandeSacerdotisaAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#grandeSacerdotisaAtividadeIniciaticaOficial"+idEquipe).val(data.grandeSacerdotisa);

					$("#guiaAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#guiaAtividadeIniciaticaOficial"+idEquipe).val(data.guia);

					$("#guardiaoInternoAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#guardiaoInternoAtividadeIniciaticaOficial"+idEquipe).val(data.guardiaoInterno);

					$("#guardiaoExternoAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#guardiaoExternoAtividadeIniciaticaOficial"+idEquipe).val(data.guardiaoExterno);

					$("#archoteAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#archoteAtividadeIniciaticaOficial"+idEquipe).val(data.archote);

					$("#medalhistaAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#medalhistaAtividadeIniciaticaOficial"+idEquipe).val(data.medalhista);

					$("#arautoAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#arautoAtividadeIniciaticaOficial"+idEquipe).val(data.arauto);

					$("#adjutorAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#adjutorAtividadeIniciaticaOficial"+idEquipe).val(data.adjutor);

					$("#sonoplastaAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#sonoplastaAtividadeIniciaticaOficial"+idEquipe).val(data.sonoplasta);

					$("#columbaAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#columbaAtividadeIniciaticaOficial"+idEquipe).val(data.columba);

					$("#recepcaoAtividadeIniciaticaOficial"+idEquipe).val('');
					$("#recepcaoAtividadeIniciaticaOficial"+idEquipe).val(data.recepcao);

					swal({title:"Atualizado!",text:"Caso queira confirmar a atualização,\n clique em 'Salvar Equipe Atualizada'!",type:"success",confirmButtonColor:"#1ab394"});

				} else {
					swal({title:"Aviso!",text:"Nenhum oficial iniciático cadastrado!",type:"warning",confirmButtonColor:"#1ab394"});
				}

			},
			error: function(xhr, textStatus, errorThrown){
				console.log('error');
			}
		});
	}

	/**
	 * Função responsável por retornar dados dos membros via ORCZ
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 * @param Int #codAfiliacao+campo : Código de Afiliação do membro a ser consultado
	 * @param Int #nome+campo : Parte do nome do membro a ser consultado
	 * @param Int idOrganismo : ID do Organismo Afiliado de quem está operando
	 * Retorna dados completos do membro, por meio de webservice.
	 */
    function getOficialECompanheiroIniciatico(campo,oficial,idAtividadeIniciaticaOficial) {

        var codigoAfiliacao = $("#codAfiliacao"+campo+idAtividadeIniciaticaOficial).val();
        var nomeMembro 		= $("#nome"+campo+idAtividadeIniciaticaOficial).val();
        var tipoMembro 		= '1';
        var idOrganismo 	= $("#fk_idOrganismoAfiliado").val();

        $.ajax({
            type:"post",
            url: "js/ajax/retornaDadosMembro.php",
            dataType: "json",
            data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function(data){
				//console.log(data);
                getOficiaisAtividadeIniciatica(campo,oficial,idOrganismo,data.result[0].fields.fSeqCadast,data.result[0].fields.fSeqCadastCompanheiroRosacruz,data.result[0].fields.fNomCliente,data.result[0].fields.fNomConjuge,idAtividadeIniciaticaOficial);
            },
            error: function(xhr, textStatus, errorThrown){
				swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
            }
        });

    }

	/**
	 * Função responsável por verificar se o membro trazido está registrado na relação de oficiais do Organismo Afiliado do operador
	 * Todos os parâmetros são passados pela função que chama esta:
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 * @param Int idOrganismo : ID do Organismo Afiliado de quem está operando
	 * @param Int seqCadast : seqCadast do membro a ser consultado
	 * @param Int fSeqCadastCompanheiroRosacruz : seqCadast do membro companheiro
	 * @param String fNomCliente : Nome do membro
	 * @param String fNomConjuge : Nome do Companheiro
	 * Verifica por meio da tabela 'membrooa'.
	 */
    function getOficiaisAtividadeIniciatica(campo,oficial,idOrganismo,seqCadast,fSeqCadastCompanheiroRosacruz,fNomCliente,fNomConjuge,idAtividadeIniciaticaOficial){
		//console.log('Titular: '+seqCadast);
		$.ajax({
            type:"post",
            url: "js/ajax/getOficiaisAtividadeIniciatica.php",
            dataType: "json",
            data: {idOrganismo: idOrganismo, seqCadast: seqCadast},
            success: function(data){
				//console.log(data);
                if(data.oficial>0){
                    //console.log('Membro Incluído');
					$("#"+oficial+idAtividadeIniciaticaOficial).val(seqCadast);
                    $("#h_seqCadast"+campo+idAtividadeIniciaticaOficial).val(seqCadast);
                    $("#nome"+campo+idAtividadeIniciaticaOficial).val(fNomCliente);
                    $("#h_nome"+campo+idAtividadeIniciaticaOficial).val(fNomCliente);
                    $("#h_seqCadastCompanheiro"+campo+idAtividadeIniciaticaOficial).val(fSeqCadastCompanheiroRosacruz);
                    $("#h_nomeCompanheiro"+campo+idAtividadeIniciaticaOficial).val(fNomConjuge);
                } else {
					swal({title:"Aviso!",text:"Membro não está registrado na relação deste Organismo",type:"warning",confirmButtonColor:"#1ab394"});
					if($('#companheiro'+campo+idAtividadeIniciaticaOficial).is(":checked")) {
						$('#companheiro'+campo+idAtividadeIniciaticaOficial).prop("checked", false);
						//$('#companheiro' + campo).attr("checked", false);
					}
					$("#"+oficial+idAtividadeIniciaticaOficial).val();
                    $("#h_seqCadast"+campo+idAtividadeIniciaticaOficial).val();
                    $("#nome"+campo+idAtividadeIniciaticaOficial).val();
                    $("#h_nome"+campo+idAtividadeIniciaticaOficial).val();
                    $("#h_seqCadastCompanheiro"+campo+idAtividadeIniciaticaOficial).val();
                    $("#h_nomeCompanheiro"+campo+idAtividadeIniciaticaOficial).val();
                }
            },
            error: function(xhr, textStatus, errorThrown){
				swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
            }
        });
    }

	/**
	 * Função responsável por fazer a troca do nome do membro pelo nome do companheiro quando selecionado checkbox indicando o companheiro
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 */
    function getCompanheiroIniciatico(campo,oficial,idAtividadeIniciaticaOficial) {
        var idOrganismo 		= $("#fk_idOrganismo").val();
        //$('#companheiro'+campo).change(function() {
            if($('#companheiro'+campo+idAtividadeIniciaticaOficial).is(":checked")) {
                if($("#h_nomeCompanheiro"+campo+idAtividadeIniciaticaOficial).val()=="") {
                    swal({title:"Aviso!",text:"Este membro não possui companheiro",type:"warning",confirmButtonColor:"#1ab394"});
                    $('#companheiro'+campo+idAtividadeIniciaticaOficial).prop("checked", false);
                } else {
                    $("#nome"+campo+idAtividadeIniciaticaOficial).val($("#h_nomeCompanheiro"+campo+idAtividadeIniciaticaOficial).val());
					$("#"+oficial+idAtividadeIniciaticaOficial).val($("#h_seqCadastCompanheiro"+campo+idAtividadeIniciaticaOficial).val());
                    getOficiaisCompanheiroAtividadeIniciatica(campo,oficial,idOrganismo,$("#h_seqCadastCompanheiro"+campo+idAtividadeIniciaticaOficial).val(),$("#h_nome"+campo+idAtividadeIniciaticaOficial).val(),$("#h_seqCadast"+campo+idAtividadeIniciaticaOficial).val(),idAtividadeIniciaticaOficial);
                }
            } else {
                $("#nome"+campo+idAtividadeIniciaticaOficial).val($("#h_nome"+campo+idAtividadeIniciaticaOficial).val());
				$("#"+oficial+idAtividadeIniciaticaOficial).val($("#h_seqCadast"+campo+idAtividadeIniciaticaOficial).val());
            }
        //});
    }

	/**
	 * Função responsável por verificar se o membro Companheiro trazido está registrado na relação de oficiais do Organismo Afiliado do operador
	 * Todos os parâmetros são passados pela função que chama esta:
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 * @param Int idOrganismo : ID do Organismo Afiliado de quem está operando
	 * @param Int fSeqCadastCompanheiroRosacruz : seqCadast do membro companheiro
	 * @param String fNomCliente : Nome do membro
	 * Verifica por meio da tabela 'membrooa'.
	 */
	function getOficiaisCompanheiroAtividadeIniciatica(campo,oficial,idOrganismo,fSeqCadastCompanheiroRosacruz,fNomCliente,seqCadast,idAtividadeIniciaticaOficial){
		//console.log('Companheiro: '+fSeqCadastCompanheiroRosacruz);
		$.ajax({
			type:"post",
			url: "js/ajax/getOficiaisAtividadeIniciatica.php",
			dataType: "json",
			data: {idOrganismo: idOrganismo, seqCadast: fSeqCadastCompanheiroRosacruz},
			success: function(data){
				if(data.oficial>0){
					console.log('Membro Companheiro Incluído');
				} else {
					swal({title:"Aviso!",text:"Membro companheiro não está registrado na relação deste Organismo",type:"warning",confirmButtonColor:"#1ab394"});
					$("#nome"+campo+idAtividadeIniciaticaOficial).val(fNomCliente);
					$("#"+oficial+idAtividadeIniciaticaOficial).val(seqCadast);
				}
			},
			error: function(xhr, textStatus, errorThrown){
				swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
			}
		});
	}

	/**
	 * Função responsável por abrir janela com relação de Oficiais na Equipe X
	 * @param Int idAtividadeIniciaticaOficial : ID da Equipe de Atividade Iniciática
	 * @param Int idOrganismo : ID do Organismo Afiliado da Equipe de Atividade Iniciática
	 */
	function abrirPopUpAtividadeIniciaticaOficialRelacao(idAtividadeIniciaticaOficial,idOrganismo)
	{
		window.open('impressao/atividadeIniciaticaOficialRelacao.php?idAtividadeIniciaticaOficial='+idAtividadeIniciaticaOficial+'&idOrganismo='+idOrganismo,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
	}

    function abrirPopUpAtividadeIniciaticaOficialRelacaoParaAssinatura(idAtividadeIniciaticaOficial,idOrganismo)
    {
        var codAfiliacaoColumba         = $("#codAfiliacaoColumba"+idAtividadeIniciaticaOficial).val();
        var nomeColumba                 = $("#nomeColumba"+idAtividadeIniciaticaOficial).val();
        var codAfiliacaoRecepcao        = $("#codAfiliacaoRecepcao"+idAtividadeIniciaticaOficial).val();
        var nomeRecepcao                = $("#nomeRecepcao"+idAtividadeIniciaticaOficial).val();

        var idAtividadeIniciatica       = $("#atividadeIniciatica"+idAtividadeIniciaticaOficial).val();

        window.open('impressao/atividadeIniciaticaEquipeOficiaisAssinatura.php?idAtividadeIniciaticaOficial='+idAtividadeIniciaticaOficial+'&idOrganismo='+idOrganismo+'&codAfiliacaoColumba='+codAfiliacaoColumba+'&nomeColumba='+nomeColumba+'&codAfiliacaoRecepcao='+codAfiliacaoRecepcao+'&nomeRecepcao='+nomeRecepcao+'&idAtividadeIniciatica='+idAtividadeIniciatica,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }

	$('.dataTables-membros-iniciaticos').dataTable({
		//"order": [[ 0, "desc" ]],
		paging: true,
		responsive: true,
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
		}
	});

	$(document).ready(function() {
		/*
		var totalMembros = $("#totalMembros").val();

		console.log("Total de membros: "+totalMembros);
		var pote=0;

		for(i=0; i < totalMembros; i++){
			//console.log("Roda roda: "+pote);
			//console.log("i tem o valor: "+i);

			var nomeMembro			= $("#nomeParaGetIniciacaoPendente"+i).val();
			var codAfilMembro 		= $("#codAfiliacaoParaGetIniciacaoPendente"+i).val();
			var seqCadast 		= $("#seqCadastParaGetIniciacaoPendente"+i).val();

			//consultaMembroDadosIniciacao(nomeMembro,codAfilMembro,seqCadast);
			//alert('aguardando...');
			pote++;
		}
		*/
	});

	/*
	function consultaMembroDadosIniciacao(nomeMembro,codigoAfiliacao,seqCadastMembro){

		var tipoMembro = 1;

		$.ajax({
			type:"post",
			url: "js/ajax/retornaDadosMembro.php",
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro},
			success: function(data){

				if(data.result[0].fields.fNomCliente.trim()!="")
				{
					var companheiro="N";
					if(data.result[0].fields.fSeqCadastCompanheiroRosacruz==data.result[0].fields.fSeqCadast)
					{
						companheiro="S";
					}

					//console.log('Nome: '+data.result[0].fields.fNomCliente+'   -   Numero de lote: '+data.result[0].fields.fNumLoteAtualRosacr);

					if(companheiro=="S") {

						var nomeMembroTitular = 'valter';

						consultaMembroTitularLoteAtual(nomeMembroTitular,codigoAfiliacao,companheiro,seqCadastMembro);

						//getIniciacaoPendenteORCZ(codigoAfiliacao,companheiro,numeroGrauRC(lote),seqCadastMembro);
					} else {
						getIniciacaoPendenteORCZ(codigoAfiliacao,companheiro,numeroGrauRC(data.result[0].fields.fNumLoteAtualRosacr),seqCadastMembro);
					}

				}

			},
			error: function(xhr, textStatus, errorThrown){
				//alert('erro ao consultar o membro');
			}
		});
	}
	*/

	/*
	function consultaMembroTitularLoteAtual(nomeMembro,codigoAfiliacao,companheiro,seqCadastMembro){

		var tipoMembro = 1;

		//console.log('Nome: '+nomeMembro+'   -   Código de Afiliação: '+codigoAfiliacao);

		$.ajax({
			type:"post",
			url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}
			success: function(data){

				if(data.result[0].fields.fNomCliente.trim()!="")
				{
					var lote = data.result[0].fields.fNumLoteAtualRosacr;

					getIniciacaoPendenteORCZ(codigoAfiliacao,companheiro,numeroGrauRC(lote),seqCadastMembro);
				}

			},
			error: function(xhr, textStatus, errorThrown){
				//alert('erro ao consultar o membro');
			}
		});
	}
	*/

	/*
	function getIniciacaoPendenteORCZ(codigoAfiliacao,companheiro,numGrau,seqCadastMembro) {

		//console.log('entrou');
		$.ajax({
			type:"post",
			url: "js/ajax/getIniciacoesPendentesMembroRCParaAtividadeIniciatica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
			dataType: "html",
			data: {codigoAfiliacao:codigoAfiliacao,companheiro:companheiro,numGrau:numGrau,seqCadastMembro:seqCadastMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}
			success: function(data){
				$("#proximoGrauPendente"+seqCadastMembro).html('');
				$("#proximoGrauPendente"+seqCadastMembro).html(data);
			},
			error: function(xhr, textStatus, errorThrown){
				alert('erro');
			},
		});
	}
	*/


	/**
	 *
	 * @param grauDisponivel
	 * @param idOrganismo
	 */

	function abreModalAtividadesDisponiveis(grauDisponivel, idOrganismo, seqCadast) {
		$.ajax({
			type:"post",
			url: "js/ajax/getIniciacoesDisponiveis.php",
			dataType: "json",
			data: {grauDisponivel:grauDisponivel,idOrganismo:idOrganismo},
			success: function(data){

				//console.log(data);

				//Área de conteúdo
				$("#conteudoModalIniciacoes").html('');
				//Nome do Organismo
				$("#conteudoModalIniciacoes").append('<h4><center>'+grauDisponivel+'º Grau de Templo</center></h4><br>');

				if(data.iniciacoes.length > 0){

					$("#conteudoModalIniciacoes").append('<div class="col-lg-8 col-lg-offset-2" id="opcoesDeData"></div>');

					for(i=0; i < data.iniciacoes.length; i++){
						$("#opcoesDeData").append(data.iniciacoes[i]);
					}

					//Área de botões
					$("#iniciacaoDisponivelButton").html('');
					$("#iniciacaoDisponivelButton").append('<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button><button type="button" class="btn btn-primary" onclick="cadastraMembroNaIniciacao('+seqCadast+');">Agendar</button>');

				} else {
					$("#conteudoModalIniciacoes").append('<div class="form-group"><div class="alert alert-danger"><center><a class="alert-link" href="">Atenção! </a>Nenhuma Atividade Iniciática cadastrada para esse Grau de Templo!</center></div></div>');

					//Área de botões
					$("#iniciacaoDisponivelButton").html('');
					$("#iniciacaoDisponivelButton").append('<button type="button" class="btn btn-white" data-dismiss="modal" name="fechar">Fechar</button>');
				}

				//ABRE MODAL
				$('#mySeeAgendamento').modal({});

			},
			error: function(xhr, textStatus, errorThrown){
				alert('erro');
			}  ,
		});
	}


	function cadastraMembroNaIniciacao(seqCadastMembro) {

		var nomeMembro = $("#nomeAtividadeIniciatica"+seqCadastMembro).val();

		//console.log(nomeMembro);

		var rads = document.getElementsByName('iniciacoes');

		for(var i = 0; i < rads.length; i++) {
			if (rads[i].checked) {
				idAtividadeIniciatica = rads[i].value;
			}
		}

		//console.log("Atividade Iniciática: "+idAtividadeIniciatica);

		if(typeof idAtividadeIniciatica !== "undefined"){
			swal({
					title:"Aviso!",
					text:"Tem certeza que deseja agendar esse membro nessa iniciação?",
					type:"warning",
					showCancelButton: true,
					confirmButtonColor:"#1ab394",
					confirmButtonText: "Sim, confirmar!",
					closeOnConfirm: false
				},
				function(){
					$.ajax({
						type:"post",
						url: "js/ajax/cadastraMembroAtividadeIniciatica.php",
						dataType: "json",
						data: {seqCadastMembro:seqCadastMembro,idAtividadeIniciatica:idAtividadeIniciatica,nomeMembro:nomeMembro},
						success: function(retorno){
							//console.log(retorno);

							if (retorno.erro != undefined && retorno.erro.length > 0) {
								/*
                                var erro = 'Ocorreram um ou mais erros:\n\n';
								for (var i =0; i < retorno.erro.length; i++) {
									erro += retorno.erro[i];
								}
                                */
                                swal({title:"ERRO!",text:retorno.erro,type:"warning",confirmButtonColor:"#1ab394"});
							}
							else if (retorno.sucesso != undefined) {
                                enviaEmailMembroAtividadeIniciatica(seqCadastMembro,idAtividadeIniciatica,nomeMembro);
								swal(
								{
									title:"Confirmado!",
									text:"O membro receberá um e-mail (caso ele o tenha) para confirmar a participação.",
									type:"success",
									showCancelButton: false,
									confirmButtonColor:"#1ab394",
									confirmButtonText: "Ok",
									closeOnConfirm: true
								}, function(){
									//FECHA MODAL
									$('#mySeeAgendamento').modal('toggle');
									//location.href='?corpo=buscaAtividadeIniciaticaMembro&idOrganismoAfiliado='+idOrganismo;
									$('#statusMembro'+seqCadastMembro).html('');
									$('#statusMembro'+seqCadastMembro).append('<span class="badge badge-primary">Agendado</span>');

									$("#botoesMembro"+seqCadastMembro).html('');
									$("#botoesMembro"+seqCadastMembro).append('<a class="btn btn-sm btn-info"  class="btn btn-sm btn-info" onclick="abreModalInfoContato('+seqCadastMembro+');" data-target="#mySeeInfoMembro"> <i class="fa fa-envelope fa-white"></i>&nbsp;Contato</a>&nbsp;<a class="btn btn-sm btn-success" href="" data-toggle="modal" onclick="abreModalAtividadeViaAjax('+idAtividadeIniciatica+');" data-target="#mySeeAtividadeIniciatica"><i class="fa fa-search fa-white"></i> Visualizar</a>');
								});
							}
							else {
								swal({title:"ERRO!",text:"Ocorrou um erro no processo de cadastro!",type:"warning",confirmButtonColor:"#1ab394"});
							}
						},
						error: function(xhr, textStatus, errorThrown){
							swal({title:"ERRO!",text:"Ocorrou um erro no processo de cadastro!",type:"warning",confirmButtonColor:"#1ab394"});
						},
					});
				}
			);
		} else {
			swal({title:"Alerta!",text:"Selecione uma data para agendar o membro!",type:"warning",confirmButtonColor:"#1ab394"});
		}

	}

	function confirmaAgendamentoMembroAtividadeIniciatica(seqCadastMembro,idAtividadeIniciatica) {
		$.ajax({
			type:"post",
			url: "js/ajax/confirmaAgendamentoMembroAtividadeIniciatica.php",
			dataType: "json",
			data: {seqCadastMembro:seqCadastMembro,idAtividadeIniciatica:idAtividadeIniciatica},
			success: function(retorno){
				//console.log(retorno);
				if(retorno['sucesso'] == true){

                    //console.log("#acoesMembroAtividade"+seqCadastMembro);
                    //console.log("#statusMembroAtividade"+seqCadastMembro);

					$("#acoesMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).html('');
					$("#statusMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).html('');

					$("#acoesMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).append("<a class='btn-xs btn-danger' onclick='cancelaAgendamentoMembroAtividadeIniciatica("+seqCadastMembro+","+idAtividadeIniciatica+")'>Cancelar</a>&nbsp");
					$("#acoesMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).append("<a class='btn-xs btn-primary' onclick='confirmaParticipacaoMembroAtividadeIniciatica("+seqCadastMembro+","+idAtividadeIniciatica+")'>Presente</a>");
					$("#statusMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).append("<span class='badge badge-success'>Confirmado</span>");

                    $("." + idAtividadeIniciatica + seqCadastMembro).val(1);
                    //$("#" + seqCadastMembro).val(1);
                    //$('#' + idAtividadeIniciatica + " #" + seqCadast).val(1);
				} else {
					swal({title:"ERRO!",text:"Ocorreu algum erro ao confirmar o membro! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
				}
			},
			error: function(xhr, textStatus, errorThrown){
				swal({title:"ERRO!",text:"Ocorreu algum erro ao confirmar o membro! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
			}  ,
		});
	}
	function cancelaAgendamentoMembroAtividadeIniciatica(seqCadastMembro,idAtividadeIniciatica) {
		$.ajax({
			type:"post",
			url: "js/ajax/cancelaAgendamentoMembroAtividadeIniciatica.php",
			dataType: "json",
			data: {seqCadastMembro:seqCadastMembro,idAtividadeIniciatica:idAtividadeIniciatica},
			success: function(retorno){
				//console.log(retorno);
				if(retorno['sucesso'] == true){
					$("#acoesMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).html('');
					$("#statusMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).html('');

					$("#acoesMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).append(" -- ");
					$("#statusMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).append("<span class='badge badge-danger'>Cancelado</span>");

                    $("." + idAtividadeIniciatica + seqCadastMembro).val(3);
                    $("#naoCompareceram").val($("#naoCompareceram").val()+seqCadastMembro+"/"+idAtividadeIniciatica+"@@");
                    //$("#" + seqCadastMembro).val(3);
                    //$('#' + idAtividadeIniciatica + " #" + seqCadast).val(3);
				} else {
					swal({title:"ERRO!",text:"Ocorreu algum erro ao cancelar o membro! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
				}
			},
			error: function(xhr, textStatus, errorThrown){
				swal({title:"ERRO!",text:"Ocorreu algum erro ao cancelar o membro! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
			}  ,
		});
	}
	function confirmaParticipacaoMembroAtividadeIniciatica(seqCadastMembro,idAtividadeIniciatica) {

		$.ajax({
			type:"post",
			url: "js/ajax/confirmaParticipacaoMembroAtividadeIniciatica.php",
			dataType: "json",
			data: {seqCadastMembro:seqCadastMembro,idAtividadeIniciatica:idAtividadeIniciatica},
			success: function(retorno){
				//console.log(retorno);
				if(retorno['sucesso'] == true){
					$("#acoesMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).html('');
					$("#statusMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).html('');

					$("#acoesMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).append(" -- ");
					$("#statusMembroAtividade"+seqCadastMembro+idAtividadeIniciatica).append("<span class='badge badge-primary compareceu'>Compareceu</span>");

                    $("." + idAtividadeIniciatica + seqCadastMembro).val(2);
                    $("#compareceram").val($("#compareceram").val()+seqCadastMembro+"/"+idAtividadeIniciatica+"@@");
                    //$("#" + idAtividadeIniciatica).val(4)
                    //$('#' + idAtividadeIniciatica + " #" + seqCadast).val(2);
				} else {
					swal({title:"ERRO!",text:"Ocorreu algum erro ao cancelar o membro! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
				}
			},
			error: function(xhr, textStatus, errorThrown){
				swal({title:"ERRO!",text:"Ocorreu algum erro ao cancelar o membro! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
			}  ,
		});
	}

	function abreModalAtividade(seqCadast) {

		$("#conteudoModalAtividade").html('');

		var tipo 		= $("#tipoAtividadeAgendada"+seqCadast).val();
		var local 		= $("#localAtividadeAgendada"+seqCadast).val();
		var data 		= $("#dataAtividadeAgendada"+seqCadast).val();
		//var anotacoes 	= $("#anotacoesAtividadeAgendada"+seqCadast).val();

		//console.log('anotacoes: ' + anotacoes);

		if(tipo != undefined){
			console.log('Entrou no preenchimento!');
			$("#conteudoModalAtividade").append('<h4><center>'+tipo+'º Grau de Templo</center></h4><br>');
			$("#conteudoModalAtividade").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Local: </label><div class="col-sm-8"><div class="col-sm-8" style="max-width: 320px"><p class="form-control-static">'+local+'</p></div></div></div></div>');
			$("#conteudoModalAtividade").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Data: </label><div class="col-sm-8"><div class="col-sm-8" style="max-width: 320px"><p class="form-control-static">'+data+'</p></div></div></div></div>');
			//$("#conteudoModalAtividade").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Anotações: </label><div class="col-sm-8"><div class="col-sm-8" style="max-width: 320px"><p class="form-control-static">'+anotacoes+'</p></div></div></div></div>');
		} else {
			$("#conteudoModalAtividade").append('<div class="form-group"><div class="alert alert-danger"><center><a class="alert-link" href="">Erro ao trazer informações da Atividade Iniciática! </a></center></div></div>');
		}
		$('#mySeeAtividadeIniciatica').modal({});
	}

	function abreModalAtividadeViaAjax(idAtividadeIniciatica) {
        $.ajax({
            type:"post",
            url: "js/ajax/getAtividadeIniciatica.php",
            dataType: "json",
            data: {idAtividadeIniciatica:idAtividadeIniciatica},
            success: function(data){
                $("#conteudoModalAtividade").html('');
                if(data.qnt > 0){
                    $("#conteudoModalAtividade").append('<h4><center>'+data.tipo+'º Grau de Templo</center></h4><br>');
                    $("#conteudoModalAtividade").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Status: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+data.status+'</p></div></div></div>');
                    $("#conteudoModalAtividade").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Local: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+data.local+'</p></div></div></div>');
                    $("#conteudoModalAtividade").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Data: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+data.data+'</p></div></div></div>');
                    $("#conteudoModalAtividade").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Anotações: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+data.anotacoes+'</p></div></div></div>');
                } else {
                    $("#conteudoModalAtividade").append('<div class="form-group"><div class="alert alert-danger"><center><a class="alert-link" href="">Erro ao trazer informações da Atividade Iniciática! </a></center></div></div>');
                }
                $('#mySeeAtividadeIniciatica').modal({});
            },
            error: function(xhr, textStatus, errorThrown){
                console.log('erro');
            }  ,
        });
    }

    function abreModalInfoContato(seqCadast) {

        var nomeMembro          = $("#nomeAtividadeIniciatica"+seqCadast).val();
        var email               = $("#emailAtividadeIniciatica"+seqCadast).val();
        var telefone1           = $("#telFixoAtividadeIniciatica"+seqCadast).val();
        var telefone2           = $("#telCelAtividadeIniciatica"+seqCadast).val();
        var telefone3           = $("#telOutroAtividadeIniciatica"+seqCadast).val();

        console.log("nomeMembro: " + nomeMembro);
        console.log("email: " + email);
        console.log("telefone1: " + telefone1);
        console.log("telefone2: " + telefone2);
        console.log("telefone3: " + telefone3);

        $("#conteudoModalInfoContato").html('');
        $("#conteudoModalInfoContato").append('<h4><center>'+nomeMembro+'</center></h4><br>');
        $("#conteudoModalInfoContato").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">E-mail: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+email+'</p></div></div></div>');
        $("#conteudoModalInfoContato").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Telefone Fixo: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+telefone1+'</p></div></div></div>');
        $("#conteudoModalInfoContato").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Celular: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+telefone2+'</p></div></div></div>');
        $("#conteudoModalInfoContato").append('<div class="row"><div class="form-group"><label class="col-sm-4 control-label" style="text-align: right">Outro Telefone: </label><div class="col-sm-8" style="max-width: 320px; padding-top: 0px; padding-bottom: 10px;"><p class="form-control-static" style="padding-top: 0px">'+telefone3+'</p></div></div></div>');

        $('#mySeeInfoMembro').modal({});
    }

	//	Atividade de Estatuto

	function abreInputNroParticipantes(idAtividadeEstatuto) {

		var nroAtual = $("#numeroAtualParticipantes"+idAtividadeEstatuto).val();

		$("#numeroParticipantes"+idAtividadeEstatuto).html('');
		$("#numeroParticipantes"+idAtividadeEstatuto).append('<center><input type="text" value="'+nroAtual+'" id="numeroNovoParticipantes'+idAtividadeEstatuto+'" name="numeroNovoParticipantes'+idAtividadeEstatuto+'" class="form-control" class="col-md-1" style="max-width: 62px" maxlength="4"></center><br>');

		$("#botaoParticipantes"+idAtividadeEstatuto).html('');
		$("#botaoParticipantes"+idAtividadeEstatuto).append('<center><a class="btn btn-xs btn-success" onclick="salvaNroParticipantes('+idAtividadeEstatuto+')">Salvar </a></center>');

	}

	function salvaNroParticipantes(idAtividadeEstatuto) {

		var nroNovo = $("#numeroNovoParticipantes"+idAtividadeEstatuto).val();

		$.ajax({
			type:"post",
			url: "js/ajax/cadastraNumeroParticipantesAtividadeEstatuto.php",
			dataType: "json",
			data: {idAtividadeEstatuto:idAtividadeEstatuto,nroNovo:nroNovo},
			success: function(retorno){

				if (retorno.erro != undefined && retorno.erro.length > 0) {

					var erro = 'Ocorreram um ou mais erros:\n\n';
					for (var i =0; i < retorno.erro.length; i++) {
						erro += retorno.erro[i] + '\n';
					}
					swal({title:"ERRO!",text:erro, type:"warning",confirmButtonColor:"#1ab394"});
					$('#myModaNroParticipantes'+idAtividadeEstatuto).modal('toggle');
				}
				else if (retorno.sucesso != undefined) {

					$("#numeroAtualParticipantes"+idAtividadeEstatuto).val(nroNovo);

					$("#numeroParticipantes"+idAtividadeEstatuto).html('');
					$("#numeroParticipantes"+idAtividadeEstatuto).append('<center><h2>'+nroNovo+'</h2></center>');

					$("#botaoParticipantes"+idAtividadeEstatuto).html('');
					$("#botaoParticipantes"+idAtividadeEstatuto).append('<center><a class="btn btn-xs btn-primary" onclick="abreInputNroParticipantes('+idAtividadeEstatuto+')">Editar </a></center>');
				}
				else {
					swal({title:"ERRO!",text:"Ocorreu algum erro ao abrir a notificaçãoo! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
					$('#myModaNroParticipantes'+idAtividadeEstatuto).modal('toggle');
				}

			},
			error: function(xhr, textStatus, errorThrown){
				swal({title:"ERRO!",text:"Ocorreu algum erro ao abrir a notificaçãoo! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
				$('#myModaNroParticipantes'+idAtividadeEstatuto).modal('toggle');
			}
		});
	}

    function abreInputNroFrequentadores(idAtividadeEstatuto) {

        var nroAtual = $("#numeroAtualFrequentadores"+idAtividadeEstatuto).val();

        $("#numeroFrequentadores"+idAtividadeEstatuto).html('');
        $("#numeroFrequentadores"+idAtividadeEstatuto).append('<center><input type="text" value="'+nroAtual+'" id="numeroNovoFrequentadores'+idAtividadeEstatuto+'" name="numeroNovoFrequentadores'+idAtividadeEstatuto+'" class="form-control" class="col-md-1" style="max-width: 62px" maxlength="4"></center><br>');

        $("#botaoFrequentadores"+idAtividadeEstatuto).html('');
        $("#botaoFrequentadores"+idAtividadeEstatuto).append('<center><a class="btn btn-xs btn-success" onclick="salvaNroFrequentadores('+idAtividadeEstatuto+')">Salvar </a></center>');

    }

    function salvaNroFrequentadores(idAtividadeEstatuto) {

        var nroNovo = $("#numeroNovoFrequentadores"+idAtividadeEstatuto).val();

        $.ajax({
            type:"post",
            url: "js/ajax/cadastraNumeroFrequentadoresAtividadeEstatuto.php",
            dataType: "json",
            data: {idAtividadeEstatuto:idAtividadeEstatuto,nroNovo:nroNovo},
            success: function(retorno){

                if (retorno.erro != undefined && retorno.erro.length > 0) {

                    var erro = 'Ocorreram um ou mais erros:\n\n';
                    for (var i =0; i < retorno.erro.length; i++) {
                        erro += retorno.erro[i] + '\n';
                    }
                    swal({title:"ERRO!",text:erro, type:"warning",confirmButtonColor:"#1ab394"});
                    $('#myModaNroFrequentadores'+idAtividadeEstatuto).modal('toggle');
                }
                else if (retorno.sucesso != undefined) {

                    $("#numeroAtualFrequentadores"+idAtividadeEstatuto).val(nroNovo);

                    $("#numeroFrequentadores"+idAtividadeEstatuto).html('');
                    $("#numeroFrequentadores"+idAtividadeEstatuto).append('<center><h2>'+nroNovo+'</h2></center>');

                    $("#botaoFrequentadores"+idAtividadeEstatuto).html('');
                    $("#botaoFrequentadores"+idAtividadeEstatuto).append('<center><a class="btn btn-xs btn-primary" onclick="abreInputNroFrequentadores('+idAtividadeEstatuto+')">Editar </a></center>');
                }
                else {
                    swal({title:"ERRO!",text:"Ocorreu algum erro ao abrir a notificaçãoo! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
                    $('#myModaNroFrequentadores'+idAtividadeEstatuto).modal('toggle');
                }

            },
            error: function(xhr, textStatus, errorThrown){
                swal({title:"ERRO!",text:"Ocorreu algum erro ao abrir a notificaçãoo! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
                $('#myModaNroFrequentadores'+idAtividadeEstatuto).modal('toggle');
            }
        });
    }


	/**
	 * Notificações
	 */
	function abreModalNotificacao(idOrganismoAfiliado,tituloPadrao) {
		//console.log(idOrganismoAfiliado);
		$.ajax({
			type:"post",
			url: "js/ajax/carregaDadosNotificacao.php",
			dataType: "json",
			data: {idOrganismoAfiliado:idOrganismoAfiliado},
			success: function(data){
				//console.log(data);
				if(data.comRegistroTotal>0){

					//Área de conteúdo
					$("#conteudoModal").html('');
					//Nome do Organismo
					$("#conteudoModal").append(data.nomeCompletoOrganismo);

					//Oficiais disponíveis para envio de notificação
					for(i=0; i<data.comRegistro.length; i++){
						$("#conteudoModal").append(data.comRegistro[i]);
					}
					//Oficiais disponíveis para envio de notificação
					$("#conteudoModal").append('<input type="hidden" id="qntOficiais" name="qntOficiais" value="'+data.comRegistroTotal+'">');
					//Oficiais não cadastrados no sistema, por isso indisponíveis
					$("#conteudoModal").append('<div class="form-group"><div class="alert alert-warning"><center>Alguns oficiais deste Organismo ainda não realizaram registro no sistema:</center><br><ul id="semRegistros"></ul></div></div>');
					for(i=0; i<data.semRegistro.length; i++){
						$("#semRegistros").append(data.semRegistro[i]);
					}

					//EXIBE: Tipo de Notificação, Título e Mensagem
					$("#blocoTipo").css('display', 'inline');
					$("#blocoTitulo").css('display', 'inline');
					if(tituloPadrao!=null){
						$("#tituloNotificacaoGlp").val(tituloPadrao);
					}
					$("#blocoMensagem").css('display', 'inline');

					//Área de botões
					$("#enviarNotificacaoButton").html('');
					$("#enviarNotificacaoButton").append('<button type="button" class="btn btn-white" data-dismiss="modal" name="fechar">Fechar</button><button type="button" class="btn btn-primary" onclick="enviaNotificacao('+idOrganismoAfiliado+');">Enviar Notificação</button>');

				} else {
					//ESCONDE: Tipo de Notificação, Título e Mensagem
					$("#blocoTipo").css('display', 'none');
					$("#blocoTitulo").css('display', 'none');
					$("#blocoMensagem").css('display', 'none');

					//Mensagem de nenhum oficial disponível
					$("#conteudoModal").html('');
					$("#conteudoModal").append('<div class="form-group"><div class="alert alert-danger"><center><a class="alert-link" href="">Atenção! </a>Não há nenhum oficial cadastrado nesse Organismo!</center></div></div>');

					//Área de botões
					$("#enviarNotificacaoButton").html('');
					$("#enviarNotificacaoButton").append('<button type="button" class="btn btn-white" data-dismiss="modal" name="fechar">Fechar</button>');
				}

				//ABRE MODAL
				$('#mySeeNotificacao').modal({});
			},
			error: function(xhr, textStatus, errorThrown){
				swal({title:"ERRO!",text:"Ocorreu algum erro ao abrir a notificaçãoo! Tente novamente mais tarde.",type:"warning",confirmButtonColor:"#1ab394"});
			}  ,
		});
	}

	function getValueNotificacoes() {

		var Values = {};


		var qntOficiais = $("#qntOficiais").val();

		Values['qntOficiais'] = qntOficiais;

		var oficiaisMarcados = new Array();

		for (var i = 0; i < qntOficiais; i++) {
			$("input[type=checkbox][name='oficial"+i+"[]']:checked").each(function(){
				oficiaisMarcados.push($(this).val());
			});
		}

		Values['oficial']	= oficiaisMarcados;

		Values['tipoNotificacaoGlp']			= $("#tipoNotificacaoGlp option:selected").val();

		Values['tituloNotificacaoGlp']			= $("#tituloNotificacaoGlp").val();

		Values['fk_seqCadastRemetente']			= $("#fk_seqCadastRemetente").val();

		var summernoteNotificacaoGlp            = $("#mensagemNotificacaoGlp.summernote").code();
		Values['mensagemNotificacaoGlp']		= summernoteNotificacaoGlp;

		//print_r(Values);
		return Values;
	}

	function limpaNotificacao(){

		var qntOficiais = $("#qntOficiais").val();
		for (var i = 0; i < qntOficiais; i++) {
			$("input[type=checkbox][name='oficial"+i+"[]']").attr("checked",false);
		}

		$("#tituloNotificacaoGlp").val('');

		$("#mensagemNotificacaoGlp.summernote").code('');

		$("#tipoNotificacaoGlp option:selected").attr("selected",false);
	}

	function enviaNotificacao(idImovel)
	{
		var values = getValueNotificacoes();
		var camposGeraisData = JSON.stringify(values);

		$.ajax({
			type:"post",
			url: "js/ajax/cadastraNotificacaoGlp.php",
			dataType: "json",
			data: {camposGerais:camposGeraisData},
			success: function(retorno){
				if (retorno.erro != undefined && retorno.erro.length > 0) {
					swal({title:"Aviso!",text:retorno.erro,type:"warning",confirmButtonColor:"#1ab394"});
					$('#'+retorno.focus).focus();
				}
				else if (retorno.sucesso != undefined) {
					swal({
						title:"Sucesso!",
						text:retorno.success,
						type:"success",
						confirmButtonColor:"#1ab394"
					});
					//FECHA MODAL
					$('#mySeeNotificacao').modal('toggle');
					limpaNotificacao();
				}
				else {
					swal({title:"Aviso!",text:"Ocorreu algum erro. Entre em contato com o setor de TI.",type:"warning",confirmButtonColor:"#1ab394"});
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log("erro");
			}
		});
	}


	$(document).ready(function() {

/*

 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------
 ---------------------------------------------------------------------------------------------------------------------------------------------------------------

*/
	});

	$('.dataTables-equipes-iniciaticas').dataTable({
		"order": [[ 0, "desc" ]],
		responsive: true,
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
		}
	});

	/** FIM */

	/*
	 Atividade Iniciatica
	 */

	/**
	 * Função responsável por mudar equipes iniciáticas quando o organismo é mudado
	 * @param Int idOrganismo : ID do Organismo Afiliado para retornar Equipe de Atividade Iniciática
	 */
	function mudaOrganismoGetEquipes(idOrganismo) {
		$('#anoAtivInicOficial').hide(300);

		var textoOa = document.getElementById("fk_idOrganismoAfiliado").options[document.getElementById("fk_idOrganismoAfiliado").selectedIndex].text;
        var res = textoOa.split(" - ");
        var res2 = res[1].split(" ");
        var classificacaoOa = res2[0];
        //alert(classificacaoOa);

		$("#classificacaoOa").val(classificacaoOa);

        $('#atividadeIniciaticaOficial').hide(300);
        $('#atividadeIniciaticaColumba').hide(300);

		if(idOrganismo==0){

		} else if(idOrganismo!=0) {
			$('#atividadeIniciaticaOficial').show(300);
            if(classificacaoOa!="Pronaos") {
                $('#atividadeIniciaticaColumba').show(300);
            }
			getEquipesIniciaticasAtivas(idOrganismo,$('#idAtividadeIniciatica').val());
			if(classificacaoOa!="Pronaos") {
                getColumbasAtivas(idOrganismo, $('#idAtividadeIniciatica').val());
            }
		} else {
			$('#atividadeIniciaticaOficial').hide(300);
			$('#atividadeIniciaticaColumba').hide(300);
		}
	}
	$(document).ready(function() {

		 if ($('#fk_idOrganismoAfiliado').val()==0) {
			 $('#atividadeIniciaticaOficial').hide(300);
			 $('#atividadeIniciaticaColumba').hide(300);
		 } else if($('#fk_idOrganismoAfiliado').val()!=0 && $('#fk_idOrganismoAfiliado').val()!=undefined) {
			 $('#atividadeIniciaticaOficial').show(300);
			 $('#atividadeIniciaticaColumba').show(300);

			 getEquipesIniciaticasAtivas($('#fk_idOrganismoAfiliado').val(),$('#idAtividadeIniciatica').val());
			 getColumbasAtivas($('#fk_idOrganismoAfiliado').val(),$('#idAtividadeIniciatica').val());

			 if(($('#codAfiliacaoRecepcao').val() != undefined) && ($('#nomeRecepcao').val() != undefined)){
				 getOficialIniciaticoResponsavel('Recepcao','recepcaoAtividadeIniciaticaOficial');
			 }
		 } else {
			 $('#atividadeIniciaticaOficial').hide(300);
			 $('#atividadeIniciaticaColumba').hide(300);
		 }
	});

	function getEquipesIniciaticasAtivas(idOrganismo,idAtividadeIniciatica) {

		$.ajax({
			type:"post",
			url: "js/ajax/getEquipesIniciaticasAtivas.php",
			dataType: "json",
			data: {idOrganismo:idOrganismo,idAtividadeIniciatica:idAtividadeIniciatica},
			success: function(data){
				//console.log(data);
				if(data.atividade.total>0){
					$("#atividadeIniciaticaOficial").html('');
					$("#atividadeIniciaticaOficial").append('<label class="col-sm-3 control-label">Equipe Iniciática: </label><div class="col-sm-9"><select class="form-control col-sm-5" name="fk_idAtividadeIniciaticaOficial" id="fk_idAtividadeIniciaticaOficial" data-placeholder="Selecione uma equipe responsável..." style="max-width: 350px" required="required"></select></div>');
					$("#fk_idAtividadeIniciaticaOficial").append('<option value="0">Selecione...</option>');
					$("#fk_idAtividadeIniciaticaOficial").append(data.atividade.equipes);
				} else {
					$("#atividadeIniciaticaOficial").html('');
					$("#atividadeIniciaticaOficial").append('<label class="col-sm-3 control-label">Equipe Iniciática: </label><div class="col-sm-9"><p class="form-control-static" style="color: #FA8072"><b>Nenhuma equipe ativa cadastrada.</b></p></div>');
				}

			},
			error: function(xhr, textStatus, errorThrown){
				//console.log('error');
				$("#atividadeIniciaticaOficial").html('');
				$("#atividadeIniciaticaOficial").append('<label class="col-sm-3 control-label">Equipe Iniciática: </label><div class="col-sm-9"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar as equipes iniciáticas! Atualize a página.</b></p></div>');
			}
		});
	}

	function getColumbasAtivas(idOrganismo,idAtividadeIniciatica) {

		$.ajax({
			type:"post",
			url: "js/ajax/getColumbasAtivas.php",
			dataType: "json",
			data: {idOrganismo:idOrganismo,idAtividadeIniciatica:idAtividadeIniciatica},
			success: function(data){
				//console.log(data);
				if(data.atividade.total>0){
					$("#atividadeIniciaticaColumba").html('');
					$("#atividadeIniciaticaColumba").append('<label class="col-sm-3 control-label">Columba: </label><div class="col-sm-9"><select class="form-control col-sm-5" name="fk_idAtividadeIniciaticaColumba" id="fk_idAtividadeIniciaticaColumba" data-placeholder="Selecione a columba escalada..." style="max-width: 350px" required="required"></select></div>');
					$("#fk_idAtividadeIniciaticaColumba").append('<option value="0">Selecione...</option>');
					$("#fk_idAtividadeIniciaticaColumba").append(data.atividade.columbas);
				} else {
					$("#atividadeIniciaticaColumba").html('');
					$("#atividadeIniciaticaColumba").append('<label class="col-sm-3 control-label">Columba: </label><div class="col-sm-9"><p class="form-control-static" style="color: #FA8072"><b>Nenhuma columba ativa cadastrada.</b></p></div>');
				}
			},
			error: function(xhr, textStatus, errorThrown){
				//console.log('error');
				$("#atividadeIniciaticaColumba").html('');
				$("#atividadeIniciaticaColumba").append('<label class="col-sm-3 control-label">Columba: </label><div class="col-sm-9"><p class="form-control-static" style="color: #FA8072"><b>Ocorreu um erro ao buscar as columbas! Atualize a página.</b></p></div>');
			}
		});
	}

	$(document).ready(function(){
		$('#datapicker_atividade_iniciatica .input-group.date').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			format: "dd/mm/yyyy",
			language: "pt-BR",
			autoclose: true
		});
	});

    $(document).ready(function(){
        //var date = new Date(2015, 11, 31, 23, 59);
        //console.log("Date: " + date);
        $('#datapicker_atividade_iniciatica_agil .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            //maxDate: new Date(2015, 11, 31),
            maxDate: "+1m +1w",
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });
        //console.log('MaxDate: ' + new Date(2015, 11, 31));
        //$('#datapicker_atividade_iniciatica_agil .input-group.date').datepicker('option', 'maxDate', new Date(2015, 11, 31));
    });

	$(document).ready(function(){
		$('.datapicker .input-group.date').datepicker({
			todayBtn: "linked",
			keyboardNavigation: false,
			forceParse: false,
			calendarWeeks: true,
			format: "dd/mm/yyyy",
			language: "pt-BR",
			autoclose: true
		});
	});

	/** FIM */


	$(document).ready(function() {
		$('#propriedadeIptuImovelControle').bind('change', function(){
			 $('#propriedadeIptuObs').hide(300);
			 $('#propriedadeIptuAnexo').hide(300);
	   		 if($(this).val()==0){
	   			//console.log($(this).val());
	   			$('#labelPropriedadeIptuAnexo').html('Anexar IPTU Pago: ');
	   			$('#propriedadeIptuObs').hide(300);
	   			$('#propriedadeIptuAnexo').show(300);
	   		 } else if($(this).val()==1){
	   			//console.log($(this).val());
	   			$('#labelPropriedadeIptuAnexo').html('Anexar IPTU Em Débito: ');
	   			$('#propriedadeIptuObs').hide(300);
	   			$('#propriedadeIptuAnexo').show(300);
	   		} else if($(this).val()==2){
	   			//console.log($(this).val());
	   			$('#labelPropriedadeIptuAnexo').html('Anexar Isenção de IPTU: ');
	   			$('#propriedadeIptuObs').hide(300);
	   			$('#propriedadeIptuAnexo').show(300);
	   		} else if($(this).val()==3){
	   			//console.log($(this).val());
	   			$('#labelPropriedadeIptuAnexo').html('Anexar Imunidade de IPTU: ');
	   			$('#propriedadeIptuObs').hide(300);
	   			$('#propriedadeIptuAnexo').show(300);
	   		} else if($(this).val()==4){
	   			//console.log($(this).val());
	   			$('#labelPropriedadeIptuAnexo').html('Anexar IPTU/Outros: ');
	   			$('#propriedadeIptuObs').show(300);
	   			$('#propriedadeIptuAnexo').show(300);
	   		 } else {
	   			//console.log($(this).val());
	   			$('#propriedadeIptuObs').hide(300);
	   			$('#propriedadeIptuAnexo').hide(300);
	   		 }
  	    });

		if($('#propriedadeIptuImovelControle').val()==0){
			$('#propriedadeIptuObs').hide(300);
			$('#propriedadeIptuAnexo').show(300);
		} else if($(this).val()==1){
			//console.log($(this).val());
			$('#propriedadeIptuObs').hide(300);
			$('#propriedadeIptuAnexo').show(300);
		} else if($(this).val()==2){
			//console.log($(this).val());
			$('#propriedadeIptuObs').hide(300);
			$('#propriedadeIptuAnexo').show(300);
		} else if($(this).val()==3){
			//console.log($(this).val());
			$('#propriedadeIptuObs').hide(300);
			$('#propriedadeIptuAnexo').show(300);
		} else if($('#propriedadeIptuImovelControle').val()==4){
			//console.log($('#propriedadeIptuImovelControle').val());
			$('#propriedadeIptuObs').show(300);
			$('#propriedadeIptuAnexo').show(300);
		} else {
			//console.log($(this).val());
			$('#propriedadeIptuObs').hide(300);
			$('#propriedadeIptuAnexo').hide(300);
		}
    });

	$(document).ready(function() {
		$('#habiteSeImovel').bind('change', function(){
			 $('#habiteSeAnexo').hide(300);
	   		 if($(this).val()==0){
	   			$('#habiteSeAnexo').show(300);
	   		 } else if($(this).val()==1) {
	   			$('#habiteSeAnexo').hide(300);
	   		 }
  	    });

		if($('#habiteSeImovel').val()!=0){
			$('#habiteSeAnexo').hide(300);
		}
    });

	$(document).ready(function() {
		//console.log($('#avaliacaoImovel').val());
		$('#avaliacaoImovel').bind('change', function(){
			 $('#avaliacao').hide(300);
			 //$('#avaliacaoValor').hide(300);
	   		 if($(this).val()==0){
	   			//console.log("Aqui caso diferente ==0: "+$('#avaliacaoImovel').val());
	   			$('#avaliacao').show(300);
	   			//$('#avaliacaoValor').show(300);
	   		 } else if($(this).val()==1) {
	   			//console.log("Aqui caso diferente ==1: "+$('#avaliacaoImovel').val());
	   			$('#avaliacao').hide(300);
	   			//$('#avaliacaoValor').show(300);
	   		 }
  	    });

		if($('#avaliacaoImovel').val()!=0 && $('#avaliacaoImovel').val()!=1){
			//console.log("Aqui caso diferente !=0 e !=1: "+$('#avaliacaoImovel').val());
			$('#avaliacao').hide(300);
			//$('#avaliacaoValor').hide(300);
		}else if($('#avaliacaoImovel').val()==1){
			//console.log("Aqui caso diferente ==1: "+$('#avaliacaoImovel').val());
			$('#avaliacao').hide(300);
			//$('#avaliacaoValor').show(300);
		}
    });

	$(document).ready(function() {
        $('#vistoriaBombeiroImovel').bind('change', function(){
            $('#vistoriaBombeiroAnexo').hide(300);
            if($(this).val()==0){
                $('#vistoriaBombeiroAnexo').show(300);
            } else if($(this).val()==1) {
                $('#vistoriaBombeiroAnexo').hide(300);
            }
        });

        if($('#vistoriaBombeiroImovel').val()!=0){
            $('#vistoriaBombeiroAnexo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#seguroImovel').bind('change', function(){
            $('#seguroAnexo').hide(300);
            if($(this).val()==0){
                $('#seguroAnexo').show(300);
            } else if($(this).val()==1) {
                $('#seguroAnexo').hide(300);
            }
        });

        if($('#seguroImovel').val()!=0){
            $('#seguroAnexo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#construcaoImovelControle').bind('change', function(){
            $('#construcao').hide(300);
            if($(this).val()==0){
                $('#construcao').show(300);
            } else if($(this).val()==1) {
                $('#construcao').hide(300);
            }
        });

        if($('#construcaoImovelControle').val()!=0){
            $('#construcao').hide(300);
        }
    });
    $(document).ready(function() {
        $('#construcaoAlvaraImovelControle').bind('change', function(){
            $('#construcaoAlvaraAnexo').hide(300);
            if($(this).val()==0){
                $('#construcaoAlvaraAnexo').show(300);
            } else if($(this).val()==1) {
                $('#construcaoAlvaraAnexo').hide(300);
            }
        });

        if($('#construcaoAlvaraImovelControle').val()!=0){
            $('#construcaoAlvaraAnexo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#construcaoRestricoesImovelControle').bind('change', function(){
            $('#construcaoRestricoesAnotacoes').hide(300);
            if($(this).val()==0){
                $('#construcaoRestricoesAnotacoes').show(300);
            } else if($(this).val()==1) {
                $('#construcaoRestricoesAnotacoes').hide(300);
            }
        });

        if($('#construcaoRestricoesImovelControle').val()!=0){
            $('#construcaoRestricoesAnotacoes').hide(300);
        }
    });
    $(document).ready(function() {
        $('#construcaoConcluidaImovelControle').bind('change', function(){

            $('#datapicker_termino_construcao').hide(300);

            if($(this).val()==0){
                $('#datapicker_termino_construcao').show(300);
                $('#datapicker_termino_construcao_label').html('Data de Término da Construção: ');
            } else if($(this).val()==1) {
                $('#datapicker_termino_construcao').show(300);
                $('#datapicker_termino_construcao_label').html('Prazo de Término da Construção: ');
            }

        });

        if($('#construcaoConcluidaImovelControle').val()==0){
            $('#datapicker_termino_construcao').show(300);
            $('#datapicker_termino_construcao_label').html('Data de Término da Construção: ');
        } else if($('#construcaoConcluidaImovelControle').val()==1) {
            $('#datapicker_termino_construcao').show(300);
            $('#datapicker_termino_construcao_label').html('Prazo de Término da Construção: ');
        } else {
        	$('#datapicker_termino_construcao').hide(300);
        }
    });
    $(document).ready(function() {
        $('#construcaoInssImovelControle').bind('change', function(){
            $('#construcaoInssAnexo').hide(300);
            if($(this).val()==0){
                $('#construcaoInssAnexo').show(300);
            } else if($(this).val()==1) {
                $('#construcaoInssAnexo').hide(300);
            }
        });

        if($('#construcaoInssImovelControle').val()!=0){
            $('#construcaoInssAnexo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#alvaraImovel').bind('change', function(){
            $('#alvaraAnexo').hide(300);
            if($(this).val()==0){
                $('#alvaraAnexo').show(300);
            } else if($(this).val()==1) {
                $('#alvaraAnexo').hide(300);
            }
        });

        if($('#alvaraImovel').val()!=0){
            $('#alvaraAnexo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#plantaImovel').bind('change', function(){
            $('#plantaAnexo').hide(300);
            if($(this).val()==0){
                $('#plantaAnexo').show(300);
            } else if($(this).val()==1) {
                $('#plantaAnexo').hide(300);
            }
        });

        if($('#plantaImovel').val()!=0){
            $('#plantaAnexo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#propriedadeEscrituraImovel').bind('change', function(){
            $('#escrituraImovel').hide(300);
            if($(this).val()==0){
                $('#escrituraImovel').show(300);
            } else if($(this).val()==1) {
                $('#escrituraImovel').hide(300);
            }
        });

        if($('#propriedadeEscrituraImovel').val()!=0){
            $('#escrituraImovel').hide(300);
        }
    });
    $(document).ready(function() {
        $('#registroImovel').bind('change', function(){
            $('#registro').hide(300);
            if($(this).val()==0){
                $('#registro').show(300);
            } else if($(this).val()==1) {
                $('#registro').hide(300);
            }
        });

        if($('#registroImovel').val()!=0){
            $('#registro').hide(300);
        }
    });
    $(document).ready(function() {

        $('#aluguelImovel').bind('change', function(){

            $('#aluguelAnexo').hide(300);

            if($(this).val()==0){
                $('#aluguelAnexo').show(300);
            } else if($(this).val()==1) {
                $('#aluguelAnexo').hide(300);
            }

        });

        if($('#aluguelImovel').val()==0){

            $('#aluguelAnexo').show(300);

        }else if($('#aluguelImovel').val()==1) {

            $('#aluguelAnexo').hide(300);

        }
    });
    $(document).ready(function() {
        $('#manutencaoImovelControle').bind('change', function(){
            $('#manutencao').hide(300);
            if($(this).val()==0){
                $('#manutencao').show(300);
            } else if($(this).val()==1) {
                $('#manutencao').hide(300);
            }
        });

        if($('#manutencaoImovelControle').val()!=0){
            $('#manutencao').hide(300);
        }
    });
    $(document).ready(function() {
        $('#manutencaoPinturaImovelControle').bind('change', function(){
            $('#manutencaoPinturaTipo').hide(300);
            if($(this).val()==0){
                $('#manutencaoPinturaTipo').show(300);
            } else if($(this).val()==1) {
                $('#manutencaoPinturaTipo').hide(300);
            }
        });

        if($('#manutencaoPinturaImovelControle').val()!=0){
            $('#manutencaoPinturaTipo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#manutencaoHidraulicaImovelControle').bind('change', function(){
            $('#manutencaoHidraulicaTipo').hide(300);
            if($(this).val()==0){
                $('#manutencaoHidraulicaTipo').show(300);
            } else if($(this).val()==1) {
                $('#manutencaoHidraulicaTipo').hide(300);
            }
        });

        if($('#manutencaoHidraulicaImovelControle').val()!=0){
            $('#manutencaoHidraulicaTipo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#manutencaoEletricaImovelControle').bind('change', function(){
            $('#manutencaoEletricaTipo').hide(300);
            if($(this).val()==0){
                $('#manutencaoEletricaTipo').show(300);
            } else if($(this).val()==1) {
                $('#manutencaoEletricaTipo').hide(300);
            }
        });

        if($('#manutencaoEletricaImovelControle').val()!=0){
            $('#manutencaoEletricaTipo').hide(300);
        }
    });
    $(document).ready(function() {
        $('#propriedadeStatusImovel').bind('change', function(){

            $('#proprioImovel').hide(300);
            $('#alugadoImovel').hide(300);

            //console.log('propriedadeStatusImovel: '+$(this).val());

            if($(this).val()==0){
                $('#proprioImovel').hide(300);
                $('#alugadoImovel').hide(300);
            } else if($(this).val()==1) {
                $('#proprioImovel').show(300);
                $('#alugadoImovel').hide(300);
            } else if($(this).val()==2) {
                $('#proprioImovel').hide(300);
                $('#alugadoImovel').show(300);
            } else if($(this).val()==3) {
                $('#proprioImovel').hide(300);
                $('#alugadoImovel').show(300);
            } else if($(this).val()==4) {
                $('#proprioImovel').hide(300);
                $('#alugadoImovel').show(300);
            }

        });

        //console.log('propriedadeStatusImovel: '+$('#propriedadeStatusImovel').val());

        if($('#propriedadeStatusImovel').val()==0){
            $('#proprioImovel').hide(300);
            $('#alugadoImovel').hide(300);
        }else if($('#propriedadeStatusImovel').val()==1){
            $('#proprioImovel').show(300);
            $('#alugadoImovel').hide(300);
        }else if($('#propriedadeStatusImovel').val()==2){
            $('#proprioImovel').hide(300);
            $('#alugadoImovel').show(300);
        }else if($('#propriedadeStatusImovel').val()==3){
            $('#proprioImovel').hide(300);
            $('#alugadoImovel').show(300);
        }else if($('#propriedadeStatusImovel').val()==4){
            $('#proprioImovel').hide(300);
            $('#alugadoImovel').show(300);
        }
    });

	$('#propriedadeIptuAnexoImovelControle').bind('change', function() {
		var inputFile = $('#propriedadeIptuAnexoImovelControle');
	  	if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
	  		inputFile.replaceWith(inputFile = inputFile.clone(true));
	  		$('#propriedadeIptuAnexoAlerta').css("color", "#FA8072");
	  		$('#propriedadeIptuAnexoAlerta').css("font-weight", "bold");
	  	} else {
	  		$('#propriedadeIptuAnexoAlerta').css("color", "#4F5B93");
	  		$('#propriedadeIptuAnexoAlerta').css("font-weight", "normal");
	  	}
	});
	$('#habiteSeAnexoImovel').bind('change', function() {
		var inputFile = $('#habiteSeAnexoImovel');
	  	if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
	  		inputFile.replaceWith(inputFile = inputFile.clone(true));
	  		$('#habiteSeAnexoAlerta').css("color", "#FA8072");
	  		$('#habiteSeAnexoAlerta').css("font-weight", "bold");
	  	} else {
	  		$('#habiteSeAnexoAlerta').css("color", "#4F5B93");
	  		$('#habiteSeAnexoAlerta').css("font-weight", "normal");
	  	}
	});
	$('#vistoriaBombeiroAnexoImovel').bind('change', function() {
		var inputFile = $('#vistoriaBombeiroAnexoImovel');
	  	if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
	  		inputFile.replaceWith(inputFile = inputFile.clone(true));
	  		$('#vistoriaBombeiroAnexoAlerta').css("color", "#FA8072");
	  		$('#vistoriaBombeiroAnexoAlerta').css("font-weight", "bold");
	  	} else {
	  		$('#vistoriaBombeiroAnexoAlerta').css("color", "#4F5B93");
	  		$('#vistoriaBombeiroAnexoAlerta').css("font-weight", "normal");
	  	}
	});
	$('#avaliacaoAnexoImovel').bind('change', function() {
		var inputFile = $('#avaliacaoAnexoImovel');
	  	if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
	  		inputFile.replaceWith(inputFile = inputFile.clone(true));
	  		$('#avaliacaoAnexoAlerta').css("color", "#FA8072");
	  		$('#avaliacaoAnexoAlerta').css("font-weight", "bold");
	  	} else {
	  		$('#avaliacaoAnexoAlerta').css("color", "#4F5B93");
	  		$('#avaliacaoAnexoAlerta').css("font-weight", "normal");
	  	}
	});
	$('#seguroAnexoImovel').bind('change', function() {
		var inputFile = $('#seguroAnexoImovel');
	  	if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
	  		inputFile.replaceWith(inputFile = inputFile.clone(true));
	  		$('#seguroAnexoAlerta').css("color", "#FA8072");
	  		$('#seguroAnexoAlerta').css("font-weight", "bold");
	  	} else {
	  		$('#seguroAnexoAlerta').css("color", "#4F5B93");
	  		$('#seguroAnexoAlerta').css("font-weight", "normal");
	  	}
	});
	$('#construcaoAlvaraAnexoImovelControle').bind('change', function() {
		var inputFile = $('#construcaoAlvaraAnexoImovelControle');
	  	if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
	  		inputFile.replaceWith(inputFile = inputFile.clone(true));
	  		$('#construcaoAlvaraAnexoAlerta').css("color", "#FA8072");
	  		$('#construcaoAlvaraAnexoAlerta').css("font-weight", "bold");
	  	} else {
	  		$('#construcaoAlvaraAnexoAlerta').css("color", "#4F5B93");
	  		$('#construcaoAlvaraAnexoAlerta').css("font-weight", "normal");
	  	}
	});
	$('#construcaoInssAnexoImovelControle').bind('change', function() {
		var inputFile = $('#construcaoInssAnexoImovelControle');
	  	if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
	  		inputFile.replaceWith(inputFile = inputFile.clone(true));
	  		$('#construcaoInssAnexoAlerta').css("color", "#FA8072");
	  		$('#construcaoInssAnexoAlerta').css("font-weight", "bold");
	  	} else {
	  		$('#construcaoInssAnexoAlerta').css("color", "#4F5B93");
	  		$('#construcaoInssAnexoAlerta').css("font-weight", "normal");
	  	}
	});
	$('#alvaraAnexoImovel').bind('change', function() {
		var inputFile = $('#alvaraAnexoImovel');
		if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
			inputFile.replaceWith(inputFile = inputFile.clone(true));
			$('#alvaraAnexoAlerta').css("color", "#FA8072");
			$('#alvaraAnexoAlerta').css("font-weight", "bold");
		} else {
			$('#alvaraAnexoAlerta').css("color", "#4F5B93");
			$('#alvaraAnexoAlerta').css("font-weight", "normal");
		}
	});
	$('#propriedadeEscrituraAnexoImovel').bind('change', function() {
		var inputFile = $('#propriedadeEscrituraAnexoImovel');
		if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
			inputFile.replaceWith(inputFile = inputFile.clone(true));
			$('#escrituraAnexoAlerta').css("color", "#FA8072");
			$('#escrituraAnexoAlerta').css("font-weight", "bold");
		} else {
			$('#escrituraAnexoAlerta').css("color", "#4F5B93");
			$('#escrituraAnexoAlerta').css("font-weight", "normal");
		}
	});
	$('#registroImovelAnexoImovel').bind('change', function() {
		var inputFile = $('#registroImovelAnexoImovel');
		if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
			inputFile.replaceWith(inputFile = inputFile.clone(true));
			$('#registroAnexoAlerta').css("color", "#FA8072");
			$('#registroAnexoAlerta').css("font-weight", "bold");
		} else {
			$('#registroAnexoAlerta').css("color", "#4F5B93");
			$('#registroAnexoAlerta').css("font-weight", "normal");
		}
	});
	$('#plantaAnexoImovel').bind('change', function() {
		var inputFile = $('#plantaAnexoImovel');
		if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
			inputFile.replaceWith(inputFile = inputFile.clone(true));
			$('#plantaAnexoAlerta').css("color", "#FA8072");
			$('#plantaAnexoAlerta').css("font-weight", "bold");
		} else {
			$('#plantaAnexoAlerta').css("color", "#4F5B93");
			$('#plantaAnexoAlerta').css("font-weight", "normal");
		}
	});
	$('#aluguelAnexoImovel').bind('change', function() {
		var inputFile = $('#aluguelAnexoImovel');
		if(this.files[0].size > 10000000){
			swal({title:"Aviso!",text:"Arquivo acima no máximo de 10MB!",type:"warning",confirmButtonColor:"#1ab394"});
			inputFile.replaceWith(inputFile = inputFile.clone(true));
			$('#aluguelAnexoAlerta').css("color", "#FA8072");
			$('#aluguelAnexoAlerta').css("font-weight", "bold");
		} else {
			$('#aluguelAnexoAlerta').css("color", "#4F5B93");
			$('#aluguelAnexoAlerta').css("font-weight", "normal");
		}
	});



	function abrirPopUpImprimirImovelControle(idImovelControle) {
		window.open('impressao/detalheImovelControle.php?idImovelControle='+idImovelControle,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
	}

    function abrirPopUpImprimirImovel(idImovel) {
        window.open('impressao/detalheImovel.php?idImovel='+idImovel,'page','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }

	/**
	 * Cadastro de Tipos de  Atividades para Organismo Afiliado
	 */
	//
	function showQtdAtividade(){

		if ($("input[type=checkbox][name='companheiro[]").is(':checked')){
			var compVal = 1;
		} else {
			var compVal = 0;
		}

		/*
		console.log("Entra");

		if ($("input[type=checkbox][name='lojaAtividadeTipo[]").is(':checked')){
			var checkBoxVal = 1;
		} else {
			var checkBoxVal = 0;
		}
		console.log(checkBoxVal);
		$('#lojaAtividadeTipoCheck').prop("checked")=="checked";


            $('#qntLojaAtividade').hide(300);

        	var aChk = $('#qntLojaAtividade').val();

            console.log(aChk);
            if(aChk.checked == true){
                $('#qntLojaAtividade').show(300);
            } else {
                $('#qntLojaAtividade').hide(300);
            }
        });

        if($('#lojaAtividadeTipo').val()!=1){
            $('#qntLojaAtividade').hide(300);
        }
        */
    }

    //Máscaras
	//$("#cepImovel").mask("99999-999");
    //$('#areaTotalImovel').mask("999.999", {reverse: true});
	//$("#telefoneComercialUsuario").mask("(999) 9999-9999");
	//$("#celularUsuario").mask("(999) 9999-9999");
    $(document).ready(function () {
        $('.numberDecimal').mask("999.999", {reverse: true});
        //$(".currency").maskMoney({symbol: 'R$ ', showSymbol: false, thousands: '.', decimal: ',', symbolStay: true});
    });



	/**
	 * Upload das imagens por meio da lib Uploadfy
	 */
	$(document).ready(function() {
		//Upload
		$('#file_upload_imovel_galeria').uploadifive({

			'uploadScript' : 'js/ajax/uploadImovelGaleria.php',
			'auto'	   : true,
			'width'    : 200,
			'height'   : 34,
			'uploadLimit' : 50,
			'fileSizeLimit' : '5MB',
			'fileTypeExts' : '*.jpeg; *.jpg; *.png; *.gif',
			'buttonText' : '<i class="fa fa-upload fa-white"></i> Carregar Foto',
			'onUploadStart' : function(file) {
				var data = {'idImovel' : $("#idImovel").val()};
				//alert($("#idDocumento").val());
				$('#file_upload_imovel_galeria').uploadifive('settings', 'formData', data);
			},
			'onUploadSuccess' : function(file, data, response) {
				if (response && (data != 1 && escape(data) != '%uFEFF%uFEFF1')) {
					$("#"+file.id+" .data").html("<font color='red'><br /><br />Erro: "+data+"</font>");
				}
				else {
					//console.log("Documento Enviado!");
                    swal({title:"Sucesso!",text:"Upload da imagem realizado com sucesso! Feche a janela Publicar Novas Fotos e confira o resultado!",type:"success",confirmButtonColor:"#1ab394"});
					getImovelGaleria($("#idImovel").val());
				}
			},
			'onSWFReady' : function() {
				//if (id == '') {
					//$("#file_upload_imovel_galeria"+posicao).uploadify('disable', true);
				//}
			}
		});
	});
	/**
	 * Carrega imagens do imóvel, excluído=0, na página
	 */
	function getImovelGaleria(idImovel) {
		$.ajax({
			//method: "post",
			type:"post",
			url: "js/ajax/getImovelGaleria.php",
			//async: false,
			//sycr: false,
			dataType: "json",
			data: {idImovel:idImovel},
			success: function(data){
				//console.log(data);
				$("#lightBoxGallery").html('');
				if(data.temFoto!=0)
				{
					for (var i = 0; i < data.foto.length; i++) {
						$("#lightBoxGallery").append(data.foto[i].galeria);
					}
				} else {
					$("#lightBoxGallery").append('<center><i>Nenhuma imagem cadastrada</i></center>');
				}
				$("#lightBoxGallery").append('<div id="blueimp-gallery" class="blueimp-gallery"><div class="slides"></div><h3 class="title"></h3><a class="prev">‹</a><a class="next">›</a><a class="close">×</a><a class="play-pause"></a><ol class="indicator"></ol></div>');
			},
			error: function(xhr, textStatus, errorThrown){
				//console.log('error');
				$("#lightBoxGallery").html('');
				$("#lightBoxGallery").append('<center style="color: #FA8072"><i>Ocorreu um erro ao buscar as imagens publicadas</i></center>');
				$("#lightBoxGallery").append('<div id="blueimp-gallery" class="blueimp-gallery"><div class="slides"></div><h3 class="title"></h3><a class="prev">‹</a><a class="next">›</a><a class="close">×</a><a class="play-pause"></a><ol class="indicator"></ol></div>');
			}
		});
	}
	/**
	 * Exclui imagem do imóvel, excluído=1, e os retira da página
	 */
	function excluiImovelGaleria(id,idImovel) {
		$.ajax({
			type:"post",
			url: "js/ajax/excluiImovelGaleriaImagem.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
			dataType: "json",
			data: {id:id, idImovel:idImovel}, // exemplo: {codigo:125, nome:"Joaozinho"}
			success: function(data){
				//console.log(data);
				if (data.sucess == true) {
					getImovelGaleria(data.idImovel);
				}
				else {
					swal({title:"Aviso!",text:"Ocorreu um erro ao excluir a imagem!",type:"warning",confirmButtonColor:"#1ab394"});
				}
			}
		});
	}

    $(document).ready(function(){

		$('#datapicker_anotacoes .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

	});

    $(document).ready(function(){

		$('#datapicker_imovel .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

	});

    $(document).ready(function(){

		$('#datapicker_escritura .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

	});
    $(document).ready(function(){

        $('#datapicker_registro_imovel .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

    });
    $(document).ready(function(){

        $('#datapicker_termino_construcao .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

    });
    $(document).ready(function(){

        $('#datapicker_contrato_inicio .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

    });
    $(document).ready(function(){

        $('#datapicker_contrato_fim .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

    });


    function mascara_cep(edit){
		if(event.keyCode<48 || event.keyCode>57){
			event.returnValue=false;
		}
		if(edit.value.length==5){
			edit.value+="-";
		}
	}

    function valida_horas(edit){
		if(event.keyCode<48 || event.keyCode>57){
			event.returnValue=false;
		}
		if(edit.value.length==2){
			edit.value+=":";
		}
		if (edit.value.length == 5){
			verifica_hora(edit.value);
		}
	}

	function verifica_hora(edit){
        hrs = (document.forms[0].edit.value.substring(0,2));
        min = (document.forms[0].edit.value.substring(3,5));

        alert('hrs '+ hrs);
        alert('min '+ min);

        situacao = "";
        // verifica data e hora
        if ((hrs < 0 ) || (hrs > 23) || ( min < 0) ||( min > 59)){
            situacao = "falsa";
        }

        if (document.forms[0].edit.value == "") {
            situacao = "falsa";
        }

        if (situacao == "falsa") {
            alert("Hora inválida!");
            document.forms[0].edit.focus();
        }
    }

	function apenasNumeros(){
		if (event.keyCode<48 || event.keyCode>57){
			event.returnValue=false;
		}
	}

	function valida_data(edit,nome){
		$('#data_invalida').hide(1);
		if (event.keyCode<48 || event.keyCode>57){
			event.returnValue=false;
		}
		if (edit.value.length==2 || edit.value.length==5){
			edit.value+="/";
		}
		if (edit.value.length >= 9){
			//console.log(edit.value.length);
            verifica_data(nome);
        }
	}

    /*
    function formatReal( int )
    {
        var tmp = int+'';
        var neg = false;
        if(tmp.indexOf("-") == 0)
        {
            neg = true;
            tmp = tmp.replace("-","");
        }

        if(tmp.length == 1) tmp = "0"+tmp

        tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
        if( tmp.length > 6)
            tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

        if( tmp.length > 9)
            tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");

        if( tmp.length > 12)
            tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");

        if(tmp.indexOf(".") == 0) tmp = tmp.replace(".","");
        if(tmp.indexOf(",") == 0) tmp = tmp.replace(",","0,");

        return (neg ? '-'+tmp : tmp);
    }
    */

	function verifica_data(nome) {
	   //console.log(nome);

	   var dia = $('#'+nome).val();
	   dia = dia.substring(0,2);

	   var mes = $('#'+nome).val();
	   mes = mes.substring(3,5);

	   var ano = $('#'+nome).val();
	   ano = ano.substring(6,10);

	   var situacao = "";
       if ((dia < 1)||(dia < 1 || dia > 30) && (  mes == 4 || mes == 6 || mes == 9 || mes == 11 ) || dia > 31) {
    	   situacao = "falsa";
       }
       if (mes < 1 || mes > 12 ) {
    	   situacao = "falsa";
       }
       if (mes == 2 && ( dia < 1 || dia > 29 || ( dia > 28 && (parseInt(ano / 4) != ano / 4)))) {
    	   situacao = "falsa";
       }
       /*
       if (document.getElementsById(nome).value == "") {
    	   situacao = "falsa";
       }
       */
       if (situacao == "falsa") {
    	   //alert("Data inválida!");
    	   $('#data_invalida_'+nome).show(1);

    	   $('#'+nome).focus();
       }
	}

    $(document).ready(function () {
        $('.contact-box').each(function () {
            animationHover(this, 'pulse');
        });
    });

    $(document).ready(function(){

        var $image = $(".image-crop > img")
        $($image).cropper({
        	autoCropArea: 0.5,
            aspectRatio: 1.618,
            preview: ".img-preview",
            done: function(data) {
        		//console.log(data);
        		//console.log("x: "+data.x);
        		//console.log("y: "+data.y);
        		//console.log("width: "+data.width);
        		//console.log("height: "+data.height);
                // Output the result data for cropping image.
            }
        });

        var $inputImage = $("#inputImage");
        if (window.FileReader) {
            $inputImage.change(function() {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function () {
                        $inputImage.val("");
                        $image.cropper("reset", true).cropper("replace", this.result);
                    };
                } else {
                    showMessage("Please choose an image file.");
                }
            });
        } else {
            $inputImage.addClass("hide");
        }

        $("#download").click(function() {
            window.open($image.cropper("getDataURL"));
        });

        $("#zoomIn").click(function() {
            $image.cropper("zoom", 0.1);
        });

        $("#zoomOut").click(function() {
            $image.cropper("zoom", -0.1);
        });

        $("#rotateLeft").click(function() {
            $image.cropper("rotate", 45);
        });

        $("#rotateRight").click(function() {
            $image.cropper("rotate", -45);
        });

        $("#setDrag").click(function() {
            $image.cropper("setDragMode", "crop");
        });

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            format: "dd/mm/yyyy",
            language: "pt-BR",
            autoclose: true
        });

        $('#data_2 .input-group.date').datepicker({
            startView: 1,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $('#data_3 .input-group.date').datepicker({
            startView: 2,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $('#data_4 .input-group.date').datepicker({
            minViewMode: 1,
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            todayHighlight: true
        });

        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });


    });

    function cadastraUsuarioInterno()
    {
        if($('#nomeUsuario').val()=="")
        {
			swal({title:"Aviso!",text:"Preencha o campo nome.",type:"warning",confirmButtonColor:"#1ab394"});
        }
        else if($('#codigoAfiliacaoUsuario').val()=="")
        {
			swal({title:"Aviso!",text:"Preencha o campo código de afiliação.",type:"warning",confirmButtonColor:"#1ab394"});
        }
        else if($('#telefoneResidencialUsuario').val()==""&&$('#celularUsuario').val()==""&&$('#telefoneComercialUsuario').val()=="")
        {
			swal({title:"Aviso!",text:"Preencha pelo menos 1 telefone.",type:"warning",confirmButtonColor:"#1ab394"});
        }
        else if($('#emailUsuario').val()=="")
        {
			swal({title:"Aviso!",text:"Preencha o campo e-mail.",type:"warning",confirmButtonColor:"#1ab394"});
        }
        else if(!validaEmail($('#emailUsuario').val()))
        {
			swal({title:"Aviso!",text:"Preencha corretamente o campo e-mail.",type:"warning",confirmButtonColor:"#1ab394"});
        }
        else if($('#loginUsuario').val()=="")
        {
			swal({title:"Aviso!",text:"Impossível gerar o login. Entre em contato com o setor de TI ti@amorc.org.br.",type:"warning",confirmButtonColor:"#1ab394"});
        }
        else if($('#seq_cadast').val()=="0"||$('#seq_cadast').val()=="")
        {
			swal({title:"Aviso!",text:"Não encontramos seu cadastro no sistema interno. Entre em contato com o setor de Organismos Afiliados.",type:"warning",confirmButtonColor:"#1ab394"});
        }
        else if($('#jaCadastrado').val()=="1")
        {
			swal({title:"Aviso!",text:"Você já foi cadastrado. Entre em contato com o setor de Organismos Afiliados para saber seu login e sua senha.",type:"warning",confirmButtonColor:"#1ab394"});
        }else{
	        // Chamar o cadastro de usuários
        	cadastraUsuarioInt();
	        // Se tiver funções então setar permissao
	        if($('#totalFuncoes').val()>0)
	        {
	        	setarPermissaoMembro();
	        }

        	// Mostrar mensagem de sucesso
			swal({title:"Sucesso!",text:"Anote seu login e senha.\nSeu login é: "+$('#loginUsuario').val()+".\nSua senha é: "+$('#senhaUsuario').val(),type:"success",confirmButtonColor:"#1ab394"});

        	window.location='?corpo=buscaUsuario';
        }
    }

    function validaEmail(email)
    {
    	var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    	if(!filter.test(email)){
    		return false;
    	}else{
    		return true;
    	}
    }

    function cadastraUsuarioInt()
    {
    	var nomeUsuario					= $("#nomeUsuario").val();
    	var codigoAfiliacaoUsuario		= $("#codigoAfiliacaoUsuario").val();
    	var telefoneResidencialUsuario	= $("#telefoneResidencialUsuario").val();
    	var celularUsuario				= $("#celularUsuario").val();
    	var telefoneComercialUsuario	= $("#telefoneComercialUsuario").val();
    	var emailUsuario				= $("#emailUsuario").val();
    	var loginUsuario				= $("#loginUsuario").val();
    	var senhaUsuario				= $("#senhaUsuario").val();
    	var seq_cadast					= $("#seq_cadast").val();
    	var regiaoUsuario				= $("#regiaoUsuario").val();
    	var fk_idDepartamento			= $("#fk_idDepartamento").val();
    	alert("departamento aqui:"+fk_idDepartamento);
    	//Se o total de funções for 0 cadastrar membro como inativo
    	if($("#totalFuncoes").val()==0)
    	{
    		var inativo=1;//Usuario inativo
    	}else{
    		var inativo=0;
    	}

    	$.ajax({
    		type:"post",
    		url: "js/ajax/cadastraUsuario.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
    		dataType: "json",
    		data: {nomeUsuario:nomeUsuario,
    				codigoAfiliacaoUsuario:codigoAfiliacaoUsuario,
    				telefoneResidencialUsuario:telefoneResidencialUsuario,
    				celularUsuario:celularUsuario,
    				telefoneComercialUsuario:telefoneComercialUsuario,
    				emailUsuario:emailUsuario,
    				loginUsuario:loginUsuario,
    				senhaUsuario:senhaUsuario,
    				seq_cadast:seq_cadast,
    				regiaoUsuario:regiaoUsuario,
    				inativo:inativo,
    				fk_idDepartamento:fk_idDepartamento}, // exemplo: {codigo:125, nome:"Joaozinho"}
    		success: function(retorno){
    			// Verifica se ocorreu algum erro
    			if (retorno.erro != undefined && retorno.erro.length > 0) {
    				var erro = 'Ocorreram um ou mais erros:\n\n';
    				for (var i =0; i < retorno.erro.length; i++) {
    					erro += retorno.erro[i] + '\n';
    				}
					swal({title:"Aviso!",text:erro,type:"warning",confirmButtonColor:"#1ab394"});
    			}
    		},

    		error: function(xhr, textStatus, errorThrown){

    		}
    	});
    }

    function setarPermissaoMembro()
    {
    	var seq_cadast	= $("#seq_cadast").val();

    	$.ajax({
    		type:"post",
    		url: "js/ajax/setarPermissaoMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
    		dataType: "json",
    		data: {seq_cadast: seq_cadast}, // exemplo: {codigo:125, nome:"Joaozinho"}
    		success: function(data){

    		},

    		error: function(xhr, textStatus, errorThrown){
				swal({title:"Aviso!",text:"erro ao setar permissao",type:"warning",confirmButtonColor:"#1ab394"});
    		}
    	});
    }

    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

	$(document).ready(function() {


	});

	function startProgress(){
		//$("#mySeeLoading").modal('show');
	}

	function stopProgress(){
		$('#mySeeLoading').modal('toggle');
	}

	$(document).ready(function() {

		$('.footable').footable();
		$('.footable2').footable();

	});




	/**
	 * Função responsável por retornar dados dos membros via ORCZ
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 * @param Int #codAfiliacao+campo : Código de Afiliação do membro a ser consultado
	 * @param Int #nome+campo : Parte do nome do membro a ser consultado
	 * @param Int idOrganismo : ID do Organismo Afiliado de quem está operando
	 * Retorna dados completos do membro, por meio de webservice.
	 */
	function getOficialIniciaticoResponsavel(campo,oficial) {

		var codigoAfiliacao = $("#codAfiliacao"+campo).val();
		var nomeMembro 		= $("#nome"+campo).val();
		var tipoMembro 		= '1';
		var idOrganismo 	= $("#fk_idOrganismoAfiliado").val();

		$.ajax({
			type:"post",
			url: "js/ajax/retornaDadosMembro.php",
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro},
			success: function(data){
				//console.log(data);

				if(data.result[0].fields.fSeqCadast != 0){

					$("#"+oficial).val(data.result[0].fields.fSeqCadast);

					$("#nome"+campo).val(data.result[0].fields.fNomCliente);

					$("#h_seqCadast"+campo).val(data.result[0].fields.fSeqCadast);
					$("#h_nome"+campo).val(data.result[0].fields.fNomCliente);
					if(data.result[0].fields.fSeqCadastCompanheiroRosacruz != 0) {
						$("#h_seqCadastCompanheiro" + campo).val(data.result[0].fields.fSeqCadastCompanheiroRosacruz);
						$("#h_nomeCompanheiro" + campo).val(data.result[0].fields.fNomConjuge);
					} else {
						$("#h_seqCadastCompanheiro"+campo).val("");
						$("#h_nomeCompanheiro"+campo).val("");
					}

				} else {

					swal({title:"Aviso!",text:"Membro não foi encontrado.",type:"warning",confirmButtonColor:"#1ab394"});

					if($('#companheiro'+campo).is(":checked")) {
						$('#companheiro'+campo).prop("checked", false);
					}

					$("#"+oficial).val("");
					$("#codAfiliacao"+campo).val("");
					$("#nome"+campo).val("");

					$("#h_seqCadast"+campo).val("");
					$("#h_nome"+campo).val("");
					$("#h_seqCadastCompanheiro"+campo).val("");
					$("#h_nomeCompanheiro"+campo).val("");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
			}
		});

	}

	/**
	 * Função responsável por fazer a troca do nome do membro pelo nome do companheiro quando selecionado checkbox indicando o companheiro
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 */
	function getCompanheiroOficialIniciatico(campo,oficial) {

		if($('#companheiro'+campo).is(":checked")) {
			if($("#h_nomeCompanheiro"+campo).val()=="") {
				swal({title:"Aviso!",text:"Este membro não possui companheiro",type:"warning",confirmButtonColor:"#1ab394"});
				$('#companheiro'+campo).prop("checked", false);
			} else {
				$("#nome"+campo).val($("#h_nomeCompanheiro"+campo).val());
				$("#"+oficial).val($("#h_seqCadastCompanheiro"+campo).val());
			}
		} else {
			$("#nome"+campo).val($("#h_nome"+campo).val());
			$("#"+oficial).val($("#h_seqCadast"+campo).val());
		}

	}

	/**
	 * Função responsável por retornar dados dos membros via ORCZ
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 * @param Int #codAfiliacao+campo : Código de Afiliação do membro a ser consultado
	 * @param Int #nome+campo : Parte do nome do membro a ser consultado
	 * @param Int idOrganismo : ID do Organismo Afiliado de quem está operando
	 * Retorna dados completos do membro, por meio de webservice.
	 */
	function getOficialIniciaticoResponsavelAlteracao(campo,oficial,idEquipe) {

		var codigoAfiliacao = $("#codAfiliacao"+campo+idEquipe).val();
		var nomeMembro 		= $("#nome"+campo+idEquipe).val();
		var tipoMembro 		= '1';

		console.log("codigoAfiliacao: " + codigoAfiliacao + " | " + campo + " | " + oficial + " | " + idEquipe);

		if(codigoAfiliacao != ""){
			$.ajax({
				type:"post",
				url: "js/ajax/retornaDadosMembro.php",
				dataType: "json",
				data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro},
				success: function(data){

					if(data.result[0].fields.fSeqCadast != 0){

						$("#" + oficial + idEquipe).val(data.result[0].fields.fSeqCadast);

						$("#nome" + campo + idEquipe).val(data.result[0].fields.fNomCliente);

						$("#h_seqCadast"+campo + idEquipe).val(data.result[0].fields.fSeqCadast);
						$("#h_nome"+campo + idEquipe).val(data.result[0].fields.fNomCliente);
						if(data.result[0].fields.fSeqCadastCompanheiroRosacruz != 0) {
							$("#h_seqCadastCompanheiro" + campo + idEquipe).val(data.result[0].fields.fSeqCadastCompanheiroRosacruz);
							$("#h_nomeCompanheiro" + campo + idEquipe).val(data.result[0].fields.fNomConjuge);
						} else {
							$("#h_seqCadastCompanheiro"+campo + idEquipe).val("");
							$("#h_nomeCompanheiro"+campo + idEquipe).val("");
						}

					} else {

						//swal({title:"Aviso!",text:"Membro não está registrado na relação deste Organismo",type:"warning",confirmButtonColor:"#1ab394"});

						if($('#companheiro'+campo + idEquipe).is(":checked")) {
							$('#companheiro'+campo + idEquipe).prop("checked", false);
						}

						$("#"+oficial + idEquipe).val("");
						$("#"+oficial + idEquipe).val("");
						$("#codAfiliacao"+campo + idEquipe).val("");
						$("#nome"+campo + idEquipe).val("");

						$("#h_seqCadast"+campo + idEquipe).val("");
						$("#h_nome"+campo + idEquipe).val("");
						$("#h_seqCadastCompanheiro"+campo + idEquipe).val("");
						$("#h_nomeCompanheiro"+campo + idEquipe).val("");
					}

				},
				error: function(xhr, textStatus, errorThrown){
					swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
				}
			});
		} else {
			console.log("else");

			if($('#companheiro'+campo + idEquipe).is(":checked")) {
				$('#companheiro'+campo + idEquipe).prop("checked", false);
			}

			$("#"+oficial + idEquipe).val("");

			$("#codAfiliacao"+campo + idEquipe).val("");
			$("#nome"+campo + idEquipe).val("");

			$("#h_seqCadast"+campo + idEquipe).val("");
			$("#h_nome"+campo + idEquipe).val("");
			$("#h_seqCadastCompanheiro"+campo + idEquipe).val("");
			$("#h_nomeCompanheiro"+campo + idEquipe).val("");
		}
	}

	/**
	 * Função responsável por fazer a troca do nome do membro pelo nome do companheiro quando selecionado checkbox indicando o companheiro
	 * @param String campo : Nome do campo para identificar qual tipo de oficial/campo será retornado
	 */
	function getCompanheiroOficialIniciaticoAlteracao(campo,oficial,idEquipe) {

		if($('#companheiro'+campo + idEquipe).is(":checked")) {
			if($("#h_nomeCompanheiro"+campo + idEquipe).val()=="") {
				swal({title:"Aviso!",text:"Este membro não possui companheiro",type:"warning",confirmButtonColor:"#1ab394"});
				$('#companheiro'+campo + idEquipe).prop("checked", false);
			} else {
				$("#nome"+campo + idEquipe).val($("#h_nomeCompanheiro"+campo + idEquipe).val());
				$("#"+oficial + idEquipe).val($("#h_seqCadastCompanheiro"+campo + idEquipe).val());
			}
		} else {
			$("#nome"+campo + idEquipe).val($("#h_nome"+campo + idEquipe).val());
			$("#"+oficial + idEquipe).val($("#h_seqCadast"+campo + idEquipe).val());
		}

	}

	function modificaColumbaRecepcao(codAfiliacaoColumba, nomeColumba, codAfiliacaoRecepcao, nomeRecepcao, idEquipe, idAtividadeIniciatica){

        $("#atividadeIniciatica" + idEquipe).val(idAtividadeIniciatica);

		if(codAfiliacaoColumba != undefined){
			$("#columba" + idEquipe).html("["+codAfiliacaoColumba+"] "+nomeColumba);
            $("#codAfiliacaoColumba" + idEquipe).val(codAfiliacaoColumba);
            $("#nomeColumba" + idEquipe).val(nomeColumba);

		} else {
			$("#columba" + idEquipe).html("Não definido!");
            $("#codAfiliacaoColumba" + idEquipe).val("");
            $("#nomeColumba" + idEquipe).val("");
		}
		if(codAfiliacaoRecepcao != undefined){
			$("#recepcao" + idEquipe).html("["+codAfiliacaoRecepcao+"] "+nomeRecepcao);
            $("#codAfiliacaoRecepcao" + idEquipe).val(codAfiliacaoRecepcao);
            $("#nomeRecepcao" + idEquipe).val(nomeRecepcao);
		} else {
			$("#recepcao" + idEquipe).html("Erro!");
            $("#codAfiliacaoRecepcao" + idEquipe).val("");
            $("#nomeRecepcao" + idEquipe).val("");
		}

	}

	function inativaOficialResponsavel(idAtividadeEstatuto, idOficialResponsavel){

		$.ajax({
			type:"post",
			url: "js/ajax/inativaOficialResponsavelAtividadeEstatuto.php",
			dataType: "json",
			data: {idAtividadeEstatuto:idAtividadeEstatuto, idOficialResponsavel:idOficialResponsavel},
			success: function(retorno){
				if (retorno.erro != undefined && retorno.erro.length > 0) {
                    /*
					var erro = 'Ocorreram um ou mais erros:\n\n';
					for (var i =0; i < retorno.erro.length; i++) {
						erro += retorno.erro[i] + '\n';
					}
					*/
					alert(retorno.erro);
				}
				else if (retorno.sucesso != undefined) {
					//alert(retorno.sucesso);
					statusAtual = 1;
					alteraStatusOficialResponsavel(idAtividadeEstatuto, idOficialResponsavel, statusAtual);
				}
				else {
					alert("Ocorreu algum erro. Entre em contato com o setor de TI.");
				}
			},
			error: function(xhr, textStatus, errorThrown){

			}
		});
	}

	function ativaOficialResponsavel(idAtividadeEstatuto, idOficialResponsavel){

		$.ajax({
			type:"post",
			url: "js/ajax/ativaOficialResponsavelAtividadeEstatuto.php",
			dataType: "json",
			data: {idAtividadeEstatuto:idAtividadeEstatuto, idOficialResponsavel:idOficialResponsavel},
			success: function(retorno){
				if (retorno.erro != undefined && retorno.erro.length > 0) {
                    /*
					var erro = 'Ocorreram um ou mais erros:\n\n';
					for (var i =0; i < retorno.erro.length; i++) {
						erro += retorno.erro[i] + '\n';
					}
					*/
					alert(retorno.erro);
				}
				else if (retorno.sucesso != undefined) {
					//alert(retorno.sucesso);
					statusAtual = 0;
					alteraStatusOficialResponsavel(idAtividadeEstatuto, idOficialResponsavel, statusAtual);
				}
				else {
					alert("Ocorreu algum erro. Entre em contato com o setor de TI.");
				}
			},
			error: function(xhr, textStatus, errorThrown){

			}
		});
	}

	function alteraStatusOficialResponsavel(idAtividadeEstatuto, idOficialResponsavel, statusAtual){

		switch(statusAtual){
			case 0:
				$("#statusTarget" + idAtividadeEstatuto + idOficialResponsavel).html("");
				$("#statusTarget" + idAtividadeEstatuto + idOficialResponsavel).append("<span class='badge badge-primary'>Ativo</span>");
				$("#botaoOficialResponsavel" + idAtividadeEstatuto + idOficialResponsavel).html("");
				$("#botaoOficialResponsavel" + idAtividadeEstatuto + idOficialResponsavel).append("<a class='btn btn-xs btn-danger' onclick='inativaOficialResponsavel(" + idAtividadeEstatuto + ", " + idOficialResponsavel + ")' data-rel='tooltip' title='Inativar'> Inativar </a>");

                var qntOficiaisResponsaveis     = $("#qntOficiaisResponsaveis" + idAtividadeEstatuto).val();
                qntOficiaisResponsaveis = parseInt(qntOficiaisResponsaveis) + 1;
                $("#qntOficiaisResponsaveis" + idAtividadeEstatuto).val(qntOficiaisResponsaveis);

				break;
			case 1:
				$("#statusTarget" + idAtividadeEstatuto + idOficialResponsavel).html("");
				$("#statusTarget" + idAtividadeEstatuto + idOficialResponsavel).append("<span class='badge badge-danger'>Inativo</span>");
				$("#botaoOficialResponsavel" + idAtividadeEstatuto + idOficialResponsavel).html("");
				$("#botaoOficialResponsavel" + idAtividadeEstatuto + idOficialResponsavel).append("<a class='btn btn-xs btn-primary' onclick='ativaOficialResponsavel(" + idAtividadeEstatuto + ", " + idOficialResponsavel + ")' data-rel='tooltip' title='Ativar'> Ativar </a>");

                var qntOficiaisResponsaveis     = $("#qntOficiaisResponsaveis" + idAtividadeEstatuto).val();
                qntOficiaisResponsaveis = parseInt(qntOficiaisResponsaveis) - 1;
                $("#qntOficiaisResponsaveis" + idAtividadeEstatuto).val(qntOficiaisResponsaveis);

                break;
			default:
		}
	}

	function retornaDadosMembroOficialECompanheiroAtividadeEstatuto(idEstatuto)
	{
		var codigoAfiliacao = $("#codAfiliacaoMembroOficial"+idEstatuto).val();
		var nomeMembro = $("#nomeMembroOficial"+idEstatuto).val();
		var tipoMembro = '1';

		//console.log("retornaDados() --- Cód. Afiliação: "+codigoAfiliacao+" Nome: "+nomeMembro+" Tipo: "+tipoMembro);
		//alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);

		$.ajax({
			type: "post",
			url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}
			success: function (data) {
				if (data.result[0].fields.fNomCliente != "")
				{
					//alert(data.result[0].fields.fDesMensag);
					$("#h_seqCadastMembroOficial"+idEstatuto).val(data.result[0].fields.fSeqCadast);
					$("#nomeMembroOficial"+idEstatuto).val(data.result[0].fields.fNomCliente);
					$("#h_nomeMembroOficial"+idEstatuto).val(data.result[0].fields.fNomCliente);
					$("#h_seqCadastCompanheiroMembroOficial"+idEstatuto).val(data.result[0].fields.fSeqCadastCompanheiroRosacruz);
					$("#h_nomeCompanheiroMembroOficial"+idEstatuto).val(data.result[0].fields.fNomConjuge);
					//alert("princ:"+data.result[0].fields.fSeqCadast+"-"+data.result[0].fields.fSeqCadastPrincipalRosacruz);
					if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalRosacruz)
					{
						$("#principalCompanheiro"+idEstatuto).val(1);
					}
					if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroRosacruz)
					{
						$("#principalCompanheiro"+idEstatuto).val(2);
					}
				} else {
					swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
				}

			},
			error: function (xhr, textStatus, errorThrown) {
				alert('erro');
			}
		});
	}

	function incluiOficialResponsavelAtividade(idAtividadeEstatuto){

		var codigoAfiliacao = $("#codAfiliacaoMembroOficial"+idAtividadeEstatuto).val();
		var nomeMembro = $("#nomeMembroOficial"+idAtividadeEstatuto).val();
		var seqCadast = $("#h_seqCadastMembroOficial"+idAtividadeEstatuto).val();

        if((codigoAfiliacao != "") && (nomeMembro != "") && (seqCadast != "")) {
            $.ajax({
                type: "post",
                url: "js/ajax/incluiOficialResponsavelAtividadeEstatuto.php",
                dataType: "json",
                data: {
                    idAtividadeEstatuto: idAtividadeEstatuto,
                    codigoAfiliacao: codigoAfiliacao,
                    nomeMembro: nomeMembro,
                    seqCadast: seqCadast
                },
                success: function (retorno) {
                    if (retorno.erro != undefined && retorno.erro.length > 0) {

                        swal({
                            title: "Ação Indevida!",
                            text: retorno.erro,
                            type: "error",
                            confirmButtonColor: "#1ab394"
                        });
                        $("#h_seqCadastMembroOficial" + idAtividadeEstatuto).val("");
                        $("#codAfiliacaoMembroOficial" + idAtividadeEstatuto).val("");
                        $("#nomeMembroOficial" + idAtividadeEstatuto).val("");
                        $("#h_nomeMembroOficial" + idAtividadeEstatuto).val("");
                        $("#h_seqCadastCompanheiroMembroOficial" + idAtividadeEstatuto).val("");
                        $("#h_nomeCompanheiroMembroOficial" + idAtividadeEstatuto).val("");
                    }
                    else if (retorno.sucesso != undefined) {
                        //alert(retorno.sucesso);
                        $("#h_seqCadastMembroOficial" + idAtividadeEstatuto).val("");
                        $("#codAfiliacaoMembroOficial" + idAtividadeEstatuto).val("");
                        $("#nomeMembroOficial" + idAtividadeEstatuto).val("");
                        $("#h_nomeMembroOficial" + idAtividadeEstatuto).val("");
                        $("#h_seqCadastCompanheiroMembroOficial" + idAtividadeEstatuto).val("");
                        $("#h_nomeCompanheiroMembroOficial" + idAtividadeEstatuto).val("");

                        atualizaTabelaOficiaisResponsaveis(idAtividadeEstatuto);
                    }
                    else {
                        swal({title: "Aviso!", text: "Ocorreu algum erro! Tente novamente mais tarde.", type: "warning", confirmButtonColor: "#1ab394"});
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    swal({title: "Aviso!", text: "Ocorreu algum erro! Tente novamente mais tarde.", type: "danger", confirmButtonColor: "#1ab394"});
                }
            });
        } else {
            swal({title: "Aviso!", text: "Você deve preencher dados do membro a ser inserido!", type: "warning", confirmButtonColor: "#1ab394"});
        }
	}

	function atualizaTabelaOficiaisResponsaveis(idAtividadeEstatuto){

		$.ajax({
			type:"post",
			url: "js/ajax/getListaOficiaisResponsaveisAtividadeEstatuto.php",
			dataType: "json",
			data: {idAtividadeEstatuto:idAtividadeEstatuto},
			success: function(data){
				//console.log(data);
				if(data.qtdOficiais!=0) {
					$("#bodyTabelaOficiaisResponsaveis" + idAtividadeEstatuto).html("");
					for(i=0; i < data.qtdOficiais; i++) {
						//console.log(data.oficiais.rows[i]);
						$("#bodyTabelaOficiaisResponsaveis" + idAtividadeEstatuto).append(data.oficiais.rows[i]);
					}
                    $("#bodyTabelaOficiaisResponsaveis" + idAtividadeEstatuto).append('<input type="hidden" value="' + data.qtdOficiaisAtivos + '" id="qntOficiaisResponsaveis' + idAtividadeEstatuto + '" name="qntOficiaisResponsaveis' + idAtividadeEstatuto + '" >');
				} else {
					$("#bodyTabelaOficiaisResponsaveis" + idAtividadeEstatuto).html("");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				//console.log('error');
				$("#bodyTabelaOficiaisResponsaveis" + idAtividadeEstatuto).html("");
				$("#bodyTabelaOficiaisResponsaveis").append('');
			}
		});

	}

	function retornaDadosMembroColumba(codigoAfiliacao, nome)
	{
		var codigoAfiliacao = $("#" + codigoAfiliacao).val();
		var nome = $("#" + nome).val();
		var tipoMembro = '2';

		$.ajax({
			type: "post",
			url: "js/ajax/retornaDadosMembro.php",
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nome, tipoMembro: tipoMembro},
			success: function (data) {
				console.log(data);7
				if (data.result[0].fields.fNomCliente != "") {
					anoNascimento = data.result[0].fields.fDatNascimento.substring(0, 4);
					date = new Date();
					if (data.result[0].fields.fIdeSexo == "F") {
						if (((date.getFullYear() - anoNascimento) <= 14) || ((date.getFullYear() - anoNascimento) >= 8)) {
							if (data.result[0].fields.fIdeTipoSituacMembroOjp != 7) {
								$("#nomeAtividadeIniciaticaColumba").val(data.result[0].fields.fNomCliente);
								$("#h_seqCadastColumba").val(data.result[0].fields.fSeqCadast);
								$("#h_nomeColumba").val(data.result[0].fields.fNomCliente);
							} else {
								swal({title: "Aviso!", text: "Membro está desligado da OGG!", type: "warning", confirmButtonColor: "#1ab394"});
								$("#codAfiliacaoAtividadeIniciaticaColumba").val("");
								$("#nomeAtividadeIniciaticaColumba").val("");
								$("#h_seqCadastColumba").val("");
								$("#h_nomeColumba").val("");
							}
						} else {
							swal({title: "Aviso!", text: "Membro não corresponde à idade para ser Columba!", type: "warning", confirmButtonColor: "#1ab394"});
							$("#codAfiliacaoAtividadeIniciaticaColumba").val("");
							$("#nomeAtividadeIniciaticaColumba").val("");
							$("#h_seqCadastColumba").val("");
							$("#h_nomeColumba").val("");
						}
					} else {
						swal({title: "Aviso!", text: "Membro deve ser do sexo Feminino!", type: "warning", confirmButtonColor: "#1ab394"});
						$("#codAfiliacaoAtividadeIniciaticaColumba").val("");
						$("#nomeAtividadeIniciaticaColumba").val("");
						$("#h_seqCadastColumba").val("");
						$("#h_nomeColumba").val("");
					}
				} else {
					swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
					$("#codAfiliacaoAtividadeIniciaticaColumba").val("");
					$("#nomeAtividadeIniciaticaColumba").val("");
					$("#h_seqCadastColumba").val("");
					$("#h_nomeColumba").val("");
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
				$("#" + codigoAfiliacao).val("");
				$("#" + nome).val("");
				$("#h_seqCadastColumba").val("");
				$("#h_nomeColumba").val("");
			}
		});
	}

	$(document).ready(function() {

		//console.log($('#avaliacaoImovel').val());

		$('#responsavelPlenoAtividadeIniciaticaColumba').bind('change', function(){
			if($(this).val()==0){
				$("#seqCadastResponsavelPlenoAtividadeIniciaticaColumba").val("");
			} else if($(this).val()==1) {
				var codigoAfiliacao = $("#codAfiliacaoTutorAtividadeIniciaticaColumba").val();
				var nomeCompleto = $("#nomeTutorAtividadeIniciaticaColumba").val();
				var nome = nomeCompleto.split(' ');
				if((codigoAfiliacao != "") && (nome[0] != "")) {
					retornaDadosMembroResponsavelPlenoColumba(codigoAfiliacao, nome[0]);
				} else {
					swal({title: "Aviso!", text: "Insira o código de afiliação e o nome do responsável indicado!", type: "warning", confirmButtonColor: "#1ab394"});
				}
			} else if($(this).val()==2) {
				var codigoAfiliacao = $("#codAfiliacaoTutoraAtividadeIniciaticaColumba").val();
				var nomeCompleto = $("#nomeTutoraAtividadeIniciaticaColumba").val();
				var nome = nomeCompleto.split(' ');
				if((codigoAfiliacao != "") && (nome[0] != "")) {
					retornaDadosMembroResponsavelPlenoColumba(codigoAfiliacao, nome[0]);
				} else {
					swal({title: "Aviso!", text: "Insira o código de afiliação e o nome do responsável indicado!", type: "warning", confirmButtonColor: "#1ab394"});
				}
			}
		});

		if($('#responsavelPlenoAtividadeIniciaticaColumba').val()==0){
			$("#seqCadastResponsavelPlenoAtividadeIniciaticaColumba").val("");
		}else if($('#responsavelPlenoAtividadeIniciaticaColumba').val()==1){
			var codigoAfiliacao = $("#codAfiliacaoTutorAtividadeIniciaticaColumba").val();
			var nomeCompleto = $("#nomeTutorAtividadeIniciaticaColumba").val();
			var nome = nomeCompleto.split(' ');
			if((codigoAfiliacao != "") && (nome[0] != "")) {
				retornaDadosMembroResponsavelPlenoColumba(codigoAfiliacao, nome[0]);
			} else {
				swal({title: "Aviso!", text: "Insira o código de afiliação e o nome do responsável indicado!", type: "warning", confirmButtonColor: "#1ab394"});
			}
		}else if($('#responsavelPlenoAtividadeIniciaticaColumba').val()==2){
			var codigoAfiliacao = $("#codAfiliacaoTutoraAtividadeIniciaticaColumba").val();
			var nomeCompleto = $("#nomeTutoraAtividadeIniciaticaColumba").val();
			var nome = nomeCompleto.split(' ');
			if((codigoAfiliacao != "") && (nome[0] != "")) {
				retornaDadosMembroResponsavelPlenoColumba(codigoAfiliacao, nome[0]);
			} else {
				swal({title: "Aviso!", text: "Insira o código de afiliação e o nome do responsável indicado!", type: "warning", confirmButtonColor: "#1ab394"});
			}
		}
	});

	function retornaDadosMembroResponsavelPlenoColumba(codigoAfiliacao, nome){

		var tipoMembro = '1';

		$.ajax({
			type: "post",
			url: "js/ajax/retornaDadosMembro.php",
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nome, tipoMembro: tipoMembro},
			success: function (data) {
				//alert(data.result[0].fields.fDesMensag);
				if (data.result[0].fields.fNomCliente != "") {
					swal({title: "Confirmado!", text: "Membro encontrado!", type: "success", confirmButtonColor: "#1ab394"});
					$("#seqCadastResponsavelPlenoAtividadeIniciaticaColumba").val(data.result[0].fields.fSeqCadast);
				} else {
					swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
					$("#seqCadastResponsavelPlenoAtividadeIniciaticaColumba").val("");
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
				$("#seqCadastResponsavelPlenoAtividadeIniciaticaColumba").val("");
			}
		});
	}


	/**
	 * ----------------------------------------------------------------------------------------------------------------------------------------------------------
	 * -------------------------              início: ATIVIDADE INICIÁTICA ÁGIL                ------------------------------------------------------------------
	 * ----------------------------------------------------------------------------------------------------------------------------------------------------------
	 */

	function cadastraAtividadeIniciaticaAgil(){

		var fk_idOrganismoAfiliado					= $("#fk_idOrganismoAfiliado").val();
		var fk_idAtividadeIniciaticaOficial			= 0;
		var fk_idAtividadeIniciaticaColumba			= 0;
		var codAfiliacaoRecepcao					= 0;
		var nomeRecepcao							= "";
		var recepcaoAtividadeIniciaticaOficial		= 0;
		var tipoAtividadeIniciatica					= $("#tipoAtividadeIniciatica").val();
		var dataRealizadaAtividadeIniciatica		= $("#dataRealizadaAtividadeIniciatica").val();
		var horaRealizadaAtividadeIniciatica		= $("#horaRealizadaAtividadeIniciatica").val();
		var localAtividadeIniciatica				= $("#localAtividadeIniciatica").val();
		var anotacoesAtividadeIniciatica			= $("#anotacoesAtividadeIniciatica.summernote").code();
		var fk_seqCadastAtualizadoPor				= $("#fk_seqCadastAtualizadoPor").val();
		var loginAtualizadoPor						= $("#loginAtualizadoPor").val();
                var seqAtualizadoPor						= $("#seqAtualizadoPor").val();
                //var siglaOA = retornaSiglaOrganismoAfiliado(fk_idOrganismoAfiliado);

		dataRealizada           = dataRealizadaAtividadeIniciatica.substr(6,4)+"-"+dataRealizadaAtividadeIniciatica.substr(3,2)+"-"+dataRealizadaAtividadeIniciatica.substr(0,2);

		var seqCadastMembro 				= new Array();
		var textoMembro 					= new Array();
		var companheiroMembro 				= new Array();
		var listaMembros = document.getElementById("membros_atividade_iniciatica");

		if(listaMembros.length > 0){

			for (var i = 0; i < listaMembros.length; i++) {
				seqCadastMembro[i] 				= listaMembros.options[i].value;
				textoMembro[i] 					= listaMembros.options[i].text;
				companheiroMembro[i] 			= listaMembros.options[i].getAttribute('name');
			}
			var camposGeraisSeqCadast 				= JSON.stringify(seqCadastMembro);
			var camposGeraisTexto 					= JSON.stringify(textoMembro);
			var camposGeraisCompanheiro 			= JSON.stringify(companheiroMembro);

			$.ajax({
				type: "post",
				url: "js/ajax/cadastraAtividadeIniciaticaAgil.php",
				dataType: "json",
				data: {
					fk_idOrganismoAfiliado: fk_idOrganismoAfiliado,
					fk_idAtividadeIniciaticaOficial: fk_idAtividadeIniciaticaOficial,
					fk_idAtividadeIniciaticaColumba: fk_idAtividadeIniciaticaColumba,
					codAfiliacaoRecepcao: codAfiliacaoRecepcao,
					nomeRecepcao: nomeRecepcao,
					recepcaoAtividadeIniciaticaOficial: recepcaoAtividadeIniciaticaOficial,
					tipoAtividadeIniciatica: tipoAtividadeIniciatica,
					dataRealizadaAtividadeIniciatica: dataRealizadaAtividadeIniciatica,
					horaRealizadaAtividadeIniciatica: horaRealizadaAtividadeIniciatica,
					localAtividadeIniciatica: localAtividadeIniciatica,
					anotacoesAtividadeIniciatica: anotacoesAtividadeIniciatica,
					fk_seqCadastAtualizadoPor: fk_seqCadastAtualizadoPor,
					loginAtualizadoPor: loginAtualizadoPor,
			 		camposGeraisSeqCadast: camposGeraisSeqCadast,
			 		camposGeraisTexto: camposGeraisTexto,
					camposGeraisCompanheiro: camposGeraisCompanheiro
				},
				success: function (data) {
					if(data.sucesso === true){
                        var pote = 0;
                        var strMembros="";
						for(var i= 0; i < listaMembros.length; i++) {
                            if(i==0)
							{
                                strMembros += listaMembros[i]+"/"+data.idAtividade;
							}else{
                                strMembros += "@@"+listaMembros[i]+"/"+data.idAtividade;
							}

                        }
                        //Debug para ver se está passando os seqs por parametro certo
                        alert("strMembros=>"+strMembros);

						var codigoAfiliacao    = 0; //retornaCodAfiliacaoExtraido(textoMembro[i]);
						var tipoMembro         = 1;
						var tipoObrigacao      = retornaIdeTipoObrigacaoORCZ(tipoAtividadeIniciatica);
						//horaRealizadaAtividadeIniciatica = '00:00';
						var dataObrigacao      = dataRealizada + " " + horaRealizadaAtividadeIniciatica + ":00"; //"2011-06-08T17:33:54"
						var siglaOA            = document.getElementById("siglaOA").getAttribute("value"); //retornaSiglaOA(fk_idOrganismoAfiliado);
						var descricaoLocal     = localAtividadeIniciatica;
						var companheiro        = ""; //companheiroMembro[i];
						var idAtividadeIniciatica = data.idAtividade;

                        atualizarObrigacaoRitualisticaMembros(loginAtualizadoPor, strMembros, 0, 1, tipoObrigacao, dataObrigacao, siglaOA, descricaoLocal, "", seqAtualizadoPor, idAtividadeIniciatica,fk_idOrganismoAfiliado);

                        //do { console.log("Total que passou pelo método: " + pote); } while (pote < listaMembros.length);
						swal({
                            title: "Confirmado!",
                            text: "Atividade Iniciática Cadastrada com sucesso!",
                            type: "success",
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "OK",
                            closeOnConfirm: false },
						 function (isConfirm) { if (isConfirm) {
						 	location.href='?corpo=buscaAtividadeIniciaticaAgil';
						}});
					}
				},
				error: function (xhr, textStatus, errorThrown) {
					swal({ title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394", confirmButtonText: "OK", closeOnConfirm: false },
					function (isConfirm) { if (isConfirm) { location.href='?corpo=buscaAtividadeIniciaticaAgil'; }});
				}
			});
		} else {
			swal({title: "Aviso!", text: "Insira membros na Atividade Iniciática!", type: "warning", confirmButtonColor: "#1ab394"});
		}
	}


    /** Novas implementações */

    function cadastraMultiplasAtividadesIniciaticasAgeis(){

        var fk_idOrganismoAfiliado					= $("#fk_idOrganismoAfiliado").val();
        var fk_idAtividadeIniciaticaOficial			= 0;
        var fk_idAtividadeIniciaticaColumba			= 0;
        var codAfiliacaoRecepcao					= 0;
        var nomeRecepcao							= "";
        var recepcaoAtividadeIniciaticaOficial		= 0;
        var tipoAtividadeIniciatica					= $("#tipoAtividadeIniciatica").val();
        var dataRealizadaAtividadeIniciatica		= $("#dataRealizadaAtividadeIniciatica").val();
        var horaRealizadaAtividadeIniciatica		= $("#horaRealizadaAtividadeIniciatica").val();
        var localAtividadeIniciatica				= $("#localAtividadeIniciatica").val();
        var anotacoesAtividadeIniciatica			= $("#anotacoesAtividadeIniciatica.summernote").code();
        var fk_seqCadastAtualizadoPor				= $("#fk_seqCadastAtualizadoPor").val();
        var loginAtualizadoPor						= $("#loginAtualizadoPor").val();

        retornaSiglaOrganismoAfiliado(fk_idOrganismoAfiliado);

        dataRealizada           =   dataRealizadaAtividadeIniciatica.substr(6,4) + "-" +
                                    dataRealizadaAtividadeIniciatica.substr(3,2) + "-" +
                                    dataRealizadaAtividadeIniciatica.substr(0,2);

        var seqCadastMembro 				= new Array();
        var textoMembro 					= new Array();

        var listaMembros = document.getElementById("membros_atividade_iniciatica");

        if(listaMembros.length > 0){

            for (var i = 0; i < listaMembros.length; i++) {
                seqCadastMembro[i] 				= listaMembros.options[i].value;
                textoMembro[i] 					= listaMembros.options[i].text;
            }
            var camposGeraisSeqCadast 				= JSON.stringify(seqCadastMembro);
            var camposGeraisTexto 					= JSON.stringify(textoMembro);

            $.ajax({
                type: "post",
                url: "js/ajax/cadastraAtividadeIniciaticaAgil.php",
                dataType: "json",
                data: {
                    fk_idOrganismoAfiliado: fk_idOrganismoAfiliado,
                    fk_idAtividadeIniciaticaOficial: fk_idAtividadeIniciaticaOficial,
                    fk_idAtividadeIniciaticaColumba: fk_idAtividadeIniciaticaColumba,
                    codAfiliacaoRecepcao: codAfiliacaoRecepcao,
                    nomeRecepcao: nomeRecepcao,
                    recepcaoAtividadeIniciaticaOficial: recepcaoAtividadeIniciaticaOficial,
                    tipoAtividadeIniciatica: tipoAtividadeIniciatica,
                    dataRealizadaAtividadeIniciatica: dataRealizadaAtividadeIniciatica,
                    horaRealizadaAtividadeIniciatica: horaRealizadaAtividadeIniciatica,
                    localAtividadeIniciatica: localAtividadeIniciatica,
                    anotacoesAtividadeIniciatica: anotacoesAtividadeIniciatica,
                    fk_seqCadastAtualizadoPor: fk_seqCadastAtualizadoPor,
                    loginAtualizadoPor: loginAtualizadoPor,
                    camposGeraisSeqCadast: camposGeraisSeqCadast,
                    camposGeraisTexto: camposGeraisTexto
                },
                success: function (data) {
                    if(data.sucesso === true){
                        var codigoAfiliacao    = 0;
                        var tipoMembro         = 1;
                        var tipoObrigacao      = retornaIdeTipoObrigacaoORCZ(tipoAtividadeIniciatica);
                        var dataObrigacao      = dataRealizada + "T" + horaRealizadaAtividadeIniciatica + ":00";
                        var siglaOA            = document.getElementById("siglaOA").getAttribute("value");
                        var descricaoLocal     = localAtividadeIniciatica;
                        var companheiro        = "";

                        atualizarMultiplasObrigacoesRitualisticasMembros(loginAtualizadoPor,
                            seqCadastMembro,
                            codigoAfiliacao,
                            tipoMembro,
                            tipoObrigacao,
                            dataObrigacao,
                            siglaOA,
                            descricaoLocal,
                            companheiro,
                            data.idAtividade);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                }
            });
        } else {
            swal({title: "Aviso!", text: "Insira membros na Atividade Iniciática!", type: "warning", confirmButtonColor: "#1ab394"});
        }
    }
    function atualizarMultiplasObrigacoesRitualisticasMembros(loginAtualizadoPor, seqCadastMembro, codigoAfiliacao, tipoMembro, tipoObrigacao, dataObrigacao, siglaOA, descricaoLocal, companheiro, idAtividade){

        //console.log("Quantidade de Membros: " + seqCadastMembro.length);
		var strMembros="";
        for(var i= 0; i < seqCadastMembro.length; i++) {
        	if(i==0){
        		strMembros += seqCadastMembro[i];
        	}else{
                strMembros += "@@"+seqCadastMembro[i];
			}
        }
		//alert("strMembros=>"+strMembros);
        $.ajax({
            type: "post",
            url: "js/ajax/atualizarMultiplasObrigacoesRitualisticasMembrosInovad.php",
            dataType: "json",
            data: {
                loginAtualizadoPor: loginAtualizadoPor,
                membros: strMembros,
                codigoAfiliacao: codigoAfiliacao,
                tipoMembro: tipoMembro,
                tipoObrigacao: tipoObrigacao,
                dataObrigacao: dataObrigacao,
                siglaOA: siglaOA,
                descricaoLocal: descricaoLocal,
                companheiro: companheiro
            },
            success: function (data) {
            	//alert(data);

            	if(data.status==1)
				{
                    swal({
                            title: "Confirmado!",
                            text: "Atividade Iniciática Cadastrada com sucesso!",
                            type: "success",
                            confirmButtonColor: "#1ab394",
                            confirmButtonText: "OK",
                            closeOnConfirm: false },
                        function (isConfirm) { if (isConfirm) {
                            location.href='?corpo=buscaAtividadeIniciaticaAgil';
                        }});
				}else{
            		alert('Erro no processo de cadastro agil');
				}


            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividade);
            }
        });
    }

    /** Novas implementações */


    function deletaAtividadeIniciaticaAgil(idAtividadeIniciatica){
        $.ajax({
            type: "post",
            url: "js/ajax/deletaAtividadeIniciaticaAgil.php",
            dataType: "json",
            data: {idAtividadeIniciatica: idAtividadeIniciatica},
            success: function (data) {
                if (data == true) {}
            },
            error: function (xhr, textStatus, errorThrown) {
                //swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }

	function retornaDadosMembro() {
		var codigoAfiliacao = $("#codAfiliacaoMembro").val();
		var nomeMembro = $("#nomeMembro").val();
		var tipoMembro = '1';

		$.ajax({
			type: "post",
			url: "js/ajax/retornaDadosMembro.php",
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro},
			success: function (data) {
				if (data.result[0].fields.fNomCliente != "")
				{
					$("#h_seqCadastMembro").val(data.result[0].fields.fSeqCadast);
					$("#nomeMembro").val(data.result[0].fields.fNomCliente);
					$("#h_nomeMembro").val(data.result[0].fields.fNomCliente);
					$("#h_seqCadastCompanheiroMembro").val(data.result[0].fields.fSeqCadastCompanheiroRosacruz);
					$("#h_nomeCompanheiroMembro").val(data.result[0].fields.fNomConjuge);
					if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalRosacruz) {
						$("#principalCompanheiro").val(1);
					}
					if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroRosacruz) {
						$("#principalCompanheiro").val(2);
					}
				} else {
					swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
			}
		});
	}

	function verificaSeJaExisteNaLista(seqCadast)
	{
		var lb = document.getElementById("membros_atividade_iniciatica");

		for (var i = 0; i < lb.length; i++) {
			if (lb.options[i].value == seqCadast)
			{
				return true;
			}
		}
		return false;
	}

	function incluiMembroAtividadeIniciatica() {
		$(".chosen-choices").html('');
		$(".chosen-results").html('');
		var codigoAfiliacao = $("#codAfiliacaoMembro").val();
		if ($('#companheiroMembro').is(":checked")) {
			var seqCadast = $("#h_seqCadastCompanheiroMembro").val();
		} else {
			var seqCadast = $("#h_seqCadastMembro").val();
		}
		var nomeMembro = $("#nomeMembro").val();
		var companheiro = $("#h_companheiro").val();
		if (verificaSeJaExisteNaLista(seqCadast))
		{
			swal({title: "Aviso!", text: "Este membro já consta na lista!", type: "warning", confirmButtonColor: "#1ab394"});
		} else {
			if (seqCadast == 0)
			{
				swal({title: "Aviso!", text: "Insira o membro novamente", type: "warning", confirmButtonColor: "#1ab394"});
			} else {
                $("#membros_atividade_iniciatica").append('<option value="' + seqCadast + '" id="" name="' + companheiro + '">[' + codigoAfiliacao + '] ' + nomeMembro + '</option>');
				//verificaMembroJaAgendado(seqCadast, codigoAfiliacao, nomeMembro, companheiro);
			}
		}
		$("#codAfiliacaoMembro").val("");
		$("#nomeMembro").val("");
	}

	function verificaMembroJaAgendado(seqCadast, codigoAfiliacao, nomeMembro, companheiro){

		var tipoAtividadeIniciatica					= $("#tipoAtividadeIniciatica").val();
		if(tipoAtividadeIniciatica > 0) {
			$.ajax({
				type: "post",
				url: "js/ajax/verificaMembroJaAgendadoAtividadeIniciatica.php",
				dataType: "json",
				data: {seqCadast: seqCadast, tipoAtividadeIniciatica: tipoAtividadeIniciatica},
				success: function (data) {
					if (data.status == 1) {
						$("#membros_atividade_iniciatica").append('<option value="' + seqCadast + '" id="" name="' + companheiro + '">[' + codigoAfiliacao + '] ' + nomeMembro + '</option>');
					} else {
						swal({ title: "Aviso!", text: "Membro já está agendado em uma iniciação desse grau de templo.", type: "warning", confirmButtonColor: "#1ab394" });
					}
				},
				error: function (xhr, textStatus, errorThrown) {
					swal({ title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394" });
				}
			});
		} else {
			swal({ title: "Aviso!", text: "Você deve selecionar um Grau de Iniciação", type: "warning", confirmButtonColor: "#1ab394" });
		}
	}

	function limpaCampoMembros(){
		$("#membros_atividade_iniciatica").html("");
	}

	function verificaCamposPreenchidosAtividadeIniciatica(){
    		var fk_idOrganismoAfiliado					       = $("#fk_idOrganismoAfiliado").val();
    		var fk_idAtividadeIniciaticaOficial			   = $("#fk_idAtividadeIniciaticaOficial").val();
    		var fk_idAtividadeIniciaticaColumba			   = $("#fk_idAtividadeIniciaticaColumba").val();
    		var codAfiliacaoRecepcao					         = $("#codAfiliacaoRecepcao").val();
    		var nomeRecepcao							             = $("#nomeRecepcao").val();
    		var recepcaoAtividadeIniciaticaOficial		 = $("#recepcaoAtividadeIniciaticaOficial").val();
    		var tipoAtividadeIniciatica					       = $("#tipoAtividadeIniciatica").val();
    		var dataRealizadaAtividadeIniciatica		   = $("#dataRealizadaAtividadeIniciatica").val();
    		var horaRealizadaAtividadeIniciatica		   = $("#horaRealizadaAtividadeIniciatica").val();
    		var localAtividadeIniciatica				       = $("#localAtividadeIniciatica").val();

        var dataArray       = dataRealizadaAtividadeIniciatica.split("/");

        var data = new Date();
        if(data.getFullYear() <= dataArray[2]){
            if((data.getMonth()+1) <= dataArray[1]){
                if(data.getDate() < dataArray[0]){
                    swal({title:"Aviso!",text:"A data estipulada é depois da data de hoje.",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
                }
            }
        }
    console.log("fk_idOrganismoAfiliado: " + fk_idOrganismoAfiliado);
		if((fk_idOrganismoAfiliado > 0) && (fk_idOrganismoAfiliado != null)){
			/*
            if(fk_idAtividadeIniciaticaOficial > 0){
				if(fk_idAtividadeIniciaticaColumba > 0){
					if((codAfiliacaoRecepcao != "") || (nomeRecepcao != "") || (recepcaoAtividadeIniciaticaOficial != "")){
					*/
						if(tipoAtividadeIniciatica > 0){
							if((dataRealizadaAtividadeIniciatica != '00/00/0000') && (dataRealizadaAtividadeIniciatica != '')){
								//if((horaRealizadaAtividadeIniciatica != '00:00') && (horaRealizadaAtividadeIniciatica != '')){
									if(localAtividadeIniciatica != ""){
										$("#btnCadastroAtividade").addClass("disabled");
										//return false;
                                        cadastraMultiplasAtividadesIniciaticasAgeis();
									} else {
										swal({title: "Atenção!", text: "Local não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
									}
								//} else {
								//	swal({title: "Atenção!", text: "Hora não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
								//}
							} else {
								swal({title: "Atenção!", text: "Data não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
							}
						} else {
							swal({title: "Atenção!", text: "Grau de Iniciação não foi selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
						}
            /*
					} else {
						swal({title: "Atenção!", text: "Membro para recepção não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
					}
				} else {
					swal({title: "Atenção!", text: "Columba não foi selecionada", type: "warning", confirmButtonColor: "#1ab394"});
				}
			} else {
				swal({title: "Atenção!", text: "Equipe de Oficiais não foi selecionada", type: "warning", confirmButtonColor: "#1ab394"});
			}
			*/
		} else {
			swal({title: "Atenção!", text: "Organismo não foi selecionado", type: "warning", confirmButtonColor: "#1ab394"});
		}
	}

	function verificaCamposPreenchidosAtividadeIniciaticaSubmit(){
        var fk_idOrganismoAfiliado			        = $("#fk_idOrganismoAfiliado").val();
		var fk_idAtividadeIniciaticaOficial			= $("#fk_idAtividadeIniciaticaOficial").val();
		var fk_idAtividadeIniciaticaColumba			= $("#fk_idAtividadeIniciaticaColumba").val();
		var codAfiliacaoRecepcao					= $("#codAfiliacaoRecepcao").val();
		var nomeRecepcao							= $("#nomeRecepcao").val();
		var recepcaoAtividadeIniciaticaOficial		= $("#recepcaoAtividadeIniciaticaOficial").val();
		var tipoAtividadeIniciatica					= $("#tipoAtividadeIniciatica").val();
		var dataRealizadaAtividadeIniciatica		= $("#dataRealizadaAtividadeIniciatica").val();
		var horaRealizadaAtividadeIniciatica		= $("#horaRealizadaAtividadeIniciatica").val();
		var localAtividadeIniciatica				= $("#localAtividadeIniciatica").val();

        var textoOa = document.getElementById("fk_idOrganismoAfiliado").options[document.getElementById("fk_idOrganismoAfiliado").selectedIndex].text;
        var res = textoOa.split(" - ");
        var res2 = res[1].split(" ");
        var classificacaoOa = res2[0];
        //alert(classificacaoOa);

		if(fk_idOrganismoAfiliado > 0){
            if(fk_idAtividadeIniciaticaOficial > 0){
                if(fk_idAtividadeIniciaticaColumba > 0){
                    if((codAfiliacaoRecepcao != "") || (nomeRecepcao != "") || (recepcaoAtividadeIniciaticaOficial != "")){
                        if(tipoAtividadeIniciatica > 0){
                            if((dataRealizadaAtividadeIniciatica != undefined) && (dataRealizadaAtividadeIniciatica != null) && (dataRealizadaAtividadeIniciatica != "00/00/0000") && (dataRealizadaAtividadeIniciatica != "")){
                                if((horaRealizadaAtividadeIniciatica != '00:00') && (horaRealizadaAtividadeIniciatica != '') && (dataRealizadaAtividadeIniciatica != null) && (dataRealizadaAtividadeIniciatica != undefined)){
                                    if(localAtividadeIniciatica != ""){
                                        return true;
                                    } else {
                                        swal({title: "Atenção!", text: "Local não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
                                        return false;
                                    }
                                } else {
                                    swal({title: "Atenção!", text: "Hora não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
                                    return false;
                                }
                            } else {
                                swal({title: "Atenção!", text: "Data não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
                                return false;
                            }
                        } else {
                            swal({title: "Atenção!", text: "Grau de Iniciação não foi selecionado!", type: "warning", confirmButtonColor: "#1ab394"});
                            return false;
                        }
                    } else {
                        swal({title: "Atenção!", text: "Membro para recepção não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
                        return false;
                    }
                } else {
                    if(classificacaoOa!="Pronaos") {
                        swal({
                            title: "Atenção!",
                            text: "Columba não foi selecionada",
                            type: "warning",
                            confirmButtonColor: "#1ab394"
                        });
                        return false;
                    }
                }
            } else {
                swal({title: "Atenção!", text: "Equipe de Oficiais não foi selecionada", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            }
        } else {
            swal({title: "Atenção!", text: "Organismo não foi selecionado", type: "warning", confirmButtonColor: "#1ab394"});
            $("#fk_idOrganismoAfiliado").chosen().focus();
            return false;
        }
	}

	function retornaDadosMembroIniciatico() {
		var codigoAfiliacao = $("#codAfiliacaoMembro").val();
		var nomeMembro = $("#nomeMembro").val();
		var tipoMembro = '1';

		$.ajax({
			type: "post",
			url: "js/ajax/retornaDadosMembro.php",
			dataType: "json",
			data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro},
			success: function (data) {
				if (data.result[0].fields.fNomCliente != "")
				{
					$("#h_seqCadastMembro").val(data.result[0].fields.fSeqCadast);
					$("#nomeMembro").val(data.result[0].fields.fNomCliente);
					$("#h_nomeMembro").val(data.result[0].fields.fNomCliente);
					if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastPrincipalRosacruz) { $("#h_companheiro").val('N'); }
					if (data.result[0].fields.fSeqCadast == data.result[0].fields.fSeqCadastCompanheiroRosacruz) { $("#h_companheiro").val('S'); }
					if ((data.result[0].fields.fSeqCadastPrincipalRosacruz == 0) && (data.result[0].fields.fSeqCadastCompanheiroRosacruz == 0)) { $("#h_companheiro").val('N'); }
				} else {
					swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
			}
		});
	}

	function retornaNomeExtraido(textoSelect){

		var arrayTexto         = textoSelect.split(" ");
		var textoParaEliminar  = arrayTexto[0] + " ";
		var nome               = substr(texto, strlen(textoParaEliminar));

		return nome;
	}

	function retornaCodAfiliacaoExtraido(textoSelect){
		var arrayTexto         = textoSelect.split(" ");
		var textoCod		   = arrayTexto[0];
		var textoCod	   = textoCod.replace("[","");
		var codAfiliacao	   = textoCod.replace("]","");
		return codAfiliacao;
	}

	function atualizarObrigacaoRitualisticaMembro(loginAtualizadoPor, seqCadast, codigoAfiliacao, tipoMembro, tipoObrigacao, dataObrigacao, siglaOA, descricaoLocal, companheiro,seqAtualizadoPor, idAtividade, fk_idOrganismoAfiliado){


/*
		alert("CodUsuario (string):       " + loginAtualizadoPor);
		alert("SeqCadast (Integer):       " + seqCadast);
		alert("CodMembro (Integer):       " + codigoAfiliacao);
		alert("IdeTipoMembro (Integer):   " + tipoMembro);
		alert("SeqTipoObriga (Integer):   " + tipoObrigacao);
		alert("DatObriga (string):        " + dataObrigacao);
		alert("SigOrgafi (string):        " + siglaOA);
		alert("DesLocal (string):         " + descricaoLocal);
		alert("IdeCompanheiro (string):   " + companheiro);
        alert("Fk_idOrganismoAfiliado:	  " + fk_idOrganismoAfiliado);
        alert("SeqAtualizadoPor (Integer):" + seqAtualizadoPor);
*/
		$.ajax({
			type: "post",
			url: "js/ajax/atualizarObrigacaoRitualisticaMembro.php",
			dataType: "json",
			data: {
				fk_idOrganismoAfiliado:fk_idOrganismoAfiliado,
				loginAtualizadoPor: loginAtualizadoPor,
                seqCadast: seqCadast,
				codigoAfiliacao: 0,
				tipoMembro: 1,
				tipoObrigacao: tipoObrigacao,
				dataObrigacao: dataObrigacao,
				siglaOA: siglaOA,
				descricaoLocal: descricaoLocal,
				companheiro: "",
                seqAtualizadoPor:seqAtualizadoPor
			},
			success: function (data) {

				//alert(data);

				if(data.status==2)
				{
                    swal({title:"Aviso!",text:"A atividade iniciática já foi cadastrada!",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
				}

                if(data.status==3)
                {
                    swal({title:"Aviso!",text:"Existe uma atividade iniciática que já foi cadastrada nesta data!",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
                }

                //   data.result[0].fields.fDesMensag:
                //      "Processamento Ok, atualização realizada !"
                //      "Processamento Ok, inclusão realizada !"
                //      "Não foi possível incluir a Obrigação Ritualística com os dados informados!"
                //      "Os dados fornecidos estão incompletos !"
                //      "Obrigação Ritualística já está incluída para este membro na mesma data e hora!"
                //alert(data);
                //if (!$.trim(data)){ console.log("data null"); } else { }
                var msgRetornoWebService;
                switch (data.status) {
                  case 1:
                    msgRetornoWebService = "Atualização realizada!"
                    break;
                  default:
                      msgRetornoWebService = "Erro no processo de inclusão da obrigação ritualística!";
                }

                if(data == undefined){
                    mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividade,'Erro em todo o processo de inclusão da obrigação ritualística');
                } else {
                    if(data.result[0].fields.fIdeInclusaoOk != 'S'){
                        mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividade,msgRetornoWebService);
                    }
                }

			},
			error: function (xhr, textStatus, errorThrown) {
				swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividade,'Erro no processo.');
			}
		});
                

	}

    function atualizarObrigacaoRitualisticaMembros(loginAtualizadoPor, membros, codigoAfiliacao, tipoMembro, tipoObrigacao, dataObrigacao, siglaOA, descricaoLocal, companheiro,seqAtualizadoPor, idAtividade, fk_idOrganismoAfiliado){

        /*
                alert("CodUsuario (string):       " + loginAtualizadoPor);
                alert("SeqCadast (Integer):       " + seqCadast);
                alert("CodMembro (Integer):       " + codigoAfiliacao);
                alert("IdeTipoMembro (Integer):   " + tipoMembro);
                alert("SeqTipoObriga (Integer):   " + tipoObrigacao);
                alert("DatObriga (string):        " + dataObrigacao);
                alert("SigOrgafi (string):        " + siglaOA);
                alert("DesLocal (string):         " + descricaoLocal);
                alert("IdeCompanheiro (string):   " + companheiro);
                alert("Fk_idOrganismoAfiliado:	  " + fk_idOrganismoAfiliado);
                alert("SeqAtualizadoPor (Integer):" + seqAtualizadoPor);
        */
        $.ajax({
            type: "post",
            url: "js/ajax/atualizarObrigacaoRitualisticaMembros.php",
            dataType: "json",
            data: {
                fk_idOrganismoAfiliado:fk_idOrganismoAfiliado,
                loginAtualizadoPor: loginAtualizadoPor,
                membros: membros,
                codigoAfiliacao: 0,
                tipoMembro: 1,
                tipoObrigacao: tipoObrigacao,
                dataObrigacao: dataObrigacao,
                siglaOA: siglaOA,
                descricaoLocal: descricaoLocal,
                companheiro: "",
                seqAtualizadoPor:seqAtualizadoPor
            },
            success: function (data) {

                //alert(data);
                return false;

                if(data.status==2)
                {
                    swal({title:"Aviso!",text:"A atividade iniciática já foi cadastrada!",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
                }

                if(data.status==3)
                {
                    swal({title:"Aviso!",text:"Existe uma atividade iniciática que já foi cadastrada nesta data!",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
                }

                //   data.result[0].fields.fDesMensag:
                //      "Processamento Ok, atualização realizada !"
                //      "Processamento Ok, inclusão realizada !"
                //      "Não foi possível incluir a Obrigação Ritualística com os dados informados!"
                //      "Os dados fornecidos estão incompletos !"
                //      "Obrigação Ritualística já está incluída para este membro na mesma data e hora!"
                //alert(data);
                //if (!$.trim(data)){ console.log("data null"); } else { }
                var msgRetornoWebService;
                switch (data.status) {
                    case 1:
                        msgRetornoWebService = "Atualização realizada!"
                        break;
                    default:
                        msgRetornoWebService = "Erro no processo de inclusão da obrigação ritualística!";
                }

                if(data == undefined){
                    mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividade,'Erro em todo o processo de inclusão da obrigação ritualística');
                } else {
                    if(data.result[0].fields.fIdeInclusaoOk != 'S'){
                        mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividade,msgRetornoWebService);
                    }
                }

            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
                //mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividade,'Erro no processo.');
            }
        });


    }

    function mudaStatusErroMembroAtividadeIniciatica(seqCadast,idAtividadeIniciatica,msgRetornoWebService){
        $.ajax({
            type: "post",
            url: "js/ajax/mudaStatusErroMembroAtividadeIniciatica.php",
            dataType: "json",
            data: {
                seqCadast: seqCadast,
                idAtividadeIniciatica: idAtividadeIniciatica,
                msgRetornoWebService: msgRetornoWebService
            },
            success: function (data) {
                if(data.alterado == true){
                    //console.log("#acoesMembroAtividade"+seqCadast+idAtividadeIniciatica);
                    //console.log("#statusMembroAtividade"+seqCadast+idAtividadeIniciatica);

                    $("#acoesMembroAtividade"+seqCadast+idAtividadeIniciatica).html('');
                    $("#statusMembroAtividade"+seqCadast+idAtividadeIniciatica).html('');

                    if (msgRetornoWebService === "Iniciação já cadastrada!"){
                      $("#acoesMembroAtividade"+seqCadast+idAtividadeIniciatica).append("--");
                    } else {
                      $("#acoesMembroAtividade"+seqCadast+idAtividadeIniciatica).append("<a class='btn-xs btn-primary' onclick='atualizarObrigacaoRitualisticaMembroNovamente("+seqCadast+","+idAtividadeIniciatica+")'>Tentar Novamente</a>");
                    }

                    $("#statusMembroAtividade"+seqCadast+idAtividadeIniciatica).append("<span class='badge badge-danger'>" + msgRetornoWebService + "</span>");

                    $("." + idAtividadeIniciatica + seqCadast).val(4);
                    //$("#" + seqCadast).val(4);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }

    function atualizarObrigacaoRitualisticaMembroNovamente(seqCadast,idAtividadeIniciatica){
        $.ajax({
            type: "post",
            url: "js/ajax/retornaDadosNovaTentativaDeCadastroAtividadeIniciatica.php",
            dataType: "json",
            data: {
                seqCadast: seqCadast,
                idAtividadeIniciatica: idAtividadeIniciatica
            },
            success: function (data) {
                console.log("-------------------------------------");
                console.log("Dados retornados da Atividade Para envio novamente: ");
                console.log(data);
                console.log("-------------------------------------");
                    //alert(data);
                if(data.encontrado==true){

                    $("#acoesMembroAtividade"+data.seqCadast+idAtividadeIniciatica).html('');
                    $("#statusMembroAtividade"+data.seqCadast+idAtividadeIniciatica).html('');

                    $("#acoesMembroAtividade"+data.seqCadast+idAtividadeIniciatica).append(" -- ");
                    $("#statusMembroAtividade"+data.seqCadast+idAtividadeIniciatica).append("<span class='badge badge-primary'>Compareceu</span>");

                    confirmaParticipacaoMembroAtividadeIniciatica(data.seqCadast,idAtividadeIniciatica);

                    atualizarObrigacaoRitualisticaMembro(
                        data.loginAtualizadoPor,
                        data.seqCadast,
                        data.codigoAfiliacao,
                        data.tipoMembro,
                        data.tipoObrigacao,
                        data.dataObrigacao,
                        data.siglaOA,
                        data.descricaoLocal,
                        data.companheiro,
                        data.loginAtualizadoPor,
                        idAtividadeIniciatica,
						data.fk_idOrganismoAfiliado
                    );
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }

    function atualizarObrigacaoRitualisticaMembroAgendado(idAtividadeIniciatica,dia,mes,ano){

        //swal({title: "Confirmado!", text: "Atividade Iniciática realizada. Os membros que compareceram já contam com tal registro. Acesse (=>Consultas=>Membros) e verifique se está tudo ok!", type: "success", confirmButtonColor: "#1ab394"});
    	//return false;

        var totalCompareceu = $('.compareceu').length;

		if(parseInt($("#contadorClicks").val())<=0&&totalCompareceu>0) {
            $("#contadorClicks").val(parseInt($("#contadorClicks").val()) + 1);
        }else{
            swal({title: "Erro!", text: "Você já clicou nessa opção! Essa informação já processada pelo sistema!", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
		}



		//Gerar array Compareceu e Não compareceu
        var arrCompareceu = new Array();
        var arrAux =  new Array();
        var string1 = $('#compareceram').val();
        //alert("string1:"+string1);
			//string1 = string1.substr(2,(string1.length - 2));
		var arrCompareceuComIdAtividade = string1.split("@@");
		//alert("teste Braz primeiro que compareceu:"+arrCompareceuComIdAtividade[0]);

        var strCompareceu ="";

        for(var i=0; i < arrCompareceuComIdAtividade.length; i++){

            arrAux = arrCompareceuComIdAtividade[i].split("/");
            strCompareceu = arrAux[0];

        }

        //alert(strCompareceu);

        //arrCompareceu.splice((arrCompareceu.length-1), 1);

        var arrNaoCompareceu = new Array();
        var arrNaoAux =  new Array();
        var string2 = $('#naoCompareceram').val();
        //alert("string2:"+string2);
        //string2 = string2.substr(2,(string2.length - 2));
        var arrNaoCompareceuComIdAtividade = string2.split("@@");
        for(var i=0; i < arrNaoCompareceuComIdAtividade.length; i++){

                arrNaoAux = arrNaoCompareceuComIdAtividade[i].split("/");
                arrNaoCompareceu.push(arrNaoAux[0]);

        }

        arrNaoCompareceu.splice((arrNaoCompareceu.length-1), 1);

        /*
        var grupoArray = new Array();
        var grupoArrayNaoCompareceram = new Array();
        var grupo = $('input[name="membros[]"]');


        for(var i=0; i < grupo.length; i++){
            //alert('teste:'+i);
            if(grupo[i].value == 2){
                grupoArray.push(grupo[i].id.replace('acoesMembroAtividade',''));
            }
        }
        for(var i=0; i < grupo.length; i++){
            if(grupo[i].value != 2){
                grupoArrayNaoCompareceram.push(grupo[i].id.replace('acoesMembroAtividade',''));
            }
        }
		*/
        var loginAtualizadoPor      = $("#loginAtualizadoPor").val();
        var seqAtualizadoPor      	= $("#seqAtualizadoPor").val();
        var tipoObrigacao           = retornaIdeTipoObrigacaoORCZ($("#tipoObrigracao" + idAtividadeIniciatica).val());
        var dataObrigacao           = $("#dataObrigacao" + idAtividadeIniciatica).val();
        var descricaoLocal          = $("#descricaoLocal" + idAtividadeIniciatica).val();
        var fk_idOrganismoAfiliado  = $("#fk_idOrganismoAfiliado" + idAtividadeIniciatica).val();

        var data = new Date();
        if(data.getFullYear() <= ano){
            if((data.getMonth()+1) <= mes){
                if(data.getDate() < dia){
                    swal({title:"Aviso!",text:"A atividade ainda não pode ser dada como realizada. Hoje não é a data estipulada.",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
                }
            }
        }

        //alert(grupoArray.length);

        if(totalCompareceu >= 1){ // && (grupoArray.length <= 20)
            if(totalCompareceu >= 13){
                swal({title: "Atenção!", text: "A atividade deve ter no máximo 12 membros que compareceram. Informe a GLP o motivo desse número ter excedido.", type: "warning", confirmButtonColor: "#1ab394"});
            }
            $.ajax({
                type: "post",
                url: "js/ajax/retornaSiglaOrganismoAfiliado.php",
                dataType: "json",
                data: {fk_idOrganismoAfiliado: fk_idOrganismoAfiliado},
                success: function (data) {
                    if (data.encontrado > 0) {

                        var siglaOA = data.siglaOrganismoAfiliado;

                        atualizarObrigacaoRitualisticaMembros(loginAtualizadoPor, string1, 0, 1, tipoObrigacao, dataObrigacao, siglaOA, descricaoLocal,'',seqAtualizadoPor,idAtividadeIniciatica, fk_idOrganismoAfiliado);
                        swal({title: "Confirmado!", text: "Atividade Iniciática realizada. Os membros que compareceram já contam com tal registro. Acesse (=>Consultas=>Membros) e verifique se está tudo ok!", type: "success", confirmButtonColor: "#1ab394"});



                        $("#botaoAtividadeRealizada" + idAtividadeIniciatica).html('');
                        $("#botaoAtividadeRealizada" + idAtividadeIniciatica).append('<a class="btn btn-sm btn-primary" onclick="abrirPopUpAtividadeIniciaticaMembrosParaAssinatura(' + idAtividadeIniciatica + ',' + fk_idOrganismoAfiliado + ')"> <i class="fa fa-print fa-white"></i> Imprimir</a><button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>');

                        $.ajax({
                            type: "post",
                            url: "js/ajax/alteraStatusAtividadeRealizada.php",
                            dataType: "json",
                            data: {idAtividadeIniciatica: idAtividadeIniciatica},
                            success: function (data) {
                                if(data.alterado == true){
                                    $(".statusTarget" + idAtividadeIniciatica).html('');
                                    $(".statusTarget" + idAtividadeIniciatica).append("<span class='badge badge-success'>Realizada</span>");
                                } else {
                                    $(".statusTarget" + idAtividadeIniciatica).html('');
                                    $(".statusTarget" + idAtividadeIniciatica).append("<span class='badge badge-warning'>Erro</span>");
                                }
                            },
                            error: function (xhr, textStatus, errorThrown) {
                                $(".statusTarget" + idAtividadeIniciatica).html('');
                                $(".statusTarget" + idAtividadeIniciatica).append("<span class='badge badge-warning'>Erro</span>");
                            }
                        });

						$(".areaInclusaoMembro"+idAtividadeIniciatica).html('');

                        for(var i=0; i < arrNaoCompareceu.length; i++){
                            $("#acoesMembroAtividade"+arrNaoCompareceu[i]+idAtividadeIniciatica).html('');
                            $("#acoesMembroAtividade"+arrNaoCompareceu[i]+idAtividadeIniciatica).append(' -- ');
                        }

                    } else {
                        swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                }
            });
        } else { swal({title: "Atenção!", text: "A atividade deve ter no mínimo 1 membro que compareceu.", type: "warning", confirmButtonColor: "#1ab394"}); }
    }

	function retornaIdeTipoObrigacaoORCZ(tipo){
		switch(tipo){
			case "1": return 3; break;
			case "2": return 5; break;
			case "3": return 7; break;
			case "4": return 9; break;
			case "5": return 11; break;
			case "6": return 13; break;
			case "7": return 15; break;
			case "8": return 17; break;
			case "9": return 19; break;
			case "10": return 32; break;
			case "11": return 33; break;
			case "12": return 34; break;
			case "13": return 1; break;
			case "14": return 36; break;
			case "15": return 35; break;
			case "16": return 24; break;
			default: return 0
		}
	}

	function retornaSiglaOrganismoAfiliado(fk_idOrganismoAfiliado){
		$.ajax({
			type: "post",
			url: "js/ajax/retornaSiglaOrganismoAfiliado.php",
			dataType: "json",
			data: {fk_idOrganismoAfiliado: fk_idOrganismoAfiliado},
			success: function (data) {
				if (data.encontrado > 0) {
					$("#siglaOA").val(data.siglaOrganismoAfiliado)
				} else {
					$("#siglaOA").val("error")
				}
			},
			error: function (xhr, textStatus, errorThrown) {
				$("#siglaOA").val("error")
			}
		});
	}

    function retornaSiglaOA(fk_idOrganismoAfiliado){
        if(fk_idOrganismoAfiliado != 0){
            $.ajax({
                type: "post",
                url: "js/ajax/retornaSiglaOrganismoAfiliado.php",
                dataType: "json",
                data: {fk_idOrganismoAfiliado: fk_idOrganismoAfiliado},
                success: function (data) {
                    if (data.encontrado > 0) {
                        return data.siglaOrganismoAfiliado;
                    } else {
                        return 0;
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    return 0;
                }
            });
        } else {
            return 0;
        }
    }

    function verificaCamposPreenchidosColumbaSubmit(){

        var fk_idOrganismoAfiliado			                        = $("#fk_idOrganismoAfiliado").val();
        var h_seqCadastColumba			                            = $("#h_seqCadastColumba").val();
        var h_nomeColumba			                                = $("#h_nomeColumba").val();
/*
        var enderecoAtividadeIniciaticaColumba			            = $("#enderecoAtividadeIniciaticaColumba").val();
        var cepAtividadeIniciaticaColumba			                = $("#cepAtividadeIniciaticaColumba").val();
        var naturalidadeAtividadeIniciaticaColumba			        = $("#naturalidadeAtividadeIniciaticaColumba").val();

        var dataNascimentoAtividadeIniciaticaColumba		        = $("#dataNascimentoAtividadeIniciaticaColumba").val();

        var escolaAtividadeIniciaticaColumba			            = $("#escolaAtividadeIniciaticaColumba").val();
        var emailAtividadeIniciaticaColumba			                = $("#emailAtividadeIniciaticaColumba").val();

        var dataAdmissaoAtividadeIniciaticaColumba			        = $("#dataAdmissaoAtividadeIniciaticaColumba").val();
        var dataInstalacaoAtividadeIniciaticaColumba			    = $("#dataInstalacaoAtividadeIniciaticaColumba").val();
        var dataAtivaAteAtividadeIniciaticaColumba			        = $("#dataAtivaAteAtividadeIniciaticaColumba").val();

        var responsavelPlenoAtividadeIniciaticaColumba			    = $("#responsavelPlenoAtividadeIniciaticaColumba").val();
        var seqCadastResponsavelPlenoAtividadeIniciaticaColumba		= $("#seqCadastResponsavelPlenoAtividadeIniciaticaColumba").val();
        var emailResponsavelAtividadeIniciaticaColumba			    = $("#emailResponsavelAtividadeIniciaticaColumba").val();
*/
        if(fk_idOrganismoAfiliado > 0){
            if((h_seqCadastColumba != "") && (h_nomeColumba != "")){
                /*
                if(enderecoAtividadeIniciaticaColumba != ""){
                    if(cepAtividadeIniciaticaColumba != ""){
                        if(naturalidadeAtividadeIniciaticaColumba != ""){
                            if((dataNascimentoAtividadeIniciaticaColumba != undefined) && (dataNascimentoAtividadeIniciaticaColumba != null) && (dataNascimentoAtividadeIniciaticaColumba != "00/00/0000") && (dataNascimentoAtividadeIniciaticaColumba != "")){
                                if(escolaAtividadeIniciaticaColumba != ""){
                                    if(emailAtividadeIniciaticaColumba != ""){
                                        if((dataAdmissaoAtividadeIniciaticaColumba != undefined) && (dataAdmissaoAtividadeIniciaticaColumba != null) && (dataAdmissaoAtividadeIniciaticaColumba != "00/00/0000") && (dataAdmissaoAtividadeIniciaticaColumba != "")){
                                            if((dataInstalacaoAtividadeIniciaticaColumba != undefined) && (dataInstalacaoAtividadeIniciaticaColumba != null) && (dataInstalacaoAtividadeIniciaticaColumba != "00/00/0000") && (dataInstalacaoAtividadeIniciaticaColumba != "")){
                                                if((dataAtivaAteAtividadeIniciaticaColumba != undefined) && (dataAtivaAteAtividadeIniciaticaColumba != null) && (dataAtivaAteAtividadeIniciaticaColumba != "00/00/0000") && (dataAtivaAteAtividadeIniciaticaColumba != "")){
                                                    if(responsavelPlenoAtividadeIniciaticaColumba > 0){
                                                        if(seqCadastResponsavelPlenoAtividadeIniciaticaColumba != ""){
                                                            if(emailResponsavelAtividadeIniciaticaColumba != ""){
                */
                                                                return true;
                /*
                                                            } else {
                                                                swal({title: "Atenção!", text: "E-mail do responsável pleno não foi preenchido", type: "warning", confirmButtonColor: "#1ab394"});
                                                                return false;
                                                            }
                                                        } else {
                                                            swal({title: "Atenção!", text: "Deve ser passa um membro como responsável pleno", type: "warning", confirmButtonColor: "#1ab394"});
                                                            return false;
                                                        }
                                                    } else {
                                                        swal({title: "Atenção!", text: "Membro responsável não foi selecionado", type: "warning", confirmButtonColor: "#1ab394"});
                                                        return false;
                                                    }
                                                } else {
                                                    swal({title: "Atenção!", text: "Data de atividade não foi preenchida", type: "warning", confirmButtonColor: "#1ab394"});
                                                    return false;
                                                }
                                            } else {
                                                swal({title: "Atenção!", text: "Data de instalação não foi preenchida", type: "warning", confirmButtonColor: "#1ab394"});
                                                return false;
                                            }
                                        } else {
                                            swal({title: "Atenção!", text: "Data de admissão não foi preenchida", type: "warning", confirmButtonColor: "#1ab394"});
                                            return false;
                                        }
                                    } else {
                                        swal({title: "Atenção!", text: "O e-mail não foi preenchido.", type: "warning", confirmButtonColor: "#1ab394"});
                                        return false;
                                    }
                                } else {
                                    swal({title: "Atenção!", text: "A escola não foi preenchida.", type: "warning", confirmButtonColor: "#1ab394"});
                                    return false;
                                }
                            } else {
                                swal({title: "Atenção!", text: "Data de nascimento não foi preenchida.", type: "warning", confirmButtonColor: "#1ab394"});
                                return false;
                            }
                        } else {
                            swal({title: "Atenção!", text: "A naturalidade não foi preenchida.", type: "warning", confirmButtonColor: "#1ab394"});
                            return false;
                        }
                    } else {
                        swal({title: "Atenção!", text: "O CEP não foi preenchido.", type: "warning", confirmButtonColor: "#1ab394"});
                        return false;
                    }
                } else {
                    swal({title: "Atenção!", text: "O endereço não foi preenchido.", type: "warning", confirmButtonColor: "#1ab394"});
                    return false;
                }
                */
            } else {
                swal({title: "Atenção!", text: "Os dados da columba (Cód de Afiliação e Nome) não foram preenchidos.", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            }
        } else {
            swal({title: "Atenção!", text: "Organismo Afiliado não foi selecionado.", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
    }

    function verificaCamposPreenchidosAtivOficiaisSubmit(){
        var fk_idOrganismoAfiliado			                = $("#fk_idOrganismoAfiliado").val();
        var anoAtividadeIniciaticaOficial			        = $("#anoAtividadeIniciaticaOficial").val();
        var tipoAtividadeIniciaticaOficial					= $("#tipoAtividadeIniciaticaOficial").val();

        if(fk_idOrganismoAfiliado > 0){
            if(anoAtividadeIniciaticaOficial > 0){
                if(tipoAtividadeIniciaticaOficial > 0){
                    return true;
                } else {
                    swal({title: "Atenção!", text: "Nº da equipe não foi selecionada", type: "warning", confirmButtonColor: "#1ab394"});
                    return false;
                }
            } else {
                swal({title: "Atenção!", text: "Ano da equipe não foi selecionada", type: "warning", confirmButtonColor: "#1ab394"});
                return false;
            }
        } else {
            swal({title: "Atenção!", text: "Organismo Afiliado não foi selecionada", type: "warning", confirmButtonColor: "#1ab394"});
            return false;
        }
    }

    function enviaEmailMembroAtividadeIniciatica(seqCadastMembro,idAtividadeIniciatica,nomeMembro)
    {
        $.ajax({
            type: "post",
            url: "js/ajax/enviaEmailMembroAtividadeIniciatica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {seqCadastMembro:seqCadastMembro, idAtividadeIniciatica:idAtividadeIniciatica, nomeMembro:nomeMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno) {

            },
            error: function (xhr, textStatus, errorThrown) {
                    swal({title:"Aviso!",text:"Erro ao enviar Email para o Membro da Atividade Iniciatica.",type:"warning",confirmButtonColor:"#1ab394"});
            }
        });
    }

    function retornaDadosMembroAtividadeAgendada(idAtividade) {
        var codigoAfiliacao = $("#codAfiliacaoMembro"+idAtividade).val();
        var nomeMembro = $("#nomeMembro"+idAtividade).val();
        var tipoMembro = '1';
        $.ajax({
            type: "post",
            url: "js/ajax/retornaDadosMembro.php",
            dataType: "json",
            data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro},
            success: function (data) {
                if (data.result[0].fields.fNomCliente != "") {
                    $("#nomeMembro" + idAtividade).val(data.result[0].fields.fNomCliente);
                    $("#h_seqCadastMembro" + idAtividade).val(data.result[0].fields.fSeqCadast);
                    $("#h_nomeMembro" + idAtividade).val(data.result[0].fields.fNomCliente);
                } else {
                    swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                    $("#codAfiliacaoMembro" + idAtividade).val('');
                    $("#nomeMembro" + idAtividade).val('');
                    $("#h_seqCadastMembro" + idAtividade).val('');
                    $("#h_nomeMembro" + idAtividade).val('');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Aviso!", text: "Erro ao realizar a ação, tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }

    function buscaAtividadeIniciaticaMembro(){

        var idAtividadeIniciatica = $("#idAtividadeIniciatica").val();
        var seqCadastMembro = $("#h_seqCadastMembro").val();
        var nomeMembro = $("#h_nomeMembro").val();

        $.ajax({
            type: "post",
            url: "js/ajax/retornaDadosMaisObrigacoesRitualisticasDoMembro.php",
            dataType: "json",
            data: { seqCadast: seqCadastMembro },
            success: function (data) {
                //console.log(data);
				//alert(data);
                var iniciacoes = JSON.stringify(data.fields);
                //print_r(data.fields);

				/*
				alert("idAtividadeIniciatica"+idAtividadeIniciatica);
                alert("seqCadastMembro"+seqCadastMembro);
                alert("nomeMembro"+nomeMembro);
                //alert(iniciacoes);
                data.fNumLoteAtualRosacr=10;
                alert("fNumLoteAtualRosacr"+data.fNumLoteAtualRosacr);
                alert("fDatAdmissRosacr"+data.fDatAdmissRosacr);
                alert("fIdeTipoSituacMembroRosacr"+data.fIdeTipoSituacMembroRosacr);
				*/
                incluiMembroAtividadeAgendada(idAtividadeIniciatica,
                                              seqCadastMembro,
                                              nomeMembro,
                                              iniciacoes,
                                              data.fNumLoteAtualRosacr,
                                              data.fDatAdmissRosacr,
                                              data.fIdeTipoSituacMembroRosacr
                                            );

            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Aviso!", text: "Erro ao realizar a ação, tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }

    function incluiMembroAtividadeAgendada(idAtividadeIniciatica,seqCadastMembro,nomeMembro,iniciacoes,fNumLoteAtualRosacr, fDatAdmissRosacr, fIdeTipoSituacMembroRosacr){

    	//alert(seqCadastMembro);
        $.ajax({
            type: "post",
            url: "js/ajax/retornaDisponibilidadeMembroAtividadeIniciatica.php",
            dataType: "json",
            data: { seqCadastMembro: seqCadastMembro, iniciacoes: iniciacoes, idAtividade: idAtividadeIniciatica, nomeMembro: nomeMembro ,fNumLoteAtualRosacr:fNumLoteAtualRosacr, fDatAdmissRosacr:fDatAdmissRosacr, fIdeTipoSituacMembroRosacr:fIdeTipoSituacMembroRosacr},
            success: function (data) {
                //console.log(data);
				//alert(data);
                if (data.disponibilidade == true) {
                    listaMembrosAtividadeIniciatica(idAtividadeIniciatica);
                    enviaEmailMembroAtividadeIniciatica(seqCadastMembro,idAtividadeIniciatica,nomeMembro);
                    swal({
                            title:"Confirmado!",
                            text:"O membro receberá um e-mail (caso ele o tenha) para confirmar a participação.",
                            type:"success",
                            showCancelButton: false,
                            confirmButtonColor:"#1ab394",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        }, function(){
                            //FECHA MODAL
                        });

                } else {
                    swal({title: "Aviso!", text: "O membro não pode realizar esta atividade iniciatica.", type: "warning", confirmButtonColor: "#1ab394"});
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Aviso!", text: "Erro ao realizar a ação, tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }

    function listaMembrosAtividadeIniciatica(){
        var idAtividadeIniciatica = $('#idAtividadeIniciatica').val();
        $.ajax({
            type: "post",
            url: "js/ajax/getListaMembrosParticipantesAtividadeIniciatica.php",
            dataType: "json",
            data: { idAtividadeIniciatica: idAtividadeIniciatica },
            success: function (data) {
                //console.log(data);
                if (data.qtdMembros > 0) {
                    $("#membrosAtividadeIniciatica").html('');
                    for(var i=0; i < data.membros.length; i++){
                        $("#membrosAtividadeIniciatica").append(data.membros[i]);
                    }
                    $("#codAfiliacaoMembro").val('');
                    $("#nomeMembro").val('');
                    $("#h_seqCadastMembro").val('');
                    $("#h_nomeMembro").val('');
                } else {
                    $("#membrosAtividadeIniciatica").html('');
                    $("#membrosAtividadeIniciatica").append('<tr><td colspan="3"><div class="alert alert-danger"><center>Ainda não há nenhum membro cadastrado nessa atividade!</center></div></td></tr>');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                $("#membrosAtividadeIniciatica").html('');
                $("#membrosAtividadeIniciatica").append('<tr><td colspan="3"><div class="alert alert-danger"><center>Ocorreu algum erro ao listar os membros! Atualize a página.</center></div></td></tr>');
            }
        });
    }

    function modeloRetornaDadosMembro(seqCadastMembro) {
        $.ajax({
            type: "post",
            url: "js/ajax/retornaDadosMembroPorSeqCadast.php",
            dataType: "json",
            data: {
              seqCadast: seqCadastMembro,
              tipoMembro: 1
            },
            success: function (data) {
                //alert(data.result[0].fields.fSeqCadast);
                if (data.fields.fSeqCadast == 0) {

                } else {

                }
            },
            error: function (xhr, textStatus, errorThrown) {
                alert('erro retorna dados');
            }
        });
    }
    
    function getMembrosAtividadeIniciatica(idAtividadeIniciatica, statusAtividadeIniciatica, idOrganismoAfiliado, dia, mes, ano){



        //console.log(idAtividadeIniciatica + " / " + statusAtividadeIniciatica + " / " + idOrganismoAfiliado + " / " + dia + " / " + mes + " / " + ano);

        $('#numeroAtividadeIniciatica').html('');
        $('#membrosAtividadeIniciatica').html('');
        $('#areaInclusaoMembro').html('');
        $('#botaoAtividadeRealizada').html('');

        $.ajax({
            type: "post",
            url: "js/ajax/getMembrosAtividadeIniciatica.php",
            dataType: "html",
            data: {idAtividadeIniciatica: idAtividadeIniciatica},
            success: function (data) {

                $('#numeroAtividadeIniciatica').append(idAtividadeIniciatica);

                $('#membrosAtividadeIniciatica').append(data);

				//console.log(statusAtividadeIniciatica);
                if(statusAtividadeIniciatica === 2){
                    $('#botaoAtividadeRealizada').
					append('<a class="btn btn-sm btn-primary" onclick="abrirPopUpAtividadeIniciaticaMembrosParaAssinatura(' + idAtividadeIniciatica + ',' + idOrganismoAfiliado + ');"><i class="fa fa-print fa-white"></i> Imprimir </a>'
						+ '<input type="hidden" name="idAtividadeIniciatica" id="idAtividadeIniciatica" value="' + idAtividadeIniciatica + '">'
                        + '<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>');

				} else {
                	$('#areaInclusaoMembro').
					append('<div class="row">'
							+ '<div class="form-group">'
								+ '<div class="col-sm-12">'
									+ '<b>Inclusão de membro: </b>'
								+ '</div>'
							+ '</div>'
                        + '</div>'
                        + '<div class="row">'
							+ '<div class="form-group form-inline">'
								+ '<div class="col-sm-12" style="margin-top: 15px">'
									+ '<input placeholder="Código" class="form-control" id="codAfiliacaoMembro" maxlength="7" type="text" value="" style="max-width: 76px">'
									+ '<input placeholder="Nome completo" class="form-control" id="nomeMembro" type="text" maxlength="100" value="" style="min-width: 320px">'
									+ '<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica(\'resultadoPesquisa\',\'codAfiliacaoMembro\',\'nomeMembro\',\'codAfiliacaoMembro\',\'nomeMembro\',\'h_seqCadastMembro\',\'h_nomeMembro\',\'myModalPesquisa\',\'informacoesPesquisa\');">'
										+ '<icon class="fa fa-search"></icon>'
									+ '</a>'
									+ '&nbsp;'
									+ '<a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="buscaAtividadeIniciaticaMembro();"> Incluir</a><br>'
									+ '<input type="hidden" name="tipo" id="tipo" value="1">'
									+ '<input type="hidden" id="h_seqCadastMembro">'
									+ '<input type="hidden" id="h_nomeMembro">'
								+ '</div>'
							+ '</div>'
						+ '</div>');

                    $('#botaoAtividadeRealizada').
					append('<a class="btn btn-sm btn-primary" id="btnAtividadeIniciatica" onclick="atualizarObrigacaoRitualisticaMembroAgendado(' + idAtividadeIniciatica + ',' + dia + ',' + mes + ',' + ano + ');"><i class="fa fa-check fa-white"></i> Atividade Realizada </a>'
						+ '<a class="btn btn-sm btn-primary" onclick="abrirPopUpAtividadeIniciaticaMembrosParaAssinatura(' + idAtividadeIniciatica + ',' + idOrganismoAfiliado + ');"><i class="fa fa-print fa-white"></i> Imprimir </a>'
                        + '<input type="hidden" name="idAtividadeIniciatica" id="idAtividadeIniciatica" value="' + idAtividadeIniciatica + '">'
                        + '<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>');
				}





            },
            error: function (xhr, textStatus, errorThrown) {
                console.log("Erro na operação! Atualize a página.");
            }
        });
    }

    function incluiMembrosCampoHiddenAtividadeIniciatica(idAtividadeIniciatica){

        //limpar campos
        $('#compareceram').val("");
        $('#naoCompareceram').val("");

        $.ajax({
            type: "post",
            url: "js/ajax/preencheCompareceramOuNaoAtividadeIniciatica.php",
            dataType: "json",
            data: {idAtividadeIniciatica: idAtividadeIniciatica},
            success: function (data) {

            	//alert(data.status);

            	if(data.status==1) {

                    for (i = 0; i < data.compareceu.length; i++) {
                        $("#compareceram").val(data.compareceu[i] + "/" + idAtividadeIniciatica + "@@");
                    }

                    for (i = 0; i < data.outros.length; i++) {
                        $("#compareceram").val(data.outros[i] + "/" + idAtividadeIniciatica + "@@");
                    }

                    for (i = 0; i < data.naoCompareceu.length; i++) {
                        $("#naoCompareceram").val(data.naoCompareceu[i] + "/" + idAtividadeIniciatica + "@@");
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                console.log("Erro na operação! Atualize a página.");
            }
        });
	}

/*
    function incluiMembroAtividadeAgendada(idAtividade){

        var seqCadastMembro = $("#h_seqCadastMembro"+idAtividade).val();

        $.ajax({
            type: "post",
            url: "js/ajax/retornaDisponibilidadeMembroAtividadeIniciatica.php",
            dataType: "json",
            data: {
                seqCadastMembro: seqCadastMembro,
                idAtividade: idAtividade
            },
            success: function (data) {
                console.log("Grau atual: " + data.grauAtual);
                if (data.grauAtual > 0) {
                    console.log("Grau atual: " + data.grauAtual);
                } else {
                    swal({title: "Aviso!", text: "O membro não pode realizar esta atividade iniciatica.", type: "warning", confirmButtonColor: "#1ab394"});
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Aviso!", text: "Erro ao realizar a ação, tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }
*/

    function contar(what){
        return document.querySelectorAll(what).length;
    }

    function atualizarAtividadeEstatutoRealizada(idAtividadeEstatuto,dia,mes,ano){
        //var statusAtividade = $("#codAfiliacaoMembro" + idAtividadeEstatuto).val();

        var numeroVisitantes                = $("#numeroAtualParticipantes" + idAtividadeEstatuto).val();
        var numeroFrequentadores            = $("#numeroAtualFrequentadores" + idAtividadeEstatuto).val();
        var qntOficiaisResponsaveis         = $("#qntOficiaisResponsaveis" + idAtividadeEstatuto).val();

		console.log("numeroVisitantes: " + numeroVisitantes);
		console.log("numeroFrequentadores: " + numeroFrequentadores);

        if((numeroVisitantes <= 0) && (numeroFrequentadores <= 0)){
        	console.log("numeroVisitantes <= 0: " + numeroVisitantes == 0);
			console.log("numeroFrequentadores <= 0: " + numeroFrequentadores <= 0);
			swal({
					title: "Aviso!",
					text: "A atividade está sendo sinalizada como realizada sem registrar nenhum frequentador e visitante.",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#1ab394",
					confirmButtonText: "Sim!",
					cancelButtonText: "Não!",
					closeOnConfirm: true,
					closeOnCancel: true},
				function (isConfirm) {
					if (isConfirm) {

					} else {
						return false;
					}
				});
        }
        if(qntOficiaisResponsaveis <= 0){
            swal({title:"Aviso!",text:"A atividade deve ter ao menos 1 oficial cadastrado como responsável.",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }


        var data = new Date();
        //console.log("Data Atual: " + data.getDate() + "/" + (data.getMonth()+1) + "/" + data.getFullYear());
        //console.log("Data Evento: " + dia + "/" + mes + "/" + ano);
        //console.log("----------------------------------------------------------------------------------------------------");
        //console.log(data.getFullYear() + " menor ou igual a " + ano);
        if(data.getFullYear() <= ano){
            //console.log((data.getMonth()+1) + " menor ou igual a " + mes);
            if((data.getMonth()+1) <= mes){
                //console.log(data.getDate() + " menor que " + dia);
                if(data.getDate() < dia){
                    swal({title:"Aviso!",text:"A atividade ainda não pode ser dada como realizada. Hoje não é a data estipulada.",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
                }
            }
        }
        //console.log("----------------------------------------------------------------------------------------------------");
        //swal({title: "Sucesso!", text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", type: "success", confirmButtonColor: "#1ab394"});

        swal({
                title: "Deseja alterar a atividade para o status de 'Realizada'?",
                text: "Nenhum dado poderá ser alterado posteriormente.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#1ab394",
                confirmButtonText: "Sim!",
                cancelButtonText: "Não!",
                closeOnConfirm: true,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {

                $.ajax({
                    type: "post",
                    url: "js/ajax/alteraStatusAtividadeEstatutoRealizada.php",
                    dataType: "json",
                    data: {idAtividadeEstatuto: idAtividadeEstatuto},
                    success: function (data) {
                        if(data.alterado == true){
                            $("#status" + idAtividadeEstatuto).html('');
                            $("#botaoEditarAtividade" + idAtividadeEstatuto).html('');
                            $("#botaoIncluirOficial" + idAtividadeEstatuto).html('');
                            $("#botaoEdicaoNroMembros" + idAtividadeEstatuto).html('');
                            $("#botaoEdicaoNroMembrosFrequentadores" + idAtividadeEstatuto).html('');
                            $(".botaoOficialResponsavel" + idAtividadeEstatuto).html('--');

                            $("#botaoAtividadeEstatutoRealizada" + idAtividadeEstatuto).html('');
                            $("#botaoAtividadeEstatutoRealizada" + idAtividadeEstatuto).append('<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>');

                            $(".statusTarget" + idAtividadeEstatuto).html('');
                            $(".statusTarget" + idAtividadeEstatuto).append("<span class='badge badge-success'>Realizada</span>");
                        } else {
                            $(".statusTarget" + idAtividadeEstatuto).html('');
                            $(".statusTarget" + idAtividadeEstatuto).append("<span class='badge badge-warning'>Erro</span>");
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        $(".statusTarget" + idAtividadeEstatuto).html('');
                        $(".statusTarget" + idAtividadeEstatuto).append("<span class='badge badge-warning'>Erro</span>");
                    }
                });

                return true;
            } else {
                return false;
            }
        });

    }

    function transferirDadosParaModalSuplente(idEquipe){
        var idAtividadeIniciatica       = $("#atividadeIniciatica" + idEquipe).val();
        $("#idAtividadeIniciaticaModalSuplente").val(idAtividadeIniciatica);
        $("#idEquipeModal").val(idEquipe);
    }

    function getOficialIniciaticoSuplente() {

        var codAfiliacaoSuplente        = $("#codAfiliacaoSuplente").val();
        var nomeSuplente 		        = $("#nomeSuplente").val();
        var tipoSuplente 		        = '1';

        $.ajax({
            type:"post",
            url: "js/ajax/retornaDadosMembro.php",
            dataType: "json",
            data: {codigoAfiliacao: codAfiliacaoSuplente, nomeMembro: nomeSuplente, tipoMembro: tipoSuplente},
            success: function(data){
                //console.log(data);
                if (data.result[0].fields.fNomCliente != "") {
                    $("#nomeSuplente").val(data.result[0].fields.fNomCliente);
                    $("#seqCadastSuplente").val(data.result[0].fields.fSeqCadast);
                    $("#h_seqCadastSuplente").val(data.result[0].fields.fSeqCadast);
                    $("#h_nomeSuplente").val(data.result[0].fields.fNomCliente);

                    if(data.result[0].fields.fSeqCadastCompanheiroRosacruz != 0) {
                        $("#h_seqCadastCompanheiroSuplente").val(data.result[0].fields.fSeqCadastCompanheiroRosacruz);
                        $("#h_nomeCompanheiroSuplente").val(data.result[0].fields.fNomConjuge);
                    } else {
                        $("#h_seqCadastCompanheiroSuplente").val('');
                        $("#h_nomeCompanheiroSuplente").val('');
                    }
                } else {
                    swal({title: "Aviso!", text: "Membro não encontrado!", type: "warning", confirmButtonColor: "#1ab394"});
                    $("#codAfiliacaoSuplente").val('');
                    $("#nomeSuplente").val('');
                    $("#h_seqCadastSuplente").val('');
                    $("#h_nomeSuplente").val('');
                    $("#h_seqCadastCompanheiroSuplente").val('');
                    $("#h_nomeCompanheiroSuplente").val('');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
            }
        });

    }

    function getCompanheiroOficialIniciaticoSuplente() {

        if($('#companheiroSuplente').is(":checked")) {
            if($("#h_nomeCompanheiroSuplente").val()=="") {
                swal({title:"Aviso!",text:"Este membro não possui companheiro",type:"warning",confirmButtonColor:"#1ab394"});
                $('#companheiroSuplente').prop("checked", false);
            } else {
                $("#nomeSuplente").val($("#h_nomeCompanheiroSuplente").val());
                $("#seqCadastSuplente").val($("#h_seqCadastCompanheiroSuplente").val());
            }
        } else {
            $("#nomeSuplente").val($("#h_nomeSuplente").val());
            $("#seqCadastSuplente").val($("#h_seqCadastSuplente").val());
        }

    }

    function adicionarSuplenteAtividadeIniciatica(){

        var idAtividadeIniciatica       = $("#idAtividadeIniciaticaModalSuplente").val();
        var tipoOficialSuplente         = $("#tipoOficialSuplente").val();
        var seqCadastSuplente           = $("#seqCadastSuplente").val();

        var codAfiliacaoSuplente        = $("#codAfiliacaoSuplente").val();
        var nomeSuplente                = $("#nomeSuplente").val();

        var idEquipe                    = $("#idEquipeModal").val();

        if(tipoOficialSuplente == 0){
            swal({title:"Aviso!",text:"Selecione a função.",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }
        if(tipoOficialSuplente == 0){
            swal({title:"Aviso!",text:"Selecione a função.",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }
        if(seqCadastSuplente == null) {
            swal({title:"Aviso!",text:"Preencha todos os campos.",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }
        if(seqCadastSuplente == '') {
            swal({title:"Aviso!",text:"Preencha todos os campos.",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }
        if(seqCadastSuplente == undefined) {
            swal({title:"Aviso!",text:"Preencha todos os campos.",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }
        $.ajax({
            type:"post",
            url: "js/ajax/adicionarSuplenteAtividadeIniciatica.php",
            dataType: "json",
            data: {idAtividadeIniciatica: idAtividadeIniciatica, tipoOficialSuplente: tipoOficialSuplente, seqCadastSuplente: seqCadastSuplente},
            success: function(data){
                //console.log(data);
                if(data.resposta == true){
                    swal({
                        title:"Confirmado!",
                        text:"Suplente cadastrado com sucesso!",
                        type:"success",
                        showCancelButton: false,
                        confirmButtonColor:"#1ab394",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    }, function(){
                        //$("#mestre" + idEquipeOficiais).css('text-decoration','');
                        //$("#mestreAdjunto" + idEquipeOficiais).css('display',none);
                    });
                    switch(tipoOficialSuplente){
                        case '1':
                            var oficiaisNome = 'mestre';
                            break;
                        case '2':
                            var oficiaisNome = 'mestreAux';
                            break;
                        case '3':
                            var oficiaisNome = 'arquivista';
                            break;
                        case '4':
                            var oficiaisNome = 'capelao';
                            break;
                        case '5':
                            var oficiaisNome = 'matre';
                            break;
                        case '6':
                            var oficiaisNome = 'grandeSacerdotisa';
                            break;
                        case '7':
                            var oficiaisNome = 'guia';
                            break;
                        case '8':
                            var oficiaisNome = 'guardiaoInterno';
                            break;
                        case '9':
                            var oficiaisNome = 'guardiaoExterno';
                            break;
                        case '10':
                            var oficiaisNome = 'archote';
                            break;
                        case '11':
                            var oficiaisNome = 'medalhista';
                            break;
                        case '12':
                            var oficiaisNome = 'arauto';
                            break;
                        case '13':
                            var oficiaisNome = 'adjutor';
                            break;
                        case '14':
                            var oficiaisNome = 'sonoplasta';
                            break;
                        case '15':
                            var oficiaisNome = 'columba';
                            break;
                    }
                    if(oficiaisNome != undefined){
                        $("#" + oficiaisNome + idEquipe).css('text-decoration','line-through');
                        $("#" + oficiaisNome + idEquipe).css('padding-bottom','0px');
                        $("#" + oficiaisNome + "Adjunto" + idEquipe).html('');
                        $("#" + oficiaisNome + "Adjunto" + idEquipe).css('display','');
                        $("#" + oficiaisNome + "Adjunto" + idEquipe).append('<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;"><a onclick="deletarOficialSuplente(\'' + oficiaisNome + '\', ' + idEquipe + ')"><i class="fa fa-times"></i></a> [' + codAfiliacaoSuplente + '] ' + nomeSuplente + '</div>');

                        $("#tipoOficialSuplente").val(0)
                        $("#codAfiliacaoSuplente").val('');
                        $("#nomeSuplente").val('');
                        $("#h_seqCadastSuplente").val('');
                        $("#h_nomeSuplente").val('');
                        $("#h_seqCadastCompanheiroSuplente").val('');
                        $("#h_nomeCompanheiroSuplente").val('');

                        $("#mySeeAdicionarSuplente").modal('toggle');
                    } else {
                        swal({title:"Aviso!",text:"Ocorreu um erro na requisição. Atualize a página.",type:"warning",confirmButtonColor:"#1ab394"});
                    }
                } else {
                    swal({title:"Aviso!",text:"Ocorreu um erro na requisição. Atualize a página.",type:"warning",confirmButtonColor:"#1ab394"});
                }
            },
            error: function(xhr, textStatus, errorThrown){
                swal({title:"Aviso!",text:"Ocorreu um erro na requisição. Atualize a página.",type:"warning",confirmButtonColor:"#1ab394"});
            }
        });
    }

    function modificaOficiaisSuplentes(idAtividadeIniciatica, idEquipeOficiais){

        if(idAtividadeIniciatica != null){
            $.ajax({
                type:"post",
                url: "js/ajax/getListaOficiaisSuplentesAtividades.php",
                dataType: "json",
                data: {idAtividadeIniciatica: idAtividadeIniciatica},
                success: function(data){
                    console.log(data);
                    var oficiaisNome = ['mestre','mestreAux','arquivista','capelao','matre','grandeSacerdotisa','guia',
                    'guardiaoInterno','guardiaoExterno','archote','medalhista','arauto','adjutor','sonoplasta','columba'];

                    for(var i=0; i < data.oficiais.length; i++){
                        if(data.oficiais[i] != 0){
                            $("#" + oficiaisNome[i] + "Adjunto" + idEquipeOficiais).html('');
                            $("#" + oficiaisNome[i] + idEquipeOficiais).css('text-decoration','line-through');
                            $("#" + oficiaisNome[i] + idEquipeOficiais).css('padding-bottom','0px');
                            $("#" + oficiaisNome[i] + "Adjunto" + idEquipeOficiais).css('display','');
                            retornaDadosMembroAdjunto(data.oficiais[i], idEquipeOficiais, oficiaisNome[i]);
                        } else {
                            $("#" + oficiaisNome[i] + idEquipeOficiais).css('text-decoration','');
                            $("#" + oficiaisNome[i] + "Adjunto" + idEquipeOficiais).css('display','none');
                            $("#" + oficiaisNome[i] + "Adjunto" + idEquipeOficiais).html('');
                        }
                    }

                    //$("#mestre" + idEquipeOficiais).css('text-decoration','');
                    //$("#mestreAdjunto" + idEquipeOficiais).css('display','none');

                    //$("#mestre" + idEquipeOficiais).css('text-decoration','line-through');
                    //$("#mestreAdjunto" + idEquipeOficiais).css('display','');
                },
                error: function(xhr, textStatus, errorThrown){
                    swal({title:"Aviso!",text:"Ocorreu um erro na requisição. Atualize a página.",type:"warning",confirmButtonColor:"#1ab394"});
                }
            });
        }
    }

    function retornaDadosMembroAdjunto(seqCadast, idEquipeOficiais, oficial) {

    	var codigo ='0';

        $.ajax({
            type:"post",
            url: "js/ajax/retornaDadosMembroPorSeqCadast.php",
            dataType: "json",
            data: {seqCadast: seqCadast},
            success: function(data){
                if (data.result[0].fields.fNomCliente != "") {
                	if(oficial=='columba')
					{
						codigo = data.result[0].fields.fCodOgg;
					}else{
                        codigo = data.result[0].fields.fCodRosacruz;
					}
                    $("#" + oficial + "Adjunto" + idEquipeOficiais).append(
                        '<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">' +
                        '<a onclick="deletarOficialSuplente(\'' + oficial + '\', ' + idEquipeOficiais + ')"><i class="fa fa-times"></i></a> ' +
                        '[' + codigo + '] ' +
                        data.result[0].fields.fNomCliente +
                        '</div>');
                } else {
                    $("#" + oficial + "Adjunto" + idEquipeOficiais).append('<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">Erro! Atualize a página!</div>');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                //swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
                $("#" + oficial + "Adjunto" + idEquipeOficiais).append('<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">Erro! Atualize a página!</div>');
            }
        });

    }

    function deletarOficialSuplente(tipoSuplente, idEquipe){

        var idAtividadeIniciatica = $("#atividadeIniciatica" + idEquipe).val();

        $.ajax({
            type:"post",
            url: "js/ajax/deletarOficialSuplente.php",
            dataType: "json",
            data: {tipoSuplente:tipoSuplente, idAtividadeIniciatica: idAtividadeIniciatica},
            success: function(data){
                if(data.resposta == true){
                    $("#" + tipoSuplente + idEquipe).css('text-decoration','');
                    //$("#" + tipoSuplente + idEquipe).css('padding-bottom','10px');
                    $("#" + tipoSuplente + "Adjunto" + idEquipe).css('display','none');
                    $("#" + tipoSuplente + "Adjunto" + idEquipe).html('');
                } else {
                    $("#" + tipoSuplente + "Adjunto" + idEquipe).html('');
                    $("#" + tipoSuplente + "Adjunto" + idEquipe).append('<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">Erro! Atualize a página!</div>');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                //swal({title:"Aviso!",text:"Ocorreu um erro na requisição!",type:"warning",confirmButtonColor:"#1ab394"});
                $("#" + tipoSuplente + "Adjunto" + idEquipe).html('');
                $("#" + tipoSuplente + "Adjunto" + idEquipe).append('<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">Erro! Atualize a página!</div>');
            }
        });

    }

    function confirmaNavegacaoParaMembrosComPendencia(idOrganismo){
        swal({
                title: "Atenção! Está página tem um tempo prolongado de carregamento.",
                text: "Deseja realmente entrar nela?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, ir para a página!",
                cancelButtonText: "Permanecer na página atual!",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    location.href="?corpo=buscaAtividadeIniciaticaMembro&idOrganismoAfiliado="+idOrganismo;
                    return true;
                } else {
                    return false;
                }
            });
    }

    function abrirPopupRelatorioMensalAtividade(idOrganismoAfiliado, mesAtual, anoAtual, numeroMembrosAtivosAnterior, elevacaoLoja, elevacaoCapitulo, elevacaoPronaos)
    {
        window.open('impressao/relatorioAtividadeMensal.php?mesAtual=' + mesAtual + '&anoAtual=' + anoAtual + '&idOrganismoAfiliado=' + idOrganismoAfiliado + '&numeroMembrosAtivosAnterior=' + numeroMembrosAtivosAnterior+'&elevacaoLoja='+elevacaoLoja+'&elevacaoCapitulo='+elevacaoCapitulo+'&elevacaoPronaos='+elevacaoPronaos, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
    }

    function listaUploadRelatorioAtividadeMensal(mesAtual, anoAtual, idOrganismoAfiliado)
    {
        $.ajax({
            type: "post",
            url: "js/ajax/getListaUploadRelatorioAtividadeMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "html",
            data: {mesAtual: mesAtual, anoAtual: anoAtual, idOrganismoAfiliado: idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (data) {
                $("#listaUpload").html(data);
            },
            error: function (xhr, textStatus, errorThrown) {
                alert('erro');
            }
        });
    }

    function excluirUploadRelatorioAtividadeMensal(idExcluir, mesAtual, anoAtual, idOrganismoAfiliado)
    {
        //var r = confirm("Tem certeza que deseja excluir esse documento?");
        //if(r == true)
        //{
        swal({
                title: "Você tem certeza?",
                text: "Com essa ação você excluirá definitivamente esse arquivo!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, exclua!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: false},
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "post",
                        url: "js/ajax/excluirUploadRelatorioAtividadeMensal.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {id: idExcluir}, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (data) {
                            if (data.status == 1)
                            {
                                swal({title: "Sucesso!", text: "Seu arquivo foi deletado com sucesso.", type: "success", confirmButtonColor: "#1ab394"});
                                listaUploadRelatorioAtividadeMensal(mesAtual, anoAtual, idOrganismoAfiliado);
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('erro');
                        }
                    });
                } else {
                    swal({title: "Cancelado", text: "Nós não excluímos o arquivo :)", type: "error", confirmButtonColor: "#1ab394"});
                }
            });
        //}else{
        //	return false;
        //}
    }

    /**
     * Carrega anexos do imóvel, excluído=0, na página
     */
    function getImovelAnexo(idImovel, idImovelAnexoTipo) {
        $.ajax({
            type:"post",
            url: "js/ajax/getImovelAnexo.php",
            dataType: "json",
            data: {idImovel:idImovel,idImovelAnexoTipo:idImovelAnexoTipo},
            success: function(data){

                $('#listaAnexos'+idImovel+idImovelAnexoTipo).html('');
                if(data.temAnexo!=0) {
                    for (var i = 0; i < data.arquivo.length; i++) {
                        $('#listaAnexos'+idImovel+idImovelAnexoTipo).append(data.arquivo[i].anexo);
                    }
                } else {
                    $('#listaAnexos'+idImovel+idImovelAnexoTipo).append('<center><i>Nenhum anexo cadastrada</i></center>');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $('#listaAnexos'+idImovel+idImovelAnexoTipo).html('');
                $('#listaAnexos'+idImovel+idImovelAnexoTipo).append('<center style="color: #FA8072"><i>Ocorreu um erro ao buscar as imagens publicadas</i></center>');
            }
        });
    }
    /**
     * Exclui anexo, excluído=1, e o retira da página
     */
    function excluiImovelAnexo(id, idImovel, IdImovelAnexoTipo) {
        $.ajax({
            type:"post",
            url: "js/ajax/excluiImovelAnexo.php",
            dataType: "json",
            data: {id:id, idImovel:idImovel},
            success: function(data){
                //console.log(data);
                if (data.sucess == true) {
                    getImovelAnexo(idImovel, IdImovelAnexoTipo);
                }
                else {
                    swal({title:"Aviso!",text:"Ocorreu um erro ao excluir a imagem!",type:"warning",confirmButtonColor:"#1ab394"});
                }
            }
        });
    }

    /**
     * Carrega anexos do imóvel, excluído=0, na página
     */
    function getImovelControleAnexo(idImovelControle, idImovelAnexoTipo) {
        $.ajax({
            type:"post",
            url: "js/ajax/getImovelControleAnexo.php",
            dataType: "json",
            data: {idImovelControle:idImovelControle,idImovelAnexoTipo:idImovelAnexoTipo},
            success: function(data){

                $('#listaAnexos'+idImovelControle+idImovelAnexoTipo).html('');
                if(data.temAnexo!=0) {
                    for (var i = 0; i < data.arquivo.length; i++) {
                        $('#listaAnexos'+idImovelControle+idImovelAnexoTipo).append(data.arquivo[i].anexo);
                    }
                } else {
                    $('#listaAnexos'+idImovelControle+idImovelAnexoTipo).append('<center><i>Nenhum anexo cadastrada</i></center>');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $('#listaAnexos'+idImovelControle+idImovelAnexoTipo).html('');
                $('#listaAnexos'+idImovelControle+idImovelAnexoTipo).append('<center style="color: #FA8072"><i>Ocorreu um erro ao buscar as imagens publicadas</i></center>');
            }
        });
    }
    /**
     * Exclui anexo, excluído=1, e o retira da página
     */
    function excluiImovelControleAnexo(id, idImovelControle, IdImovelAnexoTipo) {
        $.ajax({
            type:"post",
            url: "js/ajax/excluiImovelControleAnexo.php",
            dataType: "json",
            data: {id:id, idImovelControle:idImovelControle},
            success: function(data){
                //console.log(data);
                if (data.sucess == true) {
                    getImovelControleAnexo(idImovelControle, IdImovelAnexoTipo);
                }
                else {
                    swal({title:"Aviso!",text:"Ocorreu um erro ao excluir a imagem!",type:"warning",confirmButtonColor:"#1ab394"});
                }
            }
        });
    }

    function marcaDesmarcaNotificacao() {

        var grupo = $('input[name="notificacoes[]"]');
        if($('#marca_e_desmarca_notificacao').is(":checked")) {
            for(var i=0; i < grupo.length; i++){ grupo[i].checked = true; }
        } else {
            for(var i=0; i < grupo.length; i++){ grupo[i].checked = false; }
        }

    }

    function getNotificacoes(){
        var notificacaoArray = new Array();
        var notificacao = $('input[name="notificacoes[]"]');

        for(var i=0; i < notificacao.length; i++){
            if(notificacao[i].checked == true){
                notificacaoArray.push(notificacao[i].id);
            }
        }

        return notificacaoArray;
    }

    function mudaStatusNotificacaoParaVisualizada(){

        var values              = getNotificacoes();
        var camposGeraisData    = JSON.stringify(values);
        var seqCadast           = $('#seqCadastUsuario').val();
        var status              = 1;

        if(values.length){
            $.ajax({
                type:"post",
                url: "js/ajax/mudaStatusNotificacao.php",
                dataType: "json",
                data: {camposGeraisData:camposGeraisData, seqCadast:seqCadast, status:status},
                success: function(data){
                    if(data.ok){
                        location.href="?corpo=buscaNotificacoes";
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
                }
            });
        } else {
            swal({title:"Aviso!",text:"Deve ser marcado ao menos uma notificação!",type:"warning",confirmButtonColor:"#1ab394"});
        }

    }

    function mudaStatusNotificacaoParaNaoVisualizada(){
        var values              = getNotificacoes();
        var camposGeraisData    = JSON.stringify(values);
        var seqCadast           = $('#seqCadastUsuario').val();
        var status              = 0;

        if(values.length){
            $.ajax({
                type:"post",
                url: "js/ajax/mudaStatusNotificacao.php",
                dataType: "json",
                data: {camposGeraisData:camposGeraisData, seqCadast:seqCadast, status:status},
                success: function(data){
                    if(data.ok){
                        location.href="?corpo=buscaNotificacoes";
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
                }
            });
        } else {
            swal({title:"Aviso!",text:"Deve ser marcado ao menos uma notificação!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    }

    function mudaStatusNotificacao(status,pagina){
        var values              = getNotificacoes();
        var camposGeraisData    = JSON.stringify(values);
        var seqCadast           = $('#seqCadastUsuario').val();

        if(values.length){
            $.ajax({
                type:"post",
                url: "js/ajax/mudaStatusNotificacao.php",
                dataType: "json",
                data: {camposGeraisData:camposGeraisData, seqCadast:seqCadast, status:status},
                success: function(data){
                    if(data.ok){
                        location.href = "?corpo=buscaNotificacoes&pagina="+pagina;
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
                }
            });
        } else {
            swal({title:"Aviso!",text:"Deve ser marcado ao menos uma notificação!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    }

    function gravarStringPesquisa(){

        var pesquisaNotificacao           = $('#pesquisaNotificacao').val();

    }

    function cadastraNovaNotificacao(){

        var fk_seqCadastRemetente           = $('#fk_seqCadastRemetente').val();
        var fk_idTicket                     = $('#fk_idTicket').val();
        var mensagemNotificacao             = $('#mensagemNotificacao').code();
        if((fk_seqCadastRemetente !== "") || (fk_idTicket !== "")){
            if(mensagemNotificacao !== "<p><br></p>"){
        swal({
                title: "Atenção!",
                text: "Deseja realmente enviar a notificação?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type:"post",
                        url: "js/ajax/cadastraNotificacao.php",
                        dataType: "json",
                        data: {fk_seqCadastRemetente:fk_seqCadastRemetente, fk_idTicket:fk_idTicket, mensagemNotificacao:mensagemNotificacao},
                        success: function(data){
                            if(data.sucesso){
                                location.href = "?corpo=buscaTicketDetalhe&d="+fk_idTicket;
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
                        }
                    });
                    return true;
                } else {
                    return false;
                }
            });
            } else {
                swal({title:"Aviso!",text:"A mensagem está em branco!",type:"warning",confirmButtonColor:"#1ab394"});
            }
        } else {
            swal({title:"Aviso!",text:"Ocorreu um erro! Atualize a página.",type:"warning",confirmButtonColor:"#1ab394"});
        }
    }

    /**
     * Carrega anexos do imóvel, excluído=0, na página
     */
    function getTicketAnexo(idTicket) {
        $.ajax({
            type:"post",
            url: "js/ajax/getTicketAnexo.php",
            dataType: "json",
            data: {idTicket:idTicket},
            success: function(data){

                $('#listaAnexos').html('');
                if(data.temAnexo!=0) {
                    for (var i = 0; i < data.arquivo.length; i++) {
                        $('#listaAnexos').append(data.arquivo[i].anexo);
                    }
                } else {
                    $('#listaAnexos').append('<center><i>Nenhum anexo cadastrada</i></center>');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                //console.log('error');
                $('#listaAnexos').html('');
                $('#listaAnexos').append('<center style="color: #FA8072"><i>Ocorreu um erro ao buscar as anexos publicados</i></center>');
            }
        });
    }
    /**
     * Exclui anexo, excluído=1, e o retira da página
     */
    function excluiTicketAnexo(id, idTicket) {
        $.ajax({
            type:"post",
            url: "js/ajax/excluiTicketAnexo.php",
            dataType: "json",
            data: {id:id, idTicket:idTicket},
            success: function(data){
                //console.log(data);
                if (data.sucess == true) {
                    getTicketAnexo(idTicket);
                }
                else {
                    swal({title:"Aviso!",text:"Ocorreu um erro ao excluir o anexo!",type:"warning",confirmButtonColor:"#1ab394"});
                }
            }
        });
    }

    function cancelarPosteriormenteAtividadeIniciatica(idAtividadeIniciatica, organismo, agil){
        swal({
                title: "Atenção! Você deve excluír a obrigação ritualística de cada membro aqui incluso antes de utilizar essa opção.",
                text: "Deseja realmente cancelar a atividade?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "post",
                        url: "js/ajax/cancelarPosteriormenteAtividadeIniciatica.php",
                        dataType: "json",
                        data: {idAtividadeIniciatica: idAtividadeIniciatica},
                        success: function (data) {
                            if (data.sucesso) {
                                var org = organismo!=0 ? "&idOrganismoAfiliado="+organismo : "";
                                if(agil == 1) {
                                    location.href = "?corpo=buscaAtividadeIniciaticaAgil" + org;
                                } else {
                                    location.href = "?corpo=buscaAtividadeIniciatica" + org;
                                }
                            } else {
                                swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                        }
                    });
                    return true;
                } else {
                    return false;
                }
            });
    }

    function cancelarRealizacaoAtividade(idAtividade, mes, ano){
        $.ajax({
            type: "post",
            url: "js/ajax/cancelarRealizacaoAtividade.php",
            dataType: "json",
            data: {idAtividade: idAtividade},
            success: function (data) {
                if (data.sucesso) {
                    mes = mes!=0 ? "&mes="+mes : "";
                    ano = ano!=0 ? "&ano="+ano : "";
                    location.href = "?corpo=buscaAtividadeEstatuto" + mes + ano;
                } else {
                    swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });
    }

    function retornaStatusTicketNome(statusNovo){
        switch (statusNovo){
            case 0: return 'Aberto';break;
            case 1: return 'Em andamento';break;
            case 2: return 'Fechado';break;
            case 3: return 'Indeferido!';break;
        }
    }

    function mudaStatusTicket(idTicket, statusNovo) {

        if ((idTicket === undefined) || (idTicket === null) ||(idTicket === ""))
            return false;
        if ((statusNovo === undefined) || (statusNovo === null) ||(statusNovo === ""))
            return false;

        var mensagemNotificacao                        = "alterou o status do ticket para "+retornaStatusTicketNome(statusNovo);
        var seqCadastAtribuidor                        = $("#fk_seqCadastRemetente").val();

        $.ajax({
            type: "post",
            url: "js/ajax/alteraStatusTicket.php",
            dataType: "json",
            data: {idTicket: idTicket, statusNovo: statusNovo},
            success: function (data) {
                if (data.alterado) {
                    $("#statusTicket").html('');
                    $("#statusTicket").append(data.novoBotao);
                    cadastraNovaNotificacaoAtualizacao(seqCadastAtribuidor,idTicket,mensagemNotificacao);
                } else {
                    swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                swal({title: "Erro!", text: "Tente novamente mais tarde!", type: "warning", confirmButtonColor: "#1ab394"});
            }
        });

    }

    function fixedEncodeURI(str) {
        return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
    }

    function limparFiltroTicket(){
        location.href = "?corpo=buscaTicket";
    }

    function alterarAtribuicaoTicket(seqCadastAtribuido) {

        var seqCadastAtribuidor             = $("#fk_seqCadastRemetente").val();
        var idTicket                        = $("#fk_idTicket").val();
        var nomeAtribuido                   = document.getElementById("atribuicao").options[document.getElementById("atribuicao").selectedIndex].text;
        var mensagemNotificacao             = "atribuiu o Ticket para "+nomeAtribuido;

        $.ajax({
            type: "post",
            url: "js/ajax/alteraAtribuicaoTicket.php",
            dataType: "json",
            data: {seqCadastAtribuido: seqCadastAtribuido, seqCadastAtribuidor: seqCadastAtribuidor, idTicket: idTicket},
            success: function (data) {
                if (data.alterado) {
                    $('#check').show(300);
                    $('#erro').hide(300);
                    cadastraNovaNotificacaoAtualizacao(seqCadastAtribuidor,idTicket,mensagemNotificacao);
                } else {
                    $('#check').hide(300);
                    $('#erro').show(300);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                $('#check').hide(300);
                $('#erro').show(300);
            }
        });

    }

    function cadastraNovaNotificacaoAtualizacao(fk_seqCadastRemetente,fk_idTicket,mensagemNotificacao){
        $.ajax({
            type:"post",
            url: "js/ajax/cadastraNotificacao.php",
            dataType: "json",
            data: {fk_seqCadastRemetente:fk_seqCadastRemetente, fk_idTicket:fk_idTicket, mensagemNotificacao:mensagemNotificacao},
            success: function(data){
                //if(data.sucesso){ location.href = "?corpo=buscaTicketDetalhe&d="+fk_idTicket;}
            },
            error: function(xhr, textStatus, errorThrown){
                swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
            }
        });
    }

    function validaFormImoveis(){
		if($("#fk_idOrganismoAfiliado").val() == 0){
			$("#fk_idOrganismoAfiliado").focus();
			return false;
		}
    	if($("#enderecoImovel").val() == ''){
			$("#enderecoImovel").focus();
			return false;
		}
		if($("#numeroImovel").val() == ''){
			$("#numeroImovel").focus();
			return false;
		}
		if($("#bairroImovel").val() == ''){
			$("#bairroImovel").focus();
			return false;
		}
		if($("#paisImovel").val() == 0){
			$("#paisImovel").focus();
			return false;
		}
		if($("#estadoImovel").val() == 0){
			$("#estadoImovel").focus();
			return false;
		}
		if($("#cidadeImovel").val() == 0){
			$("#cidadeImovel").focus();
			return false;
		}
		if($("#areaTotalImovel").val() == ''){
			$("#cidadeImovel").focus();
			return false;
		}
		if($("#areaConstruidaImovel").val() == ''){
			$("#areaConstruidaImovel").focus();
			return false;
		}
		if($("#propriedadeTipoImovel").val() == ''){
			$("#propriedadeTipoImovel").focus();
			return false;
		}
		if($("#propriedadeStatusImovel").val() == 0){
			$("#propriedadeStatusImovel").focus();
			return false;
		}
	}

	function retornaPeriodicidadeTipoAtividade(idTipoAtividade) {

		$.ajax({
			type:"post",
			url: "js/ajax/retornaPeriodicidadeTipoAtividade.php",
			dataType: "json",
			data: {idTipoAtividade:idTipoAtividade},
			success: function(data){
				console.log(data);
				var periodicidade = "Periodicidade: ";

				if ((data.janeiro == 1) && (data.fevereiro == 1) && (data.marco == 1) && (data.abril == 1)
					&& (data.maio == 1) && (data.junho == 1) && (data.julho == 1) && (data.agosto == 1)
					&& (data.setembro == 1) && (data.outubro == 1) && (data.novembro == 1) && (data.dezembro == 1)){
					periodicidade += "O ano inteiro";
				}
				else {
					if(data.janeiro == 1){ periodicidade += "Janeiro "; }
					if(data.fevereiro == 1){ periodicidade += "Fevereiro "; }
					if(data.marco == 1){ periodicidade += "Março "; }
					if(data.abril == 1){ periodicidade += "Abril "; }
					if(data.maio == 1){ periodicidade += "Maio "; }
					if(data.junho == 1){ periodicidade += "Junho "; }
					if(data.julho == 1){ periodicidade += "Julho "; }
					if(data.agosto == 1){ periodicidade += "Agosto "; }
					if(data.setembro == 1){ periodicidade += "Setembro "; }
					if(data.outubro == 1){ periodicidade += "Outubro "; }
					if(data.novembro == 1){ periodicidade += "Novembro "; }
					if(data.dezembro == 1){ periodicidade += "Dezembro "; }
				}

				$("#spanPeriodicidade").html('');
				$("#spanPeriodicidade").append(periodicidade);


				$("#janeiro").val(data.janeiro);
				$("#fevereiro").val(data.fevereiro);
				$("#marco").val(data.marco);
				$("#abril").val(data.abril);
				$("#maio").val(data.maio);
				$("#junho").val(data.junho);
				$("#julho").val(data.julho);
				$("#agosto").val(data.agosto);
				$("#setembro").val(data.setembro);
				$("#outubro").val(data.outubro);
				$("#novembro").val(data.novembro);
				$("#dezembro").val(data.dezembro);
			},
			error: function(xhr, textStatus, errorThrown){
				//console.log('error');
				$("#spanPeriodicidade").html('');
				$("#spanPeriodicidade").append("Periodicidade: Erro! Atualize a página.");
				$("#janeiro").val(0);
				$("#fevereiro").val(0);
				$("#marco").val(0);
				$("#abril").val(0);
				$("#maio").val(0);
				$("#junho").val(0);
				$("#julho").val(0);
				$("#agosto").val(0);
				$("#setembro").val(0);
				$("#outubro").val(0);
				$("#novembro").val(0);
				$("#dezembro").val(0);
			}
		});
	}

	function verificaCamposPreenchidosAtividadeSubmit(){

		var fk_idTipoAtividadeEstatuto			    = $("#fk_idTipoAtividadeEstatuto").val();
		var localAtividadeEstatuto			        = $("#localAtividadeEstatuto").val();
		var dataAtividadeEstatuto					= $("#dataAtividadeEstatuto").val();
		var horaAtividadeEstatuto					= $("#horaAtividadeEstatuto").val();

		if(fk_idTipoAtividadeEstatuto <= 0){
			swal({title: "Atenção!", text: "Atividade Não foi selecionada", type: "warning", confirmButtonColor: "#1ab394"});
			return false;
		}
		if(localAtividadeEstatuto == ""){
			swal({title: "Atenção!", text: "O local não foi preenchido!", type: "warning", confirmButtonColor: "#1ab394"});
			return false;
		}
		if((dataAtividadeEstatuto == undefined) || (dataAtividadeEstatuto == null) || (dataAtividadeEstatuto == "00/00/0000") || (dataAtividadeEstatuto == "")){
			swal({title: "Atenção!", text: "Data não foi preenchida", type: "warning", confirmButtonColor: "#1ab394"});
			return false;
		}
		if((horaAtividadeEstatuto == '') || (horaAtividadeEstatuto == null) || (horaAtividadeEstatuto == undefined)){
			swal({title: "Atenção!", text: "Hora não foi preenchida", type: "warning", confirmButtonColor: "#1ab394"});
			return false;
		}

		var periodicidade = dataAtividadeEstatuto.slice(3,5);
		//console.log(periodicidade);

		switch (periodicidade){
			case '01':
				if ($("#janeiro").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '02':
				if ($("#fevereiro").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '03':
				if ($("#marco").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '04':
				if ($("#abril").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '05':
				if ($("#maio").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '06':
				if ($("#junho").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '07':
				if ($("#julho").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '08':
				if ($("#agosto").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '09':
				if ($("#setembro").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '10':
				if ($("#outubro").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '11':
				if ($("#novembro").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			case '12':
				if ($("#dezembro").val() == 0){
					swal({title: "Atenção!", text: "Essa atividade não pode ser realizada nesse mês!", type: "warning", confirmButtonColor: "#1ab394"});
					return false;
				}
				break;
			default:
				return false;
		}
	}


	/*
	 $.ajax({
	 type:"post",
	 url: "js/ajax/retornaPeriodicidadeTipoAtividade.php",
	 dataType: "json",
	 data: {idTipoAtividade:fk_idTipoAtividadeEstatuto},
	 success: function(data){
	 console.log(data);
	 },
	 error: function(xhr, textStatus, errorThrown){
	 console.log('error');
	 swal({title: "Atenção!", text: "Ocorreu um erro! Atualize a página.", type: "warning", confirmButtonColor: "#1ab394"});
	 return false;
	 }
	 });
	 */

   $(document).ready(function(){
       var updateOutput = function (e) {
           var list = e.length ? e : $(e.target),
               output = list.data('output');
           if (window.JSON) {
               var lista = window.JSON.stringify(list.nestable('serialize'));
               //output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
               if(Object.keys(list).length !== 0) {
                 $.ajax({
                	 type:"post",
                	 url: "js/ajax/atualizaListaTipoAtividadeOrdem.php",
                	 dataType: "json",
                	 data: { lista: lista },
                   success: function(data){ console.log(data); },
                	 error: function(xhr, textStatus, errorThrown){ console.log('error'); }
              	 });
               }
           } else {
               //output.val('JSON browser support required for this demo.');
           }
       };
       // activate Nestable for list 1
       $('#nestableAtividadeTipo').nestable({ group: 1, maxDepth: 1 }).on('change', updateOutput);
       // output initial serialised data
       updateOutput($('#nestableAtividadeTipo').data('output', $('#nestable-output')));
   });


$(".select2_demo_3").select2({
    placeholder: "Selecione a exibição",
    allowClear: true
});


/**
 * 
 * FUNÇÕES DO MÓDULO
 * MURAL DIGITAL
 * 
 */

/**
 * PUBLICAÇÃO
 */

//var numitens=5; //quantidade de itens a ser mostrado por página
//var pagina=1;	//página inicial

function listaPublicacoesMural(status,pagAtual,qntItens){
	//console.log("listaPublicacoesMural");

	/** Captura dados */
    
	fk_idRegiaoRosacruz 				= $("#fk_idRegiaoRosacruzUsuarioLogado").val();
	fk_idOrganismoAfiliado 				= $("#fk_idOrganismoAfiliadoUsuarioLogado").val();
	seqCadast 							= $("#seqCadastUsuarioLogado").val();
	avatarUsuario 						= $("#avatarUsuarioUsuarioLogado").val();

	/** Captura filtros */

	filtroFavorito 						= $("#filtroFavorito").val();
	filtroPropriasPublicacoes 			= $("#filtroPropriasPublicacoes").val();

	var filtroCategoria = document.getElementById("filtroCategoria"); //.getElementsByTagName("option")
	var filtroCategoriaSelecionada = [];
	for(var i = 0; i < filtroCategoria.length; i++){
		if(filtroCategoria.options[i].selected == true){
			//console.log("Categoria: " + filtroCategoria.options[i].value);
			filtroCategoriaSelecionada.push(filtroCategoria.options[i].value);
		}
	}

	var filtroPublico = document.getElementById("filtroPublico"); //.getElementsByTagName("option")
	var filtroPublicoSelecionado = [];
	for(var i = 0; i < filtroPublico.length; i++){
		if(filtroPublico.options[i].selected == true){
			//console.log("Publico: " + filtroPublico.options[i].value);
			filtroPublicoSelecionado.push(filtroPublico.options[i].value);
		}
	}

	var filtroOrganismo = document.getElementById("filtroOrganismo"); //.getElementsByTagName("option")
	var filtroOrganismosSelecionados = [];
	for(var i = 0; i < filtroOrganismo.length; i++){
		if(filtroOrganismo.options[i].selected == true){
			//console.log("Organismo: " + filtroOrganismo.options[i].value);
			filtroOrganismosSelecionados.push(filtroOrganismo.options[i].value);
		}
	}
/*
        alert('fk_idRegiaoRosacruz'+fk_idRegiaoRosacruz);
        alert('fk_idOrganismoAfiliado'+fk_idOrganismoAfiliado);
        alert('seqCadast'+seqCadast);
        alert('avatarUsuario'+avatarUsuario);
        alert('status'+status);
        alert('pagAtual'+pagAtual);
        alert('qntItens'+qntItens);
        alert('filtroFavorito'+filtroFavorito);
        alert('filtroPropriasPublicacoes'+filtroPropriasPublicacoes);
*/
    $.ajax({
        type:"post",
        url: "js/ajax/listarPublicacoesMural.php",
        dataType: "json",
        data: {
        	fk_idRegiaoRosacruz:fk_idRegiaoRosacruz, 
        	fk_idOrganismoAfiliado:fk_idOrganismoAfiliado, 
        	seqCadast:seqCadast, 
        	avatarUsuario:avatarUsuario, 
        	status:status,
        	pagAtual:pagAtual,
        	qntItens:qntItens,
        	filtroFavorito: filtroFavorito,
        	filtroPropriasPublicacoes: filtroPropriasPublicacoes,
        	filtroCategoriaSelecionada:JSON.stringify(filtroCategoriaSelecionada),
            filtroPublicoSelecionado:JSON.stringify(filtroPublicoSelecionado),
        	filtroOrganismosSelecionados:JSON.stringify(filtroOrganismosSelecionados)
        },
        success: function(data){
        	//alert(data);
        	//console.log("Publicações encontradas: " + data.qtdVetor);
        	//console.log("listaPublicacoesMural: " + JSON.stringify(data));
            if(data.qtdVetor > 0){

            	$(".mural").html('');

            	for (var i = 0; i < data.qtdVetor; i++) {
            		$(".mural").append("<div class='social-feed-box' id='social-feed-box"+data.vetor[i].id+"'></div>");
					$("#social-feed-box"+data.vetor[i].id).append("<div id='social-action"+data.vetor[i].id+"' class='pull-right social-action dropdown'></div>");
					$("#social-feed-box"+data.vetor[i].id).append("<div id='social-avatar"+data.vetor[i].id+"' class='social-avatar'></div>");
					$("#social-feed-box"+data.vetor[i].id).append("<div id='social-body"+data.vetor[i].id+"' class='social-body'></div>");
					$("#social-feed-box"+data.vetor[i].id).append("<div id='social-footer"+data.vetor[i].id+"' class='social-footer'></div>");
					$("#social-action"+data.vetor[i].id).append(data.vetor[i].socialAction);
					$("#social-avatar"+data.vetor[i].id).append(data.vetor[i].socialAvatar);
					$("#social-body"+data.vetor[i].id).append(data.vetor[i].socialBody);
					$("#social-footer"+data.vetor[i].id).append(data.vetor[i].socialFooter);
            	}

            	$('#linhaTracejadaMural').addClass('hr-line-dashed');

            } else {
		    	
	    		$(".mural").html('<center>Nenhuma publicação encontrada!</center>');
	    		$('#linhaTracejadaMural').removeAttr('class');
            }

            contadorMural(status,pagAtual);
        },
        error: function(xhr, textStatus, errorThrown){
            //swal({title:"Aviso!",text:"Ocorreu um erro ao listar, Atualize a página ou tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

function contadorMural(status,pagAtual){
	//console.log("contadorMural");

	fk_idRegiaoRosacruz 				= $("#fk_idRegiaoRosacruzUsuarioLogado").val();
	fk_idOrganismoAfiliado 				= $("#fk_idOrganismoAfiliadoUsuarioLogado").val();

	seqCadast 							= $("#seqCadastUsuarioLogado").val();
	filtroFavorito 						= $("#filtroFavorito").val();
	filtroPropriasPublicacoes 			= $("#filtroPropriasPublicacoes").val();

	var filtroCategoria = document.getElementById("filtroCategoria"); //.getElementsByTagName("option")
	var filtroCategoriaSelecionada = [];
	for(var i = 0; i < filtroCategoria.length; i++){
		if(filtroCategoria.options[i].selected == true){
			//console.log("Categoria: " + filtroCategoria.options[i].value);
			filtroCategoriaSelecionada.push(filtroCategoria.options[i].value);
		}
	}

    var filtroPublico = document.getElementById("filtroPublico"); //.getElementsByTagName("option")
    var filtroPublicoSelecionado = [];
    for(var i = 0; i < filtroPublico.length; i++){
        if(filtroPublico.options[i].selected == true){
            //console.log("Publico: " + filtroPublico.options[i].value);
            filtroPublicoSelecionado.push(filtroPublico.options[i].value);
        }
    }

	var filtroOrganismo = document.getElementById("filtroOrganismo"); //.getElementsByTagName("option")
	var filtroOrganismosSelecionados = [];
	for(var i = 0; i < filtroOrganismo.length; i++){
		if(filtroOrganismo.options[i].selected == true){
			//console.log("Organismo: " + filtroOrganismo.options[i].value);
			filtroOrganismosSelecionados.push(filtroOrganismo.options[i].value);
		}
	}

	$.ajax({
      	type: 'post',
      	url: "js/ajax/retornarQuantidadePublicacaoMural.php",
        dataType: "json",
        data: {
        	fk_idRegiaoRosacruz:fk_idRegiaoRosacruz, 
        	fk_idOrganismoAfiliado:fk_idOrganismoAfiliado,
        	status:status,
        	seqCadast:seqCadast,
        	filtroFavorito:filtroFavorito,
        	filtroPropriasPublicacoes:filtroPropriasPublicacoes,
        	filtroCategoriaSelecionada:JSON.stringify(filtroCategoriaSelecionada),
            filtroPublicoSelecionado:JSON.stringify(filtroPublicoSelecionado),
        	filtroOrganismosSelecionados:JSON.stringify(filtroOrganismosSelecionados)
        },
   		success: function(data){
            //alert(data.qntTotal);
			console.log("qntTotal: " + data.qntTotal);
			console.log("pagAtual: " + pagAtual);
        	paginadorMural(data.qntTotal,pagAtual);
      	}
    })
}

function paginadorMural(cont,pagina){

	var numitens = 5;
		
	if(cont<=numitens){
		
		$('#paginadorMural').html('');
		$('#linhaTracejadaMural').removeAttr('class');
		
	} else {
		
		$('#paginadorMural').html('');

		var qtdpaginas=Math.ceil(cont/numitens);

		if((pagina-5) >= 1){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMural('+(1)+', '+(1)+', '+numitens+');"><i class="fa fa-chevron-left"><i class="fa fa-chevron-left"> </a>');
		}
		if(pagina!=1){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMural('+(1)+', '+(pagina-1)+', '+numitens+');"><i class="fa fa-chevron-left"> </a>');
		}
		for(var i=1;i<=qtdpaginas;i++){
			if(((i-5) < pagina) && (i+5) > pagina){
				if(pagina==i){
					$('#paginadorMural').append('<a class="btn btn-white active" onclick="listaPublicacoesMural('+(1)+', '+i+', '+numitens+');">'+i+'</a>');
				} else {
					$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMural('+(1)+', '+i+', '+numitens+');">'+i+'</a>');
				}
			}
		}
		if(pagina!=qtdpaginas){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMural('+(1)+', '+(pagina+1)+', '+numitens+');"><i class="fa fa-chevron-right"></i> </a>');
		}
		if((pagina+5) <= qtdpaginas){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMural('+(1)+', '+(qtdpaginas)+', '+numitens+');"><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i> </a>');
		}
	}
}

function favoritarPublicacao(idPublicacao,seqCadast){

	console.log("favoritarPublicacao");

	$.ajax({
      	type: 'post',
      	url: "js/ajax/favoritarPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao, 
        	seqCadast:seqCadast
        },
   		success: function(data){
           	//document.getElementById('btnFavorito-'+idPublicacao+'-'+seqCadast).onclick = function() {desfavoritarPublicacao(idPublicacao,seqCadast)};
            $('#btnFavorito-'+idPublicacao+'-'+seqCadast).attr("onclick","desfavoritarPublicacao("+idPublicacao+","+seqCadast+")");
            $('#btnFavorito-'+idPublicacao+'-'+seqCadast).removeAttr('class');
			$('#btnFavorito-'+idPublicacao+'-'+seqCadast).addClass('btn btn-primary btn-xs');
			$('#btnFavorito-'+idPublicacao+'-'+seqCadast).html('');
			$('#btnFavorito-'+idPublicacao+'-'+seqCadast).append('<i class="fa fa-star"></i> Favoritar!');
      	}
    });
}

function desfavoritarPublicacao(idPublicacao,seqCadast) {
	console.log("desfavoritarPublicacao");

	$.ajax({
      	type: 'post',
      	url: "js/ajax/desfavoritarPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao, 
        	seqCadast:seqCadast
        },
   		success: function(data){
           	$('#btnFavorito-'+idPublicacao+'-'+seqCadast).attr("onclick","favoritarPublicacao("+idPublicacao+","+seqCadast+")");
            $('#btnFavorito-'+idPublicacao+'-'+seqCadast).removeAttr('class');
			$('#btnFavorito-'+idPublicacao+'-'+seqCadast).addClass('btn btn-white btn-xs');
			$('#btnFavorito-'+idPublicacao+'-'+seqCadast).html('');
			$('#btnFavorito-'+idPublicacao+'-'+seqCadast).append('<i class="fa fa-star"></i> Favoritar!');
      	}
    });
}

function curtirPublicacao(idPublicacao,seqCadast){
	console.log("curtirPublicacao");

	$.ajax({
      	type: 'post',
      	url: "js/ajax/curtirPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao, 
        	seqCadast:seqCadast
        },
   		success: function(data){
           	//document.getElementById('btnFavorito-'+idPublicacao+'-'+seqCadast).onclick = function() {desfavoritarPublicacao(idPublicacao,seqCadast)};
            $('#btnCurtir-'+idPublicacao+'-'+seqCadast).attr("onclick","descurtirPublicacao("+idPublicacao+","+seqCadast+")");
            $('#btnCurtir-'+idPublicacao+'-'+seqCadast).removeAttr('class');
			$('#btnCurtir-'+idPublicacao+'-'+seqCadast).addClass('btn btn-primary btn-xs');
			$('#btnCurtir-'+idPublicacao+'-'+seqCadast).html('');
			$('#btnCurtir-'+idPublicacao+'-'+seqCadast).append('<i class="fa fa-thumbs-up"></i> Curtir!');

			var novaQntCurtidas = parseInt($('#qntCurtidas-'+idPublicacao+'-'+seqCadast).val()) + 1;
			setaNovaAreaCurtidas(idPublicacao, seqCadast, novaQntCurtidas);
      	}
    });
}

function descurtirPublicacao(idPublicacao,seqCadast) {
	console.log("descurtirPublicacao");

	$.ajax({
      	type: 'post',
      	url: "js/ajax/descurtirPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao, 
        	seqCadast:seqCadast
        },
   		success: function(data){
           	$('#btnCurtir-'+idPublicacao+'-'+seqCadast).attr("onclick","curtirPublicacao("+idPublicacao+","+seqCadast+")");
            $('#btnCurtir-'+idPublicacao+'-'+seqCadast).removeAttr('class');
			$('#btnCurtir-'+idPublicacao+'-'+seqCadast).addClass('btn btn-white btn-xs');
			$('#btnCurtir-'+idPublicacao+'-'+seqCadast).html('');
			$('#btnCurtir-'+idPublicacao+'-'+seqCadast).append('<i class="fa fa-thumbs-up"></i> Curtir!');

			var novaQntCurtidas = parseInt($('#qntCurtidas-'+idPublicacao+'-'+seqCadast).val()) - 1;
			setaNovaAreaCurtidas(idPublicacao, seqCadast, novaQntCurtidas);
      	}
    });
}

function setaNovaAreaCurtidas(idPublicacao, seqCadast, novaQntCurtidas){

	$('#areaQntCurtidas-'+idPublicacao+'-'+seqCadast).html('');
	if(novaQntCurtidas <= 0){
		$('#areaQntCurtidas-'+idPublicacao+'-'+seqCadast).append('<input type="hidden" value="'+novaQntCurtidas+'" id="qntCurtidas-'+idPublicacao+'-'+seqCadast+'" name="qntCurtidas-'+idPublicacao+'-'+seqCadast+'">');
	} else if (novaQntCurtidas == 1){
		$('#areaQntCurtidas-'+idPublicacao+'-'+seqCadast).append('&nbsp;&nbsp;'+novaQntCurtidas+' pessoa já curtiu essa publicação!');
		$('#areaQntCurtidas-'+idPublicacao+'-'+seqCadast).append('<input type="hidden" value="'+novaQntCurtidas+'" id="qntCurtidas-'+idPublicacao+'-'+seqCadast+'" name="qntCurtidas-'+idPublicacao+'-'+seqCadast+'">');
	} else {
		$('#areaQntCurtidas-'+idPublicacao+'-'+seqCadast).append('&nbsp;&nbsp;'+novaQntCurtidas+' pessoas já curtiram essa publicação!');
		$('#areaQntCurtidas-'+idPublicacao+'-'+seqCadast).append('<input type="hidden" value="'+novaQntCurtidas+'" id="qntCurtidas-'+idPublicacao+'-'+seqCadast+'" name="qntCurtidas-'+idPublicacao+'-'+seqCadast+'">');
	}

}

function aplicarFiltroMural(){
	console.log("aplicarFiltroMural");
	listaPublicacoesMural(1,1,5);
}

function limparFiltroMural(){
	console.log("limparFiltroMural");
	var filtroCategoria = document.getElementById("filtroCategoria");
	for(var i = 0; i < filtroCategoria.length; i++){
		filtroCategoria.options[i].selected = false;
	}
	$('#filtroCategoria').val('').trigger('chosen:updated');

    var filtroPublico = document.getElementById("filtroPublico");
    for(var i = 0; i < filtroPublico.length; i++){
        filtroPublico.options[i].selected = false;
    }
    $('#filtroCategoria').val('').trigger('chosen:updated');

	var filtroOrganismo = document.getElementById("filtroOrganismo");
	for(var i = 0; i < filtroOrganismo.length; i++){
		filtroOrganismo.options[i].selected = false;
	}
	$('#filtroOrganismo').val('').trigger('chosen:updated');

	listaPublicacoesMural(1,1,5);
}

function alteraFiltroFavorito(){
	if($("#filtroFavorito").val() == 0){
		$("#filtroFavorito").val(1);
		$('#filtroFavoritoBtn').removeAttr('class');
		$('#filtroFavoritoBtn').addClass('btn btn-primary btn-sm col-lg-12');
		listaPublicacoesMural(1,1,5);
	} else if($("#filtroFavorito").val() == 1){
		$("#filtroFavorito").val(0);
		$('#filtroFavoritoBtn').removeAttr('class');
		$('#filtroFavoritoBtn').addClass('btn btn-white btn-sm col-lg-12');
		listaPublicacoesMural(1,1,5);
	}
}

function alteraFiltroPropriasPublicacoes(){
	if($("#filtroPropriasPublicacoes").val() == 0){
		$("#filtroPropriasPublicacoes").val(1);
		$('#filtroPropriasPublicacoesBtn').removeAttr('class');
		$('#filtroPropriasPublicacoesBtn').addClass('btn btn-primary btn-sm col-lg-12');
		listaPublicacoesMural(1,1,5);
	} else if($("#filtroPropriasPublicacoes").val() == 1){
		$("#filtroPropriasPublicacoes").val(0);
		$('#filtroPropriasPublicacoesBtn').removeAttr('class');
		$('#filtroPropriasPublicacoesBtn').addClass('btn btn-white btn-sm col-lg-12');
		listaPublicacoesMural(1,1,5);
	}
}
/*
function uploadImagemPublicacaoMural(){
	$('#imagemPublicacaoMural').uploadifive('upload',function(){
		cadastraPublicacaoMural();
	});
}
*/

function uploadImagemPublicacaoMural(){

	if(CKEDITOR.instances.editor.getData().length>1750)
	{
        swal({title:"Aviso!",text:"O máximo de caracteres permitidos é 1750!",type:"warning",confirmButtonColor:"#1ab394"});
        return false;
	}
	if($("#imagemCarregada").val() == 0){
		cadastraPublicacaoMural();
	} else {
		if(CKEDITOR.instances.editor.getData() == ""){
			swal({title:"Aviso!",text:"Escreva a mensagem que deseja publicar!",type:"warning",confirmButtonColor:"#1ab394"});
			return false;
		}
		if($("#categoriaPublicacaoMural").val() <= 0){
			swal({title:"Aviso!",text:"Selecione a categoria que se enquadra a publicação!",type:"warning",confirmButtonColor:"#1ab394"});
			return false;
		}
        if($("#publicoPublicacaoMural").val() <= 0){
            swal({title:"Aviso!",text:"Selecione o publico que se enquadra a publicação!",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }
		if($("#exibicaoPublicacaoMural").val() <= 0){
			swal({title:"Aviso!",text:"Selecione quem poderá ver a sua publicação!",type:"warning",confirmButtonColor:"#1ab394"});
			return false;
		}
		$('#imagemPublicacaoMural').uploadifive('upload');
	}
}

function cadastraPublicacaoMural(){

	seqCadast 							= $("#seqCadastUsuarioLogado").val();
	fk_idRegiaoOa 						= $("#fk_idRegiaoRosacruzUsuarioLogado").val();
	fk_idOrganismoAfiliado 				= $("#fk_idOrganismoAfiliadoUsuarioLogado").val();
	avatarUsuario 						= $("#avatarUsuarioUsuarioLogado").val();

	mensagemMural 				= CKEDITOR.instances.editor.getData();
	fk_idMuralCategoria 		= $("#categoriaPublicacaoMural").val();
    fk_idMuralPublico 			= $("#publicoPublicacaoMural").val();
	exibicaoPublicacaoMural 	= $("#exibicaoPublicacaoMural").val();
	fk_idMuralImagem 			= $("#fk_idMuralImagem").val();

	if(mensagemMural == ""){
		swal({title:"Aviso!",text:"Escreva a mensagem que deseja publicar!",type:"warning",confirmButtonColor:"#1ab394"});
		return false;
	}
    if(fk_idMuralCategoria <= 0){
        swal({title:"Aviso!",text:"Selecione a categoria que se enquadra a publicação!",type:"warning",confirmButtonColor:"#1ab394"});
        return false;
    }
	if(fk_idMuralPublico <= 0){
		swal({title:"Aviso!",text:"Selecione o público que se enquadra a publicação!",type:"warning",confirmButtonColor:"#1ab394"});
		return false;
	}
	if(exibicaoPublicacaoMural <= 0){
		swal({title:"Aviso!",text:"Selecione quem poderá ver a sua publicação!",type:"warning",confirmButtonColor:"#1ab394"});
		return false;
	}

	console.log("ID de Imagem cadastrada: " + fk_idMuralImagem);

    $.ajax({
        type:"post",
        url: "js/ajax/cadastraPublicacaoMural.php",
        dataType: "json",
        data: {
        	mensagemMural:mensagemMural, 
        	exibicaoMural:exibicaoPublicacaoMural,
        	fk_idMuralCategoria:fk_idMuralCategoria,
            fk_idMuralPublico:fk_idMuralPublico,
        	seqCadast:seqCadast,
        	fk_idRegiaoOa:fk_idRegiaoOa,
        	fk_idOrganismoAfiliado:fk_idOrganismoAfiliado,
        	fk_idMuralImagem:fk_idMuralImagem
        },
        success: function(data){
        	listaPublicacoesMural(1,1,5);
        	limparFormularioCadastroPublicacao();
        },
        error: function(xhr, textStatus, errorThrown){
            swal({title:"Aviso!",text:"Ocorreu um erro ao cadastrar, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});

            limparFormularioCadastroPublicacao();
        }
    });
}

/*
function diminuirCaracteresMensagemMural(txtCampo, exibicaoCampo, tamanhoMaximo){
	alert('teste braz');
	var conteudo = document.getElementById(txtCampo).innerHTML;
	document.getElementById(exibicaoCampo).innerHTML = (tamanhoMaximo - conteudo.length) + " caracteres restantes!";
}
*/
function limparFormularioCadastroPublicacao(){
	$("#mensagemMural").val('');
	$("#categoriaPublicacaoMural").val(0);
    $("#publicoPublicacaoMural").val(0);
	$("#exibicaoPublicacaoMural").val(0);
	$("#fk_idMuralImagem").val(0);

    CKEDITOR.instances.editor.setData('');

	document.getElementById('caracteresMensagemMural').innerHTML = 1750 + " caracteres restantes!";

	$('#imagemPublicacaoMural').uploadifive('clearQueue');
}

function liberarEdicaoPublicacaoMural(idPublicacao){

	var exibicaoMural 			= $("#exibicaoMural-"+idPublicacao).val();
	var fk_idMuralCategoria 	= $("#fk_idMuralCategoria-"+idPublicacao).val();
    var fk_idMuralPublico	 	= $("#fk_idMuralPublico-"+idPublicacao).val();

	var conteudo = document.getElementById("msgMural-"+idPublicacao).innerHTML;
	$("#msgMural-"+idPublicacao).html('');
	$("#msgMural-"+idPublicacao).append("<textarea id='mensagemMuralEditar"+idPublicacao+"' class='form-control' placeholder='Escreva uma publicação...' style='min-height: 250px'>"+conteudo.trim()+"</textarea><br>");
	
	/**
	 * Gambi foda, estou muito atrasado, me perdoe
	 */
	$("#msgMural-"+idPublicacao).append('<div class="row form-inline"> \
			                                <div class="form-group"> \
			                                    <select id="categoriaPublicacaoMuralEditar'+idPublicacao+'" name="categoriaPublicacaoMural" class="form-control"><!-- select2_demo_3 --></select> \
			                                </div> \
			                                <div class="form-group"> \
			                                 	<select id="publicoPublicacaoMuralEditar'+idPublicacao+'" name="publicoPublicacaoMural" class="form-control"><!-- select2_demo_3 --></select> \
			                                </div> \
			                                <div class="form-group"> \
			                                    <select id="exibicaoPublicacaoMuralEditar'+idPublicacao+'" name="exibicaoPublicacaoMural" class=" form-control"><!-- select2_demo_3 --></select> \
			                                </div> \
			                               </div><br>');

	var valueCategoria00 = fk_idMuralCategoria==0 ? "<option selected value='0'>Categoria</option>" : "<option value='0'>Categoria</option>";
	var valueCategoria01 = fk_idMuralCategoria==1 ? "<option selected value='1'>Atividades</option>" : "<option value='1'>Atividades</option>";
	var valueCategoria02 = fk_idMuralCategoria==2 ? "<option selected value='2'>Eventos</option>" : "<option value='2'>Eventos</option>";
	var valueCategoria03 = fk_idMuralCategoria==3 ? "<option selected value='3'>Notícias</option>" : "<option value='3'>Notícias</option>";

	$("#categoriaPublicacaoMuralEditar"+idPublicacao).append(valueCategoria00);
	$("#categoriaPublicacaoMuralEditar"+idPublicacao).append(valueCategoria01);
	$("#categoriaPublicacaoMuralEditar"+idPublicacao).append(valueCategoria02);
	$("#categoriaPublicacaoMuralEditar"+idPublicacao).append(valueCategoria03);

    var valuePublico00 = fk_idMuralPublico==0 ? "<option selected value='0'>Público</option>" : "<option value='0'>Público</option>";
    var valuePublico01 = fk_idMuralPublico==1 ? "<option selected value='1'>Rosacruz</option>" : "<option value='1'>Rosacruz</option>";
    var valuePublico02 = fk_idMuralPublico==2 ? "<option selected value='2'>Martinista</option>" : "<option value='2'>Martinista</option>";
    var valuePublico03 = fk_idMuralPublico==3 ? "<option selected value='3'>OGG</option>" : "<option value='3'>OGG</option>";

    $("#publicoPublicacaoMuralEditar"+idPublicacao).append(valuePublico00);
    $("#publicoPublicacaoMuralEditar"+idPublicacao).append(valuePublico01);
    $("#publicoPublicacaoMuralEditar"+idPublicacao).append(valuePublico02);
    $("#publicoPublicacaoMuralEditar"+idPublicacao).append(valuePublico03);


	var valueExibicao00 = exibicaoMural==0 ? "<option selected value='0'>Exibido para</option>" : "<option value='0'>Exibido para</option>";
	var valueExibicao01 = exibicaoMural==1 ? "<option selected value='1'>Todos</option>" : "<option value='1'>Todos</option>";
	var valueExibicao02 = exibicaoMural==2 ? "<option selected value='2'>Região</option>" : "<option value='2'>Região</option>";
	var valueExibicao03 = exibicaoMural==3 ? "<option selected value='3'>O.A.</option>" : "<option value='3'>O.A.</option>";
    var valueExibicao04 = exibicaoMural==4 ? "<option selected value='4'>GLP</option>" : "<option value='4'>GLP</option>";

	$("#exibicaoPublicacaoMuralEditar"+idPublicacao).append(valueExibicao00);
	$("#exibicaoPublicacaoMuralEditar"+idPublicacao).append(valueExibicao01);
	$("#exibicaoPublicacaoMuralEditar"+idPublicacao).append(valueExibicao02);
	$("#exibicaoPublicacaoMuralEditar"+idPublicacao).append(valueExibicao03);
    $("#exibicaoPublicacaoMuralEditar"+idPublicacao).append(valueExibicao04);
	
	$("#msgMural-"+idPublicacao).append("<div class='btn-group'> \
											<button data-toggle='modal' class='btn btn-white btn-xs' onclick='abrirModalSubstituirImagemMural("+idPublicacao+");'><i class='fa fa-file-image-o'></i> Incluir/Substituir Imagem</button> \
											<button class='btn btn-white btn-xs' onClick='alterarPublicacaoMural("+idPublicacao+")'><i class='fa fa-save'></i> Salvar</button> \
											<button class='btn btn-white btn-xs' onclick='cancelarAlteracaoPublicacaoMural("+idPublicacao+");'><i class='fa fa-times'></i> Fechar Alteração</button> \
										</div>");

	/**
	 * Alteração da Imagem
	 */
	/*
	$("#imagemPublicacao-"+idPublicacao).html('');
	$("#imagemPublicacao-"+idPublicacao).append('<div id="queueAlterar"></div>');
	$("#imagemPublicacao-"+idPublicacao).append('<input type="file" id="imagemPublicacaoMuralAlterar" name="imagemPublicacaoMuralAlterar">');
	$("#imagemPublicacao-"+idPublicacao).append('<input type="hidden" id="fk_idMuralImagemAlterar" name="fk_idMuralImagemAlterar" value="0">');
	$("#imagemPublicacao-"+idPublicacao).append('<input type="hidden" id="imagemCarregadaAlterar" name="imagemCarregadaAlterar" value="0">');
	*/
}

function abrirModalSubstituirImagemMural(idPublicacao){
	$("#idPublicacaoAlterarImagem").val(idPublicacao);
	$('#imagemPublicacaoMuralAlterar').uploadifive('clearQueue');
	$("#modalSubstituirImagem").modal('show');
}

function uploadImagemPublicacaoMuralalterar(){
	$('#imagemPublicacaoMuralAlterar').uploadifive('upload');
}

function alteraImagemPublicacaoMural(idNovaImagem){
	var idPublicacao = $("#idPublicacaoAlterarImagem").val();

	$("#imagemPublicacao-"+idPublicacao).html('');

	$.ajax({
        type:"post",
        url: "js/ajax/alteraImagemPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao,
        	idNovaImagem:idNovaImagem
        },
        success: function(data){
        	if(data.sucesso == true){
				swal({title:"Atualizado!",text:data.success,type:"success",confirmButtonColor:"#1ab394"});
				$("#imagemPublicacao-"+idPublicacao).append(data.full_path);
				$('#modalSubstituirImagem').modal('toggle');
        	} else {
        		swal({title:"Aviso!", text:data.erro, type:"warning", confirmButtonColor:"#1ab394"});
        	}
        },
        error: function(xhr, textStatus, errorThrown){
            swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });

}

function alterarPublicacaoMural(idPublicacao){

	var mensagemMural 			= document.getElementById("mensagemMuralEditar"+idPublicacao).value;
	var fk_idMuralCategoria 	= $("#categoriaPublicacaoMuralEditar"+idPublicacao).val();
    var fk_idMuralPublico	 	= $("#publicoPublicacaoMuralEditar"+idPublicacao).val();
	var exibicaoMural 			= $("#exibicaoPublicacaoMuralEditar"+idPublicacao).val();

	console.log("mensagemMural: " + mensagemMural);
	console.log("fk_idMuralCategoria: " + fk_idMuralCategoria);
    console.log("fk_idMuralPublico: " + fk_idMuralPublico);
	console.log("exibicaoMural: " + exibicaoMural);

	if(mensagemMural == ""){
		swal({title:"Aviso!",text:"Escreva o conteúdo que deseja substituir pelo anterior!",type:"warning",confirmButtonColor:"#1ab394"});
		return false;
	}
	if(fk_idMuralCategoria <= 0){
		swal({title:"Aviso!",text:"Selecione a categoria que se enquadra a publicação!",type:"warning",confirmButtonColor:"#1ab394"});
		return false;
	}
    if(fk_idMuralPublico <= 0){
        swal({title:"Aviso!",text:"Selecione o publico que se enquadra a publicação!",type:"warning",confirmButtonColor:"#1ab394"});
        return false;
    }
	if(exibicaoMural <= 0){
		swal({title:"Aviso!",text:"Selecione quem poderá ver a sua publicação!",type:"warning",confirmButtonColor:"#1ab394"});
		return false;
	}

    $.ajax({
        type:"post",
        url: "js/ajax/alteraPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao,
        	mensagemMural:mensagemMural, 
        	exibicaoMural:exibicaoMural,
        	fk_idMuralCategoria:fk_idMuralCategoria,
			fk_idMuralPublico:fk_idMuralPublico
        },
        success: function(data){
        	document.getElementById("msgMural-"+idPublicacao).value = '';
        	document.getElementById("msgMural-"+idPublicacao).innerHTML = mensagemMural;


        	var exibicao="";
        	if(exibicaoMural == 4)
			{
                exibicao = "a glp";
			}else if(exibicaoMural == 1){
                exibicao = "todos";
            } else if(exibicaoMural == 2){
                exibicao = "a região";
            } else{
                exibicao = "o organismo";
            }

            var categoria="";
            if(fk_idMuralCategoria == 1){
                categoria = "Atividades";
            } else if(fk_idMuralCategoria == 2){
                categoria = "Eventos";
            } else {
                categoria = "Notícias";
            }

            var publico="";
            if(fk_idMuralPublico == 1){
                publico = "Rosacruz";
            } else if(fk_idMuralPublico == 2){
                publico = "Martinista";
            } else {
                publico = "OGG";
            }

        	//var tagsPublicacao 	= '<li><a><i class="fa fa-tag"></i> Exibido para ' + exibicao + '</a></li> \
			//	                   <li><a><i class="fa fa-tag"></i> ' + categoria + '</a></li> \
			//	                   <li><a><i class="fa fa-tag"></i> ' + $("#fk_idOrganismoAfiliadoUsuarioLogado").val() + '</a></li>';

        	//$("#msgMural-"+idPublicacao).append(mensagemMural);
        	$("#tag-list-exibicao-"+idPublicacao).html('<a><i class="fa fa-tag"></i> Exibido para ' + exibicao + '</a>');
        	$("#tag-list-categoria-"+idPublicacao).html('<a><i class="fa fa-tag"></i> ' + categoria + '</a>');
            $("#tag-list-publico-"+idPublicacao).html('<a><i class="fa fa-tag"></i> ' + publico + '</a>');

        	$("#exibicaoMural-"+idPublicacao).val(exibicaoMural);
        	$("#fk_idMuralCategoria-"+idPublicacao).val(fk_idMuralCategoria);
            $("#fk_idMuralPublico-"+idPublicacao).val(fk_idMuralPublico);
        	$("#msgMuralExibir-"+idPublicacao).val(mensagemMural.trim());
        },
        error: function(xhr, textStatus, errorThrown){
            //swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

function cancelarAlteracaoPublicacaoMural(idPublicacao){

	//console.log("cancelarAlteracaoPublicacaoMural");
	//console.log("idPublicacao: " + idPublicacao);

	document.getElementById("msgMural-"+idPublicacao).value = '';
	document.getElementById("msgMural-"+idPublicacao).innerHTML = $("#msgMuralExibir-"+idPublicacao).val();
}

function alterarStatusPublicacaoMural(idPublicacao, status){

    $.ajax({
        type:"post",
        url: "js/ajax/alterarStatusPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao,
        	status:status
        },
        success: function(data){
        	var el = document.getElementById("social-feed-box"+idPublicacao);
			el.parentNode.removeChild(el);
        },
        error: function(xhr, textStatus, errorThrown){
            //swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

/**
 * COMENTÁRIO
 */

function cadastraComentarioPublicacaoMural(idPublicacao,seqCadast,avatarUsuario){

	mensagemMuralComentario 				= $("#comentario-publicacao-"+idPublicacao).val();

	if(mensagemMuralComentario == ""){
		swal({title:"Aviso!",text:"Escreva a mensagem que deseja publicar!",type:"warning",confirmButtonColor:"#1ab394"});
		return false;
	}

    $.ajax({
        type:"post",
        url: "js/ajax/cadastraComentarioPublicacaoMural.php",
        dataType: "json",
        data: {
        	mensagemMuralComentario:mensagemMuralComentario, 
        	fk_idMural:idPublicacao,
        	seqCadast:seqCadast
        },
        success: function(data){
        	$("#comentario-publicacao-"+idPublicacao).val('');
        	listaComentarioPublicacoesMural(idPublicacao,seqCadast,avatarUsuario);
        },
        error: function(xhr, textStatus, errorThrown){
            //swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

function listaComentarioPublicacoesMural(idPublicacao,seqCadast,avatarUsuario){
    $.ajax({
        type:"post",
        url: "js/ajax/listarComentarioPublicacoesMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao,
        	seqCadast:seqCadast,
        	avatarUsuario:avatarUsuario
        },
        success: function(data){
            //if(data.qtdVetor > 0){
            	$("#social-footer"+idPublicacao).html('');
				$("#social-footer"+idPublicacao).append(data.socialFooter);
            //}
        },
        error: function(xhr, textStatus, errorThrown){
            //swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

/* Mural Admin */

function listaPublicacoesMuralAdmin(pagAtual,qntItens){
	console.log("listaPublicacoesMuralAdmin");
	
	/** Captura dados */
	seqCadast 							= $("#seqCadastUsuarioLogado").val();

	/** Captura filtros */

	var filtroFavorito 						= $("#filtroFavorito").val();

	var filtroRegiao = document.getElementById("filtroRegiao"); //.getElementsByTagName("option")
	var filtroRegioesSelecionados = [];
	for(var i = 0; i < filtroRegiao.length; i++){
		if(filtroRegiao.options[i].selected == true){
			//console.log("Organismo: " + filtroRegiao.options[i].value);
			filtroRegioesSelecionados.push(filtroRegiao.options[i].value);
		}
	}

	var filtroOrganismo = document.getElementById("filtroOrganismo"); //.getElementsByTagName("option")
	var filtroOrganismosSelecionados = [];
	for(var i = 0; i < filtroOrganismo.length; i++){
		if(filtroOrganismo.options[i].selected == true){
			//console.log("Organismo: " + filtroOrganismo.options[i].value);
			filtroOrganismosSelecionados.push(filtroOrganismo.options[i].value);
		}
	}

	var filtroCategoria = document.getElementById("filtroCategoria"); //.getElementsByTagName("option")
	var filtroCategoriaSelecionada = [];
	for(var i = 0; i < filtroCategoria.length; i++){
		if(filtroCategoria.options[i].selected == true){
			//console.log("Categoria: " + filtroCategoria.options[i].value);
			filtroCategoriaSelecionada.push(filtroCategoria.options[i].value);
		}
	}

    var filtroPublico = document.getElementById("filtroPublico"); //.getElementsByTagName("option")
    var filtroPublicoSelecionado = [];
    for(var i = 0; i < filtroPublico.length; i++){
        if(filtroPublico.options[i].selected == true){
            //console.log("Publico: " + filtroPublico.options[i].value);
            filtroPublicoSelecionado.push(filtroPublico.options[i].value);
        }
    }

    $.ajax({
        type:"post",
        url: "js/ajax/listarPublicacoesMuralAdmin.php",
        dataType: "json",
        data: { 
        	seqCadast:seqCadast, 
        	pagAtual:pagAtual,
        	qntItens:qntItens,
        	filtroRegioesSelecionados:JSON.stringify(filtroRegioesSelecionados),
        	filtroOrganismosSelecionados:JSON.stringify(filtroOrganismosSelecionados),
        	filtroCategoriaSelecionada:JSON.stringify(filtroCategoriaSelecionada),
            filtroPublicoSelecionado:JSON.stringify(filtroPublicoSelecionado),
        	filtroFavorito:filtroFavorito
        },
        success: function(data){

        	if(data.qtdVetor > 0){

            	$(".muralAdmin").html('');

            	for (var i = 0; i < data.qtdVetor; i++) {
            		$(".muralAdmin").append(data.vetor[i].line);
            	}

            	$('#linhaTracejadaMural').addClass('hr-line-dashed');

            } else {
		    	
	    		$(".muralAdmin").html('<center>Nenhuma publicação encontrada!</center>');
	    		$('#linhaTracejadaMural').removeAttr('class');
            }
        	
        	contadorMuralAdmin(pagAtual);
        },
        error: function(xhr, textStatus, errorThrown){
            //swal({title:"Aviso!",text:"Ocorreu um erro ao listar, Atualize a página ou tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

function contadorMuralAdmin(pagAtual){
	console.log("contadorMuralAdmin");

	var filtroRegiao = document.getElementById("filtroRegiao"); //.getElementsByTagName("option")
	var filtroRegioesSelecionados = [];
	for(var i = 0; i < filtroRegiao.length; i++){
		if(filtroRegiao.options[i].selected == true){
			//console.log("Organismo: " + filtroRegiao.options[i].value);
			filtroRegioesSelecionados.push(filtroRegiao.options[i].value);
		}
	}

	var filtroOrganismo = document.getElementById("filtroOrganismo"); //.getElementsByTagName("option")
	var filtroOrganismosSelecionados = [];
	for(var i = 0; i < filtroOrganismo.length; i++){
		if(filtroOrganismo.options[i].selected == true){
			//console.log("Organismo: " + filtroOrganismo.options[i].value);
			filtroOrganismosSelecionados.push(filtroOrganismo.options[i].value);
		}
	}

	var filtroCategoria = document.getElementById("filtroCategoria"); //.getElementsByTagName("option")
	var filtroCategoriaSelecionada = [];
	for(var i = 0; i < filtroCategoria.length; i++){
		if(filtroCategoria.options[i].selected == true){
			//console.log("Categoria: " + filtroCategoria.options[i].value);
			filtroCategoriaSelecionada.push(filtroCategoria.options[i].value);
		}
	}

    var filtroPublico = document.getElementById("filtroPublico"); //.getElementsByTagName("option")
    var filtroPublicoSelecionado = [];
    for(var i = 0; i < filtroPublico.length; i++){
        if(filtroPublico.options[i].selected == true){
            //console.log("Publico: " + filtroPublico.options[i].value);
            filtroPUblicoSelecionado.push(filtroPublico.options[i].value);
        }
    }

	var filtroFavorito 						= $("#filtroFavorito").val();

	$.ajax({
      	type: 'post',
      	url: "js/ajax/retornarQuantidadePublicacaoMuralAdmin.php",
        dataType: "json",
        data: {
        	filtroRegioesSelecionados:JSON.stringify(filtroRegioesSelecionados),
        	filtroOrganismosSelecionados:JSON.stringify(filtroOrganismosSelecionados),
        	filtroCategoriaSelecionada:JSON.stringify(filtroCategoriaSelecionada),
            filtroPublicoSelecionado:JSON.stringify(filtroPublicoSelecionado),
        	filtroFavorito:filtroFavorito
        },
   		success: function(data){
			//console.log("qntTotal: " + data.qntTotal);
			//console.log("pagAtual: " + pagAtual);
        	paginadorMuralAdmin(data.qntTotal,pagAtual);
      	}
    })
}

function paginadorMuralAdmin(cont,pagina){

	var numitens = 150;
		
	if(cont<=numitens){
		
		$('#paginadorMural').html('');
		$('#linhaTracejadaMural').removeAttr('class');
		
	} else {
		
		$('#paginadorMural').html('');

		var qtdpaginas=Math.ceil(cont/numitens);

		if((pagina-5) >= 1){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMuralAdmin('+(1)+', '+numitens+');"><i class="fa fa-chevron-left"><i class="fa fa-chevron-left"> </a>');
		}
		if(pagina!=1){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMuralAdmin('+(pagina-1)+', '+numitens+');"><i class="fa fa-chevron-left"> </a>');
		}
		for(var i=1;i<=qtdpaginas;i++){
			if(((i-5) < pagina) && (i+5) > pagina){
				if(pagina==i){
					$('#paginadorMural').append('<a class="btn btn-white active" onclick="listaPublicacoesMuralAdmin('+i+', '+numitens+');">'+i+'</a>');
				} else {
					$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMuralAdmin('+i+', '+numitens+');">'+i+'</a>');
				}
			}
		}
		if(pagina!=qtdpaginas){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMuralAdmin('+(pagina+1)+', '+numitens+');"><i class="fa fa-chevron-right"></i> </a>');
		}
		if((pagina+5) <= qtdpaginas){
			$('#paginadorMural').append('<a class="btn btn-white" onclick="listaPublicacoesMuralAdmin('+(qtdpaginas)+', '+numitens+');"><i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i> </a>');
		}
	}
}

function aplicarFiltroMuralAdmin(){
	console.log("aplicarFiltroMuralAdmin");
	listaPublicacoesMuralAdmin(1,150);
}

function limparFiltroMuralAdmin(){
	console.log("limparFiltroMuralAdmin");

	var filtroRegiao = document.getElementById("filtroRegiao");
	for(var i = 0; i < filtroRegiao.length; i++){
		filtroRegiao.options[i].selected = false;
	}
	$('#filtroRegiao').val('').trigger('chosen:updated');

	var filtroOrganismo = document.getElementById("filtroOrganismo");
	for(var i = 0; i < filtroOrganismo.length; i++){
		filtroOrganismo.options[i].selected = false;
	}
	$('#filtroOrganismo').val('').trigger('chosen:updated');

	var filtroCategoria = document.getElementById("filtroCategoria");
	for(var i = 0; i < filtroCategoria.length; i++){
		filtroCategoria.options[i].selected = false;
	}
	$('#filtroCategoria').val('').trigger('chosen:updated');

	listaPublicacoesMuralAdmin(1,150);
}

function alterarStatusPublicacaoMuralAdmin(idPublicacao, status){

    $.ajax({
        type:"post",
        url: "js/ajax/alterarStatusPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao,
        	status:status
        },
        success: function(data){
        	if(status == 0){
        		// span
        		$('#status-'+idPublicacao).html('');
        		$('#status-'+idPublicacao).append("<span class='label'>Inativa</span>");

        		// btn
        		$('#btnExclusao-'+idPublicacao).attr("onclick","alterarStatusPublicacaoMuralAdmin("+idPublicacao+",1)");
            	$('#btnExclusao-'+idPublicacao).removeAttr('class');
				$('#btnExclusao-'+idPublicacao).addClass('btn btn-default btn-circle');
				//$('#btnExclusao-'+idPublicacao).html('');
        		//$('#btnExclusao-'+idPublicacao).append('<i class="fa fa-check"></i>');
        	} else {
				// span
        		$('#status-'+idPublicacao).html('');
        		$('#status-'+idPublicacao).append("<span class='label label-primary'>Lida</span>");

        		// btn
        		$('#btnExclusao-'+idPublicacao).attr("onclick","alterarStatusPublicacaoMuralAdmin("+idPublicacao+",0)");
            	$('#btnExclusao-'+idPublicacao).removeAttr('class');
				$('#btnExclusao-'+idPublicacao).addClass('btn btn-info btn-circle');
				//$('#btnExclusao-'+idPublicacao).html('');
        		//$('#btnExclusao-'+idPublicacao).append('<i class="fa fa-times"></i>');
        	}

        },
        error: function(xhr, textStatus, errorThrown){
            //swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

function favoritarPublicacaoAdmin(idPublicacao,seqCadast){
	$.ajax({
      	type: 'post',
      	url: "js/ajax/favoritarPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao, 
        	seqCadast:seqCadast
        },
   		success: function(data){
            $('#btnFavorito-'+idPublicacao).attr("onclick","desfavoritarPublicacaoAdmin("+idPublicacao+","+seqCadast+")");
            $('#btnFavorito-'+idPublicacao).removeAttr('class');
			$('#btnFavorito-'+idPublicacao).addClass('btn btn-danger btn-circle');
      	}
    });
}

function desfavoritarPublicacaoAdmin(idPublicacao,seqCadast) {
	$.ajax({
      	type: 'post',
      	url: "js/ajax/desfavoritarPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao, 
        	seqCadast:seqCadast
        },
   		success: function(data){
           	$('#btnFavorito-'+idPublicacao).attr("onclick","favoritarPublicacaoAdmin("+idPublicacao+","+seqCadast+")");
            $('#btnFavorito-'+idPublicacao).removeAttr('class');
			$('#btnFavorito-'+idPublicacao).addClass('btn btn-default btn-circle');
      	}
    });
}

function abrirModalPublicacaoMural(idPublicacao){
	
	$("#idPublicacaoModal").val(idPublicacao);
	$("#modalPublicacaoCompleta").modal('show');

	var seqCadastUsuarioLogado = $("#seqCadastUsuarioLogado").val();

	$('#status-'+idPublicacao).html('');
	$('#status-'+idPublicacao).append("<span class='label label-primary'>Lida</span>");
	
	$.ajax({
		type:"post",
        url: "js/ajax/buscarPublicacaoMural.php",
        dataType: "json",
        data: {
        	idPublicacao:idPublicacao,
        	seqCadastUsuarioLogado:seqCadastUsuarioLogado
        },
        success: function(data){

			//alert(data);
        	$("#header-titulo").html('');
        	$("#header-titulo").append(data.headerTitulo);
        	$("#header-dados").html('');
        	$("#header-dados").append(data.headerDados);

        	$("#publicacao-avatar").html('');
        	$("#publicacao-avatar").append(data.publicacaoAvatar);
        	$("#publicacao-nome").html('');
        	$("#publicacao-nome").append(data.publicacaoNome);


        	$("#publicacao-data").html('');
        	$("#publicacao-data").append(data.publicacaoData);
        	$("#publicacao-mensagem").html('');
        	$("#publicacao-mensagem").append(data.publicacaoMensagem);
        	$("#publicacao-imagem").html('');
        	$("#publicacao-imagem").append(data.publicacaoImagem);

        	$("#comentarios").html('');
			$("#comentarios").append(data.comentario);

			$("#btnsPublicacaoModal").html('');
			$("#btnsPublicacaoModal").append(data.btnFavorito);
			$("#btnsPublicacaoModal").append(data.btnExclusao);
			$("#btnsPublicacaoModal").append('<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>');



        	$("#modalPublicacaoCompleta").modal('show');
        },
        error: function(xhr, textStatus, errorThrown){
            swal({title:"Aviso!",text:"Ocorreu um erro, atualize a página ou tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
	});
	
}

function favoritarPublicacaoAdminModal(){
	var idPublicacao = $("#idPublicacaoModal").val();
	var seqCadast = $("#seqCadastUsuarioLogado").val();

	if(idPublicacao != 0) {
		favoritarPublicacaoAdmin(idPublicacao, seqCadast);
		console.log("favoritarPublicacaoAdminModal");
		$("#btnFavoritoModal").text('Favoritar');
		$('#btnFavoritoModal').attr("onclick","desfavoritarPublicacaoAdminModal()");
	}
}

function desfavoritarPublicacaoAdminModal() {
    var idPublicacao = $("#idPublicacaoModal").val();
	var seqCadast = $("#seqCadastUsuarioLogado").val();

	if(idPublicacao != 0) {
		desfavoritarPublicacaoAdmin(idPublicacao, seqCadast);
		console.log("desfavoritarPublicacaoAdminModal");
		$("#btnFavoritoModal").text('Favoritar');
		$('#btnFavoritoModal').attr("onclick","favoritarPublicacaoAdminModal()");
	}
}

function alterarStatusPublicacaoMuralAdminModal(status){

	var idPublicacao = $("#idPublicacaoModal").val();

	alterarStatusPublicacaoMuralAdmin(idPublicacao, status);

	if(status == 0){
		$("#btnExclusaoModal").text('Ativar');
		$('#btnExclusaoModal').attr("onclick","alterarStatusPublicacaoMuralAdminModal(1)");
	} else {
		$("#btnExclusaoModal").text('Inativar');
		$('#btnExclusaoModal').attr("onclick","alterarStatusPublicacaoMuralAdminModal(0)");
	}
}

function alterarStatusComentarioPublicacaoMuralAdmin(idComentario, status){
	$.ajax({
        type:"post",
        url: "js/ajax/alterarStatusComentarioPublicacaoMural.php",
        dataType: "json",
        data: {
        	idComentario:idComentario,
        	status:status
        },
        success: function(data){
        	if(data.sucesso == true){
	        	if(status == 0){
					$("#btnExclusaoComentario-"+idComentario).text('Ativar');
					$("#btnExclusaoComentario-"+idComentario).attr("onclick","alterarStatusComentarioPublicacaoMuralAdmin("+idComentario+", 1)");
					$("#btnExclusaoComentario-"+idComentario).removeAttr('class');
					$("#btnExclusaoComentario-"+idComentario).addClass('btn btn-primary btn-xs');
					$("#msg-comentario-"+idComentario).css('text-decoration','line-through');
				} else {
					$("#btnExclusaoComentario-"+idComentario).text('Inativar');
					$("#btnExclusaoComentario-"+idComentario).attr("onclick","alterarStatusComentarioPublicacaoMuralAdmin("+idComentario+", 0)");
					$("#btnExclusaoComentario-"+idComentario).removeAttr('class');
					$("#btnExclusaoComentario-"+idComentario).addClass('btn btn-danger btn-xs');
					$("#msg-comentario-"+idComentario).css('text-decoration','');
				}
        	} else {
        		swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        	}
        },
        error: function(xhr, textStatus, errorThrown){
            swal({title:"Aviso!",text:"Ocorreu um erro, tente novamente mais tarde!",type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

function alteraFiltroFavoritoAdmin(){
	if($("#filtroFavorito").val() == 0){
		$("#filtroFavorito").val(1);
		$('#filtroFavoritoBtn').removeAttr('class');
		$('#filtroFavoritoBtn').addClass('btn btn-primary btn-sm col-lg-12');
	} else if($("#filtroFavorito").val() == 1){
		$("#filtroFavorito").val(0);
		$('#filtroFavoritoBtn').removeAttr('class');
		$('#filtroFavoritoBtn').addClass('btn btn-white btn-sm col-lg-12');
	}
	listaPublicacoesMuralAdmin(1,150);
}

function selecionaTipoSuplente(id)
{
	if(id==15)
	{
        $("#tipo").val(2);
	}else{
        $("#tipo").val(1);
	}
}