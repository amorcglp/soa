function onEnter(evt)
{
    var key_code = evt.keyCode ? evt.keyCode :
            evt.charCode ? evt.charCode :
            evt.which ? evt.which : void 0;


    if (key_code == 13)
    {
        return true;
    }
}

function mudaStatus(id, status, classe)
{
    $.ajax({
        type: "post",
        url: "js/ajax/alteraStatus.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id, status: status, classe: classe}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if (data.status == 0)
            {
                $("#status" + id).html("<a class=\"btn btn-sm btn-danger\" onclick=\"mudaStatus('" + id + "','0','" + classe + "')\" data-rel=\"tooltip\" title=\"Inativar\">Inativar</a>");
                $("#statusTarget" + id).html("<span class='badge badge-primary'>Ativo</span>");
                $(".statusTarget" + id).html("<span class='badge badge-primary'>Ativo</span>");

            } else {
                $("#status" + id).html("<a class=\"btn btn-sm btn-primary\" onclick=\"mudaStatus('" + id + "','1','" + classe + "')\" data-rel=\"tooltip\" title=\"Ativar\">Ativar</a>");
                $("#statusTarget" + id).html("<span class='badge badge-danger'>Inativo</span>");
                $(".statusTarget" + id).html("<span class='badge badge-danger'>Inativo</span>");
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}


function mudaStatusFaq(id, status, classe)
{
    $.ajax({
        type: "post",
        url: "js/ajax/alteraStatus.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id, status: status, classe: classe}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if (data.status == 0)
            {
                $("#status" + id).html("<a class=\"btn btn-danger btn-xs\" onclick=\"mudaStatusFaq('" + id + "','0','" + classe + "')\" data-rel=\"tooltip\" title=\"Respondida\">Não Respondida</a>");
                $("#statusTarget" + id).html("<span class='btn btn-primary btn-xs'>Respondida</span>");
                $(".statusTarget" + id).html("<span class='btn btn-primary btn-xs'>Respondida</span>");

            } else {
                $("#status" + id).html("<a class=\"btn btn-primary btn-xs\" onclick=\"mudaStatusFaq('" + id + "','1','" + classe + "')\" data-rel=\"tooltip\" title=\"Não Respondida\">Respondida</a>");
                $("#statusTarget" + id).html("<span class='btn btn-danger btn-xs'>Não Respondida</span>");
                $(".statusTarget" + id).html("<span class='btn btn-danger btn-xs'>Não Respondida</span>");
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });

}

function retornaDados()
{
	
    $('#loadingAjax').show();
	
    var codigoAfiliacao = $("#codigoAfiliacaoUsuario").val();
    var nomeMembro = $("#nomeUsuario").val();
    var tipoMembro = '1';
    var filtro = 'externo';
    //console.log("retornaDados() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro: filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fSeqCadast);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosOGG(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente.replace("´", ""));
                $("#emailUsuario").val(data.fields.fDesEmail);
                $("#siglaPais").val(data.fields.fSigPaisEndereco);
                if(data.fields.fSeqCadast!="0")
                {
                	setInterval(function(){
      			      // method to be executed;
            			$("#loadingAjax").hide();
      			    },1500);
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                    
                }	
                
            }
            
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro retorna dados');
        }
    });
}
function retornaDadosOGG(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '2';
    var filtro = 'externo';
    //console.log("retornaDadosOGG() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosNaoMembro(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente);
                $("#emailUsuario").val(data.fields.fDesEmail);
                if(data.fields.fSeqCadast!="0")
                {
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                	
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}
function retornaDadosNaoMembro(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '4';
    var filtro = 'externo';
    
    //console.log("retornaDadosNaoMembro() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro: filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosCliente(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente);
                $("#emailUsuario").val(data.fields.fDesEmail);
                if(data.fields.fSeqCadast!="0")
                {
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                	
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}
function retornaDadosCliente(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '5';
    var filtro = 'externo';
    //console.log("retornaDadosCliente() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosTom(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente);
                $("#emailUsuario").val(data.fields.fDesEmail);
                if(data.fields.fSeqCadast!="0")
                {
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                	
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}
function retornaDadosTom(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '6';
    var filtro = 'externo';
    
    //console.log("retornaDadosTom() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosAssinante(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente);
                $("#emailUsuario").val(data.fields.fDesEmail);
                if(data.fields.fSeqCadast!="0")
                {
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                	
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}
function retornaDadosAssinante(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '7';
    var filtro = 'externo';
    
    //console.log("retornaDadosAssinante() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosAma(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente);
                $("#emailUsuario").val(data.fields.fDesEmail);
                if(data.fields.fSeqCadast!="0")
                {
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                	
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}
function retornaDadosAma(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '8';
    var filtro = 'externo';
    
    //console.log("retornaDadosAma() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosAssinanteCultural(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente);
                $("#emailUsuario").val(data.fields.fDesEmail);
                if(data.fields.fSeqCadast!="0")
                {
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                	
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}
function retornaDadosAssinanteCultural(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '9';
    var filtro = 'externo';
    
    //console.log("retornaDadosAssinanteCultural() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            if (data.fields.fSeqCadast == 0) {
                retornaDadosAssinanteOrcj(codigoAfiliacao, nomeMembro);
            } else {
                $("#seq_cadast").val(data.fields.fSeqCadast);
                $("#nomeUsuario").val(data.fields.fNomCliente);
                $("#emailUsuario").val(data.fields.fDesEmail);
                if(data.fields.fSeqCadast!="0")
                {
                	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
                	
                }
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}
function retornaDadosAssinanteOrcj(codigoAfiliacao, nomeMembro)
{
    var tipoMembro = '13';
    var filtro = 'externo';
    
    //console.log("retornaDadosAssinanteOrcj() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro: nomeMembro, tipoMembro: tipoMembro, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);			
            $("#seq_cadast").val(data.fields.fSeqCadast);
            $("#nomeUsuario").val(data.fields.fNomCliente);
            $("#emailUsuario").val(data.fields.fDesEmail);
            if(data.fields.fSeqCadast!="0")
            {
            	verificaSeJaCadastradoNoSistema(data.fields.fSeqCadast);
            	
            }else{
            	alert('Usuário não encontrado no sistema interno. Por favor verifique se o nome e o código de afiliação foram corretamente digitados!')
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function validaEmail(email)
{
    var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (!filter.test(email)) {
        return false;
    } else {
        return true;
    }
}

function geraLoginUsuario()
{
    var nome = $("#nomeUsuario").val();

    $.ajax({
        type: "post",
        url: "js/ajax/geraLoginUsuario.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {nome_completo: nome}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            $("#loginUsuario").val(data.username);
            $("#senhaUsuario").val(data.senha);
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function cadastraUsuario()
{
    var nomeUsuario = $("#nomeUsuario").val();
    var codigoAfiliacaoUsuario = $("#codigoAfiliacaoUsuario").val();
    var telefoneResidencialUsuario = $("#telefoneResidencialUsuario").val();
    var celularUsuario = $("#celularUsuario").val();
    var telefoneComercialUsuario = $("#telefoneComercialUsuario").val();
    var emailUsuario = $("#emailUsuario").val();
    var loginUsuario = $("#loginUsuario").val();
    var senhaUsuario = $("#senhaUsuario").val();
    var seq_cadast = $("#seq_cadast").val();
    var regiaoUsuario = $("#regiaoUsuario").val();
    var siglaOA = $("#siglaOA").val();
    var fk_idDepartamento = 1;//Cadastrar no departamento 1- Organismos Afiliados

    //Se o total de funções for 0 cadastrar membro como inativo
    if ($("#totalFuncoes").val() == 0)
    {
        var inativo = 1;//Usuario inativo
    } else {
        var inativo = 0;
    }

    $.ajax({
        type: "post",
        url: "js/ajax/cadastraUsuario.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {nomeUsuario: nomeUsuario,
            codigoAfiliacaoUsuario: codigoAfiliacaoUsuario,
            telefoneResidencialUsuario: telefoneResidencialUsuario,
            celularUsuario: celularUsuario,
            telefoneComercialUsuario: telefoneComercialUsuario,
            emailUsuario: emailUsuario,
            loginUsuario: loginUsuario,
            senhaUsuario: senhaUsuario,
            seq_cadast: seq_cadast,
            regiaoUsuario: regiaoUsuario,
            siglaOA: siglaOA,
            inativo: inativo,
            fk_idDepartamento: fk_idDepartamento}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	//alert(retorno);
                   
            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function verificaSeJaCadastradoNoSistema(seq_cadast)
{
    $.ajax({
        type: "post",
        url: "js/ajax/verificaSeJaCadastradoNoSistema.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seq_cadast: seq_cadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data.result[0].fields.fDesMensag);
            $("#jaCadastrado").val(data.status);
            retornaDadosFuncao(seq_cadast);
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro verifica se ja cadastrado no sistema');
        }
    });
}

function esqueciSenha()
{
    //var loginUsuario = $("#loginUsuario").val();
    var emailUsuario = $("#emailUsuario").val();
    var token = $("#token").val();
    var verificaKey = $("#verificaKey").val();
    
    $("#carregando").css("display", "block");
    $("#carregando").toggleClass("container");
    $("#svg").height(30);

    $.ajax({
        type: "post",
        url: "js/ajax/esqueciSenha.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {emailUsuario: emailUsuario, token:token, verificaKey:verificaKey}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //alert(data);
            // Mostrar mensagem de sucesso
            if(data.status=="1")
            {    
                swal({title:"Sucesso!",text:"Foram enviadas instruções para o seu e-mail para você prosseguir com a recuperação dos seus dados",type:"success",confirmButtonColor:"#1ab394"});
            }else{
                swal({title:"Aviso!",text:"Usuario não encontrado! Tente novamente.",type:"warning",confirmButtonColor:"#1ab394"});
            }
            
            $("#carregando").css("display", "none");
      },
        error: function (xhr, textStatus, errorThrown) {
            $("#carregando").css("display", "none");
            alert('Erro. Contacte a equipe de TI pelo email ti@amorc.org.br');
            $("#emailUsuario").val(emailUsuario);
        }
    });
}

function alterarSenha()
{
    var novaSenha = $("#novaSenha").val();
    var token = $("#t").val();

   
    $.ajax({
        type: "post",
        url: "js/ajax/alterarSenha.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {novaSenha: novaSenha, token: token}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            if(data.status=="1")
            {    
        	swal({title:"Sucesso!",text:"Senha alterada com sucesso!",type:"success",confirmButtonColor:"#1ab394"});
            }else{
                swal({title:"Aviso!",text:"Um erro ocorreu ao alterar a senha. Avise o setor de TI!",type:"warning",confirmButtonColor:"#1ab394"});
            }
            //location.href = 'login.php';
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function carregaCombo(parametro, idCampoOpcao, selecionaOpcao)
{
    if (selecionaOpcao == null)
    {
        selecionaOpcao = 0;
    }
    
    $.ajax({
        type: "post",
        url: "js/ajax/carregaCombo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {idCampoOpcao: idCampoOpcao, parametro: parametro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
        	
            $("#" + idCampoOpcao).html('');
            $("#" + idCampoOpcao).append(data);
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function carregaPermissoes(departamento, funcao)
{

    if (departamento != null)
    {
        var sel = document.getElementById("fk_idDepartamento").options[document.getElementById("fk_idDepartamento").selectedIndex].text;

        if (sel == 'Organismos Afiliados')
        {
            carregaFuncoes(departamento, 'fk_idFuncao');
        } else {
            $("#d_funcao").hide(300);
        }
        funcao = 0;//Agora a permissão não é para o subgrupo e sim para o grupo
    } else {

        departamento = 0;//Agora a permissão não é para o grupo e sim para o subgrupo

    }

    $.ajax({
        type: "post",
        url: "js/ajax/carregaPermissoes.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",
        data: {departamento: departamento, funcao: funcao}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {

            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            $("#permissoes").html(retorno);
            $("#permissoes").show(300);
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissao(opcao)
{

    var departamento = $("#fk_idDepartamento").val();

    if ($('#opt' + opcao).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;

    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {departamento: departamento, opcao: opcao, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissaoObjetoDashboard(objeto_dashboard)
{
    var departamento = $("#fk_idDepartamento").val();

    if ($('#optObjetoDashboard' + objeto_dashboard).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;

    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissaoObjetoDashboard.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {departamento: departamento, objeto_dashboard: objeto_dashboard, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {

            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissaoNivel(nivel)
{
    var departamento = $("#fk_idDepartamento").val();

    if ($('#optNivel' + nivel).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;

    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissaoNivelDePermissao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {departamento: departamento, nivel: nivel, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissaoSubopcao(subopcao)
{

    var departamento = $("#fk_idDepartamento").val();

    if ($('#optSub' + subopcao).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;
    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissaoOpcaoSubMenu.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {departamento: departamento, subopcao: subopcao, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissaoFuncao(opcao)
{
    var funcao = $("#fk_idFuncao").val();

    if ($('#opt' + opcao).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;

    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissaoFuncao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {funcao: funcao, opcao: opcao, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissaoObjetoDashboardFuncao(objeto_dashboard)
{
    var funcao = $("#fk_idFuncao").val();

    if ($('#optObjetoDashboard' + objeto_dashboard).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;

    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissaoObjetoDashboardFuncao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {funcao: funcao, objeto_dashboard: objeto_dashboard, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {

            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissaoNivelFuncao(nivel)
{
    var funcao = $("#fk_idFuncao").val();

    if ($('#optNivel' + nivel).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;

    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissaoNivelDePermissaoFuncao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {funcao: funcao, nivel: nivel, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function concederPermissaoSubopcaoFuncao(subopcao)
{

    var funcao = $("#fk_idFuncao").val();

    if ($('#optSub' + subopcao).is(':checked'))
    {
        var salvar = 1;
    } else {
        var salvar = 0;
    }

    $.ajax({
        type: "post",
        url: "js/ajax/concedePermissaoOpcaoSubMenuFuncao.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {funcao: funcao, subopcao: subopcao, salvar: salvar}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
            // Verifica se ocorreu algum erro
            if (retorno.erro != undefined && retorno.erro.length > 0) {
                var erro = 'Ocorreram um ou mais erros:\n\n';
                for (var i = 0; i < retorno.erro.length; i++) {
                    erro += retorno.erro[i] + '\n';
                }
                alert(erro);
            }
            //alert('PermissÃƒÂ£o concedida com sucesso!');

      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function carregaFuncoes(departamento, id_campo) {

    if (departamento != '' && departamento != undefined) {
        $.ajax({
            type: "post",
            url: "js/ajax/carregaCombo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "html",
            data: {parametro: departamento, idCampoOpcao: id_campo}, // exemplo: {codigo:125, nome:"Joaozinho"}  
            success: function (data) {

                $("#" + id_campo).html('');
                $("#" + id_campo).append(data);

                //$("#"+id_campo).val(selecionaSubgrupo);
                $("#d_funcao").show(300);
                $("#permissoes").hide(300);

          },
            error: function (xhr, textStatus, errorThrown) {
                alert('erro');
            }
        });
    }
}

function enviaMensagemChat(e)
{
    if (onEnter(e))
    {
        cadastraMensagemChat($('#msg').val(), $('#from').val(), $('#para').val());
        $('#msg').val('');
        $('#msg').focus('');
        return false;
    }
    else
    {
        return true;
    }
}

function cadastraMensagemChat(msg, from, para)
{

    if (msg != '' && msg != undefined)
    {
        $.ajax({
            type: "post",
            url: "js/ajax/cadastraMensagemChat.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {msg: msg, from: from, para: para},
            success: function (retorno) {

                getMensagemChat(from, para);
          },
            error: function (xhr, textStatus, errorThrown) {
                alert('Erro do ajax');
            }
        });

    }
}

function getMensagemChat(from, para)
{
    if (from != '' && from != undefined)
    {
        $.ajax({
            type: "post",
            url: "js/ajax/getMensagemChat.php",
            dataType: "html",
            data: {from: from, para: para},
            success: function (retorno) {
                $("#messagens").html('');
                $("#messagens").html(retorno);
          },
            error: function (xhr, textStatus, errorThrown) {
                //alert('Erro do ajax get mensagem chat');
            }
        });

    }
}

function getUsuariosOnlineChat(seqCadast,para)
{
    
    $.ajax({
        type: "post",
        url: "js/ajax/getUsuariosOnlineChat.php",
        dataType: "html",
        data: {seqCadast:seqCadast,para:para},
        success: function (retorno) {
            $("#listaUsuarios").html('');
            $("#listaUsuarios").html(retorno);
      },
        error: function (xhr, textStatus, errorThrown) {
            //alert('Erro do ajax usuarios online');
        }
    });

    
}

function enviaMensagemChatBotao()
{
    cadastraMensagemChat($('#msg').val(), $('#from').val(), $('#para').val());
    $('#msg').val('');
    $('#msg').focus('');
}

function retornaDadosFuncao(seq_cadast)
{
    var filtro = 'externo';
    //seq_cadast = ''
    //console.log("Entrou: retornaDadosFuncao() com seqCadast: " + seq_cadast);
    $.ajax({
        type: "post",
        url: "js/ajax/retornaFuncaoMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seq_cadast: seq_cadast, filtro:filtro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            //console.log("retornaRegiaoMembro("+data.fields.fArrayOficiais[0].fields.fSigOrgafi.substr(0, 3)+")");
            //console.log("Entrou success: retornaDadosFuncao()");
            if (data.fields.fQtdOficiais != 0) {
                retornaRegiaoMembro(data.fields.fArrayOficiais[0].fields.fSigOrgafi.substr(0, 3));
            } else {
                $("#regiaoUsuario").val('18');
            }
            verificarPermissaoMembro(seq_cadast);
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro retorna dados funcao');
        }
    });
}

function retornaRegiaoMembro(regiao)
{
    //console.log("Entrou: retornaDadosFuncao()");
    $.ajax({
        type: "post",
        url: "js/ajax/retornaRegiaoMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {regiao: regiao}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            console.log(data.regiao);
            if (data.regiao != '') {
                $("#regiaoUsuario").val(data.regiao);
            } else {
                $("#regiaoUsuario").val('18');
            }
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro');
        }
    });
}

function verificarPermissaoMembro(seq_cadast)
{
	var siglaPais = $("#siglaPais").val();
    $.ajax({
        type: "post",
        url: "js/ajax/verificarPermissaoMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seq_cadast: seq_cadast,siglaPais:siglaPais}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {
            $("#totalFuncoes").val(data.total);
            $("#siglaOA").val(data.siglaOA);
      },
        error: function (xhr, textStatus, errorThrown) {
            alert('erro ao contar quantas funcoes o membro possui');
        }
    });
}

function setarPermissaoMembro()
{
    var seq_cadast = $("#seq_cadast").val();

    $.ajax({
        type: "post",
        url: "js/ajax/setarPermissaoMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {seq_cadast: seq_cadast}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (data) {

      },
        error: function (xhr, textStatus, errorThrown) {
            //alert('erro ao setar permissao');
        }
    });
}

function SomenteNumero(e) {
    var tecla = (window.event) ? event.keyCode : e.which;
    if ((tecla > 47 && tecla < 58))
        return true;
    else {
        if (tecla == 8 || tecla == 0)
            return true;
        else
            return false;
    }
}

function abrirPopUpAtaReuniaoMensal(idAta)
{
    window.open('impressao/ataReuniaoMensal.php?idAta=' + idAta, 'page', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=800,height=550');
}

function selecionaTudoMultipleSelect(campo) {

    var selecionados = document.getElementById(campo);

    for (i = 0; i <= selecionados.length - 1; i++) {

        selecionados.options[i].selected = true;

    }
}

function validaData(stringData, campo)
{
    /******** VALIDA DATA NO FORMATO DD/MM/AAAA *******/

    var regExpCaracter = /[^\d]/;     //Expressão regular para procurar caracter não-numérico.
    var regExpEspaco = /^\s+|\s+$/g;  //Expressão regular para retirar espaços em branco.

    if (stringData.length != 10)
    {
        swal({title:"Aviso!",text:"Data fora do padrão DD/MM/AAAA",type:"warning",confirmButtonColor:"#1ab394"});
        return false;
    }

    splitData = stringData.split('/');

    if (splitData.length != 3)
    {
        swal({title:"Aviso!",text:"Data fora do padrão DD/MM/AAAA",type:"warning",confirmButtonColor:"#1ab394"});
        return false;
    }

    /* Retira os espaços em branco do início e fim de cada string. */
    splitData[0] = splitData[0].replace(regExpEspaco, '');
    splitData[1] = splitData[1].replace(regExpEspaco, '');
    splitData[2] = splitData[2].replace(regExpEspaco, '');

    if ((splitData[0].length != 2) || (splitData[1].length != 2) || (splitData[2].length != 4))
    {
        swal({title:"Aviso!",text:"Data fora do padrão DD/MM/AAAA",type:"warning",confirmButtonColor:"#1ab394"});
        return false;
    }

    /* Procura por caracter não-numérico. EX.: o "x" em "28/09/2x11" */
    if (regExpCaracter.test(splitData[0]) || regExpCaracter.test(splitData[1]) || regExpCaracter.test(splitData[2]))
    {
        //alert('Caracter inválido encontrado!');
        return false;
    }

    dia = parseInt(splitData[0], 10);
    mes = parseInt(splitData[1], 10) - 1; //O JavaScript representa o mês de 0 a 11 (0->janeiro, 1->fevereiro... 11->dezembro)
    ano = parseInt(splitData[2], 10);

    var novaData = new Date(ano, mes, dia);

    /* O JavaScript aceita criar datas com, por exemplo, mês=14, porém a cada 12 meses mais um ano é acrescentado à data
     final e o restante representa o mês. O mesmo ocorre para os dias, sendo maior que o número de dias do mês em
     questão o JavaScript o converterá para meses/anos.
     Por exemplo, a data 28/14/2011 (que seria o comando "new Date(2011,13,28)", pois o mês é representado de 0 a 11)
     o JavaScript converterá para 28/02/2012.
     Dessa forma, se o dia, mês ou ano da data resultante do comando "new Date()" for diferente do dia, mês e ano da
     data que está sendo testada esta data é inválida. */
    if ((novaData.getDate() != dia) || (novaData.getMonth() != mes) || (novaData.getFullYear() != ano))
    {
        swal({title:"Aviso!",text:"Data Inválida!",type:"warning",confirmButtonColor:"#1ab394"});
        campo.focus();
        return false;
    }
    else
    {
        return true;
    }
}

function validaHora(campo) {
    hrs = (campo.value.substring(0, 2));
    min = (campo.value.substring(3, 5));
    estado = "";
    if ((hrs < 00) || (hrs > 23) || (min < 00) || (min > 59)) {
        estado = "errada";
    }

    if (campo.value == "") {
        estado = "errada";
    }
    if (estado == "errada") {
    	swal({title:"Aviso!",text:"Hora Inválida!",type:"warning",confirmButtonColor:"#1ab394"});
        campo.focus();
    }
}

function excluirCadastro(tipoObjeto, id) {
    if (window.confirm("Excluir " + tipoObjeto + "?")) {
        window.location = 'models/acaoExcluir.php?tipoObjeto=' + tipoObjeto + '&id=' + id + '';

    }

}

function deleteRow(id){

    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        $("#load").html("<img src='img/loading_trans.gif' width='70'>");

        swal({
                title: "Você tem certeza?",
                text: "Que deseja excluir esse item?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, exclua!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    var pote = $("#pote").val();
                    var total = parseInt(pote) - 1;
                    $("#pote").val(total);

                    $.ajax({
                        type: "post",
                        url: "js/ajax/excluirRecebimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (retorno) {

                            //alert("retorno:"+retorno);

                            $("#load").html("");

                            $('#linha' + id).remove();
                            swal({
                                title: "Sucesso!",
                                text: "Item excluído com sucesso!",
                                type: "success",
                                confirmButtonColor: "#1ab394"
                            });
                            $("#proximoId").val(retorno.proximo_id);

                            /** Função trazida para cá após remoção da chamada de atualizaSaldoOrganismo() */
                            atualizaTotalGeralRecebimento($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                            var usuario = $("#usuario").val();

                            //alert('usuario='+usuario);

                            //alert('Vai atualizar saldo');

                            //Atualização do Saldo - Acionando trabalho a parte
                            //atualizaSaldoOrganismo(mes,ano,fk_idOrganismoAfiliado,idIgnorarRecebimento,valorRecebimento,idIgnorarDespesa,valorDespesa,usuario,ultimoId,categoriaEntrada,idRefDocumentoFirebase)
                            // atualizaSaldoOrganismo(
                            //     $('#mesAtual').val(),
                            //     $('#anoAtual').val(),
                            //     $("#fk_idOrganismoAfiliado").val(),
                            //     null,
                            //     null,
                            //     null,
                            //     null,
                            //     usuario,
                            //     null,
                            //     'recebimento',
                            //     null
                            // );

                            //Atualizar total geral
                            //atualizaTotalGeralRecebimento($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                            return false;
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                } else {
                    swal({
                        title: "Cancelado",
                        text: "Nós não excluímos esse item :)",
                        type: "error",
                        confirmButtonColor: "#1ab394"
                    });
                }
            });
    }
}

function addRow(proximo_id){
	proximo_id = $("#proximoId").val();
	//alert('proximo id:'+proximo_id);
	if(parseInt($("#adicionar").val())<1)
  	{
		var idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
		var pote = $("#pote").val();
	  	var total = parseInt(pote)+1;
	  	$("#pote").val(total);
	  	
	  	var adicionar = $("#adicionar").val();
	  	var total2 = parseInt(adicionar)+1;
	  	$("#adicionar").val(total2);
	  	
	  	if($("#linhaAberta").val()!=0)
		{
			//editarRecebimento($("#linhaAberta").val());
		}
  	
	  	//Setar linha aberta para zero
	  	$("#linhaAberta").val(0);
  	
	  	if(total==1)
	    {
	    	document.getElementById("tabela").innerHTML ='';	
	    }
	  	
	  	// Obtendo o elemento tabela
	    var tab       = document.getElementById('tabela');
	    
	    // colocando borda na tabela:
	    tab.style.border = '1px solid black';
	    
	    // Obtendo a quantidade de linhas da tabela:
	    var qntLinhas = tab.rows.length;
	    
	    // Inserindo uma linha
	    var novaLinha = tab.insertRow( qntLinhas );
	    
	    // Escolhendo um id para evitar IDs duplicados
	    //var novaID = Math.floor((Math.random()*99999)+1);
	    // E definindo seu id
	    novaLinha.id  = 'linha'+proximo_id;
	    
	    // Como nosso elemento novaLinha eh a linha que acabamos de criar
	    // podemos adicionar celulas (colunas) no mesmo.
	    
	    // Criando a primeira coluna
	    var coluna1 = novaLinha.insertCell(0);
	    coluna1.id  = '1coluna'+proximo_id;
	    // Criando a segunda coluna
	    var coluna2 = novaLinha.insertCell(1);
	    coluna2.id  = '2coluna'+proximo_id;
	    // Criando a terceira coluna
	    var coluna3 = novaLinha.insertCell(2);
	    coluna3.id  = '3coluna'+proximo_id;
	    // Criando a quarta coluna
	    var coluna4 = novaLinha.insertCell(3);
	    coluna4.id  = '4coluna'+proximo_id;
	    // Criando a quinta coluna
	    var coluna5 = novaLinha.insertCell(4);
	    coluna5.id  = '5coluna'+proximo_id;
	    // Criando a sexta coluna
	    var coluna6 = novaLinha.insertCell(5);
	    coluna6.id  = '6coluna'+proximo_id;
	    // Criando a setima coluna
	    var coluna7 = novaLinha.insertCell(6);
	    
	    // Agora vamos criar os elementos que estarão dentro de cada coluna
	    coluna1.innerHTML = '<div id=\"1col'+proximo_id+'\"><input name=\"dataRecebimento'+proximo_id+'"\" class=\"mascaraData\" style=\"width:94px\" maxlength=\"10\" id=\"dataRecebimento'+proximo_id+'"\" type=\"text\"></div>';
	    coluna2.innerHTML = '<div id=\"2col'+proximo_id+'\"><input type=\"text\" name=\"descricaoRecebimento'+proximo_id+'"\" style=\"width:110px\"  id=\"descricaoRecebimento'+proximo_id+'"\"></div>';
	    coluna3.innerHTML = '<div id=\"3col'+proximo_id+'\"><input type=\"text\" name=\"recebemosDe'+proximo_id+'\" style=\"width:110px\" id=\"recebemosDe'+proximo_id+'\"></div>';
	    coluna4.innerHTML = '<div id=\"4col'+proximo_id+'\" style=\"text-align:center\"><input type=\"text\" name=\"codigoAfiliacao'+proximo_id+'\" style=\"width:100px\" id=\"codigoAfiliacao'+proximo_id+'\"></div>';
	    coluna5.innerHTML = '<div id=\"5col'+proximo_id+'\"><input type=\"text\" name=\"valorRecebimento'+proximo_id+'\" class=\"currency\" style=\"width:84px\" onpaste=\"return false;\" id=\"valorRecebimento'+proximo_id+'\"></div>';
	    coluna6.innerHTML = '<div id=\"6col'+proximo_id+'\"><select name=\"categoriaRecebimento'+proximo_id+'\" id=\"categoriaRecebimento'+proximo_id+'\"><option value=\"0\">Selecione...</option><option value=\"5\">Atividades Sociais</option><option value=\"3\">Bazar</option><option value=\"4\">Comissões</option><option value=\"7\">Construção</option><option value=\"8\">Convenções</option><option value=\"2\">Donativos e Lei de AMRA</option><option value=\"14\">GLP Outros Valores</option><option value=\"13\">GLP Suprimentos</option><option value=\"12\">GLP Trimestralidades</option><option value=\"10\">Jornadas</option><option value=\"15\">Mensalidade Atrium Martinista</option><option value=\"1\">Mensalidade do Organismo</option><option value=\"6\">Recebimentos Diversos</option><option value=\"9\">Receitas Financeiras</option><option value=\"16\">Região</option><option value=\"11\">Suprimentos do Organismo</option></select></div>';
        coluna7.innerHTML = '<div style=\'width:200\'><span id=\"botaoEditar'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"cadastrarRecebimento(\''+proximo_id+'\');\"> Salvar</a></span> <span id=\"outros_botoes'+proximo_id+'\" style=\"display:none\"><span id=\"botaoExcluir'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRow(\''+proximo_id+'\');\"> Excluir</a></span> <span id=\"botaoRecibo'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-warning\' onclick=\"abrirPopUpReciboRecebimento(\''+proximo_id+'\',\''+idOrganismoAfiliado+'\');\"> Recibo</a></span> </span></div>';
	    //coluna7.innerHTML = '<div style=\'width:200\'><span id=\"botaoEditar'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"cadastrarRecebimento(\''+proximo_id+'\');\"> Salvar</a></span> <span id=\"outros_botoes'+proximo_id+'\" style=\"display:none\"><span id=\"botaoExcluir'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRow(\''+proximo_id+'\');\"> Excluir</a></span> <span id=\"botaoRecibo'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-warning\' onclick=\"abrirPopUpReciboRecebimento(\''+proximo_id+'\',\''+idOrganismoAfiliado+'\');\"> Recibo</a></span> <span id=\"botaoUpload'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-warning\' onclick="document.getElementById(\'idRecebimentoUpload\').value="'+proximo_id+'"; data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload"> <icon class="fa fa-paperclip"></icon></a></span>  <span id=\"botaoUploadLista'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-warning\' onclick=\"listaUploadRecebimento(\''+proximo_id+'\');\" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"> <icon class="fa fa-clipboard"></icon></a></span></span></div>';
	    
	    $(".mascaraData").mask("99/99/9999");
	    $(".currency").maskMoney({symbol:'R$ ', 
	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true, allowZero: true});
	    
	    document.getElementById('dataRecebimento'+proximo_id).focus();

  	}else{
  		swal({title:"Aviso!",text:"Você já tem uma linha aberta. Cadastre essa entrada para acrescentar mais linhas.",type:"warning",confirmButtonColor:"#1ab394"});
  	}
}

function cadastrarRecebimento(id)
{
    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        $("#load").html("<img src='img/loading_trans.gif' width='70'>");
        //alert('chamada no cadastro id:'+id);

        var dataRecebimento = $("#dataRecebimento" + id).val();
        var descricaoRecebimento = $("#descricaoRecebimento" + id).val();
        var recebemosDe = $("#recebemosDe" + id).val();
        var codigoAfiliacao = $("#codigoAfiliacao" + id).val();
        var valorRecebimento = $("#valorRecebimento" + id).val();
        var categoriaRecebimento = $("#categoriaRecebimento" + id).val();
        var usuario = $("#usuario").val();
        var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();

        var mesAtual = $("#mesAtual").val();
        var anoAtual = $("#anoAtual").val();
        var dataPreenchido = $("#dataRecebimento" + id).val();
        var mesPreenchido = dataPreenchido.substr(3, 2);
        var anoPreenchido = dataPreenchido.substr(6, 4);

        //Validação
        if (mesAtual != mesPreenchido) {
            swal({
                title: "Aviso!",
                text: "O mês da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (anoAtual != anoPreenchido) {
            swal({
                title: "Aviso!",
                text: "O ano da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        if (dataRecebimento == "") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher uma data de recebimento!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (dataRecebimento == "00/00/0000") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher a data de recebimento corretamente! Não use 00/00/0000!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        //alert(valorRecebimento);
        if (valorRecebimento == "") {
            //swal({title:"Aviso!",text:"É preciso preencher o valor do recebimento!",type:"warning",confirmButtonColor:"#1ab394"});
            //return false;
            valorRecebimento = "0,00";
        }

        if (categoriaRecebimento == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar uma categoria!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        $.ajax({
            type: "post",
            url: "js/ajax/validaRecebimentos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {
                dataRecebimento: dataRecebimento,
                fk_idOrganismoAfiliado: fk_idOrganismoAfiliado,
                recebemosDe: recebemosDe,
                valorRecebimento: valorRecebimento
            }, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno2) {
                if (retorno2.status == false) {

                    $.ajax({
                        type: "post",
                        url: "js/ajax/cadastraRecebimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {
                            dataRecebimento: dataRecebimento,
                            descricaoRecebimento: descricaoRecebimento,
                            recebemosDe: recebemosDe,
                            codigoAfiliacao: codigoAfiliacao,
                            valorRecebimento: valorRecebimento,
                            categoriaRecebimento: categoriaRecebimento,
                            usuario: usuario,
                            fk_idOrganismoAfiliado: fk_idOrganismoAfiliado
                        }, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (retorno) {
                            //alert("==>"+retorno);

                            $("#load").html("");

                            $("#1col" + id).html(dataRecebimento);
                            $("#2col" + id).html(descricaoRecebimento);
                            $("#3col" + id).html(recebemosDe);
                            if (codigoAfiliacao == "") {
                                $("#4col" + id).html("--");
                            } else {
                                $("#4col" + id).html(codigoAfiliacao);
                            }
                            $("#5col" + id).html(valorRecebimento);
                            $("#6col" + id).html(retorno.categoriaRecebimento);
                            $("#proximoId").val(parseInt(retorno.id)+1);
                            $("#adicionar").val(0);
                            $("#outros_botoes" + id).show();

                            $('#1td' + id).click(function () {
                                abriCamposEditarRecebimento(id);
                            });
                            $('#2td' + id).click(function () {
                                abriCamposEditarRecebimento(id);
                            });
                            $('#3td' + id).click(function () {
                                abriCamposEditarRecebimento(id);
                            });
                            $('#4td' + id).click(function () {
                                abriCamposEditarRecebimento(id);
                            });
                            $('#5td' + id).click(function () {
                                abriCamposEditarRecebimento(id);
                            });
                            $('#6td' + id).click(function () {
                                abriCamposEditarRecebimento(id);
                            });

                            $("#botaoEditar" + id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarRecebimento(\'" + retorno.ultimoId + "\');\"> Editar</a>");
                            $("#botaoExcluir" + id).html("<a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRow(\'" + retorno.ultimoId + "\');\"> Excluir</a>");
                            $("#botaoRecibo" + id).html("<a href=\"#\" class=\'btn btn-xs btn-warning\' onclick=\"abrirPopUpReciboRecebimento(\'" + retorno.ultimoId + "\',\'" + fk_idOrganismoAfiliado + "\');\"> Recibo</a>");

                            /** Função trazida para cá após remoção da chamada de atualizaSaldoOrganismo() */
                            atualizaTotalGeralRecebimento($('#mesAtual').val(), $('#anoAtual').val(), fk_idOrganismoAfiliado);

                            //alert('Vai atualizar saldo');

                            //Atualização do Saldo - Acionando trabalho a parte
                            // atualizaSaldoOrganismo(
                            //     $('#mesAtual').val(),
                            //     $('#anoAtual').val(),
                            //     $("#fk_idOrganismoAfiliado").val(),
                            //     null,
                            //     valorRecebimento,
                            //     null,
                            //     null,
                            //     usuario,
                            //     retorno.ultimoId,
                            //     'recebimento',
                            //     retorno.idRefDocumentoFirebase
                            // );

                            return false;
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            alert('Nao foi possivel cadastrar o recebimento');
                        }
                    });
                } else {
                    swal({
                        title: "Aviso!",
                        text: "Já existe um registro com essa data!",
                        type: "warning",
                        confirmButtonColor: "#1ab394"
                    });
                    return false;
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    
}

function atualizaSaldoOrganismo(mes,ano,fk_idOrganismoAfiliado,idIgnorarRecebimento,valorRecebimento,idIgnorarDespesa,valorDespesa,usuario,ultimoId,categoriaEntrada,idRefDocumentoFirebase) {

    //Verificando o básico para fazer atualização do saldo
    if(mes!=""&&ano!=""&&fk_idOrganismoAfiliado!=""&&usuario!="")
    {
        // Obtém a data/hora atual
        var data = new Date();

        // Guarda cada pedaço em uma variável
        var dia     = data.getDate();           // 1-31
        var dia_sem = data.getDay();            // 0-6 (zero=domingo)
        var mes3     = data.getMonth();          // 0-11 (zero=janeiro)
        var ano2    = data.getYear();           // 2 dígitos
        var ano4    = data.getFullYear();       // 4 dígitos
        var hora    = data.getHours();          // 0-23
        var min     = data.getMinutes();        // 0-59
        var seg     = data.getSeconds();        // 0-59
        var mseg    = data.getMilliseconds();   // 0-999
        var tz      = data.getTimezoneOffset(); // em minutos
        // Formata a data e a hora (note o mês + 1)
        var str_data = dia + '/' + (mes3+1) + '/' + ano4;
        var str_hora = hora + ':' + min + ':' + seg;

        $('#spaceBalance').html('<span class="badge"><i class="fa fa-money"></i> <b>Atualizando Saldo</b> em: '+ str_data + ' às ' + str_hora+'</span>');

        $.ajax({
            type: "post",
            url: "cron/atualizaSaldoFirebase.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {
                mes: mes,
                ano: ano,
                fk_idOrganismoAfiliado: fk_idOrganismoAfiliado,
                idIgnorarRecebimento: idIgnorarRecebimento,
                valorRecebimento: valorRecebimento,
                idIgnorarDespesa: idIgnorarDespesa,
                valorDespesa: valorDespesa,
                usuario: usuario,
                ultimoId:ultimoId,
                categoriaEntrada:categoriaEntrada,
                idRefDocumentoFirebase:idRefDocumentoFirebase
            },
            success: function (retorno) {

                // Obtém a data/hora atual
                var data = new Date();
                // Guarda cada pedaço em uma variável
                var dia2     = data.getDate();           // 1-31
                var dia_sem2 = data.getDay();            // 0-6 (zero=domingo)
                var mes2     = data.getMonth();          // 0-11 (zero=janeiro)
                var ano2    = data.getYear();           // 2 dígitos
                var ano2    = data.getFullYear();       // 4 dígitos
                var hora2    = data.getHours();          // 0-23
                var min2     = data.getMinutes();        // 0-59
                var seg2     = data.getSeconds();        // 0-59
                var mseg2    = data.getMilliseconds();   // 0-999
                var tz2      = data.getTimezoneOffset(); // em minutos
                // Formata a data e a hora (note o mês + 1)
                var str_data2 = dia2 + '/' + (mes2+1) + '/' + ano2;
                var str_hora2 = hora2 + ':' + min2 + ':' + seg2;
                // Mostra o resultado
                //alert('Fim: ' + str_data + ' às ' + str_hora);
                // console.log('Novo Saldo:' + retorno.balance.toFixed(2));

                // $('#spaceBalance').html('<span class="badge"><i class="fa fa-money"></i> <b>Novo Saldo: '+retorno.balance+'</b> - Data e Hora: '+ str_data2 + ' às ' + str_hora2+'</span>');
                $('#spaceBalance').html('<span class="badge"><i class="fa fa-money"></i> <b>Saldo Atualizado: ' + retorno.balance.toFixed(2) + '</b> em: '+ str_data2 + ' às ' + str_hora2+'</span>');

                //Atualizar total geral
                if(categoriaEntrada=='recebimento')
                {
                    atualizaTotalGeralRecebimento(mes, ano, fk_idOrganismoAfiliado);
                }
                if(categoriaEntrada=='despesa')
                {
                    atualizaTotalGeralDespesa(mes, ano, fk_idOrganismoAfiliado);
                }
                if(categoriaEntrada=='saldoInicial')
                {
                    location.href = "painelDeControle.php?corpo=buscaSaldoInicial";
                }
                // return false;
            },
            error: function (xhr, textStatus, errorThrown) {
                /*
                swal({
                    title: "Aviso!",
                    text: "Ocorreu um erro ao atualizar o Saldo do Organismo! Envie um email relatando para soa@amorc.org.br!",
                    type: "warning",
                    confirmButtonColor: "#1ab394"
                });
                */
                return false;
            }
        });
    } else {
        /*
        swal({
            title: "Aviso!",
            text: "Faltando informações básicas para atualização do saldo! Envie um email relatando para soa@amorc.org.br!",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
        return false;*/
    }
}

function abriCamposEditarRecebimento(id)
{
    if($("#linhaAberta").val()!=0&&$("#pote").val()>1)
    {
            //alert('vai editar recebimento com a linha aberta');
            //editarRecebimento($("#linhaAberta").val());
    }else{
            //alert('abrir linha de boa:'+id);
    }	
	
    $.ajax({
        type: "post",
        url: "js/ajax/getRecebimentoPorId.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$('#1td'+id).prop('onclick',null).off('click');
        	$('#2td'+id).prop('onclick',null).off('click');
        	$('#3td'+id).prop('onclick',null).off('click');
        	$('#4td'+id).prop('onclick',null).off('click');
        	$('#5td'+id).prop('onclick',null).off('click');
        	$('#6td'+id).prop('onclick',null).off('click');
        	
        	$("#5col"+id).html("");//Apagar
        	
        	//alert(retorno);
        	$("#1col"+id).html("<input type=\"text\" name=\"dataRecebimento"+id+"\" class=\"mascaraData\" style=\"width:105%\" id=\"dataRecebimento"+id+"\" value=\""+retorno.dataRecebimento+"\">");
        	$("#2col"+id).html("<input type=\"text\" name=\"descricaoRecebimento"+id+"\" id=\"descricaoRecebimento"+id+"\" value=\""+retorno.descricaoRecebimento+"\">");
        	$("#3col"+id).html("<input type=\"text\" name=\"recebemosDe"+id+"\" id=\"recebemosDe"+id+"\" value=\""+retorno.recebemosDe+"\">");
        	$("#4col"+id).html("<input type=\"text\" name=\"codigoAfiliacao"+id+"\" id=\"codigoAfiliacao"+id+"\" style=\"width:105%\" value=\""+retorno.codigoAfiliacao+"\">");
        	$("#5col"+id).html("<input type=\"text\" name=\"valorRecebimento"+id+"\" id=\"valorRecebimento"+id+"\" class=\"currency\" onpaste=\"return false;\" style=\"width:105%\" value=\""+retorno.valorRecebimento+"\">");
        	$("#6col"+id).html("<select name=\"categoriaRecebimento"+id+"\" id=\"categoriaRecebimento"+id+"\"><option value=\"0\">Selecione...</option><option value=\"5\">Atividades Sociais</option><option value=\"3\">Bazar</option><option value=\"4\">Comissões</option><option value=\"7\">Construção</option><option value=\"8\">Convenções</option><option value=\"2\">Donativos e Lei de AMRA</option><option value=\"14\">GLP Outros Valores</option><option value=\"13\">GLP Suprimentos</option><option value=\"12\">GLP Trimestralidades</option><option value=\"10\">Jornadas</option><option value=\"15\">Mensalidade Atrium Martinista</option><option value=\"1\">Mensalidade do Organismo</option><option value=\"6\">Recebimentos Diversos</option><option value=\"9\">Receitas Financeiras</option><option value=\"16\">Região</option><option value=\"11\">Suprimentos do Organismo</option></select>");
        	$("#categoriaRecebimento"+id).val(retorno.categoriaRecebimento);
        	$("#botaoEditar"+id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"editarRecebimento(\'"+id+"\');\"> Salvar</a>");
        	
        	$("#outros_botoes"+id).hide();
        	
        	//guardar nova linha aberta
        	$("#linhaAberta").val(id); 
        	
        	$(".mascaraData").mask("99/99/9999");
        	$(".currency").maskMoney({symbol:'R$ ', 
    	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true});

        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function editarRecebimento(id)
{
    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        $("#load").html("<img src='img/loading_trans.gif' width='70'>");

        var dataRecebimento = $("#dataRecebimento" + id).val();
        var descricaoRecebimento = $("#descricaoRecebimento" + id).val();
        var recebemosDe = $("#recebemosDe" + id).val();
        var codigoAfiliacao = $("#codigoAfiliacao" + id).val();
        var valorRecebimento = $("#valorRecebimento" + id).val();
        var categoriaRecebimento = $("#categoriaRecebimento" + id).val();
        var usuario = $("#usuario").val();
        var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();

        var mesAtual = $("#mesAtual").val();
        var anoAtual = $("#anoAtual").val();
        var dataPreenchido = $("#dataRecebimento" + id).val();
        var mesPreenchido = dataPreenchido.substr(3, 2);
        var anoPreenchido = dataPreenchido.substr(6, 4);

        //Validação
        if (mesAtual != mesPreenchido) {
            swal({
                title: "Aviso!",
                text: "O mês da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (anoAtual != anoPreenchido) {
            swal({
                title: "Aviso!",
                text: "O ano da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        //Validação
        if (dataRecebimento == "") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher uma data de recebimento!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (dataRecebimento == "00/00/0000") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher a data de recebimento corretamente! Não use 00/00/0000!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        if (valorRecebimento == "") {
            //swal({title:"Aviso!",text:"É preciso preencher o valor do recebimento!",type:"warning",confirmButtonColor:"#1ab394"});
            //return false;
            valorRecebimento = "0,00";
        }

        if (categoriaRecebimento == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar uma categoria!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        $.ajax({
            type: "post",
            url: "js/ajax/editarRecebimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {
                id: id,
                fk_idOrganismoAfiliado: fk_idOrganismoAfiliado,
                dataRecebimento: dataRecebimento,
                descricaoRecebimento: descricaoRecebimento,
                recebemosDe: recebemosDe,
                codigoAfiliacao: codigoAfiliacao,
                valorRecebimento: valorRecebimento,
                categoriaRecebimento: categoriaRecebimento,
                usuario: usuario
            }, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno) {

                $("#load").html("");

                //alert(retorno);
                $("#1col" + id).html(dataRecebimento);
                $("#2col" + id).html(descricaoRecebimento);
                $("#3col" + id).html(recebemosDe);
                if (codigoAfiliacao == "") {
                    $("#4col" + id).html("--");
                } else {
                    $("#4col" + id).html(codigoAfiliacao);
                }
                $("#5col" + id).html(valorRecebimento);
                $("#6col" + id).html(retorno.categoriaRecebimento);
                $("#botaoEditar" + id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarRecebimento(\'" + id + "\');\"> Editar</a>");

                $('#1td' + id).click(function () {
                    abriCamposEditarRecebimento(id);
                });
                $('#2td' + id).click(function () {
                    abriCamposEditarRecebimento(id);
                });
                $('#3td' + id).click(function () {
                    abriCamposEditarRecebimento(id);
                });
                $('#4td' + id).click(function () {
                    abriCamposEditarRecebimento(id);
                });
                $('#5td' + id).click(function () {
                    abriCamposEditarRecebimento(id);
                });
                $('#6td' + id).click(function () {
                    abriCamposEditarRecebimento(id);
                });

                $("#outros_botoes" + id).show();


                /** Função trazida para cá após remoção da chamada de atualizaSaldoOrganismo() */
                atualizaTotalGeralRecebimento(mesAtual, anoAtual, fk_idOrganismoAfiliado);

                //alert('Vai atualizar saldo');

                //Atualização do Saldo - Acionando trabalho a parte
                //atualizaSaldoOrganismo(mes,ano,fk_idOrganismoAfiliado,idIgnorarRecebimento,valorRecebimento,idIgnorarDespesa,valorDespesa,usuario,ultimoId,categoriaEntrada,idRefDocumentoFirebase)
                // atualizaSaldoOrganismo(
                //     $('#mesAtual').val(),
                //     $('#anoAtual').val(),
                //     $("#fk_idOrganismoAfiliado").val(),
                //     id,
                //     valorRecebimento,
                //     null,
                //     null,
                //     usuario,
                //     id,
                //     'recebimento',
                //     retorno.idRefDocumentoFirebase
                // );

                //Atualizar total geral
                //atualizaTotalGeralRecebimento($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                return false;
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
}

function atualizaTotalGeralRecebimento(mesAtual,anoAtual,idOrganismoAfiliado){
		  	
	  	$.ajax({
	        type: "post",
	        url: "js/ajax/atualizaTotalGeralRecebimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
	        dataType: "json",
	        data: {mesAtual: mesAtual,anoAtual:anoAtual,idOrganismoAfiliado:idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
	        success: function (retorno) {
	        	
	        	$('#totalGeral').html(retorno.totalGeral);
	        	
	        	return false;
	      },
	        error: function (xhr, textStatus, errorThrown) {
	
	        }
	    });
	
}

function bloquearMovimentacaoPoisSemSaldoInicial()
{
    var temSaldoInicial = $("#temSaldoInicial").val();
    //alert("temSI:"+temSaldoInicial);
    if(temSaldoInicial==0)
    {
        swal({title:"Aviso!",text:"Infelizmente não é possível realizar nenhuma movimentação financeira sem o cadastro do Saldo Inicial do Organismo!",type:"warning",confirmButtonColor:"#1ab394"});
        return false;
    }else{
        return true;
    }
}

function deleteRowDespesa(id){

    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        $("#load").html("<img src='img/loading_trans.gif' width='70'>");

        swal({
                title: "Você tem certeza?",
                text: "Que deseja excluir esse item?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, exclua!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    var pote = $("#pote").val();
                    var total = parseInt(pote) - 1;
                    $("#pote").val(total);

                    $.ajax({
                        type: "post",
                        url: "js/ajax/excluirDespesa.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (retorno) {

                            $("#load").html("");

                            $('#linha' + id).remove();
                            swal({
                                title: "Sucesso!",
                                text: "Item excluído com sucesso!",
                                type: "success",
                                confirmButtonColor: "#1ab394"
                            });
                            $("#proximoId").val(retorno.proximo_id);

                            /** Função trazida para cá após remoção da chamada de atualizaSaldoOrganismo() */
                            atualizaTotalGeralDespesa($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                            var usuario = $("#usuario").val();

                            //alert('Vai atualizar saldo');

                            //Atualização do Saldo - Acionando trabalho a parte
                            //atualizaSaldoOrganismo(mes,ano,fk_idOrganismoAfiliado,idIgnorarRecebimento,valorRecebimento,idIgnorarDespesa,valorDespesa,usuario,ultimoId,categoriaEntrada,idRefDocumentoFirebase)
                            // atualizaSaldoOrganismo(
                            //     $('#mesAtual').val(),
                            //     $('#anoAtual').val(),
                            //     $("#fk_idOrganismoAfiliado").val(),
                            //     null,
                            //     null,
                            //     null,
                            //     null,
                            //     usuario,
                            //     null,
                            //     'despesa',
                            //     null
                            // );

                            //Atualizar total geral
                            //atualizaTotalGeralDespesa($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                            return false;
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                } else {
                    swal({
                        title: "Cancelado",
                        text: "Nós não excluímos esse item :)",
                        type: "error",
                        confirmButtonColor: "#1ab394"
                    });
                }
            });
    }
}

function addRowDespesa(proximo_id){
	proximo_id = $("#proximoId").val();
	//alert('proximo id:'+proximo_id);
	if(parseInt($("#adicionar").val())<1)
  	{
		var idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
		var pote = $("#pote").val();
	  	var total = parseInt(pote)+1;
	  	$("#pote").val(total);
	  	
	  	var adicionar = $("#adicionar").val();
	  	var total2 = parseInt(adicionar)+1;
	  	$("#adicionar").val(total2);
	  	
	  	if($("#linhaAberta").val()!=0)
		{
			//editarRecebimento($("#linhaAberta").val());
		}
  	
	  	//Setar linha aberta para zero
	  	$("#linhaAberta").val(0);
  	
	  	if(total==1)
	    {
	    	document.getElementById("tabela").innerHTML ='';	
	    }
	  	
	  	// Obtendo o elemento tabela
	    var tab       = document.getElementById('tabela');
	    
	    // colocando borda na tabela:
	    tab.style.border = '1px solid black';
	    
	    // Obtendo a quantidade de linhas da tabela:
	    var qntLinhas = tab.rows.length;
	    
	    // Inserindo uma linha
	    var novaLinha = tab.insertRow( qntLinhas );
	    
	    // Escolhendo um id para evitar IDs duplicados
	    //var novaID = Math.floor((Math.random()*99999)+1);
	    // E definindo seu id
	    novaLinha.id  = 'linha'+proximo_id;
	    
	    // Como nosso elemento novaLinha eh a linha que acabamos de criar
	    // podemos adicionar celulas (colunas) no mesmo.
	    
	    // Criando a primeira coluna
	    var coluna1 = novaLinha.insertCell(0);
	    coluna1.id  = '1coluna'+proximo_id;
	    // Criando a segunda coluna
	    var coluna2 = novaLinha.insertCell(1);
	    coluna2.id  = '2coluna'+proximo_id;
	    // Criando a terceira coluna
	    var coluna3 = novaLinha.insertCell(2);
	    coluna3.id  = '3coluna'+proximo_id;
	    // Criando a quarta coluna
	    var coluna4 = novaLinha.insertCell(3);
	    coluna4.id  = '4coluna'+proximo_id;
	    // Criando a quinta coluna
	    var coluna5 = novaLinha.insertCell(4);
	    coluna5.id  = '5coluna'+proximo_id;
	    // Criando a sexta coluna
	    var coluna6 = novaLinha.insertCell(5);
	    
	    // Agora vamos criar os elementos que estarão dentro de cada coluna
	    coluna1.innerHTML = '<div id=\"1col'+proximo_id+'\"><input name=\"dataDespesa'+proximo_id+'"\" class=\"mascaraData\" style=\"width:94px\" maxlength=\"10\" id=\"dataDespesa'+proximo_id+'"\" type=\"text\"></div>';
	    coluna2.innerHTML = '<div id=\"2col'+proximo_id+'\"><input type=\"text\" name=\"descricaoDespesa'+proximo_id+'"\" style=\"width:120px\" id=\"descricaoDespesa'+proximo_id+'"\"></div>';
	    coluna3.innerHTML = '<div id=\"3col'+proximo_id+'\"><input type=\"text\" name=\"pagoA'+proximo_id+'\" style=\"width:120px\" id=\"pagoA'+proximo_id+'\"></div>';
	    coluna4.innerHTML = '<div id=\"4col'+proximo_id+'\"><input type=\"text\" name=\"valorDespesa'+proximo_id+'\" class=\"currency\" onpaste=\"return false;\" style=\"width:100px\" id=\"valorDespesa'+proximo_id+'\"></div>';
	    coluna5.innerHTML = '<div id=\"5col'+proximo_id+'\"><select name=\"categoriaDespesa'+proximo_id+'\" id=\"categoriaDespesa'+proximo_id+'\"><option value=\"0\">Selecione...</option><option value=\"4\">Água</option><option value=\"1\">Aluguel</option><option value=\"14\">Anúncios</option><option value=\"8\">Beneficência Social</option><option value=\"9\">Boletim</option><option value=\"16\">Cantina</option><option value=\"15\">Carta Constitutiva (GLP)</option><option value=\"2\">Comissões</option><option value=\"10\">Convenções</option><option value=\"13\">Despesas de Correio</option><option value=\"17\">Despesas Gerais</option><option value=\"20\">GLP – Outros</option><option value=\"19\">GLP – Pagamentos Suprimentos</option><option value=\"18\">GLP – Remessa de Trimestralidade</option><option value=\"21\">Investimentos</option><option value=\"22\">Impostos</option><option value=\"11\">Jornadas</option><option value=\"3\">Luz</option><option value=\"7\">Manutenção</option><option value=\"23\">Região</option><option value=\"12\">Reuniões Sociais</option><option value=\"6\">Tarifas</option><option value=\"5\">Telefone</option></select></div>';
	    coluna6.innerHTML = '<div style=\'width:200\'><span id=\"botaoEditar'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"cadastrarDespesa(\''+proximo_id+'\');\"> Salvar</a></span> <span id=\"outros_botoes'+proximo_id+'\" style=\"display:none\"><a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRowDespesa(\''+proximo_id+'\');\"> Excluir</a> <a href=\"#\" class=\'btn btn-xs btn-warning\' onclick=\"abrirPopUpReciboDespesa(\''+proximo_id+'\',\''+idOrganismoAfiliado+'\');\"> Recibo</a></span></div>';

	    $(".mascaraData").mask("99/99/9999");
	    $(".currency").maskMoney({symbol:'R$ ', 
	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
	    
	    document.getElementById('dataDespesa'+proximo_id).focus();
	    
  	}else{
  		swal({title:"Aviso!",text:"Você já tem uma linha aberta. Cadastre essa entrada para acrescentar mais linhas.",type:"warning",confirmButtonColor:"#1ab394"});
  	}
}

function cadastrarDespesa(id)
{
    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        //alert('chamada no cadastro id:'+id);

        var dataDespesa = $("#dataDespesa" + id).val();
        var descricaoDespesa = $("#descricaoDespesa" + id).val();
        var pagoA = $("#pagoA" + id).val();
        var valorDespesa = $("#valorDespesa" + id).val();
        var categoriaDespesa = $("#categoriaDespesa" + id).val();
        var usuario = $("#usuario").val();
        var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();

        var mesAtual = $("#mesAtual").val();
        var anoAtual = $("#anoAtual").val();
        var dataPreenchido = $("#dataDespesa" + id).val();
        var mesPreenchido = dataPreenchido.substr(3, 2);
        var anoPreenchido = dataPreenchido.substr(6, 4);

        //Validação
        if (mesAtual != mesPreenchido) {
            swal({
                title: "Aviso!",
                text: "O mês da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (anoAtual != anoPreenchido) {
            swal({
                title: "Aviso!",
                text: "O ano da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        if (dataDespesa == "") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher uma data da despesa!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (dataDespesa == "00/00/0000") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher a data da despesa corretamente! Não use 00/00/0000!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (valorDespesa == "") {
            //swal({title:"Aviso!",text:"É preciso preencher o valor da despesa!",type:"warning",confirmButtonColor:"#1ab394"});
            //return false;
            valorDespesa = "0,00";
        }
        if (categoriaDespesa == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar uma categoria!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        $("#load").html("<img src='img/loading_trans.gif' width='70'>");

        $.ajax({
            type: "post",
            url: "js/ajax/cadastraDespesa.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {
                dataDespesa: dataDespesa,
                descricaoDespesa: descricaoDespesa,
                pagoA: pagoA,
                valorDespesa: valorDespesa,
                categoriaDespesa: categoriaDespesa,
                usuario: usuario,
                fk_idOrganismoAfiliado: fk_idOrganismoAfiliado
            }, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno) {

                //alert("teste do braz categoria desp:"+retorno.categoriaDespesa);

                $("#load").html("");

                //alert("depois de cadastrado id:"+id);
                //alert("====>"+retorno);
                $("#1col" + id).html(dataDespesa);
                $("#2col" + id).html(descricaoDespesa);
                $("#3col" + id).html(pagoA);
                $("#4col" + id).html(valorDespesa);
                $("#5col" + id).html(retorno.categoriaDespesa);
                $("#proximoId").val(parseInt(retorno.id)+1);
                $("#adicionar").val(0);
                $("#outros_botoes" + id).show();

                $('#1td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#2td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#3td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#4td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#5td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });

                $("#botaoEditar" + id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarDespesa(\'" + retorno.ultimoId + "\');\"> Editar</a>");
                $("#botaoExcluir" + id).html("<a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRowDespesa(\'" + retorno.ultimoId + "\');\"> Excluir</a>");
                $("#botaoRecibo" + id).html("<a href=\"#\" class=\'btn btn-xs btn-warning\' onclick=\"abrirPopUpReciboDespesa(\'" + retorno.ultimoId + "\',\'" + fk_idOrganismoAfiliado + "\');\"> Recibo</a>");

                /** Função trazida para cá após remoção da chamada de atualizaSaldoOrganismo() */
                atualizaTotalGeralDespesa($('#mesAtual').val(), $('#anoAtual').val(), fk_idOrganismoAfiliado);

                //alert('Vai atualizar saldo');

                //Atualização do Saldo - Acionando trabalho a parte
                //atualizaSaldoOrganismo(mes,ano,fk_idOrganismoAfiliado,idIgnorarRecebimento,valorRecebimento,idIgnorarDespesa,valorDespesa,usuario,ultimoId,categoriaEntrada,idRefDocumentoFirebase)
                // atualizaSaldoOrganismo(
                //     $('#mesAtual').val(),
                //     $('#anoAtual').val(),
                //     $("#fk_idOrganismoAfiliado").val(),
                //     null,
                //     null,
                //     null,
                //     null,
                //     usuario,
                //     null,
                //     'despesa',
                //     retorno.idRefDocumentoFirebase
                // );

                //Atualizar total geral
                //atualizaTotalGeralDespesa($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                return false;
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    
}

function abriCamposEditarDespesa(id)
{
	if($("#linhaAberta").val()!=0&&$("#pote").val()>1)
	{
		//alert('vai editar recebimento com a linha aberta');
		//editarRecebimento($("#linhaAberta").val());
	}else{
		//alert('abrir linha de boa:'+id);
	}	
	
    $.ajax({
        type: "post",
        url: "js/ajax/getDespesaPorId.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$('#1td'+id).prop('onclick',null).off('click');
        	$('#2td'+id).prop('onclick',null).off('click');
        	$('#3td'+id).prop('onclick',null).off('click');
        	$('#4td'+id).prop('onclick',null).off('click');
        	$('#5td'+id).prop('onclick',null).off('click');
        	
        	$("#5col"+id).html("");//Apagar
        	
        	//alert(retorno);
        	$("#1col"+id).html("<input type=\"text\" name=\"dataDespesa"+id+"\" class=\"mascaraData\" style=\"width:105%\" id=\"dataDespesa"+id+"\" value=\""+retorno.dataDespesa+"\">");
        	$("#2col"+id).html("<input type=\"text\" name=\"descricaoDespesa"+id+"\" id=\"descricaoDespesa"+id+"\" value=\""+retorno.descricaoDespesa+"\">");
        	$("#3col"+id).html("<input type=\"text\" name=\"pagoA"+id+"\" id=\"pagoA"+id+"\" value=\""+retorno.pagoA+"\">");
        	$("#4col"+id).html("<input type=\"text\" name=\"valorDespesa"+id+"\" id=\"valorDespesa"+id+"\" class=\"currency\" onpaste=\"return false;\" style=\"width:105%\" value=\""+retorno.valorDespesa+"\">");
        	$("#5col"+id).html("<select name=\"categoriaDespesa"+id+"\" id=\"categoriaDespesa"+id+"\"><option value=\"0\">Selecione...</option><option value=\"4\">Água</option><option value=\"1\">Aluguel</option><option value=\"14\">Anúncios</option><option value=\"8\">Beneficência Social</option><option value=\"9\">Boletim</option><option value=\"16\">Cantina</option><option value=\"15\">Carta Constitutiva (GLP)</option><option value=\"2\">Comissões</option><option value=\"10\">Convenções</option><option value=\"13\">Despesas de Correio</option><option value=\"17\">Despesas Gerais</option><option value=\"20\">GLP – Outros</option><option value=\"19\">GLP – Pagamentos Suprimentos</option><option value=\"18\">GLP – Remessa de Trimestralidade</option><option value=\"21\">Investimentos</option><option value=\"22\">Impostos</option><option value=\"11\">Jornadas</option><option value=\"3\">Luz</option><option value=\"7\">Manutenção</option><option value=\"23\">Região</option><option value=\"12\">Reuniões Sociais</option><option value=\"6\">Tarifas</option><option value=\"5\">Telefone</option></select>");
        	$("#categoriaDespesa"+id).val(retorno.categoriaDespesa);
        	$("#botaoEditar"+id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"editarDespesa(\'"+id+"\');\"> Salvar</a>");
        	
        	$("#outros_botoes"+id).hide();
        	
        	//guardar nova linha aberta
        	$("#linhaAberta").val(id); 
        	
        	$(".mascaraData").mask("99/99/9999");
        	$(".currency").maskMoney({symbol:'R$ ', 
    	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function editarDespesa(id)
{
    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        var dataDespesa = $("#dataDespesa" + id).val();
        var descricaoDespesa = $("#descricaoDespesa" + id).val();
        var pagoA = $("#pagoA" + id).val();
        var valorDespesa = $("#valorDespesa" + id).val();
        //alert(valorDespesa);
        var categoriaDespesa = $("#categoriaDespesa" + id).val();
        var usuario = $("#usuario").val();
        var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();

        var mesAtual = $("#mesAtual").val();
        var anoAtual = $("#anoAtual").val();
        var dataPreenchido = $("#dataDespesa" + id).val();
        var mesPreenchido = dataPreenchido.substr(3, 2);
        var anoPreenchido = dataPreenchido.substr(6, 4);

        //Validação
        if (mesAtual != mesPreenchido) {
            swal({
                title: "Aviso!",
                text: "O mês da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (anoAtual != anoPreenchido) {
            swal({
                title: "Aviso!",
                text: "O ano da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        //Validação
        if (dataDespesa == "") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher uma data da despesa!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (dataDespesa == "00/00/0000") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher a data da despesa corretamente! Não use 00/00/0000!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (valorDespesa == "") {
            //swal({title:"Aviso!",text:"É preciso preencher o valor da despesa!",type:"warning",confirmButtonColor:"#1ab394"});
            //return false;
            valorDespesa = "0,00";
        }
        if (categoriaDespesa == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar uma categoria!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        $("#load").html("<img src='img/loading_trans.gif' width='70'>");

        $.ajax({
            type: "post",
            url: "js/ajax/editarDespesa.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {
                id: id,
                fk_idOrganismoAfiliado: fk_idOrganismoAfiliado,
                dataDespesa: dataDespesa,
                descricaoDespesa: descricaoDespesa,
                pagoA: pagoA,
                valorDespesa: valorDespesa,
                categoriaDespesa: categoriaDespesa,
                usuario: usuario
            }, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno) {

                $("#load").html("");

                //alert("retorno Atualizar Despesa:"+retorno);
                $("#1col" + id).html(dataDespesa);
                $("#2col" + id).html(descricaoDespesa);
                $("#3col" + id).html(pagoA);
                $("#4col" + id).html(valorDespesa);
                $("#5col" + id).html(retorno.categoriaDespesa);
                $("#botaoEditar" + id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarDespesa(\'" + id + "\');\"> Editar</a>");

                $('#1td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#2td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#3td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#4td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });
                $('#5td' + id).click(function () {
                    abriCamposEditarDespesa(id);
                });

                $("#outros_botoes" + id).show();

                /** Função trazida para cá após remoção da chamada de atualizaSaldoOrganismo() */
                atualizaTotalGeralDespesa($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                //alert('Vai atualizar saldo');

                //Atualização do Saldo - Acionando trabalho a parte
                //atualizaSaldoOrganismo(mes,ano,fk_idOrganismoAfiliado,idIgnorarRecebimento,valorRecebimento,idIgnorarDespesa,valorDespesa,usuario,ultimoId,categoriaEntrada,idRefDocumentoFirebase)
                // atualizaSaldoOrganismo(
                //     $('#mesAtual').val(),
                //     $('#anoAtual').val(),
                //     $("#fk_idOrganismoAfiliado").val(),
                //     null,
                //     null,
                //     id,
                //     valorDespesa,
                //     usuario,
                //     id,
                //     'despesa',
                //     retorno.idRefDocumentoFirebase
                // );

                //Atualizar total geral
                //atualizaTotalGeralDespesa($('#mesAtual').val(), $('#anoAtual').val(), $("#fk_idOrganismoAfiliado").val());

                return false;
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
}

function atualizaTotalGeralDespesa(mesAtual,anoAtual,idOrganismoAfiliado){
  	
  	$.ajax({
        type: "post",
        url: "js/ajax/atualizaTotalGeralDespesa.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {mesAtual: mesAtual,anoAtual:anoAtual,idOrganismoAfiliado:idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$('#totalGeral').html(retorno.totalGeral);
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });

} 

function deleteRowRendimento(id){

    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        swal({
                title: "Você tem certeza?",
                text: "Que deseja excluir esse item?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, exclua!",
                cancelButtonText: "Não, cancele!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    var pote = $("#pote").val();
                    var total = parseInt(pote) - 1;
                    $("#pote").val(total);

                    $.ajax({
                        type: "post",
                        url: "js/ajax/excluirRendimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (retorno) {

                            $('#linha' + id).remove();
                            swal({
                                title: "Sucesso!",
                                text: "Item excluído com sucesso!",
                                type: "success",
                                confirmButtonColor: "#1ab394"
                            });
                            $("#proximoId").val(retorno.proximo_id);

                            return false;
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                } else {
                    swal({
                        title: "Cancelado",
                        text: "Nós não excluímos esse item :)",
                        type: "error",
                        confirmButtonColor: "#1ab394"
                    });
                }
            });
    }
}

function addRowRendimento(proximo_id){
	proximo_id = $("#proximoId").val();
	//alert('proximo id:'+proximo_id);
	if(parseInt($("#adicionar").val())<1)
  	{
		var idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
		var pote = $("#pote").val();
	  	var total = parseInt(pote)+1;
	  	$("#pote").val(total);
	  	
	  	var adicionar = $("#adicionar").val();
	  	var total2 = parseInt(adicionar)+1;
	  	$("#adicionar").val(total2);
	  	
	  	if($("#linhaAberta").val()!=0)
		{
			//editarRecebimento($("#linhaAberta").val());
		}
  	
	  	//Setar linha aberta para zero
	  	$("#linhaAberta").val(0);
  	
	  	if(total==1)
	    {
	    	document.getElementById("tabela").innerHTML ='';	
	    }
	  	
	  	// Obtendo o elemento tabela
	    var tab       = document.getElementById('tabela');
	    
	    // colocando borda na tabela:
	    tab.style.border = '1px solid black';
	    
	    // Obtendo a quantidade de linhas da tabela:
	    var qntLinhas = tab.rows.length;
	    
	    // Inserindo uma linha
	    var novaLinha = tab.insertRow( qntLinhas );
	    
	    // Escolhendo um id para evitar IDs duplicados
	    //var novaID = Math.floor((Math.random()*99999)+1);
	    // E definindo seu id
	    novaLinha.id  = 'linha'+proximo_id;
	    
	    // Como nosso elemento novaLinha eh a linha que acabamos de criar
	    // podemos adicionar celulas (colunas) no mesmo.
	    
	    // Criando a primeira coluna
	    var coluna1 = novaLinha.insertCell(0);
	    coluna1.id  = '1coluna'+proximo_id;
	    // Criando a segunda coluna
	    var coluna2 = novaLinha.insertCell(1);
	    coluna2.id  = '2coluna'+proximo_id;
	    // Criando a terceira coluna
	    var coluna3 = novaLinha.insertCell(2);
	    coluna3.id  = '3coluna'+proximo_id;
	    // Criando a quarta coluna
	    var coluna4 = novaLinha.insertCell(3);
	    coluna4.id  = '4coluna'+proximo_id;
	    // Criando a quinta coluna
	    var coluna5 = novaLinha.insertCell(4);
	    coluna5.id  = '5coluna'+proximo_id;
	    // Criando a sexta coluna
	    var coluna6 = novaLinha.insertCell(5);
	    
	    // Agora vamos criar os elementos que estarão dentro de cada coluna
	    coluna1.innerHTML = '<div id=\"1col'+proximo_id+'\"><input name=\"dataVerificacao'+proximo_id+'"\" class=\"mascaraData\" style=\"width:94px\" maxlength=\"10\" id=\"dataVerificacao'+proximo_id+'"\" type=\"text\"></div>';
	    coluna2.innerHTML = '<div id=\"2col'+proximo_id+'\"><input type=\"text\" name=\"descricaoRendimento'+proximo_id+'"\" style=\"width:130px\" id=\"descricaoRendimento'+proximo_id+'"\"></div>';
	    coluna3.innerHTML = '<div id=\"3col'+proximo_id+'\"><input type=\"text\" name=\"valorRendimento'+proximo_id+'\" onkeyup="mascara_num(this);" style=\"width:80px\" onpaste=\"return false;\" id=\"valorRendimento'+proximo_id+'\"></div>';
	    coluna4.innerHTML = '<div id=\"4col'+proximo_id+'\"><select name=\"categoriaRendimento'+proximo_id+'\" id=\"categoriaRendimento'+proximo_id+'\"><option value=\"0\">Selecione...</option><option value=\"7\">Aplicação - Outros</option><option value=\"5\">Aplicação CDB</option><option value=\"4\">Aplicação Poupança</option><option value=\"6\">Aplicação RDB</option><option value=\"3\">Atrium Martinista</option><option value=\"2\">Bancos</option><option value=\"1\">Dinheiro em caixa</option></select></div>';
	    coluna5.innerHTML = '<div id=\"5col'+proximo_id+'\"><select name=\"atribuidoA'+proximo_id+'\" id=\"atribuidoA'+proximo_id+'\"><option value=\"0\">Selecione...</option><option value=\"1\">Organismo Afiliado</option><option value=\"2\">Região</option></select></div>';
	    coluna6.innerHTML = '<div style=\'width:200\'><span id=\"botaoEditar'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"cadastrarRendimento(\''+proximo_id+'\');\"> Salvar</a></span> <span id=\"outros_botoes'+proximo_id+'\" style=\"display:none\"><a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRowRendimento(\''+proximo_id+'\');\"> Excluir</a></span></div>';
	  
	    $(".mascaraData").mask("99/99/9999");
	    $(".currency").maskMoney({symbol:'R$ ', 
	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
	    
	    document.getElementById('dataVerificacao'+proximo_id).focus();
	    
  	}else{
  		swal({title:"Aviso!",text:"Você já tem uma linha aberta. Cadastre essa entrada para acrescentar mais linhas.",type:"warning",confirmButtonColor:"#1ab394"});
  	}
}

function cadastrarRendimento(id)
{
    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        //alert('chamada no cadastro id:'+id);
        var dataVerificacao = $("#dataVerificacao" + id).val();
        var descricaoRendimento = $("#descricaoRendimento" + id).val();
        var valorRendimento = $("#valorRendimento" + id).val();
        var categoriaRendimento = $("#categoriaRendimento" + id).val();
        var atribuidoA = $("#atribuidoA" + id).val();
        var usuario = $("#usuario").val();
        var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();

        var mesAtual = $("#mesAtual").val();
        var anoAtual = $("#anoAtual").val();
        var dataPreenchido = $("#dataVerificacao" + id).val();
        var mesPreenchido = dataPreenchido.substr(3, 2);
        var anoPreenchido = dataPreenchido.substr(6, 4);

        //Validação
        if (mesAtual != mesPreenchido) {
            swal({
                title: "Aviso!",
                text: "O mês da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (anoAtual != anoPreenchido) {
            swal({
                title: "Aviso!",
                text: "O ano da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        if (dataVerificacao == "") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher uma data!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (dataVerificacao == "00/00/0000") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher a data corretamente! Não use 00/00/0000!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (valorRendimento == "") {
            //swal({title:"Aviso!",text:"É preciso preencher o valor!",type:"warning",confirmButtonColor:"#1ab394"});
            //return false;
            valorRendimento = "0,00";
        }
        if (categoriaRendimento == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar uma categoria!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (atribuidoA == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar o campo atribuído à!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        $.ajax({
            type: "post",
            url: "js/ajax/cadastraRendimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {
                dataVerificacao: dataVerificacao,
                descricaoRendimento: descricaoRendimento,
                valorRendimento: valorRendimento,
                categoriaRendimento: categoriaRendimento,
                atribuidoA: atribuidoA,
                usuario: usuario,
                fk_idOrganismoAfiliado: fk_idOrganismoAfiliado
            }, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno) {
                //alert("depois de cadastrado id:"+id);
                $("#1col" + id).html(dataVerificacao);
                $("#2col" + id).html(descricaoRendimento);
                $("#3col" + id).html(valorRendimento);
                $("#4col" + id).html(retorno.categoriaRendimento);
                $("#5col" + id).html(retorno.atribuidoA);
                $("#proximoId").val(retorno.id);
                $("#adicionar").val(0);
                $("#outros_botoes" + id).show();

                $('#1td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#2td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#3td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#4td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#5td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });

                $("#botaoEditar" + id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarRendimento(\'" + retorno.ultimoId + "\');\"> Editar</a>");
                $("#botaoExcluir" + id).html("<a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRowRendimento(\'" + retorno.ultimoId + "\');\"> Excluir</a>");

                var mesAtual = $("#mesAtual").val();
                var anoAtual = $("#anoAtual").val();
                location.href = '?corpo=buscaRendimentos&mesAtual=' + mesAtual + '&anoAtual=' + anoAtual;

                return false;
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
    
}

function abriCamposEditarRendimento(id)
{
	if($("#linhaAberta").val()!=0&&$("#pote").val()>1)
	{
		//alert('vai editar recebimento com a linha aberta');
		//editarRecebimento($("#linhaAberta").val());
	}else{
		//alert('abrir linha de boa:'+id);
	}	
	
    $.ajax({
        type: "post",
        url: "js/ajax/getRendimentoPorId.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$('#1td'+id).prop('onclick',null).off('click');
        	$('#2td'+id).prop('onclick',null).off('click');
        	$('#3td'+id).prop('onclick',null).off('click');
        	$('#4td'+id).prop('onclick',null).off('click');
        	$('#5td'+id).prop('onclick',null).off('click');
        	
        	$("#5col"+id).html("");//Apagar
        	
        	//alert(retorno);
        	$("#1col"+id).html("<input type=\"text\" name=\"dataVerificacao"+id+"\" class=\"mascaraData\" style=\"width:105%\" id=\"dataVerificacao"+id+"\" value=\""+retorno.dataVerificacao+"\">");
        	$("#2col"+id).html("<input type=\"text\" name=\"descricaoRendimento"+id+"\" id=\"descricaoRendimento"+id+"\" value=\""+retorno.descricaoRendimento+"\">");
        	$("#3col"+id).html("<input type=\"text\" name=\"valorRendimento"+id+"\" onkeyup=\"mascara_num(this);\" onpaste=\"return false;\" id=\"valorRendimento"+id+"\" value=\""+retorno.valorRendimento+"\">");
        	$("#4col"+id).html("<select name=\"categoriaRendimento"+id+"\" id=\"categoriaRendimento"+id+"\"><option value=\"0\">Selecione...</option><option value=\"7\">Aplicação - Outros</option><option value=\"5\">Aplicação CDB</option><option value=\"4\">Aplicação Poupança</option><option value=\"6\">Aplicação RDB</option><option value=\"3\">Atrium Martinista</option><option value=\"2\">Bancos</option><option value=\"1\">Dinheiro em caixa</option></select>");
        	$("#5col"+id).html("<select name=\"atribuidoA"+id+"\" id=\"atribuidoA"+id+"\"><option value=\"0\">Selecione...</option><option value=\"1\">Organismo Afiliado</option><option value=\"2\">Região</option></select>");
        	$("#categoriaRendimento"+id).val(retorno.categoriaRendimento);
        	$("#atribuidoA"+id).val(retorno.atribuidoA);
        	$("#botaoEditar"+id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"editarRendimento(\'"+id+"\');\"> Salvar</a>");
        	
        	$("#outros_botoes"+id).hide();
        	
        	//guardar nova linha aberta
        	$("#linhaAberta").val(id); 
        	
        	$(".mascaraData").mask("99/99/9999");
        	$(".currency").maskMoney({symbol:'R$ ', 
    	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function editarRendimento(id)
{
    if(bloquearMovimentacaoPoisSemSaldoInicial()) {

        var dataVerificacao = $("#dataVerificacao" + id).val();
        var descricaoRendimento = $("#descricaoRendimento" + id).val();
        var valorRendimento = $("#valorRendimento" + id).val();
        var categoriaRendimento = $("#categoriaRendimento" + id).val();
        var atribuidoA = $("#atribuidoA" + id).val();
        var usuario = $("#usuario").val();

        var mesAtual = $("#mesAtual").val();
        var anoAtual = $("#anoAtual").val();
        var dataPreenchido = $("#dataVerificacao" + id).val();
        var mesPreenchido = dataPreenchido.substr(3, 2);
        var anoPreenchido = dataPreenchido.substr(6, 4);

        //Validação
        if (mesAtual != mesPreenchido) {
            swal({
                title: "Aviso!",
                text: "O mês da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (anoAtual != anoPreenchido) {
            swal({
                title: "Aviso!",
                text: "O ano da data preenchida não está certo!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        //Validação
        if (dataVerificacao == "") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher uma data!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (dataVerificacao == "00/00/0000") {
            swal({
                title: "Aviso!",
                text: "É preciso preencher a data corretamente! Não use 00/00/0000!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (valorRendimento == "") {
            //swal({title:"Aviso!",text:"É preciso preencher o valor!",type:"warning",confirmButtonColor:"#1ab394"});
            //return false;
            valorRendimento = "0,00";
        }
        if (categoriaRendimento == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar uma categoria!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }
        if (atribuidoA == 0) {
            swal({
                title: "Aviso!",
                text: "É preciso selecionar o campo atribuído à!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            return false;
        }

        $.ajax({
            type: "post",
            url: "js/ajax/editarRendimento.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {
                id: id,
                dataVerificacao: dataVerificacao,
                descricaoRendimento: descricaoRendimento,
                valorRendimento: valorRendimento,
                categoriaRendimento: categoriaRendimento,
                atribuidoA: atribuidoA,
                usuario: usuario
            }, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno) {

                $("#1col" + id).html(dataVerificacao);
                $("#2col" + id).html(descricaoRendimento);
                $("#3col" + id).html(valorRendimento);
                $("#4col" + id).html(retorno.categoriaRendimento);
                $("#5col" + id).html(retorno.atribuidoA);
                $("#botaoEditar" + id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarRendimento(\'" + id + "\');\"> Editar</a>");

                $('#1td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#2td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#3td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#4td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });
                $('#5td' + id).click(function () {
                    abriCamposEditarRendimento(id);
                });

                $("#outros_botoes" + id).show();

                var mesAtual = $("#mesAtual").val();
                var anoAtual = $("#anoAtual").val();
                location.href = '?corpo=buscaRendimentos&mesAtual=' + mesAtual + '&anoAtual=' + anoAtual;

                return false;
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }
}

function retornaDadosMembroOa()
{
	$('#mySeeLoadingAjax').modal({
    });
    var codigoAfiliacao = $("#codigoAfiliacao").val();
    var nomeMembro = $("#nomeMembroOa").val();
    var tipoMembro = '1';
    //console.log("retornaDados() --- Cód. Afiliação: " + codigoAfiliacao + " Nome: " + nomeMembro + " Tipo: " + tipoMembro);
    //alert('Nome do Membro:'+nomeMembro+' / codigo:'+codigoAfiliacao);
    
    $.ajax({
        type: "post",
        url: "js/ajax/retornaDadosMembro.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {codigoAfiliacao: codigoAfiliacao, nomeMembro:nomeMembro, tipoMembro:tipoMembro}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	//console.log(retorno.result);
        	//alert(retorno.result[0].fields.fNomCliente);
        	//alert(retorno);
        	$("#nomeMembroOa").val(retorno.result[0].fields.fNomCliente);
        	$("#seqCadastMembroOa").val(retorno.result[0].fields.fSeqCadast);
        	$("#seqCadastMembroOaDiv").html("");
        	$("#seqCadastMembroOaDiv").html(retorno.result[0].fields.fSeqCadast);
        	if(retorno.result[0].fields.fSeqCadast==retorno.result[0].fields.fSeqCadastPrincipalRosacruz)
        	{
        		$("#principalCompanheiro").val(1);
        	}
        	if(retorno.result[0].fields.fSeqCadast==retorno.result[0].fields.fSeqCadastCompanheiroRosacruz)
        	{
        		$("#principalCompanheiro").val(2);
        	}
        	setInterval(function(){
			      // method to be executed;
        			fecharModal();
			    },1000);
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
/*
function deleteRowMembrosRosacruzesAtivos(id){
	
	var r = confirm("Tem certeza que deseja excluir esse item?");
	if(r == true)
	{
		var pote = $("#pote").val();
	  	var total = parseInt(pote)-1;
	  	$("#pote").val(total);
	  	
	  	$.ajax({
	        type: "post",
	        url: "js/ajax/excluirMembrosRosacruzesAtivos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
	        dataType: "json",
	        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
	        success: function (retorno) {
	        	
	        	$('#linha'+id).remove();
	        	alert('Item excluído com sucesso!');
	        	$("#proximoId").val(retorno.proximo_id);	
	        	
	        	return false;
	      },
	        error: function (xhr, textStatus, errorThrown) {
	
	        }
	    });
	}else{
		return false;
	}
}

function addRowMembrosRosacruzesAtivos(proximo_id){
	proximo_id = $("#proximoId").val();
	//alert('proximo id:'+proximo_id);
	if(parseInt($("#adicionar").val())<1)
  	{
		var idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
		var pote = $("#pote").val();
	  	var total = parseInt(pote)+1;
	  	$("#pote").val(total);
	  	
	  	var adicionar = $("#adicionar").val();
	  	var total2 = parseInt(adicionar)+1;
	  	$("#adicionar").val(total2);
	  	
	  	if($("#linhaAberta").val()!=0)
		{
			//editarRecebimento($("#linhaAberta").val());
		}
  	
	  	//Setar linha aberta para zero
	  	$("#linhaAberta").val(0);
  	
	  	if(total==1)
	    {
	    	document.getElementById("tabela").innerHTML ='';	
	    }
	  	
	  	// Obtendo o elemento tabela
	    var tab       = document.getElementById('tabela');
	    
	    // colocando borda na tabela:
	    tab.style.border = '1px solid black';
	    
	    // Obtendo a quantidade de linhas da tabela:
	    var qntLinhas = tab.rows.length;
	    
	    // Inserindo uma linha
	    var novaLinha = tab.insertRow( qntLinhas );
	    
	    // Escolhendo um id para evitar IDs duplicados
	    //var novaID = Math.floor((Math.random()*99999)+1);
	    // E definindo seu id
	    novaLinha.id  = 'linha'+proximo_id;
	    
	    // Como nosso elemento novaLinha eh a linha que acabamos de criar
	    // podemos adicionar celulas (colunas) no mesmo.
	    
	    // Criando a primeira coluna
	    var coluna1 = novaLinha.insertCell(0);
	    coluna1.id  = '1coluna'+proximo_id;
	    // Criando a segunda coluna
	    var coluna2 = novaLinha.insertCell(1);
	    coluna2.id  = '2coluna'+proximo_id;
	    // Criando a terceira coluna
	    var coluna3 = novaLinha.insertCell(2);
	    coluna3.id  = '3coluna'+proximo_id;
	    // Criando a quarta coluna
	    var coluna4 = novaLinha.insertCell(3);
	    coluna4.id  = '4coluna'+proximo_id;
	    // Criando a quinta coluna
	    var coluna5 = novaLinha.insertCell(4);
	    coluna5.id  = '5coluna'+proximo_id;
	    // Criando a sexta coluna
	    var coluna6 = novaLinha.insertCell(5);
	    
	    // Agora vamos criar os elementos que estarão dentro de cada coluna
	    coluna1.innerHTML = '<div id=\"1col'+proximo_id+'\"><input name=\"dataVerificacao'+proximo_id+'"\" class=\"mascaraData\" style=\"width:105%\" maxlength=\"10\" id=\"dataVerificacao'+proximo_id+'"\" type=\"text\" style=\"width: 102px\"></div>';
	    coluna2.innerHTML = '<div id=\"2col'+proximo_id+'\"><input type=\"text\" name=\"novasAfiliacoes'+proximo_id+'"\" onkeypress=\"return SomenteNumero(event)\" id=\"novasAfiliacoes'+proximo_id+'"\"></div>';
	    coluna3.innerHTML = '<div id=\"3col'+proximo_id+'\"><input type=\"text\" name=\"desligamentos'+proximo_id+'\" onkeypress=\"return SomenteNumero(event)\" id=\"desligamentos'+proximo_id+'\"></div>';
	    coluna4.innerHTML = '<div id=\"4col'+proximo_id+'\"><input type=\"text\" name=\"reintegrados'+proximo_id+'\"  onkeypress=\"return SomenteNumero(event)\" id=\"reintegrados'+proximo_id+'\"></div>';
	    coluna5.innerHTML = '<div id=\"5col'+proximo_id+'\"><input type=\"text\" name=\"numeroAtualMembrosAtivos'+proximo_id+'\"  onkeypress=\"return SomenteNumero(event)\" id=\"numeroAtualMembrosAtivos'+proximo_id+'\"></div>';
	    coluna6.innerHTML = '<div style=\'width:200\'><span id=\"botaoEditar'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"cadastrarMembrosRosacruzesAtivos(\''+proximo_id+'\');\"> Salvar</a></span> <span id=\"outros_botoes'+proximo_id+'\" style=\"display:none\"><a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRowRendimento(\''+proximo_id+'\');\"> Excluir</a></span></div>';
	  
	    $(".mascaraData").mask("99/99/9999");
	    
  	}else{
  		alert('Você já tem uma linha aberta. Cadastre essa entrada para acrescentar mais linhas.');
  	}
}
*/
function cadastrarMembrosRosacruzesAtivos(id)
{
	//alert('chamada no cadastro id:'+id);
	
    var dataVerificacao = $("#dataVerificacao").val();
    var membrosAtivosMesAnterior = parseInt($("#membrosAtivosMesAnterior").html());
    var novasAfiliacoes = parseInt($("#novasAfiliacoes").val());
    var desligamentos = parseInt($("#desligamentos").val());
    var reintegrados = parseInt($("#reintegrados").val());
    var numeroAtualMembrosAtivos = (membrosAtivosMesAnterior+novasAfiliacoes+reintegrados)-desligamentos;
    var usuario = $("#usuario").val();
    var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
   
    if(numeroAtualMembrosAtivos>0)
    {
        //Verificar se já existem registros
        $.ajax({
            type: "post",
            url: "js/ajax/validaMembrosRosacruzesAtivos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {dataVerificacao: dataVerificacao,
                fk_idOrganismoAfiliado: fk_idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}
            success: function (retorno2) {
                if(retorno2.status==false)
                {
                    $.ajax({
                        type: "post",
                        url: "js/ajax/cadastraMembrosRosacruzesAtivos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                        dataType: "json",
                        data: {dataVerificacao: dataVerificacao,
                            novasAfiliacoes: novasAfiliacoes,
                            desligamentos: desligamentos,
                            reintegrados: reintegrados,
                            numeroAtualMembrosAtivos: numeroAtualMembrosAtivos,
                            usuario: usuario,
                            fk_idOrganismoAfiliado: fk_idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}
                        success: function (retorno) {
                            //alert("depois de cadastrado id:"+id);
                            $("#novasAfiliacoes").val(novasAfiliacoes);
                            $("#desligamentos").val(desligamentos);
                            $("#reintegrados").val(reintegrados);
                            $("#numeroAtualMembrosAtivos").html(numeroAtualMembrosAtivos);
                            $("#proximoId").val(retorno.id);
                            $("#adicionar").val(0);
                            $("#outros_botoes"+id).show();

                            $("#botao").html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"editarMembrosRosacruzesAtivos(\'"+retorno.ultimoId+"\');\"> Salvar</a>");

                            return false;
                        },
                        error: function (xhr, textStatus, errorThrown) {

                        }
                    });
                }else{
                    swal({title:"Aviso!",text:"Já existe um registro com essa data de verificação!",type:"warning",confirmButtonColor:"#1ab394"});
                    return false;
                }
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });

    }else{
    	location.href='?corpo=buscaMembrosRosacruzesAtivos&zero=1';
    	return false;
    }
    
}
/*
function abriCamposEditarMembrosRosacruzesAtivos(id)
{
	if($("#linhaAberta").val()!=0&&$("#pote").val()>1)
	{
		//alert('vai editar recebimento com a linha aberta');
		//editarRecebimento($("#linhaAberta").val());
	}else{
		//alert('abrir linha de boa:'+id);
	}	
	
    $.ajax({
        type: "post",
        url: "js/ajax/getMembrosRosacruzesAtivosPorId.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$('#1td'+id).prop('onclick',null).off('click');
        	$('#2td'+id).prop('onclick',null).off('click');
        	$('#3td'+id).prop('onclick',null).off('click');
        	$('#4td'+id).prop('onclick',null).off('click');
        	$('#5td'+id).prop('onclick',null).off('click');
        	
        	$("#5col"+id).html("");//Apagar
        	
        	//alert(retorno);
        	$("#1col"+id).html("<input type=\"text\" name=\"dataVerificacao"+id+"\" class=\"mascaraData\" style=\"width:105%\" id=\"dataVerificacao"+id+"\" value=\""+retorno.dataVerificacao+"\">");
        	$("#2col"+id).html("<input type=\"text\" name=\"novasAfiliacoes"+id+"\" id=\"novasAfiliacoes"+id+"\" value=\""+retorno.novasAfiliacoes+"\">");
        	$("#3col"+id).html("<input type=\"text\" name=\"desligamentos"+id+"\" id=\"desligamentos"+id+"\" value=\""+retorno.desligamentos+"\">");
        	$("#4col"+id).html("<input type=\"text\" name=\"reintegrados"+id+"\" id=\"reintegrados"+id+"\" value=\""+retorno.reintegrados+"\">");
        	$("#5col"+id).html("<input type=\"text\" name=\"numeroAtualMembrosAtivos"+id+"\" id=\"numeroAtualMembrosAtivos"+id+"\" value=\""+retorno.numeroAtualMembrosAtivos+"\">");
        	
        	$("#botaoEditar"+id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"editarMembrosRosacruzesAtivos(\'"+id+"\');\"> Salvar</a>");
        	
        	$("#outros_botoes"+id).hide();
        	
        	//guardar nova linha aberta
        	$("#linhaAberta").val(id); 
        	
        	$(".mascaraData").mask("99/99/9999");
        	
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}
*/
function editarMembrosRosacruzesAtivos(id)
{
	
    var dataVerificacao = $("#dataVerificacao").val();
    var membrosAtivosMesAnterior = parseInt($("#membrosAtivosMesAnterior").html());
    var novasAfiliacoes = parseInt($("#novasAfiliacoes").val());
    var desligamentos = parseInt($("#desligamentos").val());
    var reintegrados = parseInt($("#reintegrados").val());
    var numeroAtualMembrosAtivos = (membrosAtivosMesAnterior+novasAfiliacoes+reintegrados)-desligamentos;
    var idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
    var usuario = $("#usuario").val();
    
    if(numeroAtualMembrosAtivos>0)
    {
    
	    $.ajax({
	        type: "post",
	        url: "js/ajax/editarMembrosRosacruzesAtivos.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
	        dataType: "json",
	        data: {
	        	id:id,
	        	dataVerificacao: dataVerificacao,
	        	novasAfiliacoes: novasAfiliacoes,
	        	desligamentos: desligamentos,
	        	reintegrados: reintegrados,
	        	numeroAtualMembrosAtivos: numeroAtualMembrosAtivos,
	        	idOrganismoAfiliado:idOrganismoAfiliado,
                        usuario:usuario
                    }, // exemplo: {codigo:125, nome:"Joaozinho"}  
	        success: function (retorno) {
	        	//alert(retorno);
	        	$("#novasAfiliacoes").val(novasAfiliacoes);
	        	$("#desligamentos").val(desligamentos);
	        	$("#reintegrados").val(reintegrados);
	        	$("#numeroAtualMembrosAtivos").html(numeroAtualMembrosAtivos);
	        	
	        	return false;
	      },
	        error: function (xhr, textStatus, errorThrown) {
	
	        }
	    });
    }else{
    	location.href='?corpo=buscaMembrosRosacruzesAtivos&zero=1';
    	return false;
    }
}

function deleteRowDivida(id){
	
	swal({
        title: "Você tem certeza?",
        text: "Que deseja excluir esse item?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sim, exclua!",
        cancelButtonText: "Não, cancele!",
        closeOnConfirm: false,
        closeOnCancel: false },
    function (isConfirm) {
        if (isConfirm) {
			var pote = $("#pote").val();
		  	var total = parseInt(pote)-1;
		  	$("#pote").val(total);
		  	
		  	$.ajax({
		        type: "post",
		        url: "js/ajax/excluirDivida.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
		        dataType: "json",
		        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
		        success: function (retorno) {
		        	
		        	$('#linha'+id).remove();
		        	swal({title:"Sucesso!",text:"Item excluído com sucesso!",type:"success",confirmButtonColor:"#1ab394"});
		        	$("#proximoId").val(retorno.proximo_id);	
		        	
		        	atualizaTotalGeralDivida($("#fk_idOrganismoAfiliado").val());
		        	
		        	return false;
		      },
		        error: function (xhr, textStatus, errorThrown) {
		
		        }
		    });
		}else{
			swal({title:"Cancelado",text:"Nós não excluímos esse item :)",type:"error",confirmButtonColor:"#1ab394"});
        }
    });	
}

function addRowDivida(proximo_id){
	proximo_id = $("#proximoId").val();
	//alert('proximo id:'+proximo_id);
	if(parseInt($("#adicionar").val())<1)
  	{
		var idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
		var pote = $("#pote").val();
	  	var total = parseInt(pote)+1;
	  	$("#pote").val(total);
	  	
	  	var adicionar = $("#adicionar").val();
	  	var total2 = parseInt(adicionar)+1;
	  	$("#adicionar").val(total2);
	  	
	  	if($("#linhaAberta").val()!=0)
		{
			//editarRecebimento($("#linhaAberta").val());
		}
  	
	  	//Setar linha aberta para zero
	  	$("#linhaAberta").val(0);
  	
	  	if(total==1)
	    {
	    	document.getElementById("tabela").innerHTML ='';	
	    }
	  	
	  	// Obtendo o elemento tabela
	    var tab       = document.getElementById('tabela');
	    
	    // colocando borda na tabela:
	    tab.style.border = '1px solid black';
	    
	    // Obtendo a quantidade de linhas da tabela:
	    var qntLinhas = tab.rows.length;
	    
	    // Inserindo uma linha
	    var novaLinha = tab.insertRow( qntLinhas );
	    
	    // Escolhendo um id para evitar IDs duplicados
	    //var novaID = Math.floor((Math.random()*99999)+1);
	    // E definindo seu id
	    novaLinha.id  = 'linha'+proximo_id;
	    
	    // Como nosso elemento novaLinha eh a linha que acabamos de criar
	    // podemos adicionar celulas (colunas) no mesmo.
	    
	    // Criando a primeira coluna
	    var coluna1 = novaLinha.insertCell(0);
	    coluna1.id  = '1coluna'+proximo_id;
	    // Criando a segunda coluna
	    var coluna2 = novaLinha.insertCell(1);
	    coluna2.id  = '2coluna'+proximo_id;
	    // Criando a terceira coluna
	    var coluna3 = novaLinha.insertCell(2);
	    coluna3.id  = '3coluna'+proximo_id;
	    // Criando a quarta coluna
	    var coluna4 = novaLinha.insertCell(3);
	    coluna4.id  = '4coluna'+proximo_id;
	    // Criando a quinta coluna
	    var coluna5 = novaLinha.insertCell(4);
	    coluna5.id  = '5coluna'+proximo_id;
	    // Criando a sexta coluna
	    var coluna6 = novaLinha.insertCell(5);
            coluna5.id  = '5coluna'+proximo_id;
	    
            var coluna7 = novaLinha.insertCell(6);
            
	    // Agora vamos criar os elementos que estarão dentro de cada coluna
	    coluna1.innerHTML = '<div id=\"1col'+proximo_id+'\"><input name=\"inicioDivida'+proximo_id+'"\" class=\"mascaraData\" style=\"width:105%\" maxlength=\"10\" id=\"inicioDivida'+proximo_id+'"\" type=\"text\" style=\"width: 102px\"></div>';
	    coluna2.innerHTML = '<div id=\"2col'+proximo_id+'\"><input type=\"text\" name=\"fimDivida'+proximo_id+'\" class=\"mascaraData\" style=\"width:105%\" id=\"fimDivida'+proximo_id+'\"></div>';
	    coluna3.innerHTML = '<div id=\"3col'+proximo_id+'\"><input type=\"text\" name=\"descricaoDivida'+proximo_id+'"\" id=\"descricaoDivida'+proximo_id+'"\"></div>';
	    coluna4.innerHTML = '<div id=\"4col'+proximo_id+'\"><input type=\"text\" name=\"valorDivida'+proximo_id+'\" class=\"currency\" onpaste=\"return false;\" style=\"width:105%\" id=\"valorDivida'+proximo_id+'\"></div>';
	    coluna5.innerHTML = '<div id=\"5col'+proximo_id+'\"><input type=\"text\" name=\"observacaoDivida'+proximo_id+'\" style=\"width:105%\" id=\"observacaoDivida'+proximo_id+'\"></div>';
            coluna6.innerHTML = '<div id=\"6col'+proximo_id+'\"><select name=\"categoriaDivida'+proximo_id+'\" id=\"categoriaDivida'+proximo_id+'\"><option value=\"0\">Selecione...</option><option value=\"4\">Água</option><option value=\"1\">Aluguel</option><option value=\"14\">Anúncios</option><option value=\"8\">Beneficência Social</option><option value=\"9\">Boletim</option><option value=\"16\">Cantina</option><option value=\"15\">Carta Constitutiva (GLP)</option><option value=\"2\">Comissões</option><option value=\"10\">Convenções</option><option value=\"13\">Despesas de Correio</option><option value=\"17\">Despesas Gerais</option><option value=\"20\">GLP – Outros</option><option value=\"19\">GLP – Pagamentos Suprimentos</option><option value=\"18\">GLP – Remessa de Trimestralidade</option><option value=\"21\">Investimentos</option><option value=\"22\">Impostos</option><option value=\"11\">Jornadas</option><option value=\"3\">Luz</option><option value=\"7\">Manutenção</option><option value=\"23\">Região</option><option value=\"12\">Reuniões Sociais</option><option value=\"6\">Tarifas</option><option value=\"5\">Telefone</option></select></div>';
	    coluna7.innerHTML = '<div style=\'width:100\'><span id=\"botaoEditar'+proximo_id+'\"><a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"cadastrarDivida(\''+proximo_id+'\');\"> Salvar</a></span> <span id=\"outros_botoes'+proximo_id+'\" style=\"display:none\"><a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRowDivida(\''+proximo_id+'\');\"> Excluir</a></span></div>';
	  
	    $(".mascaraData").mask("99/99/9999");
	    $(".currency").maskMoney({symbol:'R$ ', 
	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
	    
	    document.getElementById('inicioDivida'+proximo_id).focus();
	    
  	}else{
  		swal({title:"Aviso!",text:"Você já tem uma linha aberta. Cadastre essa entrada para acrescentar mais linhas.",type:"warning",confirmButtonColor:"#1ab394"});
  	}
}

function cadastrarDivida(id)
{
    //alert('chamada no cadastro id:'+id);
	
    var inicioDivida = $("#inicioDivida"+id).val();
    var fimDivida = $("#fimDivida"+id).val();
    var descricaoDivida = $("#descricaoDivida"+id).val();
    var valorDivida = $("#valorDivida"+id).val();
    var observacaoDivida = $("#observacaoDivida"+id).val();
    var categoriaDivida = $("#categoriaDivida"+id).val();
    var usuario = $("#usuario").val();
    var fk_idOrganismoAfiliado = $("#fk_idOrganismoAfiliado").val();
    var prazo = $("#prazo").val();
    var mesPrazo = $("#mesPrazo").val();
    var anoPrazo = $("#anoPrazo").val();
    
    if(inicioDivida=="")
    {
        swal({title: "Aviso!", text: "Informe a data de inicio da divida ", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if(fimDivida=="")
    {
        swal({title: "Aviso!", text: "Informe a data de fim da divida ", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if(descricaoDivida=="")
    {
        swal({title: "Aviso!", text: "Informe a descrição da divida ", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if(valorDivida==""||valorDivida=="0")
    {
        swal({title: "Aviso!", text: "Informe o valor da divida ", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    if(categoriaDivida==""||categoriaDivida=="0")
    {
        swal({title: "Aviso!", text: "Informe a categoria da divida ", type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }
    
    //Não deixar cadastrar dividas anteriores
    var dataFormatadaFimDivida = fimDivida.split("/")[2] +"-"+ fimDivida.split("/")[1]+"-" +fimDivida.split("/")[0];
    //alert(dataFormatadaFimDivida);
    var data_1 = new Date(dataFormatadaFimDivida);
    var data_2 = new Date(prazo);
    //alert(data_1);
    //alert(data_2);
    if(data_1<data_2)
    {
        swal({title: "Aviso!", text: "Não é possível cadastrar dívidas com fim anterior a "+mesPrazo+" de "+anoPrazo, type: "warning", confirmButtonColor: "#1ab394"});
        return false;
    }else{    
   
        $.ajax({
            type: "post",
            url: "js/ajax/cadastraDivida.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
            dataType: "json",
            data: {inicioDivida: inicioDivida,
                    fimDivida: fimDivida,
                    descricaoDivida: descricaoDivida,
                    valorDivida: valorDivida,
                    observacaoDivida: observacaoDivida,
                    categoriaDivida: categoriaDivida,
                    usuario: usuario,
                    fk_idOrganismoAfiliado: fk_idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
            success: function (retorno) {
                    //alert("depois de cadastrado id:"+id);
                    $("#1col"+id).html(inicioDivida);
                    $("#2col"+id).html(fimDivida);
                    $("#3col"+id).html(descricaoDivida);
                    $("#4col"+id).html(valorDivida);
                    $("#5col"+id).html(observacaoDivida);
                    $("#6col"+id).html(retorno.categoriaDivida);
                    $("#proximoId").val(retorno.id);
                    $("#adicionar").val(0);
                    $("#outros_botoes"+id).show();

                    $('#1td'+id).click(function(){ abriCamposEditarDivida(id); });
                    $('#2td'+id).click(function(){ abriCamposEditarDivida(id); });
                    $('#3td'+id).click(function(){ abriCamposEditarDivida(id); });
                    $('#4td'+id).click(function(){ abriCamposEditarDivida(id); });
                    $('#5td'+id).click(function(){ abriCamposEditarDivida(id); });
                    $('#6td'+id).click(function(){ abriCamposEditarDivida(id); });

                    $("#botaoEditar"+id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarDivida(\'"+retorno.ultimoId+"\');\"> Editar</a>");
                    $("#botaoExcluir"+id).html("<a href=\"#\" class=\'btn btn-xs btn-danger\' onclick=\"deleteRowDivida(\'"+retorno.ultimoId+"\');\"> Excluir</a>");
                    //$("#botaoRecibo"+id).html("<a href=\"#\" class=\'btn btn-xs btn-warning\' onclick=\"abrirPopUpReciboDespesa(\'"+retorno.ultimoId+"\',\'"+fk_idOrganismoAfiliado+"\');\"> Recibo</a>");

                    //Atualizar total geral
                    atualizaTotalGeralDivida($("#fk_idOrganismoAfiliado").val());

                    return false;
          },
            error: function (xhr, textStatus, errorThrown) {

            }
        });
    }    
}

function abriCamposEditarDivida(id)
{
	if($("#linhaAberta").val()!=0&&$("#pote").val()>1)
	{
		//alert('vai editar recebimento com a linha aberta');
		//editarRecebimento($("#linhaAberta").val());
	}else{
		//alert('abrir linha de boa:'+id);
	}	
	
    $.ajax({
        type: "post",
        url: "js/ajax/getDividaPorId.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {id: id}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$('#1td'+id).prop('onclick',null).off('click');
        	$('#2td'+id).prop('onclick',null).off('click');
        	$('#3td'+id).prop('onclick',null).off('click');
        	$('#4td'+id).prop('onclick',null).off('click');
        	$('#5td'+id).prop('onclick',null).off('click');
                $('#6td'+id).prop('onclick',null).off('click');
        	
        	$("#5col"+id).html("");//Apagar
        	
        	//alert(retorno);
        	$("#1col"+id).html("<input type=\"text\" name=\"inicioDivida"+id+"\" class=\"mascaraData\" style=\"width:105%\" id=\"inicioDivida"+id+"\" value=\""+retorno.inicioDivida+"\">");
        	$("#2col"+id).html("<input type=\"text\" name=\"fimDivida"+id+"\" class=\"mascaraData\" style=\"width:105%\" id=\"fimDivida"+id+"\" value=\""+retorno.fimDivida+"\">");
        	$("#3col"+id).html("<input type=\"text\" name=\"descricaoDivida"+id+"\" id=\"descricaoDivida"+id+"\" value=\""+retorno.descricaoDivida+"\">");
        	$("#4col"+id).html("<input type=\"text\" name=\"valorDivida"+id+"\" id=\"valorDivida"+id+"\" class=\"currency\" onpaste=\"return false;\" style=\"width:105%\" value=\""+retorno.valorDivida+"\">");
        	$("#5col"+id).html("<input type=\"text\" name=\"observacaoDivida"+id+"\" id=\"observacaoDivida"+id+"\" style=\"width:105%\" value=\""+retorno.observacaoDivida+"\">");
                $("#6col"+id).html("<select name=\"categoriaDivida"+id+"\" id=\"categoriaDivida"+id+"\"><option value=\"0\">Selecione...</option><option value=\"4\">Água</option><option value=\"1\">Aluguel</option><option value=\"14\">Anúncios</option><option value=\"8\">Beneficência Social</option><option value=\"9\">Boletim</option><option value=\"16\">Cantina</option><option value=\"15\">Carta Constitutiva (GLP)</option><option value=\"2\">Comissões</option><option value=\"10\">Convenções</option><option value=\"13\">Despesas de Correio</option><option value=\"17\">Despesas Gerais</option><option value=\"20\">GLP – Outros</option><option value=\"19\">GLP – Pagamentos Suprimentos</option><option value=\"18\">GLP – Remessa de Trimestralidade</option><option value=\"21\">Investimentos</option><option value=\"22\">Impostos</option><option value=\"11\">Jornadas</option><option value=\"3\">Luz</option><option value=\"7\">Manutenção</option><option value=\"23\">Região</option><option value=\"12\">Reuniões Sociais</option><option value=\"6\">Tarifas</option><option value=\"5\">Telefone</option></select>");
                $("#categoriaDivida"+id).val(retorno.categoriaDivida);
        	$("#botaoEditar"+id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"editarDivida(\'"+id+"\');\"> Salvar</a>");
        	
        	$("#outros_botoes"+id).hide();
        	
        	//guardar nova linha aberta
        	$("#linhaAberta").val(id); 
        	
        	$(".mascaraData").mask("99/99/9999");
        	$(".currency").maskMoney({symbol:'R$ ', 
    	    	showSymbol:false, thousands:'.', decimal:',', symbolStay: true});
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function editarDivida(id)
{
	
    var inicioDivida = $("#inicioDivida"+id).val();
    var fimDivida = $("#fimDivida"+id).val();
    var descricaoDivida = $("#descricaoDivida"+id).val();
    var valorDivida = $("#valorDivida"+id).val();
    var observacaoDivida = $("#observacaoDivida"+id).val();
    var categoriaDivida = $("#categoriaDivida"+id).val();
    var usuario = $("#usuario").val();
    
    $.ajax({
        type: "post",
        url: "js/ajax/editarDivida.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {
        	id:id,
        	inicioDivida: inicioDivida,
        	fimDivida: fimDivida,
        	descricaoDivida: descricaoDivida,
        	valorDivida: valorDivida,
        	observacaoDivida: observacaoDivida,
                categoriaDivida: categoriaDivida,
                usuario:usuario
        }, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$("#1col"+id).html(inicioDivida);
        	$("#2col"+id).html(fimDivida);
        	$("#3col"+id).html(descricaoDivida);
        	$("#4col"+id).html(valorDivida);
        	$("#5col"+id).html(observacaoDivida);
                $("#6col"+id).html(retorno.categoriaDivida);
        	$("#botaoEditar"+id).html("<a href=\"#\" class=\'btn btn-xs btn-primary\' onclick=\"abriCamposEditarDivida(\'"+id+"\');\"> Editar</a>");
        	
        	$('#1td'+id).click(function(){ abriCamposEditarDivida(id); });
        	$('#2td'+id).click(function(){ abriCamposEditarDivida(id); });
        	$('#3td'+id).click(function(){ abriCamposEditarDivida(id); });
        	$('#4td'+id).click(function(){ abriCamposEditarDivida(id); });
        	$('#5td'+id).click(function(){ abriCamposEditarDivida(id); });
                $('#6td'+id).click(function(){ abriCamposEditarDivida(id); });
        	
        	$("#outros_botoes"+id).show();
        	
        	//Atualizar total geral
        	atualizaTotalGeralDivida($("#fk_idOrganismoAfiliado").val());
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });
}

function atualizaTotalGeralDivida(idOrganismoAfiliado){
  	
  	$.ajax({
        type: "post",
        url: "js/ajax/atualizaTotalGeralDivida.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "json",
        data: {idOrganismoAfiliado:idOrganismoAfiliado}, // exemplo: {codigo:125, nome:"Joaozinho"}  
        success: function (retorno) {
        	
        	$('#totalGeral').html(retorno.totalGeral);
        	
        	return false;
      },
        error: function (xhr, textStatus, errorThrown) {

        }
    });

} 

function filtraOrganismos(search,regiao,corpo)
{
	$.ajax({  
		type:"post",  
		url: "js/ajax/filtraOrganismo.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
		dataType: "html",  
		data: {search:search,regiao:regiao,corpo:corpo}, // exemplo: {codigo:125, nome:"Joaozinho"}  
		success: function(data){
				$("#organismos").html(data);
		},
		error: function(xhr, textStatus, errorThrown){ 
			alert('erro');
		}  
	});	
}

function fecharModal()
{
	$(".modal").css("display", "none");
	$( ".modal-backdrop" ).css("display", "none");
}

function buscaPeloCep(campoCep,campoLogradouro,campoBairro,campoCidade,campoUF)
{
	var cep = $("#"+campoCep).val();
        
            $.ajax({
                type: "GET",
                url: 'http://cep.correiocontrol.com.br/' + cep + '.json',
                success: function (response) {
                    
                    $('#'+campoLogradouro).val(response.logradouro);
                    $('#'+campoBairro).val(response.bairro);
                    $('#'+campoCidade).val(response.uf);
                    $('#'+campoUF).val(response.localidade);
                },
                error: function(response) {
                    swal({title:"Aviso!",text:"Erro ao buscar cep",type:"warning",confirmButtonColor:"#1ab394"});
                    $("#"+campoCep).val('');
                },
                dataType: 'json'
            });
}



function pesquisaFonetica(campoResultado,campCodigoAfiliacao,campoPesquisa,campoCod,campoNome,campoHiddenSeq,campoHiddenNome,idModal,informacoesPesquisa,pegaTipoMembro,funcao)
{
    var tipo = 1;//R+C
    //var verificaCaptcha=true;
    var captcha=$('#g-recaptcha-response').val();
    var nomeMembro = "";
    var codigoAfiliacao = "";
    var chamaFuncao = "N";//R+C
    var insereCaptcha = "N";//R+C
    
                        
        if(campCodigoAfiliacao!="")
        {    
            codigoAfiliacao = $("#"+campCodigoAfiliacao).val();
        }
        if(campoPesquisa!="")
        {
            nomeMembro = $("#"+campoPesquisa).val();
        }
        if(pegaTipoMembro=="S")
        {
            var tipo = $("#tipo").val();
        }
        if(funcao!="")
        {
            chamaFuncao = funcao;
        }

        if($("#"+campCodigoAfiliacao).val()==""&&$("#"+campoPesquisa).val()=="")
        {
            swal({title:"Aviso!",text:"Preencha um dos campos de pesquisa, ou código de afiliação ou nome do membro!",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }

        
        //alert('---->:'+captcha);
        //location.href='./pega.php?'+captcha;
    /*    
    if(captcha==null)
    {
        retornaPF(campoResultado,codigoAfiliacao,nomeMembro,campoCod,campoNome,campoHiddenSeq,campoHiddenNome,idModal,tipo,chamaFuncao);
    }else{
        if(captcha=='')
        {
            fechaModal(idModal);
            swal({title:"Aviso!",text:"Demonstre que você não é um robô marcando o Captcha",type:"warning",confirmButtonColor:"#1ab394"});
            return false;
        }else{
            $.ajax({
                type:"post",  
                url: "js/ajax/validateCaptcha.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
                dataType: "json",  
                data: {captcha:captcha}, // exemplo: {codigo:125, nome:"Joaozinho"}  
                success: function(response){
                    //alert(response);
                    //var isTrueSet = (response.success == 'true');
                    //alert(response.success);
                    if(response.success)
                    { */
                        retornaPF(campoResultado,codigoAfiliacao,nomeMembro,campoCod,campoNome,campoHiddenSeq,campoHiddenNome,idModal,tipo,chamaFuncao);
                      /*  
                    }else{
                        swal({title:"Aviso!",text:"Tente novamente responder o Captcha",type:"warning",confirmButtonColor:"#1ab394"});
                        //return false;
                    } 
                },
                error: function(response) {
                    swal({title:"Aviso!",text:"Erro na verificação do captcha: "+response,type:"warning",confirmButtonColor:"#1ab394"});
                    //$("#"+campoCep).val('');
                }
            });
        }
    }
    */ 
}

function retornaPF(campoResultado,codigoAfiliacao,nomeMembro,campoCod,campoNome,campoHiddenSeq,campoHiddenNome,idModal,tipo,chamaFuncao)
{
    $.ajax({  
        type:"post",  
        url: "js/ajax/getResultadoPesquisaFonetica.php", // exemplo: "/meuProjeto/queroPegarOsDados.php"
        dataType: "html",  
        data: {
            codigoAfiliacao:codigoAfiliacao,
            nomeMembro:nomeMembro,
            campoCod:campoCod,
            campoNome:campoNome,
            campoHiddenSeq:campoHiddenSeq,
            campoHiddenNome:campoHiddenNome,
            idModal:idModal,
            tipo:tipo,
            chamaFuncao:chamaFuncao
        }, 
        success: function (response) {
            $('#'+campoResultado).html(response);     
        },
        error: function(response) {
            swal({title:"Aviso!",text:"Erro ao buscar membro durante a pesquisa: "+response,type:"warning",confirmButtonColor:"#1ab394"});
        }
    });
}

function carregaCamposPesquisaFonetica(campoHiddenSeq,campoHiddenNome,valorCampoHiddenSeq,valorCampoHiddenNome,campoCod,valorCod,campoNome,idModal,hasTitular)
{
    if(campoHiddenSeq!="")
    {
        if($("#"+campoHiddenSeq).is( "input" ))
        {    
            document.getElementById(campoHiddenSeq).value=valorCampoHiddenSeq;
        }else{
            document.getElementById(campoHiddenSeq).innerHTML=valorCampoHiddenSeq;
        }
    }
    if(campoHiddenNome!="")
    {
        document.getElementById(campoHiddenNome).value=valorCampoHiddenNome;
    }
    if(campoCod!="")
    {
        document.getElementById(campoCod).value=valorCod;
    }
    if(campoNome!="")
    {    
        document.getElementById(campoNome).value=valorCampoHiddenNome;
    }
    if($("#principalCompanheiro").length){
        document.getElementById('principalCompanheiro').value=hasTitular;
    }
    fechaModal(idModal);
}

function fechaModal(id)
{
    $('#'+id).modal('toggle');
}

//Bloquear botão direito
function click() {
    if (event.button==2||event.button==3) {
    oncontextmenu='return false';
    }
}

function limparCamposNomeCodigoAfiliacao()
{
    $('#codigoAfiliacao').val("");
    $('#nome').val("");
    document.getElementById("codigoAfiliacao").focus();
}

function removerBugMenu()
{
    $("#side-menu ul").html().replace(/[\u200B]/g, '');
}

function marcardesmarcar(){
  $('.marcar').each(
         function(){
           if ($(this).prop( "checked")) 
           $(this).prop("checked", false);
           else $(this).prop("checked", true);               
         }
    );
}

function openWinFB() {
        window.open("facebook.php", "myWindow", "width=800,height=600");
}