<?php
@include_once("model/departamentoClass.php");

@include_once("../model/departamentoClass.php");

class departamentoController {

    private $departamento;

    public function __construct() {

        $this->departamento = new Departamento();
    }

    public function cadastroDepartamento() {

        $this->departamento->setNomeDepartamento($_POST["nomeDepartamento"]);
        $this->departamento->setDescricaoDepartamento($_POST["descricaoDepartamento"]);
        $this->departamento->setVinculoExterno($_POST["vinculoExterno"]);

        if ($this->departamento->cadastraDepartamento()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaDepartamento&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse departamento!');
		  window.location = '../painelDeControle.php?corpo=cadastroDepartamento';
		  </script>";
        }
    }

    public function criarComboBox($id = 0) {
        $resultado = $this->departamento->listaDepartamento(1);
        $script = "";

        if ($resultado) {
            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idDepartamento"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idDepartamento'] . '" ' . $selecionado . '>' . $vetor["nomeDepartamento"] . '</option>';
                $script .= "descricaoDepartamento[" . $vetor['idDepartamento'] . "] = '" . $vetor['descricaoDepartamento'] . "';";
                $script .= "vinculoExterno[" . $vetor['idDepartamento'] . "] = '" . $vetor['vinculoExterno'] . "';";
            }
            return $script;
        }
    }

 	public function listaDepartamento() {
        if (!isset($_POST['nomeDepartamento'])) {
            $resultado = $this->departamento->listaDepartamento();
        } else {
            $this->departamento->setNomeDepartamento($_POST['nomeDepartamento']);
            $resultado = $this->departamento->buscaNomeDepartamento();
        }
        return $resultado;
    }
    

    public function buscaDepartamento() {

        $resultado = $this->departamento->buscarIdDepartamento();

        if ($resultado) {

            foreach ($resultado as $vetor) {

                $this->departamento->setNomeDepartamento($vetor['nomeDepartamento']);
                $this->departamento->setDescricaoDepartamento($vetor['descricaoDepartamento']);
                $this->departamento->setVinculoExterno($vetor['vinculoExterno']);
            }

            return $this->departamento;
        } else {
            return false;
        }
    }

    public function alteraDepartamento() {

        $this->departamento->setNomeDepartamento($_POST["nomeDepartamento"]);
        $this->departamento->setDescricaoDepartamento($_POST["descricaoDepartamento"]);
        $this->departamento->setVinculoExterno($_POST["vinculoExterno"]);

        if ($this->departamento->alteraDepartamento($_POST['idDepartamento'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaDepartamento&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar este departamento!');
			window.location = '../painelDeControle.php?corpo=alteraDepartamento.php&id=" . $_POST['idDepartamento'] . "';
			</script>";
        }
    }

    public function removeDepartamento($id) {
        $resultado = $this->departamento->removeDepartamento($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Departamento excluido com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaDepartamento&id=" . $id . "';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir este departamento!');
				window.location = '../painelDeControle.php?corpo=alterarDepartamento&id=" . $id . "';
				</script>";
        }
    }

}
?>