<?php

@include_once("model/estatutoClass.php");
@include_once("../model/estatutoClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/regiaoRosacruzClass.php");
@include_once("../model/regiaoRosacruzClass.php");

@include_once ("../model/criaSessaoClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class estatutoController {

	private $estatuto;
	private $sessao;
        private $usuario;
        private $regiao;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->estatuto 	= new estatuto();
		$this->usuario		= new Usuario();
                $this->regiao		= new regiaoRosacruz();
                
	}

	public function cadastroEstatuto() {

	    $arrTipos = explode(",",$_POST['tipos']);

		$this->estatuto->setFkidOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
        $this->estatuto->setNumeroOficio($_POST['numeroOficio']);
        $this->estatuto->setCidadeOficio($_POST['cidadeOficio']);
        $this->estatuto->setUfOficio($_POST['ufOficio']);
        $this->estatuto->setDataAssembleiaGeral(substr($_POST['dataAssembleiaGeral'],6,4)."-".substr($_POST['dataAssembleiaGeral'],3,2)."-".substr($_POST['dataAssembleiaGeral'],0,2));
        $this->estatuto->setNumeroPresentes($_POST['numeroPresentes']);
        $this->estatuto->setNomeAdvogado($_POST['nomeAdvogado']);
        $this->estatuto->setNumeroOAB($_POST['numeroOAB']);

        if(in_array("P",$arrTipos)) {
            $this->estatuto->setCartorioPronaos($_POST['cartorioPronaos']);
            $this->estatuto->setComarcaPronaos($_POST['comarcaPronaos']);
            $this->estatuto->setNumeroPronaos($_POST['numeroPronaos']);
            $this->estatuto->setNumeroAverbacaoPronaos($_POST['numeroAverbacaoPronaos']);
            $this->estatuto->setNumeroLeiMunicipalPronaos($_POST['numeroLeiMunicipalPronaos']);
            $this->estatuto->setDataLeiMunicipalPronaos(substr($_POST['dataLeiMunicipalPronaos'], 6, 4) . "-" . substr($_POST['dataLeiMunicipalPronaos'], 3, 2) . "-" . substr($_POST['dataLeiMunicipalPronaos'], 0, 2));
            $this->estatuto->setNumeroLeiEstadualPronaos($_POST['numeroLeiEstadualPronaos']);
            $this->estatuto->setDataLeiEstadualPronaos(substr($_POST['dataLeiEstadualPronaos'], 6, 4) . "-" . substr($_POST['dataLeiEstadualPronaos'], 3, 2) . "-" . substr($_POST['dataLeiEstadualPronaos'], 0, 2));
        }
        if(in_array("C",$arrTipos)) {
            $this->estatuto->setCartorioCapitulo($_POST['cartorioCapitulo']);
            $this->estatuto->setComarcaCapitulo($_POST['comarcaCapitulo']);
            $this->estatuto->setNumeroCapitulo($_POST['numeroCapitulo']);
            $this->estatuto->setNumeroAverbacaoCapitulo($_POST['numeroAverbacaoCapitulo']);
            $this->estatuto->setNumeroLeiMunicipalCapitulo($_POST['numeroLeiMunicipalCapitulo']);
            $this->estatuto->setDataLeiMunicipalCapitulo(substr($_POST['dataLeiMunicipalCapitulo'], 6, 4) . "-" . substr($_POST['dataLeiMunicipalCapitulo'], 3, 2) . "-" . substr($_POST['dataLeiMunicipalCapitulo'], 0, 2));
            $this->estatuto->setNumeroLeiEstadualCapitulo($_POST['numeroLeiEstadualCapitulo']);
            $this->estatuto->setDataLeiEstadualCapitulo(substr($_POST['dataLeiEstadualCapitulo'], 6, 4) . "-" . substr($_POST['dataLeiEstadualCapitulo'], 3, 2) . "-" . substr($_POST['dataLeiEstadualCapitulo'], 0, 2));
        }
        if(in_array("L",$arrTipos)) {
            $this->estatuto->setCartorioLoja($_POST['cartorioLoja']);
            $this->estatuto->setComarcaLoja($_POST['comarcaLoja']);
            $this->estatuto->setNumeroLoja($_POST['numeroLoja']);
            $this->estatuto->setNumeroAverbacaoLoja($_POST['numeroAverbacaoLoja']);
            $this->estatuto->setNumeroLeiMunicipalLoja($_POST['numeroLeiMunicipalLoja']);
            $this->estatuto->setDataLeiMunicipalLoja(substr($_POST['dataLeiMunicipalLoja'], 6, 4) . "-" . substr($_POST['dataLeiMunicipalLoja'], 3, 2) . "-" . substr($_POST['dataLeiMunicipalLoja'], 0, 2));
            $this->estatuto->setNumeroLeiEstadualLoja($_POST['numeroLeiEstadualLoja']);
            $this->estatuto->setDataLeiEstadualLoja(substr($_POST['dataLeiEstadualLoja'], 6, 4) . "-" . substr($_POST['dataLeiEstadualLoja'], 3, 2) . "-" . substr($_POST['dataLeiEstadualLoja'], 0, 2));
        }

		$this->estatuto->setUsuario($this->sessao->getValue("seqCadast"));

		//Cadastrar estatuto e pegar ultimo id inserido
		if(!$this->estatuto->verificaSeJaExiste())
		{
			$ultimo_id = $this->estatuto->cadastroEstatuto($arrTipos);

                        echo "<script type='text/javascript'>
                                            window.location = '../painelDeControle.php?corpo=buscaEstatuto&salvo=1';
                            </script>";
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaEstatuto&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaEstatuto() {
		$retorno = $this->estatuto->listaEstatuto();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaEstatuto($idEstatuto) {

		$resultado = $this->estatuto->buscaIdEstatuto($idEstatuto);

		if ($resultado) {

			foreach ($resultado as $vetor) {

				$this->estatuto->setIdEstatuto($vetor['idEstatuto']);
				$this->estatuto->setFkidOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
                $this->estatuto->setNumeroOficio($vetor['numeroOficio']);
                $this->estatuto->setCidadeOficio($vetor['cidadeOficio']);
                $this->estatuto->setUfOficio($vetor['ufOficio']);
                $this->estatuto->setDataAssembleiaGeral($vetor['dataAssembleiaGeral']);
                $this->estatuto->setNumeroPresentes($vetor['numeroPresentes']);
                $this->estatuto->setNomeAdvogado($vetor['nomeAdvogado']);
                $this->estatuto->setNumeroOAB($vetor['numeroOAB']);

                $this->estatuto->setCartorioPronaos($vetor['cartorioPronaos']);
                $this->estatuto->setComarcaPronaos($vetor['comarcaPronaos']);
                $this->estatuto->setNumeroPronaos($vetor['numeroPronaos']);
                $this->estatuto->setNumeroAverbacaoPronaos($vetor['numeroAverbacaoPronaos']);
                $this->estatuto->setNumeroLeiMunicipalPronaos($vetor['numeroLeiMunicipalPronaos']);
                $this->estatuto->setDataLeiMunicipalPronaos(substr($vetor['dataLeiMunicipalPronaos'],8,2)."/".substr($vetor['dataLeiMunicipalPronaos'],5,2)."/".substr($vetor['dataLeiMunicipalPronaos'],0,4));
                $this->estatuto->setNumeroLeiEstadualPronaos($vetor['numeroLeiEstadualPronaos']);
                $this->estatuto->setDataLeiEstadualPronaos(substr($vetor['dataLeiEstadualPronaos'],8,2)."/".substr($vetor['dataLeiEstadualPronaos'],5,2)."/".substr($vetor['dataLeiEstadualPronaos'],0,4));

                $this->estatuto->setCartorioCapitulo($vetor['cartorioCapitulo']);
                $this->estatuto->setComarcaCapitulo($vetor['comarcaCapitulo']);
                $this->estatuto->setNumeroCapitulo($vetor['numeroCapitulo']);
                $this->estatuto->setNumeroAverbacaoCapitulo($vetor['numeroAverbacaoCapitulo']);
                $this->estatuto->setNumeroLeiMunicipalCapitulo($vetor['numeroLeiMunicipalCapitulo']);
                $this->estatuto->setDataLeiMunicipalCapitulo(substr($vetor['dataLeiMunicipalCapitulo'],8,2)."/".substr($vetor['dataLeiMunicipalCapitulo'],5,2)."/".substr($vetor['dataLeiMunicipalCapitulo'],0,4));
                $this->estatuto->setNumeroLeiEstadualCapitulo($vetor['numeroLeiEstadualCapitulo']);
                $this->estatuto->setDataLeiEstadualCapitulo(substr($vetor['dataLeiEstadualCapitulo'],8,2)."/".substr($vetor['dataLeiEstadualCapitulo'],5,2)."/".substr($vetor['dataLeiEstadualCapitulo'],0,4));

                $this->estatuto->setCartorioLoja($vetor['cartorioLoja']);
                $this->estatuto->setComarcaLoja($vetor['comarcaLoja']);
                $this->estatuto->setNumeroLoja($vetor['numeroLoja']);
                $this->estatuto->setNumeroAverbacaoLoja($vetor['numeroAverbacaoLoja']);
                $this->estatuto->setNumeroLeiMunicipalLoja($vetor['numeroLeiMunicipalLoja']);
                $this->estatuto->setDataLeiMunicipalLoja(substr($vetor['dataLeiMunicipalLoja'],8,2)."/".substr($vetor['dataLeiMunicipalLoja'],5,2)."/".substr($vetor['dataLeiMunicipalLoja'],0,4));
                $this->estatuto->setNumeroLeiEstadualLoja($vetor['numeroLeiEstadualLoja']);
                $this->estatuto->setDataLeiEstadualLoja(substr($vetor['dataLeiEstadualLoja'],8,2)."/".substr($vetor['dataLeiEstadualLoja'],5,2)."/".substr($vetor['dataLeiEstadualLoja'],0,4));

                $this->estatuto->setUsuario($vetor['usuario']);
				

			}
			return $this->estatuto;
		} else {
			return false;
		}
	}
	

	public function alteraEstatuto() {
            
        $idEstatuto = $_POST['fk_idEstatuto'];
        $arrTipos = explode(",",$_POST['tipos']);

        $this->estatuto->setNumeroOficio($_POST['numeroOficio']);
        $this->estatuto->setCidadeOficio($_POST['cidadeOficio']);
        $this->estatuto->setUfOficio($_POST['ufOficio']);
        $this->estatuto->setDataAssembleiaGeral(substr($_POST['dataAssembleiaGeral'],6,4)."-".substr($_POST['dataAssembleiaGeral'],3,2)."-".substr($_POST['dataAssembleiaGeral'],0,2));
        $this->estatuto->setNumeroPresentes($_POST['numeroPresentes']);
        $this->estatuto->setNomeAdvogado($_POST['nomeAdvogado']);
        $this->estatuto->setNumeroOAB($_POST['numeroOAB']);

        if(in_array("P",$arrTipos)) {
            $this->estatuto->setCartorioPronaos($_POST['cartorioPronaos']);
            $this->estatuto->setComarcaPronaos($_POST['comarcaPronaos']);
            $this->estatuto->setNumeroPronaos($_POST['numeroPronaos']);
            $this->estatuto->setNumeroAverbacaoPronaos($_POST['numeroAverbacaoPronaos']);
            $this->estatuto->setNumeroLeiMunicipalPronaos($_POST['numeroLeiMunicipalPronaos']);
            $this->estatuto->setDataLeiMunicipalPronaos(substr($_POST['dataLeiMunicipalPronaos'], 6, 4) . "-" . substr($_POST['dataLeiMunicipalPronaos'], 3, 2) . "-" . substr($_POST['dataLeiMunicipalPronaos'], 0, 2));
            $this->estatuto->setNumeroLeiEstadualPronaos($_POST['numeroLeiEstadualPronaos']);
            $this->estatuto->setDataLeiEstadualPronaos(substr($_POST['dataLeiEstadualPronaos'], 6, 4) . "-" . substr($_POST['dataLeiEstadualPronaos'], 3, 2) . "-" . substr($_POST['dataLeiEstadualPronaos'], 0, 2));
        }
        if(in_array("C",$arrTipos)) {
            $this->estatuto->setCartorioCapitulo($_POST['cartorioCapitulo']);
            $this->estatuto->setComarcaCapitulo($_POST['comarcaCapitulo']);
            $this->estatuto->setNumeroCapitulo($_POST['numeroCapitulo']);
            $this->estatuto->setNumeroAverbacaoCapitulo($_POST['numeroAverbacaoCapitulo']);
            $this->estatuto->setNumeroLeiMunicipalCapitulo($_POST['numeroLeiMunicipalCapitulo']);
            $this->estatuto->setDataLeiMunicipalCapitulo(substr($_POST['dataLeiMunicipalCapitulo'], 6, 4) . "-" . substr($_POST['dataLeiMunicipalCapitulo'], 3, 2) . "-" . substr($_POST['dataLeiMunicipalCapitulo'], 0, 2));
            $this->estatuto->setNumeroLeiEstadualCapitulo($_POST['numeroLeiEstadualCapitulo']);
            $this->estatuto->setDataLeiEstadualCapitulo(substr($_POST['dataLeiEstadualCapitulo'], 6, 4) . "-" . substr($_POST['dataLeiEstadualCapitulo'], 3, 2) . "-" . substr($_POST['dataLeiEstadualCapitulo'], 0, 2));
        }
        if(in_array("L",$arrTipos)) {
            $this->estatuto->setCartorioLoja($_POST['cartorioLoja']);
            $this->estatuto->setComarcaLoja($_POST['comarcaLoja']);
            $this->estatuto->setNumeroLoja($_POST['numeroLoja']);
            $this->estatuto->setNumeroAverbacaoLoja($_POST['numeroAverbacaoLoja']);
            $this->estatuto->setNumeroLeiMunicipalLoja($_POST['numeroLeiMunicipalLoja']);
            $this->estatuto->setDataLeiMunicipalLoja(substr($_POST['dataLeiMunicipalLoja'], 6, 4) . "-" . substr($_POST['dataLeiMunicipalLoja'], 3, 2) . "-" . substr($_POST['dataLeiMunicipalLoja'], 0, 2));
            $this->estatuto->setNumeroLeiEstadualLoja($_POST['numeroLeiEstadualLoja']);
            $this->estatuto->setDataLeiEstadualLoja(substr($_POST['dataLeiEstadualLoja'], 6, 4) . "-" . substr($_POST['dataLeiEstadualLoja'], 3, 2) . "-" . substr($_POST['dataLeiEstadualLoja'], 0, 2));
        }

        $this->estatuto->setUltimoAtualizar($this->sessao->getValue("seqCadast"));

		//Cadastrar estatuto e pegar ultimo id inserido
		if(!$this->estatuto->verificaSeJaExiste())
		{
			$this->estatuto->atualizaEstatuto($idEstatuto,$arrTipos);

                        echo "<script type='text/javascript'>
                                            window.location = '../painelDeControle.php?corpo=buscaEstatuto&salvo=1';
                            </script>";
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaEstatuto&jaCadastrado=1';
			 	</script>";
		}
	}

}

?>