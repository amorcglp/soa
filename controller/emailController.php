<?php

@include_once("model/emailClass.php");
@include_once("../model/emailClass.php");

@include_once("model/emailDeParaClass.php");
@include_once("../model/emailDeParaClass.php");

@include_once("model/emailAnexoClass.php");
@include_once("../model/emailAnexoClass.php");

//    @include_once ("../model/criaSessaoClass.php");


class emailController {

	private $email;
	private $emailDePara;
	private $emailAnexo;
	private $sessao;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->email		= new Email();
		$this->emailDePara	= new EmailDePara();
		$this->emailAnexo	= new emailAnexo();
		
	}

	public function alteraEmail() {
		
		$return=false;
		
		//Atualizar o email preenchendo o assunto e a mensagem
		$this->email->setIdEmail($_POST['fk_idEmail']);
		$this->email->setAssunto($_POST['assunto']);
		$this->email->setMensagem($_POST['mensagemEmail']);
		$this->email->atualizaEmail();
		
		//Fazer uma cópia para enviados
		$this->email->setDe($this->sessao->getValue("seqCadast"));
		$ultimo_id = $this->email->cadastraEmail();
		$this->email->setIdEmail($ultimo_id);
		$this->email->setAssunto($_POST['assunto']);
		$this->email->setMensagem($_POST['mensagemEmail']);
		$this->email->atualizaEmail();
		
		$this->emailDePara	= new EmailDePara();
		$this->emailDePara->setFkIdEmail($ultimo_id);
		$this->emailDePara->setPara(0);
		$this->emailDePara->setPastaInterna(2);
		$this->emailDePara->setNova(0);
		$this->emailDePara->setEnviado(1);
		$this->emailDePara->setVinculoEmailEnviado($_POST['fk_idEmail']);
		$return = $this->emailDePara->cadastraEmailDePara();
		
		//Copiar os anexos também para o email enviado
		$retornoAnexo = $this->emailAnexo->listaEmailAnexo($_POST['fk_idEmail']);
		if($retornoAnexo)
		{
			if(count($retornoAnexo))
			{
				foreach ($retornoAnexo as $vetorAnexo)
				{
					$this->emailAnexo->setFkIdEmail($ultimo_id);
					$this->emailAnexo->setCaminho($vetorAnexo['caminho']);
					$this->emailAnexo->setNomeArquivo($vetorAnexo['nome_arquivo']);
					$this->emailAnexo->cadastroEmailAnexo();
				}
			}
		}
		
		//Cadastrar Destinatários
		$arrDestinatarios=explode(",",$_POST["para"]);
		//echo "<pre>";print_r($arrDestinatarios);exit();
		
		for ($i=0;$i<count($arrDestinatarios);$i++)
		{
			$this->emailDePara	= new EmailDePara();
			$this->emailDePara->setFkIdEmail($_POST['fk_idEmail']);
			$this->emailDePara->setPara($arrDestinatarios[$i]);
			$this->emailDePara->setPastaInterna(1);
			$this->emailDePara->setNova(1);
			$this->emailDePara->setEnviado(0);
			$this->emailDePara->setVinculoEmailEnviado(0);
			$return = $this->emailDePara->cadastraEmailDePara();
		}
		
		if ($return) {

			echo "<script type='text/javascript'>
		   			window.location = '../painelDeControle.php?corpo=buscaEmail&salvo=1';
		 	</script>";
		} else {
			echo "<script type='text/javascript'>
                    alert('Não foi possível enviar este e-mail!');
                    window.location = '../painelDeControle.php?corpo=comporEmail';
		  </script>";
		}
	}

	public function listaEmail($pastaInterna=null,$seqCadast=null,$search=null) {
		if($pastaInterna==null)
		{
			$retorno = $this->email->listaEmail(null,$seqCadast);	
		}else{
			$retorno = $this->email->listaEmail($pastaInterna,$seqCadast,$search);
		}

		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function limparEmails() {

		$resultado = $this->email->limparEmails();
	}

}

?>