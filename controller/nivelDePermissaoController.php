<?php
@include_once("model/nivelDePermissaoClass.php");

@include_once("../model/nivelDePermissaoClass.php");

class nivelDePermissaoController {

    private $nivelDePermissao;

    public function __construct() {

        $this->nivelDePermissao = new NivelDePermissao();
    }

    public function cadastraNivelDePermissao() {

        $this->nivelDePermissao->setNivelDePermissao($_POST["nivelDePermissao"]);
        $this->nivelDePermissao->setDescricaoNivelDePermissao($_POST["descricaoNivelDePermissao"]);

        if ($this->nivelDePermissao->cadastraNivelDePermissao()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaNivelDePermissao&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse N�vel!');
		  		window.location = '../painelDeControle.php?corpo=buscaNivelDePermissao';
		  </script>";
        }
    }

 	public function criarComboBox($id = 0) {
        $resultado = $this->nivelDePermissao->listaNivelDePermissao();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idNivelDePermissao"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idNivelDePermissao'] . '" ' . $selecionado . '>' . $vetor["nomeNivelDePermissao"] . '</option>';
            }
        }
    }

    public function listaNivelDePermissao() {
        
        $resultado = $this->nivelDePermissao->listaNivelDePermissao();
        return $resultado;
    }

    public function buscaNivelDePermissao() {

        $resultado = $this->nivelDePermissao->buscaIdNivelDePermissao();

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->nivelDePermissao->setIdNivelDePermissao($vetor['idNivelDePermissao']);
            	$this->nivelDePermissao->setNivelDePermissao($vetor['nivelDePermissao']);
            	$this->nivelDePermissao->setDescricaoNivelDePermissao($vetor['descricaoNivelDePermissao']);
            }

            return $this->nivelDePermissao;
        } else {
            return false;
        }
    }

    public function alteraNivelDePermissao() {

        $this->nivelDePermissao->setNivelDePermissao($_POST["nivelDePermissao"]);
        $this->nivelDePermissao->setDescricaoNivelDePermissao($_POST["descricaoNivelDePermissao"]);
        

        if ($this->nivelDePermissao->alteraNivelDePermissao($_POST['idNivelDePermissao'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaNivelDePermissao&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N�o foi poss�vel alterar este N�vel!');
			window.location = '../painelDeControle.php?corpo=buscaNivelDePermissao';
			</script>";
        }
    }

	public function removeNivelDePermissao($id) {
        $resultado = $this->nivelDePermissao->removeNivelDePermissao($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('N�vel exclu�do com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaNivelDePermissao';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir este N�vel!');
				window.location = '../painelDeControle.php?corpo=buscaNivelDePermissao';
				</script>";
        }
    }

}
?>