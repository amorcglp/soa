<?php

@include_once("../model/atividadeEstatutoClass.php");
@include_once("model/atividadeEstatutoClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class atividadeEstatutoController {

    private $atividadeEstatuto;

    public function __construct() {

        $this->atividadeEstatuto = new AtividadeEstatuto();
    }

    public function cadastroAtividadeEstatuto() {

        $this->atividadeEstatuto->setComplementoNomeAtividadeEstatuto($_POST["complementoNomeAtividadeEstatuto"]);
        $this->atividadeEstatuto->setDescricaoAtividadeEstatuto($_POST["descricaoAtividadeEstatuto"]);
        $this->atividadeEstatuto->setLocalAtividadeEstatuto($_POST["localAtividadeEstatuto"]);
        $this->atividadeEstatuto->setDataAtividadeEstatuto(substr($_POST["dataAtividadeEstatuto"],6,4)."-".substr($_POST["dataAtividadeEstatuto"],3,2)."-".substr($_POST["dataAtividadeEstatuto"],0,2));
        $this->atividadeEstatuto->setHoraAtividadeEstatuto($_POST["horaAtividadeEstatuto"].':00');
        $this->atividadeEstatuto->setTituloDiscursoAtividadeEstatuto($_POST["tituloDiscursoAtividadeEstatuto"]);
        $this->atividadeEstatuto->setNomeOradorDiscursoAtividadeEstatuto($_POST["nomeOradorDiscursoAtividadeEstatuto"]);
        $this->atividadeEstatuto->setFkIdTipoAtividadeEstatuto($_POST["fk_idTipoAtividadeEstatuto"]);
        $this->atividadeEstatuto->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
        $this->atividadeEstatuto->setFkIdOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);

        //echo "<pre>"; print_r($_POST); echo "</pre>";

        if ($this->atividadeEstatuto->cadastroAtividadeEstatuto()) {

            echo "<script type='text/javascript'>
                    alert('Atividade cadastrado com sucesso!');
		    		window.location = '../painelDeControle.php?corpo=buscaAtividadeEstatuto';
		  		</script>";

        } else {

            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade!');
                    window.location = '../painelDeControle.php?corpo=cadastroatividadeEstatuto';
		  		</script>";

        }
    }
    
	public function alteraAtividadeEstatuto() {

        $this->atividadeEstatuto->setComplementoNomeAtividadeEstatuto($_POST["complementoNomeAtividadeEstatuto"]);
        $this->atividadeEstatuto->setDescricaoAtividadeEstatuto($_POST["descricaoAtividadeEstatuto"]);
        $this->atividadeEstatuto->setLocalAtividadeEstatuto($_POST["localAtividadeEstatuto"]);
        $this->atividadeEstatuto->setDataAtividadeEstatuto(substr($_POST["dataAtividadeEstatuto"],6,4)."-".substr($_POST["dataAtividadeEstatuto"],3,2)."-".substr($_POST["dataAtividadeEstatuto"],0,2));
        $this->atividadeEstatuto->setHoraAtividadeEstatuto($_POST["horaAtividadeEstatuto"].':00');
        $this->atividadeEstatuto->setTituloDiscursoAtividadeEstatuto($_POST["tituloDiscursoAtividadeEstatuto"]);
        $this->atividadeEstatuto->setNomeOradorDiscursoAtividadeEstatuto($_POST["nomeOradorDiscursoAtividadeEstatuto"]);
        $this->atividadeEstatuto->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);

        if ($this->atividadeEstatuto->alteraAtividadeEstatuto($_POST['idAtividadeEstatuto'])) {

            echo
            "<script type='text/javascript'>
			alert ('Atividade alterada com sucesso!');
			window.location = '../painelDeControle.php?corpo=buscaAtividadeEstatuto';
			</script>";

        } else {

            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar a Atividade!');
			window.location = '../painelDeControle.php?corpo=alteraAtividadeEstatuto&idAtividadeEstatuto=" . $_POST['idAtividadeEstatuto'] . "';
			</script>";

        }
    }

    public function buscaAtividadeEstatuto($idAtividadeEstatuto) {

        $resultado = $this->atividadeEstatuto->buscaIdAtividadeEstatuto($idAtividadeEstatuto);
        if ($resultado) {
            foreach ($resultado as $vetor) {
                //echo "<pre>";print_r($vetor);
                $this->atividadeEstatuto->setComplementoNomeAtividadeEstatuto($vetor["complementoNomeAtividadeEstatuto"]);
                $this->atividadeEstatuto->setDescricaoAtividadeEstatuto($vetor["descricaoAtividadeEstatuto"]);
                $this->atividadeEstatuto->setLocalAtividadeEstatuto($vetor["localAtividadeEstatuto"]);
                $this->atividadeEstatuto->setDataAtividadeEstatuto(substr($vetor['dataAtividadeEstatuto'],8,2)."/".substr($vetor['dataAtividadeEstatuto'],5,2)."/".substr($vetor['dataAtividadeEstatuto'],0,4));
                $this->atividadeEstatuto->setHoraAtividadeEstatuto(substr($vetor['horaAtividadeEstatuto'],0,5));
                $this->atividadeEstatuto->setTituloDiscursoAtividadeEstatuto($vetor['tituloDiscursoAtividadeEstatuto']);
                $this->atividadeEstatuto->setNomeOradorDiscursoAtividadeEstatuto($vetor['nomeOradorDiscursoAtividadeEstatuto']);
                $this->atividadeEstatuto->setDataCadastradoAtividadeEstatuto($vetor['dataCadastradoAtividadeEstatuto']);
                $this->atividadeEstatuto->setDataAtualizadoAtividadeEstatuto($vetor['dataAtualizadoAtividadeEstatuto']);
                $this->atividadeEstatuto->setStatusAtividadeEstatuto($vetor["statusAtividadeEstatuto"]);
                $this->atividadeEstatuto->setFkIdTipoAtividadeEstatuto($vetor["fk_idTipoAtividadeEstatuto"]);
                $this->atividadeEstatuto->setFkSeqCadastAtualizadoPor($vetor["fk_seqCadastAtualizadoPor"]);
                $this->atividadeEstatuto->setFkIdOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);
            }
            return $this->atividadeEstatuto;
        } else {
            return false;
        }
    }

    public function listaAtividadeEstatuto($idOrganismo=null,$mesAtual=null,$anoAtual=null) {

        if (!isset($_POST['idTipoAtividadeEstatuto'])) {
            $resultado = $this->atividadeEstatuto->listaAtividadeEstatuto($idOrganismo,$mesAtual,$anoAtual);
        } else {
            /*
            $this->atividadeTipo->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeTipo->buscaNomeOrganismo();
            */
        }

        return $resultado;
    }

    public function listaAtividadeEstatutoOficiais($idAtividadeEstatuto=null) {

        if (!isset($_POST['idAtividadeEstatuto'])) {
            $resultado = $this->atividadeEstatuto->listaAtividadeEstatutoOficiais($idAtividadeEstatuto);
        } else {
            /*
            $this->atividadeTipo->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeTipo->buscaNomeOrganismo();
            */
        }

        return $resultado;
    }

    public function criarComboBox($id = 0, $classificacaoOa) {
        $resultado = $this->atividadeTipo->listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacaoOa);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idTipoAtividadeEstatuto"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idTipoAtividadeEstatuto'] . '" ' . $selecionado . '>' . $vetor["nomeTipoAtividadeEstatuto"] .'</option>';
            }
        }
    }
    
    public function excluirAtividade($id)
    {
        $this->atividadeEstatuto->excluirAtividade($id);
        return true;
    }
    
    public function excluirTodasAtividadesDoMes($idOrganismoAfiliado,$mesAtual,$anoAtual)
    {
        $this->atividadeEstatuto->excluirTodasAtividadesDoMes($idOrganismoAfiliado,$mesAtual,$anoAtual);
        return true;
    }

}

?>