<?php

@include_once("../model/atividadeIniciaticaColumbaClass.php");
@include_once("model/atividadeIniciaticaColumbaClass.php");

class atividadeIniciaticaColumbaController {

    private $atividadeIniciaticaColumba;

    public function __construct() {
        $this->atividadeIniciaticaColumba = new atividadeIniciaticaColumba();
    }

    public function cadastroAtividadeIniciaticaColumba()
    {
        $this->atividadeIniciaticaColumba->setSeqCadastAtividadeIniciaticaColumba($_POST["h_seqCadastColumba"]);
        $this->atividadeIniciaticaColumba->setCodAfiliacaoAtividadeIniciaticaColumba($_POST["codAfiliacaoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setNomeAtividadeIniciaticaColumba($_POST["nomeAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setDescricaoAtividadeIniciaticaColumba($_POST["descricaoAtividadeIniciaticaColumba"]);
        /*
        $this->atividadeIniciaticaColumba->setEnderecoAtividadeIniciaticaColumba($_POST["enderecoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setCepAtividadeIniciaticaColumba($_POST["cepAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setNaturalidadeAtividadeIniciaticaColumba($_POST["naturalidadeAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setDataNascimentoAtividadeIniciaticaColumba(addslashes(substr($_POST["dataNascimentoAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataNascimentoAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataNascimentoAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setEscolaAtividadeIniciaticaColumba($_POST["escolaAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setEmailAtividadeIniciaticaColumba($_POST["emailAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setRedeSocialAtividadeIniciaticaColumba($_POST["redeSocialAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setDataAdmissaoAtividadeIniciaticaColumba(addslashes(substr($_POST["dataAdmissaoAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataAdmissaoAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataAdmissaoAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setDataInstalacaoAtividadeIniciaticaColumba(addslashes(substr($_POST["dataInstalacaoAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataInstalacaoAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataInstalacaoAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setDataAtivaAteAtividadeIniciaticaColumba(addslashes(substr($_POST["dataAtivaAteAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataAtivaAteAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataAtivaAteAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setCodAfiliacaoTutorAtividadeIniciaticaColumba($_POST["codAfiliacaoTutorAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setNomeTutorAtividadeIniciaticaColumba($_POST["nomeTutorAtividadeIniciaticaColumba"]);
        //$this->atividadeIniciaticaColumba->setCompanheiroTutorAtividadeIniciaticaColumba($_POST["companheiroTutorAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setCodAfiliacaoTutoraAtividadeIniciaticaColumba($_POST["codAfiliacaoTutoraAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setNomeTutoraAtividadeIniciaticaColumba($_POST["nomeTutoraAtividadeIniciaticaColumba"]);
        //$this->atividadeIniciaticaColumba->setCompanheiroTutoraAtividadeIniciaticaColumba($_POST["companheiroTutoraAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setResponsavelPlenoAtividadeIniciaticaColumba($_POST["responsavelPlenoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setSeqCadastResponsavelPlenoAtividadeIniciaticaColumba($_POST["seqCadastResponsavelPlenoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setEmailResponsavelAtividadeIniciaticaColumba($_POST["emailResponsavelAtividadeIniciaticaColumba"]);
        */
        $this->atividadeIniciaticaColumba->setFkIdOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->atividadeIniciaticaColumba->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
        //echo "<pre>"; print_r($_POST); echo "</pre>";
/*
        $lnx = "/home/tom/public_html/soa/img/columba/";
        $win = "C:\/xampp\/htdocs\/soa\/img\/columba\/";
        $diretorio = $lnx;

        //echo '<pre>';
        if ($_FILES['RgAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/rg/" . basename($_POST["fk_idOrganismoAfiliado"] . ".rg." . $_FILES['RgAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setRgAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'rg/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".rg." . utf8_decode($_FILES['RgAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['RgAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('RG enviado com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar o RG!');</script>");
        }
        if ($_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/certidaoNascimento\/" . basename($_POST["fk_idOrganismoAfiliado"] . ".certidaoNascimento." . $_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setCertidaoNascimentoAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'certidaoNascimento/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".certidaoNascimento." . utf8_decode($_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Certidao de Nascimento enviado com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar a Certidao de Nascimento!');</script>");
        }
        if ($_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/fotoCorpoInteiro\/" . basename($_POST["fk_idOrganismoAfiliado"] . ".fotoCorpoInteiro." . $_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setFotoCorpoInteiroAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'fotoCorpoInteiro/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".fotoCorpoInteiro." . utf8_decode($_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Foto de corpo inteiro enviada com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar a foto de corpo inteiro!');</script>");
        }
        if ($_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/cartaoMembroResponsavel\/" . basename($_POST["fk_idOrganismoAfiliado"] . ".cartaoMembroResponsavel." . $_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'cartaoMembroResponsavel/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".cartaoMembroResponsavel." . utf8_decode($_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Cartão do membro responsável enviado com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar o cartão do membro responsável!');</script>");
        }
*/
        //echo 'Debug INFO: ';
        //print_r($_FILES);
        //print "</pre>";

        if (!$this->atividadeIniciaticaColumba->verificaColumbaJaCadastrada($_POST["h_seqCadastColumba"], $_POST["fk_idOrganismoAfiliado"])) {
            if ($this->atividadeIniciaticaColumba->cadastroAtividadeIniciaticaColumba()&&$_POST["h_seqCadastColumba"]!="") {

                echo "<script type='text/javascript'>
                    alert('Columba cadastrada com sucesso!');
		    		window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciaticaColumba';
		  		</script>";

            } else {

                echo "<script type='text/javascript'>
                    alert('Não foi possível cadastrar a Columba!');
                    window.location = '../painelDeControle.php?corpo=cadastroAtividadeIniciaticaColumba';
		  		</script>";

            }
        } else {
            echo "<script type='text/javascript'>
                    alert('Columba já está cadastrada nesse Organismo!');
                    window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciaticaColumba';
		  		</script>";
        }
    }

    public function listaAtividadeIniciaticaColumba($oa='') {

        if (!isset($_POST['seqCadastAtividadeIniciaticaColumba'])) {
            $resultado = $this->atividadeIniciaticaColumba->listaAtividadeIniciaticaColumba($oa);
        } else {
            /*
            $this->atividadeIniciatica->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeIniciatica->buscaNomeOrganismo();
            */
        }
        return $resultado;
    }

    public function buscaAtividadeIniciaticaColumbaPorId($idAtividadeIniciaticaColumba) {

        $resultado = $this->atividadeIniciaticaColumba->buscAtividadeIniciaticaColumbaPorId($idAtividadeIniciaticaColumba);

        if ($resultado) {
            foreach ($resultado as $vetor) {
                //echo "<pre>";print_r($vetor);
                $this->atividadeIniciaticaColumba->setSeqCadastAtividadeIniciaticaColumba($vetor["seqCadastAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setCodAfiliacaoAtividadeIniciaticaColumba($vetor["codAfiliacaoAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setNomeAtividadeIniciaticaColumba($vetor["nomeAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setDescricaoAtividadeIniciaticaColumba($vetor["descricaoAtividadeIniciaticaColumba"]);
/*
                $this->atividadeIniciaticaColumba->setEnderecoAtividadeIniciaticaColumba($vetor["enderecoAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setCepAtividadeIniciaticaColumba($vetor["cepAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setNaturalidadeAtividadeIniciaticaColumba($vetor["naturalidadeAtividadeIniciaticaColumba"]);

                $this->atividadeIniciaticaColumba->setDataNascimentoAtividadeIniciaticaColumba(addslashes(substr($vetor["dataNascimentoAtividadeIniciaticaColumba"], 8, 2) . "/" .
                                                                                                          substr($vetor["dataNascimentoAtividadeIniciaticaColumba"], 5, 2) . "/" .
                                                                                                          substr($vetor["dataNascimentoAtividadeIniciaticaColumba"], 0, 4)));

                $this->atividadeIniciaticaColumba->setEscolaAtividadeIniciaticaColumba($vetor["escolaAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setEmailAtividadeIniciaticaColumba($vetor["emailAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setRedeSocialAtividadeIniciaticaColumba($vetor["redeSocialAtividadeIniciaticaColumba"]);

                $this->atividadeIniciaticaColumba->setDataAdmissaoAtividadeIniciaticaColumba(addslashes(substr($vetor["dataAdmissaoAtividadeIniciaticaColumba"], 8, 2) . "/" .
                                                                                                        substr($vetor["dataAdmissaoAtividadeIniciaticaColumba"], 5, 2) . "/" .
                                                                                                        substr($vetor["dataAdmissaoAtividadeIniciaticaColumba"], 0, 4)));

                $this->atividadeIniciaticaColumba->setDataInstalacaoAtividadeIniciaticaColumba(addslashes(substr($vetor["dataInstalacaoAtividadeIniciaticaColumba"], 8, 2) . "/" .
                                                                                                          substr($vetor["dataInstalacaoAtividadeIniciaticaColumba"], 5, 2) . "/" .
                                                                                                          substr($vetor["dataInstalacaoAtividadeIniciaticaColumba"], 0, 4)));

                $this->atividadeIniciaticaColumba->setDataAtivaAteAtividadeIniciaticaColumba(addslashes(substr($vetor["dataAtivaAteAtividadeIniciaticaColumba"], 8, 2) . "/" .
                                                                                                        substr($vetor["dataAtivaAteAtividadeIniciaticaColumba"], 5, 2) . "/" .
                                                                                                        substr($vetor["dataAtivaAteAtividadeIniciaticaColumba"], 0, 4)));

                $this->atividadeIniciaticaColumba->setCodAfiliacaoTutorAtividadeIniciaticaColumba($vetor["codAfiliacaoTutorAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setNomeTutorAtividadeIniciaticaColumba($vetor["nomeTutorAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setCodAfiliacaoTutoraAtividadeIniciaticaColumba($vetor["codAfiliacaoTutoraAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setNomeTutoraAtividadeIniciaticaColumba($vetor["nomeTutoraAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setResponsavelPlenoAtividadeIniciaticaColumba($vetor["responsavelPlenoAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setSeqCadastResponsavelPlenoAtividadeIniciaticaColumba($vetor["seqCadastResponsavelPlenoAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setEmailResponsavelAtividadeIniciaticaColumba($vetor["emailResponsavelAtividadeIniciaticaColumba"]);

                $this->atividadeIniciaticaColumba->setRgAnexoAtividadeIniciaticaColumba($vetor["RgAnexoAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setCertidaoNascimentoAnexoAtividadeIniciaticaColumba($vetor["certidaoNascimentoAnexoAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setFotoCorpoInteiroAnexoAtividadeIniciaticaColumba($vetor["fotoCorpoInteiroAnexoAtividadeIniciaticaColumba"]);
                $this->atividadeIniciaticaColumba->setCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba($vetor["cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba"]);
*/
                $this->atividadeIniciaticaColumba->setFkIdOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);
                $this->atividadeIniciaticaColumba->setFkSeqCadastAtualizadoPor($vetor["fk_seqCadastAtualizadoPor"]);
            }
            return $this->atividadeIniciaticaColumba;
        } else {
            return false;
        }
    }

    public function alteraAtividadeIniciaticaColumba() {

        $this->atividadeIniciaticaColumba->setDescricaoAtividadeIniciaticaColumba($_POST["descricaoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setEnderecoAtividadeIniciaticaColumba($_POST["enderecoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setCepAtividadeIniciaticaColumba($_POST["cepAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setNaturalidadeAtividadeIniciaticaColumba($_POST["naturalidadeAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setDataNascimentoAtividadeIniciaticaColumba(addslashes(substr($_POST["dataNascimentoAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataNascimentoAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataNascimentoAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setEscolaAtividadeIniciaticaColumba($_POST["escolaAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setEmailAtividadeIniciaticaColumba($_POST["emailAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setRedeSocialAtividadeIniciaticaColumba($_POST["redeSocialAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setDataAdmissaoAtividadeIniciaticaColumba(addslashes(substr($_POST["dataAdmissaoAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataAdmissaoAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataAdmissaoAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setDataInstalacaoAtividadeIniciaticaColumba(addslashes(substr($_POST["dataInstalacaoAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataInstalacaoAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataInstalacaoAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setDataAtivaAteAtividadeIniciaticaColumba(addslashes(substr($_POST["dataAtivaAteAtividadeIniciaticaColumba"], 6, 4) . "-" . substr($_POST["dataAtivaAteAtividadeIniciaticaColumba"], 3, 2) . "-" . substr($_POST["dataAtivaAteAtividadeIniciaticaColumba"], 0, 2)));
        $this->atividadeIniciaticaColumba->setCodAfiliacaoTutorAtividadeIniciaticaColumba($_POST["codAfiliacaoTutorAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setNomeTutorAtividadeIniciaticaColumba($_POST["nomeTutorAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setCodAfiliacaoTutoraAtividadeIniciaticaColumba($_POST["codAfiliacaoTutoraAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setNomeTutoraAtividadeIniciaticaColumba($_POST["nomeTutoraAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setResponsavelPlenoAtividadeIniciaticaColumba($_POST["responsavelPlenoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setSeqCadastResponsavelPlenoAtividadeIniciaticaColumba($_POST["seqCadastResponsavelPlenoAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setEmailResponsavelAtividadeIniciaticaColumba($_POST["emailResponsavelAtividadeIniciaticaColumba"]);
        $this->atividadeIniciaticaColumba->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
        //echo "<pre>"; print_r($_POST); echo "</pre>";

        $diretorio = $_SERVER['DOCUMENT_ROOT'] . "/img/columba/";

        //echo '<pre>';
        if ($_FILES['RgAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/rg/" . basename($_POST["fk_idOrganismoAfiliado"] . ".rg." . $_FILES['RgAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setRgAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'rg/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".rg." . utf8_decode($_FILES['RgAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['RgAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('RG enviado com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar o RG!');</script>");
        }
        if ($_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/certidaoNascimento\/" . basename($_POST["fk_idOrganismoAfiliado"] . ".certidaoNascimento." . $_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setCertidaoNascimentoAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'certidaoNascimento/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".certidaoNascimento." . utf8_decode($_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['certidaoNascimentoAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Certidao de Nascimento enviado com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar a Certidao de Nascimento!');</script>");
        }
        if ($_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/fotoCorpoInteiro\/" . basename($_POST["fk_idOrganismoAfiliado"] . ".fotoCorpoInteiro." . $_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setFotoCorpoInteiroAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'fotoCorpoInteiro/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".fotoCorpoInteiro." . utf8_decode($_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Foto de corpo inteiro enviada com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar a foto de corpo inteiro!');</script>");
        }
        if ($_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['name'] != "") {
            $caminho = "img\/columba\/cartaoMembroResponsavel\/" . basename($_POST["fk_idOrganismoAfiliado"] . ".cartaoMembroResponsavel." . $_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['name']);
            $this->atividadeIniciaticaColumba->setCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba($caminho);
            $uploaddir = $diretorio . 'cartaoMembroResponsavel/';
            $uploadfile = $uploaddir . basename($_POST["fk_idOrganismoAfiliado"] . ".cartaoMembroResponsavel." . utf8_decode($_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['name']));
            echo(move_uploaded_file($_FILES['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Cartão do membro responsável enviado com sucesso!');</script>" : "<script type='text/javascript'>alert('Erro ao enviar o cartão do membro responsável!');</script>");
        }
        //echo 'Debug INFO: ';
        //print_r($_FILES);
        //print "</pre>";

        if ($this->atividadeIniciaticaColumba->alteraAtividadeIniciaticaColumba($_POST['idAtividadeIniciaticaColumba'])) {

            echo
            "<script type='text/javascript'>
            alert ('Dados da Columba alterados com sucesso!');
            window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciaticaColumba';
            </script>";

        } else {

            echo
                "<script type='text/javascript'>
            alert ('Não foi possível alterar os dados da Columba!');
            window.location = '../painelDeControle.php?corpo=alteraAtividadeIniciaticaColumba&idAtividadeIniciaticaColumba=" . $_POST['idAtividadeIniciaticaColumba'] . "';
            </script>";

        }
    }

}
