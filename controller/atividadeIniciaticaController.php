<?php

@include_once("../model/atividadeIniciaticaClass.php");
@include_once("model/atividadeIniciaticaClass.php");

@include_once("../lib/functions.php");
@include_once("lib/functions.php");

class atividadeIniciaticaController {

    private $atividadeIniciatica;

    public function __construct() {

        $this->atividadeIniciatica = new atividadeIniciatica();
    }

    public function cadastroAtividadeIniciatica() {

        $dataDigitada = $_POST["dataRealizadaAtividadeIniciatica"];

        if(validaData($dataDigitada)==true) {

            $this->atividadeIniciatica->setTipoAtividadeIniciatica($_POST["tipoAtividadeIniciatica"]);
            $this->atividadeIniciatica->setLocalAtividadeIniciatica($_POST["localAtividadeIniciatica"]);
            $this->atividadeIniciatica->setDataRealizadaAtividadeIniciatica(substr($_POST["dataRealizadaAtividadeIniciatica"], 6, 4) . "-" . substr($_POST["dataRealizadaAtividadeIniciatica"], 3, 2) . "-" . substr($_POST["dataRealizadaAtividadeIniciatica"], 0, 2));
            $this->atividadeIniciatica->setHoraRealizadaAtividadeIniciatica($_POST["horaRealizadaAtividadeIniciatica"]);
            $this->atividadeIniciatica->setAnotacoesAtividadeIniciatica($_POST["anotacoesAtividadeIniciatica"]);
            $this->atividadeIniciatica->setFk_seqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
            $this->atividadeIniciatica->setFk_idAtividadeIniciaticaOficial($_POST["fk_idAtividadeIniciaticaOficial"]);
            if (isset($_POST['classificacaoOa']) && $_POST['classificacaoOa'] != 2) {
                $this->atividadeIniciatica->setFkIdAtividadeIniciaticaColumba($_POST["fk_idAtividadeIniciaticaColumba"]);
            } else {
                $this->atividadeIniciatica->setFkIdAtividadeIniciaticaColumba(0);
            }
            $this->atividadeIniciatica->setFkSeqCadastRecepcao($_POST["recepcaoAtividadeIniciaticaOficial"]);
            $this->atividadeIniciatica->setCodAfiliacaoRecepcao($_POST["codAfiliacaoRecepcao"]);
            $this->atividadeIniciatica->setNomeRecepcao($_POST["nomeRecepcao"]);
            $this->atividadeIniciatica->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);

            //echo "<pre>";print_r($_POST);echo "</pre>";

            if ($this->atividadeIniciatica->cadastroAtividadeIniciatica()) {
                echo "<script type='text/javascript'> alert('Atividade Iniciática cadastrada com sucesso!'); window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciatica';</script>";
            } else {
                echo "<script type='text/javascript'>alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade Iniciática!');window.location = '../painelDeControle.php?corpo=cadastroAtividadeIniciatica';</script>";
            }
        }else{
            echo "<script type='text/javascript'> alert('Data inválida! Foi digitado=>".$dataDigitada."'); window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciatica';</script>";
        }
    }

    public function listaAtividadeIniciatica($oa=null,$tipo=null,$agil=null,$statusAtividadeIniciatica=null, $start=null, $limit=null, $membros=true) {
        //if (!isset($_POST['localAtividadeIniciatica'])) {
            $resultado = $this->atividadeIniciatica->listaAtividadeIniciatica($oa,$tipo,$agil,$statusAtividadeIniciatica,$start,$limit,$membros);
        //} else {
        	/*
            $this->atividadeIniciatica->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeIniciatica->buscaNomeOrganismo();
        	*/
        //}

        return $resultado;
    }
    
	public function buscaAtividadeIniciatica($idAtividadeIniciatica) {

        $resultado = $this->atividadeIniciatica->buscaIdAtividadeIniciatica($idAtividadeIniciatica);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);
                $this->atividadeIniciatica->setTipoAtividadeIniciatica($vetor["tipoAtividadeIniciatica"]);
		        $this->atividadeIniciatica->setLocalAtividadeIniciatica($vetor["localAtividadeIniciatica"]);
                $this->atividadeIniciatica->setDataRealizadaAtividadeIniciatica(substr($vetor['dataRealizadaAtividadeIniciatica'],8,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],5,2)."/".substr($vetor['dataRealizadaAtividadeIniciatica'],0,4));
                $this->atividadeIniciatica->setHoraRealizadaAtividadeIniciatica(substr($vetor['horaRealizadaAtividadeIniciatica'],0,2).":".substr($vetor['horaRealizadaAtividadeIniciatica'],3,2));
                $this->atividadeIniciatica->setDataRegistroAtividadeIniciatica(substr($vetor['dataRegistroAtividadeIniciatica'],8,2)."/".substr($vetor['dataRegistroAtividadeIniciatica'],5,2)."/".substr($vetor['dataRegistroAtividadeIniciatica'],0,4));
                $this->atividadeIniciatica->setDataAtualizadaAtividadeIniciatica(substr($vetor['dataAtualizadaAtividadeIniciatica'],8,2)."/".substr($vetor['dataAtualizadaAtividadeIniciatica'],5,2)."/".substr($vetor['dataAtualizadaAtividadeIniciatica'],0,4));
                $this->atividadeIniciatica->setAnotacoesAtividadeIniciatica($vetor["anotacoesAtividadeIniciatica"]);
                $this->atividadeIniciatica->setStatusAtividadeIniciatica($vetor["statusAtividadeIniciatica"]);
                //$this->atividadeIniciatica->setFk_idAtividadeIniciaticaMembro($vetor["fk_idAtividadeIniciaticaMembro"]);
                $this->atividadeIniciatica->setFk_idAtividadeIniciaticaOficial($vetor["fk_idAtividadeIniciaticaOficial"]);
                $this->atividadeIniciatica->setFkIdAtividadeIniciaticaColumba($vetor["fk_idAtividadeIniciaticaColumba"]);
                $this->atividadeIniciatica->setFkSeqCadastRecepcao($vetor["fk_seqCadastRecepcao"]);
                $this->atividadeIniciatica->setCodAfiliacaoRecepcao($vetor["codAfiliacaoRecepcao"]);
                $this->atividadeIniciatica->setNomeRecepcao($vetor["nomeRecepcao"]);
                $this->atividadeIniciatica->setFk_idOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);
            }

            return $this->atividadeIniciatica;
        } else {
            return false;
        }
    }

    public function alteraAtividadeIniciatica() {
        //echo "Columba:".$_POST["fk_idAtividadeIniciaticaColumba"];exit();
        $this->atividadeIniciatica->setTipoAtividadeIniciatica($_POST["tipoAtividadeIniciatica"]);
        $this->atividadeIniciatica->setLocalAtividadeIniciatica($_POST["localAtividadeIniciatica"]);
        $this->atividadeIniciatica->setDataRealizadaAtividadeIniciatica(substr($_POST["dataRealizadaAtividadeIniciatica"],6,4)."-".substr($_POST["dataRealizadaAtividadeIniciatica"],3,2)."-".substr($_POST["dataRealizadaAtividadeIniciatica"],0,2));
        $this->atividadeIniciatica->setHoraRealizadaAtividadeIniciatica($_POST["horaRealizadaAtividadeIniciatica"]);
        $this->atividadeIniciatica->setAnotacoesAtividadeIniciatica($_POST["anotacoesAtividadeIniciatica"]);
        $this->atividadeIniciatica->setFk_seqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
        $this->atividadeIniciatica->setFk_idAtividadeIniciaticaOficial($_POST["fk_idAtividadeIniciaticaOficial"]);
        if(isset($_POST['classificacaoOa'])&&$_POST['classificacaoOa']!=2) {
            $this->atividadeIniciatica->setFkIdAtividadeIniciaticaColumba($_POST["fk_idAtividadeIniciaticaColumba"]);
        }else{
            $this->atividadeIniciatica->setFkIdAtividadeIniciaticaColumba(0);
        }
        $this->atividadeIniciatica->setFkSeqCadastRecepcao($_POST["recepcaoAtividadeIniciaticaOficial"]);
        $this->atividadeIniciatica->setCodAfiliacaoRecepcao($_POST["codAfiliacaoRecepcao"]);
        $this->atividadeIniciatica->setNomeRecepcao($_POST["nomeRecepcao"]);
        //echo "<pre>";print_r($_POST);

        if ($this->atividadeIniciatica->alteraAtividadeIniciatica($_POST['idAtividadeIniciatica'])) {

            echo
            "<script type='text/javascript'>
            alert ('Atividade Iniciática alterada com sucesso!');
            window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciatica';
            </script>";

        } else {

            echo
            "<script type='text/javascript'>
            alert ('N\u00e3o foi poss\u00edvel alterar a Atividade Iniciática!');
            window.location = '../painelDeControle.php?corpo=alteraAtividadeIniciatica&idAtividadeIniciatica=" . $_POST['idAtividadeIniciatica'] . "';
            </script>";

        }
    }
}

?>