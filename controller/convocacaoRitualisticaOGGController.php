<?php

@include_once("../model/convocacaoRitualisticaOGGClass.php");
@include_once("model/convocacaoRitualisticaOGGClass.php");

@include_once("../model/convocacaoRitualisticaOGGMembroClass.php");
@include_once("model/convocacaoRitualisticaOGGMembroClass.php");

@include_once("../model/convocacaoRitualisticaOGGNaoMembroClass.php");
@include_once("model/convocacaoRitualisticaOGGNaoMembroClass.php");

class convocacaoRitualisticaOGGController {

    private $convocacao;
    private $membro;
    private $naoMembro;

    public function __construct() {
        
        $this->convocacao = new convocacaoRitualisticaOGG();
        $this->membro = new convocacaoRitualisticaOGGMembro();
        $this->naoMembro = new convocacaoRitualisticaOGGNaoMembro();
        
    }

    public function cadastro() {
	
        $dataConvocacao = substr($_POST["dataConvocacao"],6,4)."-".substr($_POST["dataConvocacao"],3,2)."-".substr($_POST["dataConvocacao"],0,2);
        
        $this->convocacao->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->convocacao->setDataConvocacao($dataConvocacao);
        $this->convocacao->setHoraConvocacao($_POST['horaConvocacao']);
        $this->convocacao->setTituloMensagem($_POST['tituloMensagem']);
        $this->convocacao->setSeqCadastComendador($_POST['h_seqCadastMembroComendador']);
        $this->convocacao->setNomeComendador($_POST['h_nomeMembroComendador']);
        $this->convocacao->setUsuario($_POST['usuario']);
        $ultimoId = $this->convocacao->cadastro();
        
        //echo "<pre>";print_r($_REQUEST);
        
        //Cadastrar Membros
        $return=false;
        $ata_oa_mensal_oficiais=$_REQUEST["ata_oa_mensal_oficiais"];
        
        for ($i=0;$i<count($ata_oa_mensal_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_oficiais[$i]);
                $json = json_decode($stringJson);  
                //echo $json;exit();
                $this->membro 	= new convocacaoRitualisticaOGGMembro();
                $this->membro->setFk_idConvocacaoRitualisticaOGG($ultimoId);
                $this->membro->setFk_seq_cadastMembro($json->seqCadast);
                $this->membro->setTipo($json->tipoMembro);
                $this->membro->setEmail($json->emailMembro);
                $this->membro->cadastro();
                $return=true;
        }
        
        //Cadastrar Nao Membros
        $return=false;
        $ata_oa_mensal_nao_oficiais=$_REQUEST["ata_oa_mensal_nao_oficiais"];
        for ($i=0;$i<count($ata_oa_mensal_nao_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_nao_oficiais[$i]);
                $json = json_decode($stringJson); 
                $this->naoMembro = new convocacaoRitualisticaOGGNaoMembro();
                $this->naoMembro->setFk_idConvocacaoRitualisticaOGG($ultimoId);
                $this->naoMembro->setNome($json->nomeNaoMembro);
                $this->naoMembro->setTipo($json->tipoNaoMembro);
                $this->naoMembro->setEmail($json->emailNaoMembro);
                $this->naoMembro->cadastro();
                $return=true;
        }
        //exit();
    	if ($ultimoId) {
           
            echo "<script type='text/javascript'>
                    alert('Convocação cadastrada com sucesso!');
		    window.location = '../painelDeControle.php?corpo=buscaConvocacoesRitualisticas';
		  </script>";
        } else {
            exit();
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essa Convocação!');
                    window.location = '../painelDeControle.php?corpo=buscaConvocacoesRitualisticas';
		  </script>";
            
        }
    }

    
    public function lista($idOrganismoAfiliado,$mes=null,$ano=null) {
        $retorno = $this->convocacao->lista(null,$idOrganismoAfiliado,$mes,$ano);
        //echo "<pre>";print_r($retorno);
        if ($retorno) {
            return $retorno;
        } else {
            return false;
        }
    }

    public function altera() {
        
        $idConvocacaoRitualisticaOGG = $_POST['idConvocacaoRitualistica'];
	
        $dataConvocacao = substr($_POST["dataConvocacao"],6,4)."-".substr($_POST["dataConvocacao"],3,2)."-".substr($_POST["dataConvocacao"],0,2);
        
        $this->convocacao->setIdConvocacaoRitualisticaOGG($idConvocacaoRitualisticaOGG);
        $this->convocacao->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->convocacao->setDataConvocacao($dataConvocacao);
        $this->convocacao->setHoraConvocacao($_POST['horaConvocacao']);
        $this->convocacao->setTituloMensagem($_POST['tituloMensagem']);
        $this->convocacao->setSeqCadastComendador($_POST['h_seqCadastMembroComendador']);
        $this->convocacao->setNomeComendador($_POST['h_nomeMembroComendador']);
        $this->convocacao->setUltimoAtualizar($_POST['usuario']);
        $this->convocacao->altera();
        
        //Excluir todos os membros e nao membros
        $this->membro 	= new convocacaoRitualisticaOGGMembro();
        $this->membro->remove($idConvocacaoRitualisticaOGG);
        
        $this->naoMembro = new convocacaoRitualisticaOGGNaoMembro();
        $this->naoMembro->remove($idConvocacaoRitualisticaOGG);
        
        //Cadastrar Membros
        $return=false;
        $ata_oa_mensal_oficiais=$_REQUEST["ata_oa_mensal_oficiais"];
        for ($i=0;$i<count($ata_oa_mensal_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_oficiais[$i]);
                $json = json_decode($stringJson);  
                //echo $json;exit();
                $this->membro 	= new convocacaoRitualisticaOGGMembro();
                $this->membro->setFk_idConvocacaoRitualisticaOGG($_POST['idConvocacaoRitualistica']);
                $this->membro->setFk_seq_cadastMembro($json->seqCadast);
                $this->membro->setTipo($json->tipoMembro);
                $this->membro->setEmail($json->emailMembro);
                $this->membro->cadastro();
                $return=true;
        }
        
        //Cadastrar Nao Membros
        $return=false;
        $ata_oa_mensal_nao_oficiais=$_REQUEST["ata_oa_mensal_nao_oficiais"];
        for ($i=0;$i<count($ata_oa_mensal_nao_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_nao_oficiais[$i]);
                $json = json_decode($stringJson); 
                $this->naoMembro = new convocacaoRitualisticaOGGNaoMembro();
                $this->naoMembro->setFk_idConvocacaoRitualisticaOGG($_POST['idConvocacaoRitualistica']);
                $this->naoMembro->setNome($json->nomeNaoMembro);
                $this->naoMembro->setTipo($json->tipoNaoMembro);
                $this->naoMembro->setEmail($json->emailNaoMembro);
                $this->naoMembro->cadastro();
                $return=true;
        }
        //exit();
    	if ($return) {
           
            echo "<script type='text/javascript'>
                    alert('Convocação alterada com sucesso!');
		    window.location = '../painelDeControle.php?corpo=buscaConvocacoesRitualisticas';
		  </script>";
        } else {
            exit();
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel alterar essa Convocação!');
                    window.location = '../painelDeControle.php?corpo=buscaConvocacoesRitualisticas';
		  </script>";
            
        }
    }

}

?>