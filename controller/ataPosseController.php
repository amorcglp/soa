<?php

@include_once("model/ataPosseClass.php");
@include_once("../model/ataPosseClass.php");

@include_once("model/ataPosseEmpossadoClass.php");
@include_once("../model/ataPosseEmpossadoClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/regiaoRosacruzClass.php");
@include_once("../model/regiaoRosacruzClass.php");

@include_once ("../model/criaSessaoClass.php");

@include_once("ticketController.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class ataPosseController {

	private $ata;
	private $ata_empossado;
	private $sessao;
        private $regiao;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->ata 			= new ataPosse();
		$this->usuario		= new Usuario();
                $this->regiao		= new regiaoRosacruz();
                $this->ticket		    = new TicketController();
	}

	public function cadastroAtaPosse() {

            //Filtrar Request
            $arrFiltroRequest=array('fk_idOrganismoAfiliado',
                                'enderecoPosse',
                                'numeroPosse',
                                'bairroPosse',
                                'cidadePosse',
                                'dataPosse',
                                'horaInicioPosse',
                                'mestreRetirante',
                                'secretarioRetirante',
                                'presidenteJuntaDepositariaRetirante',
                                'secretarioJuntaDepositariaRetirante',
                                'tesoureiroJuntaDepositariaRetirante',
                                'guardiaoRetirante',
                                'nomeDirigiuPosse',
                                'cargoDirigiuPosse',
                                'detalhesAdicionaisPosse',
                                
                                'nome1',
                                'codigoAfiliacao1',
                                'endereco1',
                                'cidade1',
                                'cep1',
                                'naturalidade1',
                                'dataNascimento1',
                                'nacionalidade1',
                                'estadoCivil1',
                                'profissao1',
                                'rg1',
                                'cpf1',
                                'inicioMandato1',
                                'fimMandato1',
                                'indicado1',
                
                                'nome2',
                                'codigoAfiliacao2',
                                'endereco2',
                                'cidade2',
                                'cep2',
                                'naturalidade2',
                                'dataNascimento2',
                                'nacionalidade2',
                                'estadoCivil2',
                                'profissao2',
                                'rg2',
                                'cpf2',
                                'inicioMandato2',
                                'fimMandato2',
                                'indicado2',
                
                                'nome3',
                                'codigoAfiliacao3',
                                'endereco3',
                                'cidade3',
                                'cep3',
                                'naturalidade3',
                                'dataNascimento3',
                                'nacionalidade3',
                                'estadoCivil3',
                                'profissao3',
                                'rg3',
                                'cpf3',
                                'inicioMandato3',
                                'fimMandato3',
                                'indicado3',
                
                                'nome4',
                                'codigoAfiliacao4',
                                'endereco4',
                                'cidade4',
                                'cep4',
                                'naturalidade4',
                                'dataNascimento4',
                                'nacionalidade4',
                                'estadoCivil4',
                                'profissao4',
                                'rg4',
                                'cpf4',
                                'inicioMandato4',
                                'fimMandato4',
                                'indicado4',
                
                                'nome5',
                                'codigoAfiliacao5',
                                'endereco5',
                                'cidade5',
                                'cep5',
                                'naturalidade5',
                                'dataNascimento5',
                                'nacionalidade5',
                                'estadoCivil5',
                                'profissao5',
                                'rg5',
                                'cpf5',
                                'inicioMandato5',
                                'fimMandato5',
                                'indicado5',
                
                                'nome6',
                                'codigoAfiliacao6',
                                'endereco6',
                                'cidade6',
                                'cep6',
                                'naturalidade6',
                                'dataNascimento6',
                                'nacionalidade6',
                                'estadoCivil6',
                                'profissao6',
                                'rg6',
                                'cpf6',
                                'inicioMandato6',
                                'fimMandato6',
                                'indicado6',

                                'nome7',
                                'codigoAfiliacao7',
                                'endereco7',
                                'cidade7',
                                'cep7',
                                'naturalidade7',
                                'dataNascimento7',
                                'nacionalidade7',
                                'estadoCivil7',
                                'profissao7',
                                'rg7',
                                'cpf7',
                                'inicioMandato7',
                                'fimMandato7',
                                'indicado7',
                
                                'nomeOrganismoAta',
                                'nomeUsuarioAta'                
            );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }

            //Filtrar Input
            $clean = array();
            $dirty = array(

            0 => array($_POST["fk_idOrganismoAfiliado"],"inteiro"),
            1 => array($_POST["enderecoPosse"],"textoNumero"),
            2 => array($_POST["numeroPosse"],"textoNumero"),
            3 => array($_POST["bairroPosse"],"textoNumero"),
            4 => array($_POST["cidadePosse"],"textoNumero"),
            5 => array($_POST["dataPosse"],"textoNumero"),
            6 => array($_POST["horaInicioPosse"],"textoNumero"),
            7 => array($_POST["mestreRetirante"],"textoNumero"),
            8 => array($_POST["secretarioRetirante"],"textoNumero"),
            9 => array($_POST["presidenteJuntaDepositariaRetirante"],"textoNumero"),
            10 => array($_POST["secretarioJuntaDepositariaRetirante"],"textoNumero"),
            11 => array($_POST["tesoureiroJuntaDepositariaRetirante"],"textoNumero"),
            12 => array($_POST["guardiaoRetirante"],"textoNumero"),
            13 => array($_POST["nomeDirigiuPosse"],"textoNumero"),
            14 => array($_POST["cargoDirigiuPosse"],"textoNumero"),
            15 => array(cleanHtml($_POST["detalhesAdicionaisPosse"]),"html"),
                
            16 => array($_POST["nome1"],"textoNumero"),
            17 => array($_POST["codigoAfiliacao1"],"inteiro"),
            18 => array($_POST["endereco1"],"textoNumero"),
            19 => array($_POST["cidade1"],"textoNumero"),
            20 => array($_POST["cep1"],"textoNumero"),
            21 => array($_POST["naturalidade1"],"textoNumero"),
            22 => array($_POST["dataNascimento1"],"textoNumero"),
            23 => array($_POST["nacionalidade1"],"textoNumero"),
            24 => array($_POST["estadoCivil1"],"textoNumero"),
            25 => array($_POST["profissao1"],"textoNumero"),
            26 => array($_POST["rg1"],"textoNumero"),
            27 => array($_POST["cpf1"],"textoNumero"),
            28 => array($_POST["inicioMandato1"],"textoNumero"),
            29 => array($_POST["fimMandato1"],"textoNumero"),
            30 => array($_POST["indicado1"],"textoNumero"),
                
            31 => array($_POST["nome2"],"textoNumero"),
            32 => array($_POST["codigoAfiliacao2"],"inteiro"),
            33 => array($_POST["endereco2"],"textoNumero"),
            34 => array($_POST["cidade2"],"textoNumero"),
            35 => array($_POST["cep2"],"textoNumero"),
            36 => array($_POST["naturalidade2"],"textoNumero"),
            37 => array($_POST["dataNascimento2"],"textoNumero"),
            38 => array($_POST["nacionalidade2"],"textoNumero"),
            39 => array($_POST["estadoCivil2"],"textoNumero"),
            40 => array($_POST["profissao2"],"textoNumero"),
            41 => array($_POST["rg2"],"textoNumero"),
            42 => array($_POST["cpf2"],"textoNumero"),
            43 => array($_POST["inicioMandato2"],"textoNumero"),
            44 => array($_POST["fimMandato2"],"textoNumero"),
            45 => array($_POST["indicado2"],"textoNumero"),    
                
            46 => array($_POST["nome3"],"textoNumero"),
            47 => array($_POST["codigoAfiliacao3"],"inteiro"),
            48 => array($_POST["endereco3"],"textoNumero"),
            49 => array($_POST["cidade3"],"textoNumero"),
            50 => array($_POST["cep3"],"textoNumero"),
            51 => array($_POST["naturalidade3"],"textoNumero"),
            52 => array($_POST["dataNascimento3"],"textoNumero"),
            53 => array($_POST["nacionalidade3"],"textoNumero"),
            54 => array($_POST["estadoCivil3"],"textoNumero"),
            55 => array($_POST["profissao3"],"textoNumero"),
            56 => array($_POST["rg3"],"textoNumero"),
            57 => array($_POST["cpf3"],"textoNumero"),
            58 => array($_POST["inicioMandato3"],"textoNumero"),
            59 => array($_POST["fimMandato3"],"textoNumero"),
            60 => array($_POST["indicado3"],"textoNumero"),    
            
            61 => array($_POST["nome4"],"textoNumero"),
            62 => array($_POST["codigoAfiliacao4"],"inteiro"),
            63 => array($_POST["endereco4"],"textoNumero"),
            64 => array($_POST["cidade4"],"textoNumero"),
            65 => array($_POST["cep4"],"textoNumero"),
            66 => array($_POST["naturalidade4"],"textoNumero"),
            67 => array($_POST["dataNascimento4"],"textoNumero"),
            68 => array($_POST["nacionalidade4"],"textoNumero"),
            69 => array($_POST["estadoCivil4"],"textoNumero"),
            70 => array($_POST["profissao4"],"textoNumero"),
            71 => array($_POST["rg4"],"textoNumero"),
            72 => array($_POST["cpf4"],"textoNumero"),
            73 => array($_POST["inicioMandato4"],"textoNumero"),
            74 => array($_POST["fimMandato4"],"textoNumero"),
            75 => array($_POST["indicado4"],"textoNumero"),    
                
            76 => array($_POST["nome5"],"textoNumero"),
            77 => array($_POST["codigoAfiliacao5"],"inteiro"),
            78 => array($_POST["endereco5"],"textoNumero"),
            79 => array($_POST["cidade5"],"textoNumero"),
            80 => array($_POST["cep5"],"textoNumero"),
            81 => array($_POST["naturalidade5"],"textoNumero"),
            82 => array($_POST["dataNascimento5"],"textoNumero"),
            83 => array($_POST["nacionalidade5"],"textoNumero"),
            84 => array($_POST["estadoCivil5"],"textoNumero"),
            85 => array($_POST["profissao5"],"textoNumero"),
            86 => array($_POST["rg5"],"textoNumero"),
            87 => array($_POST["cpf5"],"textoNumero"),
            88 => array($_POST["inicioMandato5"],"textoNumero"),
            89 => array($_POST["fimMandato5"],"textoNumero"),
            90 => array($_POST["indicado5"],"textoNumero"),    
                
            91 => array($_POST["nome6"],"textoNumero"),
            92 => array($_POST["codigoAfiliacao6"],"inteiro"),
            93 => array($_POST["endereco6"],"textoNumero"),
            94 => array($_POST["cidade6"],"textoNumero"),
            95 => array($_POST["cep6"],"textoNumero"),
            96 => array($_POST["naturalidade6"],"textoNumero"),
            97 => array($_POST["dataNascimento6"],"textoNumero"),
            98 => array($_POST["nacionalidade6"],"textoNumero"),
            99 => array($_POST["estadoCivil6"],"textoNumero"),
            100 => array($_POST["profissao6"],"textoNumero"),
            101 => array($_POST["rg6"],"textoNumero"),
            102 => array($_POST["cpf6"],"textoNumero"),
            103 => array($_POST["inicioMandato6"],"textoNumero"),
            104 => array($_POST["fimMandato6"],"textoNumero"),
            105 => array($_POST["indicado6"],"textoNumero"),    
                
            106 => array($_POST["nome7"],"textoNumero"),
            107 => array($_POST["codigoAfiliacao7"],"inteiro"),
            108 => array($_POST["endereco7"],"textoNumero"),
            109 => array($_POST["cidade7"],"textoNumero"),
            110 => array($_POST["cep7"],"textoNumero"),
            111 => array($_POST["naturalidade7"],"textoNumero"),
            112 => array($_POST["dataNascimento7"],"textoNumero"),
            113 => array($_POST["nacionalidade7"],"textoNumero"),
            114 => array($_POST["estadoCivil7"],"textoNumero"),
            115 => array($_POST["profissao7"],"textoNumero"),
            116 => array($_POST["rg7"],"textoNumero"),
            117 => array($_POST["cpf7"],"textoNumero"),
            118 => array($_POST["inicioMandato7"],"textoNumero"),
            119 => array($_POST["fimMandato7"],"textoNumero"),
            120 => array($_POST["indicado7"],"textoNumero"),    
                
            121 => array($_POST["nomeOrganismoAta"],"textoNumero"),
            122 => array($_POST["nomeUsuarioAta"],"textoNumero")    
               
            );

            if(!validaSegurancaInput($dirty))
            {    
                $clean = $dirty;
            
		$this->ata->setFk_idOrganismoAfiliado($clean[0][0]);
                $this->ata->setEnderecoPosse($clean[1][0]);
                $this->ata->setNumeroPosse($clean[2][0]);	
                $this->ata->setBairroPosse($clean[3][0]);
                $this->ata->setCidadePosse($clean[4][0]);
		$this->ata->setDataPosse(substr($clean[5][0],6,4)."-".substr($clean[5][0],3,2)."-".substr($clean[5][0],0,2));
		$this->ata->setHoraInicioPosse($clean[6][0]);
		$this->ata->setMestreRetirante($clean[7][0]);
		$this->ata->setSecretarioRetirante($clean[8][0]);
		$this->ata->setPresidenteJuntaDepositariaRetirante($clean[9][0]);
		$this->ata->setSecretarioJuntaDepositariaRetirante($clean[10][0]);
		$this->ata->setTesoureiroJuntaDepositariaRetirante($clean[11][0]);
		$this->ata->setGuardiaoRetirante($clean[12][0]);
		$this->ata->setNomeDirigiuPosse($clean[13][0]);
		$this->ata->setCargoDirigiuPosse($clean[14][0]);
		$this->ata->setDetalhesAdicionaisPosse($clean[15][0]);
                $this->ata->setFk_idUsuario($this->sessao->getValue("seqCadast"));

		//Cadastrar ata e pegar ultimo id inserido
		if(!$this->ata->verificaSeJaExiste())
		{
			$ultimo_id = $this->ata->cadastroAtaPosse();

			//Cadastrar Oficiais que participaram da reunião
			$return=false;
			
			//Mestre
			if($_POST['nome1']!="")
			{
				$this->ata_empossado = new ataPosseEmpossado();
				$this->ata_empossado->setFk_idAtaPosse($ultimo_id);
				$this->ata_empossado->setNome($clean[16][0]);
				$this->ata_empossado->setCodigoAfiliacao($clean[17][0]);
				$this->ata_empossado->setEndereco($clean[18][0]);
				$this->ata_empossado->setCidade($clean[19][0]);
				$this->ata_empossado->setCep($clean[20][0]);
				$this->ata_empossado->setNaturalidade($clean[21][0]);
				$this->ata_empossado->setDataNascimento(substr($clean[22][0],6,4)."-".substr($clean[22][0],3,2)."-".substr($clean[22][0],0,2));
				$this->ata_empossado->setNacionalidade($clean[23][0]);
				$this->ata_empossado->setEstadoCivil($clean[24][0]);
				$this->ata_empossado->setProfissao($clean[25][0]);
				$this->ata_empossado->setRg($clean[26][0]);
				$this->ata_empossado->setCpf($clean[27][0]);
				$this->ata_empossado->setInicioMandato(substr($clean[28][0],6,4)."-".substr($clean[28][0],3,2)."-".substr($clean[28][0],0,2));
				$this->ata_empossado->setFimMandato(substr($clean[29][0],6,4)."-".substr($clean[29][0],3,2)."-".substr($clean[29][0],0,2));
				$this->ata_empossado->setIndicado($clean[30][0]);
				$this->ata_empossado->setFuncao(1);
				$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
			}
	
			//Mestre auxiliar
			if($_POST['nome2']!="")
			{
				$this->ata_empossado = new ataPosseEmpossado();
				$this->ata_empossado->setFk_idAtaPosse($ultimo_id);
				$this->ata_empossado->setNome($clean[31][0]);
				$this->ata_empossado->setCodigoAfiliacao($clean[32][0]);
				$this->ata_empossado->setEndereco($clean[33][0]);
				$this->ata_empossado->setCidade($clean[34][0]);
				$this->ata_empossado->setCep($clean[35][0]);
				$this->ata_empossado->setNaturalidade($clean[36][0]);
				$this->ata_empossado->setDataNascimento(substr($clean[37][0],6,4)."-".substr($clean[37][0],3,2)."-".substr($clean[37][0],0,2));
				$this->ata_empossado->setNacionalidade($clean[38][0]);
				$this->ata_empossado->setEstadoCivil($clean[39][0]);
				$this->ata_empossado->setProfissao($clean[40][0]);
				$this->ata_empossado->setRg($clean[41][0]);
				$this->ata_empossado->setCpf($clean[42][0]);
				$this->ata_empossado->setInicioMandato(substr($clean[43][0],6,4)."-".substr($clean[43][0],3,2)."-".substr($clean[44][0],0,2));
				$this->ata_empossado->setFimMandato(substr($clean[44][0],6,4)."-".substr($clean[44][0],3,2)."-".substr($clean[44][0],0,2));
				$this->ata_empossado->setIndicado($clean[45][0]);
				$this->ata_empossado->setFuncao(2);
				$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
			}
			
			//Secretario da Junta Depositária
			if($_POST['nome3']!="")
			{
				$this->ata_empossado = new ataPosseEmpossado();
				$this->ata_empossado->setFk_idAtaPosse($ultimo_id);
				$this->ata_empossado->setNome($clean[46][0]);
				$this->ata_empossado->setCodigoAfiliacao($clean[47][0]);
				$this->ata_empossado->setEndereco($clean[48][0]);
				$this->ata_empossado->setCidade($clean[49][0]);
				$this->ata_empossado->setCep($clean[50][0]);
				$this->ata_empossado->setNaturalidade($clean[51][0]);
				$this->ata_empossado->setDataNascimento(substr($clean[52][0],6,4)."-".substr($clean[52][0],3,2)."-".substr($clean[52][0],0,2));
				$this->ata_empossado->setNacionalidade($clean[53][0]);
				$this->ata_empossado->setEstadoCivil($clean[54][0]);
				$this->ata_empossado->setProfissao($clean[55][0]);
				$this->ata_empossado->setRg($clean[56][0]);
				$this->ata_empossado->setCpf($clean[57][0]);
				$this->ata_empossado->setInicioMandato(substr($clean[58][0],6,4)."-".substr($clean[58][0],3,2)."-".substr($clean[58][0],0,2));
				$this->ata_empossado->setFimMandato(substr($clean[59][0],6,4)."-".substr($clean[59][0],3,2)."-".substr($clean[59][0],0,2));
				$this->ata_empossado->setIndicado($clean[60][0]);
				$this->ata_empossado->setFuncao(3);
				$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
			}
			
			
			//Tesoureiro da Junta Depositária
			if($_POST['nome4']!="")
			{
				$this->ata_empossado = new ataPosseEmpossado();
				$this->ata_empossado->setFk_idAtaPosse($ultimo_id);
				$this->ata_empossado->setNome($clean[61][0]);
				$this->ata_empossado->setCodigoAfiliacao($clean[62][0]);
				$this->ata_empossado->setEndereco($clean[63][0]);
				$this->ata_empossado->setCidade($clean[64][0]);
				$this->ata_empossado->setCep($clean[65][0]);
				$this->ata_empossado->setNaturalidade($clean[66][0]);
				$this->ata_empossado->setDataNascimento(substr($clean[67][0],6,4)."-".substr($clean[67][0],3,2)."-".substr($clean[67][0],0,2));
				$this->ata_empossado->setNacionalidade($clean[68][0]);
				$this->ata_empossado->setEstadoCivil($clean[69][0]);
				$this->ata_empossado->setProfissao($clean[70][0]);
				$this->ata_empossado->setRg($clean[71][0]);
				$this->ata_empossado->setCpf($clean[72][0]);
				$this->ata_empossado->setInicioMandato(substr($clean[73][0],6,4)."-".substr($clean[73][0],3,2)."-".substr($clean[73][0],0,2));
				$this->ata_empossado->setFimMandato(substr($clean[74][0],6,4)."-".substr($clean[74][0],3,2)."-".substr($clean[74][0],0,2));
				$this->ata_empossado->setIndicado($clean[75][0]);
				$this->ata_empossado->setFuncao(4);
				$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
			}
			
			
			//Secretário do Organismo
			if($_POST['nome5']!="")
			{
				$this->ata_empossado = new ataPosseEmpossado();
				$this->ata_empossado->setFk_idAtaPosse($ultimo_id);
				$this->ata_empossado->setNome($clean[76][0]);
				$this->ata_empossado->setCodigoAfiliacao($clean[77][0]);
				$this->ata_empossado->setEndereco($clean[78][0]);
				$this->ata_empossado->setCidade($clean[79][0]);
				$this->ata_empossado->setCep($clean[80][0]);
				$this->ata_empossado->setNaturalidade($clean[81][0]);
				$this->ata_empossado->setDataNascimento(substr($clean[82][0],6,4)."-".substr($clean[82][0],3,2)."-".substr($clean[82][0],0,2));
				$this->ata_empossado->setNacionalidade($clean[83][0]);
				$this->ata_empossado->setEstadoCivil($clean[84][0]);
				$this->ata_empossado->setProfissao($clean[85][0]);
				$this->ata_empossado->setRg($clean[86][0]);
				$this->ata_empossado->setCpf($clean[87][0]);
				$this->ata_empossado->setInicioMandato(substr($clean[88][0],6,4)."-".substr($clean[88][0],3,2)."-".substr($clean[88][0],0,2));
				$this->ata_empossado->setFimMandato(substr($clean[89][0],6,4)."-".substr($clean[89][0],3,2)."-".substr($clean[89][0],0,2));
				$this->ata_empossado->setIndicado($clean[90][0]);
				$this->ata_empossado->setFuncao(5);
				$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
			}
			
			//Presidente da Junta Depositária
			if($_POST['nome6']!="")
			{
				$this->ata_empossado = new ataPosseEmpossado();
				$this->ata_empossado->setFk_idAtaPosse($ultimo_id);
				$this->ata_empossado->setNome($clean[91][0]);
				$this->ata_empossado->setCodigoAfiliacao($clean[92][0]);
				$this->ata_empossado->setEndereco($clean[93][0]);
				$this->ata_empossado->setCidade($clean[94][0]);
				$this->ata_empossado->setCep($clean[95][0]);
				$this->ata_empossado->setNaturalidade($clean[96][0]);
				$this->ata_empossado->setDataNascimento(substr($clean[97][0],6,4)."-".substr($clean[97][0],3,2)."-".substr($clean[97][0],0,2));
				$this->ata_empossado->setNacionalidade($clean[98][0]);
				$this->ata_empossado->setEstadoCivil($clean[99][0]);
				$this->ata_empossado->setProfissao($clean[100][0]);
				$this->ata_empossado->setRg($clean[101][0]);
				$this->ata_empossado->setCpf($clean[102][0]);
				$this->ata_empossado->setInicioMandato(substr($clean[103][0],6,4)."-".substr($clean[103][0],3,2)."-".substr($clean[103][0],0,2));
				$this->ata_empossado->setFimMandato(substr($clean[104][0],6,4)."-".substr($clean[104][0],3,2)."-".substr($clean[104][0],0,2));
				$this->ata_empossado->setIndicado($clean[105][0]);
				$this->ata_empossado->setFuncao(6);
				
				//Cadastrar o ultimo e retornar sucesso/insucesso
				$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
			}
			
			//Guardião
			if($_POST['nome7']!="")
			{
				$this->ata_empossado = new ataPosseEmpossado();
				$this->ata_empossado->setFk_idAtaPosse($ultimo_id);
				$this->ata_empossado->setNome($clean[106][0]);
				$this->ata_empossado->setCodigoAfiliacao($clean[107][0]);
				$this->ata_empossado->setEndereco($clean[108][0]);
				$this->ata_empossado->setCidade($clean[109][0]);
				$this->ata_empossado->setCep($clean[110][0]);
				$this->ata_empossado->setNaturalidade($clean[111][0]);
				$this->ata_empossado->setDataNascimento(substr($clean[112][0],6,4)."-".substr($clean[112][0],3,2)."-".substr($clean[112][0],0,2));
				$this->ata_empossado->setNacionalidade($clean[113][0]);
				$this->ata_empossado->setEstadoCivil($clean[114][0]);
				$this->ata_empossado->setProfissao($clean[115][0]);
				$this->ata_empossado->setRg($clean[116][0]);
				$this->ata_empossado->setCpf($clean[117][0]);
				$this->ata_empossado->setInicioMandato(substr($clean[118][0],6,4)."-".substr($clean[118][0],3,2)."-".substr($clean[118][0],0,2));
				$this->ata_empossado->setFimMandato(substr($clean[119][0],6,4)."-".substr($clean[119][0],3,2)."-".substr($clean[119][0],0,2));
				$this->ata_empossado->setIndicado($clean[120][0]);
				$this->ata_empossado->setFuncao(7);
				
				//Cadastrar o ultimo e retornar sucesso/insucesso
				$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
			}
			
			//echo "<pre>"; print_r($_POST); echo "</pre>";

			/**
			 * Notificar GLP que a ata foi cadastrada
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Nova Ata de Posse Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Ata de Posse Entregue pelo Organismo ".$clean[121][0].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$clean[122][0];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
                        
                        //Selecionar Grandes Conselheiros
                        $resultadoRegiao = $this->regiao->listaRegiaoRosacruz(1,$_POST['siglaOrganismoAta']);
                        $idRegiaoRosacruz = 0;
                        if($resultadoRegiao)
			{
				foreach ($resultadoRegiao as $vetor)
				{
					$idRegiaoRosacruz = $vetor['idRegiaoRosacruz'];
				}
			}
                        $resultadoUsuarios = $this->usuario->listaUsuario(null,null,"151",$idRegiaoRosacruz);
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					echo ",\"".$vetor['idUsuario']."\"";
				}
			}
                        
                        //Selecionar mestre do organismo
			$resultadoMestre = $this->usuario->listaUsuario($_POST['siglaOrganismoAta'],null,"201");
			if($resultadoMestre)
			{
				foreach ($resultadoMestre as $vetor)
				{
					echo ",\"".$vetor['idUsuario']."\"";
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
			//echo "enviada notificacao ata de posse";exit();		
			if ($return) {

                $idFuncionalidade       = 3;
                $remetente              = addslashes($_POST["fk_seqCadastAtualizadoPor"]);
                $descricao              = "Nova Ata de Posse Entregue!";
                $idOrg                  = $_POST['fk_idOrganismoAfiliado'];
                $this->ticket->cadastroTicketAltomatico($idFuncionalidade,$descricao,$remetente,$idOrg,$ultimo_id,1);

				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAtaPosse&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar essa Ata de Posse!');
	                    window.location = '../painelDeControle.php?corpo=buscaAtaPosse';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAtaPosse&jaCadastrado=1';
			 	</script>";
		}
                
            }else{
                
                echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaAtaPosse';
                                </script>";
            } 
	}

	public function listaAtaPosse($idOA=null) {
		$retorno = $this->ata->listaAtaPosse($idOA);
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaAtaPosse($idAtaPosse) {

		$resultado = $this->ata->buscarIdAtaPosse($idAtaPosse);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->ata->setIdAtaPosse($vetor['idAtaPosse']);
				$dataPosse = substr($vetor['dataPosse'],8,2)."/".substr($vetor['dataPosse'],5,2)."/".substr($vetor['dataPosse'],0,4);
				$this->ata->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
			    $this->ata->setEnderecoPosse($vetor['enderecoPosse']);
			    $this->ata->setNumeroPosse($vetor['numeroPosse']);	
			    $this->ata->setBairroPosse($vetor['bairroPosse']);
			    $this->ata->setCidadePosse($vetor['cidadePosse']);
				$this->ata->setDataPosse($dataPosse);
				$this->ata->setHoraInicioPosse($vetor['horaInicioPosse']);
				$this->ata->setMestreRetirante($vetor['mestreRetirante']);
				$this->ata->setSecretarioRetirante($vetor['secretarioRetirante']);
				$this->ata->setPresidenteJuntaDepositariaRetirante($vetor['presidenteJuntaDepositariaRetirante']);
				$this->ata->setSecretarioJuntaDepositariaRetirante($vetor['secretarioJuntaDepositariaRetirante']);
				$this->ata->setTesoureiroJuntaDepositariaRetirante($vetor['tesoureiroJuntaDepositariaRetirante']);
				$this->ata->setGuardiaoRetirante($vetor['guardiaoRetirante']);
				$this->ata->setNomeDirigiuPosse($vetor['nomeDirigiuPosse']);
				$this->ata->setCargoDirigiuPosse($vetor['cargoDirigiuPosse']);
				$this->ata->setDetalhesAdicionaisPosse($vetor['detalhesAdicionaisPosse']);
				
				
			}
			return $this->ata;
		} else {
			return false;
		}
	}
	

	public function alteraAtaPosse() {
            
            //Filtrar Request
            $arrFiltroRequest=array('fk_idOrganismoAfiliado',
                                'enderecoPosse',
                                'numeroPosse',
                                'bairroPosse',
                                'cidadePosse',
                                'dataPosse',
                                'horaInicioPosse',
                                'mestreRetirante',
                                'secretarioRetirante',
                                'presidenteJuntaDepositariaRetirante',
                                'secretarioJuntaDepositariaRetirante',
                                'tesoureiroJuntaDepositariaRetirante',
                                'guardiaoRetirante',
                                'nomeDirigiuPosse',
                                'cargoDirigiuPosse',
                                'detalhesAdicionaisPosse',
                                
                                'nome1',
                                'codigoAfiliacao1',
                                'endereco1',
                                'cidade1',
                                'cep1',
                                'naturalidade1',
                                'dataNascimento1',
                                'nacionalidade1',
                                'estadoCivil1',
                                'profissao1',
                                'rg1',
                                'cpf1',
                                'inicioMandato1',
                                'fimMandato1',
                                'indicado1',
                
                                'nome2',
                                'codigoAfiliacao2',
                                'endereco2',
                                'cidade2',
                                'cep2',
                                'naturalidade2',
                                'dataNascimento2',
                                'nacionalidade2',
                                'estadoCivil2',
                                'profissao2',
                                'rg2',
                                'cpf2',
                                'inicioMandato2',
                                'fimMandato2',
                                'indicado2',
                
                                'nome3',
                                'codigoAfiliacao3',
                                'endereco3',
                                'cidade3',
                                'cep3',
                                'naturalidade3',
                                'dataNascimento3',
                                'nacionalidade3',
                                'estadoCivil3',
                                'profissao3',
                                'rg3',
                                'cpf3',
                                'inicioMandato3',
                                'fimMandato3',
                                'indicado3',
                
                                'nome4',
                                'codigoAfiliacao4',
                                'endereco4',
                                'cidade4',
                                'cep4',
                                'naturalidade4',
                                'dataNascimento4',
                                'nacionalidade4',
                                'estadoCivil4',
                                'profissao4',
                                'rg4',
                                'cpf4',
                                'inicioMandato4',
                                'fimMandato4',
                                'indicado4',
                
                                'nome5',
                                'codigoAfiliacao5',
                                'endereco5',
                                'cidade5',
                                'cep5',
                                'naturalidade5',
                                'dataNascimento5',
                                'nacionalidade5',
                                'estadoCivil5',
                                'profissao5',
                                'rg5',
                                'cpf5',
                                'inicioMandato5',
                                'fimMandato5',
                                'indicado5',
                
                                'nome6',
                                'codigoAfiliacao6',
                                'endereco6',
                                'cidade6',
                                'cep6',
                                'naturalidade6',
                                'dataNascimento6',
                                'nacionalidade6',
                                'estadoCivil6',
                                'profissao6',
                                'rg6',
                                'cpf6',
                                'inicioMandato6',
                                'fimMandato6',
                                'indicado6',

                                'nome7',
                                'codigoAfiliacao7',
                                'endereco7',
                                'cidade7',
                                'cep7',
                                'naturalidade7',
                                'dataNascimento7',
                                'nacionalidade7',
                                'estadoCivil7',
                                'profissao7',
                                'rg7',
                                'cpf7',
                                'inicioMandato7',
                                'fimMandato7',
                                'indicado7',
                
                                'nomeOrganismoAta',
                                'nomeUsuarioAta'                
            );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }

            //Filtrar Input
            $clean = array();
            $dirty = array(

            //0 => array($_POST["fk_idOrganismoAfiliado"],"inteiro"),
            0 => array($_POST["enderecoPosse"],"textoNumero"),
            1 => array($_POST["numeroPosse"],"textoNumero"),
            2 => array($_POST["bairroPosse"],"textoNumero"),
            3 => array($_POST["cidadePosse"],"textoNumero"),
            4 => array($_POST["dataPosse"],"textoNumero"),
            5 => array($_POST["horaInicioPosse"],"textoNumero"),
            6 => array($_POST["mestreRetirante"],"textoNumero"),
            7 => array($_POST["secretarioRetirante"],"textoNumero"),
            8 => array($_POST["presidenteJuntaDepositariaRetirante"],"textoNumero"),
            9 => array($_POST["secretarioJuntaDepositariaRetirante"],"textoNumero"),
            10 => array($_POST["tesoureiroJuntaDepositariaRetirante"],"textoNumero"),
            11 => array($_POST["guardiaoRetirante"],"textoNumero"),
            12 => array($_POST["nomeDirigiuPosse"],"textoNumero"),
            13 => array($_POST["cargoDirigiuPosse"],"textoNumero"),
            14 => array(cleanHtml($_POST["detalhesAdicionaisPosse"]),"html"),
                
            15 => array($_POST["nome1"],"textoNumero"),
            16 => array($_POST["codigoAfiliacao1"],"inteiro"),
            17 => array($_POST["endereco1"],"textoNumero"),
            18 => array($_POST["cidade1"],"textoNumero"),
            19 => array($_POST["cep1"],"textoNumero"),
            20 => array($_POST["naturalidade1"],"textoNumero"),
            21 => array($_POST["dataNascimento1"],"textoNumero"),
            22 => array($_POST["nacionalidade1"],"textoNumero"),
            23 => array($_POST["estadoCivil1"],"textoNumero"),
            24 => array($_POST["profissao1"],"textoNumero"),
            25 => array($_POST["rg1"],"textoNumero"),
            26 => array($_POST["cpf1"],"textoNumero"),
            27 => array($_POST["inicioMandato1"],"textoNumero"),
            28 => array($_POST["fimMandato1"],"textoNumero"),
            29 => array($_POST["indicado1"],"textoNumero"),
                
            30 => array($_POST["nome2"],"textoNumero"),
            31 => array($_POST["codigoAfiliacao2"],"inteiro"),
            32 => array($_POST["endereco2"],"textoNumero"),
            33 => array($_POST["cidade2"],"textoNumero"),
            34 => array($_POST["cep2"],"textoNumero"),
            35 => array($_POST["naturalidade2"],"textoNumero"),
            36 => array($_POST["dataNascimento2"],"textoNumero"),
            37 => array($_POST["nacionalidade2"],"textoNumero"),
            38 => array($_POST["estadoCivil2"],"textoNumero"),
            39 => array($_POST["profissao2"],"textoNumero"),
            40 => array($_POST["rg2"],"textoNumero"),
            41 => array($_POST["cpf2"],"textoNumero"),
            42 => array($_POST["inicioMandato2"],"textoNumero"),
            43 => array($_POST["fimMandato2"],"textoNumero"),
            44 => array($_POST["indicado2"],"textoNumero"),    
                
            45 => array($_POST["nome3"],"textoNumero"),
            46 => array($_POST["codigoAfiliacao3"],"inteiro"),
            47 => array($_POST["endereco3"],"textoNumero"),
            48 => array($_POST["cidade3"],"textoNumero"),
            49 => array($_POST["cep3"],"textoNumero"),
            50 => array($_POST["naturalidade3"],"textoNumero"),
            51 => array($_POST["dataNascimento3"],"textoNumero"),
            52 => array($_POST["nacionalidade3"],"textoNumero"),
            53 => array($_POST["estadoCivil3"],"textoNumero"),
            54 => array($_POST["profissao3"],"textoNumero"),
            55 => array($_POST["rg3"],"textoNumero"),
            56 => array($_POST["cpf3"],"textoNumero"),
            57 => array($_POST["inicioMandato3"],"textoNumero"),
            58 => array($_POST["fimMandato3"],"textoNumero"),
            59 => array($_POST["indicado3"],"textoNumero"),    
            
            60 => array($_POST["nome4"],"textoNumero"),
            61 => array($_POST["codigoAfiliacao4"],"inteiro"),
            62 => array($_POST["endereco4"],"textoNumero"),
            63 => array($_POST["cidade4"],"textoNumero"),
            64 => array($_POST["cep4"],"textoNumero"),
            65 => array($_POST["naturalidade4"],"textoNumero"),
            66 => array($_POST["dataNascimento4"],"textoNumero"),
            67 => array($_POST["nacionalidade4"],"textoNumero"),
            68 => array($_POST["estadoCivil4"],"textoNumero"),
            69 => array($_POST["profissao4"],"textoNumero"),
            70 => array($_POST["rg4"],"textoNumero"),
            71 => array($_POST["cpf4"],"textoNumero"),
            72 => array($_POST["inicioMandato4"],"textoNumero"),
            73 => array($_POST["fimMandato4"],"textoNumero"),
            74 => array($_POST["indicado4"],"textoNumero"),    
                
            75 => array($_POST["nome5"],"textoNumero"),
            76 => array($_POST["codigoAfiliacao5"],"inteiro"),
            77 => array($_POST["endereco5"],"textoNumero"),
            78 => array($_POST["cidade5"],"textoNumero"),
            79 => array($_POST["cep5"],"textoNumero"),
            80 => array($_POST["naturalidade5"],"textoNumero"),
            81 => array($_POST["dataNascimento5"],"textoNumero"),
            82 => array($_POST["nacionalidade5"],"textoNumero"),
            83 => array($_POST["estadoCivil5"],"textoNumero"),
            84 => array($_POST["profissao5"],"textoNumero"),
            85 => array($_POST["rg5"],"textoNumero"),
            86 => array($_POST["cpf5"],"textoNumero"),
            87 => array($_POST["inicioMandato5"],"textoNumero"),
            88 => array($_POST["fimMandato5"],"textoNumero"),
            89 => array($_POST["indicado5"],"textoNumero"),    
                
            90 => array($_POST["nome6"],"textoNumero"),
            91 => array($_POST["codigoAfiliacao6"],"inteiro"),
            92 => array($_POST["endereco6"],"textoNumero"),
            93 => array($_POST["cidade6"],"textoNumero"),
            94 => array($_POST["cep6"],"textoNumero"),
            95 => array($_POST["naturalidade6"],"textoNumero"),
            96 => array($_POST["dataNascimento6"],"textoNumero"),
            97 => array($_POST["nacionalidade6"],"textoNumero"),
            98 => array($_POST["estadoCivil6"],"textoNumero"),
            99 => array($_POST["profissao6"],"textoNumero"),
            100 => array($_POST["rg6"],"textoNumero"),
            101 => array($_POST["cpf6"],"textoNumero"),
            102 => array($_POST["inicioMandato6"],"textoNumero"),
            103 => array($_POST["fimMandato6"],"textoNumero"),
            104 => array($_POST["indicado6"],"textoNumero"),    
                
            105 => array($_POST["nome7"],"textoNumero"),
            106 => array($_POST["codigoAfiliacao7"],"inteiro"),
            107 => array($_POST["endereco7"],"textoNumero"),
            108 => array($_POST["cidade7"],"textoNumero"),
            109 => array($_POST["cep7"],"textoNumero"),
            110 => array($_POST["naturalidade7"],"textoNumero"),
            111 => array($_POST["dataNascimento7"],"textoNumero"),
            112 => array($_POST["nacionalidade7"],"textoNumero"),
            113 => array($_POST["estadoCivil7"],"textoNumero"),
            114 => array($_POST["profissao7"],"textoNumero"),
            115 => array($_POST["rg7"],"textoNumero"),
            116 => array($_POST["cpf7"],"textoNumero"),
            117 => array($_POST["inicioMandato7"],"textoNumero"),
            118 => array($_POST["fimMandato7"],"textoNumero"),
            119 => array($_POST["indicado7"],"textoNumero"),    
               
               
            );

            if(!validaSegurancaInput($dirty))
            {    
                $clean = $dirty;

		//$this->ata->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
                $this->ata->setEnderecoPosse($clean[0][0]);
                $this->ata->setNumeroPosse($clean[1][0]);	
                $this->ata->setBairroPosse($clean[2][0]);
                $this->ata->setCidadePosse($clean[3][0]);
		$this->ata->setDataPosse(substr($clean[4][0],6,4)."-".substr($clean[4][0],3,2)."-".substr($clean[4][0],0,2));
		$this->ata->setHoraInicioPosse($clean[5][0]);
		$this->ata->setMestreRetirante($clean[6][0]);
		$this->ata->setSecretarioRetirante($clean[7][0]);
		$this->ata->setPresidenteJuntaDepositariaRetirante($clean[8][0]);
		$this->ata->setSecretarioJuntaDepositariaRetirante($clean[9][0]);
		$this->ata->setTesoureiroJuntaDepositariaRetirante($clean[10][0]);
		$this->ata->setGuardiaoRetirante($clean[11][0]);
		$this->ata->setNomeDirigiuPosse($clean[12][0]);
		$this->ata->setCargoDirigiuPosse($clean[13][0]);
		$this->ata->setDetalhesAdicionaisPosse($clean[14][0]);
                $this->ata->setUltimoAtualizar($this->sessao->getValue("seqCadast"));

		//Pegar id para edição
		$id = $_POST['fk_idAtaPosse'];
		
		$this->ata->alteraAtaPosse($id);
		
		//Remover empossados 
		$ape = new ataPosseEmpossado();
		$ape->removeEmpossados($id);
		
		//Cadastrar empossados novamente
		$return=false;
		
		//Mestre
		if($_POST['nome1']!="")
		{
			$this->ata_empossado = new ataPosseEmpossado();
			$this->ata_empossado->setFk_idAtaPosse($id);
			$this->ata_empossado->setNome($clean[15][0]);
			$this->ata_empossado->setCodigoAfiliacao($clean[16][0]);
			$this->ata_empossado->setEndereco($clean[17][0]);
			$this->ata_empossado->setCidade($clean[18][0]);
			$this->ata_empossado->setCep($clean[19][0]);
			$this->ata_empossado->setNaturalidade($clean[20][0]);
			$this->ata_empossado->setDataNascimento(substr($clean[21][0],6,4)."-".substr($clean[21][0],3,2)."-".substr($clean[21][0],0,2));
			$this->ata_empossado->setNacionalidade($clean[22][0]);
			$this->ata_empossado->setEstadoCivil($clean[23][0]);
			$this->ata_empossado->setProfissao($clean[24][0]);
			$this->ata_empossado->setRg($clean[25][0]);
			$this->ata_empossado->setCpf($clean[26][0]);
			$this->ata_empossado->setInicioMandato(substr($clean[27][0],6,4)."-".substr($clean[27][0],3,2)."-".substr($clean[27][0],0,2));
			$this->ata_empossado->setFimMandato(substr($clean[28][0],6,4)."-".substr($clean[28][0],3,2)."-".substr($clean[28][0],0,2));
			$this->ata_empossado->setIndicado($clean[29][0]);
			$this->ata_empossado->setFuncao(1);
			$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
		}

		//Mestre auxiliar
		if($_POST['nome2']!="")
		{
			$this->ata_empossado = new ataPosseEmpossado();
			$this->ata_empossado->setFk_idAtaPosse($id);
			$this->ata_empossado->setNome($clean[30][0]);
			$this->ata_empossado->setCodigoAfiliacao($clean[31][0]);
			$this->ata_empossado->setEndereco($clean[32][0]);
			$this->ata_empossado->setCidade($clean[33][0]);
			$this->ata_empossado->setCep($clean[34][0]);
			$this->ata_empossado->setNaturalidade($clean[35][0]);
			$this->ata_empossado->setDataNascimento(substr($clean[36][0],6,4)."-".substr($clean[36][0],3,2)."-".substr($clean[36][0],0,2));
			$this->ata_empossado->setNacionalidade($clean[37][0]);
			$this->ata_empossado->setEstadoCivil($clean[38][0]);
			$this->ata_empossado->setProfissao($clean[39][0]);
			$this->ata_empossado->setRg($clean[40][0]);
			$this->ata_empossado->setCpf($clean[41][0]);
			$this->ata_empossado->setInicioMandato(substr($clean[42][0],6,4)."-".substr($clean[42][0],3,2)."-".substr($clean[42][0],0,2));
			$this->ata_empossado->setFimMandato(substr($clean[43][0],6,4)."-".substr($clean[43][0],3,2)."-".substr($clean[43][0],0,2));
			$this->ata_empossado->setIndicado($clean[44][0]);
			$this->ata_empossado->setFuncao(2);
			$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
		}
		
		//Secretario da Junta Depositária
		if($_POST['nome3']!="")
		{
			$this->ata_empossado = new ataPosseEmpossado();
			$this->ata_empossado->setFk_idAtaPosse($id);
			$this->ata_empossado->setNome($clean[45][0]);
			$this->ata_empossado->setCodigoAfiliacao($clean[46][0]);
			$this->ata_empossado->setEndereco($clean[47][0]);
			$this->ata_empossado->setCidade($clean[48][0]);
			$this->ata_empossado->setCep($clean[49][0]);
			$this->ata_empossado->setNaturalidade($clean[50][0]);
			$this->ata_empossado->setDataNascimento(substr($clean[51][0],6,4)."-".substr($clean[51][0],3,2)."-".substr($clean[51][0],0,2));
			$this->ata_empossado->setNacionalidade($clean[52][0]);
			$this->ata_empossado->setEstadoCivil($clean[53][0]);
			$this->ata_empossado->setProfissao($clean[54][0]);
			$this->ata_empossado->setRg($clean[55][0]);
			$this->ata_empossado->setCpf($clean[56][0]);
			$this->ata_empossado->setInicioMandato(substr($clean[57][0],6,4)."-".substr($clean[57][0],3,2)."-".substr($clean[57][0],0,2));
			$this->ata_empossado->setFimMandato(substr($clean[58][0],6,4)."-".substr($clean[58][0],3,2)."-".substr($clean[58][0],0,2));
			$this->ata_empossado->setIndicado($clean[59][0]);
			$this->ata_empossado->setFuncao(3);
			$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
		}
		
		
		//Tesoureiro da Junta Depositária
		if($_POST['nome4']!="")
		{
			$this->ata_empossado = new ataPosseEmpossado();
			$this->ata_empossado->setFk_idAtaPosse($id);
			$this->ata_empossado->setNome($clean[60][0]);
			$this->ata_empossado->setCodigoAfiliacao($clean[61][0]);
			$this->ata_empossado->setEndereco($clean[62][0]);
			$this->ata_empossado->setCidade($clean[63][0]);
			$this->ata_empossado->setCep($clean[64][0]);
			$this->ata_empossado->setNaturalidade($clean[65][0]);
			$this->ata_empossado->setDataNascimento(substr($clean[66][0],6,4)."-".substr($clean[66][0],3,2)."-".substr($clean[66][0],0,2));
			$this->ata_empossado->setNacionalidade($clean[67][0]);
			$this->ata_empossado->setEstadoCivil($clean[68][0]);
			$this->ata_empossado->setProfissao($clean[69][0]);
			$this->ata_empossado->setRg($clean[70][0]);
			$this->ata_empossado->setCpf($clean[71][0]);
			$this->ata_empossado->setInicioMandato(substr($clean[72][0],6,4)."-".substr($clean[72][0],3,2)."-".substr($clean[72][0],0,2));
			$this->ata_empossado->setFimMandato(substr($clean[73][0],6,4)."-".substr($clean[73][0],3,2)."-".substr($clean[73][0],0,2));
			$this->ata_empossado->setIndicado($clean[74][0]);
			$this->ata_empossado->setFuncao(4);
			$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
		}
		
		//Secretário do Organismo
		if($_POST['nome5']!="")
		{
			$this->ata_empossado = new ataPosseEmpossado();
			$this->ata_empossado->setFk_idAtaPosse($id);
			$this->ata_empossado->setNome($clean[75][0]);
			$this->ata_empossado->setCodigoAfiliacao($clean[76][0]);
			$this->ata_empossado->setEndereco($clean[77][0]);
			$this->ata_empossado->setCidade($clean[78][0]);
			$this->ata_empossado->setCep($clean[79][0]);
			$this->ata_empossado->setNaturalidade($clean[80][0]);
			$this->ata_empossado->setDataNascimento(substr($clean[81][0],6,4)."-".substr($clean[81][0],3,2)."-".substr($clean[81][0],0,2));
			$this->ata_empossado->setNacionalidade($clean[82][0]);
			$this->ata_empossado->setEstadoCivil($clean[83][0]);
			$this->ata_empossado->setProfissao($clean[84][0]);
			$this->ata_empossado->setRg($clean[85][0]);
			$this->ata_empossado->setCpf($clean[86][0]);
			$this->ata_empossado->setInicioMandato(substr($clean[87][0],6,4)."-".substr($clean[87][0],3,2)."-".substr($clean[87][0],0,2));
			$this->ata_empossado->setFimMandato(substr($clean[88][0],6,4)."-".substr($clean[88][0],3,2)."-".substr($clean[88][0],0,2));
			$this->ata_empossado->setIndicado($clean[89][0]);
			$this->ata_empossado->setFuncao(5);
			$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
		}
		
		//Presidente da Junta Depositária
		if($_POST['nome6']!="")
		{
			$this->ata_empossado = new ataPosseEmpossado();
			$this->ata_empossado->setFk_idAtaPosse($id);
			$this->ata_empossado->setNome($clean[90][0]);
			$this->ata_empossado->setCodigoAfiliacao($clean[91][0]);
			$this->ata_empossado->setEndereco($clean[92][0]);
			$this->ata_empossado->setCidade($clean[93][0]);
			$this->ata_empossado->setCep($clean[94][0]);
			$this->ata_empossado->setNaturalidade($clean[95][0]);
			$this->ata_empossado->setDataNascimento(substr($clean[96][0],6,4)."-".substr($clean[96][0],3,2)."-".substr($clean[96][0],0,2));
			$this->ata_empossado->setNacionalidade($clean[97][0]);
			$this->ata_empossado->setEstadoCivil($clean[98][0]);
			$this->ata_empossado->setProfissao($clean[99][0]);
			$this->ata_empossado->setRg($clean[100][0]);
			$this->ata_empossado->setCpf($clean[101][0]);
			$this->ata_empossado->setInicioMandato(substr($clean[102][0],6,4)."-".substr($clean[102][0],3,2)."-".substr($clean[102][0],0,2));
			$this->ata_empossado->setFimMandato(substr($clean[103][0],6,4)."-".substr($clean[103][0],3,2)."-".substr($clean[103][0],0,2));
			$this->ata_empossado->setIndicado($clean[104][0]);
			$this->ata_empossado->setFuncao(6);
			
			//Cadastrar o ultimo e retornar sucesso/insucesso
			$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
		}
		
		//Guardião
		if($_POST['nome7']!="")
		{
			$this->ata_empossado = new ataPosseEmpossado();
			$this->ata_empossado->setFk_idAtaPosse($id);
			$this->ata_empossado->setNome($clean[105][0]);
			$this->ata_empossado->setCodigoAfiliacao($clean[106][0]);
			$this->ata_empossado->setEndereco($clean[107][0]);
			$this->ata_empossado->setCidade($clean[108][0]);
			$this->ata_empossado->setCep($clean[109][0]);
			$this->ata_empossado->setNaturalidade($clean[110][0]);
			$this->ata_empossado->setDataNascimento(substr($clean[111][0],6,4)."-".substr($clean[111][0],3,2)."-".substr($clean[111][0],0,2));
			$this->ata_empossado->setNacionalidade($clean[112][0]);
			$this->ata_empossado->setEstadoCivil($clean[113][0]);
			$this->ata_empossado->setProfissao($clean[114][0]);
			$this->ata_empossado->setRg($clean[115][0]);
			$this->ata_empossado->setCpf($clean[116][0]);
			$this->ata_empossado->setInicioMandato(substr($clean[117][0],6,4)."-".substr($clean[117][0],3,2)."-".substr($clean[117][0],0,2));
			$this->ata_empossado->setFimMandato(substr($clean[118][0],6,4)."-".substr($clean[118][0],3,2)."-".substr($_POST['fimMandato7'],0,2));
			$this->ata_empossado->setIndicado($clean[119][0]);
			$this->ata_empossado->setFuncao(7);
			
			//Cadastrar o ultimo e retornar sucesso/insucesso
			$return = $this->ata_empossado->cadastroAtaPosseEmpossado();
		}

		if ($return) {

                    $idFuncionalidade       = 3;
                    $remetente              = addslashes($_POST["fk_seqCadastAtualizadoPor"]);
                    $descricao              = "Houveram alterações na Ata de Posse!";
                    $idOrg                  = $_POST['fk_idOrganismoAfiliado'];
                    /*
                    echo "<br>idFuncionalidade:".$idFuncionalidade;
                    echo "<br>descricao:".$descricao;
                    echo "<br>remetente:".$remetente;
                    echo "<br>idOrganismo:".$idOrg;
                    echo "<br>ultimo_id:".$id;
                     */
                    $this->ticket->cadastroTicketAltomatico($idFuncionalidade,$descricao,$remetente,$idOrg,$id,2);

                                echo
                    "<script type='text/javascript'>
                                window.location = '../painelDeControle.php?corpo=buscaAtaPosse&salvo=1';
                                </script>";
                        } else {
                                echo
                    "<script type='text/javascript'>
                                alert ('Não foi possível alterar a Ata de Posse!');
                                window.location = '../painelDeControle.php?corpo=buscaAtaPosse';
                                </script>";
		}
            }else{
                
                echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaAtaPosse';
                                </script>";
            } 
	}

}

?>