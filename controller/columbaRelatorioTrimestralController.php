<?php

@include_once("model/columbaRelatorioTrimestralClass.php");
@include_once("../model/columbaRelatorioTrimestralClass.php");

@include_once("model/organismoClass.php");
@include_once("../model/organismoClass.php");

//@include_once ("../model/criaSessaoClass.php");

class columbaRelatorioTrimestralController {

	private $columba;
	private $organismo;
	//private $sessao;

	public function __construct()
	{
		//$this->sessao 		= new criaSessao();
		$this->columba                  = new ColumbaRelatorioTrimestral();
		$this->organismo		= new organismo();
		
	}

	public function cadastroColumba() {

		if(!$this->columba->verificaSeJaCadastrado($_POST['seqCadastMembro'],$_POST['fk_idOrganismoAfiliado']))
		{
                        $dataPrevistaInstalacao = substr($_POST['dataPrevistaInstalacao'],6,4)."-".substr($_POST['dataPrevistaInstalacao'],3,2)."-".substr($_POST['dataPrevistaInstalacao'],0,2);
			
			$this->columba->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
                        $this->columba->setCodigoAfiliacao($_POST['codigoAfiliacao']);
                        $this->columba->setNomeColumba($_POST['nomeColumba']);
                        $this->columba->setSeqCadastMembro($_POST['seqCadastMembro']);
                        $this->columba->setDataPrevistaInstalacao($dataPrevistaInstalacao);
                        $this->columba->setTipoColumba(1);//1 - Não instalada e 2 - Atuante
                        $this->columba->setUsuario($_POST['usuario']);
		   
			//Cadastrar ata e pegar ultimo id inserido
			$return = $this->columba->cadastroColumba();
			 		
			if ($return) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaColumbasNaoInstaladas&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar essa columba!');
	                    window.location = '../painelDeControle.php?corpo=buscaColumbasNaoInstaladas';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaColumbasNaoInstaladas&jaCadastrado=1&seqCadastMembro=".$_POST['seqCadastMembro']."';
			 	</script>";
		}
	}

	public function listaColumba($idOa,$tipoColumba) {
		$retorno = $this->columba->listaColumba($idOa,$tipoColumba);
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaColumbaPeloSeqCadast($seqCadast) {

		$resultado = $this->columba->buscaColumbaPeloSeqCadast($seqCadast);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);"]);
				$this->columba->setCodigoAfiliacao($vetor['codigoAfiliacao']);
				$this->columba->setNomeColumba($vetor['nomeColumba']);
			}

			return $this->columba;
		} else {
			return false;
		}
	}

}

?>