<?php

@include_once("../model/atividadeTipoClass.php");
@include_once("model/atividadeTipoClass.php");

class atividadeTipoController {

    private $atividadeTipo;

    public function __construct() {

        $this->atividadeTipo = new atividadeTipo();
    }

    public function cadastroAtividadeTipo() {
		
        $this->atividadeTipo->setNomeAtividadeTipo($_POST["nomeAtividadeTipo"]);
        $this->atividadeTipo->setDescricaoAtividadeTipo($_POST["descricaoAtividadeTipo"]);
        $this->atividadeTipo->setLojaAtividadeTipo($_POST['lojaAtividadeTipo']);
        $this->atividadeTipo->setPronaosAtividadeTipo($_POST["pronaosAtividadeTipo"]);
        $this->atividadeTipo->setCapituloAtividadeTipo($_POST["capituloAtividadeTipo"]);
        $this->atividadeTipo->setQntLojaAtividadeTipo($_POST["qntLojaAtividadeTipo"]);
        $this->atividadeTipo->setQntPronaosAtividadeTipo($_POST['qntPronaosAtividadeTipo']);
        $this->atividadeTipo->setQntCapituloAtividadeTipo($_POST["qntCapituloAtividadeTipo"]);
        
        if ($this->atividadeTipo->cadastroAtividadeTipo()) {

            echo "<script type='text/javascript'>
                    alert('Atividade para O.A. cadastrado com sucesso!');
		    		window.location = '../painelDeControle.php?corpo=buscaAtividadeTipo';
		  		</script>";
        } else {
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade para O.A.!');
                    window.location = '../painelDeControle.php?corpo=cadastroAtividadeTipo';
		  		</script>";
        }
    }
    
	public function alteraAtividadeTipo() {
        
        $this->atividadeTipo->setNomeAtividadeTipo($_POST["nomeAtividadeTipo"]);
        $this->atividadeTipo->setDescricaoAtividadeTipo($_POST["descricaoAtividadeTipo"]);
        $this->atividadeTipo->setLojaAtividadeTipo($_POST['lojaAtividadeTipo']);
        $this->atividadeTipo->setPronaosAtividadeTipo($_POST["pronaosAtividadeTipo"]);
        $this->atividadeTipo->setCapituloAtividadeTipo($_POST["capituloAtividadeTipo"]);
        $this->atividadeTipo->setQntLojaAtividadeTipo($_POST["qntLojaAtividadeTipo"]);
        $this->atividadeTipo->setQntPronaosAtividadeTipo($_POST['qntPronaosAtividadeTipo']);
        $this->atividadeTipo->setQntCapituloAtividadeTipo($_POST["qntCapituloAtividadeTipo"]);

        if ($this->atividadeTipo->alteraAtividadeTipo($_POST['idAtividadeTipo'])) {

            echo
            "<script type='text/javascript'>
			alert ('Atividade para O.A. alterada com sucesso!');
			window.location = '../painelDeControle.php?corpo=buscaatividadeTipo';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar a Atividade para O.A.!');
			window.location = '../painelDeControle.php?corpo=alteraAtividadeTipo&idAtividadeTipo=" . $_POST['idAtividadeTipo'] . "';
			</script>";
        }
    }

    public function listaIdAtividadeTipo() {
        $retorno = $this->atividadeTipo->listaAtividadeTipo();
        $listaatividadeTipo = "";

        foreach ($retorno as $dados) {
            $listaatividadeTipo[] = $dados['idAtividadeTipo'];
        }

        if ($listaatividadeTipo) {
            return $listaatividadeTipo;
        } else {
            return false;
        }
    }

    public function listaAtividadeTipo() {
    	
        if (!isset($_POST['idAtividadeTipo'])) {
            $resultado = $this->atividadeTipo->listaAtividadeTipo();
        } else {
        	/*
            $this->atividadeTipo->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeTipo->buscaNomeOrganismo();
        	*/
        }

        return $resultado;
    }
    
	public function buscaAtividadeTipo($idatividadeTipo) {

        $resultado = $this->atividadeTipo->buscaIdatividadeTipo($idatividadeTipo);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);
		        $this->atividadeTipo->setEnderecoatividadeTipo($vetor["enderecoatividadeTipo"]);
		        $this->atividadeTipo->setNumeroatividadeTipo($vetor["numeroatividadeTipo"]);
		        $this->atividadeTipo->setComplementoatividadeTipo($vetor['complementoatividadeTipo']);
		        $this->atividadeTipo->setBairroatividadeTipo($vetor["bairroatividadeTipo"]);
		        $this->atividadeTipo->setCidadeatividadeTipo($vetor["cidadeatividadeTipo"]);
		        $this->atividadeTipo->setEstadoatividadeTipo($vetor["estadoatividadeTipo"]);
		        $this->atividadeTipo->setCepatividadeTipo($vetor['cepatividadeTipo']);
		        $this->atividadeTipo->setDataFundacaoatividadeTipo(substr($vetor['dataFundacaoatividadeTipo'],8,2)."/".substr($vetor['dataFundacaoatividadeTipo'],5,2)."/".substr($vetor['dataFundacaoatividadeTipo'],0,4));
		        $this->atividadeTipo->setStatusatividadeTipo($vetor["statusatividadeTipo"]);
		        $this->atividadeTipo->setFk_idOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);
            }

            return $this->atividadeTipo;
        } else {
            return false;
        }
    }

    public function criarComboBox($id = 0) {
        $resultado = $this->atividadeTipo->listaatividadeTipo();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idatividadeTipo"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idatividadeTipo'] . '" ' . $selecionado . '>' . $vetor["enderecoatividadeTipo"] . ' - ' . $vetor["numeroatividadeTipo"] . ' - ' . $vetor["complementoatividadeTipo"] .'</option>';
            }
        }
    }

}

?>