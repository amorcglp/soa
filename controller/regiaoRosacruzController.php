<?php
@include_once("model/regiaoRosacruzClass.php");

@include_once("../model/regiaoRosacruzClass.php");

class regiaoRosaCruzController {

    private $regiaoRosacruz;

    public function __construct() {

        $this->regiaoRosacruz = new RegiaoRosacruz();
    }

    public function cadastraRegiaoRosacruz() {

        $this->regiaoRosacruz->setRegiaoRosacruz($_POST["regiaoRosacruz"]);
        $this->regiaoRosacruz->setDivisaoPolitica($_POST["divisaoPolitica"]);
        $this->regiaoRosacruz->setPaisRosacruz($_POST["paisRosacruz"]);
        $this->regiaoRosacruz->setDescricaoRegiaoRosacruz($_POST["descricaoRegiaoRosacruz"]);

        if ($this->regiaoRosacruz->cadastraRegiaoRosacruz()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaRegiaoRosacruz&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa Região Rosacruz!');
		  		window.location = '../painelDeControle.php?corpo=buscaRegiaoRosacruz';
		  </script>";
        }
    }

 	public function criarComboBox($id = 0,$regiao = null) {
 		if($regiao==null)
 		{
        	$resultado = $this->regiaoRosacruz->listaRegiaoRosacruz(1);
 		}else{
 			$resultado = $this->regiaoRosacruz->listaRegiaoRosacruz(1,$regiao);
 		}

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if (($id != 0 && $id == $vetor["idRegiaoRosacruz"])||($regiao != null && $regiao == $vetor["regiaoRosacruz"])) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idRegiaoRosacruz'] . '" ' . $selecionado . '>' . $vetor["regiaoRosacruz"] . '</option>';
            }
        }
    }

    public function listaRegiaoRosacruz() {
        
        $resultado = $this->regiaoRosacruz->listaRegiaoRosacruz();
        return $resultado;
    }

    public function buscaRegiaoRosacruz() {

        $resultado = $this->regiaoRosacruz->buscaIdRegiaoRosacruz();

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->regiaoRosacruz->setIdRegiaoRosacruz($vetor['idRegiaoRosacruz']);
            	$this->regiaoRosacruz->setRegiaoRosacruz($vetor['regiaoRosacruz']);
                $this->regiaoRosacruz->setDivisaoPolitica($vetor['divisaoPolitica']);
                $this->regiaoRosacruz->setPaisRosacruz($vetor['paisRosacruz']);
                $this->regiaoRosacruz->setDescricaoRegiaoRosacruz($vetor['descricaoRegiaoRosacruz']);
            }

            return $this->regiaoRosacruz;
        } else {
            return false;
        }
    }

    public function alteraRegiaoRosacruz() {

        $this->regiaoRosacruz->setRegiaoRosacruz($_POST["regiaoRosacruz"]);
        $this->regiaoRosacruz->setDivisaoPolitica($_POST["divisaoPolitica"]);
        $this->regiaoRosacruz->setPaisRosacruz($_POST["paisRosacruz"]);
        $this->regiaoRosacruz->setDescricaoRegiaoRosacruz($_POST["descricaoRegiaoRosacruz"]);

        if ($this->regiaoRosacruz->alteraRegiaoRosacruz($_POST['idRegiaoRosacruz'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaRegiaoRosacruz&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar esta Região Rosacruz!');
			window.location = '../painelDeControle.php?corpo=buscaRegiaoRosacruz';
			</script>";
        }
    }

    public function removeRegiaoRosacruz($id) {
        $resultado = $this->regiaoRosacruz->removeRegiaoRosacruz($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Região Rosacruz excluida com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaRegiaoRosacruz';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir esta Regiao Rosacruz!');
				window.location = '../painelDeControle.php?corpo=buscaRegiaoRosacruz';
				</script>";
        }
    }

}
?>