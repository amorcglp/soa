<?php
@include_once("model/faqClass.php");

@include_once("../model/faqClass.php");

class faqController {

    private $faq;

    public function __construct() {

        $this->faq = new Faq();
    }

    public function cadastroFaq() {
        
        $this->faq->setSeq_cadast($_POST["seq_cadast"]);
        $this->faq->setNomeUsuarioFaq($_POST["nomeUsuarioFaq"]);
        $this->faq->setCodigoDeAfiliacaoUsuarioFaq($_POST["codigoDeAfiliacaoUsuarioFaq"]);
        $this->faq->setEmailUsuarioFaq($_POST["emailUsuarioFaq"]);
        $this->faq->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->faq->setTituloPerguntaFaq($_POST["tituloPerguntaFaq"]);
        $this->faq->setPerguntaFaq($_POST["perguntaFaq"]);


        if ($this->faq->cadastraFaq()) {

            echo "<script type='text/javascript'>
		   window.location = '../painelDeControle.php?corpo=faq&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa FAQ!');
		  window.location = '../painelDeControle.php?corpo=cadastroFaq';
		  </script>";
        }
    }

    public function listaFaq() {

        $resultado = $this->faq->listaFaq();

        return $resultado;
    }

    public function buscaFaq() {

        $resultado = $this->faq->buscarIdFaq();

        if ($resultado) {

            foreach ($resultado as $vetor) {

                $this->faq->setNomeUsuario($vetor["nomeUsuario"]);
                $this->faq->setCodigoDeAfiliacao($vetor["codigoDeAfiliacao"]);
                $this->faq->setEmailUsuario($vetor["emailUsuario"]);
                $this->faq->setOrganismoAfiliado($vetor["organismoAfiliado"]);
                $this->faq->setTituloPerguntaFaq($vetor["tituloPerguntaFaq"]);
                $this->faq->setPerguntaFaq($vetor["perguntaFaq"]);
            }

            return $this->faq;
        } else {
            return false;
        }
    }

}
?>