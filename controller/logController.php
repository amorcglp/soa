<?php

include_once("model/logSistemaClass.php");
@include_once("../model/logSistemaClass.php");
require_once ("model/criaSessaoClass.php");

date_default_timezone_set('America/Sao_Paulo');

class LogSistemaController {

    private $logSistema;

    public function __construct() {

        $this->logSistema = new LogSistema();
    }

    public function verificaUsuario() {
        $sessao = new criaSessao();
        return $sessao->getValue("loginUsuario");
    }

    public function Registro() {

//pega o path completo de onde esta executanto
        $caminhoAtual = getcwd();

//muda o contexto de execução para a pasta logs
        @chdir("log");
        $data = date("d-m-y");
        $hora = date("H:i:s");
        $ip = $_SERVER['REMOTE_ADDR'];
        $host = getHostByAddr($ip);
        $pagina = $this->linkDaPagina();
        $browser = $this->verificaNavegador();
        $usuario = $this->verificaUsuario(); //Nome do arquivo:
        $arquivo = "Registro_$data.txt";

//Texto a ser impresso no log:
        $texto = "[$data][$hora][$ip][$host][$usuario][$pagina][$browser]\r\n";

        $manipular = fopen("$arquivo", "a+b");
        fwrite($manipular, $texto);
        fclose($manipular);

//Volta o contexto de execução para o caminho em que estava antes
        @chdir($caminho_atual);
    }

    public function verificaNavegador() {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('|MSIE ([0-9].[0-9]{1,2})|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'IE';
        } elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Opera';
        } elseif (preg_match('|Firefox/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Firefox';
        } elseif (preg_match('|Chrome/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Chrome';
        } elseif (preg_match('|Safari/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Safari';
        } else {
            // browser not recognized!
            $browser_version = 0;
            $browser = 'other';
        }
        return $browser;
    }

    public function linkDaPagina() {
        $protocolo = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === false) ? 'http' : 'https';
        $host = $_SERVER['HTTP_HOST'];
        $script = $_SERVER['SCRIPT_NAME'];
        $parametros = $_SERVER['QUERY_STRING'];
        $UrlAtual = $protocolo . '://' . $host . $script . '?' . $parametros;

        return $UrlAtual;
    }

}
?>