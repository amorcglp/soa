<?php

@include_once("../../model/organismoClass.php");
@include_once("../model/organismoClass.php");
@include_once("model/organismoClass.php");

@include_once("../../model/organismoHistoricoClass.php");
@include_once("../model/organismoHistoricoClass.php");
@include_once("model/organismoHistoricoClass.php");

@include_once("../../model/cidadeClass.php");
@include_once("../model/cidadeClass.php");
@include_once("model/cidadeClass.php");

@include_once("../../model/estadoClass.php");
@include_once("../model/estadoClass.php");
@include_once("model/estadoClass.php");

@include_once("../../lib/functions.php");
@include_once("../lib/functions.php");
@include_once("lib/functions.php");

class organismoController {

    private $organismo;
    private $cidade;
    private $estado;

    public function __construct() {

        $this->organismo = new organismo();
        $this->cidade = new Cidade();
        $this->estado = new Estado();
    }
   
    public function cadastroOrganismo() {
    			
        $this->organismo->setNomeOrganismoAfiliado($_POST["nomeOrganismoAfiliado"]);
        $this->organismo->setSiglaOrganismoAfiliado($_POST["siglaOrganismoAfiliado"]);
        $this->organismo->setEraSiglaOrganismoAfiliado($_POST["eraSiglaOrganismoAfiliado"]);
        $this->organismo->setPaisOrganismoAfiliado($_POST['paisOrganismoAfiliado']);
        $this->organismo->setTipoOrganismoAfiliado($_POST["tipoOrganismoAfiliado"]);
        $this->organismo->setClassificacaoOrganismoAfiliado($_POST["classificacaoOrganismoAfiliado"]);
        $this->organismo->setCepOrganismoAfiliado(substr($_POST['cepOrganismoAfiliado'],0,5).substr($_POST['cepOrganismoAfiliado'],6,3));
        $this->organismo->setFk_idRegiaoOrdemRosacruz($_POST["fk_idRegiaoOrdemRosacruz"]);
        $this->organismo->setCodigoAfiliacaoOrganismoAfiliado($_POST["codigoAfiliacaoOrganismoAfiliado"]);
        $this->organismo->setEnderecoOrganismoAfiliado($_POST["enderecoOrganismoAfiliado"]);
        $this->organismo->setNumeroOrganismoAfiliado($_POST["numeroOrganismoAfiliado"]);
        $this->organismo->setEmailOrganismoAfiliado($_POST["emailOrganismoAfiliado"]);
        $this->organismo->setSenhaEmailOrganismoAfiliado($_POST["senhaEmailOrganismoAfiliado"]);
        $this->organismo->setSituacaoOrganismoAfiliado($_POST["situacaoOrganismoAfiliado"]);
        $this->organismo->setFk_idEstado($_POST["fk_idEstado"]);
        $this->organismo->setFk_idCidade($_POST["fk_idCidade"]);
        $this->organismo->setBairroOrganismoAfiliado($_POST["bairroOrganismoAfiliado"]);
        $this->organismo->setComplementoOrganismoAfiliado($_POST["complementoOrganismoAfiliado"]);
        $this->organismo->setCnpjOrganismoAfiliado($_POST["cnpjOrganismoAfiliado"]);
        $this->organismo->setOperadoraCelularOrganismoAfiliado($_POST["operadoraCelularOrganismoAfiliado"]);
        $this->organismo->setCelularOrganismoAfiliado($_POST["celularOrganismoAfiliado"]);
        $this->organismo->setTelefoneFixoOrganismoAfiliado($_POST["telefoneFixoOrganismoAfiliado"]);
        $this->organismo->setOutroTelefoneOrganismoAfiliado($_POST["outroTelefoneOrganismoAfiliado"]);
        $this->organismo->setFaxOrganismoAfiliado($_POST["faxOrganismoAfiliado"]);
        $this->organismo->setCaixaPostalOrganismoAfiliado($_POST["caixaPostalOrganismoAfiliado"]);
        $this->organismo->setCepCaixaPostalOrganismoAfiliado($_POST["cepCaixaPostalOrganismoAfiliado"]);
        $this->organismo->setEnderecoCorrespondenciaOrganismoAfiliado($_POST["enderecoCorrespondenciaOrganismoAfiliado"]);
        $this->organismo->setSeqCadast($_POST["seq_cadast"]);
        $this->organismo->setLatitude($_POST["latitude"]);
        $this->organismo->setLongitude($_POST["longitude"]);
        $this->organismo->setNaoCobrarDashboard($_POST['naoCobrarDashboard']);
        
        if($_POST["seq_cadast"]!=0||$_POST["seq_cadast"]!="")
        {
        	if($this->organismo->verificaSeJaCadastrado($_POST["seq_cadast"]))
        	{
        		echo "<script type='text/javascript'>
		   			 window.location = '../painelDeControle.php?corpo=buscaOrganismo&jaCadastrado=1';
		  			</script>";
        	}else{
                    $idOrganismo = $this->organismo->cadastroOrganismo();
                    
                    /*Preencher Historico
                    $historico = new organismoHistorico();
                    $historico->setClassificacao($_POST["classificacaoOrganismoAfiliado"]);
                    $historico->setFk_idOrganismoAfiliado($idOrganismo);
                    $historico->setData(date('Y-m-d H:i:s'));
                    $historico->cadastraHistorico();
                        */
	            	echo "<script type='text/javascript'>
			    		window.location = '../painelDeControle.php?corpo=buscaOrganismo&salvo=1';
				  </script>";
		        
        	}
        }else{
        	echo "<script type='text/javascript'>
                    alert('Organismo não encontrado no sistema interno e não pode cadastrado!');
		   			 window.location = '../painelDeControle.php?corpo=buscaOrganismo';
		  			</script>";
        }
        
        
    }

    public function criarComboBox($id = 0, $siglaOA = null, $siglaComoId = null, $menosId = null,$idRegiao = null) {

        if ($idRegiao != null) {
            $resultado = $this->organismo->listaOrganismo(null, null, null, null, $idRegiao);
        } elseif ($menosId != null) {
            $resultado = $this->organismo->listaOrganismo(null, null, null, null, null, null, $menosId);
        } elseif ($siglaOA == null) {
            $resultado = $this->organismo->listaOrganismo();
        } else {
            $resultado = $this->organismo->listaOrganismo($siglaOA);
        }

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if (($id != 0 && $id == $vetor["idOrganismoAfiliado"]) || ($siglaOA == $vetor['siglaOrganismoAfiliado'])) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }

                switch ($vetor['classificacaoOrganismoAfiliado']) {
                    case 1:
                        $classificacao = "Loja";
                        break;
                    case 2:
                        $classificacao = "Pronaos";
                        break;
                    case 3:
                        $classificacao = "Capítulo";
                        break;
                    case 4:
                        $classificacao = "Heptada";
                        break;
                    case 5:
                        $classificacao = "Atrium";
                        break;
                }
                $classificacaoTemporaria="";
                switch ($vetor['classificacaoTemporaria']) {
                    case 1:
                        $classificacaoTemporaria = "Loja";
                        break;
                    case 2:
                        $classificacaoTemporaria = "Pronaos";
                        break;
                    case 3:
                        $classificacaoTemporaria = "Capítulo";
                        break;
                    case 4:
                        $classificacaoTemporaria = "Heptada";
                        break;
                    case 5:
                        $classificacaoTemporaria = "Atrium";
                        break;
                }
                if($classificacaoTemporaria!="")
                {
                    $classificacao=$classificacaoTemporaria;
                }
                switch ($vetor['tipoOrganismoAfiliado']) {
                    case 1:
                        $tipo = "R+C";
                        break;
                    case 2:
                        $tipo = "TOM";
                        break;
                }
                if ($siglaComoId == null) {
                    echo '<option value="' . $vetor['idOrganismoAfiliado'] . '" ' . $selecionado . '>' . $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor["nomeOrganismoAfiliado"] . '</option>';
                } else {
                    echo '<option value="' . $vetor['siglaOrganismoAfiliado'] . '" ' . $selecionado . '>' . $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor["nomeOrganismoAfiliado"] . '</option>';
                }
            }
        }
    }

    public function listaidOrganismoAfiliado() {
        $retorno = $this->organismo->listaOrganismo();
        $listaOrganismo = "";

        foreach ($retorno as $dados) {
            $listaOrganismo[] = $dados['idOrganismoAfiliado'];
        }

        if ($listaOrganismo) {
            return $listaOrganismo;
        } else {
            return false;
        }
    }

    public function listaOrganismo() {
    	
        if (!isset($_POST['nomeOrganismoAfiliado'])) {
            $resultado = $this->organismo->listaOrganismo();
        } else {
        	/*
            $this->organismo->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->organismo->buscaNomeOrganismo();
        	*/
        }

        return $resultado;
    }
    
	public function buscaOrganismo($idOrganismo) {

        $resultado = $this->organismo->buscaIdOrganismo($idOrganismo);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
                        //echo "===============>".$vetor["senhaOrganismoAfiliado"];exit();
			$this->organismo->setSeqCadast($vetor["seqCadast"]);	              
                        $this->organismo->setNomeOrganismoAfiliado($vetor["nomeOrganismoAfiliado"]);
		        $this->organismo->setSiglaOrganismoAfiliado($vetor["siglaOrganismoAfiliado"]);
		        $this->organismo->setPaisOrganismoAfiliado($vetor['paisOrganismoAfiliado']);
		        $this->organismo->setTipoOrganismoAfiliado($vetor["tipoOrganismoAfiliado"]);
		        $this->organismo->setStatusOrganismoAfiliado($vetor["statusOrganismoAfiliado"]);
		        $this->organismo->setClassificacaoOrganismoAfiliado($vetor["classificacaoOrganismoAfiliado"]);
                        $this->organismo->setAtuacaoTemporaria($vetor["classificacaoTemporaria"]);
		        $this->organismo->setCepOrganismoAfiliado(substr($vetor['cepOrganismoAfiliado'],0,5). "-" .substr($vetor['cepOrganismoAfiliado'],5,3));
		        $this->organismo->setFk_idRegiaoOrdemRosacruz($vetor["fk_idRegiaoOrdemRosacruz"]);
		        $this->organismo->setCodigoAfiliacaoOrganismoAfiliado($vetor["codigoAfiliacaoOrganismoAfiliado"]);
		        $this->organismo->setEnderecoOrganismoAfiliado($vetor["enderecoOrganismoAfiliado"]);
		        $this->organismo->setNumeroOrganismoAfiliado($vetor["numeroOrganismoAfiliado"]);
		        $this->organismo->setEmailOrganismoAfiliado($vetor["emailOrganismoAfiliado"]);
                        $this->organismo->setSenhaEmailOrganismoAfiliado($vetor["senhaEmailOrganismoAfiliado"]);
		        $this->organismo->setSituacaoOrganismoAfiliado($vetor["situacaoOrganismoAfiliado"]);
		        $this->organismo->setFk_idEstado($vetor["fk_idEstado"]);
		        $this->organismo->setFk_idCidade($vetor["fk_idCidade"]);
		        $this->organismo->setBairroOrganismoAfiliado($vetor["bairroOrganismoAfiliado"]);
		        $this->organismo->setComplementoOrganismoAfiliado($vetor["complementoOrganismoAfiliado"]);
		        $this->organismo->setCnpjOrganismoAfiliado($vetor["cnpjOrganismoAfiliado"]);
		        $this->organismo->setOperadoraCelularOrganismoAfiliado($vetor["operadoraCelularOrganismoAfiliado"]);
		        $this->organismo->setCelularOrganismoAfiliado($vetor["celularOrganismoAfiliado"]);
		        $this->organismo->setTelefoneFixoOrganismoAfiliado($vetor["telefoneFixoOrganismoAfiliado"]);
		        $this->organismo->setOutroTelefoneOrganismoAfiliado($vetor["outroTelefoneOrganismoAfiliado"]);
		        $this->organismo->setFaxOrganismoAfiliado($vetor["faxOrganismoAfiliado"]);
		        $this->organismo->setCaixaPostalOrganismoAfiliado($vetor["caixaPostalOrganismoAfiliado"]);
		        $this->organismo->setCepCaixaPostalOrganismoAfiliado($vetor["cepCaixaPostalOrganismoAfiliado"]);
		        $this->organismo->setEnderecoCorrespondenciaOrganismoAfiliado($vetor["enderecoCorrespondenciaOrganismoAfiliado"]);
		        $this->organismo->setLatitude($vetor["latitude"]);
                $this->organismo->setLongitude($vetor["longitude"]);
                $this->organismo->setNaoCobrarDashboard($vetor["naoCobrarDashboard"]);
            }

            return $this->organismo;
        } else {
            return false;
        }
    }

    public function alteraOrganismo() {
    	
    	/*
    	 * Atualizar no ORCZ as informações do OA
    	 */
    	header('Content-Type: text/html; charset=utf-8');
    	$ocultar_json=1;
    	$codigoAfiliacao=$_POST["codigoAfiliacaoOrganismoAfiliado"];
        //echo "cod=>".$codigoAfiliacao;exit();
    	$classificacaoOA=retornaLetraClassificacaoOa($_POST["classificacaoOrganismoAfiliado"]);
    	$siglaOA=$_POST["siglaOrganismoAfiliado"];
    	$nomeOA=organismoAfiliadoNomeCompleto($_POST["classificacaoOrganismoAfiliado"],$_POST["tipoOrganismoAfiliado"],$_POST["nomeOrganismoAfiliado"],$_POST["siglaOrganismoAfiliado"]);
        $nomeOA=  removeAcentos($nomeOA);
    	$pais=retornaSiglaPais($_POST['paisOrganismoAfiliado']);
    	$logradouro= removeAcentos(utf8_decode($_POST["enderecoOrganismoAfiliado"]));
    	$numero=$_POST["numeroOrganismoAfiliado"];
    	$complemento=removeAcentos(utf8_decode($_POST["complementoOrganismoAfiliado"]));
    	$bairro=$_POST["bairroOrganismoAfiliado"];
    	$cidade = $this->cidade->retornaCidadeTexto($_POST["fk_idCidade"]);
    	$uf= $this->estado->retornaUfTexto($_POST["fk_idEstado"]);
    	$cep=substr($_POST['cepOrganismoAfiliado'],0,5).substr($_POST['cepOrganismoAfiliado'],6,3);
    	$email=$_POST["emailOrganismoAfiliado"];
    	$telefoneFixo=str_replace("(","",str_replace(")","",str_replace("-","",$_POST["telefoneFixoOrganismoAfiliado"])));
    	$celular=str_replace("(","",str_replace(")","",str_replace("-","",$_POST["celularOrganismoAfiliado"])));
    	$outroTelefone=str_replace("(","",str_replace(")","",str_replace("-","",$_POST["outroTelefoneOrganismoAfiliado"])));
    	//$descricaoOutroTelefone=retornaOperadoraCelularTexto($_POST["operadoraCelularOrganismoAfiliado"]);
    	$cnpj=str_replace(".","",str_replace("/","",str_replace("-","",$_POST["cnpjOrganismoAfiliado"])));
    	$fax=str_replace("(","",str_replace(")","",str_replace("-","",$_POST["faxOrganismoAfiliado"])));
    	$caixaPostal=$_POST["caixaPostalOrganismoAfiliado"];
    	$cepCaixaPostal=$_POST["cepCaixaPostalOrganismoAfiliado"];
    	$enderecoCorrespondencia=$_POST["enderecoCorrespondenciaOrganismoAfiliado"];
        $latitude = $_POST["latitude"];
        $logitude = $_POST["longitude"];
        $CodUsuario=$_POST["usuario"]."s";
    	include('../js/ajax/atualizarOa.php');
	$obj = json_decode(json_encode($return),true);
        
		
	
    	$this->organismo->setNomeOrganismoAfiliado($_POST["nomeOrganismoAfiliado"]);
        $this->organismo->setSiglaOrganismoAfiliado($_POST["siglaOrganismoAfiliado"]);
        $this->organismo->setPaisOrganismoAfiliado($_POST['paisOrganismoAfiliado']);
        $this->organismo->setTipoOrganismoAfiliado($_POST["tipoOrganismoAfiliado"]);
        $this->organismo->setStatusOrganismoAfiliado($_POST["statusOrganismoAfiliado"]);
        $this->organismo->setClassificacaoOrganismoAfiliado($_POST["classificacaoOrganismoAfiliado"]);
        $this->organismo->setAtuacaoTemporaria($_POST["atuacaoTemporaria"]);
        $this->organismo->setCepOrganismoAfiliado(substr($_POST['cepOrganismoAfiliado'],0,5).substr($_POST['cepOrganismoAfiliado'],6,3));
        $this->organismo->setFk_idRegiaoOrdemRosacruz($_POST["fk_idRegiaoOrdemRosacruz"]);
        $this->organismo->setCodigoAfiliacaoOrganismoAfiliado($codigoAfiliacao);
        $this->organismo->setEnderecoOrganismoAfiliado($_POST["enderecoOrganismoAfiliado"]);
        $this->organismo->setNumeroOrganismoAfiliado($_POST["numeroOrganismoAfiliado"]);
        $this->organismo->setEmailOrganismoAfiliado($_POST["emailOrganismoAfiliado"]);
        $this->organismo->setSenhaEmailOrganismoAfiliado($_POST["senhaEmailOrganismoAfiliado"]);
        $this->organismo->setSituacaoOrganismoAfiliado($_POST["situacaoOrganismoAfiliado"]);
        $this->organismo->setFk_idEstado($_POST["fk_idEstado"]);
        $this->organismo->setFk_idCidade($_POST["fk_idCidade"]);
        $this->organismo->setBairroOrganismoAfiliado($_POST["bairroOrganismoAfiliado"]);
        $this->organismo->setComplementoOrganismoAfiliado($_POST["complementoOrganismoAfiliado"]);
        $this->organismo->setCnpjOrganismoAfiliado($_POST["cnpjOrganismoAfiliado"]);
        //$this->organismo->setOperadoraCelularOrganismoAfiliado($_POST["operadoraCelularOrganismoAfiliado"]);
        $this->organismo->setCelularOrganismoAfiliado($_POST["celularOrganismoAfiliado"]);
        $this->organismo->setTelefoneFixoOrganismoAfiliado($_POST["telefoneFixoOrganismoAfiliado"]);
        $this->organismo->setOutroTelefoneOrganismoAfiliado($_POST["outroTelefoneOrganismoAfiliado"]);
        $this->organismo->setFaxOrganismoAfiliado($_POST["faxOrganismoAfiliado"]);
        $this->organismo->setCaixaPostalOrganismoAfiliado($_POST["caixaPostalOrganismoAfiliado"]);
        $this->organismo->setCepCaixaPostalOrganismoAfiliado($_POST["cepCaixaPostalOrganismoAfiliado"]);
        $this->organismo->setEnderecoCorrespondenciaOrganismoAfiliado($_POST["enderecoCorrespondenciaOrganismoAfiliado"]);
        $this->organismo->setLatitude($_POST["latitude"]);
        $this->organismo->setLongitude($_POST["longitude"]);
        $this->organismo->setNaoCobrarDashboard($_POST['naoCobrarDashboard']);
//exit();
        if ($this->organismo->alteraOrganismo($_POST['idOrganismoAfiliado'])) 
        {
        
            /*Preencher Historico
            $historico = new organismoHistorico();
            $historico->setClassificacao($_POST["classificacaoOrganismoAfiliado"]);
            $historico->setFk_idOrganismoAfiliado($_POST['idOrganismoAfiliado']);
            $historico->setData(date('Y-m-d H:i:s'));
            $historico->cadastraHistorico();
            */
            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaOrganismo&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar o Organismo Afiliado!');
			window.location = '../painelDeControle.php?corpo=alteraOrganismo';
			</script>";
        }
    }
	
    /*
    public function removeOrganismo($idOrganismoAfiliado) {
        $resultado = $this->organismo->removeOrganismo($idOrganismoAfiliado);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Organismo Afiliado exclu\u00eddo com sucesso!');
				window.location = '../painelDeControle.php?url=buscaOrganismo&id=" . $idOrganismoAfiliado . "';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('N\u00e3o foi poss\u00edvel excluir o Organismo Afiliado!');
				window.location = '../painelDeControle.php?url=alteraOrganismo&id=" . $idOrganismoAfiliado . "';
				</script>";
        }
    }
    */

}

?>