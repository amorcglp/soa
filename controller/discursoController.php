<?php

@include_once "model/discursoClass.php";
@include_once "../model/discursoClass.php";

@include_once 'model/documentoClass.php';
@include_once '../model/documentoClass.php';

class DiscursoController {
    
    private $discurso;
    private $documento;
    
    public function __construct() {
        $this->discurso = new Discurso();
        $this->documento = new Documento();
    }
    
    public function cadastroDiscurso() {
        $this->discurso->setNumeroMensagem($_POST['numeroMensagem']);
        $this->discurso->setAutorMensagem($_POST['autorMensagem']);
        $this->discurso->setTituloMensagem($_POST['tituloMensagem']);
        $this->discurso->setDataCriacao(substr($_POST['dataCriacao'],6,4)."-".substr($_POST['dataCriacao'],3,2)."-".substr($_POST['dataCriacao'],0,2));
        $this->discurso->setTemExperimento($_POST['temExperimento']);
        $this->discurso->setQtdeImpressao($_POST['qtdeImpressao']);
        $this->discurso->setFkSeqCadast($_POST['seqCadast']);
        $this->discurso->setFkIdDocumento($_POST['idDocumento']);
        $this->discurso->setTipo($_POST['tipo']);
        
        $resultado = $this->discurso->listarDiscurso(null);
        
        if($resultado) {
            foreach ($resultado as $vetor) {
                if($vetor['numeroMensagem']==$this->discurso->getNumeroMensagem()) {
                    echo "<script type=\"text/javascript\">";
                    echo "window.location='../painelDeControle.php?corpo=buscaDiscurso&jaCadastrado=1';";
                    echo "</script>";
                    return false;
                }
            }
        }
        
        if($this->discurso->cadastraDiscurso()) {
            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaDiscurso&salvo=1';
		  </script>";
        }
        else {
            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse discurso!');
		  window.location = '../painelDeControle.php?corpo=cadastroDiscurso';
		  </script>";
        }
    }
    
    public function alteraDiscurso() {
        /*
        echo "<br>->".$_POST['idDiscurso']
                ."<br>->".$_POST['numeroMensagem']
                ."<br>->".$_POST['autorMensagem']
                ."<br>->".$_POST['tituloMensagem']
                ."<br>->".substr($_POST['dataCriacao'],6,4)."-".substr($_POST['dataCriacao'],3,2)."-".substr($_POST['dataCriacao'],0,2)
                ."<br>->".$_POST['temExperimento']
                ."<br>->".$_POST['qtdeImpressao']
                ."<br>->".$_POST['idDocumento'];
        exit();
         */
        $this->discurso->setIdDiscurso($_POST['idDiscurso']);
        $this->discurso->setNumeroMensagem($_POST['numeroMensagem']);
        $this->discurso->setAutorMensagem($_POST['autorMensagem']);
        $this->discurso->setTituloMensagem($_POST['tituloMensagem']);
        $this->discurso->setDataCriacao(substr($_POST['dataCriacao'],6,4)."-".substr($_POST['dataCriacao'],3,2)."-".substr($_POST['dataCriacao'],0,2));
        $this->discurso->setTemExperimento($_POST['temExperimento']);
        $this->discurso->setQtdeImpressao($_POST['qtdeImpressao']);
        $this->discurso->setFkIdDocumento($_POST['idDocumento']);
        $this->discurso->setTipo($_POST['tipo']);
        
        if($this->discurso->getFkIdDocumento() != null) {
            $resultado = $this->documento->buscarDocumentoId($this->discurso->getFkIdDocumento());
            if(count($resultado)>0) {
                  $this->documento->setDescricao(
                          $this->discurso->getNumeroMensagem()
                          .' - '
                          .$this->discurso->getTituloMensagem()
                          .($this->discurso->getAutorMensagem()==""?"":" ("
                          .$this->discurso->getAutorMensagem()
                          .")"));
                  $this->documento->setIdDocumento($this->discurso->getFkIdDocumento());
                  if(!$this->documento->alteraDescricaoDocumento()) { 
                      echo "<script type='text/javascript'>
                        alert ('Nao foi possivel atualizar descrição de arquivo!');
                         window.location = '../painelDeControle.php?corpo=buscaDiscurso';
                         </script>";
                        }
            }
        }
         
        if ($this->discurso->alteraDiscurso()) {
            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaDiscurso&salvo=1';
		  </script>";
        }
        else {
            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel atualizar esse discurso!');
		  window.location = '../painelDeControle.php?corpo=buscaDiscurso';
		  </script>";
        }
    }
    
    public function listaDiscurso($fk_seqCadast=null) {
        return $this->discurso->listarDiscurso($fk_seqCadast);
    }
    
    public function listaDiscursoId($id, $fk_seqCadast=null) {
        return $this->discurso->listarDiscursoId($id, $fk_seqCadast);
    }
    
    public function insereIdDocumento($idDiscurso) {
        return $this->discurso->cadastraIdDocumento($idDiscurso);
    }
    
    public function setIdDocumento($idDocumento) {
        $this->discurso->setFkIdDocumento($idDocumento);
    }
    
    public function removerDocumento($idDiscurso) {
        return $this->discurso->removeDocumento($idDiscurso);
    }
    
    public function excluirDiscurso($idDiscurso) {
        $this->discurso->setIdDiscurso($idDiscurso);
        
        $resultado = $this->discurso->excluiDiscurso();
        return count($resultado) ? $resultado : false;
    }
    
}