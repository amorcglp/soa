<?php

@include_once("model/isencaoOAClass.php");
@include_once("../model/isencaoOAClass.php");

@include_once("model/organismoClass.php");
@include_once("../model/organismoClass.php");

//@include_once ("../model/criaSessaoClass.php");

class isencaoOAController {

	private $isencaoOA;
	private $organismo;
	//private $sessao;

	public function __construct()
	{
		//$this->sessao 		= new criaSessao();
		$this->isencaoOA 		= new isencaoOA();
		$this->organismo		= new organismo();
		
	}

	public function cadastroIsencaoOA() {

		$dataInicial = substr($_POST['dataInicial'],6,4)."-".substr($_POST['dataInicial'],3,2)."-".substr($_POST['dataInicial'],0,2);
		$dataFinal = substr($_POST['dataFinal'],6,4)."-".substr($_POST['dataFinal'],3,2)."-".substr($_POST['dataFinal'],0,2);
		
		if(!$this->isencaoOA->verificaSeJaCadastrado($_POST['seqCadastMembroOa'],$dataInicial,$dataFinal,$_POST['fk_idOrganismoAfiliado']))
		{
			$this->isencaoOA->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		    $this->isencaoOA->setCodigoAfiliacao($_POST['codigoAfiliacao']);
		    $this->isencaoOA->setNomeMembroOa($_POST['nomeMembroOa']);
		    $this->isencaoOA->setSeqCadastMembroOa($_POST['seqCadastMembroOa']);
		    $this->isencaoOA->setDataInicial($dataInicial);
		    $this->isencaoOA->setDataFinal($dataFinal);
		    $this->isencaoOA->setMotivo($_POST['motivo']);
		    $this->isencaoOA->setUsuario($_POST['usuario']);
		   
			//Cadastrar ata e pegar ultimo id inserido
			$return = $this->isencaoOA->cadastroIsencaoOA();
			 		
			if ($return) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaIsencaoOA&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar esse membro!');
	                    window.location = '../painelDeControle.php?corpo=buscaIsencaoOA';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaIsencaoOA&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaIsencaoOA() {
		$retorno = $this->isencaoOA->listaIsencaoOA();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaMembroOaPeloSeqCadast($seqCadast) {

		$resultado = $this->isencaoOA->buscaMembroOaPeloSeqCadast($seqCadast);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);"]);
				$this->isencaoOA->setCodigoAfiliacao($vetor['codigoAfiliacao']);
				$this->isencaoOA->setNomeMembroOa($vetor['nomeMembroOa']);
			}

			return $this->isencaoOA;
		} else {
			return false;
		}
	}
	
 	public function buscaIsencaoOa($id) {

        $resultado = $this->isencaoOA->buscaIsencaoOaPeloId($id);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
            	$dataInicial = substr($vetor['dataInicial'],8,2)."/".substr($vetor['dataInicial'],5,2)."/".substr($vetor['dataInicial'],0,4);
            	$dataFinal = substr($vetor['dataFinal'],8,2)."/".substr($vetor['dataFinal'],5,2)."/".substr($vetor['dataFinal'],0,4);
				//echo "<pre>";print_r($vetor);
                $this->isencaoOA->setIdIsencaoOa($vetor['idIsencaoOa']);
                $this->isencaoOA->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
                $this->isencaoOA->setCodigoAfiliacao($vetor['codigoAfiliacao']);
                $this->isencaoOA->setNomeMembroOa($vetor['nomeMembroOa']);
                $this->isencaoOA->setSeqCadastMembroOa($vetor['seqCadastMembroOa']);
                $this->isencaoOA->setDataInicial($dataInicial);
                $this->isencaoOA->setDataFinal($dataFinal);
                $this->isencaoOA->setMotivo($vetor['motivo']);
                $this->isencaoOA->setUsuario($vetor['usuario']);
            }

            return $this->isencaoOA;
        } else {
            return false;
        }
    }
    
	public function alteraIsencaoOa() {
        
            $dataInicial = substr($_POST['dataInicial'],6,4)."-".substr($_POST['dataInicial'],3,2)."-".substr($_POST['dataInicial'],0,2);
            $dataFinal = substr($_POST['dataFinal'],6,4)."-".substr($_POST['dataFinal'],3,2)."-".substr($_POST['dataFinal'],0,2);
		
            //$this->isencaoOA->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
	    $this->isencaoOA->setCodigoAfiliacao($_POST['codigoAfiliacao']);
	    $this->isencaoOA->setNomeMembroOa($_POST['nomeMembroOa']);
	    $this->isencaoOA->setSeqCadastMembroOa($_POST['seqCadastMembroOa']);
	    $this->isencaoOA->setDataInicial($dataInicial);
	    $this->isencaoOA->setDataFinal($dataFinal);
	    $this->isencaoOA->setMotivo($_POST['motivo']);
            $this->isencaoOA->setUltimoAtualizar($_POST['usuario']);
        
        if ($this->isencaoOA->alteraIsencaoOa($_POST['idIsencaoOa'])) {

            echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaIsencaoOA&salvo=1';
			 	</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar o registro da Isenção!');
			window.location = '../painelDeControle.php?corpo=buscaIsencaoOa';
			</script>";
        }
    }

}

?>