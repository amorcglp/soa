<?php
@include_once("model/opcaoSubMenuClass.php");

@include_once("../model/opcaoSubMenuClass.php");

class opcaoSubMenuController {

    private $opcaoSubMenu;

    public function __construct() {

        $this->opcaoSubMenu = new opcaoSubMenu();
    }

    public function cadastraOpcaoSubMenu() {

        $this->opcaoSubMenu->setNomeOpcaoSubMenu($_POST["nomeOpcaoSubMenu"]);
        $this->opcaoSubMenu->setDescricaoOpcaoSubMenu($_POST["descricaoOpcaoSubMenu"]);
        $this->opcaoSubMenu->setArquivoOpcaoSubMenu($_POST["arquivoOpcaoSubMenu"]);
        $this->opcaoSubMenu->setPrioridadeOpcaoSubMenu($_POST["prioridadeOpcaoSubMenu"]);
        $this->opcaoSubMenu->setIconeOpcaoSubMenu($_POST["iconeOpcaoSubMenu"]);
        $this->opcaoSubMenu->setFkIdSubMenu($_POST["fk_idSubMenu"]);

        if ($this->opcaoSubMenu->cadastraOpcaoSubMenu()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaOpcaoSubMenu&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa Op��o Submenu!');
		  		window.location = '../painelDeControle.php?corpo=buscaOpcaoSubMenu';
		  </script>";
        }
    }

 	public function criarComboBox($id = 0) {
        $resultado = $this->opcaoSubMenu->listaOpcaoSubMenu();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idOpcaoSubMenu"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idOpcaoSubMenu'] . '" ' . $selecionado . '>' . $vetor["opcaoSubMenu"] . '</option>';
            }
        }
    }

    public function listaOpcaoSubMenu() {
        
        $resultado = $this->opcaoSubMenu->listaOpcaoSubMenu();
        return $resultado;
    }

    public function buscaOpcaoSubMenu($id) {

        $resultado = $this->opcaoSubMenu->buscaIdOpcaoSubMenu($id);

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->opcaoSubMenu->setIdOpcaoSubMenu($vetor['idOpcaoSubMenu']);
            	$this->opcaoSubMenu->setNomeOpcaoSubMenu($vetor['nomeOpcaoSubMenu']);
            	$this->opcaoSubMenu->setDescricaoOpcaoSubMenu($vetor['descricaoOpcaoSubMenu']);
                $this->opcaoSubMenu->setArquivoOpcaoSubMenu($vetor['arquivoOpcaoSubMenu']);
                $this->opcaoSubMenu->setPrioridadeOpcaoSubMenu($vetor['prioridadeOpcaoSubMenu']);
                $this->opcaoSubMenu->setIconeOpcaoSubMenu($vetor['iconeOpcaoSubMenu']);
                $this->opcaoSubMenu->setFkIdSubMenu($vetor['fk_idSubMenu']);
                $this->opcaoSubMenu->setFkIdSecaoMenu($vetor['fk_idSecaoMenu']);
            }

            return $this->opcaoSubMenu;
        } else {
            return false;
        }
    }

    public function alteraOpcaoSubMenu() {

        $this->opcaoSubMenu->setNomeOpcaoSubMenu($_POST["nomeOpcaoSubMenu"]);
        $this->opcaoSubMenu->setDescricaoOpcaoSubMenu($_POST["descricaoOpcaoSubMenu"]);
        $this->opcaoSubMenu->setArquivoOpcaoSubMenu($_POST["arquivoOpcaoSubMenu"]);
        $this->opcaoSubMenu->setPrioridadeOpcaoSubMenu($_POST['prioridadeOpcaoSubMenu']);
        $this->opcaoSubMenu->setIconeOpcaoSubMenu($_POST['iconeOpcaoSubMenu']);
        $this->opcaoSubMenu->setFkIdSubMenu($_POST['fk_idSubMenu']);
        

        if ($this->opcaoSubMenu->alteraOpcaoSubMenu($_POST['idOpcaoSubMenu'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaOpcaoSubMenu&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N�o foi poss�vel alterar esta Op��o Submenu!');
			window.location = '../painelDeControle.php?corpo=buscaOpcaoSubMenu';
			</script>";
        }
    }

	public function removeOpcaoSubMenu($id) {
        $resultado = $this->opcaoSubMenu->removeOpcaoSubMenu($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Op��o Submenu exclu�da com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaOpcaoSubMenu';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir esta Op��o Submenu!');
				window.location = '../painelDeControle.php?corpo=buscaOpcaoSubMenu';
				</script>";
        }
    }

}
?>