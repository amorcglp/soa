<?php

@include_once("../model/ritoPassagemOGGClass.php");
@include_once("model/ritoPassagemOGGClass.php");

@include_once("../model/ritoPassagemOGGMembroClass.php");
@include_once("model/ritoPassagemOGGMembroClass.php");

@include_once("../model/ritoPassagemOGGMembroTestemunhaClass.php");
@include_once("model/ritoPassagemOGGMembroTestemunhaClass.php");

@include_once("../model/ritoPassagemOGGNaoMembroClass.php");
@include_once("model/ritoPassagemOGGNaoMembroClass.php");

class ritoPassagemOGGController {

    private $ritoPassagem;
    private $membro;
    private $naoMembro;

    public function __construct() {
        
        $this->ritoPassagem = new ritoPassagemOGG();
        $this->membro = new ritoPassagemOGGMembro();
        $this->membroTestemunha = new ritoPassagemOGGMembroTestemunha();
        $this->naoMembro = new ritoPassagemOGGNaoMembro();
        
    }

    public function cadastro() {
	
        $data = substr($_POST["data"],6,4)."-".substr($_POST["data"],3,2)."-".substr($_POST["data"],0,2);
        
        $this->ritoPassagem->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->ritoPassagem->setRito($_POST['rito']);
        $this->ritoPassagem->setData($data);
        $this->ritoPassagem->setHora($_POST['hora']);
        $this->ritoPassagem->setComentario($_POST['comentario']);
        $this->ritoPassagem->setSeqCadastMestre($_POST['h_seqCadastMembroMestre']);
        $this->ritoPassagem->setNomeMestre($_POST['h_nomeMembroMestre']);
        $this->ritoPassagem->setUsuario($_POST['usuario']);
        $ultimoId = $this->ritoPassagem->cadastro();
        
        //echo "<pre>";print_r($_REQUEST);
        
        //Cadastrar Membros
        $return=false;
        $ata_oa_mensal_oficiais=$_REQUEST["ata_oa_mensal_oficiais"];
        
        for ($i=0;$i<count($ata_oa_mensal_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_oficiais[$i]);
                $json = json_decode($stringJson);  
                //echo $json;exit();
                $this->membro 	= new ritoPassagemOGGMembro();
                $this->membro->setFk_idRitoPassagemOGG($ultimoId);
                $this->membro->setFk_seq_cadastMembro($json->seqCadast);
                $this->membro->setTipo($json->tipoMembro);
                $this->membro->setEmail($json->emailMembro);
                $this->membro->cadastro();
                $return=true;
        }
        
        //Cadastrar Membros Testemunhas
        $return=false;
        $ata_oa_mensal_oficiais_testemunhas=$_REQUEST["ata_oa_mensal_oficiais_testemunhas"];
        
        for ($i=0;$i<count($ata_oa_mensal_oficiais_testemunhas);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_oficiais_testemunhas[$i]);
                $json = json_decode($stringJson);  
                //echo $json;exit();
                $this->membroTestemunha 	= new ritoPassagemOGGMembroTestemunha();
                $this->membroTestemunha->setFk_idRitoPassagemOGG($ultimoId);
                $this->membroTestemunha->setFk_seq_cadastMembro($json->seqCadast);
                $this->membroTestemunha->setTipo($json->tipoMembro);
                $this->membroTestemunha->setEmail($json->emailMembro);
                $this->membroTestemunha->cadastro();
                $return=true;
        }
        
        //Cadastrar Nao Membros
        $return=false;
        $ata_oa_mensal_nao_oficiais=$_REQUEST["ata_oa_mensal_nao_oficiais"];
        for ($i=0;$i<count($ata_oa_mensal_nao_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_nao_oficiais[$i]);
                $json = json_decode($stringJson); 
                $this->naoMembro = new ritoPassagemOGGNaoMembro();
                $this->naoMembro->setFk_idRitoPassagemOGG($ultimoId);
                $this->naoMembro->setNome($json->nomeNaoMembro);
                $this->naoMembro->setTipo($json->tipoNaoMembro);
                $this->naoMembro->setEmail($json->emailNaoMembro);
                $this->naoMembro->cadastro();
                $return=true;
        }
        
        
        //exit();
    	if ($ultimoId) {
           
            echo "<script type='text/javascript'>
                    alert('Informação do Rito cadastrado com sucesso!');
		    window.location = '../painelDeControle.php?corpo=buscaRitosPassagem';
		  </script>";
        } else {
            exit();
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essa informação sobre o Rito de Passagem!');
                    window.location = '../painelDeControle.php?corpo=buscaRitosPassagem';
		  </script>";
            
        }
    }

    
    public function lista($idOrganismoAfiliado,$mes=null,$ano=null,$rito=null,$menosRito=null) {
        $retorno = $this->ritoPassagem->lista(null,$idOrganismoAfiliado,$mes,$ano,$rito,$menosRito);
        //echo "<pre>";print_r($retorno);
        if ($retorno) {
            return $retorno;
        } else {
            return false;
        }
    }

    public function altera() {
        
        $idRitoPassagemOGG = $_POST['idRitoPassagemOGG'];
	
        $data = substr($_POST["data"],6,4)."-".substr($_POST["data"],3,2)."-".substr($_POST["data"],0,2);
        
        $this->ritoPassagem->setIdRitoPassagemOGG($idRitoPassagemOGG);
        $this->ritoPassagem->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->ritoPassagem->setRito($_POST['rito']);
        $this->ritoPassagem->setData($data);
        $this->ritoPassagem->setHora($_POST['hora']);
        $this->ritoPassagem->setComentario($_POST['comentario']);
        $this->ritoPassagem->setSeqCadastMestre($_POST['h_seqCadastMembroMestre']);
        $this->ritoPassagem->setNomeMestre($_POST['h_nomeMembroMestre']);
        $this->ritoPassagem->setUltimoAtualizar($_POST['usuario']);
        $this->ritoPassagem->altera();
        
        //Excluir todos os membros e nao membros
        $this->membro 	= new ritoPassagemOGGMembro();
        $this->membro->remove($idRitoPassagemOGG);
        
        $this->membroTestemunha 	= new ritoPassagemOGGMembroTestemunha();
        $this->membroTestemunha->remove($idRitoPassagemOGG);
        
        $this->naoMembro = new ritoPassagemOGGNaoMembro();
        $this->naoMembro->remove($idRitoPassagemOGG);
        
        //Cadastrar Membros
        $return=false;
        $ata_oa_mensal_oficiais=$_REQUEST["ata_oa_mensal_oficiais"];
        for ($i=0;$i<count($ata_oa_mensal_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_oficiais[$i]);
                $json = json_decode($stringJson);  
                //echo $json;exit();
                $this->membro 	= new ritoPassagemOGGMembro();
                $this->membro->setFk_idRitoPassagemOGG($_POST['idRitoPassagemOGG']);
                $this->membro->setFk_seq_cadastMembro($json->seqCadast);
                $this->membro->setTipo($json->tipoMembro);
                $this->membro->setEmail($json->emailMembro);
                $this->membro->cadastro();
                $return=true;
        }
        
        //Cadastrar Membros
        $return=false;
        $ata_oa_mensal_oficiais_testemunhas=$_REQUEST["ata_oa_mensal_oficiais_testemunhas"];
        
        for ($i=0;$i<count($ata_oa_mensal_oficiais_testemunhas);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_oficiais_testemunhas[$i]);
                $json = json_decode($stringJson);  
                //echo $json;exit();
                $this->membroTestemunha 	= new ritoPassagemOGGMembroTestemunha();
                $this->membroTestemunha->setFk_idRitoPassagemOGG($_POST['idRitoPassagemOGG']);
                $this->membroTestemunha->setFk_seq_cadastMembro($json->seqCadast);
                $this->membroTestemunha->setTipo($json->tipoMembro);
                $this->membroTestemunha->setEmail($json->emailMembro);
                $this->membroTestemunha->cadastro();
                $return=true;
        }
        
        //Cadastrar Nao Membros
        $return=false;
        $ata_oa_mensal_nao_oficiais=$_REQUEST["ata_oa_mensal_nao_oficiais"];
        for ($i=0;$i<count($ata_oa_mensal_nao_oficiais);$i++)
        {
                $stringJson =str_replace("'","\"",$ata_oa_mensal_nao_oficiais[$i]);
                $json = json_decode($stringJson); 
                $this->naoMembro = new ritoPassagemOGGNaoMembro();
                $this->naoMembro->setFk_idRitoPassagemOGG($_POST['idRitoPassagemOGG']);
                $this->naoMembro->setNome($json->nomeNaoMembro);
                $this->naoMembro->setTipo($json->tipoNaoMembro);
                $this->naoMembro->setEmail($json->emailNaoMembro);
                $this->naoMembro->cadastro();
                $return=true;
        }
        //exit();
    	if ($return) {
           
            echo "<script type='text/javascript'>
                    alert('Rito de Passagem alterado com sucesso!');
		    window.location = '../painelDeControle.php?corpo=buscaRitosPassagem';
		  </script>";
        } else {
            exit();
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel alterar esse Rito de Passagem!');
                    window.location = '../painelDeControle.php?corpo=buscaRitosPassagem';
		  </script>";
            
        }
    }

}

?>