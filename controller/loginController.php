<?php

include_once ("../model/loginClass.php");
include_once ("../model/criaSessaoClass.php");
include_once ("../model/funcaoUsuarioClass.php");
include_once ("../model/organismoClass.php");
include_once('../model/departamentoNivelDePermissaoClass.php');
include_once('../model/funcaoNivelDePermissaoClass.php');
require_once ("../model/tentativaLoginClass.php");
require_once ("../model/acessoClass.php");
require_once ("../model/loginClass.php");
require_once ("../model/usuarioClass.php");
require_once ("../loadEnv.php");

class loginController {

    private $sessao;
    private $funcaoUsuario;
    private $departamentoNivel;
    private $funcaoNivel;
    private $tentativaLogin;
    private $acesso;
    private $login;
    private $usuario;

    public function __construct() {
        $this->sessao = new criaSessao();
        $this->funcaoUsuario = new FuncaoUsuario();
        $this->organismo = new organismo();
        $this->departamentoNivel = new DepartamentoNivelDePermissao();
        $this->funcaoNivel = new FuncaoNivelDePermissao();
        $this->tentativaLogin = new tentativaLogin();
        $this->acesso = new acesso();
        $this->login = new login();
        $this->usuario = new usuario();
    }

    public function efetuaLogin($ip) {

        /*
         * Observação importante: se for mudar a toleracia existem vários locais para mudar, então mudar na variável abaixo.....................
         *
         */
        $diasTolerancia = 0;

        $login = new Login();
        $login->loginUsuario = ($_POST['loginUsuario']);
        $login->senhaUsuario = ($_POST['senhaUsuario']);
        
        $hashVerificado=false;

        if(isset($_POST['verificarHash'])) {

            if($_POST['verificarHash'] == true)
                $hashVerificado = $this->login->verificaHash($login->loginUsuario, $login->senhaUsuario);
        }
        $resultado = $login->efetuaLogin();
        if (count($resultado)) {
            foreach ($resultado as $vetor) {
                if (isset($vetor["loginUsuario"])) {

                    $senhaDigitada = (string) $_POST['senhaUsuario'];
                    $hashDB = $vetor["senhaUsuario"];

                    if(crypt($senhaDigitada, $hashDB) === $hashDB || $hashVerificado == true) {

                        if($vetor["statusUsuario"] == 0)
                        {
                            $arrData = array();
                            $arrData['key'] = $vetor["seqCadast"];
                            $logged=true;
                            require_once ('../sec/make.php');

                            //Atualizar funções do usuário
                            include_once('../lib/functions.php');
                            include_once('../model/organismoClass.php');
                            include_once('../model/usuarioClass.php');
                            include_once('../model/funcaoUsuarioClass.php');
                            include_once('../model/funcaoClass.php');
                            include_once('../model/funcaoUsuarioClass.php');

                            if(getenv('ENVIRONMENT') !== 'development') {

                                require_once '../webservice/wsInovad.php';

                                $seqCadast = $vetor["seqCadast"];

                                //error_reporting(E_ALL);
                                ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
                                ini_set("display_errors", 1);

                                //Limpar funções do usuário
                                $funcaoUsuario = new FuncaoUsuario();
                                $funcaoUsuario->setFk_seq_cadast($seqCadast);
                                $funcaoUsuario->removePorUsuario();

                                //Popular funções do usuário
                                $array = [0 => $seqCadast];
                                $teste = '';
                                foreach ($array as $key => $value) {
                                    if ($teste != '') {
                                        $teste = $teste . ', ' . $value;
                                    } else {
                                        $teste = $value;
                                    }
                                }

                                $siglaOrganismoLotacao = "";
                                $temFuncao = false;

                                //Buscar Funções
                                $vars = array('seq_cadast' => '[' . $teste . ']');
                                $resposta20 = json_decode(json_encode(restAmorc("membros/oficiais", $vars)), true);
                                $obj = json_decode(json_encode($resposta20), true);
                                //echo "<pre>";print_r($obj);exit();

                                $arrFuncoes = array();
                                if (isset($obj['data'][0])) {

                                    //Atualizar Código de Afiliacao se preciso
                                    if($vetor["codigoDeAfiliacao"]!=$obj['data'][0]['cod_rosacr'])
                                    {
                                        $u = new Usuario();
                                        $u->atualizaCodigoDeAfiliacaoUsuario($seqCadast,$obj['data'][0]['cod_rosacr']);
                                    }

                                    $arrFuncoes = $obj['data'][0]['funcoes'];

                                    foreach ($arrFuncoes as $vetor10) {

                                        if ($vetor10['seq_tipo_funcao_oficial'] == 100)//Se tiver dignitária priorizar lotação
                                        {
                                            if ($vetor10['dat_saida'] == 0 && isset($vetor10['funcao']) && isset($vetor10['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                                            {
                                                $dataTerminoMandato = $vetor10['dat_termin_mandat'];
                                                $codFuncao = $vetor10['funcao']['seq_funcao'];
                                                $tipoCargo = $vetor10['funcao']['seq_tipo_funcao_oficial'];
                                                $dataEntrada = $vetor10['dat_entrad'];
                                                $desFuncao = substr($vetor10['funcao']['des_funcao'], 0, 3);

                                                $siglaOrganismoWS = $vetor10['oa']['sig_orgafi'];

                                                $data1_inteiro = strtotime("+".$diasTolerancia." days", strtotime(substr($dataTerminoMandato, 0, 10)));
                                                //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                                $data2_inteiro = strtotime(date("Y-m-d"));

                                                //if($desFuncao!="EX-")
                                                //{
                                                //echo "entrou diferente de ex"."<br>";
                                                if ($data1_inteiro >= $data2_inteiro) {
                                                    //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                                    //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                                    if ($vetor10['dat_saida'] == null) {
                                                        if ($vetor10['funcao']['ide_tipo_oa'] == 'O') {
                                                            //Pegar a sigla do Organismo
                                                            $siglaOrganismoLotacao = $vetor10['oa']['sig_orgafi'];
                                                        }
                                                    }
                                                }
                                                //}
                                            }
                                        }
                                    }
                                    //Se não conseguiu encontrar uma lotação para função dignitária pegar administrativa
                                    if ($siglaOrganismoLotacao == "") {
                                        foreach ($arrFuncoes as $vetor11) {
                                            //echo "<pre>";print_r($vetor);exit();
                                            if ($vetor11['seq_tipo_funcao_oficial'] == 200)//Se tiver administrativa priorizar lotação
                                            {
                                                if ($vetor11['dat_saida'] == 0 && isset($vetor11['funcao']) && isset($vetor11['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                                                {
                                                    $dataTerminoMandato = $vetor11['dat_termin_mandat'];
                                                    $codFuncao = $vetor11['funcao']['seq_funcao'];
                                                    $tipoCargo = $vetor11['funcao']['seq_tipo_funcao_oficial'];
                                                    $dataEntrada = $vetor11['dat_entrad'];
                                                    $desFuncao = substr($vetor11['funcao']['des_funcao'], 0, 3);

                                                    $siglaOrganismoWS = $vetor11['oa']['sig_orgafi'];

                                                    $data1_inteiro = strtotime("+".$diasTolerancia." days", strtotime(substr($dataTerminoMandato, 0, 10)));
                                                    //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                                    $data2_inteiro = strtotime(date("Y-m-d"));

                                                    //if($desFuncao!="EX-")
                                                    //{
                                                    //echo "entrou diferente de ex"."<br>";
                                                    if ($data1_inteiro >= $data2_inteiro) {
                                                        //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                                        //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                                        if ($vetor11['dat_saida'] == null) {
                                                            if ($vetor11['funcao']['ide_tipo_oa'] == 'O') {
                                                                $siglaOrganismoLotacao = $vetor11['oa']['sig_orgafi'];
                                                            }
                                                        }
                                                    }
                                                    //}
                                                }
                                            }
                                        }
                                    }
                                    //Se não conseguiu encontrar uma lotação para função administrativa então pegar de qualquer outro tipo
                                    if ($siglaOrganismoLotacao == "") {
                                        foreach ($arrFuncoes as $vetor12) {
                                            if ($vetor12['dat_saida'] == 0 && isset($vetor12['funcao']) && isset($vetor12['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                                            {
                                                $dataTerminoMandato = $vetor12['dat_termin_mandat'];
                                                $codFuncao = $vetor12['funcao']['seq_funcao'];
                                                $tipoCargo = $vetor12['funcao']['seq_tipo_funcao_oficial'];
                                                $dataEntrada = $vetor12['dat_entrad'];
                                                $desFuncao = substr($vetor12['funcao']['des_funcao'], 0, 3);

                                                $siglaOrganismoWS = $vetor12['oa']['sig_orgafi'];

                                                $data1_inteiro = strtotime("+".$diasTolerancia." days", strtotime(substr($dataTerminoMandato, 0, 10)));
                                                //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                                $data2_inteiro = strtotime(date("Y-m-d"));

                                                //if($desFuncao!="EX-")
                                                //{
                                                //echo "entrou diferente de ex"."<br>";
                                                if ($data1_inteiro >= $data2_inteiro) {
                                                    //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                                    //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                                    if ($vetor12['dat_saida'] == null) {

                                                        if ($vetor12['funcao']['ide_tipo_oa'] == 'O') {
                                                            $siglaOrganismoLotacao = $vetor12['oa']['sig_orgafi'];
                                                        }
                                                    }
                                                }
                                                //}
                                            }
                                        }
                                    }

                                    //echo "<br>Sigla atualizada:".$siglaOrganismoLotacao;exit();

                                    foreach ($arrFuncoes as $vetor13) {
                                        //echo $vetor['funcao']['des_funcao']."-".$vetor['dat_saida']."<br>";
                                        if ($vetor13['dat_saida'] == 0 && isset($vetor13['funcao']) && isset($vetor13['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                                        {
                                            if ($vetor13['funcao']['ide_tipo_oa'] == "O") {//R+Ccccccccccccccccccccccccccccccccccccccccccccccccc veja aqui óoooo
                                                //echo "entrou data nula"."<br>";
                                                $dataTerminoMandato = $vetor13['dat_termin_mandat'];
                                                $codFuncao = $vetor13['funcao']['seq_funcao'];
                                                $tipoCargo = $vetor13['funcao']['seq_tipo_funcao_oficial'];
                                                $dataEntrada = $vetor13['dat_entrad'];
                                                $desFuncao = substr($vetor13['funcao']['des_funcao'], 0, 3);

                                                $siglaOrganismoWS = $vetor13['oa']['sig_orgafi'];

                                                $data1_inteiro = strtotime("+".$diasTolerancia." days", strtotime(substr($dataTerminoMandato, 0, 10)));
                                                //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                                $data2_inteiro = strtotime(date("Y-m-d"));

                                                // if($desFuncao!="EX-")
                                                // {
                                                //echo "entrou diferente de ex"."<br>";
                                                if ($data1_inteiro >= $data2_inteiro) {
                                                    //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                                    //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                                    if ($vetor13['dat_saida'] == null) {
                                                        $f = new funcao();
                                                        $f->setVinculoExterno($codFuncao);
                                                        $retorno7 = $f->buscaVinculoExterno();
                                                        if ($retorno7) {
                                                            foreach ($retorno7 as $vetor7) {
                                                                $fu = new funcaoUsuario();
                                                                $fu->setFk_seq_cadast($seqCadast);
                                                                $fu->setFk_idFuncao($vetor7['idFuncao']);
                                                                $fu->setDataInicioMandato(substr($dataEntrada, 0, 10));
                                                                $fu->setDataFimMandato(substr($dataTerminoMandato, 0, 10));
                                                                $fu->setSiglaOA($siglaOrganismoWS);
                                                                if (!$fu->verificaSeJaExiste()) {
                                                                    if ($fu->cadastra()) {
                                                                        $temFuncao = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //}
                                            }
                                        }
                                    }
                                    //Atualizado a lotação do usuario
                                    if ($siglaOrganismoLotacao != "") {
                                        if ($temFuncao) {
                                            $usua = new Usuario();
                                            $usua->setSeqCadast($seqCadast);
                                            $usua->setSiglaOA($siglaOrganismoLotacao);
                                            $resp = $usua->alteraLotacaoUsuario();
                                        } else {
                                            $usua = new Usuario();
                                            $usua->setSeqCadast($seqCadast);
                                            $usua->setSiglaOA("PR113");
                                            $resp = $usua->alteraLotacaoUsuario();
                                        }
                                    }
                                    //Se tem função então ativar status do usuário
                                    if ($temFuncao) {
                                        $usua = new Usuario();
                                        $usua->setSeqCadast($seqCadast);
                                        $usua->setStatusUsuario(0);
                                        $resp = $usua->alteraStatusUsuario();
                                    }
                                }
                            }
                            //Popular Sessão
                            $this->sessao->setValue("nomeUsuario",$vetor["nomeUsuario"]);

                            //Start nos Cookies com 'avatar','nome', 'login' e 'token'
                            $avatar = $vetor["avatarUsuario"] != '' ? $vetor["avatarUsuario"] : "img/default-user-small.png";
                            setcookie('av', $avatar, time() + 86400, "/");
                            setcookie('nm', $vetor["nomeUsuario"], time() + 86400, "/");
                            setcookie('lg', $vetor["loginUsuario"], time() + 86400, "/");

                            $this->sessao->setValue("tk",$token);
                            $this->sessao->setValue("vk",$arrData['key']);

                            //Popular sessão
                                $this->sessao->setValue("loginUsuario", $vetor["loginUsuario"]);
                            $this->sessao->setValue("seqCadast", $vetor["seqCadast"]);
                            $this->sessao->setValue("fk_idDepartamento", $vetor["fk_idDepartamento"]);

                            if($vetor["siglaOA"]!="")
                            {
                                $this->sessao->setValue("siglaOA", $vetor["siglaOA"]);
                            }else{
                                $this->sessao->setValue("siglaOA", "PR101");//Se vier vazio setar PR101 como oa do usuário
                            }

                            //Setar Id do OA de Atuação na sessão
                            $resultado2  = $this->organismo->listaOrganismo(null,$this->sessao->getValue("siglaOA"));
                            if($resultado2)
                            {
                                foreach($resultado2 as $vetor2)
                                {
                                    $this->sessao->setValue("idOrganismoAfiliado", $vetor2["idOrganismoAfiliado"]);
                                    $this->sessao->setValue("idRegiao", $vetor2["fk_idRegiaoOrdemRosacruz"]);
                                }
                            }

                            //Região
                            if($vetor["siglaOA"]!="")
                            {
                                $this->sessao->setValue("regiao", substr($vetor["siglaOA"],0,3));

                            }else{
                                $this->sessao->setValue("regiao", "PR1");//Se vier vazio setar PR1 como região do usuário
                            }


                            //Pegar funções e colocar na sessão
                            $this->funcaoUsuario->setFk_seq_cadast($vetor["seqCadast"]);
                            $string = $this->funcaoUsuario->buscaPorUsuario();
                            $this->sessao->setValue("funcoes", $string);

                            //Pegar os níveis e colocar na sessão
                            $this->departamentoNivel->setFkIdDepartamento($vetor["fk_idDepartamento"]);
                            $string2 = $this->departamentoNivel->buscaPorDepartamento(1);
                            $this->sessao->setValue("niveis", $string2);
                            if($string2==0||$string2=="")
                            {
                                $stringArrumada = str_replace(",","','",$string);
                                $string3 = $this->funcaoNivel->buscaPorFuncao(null,$stringArrumada);
                                $this->sessao->setValue("niveis", $string3);
                            }

                            //Corpo vindo da tela de bloqueio
                            $this->sessao->setValue("registro", time());
                            $this->sessao->setValue("limite", 1800); // manter 1800

                            $corpo = '';
                            if((isset($_POST['corpo'])) && ($_POST['corpo'] != '')){
                                $corpo = '?corpo=' . $_POST['corpo'];
                            }

                            if($this->tentativaLogin->limpaTentativaLogin($ip))
                            {
                                //Registrar acesso
                                $ip = $_SERVER['REMOTE_ADDR'];
                                $this->acesso->setSeqCadast($vetor["seqCadast"]);
                                $this->acesso->setIp($ip);
                                $this->acesso->setSiglaOA($vetor["siglaOA"]);
                                $this->acesso->setLogin($login->loginUsuario);
                                $this->acesso->setNome($vetor["nomeUsuario"]);
                                $this->acesso->cadastroAcesso();

                                if($vetor["cienteAssinaturaDigital"] == 0) {
                                    include("../termoAssinaturaDigital.php");
                                } else {
                                    echo "<script> window.location = '../painelDeControle.php" . $corpo . "';</script>";
                                }
                            }

                        } else {
                            echo '<script type="text/javascript"> window.location = "../login.php?inativo=1"; </script>';
                        }
                    } else {
                        $this->tentativaLogin->setIp($ip);
                        $this->tentativaLogin->setLogin($_POST['loginUsuario']);
                        $this->tentativaLogin->setSenha($_POST['senhaUsuario']);
                        if ($this->tentativaLogin->cadastroTentativaLogin())
                            echo '<script type="text/javascript"> window.location = "../login.php?incorreto=1"; </script>';
                    }
                }
            }
        } else {
            //Executa tentativa de Login
            $this->tentativaLogin->setIp($ip);
            $this->tentativaLogin->setLogin($_POST['loginUsuario']);
            $this->tentativaLogin->setSenha($_POST['senhaUsuario']);
            if($this->tentativaLogin->cadastroTentativaLogin())
                echo '<script type="text/javascript"> window.location = "../login.php?incorreto=1"; </script>';
        }
    }

    function efetuaLogout() {
        
        //Apagar cookies do token na memória
        unset($_COOKIE['tk']);
        unset($_COOKIE['vk']);
        
        //tirar usuário da tabela usersonline
        $seqCadast=$this->sessao->getValue("seqCadast");//seqCadast do usuário
        $this->login->removeUsuarioDeUsuariosOnline($seqCadast);
        
        //remover sessão
        $this->sessao->freeSession();
        
        //redirecionar
        echo '<script type="text/javascript">
                window.location = "../login.php";
              </script>';
    }

}
