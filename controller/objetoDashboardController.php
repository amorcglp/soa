<?php
@include_once("model/objetoDashboardClass.php");

@include_once("../model/objetoDashboardClass.php");

class objetoDashboardController {

    private $objetoDashboard;

    public function __construct() {

        $this->objetoDashboard = new ObjetoDashboard();
    }

    public function cadastraObjetoDashboard() {

        $this->objetoDashboard->setNomeObjetoDashboard($_POST["nomeObjetoDashboard"]);
        $this->objetoDashboard->setDescricaoObjetoDashboard($_POST["descricaoObjetoDashboard"]);
        $this->objetoDashboard->setArquivoObjetoDashboard($_POST["arquivoObjetoDashboard"]);
        $this->objetoDashboard->setPrioridadeObjetoDashboard($_POST["prioridadeObjetoDashboard"]);
        $this->objetoDashboard->setFkIdTipoObjetoDashboard($_POST["fk_idTipoObjetoDashboard"]);

        if ($this->objetoDashboard->cadastraObjetoDashboard()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaObjetoDashboard&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse Dashboard!');
		  		window.location = '../painelDeControle.php?corpo=buscaObjetoDashboard';
		  </script>";
        }
    }

 	public function criarComboBox($id = 0) {
        $resultado = $this->objetoDashboard->listaObjetoDashboard();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idObjetoDashboard"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idObjetoDashboard'] . '" ' . $selecionado . '>' . $vetor["nomeObjetoDashboard"] . '</option>';
            }
        }
    }

    public function listaObjetoDashboard() {
        
        $resultado = $this->objetoDashboard->listaObjetoDashboard();
        return $resultado;
    }

    public function buscaObjetoDashboard() {

        $resultado = $this->objetoDashboard->buscaIdObjetoDashboard();

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->objetoDashboard->setIdObjetoDashboard($vetor['idObjetoDashboard']);
            	$this->objetoDashboard->setNomeObjetoDashboard($vetor['nomeObjetoDashboard']);
            	$this->objetoDashboard->setDescricaoObjetoDashboard($vetor['descricaoObjetoDashboard']);
                $this->objetoDashboard->setArquivoObjetoDashboard($vetor['arquivoObjetoDashboard']);
                $this->objetoDashboard->setPrioridadeObjetoDashboard($vetor['prioridadeObjetoDashboard']);
                $this->objetoDashboard->setFkIdTipoObjetoDashboard($vetor['fk_idTipoObjetoDashboard']);
            }

            return $this->objetoDashboard;
        } else {
            return false;
        }
    }

    public function alteraObjetoDashboard() {

        $this->objetoDashboard->setNomeObjetoDashboard($_POST["nomeObjetoDashboard"]);
        $this->objetoDashboard->setDescricaoObjetoDashboard($_POST["descricaoObjetoDashboard"]);
        $this->objetoDashboard->setArquivoObjetoDashboard($_POST["arquivoObjetoDashboard"]);
        $this->objetoDashboard->setPrioridadeObjetoDashboard($_POST['prioridadeObjetoDashboard']);
        $this->objetoDashboard->setFkIdTipoObjetoDashboard($_POST['fk_idTipoObjetoDashboard']);
        

        if ($this->objetoDashboard->alteraObjetoDashboard($_POST['idObjetoDashboard'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaObjetoDashboard&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N�o foi poss�vel alterar este Dashboard!');
			window.location = '../painelDeControle.php?corpo=buscaObjetoDashboard';
			</script>";
        }
    }

	public function removeObjetoDashboard($id) {
        $resultado = $this->objetoDashboard->removeObjetoDashboard($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Dashboard exclu�do com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaObjetoDashboard';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir este Dashboard!');
				window.location = '../painelDeControle.php?corpo=buscaObjetoDashboard';
				</script>";
        }
    }

}
?>