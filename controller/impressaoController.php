<?php

@include_once '../model/impressaoClass.php';
@include_once 'model/impressaoClass.php';

class impressaoController {
    
    private $impressao;
    private $impressaoDiscurso;
    private $usuario;
    private $documento;
    private $discurso;
    
    public function __construct() {
        $this->impressao = new Impressao();
    }
   
    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    
    public function setDocumento($documento) {
        $this->documento = $documento;
    }
    
    public function setImpressaoDiscurso($impressaoDiscurso) {
        $this->impressaoDiscurso = $impressaoDiscurso;
    }
    
    public function setDiscurso($discurso) {
        $this->discurso = $discurso;
    }
    
    public function cadastroImpressao() {
        $this->impressao->setFk_seqCadast($_SESSION['seqCadast']);
       // $this->impressao->setDataHora(date('Y/m/d H:i:s'));
        $this->impressao->setFk_idDocumento($this->documento->getIdDocumento());
        $this->impressao->setIpMaquina($_SERVER['REMOTE_ADDR']);
        $this->impressao->setNomeUsuario($this->usuario->getNomeUsuario());
        $this->impressao->setCodigoDeAfiliacao($this->usuario->getCodigoDeAfiliacaoUsuario());
        
        $docDiscurso = $this->documento->getDocumentoTipo() == "Discurso" ;
 
        if($docDiscurso) {
            $proximoId = 1;
            $resultado = $this->impressao->getUltimoId();
            if($resultado) {
                foreach ($resultado as $vetor) {
                    $proximoId = $vetor['maxId'] + 1;
                }
            }
            $this->impressaoDiscurso->setFkidImpressao($proximoId);
            // Incrementa +1 para qtdeImpressao
            $proximoId = 1;
            $resultado = $this->impressaoDiscurso->getIdQtdeImpressao($this->impressaoDiscurso->getFkIdDiscurso());
            if($resultado) {
                foreach ($resultado as $vetor) {
                    $proximoId = $vetor['qtdeImpressao'] + 1;
                }
            }
            $this->impressaoDiscurso->setQtdeImpressao($proximoId);
            $this->impressaoDiscurso->inseriQtdeImpressao($this->impressaoDiscurso->getFkIdDiscurso());
        }
        
        if($this->impressao->cadastraImpressao() &&
                ($docDiscurso ? $this->impressaoDiscurso->cadastroDiscursoImpressao(false) : true)) {
            return true;
        } else {
            echo "<script type='text/javascript'>
                   alert ('Nao foi possivel registrar a impressão!');
                   window.location = 'painelDeControle.php';
                   </script>";
        }
    }
    
    public function listaImpressao($siglaOA=null) {
        return $this->impressao->listaImpressao($siglaOA);
    }
    
    public function getId() {
        $resultado = $this->impressao->getUltimoId();
        return count($resultado) ? $resultado : false;
    }
    
    public function listaTodasImpressao($discurso=false) {
        $resultado = $this->impressao->listarTodasImpressao($discurso);
        return count($resultado) ? $resultado : false;
    }
    
    public function excluirImpressao($idDocumento) {
        $this->impressao->setFk_idDocumento($idDocumento);
        
        $resultado = $this->impressao->excluiImpressao();
        return count($resultado) ? $resultado : false;
    }
    
    public function buscarImpressaoDocumento($idDocumento) {
        $this->impressao->setFk_idDocumento($idDocumento);
        return $this->impressao->buscaDocumentoImpresso();
    }
}
