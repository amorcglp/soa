<?php

@include_once("model/membroOAClass.php");
@include_once("../model/membroOAClass.php");

@include_once("model/organismoClass.php");
@include_once("../model/organismoClass.php");

//@include_once ("../model/criaSessaoClass.php");

class membroOAController {

	private $membroOA;
	private $organismo;
	//private $sessao;

	public function __construct()
	{
		//$this->sessao 		= new criaSessao();
		$this->membroOA 		= new membroOA();
		$this->organismo		= new organismo();
		
	}

	public function cadastroMembroOA() {

        if(!$this->membroOA->verificaSeJaCadastrado($_POST['seqCadastMembroOa']))
        {
            //Selecionar a sigla do OA
            $siglaOa="";
            $resultado = $this->organismo->listaOrganismo(null,null,null,null,null,$_POST['fk_idOrganismoAfiliado']);
            if($resultado)
            {
                foreach($resultado as $vetor)
                {
                    $siglaOa = $vetor['siglaOrganismoAfiliado'];
                }
            }

            //Atualizar OA atuação

            $seqCadast =$_POST['seqCadastMembroOa'];
            $status=0;

            include('../js/ajax/atualizarOaAtuacao.php');

            if($status==1) {
			
                $this->membroOA->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
                $this->membroOA->setCodigoAfiliacao($_POST['codigoAfiliacao']);
                $this->membroOA->setNomeMembroOa($_POST['nomeMembroOa']);
                $this->membroOA->setSeqCadastMembroOa($_POST['seqCadastMembroOa']);
                $this->membroOA->setPrincipalCompanheiro($_POST['principalCompanheiro']);
                $this->membroOA->setUsuario($_POST['usuario']);

                //Cadastrar ata e pegar ultimo id inserido
                $return = $this->membroOA->cadastroMembroOA();

                if ($return) {

                    echo "<script type='text/javascript'>
                            window.location = '../painelDeControle.php?corpo=buscaMembroOA&salvo=1';
                    </script>";
                } else {
                    echo "<script type='text/javascript'>
                            alert('Não foi possível cadastrar esse membro!');
                            window.location = '../painelDeControle.php?corpo=buscaMembroOA';
                  </script>";
                }
            }else{
                if($status==2) {
                    echo "<script type='text/javascript'>
                            window.location = '../painelDeControle.php?corpo=buscaMembroOA&salvo=2';
                    </script>";
                }else{
                    echo "<script type='text/javascript'>
                            alert('Ocorreu um erro ao cadastrar. Avise o Setor de Organismos Afiliados da GLP!');
                            window.location = '../painelDeControle.php?corpo=buscaMembroOA';
                    </script>";
                }
            }
        }else{
            echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaMembroOA&jaCadastrado=1&seqCadastMembroOa=".$_POST['seqCadastMembroOa']."';
			 	</script>";
        }
	}

	public function listaMembroOA($idOA=null,$falecimento=null,$letraNome=null) {
            
            if($idOA!=null)
            {    
		        $retorno = $this->membroOA->listaMembroOA($idOA,$falecimento,$letraNome);
            }else{
                $retorno = $this->membroOA->listaMembroOA(null,$falecimento,$letraNome);
            }
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaMembroOaPeloSeqCadast($seqCadast) {

		$resultado = $this->membroOA->buscaMembroOaPeloSeqCadast($seqCadast);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);"]);
				$this->membroOA->setCodigoAfiliacao($vetor['codigoAfiliacao']);
				$this->membroOA->setNomeMembroOa($vetor['nomeMembroOa']);
			}

			return $this->membroOA;
		} else {
			return false;
		}
	}

}

?>