<?php

@include_once("../model/tentativaLoginClass.php");
@include_once("model/tentativaLoginClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class tentativaLoginController {

    private $tentativaLogin;

    public function __construct() {

        $this->tentativaLogin = new tentativaLogin();
    }

    public function cadastroTentativaLogin($ipAntigo,$login,$senha) {
        
        $ip=  (int) str_replace(".", "", str_replace(":", "",$ipAntigo));
        //Filtrar Request
        $arrFiltroRequest=array(
                            'login',
                            'senha');
        if(validaSegurancaRequest($arrFiltroRequest))
        {
            enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
            echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
            exit();
        }
	
        //Filtrar Input
        $clean = array();
        $dirty = array(
        
        0 => array($ip,"numero"),
        1 => array($login,"textoNumero"),
        2 => array($senha,"textoNumero")
            
        );
        
        if(!validaSegurancaInput($dirty))
        {    
            $clean = $dirty;
        
            $this->tentativaLogin->setIp($ipAntigo);
            $this->tentativaLogin->setLogin($clean[1][0]);
            $this->tentativaLogin->setSenha($clean[2][0]);

            if ($this->tentativaLogin->cadastroTentativaLogin()) {
                return true;
            } else {
                return false;
            }
        }else{
            echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel efetuar o login pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                        window.location = '../index.php';
                       </script>";
        }
    }

    public function selecionaUltimaTentativaLogin($ip, $limite=null) {

        return $this->tentativaLogin->selecionaUltimaTentativaLogin($ip, $limite);
    }
    
    public function limpaTentativaLogin($ip) {
        
        $retorno = $this->tentativaLogin->limpaTentativaLogin($ip);

        if ($retorno) {
            return $retorno;
        } else {
            return false;
        }
    }
}

?>