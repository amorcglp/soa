<?php
@include_once("model/notificacaoAtualizacaoServerClass.php");

@include_once("../model/notificacaoAtualizacaoServerClass.php");

class notificacaoServerController {

    private $notificacaoServer;

    public function __construct() {

        $this->notificacaoServer = new NotificacaoServer();
    }

    public function cadastroNotificacaoServer() {

        $this->notificacaoServer->setTituloNotificacao($_POST["tituloNotificacao"]);
        $this->notificacaoServer->setDescricaoNotificacao($_POST["descricaoNotificacao"]);
        $this->notificacaoServer->setTipoNotificacao($_POST["tipoNotificacao"]);
        $dataAgendada = substr($_POST["dataAgendada"],6,4)."-".substr($_POST["dataAgendada"],3,2)."-".substr($_POST["dataAgendada"],0,2);
        $this->notificacaoServer->setDataAgendada($dataAgendada);

        if ($this->notificacaoServer->cadastraNotificacaoServer()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaNotificacaoAtualizacaoServer&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa notificacao!');
		  window.location = '../painelDeControle.php?corpo=cadastroNotificacaoAtualizacaoServer';
		  </script>";
        }
    }

    public function listaNotificacaoServer($ativo=null) {

    	if($ativo==null)
    	{
        	$resultado = $this->notificacaoServer->listaNotificacaoServer();
    	}else{
    		$resultado = $this->notificacaoServer->listaNotificacaoServer(1);
    	}

        return $resultado;
    }

    public function buscaNotificacaoServer($idNotificacao) {

        $resultado = $this->notificacaoServer->buscarIdNotificacaoServer($idNotificacao);

        if ($resultado) {

            foreach ($resultado as $vetor) {

                $this->notificacaoServer->setTituloNotificacao($vetor['tituloNotificacao']);
                $this->notificacaoServer->setDataNotificacao($vetor['dataNotificacao']);
                $this->notificacaoServer->setDescricaoNotificacao($vetor['descricaoNotificacao']);
                $this->notificacaoServer->setTipoNotificacao($vetor['tipoNotificacao']);
                $this->notificacaoServer->setStatusNotificacao($vetor['statusNotificacao']);
                $this->notificacaoServer->setDataAgendada($vetor['dataAgendada']);
            }

            return $this->notificacaoServer;
        } else {
            return false;
        }
    }

    public function alteraNotificacaoServer() {

        $this->notificacaoServer->setTituloNotificacao($_POST["tituloNotificacao"]);
        $this->notificacaoServer->setDescricaoNotificacao($_POST["descricaoNotificacao"]);
        $this->notificacaoServer->setTipoNotificacao($_POST["tipoNotificacao"]);
        $dataAgendada = substr($_POST["dataAgendada"],6,4)."-".substr($_POST["dataAgendada"],3,2)."-".substr($_POST["dataAgendada"],0,2);
        $this->notificacaoServer->setDataAgendada($dataAgendada);

        if ($this->notificacaoServer->alteraNotificacaoServer($_POST['idNotificacao'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaNotificacaoAtualizacaoServer&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar esta notificacao!');
			window.location = '../painelDeControle.php?corpo=buscaNotificacaoAtualizacaoServer';
			</script>";
        }
    }

    public function removeNotificacaoServer($id) {
        $resultado = $this->notificacaoServer->removeNotificacao($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				window.location = '?corpo=buscaNotificacaoAtualizacaoServer&excluiu=1';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir esta notificação!');
				window.location = '?corpo=alterarNotificacaoAtualizacaoServer';
				</script>";
        }
    }

}
?>