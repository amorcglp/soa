<?php
@include_once("model/organismoHistoricoClass.php");

@include_once("../model/organismoHistoricoClass.php");

class historicoOAController {

    private $historicoOA;

    public function __construct() {

        $this->historicoOA = new organismoHistorico();
    }

    public function cadastraHistorico() {

        $data = substr($_POST["data"],6,4).'-'.substr($_POST["data"],3,2).'-'.substr($_POST["data"],0,2);
        
        $this->historicoOA->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->historicoOA->setClassificacao($_POST["classificacao"]);
        $this->historicoOA->setData($data);

        if ($this->historicoOA->cadastraHistorico()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaHistoricoOA&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse Historico!');
		  		window.location = '../painelDeControle.php?corpo=buscaHistoricoOA';
		  </script>";
        }
    }

    public function listaHistoricoOA() {
        
        $resultado = $this->historicoOA->listaHistorico();
        return $resultado;
    }

    public function buscaHistorico($id) {

        $resultado = $this->historicoOA->buscaIdHistorico($id);

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->historicoOA->setIdOrganismoAfiliadoHistorico($vetor['idOrganismoAfiliadoHistorico']);
            	$this->historicoOA->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
            	$this->historicoOA->setClassificacao($vetor['classificacao']);
                $this->historicoOA->setData($vetor['data']);
            }

            return $this->historicoOA;
        } else {
            return false;
        }
    }

    public function alteraHistorico() {

        $data = substr($_POST["data"],6,4).'-'.substr($_POST["data"],3,2).'-'.substr($_POST["data"],0,2);
        
        $this->historicoOA->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->historicoOA->setClassificacao($_POST["classificacao"]);
        $this->historicoOA->setData($data);
        

        if ($this->historicoOA->alteraHistorico($_POST['idOrganismoAfiliadoHistorico'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaHistoricoOA&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Nao foi possivel alterar este Historico!');
			window.location = '../painelDeControle.php?corpo=buscaHistoricoOA';
			</script>";
        }
    }

    public function removeHistorico($id) {
        $resultado = $this->historicoOA->removeHistorico($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
            	alert ('Historico excluido com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaHistoricoOA&salvo=1';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir este Historico!');
				window.location = '../painelDeControle.php?corpo=buscaHistoricoOA';
				</script>";
        }
    }

}
?>