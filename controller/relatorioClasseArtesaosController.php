<?php

@include_once("model/relatorioClasseArtesaosClass.php");
@include_once("../model/relatorioClasseArtesaosClass.php");

@include_once("model/relatorioClasseArtesaosMembrosClass.php");
@include_once("../model/relatorioClasseArtesaosMembrosClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once ("../model/criaSessaoClass.php");

class relatorioClasseArtesaosController {

	private $relatorioClasseArtesaos;
        private $relatorio_membros;
	private $sessao;

	public function __construct()
	{
		$this->sessao               = new criaSessao();
		$this->relatorioClasseArtesaos    = new relatorioClasseArtesaos();
		$this->usuario              = new Usuario();
		
	}

	public function cadastroRelatorioClasseArtesaos() {

                $ultimo_id="";
                    
		$this->relatorioClasseArtesaos->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->relatorioClasseArtesaos->setMesCompetencia($_POST['mesCompetencia']);
                $this->relatorioClasseArtesaos->setAnoCompetencia($_POST['anoCompetencia']);
                $this->relatorioClasseArtesaos->setDataEncontro(substr($_POST['dataEncontro'],6,4)."-".substr($_POST['dataEncontro'],3,2)."-".substr($_POST['dataEncontro'],0,2));
		$this->relatorioClasseArtesaos->setNumeroParticipantes($_POST['numeroParticipantes']);
		$this->relatorioClasseArtesaos->setNaturezaMensagemApresentada($_POST['naturezaMensagemApresentada']);
		$this->relatorioClasseArtesaos->setQuestoesLevantadas($_POST['questoesLevantadas']);
                $this->relatorioClasseArtesaos->setClimaGeralClasse($_POST['climaGeralClasse']);
                $this->relatorioClasseArtesaos->setSurgiuProblema($_POST['surgiuProblema']);
                $this->relatorioClasseArtesaos->setSurgiuProblemaQual($_POST['surgiuProblemaQual']);
                $this->relatorioClasseArtesaos->setObservacoes($_POST['observacoes']);
		$this->relatorioClasseArtesaos->setUsuario($this->sessao->getValue("seqCadast"));
                

		//Cadastrar relatorioClasseArtesaos e pegar ultimo id inserido
		if(!$this->relatorioClasseArtesaos->verificaSeJaExiste())
		{
			$ultimo_id = $this->relatorioClasseArtesaos->cadastroRelatorioClasseArtesaos();
                        
                        //Cadastrar Membros que participaram da classe
			$return=false;
			$membros=$_POST["membros"];
			for ($i=0;$i<count($membros);$i++)
			{
				$this->relatorio_membros = new relatorioClasseArtesaosMembros();
				$this->relatorio_membros->setFk_idRelatorioClasseArtesaos($ultimo_id);
				$this->relatorio_membros->setFk_seq_cadastMembro($membros[$i]);
				$this->relatorio_membros->cadastro();
				$return=true;
			}
		
			/**
			 * Notificar GLP que a relatorioClasseArtesaos foi cadastrada
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Novo Relatorio Classe Artesaos Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Relatorio Classe Artesaos Entregue pelo Organismo ".$_POST['nomeOrganismoRelatorio'].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuarioRelatorio'];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/drelatorioClasseArtesaospicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/drelatorioClasseArtesaosTables/jquery.drelatorioClasseArtesaosTables.js\"></script>
			<script src=\"../js/plugins/drelatorioClasseArtesaosTables/drelatorioClasseArtesaosTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/drelatorioClasseArtesaosTables/drelatorioClasseArtesaosTables.responsive.js\"></script>
			<script src=\"../js/plugins/drelatorioClasseArtesaosTables/drelatorioClasseArtesaosTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
                        <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
                        <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
			//exit();	
			if ($ultimo_id!="") {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaRelatorioClasseArtesaos&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Nao foi possivel cadastrar esse relatório!');
	                    window.location = '../painelDeControle.php?corpo=buscaRelatorioClasseArtesaos';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaRelatorioClasseArtesaos&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaRelatorioClasseArtesaos($idOa=null) {
		$retorno = $this->relatorioClasseArtesaos->listaRelatorioClasseArtesaos($idOa);
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaRelatorioClasseArtesaos($idRelatorioClasseArtesaos) {

		$resultado = $this->relatorioClasseArtesaos->buscarIdRelatorioClasseArtesaos($idRelatorioClasseArtesaos);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->relatorioClasseArtesaos->setIdRelatorioClasseArtesaos($vetor['idRelatorioClasseArtesaos']);
				$this->relatorioClasseArtesaos->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
				$this->relatorioClasseArtesaos->setMesCompetencia($vetor['mesCompetencia']);
                                $this->relatorioClasseArtesaos->setAnoCompetencia($vetor['anoCompetencia']);
				$this->relatorioClasseArtesaos->setDataEncontro(substr($vetor['dataEncontro'],8,2)."/".substr($vetor['dataEncontro'],5,2)."/".substr($vetor['dataEncontro'],0,4));
				$this->relatorioClasseArtesaos->setNumeroParticipantes($vetor['numeroParticipantes']);
				$this->relatorioClasseArtesaos->setNaturezaMensagemApresentada($vetor['naturezaMensagemApresentada']);
				$this->relatorioClasseArtesaos->setQuestoesLevantadas($vetor['questoesLevantadas']);
				$this->relatorioClasseArtesaos->setClimaGeralClasse($vetor['climaGeralClasse']);
				$this->relatorioClasseArtesaos->setSurgiuProblema($vetor['surgiuProblema']);
                                $this->relatorioClasseArtesaos->setSurgiuProblemaQual($vetor['surgiuProblemaQual']);
                                $this->relatorioClasseArtesaos->setObservacoes($vetor['observacoes']);
                                
			}
			return $this->relatorioClasseArtesaos;
		} else {
			return false;
		}
	}
	

	public function alteraRelatorioClasseArtesaos() {

		//$this->relatorioClasseArtesaos->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->relatorioClasseArtesaos->setMesCompetencia($_POST['mesCompetencia']);
                $this->relatorioClasseArtesaos->setAnoCompetencia($_POST['anoCompetencia']);
                $this->relatorioClasseArtesaos->setDataEncontro(substr($_POST['dataEncontro'],6,4)."-".substr($_POST['dataEncontro'],3,2)."-".substr($_POST['dataEncontro'],0,2));
		$this->relatorioClasseArtesaos->setNumeroParticipantes($_POST['numeroParticipantes']);
		$this->relatorioClasseArtesaos->setNaturezaMensagemApresentada($_POST['naturezaMensagemApresentada']);
		$this->relatorioClasseArtesaos->setQuestoesLevantadas($_POST['questoesLevantadas']);
                $this->relatorioClasseArtesaos->setClimaGeralClasse($_POST['climaGeralClasse']);
                $this->relatorioClasseArtesaos->setSurgiuProblema($_POST['surgiuProblema']);
                $this->relatorioClasseArtesaos->setSurgiuProblemaQual($_POST['surgiuProblemaQual']);
                $this->relatorioClasseArtesaos->setObservacoes($_POST['observacoes']);
		$this->relatorioClasseArtesaos->setUltimoAtualizar($this->sessao->getValue("seqCadast"));
                
                //Remover todos os membros e inserir novamente
		$rcam= new relatorioClasseArtesaosMembros();
		$rcam->remove($_POST['fk_idRelatorioClasseArtesaos']);
		
		$return=false;
		$membros=$_POST["membros"];
		for ($i=0;$i<count($membros);$i++)
		{
			$this->relatorio_membros = new relatorioClasseArtesaosMembros();
			$this->relatorio_membros->setFk_idRelatorioClasseArtesaos($_POST['fk_idRelatorioClasseArtesaos']);
			$this->relatorio_membros->setFk_seq_cadastMembro($membros[$i]);
			$this->relatorio_membros->cadastro();
			$return=true;
		}
            
		if ($this->relatorioClasseArtesaos->alteraRelatorioClasseArtesaos($_POST['fk_idRelatorioClasseArtesaos'])) {

			echo
                        "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaRelatorioClasseArtesaos&salvo=1';
			</script>";
		} else {
			echo
                        "<script type='text/javascript'>
			alert ('Nao foi possivel alterar o relatório!');
			window.location = '../painelDeControle.php?corpo=buscaRelatorioClasseArtesaos';
			</script>";
		}
	}

}

?>