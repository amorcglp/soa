<?php

@include_once("model/funcaoClass.php");
@include_once("../model/funcaoClass.php");

class funcaoController {

    private $funcao;

    public function __construct() {

        $this->funcao = new funcao();
    }

    public function cadastroFuncao() {

        $this->funcao->setNomeFuncao($_POST['nomeFuncao']);
        $this->funcao->setVinculoExterno($_POST['vinculoExterno']);
        if($_POST['atuaRegiao'])
        {
        	$this->funcao->setAtuaRegiao(1);
        }else{
        	$this->funcao->setAtuaRegiao(0);
        }
        $this->funcao->setDescricaoFuncao($_POST['descricaoFuncao']);
        $this->funcao->setFk_idDepartamento($_POST['fk_idDepartamento']);

        if ($this->funcao->cadastroFuncao()) {

            echo "<script type='text/javascript'>
		    window.location = '../painelDeControle.php?corpo=buscaFuncao&salvo=1';
		  </script>";
        } else {
            echo "<script type='text/javascript'>
                    alert('Não foi possível cadastrar essa Função!');
                    window.location = '../painelDeControle.php?corpo=buscaFuncao';
		  </script>";
        }
    }

    public function criarComboBox($id = 0,$idVinculoExterno=null,$permissaoGLP=null) {
        $resultado = $this->funcao->listaFuncao();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idFuncao"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                if($idVinculoExterno==null)
                {
                	echo '<option value="' . $vetor['idFuncao'] . '" ' . $selecionado . '>' . $vetor["nomeFuncao"] . '</option>';
                }else{
                	if(((substr($vetor['vinculoExternoFuncao'],0,1)!="1"
                                        &&substr($vetor['vinculoExternoFuncao'],0,1)!="2"
                			&&substr($vetor['vinculoExternoFuncao'],0,1)!="8"
                			&&substr($vetor['vinculoExternoFuncao'],0,1)!="9"
                			)
                			&&
                			$permissaoGLP==null)
                			||
                			$permissaoGLP=="1"
                	)		
                	{		
                		echo '<option value="' . $vetor['vinculoExternoFuncao'] . '" ' . $selecionado . '>' .$vetor['vinculoExternoFuncao']." - ". $vetor["nomeFuncao"] . '</option>';
                	}
                }
            }
        }
    }

    public function listaidFuncao() {
        $retorno = $this->funcao->listaFuncao();
        $listaFuncao = "";

        foreach ($retorno as $dados) {
            $listaFuncao[] = $dados['idFuncao'];
        }

        if ($listaFuncao) {
            return $listaFuncao;
        } else {
            return false;
        }
    }

    public function listaFuncao() {
        if (!isset($_POST['nomeFuncao'])) {
            $resultado = $this->funcao->listaFuncao();
        } else {
            $this->funcao->setNomeFuncao($_POST['nomeFuncao']);
            $resultado = $this->funcao->buscaNomeFuncao();
        }
        return $resultado;
    }

	 public function buscaFuncao($idFuncao) {
	
	        $resultado = $this->funcao->buscarIdFuncao($idFuncao);
	
	
	        if ($resultado) {
	
	            foreach ($resultado as $vetor) {
	                $this->funcao->setIdFuncao($vetor['idFuncao']);
	                $this->funcao->setNomeFuncao($vetor['nomeFuncao']);
	                $this->funcao->setVinculoExterno($vetor['vinculoExterno']);
	                $this->funcao->setAtuaRegiao($vetor['atuaRegiao']);
	                $this->funcao->setDescricaoFuncao($vetor['descricaoFuncao']);
	                $this->funcao->setFk_idDepartamento($vetor['fk_idDepartamento']);
	            }
	            return $this->funcao;
	        } else {
	            return false;
	        }
	 }
    public function buscaNomeFuncao($idFuncao) {
        $resultado = $this->funcao->buscaIdFuncao($idFuncao);

        $nomeFuncao = "";

        foreach ($resultado as $dados) {
            $nomeFuncao = $dados['nomeFuncao'];
        }

        if ($nomeFuncao) {
            return $nomeFuncao;
        } else {
            return false;
        }
    }

    public function alteraFuncao() {
		
    	$this->funcao->setIdFuncao($_POST['idFuncao']);
        $this->funcao->setNomeFuncao($_POST['nomeFuncao']);
        $this->funcao->setVinculoExterno($_POST['vinculoExterno']);
    	if(isset($_POST['atuaRegiao']))
        {
        	$this->funcao->setAtuaRegiao(1);
        }else{
        	$this->funcao->setAtuaRegiao(0);
        }
        $this->funcao->setDescricaoFuncao($_POST['descricaoFuncao']);
        $this->funcao->setFk_idDepartamento($_POST['fk_idDepartamento']);

        if ($this->funcao->alteraFuncao($_POST['idFuncao'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaFuncao&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar a Função!');
			window.location = '../painelDeControle.php?corpo=buscaFuncao';
			</script>";
        }
    }

    public function removeFuncao($idFuncao) {
        $resultado = $this->funcao->removeFuncao($idFuncao);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Função excluída com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaFuncao';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Não foi possível excluir a Função!');
				window.location = '../painelDeControle.php?corpo=buscaFuncao';
				</script>";
        }
    }

}

?>