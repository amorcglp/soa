<?php
@include_once("model/quitacaoDividaClass.php");

@include_once("../model/quitacaoDividaClass.php");

class quitacaoDividaController {

    private $quitacaoDivida;

    public function __construct() {

        $this->quitacaoDivida = new QuitacaoDivida();
    }

    public function cadastroQuitacaoDivida() {

        $dataPagamento = substr($_POST["dataPagamento"],6,4)."-".substr($_POST["dataPagamento"],3,2)."-".substr($_POST["dataPagamento"],0,2);
        $this->quitacaoDivida->setFkIdDivida($_POST["fk_idDivida"]);
        $this->quitacaoDivida->setDataPagamento($dataPagamento);
        $this->quitacaoDivida->setValorPagamento($_POST["valorPagamento"]);
        $this->quitacaoDivida->setObservacao($_POST["observacao"]);
        $this->quitacaoDivida->setUsuario($_POST["usuario"]);

        if ($this->quitacaoDivida->cadastraQuitacaoDivida()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaQuitacaoDivida&salvo=1';
		  </script>";
        } else {
/*
            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa quitação de dívida!');
		  window.location = '../painelDeControle.php?corpo=cadastroQuitacaoDivida';
		  </script>";
*/
        }
    }

    public function listaQuitacaoDivida() {
        $resultado = $this->quitacaoDivida->listaQuitacaoDivida();
        return $resultado;
    }

    public function buscaQuitacaoDivida($id) {

        //echo "--------------------------------------------------------------------------->".$id;
        $resultado = $this->quitacaoDivida->buscarIdQuitacaoDivida($id);

        if ($resultado) {

            foreach ($resultado as $vetor) {

                $this->quitacaoDivida->setFkIdDivida($vetor['fk_idDivida']);
                $this->quitacaoDivida->setDataPagamento($vetor['dataPagamento']);
                $this->quitacaoDivida->setValorPagamento($vetor['valorPagamento']);
                $this->quitacaoDivida->setObservacao($vetor['observacao']);
            }
            
            return $this->quitacaoDivida;
        } else {
            return false;
        }
    }

    public function alteraQuitacaoDivida() {

        $dataPagamento = substr($_POST["dataPagamento"],6,4)."-".substr($_POST["dataPagamento"],3,2)."-".substr($_POST["dataPagamento"],0,2);
        
        $this->quitacaoDivida->setFkIdDivida($_POST["fk_idDivida"]);
        $this->quitacaoDivida->setDataPagamento($dataPagamento);
        $this->quitacaoDivida->setValorPagamento($_POST["valorPagamento"]);
        $this->quitacaoDivida->setObservacao($_POST["observacao"]);
        $this->quitacaoDivida->setUltimoAtualizar($_POST["usuario"]);

        if ($this->quitacaoDivida->alteraQuitacaoDivida($_POST['idQuitacaoDivida'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaQuitacaoDivida&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar esta quitação!');
			window.location = '../painelDeControle.php?corpo=alteraQuitacaoDivida.php&id=" . $_POST['idQuitacaoDivida'] . "';
			</script>";
        }
    }

    public function removeQuitacaoDivida($id) {
        $resultado = $this->quitacaoDivida->removeQuitacaoDivida($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('QuitacaoDivida excluido com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaQuitacaoDivida';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir este departamento!');
				window.location = '../painelDeControle.php?corpo=buscaQuitacaoDivida';
				</script>";
        }
    }

}
?>