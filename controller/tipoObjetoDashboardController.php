<?php
@include_once("model/tipoObjetoDashboardClass.php");

@include_once("../model/tipoObjetoDashboardClass.php");

class tipoObjetoDashboardController {

    private $tipoObjetoDashboard;

    public function __construct() {

        $this->tipoObjetoDashboard = new TipoObjetoDashboard();
    }

    public function cadastraTipoObjetoDashboard() {

        $this->tipoObjetoDashboard->setTipoObjetoDashboard($_POST["tipoObjetoDashboard"]);
        $this->tipoObjetoDashboard->setPorLinhaObjetoDashboard($_POST["porLinhaObjetoDashboard"]);
        $this->tipoObjetoDashboard->setDescricaoTipoObjetoDashboard($_POST["descricaoTipoObjetoDashboard"]);

        if ($this->tipoObjetoDashboard->cadastraTipoObjetoDashboard()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaTipoObjetoDashboard&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse Tipo de Objeto Dashboard!');
		  		window.location = '../painelDeControle.php?corpo=buscaTipoObjetoDashboard';
		  </script>";
        }
    }

 	public function criarComboBox($id = 0) {
        $resultado = $this->tipoObjetoDashboard->listaTipoObjetoDashboard(1);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idTipoObjetoDashboard"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idTipoObjetoDashboard'] . '" ' . $selecionado . '>' . $vetor["tipoObjetoDashboard"] . '</option>';
            }
        }
    }

    public function listaTipoObjetoDashboard() {
        
        $resultado = $this->tipoObjetoDashboard->listaTipoObjetoDashboard();
        return $resultado;
    }

    public function buscaTipoObjetoDashboard() {

        $resultado = $this->tipoObjetoDashboard->buscaIdTipoObjetoDashboard();

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->tipoObjetoDashboard->setIdTipoObjetoDashboard($vetor['idTipoObjetoDashboard']);
            	$this->tipoObjetoDashboard->setTipoObjetoDashboard($vetor['tipoObjetoDashboard']);
            	$this->tipoObjetoDashboard->setPorLinhaObjetoDashboard($vetor['porLinhaObjetoDashboard']);
            	$this->tipoObjetoDashboard->setDescricaoTipoObjetoDashboard($vetor['descricaoTipoObjetoDashboard']);
            }

            return $this->tipoObjetoDashboard;
        } else {
            return false;
        }
    }

    public function alteraTipoObjetoDashboard() {

        $this->tipoObjetoDashboard->setTipoObjetoDashboard($_POST["tipoObjetoDashboard"]);
        $this->tipoObjetoDashboard->setPorLinhaObjetoDashboard($_POST["porLinhaObjetoDashboard"]);
        $this->tipoObjetoDashboard->setDescricaoTipoObjetoDashboard($_POST["descricaoTipoObjetoDashboard"]);
        

        if ($this->tipoObjetoDashboard->alteraTipoObjetoDashboard($_POST['idTipoObjetoDashboard'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaTipoObjetoDashboard&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N�o foi poss�vel alterar este Tipo de Objeto Dashboard!');
			window.location = '../painelDeControle.php?corpo=buscaTipoObjetoDashboard';
			</script>";
        }
    }

	public function removeTipoObjetoDashboard($id) {
        $resultado = $this->tipoObjetoDashboard->removeTipoObjetoDashboard($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Tipo de Objeto Dashboard exclu�do com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaTipoObjetoDashboard';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir este Tipo de Objeto Dashboard!');
				window.location = '../painelDeControle.php?corpo=buscaTipoObjetoDashboard';
				</script>";
        }
    }

}
?>