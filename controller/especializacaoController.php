<?php

@include_once("../model/especializacaoClass.php");
@include_once("model/especializacaoClass.php");

class especializacaoController {

    private $especializacao;

    public function __construct() {

        $this->especializacao = new especializacao();
    }

    public function criarComboBox($id = 0) {
    	
    	$resultado = $this->especializacao->lista();
    	
        if ($resultado) {

            foreach ($resultado as $vetor) {
                if (($id != 0 && $id == $vetor["SeqEspeci"])) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                
	            echo '<option value="' . $vetor['SeqEspeci'] . '" ' . $selecionado . '>'. $vetor["DesEspeci"] . '</option>';
            }
        }
    }

}

?>