<?php

@include_once("model/agendaAtividadeClass.php");
@include_once("../model/agendaAtividadeClass.php");

@include_once("model/agendaAtividadeMembrosClass.php");
@include_once("../model/agendaAtividadeMembrosClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once ("../model/criaSessaoClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class agendaAtividadeController {

	private $agendaAtividade;
        private $relatorio_membros;
	private $sessao;

	public function __construct()
	{
		$this->sessao               = new criaSessao();
		$this->agendaAtividade    = new agendaAtividade();
		$this->usuario              = new Usuario();
		
	}

	public function cadastroAgendaAtividade() 
        {
        
            
            //Filtrar Request
            $arrFiltroRequest=array('fk_idOrganismoAfiliado',
                                'tipoAtividade',
                                'local',
                                'anoCompetencia',
                                'diasHorarios',
                                'recorrente',
                                'dataInicial',
                                'dataFinal',
                                'horaInicial',
                                'horaFinal',
                                'tag',
                                'fk_idPublico',
                                'observacoes'
                                );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }

            //Filtrar Input
            $clean = array();
            $dirty = array(

            0 => array($_POST["fk_idOrganismoAfiliado"],"inteiro"),
            1 => array($_POST["fk_idTipoAtividadeEstatuto"],"inteiro"),
            2 => array($_POST["tag"],"textoNumero"),
            3 => array($_POST["local"],"textoNumero"),
            4 => array($_POST["anoCompetencia"],"inteiro"),
            5 => array($_POST["diasHorarios"],"inteiroNumero"),    
            6 => array($_POST["observacoes"],"textoNumero"),
            7 => array($this->sessao->getValue("seqCadast"),"inteiro"),
            8 => array($_POST["recorrente"],"inteiro"),       
            9 => array($_POST["dataInicial"],"textoNumero"),    
            10 => array($_POST["dataFinal"],"textoNumero"),        
            11 => array($_POST["horaInicial"],"textoNumero"),        
            12 => array($_POST["horaFinal"],"textoNumero"),
            13 => array($_POST["tag"],"inteiro"),
            14 => array($_POST["complementoNomeAtividade"],""),    
            15 => array($_POST["fk_idPublico"],"inteiro")   
            );

            //if(!validaSegurancaInput($dirty))
            //{    
                $clean = $dirty;

                $ultimo_id="";
                    
		$this->agendaAtividade->setFk_idOrganismoAfiliado($clean[0][0]);
		$this->agendaAtividade->setTipoAtividade($clean[1][0]);
                
                $this->agendaAtividade->setLocal($clean[3][0]);
                $this->agendaAtividade->setAnoCompetencia($clean[4][0]);
		$this->agendaAtividade->setDiasHorarios($clean[5][0]);
                $this->agendaAtividade->setObservacoes($clean[6][0]);
		$this->agendaAtividade->setUsuario($clean[7][0]);
                
                $this->agendaAtividade->setRecorrente($clean[8][0]);
                if($clean[9][0]!=""&&$clean[10][0]!="")
                {
                    $this->agendaAtividade->setDataInicial(substr($clean[9][0],6,4)."-".substr($clean[9][0],3,2)."-".substr($clean[9][0],0,2));
                    $this->agendaAtividade->setDataFinal(substr($clean[10][0],6,4)."-".substr($clean[10][0],3,2)."-".substr($clean[10][0],0,2));
                }else{
                    $this->agendaAtividade->setDataInicial("0000-00-00");
                    $this->agendaAtividade->setDataFinal("0000-00-00");
                }
                $this->agendaAtividade->setHoraInicial($clean[11][0]);
                $this->agendaAtividade->setHoraFinal($clean[12][0]);
                $this->agendaAtividade->setTag($clean[13][0]);
                $this->agendaAtividade->setFk_idPublico($clean[15][0]);
                
                $this->agendaAtividade->setComplementoNomeAtividade($clean[14][0]);
                

		//Cadastrar agendaAtividade e pegar ultimo id inserido
		//if(!$this->agendaAtividade->verificaSeJaCadastrado($_POST['fk_idTipoAtividadeEstatuto'],$_POST['anoCompetencia'],$_POST['fk_idOrganismoAfiliado']))
		//{
			if($this->agendaAtividade->cadastroAgendaAtividade())
                        {        
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAgendaAtividade&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Nao foi possivel cadastrar essa atividade!');
	                    window.location = '../painelDeControle.php?corpo=buscaAgendaAtividade';
			  </script>";
			}
		/*
            }else{
                echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes para O.A. pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                        window.location = '../painelDeControle.php?corpo=buscaAgendaAtividade';
                                    </script>";
            }    */
        }
      
	public function listaAgendaAtividade() {
		$retorno = $this->agendaAtividade->listaAgendaAtividade();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaAgendaAtividade($idAgendaAtividade) {

		$resultado = $this->agendaAtividade->buscaAgendaAtividadePorId($idAgendaAtividade);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->agendaAtividade->setIdAgendaAtividade($vetor['idAgendaAtividade']);
				$this->agendaAtividade->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
				$this->agendaAtividade->setTipoAtividade($vetor['tipoAtividade']);
                                $this->agendaAtividade->setComplementoNomeAtividade($vetor['complementoNomeAtividade']);
                                $this->agendaAtividade->setLocal($vetor['local']);
                                $this->agendaAtividade->setAnoCompetencia($vetor['anoCompetencia']);
                                $this->agendaAtividade->setDiasHorarios($vetor['diasHorarios']);     
                                $this->agendaAtividade->setObservacoes($vetor['observacoes']);
                                
                                $this->agendaAtividade->setRecorrente($vetor['recorrente']);
                                $this->agendaAtividade->setDataInicial(substr($vetor['dataInicial'],8,2)."/".substr($vetor['dataInicial'],5,2)."/".substr($vetor['dataInicial'],0,4));
                                $this->agendaAtividade->setDataFinal(substr($vetor['dataFinal'],8,2)."/".substr($vetor['dataFinal'],5,2)."/".substr($vetor['dataFinal'],0,4));
                                $this->agendaAtividade->setHoraInicial($vetor['horaInicial']);
                                $this->agendaAtividade->setHoraFinal($vetor['horaFinal']);
                                $this->agendaAtividade->setTag($vetor['tag']);
                                $this->agendaAtividade->setFk_idPublico($vetor['fk_idPublico']);
			}
			return $this->agendaAtividade;
		} else {
			return false;
		}
	}
	

	public function atualizaAgendaAtividade($id) {
            
            //Filtrar Request
            $arrFiltroRequest=array(
                                'tipoAtividade',
                                'local',
                                'anoCompetencia',
                                'diasHorarios',
                                'recorrente',
                                'dataInicial',
                                'dataFinal',
                                'horaInicial',
                                'horaFinal',
                                'tag',
                                'fk_idPublico',
                                'observacoes'
                                );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }

            //Filtrar Input
            $clean = array();
            $dirty = array(

            0 => array($_POST["fk_idTipoAtividadeEstatuto"],"inteiro"),
            1 => array($_POST["tag"],"textoNumero"),
            2 => array($_POST["local"],"textoNumero"),
            3 => array($_POST["anoCompetencia"],"inteiro"),
            4 => array($_POST["diasHorarios"],"inteiroNumero"),    
            5 => array($_POST["observacoes"],"textoNumero"),
            6 => array($this->sessao->getValue("seqCadast"),"inteiro"),
            7 => array($_POST["recorrente"],"inteiro"),       
            8 => array($_POST["dataInicial"],"textoNumero"),    
            9 => array($_POST["dataFinal"],"textoNumero"),        
            10 => array($_POST["horaInicial"],"textoNumero"),        
            11 => array($_POST["horaFinal"],"textoNumero"),
            12 => array($_POST["tag"],"inteiro"),
            13 => array($_POST["complementoNomeAtividade"]),    
            14 => array($_POST["fk_idPublico"]),
            15 => array($this->sessao->getValue("seqCadast"))    
            );

            //if(!validaSegurancaInput($dirty))
            //{    
                $clean = $dirty;

		//$this->agendaAtividade->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->agendaAtividade->setTipoAtividade($clean[0][0]);
                
                $this->agendaAtividade->setLocal($clean[2][0]);
                $this->agendaAtividade->setAnoCompetencia($clean[3][0]);
                $this->agendaAtividade->setDiasHorarios($clean[4][0]);
                $this->agendaAtividade->setObservacoes($clean[5][0]);
		$this->agendaAtividade->setAlteracaoUsuario($clean[6][0]);
                
                $this->agendaAtividade->setRecorrente($clean[7][0]);
                if($clean[8][0]!=""&&$clean[9][0]!="")
                {       
                    $this->agendaAtividade->setDataInicial(substr($clean[8][0],6,4)."-".substr($clean[8][0],3,2)."-".substr($clean[8][0],0,2));
                    $this->agendaAtividade->setDataFinal(substr($clean[9][0],6,4)."-".substr($clean[9][0],3,2)."-".substr($clean[9][0],0,2));
                }else{
                    $this->agendaAtividade->setDataInicial("0000-00-00");
                    $this->agendaAtividade->setDataFinal("0000-00-00");
                }
                $this->agendaAtividade->setHoraInicial($clean[10][0]);
                $this->agendaAtividade->setHoraFinal($clean[11][0]);
                $this->agendaAtividade->setTag($clean[12][0]);
                $this->agendaAtividade->setFk_idPublico($clean[14][0]);
                
                $this->agendaAtividade->setComplementoNomeAtividade($clean[13][0]);
                
                $this->agendaAtividade->setUltimoAtualizar($clean[15][0]);
                

		if ($this->agendaAtividade->atualizaAgendaAtividade($id)) {

			echo
                        "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaAgendaAtividade&salvo=1';
			</script>";
		} else {
			echo
                        "<script type='text/javascript'>
			alert ('Nao foi possivel alterar o relatório!');
			window.location = '../painelDeControle.php?corpo=buscaAgendaAtividade';
			</script>";
		}
            /*    
            }else{
                
                echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes para O.A. pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                        window.location = '../painelDeControle.php?corpo=buscaAgendaAtividade';
                                    </script>";
            } */   
	}

}

?>