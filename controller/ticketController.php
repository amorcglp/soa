<?php
@include_once("model/ticketClass.php");
@include_once("model/usuarioClass.php");
@include_once("lib/functions.php");

@include_once("../model/ticketClass.php");
@include_once("../model/usuarioClass.php");
@include_once("../lib/functions.php");

class ticketController {

    private $ticket;

    public function __construct() {

        $this->ticket = new Ticket();
        $this->usuario = new Usuario();
    }

    public function cadastroTicket() {
        $selectedOption = "";
        foreach ($_POST["tagTicket"] as $vetorSelected)
            $selectedOption = $selectedOption . " " . $vetorSelected;

        $retornoFuncionalidade = $this->ticket->buscaFuncionalidade($_POST["fk_idFuncionalidade"]);

        if(!$retornoFuncionalidade)
            return false;
        foreach($retornoFuncionalidade as $vetorFuncionalidade){
            $nomeFuncionalidade = $vetorFuncionalidade['nomeFuncionalidade'];
        }

        $this->ticket->setTituloTicket($nomeFuncionalidade." [ID: ".$_POST["identificadorTicket"]."]"); //  - [DATA: ".date('d-m-Y')." às ".date('H:i')."]
        $this->ticket->setDescricaoTicket($_POST["descricaoTicket"]);
        $this->ticket->setCriadoPorTicket($_POST["criadoPorTicket"]);
        $this->ticket->setCriadoPorDepartTicket($_POST["criadoPorDepartTicket"]);
        $this->ticket->setPrioridadeTicket($_POST["prioridadeTicket"]);
        $this->ticket->setTagTicket($selectedOption);
        $this->ticket->setIdentificadorTicket($_POST["identificadorTicket"]);
        $this->ticket->setFkIdFuncionalidade($_POST["fk_idFuncionalidade"]);
        $this->ticket->setFkIdTicketFinalidade($_POST["fk_idTicketFinalidade"]);
        $this->ticket->setFkIdOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);

        echo "<pre>"; print_r($_POST); echo "</pre>";

        if ($this->ticket->cadastroTicket()) {

            /* Cadastro de usuários da GLP no Ticket */
            $id = $this->ticket->selecionaUltimoId();
            echo "id: " . $id . "<br>";

            $resultado = $this->usuario->listaUsuarioPeloDepartamento(2);
            if($resultado){
                foreach ($resultado as $vetor){
                    //echo "<pre>"; print_r($vetor); echo "</pre>";
                    $this->ticket->cadastroEnvolvidos($id,$vetor['seqCadast'],2,1);
                }
            }
            /* Cadastro do usuário que abriu ticket */
            $this->ticket->cadastroEnvolvidos($id,$_POST["criadoPorTicket"],1,1);
            /*
                        echo "<script type='text/javascript'>
                                alert ('Ticket cadastrada com sucesso!');
                                window.location = '../painelDeControle.php?corpo=buscaTicket';
                            </script>";
            */
        } else {

            echo "<script type='text/javascript'>
                    alert ('Nao foi possivel cadastrar esse ticket!');
                    window.location = '../painelDeControle.php?corpo=cadastroTicket';
                </script>";
        }

    }

    public function cadastroTicketAltomatico($fk_idFuncionalidade,$descricaoTicket,$criadoPorTicket,$fk_idOrganismoAfiliado,$identificadorTicket,$fk_idTicketFinalidade) {

        $retornoFuncionalidade = $this->ticket->buscaFuncionalidade($fk_idFuncionalidade);
        if(!$retornoFuncionalidade)
            return false;
        foreach($retornoFuncionalidade as $vetorFuncionalidade){
            $nomeFuncionalidade = $vetorFuncionalidade['nomeFuncionalidade'];
        }

        $this->ticket->setTituloTicket($nomeFuncionalidade." [ID: ".$identificadorTicket."]"); //  - [DATA: ".date('d-m-Y')." às ".date('H:i')."]
        $this->ticket->setDescricaoTicket($descricaoTicket);
        $this->ticket->setCriadoPorTicket($criadoPorTicket);
        $this->ticket->setCriadoPorDepartTicket(1);
        $this->ticket->setPrioridadeTicket(1);
        $this->ticket->setTagTicket($nomeFuncionalidade);
        $this->ticket->setIdentificadorTicket($identificadorTicket);
        $this->ticket->setFkIdFuncionalidade($fk_idFuncionalidade);
        $this->ticket->setFkIdTicketFinalidade($fk_idTicketFinalidade);
        $this->ticket->setFkIdOrganismoAfiliado($fk_idOrganismoAfiliado);

        if ($this->ticket->cadastroTicket()) {
            $id = $this->ticket->selecionaUltimoId();
            $resultado = $this->usuario->listaUsuarioPeloDepartamento(2);
            if($resultado){
                foreach ($resultado as $vetor){
                    //echo "=>Id:".$id;
                    //echo "=>SeqCadast:".$vetor['seqCadast'];
                    $this->ticket->cadastroEnvolvidos($id,$vetor['seqCadast'],2,0);
                }
            }
            $this->ticket->cadastroEnvolvidos($id,$criadoPorTicket,1,1);
            return $this->ticket->selecionaUltimoIdInserido();
        } else { return false;}
    }

    public function listaFinalidade() {

        $resultado = $this->ticket->listaFinalidade();

        return $resultado;
    }

    public function listaTicket($idOrganismo=null,$pagina=null,$statusSearch=null,$organismoSearch=null,$funcionalidadeSearch=null,$atribuicaoSearch=null,$finalidade=null) {

        $resultado = $this->ticket->listaTicket($idOrganismo,$pagina,$statusSearch,$organismoSearch,$funcionalidadeSearch,$atribuicaoSearch,$finalidade);

        return $resultado;
    }

    public function retornaQuantidadeTicket($idOrganismo=null,$statusSearch=null,$organismoSearch=null,$funcionalidadeSearch=null,$atribuicaoSearch=null,$finalidade=null) {

        $resultado = $this->ticket->retornaQuantidadeTicket($idOrganismo,$statusSearch,$organismoSearch,$funcionalidadeSearch,$atribuicaoSearch,$finalidade);

        return $resultado;
    }

    public function buscaTicket($idTicket) {

        $resultado = $this->ticket->buscaTicket($idTicket);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                $selectedOption = [];
                $selectedOption = explode(" ", $vetor["tagTicket"]);

                $this->ticket->setTituloTicket($vetor["tituloTicket"]);
                $this->ticket->setDescricaoTicket($vetor["descricaoTicket"]);
                $this->ticket->setCriadoPorTicket($vetor["criadoPorTicket"]);
                $this->ticket->setCriadoPorDepartTicket($vetor["criadoPorDepartTicket"]);
                $this->ticket->setAtribuidoTicket($vetor["atribuidoTicket"]);
                $this->ticket->setAtribuidoDepartTicket($vetor["atribuidoDepartTicket"]);
                $this->ticket->setStatusTicket($vetor["statusTicket"]);
                $this->ticket->setDataCriacaoTicket(substr($vetor["dataCriacaoTicket"],8,2)."/".substr($vetor["dataCriacaoTicket"],5,2)."/".substr($vetor["dataCriacaoTicket"],0,4) . " às " . substr($vetor["dataCriacaoTicket"],11,5));
                $this->ticket->setDataAtualizacaoTicket(substr($vetor["dataAtualizacaoTicket"],8,2)."/".substr($vetor["dataAtualizacaoTicket"],5,2)."/".substr($vetor["dataAtualizacaoTicket"],0,4) . " às " . substr($vetor["dataAtualizacaoTicket"],11,5));
                $this->ticket->setPrioridadeTicket($vetor["prioridadeTicket"]);
                $this->ticket->setTagTicket($vetor["tagTicket"]);
                $this->ticket->setIdentificadorTicket($vetor["identificadorTicket"]);
                $this->ticket->setFkIdFuncionalidade($vetor["fk_idFuncionalidade"]);
                $this->ticket->setFkIdOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);
                $this->ticket->setFkIdTicketFinalidade($vetor["fk_idTicketFinalidade"]);
            }

            return $this->ticket;
        } else {
            return false;
        }
    }

    public function buscaFuncionalidade($idFuncionalidade=null) {

        $resultado = $this->ticket->buscaFuncionalidade($idFuncionalidade);

        if ($resultado) {
            foreach ($resultado as $vetor) {
                $this->ticket->setNomeFuncionalidade($vetor["nomeFuncionalidade"]);
                $this->ticket->setCorpoFuncionalidade($vetor["corpoFuncionalidade"]);
                $this->ticket->setReferenciaIdFuncionalidade($vetor["referenciaIdFuncionalidade"]);
            }
            return $this->ticket;
        } else {
            return false;
        }
    }

    public function criarComboBoxFuncionalidade($id = 0) {

        $resultado = $this->ticket->listaFuncionalidade();
        if ($resultado) {
            foreach ($resultado as $vetor) {
                $selecionado = ($id != 0 && $id == $vetor["idFuncionalidade"]) ? 'selected = selected' : '';

                echo '<option value="' . $vetor['idFuncionalidade'] . '" ' . $selecionado . '>' . $vetor["nomeFuncionalidade"] . '</option>';
            }
        }
    }

    public function criarComboBoxUsuarioPeloDepartamento($id = 0) {
        $resultado = $this->usuario->listaUsuarioPeloDepartamento(2);
        if ($resultado) {
            foreach ($resultado as $vetor) {
                $selecionado = ($id != 0 && $id == $vetor["seqCadast"]) ? 'selected = selected' : '';
                echo '<option value="' . $vetor['seqCadast'] . '" ' . $selecionado . '>' . retornaNomeFormatado($vetor["nomeUsuario"],3) . '</option>';
            }
        }
    }

    public function criarComboBoxUsuarioAtribuido($id = 0) {
        $resultado = $this->usuario->listaUsuarioPeloDepartamento(2);
        if ($resultado) {
            foreach ($resultado as $vetor) {
                $selecionado = ($id != 0 && $id == $vetor["seqCadast"]) ? 'selected = selected' : '';
                echo '<option value="' . $vetor['seqCadast'] . '" ' . $selecionado . '>' . retornaNomeFormatado($vetor["nomeUsuario"],3) . '</option>';
            }
        }
    }

    public function criarComboBoxFinalidade($id = 0) {

        $resultado = $this->ticket->listaFinalidade();
        if ($resultado) {
            foreach ($resultado as $vetor) {
                $selecionado = ($id != 0 && $id == $vetor["idTicketFinalidade"]) ? 'selected = selected' : '';

                echo '<option value="' . $vetor['idTicketFinalidade'] . '" ' . $selecionado . '>' . $vetor["nomeTicketFinalidade"] . '</option>';
            }
        }
    }

    public function listaEnvolvidos($idTicket,$fk_idDepartamento) {

        $resultado = $this->ticket->listaEnvolvidos($idTicket,$fk_idDepartamento);

        return $resultado;
    }

}
?>