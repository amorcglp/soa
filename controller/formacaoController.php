<?php

@include_once("../model/formacaoClass.php");
@include_once("model/formacaoClass.php");

class formacaoController {

    private $formacao;

    public function __construct() {

        $this->formacao = new formacao();
    }

    public function criarComboBox($id = 0) {
    	
    	$resultado = $this->formacao->lista();
    	
        if ($resultado) {

            foreach ($resultado as $vetor) {
                if (($id != 0 && $id == $vetor["SeqFormac"])) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                
	            echo '<option value="' . $vetor['SeqFormac'] . '" ' . $selecionado . '>'. $vetor["DesFormac"] . '</option>';
            }
        }
    }

}

?>