<?php

@include_once("../model/atividadeAgendaTipoClass.php");
@include_once("model/atividadeAgendaTipoClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");


class atividadeAgendaTipoController {

    public $atividadeAgendaTipo;

    public function __construct() {

        $this->atividadeAgendaTipo = new AtividadeAgendaTipo();
    }

    public function cadastroTipoAtividadeAgenda() 
    {
        //Filtrar Request
        $arrFiltroRequest=array('nome',
                            'descricao',
                            'classiOaTipoAtividadeAgenda',
                            'statusTipoAtividadeAgenda',
                            'usuario',
                            'fk_idAtividadeAgendaTipo');
        if(validaSegurancaRequest($arrFiltroRequest))
        {
            enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
            echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
            exit();
        }
	
        //Filtrar Input
        $clean = array();
        $dirty = array(
        
        0 => array($_POST["nome"],"texto"),
        1 => array(cleanHtml($_POST["descricao"]),"html"),
        2 => array($_POST["classiOaTipoAtividadeAgenda"],"inteiro"),
        3 => array($_POST["statusTipoAtividadeAgenda"],"inteiro"),
        4 => array($_POST["usuario"],"inteiro")
            
        );
        
        if(!validaSegurancaInput($dirty))
        {    
            $clean = $dirty;
            $this->atividadeAgendaTipo->setNome($clean[0][0]);
            $this->atividadeAgendaTipo->setDescricao($clean[1][0]);
            $this->atividadeAgendaTipo->setClassiOaTipoAtividadeAgenda($clean[2][0]);
            $this->atividadeAgendaTipo->setStatusTipoAtividadeAgenda($clean[3][0]);
            $this->atividadeAgendaTipo->setUsuario($clean[4][0]);

            if ($this->atividadeAgendaTipo->cadastroTipoAtividadeAgenda()) {

                echo "<script type='text/javascript'>
                        alert('Tipo de Atividade para O.A. cadastrado com sucesso!');
                                    window.location = '../painelDeControle.php?corpo=buscaAtividadeAgendaTipo';
                                    </script>";

            } else {
                //echo "<pre>";print_r($clean);
                echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade para O.A.!');
                        window.location = '../painelDeControle.php?corpo=cadastroAtividadeAgendaTipo';
                                    </script>";

            }
        }else{
            echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade para O.A. pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                        window.location = '../painelDeControle.php?corpo=buscaAtividadeAgendaTipo';
                                    </script>";
        }
    }
    
    public function alteraTipoAtividadeAgenda() 
    {
        //Filtrar Request
        //Filtrar Request
        $arrFiltroRequest=array('nome',
                            'descricao',
                            'classiOaTipoAtividadeAgenda',
                            'statusTipoAtividadeAgenda',
                            'usuario',
                            'fk_idAtividadeAgendaTipo');
        if(validaSegurancaRequest($arrFiltroRequest))
        {
            enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
            echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
            exit();
        }
	
        //Filtrar Input
        $clean = array();
        $dirty = array(
        
        0 => array($_POST["nome"],"texto"),
        1 => array($_POST["descricao"],"html"),
        2 => array($_POST["classiOaTipoAtividadeAgenda"],"inteiro"),
        3 => array($_POST["statusTipoAtividadeAgenda"],"inteiro"),
        4 => array($_POST["usuario"],"inteiro"),
        5 => array($_POST["fk_idAtividadeAgendaTipo"],"inteiro")    
            
        );
        
        if(!validaSegurancaInput($dirty))
        {    

            $clean = $dirty;

            //Se estão todas as variáveis setadas
           
            $this->atividadeAgendaTipo->setNome($clean[0][0]);
            $this->atividadeAgendaTipo->setDescricao($clean[1][0]);
            $this->atividadeAgendaTipo->setClassiOaTipoAtividadeAgenda($clean[2][0]);
            $this->atividadeAgendaTipo->setStatusTipoAtividadeAgenda($clean[3][0]);
            $this->atividadeAgendaTipo->setUsuarioAtualizacao($clean[4][0]);

            if ($this->atividadeAgendaTipo->alteraTipoAtividadeAgenda($clean[5][0])) {

                echo
                "<script type='text/javascript'>
                            alert ('Tipo de Atividade alterada com sucesso!');
                            window.location = '../painelDeControle.php?corpo=buscaAtividadeAgendaTipo';
                            </script>";

            } else {

                echo
                "<script type='text/javascript'>
                            alert ('N\u00e3o foi poss\u00edvel alterar o Tipo de Atividade!');
                            window.location = '../painelDeControle.php?corpo=alteraAtividadeAgendaTipo&idTipoAtividadeAgenda=" . $clean[5] . "';
                            </script>";

            }
          
        }else{
            echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade para O.A. pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                        window.location = '../painelDeControle.php?corpo=buscaAtividadeAgendaTipo';
                                    </script>";
        }         
    }

    public function listaAtividadeAgendaTipo() {

        if (!isset($_POST['idTipoAtividadeAgenda'])) {
            $resultado = $this->atividadeAgendaTipo->listaAtividadeAgendaTipo();
        } 
        return $resultado;
    }

    public function buscaAtividadeAgendaTipo($idAlteraTipoAtividadeAgenda) {

        $resultado = $this->atividadeAgendaTipo->buscaIdTipoAtividadeAgenda($idAlteraTipoAtividadeAgenda);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                //echo "<pre>";print_r($vetor);
                $this->atividadeAgendaTipo->setNome($vetor["nome"]);
                $this->atividadeAgendaTipo->setDescricao($vetor["descricao"]);
                $this->atividadeAgendaTipo->setClassiOaTipoAtividadeAgenda($vetor['classiOaTipoAtividadeAgenda']);
                $this->atividadeAgendaTipo->setStatusTipoAtividadeAgenda($vetor["statusTipoAtividadeAgenda"]);
            }
            return $this->atividadeAgendaTipo;
        } else {
            return false;
        }
    }

    /*
     * Verificação de segurança no output da combobox
     */
    public function criarComboBox($id = 0, $classificacaoOa) {
        $resultado = $this->atividadeAgendaTipo->listaAtividadeAgendaTipoPelaClassificacaoOa($classificacaoOa);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idTipoAtividadeAgenda"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idTipoAtividadeAgenda'] . '" ' . $selecionado . '>' . validaSegurancaOutput($vetor["nomeTipoAtividadeAgenda"]) .'</option>';
            }
        }
    }

    public function listaIdAtividadeTipo() {
        $retorno = $this->atividadeTipo->listaAtividadeTipo();
        $listaatividadeTipo = "";

        foreach ($retorno as $dados) {
            $listaatividadeTipo[] = $dados['idAtividadeTipo'];
        }

        if ($listaatividadeTipo) {
            return $listaatividadeTipo;
        } else {
            return false;
        }
    }

}

?>