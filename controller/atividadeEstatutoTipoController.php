<?php

@include_once("../model/atividadeEstatutoTipoClass.php");
@include_once("model/atividadeEstatutoTipoClass.php");

class atividadeEstatutoTipoController {

    private $atividadeEstatutoTipo;

    public function __construct() {

        $this->atividadeEstatutoTipo = new AtividadeEstatutoTipo();
    }

    public function cadastroTipoAtividadeEstatuto() {
        $portal = 0;
        $estatuto = 0;
        $janeiroAtividade = 0;
        $fevereiroAtividade = 0;
        $marcoAtividade = 0;
        $abrilAtividade = 0;
        $maioAtividade = 0;
        $junhoAtividade = 0;
        $julhoAtividade = 0;
        $agostoAtividade = 0;
        $setembroAtividade = 0;
        $outubroAtividade = 0;
        $novembroAtividade = 0;
        $dezembroAtividade = 0;
        if (isset($_POST["disponibilidade"])) {
            $portal = in_array('portal', $_POST["disponibilidade"]) ? 1 : 0;
            $estatuto = in_array('estatuto', $_POST["disponibilidade"]) ? 1 : 0;
        }
        if (isset($_POST["frequencia"])) {
            $janeiroAtividade = in_array('janeiroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $fevereiroAtividade = in_array('fevereiroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $marcoAtividade = in_array('marcoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $abrilAtividade = in_array('abrilAtividade', $_POST["frequencia"]) ? 1 : 0;
            $maioAtividade = in_array('maioAtividade', $_POST["frequencia"]) ? 1 : 0;
            $junhoAtividade = in_array('junhoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $julhoAtividade = in_array('julhoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $agostoAtividade = in_array('agostoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $setembroAtividade = in_array('setembroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $outubroAtividade = in_array('outubroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $novembroAtividade = in_array('novembroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $dezembroAtividade = in_array('dezembroAtividade', $_POST["frequencia"]) ? 1 : 0;
        }

        $this->atividadeEstatutoTipo->setNomeTipoAtividadeEstatuto($_POST["nomeTipoAtividadeEstatuto"]);
        $this->atividadeEstatutoTipo->setDescricaoTipoAtividadeEstatuto($_POST["descricaoTipoAtividadeEstatuto"]);
        $this->atividadeEstatutoTipo->setClassiOaTipoAtividadeEstatuto($_POST['classiOaTipoAtividadeEstatuto']);
        $this->atividadeEstatutoTipo->setQntTipoAtividadeEstatuto($_POST["qntTipoAtividadeEstatuto"]);
        $this->atividadeEstatutoTipo->setPortalTipoAtividadeEstatuto($portal);
        $this->atividadeEstatutoTipo->setEstatutoTipoAtividadeEstatuto($estatuto);
        $this->atividadeEstatutoTipo->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);

        $this->atividadeEstatutoTipo->setJaneiroTipoAtividadeEstatuto($janeiroAtividade);
        $this->atividadeEstatutoTipo->setFevereiroTipoAtividadeEstatuto($fevereiroAtividade);
        $this->atividadeEstatutoTipo->setMarcoTipoAtividadeEstatuto($marcoAtividade);
        $this->atividadeEstatutoTipo->setAbrilTipoAtividadeEstatuto($abrilAtividade);
        $this->atividadeEstatutoTipo->setMaioTipoAtividadeEstatuto($maioAtividade);
        $this->atividadeEstatutoTipo->setJunhoTipoAtividadeEstatuto($junhoAtividade);
        $this->atividadeEstatutoTipo->setJulhoTipoAtividadeEstatuto($julhoAtividade);
        $this->atividadeEstatutoTipo->setAgostoTipoAtividadeEstatuto($agostoAtividade);
        $this->atividadeEstatutoTipo->setSetembroTipoAtividadeEstatuto($setembroAtividade);
        $this->atividadeEstatutoTipo->setOutubroTipoAtividadeEstatuto($outubroAtividade);
        $this->atividadeEstatutoTipo->setNovembroTipoAtividadeEstatuto($novembroAtividade);
        $this->atividadeEstatutoTipo->setDezembroTipoAtividadeEstatuto($dezembroAtividade);

        //echo "<pre>"; print_r($_POST); echo "</pre>";

        if ($this->atividadeEstatutoTipo->cadastroTipoAtividadeEstatuto()) {

            echo "<script type='text/javascript'>
                    alert('Atividade para O.A. cadastrado com sucesso!');
		    		window.location = '../painelDeControle.php?corpo=buscaAtividadeEstatutoTipo';
		  		</script>";

        } else {

            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade para O.A.!');
                    window.location = '../painelDeControle.php?corpo=cadastroatividadeEstatutoTipo';
		  		</script>";

        }
    }

	public function alteraTipoAtividadeEstatuto() {

        $portal = 0;
        $estatuto = 0;
        $janeiroAtividade = 0;
        $fevereiroAtividade = 0;
        $marcoAtividade = 0;
        $abrilAtividade = 0;
        $maioAtividade = 0;
        $junhoAtividade = 0;
        $julhoAtividade = 0;
        $agostoAtividade = 0;
        $setembroAtividade = 0;
        $outubroAtividade = 0;
        $novembroAtividade = 0;
        $dezembroAtividade = 0;
        if (isset($_POST["disponibilidade"])) {
            $portal = in_array('portal', $_POST["disponibilidade"]) ? 1 : 0;
            $estatuto = in_array('estatuto', $_POST["disponibilidade"]) ? 1 : 0;
        }
        if (isset($_POST["frequencia"])) {
            $janeiroAtividade = in_array('janeiroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $fevereiroAtividade = in_array('fevereiroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $marcoAtividade = in_array('marcoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $abrilAtividade = in_array('abrilAtividade', $_POST["frequencia"]) ? 1 : 0;
            $maioAtividade = in_array('maioAtividade', $_POST["frequencia"]) ? 1 : 0;
            $junhoAtividade = in_array('junhoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $julhoAtividade = in_array('julhoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $agostoAtividade = in_array('agostoAtividade', $_POST["frequencia"]) ? 1 : 0;
            $setembroAtividade = in_array('setembroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $outubroAtividade = in_array('outubroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $novembroAtividade = in_array('novembroAtividade', $_POST["frequencia"]) ? 1 : 0;
            $dezembroAtividade = in_array('dezembroAtividade', $_POST["frequencia"]) ? 1 : 0;
        }

        $this->atividadeEstatutoTipo->setNomeTipoAtividadeEstatuto($_POST["nomeTipoAtividadeEstatuto"]);
        $this->atividadeEstatutoTipo->setDescricaoTipoAtividadeEstatuto($_POST["descricaoTipoAtividadeEstatuto"]);
        $this->atividadeEstatutoTipo->setClassiOaTipoAtividadeEstatuto($_POST['classiOaTipoAtividadeEstatuto']);
        $this->atividadeEstatutoTipo->setQntTipoAtividadeEstatuto($_POST["qntTipoAtividadeEstatuto"]);
        $this->atividadeEstatutoTipo->setPortalTipoAtividadeEstatuto($portal);
        $this->atividadeEstatutoTipo->setEstatutoTipoAtividadeEstatuto($estatuto);
        $this->atividadeEstatutoTipo->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);

        $this->atividadeEstatutoTipo->setJaneiroTipoAtividadeEstatuto($janeiroAtividade);
        $this->atividadeEstatutoTipo->setFevereiroTipoAtividadeEstatuto($fevereiroAtividade);
        $this->atividadeEstatutoTipo->setMarcoTipoAtividadeEstatuto($marcoAtividade);
        $this->atividadeEstatutoTipo->setAbrilTipoAtividadeEstatuto($abrilAtividade);
        $this->atividadeEstatutoTipo->setMaioTipoAtividadeEstatuto($maioAtividade);
        $this->atividadeEstatutoTipo->setJunhoTipoAtividadeEstatuto($junhoAtividade);
        $this->atividadeEstatutoTipo->setJulhoTipoAtividadeEstatuto($julhoAtividade);
        $this->atividadeEstatutoTipo->setAgostoTipoAtividadeEstatuto($agostoAtividade);
        $this->atividadeEstatutoTipo->setSetembroTipoAtividadeEstatuto($setembroAtividade);
        $this->atividadeEstatutoTipo->setOutubroTipoAtividadeEstatuto($outubroAtividade);
        $this->atividadeEstatutoTipo->setNovembroTipoAtividadeEstatuto($novembroAtividade);
        $this->atividadeEstatutoTipo->setDezembroTipoAtividadeEstatuto($dezembroAtividade);

        //echo "<pre>"; print_r($_POST); echo "</pre>";

        if ($this->atividadeEstatutoTipo->alteraTipoAtividadeEstatuto($_POST['idTipoAtividadeEstatuto'])) {

            echo
            "<script type='text/javascript'>
			alert ('Tipo de Atividade alterada com sucesso!');
			window.location = '../painelDeControle.php?corpo=buscaAtividadeEstatutoTipo';
			</script>";

        } else {

            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar o Tipo de Atividade!');
			window.location = '../painelDeControle.php?corpo=alteraAtividadeEstatutoTipo&idTipoAtividadeEstatuto=" . $_POST['idTipoAtividadeEstatuto'] . "';
			</script>";

        }
    }

    public function listaAtividadeEstatutoTipo($classiOaTipoAtividadeEstatuto=null,$estatutoTipoAtividadeEstatuto=null,$statusTipoAtividadeEstatuto=null) {

        if (!isset($_POST['idTipoAtividadeEstatuto'])) {
            $resultado = $this->atividadeEstatutoTipo->listaAtividadeEstatutoTipo($classiOaTipoAtividadeEstatuto,
                                                                                  $estatutoTipoAtividadeEstatuto,
                                                                                  $statusTipoAtividadeEstatuto);
        } else {
            /*
            $this->atividadeTipo->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeTipo->buscaNomeOrganismo();
            */
        }
        return $resultado;
    }

    public function buscaAtividadeEstatutoTipo($idAlteraTipoAtividadeEstatuto) {

        $resultado = $this->atividadeEstatutoTipo->buscaIdTipoAtividadeEstatuto($idAlteraTipoAtividadeEstatuto);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                //echo "<pre>";print_r($vetor);
                $this->atividadeEstatutoTipo->setIdTipoAtividadeEstatuto($vetor["idTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setNomeTipoAtividadeEstatuto($vetor["nomeTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setDescricaoTipoAtividadeEstatuto($vetor["descricaoTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setClassiOaTipoAtividadeEstatuto($vetor['classiOaTipoAtividadeEstatuto']);
                $this->atividadeEstatutoTipo->setPortalTipoAtividadeEstatuto($vetor["portalTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setEstatutoTipoAtividadeEstatuto($vetor["estatutoTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setQntTipoAtividadeEstatuto($vetor["qntTipoAtividadeEstatuto"]);

                $this->atividadeEstatutoTipo->setJaneiroTipoAtividadeEstatuto($vetor["janeiroTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setFevereiroTipoAtividadeEstatuto($vetor["fevereiroTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setMarcoTipoAtividadeEstatuto($vetor["marcoTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setAbrilTipoAtividadeEstatuto($vetor["abrilTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setMaioTipoAtividadeEstatuto($vetor["maioTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setJunhoTipoAtividadeEstatuto($vetor["junhoTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setJulhoTipoAtividadeEstatuto($vetor["julhoTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setAgostoTipoAtividadeEstatuto($vetor["agostoTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setSetembroTipoAtividadeEstatuto($vetor["setembroTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setOutubroTipoAtividadeEstatuto($vetor["outubroTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setNovembroTipoAtividadeEstatuto($vetor["novembroTipoAtividadeEstatuto"]);
                $this->atividadeEstatutoTipo->setDezembroTipoAtividadeEstatuto($vetor["dezembroTipoAtividadeEstatuto"]);
            }
            return $this->atividadeEstatutoTipo;
        } else {
            return false;
        }
    }

    public function criarComboBox($id = 0, $classificacaoOa,$portal=null,$estatuto=null,$status=null) {
        $resultado = $this->atividadeEstatutoTipo->listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacaoOa,$portal,$estatuto,$status);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idTipoAtividadeEstatuto"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idTipoAtividadeEstatuto'] . '" ' . $selecionado . '> (Cod. ' . $vetor['idTipoAtividadeEstatuto'] . ') - ' . $vetor["nomeTipoAtividadeEstatuto"] .'</option>';
            }
        }
    }

    public function listaIdAtividadeTipo() {
        $retorno = $this->atividadeTipo->listaAtividadeTipo();
        $listaatividadeTipo = "";

        foreach ($retorno as $dados) {
            $listaatividadeTipo[] = $dados['idAtividadeTipo'];
        }

        if ($listaatividadeTipo) {
            return $listaatividadeTipo;
        } else {
            return false;
        }
    }

}

?>
