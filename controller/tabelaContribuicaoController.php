<?php
@include_once("model/tabelaContribuicaoClass.php");

@include_once("../model/tabelaContribuicaoClass.php");

class tabelaContribuicaoController {

    private $tabelaContribuicao;

    public function __construct() {

        $this->tabelaContribuicao = new tabelaContribuicao();
    }
    
    public function cadastraTabelaContribuicao() {

        $this->tabelaContribuicao->setTipo($_POST["tipo"]);
        $this->tabelaContribuicao->setModalidadeMensalIndividual($_POST["modalidadeMensalIndividual"]);
        $this->tabelaContribuicao->setModalidadeTrimestralIndividualReal($_POST["modalidadeTrimestralIndividualReal"]);
        $this->tabelaContribuicao->setModalidadeTrimestralIndividualDolar($_POST["modalidadeTrimestralIndividualDolar"]);
        $this->tabelaContribuicao->setModalidadeTrimestralIndividualEuro($_POST["modalidadeTrimestralIndividualEuro"]);
        $this->tabelaContribuicao->setModalidadeAnualIndividualReal($_POST["modalidadeAnualIndividualReal"]);
        $this->tabelaContribuicao->setModalidadeAnualIndividualCFD($_POST["modalidadeAnualIndividualCFD"]);
        $this->tabelaContribuicao->setModalidadeMensalDual($_POST["modalidadeMensalDual"]);
        $this->tabelaContribuicao->setModalidadeTrimestralDualReal($_POST["modalidadeTrimestralDualReal"]);
        $this->tabelaContribuicao->setModalidadeTrimestralDualDolar($_POST["modalidadeTrimestralDualDolar"]);
        $this->tabelaContribuicao->setModalidadeTrimestralDualEuro($_POST["modalidadeTrimestralDualEuro"]);
        $this->tabelaContribuicao->setModalidadeAnualDualReal($_POST["modalidadeAnualDualReal"]);
        $this->tabelaContribuicao->setModalidadeAnualDualCFD($_POST["modalidadeAnualDualCFD"]);
        $this->tabelaContribuicao->setTaxaInscricaoReal($_POST["taxaInscricaoReal"]);
        $this->tabelaContribuicao->setTaxaInscricaoDolar($_POST["taxaInscricaoDolar"]);
        $this->tabelaContribuicao->setTaxaInscricaoEuro($_POST["taxaInscricaoEuro"]);
        $this->tabelaContribuicao->setUsuario($_POST["usuario"]);
        
        if ($this->tabelaContribuicao->cadastraTabelaContribuicao()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaTabelaContribuicao&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse Tipo de Objeto Dashboard!');
		  		window.location = '../painelDeControle.php?corpo=buscaTabelaContribuicao';
		  </script>";
        }
    }

    public function listaTabelaContribuicao($tipo=null) {
        
        $resultado = $this->tabelaContribuicao->listaTabelaContribuicao($tipo);
        return $resultado;
    }

    public function buscaTabelaContribuicao($id) {

        $resultado = $this->tabelaContribuicao->buscaIdTabelaContribuicao($id);

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->tabelaContribuicao->setIdTabelaContribuicao($vetor['idTabelaContribuicao']);
            	$this->tabelaContribuicao->setTipo($vetor['tipo']);
            	$this->tabelaContribuicao->setModalidadeMensalIndividual($vetor["modalidadeMensalIndividual"]);
                $this->tabelaContribuicao->setModalidadeTrimestralIndividualReal($vetor["modalidadeTrimestralIndividualReal"]);
                $this->tabelaContribuicao->setModalidadeTrimestralIndividualDolar($vetor["modalidadeTrimestralIndividualDolar"]);
                $this->tabelaContribuicao->setModalidadeTrimestralIndividualEuro($vetor["modalidadeTrimestralIndividualEuro"]);
                $this->tabelaContribuicao->setModalidadeAnualIndividualReal($vetor["modalidadeAnualIndividualReal"]);
                $this->tabelaContribuicao->setModalidadeAnualIndividualCFD($vetor["modalidadeAnualIndividualCFD"]);
                $this->tabelaContribuicao->setModalidadeMensalDual($vetor["modalidadeMensalDual"]);
                $this->tabelaContribuicao->setModalidadeTrimestralDualReal($vetor["modalidadeTrimestralDualReal"]);
                $this->tabelaContribuicao->setModalidadeTrimestralDualDolar($vetor["modalidadeTrimestralDualDolar"]);
                $this->tabelaContribuicao->setModalidadeTrimestralDualEuro($vetor["modalidadeTrimestralDualEuro"]);
                $this->tabelaContribuicao->setModalidadeAnualDualReal($vetor["modalidadeAnualDualReal"]);
                $this->tabelaContribuicao->setModalidadeAnualDualCFD($vetor["modalidadeAnualDualCFD"]);
                $this->tabelaContribuicao->setTaxaInscricaoReal($vetor["taxaInscricaoReal"]);
                $this->tabelaContribuicao->setTaxaInscricaoDolar($vetor["taxaInscricaoDolar"]);
                $this->tabelaContribuicao->setTaxaInscricaoEuro($vetor["taxaInscricaoEuro"]);
                $this->tabelaContribuicao->setUsuario($vetor["usuario"]);
                $this->tabelaContribuicao->setUsuarioAlteracao($vetor["usuarioAlteracao"]);
            }

            return $this->tabelaContribuicao;
        } else {
            return false;
        }
    }

    public function alteraTabelaContribuicao() {

        $this->tabelaContribuicao->setIdTabelaContribuicao($_POST["idTabelaContribuicao"]);
        $this->tabelaContribuicao->setTipo($_POST["tipo"]);
        $this->tabelaContribuicao->setModalidadeMensalIndividual($_POST["modalidadeMensalIndividual"]);
        $this->tabelaContribuicao->setModalidadeTrimestralIndividualReal($_POST["modalidadeTrimestralIndividualReal"]);
        $this->tabelaContribuicao->setModalidadeTrimestralIndividualDolar($_POST["modalidadeTrimestralIndividualDolar"]);
        $this->tabelaContribuicao->setModalidadeTrimestralIndividualEuro($_POST["modalidadeTrimestralIndividualEuro"]);
        $this->tabelaContribuicao->setModalidadeAnualIndividualReal($_POST["modalidadeAnualIndividualReal"]);
        $this->tabelaContribuicao->setModalidadeAnualIndividualCFD($_POST["modalidadeAnualIndividualCFD"]);
        $this->tabelaContribuicao->setModalidadeMensalDual($_POST["modalidadeMensalDual"]);
        $this->tabelaContribuicao->setModalidadeTrimestralDualReal($_POST["modalidadeTrimestralDualReal"]);
        $this->tabelaContribuicao->setModalidadeTrimestralDualDolar($_POST["modalidadeTrimestralDualDolar"]);
        $this->tabelaContribuicao->setModalidadeTrimestralDualEuro($_POST["modalidadeTrimestralDualEuro"]);
        $this->tabelaContribuicao->setModalidadeAnualDualReal($_POST["modalidadeAnualDualReal"]);
        $this->tabelaContribuicao->setModalidadeAnualDualCFD($_POST["modalidadeAnualDualCFD"]);
        $this->tabelaContribuicao->setTaxaInscricaoReal($_POST["taxaInscricaoReal"]);
        $this->tabelaContribuicao->setTaxaInscricaoDolar($_POST["taxaInscricaoDolar"]);
        $this->tabelaContribuicao->setTaxaInscricaoEuro($_POST["taxaInscricaoEuro"]);
        $this->tabelaContribuicao->setUsuarioAlteracao($_POST["usuarioAlteracao"]);
        
        if ($this->tabelaContribuicao->alteraTabelaContribuicao($_POST["idTabelaContribuicao"])) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaTabelaContribuicao&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse Tipo de Objeto Dashboard!');
		  		window.location = '../painelDeControle.php?corpo=buscaTabelaContribuicao';
		  </script>";
        }
    }

}
?>