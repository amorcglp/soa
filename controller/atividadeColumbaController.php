<?php

@include_once("model/atividadeColumbaClass.php");
@include_once("../model/atividadeColumbaClass.php");

@include_once("model/organismoClass.php");
@include_once("../model/organismoClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

//@include_once ("../model/criaSessaoClass.php");

class atividadeColumbaController {

	private $atividadeColumba;
	private $organismo;
	//private $sessao;

	public function __construct()
	{
		//$this->sessao 		= new criaSessao();
		$this->atividadeColumba	= new atividadeColumba();
		$this->organismo		= new organismo();
		
	}

	public function cadastroAtividadeColumba() {
            
            //Filtrar Request
            $arrFiltroRequest=array('h_seqCadastColumba',
                                'codAfiliacaoColumba',
                                'nomeColumba',
                                'fk_idOrganismoAfiliado',
                                'trimestre',
                                'ano',
                                'paisAtivosOA',
                                'reunioes',
                                'ensaios',
                                'iniciacoesLojaCapitulo',
                                'aposicaoNome',
                                'casamento',
                                'convocacoesRitualisticas',
                                'iniciacoesGraus',
                                'instalacao',
                                'avaliacaoCoordenadora',
                                'comentarios',
                                'usuario'
                            );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }

            //Filtrar Input
            $clean = array();
            $dirty = array(

            0 => array($_POST["h_seqCadastColumba"],"inteiro"),
            1 => array($_POST["fk_idOrganismoAfiliado"],"inteiro"),
            2 => array($_POST["trimestre"],"inteiro"),
            3 => array($_POST["ano"],"inteiro"),
            4 => array($_POST["paisAtivosOA"],"inteiro"),
            5 => array($_POST["reunioes"],"inteiro"),
            6 => array($_POST["ensaios"],"inteiro"),
            7 => array($_POST["iniciacoesLojaCapitulo"],"inteiro"),
            8 => array($_POST["aposicaoNome"],"inteiro"),
            9 => array($_POST["casamento"],"inteiro"),
            10 => array($_POST["convocacoesRitualisticas"],"inteiro"),
            11 => array($_POST["iniciacoesGraus"],"inteiro"),
            12 => array($_POST["instalacao"],"inteiro"),
            13 => array($_POST["avaliacaoCoordenadora"],"inteiro"),
            14 => array(cleanHtml($_POST["comentarios"]),"html"),
            15 => array($_POST["usuario"],"inteiro"),
            16 => array($_POST["codAfiliacaoColumba"],"inteiro"),
            17 => array($_POST["nomeColumba"],"texto")    

            );

            if(!validaSegurancaInput($dirty))
            {    
                $clean = $dirty;
            
		if(!$this->atividadeColumba->verificaSeJaCadastrado($clean[0][0],$clean[1][0],$clean[2][0],$clean[3][0]))
		{	
                        $this->atividadeColumba->setSeqCadastMembro($clean[0][0]);
			$this->atividadeColumba->setFk_idOrganismoAfiliado($clean[1][0]);
                        $this->atividadeColumba->setTrimestre($clean[2][0]);
                        $this->atividadeColumba->setAno($clean[3][0]);
                        $this->atividadeColumba->setPaisAtivosOA($clean[4][0]);
                        $this->atividadeColumba->setReunioes($clean[5][0]);
                        $this->atividadeColumba->setEnsaios($clean[6][0]);
                        $this->atividadeColumba->setIniciacoesLojaCapitulo($clean[7][0]);
                        $this->atividadeColumba->setAposicaoNome($clean[8][0]);
                        $this->atividadeColumba->setCasamento($clean[9][0]);
                        $this->atividadeColumba->setConvocacoesRitualisticas($clean[10][0]);
                        $this->atividadeColumba->setIniciacoesGraus($clean[11][0]);
                        $this->atividadeColumba->setInstalacao($clean[12][0]);
                        $this->atividadeColumba->setAvaliacao($clean[13][0]);
                        $this->atividadeColumba->setComentarios($clean[14][0]);
                        $this->atividadeColumba->setUsuario($clean[15][0]);
                        $this->atividadeColumba->setCodigoAfiliacao($clean[16][0]);
                        $this->atividadeColumba->setNomeColumba($clean[17][0]);
		   
			//Cadastrar ata e pegar ultimo id inserido
			$return = $this->atividadeColumba->cadastroAtividadeColumba();
			 		
			if ($return) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAtividadesColumbas&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar essa atividade da Columba!');
	                    window.location = '../painelDeControle.php?corpo=buscaAtividadesColumbas';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAtividadesColumbas&jaCadastrado=1&seqCadastMembro=".$_POST['seqCadastMembro']."';
			 	</script>";
		}
                
            }else{
                echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade para O.A. pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                        window.location = '../painelDeControle.php?corpo=buscaAtividadesColumbas';
                                    </script>";
            }
	}

	public function listaAtividadeColumba($idOa=null,$tipoColumba=null) {
		$retorno = $this->atividadeColumba->listaAtividadeColumba($idOa,$tipoColumba);
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaAtividadeColumbaPorId($id) {

		$resultado = $this->atividadeColumba->buscaAtividadeColumbaPorId($id);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);"]);
                                $this->atividadeColumba->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
				$this->atividadeColumba->setTrimestre($vetor['trimestre']);
				$this->atividadeColumba->setAno($vetor['ano']);
                                $this->atividadeColumba->setSeqCadastMembro($vetor['seqCadastMembro']);
                                $this->atividadeColumba->setCodigoAfiliacao($vetor['codigoAfiliacao']);
                                $this->atividadeColumba->setNomeColumba($vetor['nomeColumba']);
                                $this->atividadeColumba->setPaisAtivosOA($vetor['paisAtivosOA']);
                                $this->atividadeColumba->setReunioes($vetor['reunioes']);
                                $this->atividadeColumba->setEnsaios($vetor['ensaios']);
                                $this->atividadeColumba->setIniciacoesLojaCapitulo($vetor['iniciacoesLojaCapitulo']);
                                $this->atividadeColumba->setAposicaoNome($vetor['aposicaoNome']);
                                $this->atividadeColumba->setCasamento($vetor['casamento']);
                                $this->atividadeColumba->setConvocacoesRitualisticas($vetor['convocacoesRitualisticas']);
                                $this->atividadeColumba->setIniciacoesGraus($vetor['iniciacoesGraus']);
                                $this->atividadeColumba->setInstalacao($vetor['instalacao']);
                                $this->atividadeColumba->setAvaliacao($vetor['avaliacao']);
                                $this->atividadeColumba->setComentarios($vetor['comentarios']);
			}

			return $this->atividadeColumba;
		} else {
			return false;
		}
	}
        
        public function alteraAtividadeColumba($id) {
            
            //Filtrar Request
            $arrFiltroRequest=array(
                                'h_seqCadastColumba',
                                'codAfiliacaoColumba',
                                'nomeColumba',
                                'trimestre',
                                'ano',
                                'paisAtivosOA',
                                'reunioes',
                                'ensaios',
                                'iniciacoesLojaCapitulo',
                                'aposicaoNome',
                                'casamento',
                                'convocacoesRitualisticas',
                                'iniciacoesGraus',
                                'instalacao',
                                'avaliacaoCoordenadora',
                                'comentarios',
                                'usuario'
                            );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }

            //Filtrar Input
            $clean = array();
            $dirty = array(

            0 => array($id,"inteiro"),
            1 => array($_POST["trimestre"],"inteiro"),    
            2 => array($_POST["h_seqCadastColumba"],"inteiro"),
            3 => array($_POST["ano"],"inteiro"),
            4 => array($_POST["paisAtivosOA"],"inteiro"),
            5 => array($_POST["reunioes"],"inteiro"),
            6 => array($_POST["ensaios"],"inteiro"),
            7 => array($_POST["iniciacoesLojaCapitulo"],"inteiro"),
            8 => array($_POST["aposicaoNome"],"inteiro"),
            9 => array($_POST["casamento"],"inteiro"),
            10 => array($_POST["convocacoesRitualisticas"],"inteiro"),
            11 => array($_POST["iniciacoesGraus"],"inteiro"),
            12 => array($_POST["instalacao"],"inteiro"),
            13 => array($_POST["avaliacaoCoordenadora"],"inteiro"),
            14 => array(cleanHtml($_POST["comentarios"]),"html"),
            15 => array($_POST["usuario"],"inteiro"),
            16 => array($_POST["codAfiliacaoColumba"],"inteiro"),
            17 => array($_POST["nomeColumba"],"texto")    

            );

            if(!validaSegurancaInput($dirty))
            {    
                $clean = $dirty;

                $this->atividadeColumba->setTrimestre($clean[1][0]);
                $this->atividadeColumba->setSeqCadastMembro($clean[2][0]);
                $this->atividadeColumba->setAno($clean[3][0]);
                $this->atividadeColumba->setPaisAtivosOA($clean[4][0]);
                $this->atividadeColumba->setReunioes($clean[5][0]);
                $this->atividadeColumba->setEnsaios($clean[6][0]);
                $this->atividadeColumba->setIniciacoesLojaCapitulo($clean[7][0]);
                $this->atividadeColumba->setAposicaoNome($clean[8][0]);
                $this->atividadeColumba->setCasamento($clean[9][0]);
                $this->atividadeColumba->setConvocacoesRitualisticas($clean[10][0]);
                $this->atividadeColumba->setIniciacoesGraus($clean[11][0]);
                $this->atividadeColumba->setInstalacao($clean[12][0]);
                $this->atividadeColumba->setAvaliacao($clean[13][0]);
                $this->atividadeColumba->setComentarios($clean[14][0]);
                $this->atividadeColumba->setUltimoAtualizar($clean[15][0]);
                $this->atividadeColumba->setCodigoAfiliacao($clean[16][0]);
                $this->atividadeColumba->setNomeColumba($clean[17][0]);

                //Cadastrar ata e pegar ultimo id inserido
                $return = $this->atividadeColumba->alteraAtividadeColumba($clean[0][0]);

                if ($return) {

                        echo "<script type='text/javascript'>
                                        window.location = '../painelDeControle.php?corpo=buscaAtividadesColumbas&salvo=1';
                        </script>";
                } else {
                        echo "<script type='text/javascript'>
                    alert('Não foi possível alterar essa atividade da Columba!');
                    window.location = '../painelDeControle.php?corpo=buscaAtividadesColumbas';
                  </script>";
                }
            }else{
                echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade para O.A. pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                        window.location = '../painelDeControle.php?corpo=buscaAtividadesColumbas';
                                    </script>";
            }
		
	}

}

?>