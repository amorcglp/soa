<?php

@include_once("../model/anotacoesClass.php");
@include_once("model/anotacoesClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class anotacoesController {

    private $anotacao;

    public function __construct() {

        $this->anotacao = new anotacoes();
    }

    public function cadastroAnotacoes() {
        
        //Filtrar Request
        $arrFiltroRequest=array('tituloAnotacoes',
                            'conteudoAnotacoes',
                            'dataAnotacoes',
                            'horaAnotacoes',
                            'fk_idUsuario'
                            );
        if(validaSegurancaRequest($arrFiltroRequest))
        {
            enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
            echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
            exit();
        }
        
        //Filtrar Input
        $clean = array();
        $dirty = array(

        0 => array($_POST["tituloAnotacoes"],"textoNumero"),
        1 => array($_POST["conteudoAnotacoes"],"textoNumero"),
        2 => array($_POST["dataAnotacoes"],"textoNumero"),
        3 => array($_POST["horaAnotacoes"],"textoNumero"),
        4 => array($_POST["fk_idUsuario"],"inteiro")
        );

        if(!validaSegurancaInput($dirty))
        {    
            $clean = $dirty;
		        
            $this->anotacao->setTituloAnotacoes($clean[0][0]);
            $this->anotacao->setConteudoAnotacoes($clean[1][0]);
            $this->anotacao->setDataAnotacoes(substr($clean[2][0],6,4)."-".substr($clean[2][0],3,2)."-".substr($clean[2][0],0,2));
            $this->anotacao->setHoraAnotacoes($clean[3][0]);
            $this->anotacao->setFk_idUsuario($clean[4][0]);

            if ($this->anotacao->cadastroAnotacoes()) {

                echo "<script type='text/javascript'>
                        window.location = '../painelDeControle.php?corpo=buscaAnotacoes&salvo=1';
                      </script>";
            } else {
                echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essa Anotação!');
                        window.location = '../painelDeControle.php?corpo=cadastroAnotacoes';
                      </script>";
            }
        }else{
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaAnotacoes';
                                </script>";
        } 
    }

    public function listaidAnotacoes() {
        $retorno = $this->anotacao->listaAnotacoes($fk_idUsuario);
        $listaAnotacoes = "";

        foreach ($retorno as $dados) {
            $listaAnotacoes[] = $dados['idAnotacoes'];
        }

        if ($listaAnotacoes) {
            return $listaAnotacoes;
        } else {
            return false;
        }
    }

    public function listaAnotacoes($fk_idUsuario) {
        if (!isset($_POST['dataAnotacoes'])) {
            $resultado = $this->anotacao->listaAnotacoes($fk_idUsuario);
        } else {
        	/*
            $this->anotacao->set("fk_idUsuario", $_POST['fk_idUsuario']);
            $resultado = $this->anotacao->buscaAnotacoesUsuario($_POST['idAnotacoes']);
            */
        }
		
        return $resultado;
    }

    public function buscaAnotacoes($idAnotacoes) {

        $resultado = $this->anotacao->buscaIdAnotacoes($idAnotacoes);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);
                $this->anotacao->setTituloAnotacoes($vetor['tituloAnotacoes']);
                $this->anotacao->setConteudoAnotacoes($vetor['conteudoAnotacoes']);
                $this->anotacao->setDataAnotacoes(date('d/m/Y', strtotime($vetor['dataAnotacoes'])));
                $this->anotacao->setHoraAnotacoes(substr($vetor['horaAnotacoes'],0,2).":".substr($vetor['horaAnotacoes'],3,2));
                $this->anotacao->setFk_idUsuario($vetor['fk_idUsuario']);
                
            }

            return $this->anotacao;
        } else {
            return false;
        }
    }
    
    public function alteraAnotacoes() {
        
        //Filtrar Request
        $arrFiltroRequest=array('tituloAnotacoes',
                            'conteudoAnotacoes',
                            'dataAnotacoes',
                            'horaAnotacoes',
                            'idAnotacoes'
                            );
        if(validaSegurancaRequest($arrFiltroRequest))
        {
            enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
            echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
            exit();
        }
        
        //Filtrar Input
        $clean = array();
        $dirty = array(

        0 => array($_POST["tituloAnotacoes"],"textoNumero"),
        1 => array($_POST["conteudoAnotacoes"],"textoNumero"),
        2 => array($_POST["dataAnotacoes"],"textoNumero"),
        3 => array($_POST["horaAnotacoes"],"textoNumero"),
        4 => array($_POST["idAnotacoes"],"inteiro")
        );

        if(!validaSegurancaInput($dirty))
        {    
            $clean = $dirty;
        
            $this->anotacao->setTituloAnotacoes($clean[0][0]);
            $this->anotacao->setConteudoAnotacoes($clean[1][0]);
            $this->anotacao->setDataAnotacoes(substr($clean[2][0],6,4)."-".substr($clean[2][0],3,2)."-".substr($clean[2][0],0,2));
            $this->anotacao->setHoraAnotacoes($clean[3][0]);

            if ($this->anotacao->alteraAnotacoes($clean[4][0])) {

                echo
                "<script type='text/javascript'>
                            window.location = '../painelDeControle.php?corpo=buscaAnotacoes&salvo=1';
                            </script>";
            } else {
                echo
                "<script type='text/javascript'>
                            alert ('N\u00e3o foi poss\u00edvel alterar a Anotação!');
                            window.location = '../painelDeControle.php?corpo=alteraAnotacoes&id=" . $_POST['idAnotacoes'] . "';
                            </script>";
            }
        }else{  
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaAnotacoes';
                                </script>";
        } 
    }

    public function removeAnotacoes($idAnotacoes) {
    	$this->anotacao->getIdAnotacoes($idAnotacoes);
    	
        $resultado = $this->anotacao->removeAnotacoes($idAnotacoes);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				window.location = 'painelDeControle.php?corpo=buscaAnotacoes&excluido=1';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('N\u00e3o foi poss\u00edvel excluir essa Anotação!');
				window.location = '../painelDeControle.php?corpo=buscaAnotacoes';
				</script>";
        }
    }

}

?>