<?php

@include_once("model/auxiliarWebClass.php");
@include_once("../model/auxiliarWebClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once ("../model/criaSessaoClass.php");

class auxiliarWebController {

	private $auxiliarWeb;
	private $sessao;

	public function __construct()
	{
		$this->sessao         = new criaSessao();
		$this->auxiliarWeb    = new auxiliarWeb();
		$this->usuario        = new Usuario();
		
	}

	public function cadastroAuxiliarWeb() {

                $ultimo_id="";
                    
		$this->auxiliarWeb->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
                $this->auxiliarWeb->setDataTermoCompromisso(substr($_POST['dataTermoCompromisso'],6,4)."-".substr($_POST['dataTermoCompromisso'],3,2)."-".substr($_POST['dataTermoCompromisso'],0,2));
		$this->auxiliarWeb->setNome($_POST['nome']);
		$this->auxiliarWeb->setCodigoAfiliacao($_POST['codigoAfiliacao']);
		$this->auxiliarWeb->setCpf($_POST['cpf']);
                $this->auxiliarWeb->setEmail($_POST['email']);
                $this->auxiliarWeb->setLogradouro($_POST['logradouro']);
                $this->auxiliarWeb->setNumero($_POST['numero']);
                $this->auxiliarWeb->setComplemento($_POST['complemento']);
                $this->auxiliarWeb->setBairro($_POST['bairro']);
                $this->auxiliarWeb->setCep($_POST['cep']);
                $this->auxiliarWeb->setCidade($_POST['cidade']);
                $this->auxiliarWeb->setUf($_POST['uf']);
                $this->auxiliarWeb->setPais($_POST['pais']);
                $this->auxiliarWeb->setTelefoneResidencial($_POST['telefoneResidencial']);
                $this->auxiliarWeb->setTelefoneComercial($_POST['telefoneComercial']);
                $this->auxiliarWeb->setCelular($_POST['celular']);
		$this->auxiliarWeb->setUsuario($this->sessao->getValue("seqCadast"));
                

		//Cadastrar auxiliarWeb e pegar ultimo id inserido
		if(!$this->auxiliarWeb->verificaSeJaExiste())
		{
			$ultimo_id = $this->auxiliarWeb->cadastroAuxiliarWeb();
		
			
			//exit();	
			if ($ultimo_id!="") {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAuxiliarWeb&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Nao foi possivel cadastrar esse termo!');
	                    window.location = '../painelDeControle.php?corpo=buscaAuxiliarWeb';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAuxiliarWeb&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaAuxiliarWeb() {
		$retorno = $this->auxiliarWeb->listaAuxiliarWeb();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaAuxiliarWeb($idAuxiliarWeb) {

		$resultado = $this->auxiliarWeb->buscarIdAuxiliarWeb($idAuxiliarWeb);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->auxiliarWeb->setIdAuxiliarWeb($vetor['idAuxiliarWeb']);
				$this->auxiliarWeb->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
                                $this->auxiliarWeb->setDataTermoCompromisso(substr($vetor['dataTermoCompromisso'],8,2)."/".substr($vetor['dataTermoCompromisso'],5,2)."/".substr($vetor['dataTermoCompromisso'],0,4));
				$this->auxiliarWeb->setNome($vetor['nome']);
				$this->auxiliarWeb->setCodigoAfiliacao($vetor['codigoAfiliacao']);
				$this->auxiliarWeb->setCpf($vetor['cpf']);
				$this->auxiliarWeb->setEmail($vetor['email']);
                                $this->auxiliarWeb->setLogradouro($vetor['logradouro']);
                                $this->auxiliarWeb->setNumero($vetor['numero']);
                                $this->auxiliarWeb->setComplemento($vetor['complemento']);
                                $this->auxiliarWeb->setBairro($vetor['bairro']);
                                $this->auxiliarWeb->setCep($vetor['cep']);
                                $this->auxiliarWeb->setCidade($vetor['cidade']);
                                $this->auxiliarWeb->setUf($vetor['uf']);
                                $this->auxiliarWeb->setPais($vetor['pais']);
                                $this->auxiliarWeb->setTelefoneResidencial($vetor['telefoneResidencial']);
                                $this->auxiliarWeb->setTelefoneComercial($vetor['telefoneComercial']);
                                $this->auxiliarWeb->setCelular($vetor['celular']);
			}
			return $this->auxiliarWeb;
		} else {
			return false;
		}
	}
	

	public function alteraAuxiliarWeb() {

		//$this->auxiliarWeb->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->auxiliarWeb->setDataTermoCompromisso(substr($_POST['dataTermoCompromisso'],6,4)."-".substr($_POST['dataTermoCompromisso'],3,2)."-".substr($_POST['dataTermoCompromisso'],0,2));
		$this->auxiliarWeb->setNome($_POST['nome']);
		$this->auxiliarWeb->setCodigoAfiliacao($_POST['codigoAfiliacao']);
		$this->auxiliarWeb->setCpf($_POST['cpf']);
                $this->auxiliarWeb->setEmail($_POST['email']);
                $this->auxiliarWeb->setLogradouro($_POST['logradouro']);
                $this->auxiliarWeb->setNumero($_POST['numero']);
                $this->auxiliarWeb->setComplemento($_POST['complemento']);
                $this->auxiliarWeb->setBairro($_POST['bairro']);
                $this->auxiliarWeb->setCep($_POST['cep']);
                $this->auxiliarWeb->setCidade($_POST['cidade']);
                $this->auxiliarWeb->setUf($_POST['uf']);
                $this->auxiliarWeb->setPais($_POST['pais']);
                $this->auxiliarWeb->setTelefoneResidencial($_POST['telefoneResidencial']);
                $this->auxiliarWeb->setTelefoneComercial($_POST['telefoneComercial']);
                $this->auxiliarWeb->setCelular($_POST['celular']);
		$this->auxiliarWeb->setUltimoAtualizar($this->sessao->getValue("seqCadast"));
                
		if ($this->auxiliarWeb->alteraAuxiliarWeb($_POST['idAuxiliarWeb'])) {

			echo
                        "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaAuxiliarWeb&salvo=1';
			</script>";
		} else {
                    exit();
			echo
                        "<script type='text/javascript'>
			alert ('Nao foi possivel alterar o termo!');
			window.location = '../painelDeControle.php?corpo=buscaAuxiliarWeb';
			</script>";
		}
	}

}

?>