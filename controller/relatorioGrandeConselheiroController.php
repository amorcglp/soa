<?php

@include_once("model/relatorioGrandeConselheiroClass.php");
@include_once("../model/relatorioGrandeConselheiroClass.php");

@include_once("model/relatorioGrandeConselheiroSupervisaoClass.php");
@include_once("../model/relatorioGrandeConselheiroSupervisaoClass.php");

@include_once("model/relatorioGrandeConselheiroMonitorClass.php");
@include_once("../model/relatorioGrandeConselheiroMonitorClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/regiaoRosacruzClass.php");
@include_once("../model/regiaoRosacruzClass.php");

@include_once ("../model/criaSessaoClass.php");

class relatorioGrandeConselheiroController {

	private $relatorio;
	private $sessao;
	private $supervisao;
	private $monitor;
	private $usuario;
	private $regiao;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->relatorio	= new RelatorioGrandeConselheiro();
		$this->supervisao	= new relatorioGrandeConselheiroSupervisao();
		$this->monitor		= new relatorioGrandeConselheiroMonitor();
		$this->usuario		= new Usuario();
		$this->regiao		= new regiaoRosacruz();
		
	}

	public function cadastroRelatorioGrandeConselheiro() {
		
		$this->relatorio->setCriadoPor($_POST['usuario']);
		$this->relatorio->setRegiao($_POST['regiao']);
		$this->relatorio->setSemestre($_POST['semestre']);
		$this->relatorio->setAno($_POST['ano']);
		$this->relatorio->setPergunta1($_POST['pergunta1']);
		$this->relatorio->setPergunta3($_POST['pergunta3']);
		$this->relatorio->setPergunta4($_POST['pergunta4']);
		$this->relatorio->setPergunta5($_POST['pergunta5']);
		$this->relatorio->setPergunta6($_POST['pergunta6']);
		$this->relatorio->setPergunta7($_POST['pergunta7']);
		$this->relatorio->setPergunta8($_POST['pergunta8']);
		$this->relatorio->setPergunta9($_POST['pergunta9']);
		$this->relatorio->setPergunta10($_POST['pergunta10']);
		$this->relatorio->setPergunta11($_POST['pergunta11']);
		$this->relatorio->setPergunta12($_POST['pergunta12']);
		$this->relatorio->setPergunta14($_POST['pergunta14']);
		$this->relatorio->setPergunta15($_POST['pergunta15']);
		$this->relatorio->setPergunta16($_POST['pergunta16']);
		$this->relatorio->setPergunta17($_POST['pergunta17']);
		$this->relatorio->setPergunta18($_POST['pergunta18']);
		$this->relatorio->setPergunta19($_POST['pergunta19']);
		$this->relatorio->setPergunta20($_POST['pergunta20']);
		$this->relatorio->setPergunta21($_POST['pergunta21']);
		$this->relatorio->setPergunta22($_POST['pergunta22']);
		$this->relatorio->setPergunta23($_POST['pergunta23']);
		$this->relatorio->setPergunta24($_POST['pergunta24']);
		$this->relatorio->setPergunta25($_POST['pergunta25']);
		$this->relatorio->setPergunta26($_POST['pergunta26']);
		$this->relatorio->setPergunta27($_POST['pergunta27']);
		$this->relatorio->setPergunta28($_POST['pergunta28']);
		$this->relatorio->setPergunta29($_POST['pergunta29']);
		$this->relatorio->setPergunta30($_POST['pergunta30']);
		$this->relatorio->setPergunta31($_POST['pergunta31']);
		$this->relatorio->setPergunta32($_POST['pergunta32']);
		$this->relatorio->setPergunta33($_POST['pergunta33']);
		$this->relatorio->setPergunta34($_POST['pergunta34']);
		$this->relatorio->setPergunta35($_POST['pergunta35']);
		$this->relatorio->setPergunta36($_POST['pergunta36']);
		$this->relatorio->setPergunta37($_POST['pergunta37']);
		$this->relatorio->setComentario1($_POST['comentario1']);
		$this->relatorio->setComentario3($_POST['comentario3']);
		$this->relatorio->setComentario4($_POST['comentario4']);
		$this->relatorio->setComentario5($_POST['comentario5']);
		$this->relatorio->setComentario6($_POST['comentario6']);
		$this->relatorio->setComentario7($_POST['comentario7']);
		$this->relatorio->setComentario8($_POST['comentario8']);
		$this->relatorio->setComentario9($_POST['comentario9']);
		$this->relatorio->setComentario10($_POST['comentario10']);
		$this->relatorio->setComentario11($_POST['comentario11']);
		$this->relatorio->setComentario12($_POST['comentario12']);
		$this->relatorio->setComentario14($_POST['comentario14']);
		$this->relatorio->setComentario15($_POST['comentario15']);
		$this->relatorio->setComentario16($_POST['comentario16']);
		$this->relatorio->setComentario17($_POST['comentario17']);
		$this->relatorio->setComentario18($_POST['comentario18']);
		$this->relatorio->setComentario19($_POST['comentario19']);
		$this->relatorio->setComentario20($_POST['comentario20']);
		$this->relatorio->setComentario21($_POST['comentario21']);
		$this->relatorio->setComentario22($_POST['comentario22']);
		$this->relatorio->setComentario23($_POST['comentario23']);
		$this->relatorio->setComentario24($_POST['comentario24']);
		$this->relatorio->setComentario25($_POST['comentario25']);
		$this->relatorio->setComentario26($_POST['comentario26']);
		$this->relatorio->setComentario27($_POST['comentario27']);
		$this->relatorio->setComentario28($_POST['comentario28']);
		$this->relatorio->setComentario29($_POST['comentario29']);
		$this->relatorio->setComentario30($_POST['comentario30']);
		$this->relatorio->setComentario31($_POST['comentario31']);
		$this->relatorio->setComentario32($_POST['comentario32']);
		$this->relatorio->setComentario33($_POST['comentario33']);
		$this->relatorio->setComentario34($_POST['comentario34']);
		$this->relatorio->setComentario35($_POST['comentario35']);
		$this->relatorio->setComentario36($_POST['comentario36']);
		$this->relatorio->setComentario37($_POST['comentario37']);
		
		//Cadastrar ata e pegar ultimo id inserido
		if(!$this->relatorio->verificaSeJaExiste())
		{
			$ultimo_id = $this->relatorio->cadastroRelatorioGrandeConselheiro();
			
			//Cadastrar oas que o gc supervisiona
			$return=true;
			
			if(isset($_POST["supervisao"]))
			{
				$supervisao=$_POST["supervisao"];
				for ($i=0;$i<count($supervisao);$i++)
				{
					$this->supervisao	= new relatorioGrandeConselheiroSupervisao();
					$this->supervisao->setFk_idRelatorioGrandeConselheiro($ultimo_id);
					$this->supervisao->setFk_idOrganismoAfiliado($supervisao[$i]);
					$this->supervisao->cadastroRelatorioGrandeConselheiroSupervisao();
					
				}
			}
			
			//Cadastrar monitores que atuam na região do grande conselheiro
			if(isset($_POST["monitores"]))
			{
				$monitores=$_POST["monitores"];
				for ($i=0;$i<count($monitores);$i++)
				{
					$this->monitor	= new RelatorioGrandeConselheiroMonitor();
					$this->monitor->setFk_idRelatorioGrandeConselheiro($ultimo_id);
					$this->monitor->setSeqCadast($monitores[$i]);
					$this->monitor->cadastroRelatorioGrandeConselheiroMonitor();
					
				}
			}
			
			//Pegar a Região
			$resultado = $this->regiao->listaRegiaoRosacruz(null,null,$_POST['regiao']);
			$regiaoRC="--";
			if($resultado)
			{
				foreach($resultado as $vetor)
				{
					$regiaoRC = $vetor['regiaoRosacruz'];
				}
			}
			
			
			/**
			 * Notificar GLP que o relatorio foi cadastrado
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Novo relatorio de Grande Conselheiro Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Relatorio de Grande Conselheiro Entregue pela Regiao ".$regiaoRC.", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuarioRelatorio'];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
					
			if ($return) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaRelatorioGrandeConselheiro&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar este relatorio!');
	                    window.location = '../painelDeControle.php?corpo=buscaRelatorioGrandeConselheiro';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaRelatorioGrandeConselheiro&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaRelatorioGrandeConselheiro() {
		$retorno = $this->relatorio->listaRelatorioGrandeConselheiro();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaRelatorioGrandeConselheiro($idRelatorioGrandeConselheiro) {

		$resultado = $this->relatorio->buscarIdRelatorioGrandeConselheiro($idRelatorioGrandeConselheiro);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->relatorio->setIdRelatorioGrandeConselheiro($vetor['idRelatorioGrandeConselheiro']);
				$this->relatorio->setCriadoPor($vetor['nomeUsuario']);
				$this->relatorio->setCriadoPorId($vetor['criadoPor']);
				$this->relatorio->setCodigoAfiliacao($vetor['codigoDeAfiliacao']);
				$this->relatorio->setDataCriacao($vetor['dataCriacao']);
				$this->relatorio->setRegiao($vetor['regiaoRosacruz']);
				$this->relatorio->setSemestre($vetor['semestre']);
				$this->relatorio->setAno($vetor['ano']);
				$this->relatorio->setPergunta1($vetor['pergunta1']);
				$this->relatorio->setPergunta3($vetor['pergunta3']);
				$this->relatorio->setPergunta4($vetor['pergunta4']);
				$this->relatorio->setPergunta5($vetor['pergunta5']);
				$this->relatorio->setPergunta6($vetor['pergunta6']);
				$this->relatorio->setPergunta7($vetor['pergunta7']);
				$this->relatorio->setPergunta8($vetor['pergunta8']);
				$this->relatorio->setPergunta9($vetor['pergunta9']);
				$this->relatorio->setPergunta10($vetor['pergunta10']);
				$this->relatorio->setPergunta11($vetor['pergunta11']);
				$this->relatorio->setPergunta12($vetor['pergunta12']);
				$this->relatorio->setPergunta14($vetor['pergunta14']);
				$this->relatorio->setPergunta15($vetor['pergunta15']);
				$this->relatorio->setPergunta16($vetor['pergunta16']);
				$this->relatorio->setPergunta17($vetor['pergunta17']);
				$this->relatorio->setPergunta18($vetor['pergunta18']);
				$this->relatorio->setPergunta19($vetor['pergunta19']);
				$this->relatorio->setPergunta20($vetor['pergunta20']);
				$this->relatorio->setPergunta21($vetor['pergunta21']);
				$this->relatorio->setPergunta22($vetor['pergunta22']);
				$this->relatorio->setPergunta23($vetor['pergunta23']);
				$this->relatorio->setPergunta24($vetor['pergunta24']);
				$this->relatorio->setPergunta25($vetor['pergunta25']);
				$this->relatorio->setPergunta26($vetor['pergunta26']);
				$this->relatorio->setPergunta27($vetor['pergunta27']);
				$this->relatorio->setPergunta28($vetor['pergunta28']);
				$this->relatorio->setPergunta29($vetor['pergunta29']);
				$this->relatorio->setPergunta30($vetor['pergunta30']);
				$this->relatorio->setPergunta31($vetor['pergunta31']);
				$this->relatorio->setPergunta32($vetor['pergunta32']);
				$this->relatorio->setPergunta33($vetor['pergunta33']);
				$this->relatorio->setPergunta34($vetor['pergunta34']);
				$this->relatorio->setPergunta35($vetor['pergunta35']);
				$this->relatorio->setPergunta36($vetor['pergunta36']);
				$this->relatorio->setPergunta37($vetor['pergunta37']);
				$this->relatorio->setComentario1($vetor['comentario1']);
				$this->relatorio->setComentario3($vetor['comentario3']);
				$this->relatorio->setComentario4($vetor['comentario4']);
				$this->relatorio->setComentario5($vetor['comentario5']);
				$this->relatorio->setComentario6($vetor['comentario6']);
				$this->relatorio->setComentario7($vetor['comentario7']);
				$this->relatorio->setComentario8($vetor['comentario8']);
				$this->relatorio->setComentario9($vetor['comentario9']);
				$this->relatorio->setComentario10($vetor['comentario10']);
				$this->relatorio->setComentario11($vetor['comentario11']);
				$this->relatorio->setComentario12($vetor['comentario12']);
				$this->relatorio->setComentario14($vetor['comentario14']);
				$this->relatorio->setComentario15($vetor['comentario15']);
				$this->relatorio->setComentario16($vetor['comentario16']);
				$this->relatorio->setComentario17($vetor['comentario17']);
				$this->relatorio->setComentario18($vetor['comentario18']);
				$this->relatorio->setComentario19($vetor['comentario19']);
				$this->relatorio->setComentario20($vetor['comentario20']);
				$this->relatorio->setComentario21($vetor['comentario21']);
				$this->relatorio->setComentario22($vetor['comentario22']);
				$this->relatorio->setComentario23($vetor['comentario23']);
				$this->relatorio->setComentario24($vetor['comentario24']);
				$this->relatorio->setComentario25($vetor['comentario25']);
				$this->relatorio->setComentario26($vetor['comentario26']);
				$this->relatorio->setComentario27($vetor['comentario27']);
				$this->relatorio->setComentario28($vetor['comentario28']);
				$this->relatorio->setComentario29($vetor['comentario29']);
				$this->relatorio->setComentario30($vetor['comentario30']);
				$this->relatorio->setComentario31($vetor['comentario31']);
				$this->relatorio->setComentario32($vetor['comentario32']);
				$this->relatorio->setComentario33($vetor['comentario33']);
				$this->relatorio->setComentario34($vetor['comentario34']);
				$this->relatorio->setComentario35($vetor['comentario35']);
				$this->relatorio->setComentario36($vetor['comentario36']);
				$this->relatorio->setComentario37($vetor['comentario37']);
				
			}
			return $this->relatorio;
		} else {
			return false;
		}
	}
	

	public function alteraRelatorioGrandeConselheiro() {

		$this->relatorio->setCriadoPor($_POST['usuario']);
		$this->relatorio->setRegiao($_POST['regiao']);
		$this->relatorio->setSemestre($_POST['semestre']);
		$this->relatorio->setAno($_POST['ano']);
		$this->relatorio->setPergunta1($_POST['pergunta1']);
		$this->relatorio->setPergunta3($_POST['pergunta3']);
		$this->relatorio->setPergunta4($_POST['pergunta4']);
		$this->relatorio->setPergunta5($_POST['pergunta5']);
		$this->relatorio->setPergunta6($_POST['pergunta6']);
		$this->relatorio->setPergunta7($_POST['pergunta7']);
		$this->relatorio->setPergunta8($_POST['pergunta8']);
		$this->relatorio->setPergunta9($_POST['pergunta9']);
		$this->relatorio->setPergunta10($_POST['pergunta10']);
		$this->relatorio->setPergunta11($_POST['pergunta11']);
		$this->relatorio->setPergunta12($_POST['pergunta12']);
		$this->relatorio->setPergunta14($_POST['pergunta14']);
		$this->relatorio->setPergunta15($_POST['pergunta15']);
		$this->relatorio->setPergunta16($_POST['pergunta16']);
		$this->relatorio->setPergunta17($_POST['pergunta17']);
		$this->relatorio->setPergunta18($_POST['pergunta18']);
		$this->relatorio->setPergunta19($_POST['pergunta19']);
		$this->relatorio->setPergunta20($_POST['pergunta20']);
		$this->relatorio->setPergunta21($_POST['pergunta21']);
		$this->relatorio->setPergunta22($_POST['pergunta22']);
		$this->relatorio->setPergunta23($_POST['pergunta23']);
		$this->relatorio->setPergunta24($_POST['pergunta24']);
		$this->relatorio->setPergunta25($_POST['pergunta25']);
		$this->relatorio->setPergunta26($_POST['pergunta26']);
		$this->relatorio->setPergunta27($_POST['pergunta27']);
		$this->relatorio->setPergunta28($_POST['pergunta28']);
		$this->relatorio->setPergunta29($_POST['pergunta29']);
		$this->relatorio->setPergunta30($_POST['pergunta30']);
		$this->relatorio->setPergunta31($_POST['pergunta31']);
		$this->relatorio->setPergunta32($_POST['pergunta32']);
		$this->relatorio->setPergunta33($_POST['pergunta33']);
		$this->relatorio->setPergunta34($_POST['pergunta34']);
		$this->relatorio->setPergunta35($_POST['pergunta35']);
		$this->relatorio->setPergunta36($_POST['pergunta36']);
		$this->relatorio->setPergunta37($_POST['pergunta37']);
		$this->relatorio->setComentario1($_POST['comentario1']);
		$this->relatorio->setComentario3($_POST['comentario3']);
		$this->relatorio->setComentario4($_POST['comentario4']);
		$this->relatorio->setComentario5($_POST['comentario5']);
		$this->relatorio->setComentario6($_POST['comentario6']);
		$this->relatorio->setComentario7($_POST['comentario7']);
		$this->relatorio->setComentario8($_POST['comentario8']);
		$this->relatorio->setComentario9($_POST['comentario9']);
		$this->relatorio->setComentario10($_POST['comentario10']);
		$this->relatorio->setComentario11($_POST['comentario11']);
		$this->relatorio->setComentario12($_POST['comentario12']);
		$this->relatorio->setComentario14($_POST['comentario14']);
		$this->relatorio->setComentario15($_POST['comentario15']);
		$this->relatorio->setComentario16($_POST['comentario16']);
		$this->relatorio->setComentario17($_POST['comentario17']);
		$this->relatorio->setComentario18($_POST['comentario18']);
		$this->relatorio->setComentario19($_POST['comentario19']);
		$this->relatorio->setComentario20($_POST['comentario20']);
		$this->relatorio->setComentario21($_POST['comentario21']);
		$this->relatorio->setComentario22($_POST['comentario22']);
		$this->relatorio->setComentario23($_POST['comentario23']);
		$this->relatorio->setComentario24($_POST['comentario24']);
		$this->relatorio->setComentario25($_POST['comentario25']);
		$this->relatorio->setComentario26($_POST['comentario26']);
		$this->relatorio->setComentario27($_POST['comentario27']);
		$this->relatorio->setComentario28($_POST['comentario28']);
		$this->relatorio->setComentario29($_POST['comentario29']);
		$this->relatorio->setComentario30($_POST['comentario30']);
		$this->relatorio->setComentario31($_POST['comentario31']);
		$this->relatorio->setComentario32($_POST['comentario32']);
		$this->relatorio->setComentario33($_POST['comentario33']);
		$this->relatorio->setComentario34($_POST['comentario34']);
		$this->relatorio->setComentario35($_POST['comentario35']);
		$this->relatorio->setComentario36($_POST['comentario36']);
		$this->relatorio->setComentario37($_POST['comentario37']);
		
		//Remover oas supervisionados do relatorio
                
                if(isset($_POST["supervisao"]))
                {
                
                    $superv = new relatorioGrandeConselheiroSupervisao();
                    $superv->removeOrganismos($_POST['fk_idRelatorioGrandeConselheiro']);
                    //Cadastrar oas supervisionados do relatorio

                    $supervisao=$_POST["supervisao"];
                    for ($i=0;$i<count($supervisao);$i++)
                    {	
                            $this->supervisao	= new relatorioGrandeConselheiroSupervisao();
                            $this->supervisao->setFk_idRelatorioGrandeConselheiro($_POST['fk_idRelatorioGrandeConselheiro']);
                            $this->supervisao->setFk_idOrganismoAfiliado($supervisao[$i]);
                            $this->supervisao->cadastroRelatorioGrandeConselheiroSupervisao();
                    }
		}
                
                if(isset($_POST["monitores"]))
                {    
                    //Remover monitores do relatorio
                    $mon = new relatorioGrandeConselheiroMonitor();
                    $mon->removeMonitores($_POST['fk_idRelatorioGrandeConselheiro']);
                    //Cadastrar oas supervisionados do relatorio
                    $monitores=$_POST["monitores"];
                    for ($i=0;$i<count($monitores);$i++)
                    {	
                            $this->monitor	= new relatorioGrandeConselheiroMonitor();
                            $this->monitor->setFk_idRelatorioGrandeConselheiro($_POST['fk_idRelatorioGrandeConselheiro']);
                            $this->monitor->setSeqCadast($monitores[$i]);
                            $this->monitor->cadastroRelatorioGrandeConselheiroMonitor();
                    }
                }
		if ($this->relatorio->alteraRelatorioGrandeConselheiro($_POST['fk_idRelatorioGrandeConselheiro'])) {

			echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaRelatorioGrandeConselheiro&salvo=1';
			</script>";
		} else {
			echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar o relatorio!');
			window.location = '../painelDeControle.php?corpo=buscaRelatorioGrandeConselheiro';
			</script>";
		}
	}

}

?>