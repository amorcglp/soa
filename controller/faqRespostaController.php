<?php
@include_once("model/faqRespostaClass.php");

@include_once("../model/faqRespostaClass.php");

class faqRespostaController {

    private $faqRespostaController;

    public function __construct() {

        $this->faqRespostaController = new FaqResposta();
    }

    public function cadastroRespostaFaq() {

        $this->faqRespostaController->setIdRespostaFaq($_POST["idRespostaFaq"]);
        $this->faqRespostaController->setTituloRespostaFaq($_POST["tituloRespostaFaq"]);
        $this->faqRespostaController->setResponsavelRespostaFaq($_POST["responsavelRespostaFaq"]);
        $this->faqRespostaController->setRespostaFaq($_POST["respostaFaq"]);


        if ($this->faqRespostaController->cadastraRespostaFaq()) {

            echo "<script type='text/javascript'>
		   window.location = '../painelDeControle.php?corpo=buscaRespostaFaq&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa resposta!');
		  window.location = '../painelDeControle.php?corpo=cadastroRespostaFaq';
		  </script>";
        }
    }

    public function listaRespostaFaq() {

        $resultado = $this->faqRespostaController->listaRespostaFaq();

        return $resultado;
    }

    public function buscaRespostaFaq() {

        $resultado = $this->faqRespostaController->buscarIdRespostaFaq();

        if ($resultado) {

            foreach ($resultado as $vetor) {

                $this->faqRespostaController->setIdRespostaFaq($vetor["idRespostaFaq"]);
                $this->faqRespostaController->setTituloRespostaFaq($vetor["tituloRespostaFaq"]);
                $this->faqRespostaController->setResponsavelRespostaFaq($vetor["responsavelRespostaFaq"]);
                $this->faqRespostaController->setRespostaFaq($vetor["respostaFaq"]);
            }

            return $this->faqRespostaController;
        } else {
            return false;
        }
    }

    public function alteraRespostaFaq() {

        $this->faqRespostaController->setTituloRespostaFaq($_POST["tituloRespostaFaq"]);
        $this->faqRespostaController->setResponsavelRespostaFaq($_POST["responsavelRespostaFaq"]);
        $this->faqRespostaController->setRespostaFaq($_POST["respostaFaq"]);

        if ($this->faqRespostaController->alteraRespostaFaq($_POST['idRespostaFaq'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaRespostaFaq&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar essa resposta do FAQ!');
			window.location = '../painelDeControle.php?corpo=alteraRespostaFaq&id=" . $_POST['idRespostaFaq'] . "';
			</script>";
        }
    }

    public function removeRespostaFaq($id) {
        $resultado = $this->faqRespostaController->removeRespostaFaq($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
                alert ('Resposta do FAQ exclu\u00eddo com sucesso!');
                window.location = '../home.php?url=buscaRespostaFaq&id=" . $id . "';
                </script>";
        } else {
            echo
            "<script type='text/javascript'>
                alert ('N\u00e3o foi poss\u00edvel excluir essa resposta do FAQ!');
                window.location = '../home.php?url=buscaRespostaFaq';
                </script>";
        }
    }

}
?>