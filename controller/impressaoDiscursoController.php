<?php

@include_once 'model/impressaoDiscursoClass.php';
@include_once '../model/impressaoDiscursoClass.php';

class ImpressaoDiscursoController {
    
    private $impressaoDiscurso;
    
    public function __construct() {
        $this->impressaoDiscurso = new ImpressaoDiscurso();
    }
    
    public function cadastroDiscursoImpressao($post=true) {
        if($post) {
            $this->impressaoDiscurso->setMotivoDaImpressao($_POST['motivoDaImpressao']);
            $this->impressaoDiscurso->setFkidImpressao($_POST['fk_idImpressao']);
            $this->impressaoDiscurso->setFkIdDiscurso($_POST['fk_idDiscurso']);
        }
        
        return $this->impressaoDiscurso->cadastraImpressaoDiscurso();
    }
    
    public function setMotivoDiscursoImpressao($motivoImpressaoDiscurso) {
        $this->impressaoDiscurso->setMotivoDaImpressao($motivoImpressaoDiscurso);
    }
    
    public function setFkIdImpressao($fk_idImpressao) {
        $this->impressaoDiscurso->setFkidImpressao($fk_idImpressao);
    }
    
    public function setFkIdDiscurso($fk_idDiscurso) {
        $this->impressaoDiscurso->setFkIdDiscurso($fk_idDiscurso);
    }
    
    public function getMotivoDiscursoImpressao() {
        return $this->impressaoDiscurso->getMotivoDaImpressao();
    }
    
    public function getFkIdDiscurso() {
        return $this->impressaoDiscurso->getFkIdDiscurso();
    }
    
    public function setQtdeImpressao($qtdeImpressao) {
        $this->impressaoDiscurso->setQtdeImpressaoDiscurso($qtdeImpressao);
    }
    
    public function listarImpressaoDiscurso() {
        return $this->impressaoDiscurso->listaImpressaoDiscurso();
    }
    
    public function getIdQtdeImpressao($id) {
        $resultado = $this->impressaoDiscurso->getIdQtdeImpressao($id);
        return count($resultado) ? $resultado : false;
    } 
    
    public function inseriQtdeImpressao($id) {
        return $this->impressaoDiscurso->inseriQtdeImpressao($id);
    }
    
    public function buscarIdImpressao($id) {
        $this->impressaoDiscurso->setFkidImpressao($id);
        
        $resultado = $this->impressaoDiscurso->buscaIdImpressao();
        return count($resultado) ? $resultado : false;
    }
    
    public function buscarIdDiscurso($id) {
        $this->impressaoDiscurso->setFkIdDiscurso($id);
        
        $resultado = $this->impressaoDiscurso->buscaIdDiscurso();
        return count($resultado) ? $resultado : false;
    }
    
    public function excluirImpressaoDiscurso($id) {
        $this->impressaoDiscurso->setFkIdDiscurso($id);
        $resultado = $this->impressaoDiscurso->excluiImpressaoDiscurso();
        
        return count($resultado) ? $resultado : false;
    }
}

