<?php

@include_once("../model/imovelClass.php");
@include_once("model/imovelClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/organismoClass.php");
@include_once("../model/organismoClass.php");

@include_once ("../model/criaSessaoClass.php");

@include_once ("../lib/functions.php");

@include_once("ticketController.php");

class imovelController {

    private $imovel;
    private $sessao;

    public function __construct() {

        $this->imovel           = new imovel();
        $this->sessao 		    = new criaSessao();
        $this->usuario		    = new Usuario();
        $this->organismo		= new organismo();
        $this->ticket		    = new TicketController();
    }

    public function cadastroImovel() {
		
        $this->imovel->setEnderecoImovel(addslashes($_POST["enderecoImovel"]));
        $this->imovel->setNumeroImovel(addslashes($_POST["numeroImovel"]));
        $this->imovel->setComplementoImovel(addslashes($_POST['complementoImovel']));
        $this->imovel->setBairroImovel(addslashes($_POST["bairroImovel"]));
        $this->imovel->setFk_idCidade(addslashes($_POST["cidadeImovel"]));
        $this->imovel->setFk_idEstado(addslashes($_POST["estadoImovel"]));
        $this->imovel->setFkIdPais(addslashes($_POST["paisImovel"]));
        $this->imovel->setCepImovel(addslashes($_POST['cepImovel']));
        $this->imovel->setDataFundacaoImovel(addslashes(substr($_POST["dataFundacaoImovel"],6,4)."-".substr($_POST["dataFundacaoImovel"],3,2)."-".substr($_POST["dataFundacaoImovel"],0,2)));
        $this->imovel->setFk_seqCadastAtualizadoPor(addslashes($_POST["fk_seqCadastAtualizadoPor"]));
        $this->imovel->setFk_idOrganismoAfiliado(addslashes($_POST["fk_idOrganismoAfiliado"]));

        $this->imovel->setPropriedadeTipoImovel(addslashes($_POST["propriedadeTipoImovel"]));
        $this->imovel->setPropriedadeEmNomeImovel(addslashes($_POST["propriedadeEmNomeImovel"]));
        $this->imovel->setPropriedadeStatusImovel(addslashes($_POST["propriedadeStatusImovel"]));
        $this->imovel->setPropriedadeEscrituraDataImovel(addslashes(substr($_POST["propriedadeEscrituraDataImovel"],6,4)."-".substr($_POST["propriedadeEscrituraDataImovel"],3,2)."-".substr($_POST["propriedadeEscrituraDataImovel"],0,2)));
        $this->imovel->setRegistroImovelNumeroImovel(addslashes($_POST["registroImovelNumeroImovel"]));
        $this->imovel->setRegistroImovelDataImovel(addslashes(substr($_POST["registroImovelDataImovel"],6,4)."-".substr($_POST["registroImovelDataImovel"],3,2)."-".substr($_POST["registroImovelDataImovel"],0,2)));
        $this->imovel->setAvaliacaoImovel(addslashes($_POST["avaliacaoImovel"]));
        $this->imovel->setAvaliacaoValorImovel(addslashes($_POST["avaliacaoValorImovel"]));
        $this->imovel->setAvaliacaoNomeImovel(addslashes($_POST["avaliacaoNomeImovel"]));
        $this->imovel->setAvaliacaoCreciImovel(addslashes($_POST["avaliacaoCreciImovel"]));
        $this->imovel->setAvaliacaoDataImovel(addslashes(substr($_POST["avaliacaoDataImovel"],6,4)."-".substr($_POST["avaliacaoDataImovel"],3,2)."-".substr($_POST["avaliacaoDataImovel"],0,2)));

        $this->imovel->setAreaTotalImovel(addslashes($_POST["areaTotalImovel"]));
        $this->imovel->setAreaConstruidaImovel(addslashes($_POST["areaConstruidaImovel"]));
        $this->imovel->setPropriedadeEscrituraImovel(addslashes($_POST["propriedadeEscrituraImovel"]));
        $this->imovel->setRegistroImovel(addslashes($_POST["registroImovel"]));
        $this->imovel->setRegistroNomeImovel(addslashes($_POST["registroNomeImovel"]));
        $this->imovel->setValorVenalImovel(addslashes($_POST["valorVenalImovel"]));
        $this->imovel->setValorAproximadoImovel(addslashes($_POST["valorAproximadoImovel"]));
        $this->imovel->setLocadorImovel(addslashes($_POST["locadorImovel"]));
        $this->imovel->setLocatarioImovel(addslashes($_POST["locatarioImovel"]));
        $this->imovel->setAluguelContratoInicioImovel(addslashes(substr($_POST["aluguelContratoInicioImovel"],6,4)."-".substr($_POST["aluguelContratoInicioImovel"],3,2)."-".substr($_POST["aluguelContratoInicioImovel"],0,2)));
        $this->imovel->setAluguelContratoFimImovel(addslashes(substr($_POST["aluguelContratoFimImovel"],6,4)."-".substr($_POST["aluguelContratoFimImovel"],3,2)."-".substr($_POST["aluguelContratoFimImovel"],0,2)));
        $this->imovel->setAluguelValorImovel(addslashes($_POST["aluguelValorImovel"]));
        $this->imovel->setFk_idTipoEscrituraImovel(addslashes($_POST["fk_idTipoEscrituraImovel"]));

        $this->imovel->setDescricaoImovel(addslashes($_POST["descricaoImovel"]));
        

        if ($this->imovel->cadastroImovel()) {

            /**
             * Notificar GLP que a imóvel foi cadastrada
             */

            $idOrga = $_POST["fk_idOrganismoAfiliado"];

            date_default_timezone_set('America/Sao_Paulo');
            $tituloNotificacao="Novo Imovel Cadastrado!";
            $tipoNotificacao=6;//Entregue
            $remetenteNotificacao=addslashes($_POST["fk_seqCadastAtualizadoPor"]);

            $retornoUsuario = $this->usuario->buscaUsuario($remetenteNotificacao);
            if($retornoUsuario){
                foreach($retornoUsuario as $vetorUsuario){
                    $nomeUsuario = ucwords(mb_strtolower($vetorUsuario['nomeUsuario'],'UTF-8'));
                }
            }
            $dadosOrg = $this->organismo->buscaIdOrganismo($idOrga);
            if($dadosOrg){
                foreach($dadosOrg as $vetorOrg){
                    $classificacaoOrganismoAfiliado         = $vetorOrg['classificacaoOrganismoAfiliado'];
                    $tipoOrganismoAfiliado                  = $vetorOrg['tipoOrganismoAfiliado'];
                    $nomeOrganismoAfiliado                  = $vetorOrg['nomeOrganismoAfiliado'];
                    $siglaOrganismoAfiliado                 = $vetorOrg['siglaOrganismoAfiliado'];
                }
            }
            $nomeOa = organismoAfiliadoNomeCompleto($classificacaoOrganismoAfiliado,$tipoOrganismoAfiliado,$nomeOrganismoAfiliado,$siglaOrganismoAfiliado);

            $mensagemNotificacao="Imovel cadastrado pelo Organismo ".utf8_decode($nomeOa).", ".date("d/m/Y")." as ".date("H:i:s"). " por ".utf8_decode($nomeUsuario);

            echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
            echo "<script>var oficiaisMarcados = [";
            //Selecionar todos os usuários com departamento 2 e 3
            $resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
            $i=0;
            if($resultadoUsuarios)
            {
                foreach ($resultadoUsuarios as $vetor)
                {
                    if($i==0)
                    {
                        echo "\"".$vetor['idUsuario']."\"";
                    }else{
                        echo ",\"".$vetor['idUsuario']."\"";
                    }
                    $i++;
                }
            }

            echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
            //echo "enviada notificacao ata de posse";exit();

            $idFuncionalidade       = 1;
            $remetente              = addslashes($_POST["fk_seqCadastAtualizadoPor"]);
            $descricao              = "Novo Imovel Cadastrado!";
            $idOrg                  = $_POST["fk_idOrganismoAfiliado"];
            $identificador          = $this->imovel->selecionaUltimoId();
            $this->ticket->cadastroTicketAltomatico($idFuncionalidade,$descricao,$remetente,$idOrg,$identificador,1);

            if($_POST["nroCampo"] >= 1){
                for($z=1; $z<=$_POST["nroCampo"];$z++){
                    if(isset($_POST["matricula".$z]) && ($_POST["matricula".$z]!="")){
                        //echo "matricula ".$z.": ".$_POST["matricula".$z]."<br>";
                        if(!$this->imovel->verificaJaExisteImovelMatricula($identificador,$_POST["matricula".$z])){
                            $this->imovel->cadastroImovelMatricula($identificador,$_POST["matricula".$z]);
                        }
                    }
                }
            }
            //echo "<pre>";print_r($_POST);echo "</pre>";
            
            echo "<script type='text/javascript'>window.location = '../painelDeControle.php?corpo=buscaImovel&salvo=1';</script>";

        } else {

            echo "<script type='text/javascript'>
                    alert('Não foi possível cadastrar esse Imóvel!');
                    window.location = '../painelDeControle.php?corpo=cadastroImovel';
		  		</script>";

        }
    }

    public function listaImovel($oa='', $statusImovel='') {
    	
        if (!isset($_POST['enderecoImovel'])) {
            $resultado = $this->imovel->listaImovel($oa, $statusImovel);
        } else {
        	/*
            $this->imovel->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->imovel->buscaNomeOrganismo();
        	*/
        }

        return $resultado;
    }
    
	public function buscaImovel($idImovel) {

        $resultado = $this->imovel->buscaIdImovel($idImovel);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);
		        $this->imovel->setEnderecoImovel($vetor["enderecoImovel"]);
		        $this->imovel->setNumeroImovel($vetor["numeroImovel"]);
		        $this->imovel->setComplementoImovel($vetor['complementoImovel']);
		        $this->imovel->setBairroImovel($vetor["bairroImovel"]);
		        $this->imovel->setFk_idCidade($vetor["fk_idCidade"]);
		        $this->imovel->setFk_idEstado($vetor["fk_idEstado"]);
                $this->imovel->setFkIdPais($vetor["fk_idPais"]);
		        $this->imovel->setCepImovel($vetor['cepImovel']);
		        $this->imovel->setDataFundacaoImovel(substr($vetor['dataFundacaoImovel'],8,2)."/".substr($vetor['dataFundacaoImovel'],5,2)."/".substr($vetor['dataFundacaoImovel'],0,4));
		        $this->imovel->setStatusImovel($vetor["statusImovel"]);
                $this->imovel->setFk_seqCadastAtualizadoPor($vetor["fk_seqCadastAtualizadoPor"]);
		        $this->imovel->setFk_idOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);


                $this->imovel->setPropriedadeTipoImovel($vetor["propriedadeTipoImovel"]);
                $this->imovel->setPropriedadeEmNomeImovel($vetor["propriedadeEmNomeImovel"]);
                $this->imovel->setPropriedadeStatusImovel($vetor["propriedadeStatusImovel"]);
                $this->imovel->setPropriedadeEscrituraDataImovel(substr($vetor['propriedadeEscrituraDataImovel'],8,2)."/".substr($vetor['propriedadeEscrituraDataImovel'],5,2)."/".substr($vetor['propriedadeEscrituraDataImovel'],0,4));
                $this->imovel->setRegistroImovelNumeroImovel($vetor["registroImovelNumeroImovel"]);
                $this->imovel->setRegistroImovelDataImovel(substr($vetor['registroImovelDataImovel'],8,2)."/".substr($vetor['registroImovelDataImovel'],5,2)."/".substr($vetor['registroImovelDataImovel'],0,4));
                $this->imovel->setAvaliacaoImovel($vetor["avaliacaoImovel"]);
                $this->imovel->setAvaliacaoValorImovel($vetor["avaliacaoValorImovel"]);
                $this->imovel->setAvaliacaoNomeImovel($vetor["avaliacaoNomeImovel"]);
                $this->imovel->setAvaliacaoCreciImovel($vetor["avaliacaoCreciImovel"]);
                $this->imovel->setAvaliacaoDataImovel(substr($vetor['avaliacaoDataImovel'],8,2)."/".substr($vetor['avaliacaoDataImovel'],5,2)."/".substr($vetor['avaliacaoDataImovel'],0,4));

                $this->imovel->setAreaTotalImovel($vetor["areaTotalImovel"]);
                $this->imovel->setAreaConstruidaImovel($vetor["areaConstruidaImovel"]);
                $this->imovel->setPropriedadeEscrituraImovel($vetor["propriedadeEscrituraImovel"]);
                $this->imovel->setRegistroImovel($vetor["registroImovel"]);
                $this->imovel->setRegistroNomeImovel($vetor["registroNomeImovel"]);
                $this->imovel->setValorVenalImovel($vetor["valorVenalImovel"]);
                $this->imovel->setValorAproximadoImovel($vetor["valorAproximadoImovel"]);
                $this->imovel->setLocadorImovel($vetor["locadorImovel"]);
                $this->imovel->setLocatarioImovel($vetor["locatarioImovel"]);
                $this->imovel->setAluguelContratoInicioImovel(substr($vetor["aluguelContratoInicioImovel"],8,2)."/".substr($vetor["aluguelContratoInicioImovel"],5,2)."/".substr($vetor["aluguelContratoInicioImovel"],0,4));
                $this->imovel->setAluguelContratoFimImovel(substr($vetor["aluguelContratoFimImovel"],8,2)."/".substr($vetor["aluguelContratoFimImovel"],5,2)."/".substr($vetor["aluguelContratoFimImovel"],0,4));
                $this->imovel->setAluguelValorImovel($vetor["aluguelValorImovel"]);
                $this->imovel->setFk_idTipoEscrituraImovel($vetor["fk_idTipoEscrituraImovel"]);

                $this->imovel->setDescricaoImovel($vetor["descricaoImovel"]);
            }

            return $this->imovel;
        } else {
            return false;
        }
    }

    public function alteraImovel() {

        $this->imovel->setEnderecoImovel(addslashes($_POST["enderecoImovel"]));
        $this->imovel->setNumeroImovel(addslashes($_POST["numeroImovel"]));
        $this->imovel->setComplementoImovel(addslashes($_POST['complementoImovel']));
        $this->imovel->setBairroImovel(addslashes($_POST["bairroImovel"]));
        $this->imovel->setFk_idCidade(addslashes($_POST["cidadeImovel"]));
        $this->imovel->setFk_idEstado(addslashes($_POST["estadoImovel"]));
        $this->imovel->setFkIdPais(addslashes($_POST["paisImovel"]));
        $this->imovel->setCepImovel(addslashes($_POST['cepImovel']));
        $this->imovel->setDataFundacaoImovel(addslashes(substr($_POST["dataFundacaoImovel"],6,4)."-".substr($_POST["dataFundacaoImovel"],3,2)."-".substr($_POST["dataFundacaoImovel"],0,2)));
        $this->imovel->setFk_seqCadastAtualizadoPor(addslashes($_POST["fk_seqCadastAtualizadoPor"]));
        $this->imovel->setFk_idOrganismoAfiliado(addslashes($_POST["fk_idOrganismoAfiliado"]));

        $this->imovel->setPropriedadeTipoImovel(addslashes($_POST["propriedadeTipoImovel"]));
        $this->imovel->setPropriedadeEmNomeImovel(addslashes($_POST["propriedadeEmNomeImovel"]));
        $this->imovel->setPropriedadeStatusImovel(addslashes($_POST["propriedadeStatusImovel"]));
        $this->imovel->setPropriedadeEscrituraDataImovel(addslashes(substr($_POST["propriedadeEscrituraDataImovel"],6,4)."-".substr($_POST["propriedadeEscrituraDataImovel"],3,2)."-".substr($_POST["propriedadeEscrituraDataImovel"],0,2)));
        $this->imovel->setRegistroImovelNumeroImovel(addslashes($_POST["registroImovelNumeroImovel"]));
        $this->imovel->setRegistroImovelDataImovel(addslashes(substr($_POST["registroImovelDataImovel"],6,4)."-".substr($_POST["registroImovelDataImovel"],3,2)."-".substr($_POST["registroImovelDataImovel"],0,2)));
        $this->imovel->setAvaliacaoImovel(addslashes($_POST["avaliacaoImovel"]));
        $this->imovel->setAvaliacaoValorImovel(addslashes($_POST["avaliacaoValorImovel"]));
        $this->imovel->setAvaliacaoNomeImovel(addslashes($_POST["avaliacaoNomeImovel"]));
        $this->imovel->setAvaliacaoCreciImovel(addslashes($_POST["avaliacaoCreciImovel"]));
        $this->imovel->setAvaliacaoDataImovel(addslashes(substr($_POST["avaliacaoDataImovel"],6,4)."-".substr($_POST["avaliacaoDataImovel"],3,2)."-".substr($_POST["avaliacaoDataImovel"],0,2)));

        $this->imovel->setAreaTotalImovel(addslashes($_POST["areaTotalImovel"]));
        $this->imovel->setAreaConstruidaImovel(addslashes($_POST["areaConstruidaImovel"]));
        $this->imovel->setPropriedadeEscrituraImovel(addslashes($_POST["propriedadeEscrituraImovel"]));
        $this->imovel->setRegistroImovel(addslashes($_POST["registroImovel"]));
        $this->imovel->setRegistroNomeImovel(addslashes($_POST["registroNomeImovel"]));
        $this->imovel->setValorVenalImovel(addslashes($_POST["valorVenalImovel"]));
        $this->imovel->setValorAproximadoImovel(addslashes($_POST["valorAproximadoImovel"]));
        $this->imovel->setLocadorImovel(addslashes($_POST["locadorImovel"]));
        $this->imovel->setLocatarioImovel(addslashes($_POST["locatarioImovel"]));
        $this->imovel->setAluguelContratoInicioImovel(addslashes(substr($_POST["aluguelContratoInicioImovel"],6,4)."-".substr($_POST["aluguelContratoInicioImovel"],3,2)."-".substr($_POST["aluguelContratoInicioImovel"],0,2)));
        $this->imovel->setAluguelContratoFimImovel(addslashes(substr($_POST["aluguelContratoFimImovel"],6,4)."-".substr($_POST["aluguelContratoFimImovel"],3,2)."-".substr($_POST["aluguelContratoFimImovel"],0,2)));
        $this->imovel->setAluguelValorImovel(addslashes($_POST["aluguelValorImovel"]));
        $this->imovel->setFk_idTipoEscrituraImovel(addslashes($_POST["fk_idTipoEscrituraImovel"]));

        $this->imovel->setDescricaoImovel(addslashes($_POST["descricaoImovel"]));
        //echo "<pre>";print_r($_POST);

        if ($this->imovel->alteraImovel($_POST['idAlteraImovel'])) {

            $idFuncionalidade       = 1;
            $remetente              = $_POST["fk_seqCadastAtualizadoPor"];
            $descricao              = "Houve alterações no Imovel!";
            $idOrg                  = $_POST["fk_idOrganismoAfiliado"];
            $identificador          = $_POST['idAlteraImovel'];
            $this->ticket->cadastroTicketAltomatico($idFuncionalidade,$descricao,$remetente,$idOrg,$identificador,2);

            echo
            "<script type='text/javascript'>
            window.location = '../painelDeControle.php?corpo=buscaImovel&salvo=1';
            </script>";

        } else {

            echo
            "<script type='text/javascript'>
            alert ('Não foi possível alterar o Imóvel!');
            window.location = '../painelDeControle.php?corpo=alteraImovel&idImovel=" . $_POST['idAlteraImovel'] . "';
            </script>";

        }
    }

    public function listaImovelMatricula($idImovel=null) {
        
        if (!isset($_POST['enderecoImovel'])) {
            $resultado = $this->imovel->listaImovelMatricula($idImovel);
        } else {
            /*
            $this->imovel->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->imovel->buscaNomeOrganismo();
            */
        }

        return $resultado;
    }
}

?>