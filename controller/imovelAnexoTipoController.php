<?php

@include_once("../model/imovelAnexoTipoClass.php");
@include_once("model/imovelAnexoTipoClass.php");

class imovelAnexoTipoController {

    private $imovelAnexoTipo;

    public function __construct() {

        $this->imovelAnexoTipo = new ImovelAnexoTipo();
    }

    public function cadastroTipoAtividadeEstatuto() {
		
        $this->imovelAnexoTipo->setNomeImovelAnexoTipo($_POST["nomeImovelAnexoTipo"]);
        $this->imovelAnexoTipo->setDescricaoImovelAnexoTipo($_POST["descricaoImovelAnexoTipo"]);
        $this->imovelAnexoTipo->setAnualImovelAnexoTipo($_POST["anualImovelAnexoTipo"]);
        $this->imovelAnexoTipo->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
        
        if ($this->imovelAnexoTipo->cadastroImovelAnexoTipo()) {

            echo "<script type='text/javascript'>
                    alert('Tipo de Anexo cadastrado com sucesso!');
		    		window.location = '../painelDeControle.php?corpo=buscaImovelAnexoTipo';
		  		</script>";

        } else {

            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar esse Tipo de Anexo!');
                    window.location = '../painelDeControle.php?corpo=cadastroImovelAnexoTipo';
		  		</script>";

        }
    }
    
	public function alteraImovelAnexoTipo() {
        
        $this->imovelAnexoTipo->setNomeImovelAnexoTipo($_POST["nomeImovelAnexoTipo"]);
        $this->imovelAnexoTipo->setDescricaoImovelAnexoTipo($_POST["descricaoImovelAnexoTipo"]);
        $this->imovelAnexoTipo->setAnualImovelAnexoTipo($_POST["anualImovelAnexoTipo"]);
        $this->imovelAnexoTipo->setFkSeqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);

        if ($this->imovelAnexoTipo->alteraImovelAnexoTipo($_POST['idImovelAnexoTipo'])) {

            echo
            "<script type='text/javascript'>
			alert ('Tipo de Anexo alterada com sucesso!');
			window.location = '../painelDeControle.php?corpo=buscaImovelAnexoTipo';
			</script>";

        } else {

            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar o Tipo de Anexo!');
			window.location = '../painelDeControle.php?corpo=alteraImovelAnexoTipo&idImovelAnexoTipo=" . $_POST['idImovelAnexoTipo'] . "';
			</script>";

        }
    }

    public function listaImovelAnexoTipo($anual=null,$status=null) {

        if (!isset($_POST['idImovelAnexoTipo'])) {
            $resultado = $this->imovelAnexoTipo->listaImovelAnexoTipo($anual,$status);
        } else {
            /*
            $this->atividadeTipo->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeTipo->buscaNomeOrganismo();
            */
        }

        return $resultado;
    }

    public function buscaImovelAnexoTipo($idImovelAnexoTipo) {

        $resultado = $this->imovelAnexoTipo->buscaIdImovelAnexoTipo($idImovelAnexoTipo);

        if ($resultado) {

            foreach ($resultado as $vetor) {
                //echo "<pre>";print_r($vetor);
                $this->imovelAnexoTipo->setNomeImovelAnexoTipo($vetor["nomeImovelAnexoTipo"]);
                $this->imovelAnexoTipo->setDescricaoImovelAnexoTipo($vetor["descricaoImovelAnexoTipo"]);
                $this->imovelAnexoTipo->setAnualImovelAnexoTipo($vetor["anualImovelAnexoTipo"]);
                $this->imovelAnexoTipo->setStatusImovelAnexoTipo($vetor["statusImovelAnexoTipo"]);
                $this->imovelAnexoTipo->setCadastroDataImovelAnexoTipo(substr($vetor['cadastroDataImovelAnexoTipo'],8,2)."/".substr($vetor['cadastroDataImovelAnexoTipo'],5,2)."/".substr($vetor['cadastroDataImovelAnexoTipo'],0,4));
                $this->imovelAnexoTipo->setAtualizadoDataImovelAnexoTipo(substr($vetor['atualizadoDataImovelAnexoTipo'],8,2)."/".substr($vetor['atualizadoDataImovelAnexoTipo'],5,2)."/".substr($vetor['atualizadoDataImovelAnexoTipo'],0,4));
                $this->imovelAnexoTipo->setFkSeqCadastAtualizadoPor($vetor["fk_seqCadastAtualizadoPor"]);
            }
            return $this->imovelAnexoTipo;
        } else {
            return false;
        }
    }

    public function criarComboBox($id = 0) {
        $resultado = $this->imovelAnexoTipo->listaImovelAnexoTipo();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idTipoAtividadeEstatuto"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idTipoAtividadeEstatuto'] . '" ' . $selecionado . '>' . $vetor["nomeTipoAtividadeEstatuto"] .'</option>';
            }
        }
    }

}

?>