<?php
@include_once("model/subMenuClass.php");

@include_once("../model/subMenuClass.php");

class subMenuController {

    private $subMenu;

    public function __construct() {

        $this->subMenu = new subMenu();
    }

    public function cadastraSubMenu() {

        $this->subMenu->setNomeSubMenu($_POST["nomeSubMenu"]);
        $this->subMenu->setDescricaoSubMenu($_POST["descricaoSubMenu"]);
        $this->subMenu->setArquivoSubMenu($_POST["arquivoSubMenu"]);
        $this->subMenu->setPrioridadeSubMenu($_POST["prioridadeSubMenu"]);
        $this->subMenu->setIconeSubMenu($_POST["iconeSubMenu"]);
        $this->subMenu->setFkIdSecaoMenu($_POST["fk_idSecaoMenu"]);
        $this->subMenu->setMsgManutencao($_POST["msgManutencao"]);

        if ($this->subMenu->cadastraSubMenu()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaSubMenu&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar esse Submenu!');
		  		window.location = '../painelDeControle.php?corpo=buscaSubMenu';
		  </script>";
        }
    }

 	public function criarComboBox($id = 0, $param = null) {
 	
 		if($param == null)
 		{
 			
        	$resultado = $this->subMenu->listaSubMenu();
 		}else{
 			
 			$resultado = $this->subMenu->listaSubMenu($param);
 		}

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idSubMenu"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idSubMenu'] . '" ' . $selecionado . '>' . $vetor["nomeSubMenu"] . '</option>';
            }
        }
    }

    public function listaSubMenu() {
        
        $resultado = $this->subMenu->listaSubMenu();
        return $resultado;
    }

    public function buscaSubMenu() {

        $resultado = $this->subMenu->buscaIdSubMenu();

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->subMenu->setIdSubMenu($vetor['idSubMenu']);
            	$this->subMenu->setNomeSubMenu($vetor['nomeSubMenu']);
            	$this->subMenu->setDescricaoSubMenu($vetor['descricaoSubMenu']);
                $this->subMenu->setArquivoSubMenu($vetor['arquivoSubMenu']);
                $this->subMenu->setPrioridadeSubMenu($vetor['prioridadeSubMenu']);
                $this->subMenu->setIconeSubMenu($vetor['iconeSubMenu']);
                $this->subMenu->setFkIdSecaoMenu($vetor['fk_idSecaoMenu']);
                $this->subMenu->setMsgManutencao($vetor['msgManutencao']);
            }

            return $this->subMenu;
        } else {
            return false;
        }
    }

    public function alteraSubMenu() {

        $this->subMenu->setNomeSubMenu($_POST["nomeSubMenu"]);
        $this->subMenu->setDescricaoSubMenu($_POST["descricaoSubMenu"]);
        $this->subMenu->setArquivoSubMenu($_POST["arquivoSubMenu"]);
        $this->subMenu->setPrioridadeSubMenu($_POST['prioridadeSubMenu']);
        $this->subMenu->setIconeSubMenu($_POST['iconeSubMenu']);
        $this->subMenu->setFkIdSecaoMenu($_POST['fk_idSecaoMenu']);
        $this->subMenu->setMsgManutencao($_POST["msgManutencao"]);
        

        if ($this->subMenu->alteraSubMenu($_POST['idSubMenu'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaSubMenu&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N�o foi poss�vel alterar este Submenu!');
			window.location = '../painelDeControle.php?corpo=buscaSubMenu';
			</script>";
        }
    }

	public function removeSubMenu($id) {
        $resultado = $this->subMenu->removeSubMenu($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
            	alert ('Submenu excluido com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaSubMenu&salvo=1';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir este Submenu!');
				window.location = '../painelDeControle.php?corpo=buscaSubMenu';
				</script>";
        }
    }

}
?>