<?php

@include_once("../model/imovelControleClass.php");
@include_once("model/imovelControleClass.php");

class imovelControleController {

    private $imovelControle;

    public function __construct() {

        $this->imovelControle = new imovelControle();
    }

    public function cadastroImovelControle() {
		
        $this->imovelControle->setReferenteAnoImovelControle($_POST["referenteAnoImovelControle"]);

        $this->imovelControle->setPropriedadeIptuImovelControle($_POST["propriedadeIptuImovelControle"]);
        $this->imovelControle->setPropriedadeIptuObsImovelControle($_POST["propriedadeIptuObsImovelControle"]);

        $this->imovelControle->setConstrucaoImovelControle($_POST["construcaoImovelControle"]);
        //$this->imovelControle->setConstrucaoAlvaraImovelControle($_POST["construcaoAlvaraImovelControle"]);
        $this->imovelControle->setConstrucaoRestricoesImovelControle($_POST["construcaoRestricoesImovelControle"]);
        $this->imovelControle->setConstrucaoRestricoesAnotacoesImovelControle($_POST["construcaoRestricoesAnotacoesImovelControle"]);
        $this->imovelControle->setConstrucaoConcluidaImovelControle($_POST["construcaoConcluidaImovelControle"]);
        $this->imovelControle->setConstrucaoTerminoDataImovelControle(substr($_POST["construcaoTerminoDataImovelControle"],6,4)."-".substr($_POST["construcaoTerminoDataImovelControle"],3,2)."-".substr($_POST["construcaoTerminoDataImovelControle"],0,2));
        //$this->imovelControle->setConstrucaoInssImovelControle($_POST["construcaoInssImovelControle"]);

        $this->imovelControle->setManutencaoImovelControle($_POST["manutencaoImovelControle"]);
        $this->imovelControle->setManutencaoPinturaImovelControle($_POST["manutencaoPinturaImovelControle"]);
        $this->imovelControle->setManutencaoPinturaTipoImovelControle($_POST["manutencaoPinturaTipoImovelControle"]);
        $this->imovelControle->setManutencaoHidraulicaImovelControle($_POST["manutencaoHidraulicaImovelControle"]);
        $this->imovelControle->setManutencaoHidraulicaTipoImovelControle($_POST["manutencaoHidraulicaTipoImovelControle"]);
        $this->imovelControle->setManutencaoEletricaImovelControle($_POST["manutencaoEletricaImovelControle"]);
        $this->imovelControle->setManutencaoEletricaTipoImovelControle($_POST["manutencaoEletricaTipoImovelControle"]);
        $this->imovelControle->setAnotacoesImovelControle($_POST["anotacoesImovelControle"]);
        $this->imovelControle->setFk_seqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
        $this->imovelControle->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->imovelControle->setFk_idImovel($_POST["fk_idImovel"]);
		//echo "<pre>";print_r($_POST);
		
        //echo '<pre>';
        /*
    	if ($_FILES['propriedadeIptuAnexoImovelControle']['name'] != "") {
        	$caminho = "img/imovel/controle/iptu/" . basename($_POST["seqCadast"].".iptu.".$_FILES['propriedadeIptuAnexoImovelControle']['name']);
			$this->imovelControle->setPropriedadeIptuAnexoImovelControle($caminho);
			$uploaddir = '/home/tom/public_html/treinamento/img/imovel/controle/iptu/';
			$uploadfile = $uploaddir . basename($_POST["seqCadast"].".iptu.".utf8_decode($_FILES['propriedadeIptuAnexoImovelControle']['name']));
			echo(move_uploaded_file($_FILES['propriedadeIptuAnexoImovelControle']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('IPTU enviado com sucesso!');</script>"  : "<script type='text/javascript'>alert('Erro ao enviar a imagem!');</script>");
		}
    	if ($_FILES['construcaoAlvaraAnexoImovelControle']['name'] != "") {
        	$caminho = "img/imovel/controle/alvara/" . basename($_POST["seqCadast"].".alvara.".$_FILES['construcaoAlvaraAnexoImovelControle']['name']);
			$this->imovelControle->setConstrucaoAlvaraAnexoImovelControle($caminho);
			$uploaddir = '/home/tom/public_html/treinamento/img/imovel/controle/alvara/';
			$uploadfile = $uploaddir . basename($_POST["seqCadast"].".alvara.".utf8_decode($_FILES['construcaoAlvaraAnexoImovelControle']['name']));
			echo(move_uploaded_file($_FILES['construcaoAlvaraAnexoImovelControle']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Alvará enviado com sucesso!');</script>"  : "<script type='text/javascript'>alert('Erro ao enviar a imagem!');</script>");
		}
    	if ($_FILES['construcaoInssAnexoImovelControle']['name'] != "") {
        	$caminho = "img/imovel/controle/inss/" . basename($_POST["seqCadast"].".inss.".$_FILES['construcaoInssAnexoImovelControle']['name']);
			$this->imovelControle->setConstrucaoInssAnexoImovelControle($caminho);
			$uploaddir = '/home/tom/public_html/treinamento/img/imovel/controle/inss/';
			$uploadfile = $uploaddir . basename($_POST["seqCadast"].".inss.".utf8_decode($_FILES['construcaoInssAnexoImovelControle']['name']));
			echo(move_uploaded_file($_FILES['construcaoInssAnexoImovelControle']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('INSS enviado com sucesso!');</script>"  : "<script type='text/javascript'>alert('Erro ao enviar a imagem!');</script>");
		}
        */
    	//echo 'Debug INFO: ';
		//print_r($_FILES);
		//print "</pre>";
        
		
		
		if(!$this->imovelControle->buscaAno($_POST["fk_idImovel"],$_POST["referenteAnoImovelControle"])){
	        if ($this->imovelControle->cadastroImovelControle()) {

	            echo "<script type='text/javascript'>
	                    alert('Controle de Documentos cadastrado com sucesso!');
			    		window.location = '../painelDeControle.php?corpo=buscaImovelControle&idOrganismoAfiliado=" . $_POST["fk_idOrganismoAfiliado"] . "&referenteAnoImovelControle=" . $_POST["referenteAnoImovelControle"] . "';
			  </script>";

	        } else {

	            echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar esse Controle de Documentos!');
	                    window.location = '../painelDeControle.php?corpo=cadastroImovelControle';
			  </script>";

	        }
		} else {

			echo "<script type='text/javascript'>
	                    alert('O Ano " . $_POST["referenteAnoImovelControle"] . " já tem um Controle de Documentos Cadastrados!');
	                    window.location = '../painelDeControle.php?corpo=cadastroImovelControle';
			  </script>";

		}
    }

    public function listaImovelControle($idOrganismoAfiliado='',$data='') {
    	
        if (!isset($_POST['referenteAnoImovelControle'])) {
            $resultado = $this->imovelControle->listaImovelControle($idOrganismoAfiliado,$data);
        } else {
        	/*
            $this->imovelControle->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->imovelControle->buscaNomeOrganismo();
        	*/
        }

        return $resultado;
    }

    public function listaIdImovel() {
        $retorno = $this->imovelControle->listaImovel();
        $listaImovel = "";

        foreach ($retorno as $dados) {
            $listaImovel[] = $dados['idImovel'];
        }

        if ($listaImovel) {
            return $listaImovel;
        } else {
            return false;
        }
    }
    
	public function buscaImovelControle($idImovelControle) {

        $resultado = $this->imovelControle->buscaIdImovelControle($idImovelControle);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);		        
		        $this->imovelControle->setReferenteAnoImovelControle($vetor["referenteAnoImovelControle"]);
                $this->imovelControle->setPropriedadeIptuImovelControle($vetor["propriedadeIptuImovelControle"]);
                $this->imovelControle->setPropriedadeIptuObsImovelControle($vetor["propriedadeIptuObsImovelControle"]);
                //$this->imovelControle->setPropriedadeIptuAnexoImovelControle($vetor["propriedadeIptuAnexoImovelControle"]);

                $this->imovelControle->setConstrucaoImovelControle($vetor["construcaoImovelControle"]);
		        //$this->imovelControle->setConstrucaoAlvaraImovelControle($vetor["construcaoAlvaraImovelControle"]);
                //$this->imovelControle->setConstrucaoAlvaraAnexoImovelControle($vetor["construcaoAlvaraAnexoImovelControle"]);
		        $this->imovelControle->setConstrucaoRestricoesImovelControle($vetor["construcaoRestricoesImovelControle"]);
		        $this->imovelControle->setConstrucaoRestricoesAnotacoesImovelControle($vetor["construcaoRestricoesAnotacoesImovelControle"]);
		        $this->imovelControle->setConstrucaoConcluidaImovelControle($vetor["construcaoConcluidaImovelControle"]);
		        $this->imovelControle->setConstrucaoTerminoDataImovelControle(substr($vetor['construcaoTerminoDataImovelControle'],8,2)."/".substr($vetor['construcaoTerminoDataImovelControle'],5,2)."/".substr($vetor['construcaoTerminoDataImovelControle'],0,4));
		        //$this->imovelControle->setConstrucaoInssImovelControle($vetor["construcaoInssImovelControle"]);
		        //$this->imovelControle->setConstrucaoInssAnexoImovelControle($vetor["construcaoInssAnexoImovelControle"]);

		        $this->imovelControle->setManutencaoImovelControle($vetor["manutencaoImovelControle"]);
		        $this->imovelControle->setManutencaoPinturaImovelControle($vetor["manutencaoPinturaImovelControle"]);
		        $this->imovelControle->setManutencaoPinturaTipoImovelControle($vetor["manutencaoPinturaTipoImovelControle"]);
		        $this->imovelControle->setManutencaoHidraulicaImovelControle($vetor["manutencaoHidraulicaImovelControle"]);
		        $this->imovelControle->setManutencaoHidraulicaTipoImovelControle($vetor["manutencaoHidraulicaTipoImovelControle"]);
		        $this->imovelControle->setManutencaoEletricaImovelControle($vetor["manutencaoEletricaImovelControle"]);
		        $this->imovelControle->setManutencaoEletricaTipoImovelControle($vetor["manutencaoEletricaTipoImovelControle"]);
		        $this->imovelControle->setAnotacoesImovelControle($vetor["anotacoesImovelControle"]);
                $this->imovelControle->setFk_seqCadastAtualizadoPor($vetor["fk_seqCadastAtualizadoPor"]);
		        $this->imovelControle->setFk_idOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);
		        $this->imovelControle->setFk_idImovel($vetor["fk_idImovel"]);
            }

            return $this->imovelControle;
        } else {
            return false;
        }
    }

    public function alteraImovelControle() {

        $this->imovelControle->setReferenteAnoImovelControle($_POST["referenteAnoImovelControle"]);

        $this->imovelControle->setPropriedadeIptuImovelControle($_POST["propriedadeIptuImovelControle"]);
        $this->imovelControle->setPropriedadeIptuObsImovelControle($_POST["propriedadeIptuObsImovelControle"]);

        $this->imovelControle->setConstrucaoImovelControle($_POST["construcaoImovelControle"]);
        //$this->imovelControle->setConstrucaoAlvaraImovelControle($_POST["construcaoAlvaraImovelControle"]);
        $this->imovelControle->setConstrucaoRestricoesImovelControle($_POST["construcaoRestricoesImovelControle"]);
        $this->imovelControle->setConstrucaoRestricoesAnotacoesImovelControle($_POST["construcaoRestricoesAnotacoesImovelControle"]);
        $this->imovelControle->setConstrucaoConcluidaImovelControle($_POST["construcaoConcluidaImovelControle"]);
        $this->imovelControle->setConstrucaoTerminoDataImovelControle(substr($_POST["construcaoTerminoDataImovelControle"],6,4)."-".substr($_POST["construcaoTerminoDataImovelControle"],3,2)."-".substr($_POST["construcaoTerminoDataImovelControle"],0,2));
        //$this->imovelControle->setConstrucaoInssImovelControle($_POST["construcaoInssImovelControle"]);

        $this->imovelControle->setManutencaoImovelControle($_POST["manutencaoImovelControle"]);
        $this->imovelControle->setManutencaoPinturaImovelControle($_POST["manutencaoPinturaImovelControle"]);
        $this->imovelControle->setManutencaoPinturaTipoImovelControle($_POST["manutencaoPinturaTipoImovelControle"]);
        $this->imovelControle->setManutencaoHidraulicaImovelControle($_POST["manutencaoHidraulicaImovelControle"]);
        $this->imovelControle->setManutencaoHidraulicaTipoImovelControle($_POST["manutencaoHidraulicaTipoImovelControle"]);
        $this->imovelControle->setManutencaoEletricaImovelControle($_POST["manutencaoEletricaImovelControle"]);
        $this->imovelControle->setManutencaoEletricaTipoImovelControle($_POST["manutencaoEletricaTipoImovelControle"]);
        $this->imovelControle->setAnotacoesImovelControle($_POST["anotacoesImovelControle"]);
        $this->imovelControle->setFk_seqCadastAtualizadoPor($_POST["fk_seqCadastAtualizadoPor"]);
        $this->imovelControle->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->imovelControle->setFk_idImovel($_POST["fk_idImovel"]);
		//echo "<pre>";print_r($_POST);
		
        //echo '<pre>';
        /*
    	if ($_FILES['propriedadeIptuAnexoImovelControle']['name'] != "") {
        	$caminho = "img/imovel/controle/iptu/" . basename($_POST["seqCadast"].".iptu.".$_FILES['propriedadeIptuAnexoImovelControle']['name']);
			$this->imovelControle->setPropriedadeIptuAnexoImovelControle($caminho);
			$uploaddir = '/home/tom/public_html/treinamento/img/imovel/controle/iptu/';
			$uploadfile = $uploaddir . basename($_POST["seqCadast"].".iptu.".utf8_decode($_FILES['propriedadeIptuAnexoImovelControle']['name']));
			echo(move_uploaded_file($_FILES['propriedadeIptuAnexoImovelControle']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('IPTU enviado com sucesso!');</script>"  : "<script type='text/javascript'>alert('Erro ao enviar a imagem!');</script>");
		}
    	if ($_FILES['construcaoAlvaraAnexoImovelControle']['name'] != "") {
        	$caminho = "img/imovel/controle/alvara/" . basename($_POST["seqCadast"].".alvara.".$_FILES['construcaoAlvaraAnexoImovelControle']['name']);
			$this->imovelControle->setConstrucaoAlvaraAnexoImovelControle($caminho);
			$uploaddir = '/home/tom/public_html/treinamento/img/imovel/controle/alvara/';
			$uploadfile = $uploaddir . basename($_POST["seqCadast"].".alvara.".utf8_decode($_FILES['construcaoAlvaraAnexoImovelControle']['name']));
			echo(move_uploaded_file($_FILES['construcaoAlvaraAnexoImovelControle']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('Alvará enviado com sucesso!');</script>"  : "<script type='text/javascript'>alert('Erro ao enviar a imagem!');</script>");
		}
    	if ($_FILES['construcaoInssAnexoImovelControle']['name'] != "") {
        	$caminho = "img/imovel/controle/inss/" . basename($_POST["seqCadast"].".inss.".$_FILES['construcaoInssAnexoImovelControle']['name']);
			$this->imovelControle->setConstrucaoInssAnexoImovelControle($caminho);
			$uploaddir = '/home/tom/public_html/treinamento/img/imovel/controle/inss/';
			$uploadfile = $uploaddir . basename($_POST["seqCadast"].".inss.".utf8_decode($_FILES['construcaoInssAnexoImovelControle']['name']));
			echo(move_uploaded_file($_FILES['construcaoInssAnexoImovelControle']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>console.log('INSS enviado com sucesso!');</script>"  : "<script type='text/javascript'>alert('Erro ao enviar a imagem!');</script>");
		}
        */
		//echo 'Debug INFO: ';
		//print_r($_FILES);
        //print "</pre>";

        if ($this->imovelControle->alteraImovelControle($_POST['idImovelControle'])) {

            echo
            "<script type='text/javascript'>
			alert ('Controle de Documentos alterado com sucesso!');
			window.location = '../painelDeControle.php?corpo=buscaImovelControle&idOrganismoAfiliado=" . $_POST["fk_idOrganismoAfiliado"] . "&referenteAnoImovelControle=" . $_POST["referenteAnoImovelControle"] . "';
			</script>";

        } else {

            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar o Controle de Documentos!');
			window.location = '../painelDeControle.php?corpo=alteraImovelControle&idImovelControle=" . $_POST['idImovelControle'] . "';
			</script>";

        }
    }

    public function criarComboBox($id = 0) {
        $resultado = $this->imovelControle->listaImovel;

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idImovel"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idImovel'] . '" ' . $selecionado . '>' . $vetor["enderecoImovel"] . ' - ' . $vetor["numeroImovel"] .'</option>';
            }
        }
    }

}

?>