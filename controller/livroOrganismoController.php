<?php

@include_once("model/livroOrganismoClass.php");
@include_once("../model/livroOrganismoClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/organismoClass.php");
@include_once("../model/organismoClass.php");

@include_once ("../model/criaSessaoClass.php");

@include_once ("../lib/functions.php");

class livroOrganismoController {

	private $livro;
	private $sessao;
	private $usuario;
	private $organismo;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->livro 		= new livroOrganismo();
		$this->usuario		= new Usuario();
		$this->organismo	= new organismo();
		
	}

	public function cadastroLivroOrganismo() {

		$this->livro->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->livro->setTitulo($_POST['titulo']);
		$this->livro->setAssunto($_POST['assunto']);
		$this->livro->setDescricao($_POST['descricao']);
		$this->livro->setPalavraChave($_POST['palavraChave']);
		$this->livro->setUsuario($_POST['usuario']);

		//Cadastrar ata e pegar ultimo id inserido
		if(!$this->livro->verificaSeJaExiste())
		{
			$resposta = $this->livro->cadastroLivroOrganismo();
			
			//Pegar o OA
			$resultado = $this->organismo->listaOrganismo(null,null,null,null,null,$_POST['fk_idOrganismoAfiliado']);
			$nomeOrganismo="--";
			if($resultado)
			{
				foreach($resultado as $vetor)
				{
					$nomeOrganismo = organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'],$vetor['tipoOrganismoAfiliado'],$vetor['nomeOrganismoAfiliado'],$vetor['siglaOrganismoAfiliado']);
				}
			}
			
			/**
			 * Notificar GLP que o capitulo foi cadastrado
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Novo Capitulo do Livro do Organismo Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Capitulo do Livro Entregue pelo Organismo ".$nomeOrganismo.", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuarioCapitulo'];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
					
			if ($resposta) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaLivroOrganismo&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar este capitulo!');
	                    window.location = '../painelDeControle.php?corpo=buscaLivroOrganismo';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaLivroOrganismo&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaLivroOrganismo() {
		$retorno = $this->livro->listaLivroOrganismo();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaLivroOrganismo($idLivroOrganismo) {

		$resultado = $this->livro->buscarIdLivroOrganismo($idLivroOrganismo);

		if ($resultado) {

			foreach ($resultado as $vetor) {
                                $this->livro->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
				$this->livro->setIdLivroOrganismo($vetor['idLivroOrganismo']);
				$this->livro->setTitulo($vetor['titulo']);
				$this->livro->setAssunto($vetor['assunto']);
				$this->livro->setDescricao($vetor['descricao']);
				$this->livro->setPalavraChave($vetor['palavraChave']);
			}
			return $this->livro;
		} else {
			return false;
		}
	}
	

	public function alteraLivroOrganismo() {

		//$this->livro->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->livro->setTitulo($_POST['titulo']);
		$this->livro->setAssunto($_POST['assunto']);
		$this->livro->setDescricao($_POST['descricao']);
		$this->livro->setPalavraChave($_POST['palavraChave']);
		$this->livro->setUsuarioAlteracao($_POST['usuario']);

		if ($this->livro->alteraLivroOrganismo($_POST['fk_idLivroOrganismo'])) {

			echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaLivroOrganismo&salvo=1';
			</script>";
		} else {
			echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar o capitulo!');
			window.location = '../painelDeControle.php?corpo=buscaLivroOrganismo';
			</script>";
		}
	}

}

?>