<?php
@include_once("model/secaoMenuClass.php");

@include_once("../model/secaoMenuClass.php");

class secaoMenuController {

    private $secaoMenu;

    public function __construct() {

        $this->secaoMenu = new SecaoMenu();
    }

    public function cadastraSecaoMenu() {

        $this->secaoMenu->setNomeSecaoMenu($_POST["nomeSecaoMenu"]);
        $this->secaoMenu->setDescricaoSecaoMenu($_POST["descricaoSecaoMenu"]);
        $this->secaoMenu->setIconeSecaoMenu($_POST["iconeSecaoMenu"]);
        $this->secaoMenu->setArquivoSecaoMenu($_POST["arquivoSecaoMenu"]);
        $this->secaoMenu->setPrioridadeSecaoMenu($_POST["prioridadeSecaoMenu"]);

        if ($this->secaoMenu->cadastraSecaoMenu()) {

            echo "<script type='text/javascript'>
		  window.location = '../painelDeControle.php?corpo=buscaSecaoMenu&salvo=1';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa Se��o!');
		  		window.location = '../painelDeControle.php?corpo=buscaSecaoMenu';
		  </script>";
        }
    }

 	public function criarComboBox($id = 0) {
        $resultado = $this->secaoMenu->listaSecaoMenu();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idSecaoMenu"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['idSecaoMenu'] . '" ' . $selecionado . '>' . $vetor["nomeSecaoMenu"] . '</option>';
            }
        }
    }

    public function listaSecaoMenu() {
        
        $resultado = $this->secaoMenu->listaSecaoMenu();
        return $resultado;
    }

    public function buscaSecaoMenu() {

        $resultado = $this->secaoMenu->buscaIdSecaoMenu();

        if ($resultado) {

            foreach ($resultado as $vetor) {

            	$this->secaoMenu->setIdSecaoMenu($vetor['idSecaoMenu']);
            	$this->secaoMenu->setNomeSecaoMenu($vetor['nomeSecaoMenu']);
            	$this->secaoMenu->setDescricaoSecaoMenu($vetor['descricaoSecaoMenu']);
            	$this->secaoMenu->setIconeSecaoMenu($vetor['iconeSecaoMenu']);
                $this->secaoMenu->setArquivoSecaoMenu($vetor['arquivoSecaoMenu']);
                $this->secaoMenu->setPrioridadeSecaoMenu($vetor['prioridadeSecaoMenu']);
            }

            return $this->secaoMenu;
        } else {
            return false;
        }
    }

    public function alteraSecaoMenu() {

        $this->secaoMenu->setNomeSecaoMenu($_POST["nomeSecaoMenu"]);
        $this->secaoMenu->setDescricaoSecaoMenu($_POST["descricaoSecaoMenu"]);
        $this->secaoMenu->setIconeSecaoMenu($_POST["iconeSecaoMenu"]);
        $this->secaoMenu->setArquivoSecaoMenu($_POST["arquivoSecaoMenu"]);
        $this->secaoMenu->setPrioridadeSecaoMenu($_POST['prioridadeSecaoMenu']);
        

        if ($this->secaoMenu->alteraSecaoMenu($_POST['idSecaoMenu'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaSecaoMenu&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N�o foi poss�vel alterar esta Se��o!');
			window.location = '../painelDeControle.php?corpo=buscaSecaoMenu';
			</script>";
        }
    }

	public function removeSecaoMenu($id) {
        $resultado = $this->secaoMenu->removeSecaoMenu($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Se��o exclu�da com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaSecaoMenu';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir esta Se��o!');
				window.location = '../painelDeControle.php?corpo=buscaSecaoMenu';
				</script>";
        }
    }

}
?>