<?php

@include_once("../model/ocupacaoClass.php");
@include_once("model/ocupacaoClass.php");

class ocupacaoController {

    private $ocupacao;

    public function __construct() {

        $this->ocupacao = new ocupacao();
    }

    public function criarComboBox($id = 0) {
    	
    	$resultado = $this->ocupacao->lista();
    	
        if ($resultado) {

            foreach ($resultado as $vetor) {
                if (($id != 0 && $id == $vetor["SeqOcupac"])) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                
	            echo '<option value="' . $vetor['SeqOcupac'] . '" ' . $selecionado . '>'. $vetor["DesOcupac"] . '</option>';
            }
        }
    }

}

?>