<?php

@include_once("model/planoAcaoRegiaoClass.php");

if(realpath('../model/planoAcaoRegiaoClass.php')){
    @include_once("../model/planoAcaoRegiaoClass.php");
}else{
    @include_once("../../model/planoAcaoRegiaoClass.php");
}

@include_once("model/planoAcaoRegiaoParticipanteClass.php");

if(realpath('../model/planoAcaoRegiaoParticipanteClass.php')){
    @include_once("../model/planoAcaoRegiaoParticipanteClass.php");
}else{
    @include_once("../../model/planoAcaoRegiaoParticipanteClass.php");
}

@include_once("model/usuarioClass.php");

if(realpath('../model/usuarioClass.php')){
    @include_once("../model/usuarioClass.php");
}else{
    @include_once("../../model/usuarioClass.php");
}

@include_once("model/regiaoRosacruzClass.php");

if(realpath('../model/regiaoRosacruzClass.php')){
    @include_once("../model/regiaoRosacruzClass.php");
}else{
    @include_once("../../model/regiaoRosacruzClass.php");
}

if(realpath('../model/criaSessaoClass.php')){
    @include_once ("../model/criaSessaoClass.php");
}else{
    @include_once ("../../model/criaSessaoClass.php");
}

class planoAcaoRegiaoController {

	private $plano;
	private $sessao;
	private $participante;
	private $usuario;
	private $regiao;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->plano 		= new PlanoAcaoRegiao();
		$this->participante	= new planoAcaoRegiaoParticipante();
		$this->usuario		= new Usuario();
		$this->regiao		= new regiaoRosacruz();
		
	}

	public function cadastroPlanoAcaoRegiao() {
		
		$this->plano->setRegiao($_POST['regiao']);
		$this->plano->setTituloPlano($_POST['tituloPlano']);
		$this->plano->setPrioridade($_POST['prioridade']);
		$this->plano->setDescricaoPlano($_POST['descricaoPlano']);
		$this->plano->setPalavraChave($_POST['palavraChave']);
		$this->plano->setCriadoPor($_POST['usuario']);
		$this->plano->setStatusPlano(1);

		//Cadastrar ata e pegar ultimo id inserido
		if(!$this->plano->verificaSeJaExiste())
		{
			$ultimo_id = $this->plano->cadastroPlanoAcaoRegiao();
			
			//Cadastrar Oficiais que participaram da reuni�o
			$return=true;
			
			if(isset($_POST["ata_oa_mensal_oficiais"]))
			{
				$participantes=$_POST["ata_oa_mensal_oficiais"];
				for ($i=0;$i<count($participantes);$i++)
				{
					$this->participante	= new planoAcaoRegiaoParticipante();
					$this->participante->setFk_idPlanoAcaoRegiao($ultimo_id);
					$this->participante->setSeqCadast($participantes[$i]);
					$this->participante->cadastroPlanoAcaoRegiaoParticipante();
					
				}
			}
			
			//Pegar a Região
			$resultado = $this->regiao->listaRegiaoRosacruz(null,null,$_POST['regiao']);
			$regiaoRC="--";
			if($resultado)
			{
				foreach($resultado as $vetor)
				{
					$regiaoRC = $vetor['regiaoRosacruz'];
				}
			}
			
			
			/**
			 * Notificar GLP que o plano foi cadastrado
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Novo Plano de Acao da Regiao Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Plano de Acao Entregue pela Regiao ".$regiaoRC.", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuarioPlano'];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
					
			if ($return) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoRegiao&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar este plano!');
	                    window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoRegiao';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoRegiao&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaPlanoAcaoRegiao($pesquisar=null,$linha_inicial=null,$qtdRegistros=null) {
            //echo $pesquisar;
		$retorno = $this->plano->listaPlanoAcaoRegiao(null,null,$pesquisar,$linha_inicial,$qtdRegistros);
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
        
        public function totalPlanoAcaoRegiao($pesquisar=null,$linha_inicial=null,$qtdRegistros=null) {
            //echo $pesquisar;
		$retorno = $this->plano->listaPlanoAcaoRegiao(null,null,$pesquisar,$linha_inicial,$qtdRegistros);
		
		if ($retorno) {
			return count($retorno);
		} else {
			return false;
		}
	}

	public function buscaPlanoAcaoRegiao($idPlanoAcaoRegiao) {

		$resultado = $this->plano->buscarIdPlanoAcaoRegiao($idPlanoAcaoRegiao);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->plano->setIdPlanoAcaoRegiao($vetor['idPlanoAcaoRegiao']);
				$this->plano->setRegiao($vetor['regiaoRosacruz']);
				$this->plano->setTituloPlano($vetor['tituloPlano']);
				$this->plano->setPrioridade($vetor['prioridade']);
				$this->plano->setDescricaoPlano($vetor['descricaoPlano']);
				$this->plano->setCriadoPor($vetor['nomeUsuario']);
				$this->plano->setDataCriacao($vetor['dataCriacao']);
				$this->plano->setStatusPlano($vetor['statusPlano']);
				$this->plano->setPalavraChave($vetor['palavrasChave']);
			}
			return $this->plano;
		} else {
			return false;
		}
	}
	

	public function alteraPlanoAcaoRegiao() {

		//$this->plano->setRegiao($_POST['regiao']);
		$this->plano->setTituloPlano($_POST['tituloPlano']);
		$this->plano->setPrioridade($_POST['prioridade']);
		$this->plano->setDescricaoPlano($_POST['descricaoPlano']);
		$this->plano->setPalavraChave($_POST['palavraChave']);
		$this->plano->setUltimoAtualizar($_POST['usuario']);
                
		//Remover participantes do plano
		$participante = new planoAcaoRegiaoParticipante();
		$participante->removeParticipantes($_POST['fk_idPlanoAcaoRegiao']);
		//Cadastrar participantes do plano
                if(isset($_POST["ata_oa_mensal_oficiais"]))
                {    
			$participantes=$_POST["ata_oa_mensal_oficiais"];
			for ($i=0;$i<count($participantes);$i++)
			{
				$this->participante	= new planoAcaoRegiaoParticipante();
				$this->participante->setFk_idPlanoAcaoRegiao($_POST['fk_idPlanoAcaoRegiao']);
				$this->participante->setSeqCadast($participantes[$i]);
				$this->participante->cadastroPlanoAcaoRegiaoParticipante();
			}
                }

		if ($this->plano->alteraPlanoAcaoRegiao($_POST['fk_idPlanoAcaoRegiao'])) {

			echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoRegiao&salvo=1';
			</script>";
		} else {
			echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar o plano!');
			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoRegiao';
			</script>";
		}
	}

}

?>