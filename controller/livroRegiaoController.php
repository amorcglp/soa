<?php

@include_once("model/livroRegiaoClass.php");
@include_once("../model/livroRegiaoClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/regiaoRosacruzClass.php");
@include_once("../model/regiaoRosacruzClass.php");

@include_once ("../model/criaSessaoClass.php");

class livroRegiaoController {

	private $livro;
	private $sessao;
	private $usuario;
	private $regiao;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->livro 		= new livroRegiao();
		$this->usuario		= new Usuario();
		$this->regiao		= new regiaoRosacruz();
		
	}

	public function cadastroLivroRegiao() {

		$this->livro->setRegiao($_POST['regiao']);
		$this->livro->setTitulo($_POST['titulo']);
		$this->livro->setAssunto($_POST['assunto']);
		$this->livro->setDescricao($_POST['descricao']);
		$this->livro->setPalavraChave($_POST['palavraChave']);
		$this->livro->setUsuario($_POST['usuario']);
		
		//Pegar a Região
			$resultado = $this->regiao->listaRegiaoRosacruz(null,null,$_POST['regiao']);
			$regiaoRC="--";
			if($resultado)
			{
				foreach($resultado as $vetor)
				{
					$regiaoRC = $vetor['regiaoRosacruz'];
				}
			}
			
			
			/**
			 * Notificar GLP que o capítulo foi cadastrado
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Novo Capitulo do Livro da Regiao Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Capitulo do Livro Entregue pela Regiao ".$regiaoRC.", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuarioCapitulo'];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
                       
		//Cadastrar ata e pegar ultimo id inserido
		if(!$this->livro->verificaSeJaExiste())
		{
			$resposta = $this->livro->cadastroLivroRegiao();
			 		
			if ($resposta) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaLivroRegiao&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar este capitulo de livro!');
	                    window.location = '../painelDeControle.php?corpo=buscaLivroRegiao';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaLivroRegiao&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaLivroRegiao() {
		$retorno = $this->livro->listaLivroRegiao();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaLivroRegiao($idLivroRegiao) {

		$resultado = $this->livro->buscarIdLivroRegiao($idLivroRegiao);

		if ($resultado) {

			foreach ($resultado as $vetor) {
                                $this->livro->setRegiao($vetor['regiaoRosacruz']);
				$this->livro->setIdLivroRegiao($vetor['idLivroRegiao']);
				$this->livro->setTitulo($vetor['titulo']);
				$this->livro->setAssunto($vetor['assunto']);
				$this->livro->setDescricao($vetor['descricao']);
				$this->livro->setPalavraChave($vetor['palavraChave']);
			}
			return $this->livro;
		} else {
			return false;
		}
	}
	

	public function alteraLivroRegiao() {

		$this->livro->setTitulo($_POST['titulo']);
		$this->livro->setAssunto($_POST['assunto']);
		$this->livro->setDescricao($_POST['descricao']);
		$this->livro->setPalavraChave($_POST['palavraChave']);
		$this->livro->setUsuarioAlteracao($_POST['usuario']);

		if ($this->livro->alteraLivroRegiao($_POST['fk_idLivroRegiao'])) {

			echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaLivroRegiao&salvo=1';
			</script>";
		} else {
			echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar o capitulo!');
			window.location = '../painelDeControle.php?corpo=buscaLivroRegiao';
			</script>";
		}
	}

}

?>