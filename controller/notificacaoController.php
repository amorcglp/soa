<?php
@include_once("model/notificacaoClass.php");

@include_once("../model/notificacaoClass.php");

class notificacaoController {

    private $notificacao;

    public function __construct() {

        $this->notificacao = new Notificacao();
    }

    public function cadastroNotificacao() {

        $this->notificacao->setMensagemNotificacao($_POST["mensagemNotificacao"]);
        $this->notificacao->setDescricaoNotificacao($_POST["descricaoNotificacao"]);
        $this->notificacao->setTipoNotificacao($_POST["tipoNotificacao"]);

        if ($this->notificacao->cadastraNotificacaoServer()) {

            echo "<script type='text/javascript'>
		  alert ('Notificacao cadastrada com sucesso!');
		  window.location = '../painelDeControle.php?corpo=buscanotificacaoGlp';
		  </script>";
        } else {

            echo "<script type='text/javascript'>
                  alert ('Nao foi possivel cadastrar essa notificacao!');
		  window.location = '../painelDeControle.php?corpo=cadastronotificacaoGlp';
		  </script>";
        }
    }

    public function retornaListaNotificacao($fk_idTicket=null) {
    	$resultado = $this->notificacao->retornaListaNotificacao($fk_idTicket);
        return $resultado;
    }

    public function retornaNumeroNotificacao($fk_idTicket=null) {
        $resultado = $this->notificacao->retornaNumeroNotificacao($fk_idTicket);
        return $resultado;
    }
    
	public function listaNotificacaoGlp($fk_idNotificacaoGlp=null,$pesquisa="") {

    	$resultado = $this->notificacao->listaNotificacaoGlp($fk_idNotificacaoGlp,$pesquisa);

        return $resultado;
    }

    public function buscaNotificacaoServer($idNotificacao) {

        $resultado = $this->notificacao->buscarIdNotificacaoServer($idNotificacao);

        if ($resultado) {

            foreach ($resultado as $vetor) {

                $this->notificacao->setTituloNotificacao($vetor['tituloNotificacao']);
                $this->notificacao->setDataNotificacao($vetor['dataNotificacao']);
                $this->notificacao->setDescricaoNotificacao($vetor['descricaoNotificacao']);
                $this->notificacao->setTipoNotificacao($vetor['tipoNotificacao']);
                $this->notificacao->setStatusNotificacao($vetor['statusNotificacao']);
            }

            return $this->notificacao;
        } else {
            return false;
        }
    }

    public function alteraNotificacaoServer() {

        $this->notificacao->setTituloNotificacao($_POST["tituloNotificacao"]);
        $this->notificacao->setDescricaoNotificacao($_POST["descricaoNotificacao"]);
        $this->notificacao->setTipoNotificacao($_POST["tipoNotificacao"]);

        if ($this->notificacao->alteraNotificacaoServer($_POST['idNotificacao'])) {

            echo
            "<script type='text/javascript'>
			alert ('Notificação alterada com sucesso!');
			window.location = '../painelDeControle.php?corpo=buscanotificacaoGlp';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar esta notificacao!');
			window.location = '../painelDeControle.php?corpo=buscanotificacaoGlp';
			</script>";
        }
    }

    public function removeNotificacaoServer($id) {
        $resultado = $this->notificacao->removeNotificacao($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Notificação excluida com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscanotificacaoGlp&id=" . $id . "';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Nao foi possivel excluir esta notificação!');
				window.location = '../painelDeControle.php?corpo=alterarnotificacaoGlp&id=" . $id . "';
				</script>";
        }
    }
    
	public function buscaDepartamento($idDepartamento=null) {

    	$resultado = $this->notificacao->buscaDepartamento($idDepartamento);

        return $resultado;
    }

}
?>