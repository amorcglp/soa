<?php

@include_once("../model/videoAulaClass.php");
@include_once("model/videoAulaClass.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class videoAulaController {

    private $videoAula;

    public function __construct() {

        $this->videoAula = new videoAula();
    }

    public function cadastroVideoAula() {
        
        //Filtrar Request
        $arrFiltroRequest=array('tituloVideoAula',
                            'descricaoVideoAula',
                            'dataInicial',
                            'dataFinal',
                            'link',
                            'usuario'
                            );
        if(validaSegurancaRequest($arrFiltroRequest))
        {
            enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
            echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
            exit();
        }
        
        //Filtrar Input
        $clean = array();
        $dirty = array(

        0 => array($_POST["tituloVideoAula"],"textoNumero"),
        1 => array(cleanHtml($_POST["descricaoVideoAula"]),"html"),
        2 => array($_POST["dataInicial"],"textoNumero"),
        3 => array($_POST["dataFinal"],"textoNumero"),
        4 => array($_POST["link"],"textoNumero"),
        5 => array($_POST["usuario"],"inteiro")    
        );

        if(!validaSegurancaInput($dirty))
        {    
            $clean = $dirty;
		        
            $this->videoAula->setTitulo($clean[0][0]);
            $this->videoAula->setDescricao($clean[1][0]);
            $this->videoAula->setDataInicial(substr($clean[2][0],6,4)."-".substr($clean[2][0],3,2)."-".substr($clean[2][0],0,2));
            $this->videoAula->setDataFinal(substr($clean[3][0],6,4)."-".substr($clean[3][0],3,2)."-".substr($clean[3][0],0,2));
            $this->videoAula->setLink($clean[4][0]);
            $this->videoAula->setStatus(1);
            $this->videoAula->setUsuario($clean[5][0]);

            if ($this->videoAula->cadastroVideoAula()) {

                echo "<script type='text/javascript'>
                        window.location = '../painelDeControle.php?corpo=buscaGerenciamentoVideoAula&salvo=1';
                      </script>";
            } else {
                echo "<script type='text/javascript'>
                        alert('N\u00e3o foi poss\u00edvel cadastrar essa Video Aula!');
                        window.location = '../painelDeControle.php?corpo=cadastroVideoAula';
                      </script>";
            }
        }else{
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaGerenciamentoVideoAula';
                                </script>";
        } 
    }

    public function listaidVideoAula() {
        $retorno = $this->videoAula->listaVideoAula($fk_idUsuario);
        $listaVideoAula = "";

        foreach ($retorno as $dados) {
            $listaVideoAula[] = $dados['idVideoAula'];
        }

        if ($listaVideoAula) {
            return $listaVideoAula;
        } else {
            return false;
        }
    }

    public function listaVideoAula($ativas=null) {
        
        if($ativas==null)
        {
            $resultado = $this->videoAula->listaVideoAula();
        }else{
            $resultado = $this->videoAula->listaVideoAula(true);
        }
        	
        return $resultado;
    }

    public function buscaVideoAula($idVideoAula) {

        $resultado = $this->videoAula->buscarIdVideoAula($idVideoAula);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);
                $this->videoAula->setTitulo($vetor['titulo']);
                $this->videoAula->setDescricao($vetor['descricao']);
                $this->videoAula->setDataInicial(date('d/m/Y', strtotime($vetor['dataInicial'])));
                $this->videoAula->setDataFinal(date('d/m/Y', strtotime($vetor['dataFinal'])));
                $this->videoAula->setLink($vetor['link']);
                $this->videoAula->setStatus($vetor['status']);
            }

            return $this->videoAula;
        } else {
            return false;
        }
    }
    
    public function alteraVideoAula() {
        
        //Filtrar Request
        $arrFiltroRequest=array('tituloVideoAula',
                            'descricaoVideoAula',
                            'dataInicial',
                            'dataFinal',
                            'link',
                            'usuario'
                            );
        if(validaSegurancaRequest($arrFiltroRequest))
        {
            enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
            echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
            exit();
        }
        
        //Filtrar Input
        $clean = array();
        $dirty = array(

        0 => array($_POST["tituloVideoAula"],"textoNumero"),
        1 => array(cleanHtml($_POST["descricaoVideoAula"]),"html"),
        2 => array($_POST["dataInicial"],"textoNumero"),
        3 => array($_POST["dataFinal"],"textoNumero"),
        4 => array($_POST["link"],"textoNumero"),
        5 => array($_POST["usuario"],"inteiro"),
        6 => array($_POST["idVideoAula"],"inteiro")    
        );

        if(!validaSegurancaInput($dirty))
        {    
            $clean = $dirty;
        
            $this->videoAula->setTitulo($clean[0][0]);
            $this->videoAula->setDescricao($clean[1][0]);
            $this->videoAula->setDataInicial(substr($clean[2][0],6,4)."-".substr($clean[2][0],3,2)."-".substr($clean[2][0],0,2));
            $this->videoAula->setDataFinal(substr($clean[3][0],6,4)."-".substr($clean[3][0],3,2)."-".substr($clean[3][0],0,2));
            $this->videoAula->setLink($clean[4][0]);
            $this->videoAula->setUsuario($clean[5][0]);

            if ($this->videoAula->alteraVideoAula($clean[6][0])) {

                echo
                "<script type='text/javascript'>
                            window.location = '../painelDeControle.php?corpo=buscaGerenciamentoVideoAula&salvo=1';
                            </script>";
            } else {
                echo
                "<script type='text/javascript'>
                            alert ('N\u00e3o foi poss\u00edvel alterar a Anotação!');
                            window.location = '../painelDeControle.php?corpo=alteraVideoAula&id=" . $_POST['idVideoAula'] . "';
                            </script>";
            }
        }else{  
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaGerenciamentoVideoAula';
                                </script>";
        } 
    }

    public function removeVideoAula($idVideoAula) {
    	$this->videoAula->getIdVideoAula($idVideoAula);
    	
        $resultado = $this->videoAula->removeVideoAula($idVideoAula);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				window.location = 'painelDeControle.php?corpo=buscaGerenciamentoVideoAula&excluido=1';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('N\u00e3o foi poss\u00edvel excluir essa Anotação!');
				window.location = '../painelDeControle.php?corpo=buscaGerenciamentoVideoAula';
				</script>";
        }
    }

}

?>