<?php

@include_once("../model/usuarioClass.php");
@include_once("model/usuarioClass.php");

class usuarioController {

    private $usuario;

    public function __construct() {

        $this->usuario = new Usuario();
    }

    public function cadastroUsuario() {

        $this->usuario->seqCadast = ($_POST["seqCadast"]);
        $this->usuario->nomeUsuario = ($_POST["nomeUsuario"]);
        $this->usuario->emailnomeUsuario = ($_POST['emailUsuario']);
        $this->usuario->telefoneUsuario = ($_POST['telefoneUsuario']);
        $this->usuario->celularUsuario = ($_POST['celularUsuario']);
        $this->usuario->ramalUsuario = ($_POST['ramalUsuario']);
        $this->usuario->loginUsuario = ($_POST['loginUsuario']);
        $this->usuario->senhaUsuario = password_hash($_POST['senhaUsuario'], PASSWORD_DEFAULT);
        $this->usuario->dataCadastroUsuario = ($_POST['dataCadastroUsuario']);
        $this->usuario->codigoDeAfiliacao = ($_POST['codigoDeAfiliacao']);
        $this->usuario->fk_idDepartamento = ($_POST['fk_idDepartamento']);
        $this->usuario->fk_idRegiaoRosacruz = ($_POST['fk_idRegiaoRosacruz']);

        if ($this->usuario->cadastraUsuario()) {

            echo "<script type='text/javascript'>
            window.location = '../painelDeControle.php?corpo=buscaUsuario&salvo=1';
            </script>";
        } else {

            echo "<script type='text/javascript'>
            alert('Nao foi possivel cadastrar esse Administrador!');
            window.location = '../views/cadastroAdmin.php';
            </script>";
        }
    }

    public function criarComboBox($id = 0) {
        $resultado = $this->usuario->listaUsuario();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["seqCadast"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                echo '<option value="' . $vetor['seqCadast'] . '" ' . $selecionado . '>' . $vetor["nomeUsuario"] . '</option>';
            }
        }
    }

    public function listaUsuario($siglaOrganismo=null,$letra=null,$letraNome=null) {
        //echo "letra:".$letra;
    	if (!isset($_POST['nomeUsuario'])) {
    		$resultado = $this->usuario->listaUsuario($siglaOrganismo,null,null,null,null,null,null,null,null,null,null,null,null,null,$letra,$letraNome);
    	}
        
		return $resultado;
        
    }

    public function buscaUsuario($seqCadast) {

        $resultado = $this->usuario->buscaUsuario($seqCadast);


        if ($resultado) {

            foreach ($resultado as $vetor) {
            	//echo "<pre>";print_r($resultado);
                $this->usuario->setNomeUsuario($vetor['nomeUsuario']);
                $this->usuario->setEmailUsuario($vetor['emailUsuario']);
                $this->usuario->setLoginUsuario($vetor['loginUsuario']);
                $this->usuario->setSenhaUsuario("");
                $this->usuario->setTelefoneResidencialUsuario($vetor['telefoneResidencialUsuario']);
                $this->usuario->setTelefoneComercialUsuario($vetor['telefoneComercialUsuario']);
                $this->usuario->setCelularUsuario($vetor['celularUsuario']);
                $this->usuario->setCodigoDeAfiliacaoUsuario($vetor['codigoDeAfiliacao']);
				$this->usuario->setFk_idDepartamento($vetor['fk_idDepartamento']);
            }
             //echo "<pre>";print_r($this->usuario);exit();
            return $this->usuario;
        } else {
            return false;
        }
    }

    public function alteraUsuario() {
        
        $this->usuario->setNomeUsuario($_POST['nomeUsuario']);
        $this->usuario->setEmailUsuario($_POST['emailUsuario']);
        $this->usuario->setTelefoneResidencialUsuario($_POST['telefoneResidencialUsuario']);
        $this->usuario->setTelefoneComercialUsuario($_POST['telefoneComercialUsuario']);
        $this->usuario->setCelularUsuario($_POST['celularUsuario']);
        $this->usuario->setLoginUsuario($_POST['loginUsuario']);
        $hashSenha = password_hash($_POST['senhaUsuario'], PASSWORD_DEFAULT);
        //echo $hashSenha;exit();
        $this->usuario->setSenhaUsuario($hashSenha);
        $fk_idDepartamento = isset($_POST['fk_idDepartamento'])?$_POST['fk_idDepartamento']:null;
       //echo "fk_idDepartamento:".$fk_idDepartamento;exit();
        if($fk_idDepartamento!=null)
        {
        	$this->usuario->setFk_idDepartamento($fk_idDepartamento);
        }
        if ($this->usuario->alteraUsuario($_POST['seqCadast'])) {

            $letra = $_REQUEST['letra'];
            $letraNome = $_REQUEST['letraNome'];

            echo
            "<script type='text/javascript'>
				window.location = '../painelDeControle.php?corpo=buscaUsuario&salvo=1&letra=$letra&letraNome=$letraNome';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('N\u00e3o foi poss\u00edvel alterar os dados cadastrais!');
				window.location = '../painelDeControle.php?corpo=buscaUsuario';
			</script>";
        }
        
    }

    public function removeAdmin($id) {
        $resultado = $this->usuario->removeAdmin($id);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
            alert('Administrador excluido com sucesso!');
            window.location = '../homeGerenciador.php?url=buscaAdmin&id=" . $id . "';
            </script>";
        } else {
            echo
            "<script type='text/javascript'>
            alert('Nao foi possivel excluir este Administrador!');
            window.location = '../homeGerenciador.php?url=alteraAdmin&id=" . $id . "';
            </script>";
        }
    }

    public function pontoAdmin($idAdmin) {
        $resultado = $this->usuario->buscarIdAdmin($idAdmin);

        if ($resultado) {
            foreach ($resultado as $vetor) {
                $this->usuario->setIdAdmin($vetor["idAdmin"]);
                $this->usuario->setNomeAdmin($vetor["nomeAdmin"]);
                $this->usuario->setAtividadeAdmin($vetor["atividadeAdmin"]);
            }
        }
        if ($this->usuario->getAtividadeAdmin() == 0) {
            $this->usuario->setAtividadeAdmin(1);
            echo
            "<script type='text/javascript'>
            alert('Entrada realizada com sucesso!');
            window.location = '../painelDeControle.php';
            </script>";
        } else if ($this->usuario->getAtividadeAdmin() == 1) {
            $this->usuario->setAtividadeAdmin(0);
            echo
            "<script type='text/javascript'>
            alert('Saida realizada com sucesso!');
            window.location = '../painelDeControle.php';
            </script>";
        }

        if ($this->usuario->alteraAtividadeAdmin()) {
            return $this->usuario->getAtividadeAdmin();
        } else {
            return false;
        }
    }

}

?>