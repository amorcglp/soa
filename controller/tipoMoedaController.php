<?php

@include_once("../model/tipoMoedaClass.php");
@include_once("model/tipoMoedaClass.php");

class tipoMoedaController {

    private $tipoMoeda;

    public function __construct() {

        $this->tipoMoeda = new tipoMoeda();
    }

    public function criarComboBox($id = 0) {
    	
    	$resultado = $this->tipoMoeda->lista();
    	
        if ($resultado) {

            foreach ($resultado as $vetor) {
                if (($id != 0 && $id == $vetor["SeqMoeda"])) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                
	            echo '<option value="' . $vetor['SeqMoeda'] . '" ' . $selecionado . '>'. $vetor["DesTipoMoeda"] . '</option>';
            }
        }
    }

}

?>