<?php

@include_once '../model/documentoClass.php';
@include_once 'model/documentoClass.php';

class documentoController {
    private $documento;
    
    public function __construct() {
        $this->documento = new Documento();
    }
    
    public function setIdDocumento($id) {
        $this->documento->setIdDocumento($id);
    }
    
    public function getIdDocumento() {
        return $this->documento->getIdDocumento();
    }
    
    public function setDocumentoExtensao($ext) {
        $this->documento->setExtensao($ext);
    }
    
    public function getDocumentoExtensao() {
        return $this->documento->getExtensao();
    }
    
    public function setDocumentoTipo($documentoTipo) {
        $this->documento->setTipo($documentoTipo);
    }
    
    public function setPaginaImpressao($pagina=null) {
        $this->documento->setPaginaImpressao($pagina);
    }
    
    public function setDocumentoDescricao($descricao) {
        $this->documento->setDescricao($descricao);
    }
    
    public function getPaginaImpressao() {
        return $this->documento->getPaginaImpressao();
    }
    
    public function setDocumentoArquivo($documentoArquivo) {
        $this->documento->setArquivo($documentoArquivo);
    }
    
    public function getDocumentoDescricao() {
        return $this->documento->getDescricao();
    }
    
    public function getDocumentoArquivo() {
        return $this->documento->getArquivo();
    }
    
    public function getDocumentoTipo() {
        return $this->documento->getTipo();
    }
    
    public function getDocumentoMaxId() {
        return $this->documento->getIdValorMaximo();
    }
    
    public function listaDocumento($t = null, $e = null) {
        return $this->documento->listaDocumento($t, $e);
    }
    
    public function buscarDados($arq) {
        return $this->documento->buscarDocumento($arq);
    }
    
    public function buscarDadosId($id) {
        return $this->documento->buscarDocumentoId($id);
    }
    
    public function cadastroDocumento($post=true) {
        if($post) {
            $this->documento->setArquivo($_POST['arquivo']);
            $this->documento->setExtensao($_POST['extensao']);
            $this->documento->setIdDocumento($_POST['proximoId']);
            $this->documento->setPaginaImpressao(isset($_POST['paginaImpressao'])?$_POST['paginaImpressao']:null);
            $this->documento->setTipo($_POST['tipo']);
        }
        return $this->documento->cadastraDocumento();
    }
    
    public function criarComboBox($id=null, $tipo=null) {
        $resultado = $this->listaDocumento($tipo);
        
        if($resultado) {
            foreach ($resultado as $vetor) {
                if($id != null && $id == $vetor['idDocumento']) {
                    $selecionado = "selected";
                } else {
                    $selecionado = "";
                }
                echo "<option value=\"".$vetor['idDocumento']."\" ".$selecionado.">".strtoupper($vetor['arquivo'])."</option>";
            }
        }
    }
    
    public function somaUltimaId() {
        $proximoId = 1;
        $resultado = $this->documento->getIdValorMaximo();
        
        if($resultado) {
            foreach ($resultado as $vetor) {
                $proximoId = $vetor['maxId'] + 1;
            }
        }
        
        return $proximoId;
    }
    
    public function deletarDocumento($idDocumento) {
        return $this->documento->deletaDocumento($idDocumento);
    }
}
