<?php

@include_once("model/publicoClass.php");
@include_once("../model/publicoClass.php");

class publicoController {

    private $publico;

    public function __construct() {

        $this->publico = new Publico();
    }

    public function cadastroPublico() {

        $this->publico->setPublico($_POST['publico']);
        

        if ($this->publico->cadastroPublico()) {

            echo "<script type='text/javascript'>
		    window.location = '../painelDeControle.php?corpo=buscaPublico&salvo=1';
		  </script>";
        } else {
            echo "<script type='text/javascript'>
                    alert('Não foi possível cadastrar esse Publico!');
                    window.location = '../painelDeControle.php?corpo=buscaPublico';
		  </script>";
        }
    }

    public function criarComboBox($id = 0) {
        $resultado = $this->publico->listaPublico();

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if ($id != 0 && $id == $vetor["idPublico"]) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                
                echo '<option value="' . $vetor['idPublico'] . '" ' . $selecionado . '>' . $vetor["publico"] . '</option>';
                
            }
        }
    }

    public function listaPublico() {
        $retorno = $this->publico->listaPublico();
        
        if ($retorno) {
            return $retorno;
        } else {
            return false;
        }
    }

    public function buscaPublico($idPublico) {
	
	        $resultado = $this->publico->listaPublico($idPublico);
	
	        if ($resultado) {
	
	            foreach ($resultado as $vetor) {
	                $this->publico->setIdPublico($vetor['idPublico']);
	                $this->publico->setPublico($vetor['publico']);
	            }
	            return $this->publico;
	        } else {
	            return false;
	        }
	 }
    
    public function alteraPublico() {
		
        $this->publico->setIdPublico($_POST['idPublico']);
        $this->publico->setPublico($_POST['publico']);

        if ($this->publico->alteraPublico($_POST['idPublico'])) {

            echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaPublico&salvo=1';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar o público!');
			window.location = '../painelDeControle.php?corpo=buscaPublico';
			</script>";
        }
    }

    public function removePublico($idPublico) {
        
        $resultado = $this->publico->removePublico($idPublico);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Público excluído com sucesso!');
				window.location = '../painelDeControle.php?corpo=buscaPublico';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('Não foi possível excluir o Público!');
				window.location = '../painelDeControle.php?corpo=buscaPublico';
				</script>";
        }
    }

}

?>