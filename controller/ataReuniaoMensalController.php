<?php

@include_once("model/ataReuniaoMensalClass.php");
@include_once("../model/ataReuniaoMensalClass.php");

@include_once("model/ataReuniaoMensalOficialClass.php");
@include_once("../model/ataReuniaoMensalOficialClass.php");

@include_once("model/ataReuniaoMensalTopicoClass.php");
@include_once("../model/ataReuniaoMensalTopicoClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/regiaoRosacruzClass.php");
@include_once("../model/regiaoRosacruzClass.php");

@include_once ("../model/criaSessaoClass.php");

@include_once("ticketController.php");

@include_once("../lib/functionsSeguranca.php");
@include_once("lib/functionsSeguranca.php");

class ataReuniaoMensalController {

	private $ata;
	private $ata_oficial;
	private $ata_topico;
	private $sessao;
        private $regiao;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->ata 		= new ataReuniaoMensal();
		$this->usuario		= new Usuario();
        $this->regiao		= new regiaoRosacruz();
        $this->ticket		    = new TicketController();
		
	}

	public function cadastroAtaReuniaoMensal() {
            
            //Filtrar Request
            $arrFiltroRequest=array('dataAtaReuniaoMensal',
                'fk_idOrganismoAfiliado',
                'h_seqCadastMembroPresidente',
                'h_nomeMembroPresidente',
                'horaInicialAtaReuniaoMensal',
                'horaFinalAtaReuniaoMensal',
                'mesCompetencia',
                'anoCompetencia',
                'numeroMembrosAtaReuniaoMensal',
                'monitorRegionalPresente',
                'ata_oa_mensal_oficiais',
                'topico_um',
                'topico_dois',
                'topico_tres',
                'topico_quatro',
                'topico_cinco',
                'topico_seis',
                'topico_sete',
                'topico_oito',
                'topico_nove',
                'topico_dez',
                'siglaOrganismoAta',
                'nomeUsuarioAta'
            );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }

            //Filtrar Input
            $clean = array();
            $dirty = array(
                0 => array($_POST["dataAtaReuniaoMensal"],"textoNumero"),
                1 => array($_POST["fk_idOrganismoAfiliado"],"inteiro"),
                2 => array($_POST["h_seqCadastMembroPresidente"],"inteiro"),
                3 => array($_POST["h_nomeMembroPresidente"],"textoNumero"),
                4 => array($_POST["horaInicialAtaReuniaoMensal"],"textoNumero"),
                5 => array($_POST["horaFinalAtaReuniaoMensal"],"textoNumero"),
                6 => array($_POST["mesCompetencia"],"inteiro"),
                7 => array($_POST["anoCompetencia"],"inteiro"),
                8 => array($_POST["numeroMembrosAtaReuniaoMensal"],"inteiro"),
                9 => array($_POST["monitorRegionalPresente"],"textoNumero"),
                10 => array($_POST["ata_oa_mensal_oficiais"],"textoNumero"),
                11 => array(cleanHtml($_POST["topico_um"]),"html"),
                12 => array(cleanHtml($_POST["topico_dois"]),"html"),
                13 => array(cleanHtml($_POST["topico_tres"]),"html"),
                14 => array(cleanHtml($_POST["topico_quatro"]),"html"),
                15 => array(cleanHtml($_POST["topico_cinco"]),"html"),
                16 => array(cleanHtml($_POST["topico_seis"]),"html"),
                17 => array(cleanHtml($_POST["topico_sete"]),"html"),
                18 => array(cleanHtml($_POST["topico_oito"]),"html"),
                19 => array(cleanHtml($_POST["topico_nove"]),"html"),
                20 => array(cleanHtml($_POST["topico_dez"]),"html"),
                21 => array($_POST["siglaOrganismoAta"],"textoNumero"),
                22 => array($_POST["nomeUsuarioAta"],"textoNumero")
                );

            if(!validaSegurancaInput($dirty))
            {    
                $clean = $dirty;

		$this->ata->setDataAtaReuniaoMensal(substr($clean[0][0],6,4)."-".substr($clean[0][0],3,2)."-".substr($clean[0][0],0,2));
		$this->ata->setFk_idOrganismoAfiliado($clean[1][0]);
                /*
		if(isset($_POST['companheiroMembroPresidente']))
		{
			$this->ata->setCompanheiro(1);
			$this->ata->setPresidenteAtaReuniaoMensal($_POST['h_seqCadastCompanheiroMembroPresidente']);
			$this->ata->setNomePresidenteAtaReuniaoMensal($_POST['h_nomeCompanheiroMembroPresidente']);
		}else{*/
			$this->ata->setCompanheiro(0);
			$this->ata->setPresidenteAtaReuniaoMensal($clean[2][0]);
			$this->ata->setNomePresidenteAtaReuniaoMensal($clean[3][0]);
                       /* 
		}*/
		$this->ata->setHoraInicialAtaReuniaoMensal($clean[4][0]);
		$this->ata->setHoraFinalAtaReuniaoMensal($clean[5][0]);
		$this->ata->setMesCompetencia($clean[6][0]);
		$this->ata->setAnoCompetencia($clean[7][0]);
		$this->ata->setNumeroMembrosAtaReuniaoMensal($clean[8][0]);
		$this->ata->setFk_seqCadast($this->sessao->getValue("seqCadast"));
                
                if(isset($clean[9][0]))
                {    
                    if($clean[9][0]=="on")
                    {
                        $this->ata->setMonitorRegionalPresente(1);
                    }else{
                        $this->ata->setMonitorRegionalPresente(0);
                    }    
                }else{
                    $this->ata->setMonitorRegionalPresente(0);
                }
		//Cadastrar ata e pegar ultimo id inserido
		if(!$this->ata->verificaSeJaExiste())
		{
			$ultimo_id = $this->ata->cadastroAtaReuniaoMensal();
		
		
			//Cadastrar Oficiais que participaram da reuni�o
			$return=false;
			$ata_oa_mensal_oficiais=$clean[10][0];
			for ($i=0;$i<count($ata_oa_mensal_oficiais);$i++)
			{
				$this->ata_oficial 	= new ataReuniaoMensalOficial();
				$this->ata_oficial->setFk_idAtaReuniaoMensal($ultimo_id);
				$this->ata_oficial->setFk_seqCadastMembro($ata_oa_mensal_oficiais[$i]);
				$this->ata_oficial->cadastroAtaReuniaoMensalOficial();
				$return=true;
			}
			
			//Cadastrar T�picos
					
			$this->ata_topico = new ataReuniaoMensalTopico();
			$this->ata_topico->setFk_idAtaReuniaoMensal($ultimo_id);
			$this->ata_topico->setTopicoUm($clean[11][0]);
			$this->ata_topico->setTopicoDois($clean[12][0]);
			$this->ata_topico->setTopicoTres($clean[13][0]);
			$this->ata_topico->setTopicoQuatro($clean[14][0]);
			$this->ata_topico->setTopicoCinco($clean[15][0]);
			$this->ata_topico->setTopicoSeis($clean[16][0]);
			$this->ata_topico->setTopicoSete($clean[17][0]);
			$this->ata_topico->setTopicoOito($clean[18][0]);
			$this->ata_topico->setTopicoNove($clean[19][0]);
			$this->ata_topico->setTopicoDez($clean[20][0]);
			$this->ata_topico->cadastroAtaReuniaoMensalTopico();
			
			/**
			 * Notificar GLP que a ata foi cadastrada
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Nova Ata de Reuniao Mensal Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Ata de Reuniao Mensal Entregue pelo Organismo ".$clean[21][0].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$clean[22][0];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
                        <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
                        <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
                        
                        //Selecionar Grandes Conselheiros
                        $resultadoRegiao = $this->regiao->listaRegiaoRosacruz(1,$_POST['siglaOrganismoAta']);
                        $idRegiaoRosacruz = 0;
                        if($resultadoRegiao)
			{
				foreach ($resultadoRegiao as $vetor)
				{
					$idRegiaoRosacruz = $vetor['idRegiaoRosacruz'];
				}
			}
                        $resultadoUsuarios = $this->usuario->listaUsuario(null,null,"151",$idRegiaoRosacruz);
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					echo ",\"".$vetor['idUsuario']."\"";
				}
			}
                        
                        //Selecionar mestre do organismo
			$resultadoMestre = $this->usuario->listaUsuario($_POST['siglaOrganismoAta'],null,"201");
			if($resultadoMestre)
			{
				foreach ($resultadoMestre as $vetor)
				{
					echo ",\"".$vetor['idUsuario']."\"";
				}
			}
                        
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
			//exit();	
			if ($return) {

                                $idFuncionalidade       = 2;
                                $remetente              = addslashes($_POST["fk_seqCadastAtualizadoPor"]);
                                $descricao              = "Nova Ata de Reunião Mensal Entregue";
                                $idOrg                  = $_POST['fk_idOrganismoAfiliado'];
                                $this->ticket->cadastroTicketAltomatico($idFuncionalidade,$descricao,$remetente,$idOrg,$ultimo_id,1);

				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAtaReuniaoMensal&salvo=1';
			 	</script>";

			} else {
				echo "<script type='text/javascript'>
	                    alert('Nao foi possivel cadastrar essa Ata!');
	                    window.location = '../painelDeControle.php?corpo=buscaAtaReuniaoMensal';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaAtaReuniaoMensal&jaCadastrado=1';
			 	</script>";
		}
             }else{
                
                echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaAtaReuniaoMensal';
                                </script>";
            } 
	}

	public function listaAtaReuniaoMensal($idOA=null) {
		$retorno = $this->ata->listaAtaReuniaoMensal(null,null,$idOA);
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaAtaReuniaoMensal($idAtaReuniaoMensal) {

		$resultado = $this->ata->buscarIdAtaReuniaoMensal($idAtaReuniaoMensal);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->ata->setIdAtaReuniaoMensal($vetor['idAtaReuniaoMensal']);
				$this->ata->setDataAtaReuniaoMensal($vetor['dataAtaReuniaoMensal']);
				$this->ata->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
				$this->ata->setPresidenteAtaReuniaoMensal($vetor['presidenteAtaReuniaoMensal']);
				$this->ata->setNomePresidenteAtaReuniaoMensal($vetor['nomePresidenteAtaReuniaoMensal']);
				$this->ata->setCompanheiro($vetor['companheiro']);
				$this->ata->setHoraInicialAtaReuniaoMensal($vetor['horaInicialAtaReuniaoMensal']);
				$this->ata->setHoraFinalAtaReuniaoMensal($vetor['horaFinalAtaReuniaoMensal']);
				$this->ata->setMesCompetencia($vetor['mesCompetencia']);
				$this->ata->setAnoCompetencia($vetor['anoCompetencia']);
				$this->ata->setNumeroMembrosAtaReuniaoMensal($vetor['numeroMembrosAtaReuniaoMensal']);
                $this->ata->setMonitorRegionalPresente($vetor['monitorRegionalPresente']);
			}
			return $this->ata;
		} else {
			return false;
		}
	}
	

	public function alteraAtaReuniaoMensal() {
            
            //Filtrar Request
            $arrFiltroRequest=array('dataAtaReuniaoMensal',
                'fk_idOrganismoAfiliado',
                'h_seqCadastMembroPresidente',
                'h_nomeMembroPresidente',
                'horaInicialAtaReuniaoMensal',
                'horaFinalAtaReuniaoMensal',
                'mesCompetencia',
                'anoCompetencia',
                'numeroMembrosAtaReuniaoMensal',
                'monitorRegionalPresente',
                'ata_oa_mensal_oficiais',
                'topico_um',
                'topico_dois',
                'topico_tres',
                'topico_quatro',
                'topico_cinco',
                'topico_seis',
                'topico_sete',
                'topico_oito',
                'topico_nove',
                'topico_dez',
                'siglaOrganismoAta',
                'nomeUsuarioAta'
            );
            if(validaSegurancaRequest($arrFiltroRequest))
            {
                enviaEmailViolacaoSeguranca($_SERVER['REMOTE_ADDR']);
                echo utf8_decode("Violacao de Seguranca, o Administrador foi alertado!");
                exit();
            }
            
            $_POST["monitorRegionalPresente"] = isset($_POST["monitorRegionalPresente"])?$_POST["monitorRegionalPresente"]:null;

            //Filtrar Input
            $clean = array();
            $dirty = array(
                0 => array($_POST["dataAtaReuniaoMensal"],"textoNumero"),
                1 => array($_POST["h_seqCadastMembroPresidente"],"inteiro"),
                2 => array($_POST["h_nomeMembroPresidente"],"textoNumero"),
                3 => array($_POST["horaInicialAtaReuniaoMensal"],"textoNumero"),
                4 => array($_POST["horaFinalAtaReuniaoMensal"],"textoNumero"),
                5 => array($_POST["mesCompetencia"],"inteiro"),
                6 => array($_POST["anoCompetencia"],"inteiro"),
                7 => array($_POST["numeroMembrosAtaReuniaoMensal"],"inteiro"),
                8 => array($_POST["monitorRegionalPresente"],"textoNumero"),
                9 => array($_POST["ata_oa_mensal_oficiais"],"textoNumero"),
                10 => array(cleanHtml($_POST["topico_um"]),"html"),
                11 => array(cleanHtml($_POST["topico_dois"]),"html"),
                12 => array(cleanHtml($_POST["topico_tres"]),"html"),
                13 => array(cleanHtml($_POST["topico_quatro"]),"html"),
                14 => array(cleanHtml($_POST["topico_cinco"]),"html"),
                15 => array(cleanHtml($_POST["topico_seis"]),"html"),
                16 => array(cleanHtml($_POST["topico_sete"]),"html"),
                17 => array(cleanHtml($_POST["topico_oito"]),"html"),
                18 => array(cleanHtml($_POST["topico_nove"]),"html"),
                19 => array(cleanHtml($_POST["topico_dez"]),"html"),
                20 => array($_POST["fk_idAtaReuniaoMensal"],"inteiro")
                );

            if(!validaSegurancaInput($dirty))
            {    
                $clean = $dirty;

		$this->ata->setDataAtaReuniaoMensal(substr($clean[0][0],6,4)."-".substr($clean[0][0],3,2)."-".substr($clean[0][0],0,2));
		//$this->ata->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
                /*
		if(isset($_POST['companheiroMembroPresidente']))
		{
			$this->ata->setCompanheiro(1);
			$this->ata->setPresidenteAtaReuniaoMensal($_POST['h_seqCadastCompanheiroMembroPresidente']);
			$this->ata->setNomePresidenteAtaReuniaoMensal($_POST['h_nomeCompanheiroMembroPresidente']);
		}else{
                 */
			$this->ata->setCompanheiro(0);
			$this->ata->setPresidenteAtaReuniaoMensal($clean[1][0]);
			$this->ata->setNomePresidenteAtaReuniaoMensal($clean[2][0]);
                        /*
		}*/
		$this->ata->setHoraInicialAtaReuniaoMensal($clean[3][0]);
		$this->ata->setHoraFinalAtaReuniaoMensal($clean[4][0]);
		$this->ata->setMesCompetencia($clean[5][0]);
		$this->ata->setAnoCompetencia($clean[6][0]);
		$this->ata->setNumeroMembrosAtaReuniaoMensal($clean[7][0]);
                $this->ata->setUltimoAtualizar($this->sessao->getValue("seqCadast"));
                
                if(isset($clean[8][0]))
                {    
                    if($clean[8][0]=="on")
                    {
                        $this->ata->setMonitorRegionalPresente(1);
                    }else{
                        $this->ata->setMonitorRegionalPresente(0);
                    }    
                }else{
                    $this->ata->setMonitorRegionalPresente(0);
                }
		
		//Remover todos os oficiais e inserir novamente
		$oficiais = new ataReuniaoMensalOficial();
		$oficiais->removeOficiais($clean[20][0]);
		
		$return=false;
		$ata_oa_mensal_oficiais=$clean[9][0];
		for ($i=0;$i<count($ata_oa_mensal_oficiais);$i++)
		{
			$this->ata_oficial 	= new ataReuniaoMensalOficial();
			$this->ata_oficial->setFk_idAtaReuniaoMensal($clean[20][0]);
			$this->ata_oficial->setFk_seqCadastMembro($ata_oa_mensal_oficiais[$i]);
			$this->ata_oficial->cadastroAtaReuniaoMensalOficial();
			$return=true;
		}
		
		//Remover todos os t�picos e inserir novamente
		$topicos = new ataReuniaoMensalTopico();
		$topicos->removeTopicos($clean[20][0]);
				
		$this->ata_topico = new ataReuniaoMensalTopico();
		$this->ata_topico->setFk_idAtaReuniaoMensal($clean[20][0]);
		$this->ata_topico->setTopicoUm($clean[10][0]);
		$this->ata_topico->setTopicoDois($clean[11][0]);
		$this->ata_topico->setTopicoTres($clean[12][0]);
		$this->ata_topico->setTopicoQuatro($clean[13][0]);
		$this->ata_topico->setTopicoCinco($clean[14][0]);
		$this->ata_topico->setTopicoSeis($clean[15][0]);
		$this->ata_topico->setTopicoSete($clean[16][0]);
		$this->ata_topico->setTopicoOito($clean[17][0]);
		$this->ata_topico->setTopicoNove($clean[18][0]);
		$this->ata_topico->setTopicoDez($clean[19][0]);
		$this->ata_topico->cadastroAtaReuniaoMensalTopico();

		if ($this->ata->alteraAtaReuniaoMensal($clean[20][0])) {
/*
            $idFuncionalidade       = 2;
            $remetente              = addslashes($_POST["fk_seqCadastAtualizadoPor"]);
            $descricao              = "Houveram alterações na Ata de Reunião Mensal";
            $idOrg                  = $_POST['fk_idOrganismoAfiliado'];
            $this->ticket->cadastroTicketAltomatico($idFuncionalidade,$descricao,$remetente,$idOrg,$_POST['fk_idAtaReuniaoMensal']);
*/
			echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaAtaReuniaoMensal&salvo=1';
			</script>";
		} else {
			echo
            "<script type='text/javascript'>
			alert ('N�o foi poss�vel alterar a Ata!');
			window.location = '../painelDeControle.php?corpo=buscaAtaReuniaoMensal';
			</script>";
		}
            }else{    
                echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essas informacoes pois os valores informados n\u00e3o correspondem aos tipos aceitos!');
                    window.location = '../painelDeControle.php?corpo=buscaAtaReuniaoMensal';
                                </script>";
            }
	}
}

?>