<?php

@include_once("model/transferenciaMembroClass.php");
@include_once("../model/transferenciaMembroClass.php");

@include_once("model/membroOAClass.php");
@include_once("../model/membroOAClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once("model/organismoClass.php");
@include_once("../model/organismoClass.php");

//@include_once ("../model/criaSessaoClass.php");

class transferenciaMembroController {

	private $transferenciaMembro;
	//private $sessao;

	public function __construct()
	{
		//$this->sessao 		= new criaSessao();
		$this->transferenciaMembro 		= new transferenciaMembro();
		$this->membroOA 				= new membroOA();
		$this->usuario					= new Usuario();
		$this->organismo				= new organismo();
		
	}

	public function cadastroTransferenciaMembro() {

		if(!$this->transferenciaMembro->verificaSeJaCadastrado($_POST['seqCadastMembro'],date("Y-m-d"),$_POST['fk_idOrganismoAfiliadoOrigem'],$_POST['fk_idOrganismoAfiliadoDestino']))
		{
			//Atualizar OA de Atuação no ORCZ
			
			//Selecionar a sigla do OA
			$siglaOa="";
			$resultado3 = $this->organismo->listaOrganismo(null,null,null,null,null,$_POST['fk_idOrganismoAfiliadoDestino']);
			if($resultado3)
			{
				foreach($resultado3 as $vetor3)
				{
					$siglaOa = $vetor3['siglaOrganismoAfiliado'];
				}
			}
			
			//Atualizar OA atuação
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>atualizaOaAtuacao('".$_POST['seqCadastMembro']."','".$siglaOa."');</script>";
			//exit();
			//Atualizar organismo na tabela MEMBROOA
			$this->membroOA->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliadoDestino']);
			$this->membroOA->setSeqCadastMembroOa($_POST['seqCadastMembro']);
			$this->membroOA->atualizaOrganismoMembroOA();
		
			/**
			 * Notificar Organismo Destino que houve uma transferência nova
			 */
			
			$resultado = $this->organismo->listaOrganismo(null,null,null,null,null,$_POST['fk_idOrganismoAfiliadoOrigem']);
			
			if ($resultado) {
				foreach ($resultado as $vetor) {

					switch ($vetor['classificacaoOrganismoAfiliado']) {
						case 1:
							$classificacao = "Loja";
							break;
						case 2:
							$classificacao = "Pronaos";
							break;
						case 3:
							$classificacao = "Capítulo";
							break;
						case 4:
							$classificacao = "Heptada";
							break;
						case 5:
							$classificacao = "Atrium";
							break;
					}
					switch ($vetor['tipoOrganismoAfiliado']) {
						case 1:
							$tipo = "R+C";
							break;
						case 2:
							$tipo = "TOM";
							break;
					}
				}
				$nomeOAOrigem = $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado'];
			}
			
			$resultado = $this->organismo->listaOrganismo(null,null,null,null,null,$_POST['fk_idOrganismoAfiliadoDestino']);
			$siglaOADestino="";
			if ($resultado) {
				foreach ($resultado as $vetor) {

					switch ($vetor['classificacaoOrganismoAfiliado']) {
						case 1:
							$classificacao = "Loja";
							break;
						case 2:
							$classificacao = "Pronaos";
							break;
						case 3:
							$classificacao = "Capítulo";
							break;
						case 4:
							$classificacao = "Heptada";
							break;
						case 5:
							$classificacao = "Atrium";
							break;
					}
					switch ($vetor['tipoOrganismoAfiliado']) {
						case 1:
							$tipo = "R+C";
							break;
						case 2:
							$tipo = "TOM";
							break;
					}
					$siglaOADestino = $vetor['siglaOrganismoAfiliado'];
				}
				$nomeOADestino = $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado'];
			}
			
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Membro novo a caminho";
			$tipoNotificacao=7;//Transferência
			$remetenteNotificacao=$_POST['usuario'];
			$mensagemNotificacao="O membro ".$_POST['nome'].", foi transferido do Organismo ".$nomeOAOrigem." para o Organismo ".$nomeOADestino." em ".date("d/m/Y")." - ".date("H:i:s"). " por ".$_POST['nomeUsuarioTransferencia'];
		
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários do OA destino
			$resultadoUsuarios = $this->usuario->listaUsuario($siglaOADestino);
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['seqCadast']."\"";
					}else{
						echo ",\"".$vetor['seqCadast']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
			
			//Setar variáveis
			$this->transferenciaMembro->setNome($_POST['nome']);
			$this->transferenciaMembro->setCodigoAfiliacao($_POST['codigoAfiliacao']);
			$this->transferenciaMembro->setSeqCadastMembro($_POST['seqCadastMembro']);
			$this->transferenciaMembro->setFk_idOrganismoAfiliadoOrigem($_POST['fk_idOrganismoAfiliadoOrigem']);
			$this->transferenciaMembro->setFk_idOrganismoAfiliadoDestino($_POST['fk_idOrganismoAfiliadoDestino']);
			$this->transferenciaMembro->setMotivoTransferencia($_POST['motivoTransferencia']);
			$this->transferenciaMembro->setOutrasInformacoes($_POST['outrasInformacoes']);
			$this->transferenciaMembro->setObservacoes($_POST['observacoes']);
		    $this->transferenciaMembro->setSiglaOAOrigem($_POST['siglaOAOrigem']);
		    $this->transferenciaMembro->setUsuario($_POST['usuario']);
		    $this->transferenciaMembro->setMestreOAOrigem($_POST['mestreOAOrigem']);
		    $this->transferenciaMembro->setSecretarioOAOrigem($_POST['secretarioOAOrigem']);
		   
			//Cadastrar ata e pegar ultimo id inserido
			$return = $this->transferenciaMembro->cadastroTransferenciaMembro();
			 		
			if ($return) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=listaMembrosTransferidos&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar esse membro!');
	                    window.location = '../painelDeControle.php?corpo=buscaTransferenciaMembro';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaTransferenciaMembro&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaTransferenciaMembro() {
		$retorno = $this->transferenciaMembro->listaTransferenciaMembro();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaTransferenciaPeloSeqCadast($seqCadast) {

		$resultado = $this->transferenciaMembro->buscaMembroOaPeloSeqCadast($seqCadast);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);"]);
				$this->transferenciaMembro->setCodigoAfiliacao($vetor['codigoAfiliacao']);
				$this->transferenciaMembro->setNomeMembroOa($vetor['nome']);
			}

			return $this->transferenciaMembro;
		} else {
			return false;
		}
	}

}

?>