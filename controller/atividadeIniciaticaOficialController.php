<?php

@include_once("../model/atividadeIniciaticaOficialClass.php");
@include_once("model/atividadeIniciaticaOficialClass.php");

class atividadeIniciaticaOficialController {

    private $atividadeIniciaticaOficial;

    public function __construct() {

        $this->atividadeIniciaticaOficial = new atividadeIniciaticaOficial();
    }

    public function cadastroAtividadeIniciaticaOficial() {

        $this->atividadeIniciaticaOficial->setAnoAtividadeIniciaticaOficial($_POST["anoAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setAnotacoesAtividadeIniciaticaOficial($_POST["anotacoesAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setTipoAtividadeIniciaticaOficial($_POST["tipoAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setFk_idOrganismoAfiliado($_POST["fk_idOrganismoAfiliado"]);
        $this->atividadeIniciaticaOficial->setMestreAtividadeIniciaticaOficial($_POST["mestreAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setMestreAuxAtividadeIniciaticaOficial($_POST["mestreAuxAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setArquivistaAtividadeIniciaticaOficial($_POST["arquivistaAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setCapelaoAtividadeIniciaticaOficial($_POST["capelaoAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setMatreAtividadeIniciaticaOficial($_POST["matreAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setGrandeSacerdotisaAtividadeIniciaticaOficial($_POST["grandeSacerdotisaAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setGuiaAtividadeIniciaticaOficial($_POST["guiaAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setGuardiaoInternoAtividadeIniciaticaOficial($_POST["guardiaoInternoAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setGuardiaoExternoAtividadeIniciaticaOficial($_POST["guardiaoExternoAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setArchoteAtividadeIniciaticaOficial($_POST["portadorArchoteAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setMedalhistaAtividadeIniciaticaOficial($_POST["medalhistaAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setArautoAtividadeIniciaticaOficial($_POST["arautoAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setAdjutorAtividadeIniciaticaOficial($_POST["adjutorAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setSonoplastaAtividadeIniciaticaOficial($_POST["sonoplastaAtividadeIniciaticaOficial"]);
        $this->atividadeIniciaticaOficial->setAnotacoesAtividadeIniciaticaOficial($_POST["anotacoesAtividadeIniciaticaOficial"]);
        //echo "<pre>";print_r($_POST);echo "</pre>";

        if ($this->atividadeIniciaticaOficial->cadastroAtividadeIniciaticaOficial()) {

            echo "<script type='text/javascript'>
                    alert('Atividade Iniciática cadastrada com sucesso!');
		    		window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciaticaOficial';
		  		</script>";

        } else {

            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar essa Atividade Iniciática!');
                    window.location = '../painelDeControle.php?corpo=cadastroAtividadeIniciaticaOficial';
		  		</script>";

        }
    }

    public function listaAtividadeIniciaticaOficial($oa='') {
    	
        if (!isset($_POST['anoAtividadeIniciaticaOficial'])) {
            $resultado = $this->atividadeIniciaticaOficial->listaAtividadeIniciaticaOficial($oa);
        } else {
        	/*
            $this->atividadeIniciaticaOficial->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->atividadeIniciaticaOficial->buscaNomeOrganismo();
        	*/
        }

        return $resultado;
    }
    
	public function buscaAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial) {

        $resultado = $this->atividadeIniciaticaOficial->buscaIdAtividadeIniciaticaOficial($idAtividadeIniciaticaOficial);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);
                $this->atividadeIniciaticaOficial->setAnoAtividadeIniciaticaOficial($vetor["anoAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setDataRegistroAtividadeIniciaticaOficial(substr($vetor['dataRegistroAtividadeIniciaticaOficial'],8,2)."/".substr($vetor['dataRegistroAtividadeIniciaticaOficial'],5,2)."/".substr($vetor['dataRegistroAtividadeIniciaticaOficial'],0,4)." - ".substr($vetor['dataRegistroAtividadeIniciaticaOficial'],10,6));
                $this->atividadeIniciaticaOficial->setDataAtualizadaAtividadeIniciaticaOficial(substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'],8,2)."/".substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'],5,2)."/".substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'],0,4)." - ".substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'],10,6));
                $this->atividadeIniciaticaOficial->setAnotacoesAtividadeIniciaticaOficial($vetor["anotacoesAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setTipoAtividadeIniciaticaOficial($vetor["tipoAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setMestreAtividadeIniciaticaOficial($vetor["mestreAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setMestreAuxAtividadeIniciaticaOficial($vetor["mestreAuxAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setArquivistaAtividadeIniciaticaOficial($vetor["arquivistaAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setCapelaoAtividadeIniciaticaOficial($vetor["capelaoAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setMatreAtividadeIniciaticaOficial($vetor["matreAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setGrandeSacerdotisaAtividadeIniciaticaOficial($vetor["grandeSacerdotisaAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setGuiaAtividadeIniciaticaOficial($vetor["guiaAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setGuardiaoInternoAtividadeIniciaticaOficial($vetor["guardiaoInternoAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setGuardiaoExternoAtividadeIniciaticaOficial($vetor["guardiaoExternoAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setArchoteAtividadeIniciaticaOficial($vetor["archoteAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setMedalhistaAtividadeIniciaticaOficial($vetor["medalhistaAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setArautoAtividadeIniciaticaOficial($vetor["arautoAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setAdjutorAtividadeIniciaticaOficial($vetor["adjutorAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setSonoplastaAtividadeIniciaticaOficial($vetor["sonoplastaAtividadeIniciaticaOficial"]);
                $this->atividadeIniciaticaOficial->setFk_idOrganismoAfiliado($vetor["fk_idOrganismoAfiliado"]);
            }

            return $this->atividadeIniciaticaOficial;
        } else {
            return false;
        }
    }

    public function alteraOficiaisAtividadeIniciaticaOficial() {

        $idEquipe = $_POST['idAtividadeIniciaticaOficial'];

        $this->atividadeIniciaticaOficial->setMestreAtividadeIniciaticaOficial($_POST["mestreAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setMestreAuxAtividadeIniciaticaOficial($_POST["mestreAuxAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setArquivistaAtividadeIniciaticaOficial($_POST["arquivistaAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setCapelaoAtividadeIniciaticaOficial($_POST["capelaoAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setMatreAtividadeIniciaticaOficial($_POST["matreAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setGrandeSacerdotisaAtividadeIniciaticaOficial($_POST["grandeSacerdotisaAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setGuiaAtividadeIniciaticaOficial($_POST["guiaAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setGuardiaoInternoAtividadeIniciaticaOficial($_POST["guardiaoInternoAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setGuardiaoExternoAtividadeIniciaticaOficial($_POST["guardiaoExternoAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setArchoteAtividadeIniciaticaOficial($_POST["portadorArchoteAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setMedalhistaAtividadeIniciaticaOficial($_POST["medalhistaAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setArautoAtividadeIniciaticaOficial($_POST["arautoAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setAdjutorAtividadeIniciaticaOficial($_POST["adjutorAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setSonoplastaAtividadeIniciaticaOficial($_POST["sonoplastaAtividadeIniciaticaOficial$idEquipe"]);
        $this->atividadeIniciaticaOficial->setAnotacoesAtividadeIniciaticaOficial($_POST["anotacoesAtividadeIniciaticaOficial$idEquipe"]);
        //echo "<pre>";print_r($_POST);echo "</pre>";

        if ($this->atividadeIniciaticaOficial->alteraOficiaisAtividadeIniciaticaOficial($_POST['idAtividadeIniciaticaOficial'])) {

            echo
            "<script type='text/javascript'>
            alert ('Equipe Iniciática alterada com sucesso!');
            window.location = '../painelDeControle.php?corpo=buscaAtividadeIniciaticaOficial';
            </script>";

        } else {

            echo
                "<script type='text/javascript'>
            alert ('N\u00e3o foi poss\u00edvel alterar a Equipe Iniciática!');
            //window.location = '../painelDeControle.php?corpo=alteraAtividadeIniciaticaOficial&idAtividadeIniciaticaOficial=" . $_POST['idAtividadeIniciaticaOficial'] . "';
            </script>";

        }

    }
}

?>