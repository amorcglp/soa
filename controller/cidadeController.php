<?php

@include_once("../model/cidadeClass.php");
@include_once("model/cidadeClass.php");

class cidadeController {

    private $cidade;

    public function __construct() {

        $this->cidade = new Cidade();
    }

    public function cadastroCidade() {
		
        //$this->organismo->setNomeOrganismoAfiliado($_POST["nomeOrganismoAfiliado"]);
        /*
    	if ($this->cidade->cadastroOrganismo()) {

            echo "<script type='text/javascript'>
                    alert('Cidade cadastrado com sucesso!');
		    window.location = '../painelDeControle.php?corpo=buscaCidade';
		  </script>";
        } else {
            echo "<script type='text/javascript'>
                    alert('N\u00e3o foi poss\u00edvel cadastrar esse Cidade!');
                    window.location = '../painelDeControle.php?corpo=cadastroCidade';
		  </script>";
        }*/
    }

    public function criarComboBox($id = 0,$estado=null) {
    	
    	if($estado==null)
    	{
        	$resultado = $this->cidade->listaCidade();
    	}else{
    		$resultado = $this->cidade->listaCidade($estado);
    	}

        if ($resultado) {

            foreach ($resultado as $vetor) {
                if (($id != 0 && $id == $vetor["id"])) {

                    $selecionado = 'selected = selected';
                } else {
                    $selecionado = '';
                }
                
	            echo '<option value="' . $vetor['id'] . '" ' . $selecionado . '>'. $vetor["nome"] . '</option>';
            }
        }
    }
	/*
    public function listaidOrganismoAfiliado() {
        $retorno = $this->organismo->listaOrganismo();
        $listaOrganismo = "";

        foreach ($retorno as $dados) {
            $listaOrganismo[] = $dados['idOrganismoAfiliado'];
        }

        if ($listaOrganismo) {
            return $listaOrganismo;
        } else {
            return false;
        }
    }

    public function listaOrganismo() {
    	
        if (!isset($_POST['nomeOrganismoAfiliado'])) {
            $resultado = $this->organismo->listaOrganismo();
        } else {
        	/*
            $this->organismo->set("nomeOrganismoAfiliado", $_POST['nomeOrganismoAfiliado']);
            $resultado = $this->organismo->buscaNomeOrganismo();
        	*/
/*
        }

        return $resultado;
    }
    
	public function buscaOrganismo($idOrganismo) {

        $resultado = $this->organismo->buscaIdOrganismo($idOrganismo);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				              
                $this->organismo->setNomeOrganismoAfiliado($vetor["nomeOrganismoAfiliado"]);
		        $this->organismo->setSiglaOrganismoAfiliado($vetor["siglaOrganismoAfiliado"]);
		        $this->organismo->setPaisOrganismoAfiliado($vetor['paisOrganismoAfiliado']);
		        $this->organismo->setTipoOrganismoAfiliado($vetor["tipoOrganismoAfiliado"]);
		        $this->organismo->setStatusOrganismoAfiliado($vetor["statusOrganismoAfiliado"]);
		        $this->organismo->setClassificacaoOrganismoAfiliado($vetor["classificacaoOrganismoAfiliado"]);
		        $this->organismo->setCepOrganismoAfiliado(substr($vetor['cepOrganismoAfiliado'],0,5). "-" .substr($vetor['cepOrganismoAfiliado'],5,3));
		        $this->organismo->setFk_idRegiaoOrdemRosacruz($vetor["fk_idRegiaoOrdemRosacruz"]);
		        $this->organismo->setCodigoAfiliacaoOrganismoAfiliado($vetor["codigoAfiliacaoOrganismoAfiliado"]);
		        $this->organismo->setEnderecoOrganismoAfiliado($vetor["enderecoOrganismoAfiliado"]);
		        $this->organismo->setNumeroOrganismoAfiliado($vetor["numeroOrganismoAfiliado"]);
		        $this->organismo->setEmailOrganismoAfiliado($vetor["emailOrganismoAfiliado"]);
		        $this->organismo->setSituacaoOrganismoAfiliado($vetor["situacaoOrganismoAfiliado"]);
		        $this->organismo->setFk_idCidade($vetor["fk_idCidade"]);
		        $this->organismo->setFk_idCidade($vetor["fk_idCidade"]);
		        $this->organismo->setBairroOrganismoAfiliado($vetor["bairroOrganismoAfiliado"]);
		        $this->organismo->setComplementoOrganismoAfiliado($vetor["complementoOrganismoAfiliado"]);
		        $this->organismo->setCnpjOrganismoAfiliado($vetor["cnpjOrganismoAfiliado"]);
		        $this->organismo->setOperadoraCelularOrganismoAfiliado($vetor["operadoraCelularOrganismoAfiliado"]);
		        $this->organismo->setCelularOrganismoAfiliado($vetor["celularOrganismoAfiliado"]);
		        $this->organismo->setTelefoneFixoOrganismoAfiliado($vetor["telefoneFixoOrganismoAfiliado"]);
		        $this->organismo->setOutroTelefoneOrganismoAfiliado($vetor["outroTelefoneOrganismoAfiliado"]);
		        $this->organismo->setFaxOrganismoAfiliado($vetor["faxOrganismoAfiliado"]);
		        $this->organismo->setCaixaPostalOrganismoAfiliado($vetor["caixaPostalOrganismoAfiliado"]);
		        $this->organismo->setCepCaixaPostalOrganismoAfiliado($vetor["cepCaixaPostalOrganismoAfiliado"]);
		        $this->organismo->setEnderecoCorrespondenciaOrganismoAfiliado($vetor["enderecoCorrespondenciaOrganismoAfiliado"]);
		        
            }

            return $this->organismo;
        } else {
            return false;
        }
    }

    public function alteraOrganismo() {
		
    	$this->organismo->setNomeOrganismoAfiliado($_POST["nomeOrganismoAfiliado"]);
        $this->organismo->setSiglaOrganismoAfiliado($_POST["siglaOrganismoAfiliado"]);
        $this->organismo->setPaisOrganismoAfiliado($_POST['paisOrganismoAfiliado']);
        $this->organismo->setTipoOrganismoAfiliado($_POST["tipoOrganismoAfiliado"]);
        $this->organismo->setStatusOrganismoAfiliado($_POST["statusOrganismoAfiliado"]);
        $this->organismo->setClassificacaoOrganismoAfiliado($_POST["classificacaoOrganismoAfiliado"]);
        $this->organismo->setCepOrganismoAfiliado(substr($_POST['cepOrganismoAfiliado'],0,5).substr($_POST['cepOrganismoAfiliado'],6,3));
        $this->organismo->setFk_idRegiaoOrdemRosacruz($_POST["fk_idRegiaoOrdemRosacruz"]);
        $this->organismo->setCodigoAfiliacaoOrganismoAfiliado($_POST["codigoAfiliacaoOrganismoAfiliado"]);
        $this->organismo->setEnderecoOrganismoAfiliado($_POST["enderecoOrganismoAfiliado"]);
        $this->organismo->setNumeroOrganismoAfiliado($_POST["numeroOrganismoAfiliado"]);
        $this->organismo->setEmailOrganismoAfiliado($_POST["emailOrganismoAfiliado"]);
        $this->organismo->setSituacaoOrganismoAfiliado($_POST["situacaoOrganismoAfiliado"]);
        $this->organismo->setFk_idCidade($_POST["fk_idCidade"]);
        $this->organismo->setFk_idCidade($_POST["fk_idCidade"]);
        $this->organismo->setBairroOrganismoAfiliado($_POST["bairroOrganismoAfiliado"]);
        $this->organismo->setComplementoOrganismoAfiliado($_POST["complementoOrganismoAfiliado"]);
        $this->organismo->setCnpjOrganismoAfiliado($_POST["cnpjOrganismoAfiliado"]);
        $this->organismo->setOperadoraCelularOrganismoAfiliado($_POST["operadoraCelularOrganismoAfiliado"]);
        $this->organismo->setCelularOrganismoAfiliado($_POST["celularOrganismoAfiliado"]);
        $this->organismo->setTelefoneFixoOrganismoAfiliado($_POST["telefoneFixoOrganismoAfiliado"]);
        $this->organismo->setOutroTelefoneOrganismoAfiliado($_POST["outroTelefoneOrganismoAfiliado"]);
        $this->organismo->setFaxOrganismoAfiliado($_POST["faxOrganismoAfiliado"]);
        $this->organismo->setCaixaPostalOrganismoAfiliado($_POST["caixaPostalOrganismoAfiliado"]);
        $this->organismo->setCepCaixaPostalOrganismoAfiliado($_POST["cepCaixaPostalOrganismoAfiliado"]);
        $this->organismo->setEnderecoCorrespondenciaOrganismoAfiliado($_POST["enderecoCorrespondenciaOrganismoAfiliado"]);

        if ($this->organismo->alteraOrganismo($_POST['idOrganismoAfiliado'])) {

            echo
            "<script type='text/javascript'>
			alert ('Organismo Afiliado alterado com sucesso!');
			window.location = '../painelDeControle.php?corpo=buscaOrganismo';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
			alert ('N\u00e3o foi poss\u00edvel alterar o Organismo Afiliado!');
			window.location = '../painelDeControle.php?corpo=alteraOrganismo';
			</script>";
        }
    }
	
    
    public function removeOrganismo($idOrganismoAfiliado) {
        $resultado = $this->organismo->removeOrganismo($idOrganismoAfiliado);

        if ($resultado) {
            echo
            "<script type='text/javascript'>
				alert ('Organismo Afiliado exclu\u00eddo com sucesso!');
				window.location = '../painelDeControle.php?url=buscaOrganismo&id=" . $idOrganismoAfiliado . "';
				</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('N\u00e3o foi poss\u00edvel excluir o Organismo Afiliado!');
				window.location = '../painelDeControle.php?url=alteraOrganismo&id=" . $idOrganismoAfiliado . "';
				</script>";
        }
    }
    */

}

?>