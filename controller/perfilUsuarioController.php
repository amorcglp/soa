<?php

@include_once("../model/perfilUsuarioClass.php");
@include_once("model/perfilUsuarioClass.php");

class perfilController {

	private $perfil;

	public function __construct() {

		$this->perfil = new perfilUsuario();
	}
    
	public function alteraUsuario() {
            
            $hash = password_hash($_POST['senhaUsuario'], PASSWORD_DEFAULT);
        //echo "hash: ".$hash;
        $this->perfil->setNomeUsuario($_POST['nomeUsuario']);
        $this->perfil->setEmailUsuario($_POST['emailUsuario']);
        $this->perfil->setTelefoneResidencialUsuario($_POST['telefoneResidencialUsuario']);
        $this->perfil->setTelefoneComercialUsuario($_POST['telefoneComercialUsuario']);
        $this->perfil->setCelularUsuario($_POST['celularUsuario']);
        $this->perfil->setLoginUsuario($_POST['loginUsuario']);
        $this->perfil->setSenhaUsuario($hash);
        //exit();
//        if ($_FILES['userfile']['name'] != "") {
//
//        	$caminho = "img/perfil/" . basename($_POST["seqCadast"].".".$_FILES['userfile']['name']);
//			$this->perfil->setAvatarUsuario($caminho);
//
//            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/perfil/';
//			//$uploaddir = 'C:\/wamp\/www\/soa_mvc\/img\/perfil\/';
//			//$uploaddir = 'C:\/xampp\/htdocs\/img\/perfil\/';
//			$uploadfile = $uploaddir . basename($_POST["seqCadast"].".".$_FILES['userfile']['name']);
//
//			//echo '<pre>';
//                        if (is_dir($uploaddir) ){
//                                if(is_writable($uploaddir)) {
//                                    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
//                                            //echo "Arquivo enviado com sucesso!\n";
//                                            //echo "<script type='text/javascript'>window.location = '../painelDeControle.php?corpo=perfilUsuario'</script>";
//                                    } else {
//                                            echo "<script type='text/javascript'>alert('Erro ao enviar a imagem!');</script>";
//                                            //echo "Possível ataque de upload de arquivo!\n";
//                                    }
//                                }else{
//                                    echo "<script type='text/javascript'>alert('Upload directory is not writable');</script>";
//                                }
//                        }else{
//                            echo "<script type='text/javascript'>alert('Upload directory is not exist!');</script>";
//                        }
//                        /*
//		 	echo 'Aqui está mais informações de debug:';
//			print_r($_FILES);
//			print "</pre>";
//			exit();
//			*/
//        }
        
        if ($this->perfil->alteraUsuario($_POST['seqCadast'])) {

            echo
            "<script type='text/javascript'>
				window.location = '../painelDeControle.php?corpo=perfilUsuario';
			</script>";
        } else {
            echo
            "<script type='text/javascript'>
				alert ('N\u00e3o foi poss\u00edvel alterar os dados cadastrais!');
				window.location = '../painelDeControle.php?corpo=perfilUsuario';
			</script>";
        }
        
    }
    
	public function listaAvatarUsuario($seqCadast) {
	
		$resultado = $this->perfil->listaAvatarUsuario($seqCadast);
		
		if ($resultado) {

			foreach ($resultado as $vetor) {
				if ($vetor['avatarUsuario'] != "") {
					$this->perfil->setAvatarUsuario($vetor['avatarUsuario']);
				} else {
					$this->perfil->setAvatarUsuario("img/default-user.png");
				}
			}
			return $this->perfil;
			
		} else {
			
			return false;
			
		}
	}
	
	public function buscaUsuario($idUsuario) {

        $resultado = $this->perfil->buscaIdUsuario($idUsuario);

        if ($resultado) {
		
            foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);
                $this->perfil->setNomeUsuario($vetor['nomeUsuario']);
                $this->perfil->setEmailUsuario($vetor['emailUsuario']);
                $this->perfil->setTelefoneResidencialUsuario($vetor['telefoneResidencialUsuario']);
                $this->perfil->setTelefoneComercialUsuario($vetor['telefoneComercialUsuario']);
                $this->perfil->setCelularUsuario($vetor['celularUsuario']);
                $this->perfil->setLoginUsuario($vetor['loginUsuario']);
                $this->perfil->setSenhaUsuario("");
                $this->perfil->setCodigoDeAfiliacao($vetor['codigoDeAfiliacao']);
                $this->perfil->setFkIdDepartamento($vetor['fk_idDepartamento']);
                $this->perfil->setCienteAssinaturaDigital($vetor['cienteAssinaturaDigital']);
                $this->perfil->setDataAssinaturaDigital($vetor['dataAssinaturaDigital']);
            }

            return $this->perfil;
        } else {
            return false;
        }
    }
    
	public function listaContato() {
		
        $resultado = $this->perfil->listaContato();
        
        return $resultado;
    }
    
}