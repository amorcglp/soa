<?php

@include_once("model/termoVoluntariadoClass.php");
@include_once("../model/termoVoluntariadoClass.php");

@include_once("model/usuarioClass.php");
@include_once("../model/usuarioClass.php");

@include_once ("../model/criaSessaoClass.php");

class termoVoluntariadoController {

	private $termoVoluntariado;
	private $sessao;

	public function __construct()
	{
		$this->sessao               = new criaSessao();
		$this->termoVoluntariado    = new termoVoluntariado();
		$this->usuario              = new Usuario();
		
	}

	public function cadastroTermoVoluntariado() {

                $ultimo_id="";
                    
		$this->termoVoluntariado->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->termoVoluntariado->setNomeVoluntario($_POST['nomeVoluntario']);
		$this->termoVoluntariado->setCodigoAfiliacao($_POST['codigoAfiliacao']);
		$this->termoVoluntariado->setCpf($_POST['cpf']);
		$this->termoVoluntariado->setRg($_POST['rg']);
		$this->termoVoluntariado->setProfissao($_POST['profissao']);
                $this->termoVoluntariado->setNacionalidade($_POST['nacionalidade']);
                $this->termoVoluntariado->setEmail($_POST['email']);
                $this->termoVoluntariado->setLogradouro($_POST['logradouro']);
                $this->termoVoluntariado->setNumero($_POST['numero']);
                $this->termoVoluntariado->setComplemento($_POST['complemento']);
                $this->termoVoluntariado->setBairro($_POST['bairro']);
                $this->termoVoluntariado->setCep($_POST['cep']);
                $this->termoVoluntariado->setCidade($_POST['cidade']);
                $this->termoVoluntariado->setUf($_POST['uf']);
                $this->termoVoluntariado->setTelefoneResidencial($_POST['telefoneResidencial']);
                $this->termoVoluntariado->setTelefoneComercial($_POST['telefoneComercial']);
                $this->termoVoluntariado->setCelular($_POST['celular']);
                $this->termoVoluntariado->setAreaTrabalhoVoluntario($_POST['areaTrabalhoVoluntario']);
                $this->termoVoluntariado->setTarefaEspecifica($_POST['tarefaEspecifica']);
                $this->termoVoluntariado->setDiasSemanaVoluntariado($_POST['diasSemanaVoluntariado']);
                $this->termoVoluntariado->setAnoInicial($_POST['anoInicial']);
                $this->termoVoluntariado->setAnoFinal($_POST['anoFinal']);
		$this->termoVoluntariado->setUsuario($this->sessao->getValue("seqCadast"));
                $this->termoVoluntariado->setDataTermoVoluntariado(substr($_POST['dataTermoVoluntariado'],6,4)."-".substr($_POST['dataTermoVoluntariado'],3,2)."-".substr($_POST['dataTermoVoluntariado'],0,2));

		//Cadastrar termoVoluntariado e pegar ultimo id inserido
		if(!$this->termoVoluntariado->verificaSeJaExiste())
		{
			$ultimo_id = $this->termoVoluntariado->cadastroTermoVoluntariado();
		
			/**
			 * Notificar GLP que a termoVoluntariado foi cadastrada
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Novo Termo de Voluntariado Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Termo de Voluntariado Entregue pelo Organismo ".$_POST['nomeOrganismoTermo'].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuarioTermo'];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/dtermoVoluntariadopicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dtermoVoluntariadoTables/jquery.dtermoVoluntariadoTables.js\"></script>
			<script src=\"../js/plugins/dtermoVoluntariadoTables/dtermoVoluntariadoTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dtermoVoluntariadoTables/dtermoVoluntariadoTables.responsive.js\"></script>
			<script src=\"../js/plugins/dtermoVoluntariadoTables/dtermoVoluntariadoTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
			//exit();	
			if ($ultimo_id!="") {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaTermoVoluntariado&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Nao foi possivel cadastrar esse termo!');
	                    window.location = '../painelDeControle.php?corpo=buscaTermoVoluntariado';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaTermoVoluntariado&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaTermoVoluntariado() {
		$retorno = $this->termoVoluntariado->listaTermoVoluntariado();
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}

	public function buscaTermoVoluntariado($idTermoVoluntariado) {

		$resultado = $this->termoVoluntariado->buscarIdTermoVoluntariado($idTermoVoluntariado);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->termoVoluntariado->setIdTermoVoluntariado($vetor['idTermoVoluntariado']);
				$this->termoVoluntariado->setFk_idOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
				$this->termoVoluntariado->setNomeVoluntario($vetor['nomeVoluntario']);
				$this->termoVoluntariado->setCodigoAfiliacao($vetor['codigoAfiliacao']);
				$this->termoVoluntariado->setCpf($vetor['cpf']);
				$this->termoVoluntariado->setRg($vetor['rg']);
				$this->termoVoluntariado->setProfissao($vetor['profissao']);
				$this->termoVoluntariado->setNacionalidade($vetor['nacionalidade']);
				$this->termoVoluntariado->setEmail($vetor['email']);
                                $this->termoVoluntariado->setLogradouro($vetor['logradouro']);
                                $this->termoVoluntariado->setNumero($vetor['numero']);
                                $this->termoVoluntariado->setComplemento($vetor['complemento']);
                                $this->termoVoluntariado->setBairro($vetor['bairro']);
                                $this->termoVoluntariado->setCep($vetor['cep']);
                                $this->termoVoluntariado->setCidade($vetor['cidade']);
                                $this->termoVoluntariado->setUf($vetor['uf']);
                                $this->termoVoluntariado->setTelefoneResidencial($vetor['telefoneResidencial']);
                                $this->termoVoluntariado->setTelefoneComercial($vetor['telefoneComercial']);
                                $this->termoVoluntariado->setCelular($vetor['celular']);
                                $this->termoVoluntariado->setAreaTrabalhoVoluntario($vetor['areaTrabalhoVoluntario']);
                                $this->termoVoluntariado->setTarefaEspecifica($vetor['tarefaEspecifica']);
                                $this->termoVoluntariado->setDiasSemanaVoluntariado($vetor['diasSemanaVoluntariado']);
                                $this->termoVoluntariado->setAnoInicial($vetor['anoInicial']);
                                $this->termoVoluntariado->setAnoFinal($vetor['anoFinal']);
                                $this->termoVoluntariado->setDataTermoVoluntariado(substr($vetor['dataTermoVoluntariado'],8,2)."/".substr($vetor['dataTermoVoluntariado'],5,2)."/".substr($vetor['dataTermoVoluntariado'],0,4));
                                
			}
			return $this->termoVoluntariado;
		} else {
			return false;
		}
	}
	

	public function alteraTermoVoluntariado() {

		//$this->termoVoluntariado->setFk_idOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->termoVoluntariado->setNomeVoluntario($_POST['nomeVoluntario']);
		$this->termoVoluntariado->setCodigoAfiliacao($_POST['codigoAfiliacao']);
		$this->termoVoluntariado->setCpf($_POST['cpf']);
		$this->termoVoluntariado->setRg($_POST['rg']);
		$this->termoVoluntariado->setProfissao($_POST['profissao']);
                $this->termoVoluntariado->setNacionalidade($_POST['nacionalidade']);
                $this->termoVoluntariado->setEmail($_POST['email']);
                $this->termoVoluntariado->setLogradouro($_POST['logradouro']);
                $this->termoVoluntariado->setNumero($_POST['numero']);
                $this->termoVoluntariado->setComplemento($_POST['complemento']);
                $this->termoVoluntariado->setBairro($_POST['bairro']);
                $this->termoVoluntariado->setCep($_POST['cep']);
                $this->termoVoluntariado->setCidade($_POST['cidade']);
                $this->termoVoluntariado->setUf($_POST['uf']);
                $this->termoVoluntariado->setTelefoneResidencial($_POST['telefoneResidencial']);
                $this->termoVoluntariado->setTelefoneComercial($_POST['telefoneComercial']);
                $this->termoVoluntariado->setCelular($_POST['celular']);
                $this->termoVoluntariado->setAreaTrabalhoVoluntario($_POST['areaTrabalhoVoluntario']);
                $this->termoVoluntariado->setTarefaEspecifica($_POST['tarefaEspecifica']);
                $this->termoVoluntariado->setDiasSemanaVoluntariado($_POST['diasSemanaVoluntariado']);
                $this->termoVoluntariado->setAnoInicial($_POST['anoInicial']);
                $this->termoVoluntariado->setAnoFinal($_POST['anoFinal']);
		$this->termoVoluntariado->setUsuario($this->sessao->getValue("seqCadast"));
                $this->termoVoluntariado->setDataTermoVoluntariado(substr($_POST['dataTermoVoluntariado'],6,4)."-".substr($_POST['dataTermoVoluntariado'],3,2)."-".substr($_POST['dataTermoVoluntariado'],0,2));
                
		if ($this->termoVoluntariado->alteraTermoVoluntariado($_POST['fk_idTermoVoluntariado'])) {

			echo
                        "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaTermoVoluntariado&salvo=1';
			</script>";
		} else {
			echo
                        "<script type='text/javascript'>
			alert ('Nao foi possivel alterar o termo!');
			window.location = '../painelDeControle.php?corpo=buscaTermoVoluntariado';
			</script>";
		}
	}

}

?>