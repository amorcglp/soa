<?php

@include_once("model/planoAcaoOrganismoClass.php");

if(realpath('../model/planoAcaoOrganismoClass.php')){
    @include_once("../model/planoAcaoOrganismoClass.php");
}else{
    @include_once("../../model/planoAcaoOrganismoClass.php");
}

@include_once("model/planoAcaoOrganismoParticipanteClass.php");

if(realpath('../model/planoAcaoOrganismoParticipanteClass.php')){
    @include_once("../model/planoAcaoOrganismoParticipanteClass.php");
}else{
    @include_once("../../model/planoAcaoOrganismoParticipanteClass.php");
}

@include_once("model/usuarioClass.php");

if(realpath('../model/usuarioClass.php')){
    @include_once("../model/usuarioClass.php");
}else{
    @include_once("../../model/usuarioClass.php");
}

@include_once("model/organismoClass.php");

if(realpath('../model/organismoClass.php')){
    @include_once("../model/organismoClass.php");
}else{
    @include_once("../../model/organismoClass.php");
}

if(realpath('../model/criaSessaoClass.php')){
    @include_once ("../model/criaSessaoClass.php");
}else{
    @include_once ("../../model/criaSessaoClass.php");
}

if(realpath('../lib/functions.php')){
    @include_once ("../lib/functions.php");
}else{
    @include_once ("../../model/lib/functions.php");
}



class planoAcaoOrganismoController {

	private $plano;
	private $sessao;
	private $usuario;
	private $organismo;
	private $participante;

	public function __construct()
	{
		$this->sessao 		= new criaSessao();
		$this->plano 		= new PlanoAcaoOrganismo();
		$this->usuario		= new Usuario();
		$this->organismo	= new organismo();
		$this->participante	= new planoAcaoOrganismoParticipante();
		
	}

	public function cadastroPlanoAcaoOrganismo() {
		
		$this->plano->setFkIdOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->plano->setTituloPlano($_POST['tituloPlano']);
		$this->plano->setPrioridade($_POST['prioridade']);
		$this->plano->setDescricaoPlano($_POST['descricaoPlano']);
		$this->plano->setPalavraChave($_POST['palavraChave']);
		$this->plano->setCriadoPor($_POST['usuario']);
		$this->plano->setStatusPlano(1);

		//Cadastrar ata e pegar ultimo id inserido
		if(!$this->plano->verificaSeJaExiste())
		{
			$ultimo_id = $this->plano->cadastroPlanoAcaoOrganismo();
			
			//Cadastrar Oficiais que participaram da reuni�o
			$return=true;
			
			if(isset($_POST["ata_oa_mensal_oficiais"]))
			{
				$participantes=$_POST["ata_oa_mensal_oficiais"];
				for ($i=0;$i<count($participantes);$i++)
				{
					$this->participante	= new planoAcaoOrganismoParticipante();
					$this->participante->setFk_idPlanoAcaoOrganismo($ultimo_id);
					$this->participante->setSeqCadast($participantes[$i]);
					$this->participante->cadastroPlanoAcaoOrganismoParticipante();
					
				}
			}
			//Pegar o OA
			$resultado = $this->organismo->listaOrganismo(null,null,null,null,null,$_POST['fk_idOrganismoAfiliado']);
			$nomeOrganismo="--";
			if($resultado)
			{
				foreach($resultado as $vetor)
				{
					$nomeOrganismo = organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'],$vetor['tipoOrganismoAfiliado'],$vetor['nomeOrganismoAfiliado'],$vetor['siglaOrganismoAfiliado']);
				}
			}
			
			/**
			 * Notificar GLP que o plano foi cadastrado
			 */
			date_default_timezone_set('America/Sao_Paulo');
			$tituloNotificacao="Novo Plano de Acao do Organismo Entregue";
			$tipoNotificacao=6;//Entregue
			$remetenteNotificacao=$this->sessao->getValue("seqCadast");
			$mensagemNotificacao="Plano de Acao Entregue pelo Organismo ".$nomeOrganismo.", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuarioPlano'];
			
			echo "
			<meta charset=\"utf-8\">
			<script src=\"../js/jquery-2.1.1.js\"></script>
			<script src=\"../js/bootstrap.min.js\"></script>
			<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
			<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
			<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
			<script src=\"../js/functions3.js\"></script>
			<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
			<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
			<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
			<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
			<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
			<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
			<script src=\"../js/inspinia.js\"></script>
			<script src=\"../js/plugins/pace/pace.min.js\"></script>
			<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
			<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
			<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
		    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
		    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
			<script src=\"../js/jquery.uploadify.min.js\"></script>
			";
			echo "<script>var oficiaisMarcados = [";
			//Selecionar todos os usuários com departamento 2 e 3
			$resultadoUsuarios = $this->usuario->listaUsuario(null,"2,3");
			$i=0;
			if($resultadoUsuarios)
			{
				foreach ($resultadoUsuarios as $vetor)
				{
					if($i==0)
					{
						echo "\"".$vetor['idUsuario']."\"";
					}else{
						echo ",\"".$vetor['idUsuario']."\"";
					}	
					$i++;
				}
			}
			
			echo "];enviaNotificacaoGLP('".utf8_encode($tituloNotificacao)."',oficiaisMarcados,'".$tipoNotificacao."','".$remetenteNotificacao."','".utf8_encode($mensagemNotificacao)."');</script>";
					
			if ($return) {
	
				echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoOrganismo&salvo=1';
			 	</script>";
			} else {
				echo "<script type='text/javascript'>
	                    alert('Não foi possível cadastrar este plano!');
	                    window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoOrganismo';
			  </script>";
			}
		}else{
			echo "<script type='text/javascript'>
			   			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoOrganismo&jaCadastrado=1';
			 	</script>";
		}
	}

	public function listaPlanoAcaoOrganismo($pesquisar=null,$linha_inicial=null,$qtdRegistros=null) {
            
		$retorno = $this->plano->listaPlanoAcaoOrganismo(null,null,$pesquisar,$linha_inicial,$qtdRegistros);
		
		if ($retorno) {
			return $retorno;
		} else {
			return false;
		}
	}
        
        public function totalPlanoAcaoOrganismo($pesquisar=null,$linha_inicial=null,$qtdRegistros=null) {
            
		$retorno = $this->plano->listaPlanoAcaoOrganismo(null,null,$pesquisar,$linha_inicial,$qtdRegistros);
		
		if ($retorno) {
			return count($retorno);
		} else {
			return false;
		}
	}

	public function buscaPlanoAcaoOrganismo($idPlanoAcaoOrganismo) {

		$resultado = $this->plano->buscarIdPlanoAcaoOrganismo($idPlanoAcaoOrganismo);

		if ($resultado) {

			foreach ($resultado as $vetor) {
				$this->plano->setIdPlanoAcaoOrganismo($vetor['idPlanoAcaoOrganismo']);
				$this->plano->setTituloPlano($vetor['tituloPlano']);
				$this->plano->setPrioridade($vetor['prioridade']);
				$this->plano->setFkIdOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
				$this->plano->setDescricaoPlano($vetor['descricaoPlano']);
				$this->plano->setCriadoPor($vetor['nomeUsuario']);
				$this->plano->setDataCriacao($vetor['dataCriacao']);
				$this->plano->setStatusPlano($vetor['statusPlano']);
				$this->plano->setPalavraChave($vetor['palavrasChave']);
			}
			return $this->plano;
		} else {
			return false;
		}
	}
	

	public function alteraPlanoAcaoOrganismo() {

		//$this->plano->setFkIdOrganismoAfiliado($_POST['fk_idOrganismoAfiliado']);
		$this->plano->setTituloPlano($_POST['tituloPlano']);
		$this->plano->setPrioridade($_POST['prioridade']);
		$this->plano->setDescricaoPlano($_POST['descricaoPlano']);
		$this->plano->setPalavraChave($_POST['palavraChave']);
		$this->plano->setUltimoAtualizar($_POST['usuario']);
                
		//Remover participantes do plano
		$participante = new planoAcaoOrganismoParticipante();
		$participante->removeParticipantes($_POST['fk_idPlanoAcaoOrganismo']);
		//Cadastrar participantes do plano
                if(isset($_POST["ata_oa_mensal_oficiais"]))
                {    
			$participantes=$_POST["ata_oa_mensal_oficiais"];
			for ($i=0;$i<count($participantes);$i++)
			{
				$this->participante	= new planoAcaoOrganismoParticipante();
				$this->participante->setFk_idPlanoAcaoOrganismo($_POST['fk_idPlanoAcaoOrganismo']);
				$this->participante->setSeqCadast($participantes[$i]);
				$this->participante->cadastroPlanoAcaoOrganismoParticipante();
			}
                }
		if ($this->plano->alteraPlanoAcaoOrganismo($_POST['fk_idPlanoAcaoOrganismo'])) {

			echo
            "<script type='text/javascript'>
			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoOrganismo&salvo=1';
			</script>";
		} else {
			echo
            "<script type='text/javascript'>
			alert ('Não foi possível alterar o plano!');
			window.location = '../painelDeControle.php?corpo=buscaPlanoAcaoOrganismo';
			</script>";
		}
	}

}

?>