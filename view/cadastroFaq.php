<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Envio de feedback do SOA</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaFaq">Envio de feedback do SOA</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de feedback do SOA</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=faq">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="post" action="acoes/acaoCadastrar.php">
                        <input type="hidden" name="seq_cadast" id="seq_cadast">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nome</label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeUsuarioFaq" id="nomeUsuario" class="form-control" required="" onChange="this.value = this.value.toUpperCase()" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Código de Afiliação</label>
                            <div class="col-sm-10">
                                <input type="text" name="codigoDeAfiliacaoUsuarioFaq" id="codigoAfiliacaoUsuario" class="form-control" required="" onkeypress="return SomenteNumero(event)" onblur="retornaDados()" value="" style="max-width: 75px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">E-mail</label>
                            <div class="col-sm-10">
                                <input type="text" name="emailUsuarioFaq" id="emailUsuario" class="form-control" required="" onChange="this.value = this.value.toLowerCase()" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox();
                                    ?>
                                </select>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título da Pergunta</label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloPerguntaFaq" id="tituloPerguntaFaq" class="form-control" required="" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pergunta</label>
                            <div class="col-sm-10">
                                <textarea rows="7" name="perguntaFaq" id="perguntaFaq" class="form-control" style="max-width: 500px"></textarea>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" />
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>