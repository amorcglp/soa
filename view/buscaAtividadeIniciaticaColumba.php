<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Columbas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Columbas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Columbas</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciatica">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar para Atividades
                        </a>
                        <?php if(!$usuarioApenasLeitura){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeIniciaticaColumba">
                            <i class="fa fa-plus"></i> Cadastrar Nova
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <!--
                                <th>Data de Admissão: </th>
                                <th>Data da Instalação: </th>
                                <th>Ativa até: </th>
                                -->
                                <th><center>Cadastrado em: </center></th>
                        		<th><center>Status</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/atividadeIniciaticaColumbaController.php");
                            include_once("controller/usuarioController.php");

                            $acc = new atividadeIniciaticaColumbaController();
                            $um = new usuarioController();

                            $resultado = $acc->listaAtividadeIniciaticaColumba($idOrganismoAfiliado);

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    //echo "<pre>";print_r($vetor);echo "</pre>";
                                    ?>
							<tr>
								<td>
                                    <?php
                                    $arrNome				= explode(" ",$vetor['nomeAtividadeIniciaticaColumba']);
                                    echo "[" . $vetor['codAfiliacaoAtividadeIniciaticaColumba'] . "] " . ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNome[1],'UTF-8'));
                                    ?>
								</td>
                                <!--
                                <td>
                                    <?php
                                    //echo addslashes(substr($vetor["dataAdmissaoAtividadeIniciaticaColumba"], 8, 2) . "/" . substr($vetor["dataAdmissaoAtividadeIniciaticaColumba"], 5, 2) . "/" . substr($vetor["dataAdmissaoAtividadeIniciaticaColumba"], 0, 4))
                                    ?>
                                </td>
								<td>
                                    <?php
                                        //echo addslashes(substr($vetor["dataInstalacaoAtividadeIniciaticaColumba"], 8, 2) . "/" . substr($vetor["dataInstalacaoAtividadeIniciaticaColumba"], 5, 2) . "/" . substr($vetor["dataInstalacaoAtividadeIniciaticaColumba"], 0, 4))
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    //echo addslashes(substr($vetor["dataAtivaAteAtividadeIniciaticaColumba"], 8, 2) . "/" . substr($vetor["dataAtivaAteAtividadeIniciaticaColumba"], 5, 2) . "/" . substr($vetor["dataAtivaAteAtividadeIniciaticaColumba"], 0, 4))
                                    ?>
                                </td>
                                -->
                                <td>
                                    <center>
                                    <?php
                                    echo addslashes(substr($vetor["cadastradoEmAtividadeIniciaticaColumba"], 8, 2) . "/" .
                                                    substr($vetor["cadastradoEmAtividadeIniciaticaColumba"], 5, 2) . "/" .
                                                    substr($vetor["cadastradoEmAtividadeIniciaticaColumba"], 0, 4) . " - " . substr($vetor['cadastradoEmAtividadeIniciaticaColumba'], 11, 2) . ":" . substr($vetor['cadastradoEmAtividadeIniciaticaColumba'], 14, 2));
                                    ?>
                                    <?php if($vetor['atualizadoEmAtividadeIniciaticaColumba']!='0000-00-00 00:00:00'){ ?>
                                        <br>
                                        <div>
                                            Última atualização em: <br>
                                            <?php echo substr($vetor['atualizadoEmAtividadeIniciaticaColumba'], 8, 2) . "/" . substr($vetor['atualizadoEmAtividadeIniciaticaColumba'], 5, 2) . "/" . substr($vetor['atualizadoEmAtividadeIniciaticaColumba'], 0, 4) . " - " . substr($vetor['atualizadoEmAtividadeIniciaticaColumba'], 11, 2) . ":" . substr($vetor['atualizadoEmAtividadeIniciaticaColumba'], 14, 2);?>
                                        </div>
                                    <?php } ?>
                                    </center>
                                </td>
                                <td>
	                                <center>
	                                    <div id="statusTarget<?php echo $vetor['idAtividadeIniciaticaColumba'] ?>">
	                                        <?php
	                                        switch ($vetor['statusAtividadeIniciaticaColumba']) {
	                                            case 0:
	                                                echo "<span class='badge badge-primary'>Ativo</span>";
	                                                break;
	                                            case 1:
	                                                echo "<span class='badge badge-danger'>Inativo</span>";
	                                                break;
	                                        }
	                                        ?>
	                                    </div>
	                                </center>
                                </td>
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idAtividadeIniciaticaColumba']; ?>">
                                            <i class="fa fa-search-plus fa-white"></i>  
                                            Detalhes                                            
                                        </a>
                                        <?php if(!$usuarioApenasLeitura){?>
                                            <a class="btn btn-sm btn-info" href="?corpo=alteraAtividadeIniciaticaColumba&idAtividadeIniciaticaColumba=<?php echo $vetor['idAtividadeIniciaticaColumba']; ?>" data-rel="tooltip" title="">
                                                <i class="fa fa-edit fa-white"></i>
                                                Editar
                                            </a>
                                            <span id="status<?php echo $vetor['idAtividadeIniciaticaColumba'] ?>">
                                                <?php if ($vetor['statusAtividadeIniciaticaColumba'] == 1) { ?>
                                                    <a class="btn btn-sm btn-primary" onclick="mudaStatus('<?php echo $vetor['idAtividadeIniciaticaColumba'] ?>', '<?php echo $vetor['statusAtividadeIniciaticaColumba'] ?>', 'atividadeIniciaticaColumba')" data-rel="tooltip" title="Ativar">
                                                        Ativar
                                                    </a>
                                                <?php } else { ?>
                                                    <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idAtividadeIniciaticaColumba'] ?>', '<?php echo $vetor['statusAtividadeIniciaticaColumba'] ?>', 'atividadeIniciaticaColumba')" data-rel="tooltip" title="Inativar">

                                                        Inativar
                                                    </a>
                                                <?php } ?>
                                            </span>
                                        <?php } ?>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->
<?php

$resultadoInfo = $acc->listaAtividadeIniciaticaColumba();

if ($resultadoInfo) {
	foreach ($resultadoInfo as $vetorInfo) {
        ?>
        <!-- Modal de detalhes do usuário -->
        <div class="modal inmodal" id="myModaInfo<?php echo $vetorInfo['idAtividadeIniciaticaColumba']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Dados da Columba</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Cód. de Afiliação: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['codAfiliacaoAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Columba: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['nomeAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Endereço: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['enderecoAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">CEP: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['cepAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Naturalidade: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['naturalidadeAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Data de Nascimento: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            echo substr($vetorInfo['dataNascimentoAtividadeIniciaticaColumba'],8,2)."/".
                                                substr($vetorInfo['dataNascimentoAtividadeIniciaticaColumba'],5,2)."/".
                                                substr($vetorInfo['dataNascimentoAtividadeIniciaticaColumba'],0,4);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Escola que frequenta: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['escolaAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">E-mail: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['emailAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Rede Social: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['redeSocialAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Data de Admissão: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            echo substr($vetorInfo['dataAdmissaoAtividadeIniciaticaColumba'],8,2)."/".
                                                substr($vetorInfo['dataAdmissaoAtividadeIniciaticaColumba'],5,2)."/".
                                                substr($vetorInfo['dataAdmissaoAtividadeIniciaticaColumba'],0,4);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Data de Instalação: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            echo substr($vetorInfo['dataInstalacaoAtividadeIniciaticaColumba'],8,2)."/".
                                                substr($vetorInfo['dataInstalacaoAtividadeIniciaticaColumba'],5,2)."/".
                                                substr($vetorInfo['dataInstalacaoAtividadeIniciaticaColumba'],0,4);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Ativa Até: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            echo substr($vetorInfo['dataAtivaAteAtividadeIniciaticaColumba'],8,2)."/".
                                                substr($vetorInfo['dataAtivaAteAtividadeIniciaticaColumba'],5,2)."/".
                                                substr($vetorInfo['dataAtivaAteAtividadeIniciaticaColumba'],0,4);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php if($vetorInfo['codAfiliacaoTutorAtividadeIniciaticaColumba'] != 0){ ?>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Pai ou Tutor: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo "[" . $vetorInfo['codAfiliacaoTutorAtividadeIniciaticaColumba'] . "] " . $vetorInfo['nomeTutorAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($vetorInfo['codAfiliacaoTutoraAtividadeIniciaticaColumba'] != 0){ ?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Mãe ou Tutora: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php echo "[" . $vetorInfo['codAfiliacaoTutoraAtividadeIniciaticaColumba'] . "] " . $vetorInfo['nomeTutoraAtividadeIniciaticaColumba']; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Responsável: </label>
                                    <div class="col-sm-6">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            switch ($vetorInfo['responsavelPlenoAtividadeIniciaticaColumba']) {
                                                case 0:
                                                    echo "Não informado!";
                                                    break;
                                                case 1:
                                                    echo "Pai ou tutor";
                                                    break;
                                                case 2:
                                                    echo "Mãe ou tutora";
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">E-mail do Responsável: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['emailResponsavelAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php if($vetorInfo['RgAnexoAtividadeIniciaticaColumba'] != ''){?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">RG Anexada: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php $arrLink = explode('.', $vetorInfo['RgAnexoAtividadeIniciaticaColumba']);
                                                $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                ?>
                                                <a target="_BLANK" href="<?php echo $vetorInfo['RgAnexoAtividadeIniciaticaColumba']; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                            <?php if($vetorInfo['certidaoNascimentoAnexoAtividadeIniciaticaColumba'] != ''){?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Cert. de Nascimento Anexada: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php $arrLink = explode('.', $vetorInfo['certidaoNascimentoAnexoAtividadeIniciaticaColumba']);
                                                $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                ?>
                                                <a target="_BLANK" href="<?php echo $vetorInfo['certidaoNascimentoAnexoAtividadeIniciaticaColumba']; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                            <?php if($vetorInfo['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba'] != ''){?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Foto de Corpo Inteiro Anexada: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php $arrLink = explode('.', $vetorInfo['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']);
                                                $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                ?>
                                                <a target="_BLANK" href="<?php echo $vetorInfo['fotoCorpoInteiroAnexoAtividadeIniciaticaColumba']; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                            <?php if($vetorInfo['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba'] != ''){?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Cartão do Membro Responsável Anexada: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php $arrLink = explode('.', $vetorInfo['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']);
                                                $arrLinkNome	= $arrLink[count($arrLink)-2];
                                                $arrLinkExt		= $arrLink[count($arrLink)-1];
                                                ?>
                                                <a target="_BLANK" href="<?php echo $vetorInfo['cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba']; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                            -->
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Descrição: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorInfo['descricaoAtividadeIniciaticaColumba']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Cadastrada em: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            echo substr($vetorInfo['cadastradoEmAtividadeIniciaticaColumba'],8,2)."/".
                                                substr($vetorInfo['cadastradoEmAtividadeIniciaticaColumba'],5,2)."/".
                                                substr($vetorInfo['cadastradoEmAtividadeIniciaticaColumba'],0,4)." às ".
                                                substr($vetorInfo['cadastradoEmAtividadeIniciaticaColumba'],10,6);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if($vetorInfo['atualizadoEmAtividadeIniciaticaColumba'] != '0000-00-00 00:00:00') {
                            ?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Atualizado em: </label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php
                                                echo substr($vetorInfo['atualizadoEmAtividadeIniciaticaColumba'], 8, 2) . "/" .
                                                    substr($vetorInfo['atualizadoEmAtividadeIniciaticaColumba'], 5, 2) . "/" .
                                                    substr($vetorInfo['atualizadoEmAtividadeIniciaticaColumba'], 0, 4) . " às " .
                                                    substr($vetorInfo['atualizadoEmAtividadeIniciaticaColumba'], 10, 6);
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Atualizado Por: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            $resultadoAtualizadoPor = $um->buscaUsuario($vetorInfo['fk_seqCadastAtualizadoPor']);
                                            echo $resultadoAtualizadoPor->getNomeUsuario();
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right">Status: </label>
                                    <div class="col-sm-6">
                                        <p class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            switch ($vetorInfo['statusAtividadeIniciaticaColumba']) {
                                                case 0:
                                                    echo "<span class='badge badge-primary'>Ativo</span>";
                                                    break;
                                                case 1:
                                                    echo "<span class='badge badge-danger'>Inativo</span>";
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
				