
<!-- Conteúdo DE INCLUDE INÍCIO -->

<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório de Columbas validadas na GLP</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Relatório</a>
            </li>
            <li class="active">
                <strong><a>Relatório de Columbas validadas na GLP</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Relatório de Columbas validadas na GLP</h5>
                </div>
                <div class="ibox-content">
                    <form name="extratoFinanceiro" class="form-horizontal" method="post">
                        
                                <?php if(!isset($regiaoUsuario)&&$_SESSION['fk_idDepartamento'] != 2 && $_SESSION['fk_idDepartamento'] != 3){ 
                                            $oaUsuario = $idOrganismoAfiliado;
                                            ?>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                        <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                            <option value="0">Selecione</option>
                                            <?php
                                            include_once 'controller/organismoController.php';
                                            $oc = new organismoController();
                                            $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                            ?>
                                        </select>
                            </div>    
                        </div>    
                                            <?php
                                        }else{ 
                                            $oaUsuario = "";
                                            ?>
                            <div class="form-group form-inline">
                                <label class="col-sm-3 control-label">Região:</label>
                                
                                <div class="col-sm-9">
                                        <select name="regiao" id="regiao" data-placeholder="Selecione uma regiao..." style="width:350px;" tabindex="2" onchange="carregaOrganismoMailist(this.value);">
                                            <option value="0">Selecione</option>
                                            <?php
                                            include_once 'controller/regiaoRosacruzController.php';
                                            $rrc = new regiaoRosacruzController();
                                            $rrc->criarComboBox(0,$regiaoUsuario);
                                            ?>
                                        </select>
                                </div>    
                            </div>
                            <div class="form-group form-inline">
                        <label class="col-sm-3 control-label">Organismo:</label>
                                <div class="col-sm-9">
                                        <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" style="width:350px;">
                                            
                                        </select>
                                </div>    
                        </div>
                                
                            
                        <?php }?>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <a href="#" onclick="abrirPopupRelatorioColumbasGLP();" class="btn btn-sm btn-success" data-placement="left" title="Gerar Relatório Quantitativo">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Gerar Relatório de Columbas
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	