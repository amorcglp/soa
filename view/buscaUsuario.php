<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Usuário salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php

$funcoesAtualizadas = isset($_REQUEST['funcoesAtualizadas'])?$_REQUEST['funcoesAtualizadas']:null;
$usuariosNaoIdentificados = isset($_REQUEST['usuariosNaoIdentificados'])?$_REQUEST['usuariosNaoIdentificados']:null;
$nome = isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
$letra = (isset($_REQUEST['letra']))?$_REQUEST['letra']:null;
$letraNome = (isset($_REQUEST['letraNome']))?$_REQUEST['letraNome']:null;

if(($letra==null&&$letraNome==null)||($letra==""&&$letraNome==""))
{
    $letraNome="a";
}


if($funcoesAtualizadas==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Funções do Usuário <?php echo $nome;?> atualizadas com sucesso.",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Usuários</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Usuários</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Usuários Cadastrados</h5>
                    <div class="ibox-tools">
                    <?php
                    if($_SESSION['seqCadast']==450285||
                            $_SESSION['seqCadast']==540135||
                            $_SESSION['seqCadast']==549676||
                            $_SESSION['seqCadast']==550110)
                    {
                    ?>
                    <!--    
                    <a class="btn btn-xs btn-warning" href="cron/limpa_funcoes_usuario.php" target="_blank" style="color: white">
                            <i class="fa fa-refresh"></i> Limpa Funções
                    </a>
                    -->
                     <!--
                        <a class="btn btn-xs btn-primary" href="alteraLotacao.php?corpo=buscaUsuario&sigla=PR113" style="color: white">
                            <i class="fa fa-user"></i> Usuários ativos PR113
                        </a>
                    <a class="btn btn-xs btn-danger" href="?corpo=buscaUsuario&usuariosNaoIdentificados=1" style="color: white">
                            <i class="fa fa-user"></i> Usuários inativos PR113
                        </a>-->
                    <a class="btn btn-xs btn-warning" href="cron/atualiza_funcoes_usuario3.php" target="_blank" style="color: white">
                            <i class="fa fa-refresh"></i> Atualizar Funções
                    </a>
                    <a class="btn btn-xs btn-warning" href="cron/inativa_usuario_com_termino_mandato.php" target="_blank" style="color: white">
                            <i class="fa fa-refresh"></i> Inativar Usuários com Térm. Mandato
                    </a>    
                    <?php
                    }
                    ?>
                    <a class="btn btn-xs btn-warning" href="cron/baixa_funcoes_usuario.php" target="_blank" style="color: white">
                            <i class="fa fa-refresh"></i> Baixa Automática Funções de Todos os Usuários Atuantes no ORCZ
                    </a>    
                        
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroUsuario">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <div>
                        Filtro por nome:
                        <a href="?corpo=buscaUsuario&letraNome=a"><?php if($letraNome=="a"){ echo "<b>a</b>";}else{ echo "a";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=b"><?php if($letraNome=="b"){ echo "<b>b</b>";}else{ echo "b";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=c"><?php if($letraNome=="c"){ echo "<b>c</b>";}else{ echo "c";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=d"><?php if($letraNome=="d"){ echo "<b>d</b>";}else{ echo "d";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=e"><?php if($letraNome=="e"){ echo "<b>e</b>";}else{ echo "e";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=f"><?php if($letraNome=="f"){ echo "<b>f</b>";}else{ echo "f";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=g"><?php if($letraNome=="g"){ echo "<b>g</b>";}else{ echo "g";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=h"><?php if($letraNome=="h"){ echo "<b>h</b>";}else{ echo "h";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=i"><?php if($letraNome=="i"){ echo "<b>i</b>";}else{ echo "i";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=j"><?php if($letraNome=="j"){ echo "<b>j</b>";}else{ echo "j";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=k"><?php if($letraNome=="k"){ echo "<b>k</b>";}else{ echo "k";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=l"><?php if($letraNome=="l"){ echo "<b>l</b>";}else{ echo "l";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=m"><?php if($letraNome=="m"){ echo "<b>m</b>";}else{ echo "m";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=n"><?php if($letraNome=="n"){ echo "<b>n</b>";}else{ echo "n";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=o"><?php if($letraNome=="o"){ echo "<b>o</b>";}else{ echo "o";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=p"><?php if($letraNome=="p"){ echo "<b>p</b>";}else{ echo "p";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=q"><?php if($letraNome=="q"){ echo "<b>q</b>";}else{ echo "q";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=r"><?php if($letraNome=="r"){ echo "<b>r</b>";}else{ echo "r";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=s"><?php if($letraNome=="s"){ echo "<b>s</b>";}else{ echo "s";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=t"><?php if($letraNome=="t"){ echo "<b>t</b>";}else{ echo "t";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=u"><?php if($letraNome=="u"){ echo "<b>u</b>";}else{ echo "u";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=v"><?php if($letraNome=="v"){ echo "<b>v</b>";}else{ echo "v";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=x"><?php if($letraNome=="x"){ echo "<b>x</b>";}else{ echo "x";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=w"><?php if($letraNome=="w"){ echo "<b>w</b>";}else{ echo "w";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=y"><?php if($letraNome=="y"){ echo "<b>y</b>";}else{ echo "y";}?></a>&nbsp;&nbsp;
                        <a href="?corpo=buscaUsuario&letraNome=z"><?php if($letraNome=="z"){ echo "<b>z</b>";}else{ echo "z";}?></a>&nbsp;&nbsp;
                        <br>&nbsp;
                    </div>
                    <div>
                    Filtro por login:
                    <a href="?corpo=buscaUsuario&letra=a"><?php if($letra=="a"){ echo "<b>a</b>";}else{ echo "a";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=b"><?php if($letra=="b"){ echo "<b>b</b>";}else{ echo "b";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=c"><?php if($letra=="c"){ echo "<b>c</b>";}else{ echo "c";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=d"><?php if($letra=="d"){ echo "<b>d</b>";}else{ echo "d";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=e"><?php if($letra=="e"){ echo "<b>e</b>";}else{ echo "e";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=f"><?php if($letra=="f"){ echo "<b>f</b>";}else{ echo "f";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=g"><?php if($letra=="g"){ echo "<b>g</b>";}else{ echo "g";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=h"><?php if($letra=="h"){ echo "<b>h</b>";}else{ echo "h";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=i"><?php if($letra=="i"){ echo "<b>i</b>";}else{ echo "i";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=j"><?php if($letra=="j"){ echo "<b>j</b>";}else{ echo "j";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=k"><?php if($letra=="k"){ echo "<b>k</b>";}else{ echo "k";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=l"><?php if($letra=="l"){ echo "<b>l</b>";}else{ echo "l";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=m"><?php if($letra=="m"){ echo "<b>m</b>";}else{ echo "m";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=n"><?php if($letra=="n"){ echo "<b>n</b>";}else{ echo "n";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=o"><?php if($letra=="o"){ echo "<b>o</b>";}else{ echo "o";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=p"><?php if($letra=="p"){ echo "<b>p</b>";}else{ echo "p";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=q"><?php if($letra=="q"){ echo "<b>q</b>";}else{ echo "q";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=r"><?php if($letra=="r"){ echo "<b>r</b>";}else{ echo "r";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=s"><?php if($letra=="s"){ echo "<b>s</b>";}else{ echo "s";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=t"><?php if($letra=="t"){ echo "<b>t</b>";}else{ echo "t";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=u"><?php if($letra=="u"){ echo "<b>u</b>";}else{ echo "u";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=v"><?php if($letra=="v"){ echo "<b>v</b>";}else{ echo "v";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=x"><?php if($letra=="x"){ echo "<b>x</b>";}else{ echo "x";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=w"><?php if($letra=="w"){ echo "<b>w</b>";}else{ echo "w";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=y"><?php if($letra=="y"){ echo "<b>y</b>";}else{ echo "y";}?></a>&nbsp;&nbsp;
                    <a href="?corpo=buscaUsuario&letra=z"><?php if($letra=="z"){ echo "<b>z</b>";}else{ echo "z";}?></a>&nbsp;&nbsp;
                        <br>&nbsp;
                    </div>
                    <br>
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                            	<th>SeqCadast</th>
                                <th>Usuário</th>
                                <th>E-mail</th>
                                <th><center>Cód. de Afiliação</center></th>
                                <th><center>Login</center></th>
		                        <th>Departamento</th>
		                        <th>Função</th>
		                        <th>Sigla do OA</th>
                                <th><center>Ciente Ass. Eletr.</center></th>
		                        <th><center>Status</center></th>
		                        <th><center>Ações</center></th>
		                    </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/usuarioController.php");
							include_once("model/funcaoUsuarioClass.php");
                            $uc = new usuarioController();

                            $resultado = $uc->listaUsuario(null,$letra,$letraNome);

                            if ($resultado) {
                                foreach ($resultado as $vetor) {

                                    switch ($vetor['statusUsuario']){
                                        case 0:
                                            $statusUsuario = "Ativo";
                                            break;
                                        case 1:
                                            $statusUsuario = "Inativo";
                                            break;
                                    }
                                    /*
                                    if(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA"))) {
                                        if(("PR113"==strtoupper($sessao->getValue("siglaOA"))
                                                &&($vetor['statusUsuario']==1
                                                &&$usuariosNaoIdentificados==1)
                                            )||($vetor["siglaOrganismoAfiliado"]=="PR113"
                                                &&$vetor['statusUsuario']==0
                                                &&$usuariosNaoIdentificados==null
                                            )
                                            ||"PR113"!=strtoupper($vetor["siglaOrganismoAfiliado"])

                                        ) {*/
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $vetor['seqCadast']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $vetor['nomeUsuario']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $vetor['emailUsuario']; ?>
                                                </td>
                                                <td>
                                                    <center>
                                                        <div id="codigoDeAfiliacao<?php echo $vetor['seqCadast'];?>">
                                                            <?php echo $vetor['codigoDeAfiliacao']; ?>
                                                            <br><br>
                                                            <a href="#" class="btn btn-sm btn-info" onclick="atualizarCodigoDeAfiliacao('<?php echo $vetor['seqCadast'];?>','<?php echo $vetor['codigoDeAfiliacao'];?>');">Atualizar</a>
                                                        </div>
                                                    </center>

                                                </td>
                                                <td>
                                                    <center>
                                                        <?php echo $vetor['loginUsuario']; ?>
                                                    </center>
                                                </td>
                                                <td>
                                                    <?php echo $vetor['nomeDepartamento']; ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $fu = new funcaoUsuario();
                                                    $resultado2 = $fu->buscaPorMaisDeUmaFuncao($vetor['idUsuario']);
                                                    if ($resultado2) {
                                                        foreach ($resultado2 as $vetor2) {
                                                            echo $vetor2['nomeFuncao'] . "<br>";
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo $vetor['siglaOA']; ?>
                                                </td>
                                                <td>
                                                    <center>
                                                    <?php if($vetor['cienteAssinaturaDigital']==1){ echo "<span class='badge badge-primary'>sim</span>";}else{ echo "<span class='badge badge-danger'>não</span>";} ?>
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <div id="statusTarget<?php echo $vetor['seqCadast']; ?>">
                                                            <?php
                                                            switch ($vetor['statusUsuario']) {
                                                                case 0:
                                                                    echo "<span class='badge badge-primary'>Ativo</span>";
                                                                    break;
                                                                case 1:
                                                                    echo "<span class='badge badge-danger'>Inativo</span>";
                                                                    break;
                                                            }
                                                            ?>
                                                        </div>
                                                    </center>
                                                </td>
                                                <td>
                                                    <center>
                                                        <div id="acoes_confirma_cancela">
                                                            <a class="btn btn-sm btn-success" href=""
                                                               data-toggle="modal"
                                                               data-target="#myModaInfo"
                                                               onclick="
                                                                       document.getElementById('nomeCompleto').innerHTML='<?php echo $vetor['nomeUsuario'];?>';
                                                                       document.getElementById('codigoDeAfiliacao').innerHTML='<?php echo $vetor['codigoDeAfiliacao'];?>';
                                                                       document.getElementById('status').innerHTML='<?php echo $statusUsuario;?>';
                                                                       document.getElementById('email').innerHTML='<?php echo $vetor['emailUsuario'];?>';
                                                                       document.getElementById('telefoneResidencial').innerHTML='<?php echo $vetor['telefoneResidencialUsuario'];?>';
                                                                       document.getElementById('telefoneComercial').innerHTML='<?php echo $vetor['telefoneComercialUsuario'];?>';
                                                                       document.getElementById('celular').innerHTML='<?php echo $vetor['celularUsuario'];?>';
                                                                       document.getElementById('login').innerHTML='<?php echo $vetor['loginUsuario'];?>';
                                                                       document.getElementById('dataCadastro').innerHTML='<?php echo substr($vetor['dataCadastroUsuario'],8,2)."/".substr($vetor['dataCadastroUsuario'],5,2)."/".substr($vetor['dataCadastroUsuario'],0,4);?>';
                                                                       document.getElementById('departamento').innerHTML='<?php echo $vetor['nomeDepartamento'];?>';
                                                                       document.getElementById('seqCadastUsuario').value='<?php echo $vetor['seqCadast'];?>';
                                                                       document.getElementById('letra').value='<?php echo $letra;?>';
                                                                       document.getElementById('letraNome').value='<?php echo $letraNome;?>';
                                                                       "
                                                                >
                                                                <i class="icon-edit icon-white"
                                                                ></i>
                                                                Detalhes
                                                            </a>
                                                            <?php if (in_array("2", $arrNivelUsuario)) { ?>
                                                                <a class="btn btn-sm btn-info"
                                                                   href="?corpo=alteraUsuario&letra=<?php echo $letra;?>&letraNome=<?php echo $letraNome;?>&seqCadast=<?php echo $vetor['seqCadast']; ?>"
                                                                   data-rel="tooltip" title="">
                                                                    <i class="icon-edit icon-white"></i>
                                                                    Editar
                                                                </a>
                                                            <?php } ?>
                                                            <?php if (in_array("3", $arrNivelUsuario)) { ?>
                                                                <span id="status<?php echo $vetor['seqCadast'] ?>">
        <?php if ($vetor['statusUsuario'] == 1) { ?>
            <a class="btn btn-sm btn-primary"
               onclick="mudaStatus('<?php echo $vetor['seqCadast'] ?>', '<?php echo $vetor['statusUsuario'] ?>', 'usuario')"
               data-rel="tooltip" title="Ativar">
                                                    Ativar                                            
                                                </a>
        <?php } else { ?>
            <a class="btn btn-sm btn-danger"
               onclick="mudaStatus('<?php echo $vetor['seqCadast'] ?>', '<?php echo $vetor['statusUsuario'] ?>', 'usuario')"
               data-rel="tooltip" title="Inativar">
                                                    Inativar                                            
                                                </a>

        <?php } ?>
                                        </span>
                                                            <?php } ?>
                                                        </div>
                                                        <a class="btn btn-sm btn-warning"
                                                           onclick="atualizarFuncoesDoUsuario('<?php echo $vetor['seqCadast'] ?>','<?php echo $vetor['nomeUsuario']; ?>')"
                                                           data-rel="tooltip" title="Ativar">
                                                            Atualizar Funções
                                                        </a>
                                                    </center>
                                                </td>
                                            </tr>
                                            <?php
                                            /*
                                        }
                                    }*/
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<!-- Modal de detalhes do usuário -->
<div class="modal inmodal" id="myModaInfo" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Detalhes do Usuário</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nome Completo</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><span id="nomeCompleto"></span></p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Código de Afiliação</label>
                            <div class="col-sm-8" style="">
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <span id="codigoDeAfiliacao"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Status</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8">
                                    <p class="form-control-static">
                                        <span id="status"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">E-mail</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><span id="email"></span></p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Telefone Residêncial</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><span id="telefoneResidencial"></span></p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Telefone Comercial</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><span id="telefoneComercial"></span></p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Celular</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><span id="celular"></span></p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Login</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><span id="login"></span></p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Senha</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static">***Criptografada, se o usuário não souber deverá redefinir em esqueci senha</p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Data de Cadastro</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><span id="dataCadastro"></span></p></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Departamento</label>
                            <div class="col-sm-8">
                                <div class="col-sm-8" style="max-width: 320px">
                                    <p class="form-control-static">
                                        <span id="departamento"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="letra" id="letra" class="form-control" value="">
                    <input type="hidden" name="letraNome" id="letraNome" class="form-control" value="">
                    <input type="hidden" name="seqCadastUsuario" id="seqCadastUsuario" class="form-control" value="">
                    <input type="hidden" name="seqCadast" id="seqCadast" class="form-control" value="<?php echo $sessao->getValue("seqCadast"); ?>">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    <?php if(in_array("2",$arrNivelUsuario)){?>
                    <a href="#" onclick="alterarUsuario();" class="btn btn-primary" id="alterar" name="alterar">Atuarlizar</a>
                    <?php }?>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->



<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

        /* Init DataTables */
        var oTable = $('#editable').dataTable();

        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });


    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row"]);

    }
</script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	