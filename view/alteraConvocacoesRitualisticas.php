<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
       @include_once("lib/webservice/retornaInformacoesMembro.php");
      
include_once("model/convocacaoRitualisticaOGGClass.php");
$cr = new convocacaoRitualisticaOGG();
$dados = $cr->lista($_GET['id']);
//echo "<pre>";print_r($dados);
?>			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Convocações - OGG</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>             
	                        <li class="active">
	                            <strong><a>Incluir Convocação</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->

				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de Cadastro de Convocações Ritualísticas da OGG do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaConvocacoesRitualisticas">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="formulario" class="form-horizontal" method="post" action="acoes/acaoAlterar.php" onsubmit="return validaConvocacaoRitualisticaOGG();">
                        <input type="hidden" name="idConvocacaoRitualistica" id="idConvocacaoRitualistica" value="<?php echo $_REQUEST['id'];?>">
                        <div class="row">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2" required="required">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox($dados[0]['fk_idOrganismoAfiliado'],$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data da Convocação:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataConvocacao" onBlur="" value="<?php echo substr($dados[0]['dataConvocacao'],8,2)."/".substr($dados[0]['dataConvocacao'],5,2)."/".substr($dados[0]['dataConvocacao'],0,4);?>" maxlength="10" id="dataConvocacao" type="text" class="form-control" style="max-width: 102px">
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-3 control-label">Hora:</label>
                                <div class="col-sm-2 input-group" style="padding: 0px 0px 0px 15px">
                                    <div class="input-group clockpicker"  data-autoclose="true">
                                    <input type="text" class="form-control" onBlur="" maxlength="5" id="horaConvocacao" name="horaConvocacao" value="<?php echo $dados[0]['horaConvocacao'];?>">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>  
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Título da Mensagem: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="250" id="tituloMensagem" name="tituloMensagem" type="text" value="<?php echo $dados[0]['tituloMensagem'];?>" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Comendador:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembroComendador" maxlength="7" type="text" value="<?php echo retornaCodigoAfiliacao($dados[0]['seqCadastComendador']);?>" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroComendador" type="text" maxlength="100" value="<?php echo $dados[0]['nomeComendador'];?>" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroComendador','nomeMembroComendador','codAfiliacaoMembroComendador','nomeMembroComendador','h_seqCadastMembroComendador','h_nomeMembroComendador','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Digite o código de afiliação do membro e no outro campo acima o primeiro nome e pressione TAB
                                        <!--<input type="checkbox" value="" name="companheiroMembroPresidente" id="companheiroMembroPresidente"> Companheiro-->
                                <input type="hidden" name="h_seqCadastMembroComendador" id="h_seqCadastMembroComendador" value="<?php echo $dados[0]['seqCadastComendador']; ?>">
                                <input type="hidden" name="h_nomeMembroComendador" id="h_nomeMembroComendador" value="<?php echo $dados[0]['nomeComendador']; ?>">   
                            </div>
                        </div>    

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Frequência </label>
                            <div class="col-sm-9">
                               
                            </div>
                        </div>    
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Membros:</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipo" id="tipo">
                                    <option value="0">Selecione</option>
                                    <option value="1">Adulto</option>
                                    <option value="2">Jovem</option>
                                </select>
                                <input class="form-control" id="codAfiliacaoMembroOficial" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroOficial" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroOficial','nomeMembroOficial','codAfiliacaoMembroOficial','nomeMembroOficial','h_seqCadastMembroOficial','h_nomeMembroOficial','myModalPesquisa','informacoesPesquisa', 'S');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                        E-mail
                                <input type="text" id="emailMembro" name="emailMembro" value="">
                                <br>
                                <input type="hidden" id="h_seqCadastMembroOficial">
                                <input type="hidden" id="h_nomeMembroOficial">
                                <input type="hidden" id="h_seqCadastCompanheiroMembroOficial">
                                <input type="hidden" id="h_nomeCompanheiroMembroOficial">
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiMembrosConvocacaoRitualistica();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="ata_oa_mensal_oficiais[]" id="ata_oa_mensal_oficiais" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                            <?php
                                            include_once 'model/convocacaoRitualisticaOGGMembroClass.php';
                                            $membros = new convocacaoRitualisticaOGGMembro();
                                            $resultado = $membros->lista($dados[0]['idConvocacaoRitualisticaOGG']);

                                            foreach ($resultado as $vetor) {
                                                ?>
                                            <option value="{'emailMembro' : '<?php echo $vetor['email']; ?>', 'tipoMembro' : <?php echo $vetor['tipo']; ?>, 'seqCadast': <?php echo $vetor['fk_seq_cadastMembro']; ?>}">[<?php echo retornaCodigoAfiliacao($vetor['fk_seq_cadastMembro'],$vetor['tipo']) ?>] <?php echo retornaNomeCompleto($vetor['fk_seq_cadastMembro']) ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Não Membros:</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipoNaoMembro" id="tipoNaoMembro">
                                    <option value="0">Selecione</option>
                                    <option value="1">Adulto</option>
                                    <option value="2">Jovem</option>
                                </select>
                                <input class="form-control" id="nomeNaoMembro" type="text" maxlength="100" value="" style="min-width: 320px">
                                E-mail
                                <input type="text" id="emailNaoMembro" name="emailNaoMembro" value="">
                                <br>
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiNaoMembrosConvocacaoRitualistica();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="ata_oa_mensal_nao_oficiais[]" id="ata_oa_mensal_nao_oficiais" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                            <?php
                                            include_once 'model/convocacaoRitualisticaOGGNaoMembroClass.php';
                                            $nao_membros = new convocacaoRitualisticaOGGNaoMembro();
                                            $resultado = $nao_membros->lista($dados[0]['idConvocacaoRitualisticaOGG']);

                                            foreach ($resultado as $vetor) {
                                                ?>
                                            <option value="{'emailNaoMembro' : '<?php echo $vetor['email']; ?>', 'tipoNaoMembro' : <?php echo $vetor['tipo']; ?>, 'nomeNaoMembro': '<?php echo $vetor['nome']; ?>'}"><?php echo $vetor['nome'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-sm btn-success"
                                                data-toggle="tooltip" data-placement="left" title="Salvar" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');selecionaTudoMultipleSelect('ata_oa_mensal_nao_oficiais');">
                                                <i class="fa fa-check fa-white"></i>&nbsp; Salvar
                                        </button>
                                        &nbsp; <a href="?corpo=buscaConvocacoesRitualisticas"
                                                class="btn btn-sm btn-danger" data-toggle="tooltip"
                                                data-placement="left" title="Cancelar e voltar!"> <i
                                                class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
                                </div>
                        </div>
                        </div>
		</form>

                </div>
            </div>
        </div>
	</div>
</div>
				<!-- Tabela Fim -->
				
				<!-- Conteúdo DE INCLUDE FIM -->
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            
            <div class="modal-body">
                <span id="resultadoPesquisa" onmousedown="bloquearControlCControlV()">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>