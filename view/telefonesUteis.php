<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<body>
    <h1>Telefones Úteis</h1>
    <input type="hidden" name="arquivo" id="arquivo" value="">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Telefones Úteis</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th><center>Nome</center></th>
                            <th><center>Setor</center></th>
                            <th><center>Responsável Por</center></th>
                            <th><center>Telefone</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Caroline</td>
                                <td><br>Organismos Afiliados</td>
                                <td>
                            <center>Ata de Posse</center>
                            <center>Ata de Reunião Mensal</center>
                            <center>Financeiro</center>
		            <center>Livros</center>
                            <center>Plano de Ação</center>
                            <center>Cadastros</center>
                            <center>FAQ</center>
                            <center>E-mail</center>
                            <center>Anotações</center>
                            <center>Chat</center>
                            <center>Biblioteca</center>
                            <center>Vídeo-Aulas</center>
                            </td>
                            <td><center><br>(41) 3351-3034</center></td>
                            </tr>

                            <tr>
                                <td>William Jaruga</td>
                                <td>Organismos Afiliados</td>
                                <td>
                            <center>Imóveis</center>
                            </td>
                            <td><center>(41) 3351-3033</center></td>
                            </tr>

                            <tr>
                                <td>Camila Elisa Carvalho Ramos</td>
                                <td>Organismos Afiliados</td>
                                <td>
                            <center>TOM - Tradicional Ordem Martinista</center>
                            </td>
                            <td><center>(41) 3351-3035</center></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>