
<!-- Conteúdo DE INCLUDE INÍCIO -->

<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório Gerencial Atividades/Membros Ativos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Relatório</a>
            </li>
            <li class="active">
                <strong><a>Relatório Gerencial Atividades/Membros Ativos</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Relatório Gerencial Atividades/Membros Ativos</h5>
                </div>
                <div class="ibox-content">
                    <form name="extratoFinanceiro" class="form-horizontal" method="post">
                        
                                <?php if(!isset($regiaoUsuario)&&$_SESSION['fk_idDepartamento'] != 2 && $_SESSION['fk_idDepartamento'] != 3){ 
                                            $oaUsuario = $idOrganismoAfiliado;
                                            ?>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                        <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                            <option value="0">Selecione</option>
                                            <?php
                                            include_once 'controller/organismoController.php';
                                            $oc = new organismoController();
                                            $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                            ?>
                                        </select>
                            </div>    
                        </div>    
                                            <?php
                                        }else{ 
                                            $oaUsuario = "";
                                            ?>
                            <div class="form-group form-inline">
                                <label class="col-sm-3 control-label">Região:</label>
                                
                                <div class="col-sm-9">
                                        <select name="regiao" id="regiao" data-placeholder="Selecione uma regiao..." style="width:350px;" tabindex="2" onchange="carregaOrganismoMailist(this.value);">
                                            <option value="0">Selecione</option>
                                            <?php
                                            include_once 'controller/regiaoRosacruzController.php';
                                            $rrc = new regiaoRosacruzController();
                                            $rrc->criarComboBox(0,$regiaoUsuario);
                                            ?>
                                        </select>
                                </div>    
                            </div>
                            <div class="form-group form-inline">
                        <label class="col-sm-3 control-label">Organismo:</label>
                                <div class="col-sm-9">
                                        <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" style="width:350px;">
                                            
                                        </select>
                                </div>    
                        </div>
                                
                            
                                <?php }?>
                            
                        
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="ano" name="ano" type="text" value="" style="max-width: 83px">
                            </div>
                        </div>
                        -->
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Mês/Ano Inicial:</label>
                            <div class="col-sm-9">
                                <select name="mesInicial" id="mesInicial" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        for($i=1;$i<=12;$i++)
                                        {
                                            echo "<option value=\"".str_pad($i, 2, "0", STR_PAD_LEFT)."\">".str_pad($i, 2, "0", STR_PAD_LEFT)."</option>";
                                        }
                                    ?>    
                                </select>
                                /
                                <select name="anoInicial" id="anoInicial" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        $tAno = date('Y')+4;
                                        for($i=2016;$i<=$tAno;$i++)
                                        {
                                            echo "<option value=\"".$i."\">".$i."</option>";
                                        }
                                    ?>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Mês/Ano Final:</label>
                            <div class="col-sm-9">
                                <select name="mesFinal" id="mesFinal" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        for($i=1;$i<=12;$i++)
                                        {
                                            echo "<option value=\"".str_pad($i, 2, "0", STR_PAD_LEFT)."\">".str_pad($i, 2, "0", STR_PAD_LEFT)."</option>";
                                        }
                                    ?>    
                                </select>
                                /
                                <select name="anoFinal" id="anoFinal" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        $tAno = date('Y')+4;
                                        for($i=2016;$i<=$tAno;$i++)
                                        {
                                            echo "<option value=\"".$i."\">".$i."</option>";
                                        }
                                    ?>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Atividades para:</label>
                            <div class="col-sm-9">
                                <input type="checkbox" name="classificacaoOa" id="1" checked="checked" value="1" onclick="getListaAtividadesEstatuto();"> Loja<br>
                                <input type="checkbox" name="classificacaoOa" id="2" checked="checked" value="2" onclick="getListaAtividadesEstatuto();"> Pronaos<br>
                                <input type="checkbox" name="classificacaoOa" id="3" checked="checked" value="3" onclick="getListaAtividadesEstatuto();"> Capítulo<br>
                            </div>
                        </div>
                        <div class="form-group" id="mostrar">
                            <label class="col-sm-3 control-label">Mostrar:</label>
                            <div class="col-sm-9">
                                <div id="listaAtividades">
                                    <input type="checkbox" name='todos' checked="checked" onchange="marcardesmarcar();"/> Todos<br>
                                    <?php 
                                        include_once 'model/atividadeEstatutoTipoClass.php';
                                        $a = new atividadeEstatutoTipo();
                                        $resultado = $a->listaAtividadeEstatutoTipoPelaClassificacaoOa(null,null,1);
                                        $total=0;
                                        if($resultado)
                                        {
                                            foreach ($resultado as $vetor)
                                            {   
                                                $total++;
                                    ?>            
                                                <input type="checkbox" name="atividade" id="<?php echo $vetor['idTipoAtividadeEstatuto'];?>" class="marcar" checked="checked"> <?php echo $vetor['nomeTipoAtividadeEstatuto'];?><br>
                                    <?php

                                            }    
                                        }

                                    ?>

                                <input type="checkbox" name="iniciacao" id="1" class="marcar" checked="checked"> Iniciação ao 1º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="2" class="marcar" checked="checked"> Iniciação ao 2º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="3" class="marcar" checked="checked"> Iniciação ao 3º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="4" class="marcar" checked="checked"> Iniciação ao 4º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="5" class="marcar" checked="checked"> Iniciação ao 5º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="6" class="marcar" checked="checked"> Iniciação ao 6º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="7" class="marcar" checked="checked"> Iniciação ao 7º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="8" class="marcar" checked="checked"> Iniciação ao 8º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="9" class="marcar" checked="checked"> Iniciação ao 9º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="10" class="marcar" checked="checked"> Iniciação ao 10º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="11" class="marcar" checked="checked"> Iniciação ao 11º Grau de Templo<br>
                                <input type="checkbox" name="iniciacao" id="12" class="marcar" checked="checked"> Iniciação ao 12º Grau de Templo<br>

                                <input type="checkbox" name="iniciacao" id="13" class="marcar" checked="checked"> Iniciação à Loja<br>
                                <input type="checkbox" name="iniciacao" id="14" class="marcar" checked="checked"> Iniciação ao Pronaos<br>
                                <input type="checkbox" name="iniciacao" id="16" class="marcar" checked="checked"> Iniciação ao Capitulo<br>

                                <input type="checkbox" name="iniciacao" id="15" class="marcar" checked="checked"> Discurso de Orientação<br>
                                
                                <input type="checkbox" name="atividade" id="linha8" class="marcar" checked="checked"> Membros Ativos<br>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <input type="hidden" name="totalAtividades" id="totalAtividades" value="<?php echo $total;?>">
                        <input type="hidden" name="oaUsuario" id="oaUsuario" value="<?php echo $oaUsuario;?>">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <a href="#" onclick="abrirPopupRelatorioGerencialAtividade();" class="btn btn-sm btn-success" data-placement="left" title="Gerar Relatório Quantitativo">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Gerar Relatório Quantitativo
                                </a>
                                <a href="#" onclick="abrirPopupRelatorioGerencialAtividadeAnalitico();" class="btn btn-sm btn-success" data-placement="left" title="Gerar Relatório Analítico">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Gerar Relatório Analítico
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	