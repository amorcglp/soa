<?php 

$arrOficial = array();
$arrOficial['nome'][0]							= "VILMA PEGORETTI";
$arrOficial['cod_afiliacao'][0]					= "41087";
$arrOficial['perfil'][0]						= "Rosacruz";
$arrOficial['obrigacao'][0]						= "6º Grau do Templo";
$arrOficial['data'][0]							= "17/10/14";
$arrOficial['hora_inicio'][0]					= "15:00";
$arrOficial['oa'][0]							= "PRONAOS R+C JARAGUA DO SUL - AMORC - SC107";
$arrOficial['imprime'][0]						= "Confirmados";
$arrOficial['status'][0]						= "1";

$arrOficial['nome'][1]							= "CLAUDIO FURLAN";
$arrOficial['cod_afiliacao'][1]					= "122227";
$arrOficial['perfil'][1]						= "Rosacruz";
$arrOficial['obrigacao'][1]						= "4º Grau do Templo";
$arrOficial['data'][1]							= "26/02/2015";
$arrOficial['hora_inicio'][1]					= "14:00";
$arrOficial['oa'][1]							= "LOJA R+C SAO PAULO-AMORC - SP101";
$arrOficial['imprime'][1]						= "Agendados";
$arrOficial['status'][1]						= "2";

$arrOficial['nome'][2]							= "JOSE RAMOS DE ASSUMPCAO";
$arrOficial['cod_afiliacao'][2]					= "132011";
$arrOficial['perfil'][2]						= "TOM";
$arrOficial['obrigacao'][2]						= "2º Grau do Templo";
$arrOficial['data'][2]							= "17/12/14";
$arrOficial['hora_inicio'][2]					= "11:00";
$arrOficial['oa'][2]							= "LOJA R+C CAMPINAS - AMORC - SP301";
$arrOficial['imprime'][2]						= "Confirmados";
$arrOficial['status'][2]						= "1";

$arrOficial['nome'][3]							= "DAMIANA SANTOS CABRAL";
$arrOficial['cod_afiliacao'][3]					= "144575";
$arrOficial['perfil'][3]						= "Rosacruz";
$arrOficial['obrigacao'][3]						= "9º Grau do Templo";
$arrOficial['data'][3]							= "17/04/15";
$arrOficial['hora_inicio'][3]					= "09:00";
$arrOficial['oa'][3]							= "LOJA R+C BELEM-AMORC - PA101";
$arrOficial['imprime'][3]						= "Agendados";
$arrOficial['status'][3]						= "2";

$arrOficial['nome'][4]							= "ADRIANA WESTPHALEN VESCIA";
$arrOficial['cod_afiliacao'][4]					= "333102";
$arrOficial['perfil'][4]						= "TOM";
$arrOficial['obrigacao'][4]						= "3º Grau do Templo";
$arrOficial['data'][4]							= "05/11/14";
$arrOficial['hora_inicio'][4]					= "07:00";
$arrOficial['oa'][4]							= "LOJA R+C PORTO ALEGRE-AMORC - RS101";
$arrOficial['imprime'][4]						= "Confirmados";
$arrOficial['status'][4]						= "3";

?>
			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Área de Iniciações</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>
	                        <li>
	                            <a href="index.html">Iniciações</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Iniciações Pendentes</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
	            
	            <!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Iniciações Pendentes</h5>
									<div class="ibox-tools">
										<!--
										<a class="btn btn-xs btn-primary" href="http://localhost/soa/includes/ritualistica_nova.php">
											<i class="fa fa-plus"></i> Agendar Nova
										</a>
										-->
									</div>
								</div>
								<div class="ibox-content">
									<table class="table table-striped table-bordered table-hover dataTables-example" >
										<thead>
											<tr>
												<th>Nome</th>
												<th><center>Cód. Afiliação</center></th>
												<th><center>Perfil</center></th>
												<th><center>Obrigação</center></th>
												<th><center>Status</center></th>
												<th><center>Ações</center></th>
											</tr>
										</thead>
										<tbody>
										<?php if(count($arrOficial)>0)
									  		{ 
										  		for($i=0;$i<count($arrOficial['perfil']);$i++){
										  	?>
										 <tr>
											<td>
												<?php echo $arrOficial['nome'][$i];?>
											</td>
											<td>
												<center>
													<?php echo $arrOficial['cod_afiliacao'][$i];?>
												</center>
											</td>
											<td>
												<?php echo $arrOficial['perfil'][$i];?>
											</td>
											<td>
												<center>
													<?php echo $arrOficial['obrigacao'][$i];?>
												</center>
											</td>
											<td>
												<center>
													<div>
													<?php if($arrOficial['status'][$i]==1){?>
														<span class="label label-primary">Presente</span>
													<?php }else{?>
														<?php if($arrOficial['status'][$i]==2){?>
															<span class="label label-danger">Pendente</span>
														<?php }else{?>
															<span class="label label-success">Agendado</span>
														<?php }?>
													<?php }?>
													</div>
												</center>
											</td>
											<td>
												<center>
													<?php if($arrOficial['status'][$i]==1){?>
														<?php echo "<div style='color: green'><strong>Presença Confirmada</strong></div>";?>
													<?php }else{?>
														<?php if($arrOficial['status'][$i]==2){?>
															<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#mySeeAgendamento" data-toggle="tooltip" data-placement="left" title="Agendar um membro pendente para uma Iniciação Ritualística!">
							                                    <i class="fa fa-edit fa-white"></i>&nbsp;
							                                    Agendar
							                                </button>
														<?php }else{?>
															<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
							                                    <i class="fa fa-check fa-white"></i>&nbsp;
							                                    Presente
							                                </button>
							                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#mySeeMotivoCancelamento" data-toggle="tooltip" data-placement="left" title="Cancelar um membro pendente para uma Iniciação Ritualística!">
							                                    <i class="fa fa-times fa-white"></i>&nbsp;
							                                    Cancelar
							                                </button>
							                                <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#mySeeAgendamento" data-toggle="tooltip" data-placement="left" title="Reagendar um membro para outra Iniciação Ritualística!">
							                                    <i class="fa fa-calendar fa-white"></i>&nbsp;
							                                    Reagendar
							                                </button>
														<?php }?>
													<?php }?>
												</center>
											</td>
										</tr>
										<?php }
									  	}
									  	?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->
	            
				<!-- Window MODAL Início -->
				
				<!-- Cancelamento de Agendamento de Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeMotivoCancelamento" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-times-circle modal-icon"></i>
								<h4 class="modal-title">Cancelamento de Agendamento</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<!--
								<div class="alert alert-danger">
	                                <a class="alert-link" href="#">Atenção! </a>Você têm certeza que deseja cancelar este agendamento? Ou talvez reagendar? Se for este o caso feche a janela atual e clique na opção <b>Reagendar</b> que se encontra abaixo da <b>Data/Hora de início</b> na lista.
	                            </div>
								-->
								<label>Motivo do Cancelamento:</label>
								<div style="border: #ccc solid 1px">
									<div class="summernote"></div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary">Cancelar Agendamento</button>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Agendamento e Reagendamento de Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeAgendamento" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-calendar modal-icon"></i>
								<h4 class="modal-title">Agendamento de Iniciação</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<div style="margin-left: 20px">
									<label>Iniciações Disponíveis:</label>
									<div class="row">
	                                    <div class="radio i-checks">
											<input type="radio" name="ritual_marcado" id="ritual_marcado_01" value="ritual_marcado_01">
											05/10/2014 às 11:00 horas - LOJA R+C PORTO ALEGRE-AMORC - RS101
											<br>
										</div>
										<div class="radio i-checks">
											<input type="radio" name="ritual_marcado" id="ritual_marcado_02" value="ritual_marcado_02">
											17/11/2014 às 16:00 horas - PRONAOS R+C JARAGUA DO SUL - AMORC - SC107
											<br>
										</div>
										<div class="radio i-checks">
											<input type="radio" name="ritual_marcado" id="ritual_marcado_03" value="ritual_marcado_03">
											30/04/2015 às 16:00 horas - LOJA R+C CAMPINAS - AMORC - SP301
											<br>
										</div>
										<div class="radio i-checks">
											<input type="radio" name="ritual_marcado" id="ritual_marcado_04" value="ritual_marcado_04">
											25/03/2015 às 16:00 horas - LOJA R+C SAO PAULO-AMORC - SP101
											<br>
										</div>
		                            </div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary">Agendar/Reagendar</button>
							</div>
						</div>
					</div>
				</div>
				<!-- Window MODAL Fim -->
				
				<!-- Conteúdo DE INCLUDE FIM -->
	