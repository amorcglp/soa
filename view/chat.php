<?php
$from = isset($_SESSION['seqCadast']) ? $_SESSION['seqCadast'] : null;
$para = isset($_REQUEST['para']) ? $_REQUEST['para'] : null;

include_once("model/perfilUsuarioClass.php");
$perfilUsuario = new perfilUsuario();

include_once("model/chatClass.php");
$chat = new Chat();
$chat->setFromChat($from);
$chat->setParaChat($para);
$msgs = $chat->selecionaChat();
$nome = $chat->getNomeDestino(); //Pegar nome do usuário que se está conversando
//Zerar todas as novas para o usuário da sessão



?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Chat</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li class="active">
                <strong>Chat</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <h5><strong>Sala de Chat </strong></h5> 
                    <br>
                    <div id="listaUsuariosQueMandaramMensagem">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">

                <div class="ibox chat-view">

                    <div class="ibox-title">
                        <small class="pull-right text-muted"><!--Last message:  Mon Jan 26 2015 - 18:39:23--></small>
                         Chat <?php
                            if ($nome != "") {
                                    echo "com " . $nome;
                            }
                            ?>
                    </div>


                    <div class="ibox-content">

                        <div class="row">

                            <div class="col-md-9 ">
                                <div class="chat-discussion" id="messagens">
                                    
                                    <?php
                                    if (isset($msgs) && count($msgs) > 0) {
                                            for ($i = 0; $i < count($msgs['idChat']); $i++) {
                                                    if ($msgs['from'][$i] == $para && $msgs['para'][$i] == $from) {
                                                            $resultado = $perfilUsuario->listaAvatarUsuario($para);
                                                            $avatar = "";
                                                            foreach ($resultado as $vetor) {
                                                                    $avatar = $vetor['avatarUsuario'];
                                                            }
                                                            ?>

                                    <div class="chat-message left">
                                        <img class="message-avatar" src="<?php if ($avatar == ""|| !file_exists($avatar)) { ?>img/default-user-small.png<?php } else { str_replace("../../","",$avatar);echo $avatar;} ?>" alt="" >
                                        <div class="message">
                                            <a class="message-author" href="#"> <?php echo $nome; ?> </a>
						<span class="message-date"> <?php echo $msgs['data'][$i]; ?> </span>
                                            <span class="message-content">
						<?php echo $msgs['mensagem'][$i]; ?>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    
                                    <?php
                                    }
                                    if ($msgs['from'][$i] == $from && $msgs['para'][$i] == $para) {
                                            $resultado = $perfilUsuario->listaAvatarUsuario($from);
                                            $avatar = "";
                                            foreach ($resultado as $vetor) {
                                                    $avatar = $vetor['avatarUsuario'];
                                            }
                                            //$avatar =   '../../'.$avatar;
                                            ?>
                                    <div class="chat-message right">
                                        <img class="message-avatar" src="<?php if ($avatar == ""|| !file_exists($avatar)) { ?>img/default-user-small.png<?php } else { str_replace("../../","",$avatar); echo $avatar;} ?>" alt="" >
                                        <div class="message">
                                            <a class="message-author" href="#"> Eu </a>
                                            <span class="message-date">  <?php echo $msgs['data'][$i]; ?> </span>
                                            <span class="message-content">
						<?php echo $msgs['mensagem'][$i]; ?>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <?php
                                            }
                                    }
                            }
                            ?>

                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="chat-users">
                                    <div class="users-list" id="listaUsuarios">
                                        
                                    </div>
                                    <input type="hidden" name="from" id="from"
                                        value="<?php echo $from; ?>"> <input type="hidden"
                                        name="para" id="para" value="<?php echo $para; ?>">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="chat-message-form">

                                    <div class="form-group">

                                        <textarea class="form-control message-input" name="msg" id="msg" placeholder="Entre com a mensagem e aperte ENTER" onkeypress="return enviaMensagemChat(event);"></textarea>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>

                </div>
        </div>

    </div>


</div>
<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script>
    setInterval(function () {
        // method to be executed;
        getMensagemChat('<?php echo $from; ?>', '<?php echo $para; ?>');
    }, 300);
    setInterval(function () {
        // method to be executed;
        getUsuariosOnlineChat(<?php echo $_SESSION['seqCadast']; ?>, '<?php echo $para; ?>');
    }, 300);
    setInterval(function () {
        // method to be executed;
        getUsuariosQueMandaramMensagem(<?php echo $_SESSION['seqCadast']; ?>, '<?php echo $para; ?>');
    }, 300);
</script>
<?php
$return = $chat->apagarNovas();

require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>