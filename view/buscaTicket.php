<?php
include_once("controller/ticketController.php");
include_once("controller/organismoController.php");
include_once("controller/usuarioController.php");

include_once('model/ticketItemFuncionalidadeClass.php');
$tif = new TicketItemFuncionalidade();

$tc     = new ticketController();
$oc     = new organismoController();
$uc     = new usuarioController();

$liberaGLP=false;
if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
    $liberaGLP = true;
}

// $idOrganismoAfiliado;   -->   ID vindo da escolha da lotação
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Ticket</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li class="active">
                <strong>Ticket</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Ticket</h5>
                    <div class="ibox-tools">
                        <?php if($sessao->getValue("seqCadast") == 540135){ ?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroTicket">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    $status	            = isset($_GET['status']) ? stripslashes($_GET['status']) : 99;
                    $organismo	        = isset($_GET['organismo']) ? stripslashes($_GET['organismo']) : 0;
                    $funcionalidade	    = isset($_GET['funcionalidade']) ? stripslashes($_GET['funcionalidade']) : 0;
                    $atribuicao	        = isset($_GET['atribuicao']) ? stripslashes($_GET['atribuicao']) : 0;
                    $finalidade         = isset($_GET['finalidade']) ? stripslashes($_GET['finalidade']) : 0;

                    if($sessao->getValue("fk_idDepartamento")==2) {
                        //$atribuicao = $sessao->getValue("seqCadast");
                    }
                    ?>
                    <div class="row m-b-sm m-t-sm">
                        <form name="search" action="painelDeControle.php?corpo=buscaTicket" method="GET" novalidate>
                            <div class="row m-b-sm m-t-sm col-md-12">
                                <input type="hidden" name="corpo" id="corpo" value="buscaTicket">
                                <div class="col-md-2" style="margin-left: 50px">
                                    <select class="form-control col-sm-12 chosen-select" name="status" id="status">
                                        <option value="">Status</option>
                                        <option <?php if($status == "0"){ echo "selected"; }?> value="0">Aberto</option>
                                        <option <?php if($status == "1"){ echo "selected"; }?> value="1">Em andamento</option>
                                        <option <?php if($status == "3"){ echo "selected"; }?> value="3">Indeferido!</option>
                                        <option <?php if($status == "2"){ echo "selected"; }?> value="2">Fechado</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control col-sm-12 chosen-select" name="funcionalidade" id="funcionalidade">
                                        <option value="0">Funcionalidade</option>
                                        <?php $tc->criarComboBoxFuncionalidade($funcionalidade); ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control col-sm-12 chosen-select" name="finalidade" id="finalidade">
                                        <option value="0">Finalidade</option>
                                        <?php $tc->criarComboBoxFinalidade($finalidade); ?>
                                    </select>
                                </div>
                                <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) { ?>
                                <div class="col-md-3">
                                    <select class="form-control col-sm-12 chosen-select" data-placeholder="Filtre por Organismo" name="organismo" id="organismo">
                                        <option value="0">Organismo</option>
                                        <?php $oc->criarComboBox($organismo) ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control col-sm-12 chosen-select" name="atribuicao" id="atribuicao">
                                        <option value="0">Atribuição</option>
                                        <?php $tc->criarComboBoxUsuarioPeloDepartamento($atribuicao); ?>
                                    </select>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="row center col-md-12">
                                <div class="col-md-6 center">
                                    <button type="submit" style="float: right;" class="btn btn-white btn-sm" ><i class="fa fa-refresh"></i> Pesquisar</button>
                                </div>
                                <div class="col-md-6">
                                    <a class="btn btn-white btn-sm" style="float: left;" onclick="limparFiltroTicket();"><i class="fa fa-refresh"></i> Limpar Filtro</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <div class="project-list">
                        <table class="table table-hover">
                            <tbody>
                            <?php
                            $pagina	    = isset($_GET['pagina']) ? $_GET['pagina'] : null;
                            $pagina     = ($pagina <= 0) ? 1 : $pagina;

                            $statusSearch	                = $status!=99 ? $status : null;
                            $organismoSearch	            = $organismo!=0 ? $organismo : null;
                            $funcionalidadeSearch	        = $funcionalidade!=0 ? $funcionalidade : null;
                            $atribuicaoSearch	            = $atribuicao!=0 ? $atribuicao : null;
                            $finalidadeSearch               = $finalidade!=0 ? $finalidade : null;

                            //echo "| statusSearch: ".$statusSearch . " | <br>";
                            //echo "| organismoSearch: ".$organismoSearch . " | <br>";
                            //echo "| funcionalidadeSearch: ".$funcionalidadeSearch . " | <br>";
                            //echo "| atribuicaoSearch: ".$atribuicaoSearch . " | <br>";

                            if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
                                $listaTickets       = $tc->listaTicket(null,$pagina,$statusSearch,$organismoSearch,$funcionalidadeSearch,$atribuicaoSearch,$finalidadeSearch);
                                $nroRegistros       = $tc->retornaQuantidadeTicket(null,$statusSearch,$organismoSearch,$funcionalidadeSearch,$atribuicaoSearch,$finalidadeSearch);
                            } else {
                                $listaTickets       = $tc->listaTicket($idOrganismoAfiliado,$pagina,$statusSearch,$organismoSearch,$funcionalidadeSearch,$atribuicaoSearch,$finalidadeSearch);
                                $nroRegistros       = $tc->retornaQuantidadeTicket($idOrganismoAfiliado,$statusSearch,$organismoSearch,$funcionalidadeSearch,$atribuicaoSearch,$finalidadeSearch);
                            }
                            $nroPaginas         = ceil($nroRegistros/10);

                            if($listaTickets){
                                foreach ($listaTickets as $vetorTickets){
                                    switch($vetorTickets['statusTicket']) {
                                        case 0: $statusTicket = "<span class='label label-default'>Aberto</span>"; break;
                                        case 1: $statusTicket = "<span class='label label-warning'>Em andamento</span>"; break;
                                        case 2: $statusTicket = "<span class='label label-primary'>Fechado</span>"; break;
                                        case 3: $statusTicket = "<span class='label label-danger'>Indeferido</span>"; break;
                                        default: $statusTicket = "<span class='label label-danger'>Erro! Atualize a página.</span>";
                                    }
                                    $tags = [];
                                    $tags = explode(" ", $vetorTickets['tagTicket']);
                            ?>
                                <tr>
                                    <td class="text-center">
                                        <strong>
                                            <?php echo $vetorTickets['idTicket']; ?>
                                        </strong>
                                    </td>
                                    <td class="project-status">
                                        <center>
                                            <?php echo $statusTicket; ?>
                                        </center>
                                    </td>
                                    <td>
                                        <?php if ($liberaGLP&&$vetorTickets['fk_idTicketFinalidade'] == 3) { 
                                            //Pesquisar informações do vinculo do ticket com a solicitação do token
                                            $arrVinculo = $tif->listaVinculoItemTicket($vetorTickets['fk_idFuncionalidade'],null,null,3,$vetorTickets['idTicket']);
                                            //echo "<pre>";print_r($arrVinculo);
                                            if($arrVinculo[0]['mes']!=0&&$arrVinculo[0]['ano']!=0)
                                            {    
                                                $referenteMesAno = ' Mês: '. mesExtensoPortugues($arrVinculo[0]['mes']).' Ano: '.$arrVinculo[0]['ano'];
                                                $mes = $arrVinculo[0]['mes'];
                                                $ano = $arrVinculo[0]['ano'];
                                                
                                            }else{
                                                $referenteMesAno = '';
                                                $mes=0;
                                                $ano=0;
                                                
                                            }   
                                            if(($arrVinculo[0]['fk_idFuncionalidade']!="")
                                                &&($arrVinculo[0]['fk_idItemFuncionalidade']!="")
                                                &&($arrVinculo[0]['loginUsuario']!="")        
                                                &&($vetorTickets['tituloTicket']!="") 
                                                &&(htmlspecialchars(retornaNomeCompletoOrganismoAfiliado($vetorTickets['fk_idOrganismoAfiliado']))!="") 
                                                &&(htmlspecialchars(retornaNomeFormatado($arrVinculo[0]['nomeUsuario']))!="") 
                                               
                                                    
                                                    ){
                                            ?>
                                            <center>
                                                <a href="#" onclick="document.getElementById('motivo').innerHTML=document.getElementById('motivoHidden<?php echo $vetorTickets['idTicket'];?>').value;document.getElementById('idFuncionalidade').value='<?php echo $arrVinculo[0]['fk_idFuncionalidade'];?>';document.getElementById('idItemFuncionalidade').value='<?php echo $arrVinculo[0]['fk_idItemFuncionalidade'];?>';document.getElementById('usuarioToken').value='<?php echo $arrVinculo[0]['loginUsuario'];?>';document.getElementById('documento').innerHTML='<?php echo $vetorTickets['tituloTicket']; ?>';document.getElementById('oaSolicitante').innerHTML='<?php echo htmlspecialchars(retornaNomeCompletoOrganismoAfiliado($vetorTickets['fk_idOrganismoAfiliado'])); ?>';document.getElementById('solicitante').innerHTML='<?php echo htmlspecialchars(retornaNomeFormatado($arrVinculo[0]['nomeUsuario']));?>';document.getElementById('referenteMesAno').innerHTML='<?php echo $referenteMesAno;?>';document.getElementById('mesToken').value='<?php echo $mes;?>';document.getElementById('anoToken').value='<?php echo $ano;?>';document.getElementById('idOrganismoAfiliadoLiberarToken').value='<?php echo $vetorTickets['fk_idOrganismoAfiliado'];?>';document.getElementById('tbMesAno').style.display='<?php if($ano==0&&$mes==0){ echo "none";}else{ echo "block"; }?>';" data-target="#mySeeToken" data-toggle="modal" data-placement="left">
                                                    <span class="badge badge-info"><i class="fa fa-key"></i> Liberar token</span>
                                                </a>
                                            </center>
                                            <input type="hidden" name="motivoHidden<?php echo $vetorTickets['idTicket'];?>" id="motivoHidden<?php echo $vetorTickets['idTicket'];?>" value="<?php echo htmlspecialchars($arrVinculo[0]['motivo']);?>">
                                                <?php }?>
                                        <?php }?>
                                    </td>
                                    <td class="project-title">
                                        <a href="?corpo=buscaTicketDetalhe&d=<?php echo $vetorTickets['idTicket']; ?>"><?php echo $vetorTickets['tituloTicket']; ?></a>
                                        <br/>
                                        <small>
                                            <?php
                                            $descricaoTicket = str_split($vetorTickets['descricaoTicket'],40);
                                            $descricaoFormatado = strlen($descricaoTicket[0]) == 40 ? $descricaoTicket[0]."..." : $descricaoTicket[0];
                                            echo $descricaoFormatado."<br>";
                                            ?>
                                        </small>
                                        <?php /*if($tags){ ?>
                                        <br/> <small>Tag: <?php foreach($tags as $tagsVetor){ echo $tagsVetor." "; } ?> </small>
                                        <?php } */?>
                                    </td>
                                    <td class="project-completion">
                                        <?php
                                        if($vetorTickets['criadoPorDepartTicket'] == 1){
                                        ?>
                                            <b>De: </b>
                                            <small><?php echo retornaNomeCompletoOrganismoAfiliado($vetorTickets['fk_idOrganismoAfiliado']); ?></small>
                                            <br>
                                            <b>Para: </b>
                                            <small>Departamento de Organismos Afiliados</small>
                                        <?php
                                        } else {
                                        ?>
                                            <b>De: </b>
                                            <small>Departamento de Organismos Afiliados</small>
                                            <br>
                                            <b>Para: </b>
                                            <small><?php echo retornaNomeCompletoOrganismoAfiliado($vetorTickets['fk_idOrganismoAfiliado']); ?></small>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <dt>Criado por:</dt>
                                        <dd><?php
                                            if($vetorTickets['criadoPorTicket'] != 0){
                                                $retornoUsuario = $uc->buscaUsuario($vetorTickets['criadoPorTicket']);
                                                echo retornaNomeFormatado($retornoUsuario->getNomeUsuario(),3);
                                                $retornoUsuario->setNomeUsuario('');
                                            } else {
                                                echo "Erro!";
                                            }
                                            ?>
                                        </dd>
                                        <dt>Atribuído para:</dt>
                                        <dd><?php
                                            if($vetorTickets['atribuidoTicket'] != 0){
                                                $retornoUsuario = $uc->buscaUsuario($vetorTickets['atribuidoTicket']);
                                                echo retornaNomeFormatado($retornoUsuario->getNomeUsuario(),3);
                                                $retornoUsuario->setNomeUsuario('');
                                            } else {
                                                echo "Não atribuído!";
                                            }
                                            ?>
                                        </dd>
                                    </td>
                                    <td>
                                        <dt>Criado em:</dt>
                                        <dd>
                                            <?php
                                            $dataCriacao = substr($vetorTickets['dataCriacaoTicket'],8,2)."/".substr($vetorTickets['dataCriacaoTicket'],5,2)."/".substr($vetorTickets['dataCriacaoTicket'],0,4) . " às " . substr($vetorTickets['dataCriacaoTicket'],11,5);
                                            echo $dataCriacao != "00/00/0000 às 00:00" ? $dataCriacao : "...";
                                            ?>
                                        </dd>
                                        <dt>Última atualização:</dt>
                                        <dd>
                                            <?php
                                            $dataAtualizacao = substr($vetorTickets['dataAtualizacaoTicket'],8,2)."/".substr($vetorTickets['dataAtualizacaoTicket'],5,2)."/".substr($vetorTickets['dataAtualizacaoTicket'],0,4) . " às " . substr($vetorTickets['dataAtualizacaoTicket'],11,5);
                                            echo $dataAtualizacao != "00/00/0000 às 00:00" ? $dataAtualizacao : "...";
                                            ?>
                                        </dd>
                                    </td>
                                    <td class="project-actions">
                                        <a href="?corpo=buscaTicketDetalhe&d=<?php echo $vetorTickets['idTicket']; ?>" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> Visualizar </a>
                                    </td>
                                </tr>
                            <?php
                                }
                            } else {
                            ?>
                            <tr>
                                <td>
                                    <center>
                                        Nenhuma Notificação Recebida
                                    </center>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row m-b-sm m-t-sm">
                        <center>
                            <div class="btn-group">
                                <?php
                                if(($nroPaginas-1) >= 1) {
                                    if (($pagina - 1) >= 1) {
                                        if (($pagina - 1) != 1) { 
                                            ?>
                                            <a type="button" href='?corpo=buscaTicket&pagina=1&status=<?php echo $status; ?>&funcionalidade=<?php echo $funcionalidade; ?>&finalidade=<?php echo $finalidade; ?>&organismo=<?php echo $organismo; ?>&atribuicao=<?php echo $atribuicao; ?>' class="btn btn-white">
                                                <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>
                                            </a>
                                        <?php } ?>
                                        <a type="button" href='?corpo=buscaTicket&pagina=<?php echo $pagina - 1 ?>&status=<?php echo $status; ?>&funcionalidade=<?php echo $funcionalidade; ?>&finalidade=<?php echo $finalidade; ?>&organismo=<?php echo $organismo; ?>&atribuicao=<?php echo $atribuicao; ?>'
                                           class="btn btn-white">
                                            <i class="fa fa-chevron-left"></i>
                                        </a>
                                        <?php
                                    }
                                    for ($i = 1; $i < ($nroPaginas + 1); $i++) {
                                        if ((($i - 3) <= $pagina) && (($i + 3) >= $pagina)) {
                                            $active = ($i == $pagina) ? "btn btn-white active" : "btn btn-white";
                                            echo "<a href='?corpo=buscaTicket&pagina=" . $i . "&status=" . $status . "&funcionalidade=" . $funcionalidade . "&finalidade=" . $finalidade . "&organismo=" . $organismo . "&atribuicao=" . $atribuicao . "' class='" . $active . "'>" . $i . "</a>";
                                        }
                                    }
                                    if (($pagina) < $nroPaginas) { ?>
                                        <a type="button" href='?corpo=buscaTicket&pagina=<?php echo $pagina + 1 ?>&status=<?php echo $status; ?>&funcionalidade=<?php echo $funcionalidade; ?>&finalidade=<?php echo $finalidade; ?>&organismo=<?php echo $organismo; ?>&atribuicao=<?php echo $atribuicao; ?>'
                                           class="btn btn-white">
                                            <i class="fa fa-chevron-right"></i>
                                        </a>
                                        <?php
                                        if (($pagina + 1) != $nroPaginas) { ?>
                                            <a type="button" href='?corpo=buscaTicket&pagina=<?php echo $nroPaginas ?>&status=<?php echo $status ?>&funcionalidade=<?php echo $funcionalidade ?>&finalidade=<?php echo $finalidade ?>&organismo=<?php echo $organismo ?>&atribuicao=<?php echo $atribuicao ?>'
                                               class="btn btn-white">
                                                <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i>
                                            </a>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeToken" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-unlock modal-icon"></i>
                <h4 class="modal-title">Liberar Token para Usuário</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-danger alert-dismissable">
                        <b>Solicitante:</b> <span id="solicitante"></span> <br>
                        <b>Organismo:</b> <span id="oaSolicitante"></span> <br>
                        <b>Documento:</b> <span id="documento"></span> <span id="referenteMesAno"></span><br>
                        <b>Motivo:</b> <span id="motivo"></span> 
                    </div>
                    <div class="alert alert-warning alert-dismissable">
                        <table id="tbMesAno">
                            <tr>
                                <td width="90px"><br>Mês:</td>
                                <td colspan="2">
                                    <br><input type="text" name="mesToken" id="mesToken">
                                </td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Ano:</td>
                                <td colspan="2">
                                    <br><input type="text" name="anoToken" id="anoToken">
                                </td>
                            </tr>
                        </table>
                        <br>
                        <table>
                            <tr>
                                <td width="130px">Login no SOA:</td>
                                <td><input type="text" name="usuarioToken" id="usuarioToken" /></td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Data Inicial:</td>
                                <td><br><input type="text" class="data" name="dataInicial" id="dataInicial" value="<?php echo date('d/m/Y');?>"></td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Data Final:</td>
                                <td>
                                    <br><input type="text" class="data" name="dataFinal" id="dataFinal">
                                </td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Liberar em massa:</td>
                                <td>
                                    <br><input type="checkbox" name="liberarEmMassa" id="liberarEmMassa">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <br>
                                    <input type="hidden" name="idOrganismoAfiliadoLiberarToken" id="idOrganismoAfiliadoLiberarToken" value=""/>
                                    <input type="hidden" name="idFuncionalidade" id="idFuncionalidade" />
                                    <input type="hidden" name="idItemFuncionalidade" id="idItemFuncionalidade" />
                                    <button class="btn btn-sm btn-danger" type="button" onclick="liberarTokenUsuario();">
                                        <i class="fa fa-key"></i>Liberar
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
