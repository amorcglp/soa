<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Publicações | Administração</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Início</a>
            </li>
            <li>
                <a>Publicações</a>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">

                    <div class="m-b-lg">

                        <div class="input-group">
                            <input type="text" placeholder="Digite o que deseja buscar..." class=" form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-white"> Procurar</button>
                            </span>
                        </div>

                        <div class="m-t-md">

                            <div class="pull-right">
                                <button type="button" class="btn btn-sm btn-white" onclick="window.location.href='?corpo=muralDigitalEstatisticas'" formtarget="_blank"> <i class="fa fa-area-chart"></i></button>
                            </div>

                            <strong>Listagem das Postagens</strong>

                        </div>

                    </div>

                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Tipo de Postagem </th>
                                        <th>Quem </th>
                                        <th>Quando</th>
                                        <th>Postagem Completa</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <!-- SE FOI LIDA -->
                                    <tr>
                                        <td><span class="label label-primary">Lida</span></td>
                                        <td>Notícia</td>
                                        <td><span class="pie">Loja R+C Não Identificado</span></td>
                                        <td>00/00/0000 - 00:00:00</td>
                                        <td>
                                            <button type="button" class="btn btn-w-m btn-info" data-toggle="modal" data-target="#myModal">Ver Mais</button>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-circle btn-outline" type="button">
                                                <i class="fa fa-heart"></i>
                                            </button>
                                            <button class="btn btn-warning btn-circle" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </td>
                                    </tr>

                                    <!-- SE NÃO FOI LIDA -->
                                    <tr>
                                        <td><span class="label label-danger">Não Lida</span></td>
                                        <td>Evento</td>
                                        <td><span class="pie">Loja R+C Não Identificado</span></td>
                                        <td>00/00/0000 - 00:00:00</td>
                                        <td>
                                            <button type="button" class="btn btn-w-m btn-info" data-toggle="modal" data-target="#myModal">Ver Mais</button>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-circle btn-outline" type="button">
                                                <i class="fa fa-heart"></i>
                                            </button>
                                            <button class="btn btn-warning btn-circle" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL -->

    <div class="modal inmodal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 900px !important;">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Organismo Afiliado: | Publicado em: 00/00/0000</h4>
                    <h3 class="font-bold">Tipo da Postagem: Evento | Para: Região</h3>
                </div>

                <div class="modal-body">

                    <h3>Postagem Completa</h3>

                    <div class="feed-activity-list">
                        <div class="feed-element">
                            <a href="#" class="pull-left">
                                <img alt="image" class="img-circle" src="http://webapplayers.com/inspinia_admin-v2.7/img/profile_small.jpg">
                            </a>
                            
                            <div class="media-body ">
                                <strong>Alguém da Silva</strong><br>
                                <span class="date">00/00/0000 - 00:00</span>

                                <div class="well">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                    Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                </div>

                                <div class="photos">
                                    <a target="_blank" href="#">
                                        <img alt="image" class="feed-photo" src="http://www.uniwallpaper.com/static/images/eiffel-tower-wallpaper-18_fRZLW4V.jpg">
                                    </a>

                                </div>

                            </div>

                        </div>


                        <div class="panel-body">

                            <h3>Comentários</h3>

                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                    <div class="feed-activity-list">
                                        <div class="feed-element">
                                            <a href="#" class="pull-left">
                                                <img alt="image" class="img-circle" src="http://webapplayers.com/inspinia_admin-v2.7/img/a2.jpg">
                                            </a>
                                            <div class="media-body ">
                                                <small class="pull-right">
                                                    <button type="button" class="btn btn-danger btn-xs">Excluir</button>
                                                </small>
                                                <strong>Alguém da Silva</strong>
                                                <small class="text-muted">00/00/0000 - 00:00:00</small>
                                                <div class="well">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                                    Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline btn-warning">Favoritar</button>
                        <button type="button" class="btn btn-outline btn-warning">Excluir</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
