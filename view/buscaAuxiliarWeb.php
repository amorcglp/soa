<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/usuarioController.php");
$um     = new Usuario();

require_once ("controller/organismoController.php");
$oc     = new organismoController();

include_once("model/auxiliarWebAssinadoClass.php");
$awa = new auxiliarWebAssinado();

$anexo 	= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;

/**
 * Upload do Termo Assinado
 */
$termoAssinado=0;
$extensaoNaoValida = 0;
if($anexo != "")
{
	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo']['name'], '.');
                if ($extensao == ".pdf" ||
                        $extensao == ".png" ||
                        $extensao == ".jpg" ||
                        $extensao == ".jpeg" ||
                        $extensao == ".PDF" ||
                        $extensao == ".PNG" ||
                        $extensao == ".JPG" ||
                        $extensao == ".JPEG"
                ) {
                    $proximo_id = $awa->selecionaUltimoId();
                    $caminho = "img/auxiliar_web/" . basename($_POST["idTermoUpload"].".termo.compromisso.".$proximo_id);
                    $awa->setFkIdAuxiliarWeb($_POST["idTermoUpload"]);
                    $awa->setCaminho($caminho);
                    if($awa->cadastroAuxiliarWebAssinado())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/auxiliar_web/';
                            $uploadfile = $uploaddir . basename($_POST["idTermoUpload"].".termo.compromisso.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            {
                                    $termoAssinado = 1;
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o termo assinado!');</script>";
                            }
                    }else{
                        //echo "<script type='text/javascript'>alert('Erro ao enviar a ata assinada2!');</script>";
                    }
                } else {
                    $extensaoNaoValida = 1;
                    //echo "<script type='text/javascript'>alert('Erro ao enviar a ata assinada3!');</script>";
            }
        }else{
            $extensaoNaoValida = 2;
            //echo "<script type='text/javascript'>alert('Erro ao enviar a ata assinada4!');</script>";
        }
    }
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php
}

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Termo de Voluntariado salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Termo de Voluntariado já cadastrado anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Auxiliar Web</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAuxiliarWeb">Cadastros</a>
            </li>
            <li class="active">
                <strong><a>Auxiliar Web</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Auxiliares</h5>
                    <div class="ibox-tools">
                    	<?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAuxiliarWeb">
                            <i class="fa fa-plus"></i> Novo Termo
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Nome do Auxiliar Web</th>
                                <th>Organismo</th>
                                <th><center>Data do Termo</center></th>
                        <th><center>Termo Assinado</center></th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/auxiliarWebController.php");

                            $tvc = new auxiliarWebController;
                            $resultado = $tvc->listaAuxiliarWeb();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {

                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                                    
                                    $resposta3 = $awa->listaAuxiliarWebAssinado($vetor['idAuxiliarWeb']);
                                    if($resposta3)
                                    {
                                    	$total = count($resposta3);
                                    }else{
                                    	$total = 0;
                                    }
                                    
                                    
                                    ?>
                                    <?php if((strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA")))||(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2))){?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['nome']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                        </td>
                                        <td>
                                <center>
                                    <?php echo substr($vetor['dataTermoCompromisso'], 8, 2) . "/" . substr($vetor['dataTermoCompromisso'], 5, 2) . "/" . substr($vetor['dataTermoCompromisso'], 0, 4); ?>
                                </center>
                                </td>
                                <td>
                                    <center><?php if($total>0){?><button class="btn btn-info  dim" type="button" onclick="listaUploadAuxiliarWeb('<?php echo $vetor['idAuxiliarWeb']; ?>')" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"><i class="fa fa-paste"></i> </button><?php }else{ echo "Não enviado";}?></center>
                                </td>
                                <td>
                                <center>
                                    <!--
                                    <button type="button" class="btn btn-sm btn-info" onclick="mostraDetalhesAuxiliarWeb('<?php echo $vetor['idAuxiliarWeb']; ?>');" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Detalhes do Termo de Voluntariado">
                                        <i class="fa fa-search-plus fa-white"></i>&nbsp;
                                        Detalhes
                                    </button>
                                    -->
                                    <?php if(in_array("2",$arrNivelUsuario)){?>
                                    <a href="?corpo=alteraAuxiliarWeb&id=<?php echo $vetor['idAuxiliarWeb']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar">
                                        <i class="fa fa-edit fa-white"></i>&nbsp;
                                        Editar
                                    </a>
                                    <?php }?>
                                    <button type="button" class="btn btn-sm btn-success" onclick="abrirPopUpAuxiliarWeb('<?php echo $vetor['idAuxiliarWeb']; ?>');" data-toggle="tooltip" data-placement="left" title="Imprimir ATA de Reunião Mensal dos OA's">
                                        <i class="fa fa-print fa-white"></i>&nbsp;
                                        Imprimir
                                    </button>
                                    <?php if(in_array("4",$arrNivelUsuario)){?>
                                    <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('idTermoUpload').value=<?php echo $vetor['idAuxiliarWeb']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload">
                                        <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                        Enviar Termo Assinado
                                    </button>
                                    <?php }?>
                                    <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                            <a class="btn btn-sm btn-danger" href="#" onclick="abreModalNotificacao(<?php echo $vetor['fk_idOrganismoAfiliado'];?>,null)">
                                                <i class="fa fa-envelope icon-white"></i>
                                                Notificar
                                            </a>
                                    <?php } ?>
                                </center>
                                </td>
                                </tr>
                            <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes do Termo de Voluntariado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Ata:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="dataAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora inicial:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaInicialAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora do encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaFinalAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número de membros:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="numeroMembrosAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Membro que presidiu:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="nomePresidenteAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Oficiais Administrativos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="oficiaisAdministrativos"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura do relatório mensal anterior com as seguintes restrições:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_um"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura da ata anterior:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_dois"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos Pendentes:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_quatro"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos novos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_cinco"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Expansão da Ordem:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_seis"></span></div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações da Suprema Grande Loja, da GLP e Grande Conselheiro(a):</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_oito"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações Gerais:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_nove"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Relatório das Comissões:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_tres"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Palavra livre:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_sete"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_dez"></span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Termo Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="termo" name="termo" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="idTermoUpload" name="idTermoUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.termo.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUploadJuramento" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Juramento Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="juramento" name="juramento" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo2"
								id="anexo2" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="idJuramentoUpload" name="idJuramentoUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.juramento.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Termo Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUploadJuramento" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Juramento Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUploadJuramento"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php 
modalNotificacao($sessao->getValue("seqCadast"));
?>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
