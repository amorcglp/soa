<?php
include_once("controller/anotacoesController.php");
require_once ("model/anotacoesClass.php");

$ac = new anotacoesController();
$dados = $ac->buscaAnotacoes($_GET['idAnotacoes']);
?>

<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Anotações Pessoais</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAnotacoes">Anotações Pessoais</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Nova Anotação Pessoal</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Nova Anotação Pessoal</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAnotacoes">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título</label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloAnotacoes" id="tituloAnotacoes" class="form-control" value="<?php echo $dados->getTituloAnotacoes(); ?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_anotacoes">
                            <label class="col-sm-2 control-label">Data</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAnotacoes" onkeypress="valida_data(this)" maxlength="10" id="dataAnotacoes" type="text" class="form-control" value="<?php echo $dados->getDataAnotacoes(); ?>" style="max-width: 105px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Hora</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="horaAnotacoes" onkeypress="valida_horas(this)" maxlength="5" id="horaAnotacoes" type="text" class="form-control" value="<?php echo $dados->getHoraAnotacoes(); ?>" style="max-width: 105px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Conteúdo</label>
                            <div class="col-sm-10">
                                <textarea rows="7" name="conteudoAnotacoes" id="conteudoAnotacoes" class="form-control" value="" style="max-width: 500px"><?php echo $dados->getConteudoAnotacoes(); ?></textarea>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <input type="hidden" name="idAnotacoes" id="idAnotacoes" value="<?php echo $_GET['idAnotacoes']; ?>" />
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>