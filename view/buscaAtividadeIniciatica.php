<!-- Conteúdo DE INCLUDE INICIO -->
<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("model/listaMembrosAtividadeIniciaticaAssinadaClass.php");
$lma = new listaMembrosAtividadeIniciaticaAssinada();

include_once("model/listaOficiaisAtividadeIniciaticaAssinadaClass.php");
$loa = new listaOficiaisAtividadeIniciaticaAssinada();

/**
 * Upload da lista de membros assinada
 */
$anexo 	= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;

$listaMembrosAssinada=0;
$extensaoNaoValida = 0;
if($anexo != "")
{
    //echo "aquiiiiiii ooooooo braz...";
	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo']['name'], '.');
                if ($extensao == ".pdf" ||
                        $extensao == ".png" ||
                        $extensao == ".jpg" ||
                        $extensao == ".jpeg" ||
                        $extensao == ".PDF" ||
                        $extensao == ".PNG" ||
                        $extensao == ".JPG" ||
                        $extensao == ".JPEG"
                ) {
                    //echo "vai carregar os dados para o cadastro";
                    $proximo_id = $lma->selecionaUltimoId();
                    $caminho = "img/lista_membros_atividade_iniciatica/" . basename($_POST["idListaMembrosUpload"].".lista.membros.atividade.iniciatica.".$proximo_id);
                    $lma->setFk_idAtividadeIniciatica($_POST["idListaMembrosUpload"]);
                    $lma->setCaminho($caminho);
                    if($lma->cadastroListaMembrosAssinada())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/lista_membros_atividade_iniciatica/';
                            $uploadfile = $uploaddir . basename($_POST["idListaMembrosUpload"].".lista.membros.atividade.iniciatica.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            {
                                    $listaMembrosAssinada = 1;
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar a lista de membros assinada!');</script>";
                            }
                    }
                } else {
                    $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}

if($listaMembrosAssinada==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Lista de Membros assinada enviada com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php
}

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

$anexo2 	= isset($_FILES['anexo2']['name'])?$_FILES['anexo2']['name']:null;

/**
 * Upload da lista de oficiais assinada
 */
$listaOficiaisAssinada=0;
$extensaoNaoValida = 0;
if($anexo2 != "")
{
    //echo "aqui ooooooooooooooooooooooooooooooo";
	if ($_FILES['anexo2']['name'] != "") {
            if(substr_count($_FILES['anexo2']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo2']['name'], '.');
                if ($extensao == ".pdf" ||
                        $extensao == ".png" ||
                        $extensao == ".jpg" ||
                        $extensao == ".jpeg" ||
                        $extensao == ".PDF" ||
                        $extensao == ".PNG" ||
                        $extensao == ".JPG" ||
                        $extensao == ".JPEG"
                ) {
                    $proximo_id = $loa->selecionaUltimoId();
                    $caminho = "img/lista_oficiais_atividade_iniciatica/" . basename($_POST["idListaOficiaisUpload"].".lista.oficiais.atividade.iniciatica.".$proximo_id);
                    //echo $caminho;
                    $loa->setFk_idAtividadeIniciatica($_POST["idListaOficiaisUpload"]);
                    $loa->setCaminho($caminho);
                    if($loa->cadastroListaOficiaisAssinada())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/lista_oficiais_atividade_iniciatica/';
                            $uploadfile = $uploaddir . basename($_POST["idListaOficiaisUpload"].".lista.oficiais.atividade.iniciatica.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo2']['tmp_name'], $uploadfile))
                            {
                                    $listaOficiaisAssinada = 1;
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o juramento assinado!');</script>";
                            }
                    }
                } else {
                    $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}

if($listaOficiaisAssinada==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Lista de Oficiais assinada enviada com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php
}

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades Iniciáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Atividades Iniciáticas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<?php
/*
if($sessao->getValue("fk_idDepartamento")==1 || $sessao->getValue("fk_idDepartamento")==3) {
?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Atividades Iniciáticas</h5>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="alert alert-success">
                            <center>
                                Módulo ainda não disponível. <br>
                                <a class="alert-link">Calma! Falta pouco :]</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
*/
include_once("controller/atividadeIniciaticaController.php");
include_once("controller/atividadeIniciaticaOficialController.php");
include_once("controller/organismoController.php");
include_once("controller/atividadeIniciaticaColumbaController.php");

$aim = new atividadeIniciatica();
$aic = new atividadeIniciaticaController();
$aioc = new atividadeIniciaticaOficialController();
$oc = new organismoController();
$om = new organismo();
$sessao = new criaSessao();
$aicc = new atividadeIniciaticaColumbaController();

if($sessao->getValue("alerta-atividade-iniciatica")==null){
    if (($funcoesUsuarioString == 0 && $_SESSION['fk_idDepartamento'] != 1) || $regiaoUsuario != null || $_SESSION['fk_idDepartamento'] == 2 || $_SESSION['fk_idDepartamento'] == 3) {
    ?>
        <!--
				<script>
            window.onload = function(){
                swal({
                    title: "Aviso!",
                    text: "Você pode mudar o organismo listado no cabeçalho!",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
				-->
        <?php
    }
    $sessao->setValue("alerta-atividade-iniciatica", true);
}

$idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';

if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
    $idOrg = $idOrganismo;
} else {
    $idOrg = $idOrganismoAfiliado;
}

?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Atividades Iniciáticas
                        <?php
                        if($idOrganismoAfiliado!=''){
                            $nomeOa = retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);
                            echo ' - '.$nomeOa;
                        }
                        ?>
                    </h5>
                    <div class="ibox-tools">
                        <?php if($idOrganismoAfiliado!=''){ ?>
                            <?php if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){ ?>
                            <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciatica">
                                <i class="fa fa-undo"></i> Eliminar Filtro
                            </a>
                            <?php } ?>
                        <?php } ?>
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciaticaColumba&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>">
                            <i class="fa fa-share"></i> Columbas
                        </a>
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciaticaOficial&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>">
                            <i class="fa fa-share"></i> Equipes Iniciáticas
                        </a>
                        <a class="btn btn-xs btn-info" style="color: white" href="?corpo=buscaAtividadeIniciaticaAgil&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>">
                            <i class="fa fa-users"></i> Cadastro Ágil
                        </a>
                        <?php if(!$usuarioApenasLeitura){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeIniciatica">
                            <i class="fa fa-plus"></i> Agendar Nova
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <?php if($idOrg==''){ ?>
                                    <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                        <th>Organismo</th>
                                    <?php } ?>
                                <?php } ?>
                                <th>Grau</th>
                                <th><center>Responsável</center></th>
                                <th><center>Data</center></th>
                                <th><center>Registrado em</center></th>
                        		<th><center>Status</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                        <?php
                        $resultado = $aic->listaAtividadeIniciatica($idOrganismoAfiliado, null, null, null,  null, null, false);
                        if($idOrganismoAfiliado==235)
                        {
                            //echo "<pre>";print_r($resultado);
                        }

                        if ($resultado) {
                            foreach ($resultado as $vetor) {
                                if($vetor['fk_idAtividadeIniciaticaOficial']!=0)
                                {
                                    $dadosEquipe = $aioc->buscaAtividadeIniciaticaOficial($vetor['fk_idAtividadeIniciaticaOficial']);
                                }else{
                                    $dadosEquipe=array();
                                }

                                if($vetor['statusAtividadeIniciatica'] != 3){
                        ?>
                            <tr>
                                <td>
                                    <?php echo $vetor['idAtividadeIniciatica']; ?>
                                </td>
                                <?php if($idOrg==''){ ?>
                                    <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                        <td>
                                            <?php // if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                                <!--<a class="btn-xs btn-primary btn-outline" href="?corpo=buscaAtividadeIniciatica&idOrganismoAfiliado=<?php // echo $vetor['fk_idOrganismoAfiliado']; ?>" data-rel="tooltip" title="">Filtrar</a>-->
                                            <?php // } ?>
                                            <?php
                                                echo retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
                                            ?>
                                        </td>
                                    <?php } ?>
                                <?php } ?>
                                <td>
                                    <?php echo retornaTipoAtividadeParaExibicao($vetor['tipoAtividadeIniciatica']); ?>
                                </td>
                                <td>
                                    <center>
                                        <div class="row">
                                            <span>Equipe<br> Iniciática <?php if(count($dadosEquipe)>0) { echo $dadosEquipe->getTipoAtividadeIniciaticaOficial(); } ?></span>
                                        </div>
                                        <div class="row" style="margin-top: 7px">
                                            <?php
                                            $dadosColumba = $aicc->buscaAtividadeIniciaticaColumbaPorId($vetor['fk_idAtividadeIniciaticaColumba']);
                                            //echo "<pre>";print_r($vetor['fk_idAtividadeIniciaticaColumba']);
                                            if($dadosColumba!=false)
                                            {
                                            ?>
                                            <a class="btn-xs btn-success"
                                               href=""
                                               data-toggle="modal"
                                               data-target="#modalOficiais<?php echo $vetor['fk_idAtividadeIniciaticaOficial']; ?>"
                                               onclick="modificaOficiaisSuplentes('<?php echo $vetor['idAtividadeIniciatica']; ?>','<?php echo $vetor['fk_idAtividadeIniciaticaOficial']; ?>');
                                                   modificaColumbaRecepcao('<?php if($dadosColumba->getSeqCadastAtividadeIniciaticaColumba()){ echo $dadosColumba->getSeqCadastAtividadeIniciaticaColumba();}else{ echo "0";} ?>',
																									 '<?php echo ucwords(mb_strtolower($dadosColumba->getNomeAtividadeIniciaticaColumba())); ?>',
																									 '<?php echo $vetor['codAfiliacaoRecepcao']; ?>',
																									 '<?php echo ucwords(mb_strtolower($vetor['nomeRecepcao'])); ?>',
																									 '<?php echo $vetor['fk_idAtividadeIniciaticaOficial']; ?>',
																									 '<?php echo $vetor['idAtividadeIniciatica']; ?>'
																									 );">Visualizar </a>
                                            <?php } ?>
                                        </div>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                    <?php echo substr($vetor['dataRealizadaAtividadeIniciatica'], 8, 2) . "/" . substr($vetor['dataRealizadaAtividadeIniciatica'], 5, 2) . "/" . substr($vetor['dataRealizadaAtividadeIniciatica'], 0, 4) . ' às ' . substr($vetor['horaRealizadaAtividadeIniciatica'], 0, 2) . ":" . substr($vetor['horaRealizadaAtividadeIniciatica'], 3, 2);?>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                    <?php echo substr($vetor['dataRegistroAtividadeIniciatica'], 8, 2) . "/" . substr($vetor['dataRegistroAtividadeIniciatica'], 5, 2) . "/" . substr($vetor['dataRegistroAtividadeIniciatica'], 0, 4) . " - " . substr($vetor['dataRegistroAtividadeIniciatica'], 11, 2) . ":" . substr($vetor['dataRegistroAtividadeIniciatica'], 14, 2); ?>
                                    <?php if($vetor['dataAtualizadaAtividadeIniciatica']!='0000-00-00 00:00:00'){ ?>
                                        <br>
                                        <div>
                                            Última atualização em: <br>
                                            <?php echo substr($vetor['dataAtualizadaAtividadeIniciatica'], 8, 2) . "/" . substr($vetor['dataAtualizadaAtividadeIniciatica'], 5, 2) . "/" . substr($vetor['dataAtualizadaAtividadeIniciatica'], 0, 4) . " - " . substr($vetor['dataAtualizadaAtividadeIniciatica'], 11, 2) . ":" . substr($vetor['dataAtualizadaAtividadeIniciatica'], 14, 2);?>
                                        </div>
                                    <?php } ?>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <div class="statusTarget<?php echo $vetor['idAtividadeIniciatica'] ?>">
                                            <?php echo retornaStatusAtividadeSpan($vetor['statusAtividadeIniciatica']); ?>
                                            <?php
                                            if(($vetor['statusAtividadeIniciatica'] == 2)){
                                                if (($sessao->getValue("fk_idDepartamento") == 2) || ($sessao->getValue("fk_idDepartamento") == 3)){
                                                    echo "<br><br>";
                                                    $organismo = isset($_GET['idOrganismoAfiliado']) ? $_GET['idOrganismoAfiliado'] : 0;
                                                    ?>
                                                    <a class="btn btn-sm btn-danger"
                                                       onclick="cancelarPosteriormenteAtividadeIniciatica(<?php echo $vetor['idAtividadeIniciatica']; ?>,<?php echo $organismo; ?>,0)">
                                                        <i class="fa fa-times-circle fa-white"></i>
                                                        Cancelar Posteriormente
                                                    </a>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <?php if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){ ?>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-warning btn-sm dropdown-toggle"><i class="fa fa-share fa-white"></i>&nbsp; Filtrar <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <?php if($idOrganismo==''){ ?>
                                                <li><a href="?corpo=buscaAtividadeIniciatica&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Atividades Iniciáticas deste Organismo</a></li>
                                                <li class="divider"></li>
                                                <?php } ?>
                                                <li><a href="?corpo=buscaAtividadeIniciaticaOficial&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Equipes Iniciáticas deste Organismo</a></li>
                                                <li><a href="?corpo=buscaAtividadeIniciaticaColumba&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Columbas deste Organismo</a></li>
                                                <li><a href="?corpo=buscaAtividadeIniciaticaMembro&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Membros com Pendências</a></li>
                                            </ul>
                                        </div>
                                        <?php } ?>
                                        <!--
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target=""><i class="fa fa-search-plus fa-white"></i> Detalhes</a>
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#mySeeDetalhes"><i class="fa fa-search-plus fa-white"></i> Detalhes</a>
                                        -->
                                        <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                        <!--<br>-->
                                        <?php } ?>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-warning btn-sm dropdown-toggle"><i class="fa fa-share fa-white"></i>&nbsp; Arquivos <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" onclick="document.getElementById('idListaMembrosUpload').value=<?php echo $vetor['idAtividadeIniciatica']; ?>">Enviar Lista Assinada de Membros</a></li>
                                                <li><a href="#" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left" onclick="listaUploadMembrosAtividadeIniciatica('<?php echo $vetor['idAtividadeIniciatica']; ?>')">Listas Assinadas de Membros</a></li>
                                                <li><a href="#" data-target="#mySeeUploadOficiais" data-toggle="modal" data-placement="left" onclick="document.getElementById('idListaOficiaisUpload').value=<?php echo $vetor['idAtividadeIniciatica']; ?>">Enviar Lista Assinada de Oficiais</a></li>
                                                <li><a href="#" data-target="#mySeeListaUploadOficiais" data-toggle="modal" data-placement="left" onclick="listaUploadOficiaisAtividadeIniciatica('<?php echo $vetor['idAtividadeIniciatica']; ?>')">Listas Assinadas de Oficiais</a></li>
                                            </ul>
                                        </div>
                                        <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                            <br>
                                        <?php } ?>
                                        <?php if(($idOrganismoAfiliado == $vetor['fk_idOrganismoAfiliado']) || ($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){ ?>
                                            <?php if ($vetor['statusAtividadeIniciatica'] != 2) { ?>
                                                <a class="btn btn-sm btn-info" href="?corpo=alteraAtividadeIniciatica&idAtividadeIniciatica=<?php echo $vetor['idAtividadeIniciatica']; ?>" data-rel="tooltip" title="">
                                                    <i class="fa fa-edit fa-white"></i>
                                                    Editar
                                                </a>
                                                <span id="status<?php echo $vetor['idAtividadeIniciatica'] ?>">
                                                    <?php if ($vetor['statusAtividadeIniciatica'] == 1) { ?>
                                                        <a class="btn btn-sm btn-primary" onclick="mudaStatus('<?php echo $vetor['idAtividadeIniciatica'] ?>', '<?php echo $vetor['statusAtividadeIniciatica'] ?>', 'atividadeIniciatica')" data-rel="tooltip" title="Ativar">
                                                            Ativar
                                                        </a>
                                                    <?php } else { ?>
                                                        <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idAtividadeIniciatica'] ?>', '<?php echo $vetor['statusAtividadeIniciatica'] ?>', 'atividadeIniciatica')" data-rel="tooltip" title="Inativar">
                                                            Inativar
                                                        </a>
                                                    <?php } ?>
                                                </span>
                                            <?php } ?>
                                        <?php } ?>
                                        <a class="btn btn-sm btn-success"  href="" data-toggle="modal" data-target="#modalMembros" onclick="incluiMembrosCampoHiddenAtividadeIniciatica(<?php echo $vetor['idAtividadeIniciatica'];?>);getMembrosAtividadeIniciatica(<?php echo $vetor['idAtividadeIniciatica'];?>,<?php echo $vetor['statusAtividadeIniciatica'];?>,<?php echo $vetor['fk_idOrganismoAfiliado']; ?>,<?php echo substr($vetor['dataRealizadaAtividadeIniciatica'],8,2);?>,<?php echo substr($vetor['dataRealizadaAtividadeIniciatica'],5,2)?>,<?php echo substr($vetor['dataRealizadaAtividadeIniciatica'],0,4) ?>);">
                                            <i class="fa fa-users fa-white"></i>
                                            Participantes
                                        </a>
                                    </center>
                                    <input type="hidden" id="tipoObrigracao<?php echo $vetor['idAtividadeIniciatica'] ?>" name="tipoObrigracao<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['tipoAtividadeIniciatica']; ?>">
                                    <input type="hidden" id="dataObrigacao<?php echo $vetor['idAtividadeIniciatica'] ?>" name="dataObrigacao<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['dataRealizadaAtividadeIniciatica']."T".$vetor['horaRealizadaAtividadeIniciatica']; ?>">
                                    <input type="hidden" id="descricaoLocal<?php echo $vetor['idAtividadeIniciatica'] ?>" name="descricaoLocal<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['localAtividadeIniciatica']; ?>">
                                    <input type="hidden" id="fk_idOrganismoAfiliado<?php echo $vetor['idAtividadeIniciatica'] ?>" name="fk_idOrganismoAfiliado<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">
                                </td>
                            </tr>
                        <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <input type="hidden" id="loginAtualizadoPor" name="loginAtualizadoPor" value="<?php echo $sessao->getValue("loginUsuario") ?>">
                        <input type="hidden" id="seqAtualizadoPor" name="seqAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->


<!-- Notificar Responsáveis -->
<div class="modal inmodal fade" id="modalMembros" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">
                    <span id="nomeOa"></span>
                    Membros Cadastrados na Atividade <span id="numeroAtividadeIniciatica"></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning">
                    <center>
                        <a class="alert-link" href="#">Atenção!</a> Segundo o Manual Administrativo é permitido um número máximo de ​<a class="alert-link">12 candidatos</a>​ realizar as atividades iniciáticas. Qualquer dúvida entrar em contato com a GLP. <br>
                        Ou seja, mesmo que mais membros tenham ​se apresentado​ a participar, no máximo ​<a class="alert-link">12 membros</a>​ devem ter seus status alterados para 'Compareceu'.
                    </center>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th style="min-width: 200px"><center>Status</center></th>
                            <th style="min-width: 150px"><center>Ações</center></th>
                        </tr>
                    </thead>
                    <tbody id="membrosAtividadeIniciatica"></tbody>
                </table>
                <div id="areaInclusaoMembro"></div>
            </div>
            <div class="modal-footer">
                <div id="botaoAtividadeRealizada"></div>
            </div>
            <input type="hidden" id="contadorClicks" value="0">
            <input type="hidden" name="compareceram" id="compareceram" value="">
            <input type="hidden" name="naoCompareceram" id="naoCompareceram" value="">
        </div>
    </div>
</div>


<?php
$resultado = $aioc->listaAtividadeIniciaticaOficial($idOrganismoAfiliado);
$ocultar_json=1;
if ($resultado) {
    foreach ($resultado as $vetor) {
				//echo "<pre>";print_r($vetor);echo "</pre>";
        $idEquipe       = $vetor['idAtividadeIniciaticaOficial'];
        $equipeTipo   = $vetor['tipoAtividadeIniciaticaOficial'];
        $equipeAno    = $vetor['anoAtividadeIniciaticaOficial'];
        ?>
        <!-- Notificar Responsáveis -->
        <div class="modal inmodal" id="modalOficiais<?php echo $idEquipe;?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <i class="fa fa-users modal-icon"></i>
                        <h4 class="modal-title">
                            <?php
                            if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
                                if($idOrganismo==''){
                                    echo retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']).'<br>';
                                }
                            }
                            ?>
                            Equipe Iniciática <?php echo $vetor['tipoAtividadeIniciaticaOficial'];?><br>
														<?php //echo "mestreAtividadeIniciaticaOficial: ".$vetor['mestreAtividadeIniciaticaOficial'];?>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Mestre: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="mestre<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['mestreAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="mestreAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Mestre Auxiliar: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="mestreAux<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['mestreAuxAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="mestreAuxAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Arquivista: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="arquivista<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['arquivistaAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="arquivistaAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Capelão: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="capelao<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['capelaoAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="capelaoAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Matre: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="matre<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['matreAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="matreAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Grande Sacerdotisa: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="grandeSacerdotisa<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['grandeSacerdotisaAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="grandeSacerdotisaAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Guia: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="guia<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['guiaAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="guiaAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Guardião Interno: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="guardiaoInterno<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['guardiaoInternoAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="guardiaoInternoAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Guardião Externo: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="guardiaoExterno<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['guardiaoExternoAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="guardiaoExternoAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Portador do Archote: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="archote<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['archoteAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="archoteAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Medalhista: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="medalhista<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['medalhistaAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="medalhistaAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Arauto: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="arauto<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['arautoAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="arautoAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Adjutor: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="adjutor<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['adjutorAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="adjutorAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Sonoplasta: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px" id="sonoplasta<?php echo $idEquipe ?>">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php imprimeDadosMembroPeloSeqCadast($vetor['sonoplastaAtividadeIniciaticaOficial']); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="sonoplastaAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Columba: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px">
                                        <div class="form-control-static" id="columba<?php echo $idEquipe; ?>" style="padding-top: 0px; padding-bottom: 10px;">Não definido!</div>
                                    </div>
                                    <div class="col-sm-12" style="max-width: 320px" id="columbaAdjunto<?php echo $idEquipe ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Recepção: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px">
                                        <div class="form-control-static" id="recepcao<?php echo $idEquipe; ?>" style="padding-top: 0px; padding-bottom: 10px;">Não definido!</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Anotações: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetor['anotacoesAtividadeIniciaticaOficial']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#mySeeAdicionarSuplente"
                           onclick="transferirDadosParaModalSuplente(<?php echo $idEquipe; ?>)">
                            <i class="fa fa-plus fa-white"></i>
                            Adicionar Suplente
                        </a>

                        <input type="hidden" id="codAfiliacaoColumba<?php echo $idEquipe; ?>" value="">
                        <input type="hidden" id="nomeColumba<?php echo $idEquipe; ?>" value="">
                        <input type="hidden" id="codAfiliacaoRecepcao<?php echo $idEquipe; ?>" value="">
                        <input type="hidden" id="nomeRecepcao<?php echo $idEquipe; ?>" value="">
                        <input type="hidden" id="atividadeIniciatica<?php echo $idEquipe; ?>" value="">

                        <a class="btn btn-sm btn-primary"
                           onclick="abrirPopUpAtividadeIniciaticaOficialRelacaoParaAssinatura(<?php echo $vetor['idAtividadeIniciaticaOficial']; ?>,
                                                                                               <?php echo $vetor['fk_idOrganismoAfiliado']; ?>)">
                            <i class="fa fa-print fa-white"></i>
                            Imprimir
                        </a>
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>
<div class="modal inmodal" id="mySeeAdicionarSuplente" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user modal-icon"></i>
                <h4 class="modal-title">Adicionar Suplente</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="form-group form-inline">
                            <!--<label class="col-sm-2 control-label">Função: </label>-->
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="tipoOficialSuplente" id="tipoOficialSuplente" style="max-width: 320px" required="required" onchange="selecionaTipoSuplente(this.value)">
                                    <option value="0">Selecione a função</option>
                                    <option value="1">Mestre</option>
                                    <option value="2">Mestre Auxiliar</option>
                                    <option value="3">Arquivista</option>
                                    <option value="4">Capelão</option>
                                    <option value="5">Matre</option>
                                    <option value="6">Grande Sacerdotisa</option>
                                    <option value="7">Guia</option>
                                    <option value="8">Guardião Interno</option>
                                    <option value="9">Guardião Externo</option>
                                    <option value="10">Portador do Archote</option>
                                    <option value="11">Medalhista</option>
                                    <option value="12">Arauto</option>
                                    <option value="13">Adjutor</option>
                                    <option value="14">Sonoplasta</option>
                                    <option value="15">Columba</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="form-group form-inline">
                            <!--<label class="col-sm-12 control-label">Suplente: </label>-->
                            <div class="col-sm-12">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoSuplente" type="text" maxlength="7" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeSuplente" type="text" maxlength="100" value="" style="min-width: 320px"
                                       >
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoSuplente','nomeSuplente','codAfiliacaoSuplente','nomeSuplente','seqCadastSuplente','h_nomeSuplente','myModalPesquisa','informacoesPesquisa','S');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" value="" name="seqCadastSuplente" id="seqCadastSuplente" maxlength="50" class="form-control" style="max-width: 320px">
                            <input type="hidden" value="" name="h_seqCadastSuplente" id="h_seqCadastSuplente">
                            <input type="hidden" value="" name="h_nomeSuplente" id="h_nomeSuplente">
                            <input type="hidden" value="" name="h_seqCadastCompanheiroSuplente" id="h_seqCadastCompanheiroSuplente">
                            <input type="hidden" value="" name="h_nomeCompanheiroSuplente" id="h_nomeCompanheiroSuplente">
                            <input type="hidden" value="" name="tipo" id="tipo">
                        </div>
                    </div>
                </div>
                <input type="hidden" id="idAtividadeIniciaticaModalSuplente" value="">
            </div>
            <div class="modal-footer">
                <input type="hidden" id="idEquipeModal" name="idEquipeModal" value="">
                <a class="btn btn-sm btn-primary"
                   onclick="adicionarSuplenteAtividadeIniciatica()">
                    <i class="fa fa-plus fa-white"></i>
                    Cadastrá-lo
                </a>
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio da Lista de Membros Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="membros" name="membros" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>

					</div>
					<input type="hidden" id="idListaMembrosUpload" name="idListaMembrosUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.membros.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUploadOficiais" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio da Lista de Oficiais Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="oficiais" name="oficiais" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo2"
								id="anexo2" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>

					</div>
					<input type="hidden" id="idListaOficiaisUpload" name="idListaOficiaisUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.oficiais.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Lista de Membros Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUploadOficiais" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Lista de Oficiais Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUploadOficiais"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php //} ?>
