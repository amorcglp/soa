<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php

@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once ('model/isencaoOAClass.php');
include_once ('model/mensalidadeOAClass.php');
include_once ('model/membroOAClass.php');
include_once ('model/organismoClass.php');
include_once ('lib/functions.php');

$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Atividade da Columba salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Esse registro sobre a Columba já foi incluído!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( document ).tooltip();
  });
  </script>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades das Columbas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaAtividadesColumbas">Atividades das Columbas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Atividades das Columbas do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <?php if(in_array("1",$arrNivelUsuario)){?>
                            <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeColumba">
                                <i class="fa fa-plus"></i> Registrar Atividade da Columba
                            </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód. de Afiliação</th>
                                <th>Nome da Columba</th>
                                <th>Idade Atual da Columba</th>
                                <th>Trimestre e ano que atuou</th>
                                <th>Avaliação</th>
		                <th><center>Ações</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("lib/functions.php");
                            include_once("lib/webservice/retornaInformacoesMembro.php");
                            include_once("controller/atividadeColumbaController.php");

                            $m = new atividadeColumbaController();
                            $resultado = $m->listaAtividadeColumba($idOrganismoAfiliado);
							
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                               <?php if(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA"))){

                               	?>
                                    <tr>
                                        <td>
                                            <?php echo retornaCodigoAfiliacao($vetor['seqCadastMembro'],2); ?>
                                        </td>
                                        <td>
                                            <?php echo retornaNomeCompleto($vetor['seqCadastMembro']); ?>
                                        </td>
                                        <td>
                                            <?php echo getIdade(substr(retornaDataNascimento($vetor['seqCadastMembro']),0,10)); ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['trimestre']; ?>/<?php echo $vetor['ano']; ?>
                                        </td>                  
                                        <td>
                                            <?php 
                                                switch ($vetor['avaliacao'])
                                                {
                                                    case 1:
                                                        echo "<span class=\"badge badge-warning\">Regular</span>";
                                                        break;
                                                    case 2:
                                                        echo "<span class=\"badge badge-success\">Bom</span>";
                                                        break;
                                                    case 3:
                                                        echo "<span class=\"badge badge-primary\">Ótima</span>";
                                                        break;
                                                    default:
                                                        echo "Não avaliado";
                                                        break;
                                                }
                                            ?>
                                        </td>
                                        <td>
                                                <center>
                                                    <?php if(!$usuarioApenasLeitura) {?>
                                                        <?php if (in_array("2", $arrNivelUsuario)) { ?>
                                                            <a href="?corpo=alteraAtividadeColumba&idatividade=<?php echo $vetor['idAtividadeColumba']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar">
                                                                <i class="fa fa-edit fa-white"></i>&nbsp;
                                                                Editar
                                                            </a>
                                                        <?php } ?>
                                                    <?php }else{
                                                        echo "<span class=\"btn btn-default btn-rounded\">Apenas Leitura</span>";
                                                    }?>
                                                </center>
                                        </td>
                                </tr>
                            	<?php
                                    }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
            $(document).on('ready', function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
</script>          

<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
