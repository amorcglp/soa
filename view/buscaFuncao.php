<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Função salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Função</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Função</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Funções Cadastradas</h5>
                    <div class="ibox-tools">
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroFuncao">
                            <i class="fa fa-plus"></i> Cadastrar Nova
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Função</th>
                                <th>Vínculo Externo</th>
                                <th>Departamento</th>
                                <th>Status</th>
                                <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/funcaoController.php");

                            $fc = new funcaoController();
                            $resultado = $fc->listaFuncao();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['nomeFuncao']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['vinculoExternoFuncao']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['nomeDepartamento']; ?>
                                        </td>
                                        <td>
                                <center>
                                    <div id="statusTarget<?php echo $vetor['idFuncao'] ?>">
                                        <?php
                                        switch ($vetor['statusFuncao']) {
                                            case 0:
                                                echo "<span class='badge badge-primary'>Ativo</span>";
                                                break;
                                            case 1:
                                                echo "<span class='badge badge-danger'>Inativo</span>";
                                                break;
                                        }
                                        ?>
                                    </div>
                                </center>
                                </td>
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                    <?php if(in_array("2",$arrNivelUsuario)){?>
                                        <a class="btn btn-sm btn-info" href="painelDeControle.php?corpo=alteraFuncao&id=<?php echo $vetor['idFuncao'] ?>" data-rel="tooltip" title="">
                                            <i class="icon-edit icon-white"></i>  
                                            Editar                                            
                                        </a>
                                    <?php }?>
                                    <?php if(in_array("3",$arrNivelUsuario)){?>
                                        <span id="status<?php echo $vetor['idFuncao'] ?>">
        <?php if ($vetor['statusFuncao'] == 1) { ?>
                                                <a class="btn btn-sm btn-primary" href="#" onclick="mudaStatus('<?php echo $vetor['idFuncao'] ?>', '<?php echo $vetor['statusFuncao'] ?>', 'funcao')" data-rel="tooltip" title="Ativar"> 
                                                    Ativar                                            
                                                </a>
        <?php } else { ?>
                                                <a class="btn btn-sm btn-danger" href="#" onclick="mudaStatus('<?php echo $vetor['idFuncao'] ?>', '<?php echo $vetor['statusFuncao'] ?>', 'funcao')" data-rel="tooltip" title="Inativar">
                                                    Inativar                                            
                                                </a>
        <?php } ?>
                                        </span>
                                    <?php }?>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

