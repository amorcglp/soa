<!-- Conteúdo DE INCLUDE INICIO -->
<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("model/convocacaoRitualisticaOGGClass.php");
include_once("model/convocacaoRitualisticaOGGMembroClass.php");
include_once("model/convocacaoRitualisticaOGGNaoMembroClass.php");

$excluir = isset($_REQUEST['excluirConvocacaoRitualistica'])?$_REQUEST['excluirConvocacaoRitualistica']:null;
$idExcluir = isset($_REQUEST['id'])?$_REQUEST['id']:null;

if ($excluir == 1) 
{
    
    //Excluir informação
    $convocacao = new convocacaoRitualisticaOGG();
    if($convocacao->remove($idExcluir))
    {  
        $membros = new convocacaoRitualisticaOGGMembro();
        $membros->remove($idExcluir);
        
        $nao_membros = new convocacaoRitualisticaOGGNaoMembro();
        $nao_membros->remove($idExcluir);
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Item excluido com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php 
    }
}
?>
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Convocação salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>    
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Convocações Ritualísticas - OGG</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Convocações Ritualísticas - OGG</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<?php
/*
if($sessao->getValue("fk_idDepartamento")==1 || $sessao->getValue("fk_idDepartamento")==3) {
?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Atividades Iniciáticas</h5>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="alert alert-success">
                            <center>
                                Módulo ainda não disponível. <br>
                                <a class="alert-link">Calma! Falta pouco :]</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
*/
include_once("controller/convocacaoRitualisticaOGGController.php");

$cr = new convocacaoRitualisticaOGGController();


?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Convocações Ritualísticas - OGG
                    </h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroConvocacoesRitualisticas">
                            <i class="fa fa-plus"></i> Incluir Nova
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                    <th>Organismo</th>
                                    <th><center>Comendador</center></th>
                                    <th><center>Data</center></th>
                                    <th><center>Jovens</center></th>
                                    <th><center>Adultos</center></th>
                                    <th><center>Total</center></th>
                                    <th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                        <?php
                        $resultado = $cr->lista($idOrganismoAfiliado);
 
                        if($resultado)
                        {
                            foreach ($resultado as $vetor)
                            {    
                                $dataConvocacao = substr($vetor['dataConvocacao'],8,2)."/".substr($vetor['dataConvocacao'],5,2)."/".substr($vetor['dataConvocacao'],0,4). " ".$vetor['horaConvocacao'];
                                
                                $membros = new convocacaoRitualisticaOGGMembro();
                                $totalMembrosAdulto = $membros->total($vetor['idConvocacaoRitualisticaOGG'],null,1);
                                $totalMembrosJovem = $membros->total($vetor['idConvocacaoRitualisticaOGG'],null,2);
                                
                                $nao_membros = new convocacaoRitualisticaOGGNaoMembro();
                                $totalNaoMembrosAdulto = $nao_membros->total($vetor['idConvocacaoRitualisticaOGG'],null,1);
                                $totalNaoMembrosJovem = $nao_membros->total($vetor['idConvocacaoRitualisticaOGG'],null,2);
                                
                                $totalJovem = $totalMembrosJovem+$totalNaoMembrosJovem;
                                $totalAdulto = $totalMembrosAdulto+$totalNaoMembrosAdulto;
                                
                                $total = $totalJovem+$totalAdulto;
                                
                        ?>
                            <tr>
                                    <td><?php echo retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);?></td>
                                    <td><center><?php echo $vetor['nomeComendador'];?></center></td>
                                    <td><center><?php echo $dataConvocacao;?></center></td>
                                    <td><center><?php echo $totalJovem;?></center></td>
                                    <td><center><?php echo $totalAdulto;?></center></td>
                                    <td><center><?php echo $total;?></center></td>
                                    <td>
                                                <center>
                                                    <?php if (in_array("2", $arrNivelUsuario)) { ?>
                                                        <a href="?corpo=alteraConvocacoesRitualisticas&id=<?php echo $vetor['idConvocacaoRitualisticaOGG']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar">
                                                            <i class="fa fa-edit fa-white"></i>&nbsp;
                                                            Editar
                                                        </a>
                                                    <?php } ?>
                                                    <?php if (in_array("5", $arrNivelUsuario)) { ?>
                                                    <button type="button" class="btn btn-danger" onclick="location.href='painelDeControle.php?corpo=buscaConvocacoesRitualisticas&id=<?php echo $vetor['idConvocacaoRitualisticaOGG'];?>&excluirConvocacaoRitualistica=1';">
                                                        <i class="fa fa-times fa-white"></i>&nbsp;
                                                        Excluir</button>
                                                    <?php } ?>
                                                </center>
                                    </td>
                            </tr>
                        <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->


