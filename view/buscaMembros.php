<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>

<!-- Conteúdo DE INCLUDE INÍCIO -->
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Consulta de Membros</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaMembros">Consulta</a>
            </li>
            <li class="active">
                <strong><a>Membros</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!--
<div class="alert alert-warning">
                                Para fazer a pesquisa digite o <a class="alert-link" href="#"> código de afiliação e o nome do membro</a>.
                    </div>-->
<!-- Caminho de Migalhas Fim -->
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Consulta de Membros</h5>
                    <div class="ibox-tools">
                        <!--
                        <a class="btn btn-xs btn-primary" href="?opcao=oficial_form.php">
                            <i class="fa fa-plus"></i> Novo Membro
                        </a>
                        -->
                    </div>
                </div>
                <div class="ibox-content">
                    <table style="border-spacing: 10px;border-collapse: separate;">
                        <tr>
                            <td>
                                Tipo:
                            </td>
                            <td>
                                <select id="tipo" name="tipo" class="form-control col-sm-3">
                                    <option value="1">R+C ou TOM</option>
                                    <option value="2">OGG</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Código de Afiliação:
                            </td>
                            <td>
                                <input type="text" name="codigoAfiliacao" id="codigoAfiliacao" onkeypress="return SomenteNumero(event)">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                ou Nome:
                            </td>
                            <td>
                                <input type="text" name="nome" id="nome">
                            </td>
                        </tr>
                        <!--
                        <tr>
                            <td>
                                <script src='https://www.google.com/recaptcha/api.js?hl=pt-BR'></script>
                            </td>
                            <td>
                                <div class="g-recaptcha" data-sitekey="6LeythgUAAAAABw97Wfwu7sjKNpbGp-DpOaA5zx2"></div>
                            </td>
                        </tr>
                        -->
                        <tr>
                            <td></td>
                            <td>
                                <a href="#" class="btn btn-outline btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codigoAfiliacao','nome','codigoAfiliacao','nome','','','myModalPesquisa','informacoesPesquisa','S','consultaMembro');">Pesquisar</a>
                                &nbsp;
                                <a href="#" class="btn btn-outline btn-primary" onclick="location.href='?corpo=buscaMembros'">Atualizar página</a>

                                <input type="hidden" id="abrirPesquisa" name="abrirPesquisa" value="1">
                            </td>
                        </tr>
                    </table>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->


<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" onclick="fechaModalConsultaMembros()"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Consulta de Membros</h4>
            </div>
            <div class="modal-body">
                <table cellpadding="10">
                    <tbody>
                        <tr>
                            <td width="35%">Nome:</td>
                            <td><span id="m_nome"></span></td>
                        </tr>
                        <tr>
                            <td>Cód. Afiliação:</td>
                            <td><span id="m_codigoAfiliacao"></span></td>
                        </tr>
                        <!--
                        <tr>
                            <td>Endereço:</td><td><span id="logradouro"></span></td>
                        </tr>
                        <tr>
                            <td>Número:</td><td><span id="numero"></span></td>
                        </tr>
                        -->
                        <tr>
                            <td>Cidade:</td>
                            <td><span id="cidade"></span></td>
                        </tr>
                        <!--
                        <tr>
                            <td>CEP:</td><td><span id="cep"></span></td>
                        </tr>
                        -->
                        <tr>
                            <td>Data de Nascimento:</td>
                            <td><span id="dataNascimento"></span></td>
                        </tr>
                        <!--<tr>
                            <td>Profissão:</td>
                            <td><span id="profissao"></span></td>
                        </tr>
                        <tr>
                            <td>Ocupação:</td>
                            <td><span id="ocupacao"></span></td>
                        </tr>

                        <tr>
                            <td>RG:</td><td><span id="rg"></span></td>
                        </tr>
                        <tr>
                            <td>CPF:</td><td><span id="cpf"></span></td>
                        </tr>
                        <tr>
                            <td>Telefone:</td><td><span id="telefone"></span></td>
                        </tr>
                        <tr>
                            <td>E-mail:</td><td><span id="email"></span></td>
                        </tr>-->

                        <tr>
                            <td>Admissão:</td>
                            <td> <span id="admissao"></span></td>
                        </tr>
                        <tr>
                            <td>Afiliação R+C:</td>
                            <td> <span id="tipoAfiliacao"></span> <span id="dual"></span></td>
                        </tr>
                        <tr>
                            <td>Afiliação TOM:</td>
                            <td> <span id="tom"></span> <span id="tipoAfiliacaoTom"></span> <span id="dualTom"></span></td>
                        </tr>
                        <tr>
                            <td>Afiliado ao Organismo:</td>
                            <td> <span id="organismoAfiliado"></span></td>
                        </tr>
                        <tr id="tr_revistas" style="display:none">
                            <td>Revistas Impressas:</td>
                            <td> <span id="revistas"></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td> <br><span id="botaoDual"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>R+C<i class=""></i></center></a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <div class="mail-text h-200">
                                            <div class="alert alert-info">
                                                O <a class="alert-link" href="#">tempo de admissão</a> é relevante para liberar iniciação apenas quando trata-se do <a class="alert-link" href="#">companheiro</a> nos casos duais.
                                            </div>
                                            <table cellpading="5">
                                                <tr>
                                                    <td width="33%">Sit. Cadastral:</td>
                                                    <td> <span id="situacaoCadastralRC"></span></td>
                                                </tr>
                                                <tr id="tr_motivoDesligamentoRC" style="display:none">
                                                    <td>Motivo:</td>
                                                    <td> <span id="motivoDesligamentoRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Sit. Remessa:</td>
                                                    <td> <span id="situacaoRemessaRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Data da Quitação:</td>
                                                    <td> <span id="dataQuitacaoRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Grau:</td>
                                                    <td> <span id="grauRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Lote:</td>
                                                    <td> <span id="loteRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Tempo de Admissão (<a href="#" onclick="tabelaTempoCasa();">ver tabela</a>):</td>
                                                    <td> <span id="tempoCasaRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Iniciações disponíveis:</td>
                                                    <td><span id="iniciacoesPendentesRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Concluiu e reiniciou os ensinamentos:</td>
                                                    <td> <span id="reiniciouRC"></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><center>TOM</center></a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table cellpading="5">
                                            <tr>
                                                <td>Sit. Cadastral:</td>
                                                <td> <span id="situacaoCadastralTOM"></span></td>
                                            </tr>
                                            <tr id="tr_motivoDesligamentoTom" style="display:none">
                                                    <td>Motivo:</td>
                                                    <td> <span id="motivoDesligamentoTom"></span></td>
                                                </tr>
                                            <tr>
                                                <td>Sit. Remessa:</td>
                                                <td> <span id="situacaoRemessaTOM"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Data da Quitação:</td>
                                                <td> <span id="dataQuitacaoTOM"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Lote:</td>
                                                <td> <span id="loteTOM"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Concluiu e reiniciou os ensinamentos:</td>
                                                <td> <span id="reiniciouTOM"></span></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><center>OGG</center></a>
                                </h5>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table cellpading="5">
                                            <tr>
                                                <td>Sit. Cadastral:</td>
                                                <td> <span id="situacaoCadastralOGG"></span></td>
                                            </tr>
                                            <tr id="tr_motivoDesligamentoOjp" style="display:none">
                                                    <td>Motivo:</td>
                                                    <td> <span id="motivoDesligamentoOjp"></span></td>
                                                </tr>
                                            <tr>
                                            <tr>
                                                <td>Sit. Remessa:</td>
                                                <td> <span id="situacaoRemessaOGG"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Data da Quitação:</td>
                                                <td> <span id="dataQuitacaoOGG"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Lote:</td>
                                                <td> <span id="loteOGG"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Grau:</td>
                                                <td> <span id="grauOGG"></span></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><center>Iniciações R+C</center></a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">

                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="iniciacoes"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><center>Iniciações TOM</center></a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">

                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="iniciacoesTOM"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen"><center>Iniciações OGG</center></a>
                                </h5>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">

                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="iniciacoesOGG"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><center>Cargos Atuantes</center></a>
                                </h5>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="cargos"></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><center>Cargos Exercídos</center></a>
                                </h5>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="cargosNaoAtuantes"></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="fechaModalConsultaMembros()">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>