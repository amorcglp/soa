<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Imóveis</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Tipos de Anexo</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tipos de Anexo de Imóveis Cadastrados</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroImovelAnexoTipo">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Anexo</th>
                                <th><center>Anualmente</center></th>
                        		<th><center>Status</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/imovelAnexoTipoController.php");
                            include_once("controller/usuarioController.php");

                            $iatc = new imovelAnexoTipoController();
                            $um     = new Usuario();

                            $resultado = $iatc->listaImovelAnexoTipo();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
							<tr>
								<td>
									<?php echo $vetor['nomeImovelAnexoTipo'];?>
								</td>
								<td>
                                    <?php
                                    switch($vetor['anualImovelAnexoTipo']){
                                        case 1: echo "Sim"; break;
                                        case 0: echo "Não"; break;
                                    }
                                    ?>
                                </td>
                                <td>
	                                <center>
	                                    <div id="statusTarget<?php echo $vetor['idImovelAnexoTipo'] ?>">
	                                        <?php
	                                        switch ($vetor['statusImovelAnexoTipo']) {
	                                            case 0:
	                                                echo "<span class='badge badge-primary'>Ativo</span>";
	                                                break;
	                                            case 1:
	                                                echo "<span class='badge badge-danger'>Inativo</span>";
	                                                break;
	                                        }
	                                        ?>
	                                    </div>
	                                </center>
                                </td>
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idImovelAnexoTipo']; ?>">
                                            <i class="fa fa-search-plus fa-white"></i>  
                                            Detalhes                                            
                                        </a>
                                        <a class="btn btn-sm btn-info" href="?corpo=alteraImovelAnexoTipo&idImovelAnexoTipo=<?php echo $vetor['idImovelAnexoTipo']; ?>" data-rel="tooltip" title="">
                                            <i class="fa fa-edit fa-white"></i>  
                                            Editar                                            
                                        </a>
                                        <span id="status<?php echo $vetor['idImovelAnexoTipo'] ?>">
        									<?php if ($vetor['statusImovelAnexoTipo'] == 1) { ?>
                                                <a class="btn btn-sm btn-success" onclick="mudaStatus('<?php echo $vetor['idImovelAnexoTipo'] ?>', '<?php echo $vetor['statusImovelAnexoTipo'] ?>', 'imovelAnexoTipo')" data-rel="tooltip" title="Ativar">
                                                    Ativar                                            
                                                </a>
        									<?php } else { ?>
                                                <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idImovelAnexoTipo'] ?>', '<?php echo $vetor['statusImovelAnexoTipo'] ?>', 'imovelAnexoTipo')" data-rel="tooltip" title="Inativar">
                                                    
                                                    Inativar                                            
                                                </a>
        									<?php } ?>
                                        </span>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->
<?php

$resultadoInfo = $iatc->listaImovelAnexoTipo();

if ($resultadoInfo) {
	foreach ($resultadoInfo as $vetorInfo) {
        ?>
        <!-- Modal de detalhes do usuário -->
        <div class="modal inmodal" id="myModaInfo<?php echo $vetorInfo['idImovelAnexoTipo']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Detalhe do Tipo de Anexo</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Anexo: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php echo $vetorInfo['nomeImovelAnexoTipo']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Descrição: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php echo $vetorInfo['descricaoImovelAnexoTipo']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Cadastrado em: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            echo substr($vetorInfo['cadastroDataImovelAnexoTipo'],8,2)."/".
                                                substr($vetorInfo['cadastroDataImovelAnexoTipo'],5,2)."/".
                                                substr($vetorInfo['cadastroDataImovelAnexoTipo'],0,4)." às ".
                                                substr($vetorInfo['cadastroDataImovelAnexoTipo'],10,6);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Atualizado em: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            echo substr($vetorInfo['atualizadoDataImovelAnexoTipo'],8,2)."/".
                                                substr($vetorInfo['atualizadoDataImovelAnexoTipo'],5,2)."/".
                                                substr($vetorInfo['atualizadoDataImovelAnexoTipo'],0,4)." às ".
                                                substr($vetorInfo['atualizadoDataImovelAnexoTipo'],10,6);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Atualizado Por: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php

                                            $resultadoAtualizadoPor = $um->buscaUsuario($vetorInfo['fk_seqCadastAtualizadoPor']);
                                            if ($resultadoAtualizadoPor) {
                                                foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                                    echo $vetorAtualizadoPor['nomeUsuario'];
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Status: </label>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <?php
                                            switch ($vetorInfo['statusImovelAnexoTipo']) {
                                                case 0:
                                                    echo "<span class='badge badge-primary'>Ativo</span>";
                                                    break;
                                                case 1:
                                                    echo "<span class='badge badge-danger'>Inativo</span>";
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
				