<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>


<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividade</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Atividade Para Organismos</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Atividades Para Organismos Cadastrados</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeTipo">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Atividade</th>
                                <th><center>Destinando ao Organismo(a)/Frequência Mensal</center></th>
                        		<th><center>Status</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/atividadeTipoController.php");

                            $atc = new atividadeTipoController();
                            $resultado = $atc->listaAtividadeTipo();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
							<tr>
								<td>
									<?php echo $vetor['nomeAtividadeTipo'];?>
								</td>
								<td>
	                                <center>
	                                	<?php if($vetor['lojaAtividadeTipo'] == 0) {?>
	                                		<?php echo "Loja - Quantidade Recomendada: " . $vetor['qntLojaAtividadeTipo']; ?>
	                                		<?php echo "<br>"; ?>
	                                	<?php }?>
	                                	<?php if($vetor['pronaosAtividadeTipo'] == 0) {?>
	                                		<?php echo "Pronaos - Quantidade Recomendada: " . $vetor['qntPronaosAtividadeTipo']; ?>
	                                		<?php echo "<br>"; ?>
	                                	<?php }?>
	                                	<?php if($vetor['capituloAtividadeTipo'] == 0) {?>
	                                		<?php echo "Capítulo - Quantidade Recomendada: " . $vetor['qntCapituloAtividadeTipo']; ?>
	                                	<?php }?>
	                                </center>
                                </td>
                                <td>
	                                <center>
	                                    <div id="statusTarget<?php echo $vetor['statusAtividadeTipo'] ?>">
	                                        <?php
	                                        switch ($vetor['statusAtividadeTipo']) {
	                                            case 0:
	                                                echo "<span class='badge badge-primary'>Ativo</span>";
	                                                break;
	                                            case 1:
	                                                echo "<span class='badge badge-danger'>Inativo</span>";
	                                                break;
	                                        }
	                                        ?>
	                                    </div>
	                                </center>
                                </td>
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idAtividadeTipo']; ?>">
                                            <i class="fa fa-search-plus fa-white"></i>  
                                            Detalhes                                            
                                        </a>
                                        <a class="btn btn-sm btn-info" href="?corpo=alteraAtividadeTipo&idAtividadeTipo=<?php echo $vetor['idAtividadeTipo']; ?>" data-rel="tooltip" title="">
                                            <i class="fa fa-edit fa-white"></i>  
                                            Editar                                            
                                        </a>
                                        <span id="status<?php echo $vetor['idAtividadeTipo'] ?>">
        									<?php if ($vetor['statusAtividadeTipo'] == 1) { ?>
                                                <a class="btn btn-sm btn-success" onclick="mudaStatus('<?php echo $vetor['idAtividadeTipo'] ?>', '<?php echo $vetor['statusAtividadeTipo'] ?>', 'atividadeTipo')" data-rel="tooltip" title="Ativar"> 
                                                    Ativar                                            
                                                </a>
        									<?php } else { ?>
                                                <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idAtividadeTipo'] ?>', '<?php echo $vetor['statusAtividadeTipo'] ?>', 'atividadeTipo')" data-rel="tooltip" title="Inativar">
                                                    
                                                    Inativar                                            
                                                </a>
        									<?php } ?>
                                        </span>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->
<?php
include_once("controller/atividadeTipoController.php");

$atc = new atividadeTipoController();
$resultado = $atc->listaAtividadeTipo();

if ($resultado) {
	foreach ($resultado as $vetor) {
        ?>
        <!-- Modal de detalhes do usuário -->
        <div class="modal inmodal" id="myModaInfo<?php echo $vetor['idAtividadeTipo']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Detalhe da Atividade</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">					
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Atividade: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $vetor['nomeAtividadeTipo']; ?></p></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Descrição: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $vetor['descricaoAtividadeTipo']; ?></p></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Atividade Para os Organismos: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-8" style="max-width: 320px">
	                                    <p class="form-control-static">
	                                    	<?php if($vetor['lojaAtividadeTipo'] == 0) {?>
		                                		<?php echo "Loja - Quantidade Recomendada: " . $vetor['qntLojaAtividadeTipo']; ?>
		                                		<?php echo "<br>"; ?>
		                                	<?php }?>
		                                	<?php if($vetor['pronaosAtividadeTipo'] == 0) {?>
		                                		<?php echo "Pronaos - Quantidade Recomendada: " . $vetor['qntPronaosAtividadeTipo']; ?>
		                                		<?php echo "<br>"; ?>
		                                	<?php }?>
		                                	<?php if($vetor['capituloAtividadeTipo'] == 0) {?>
		                                		<?php echo "Capítulo - Quantidade Recomendada: " . $vetor['qntCapituloAtividadeTipo']; ?>
		                                	<?php }?>
	                                    </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Status</label>
                                <div class="col-sm-8">
                                    <div class="col-sm-8" style="max-width: 320px">
                                        <p class="form-control-static">
                                            <?php
                                            switch ($vetor['statusAtividadeTipo']) {
                                                case 0:
                                                    echo "<span class='badge badge-primary'>Ativo</span>";
                                                    break;
                                                case 1:
                                                    echo "<span class='badge badge-danger'>Inativo</span>";
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>
<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

        /* Init DataTables */
        var oTable = $('#editable').dataTable();

        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });


    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row"]);

    }
</script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
				