<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once 'model/organismoClass.php';
$o = new organismo();
$resultado = $o->buscaIdOrganismo($idOrganismoAfiliado);
$email="";
$senhaEmail="";
if($resultado)
{
	foreach($resultado as $vetor)
	{
		$email = $vetor['emailOrganismoAfiliado'];
		$senhaEmail = $vetor['senhaEmailOrganismoAfiliado'];
	}
}
?>
<script src='js/pubhtml5-light-box-api-min.js'></script>
<body>
    <h1>Senha do Webmail AMORC do Organismo </h1>
    <input type="hidden" name="arquivo" id="arquivo" value="">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    	Informações
                        <!--  <h5>Bibliotecas de Vídeos-Aulas</h5>-->
                    </div>
                    <div class="ibox-content">
                    	<div class="alert alert-warning">
	                    	<b>E-mail:</b> <?php if($email!=""){ echo $email;}else{ echo "Não encontrado";}?><br>
	                    	<b>Senha:</b> <?php if($senhaEmail!=""){echo $senhaEmail;}else{ echo "Não encontrada";}?><br>
                    	</div>
                    	<!--  
                        <table class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th><center>Título</center></th>
                            <th><center>Descrição</center></th>
                            <th><center>Ações</center></th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Aula #1 - Cadastro e Acesso ao Sistema</td>
                                    <td>Nessa vídeo-aula nós ensinaremos como se cadastrar e acessar o SOA.</td>
                                    <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/g4A4sxzc41M?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/etic/' data-width='900' data-height='500' data-title='Aula #1 - Cadastro e Acesso ao Sistema'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #2 - Explicando a Interface</td>
                                <td>Nessa vídeo-aula nós ensinaremos como identificar os ítens de menu dentro do SOA, <br>como procurar e acessar as funcionalidades.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/c9Mm68riGik?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/etic/' data-width='900' data-height='500' data-title='Aula #2 - Explicando a Interface'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>
                            <tr>
                                <td>Aula #2.1 - Mobilidade</td>
                                <td>Nessa vídeo-aula é apresentado como acessar o SOA por qualquer dispositivo móvel.</td>
                                <td>
                            <center>
                                 <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/Fiic3RIJJwo?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/etic/' data-width='900' data-height='500' data-title='Aula #2.1 - Mobilidade'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #3 - Perfil</td>
                                <td>Nessa vídeo-aula ensinaremos como acessar seu perfil, como trocar sua foto de perfil dentro do SOA,<br> e visualizar os contatos de outros Oficiais.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/50xPF_pNuJo?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/etic/' data-width='900' data-height='500' data-title='Aula #3 - Perfil'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>
                            <tr>
                                <td>Aula #4 - Chat</td>
                                <td>Nessa vídeo-aula ensinaremos como conversar com outros Oficiais dentro do SOA.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/lDFzMGOJu_o?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/etic/' data-width='900' data-height='500' data-title='Aula #4 - Chat'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>
                            <tr>
                                <td>Aula #5 - Biblioteca</td>
                                <td>Nessa vídeo-aula ensinaremos como visualizar os manuais administrativos da Ordem e como imprimí-los.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/yyAsEGMBFRY?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/etic/' data-width='900' data-height='500' data-title='Aula #5 - Biblioteca'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #6 - Anotações</td>
                                <td>Nessa vídeo-aula ensinaremos como criar, editar e excluir suas anotações.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/Mi4N1mwumUA?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #6 - Anotações'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #7 - Imóveis</td>
                                <td>Nessa vídeo-aula ensinaremos como criar, editar e excluir os imóveis de seu O.A</td>
                                <td>
                            <center>
                               <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/1T8ughO_AdU?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #7 - Imóveis'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #7.1 - Controle de Documentos dos Imóveis</td>
                                <td>Nessa vídeo-aula ensinaremos como enviar o Relatório Anual de Imóveis.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/xvHRmlMStWg?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #7.1 - Controle de Documentos dos Imóveis'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #8 - Movimento Financeiro</td>
                                <td>Nessa vídeo-aula ensinaremos como inserir o Saldo Inicial, Recebimentos e Despesas</td>
                                <td>
                            <center>
                                 <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/77mWfYETFC8?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #8 - Movimento Financeiro'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #8.1 - Movimento Financeiro</td>
                                <td>Nessa vídeo-aula ensinaremos como inserir os Fundos Vinculados, Membros Ativos do O.A e Dívidas</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/jpKD57dWCeE?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #8.1 - Movimento Financeiro'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #8.2 - Movimento Financeiro</td>
                                <td>Nessa vídeo-aula ensinaremos como inserir a Mensalidade do O.A, Relatório Mensal e Anual</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/b0hI1K6Tgy4?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #8.2 - Movimento Financeiro'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #9 - Livros</td>
                                <td>Nessa vídeo-aula ensinaremos como cadastrar os Livros do Organismo e Região</td>
                                <td>
                            <center>
                                 <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/FeYEhkZtSOo?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #9 - Livros'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #10 - Discursos</td>
                                <td>Nessa vídeo-aula ensinaremos como visualizar e imprimir os discursos pelo SOA.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/gdjwF6X9gmM?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #10 - Discursos'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            <tr>
                                <td>Aula #11 - Plano de Ação</td>
                                <td>Nessa vídeo-aula ensinaremos como criar um Plano de Ação da Região e do Organismo.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/6Rj8htfFhkI?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #11 - Plano de Ação'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>
                            
                            
                            <tr>
                                <td>Aula #12 - Ata de Reunião Mensal</td>
                                <td>Nessa vídeo-aula ensinaremos como criar uma Ata de Reunião Mensal.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/ZeuJA5Z5zBA?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #12 - Ata de Reunião Mensal'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>
                            
                             <tr>
                                <td>Aula #13 - Ata de Posse</td>
                                <td>Nessa vídeo-aula ensinaremos como criar uma Ata de Posse.</td>
                                <td>
                            <center>
                                <a data-rel='fh5-light-box-demo' data-href='https://www.youtube.com/embed/uZuTu8Y0TKM?rel=0&showinfo=0' data-rel='fh5-light-box-demo' data-width='900' data-height='500' data-title='Aula #13 - Ata de Posse'>
                                    <button type="button" class="btn btn-sm btn-success">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>

                            </tbody>
                        </table>
                        -->
                    </div>
                </div>
            </div>
        </div>
    </div>

