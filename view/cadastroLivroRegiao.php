<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Materiais da Região para GLP</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaLivroRegiao">Livros</a>
            </li>
            <li class="active">
                <strong><a>Materiais da Região para GLP</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de criação dos Materiais da Região para GLP</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaLivroRegiao">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaLivroRegiao()" >
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Regiao:</label>
                            <div class="col-sm-9">
                                <select name="regiao" id="regiao" data-placeholder="Selecione uma regiao..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/regiaoRosacruzController.php';
                                    $rrc = new regiaoRosacruzController();
                                    $rrc->criarComboBox(0,substr($sessao->getValue("siglaOA"),0,3));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Título: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="titulo" name="titulo" type="text" value="" style="max-width: 450px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Assunto: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="assunto" name="assunto" type="text" value="" style="max-width: 450px">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Escreva a notícia aqui<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="descricao" name="descricao"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Palavras-chave (separe-as com vírgula): </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="palavraChave" name="palavraChave" type="text" value="" style="max-width: 450px">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                        	<input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                                <input type="hidden" name="nomeUsuarioCapitulo" id="nomeUsuarioCapitulo" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar a Ata" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaLivroRegiao" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	