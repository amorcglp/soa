<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once('model/relatorioFinanceiroMensalClass.php');
include_once('model/usuarioClass.php');
include_once('model/notificacaoGlpClass.php');
include_once('model/saldoInicialClass.php');

$rfm 		= new RelatorioFinanceiroMensal();
$usu		= new Usuario();
$u 			= new NotificacaoGlp();

$si = new saldoInicial();
$temSaldoInicial = 1;
$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if(!$resultado)
{
    $temSaldoInicial = 0;
}

//$mesAtual = isset($_REQUEST['mesAtual'])?$_REQUEST['mesAtual']:date('m');
//$anoAtual = isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');


if(isset($_REQUEST['mesAtual']))
{
	$mesAtual = $_REQUEST['mesAtual'];
}else{

		if(isset($_SESSION['mesAtualRelatorioFinanceiroMensal']))
		{
			$mesAtual = $_SESSION['mesAtualRelatorioFinanceiroMensal'];
		}else{
			$mesAtual = date('m');
			$_SESSION['mesAtualRelatorioFinanceiroMensal'] = $mesAtual;
		}

}
//echo $mesAtual;
$_SESSION['mesAtualRelatorioFinanceiroMensal'] = $mesAtual;

if(isset($_REQUEST['anoAtual']))
{
	$anoAtual = $_REQUEST['anoAtual'];
}else{

	if(isset($_SESSION['anoAtualRelatorioFinanceiroMensal']))
	{
		$anoAtual = $_SESSION['anoAtualRelatorioFinanceiroMensal'];
	}else{
		$anoAtual = date('Y');
		$_SESSION['anoAtualRelatorioFinanceiroMensal'] = $anoAtual;
	}
}
//echo "anoatual=>".$anoAtual;
$_SESSION['anoAtualRelatorioFinanceiroMensal'] = $anoAtual;


//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;
$usuario 				= isset($_REQUEST['usuario'])?$_REQUEST['usuario']:null;
$anexo 					= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;

//Funções do usuário
$arrFuncoesUsuario = explode(",",$_SESSION['funcoes']);
//echo "<pre>";print_r($arrFuncoesUsuario);
/**
 * Upload do Relatório Assinado
 */
$relatorio=0;
$extensaoNaoValida=0;
if($anexo != "")
{
	//Verificar se já está cadastrado
	$rfm->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
	$rfm->setMes($mesAtual);
	$rfm->setAno($anoAtual);
	$rfm->setUsuario($usuario);

	$idRelatorioFinanceiro = $rfm->cadastraRelatorioFinanceiroMensal();

	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
		$extensao = strstr($_FILES['anexo']['name'], '.');
                if($extensao==".pdf"||
                        $extensao==".png"||
                        $extensao==".jpg"||
                        $extensao==".jpeg"||
                        $extensao==".PDF"||
                        $extensao==".PNG"||
                        $extensao==".JPG"||
                        $extensao==".JPEG"
                        )
                {
                    $caminho = "img/relatorio_financeiro_mensal/" . basename($idRelatorioFinanceiro.".relatorio.financeiro.mensal.".$mesAtual.".".$anoAtual);
                    //Guardar Link do Caminho no Banco de Dados
                    $rfm->setCaminhoRelatorioFinanceiroMensal($caminho);
                    if($rfm->atualizaCaminhoRelatorioFinanceiroMensalAssinado($idRelatorioFinanceiro))
                    {
                        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/relatorio_financeiro_mensal/';
                //$uploaddir = 'C:\/wamp\/www\/soa_mvc\/img\/relatorio_financeiro_mensal\/';
                            $uploadfile = $uploaddir . basename($idRelatorioFinanceiro.".relatorio.financeiro.mensal.".$mesAtual.".".$anoAtual);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            {
                                    $relatorio = 1;
                                    /**
                                     * Notificar GLP que o relatório assinado foi inserido
                                     */
                                    date_default_timezone_set('America/Sao_Paulo');
                                    $tituloNotificacao="Novo Relatorio Financeiro Mensal Entregue";
                                    $tipoNotificacao=6;//Entregue
                                    $remetenteNotificacao=$sessao->getValue("seqCadast");
                                    $mensagemNotificacao="Relatorio Financeiro Mensal Entregue pelo Organismo ".$_POST['nomeOrganismo'].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuario'];

                                    //Selecionar todos os usuários com departamento 2 e 3
                                    $resultadoUsuarios = $usu->listaUsuario(null,"2,3");
                                    $arrOficiais=array();
                                    if($resultadoUsuarios)
                                    {
                                            foreach ($resultadoUsuarios as $vetor)
                                            {
                                                    $arrOficiais[] = $vetor['idUsuario'];
                                            }
                                    }

                                    $resultado = $u->cadastroNotificacao(utf8_encode($tituloNotificacao),utf8_encode($mensagemNotificacao),$tipoNotificacao,$remetenteNotificacao);

                                    if ($resultado==true) {
                                            for ($i=0; $i<count($arrOficiais); $i++){
                                                    $ultimoId = $u->selecionaUltimoId();
                                                    $resultadoOficiais = $u->cadastroNotificacaoGlp($ultimoId,$arrOficiais[$i]);
                                            }
                                    }
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o relatório assinado!');</script>";
                            }
                    }
                }else{
                    $extensaoNaoValida=1;
                }
            }else{
                $extensaoNaoValida=2;
            }
	}
}

if($relatorio==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Relatório Assinado Enviado com Sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($extensaoNaoValida==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
/*
 * Verificar se já foi feito o upload do relatório assinado
 */
$caminhoRelatorioFinanceiroMensal="";
if($rfm->verificaSeJaCadastrado($mesAtual,$anoAtual,$idOrganismoAfiliado))
{
	$resultado = $rfm->listaRelatorioFinanceiroMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
	if($resultado)
	{
		foreach($resultado as $vetor)
		{
			$caminhoRelatorioFinanceiroMensal = $vetor['caminhoRelatorioFinanceiroMensal'];
		}
	}
}
?>
<?php
include_once('model/recebimentoClass.php');
include_once('model/despesaClass.php');
include_once("model/rendimentoClass.php");
include_once('model/membrosRosacruzesAtivosClass.php');
include_once("lib/functions.php");


/****
 * Montar navegação por meses
 *****/

/*
 * Montar anterior e próximo
 */

$mesAnterior=date('m', strtotime('-1 months', strtotime($anoAtual."-".$mesAtual."-01")));
$mesProximo=date('m', strtotime('+1 months', strtotime($anoAtual."-".$mesAtual."-01")));

if($mesAtual==1)
{
	$anoAnterior=$anoAtual-1;
}else{
	$anoAnterior=$anoAtual;
}
if($mesAtual==12)
{
	$anoProximo=$anoAtual+1;
}else{
	$anoProximo=$anoAtual;
}

/*
 * Montar texto para ser exibido
 */
$mesAtualTexto = mesExtensoPortugues($mesAtual);
$mesProximoTexto = mesExtensoPortugues($mesProximo);
$mesAnteriorTexto = mesExtensoPortugues($mesAnterior);


$r = new Recebimento();
$d = new Despesa();
$rendimento = new Rendimento();
$membrosRosacruzesAtivos = new MembrosRosacruzesAtivos();

/*
 * Calcular saldo do mês anterior
 */
/*
//Encontrar Saldo e Data Inicial
include_once('model/saldoInicialClass.php');
$si = new saldoInicial();
$mesSaldoInicial = "01";
$anoSaldoInicial = "2016";
$saldoInicial=0;
$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if($resultado)
{
	foreach($resultado as $vetor)
	{
		$mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
		$anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
		$saldoInicial = (float) str_replace(",",".",str_replace(".","",$vetor['saldoInicial']));
	}
}

//Encontrar Saldo dos Meses Anteriores
$mes=intval($mesSaldoInicial);
$ano=intval($anoSaldoInicial);
$saldoMesesAnteriores=0;
$saldoGeral=0;
//echo "<br>mesSaldoInicial=>".$mes;
//echo "<br>anoSaldoInicial=>".$ano;

$y=0;
if($mesAtual==$mesSaldoInicial&&$anoAtual==$anoSaldoInicial)
{
		$saldoGeral = $saldoInicial;
		$saldoMesesAnteriores = $saldoGeral;
}else{
	$i=0;
	while($ano<=$anoAtual)
	{
			//echo "<br>data=>".$mes."-".$ano;
			if($mes<=12)
			{
				if($mes==$mesSaldoInicial&&$ano==$anoSaldoInicial)
				{
					$saldoMesesAnteriores = $saldoInicial;
				}
				//echo "i:".$i."-mes".$mes."<br>";
				//echo "saldoDoMesAnteriorParaSerCalculado:".$saldoMesesAnteriores."<br>";
				$saldoGeral = retornaSaldo($r,$d,$mes,$ano,$idOrganismoAfiliado,$saldoMesesAnteriores);
				$saldoMesesAnteriores = $saldoGeral;
				//echo "saldoMesesAnteriores".$saldoMesesAnteriores."<br>";
				$mes++;	
			}else{
				$ano++;
                                $mes=1;
			}
                        
                        if($ano==$anoAtual&&$mes>=$mesAtual)
                        {
                            break;
                        }
			
			//echo "<br>saldoGeral=>".$saldoGeral;
		$i++;
	}
}
*/
$saldoMesesAnteriores=0;//Iniciar saldo Meses anteriores como 0 zero nessa pagina para impressão tbm
/*
 * Seleção dos rosacruzes ativos no OA
 */
$arrRosacruzesAtivosOa = $membrosRosacruzesAtivos->retornaMembrosRosacruzesAtivos($mesAnterior,$anoAnterior,$idOrganismoAfiliado);

/*
 * Gráfico
 */
$num = cal_days_in_month(CAL_GREGORIAN, $mesAtual, $anoAtual);
$arrRecebimentos=array();
$dia=1;
while($dia<=$num){
	if($r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,null,$dia)!="")
	{
    	$arrRecebimentos['total'][$dia] = round($r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado,null,$dia));
	}else{
		$arrRecebimentos['total'][$dia] = 0;
	}   	
	$dia++;
}
$arrDespesas=array();
$dia=1;
while($dia<=$num){
	if($d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,null,$dia)!="")
	{
    	$arrDespesas['total'][$dia] = round($d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado,null,$dia));
	}else{
		$arrDespesas['total'][$dia] = 0;
	}   	
	$dia++;
}

include_once('model/financeiroMensalClass.php');
$f = new financeiroMensal();
$resultado7 = $f->listaFinanceiroMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
//echo "<pre>";print_r($resultado7);
$entregue=0;
$entregueCompletamente=0;
$dataEntrega="";
$dataEntregaCompletamente="";
$statusEntrega=1;

$quemEntregou = "";
$codigoAssinatura = "";

if($resultado7)
{
    $entregue=1;
    $statusEntrega=2;
    foreach ($resultado7 as $v)
    {
        $codigoAssinatura = $v['numeroAssinatura'];
        $quemEntregou = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
        $dataEntrega = substr($v['dataEntrega'],8,2)."/".substr($v['dataEntrega'],5,2)."/".substr($v['dataEntrega'],0,4)." às ".substr($v['dataEntrega'],11,8);
        //echo "dataEntrega:".$dataEntrega;
        if($v['entregueCompletamente']==1)
        {
            $entregueCompletamente=1;
            $statusEntrega=3;
            $dataEntregaCompletamente = substr($v['dataEntregouCompletamente'],8,2)."/".substr($v['dataEntregouCompletamente'],5,2)."/".substr($v['dataEntregouCompletamente'],0,4)." às ".substr($v['dataEntregouCompletamente'],11,8);
        }

    }
}

//Conforme status escolher a cor que o box aparecerá
switch ($statusEntrega)
{
    case 1:
        $class = "warning";
        $icon = "fa-location-arrow";
        $tituloBox = "Assinatura Eletrônica";
        break;
    case 2:
        $class = "default";
        $icon = "fa-pencil";
        $tituloBox = "O Documento não foi entregue completamente. Faltam assinaturas!";
        break;
    case 3:
        $class = "info";
        $icon = "fa-thumbs-up";
        $tituloBox = "O Documento foi 100% entregue";
        break;
}

//Ver se tem upload criado
include_once('model/relatorioFinanceiroMensalClass.php');
$rfm = new RelatorioFinanceiroMensal();
$resultado = $rfm->listaRelatorioFinanceiroMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
$upload = "não";
if($resultado) {
    if (count($resultado) > 0) {
        $upload = "sim";
    }
}

//Ver data mais antiga da criação desse relatório
$dataR = $r->dataCriacao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData[]=$dataR;
$dataD = $d->dataCriacao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData[]=$dataD;
$dataRendimento = $rendimento->dataCriacao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData[]=$dataRendimento;
$dataMembrosRosacruzesAtivos = $membrosRosacruzesAtivos->dataCriacao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData[]=$dataMembrosRosacruzesAtivos;
sort($arrData);
$dataCriacaoDocumento = substr($arrData[0],8,2)."/".substr($arrData[0],5,2)."/".substr($arrData[0],0,4);

//Ver data mais antiga da criação desse relatório
$dataR = $r->dataUltimaAlteracao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData2[]=$dataR;
$dataD = $d->dataUltimaAlteracao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData2[]=$dataD;
$dataRendimento = $rendimento->dataUltimaAlteracao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData2[]=$dataRendimento;
$dataMembrosRosacruzesAtivos = $membrosRosacruzesAtivos->dataUltimaAlteracao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrData2[]=$dataMembrosRosacruzesAtivos;
rsort($arrData2);
$dataUltimaAlteracaoDocumento = substr($arrData2[0],8,2)."/".substr($arrData2[0],5,2)."/".substr($arrData2[0],0,4);

//Responsáveis pela construção do documento
$arrResp = $r->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);
$arrRespD = $d->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);
$i=0;
if(count($arrRespD)) {
    foreach ($arrRespD['seq'] as $k => $v) {
        if (!in_array($v, $arrResp['seq'])) {
            $arrResp['seq'][$i] = $v;
            $arrResp['nome'][$i] = $arrRespD['nome'][$k];
            $i++;
        }
    }
}
$arrRespRendimento = $rendimento->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);
if (count($arrRespRendimento)) {
    foreach ($arrRespRendimento['seq'] as $k => $v) {
        if (!in_array($v, $arrResp['seq'])) {
            $arrResp['seq'][$i] = $v;
            $arrResp['nome'][$i] = $arrRespRendimento['nome'][$k];
            $i++;
        }
    }
}
$arrRespMembrosRosacruzesAtivos = $membrosRosacruzesAtivos->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);
if (count($arrRespMembrosRosacruzesAtivos)) {
    foreach ($arrRespMembrosRosacruzesAtivos['seq'] as $k => $v) {
        if (!in_array($v, $arrResp['seq'])) {
            $arrResp['seq'][$i] = $v;
            $arrResp['nome'][$i] = $arrRespMembrosRosacruzesAtivos['nome'][$k];
            $i++;
        }
    }
}

//Responsáveis pela alteracao do documento
$arrRespR = $d->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado,true);
if (count($arrRespR)) {
    foreach ($arrRespR['seq'] as $k => $v) {
        if (!in_array($v, $arrResp['seq'])) {
            $arrResp['seq'][$i] = $v;
            $arrResp['nome'][$i] = $arrRespR['nome'][$k];
            $i++;
        }
    }
}
$arrRespD = $d->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado,true);
$i=0;
if (count($arrRespD)) {
    foreach ($arrRespD['seq'] as $k => $v) {
        if (!in_array($v, $arrResp['seq'])) {
            $arrResp['seq'][$i] = $v;
            $arrResp['nome'][$i] = $arrRespD['nome'][$k];
            $i++;
        }
    }
}
$arrRespRendimento = $rendimento->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado,true);
if (count($arrRespRendimento)) {
    foreach ($arrRespRendimento['seq'] as $k => $v) {
        if (!in_array($v, $arrResp['seq'])) {
            $arrResp['seq'][$i] = $v;
            $arrResp['nome'][$i] = $arrRespRendimento['nome'][$k];
            $i++;
        }
    }
}
$arrRespMembrosRosacruzesAtivos = $membrosRosacruzesAtivos->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado,true);
if (count($arrRespMembrosRosacruzesAtivos)) {
    foreach ($arrRespMembrosRosacruzesAtivos['seq'] as $k => $v) {
        if (!in_array($v, $arrResp['seq'])) {
            $arrResp['seq'][$i] = $v;
            $arrResp['nome'][$i] = $arrRespMembrosRosacruzesAtivos['nome'][$k];
            $i++;
        }
    }
}

//Ordernar responsáveis por ordem do inicio de criação
/*
rsort ($arrResp['nome']);
rsort ($arrResp['seq']);
rsort ($arrResp['data']);
*/
$arrResp = unique_multidim_array_vals($arrResp);

if (count($arrResp))
    sort($arrResp['nome'], SORT_NATURAL | SORT_FLAG_CASE);


//echo "<pre>";print_r($arrResp['nome']);


//Faltam assinar
$f = new financeiroMensal();
$mestre = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
$secretario = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);
$tjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,375);
$vdc = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,533);

//Montar Faltam assinar
$arrOficiaisFaltamAssinar=array();
if(!$mestre)
{
    $arrOficiaisFaltamAssinar[]="MESTRE DO ORGANISMO AFILIADO";
}
if(!$secretario)
{
    $arrOficiaisFaltamAssinar[]="SECRETÁRIO DO ORGANISMO AFILIADO";
}

if(!$tjd&&($idClassificacaoOa==3||$idClassificacaoOa==1))
{
    $arrOficiaisFaltamAssinar[]="TESOUREIRO DA JUNTA DEPOSITÁRIA";
}
if(!$vdc)
{
    $arrOficiaisFaltamAssinar[]="VERIFICAÇÃO DE CONTAS";
}
//$oficiaisFaltamAssinar = implode(", ",$arr);
$arrOficiaisQueAssinaram = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado);
?>


<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Dias');
      data.addColumn('number', 'Recebimentos');
      data.addColumn('number', 'Despesas');

      data.addRows([<?php $dia=1; while($dia<=$num){ ?><?php if($dia==1){?>[<?php echo $dia;?>,<?php echo $arrRecebimentos['total'][$dia];?>,<?php echo $arrDespesas['total'][$dia];?>]<?php }else{?>,[<?php echo $dia;?>,<?php echo $arrRecebimentos['total'][$dia];?>,<?php echo $arrDespesas['total'][$dia];?>]<?php }$dia++; ?><?php }?>]);

      var options = {
        hAxis: {
          title: 'Tempo'
        },
        vAxis: {
          title: 'Entradas/Saidas'
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
  </script>


<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Relatório Financeiro Mensal</h2>
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a>
			</li>
			<li><a>Financeiro</a>
			</li>
			<li class="active"><strong>Relatório Financeiro Mensal</strong>
			</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Relatório Financeiro Mensal</h5>
					</div>
					<div class="ibox-content">

						<div class="row">
							<div class="col-lg-6">
								<div class="widget lazur-bg p-xl">
									<ul class="list-unstyled m-t-md">
										<h2>
											<i class="fa fa-exclamation-triangle"></i> Avisos
										</h2>
										<br>
										<?php 
											$totalRecebimentos = $r->retornaEntrada($mesAtual,$anoAtual,$idOrganismoAfiliado);
											if($totalRecebimentos>0)
											{
											?>
												<li><span class="fa fa-thumbs-up"></span> <label>RECEBIMENTOS</label> ok!</li>
											<?php 
											}else{
											?>
												<li><span class="fa fa-thumbs-down"></span> Não houve nenhuma entrada nos <label>RECEBIMENTOS</label> para este mês!</li>
											<?php 
											}
										?>
										<?php 
											$totalDespesas = $d->retornaSaida($mesAtual,$anoAtual,$idOrganismoAfiliado);
											if($totalDespesas>0)
											{
											?>
												<li><span class="fa fa-thumbs-up"></span> <label>DESPESAS</label> ok!</li>
											<?php 
											}else{
											?>
												<li><span class="fa fa-thumbs-down"></span> Não houve nenhuma entrada nas <label>DESPESAS</label> para este mês!</li>
											<?php 
											}
										?>
										<?php 
											$resultado = $rendimento->listaRendimento($mesAtual,$anoAtual,$idOrganismoAfiliado);
											if($resultado)
											{
												if(count($resultado)>0)
												{
												?>
													<li><span class="fa fa-thumbs-up"></span> <label>SALDO</label> ok!</li>
												<?php 
												}
											}else{
												?>
													<li><span class="fa fa-thumbs-down"></span> Não houve nenhuma entrada no <label>SALDO</label> para este mês!</li>
												<?php 
											}
										?>
										<?php 
											$resultado = $membrosRosacruzesAtivos->listaMembrosRosacruzesAtivos($mesAtual,$anoAtual,$idOrganismoAfiliado);
											if($resultado)
											{
												if(count($resultado)>0)
												{
												?>
													<li><span class="fa fa-thumbs-up"></span> <label>MEMBROS ATIVOS OA</label> ok!</li>
												<?php 
												}
											}else{
												?>
													<li><span class="fa fa-thumbs-down"></span> Não houve nenhuma entrada nos <label>MEMBROS ATIVOS OA</label> para este mês!</li>
												<?php 
											}
										?>
										<?php
										if($totalRecebimentos!=0)
										{ 
											if($totalDespesas>$totalRecebimentos){?>
											<li><span class="fa fa-thumbs-down"></span> Houve mais <label>DESPESAS</label> que <label>RECEBIMENTOS</label> este mês, verifique se não foi esquecida alguma entrada!</li>
										<?php }else{?>
											<li><span class="fa fa-thumbs-up"></span> Houve mais <label>RECEBIMENTOS</label> que <label>DESPESAS</label> este mês, ou seja, o saldo é positivo! Parabéns!</li>
										<?php }
										}
                                        ?>
									</ul>

								</div>
							</div>
							<div class="col-lg-6">
								<table width="100%" border="0">
									<tr>
										<td align="center">
                                                                                    <div class="form-group" id="data_4">
                                                                                            <table width="100">
                                                                                                <tr> 
                                                                                                    <td>
                                                                                                        <div class="input-group date">
                                                                                                            <span class="btn btn-w-m btn-primary" style="cursor: pointer; color:#FFF" ;>
                                                                                                                <i class="fa fa-calendar-o" style="color:#fff"></i>

                                                                                                                <input type="hidden" id="calendario" class="form-control" value="<?php echo $mesAtual;?>/<?php echo date('d');?>/<?php echo $anoAtual;?>" />
                                                                                                                <span style="color:#fff">
                                                                                                                    <?php echo $mesAtualTexto;?><?php echo $anoAtual;?>
                                                                                                                </span>
                                                                                                            </span>
                                                                                                            </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div> 
                                                                                    <input type="hidden" id="paginaAtual" class="form-control" value="<?php echo $corpo;?>">
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align="center">
                                            <table cellspacing="10" width="<?php if(!in_array("533",$arrFuncoesUsuario)&&!$usuarioApenasLeitura){?>400<?php }else{?>300<?php }?>">
                                                <td>
                                                    <td>
                                                        <a href="#"
                                                           class="btn btn-info  dim btn-large-dim btn-outline"
                                                           onClick="abrirPopupRelatorioFinanceiroMensalPreview('<?php echo $mesAtual;?>','<?php echo $anoAtual;?>','<?php echo $idOrganismoAfiliado;?>','<?php echo $saldoMesesAnteriores;?>','<?php echo $arrRosacruzesAtivosOa['numeroAtualMembrosAtivos']?>');"><i
                                                                    class="fa fa-search" value="Preview"></i> </a>
                                                    </td>
                                                    <td>
											<a href="#"
											class="btn btn-info  dim btn-large-dim btn-outline"
											onClick="abrirPopupRelatorioFinanceiroMensal('<?php echo $mesAtual;?>','<?php echo $anoAtual;?>','<?php echo $idOrganismoAfiliado;?>','<?php echo $saldoMesesAnteriores;?>','<?php echo $arrRosacruzesAtivosOa['numeroAtualMembrosAtivos']?>');"><i
											class="fa fa-print" value="Impressão"></i> </a>
                                                    </td>
                                                                                        <?php if($temSaldoInicial==1){?>
											&nbsp;       <td>
											<a href="#" target="_blank"
											class="btn btn-info  dim btn-large-dim btn-outline"
											onClick="listaUploadRelatorioFinanceiroMensal('<?php echo $mesAtual; ?>','<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>')"
											data-target="#mySeeListaUpload" data-toggle="modal"><i class="fa fa-paste"></i> </a>
                                                    </td>
                                                    <?php if(!in_array("533",$arrFuncoesUsuario)){?>
                                                    <td>
                                                        <div id="entregarFinanceiro">
                                                        <?php

                                                            if($statusEntrega==1&&!$usuarioApenasLeitura){
                                                        ?>
                                                            <a href="#" id="entregar"
                                                               class="btn btn-info dim btn-large-dim btn-outline"
                                                               onClick="entregarFinanceiroMensal('<?php echo $mesAtual; ?>','<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>','<?php echo $_SESSION['seqCadast'];?>')"><i class="fa fa-location-arrow"></i> </a>
                                                        <?php
                                                            }
                                                            $mestre = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
                                                            $secretario = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);

                                                            //$guardiao = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,369);

                                                            //$pjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,371);
                                                            //$sjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,373);
                                                            $tjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,375);
                                                            //$ma = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,377);
                                                            $vdc = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,533);

                                                            //Verificar se houve uma assinatura
                                                            $primeiraAssinatura=false;
                                                            if($mestre||$secretario||$tjd||$vdc){
                                                                $primeiraAssinatura=true;
                                                            }
                                                            //Montar Faltam assinar
                                                            $arr=array();
                                                            if(!$mestre)
                                                            {
                                                                $arr[]="MESTRE DO ORGANISMO AFILIADO";
                                                            }
                                                            if(!$secretario)
                                                            {
                                                                $arr[]="SECRETÁRIO DO ORGANISMO AFILIADO";
                                                            }

                                                            if(!$tjd&&($idClassificacaoOa==3||$idClassificacaoOa==1))
                                                            {
                                                                $arr[]="TESOUREIRO DA JUNTA DEPOSITÁRIA";
                                                            }

                                                            if(!$vdc)
                                                            {
                                                                $arr[]="VERIFICAÇÃO DE CONTAS";
                                                            }
                                                            /*
                                                            if(!$sjd)
                                                            {
                                                                $arr[]="SECRETÁRIO DA JUNTA DEPOSITÁRIA";
                                                            }
                                                            if(!$tjd)
                                                            {
                                                                $arr[]="TESOUREIRO DA JUNTA DEPOSITÁRIA";
                                                            }
                                                            if(!$ma)
                                                            {
                                                                $arr[]="MESTRE AUXILIAR DO ORGANISMO AFILIADO";
                                                            }
                                                            */
                                                            $oficiaisFaltamAssinar = implode(",",$arr);
                                                            if($statusEntrega==2)
                                                            {
                                                        ?>
                                                        
                                                            <a href="#" data-container="body" data-toggle="popover" data-placement="top" data-content="<?php if((($mestre&&$secretario&&$tjd&&$vdc)&&($idClassificacaoOa==3||$idClassificacaoOa==1))||(($mestre&&$secretario)&&($idClassificacaoOa==2))){?>Todos Assinaram. Processo de entrega finalizado!<?php }else{?>Faltam assinar: <?php echo $oficiaisFaltamAssinar;}?>" data-original-title="" title="" aria-describedby="popover408083" style="display: <?php if($entregue==0){?>none<?php }else{?>block<?php }?>;" id="faltaAssinar"
                                                               class="btn btn-info dim btn-large-dim btn-outline"
                                                               ><i class="fa fa-pencil"></i> </a>
                                                        <?php
                                                            }
                                                            if($statusEntrega==3)
                                                            {
                                                                ?>
                                                                <a href="#" data-container="body" data-toggle="popover" data-placement="top" data-content="Todos Assinaram. Processo de entrega finalizado!" data-original-title="" title="" aria-describedby="popover408083" style="display: <?php if($entregue==0){?>none<?php }else{?>block<?php }?>;" id="faltaAssinar"
                                                                   class="btn btn-info dim btn-large-dim btn-outline"
                                                                ><i class="fa fa-thumbs-up"></i> </a>
                                                                <?php
                                                            }
                                                        ?>
                                                        </div>
                                                    </td>
                                                    <?php }?>


                                                                                        <?php }else{?>
                                                                                        <!--
                                                                                        <br><br><div class="alert alert-danger">
                                Como o Organismo não cadastrou o <a class="alert-link" href="#">Saldo Inicial</a>, não são liberados os botões para upload de relatórios assinados.
                            </div>-->
                                                                                        <?php }?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
									</tr>
								</table>
							</div>
                    </div>
                    <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <i class="fa fa-info-circle"></i> <b>Dados do Relatório</b>
                                        </div>
                                        <div class="panel-body">
                                            <p>
                                                <ul>
                                                <li><b>Arquivos enviados: <?php echo $upload; if($upload=="sim"){ echo " - Não precisa necessariamente de assinatura eletrônica!";}?></b></li>
                                                    <li>Data de Início da criação: <?php if($dataCriacaoDocumento!="//"){ $criado=1; echo $dataCriacaoDocumento;}else{ $criado=0; echo " <b>não começou a ser criado</b>";}?></li>
                                                    <li>Última alteração: <?php if($dataUltimaAlteracaoDocumento!="//"){ echo $dataUltimaAlteracaoDocumento;}else{ echo "--";}?></li>
                                                    <li>Responsável(is) pela elaboração desse documento:<?php if(count($arrResp) == 0){ echo "--";}?></li>
                                                    <ol>
                                                        <?php if(count($arrResp)){?>
                                                            <?php foreach ($arrResp['nome'] as $k => $v){?>
                                                                <li><?php echo $v;?></li>
                                                            <?php }?>
                                                        <?php }?>
                                                    </ol>
                                                    <li>Oficiais que precisam assinar esse documento se for entregue eletronicamente:</li>
                                                    <ol>
                                                        <li>MESTRE DO ORGANISMO AFILIADO</li>
                                                        <li>SECRETÁRIO DO ORGANISMO AFILIADO</li>
                                                        <?php if($idClassificacaoOa==1||$idClassificacaoOa==3){?><li>TESOUREIRO DA JUNTA DEPOSITÁRIA</li><?php }?>
                                                        <li>VERIFICAÇÃO DE CONTAS</li>
                                                    </ol>

                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                    <input type="hidden" name="criado" id="criado" value="<?php echo $criado;?>">
                                </div>
                                <div class="col-lg-6">
                                    <div class="panel panel-<?php echo $class;?>">
                                        <div class="panel-heading">
                                            <i class="fa <?php echo $icon;?>"></i> <b><?php echo $tituloBox;?></b>
                                        </div>
                                        <div class="panel-body">
                                            <?php

                                            ?>
                                            <p>
                                                <ul>
                                                    <li>Data da Entrega (Ass. Eletrônica): <?php if($dataEntrega!=""){ echo $dataEntrega;}else{ echo "<span id='dataEntrega'>--</span>";}?></li>
                                                    <li>Quem Entregou (Ass. Eletrônica): <?php if($quemEntregou!=""){ echo $quemEntregou;}else{ echo "<span id='quemEntregou'>--</span>";}?></li>
                                                    <li>Oficiais que assinaram esse documento: <?php if($entregue==0||$arrOficiaisQueAssinaram==false){?>--<?php }?></li>
                                                    <?php if($entregue==1){?>
                                                        <ol>
                                                            <?php
                                                            if(count($arrOficiaisQueAssinaram)>0) {
                                                                foreach ($arrOficiaisQueAssinaram as $v) {
                                                                    ?>
                                                                    <li><?php echo $v['nomeUsuario']; ?> - <?php echo $v['nomeFuncao']; ?></li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ol>
                                                    <?php }?>
                                                    <li>Assinaturas: <?php if($dataEntregaCompletamente!=""){ echo $dataEntregaCompletamente;}else{ echo "--";}?></li>
                                                    <li>Faltam assinar: <?php if($entregue==0||count($arrOficiaisFaltamAssinar)==0){ echo "<span id='faltamAssinar'>--</span>";}?></li>
                                                    <?php if($entregue==1){?>
                                                    <ol>
                                                        <?php
                                                        if(count($arrOficiaisFaltamAssinar)>0) {
                                                            foreach ($arrOficiaisFaltamAssinar as $v) {
                                                                ?>
                                                                <li><?php echo $v; ?></li>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ol>
                                                    <?php }?>
                                                    <li>Código da Assinatura Eletrônica: <?php if($codigoAssinatura!=""){ echo $codigoAssinatura;}else{ echo "<span id='numeroAssinatura'>--</span>";}?></li>
                                                </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="wrapper wrapper-content animated fadeInRight">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Gráfico Financeiro Mensal</h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div class="row">
                                                        <div id="chart_div"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>


<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Relatório Financeiro Mensal Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="relatorioFinanceiro" name="relatorioFinanceiro" class="form-horizontal" method="post" enctype="multipart/form-data" onSubmit="return validaUpload()">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> é preciso enviar o relatório original para GLP por correio, mesmo fazendo o upload do relatório assinado. Caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"	id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<?php 
                        	include_once 'model/organismoClass.php';
                            $o = new organismo();
                        	$nomeOrganismo="";
                        	$resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
		                    if($resultado2)
		                    {
			                    	foreach($resultado2 as $vetor2)
			                    	{
			                    		switch($vetor2['classificacaoOrganismoAfiliado']){
											case 1:
												$classificacao =  "Loja";
												break;
											case 2:
												$classificacao =  "Pronaos";
												break;
											case 3:
												$classificacao =  "Capítulo";
												break;
											case 4:
												$classificacao =  "Heptada";
												break;
											case 5:
												$classificacao =  "Atrium";
												break;
										}
										switch($vetor2['tipoOrganismoAfiliado']){
											case 1:
												$tipo = "R+C";
												break;
											case 2:
												$tipo = "TOM";
												break;
										}
											
										$nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];
		                    	
		                    		}
		                    }
                        ?>
                    <input type="hidden" name="nomeOrganismo" id="nomeOrganismo" value="<?php echo $nomeOrganismo;?>"> 
                    <input type="hidden" name="nomeUsuario" id="nomeUsuario" value="<?php echo $dadosUsuario->getNomeUsuario();?>">   
					<input type="hidden" name="mesAtual" id="mesAtual" value="<?php echo $mesAtual;?>">
					<input type="hidden" name="anoAtual" id="anoAtual" value="<?php echo $anoAtual;?>">
					<input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
					<input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" value="<?php echo $_SESSION['idOrganismoAfiliado'];?>">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.relatorioFinanceiro.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Consulta aos arquivos entregues (**formato anterior)</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>



