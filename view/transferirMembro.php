<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Transferir Membro</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaTransferenciaMembro">Transferência de Membros do Organismo Afiliado</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de Transferência de Membros do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaTransferenciaMembro">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaTransferenciaMembro()" >
                        <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nome do Membro: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="nome" name="nome" type="text" value="<?php echo $_REQUEST['nome'];?>" readonly style="max-width: 343px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Afiliação: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="codigoAfiliacao" name="codigoAfiliacao" type="text" value="<?php echo $_REQUEST['codigoAfiliacao'];?>" readonly style="max-width: 83px"  required="required" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Interno: </label>
                            <div class="col-sm-9">
                                <div id="seqCadastMembroOaDiv" style="margin-top: 7px"><?php echo $_REQUEST['seqCadastMembro'];?></div>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo Destino:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliadoDestino" id="fk_idOrganismoAfiliadoDestino" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(null,null,null,$idOrganismoAfiliado);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Motivo da transferência:</label>
                            <div class="col-sm-9"><textarea rows="7" name="motivoTransferencia" id="motivoTransferencia" class="form-control" style="max-width: 500px"></textarea></div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Outras Informações:</label>
                            <div class="col-sm-9"><textarea rows="7" name="outrasInformacoes" id="outrasInformacoes" class="form-control" style="max-width: 500px"></textarea></div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">Observações:</label>
                            <div class="col-sm-9"><textarea rows="7" name="observacoes" id="observacoes" class="form-control" style="max-width: 500px"></textarea></div>
                        </div>
                        <input type="hidden" id="seqCadastMembro" name="seqCadastMembro" value="<?php echo $_REQUEST['seqCadastMembro'];?>" >
                        <input type="hidden" id="fk_idOrganismoAfiliadoOrigem" name="fk_idOrganismoAfiliadoOrigem" value="<?php echo $_REQUEST['origem']?>" >
                        <input type="hidden" id="siglaOAOrigem" name="siglaOAOrigem" value="<?php echo $_REQUEST['siglaOAOrigem'];?>" >
                        <?php 
                        	$ocultar_json=1;
							$naoAtuantes='N';
							$atuantes='S';
							$siglaOA=$_REQUEST['siglaOAOrigem'];
							$seqFuncao='201';//Mestre do OA
							include 'js/ajax/retornaFuncaoMembro.php';
							$obj = json_decode(json_encode($return),true);
							$mestreOAOrigem=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                        ?>
                        <input type="hidden" id="mestreOAOrigem" name="mestreOAOrigem" value="<?php echo $mestreOAOrigem;?>" >
                        <?php 
                        	$ocultar_json=1;
							$naoAtuantes='N';
							$atuantes='S';
							$siglaOA=$_REQUEST['siglaOAOrigem'];
							$seqFuncao='203';//Secretario do OA
							include 'js/ajax/retornaFuncaoMembro.php';
							$obj = json_decode(json_encode($return),true);
							$secretarioOAOrigem=$obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];
                        ?>
                        <input type="hidden" id="secretarioOAOrigem" name="secretarioOAOrigem" value="<?php echo $secretarioOAOrigem;?>" >
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        
                         <input type="hidden" name="nomeUsuarioTransferencia" id="nomeUsuarioTransferencia" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Salvar"
									>
									<i class="fa fa-mail-forward fa-white"></i>&nbsp; Transferir
								</button>
								&nbsp; <a href="?corpo=buscaTransferenciaMembro"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
	
					</form>
                </div>
            </div>
        </div>
	</div>
</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	