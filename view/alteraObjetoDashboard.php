
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboards</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaObjetoDashboard">Dashboards</a>
            </li>
            <li class="active">
                <a>Edição de Dashboard</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição de Dashboard</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaObjetoDashboard">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <?php
                include_once("controller/objetoDashboardController.php");

                $odc = new objetoDashboardController();
                $dados = $odc->buscaObjetoDashboard($_GET['id']);
                ?>
                <div class="ibox-content">
                    <form id="cadastro" method="POST" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idObjetoDashboard" id="idObjetoDashboard" value="<?php echo $_GET['id']; ?>" />
                        <div class="form-group"><label class="col-sm-2 control-label">*Nome</label>
                            <div class="col-sm-10"><input type="text" name="nomeObjetoDashboard" id="nomeObjetoDashboard" class="form-control" value="<?php echo $dados->getNomeObjetoDashboard(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Dashboard</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idTipoObjetoDashboard" id="fk_idTipoObjetoDashboard" style="max-width: 150px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'controller/tipoObjetoDashboardController.php';
                                    $todc = new tipoObjetoDashboardController();
                                    $todc->criarComboBox($dados->getFkIdTipoObjetoDashboard());
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Arquivo</label>
                            <div class="col-sm-10"><input type="text" name="arquivoObjetoDashboard" id="arquivoObjetoDashboard" class="form-control" value="<?php echo $dados->getArquivoObjetoDashboard(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Prioridade</label>
                            <div class="col-sm-10"><input type="text" name="prioridadeObjetoDashboard" id="prioridadeObjetoDashboard" class="form-control" value="<?php echo $dados->getPrioridadeObjetoDashboard(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea rows="7" name="descricaoObjetoDashboard" id="descricaoObjetoDashboard" class="form-control" style="max-width: 500px" required="required"><?php echo $dados->getDescricaoObjetoDashboard(); ?></textarea></div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->implode(Registro());
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

