<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>
$(document).ready(function(){
	$("#dataNascimento1").mask("99/99/9999");
});
</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Área de ATAs</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaPosse">ATA</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaPosse">Organismo Afiliado</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaPosse">Posse</a>
            </li>
            <li class="active">
                <strong><a>Formulário de Criação</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de criação de ATA de Posse</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtaPosse">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaAtaReuniaoMensal()" >
                        <div class="row">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Endereço do Local da Posse: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="enderecoPosse" name="enderecoPosse" type="text" value="" style="max-width: 343px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número do Local da Posse: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="5" onkeypress="return SomenteNumero(event)" id="numeroPosse" name="numeroPosse" type="text" value="" style="max-width: 83px"  required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Bairro do Local da Posse: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="bairroPosse" name="bairroPosse" type="text" value="" style="max-width: 343px"  required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cidade do Local da Posse: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="cidadePosse" name="cidadePosse" type="text" value="" style="max-width: 343px"  required="required">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataPosse" maxlength="10" id="dataPosse" type="text" class="form-control" style="max-width: 102px"  required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Hora de Início:</label>
                            <div class="col-sm-2 input-group" style="padding: 0px 0px 0px 15px">
                                <div class="input-group clockpicker"  data-autoclose="true">
                                <input type="text" class="form-control" onBlur="validaHora(this);" maxlength="5" id="horaInicioPosse" name="horaInicioPosse" value="" required="required">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            	</div>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Mestre Retirante:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="mestreRetirante" name="mestreRetirante" type="text" maxlength="100" value="" style="min-width: 320px">
                            </div>
                        </div>
                        
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Secretário(a) do Organismo Retirante:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="secretarioRetirante" name="secretarioRetirante" type="text" maxlength="100" value="" style="min-width: 320px">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Presidente da Junta Depositária Retirante:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="presidenteJuntaDepositariaRetirante" name="presidenteJuntaDepositariaRetirante" type="text" maxlength="100" value="" style="min-width: 320px">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Secretário(a) da Junta Depositária Retirante:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="secretarioJuntaDepositariaRetirante" name="secretarioJuntaDepositariaRetirante" type="text" maxlength="100" value="" style="min-width: 320px">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Tesoureiro(a) da Junta Depositária Retirante:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="tesoureiroJuntaDepositariaRetirante" name="tesoureiroJuntaDepositariaRetirante" type="text" maxlength="100" value="" style="min-width: 320px">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Guardião Retirante:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="guardiaoRetirante" name="guardiaoRetirante" type="text" maxlength="100" value="" style="min-width: 320px">
                            </div>
                        </div>
                        <div class="form-group form-inline">
	                            <label class="col-sm-3 control-label">Nome de quem dirigiu a posse:</label>
	                            <div class="col-sm-9" style="margin-top:0.5%">
	                                 <input class="form-control" id="nomeDirigiuPosse" name="nomeDirigiuPosse" type="text" maxlength="100" value="" style="min-width: 320px">
	                            </div>
	                    </div>
	                    <div class="form-group form-inline">
	                            <label class="col-sm-3 control-label">Cargo de quem dirigiu a posse:</label>
	                            <div class="col-sm-9" style="margin-top:0.5%">
	                                 <input class="form-control" id="cargoDirigiuPosse" name="cargoDirigiuPosse" type="text" maxlength="100" value="Mestre" style="min-width: 320px">
	                            </div>
	                    </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Detalhes adicionais<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="detalhesAdicionaisPosse" name="detalhesAdicionaisPosse"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>MESTRE</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Nome:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nome1" name="nome1" type="text" maxlength="100" value="" style="min-width: 320px" >
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cód. Afiliação:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" onblur="retornaDadosMembroNaAta(1)" id="codigoAfiliacao1" name="codigoAfiliacao1" type="text" maxlength="11" value="" style="min-width: 320px">
					                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Endereço:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="endereco1" name="endereco1" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cidade1" name="cidade1" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Cep:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cep1" name="cep1" type="text" maxlength="9" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Naturalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="naturalidade1" name="naturalidade1" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata2">
				                            <label class="col-sm-3 control-label">Data de Nasc.:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="dataNascimento1" name="dataNascimento1" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Nacionalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nacionalidade1" name="nacionalidade1" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Estado civil:</label>
					                            <div class="col-sm-9">
					                                <select class="form-control col-sm-3" name="estadoCivil1" id="estadoCivil1" style="max-width: 150px">
                                    					<option value="0">Selecione...</option>
                                    					<option value="1">Solteiro(a)</option>
                                    					<option value="2">Casado(a)</option>
                                    					<option value="3">Desquitado(a)</option>
                                    					<option value="4">Divorciado(a)</option>
                                    					<option value="5">União Estável</option>
                                    					<option value="6">Viúvo(a)</option>
                                   					</select>
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Profissão:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="profissao1" name="profissao1" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">RG/BI:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="rg1" name="rg1" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">CPF:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cpf1" name="cpf1" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata3">
				                            <label class="col-sm-3 control-label">Início Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="inicioMandato1" name="inicioMandato1" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
				                        <div class="form-group" id="datapicker_ata4">
				                            <label class="col-sm-3 control-label">Fim Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="fimMandato1" name="fimMandato1" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">&nbsp;</label>
					                            <div class="col-sm-9">
					                                <input type="radio" id="indicado1" name="indicado1" value="1" checked> Empossado
					                                <input type="radio" id="indicado1" name="indicado1" value="0"> Permanece 
					                            </div>
					                    </div>
					               </div>
					            </div>
					        </div>
					        <div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>MESTRE AUXILIAR</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Nome:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nome2" name="nome2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cód. Afiliação:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" onblur="retornaDadosMembroNaAta(2)" id="codigoAfiliacao2" name="codigoAfiliacao2" type="text" maxlength="11" value="" style="min-width: 320px">
					                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Endereço:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="endereco2" name="endereco2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cidade2" name="cidade2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Cep:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cep2" name="cep2" type="text" maxlength="9" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Naturalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="naturalidade2" name="naturalidade2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata5">
				                            <label class="col-sm-3 control-label">Data de Nasc.:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="dataNascimento2" name="dataNascimento2" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Nacionalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nacionalidade2" name="nacionalidade2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Estado civil:</label>
					                            <div class="col-sm-9">
					                                <select class="form-control col-sm-3" name="estadoCivil2" id="estadoCivil2" style="max-width: 150px">
                                    					<option value="0">Selecione...</option>
                                    					<option value="1">Solteiro(a)</option>
                                    					<option value="2">Casado(a)</option>
                                    					<option value="3">Disquitado(a)</option>
                                    					<option value="4">Divorciado(a)</option>
                                    					<option value="5">União Estável</option>
                                    					<option value="6">Viúvo(a)</option>
                                   					</select>
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Profissão:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="profissao2" name="profissao2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">RG/BI:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="rg2" name="rg2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">CPF:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cpf2" name="cpf2" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata6">
				                            <label class="col-sm-3 control-label">Início Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="inicioMandato2" name="inicioMandato2" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
				                        <div class="form-group" id="datapicker_ata7">
				                            <label class="col-sm-3 control-label">Fim Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="fimMandato2" name="fimMandato2" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">&nbsp;</label>
					                            <div class="col-sm-9">
					                                <input type="radio" id="indicado2" name="indicado2" value="1" checked> Empossado
					                                <input type="radio" id="indicado2" name="indicado2" value="0"> Permanece
					                            </div>
					                    </div>
					               </div>
					            </div>
					        </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>SECRETÁRIO(A) DA JUNTA DEPOSITÁRIA</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Nome:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nome3" name="nome3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cód. Afiliação:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" onblur="retornaDadosMembroNaAta(3)" id="codigoAfiliacao3" name="codigoAfiliacao3" type="text" maxlength="11" value="" style="min-width: 320px">
					                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Endereço:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="endereco3" name="endereco3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cidade3" name="cidade3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Cep:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cep3" name="cep3" type="text" maxlength="9" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Naturalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="naturalidade3" name="naturalidade3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata8">
				                            <label class="col-sm-3 control-label">Data de Nasc.:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="dataNascimento3" name="dataNascimento3" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Nacionalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nacionalidade3" name="nacionalidade3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Estado civil:</label>
					                            <div class="col-sm-9">
					                                <select class="form-control col-sm-3" name="estadoCivil3" id="estadoCivil3" style="max-width: 150px">
                                    					<option value="0">Selecione...</option>
                                    					<option value="1">Solteiro(a)</option>
                                    					<option value="2">Casado(a)</option>
                                    					<option value="3">Disquitado(a)</option>
                                    					<option value="4">Divorciado(a)</option>
                                    					<option value="5">União Estável</option>
                                    					<option value="6">Viúvo(a)</option>
                                   					</select>
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Profissão:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="profissao3" name="profissao3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">RG/BI:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="rg3" name="rg3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">CPF:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cpf3" name="cpf3" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata9">
				                            <label class="col-sm-3 control-label">Início Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="inicioMandato3" name="inicioMandato3" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
				                        <div class="form-group" id="datapicker_ata10">
				                            <label class="col-sm-3 control-label">Fim Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="fimMandato3" name="fimMandato3" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">&nbsp;</label>
					                            <div class="col-sm-9">
					                                <input type="radio" id="indicado3" name="indicado3" value="1" checked> Empossado
					                                <input type="radio" id="indicado3" name="indicado3" value="0"> Permanece
					                            </div>
					                    </div>
					               </div>
					            </div>
					        </div>
					        <div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>TESOUREIRO(A) DA JUNTA DEPOSITÁRIA</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Nome:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nome4" name="nome4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cód. Afiliação:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" onblur="retornaDadosMembroNaAta(4)" id="codigoAfiliacao4" name="codigoAfiliacao4" type="text" maxlength="11" value="" style="min-width: 320px">
					                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Endereço:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="endereco4" name="endereco4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cidade4" name="cidade4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Cep:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cep4" name="cep4" type="text" maxlength="9" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Naturalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="naturalidade4" name="naturalidade4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata11">
				                            <label class="col-sm-3 control-label">Data de Nasc.:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="dataNascimento4" name="dataNascimento4" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Nacionalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nacionalidade4" name="nacionalidade4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Estado civil:</label>
					                            <div class="col-sm-9">
					                                <select class="form-control col-sm-3" name="estadoCivil4" id="estadoCivil4" style="max-width: 150px">
                                    					<option value="0">Selecione...</option>
                                    					<option value="1">Solteiro(a)</option>
                                    					<option value="2">Casado(a)</option>
                                    					<option value="3">Disquitado(a)</option>
                                    					<option value="4">Divorciado(a)</option>
                                    					<option value="5">União Estável</option>
                                    					<option value="6">Viúvo(a)</option>
                                   					</select>
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Profissão:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="profissao4" name="profissao4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">RG/BI:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="rg4" name="rg4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">CPF:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cpf4" name="cpf4" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata12">
				                            <label class="col-sm-3 control-label">Início Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="inicioMandato4" name="inicioMandato4" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
				                        <div class="form-group" id="datapicker_ata13">
				                            <label class="col-sm-3 control-label">Fim Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="fimMandato4" name="fimMandato4" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">&nbsp;</label>
					                            <div class="col-sm-9">
					                                <input type="radio" id="indicado4" name="indicado4" value="1" checked> Empossado
					                                <input type="radio" id="indicado4" name="indicado4" value="0"> Permanece
					                            </div>
					                    </div>
					               </div>
					            </div>
					        </div>
                        </div>
                        <div class="row">
                        	<div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>SECRETÁRIO(A) DO ORGANISMO</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Nome:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nome5" name="nome5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cód. Afiliação:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" onblur="retornaDadosMembroNaAta(5)" id="codigoAfiliacao5" name="codigoAfiliacao5" type="text" maxlength="11" value="" style="min-width: 320px">
					                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Endereço:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="endereco5" name="endereco5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cidade5" name="cidade5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Cep:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cep5" name="cep5" type="text" maxlength="9" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Naturalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="naturalidade5" name="naturalidade5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata14">
				                            <label class="col-sm-3 control-label">Data de Nasc.:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="dataNascimento5" name="dataNascimento5" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Nacionalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nacionalidade5" name="nacionalidade5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Estado civil:</label>
					                            <div class="col-sm-9">
					                                <select class="form-control col-sm-3" name="estadoCivil5" id="estadoCivil5" style="max-width: 150px">
                                    					<option value="0">Selecione...</option>
                                    					<option value="1">Solteiro(a)</option>
                                    					<option value="2">Casado(a)</option>
                                    					<option value="3">Disquitado(a)</option>
                                    					<option value="4">Divorciado(a)</option>
                                    					<option value="5">União Estável</option>
                                    					<option value="6">Viúvo(a)</option>
                                   					</select>
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Profissão:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="profissao5" name="profissao5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">RG/BI:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="rg5" name="rg5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">CPF:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cpf5" name="cpf5" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata15">
				                            <label class="col-sm-3 control-label">Início Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="inicioMandato5" name="inicioMandato5" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
				                        <div class="form-group" id="datapicker_ata16">
				                            <label class="col-sm-3 control-label">Fim Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="fimMandato5" name="fimMandato5" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">&nbsp;</label>
					                            <div class="col-sm-9">
					                                <input type="radio" id="indicado5" name="indicado5" value="1" checked> Empossado
					                                <input type="radio" id="indicado5" name="indicado5" value="0"> Permanece
					                            </div>
					                    </div>
					               </div>
					            </div>
					        </div>
					        <div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>PRESIDENTE DA JUNTA DEPOSITÁRIA</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Nome:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nome6" name="nome6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cód. Afiliação:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" onblur="retornaDadosMembroNaAta(6)" id="codigoAfiliacao6" name="codigoAfiliacao6" type="text" maxlength="11" value="" style="min-width: 320px">
					                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Endereço:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="endereco6" name="endereco6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Cidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cidade6" name="cidade6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Cep:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cep6" name="cep6" type="text" maxlength="9" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Naturalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="naturalidade6" name="naturalidade6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata17">
				                            <label class="col-sm-3 control-label">Data de Nasc.:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="dataNascimento6" name="dataNascimento6" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Nacionalidade:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="nacionalidade6" name="nacionalidade6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Estado civil:</label>
					                            <div class="col-sm-9">
					                                <select class="form-control col-sm-3" name="estadoCivil6" id="estadoCivil6" style="max-width: 150px" required="required">
                                    					<option value="0">Selecione...</option>
                                    					<option value="1">Solteiro(a)</option>
                                    					<option value="2">Casado(a)</option>
                                    					<option value="3">Disquitado(a)</option>
                                    					<option value="4">Divorciado(a)</option>
                                    					<option value="5">União Estável</option>
                                    					<option value="6">Viúvo(a)</option>
                                   					</select>
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">Profissão:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="profissao6" name="profissao6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">RG/BI:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="rg6" name="rg6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">CPF:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cpf6" name="cpf6" type="text" maxlength="100" value="" style="min-width: 320px">
					                            </div>
					                    </div>
					                    <div class="form-group" id="datapicker_ata18">
				                            <label class="col-sm-3 control-label">Início Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="inicioMandato6" name="inicioMandato6" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
				                        <div class="form-group" id="datapicker_ata19">
				                            <label class="col-sm-3 control-label">Fim Mandato:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                                <input maxlength="10" id="fimMandato6" name="fimMandato6" type="text" class="form-control" style="max-width: 102px">
				                            </div>
				                        </div>
					                    <div class="form-group">
					                            <label class="col-sm-3 control-label">&nbsp;</label>
					                            <div class="col-sm-9">
					                                <input type="radio" id="indicado6" name="indicado6" value="1" checked> Empossado
					                                <input type="radio" id="indicado6" name="indicado6" value="0"> Permanece
					                            </div>
					                    </div>
					               </div>
					            </div>
					        </div>
                        
	                        <div class="col-lg-6">
						            <div class="ibox float-e-margins">
						                <div class="ibox-title">
						                    <h5>GUARDIÃO</h5>
						                    <div class="ibox-tools">
						                            <i class="fa fa-pencil-square-o"></i>
						                    </div>
						                </div>
						                <div class="ibox-content">
						                	<div class="form-group">
						                            <label class="col-sm-3 control-label">Nome:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="nome7" name="nome7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
					                        </div>
					                        <div class="form-group">
						                            <label class="col-sm-3 control-label">Cód. Afiliação:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" onblur="retornaDadosMembroNaAta(7)" id="codigoAfiliacao7" name="codigoAfiliacao7" type="text" maxlength="11" value="" style="min-width: 320px">
						                            </div>
					                        </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">Endereço:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="endereco7" name="endereco7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
					                        </div>
					                        <div class="form-group">
						                            <label class="col-sm-3 control-label">Cidade:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="cidade7" name="cidade7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
						                    </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">Cep:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="cep7" name="cep7" type="text" maxlength="9" value="" style="min-width: 320px">
						                            </div>
						                    </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">Naturalidade:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="naturalidade7" name="naturalidade7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
						                    </div>
						                    <div class="form-group" id="datapicker_ata20">
					                            <label class="col-sm-3 control-label">Data de Nasc.:</label>
					                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
					                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					                                <input maxlength="10" id="dataNascimento7" name="dataNascimento7" type="text" class="form-control" style="max-width: 102px">
					                            </div>
					                        </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">Nacionalidade:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="nacionalidade7" name="nacionalidade7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
						                    </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">Estado civil:</label>
						                            <div class="col-sm-9">
						                                <select class="form-control col-sm-3" name="estadoCivil7" id="estadoCivil7" style="max-width: 150px" required="required">
	                                    					<option value="0">Selecione...</option>
	                                    					<option value="1">Solteiro(a)</option>
	                                    					<option value="2">Casado(a)</option>
	                                    					<option value="3">Disquitado(a)</option>
	                                    					<option value="4">Divorciado(a)</option>
	                                    					<option value="5">União Estável</option>
	                                    					<option value="6">Viúvo(a)</option>
	                                   					</select>
						                            </div>
						                    </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">Profissão:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="profissao7" name="profissao7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
						                    </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">RG/BI:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="rg7" name="rg7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
						                    </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">CPF:</label>
						                            <div class="col-sm-9">
						                                <input class="form-control" id="cpf7" name="cpf7" type="text" maxlength="100" value="" style="min-width: 320px">
						                            </div>
						                    </div>
						                    <div class="form-group" id="datapicker_ata21">
					                            <label class="col-sm-3 control-label">Início Mandato:</label>
					                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
					                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					                                <input maxlength="10" id="inicioMandato7" name="inicioMandato7" type="text" class="form-control" style="max-width: 102px">
					                            </div>
					                        </div>
					                        <div class="form-group" id="datapicker_ata22">
					                            <label class="col-sm-3 control-label">Fim Mandato:</label>
					                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
					                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					                                <input maxlength="10" id="fimMandato7" name="fimMandato7" type="text" class="form-control" style="max-width: 102px">
					                            </div>
					                        </div>
						                    <div class="form-group">
						                            <label class="col-sm-3 control-label">&nbsp;</label>
						                            <div class="col-sm-9">
						                                <input type="radio" id="indicado7" name="indicado7" value="1" checked> Empossado
						                                <input type="radio" id="indicado7" name="indicado7" value="0"> Permanece
						                            </div>
						                    </div>
						               </div>
						            </div>
						        </div>
	                        </div>
                        <?php 
                        	include_once 'model/organismoClass.php';
                            $o = new organismo();
                        	$nomeOrganismo="";
                        	$resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
		                    if($resultado2)
		                    {
			                    	foreach($resultado2 as $vetor2)
			                    	{
			                    		switch($vetor2['classificacaoOrganismoAfiliado']){
											case 1:
												$classificacao =  "Loja";
												break;
											case 2:
												$classificacao =  "Pronaos";
												break;
											case 3:
												$classificacao =  "Capítulo";
												break;
											case 4:
												$classificacao =  "Heptada";
												break;
											case 5:
												$classificacao =  "Atrium";
												break;
										}
										switch($vetor2['tipoOrganismoAfiliado']){
											case 1:
												$tipo = "R+C";
												break;
											case 2:
												$tipo = "TOM";
												break;
										}
											
										$nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];
                                                                                $siglaOrganismo = $vetor2["siglaOrganismoAfiliado"];
		                    		}
		                    }
                        ?>
                        <input type="hidden" name="siglaOrganismoAta" id="siglaOrganismoAta" value="<?php echo $siglaOrganismo;?>">
                        <input type="hidden" name="nomeOrganismoAta" id="nomeOrganismoAta" value="<?php echo $nomeOrganismo;?>">
                        <input type="hidden" name="nomeUsuarioAta" id="nomeUsuarioAta" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
                        <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-12">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Salvar a Ata"
									OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');">
									<i class="fa fa-check fa-white"></i>&nbsp; Salvar
								</button>
								&nbsp; <a href="?corpo=buscaAtaPosse"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
	
					</form>
                </div>
            </div>
        </div>
	</div>
	
	

</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-times-circle modal-icon"></i>
                <h4 class="modal-title">Edição de ATA de Reunião Mensal do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <label>Motivo da edição:</label>
                <div style="border: #ccc solid 1px">
                    <div class="summernote"></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="atualizaMotivoEdicaoATA();">Editar ATA</button>
            </div>
        </div>
    </div>
</div>

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	