<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<style>
    .form-control-static {padding-top: 0px;}


</style>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
    <script>
        window.onload = function(){
            swal({
                title: "Sucesso!",
                text: "Imóvel salvo com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Imóveis</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Imóveis</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<?php
include_once("controller/organismoController.php");
include_once("controller/usuarioController.php");
include_once("controller/imovelController.php");
include_once("controller/imovelAnexoTipoController.php");
require_once ("model/organismoClass.php");
require_once ("model/imovelClass.php");
require_once ("model/funcaoUsuarioClass.php");
require_once ("model/funcaoClass.php");
require_once ("model/membroOAClass.php");
require_once("model/criaSessaoClass.php");

$oc     = new organismoController();
$om     = new organismo();
$ic     = new imovelController();
$im     = new imovel();
$uc     = new usuarioController();
$um     = new Usuario();
$fum    = new FuncaoUsuario();
$fm     = new funcao();
$mm     = new membroOA();
$sessao = new criaSessao();
$iatc   = new imovelAnexoTipoController();
$iatm   = new ImovelAnexoTipo();

$idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';

if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
    $idOrg = $idOrganismo;
} else {
    $idOrg = $idOrganismoAfiliado;
    if($sessao->getValue("alerta-imovel")==null){
        $poteOAs=0;
        $resultadoOAs = $om->listaOrganismo(null, null, "siglaOrganismoAfiliado", $sessao->getValue("regiao"));
        if($resultadoOAs){
            foreach($resultadoOAs as $vetorOAs){
                $poteOAs++;
            }
        }
        if($poteOAs>1){
            ?>
            <script>
                window.onload = function(){
                    swal({
                        title: "Aviso!",
                        text: "Você pode mudar o organismo listado no cabeçalho!",
                        confirmButtonColor: "#1ab394"
                    });
                }
            </script>
            <?php
        }
        $sessao->setValue("alerta-imovel", true);
    }
}
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Imóveis Cadastrados
                        <?php
                        if($idOrg!=''){
                            $dadosOrg = $oc->buscaOrganismo($idOrg);
                            $nomeOa = organismoAfiliadoNomeCompleto($dadosOrg->getClassificacaoOrganismoAfiliado(),$dadosOrg->getTipoOrganismoAfiliado(),$dadosOrg->getNomeOrganismoAfiliado(),$dadosOrg->getSiglaOrganismoAfiliado());
                            echo ' - '.$nomeOa;
                        }
                        ?>
                    </h5>
                    <div class="ibox-tools">
                        <?php if($idOrganismo!=''){ ?>
                            <?php if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){ ?>
                                <a class="btn btn-xs btn-primary" href="?corpo=buscaImovel">
                                    <i class="fa fa-undo"></i> Eliminar Filtro
                                </a>
                            <?php } ?>
                        <?php } ?>
                        <?php if(in_array("1",$arrNivelUsuario)){?>
                            <a class="btn btn-xs btn-primary" href="?corpo=cadastroImovel">
                                <i class="fa fa-plus"></i> Cadastrar Novo
                            </a>
                        <?php }?>
                        <?php if($idOrg!='') { ?>
                            <a class="btn btn-xs btn-primary" href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $idOrg; ?>">
                                <i class="fa fa-mail-forward"></i> Acessar Controle de Documentos
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>ID</th>
                            <?php
                            if($idOrg==''){
                                if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){
                                    ?>
                                    <th>Organismo Afiliado</th>
                                    <?php
                                }
                            }
                            ?>
                            <th>Imovel</th>
                            <th><center>CEP</center></th>
                            <th><center>Status</center></th>
                            <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $statusImovel=null;
                        $resultado = $ic->listaImovel($idOrg, $statusImovel);

                        if ($resultado) {
                            foreach ($resultado as $vetor) {

                                $complemento='';
                                if($vetor['complementoImovel']!=''){
                                    $complemento = ' - '.$vetor['complementoImovel'];
                                }
                                $nomeImovel = $vetor['enderecoImovel'].' - '.$vetor['numeroImovel'].$complemento;

                                ?>
                                <tr>
                                    <td>
                                        <?php echo $vetor['idImovel'] ?>
                                    </td>
                                    <?php
                                    if($idOrg==''){
                                        if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){
                                            ?>
                                            <td>
                                                <?php
                                                $dados = $oc->buscaOrganismo($vetor['fk_idOrganismoAfiliado']);
                                                $nomeOa = organismoAfiliadoNomeCompleto($dados->getClassificacaoOrganismoAfiliado(),$dados->getTipoOrganismoAfiliado(),$dados->getNomeOrganismoAfiliado(),$dados->getSiglaOrganismoAfiliado());
                                                echo $nomeOa;
                                                ?>
                                                <div style="float: right" class="btn-group">
                                                    <button data-toggle="dropdown" class="btn btn-primary btn-xs dropdown-toggle"><i class="fa fa-share fa-white"></i>&nbsp; Filtrar <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <?php if($idOrg==''){ ?>
                                                            <li><a href="?corpo=buscaImovel&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Imóveis apenas deste O.A.</a></li>
                                                            <li class="divider"></li>
                                                        <?php } ?>
                                                        <li><a href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Controle de Documentos deste O.A.</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <td>
                                        <?php
                                            echo $nomeImovel;
                                        ?>
                                    </td>
                                    <td>
                                        <center>
                                            <?php echo $vetor['cepImovel']; ?>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <div class="statusTarget<?php echo $vetor['idImovel'] ?>">
                                                <?php
                                                switch ($vetor['statusImovel']) {
                                                    case 0: echo "<span class='badge badge-primary'>Ativo</span>"; break;
                                                    case 1: echo "<span class='badge badge-danger'>Inativo</span>"; break;
                                                }
                                                ?>
                                            </div>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <div id="acoes_confirma_cancela">
                                                <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idImovel']; ?>">
                                                    <i class="fa fa-search-plus fa-white"></i>
                                                    Detalhes
                                                </a>
                                                <a class="btn btn-sm btn-white" onclick="abrirPopUpImprimirImovel(<?php echo $vetor['idImovel'];?>);">
                                                    <i class="fa fa-print fa-white"></i>
                                                    Imprimir
                                                </a>
                                                <?php if(in_array("2",$arrNivelUsuario)){?>
                                                    <a class="btn btn-sm btn-info" href="?corpo=alteraImovel&idImovel=<?php echo $vetor['idImovel']; ?>" data-rel="tooltip" title="">
                                                        <i class="fa fa-edit fa-white"></i>
                                                        Atualizar
                                                    </a>
                                                    <br>
                                                <?php } ?>
                                                <?php if(!$usuarioApenasLeitura){?>
                                                <a class="btn btn-sm btn-warning" onclick="" data-target="#mySeeAnexos<?php echo $vetor['idImovel']; ?>" data-toggle="modal" data-placement="left" title="Anexos">
                                                    <i class="fa fa-paperclip fa-white"></i>
                                                    Anexos
                                                </a>
                                                <?php } ?>
                                                <a class="btn btn-sm btn-success" href="?corpo=buscaImovelGaleria&idImovel=<?php echo $vetor['idImovel']; ?>" data-rel="tooltip" title="">
                                                    <i class="fa fa-picture-o fa-white"></i>
                                                    Galeria
                                                </a>
                                                <?php if(in_array("3",$arrNivelUsuario)){?>
                                                    <span id="status<?php echo $vetor['idImovel'] ?>">
                                                    <?php if ($vetor['statusImovel'] == 1) { ?>
                                                        <a class="btn btn-sm btn-primary" onclick="mudaStatus('<?php echo $vetor['idImovel'] ?>', '<?php echo $vetor['statusImovel'] ?>', 'imovel')" data-rel="tooltip" title="Ativar">
                                                            Ativar
                                                        </a>
                                                    <?php } else { ?>
                                                        <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idImovel'] ?>', '<?php echo $vetor['statusImovel'] ?>', 'imovel')" data-rel="tooltip" title="Inativar">

                                                            Inativar
                                                        </a>
                                                    <?php } ?>
                                                </span>
                                                <?php } ?>
                                                <?php $titulo = "Dados do Imóvel ".$nomeImovel; ?>
                                                <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                                    <!-- abreModalNotificacao(ID do Organismo , Título Padrão Desejado) -->
                                                    <a class="btn btn-sm btn-warning" onclick="abreModalNotificacao(<?php echo $vetor['fk_idOrganismoAfiliado'];?>,'<?php echo $titulo; ?>')">
                                                        <i class="fa fa-envelope icon-white"></i>
                                                        Notificar
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </center>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<!-- Modal's de DETALHES DO IMÓVEL -->
<?php
$resultado = $ic->listaImovel($idOrg);
if ($resultado) {
    foreach ($resultado as $vetorDetalhe) {
        ?>
        <!-- Modal de detalhes do usuário -->
        <div class="modal inmodal" id="myModaInfo<?php echo $vetorDetalhe['idImovel']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Detalhe do Imóvel</h4>
                    </div>
                    <div class="modal-body">

                        <!--
                        ANEXOS
                        -->
                        <!--
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: right">Anexo: </label>
                                <div class="col-sm-8">
                                    <div class="col-sm-12" style="max-width: 320px">
                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php $arrLink = explode('.', "img/imovel/controle/bombeiros/anexo.pdf");
                                            $arrLinkNome	= $arrLink[count($arrLink)-2];
                                            $arrLinkExt		= $arrLink[count($arrLink)-1];
                                            ?>
                                            <a target="_BLANK" href="<?php echo "img/imovel/controle/bombeiros/anexo.pdf"; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        -->

                        <div class="row">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Endereço: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $vetorDetalhe['enderecoImovel']; ?></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Nº: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $vetorDetalhe['numeroImovel']; ?></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Complemento: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $vetorDetalhe['complementoImovel']; ?></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Status: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px">
                                            <p class="form-control-static">
                                            <div class="statusTarget<?php echo $vetorDetalhe['idImovel'] ?>">
                                                <?php
                                                switch ($vetorDetalhe['statusImovel']) {
                                                    case 0:
                                                        echo "<span class='badge badge-primary'>Ativo</span>";
                                                        break;
                                                    case 1:
                                                        echo "<span class='badge badge-danger'>Inativo</span>";
                                                        break;
                                                }
                                                ?>
                                            </div>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Bairro: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $vetorDetalhe['bairroImovel']; ?></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Cidade: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px">
                                            <p class="form-control-static">
                                                <?php
                                                $resultadoCidade = $im->buscaCidade($vetorDetalhe['fk_idCidade']);
                                                if ($resultadoCidade) {
                                                    foreach ($resultadoCidade as $vetorCidade) {
                                                        echo $vetorCidade['nome'];
                                                    }
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Estado: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px">
                                            <p class="form-control-static">
                                                <?php
                                                $resultadoEstado = $im->buscaEstado($vetorDetalhe['fk_idEstado']);
                                                if ($resultadoEstado) {
                                                    foreach ($resultadoEstado as $vetorEstado) {
                                                        echo $vetorEstado['nome'];
                                                    }
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">País: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px">
                                            <p class="form-control-static">
                                                <?php
                                                switch ($vetorDetalhe['fk_idPais']){
                                                    case 1:
                                                        echo "Brasil";
                                                        break;
                                                    case 2:
                                                        echo "Portugal";
                                                        break;
                                                    case 3:
                                                        echo "Angola";
                                                        break;
                                                }
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">CEP: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php echo $vetorDetalhe['cepImovel']; ?></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Área Total (m²): </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-1" style="max-width: 320px"><p class="form-control-static"><?php echo $vetorDetalhe['areaTotalImovel']; ?></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Área Contruída (m²): </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-1" style="max-width: 320px"><p class="form-control-static"><?php echo $vetorDetalhe['areaConstruidaImovel']; ?></p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Tipo de Propriedade: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-12" style="max-width: 320px">
                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php switch($vetorDetalhe['propriedadeTipoImovel']){
                                                    case '': echo "Não Informado"; break;
                                                    case 0: echo "Prédio"; break;
                                                    case 1: echo "Sobrado"; break;
                                                    case 2: echo "Casa"; break;
                                                    case 3: echo "Barracão"; break;
                                                    case 4: echo "Outros"; break;
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Status da Propriedade: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-12" style="max-width: 320px">
                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                <?php switch($vetorDetalhe['propriedadeStatusImovel']){
                                                    case 0: echo "Não Informado"; break;
                                                    case 1: echo "Própria"; break;
                                                    case 2: echo "Alugada"; break;
                                                    case 3: echo "Cedida";  break;
                                                    case 4: echo "Comodato"; break;
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if($vetorDetalhe['propriedadeStatusImovel'] != 0){ ?>
                                <?php if($vetorDetalhe['propriedadeStatusImovel'] == 1){ ?>
                                    <hr>
                                    <div id="statusProprio">
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Data de Fundação</label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-8" style="max-width: 320px"><p class="form-control-static"><?php if($vetorDetalhe['dataFundacaoImovel']!='0000-00-00'){ echo substr($vetorDetalhe['dataFundacaoImovel'], 8, 2) . "/" . substr($vetorDetalhe['dataFundacaoImovel'], 5, 2) . "/" . substr($vetorDetalhe['dataFundacaoImovel'], 0, 4);} else { echo 'Não Informado';}?></p></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Propriedade em nome: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php switch($vetorDetalhe['propriedadeEmNomeImovel']){
                                                                case 0: echo "Indefinido"; break;
                                                                case 1: echo "GLB"; break;
                                                                case 2: echo "GLP"; break;
                                                                case 3: echo "O.A."; break;
                                                                case 4: echo "Outros"; break;
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Possui Escritura: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php
                                                            switch($vetorDetalhe['propriedadeEscrituraImovel']){
                                                                case 0: echo "Sim";  break;
                                                                case 1: echo "Não";  break;
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($vetorDetalhe['propriedadeEscrituraImovel'] == 0){ ?>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Tipo de Escritura: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php
                                                                $resultado2 = $im->buscaTipoEscrituraImovel($vetorDetalhe['fk_idTipoEscrituraImovel']);
                                                                if ($resultado2) {
                                                                    foreach ($resultado2 as $vetor2) {
                                                                        echo $vetor2['tipoEscrituraImovel'];
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Data da Escritura: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($vetorDetalhe['propriedadeEscrituraDataImovel']!='0000-00-00'){ echo substr($vetorDetalhe['propriedadeEscrituraDataImovel'],8,2)."/".substr($vetorDetalhe['propriedadeEscrituraDataImovel'],5,2)."/".substr($vetorDetalhe['propriedadeEscrituraDataImovel'],0,4);} else { echo 'Não Informado';}?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Possui Registro de Imóvel: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php
                                                            switch($vetorDetalhe['registroImovel']){
                                                                case 0: echo "Sim";  break;
                                                                case 1: echo "Não";  break;
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($vetorDetalhe['registroImovel'] == 0){ ?>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Data do Registro de Imóvel: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($vetorDetalhe['registroImovelDataImovel']!='0000-00-00'){ echo substr($vetorDetalhe['registroImovelDataImovel'],8,2)."/".substr($vetorDetalhe['registroImovelDataImovel'],5,2)."/".substr($vetorDetalhe['registroImovelDataImovel'],0,4);} else { echo 'Não Informado';}?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Nº da Matrícula: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($vetorDetalhe['registroImovelNumeroImovel']!=''){ echo $vetorDetalhe['registroImovelNumeroImovel']; } else { echo 'Não Informado';} ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Valor Venal: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php if($vetorDetalhe['valorVenalImovel']!=''){ echo 'R$ '.$vetorDetalhe['valorVenalImovel'];  } else { echo 'Não Informado';} ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Valor Atual Aproximado: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php if($vetorDetalhe['valorAproximadoImovel']!=''){ echo 'R$ '.$vetorDetalhe['valorAproximadoImovel'];  } else { echo 'Não Informado';} ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Imóvel Avaliado: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php
                                                            switch($vetorDetalhe['avaliacaoImovel']){
                                                                case 0: echo "Sim";  break;
                                                                case 1: echo "Não";  break;
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($vetorDetalhe['avaliacaoImovel'] == 0){?>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Valor Avaliado: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($vetorDetalhe['avaliacaoValorImovel']!=''){ echo 'R$ '.$vetorDetalhe['avaliacaoValorImovel'];  } else { echo 'Não Informado';} ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Nome do Avaliador: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php echo $vetorDetalhe['avaliacaoNomeImovel']; ?>
                                                                <?php if($vetorDetalhe['avaliacaoNomeImovel']!=''){ echo $vetorDetalhe['avaliacaoNomeImovel']; } else { echo 'Não Informado';} ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Creci do Avaliador: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($vetorDetalhe['avaliacaoCreciImovel']!=''){ echo $vetorDetalhe['avaliacaoCreciImovel']; } else { echo 'Não Informado';} ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: right">Data da Avaliação: </label>
                                                    <div class="col-sm-8">
                                                        <div class="col-sm-12" style="max-width: 320px">
                                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                                <?php if($vetorDetalhe['avaliacaoDataImovel']!='0000-00-00'){ echo substr($vetorDetalhe['avaliacaoDataImovel'],8,2)."/".substr($vetorDetalhe['avaliacaoDataImovel'],5,2)."/".substr($vetorDetalhe['avaliacaoDataImovel'],0,4);} else { echo 'Não Informado';}?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                        <hr>
                                    </div>
                                <?php } ?>
                                <?php if(($vetorDetalhe['propriedadeStatusImovel']==2) || ($vetorDetalhe['propriedadeStatusImovel']==3) || ($vetorDetalhe['propriedadeStatusImovel']==4)){ ?>
                                    <div id="statusOutro">
                                        <hr>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Locador/Cessionário: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php if($vetorDetalhe['locadorImovel']!=''){ echo $vetorDetalhe['locadorImovel'];  } else { echo 'Não Informado';} ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" style="text-align: right">Locatário/Cedente: </label>
                                                <div class="col-sm-8">
                                                    <div class="col-sm-12" style="max-width: 320px">
                                                        <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                            <?php if($vetorDetalhe['locatarioImovel']!=''){ echo $vetorDetalhe['locatarioImovel'];  } else { echo 'Não Informado';} ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Início do Contrato/Acordo: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-12" style="max-width: 320px">
                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                        <?php if($vetorDetalhe['aluguelContratoInicioImovel']!='0000-00-00'){ echo substr($vetorDetalhe['aluguelContratoInicioImovel'],8,2)."/".substr($vetorDetalhe['aluguelContratoInicioImovel'],5,2)."/".substr($vetorDetalhe['aluguelContratoInicioImovel'],0,4);} else { echo 'Não Informado';}?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Término do Contrato/Acordo: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-12" style="max-width: 320px">
                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                        <?php if($vetorDetalhe['aluguelContratoFimImovel']!='0000-00-00'){ echo substr($vetorDetalhe['aluguelContratoFimImovel'],8,2)."/".substr($vetorDetalhe['aluguelContratoFimImovel'],5,2)."/".substr($vetorDetalhe['aluguelContratoFimImovel'],0,4);} else { echo 'Não Informado';}?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" style="text-align: right">Valor de Aluguel: </label>
                                            <div class="col-sm-8">
                                                <div class="col-sm-12" style="max-width: 320px">
                                                    <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                                        <?php if($vetorDetalhe['aluguelValorImovel']!=''){ echo 'R$ '.$vetorDetalhe['aluguelValorImovel'];  } else { echo 'Não Informado';} ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Observações: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-12" style="max-width: 320px">
                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php echo $vetorDetalhe['descricaoImovel']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: right">Último a atualizar: </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-12" style="max-width: 320px">
                                            <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
                                            <?php
                                            $resultadoAtualizadoPor = $um->buscaUsuario($vetorDetalhe['fk_seqCadastAtualizadoPor']);
                                            if ($resultadoAtualizadoPor) {
                                                foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                                    echo $vetorAtualizadoPor['nomeUsuario'];
                                                }
                                            }
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<?php modalNotificacao($sessao->getValue("seqCadast")); ?>

<!-- Modal's de ANEXOS DO IMÓVEL -->
<?php
$resultadoAnexo = $ic->listaImovel($idOrg);
if ($resultadoAnexo) {
    foreach ($resultadoAnexo as $vetorAnexo) {
?>
<div class="modal inmodal" id="mySeeAnexos<?php echo $vetorAnexo['idImovel']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <i class="fa fa-paperclip modal-icon"></i>
                <h4 class="modal-title">Anexos do Imóvel</h4>
            </div>
            <div class="modal-body">
                <?php
                $objImovelAnexoTipo = $iatc->listaImovelAnexoTipo(0,0);
                if($objImovelAnexoTipo){
                    foreach($objImovelAnexoTipo as $vetorImovelAnexoTipo){
                ?>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" style="text-align: right">
                            <?php echo $vetorImovelAnexoTipo['nomeImovelAnexoTipo'] ?>:
                        </label>
                        <div class="col-sm-8">
                            <div class="col-sm-12" style="max-width: 320px">
                                <input
                                    name="file_upload_imovel_anexo<?php echo $vetorAnexo['idImovel'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>"
                                    id="file_upload_imovel_anexo<?php echo $vetorAnexo['idImovel'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>"
                                    type="file"
                                /><br>
                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;" name="listaAnexos<?php echo $vetorAnexo['idImovel'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>" id="listaAnexos<?php echo $vetorAnexo['idImovel'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>">
                                    <?php
                                    $objImovelAnexo = $im->listaImovelAnexo($vetorAnexo['idImovel'], $vetorImovelAnexoTipo['idImovelAnexoTipo']);
                                    if($objImovelAnexo){
                                        foreach($objImovelAnexo as $vetorImovelAnexo){
                                            $attach = $vetorImovelAnexo['full_path'];
                                            if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $attach)) {
                                                $attach = '/soa' . $attach; // treinamento
                                            }
                                            echo "<a target='_BLANK' href='" . $attach . "'>". $vetorImovelAnexo['nome_original'] . "</a><a style='margin-right: 25px' onclick='excluiImovelAnexo(" . $vetorImovelAnexo['idImovelAnexo'] . "," . $vetorAnexo['idImovel'] . "," . $vetorImovelAnexo['fk_idImovelAnexoTipo'] . ");'> <i class='fa fa-trash'></i></a><br>";
                                        }
                                    } else {
                                        echo "Nenhum anexo enviado!";
                                    }

                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <?php
                    }
                }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
    }
}
?>

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
				