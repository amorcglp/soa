<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
       @include_once("lib/webservice/retornaInformacoesMembro.php");
      
include_once("model/ritoPassagemOGGClass.php");
$cr = new ritoPassagemOGG();
$dados = $cr->lista($_GET['id']);
//echo "<pre>";print_r($dados);
?>			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Ritos de Passagem - OGG</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>             
	                        <li class="active">
	                            <strong><a>Alterar Rito de Passagem</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->

				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de Alteração de Ritos de Passagem da OGG do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaRitosPassagem">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="formulario" class="form-horizontal" method="post" action="acoes/acaoAlterar.php" onsubmit="return validaRitoPassagemOGG();">
                        <input type="hidden" id="idRitoPassagemOGG" name="idRitoPassagemOGG" value="<?php echo $_GET['id'];?>">
                        <div class="row">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2" required="required">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox($dados[0]['fk_idOrganismoAfiliado'],$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Rito:</label>
                            <div class="col-sm-9">
                                <select name="rito" id="rito" data-placeholder="Selecione um rito..." class="chosen-select" style="width:350px;" tabindex="2" required="required">
                                    <option value="0">Selecione</option>
                                    <option value="1" <?php if($dados[0]['rito']==1){ echo "selected";}?>>O Chamado</option>
                                    <option value="2" <?php if($dados[0]['rito']==2){ echo "selected";}?>>Guardião do Castelo</option>
                                    <option value="3" <?php if($dados[0]['rito']==3){ echo "selected";}?>>Escudeiro da Verdade</option>
                                    <option value="4" <?php if($dados[0]['rito']==4){ echo "selected";}?>>Fiel Servidor</option>
                                    <option value="5" <?php if($dados[0]['rito']==5){ echo "selected";}?>>Cavaleiro da Perseverança</option>
                                    <option value="6" <?php if($dados[0]['rito']==6){ echo "selected";}?>>Guias do Graal</option>
                                </select>
                            </div>
                        </div>    
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="data" onBlur="" maxlength="10" id="data" type="text" class="form-control" style="max-width: 102px"
                                       value="<?php echo substr($dados[0]['data'],8,2)."/".substr($dados[0]['data'],5,2)."/".substr($dados[0]['data'],0,4);?>"
                                       >
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-3 control-label">Hora:</label>
                                <div class="col-sm-2 input-group" style="padding: 0px 0px 0px 15px">
                                    <div class="input-group clockpicker"  data-autoclose="true">
                                    <input type="text" class="form-control" onBlur="" maxlength="5" id="hora" name="hora" value="<?php echo $dados[0]['hora'];?>">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>  
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comentário: </label>
                            <div class="col-sm-9">
                                <textarea cols="50" rows="5" name="comentario" id="comentario"><?php echo $dados[0]['comentario'];?></textarea>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Mestre na Ocasião:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembroMestre" maxlength="7" type="text" value="<?php echo retornaCodigoAfiliacao($dados[0]['seqCadastMestre']);?>" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroMestre" type="text" maxlength="100" value="<?php echo $dados[0]['nomeMestre'];?>" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroMestre','nomeMembroMestre','codAfiliacaoMembroMestre','nomeMembroMestre','h_seqCadastMembroMestre','h_nomeMembroMestre','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Digite o código de afiliação do membro e no outro campo acima o primeiro nome e pressione TAB
                                        <!--<input type="checkbox" value="" name="companheiroMembroPresidente" id="companheiroMembroPresidente"> Companheiro-->
                                <input type="hidden" name="h_seqCadastMembroMestre" id="h_seqCadastMembroMestre" value="<?php echo $dados[0]['seqCadastMestre']; ?>">
                                <input type="hidden" name="h_nomeMembroMestre" id="h_nomeMembroMestre" value="<?php echo $dados[0]['nomeMestre']; ?>">   
                            </div>
                        </div>    

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Frequência </label>
                            <div class="col-sm-9">
                               
                            </div>
                        </div>    
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Membros:</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipo" id="tipo">
                                    <option value="0">Selecione</option>
                                    <option value="1">Adulto</option>
                                    <option value="2">Jovem</option>
                                </select>
                                <input class="form-control" id="codAfiliacaoMembroOficial" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroOficial" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroOficial','nomeMembroOficial','codAfiliacaoMembroOficial','nomeMembroOficial','h_seqCadastMembroOficial','h_nomeMembroOficial','myModalPesquisa','informacoesPesquisa', 'S');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                E-mail
                                <input type="text" id="emailMembro" name="emailMembro" value="">
                                <br>
                                <input type="hidden" id="h_seqCadastMembroOficial">
                                <input type="hidden" id="h_nomeMembroOficial">
                                <input type="hidden" id="h_seqCadastCompanheiroMembroOficial">
                                <input type="hidden" id="h_nomeCompanheiroMembroOficial">
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiMembrosConvocacaoRitualistica();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="ata_oa_mensal_oficiais[]" id="ata_oa_mensal_oficiais" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                            <?php
                                            include_once 'model/ritoPassagemOGGMembroClass.php';
                                            $membros = new ritoPassagemOGGMembro();
                                            $resultado = $membros->lista($dados[0]['idRitoPassagemOGG']);

                                            foreach ($resultado as $vetor) {
                                                ?>
                                            <option value="{'emailMembro' : '<?php echo $vetor['email']; ?>', 'tipoMembro' : <?php echo $vetor['tipo']; ?>, 'seqCadast': <?php echo $vetor['fk_seq_cadastMembro']; ?>}">[<?php echo retornaCodigoAfiliacao($vetor['fk_seq_cadastMembro'],$vetor['tipo']) ?>] <?php echo retornaNomeCompleto($vetor['fk_seq_cadastMembro']) ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Testemunhas:</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipoTestemunha" id="tipoTestemunha">
                                    <option value="0">Selecione</option>
                                    <option value="1" selected="selected">Adulto</option>
                                    <!--<option value="2">Jovem</option>-->
                                </select>
                                <input class="form-control" id="codAfiliacaoMembroOficialTestemunha" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroOficialTestemunha" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroOficialTestemunha','nomeMembroOficialTestemunha','codAfiliacaoMembroOficialTestemunha','nomeMembroOficialTestemunha','h_seqCadastMembroOficialTestemunha','h_nomeMembroOficialTestemunha','myModalPesquisa','informacoesPesquisa', 'N');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                E-mail
                                <input type="text" id="emailMembroTestemunha" name="emailMembroTestemunha" value="">
                                <br>
                                <input type="hidden" id="h_seqCadastMembroOficialTestemunha">
                                <input type="hidden" id="h_nomeMembroOficialTestemunha">
                                <input type="hidden" id="h_seqCadastCompanheiroMembroOficialTestemunha">
                                <input type="hidden" id="h_nomeCompanheiroMembroOficialTestemunha">
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiMembrosTestemunhas();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="ata_oa_mensal_oficiais_testemunhas[]" id="ata_oa_mensal_oficiais_testemunhas" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                            <?php
                                            include_once 'model/ritoPassagemOGGMembroTestemunhaClass.php';
                                            $membros = new ritoPassagemOGGMembroTestemunha();
                                            $resultado = $membros->lista($dados[0]['idRitoPassagemOGG']);

                                            foreach ($resultado as $vetor) {
                                                ?>
                                            <option value="{'emailMembro' : '<?php echo $vetor['email']; ?>', 'tipoMembro' : <?php echo $vetor['tipo']; ?>, 'seqCadast': <?php echo $vetor['fk_seq_cadastMembro']; ?>}">[<?php echo retornaCodigoAfiliacao($vetor['fk_seq_cadastMembro'],$vetor['tipo']) ?>] <?php echo retornaNomeCompleto($vetor['fk_seq_cadastMembro']) ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Não Membros:</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipoNaoMembro" id="tipoNaoMembro">
                                    <option value="0">Selecione</option>
                                    <option value="1">Adulto</option>
                                    <option value="2">Jovem</option>
                                </select>
                                <input class="form-control" id="nomeNaoMembro" type="text" maxlength="100" value="" style="min-width: 320px">
                                 E-mail
                                <input type="text" id="emailNaoMembro" name="emailNaoMembro" value="">
                                        <!--<input type="checkbox" class="companheiro" id="companheiroMembroOficial" value=""> Companheiro-->
                                <br>
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiNaoMembrosConvocacaoRitualistica();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="ata_oa_mensal_nao_oficiais[]" id="ata_oa_mensal_nao_oficiais" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                            <?php
                                            include_once 'model/ritoPassagemOGGNaoMembroClass.php';
                                            $nao_membros = new ritoPassagemOGGNaoMembro();
                                            $resultado = $nao_membros->lista($dados[0]['idRitoPassagemOGG']);

                                            foreach ($resultado as $vetor) {
                                                ?>
                                            <option value="{'emailNaoMembro' : '<?php echo $vetor['email']; ?>', 'tipoNaoMembro' : <?php echo $vetor['tipo']; ?>, 'nomeNaoMembro': '<?php echo $vetor['nome']; ?>'}"><?php echo $vetor['nome'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-sm btn-success"
                                                data-toggle="tooltip" data-placement="left" title="Salvar" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');selecionaTudoMultipleSelect('ata_oa_mensal_oficiais_testemunhas');selecionaTudoMultipleSelect('ata_oa_mensal_nao_oficiais');">
                                                <i class="fa fa-check fa-white"></i>&nbsp; Salvar
                                        </button>
                                        &nbsp; <a href="?corpo=buscaRitosPassagem"
                                                class="btn btn-sm btn-danger" data-toggle="tooltip"
                                                data-placement="left" title="Cancelar e voltar!"> <i
                                                class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
                                </div>
                        </div>
                        </div>
		</form>

                </div>
            </div>
        </div>
	</div>
</div>
				<!-- Tabela Fim -->
				
				<!-- Conteúdo DE INCLUDE FIM -->
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            
            <div class="modal-body">
                <span id="resultadoPesquisa" onmousedown="bloquearControlCControlV()">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>