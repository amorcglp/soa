<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline(); ?>

<?php 

$arrObrigacao = array();
$arrObrigacao['nome'][0]							= "VILMA PEGORETTI";
$arrObrigacao['cod_afiliacao'][0]					= "41087";
$arrObrigacao['perfil'][0]							= "Titular";
$arrObrigacao['tipo'][0]							= "TOM";
$arrObrigacao['data'][0]							= "17/10/2014";
$arrObrigacao['oa'][0]								= "PRONAOS R+C JARAGUA DO SUL - AMORC - SC107";
$arrObrigacao['situacao'][0]						= "Stop Def.";
$arrObrigacao['remessa'][0]							= "Normal";
$arrObrigacao['lotegrc'][0]							= "03";
$arrObrigacao['lotegt'][0]							= "01";
$arrObrigacao['lotelrc'][0]							= "0";
$arrObrigacao['lotelt'][0]							= "0";
$arrObrigacao['telefone'][0]						= "(41)5555-5555";
$arrObrigacao['email'][0]							= "vilma@amorc.org.br";
$arrObrigacao['endereco'][0]						= "Rua das Andorinhas, 105 - Centro";

$arrObrigacao['nome'][1]							= "CLAUDIO FURLAN";
$arrObrigacao['cod_afiliacao'][1]					= "122227";
$arrObrigacao['perfil'][1]							= "Comp.";
$arrObrigacao['tipo'][1]							= "RC";
$arrObrigacao['data'][1]							= "26/02/2015";
$arrObrigacao['oa'][1]								= "LOJA R+C SAO PAULO-AMORC - SP101";
$arrObrigacao['situacao'][1]						= "Inativo";
$arrObrigacao['remessa'][1]							= "Stop Ini.";
$arrObrigacao['lotegrc'][1]							= "04";
$arrObrigacao['lotegt'][1]							= "01";
$arrObrigacao['lotelrc'][1]							= "0";
$arrObrigacao['lotelt'][1]							= "0";
$arrObrigacao['telefone'][1]						= "(41)6666-6666";
$arrObrigacao['email'][1]							= "claudio@amorc.org.br";
$arrObrigacao['endereco'][1]						= "Rua dos Papagaios, 8989 - Água Verde";

$arrObrigacao['nome'][2]							= "JOSE RAMOS DE ASSUMPÇÃO";
$arrObrigacao['cod_afiliacao'][2]					= "132011";
$arrObrigacao['perfil'][2]							= "Titular";
$arrObrigacao['tipo'][2]							= "TOM";
$arrObrigacao['data'][2]							= "17/12/2014";
$arrObrigacao['oa'][2]								= "LOJA R+C CAMPINAS - AMORC - SP301";
$arrObrigacao['situacao'][2]						= "Ativo";
$arrObrigacao['remessa'][2]							= "Normal";
$arrObrigacao['lotegrc'][2]							= "02";
$arrObrigacao['lotegt'][2]							= "01";
$arrObrigacao['lotelrc'][2]							= "0";
$arrObrigacao['lotelt'][2]							= "0";
$arrObrigacao['telefone'][2]						= "(41)7777-7777";
$arrObrigacao['email'][2]							= "jose@amorc.org.br";
$arrObrigacao['endereco'][2]						= "Rua dos Beija-flor, 7283 - Nª Senhora de Fátima";

$arrObrigacao['nome'][3]							= "DAMIANA SANTOS CABRAL";
$arrObrigacao['cod_afiliacao'][3]					= "144575";
$arrObrigacao['perfil'][3]							= "Comp.";
$arrObrigacao['tipo'][3]							= "RC";
$arrObrigacao['data'][3]							= "17/04/2015";
$arrObrigacao['oa'][3]								= "LOJA R+C BELEM-AMORC - PA101";
$arrObrigacao['situacao'][3]						= "Inativo";
$arrObrigacao['remessa'][3]							= "Stop Obri.";
$arrObrigacao['lotegrc'][3]							= "03";
$arrObrigacao['lotegt'][3]							= "01";
$arrObrigacao['lotelrc'][3]							= "0";
$arrObrigacao['lotelt'][3]							= "0";
$arrObrigacao['telefone'][3]						= "(41)8888-8888";
$arrObrigacao['email'][3]							= "damiana@amorc.org.br";
$arrObrigacao['endereco'][3]						= "Rua das Gaivotas, 1900 - Bacacheri";

$arrObrigacao['nome'][4]							= "ADRIANA WESTPHALEN VESCIA";
$arrObrigacao['cod_afiliacao'][4]					= "333102";
$arrObrigacao['perfil'][4]							= "Titular";
$arrObrigacao['tipo'][4]							= "TOM";
$arrObrigacao['data'][4]							= "05/11/2014";
$arrObrigacao['oa'][4]								= "LOJA R+C PORTO ALEGRE-AMORC - RS101";
$arrObrigacao['situacao'][4]						= "Ativo";
$arrObrigacao['remessa'][4]							= "Normal";
$arrObrigacao['lotegrc'][4]							= "04";
$arrObrigacao['lotegt'][4]							= "01";
$arrObrigacao['lotelrc'][4]							= "0";
$arrObrigacao['lotelt'][4]							= "0";
$arrObrigacao['telefone'][4]						= "(41)9999-9999";
$arrObrigacao['email'][4]							= "adriana@amorc.org.br";
$arrObrigacao['endereco'][4]						= "Rua dos Quero-quero, 277 - São Vicente";

?>
			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Consulta de Membros Iniciantes</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>
	                        <li>
	                            <a href="index.html">Consulta</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Membros Iniciantes</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
	            
	            <!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Consulta de Membros Iniciantes</h5>
									<div class="ibox-tools">
										<!--  
										<a class="btn btn-xs btn-primary" href="?opcao=oficial_form.php">
											<i class="fa fa-plus"></i> Novo Oficial
										</a>
										-->
									</div>
								</div>
								<div class="ibox-content">
									<table class="table table-striped table-bordered table-hover dataTables-example" >
										<thead>
											<tr>
												<th>Nome.</th>
												<th>Cód. Afiliação</th>
												<th><center>Perfil</center></th>
												<th><center>Tipo</center></th>
												<th><center>Admissão</center></th>
												<th><center>Lote R+C</center></th>
												<th><center>Endereço</center></th>
												<th><center>Telefone</center></th>
												<th><center>E-Mail</center></th>
												<th><center>Ações</center></th>
											</tr>
										</thead>
										<tbody>
											<?php if(count($arrObrigacao)>0)
											  		{ 
												  		for($i=0;$i<count($arrObrigacao['nome']);$i++){
													  	?>
													 <tr>
														<td>
															<?php echo $arrObrigacao['nome'][$i];?>
														</td>
														<td>
															<?php echo $arrObrigacao['cod_afiliacao'][$i];?>
														</td>
														<td>
															<?php echo $arrObrigacao['perfil'][$i];?>
														</td>
														<td>
															<?php echo utf8_encode($arrObrigacao['tipo'][$i]);?>
														</td>
														<td>
															<?php echo $arrObrigacao['data'][$i];?>
														</td>
														<td>
															<center>
															<?php echo $arrObrigacao['lotegrc'][$i];?>
															</center>
														</td>
														<td>
															<?php echo substr($arrObrigacao['endereco'][$i],0,7);?>...
															<i class="fa fa-eye-slash"></i>
														</td>
														<td>
															<?php echo substr($arrObrigacao['telefone'][$i],0,9);?>...
															<i class="fa fa-eye-slash"></i>
														</td>
														<td>
															<?php echo $arrObrigacao['email'][$i];?>
														</td>
														<td>
															<center>
																<button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
								                                    <center><i class="fa fa-info-circle fa-white"></i></center>
								                                </button>
								                                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
								                                    <center><i class="fa fa-envelope-o fa-white"></i></center>
								                                </button>
							                                </center>
														</td>
													</tr>
												<?php } ?>
											<?php } ?>
										</tbody>
										<tfoot>
											<!--
											<tr>
												<th>Rendering engine</th>
												<th>Browser</th>
												<th>Platform(s)</th>
												<th>Engine version</th>
												<th>CSS grade</th>
											</tr>
											-->
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->

			
		
		<?php if(count($arrObrigacao)>0){ ?>
			<?php for($i=0;$i<count($arrObrigacao['nome']);$i++){ ?>
		<div class="modal hide fade" id="mySee<?php echo $i;?>" style="">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">X</button>
				<h3>Obrigação Ritualística</h3>
			</div>
			<div class="modal-body">
				<table cellpadding="10">
				<tbody>
				<tr>
				<td width="35%"><b>Nome:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['nome'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Cód. Afiliação:</b></td><td> <span><?php echo $arrObrigacao['cod_afiliacao'][$i];?></span></td>
				</tr>
				<tr>
				<td><b>Perfil:</b></td><td> <span><?php echo $arrObrigacao['perfil'][$i];?></span></td>
				</tr>
				<tr>
				<td><b>Tipo:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['tipo'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Admissão:</b></td><td> <span><?php echo $arrObrigacao['data'][$i];?></span></td>
				</tr>
				<tr>
				<td><b>Organismo Afiliado:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['oa'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Sit. Cadastral:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['situacao'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Sit. Remessa:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['remessa'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Quitação RC:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['data'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Lote/Grau RC:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['lotegrc'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Lote limite RC:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['lotelrc'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Quitação TOM:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['data'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Lote/Grau TOM:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['lotegt'][$i]);?></span></td>
				</tr>
				<tr>
				<td><b>Lote limite TOM:</b></td><td> <span><?php echo utf8_encode($arrObrigacao['lotelt'][$i]);?></span></td>
				</tr>
				</tbody>	
				</table>
			</div>
			<input type="hidden" name="idinscricao" id="idinscricao">
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Fechar</a>
			</div>
		</div>
		<?php } ?>
	<?php } ?>
	