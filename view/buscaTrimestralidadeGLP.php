<?php
include_once 'model/trimestralidadeClass.php';
$t = new trimestralidade();

$idTrimestralidade = "";
$dataTrimestralidade = "";
$valorTrimestralidade = "";
$resultado = $t->listaTrimestralidade();
if ($resultado) {
    foreach ($resultado as $vetor) {
        $idTrimestralidade = $vetor['idTrimestralidade'];
        $dataTrimestralidade = substr($vetor['dataTrimestralidade'], 8, 2) . "/" . substr($vetor['dataTrimestralidade'], 5, 2) . "/" . substr($vetor['dataTrimestralidade'], 0, 4);
        $valorTrimestralidade = $vetor['valorTrimestralidade'];
    }
}
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php
$salvo = isset($_REQUEST['salvo']) ? $_REQUEST['salvo'] : null;
if ($salvo == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Valor da Trimestralidade salvo com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php } ?>
<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Trimestralidade GLP</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaSaldoInicial">Financeiro</a>
            </li>
            <li class="active">
                <strong><a>Trimestralidade GLP</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Trimestralidade GLP</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="saldoInicial" class="form-horizontal" method="post">
                    	<!--  
                        <div class="alert alert-warning alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link">Atenção:</a> Instruções para preenchimento do <a class="alert-link">Trimestralidade</a>.<br>
                            Este campo inicializa o módulo financeiro e <a class="alert-link">não é possível alteração desta informação</a>.<br>
                            Insira no campo SALDO na data de <a class="alert-link">01/01/2015</a> os valores contabilizados como SALDO ATUAL ao final do ano anterior.
                        </div>
						-->
                            <div class="form-group" id="datapicker_ata">
                                <label class="col-sm-3 control-label">Data da Atualização:</label>
                                <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="dataTrimestralidade" maxlength="10" id="dataTrimestralidade" type="text" value="<?php echo $dataTrimestralidade;?>" class="form-control" style="max-width: 102px">
                                </div>
                            </div>
                        

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Valor da Trimestralidade:</label>
                            <div class="col-sm-9 input-group" style="padding: 0px 0px 0px 15px">
                                <input type="text" class="form-control currency" id="valorTrimestralidade" name="valorTrimestralidade" value="<?php echo $valorTrimestralidade; ?>"  style="max-width: 150px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">50% do Trimestre:</label>
                            <div class="col-sm-9 input-group" style="padding: 0px 0px 0px 15px">
                                <input type="text" class="form-control currency" id="valor" name="valor" value="<?php echo number_format(((float) $valorTrimestralidade)/2,2,",","."); ?>" readonly  style="max-width: 150px">
                            </div>
                        </div>
                        <input type="hidden" name="idTrimestralidade" id="idTrimestralidade" value="<?php echo $idTrimestralidade; ?>">
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast']; ?>">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
								<a href="#" class="btn btn-sm btn-success" data-toggle="tooltip"
									data-placement="left" title="Salvar"
									onclick="salvarTrimestralidade();"> <i
									class="fa fa-check fa-white"></i>&nbsp; Salvar </a> &nbsp;
                                <a href="?corpo=buscaTrimestralidadeGLP" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>	