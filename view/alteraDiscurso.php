<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Alteração de Discurso</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php?corpo=buscaDiscurso">Discursos</a>
            </li>
            <li class="active">
                <strong>
                    <a>Alteração de Discurso</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<?php
include_once 'controller/discursoController.php';
include_once 'controller/documentoController.php';

$idDiscurso = isset($_REQUEST['iddiscurso']) ? $_REQUEST['iddiscurso'] : null;

$discurso = new DiscursoController();
$documento = new documentoController();

if($idDiscurso==null) {
    echo "<script type=\"text/javascript\">window.location='painelDeControle?corpo=buscaDiscurso'</script>";
}

?>
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Alteração de Discurso para Convocação Ritualística</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white; right: auto" href="?corpo=buscaDiscurso">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <?php
                           $resultado = $discurso->listaDiscursoId($idDiscurso);
                           
                           if($resultado) {
                               foreach ($resultado as $vetor) {
                                   if($vetor['fk_seqCadast'] == $_SESSION['seqCadast']) {
                        ?>
                        <input type="hidden" name="idDiscurso" id="idDiscurso" value="<?php echo $idDiscurso; ?>">
                        <input type="hidden" name="qtdeImpressao" id="qtdeImpressao" value="<?php echo $vetor['qtdeImpressao']; ?>">
                        <input type="hidden" name="idDocumento" id="idDocumento" value="<?php echo($vetor['fk_idDocumento']==""?"null":$vetor['fk_idDocumento']); ?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Número da Mensagem</label>
                            <div class="col-sm-10">
                                <input type="text" name="numeroMensagem" id="numeroMensagem" class="form-control" style="max-width: 100px" value="<?php echo $vetor['numeroMensagem']; ?>" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Autor da Mensagem</label>
                            <div class="col-sm-10">
                                <input type="text" name="autorMensagem" id="autorMensagem" class="form-control" style="max-width: 200px" value="<?php echo $vetor['autorMensagem']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Título da Mensagem</label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloMensagem" id="tituloMensagem" class="form-control" style="max-width: 200px" value="<?php echo $vetor['tituloMensagem']; ?>" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Tipo</label>
                            <div class="col-sm-10">
                                <select name="tipo" id="tipo">
                                    <option value="0">Selecione</option>
                                    <option value="1" <?php if($vetor['tipo']==1){ echo "selected";}?>>R+C</option>
                                    <option value="2" <?php if($vetor['tipo']==2){ echo "selected";}?>>OGG</option>
                                </select>    
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_discurso">
                            <label class="col-sm-2 control-label">Data da Criação do Discurso</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataCriacao" onBlur="validaData(this.value, this);" maxlength="10" id="dataCriacao" type="text" class="form-control data" value="<?php if($vetor['dataCriacao'] != '0000-00-00'){ echo substr($vetor['dataCriacao'], 8, 2) . "/" . substr($vetor['dataCriacao'], 5, 2) . "/" . substr($vetor['dataCriacao'], 0, 4); }?>" style="max-width: 102px">
                                <div hidden id="data_invalida_dataCriacao" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Tem experimento?</label>
                            <div class="col-sm-10">
                                <select name="temExperimento" id="temExperimento" required="required">
                                    <?php
                                        if($vetor['temExperimento'] == 1) {
                                    ?>
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                    <?php
                                        }
                                        else {
                                    ?>
                                    <option value="0">Não</option>
                                    <option value="1">Sim</option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="col-sm-10">
                                <p style="font-size: smaller; color: #FA8072;">
                                    * Preenchimento obrigatório
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Alterar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location='painelDeControle.php?corpo=buscaDiscurso'">
                            </div>
                        </div>
                        <?php
                                   }
                                   else {
                                       echo "<script type=\"text/javascript\">alert(\"Você não tem permissão para alterar este discurso!\");";
                                       echo "window.location='painelDeControle.php?corpo=buscaDiscurso'</script>";
                                   }
                               }
                           }
                        ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>



