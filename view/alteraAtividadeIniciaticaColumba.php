<?php
include_once("controller/atividadeIniciaticaColumbaController.php");
require_once ("model/criaSessaoClass.php");

$aicc = new atividadeIniciaticaColumbaController();
$dados = $aicc->buscaAtividadeIniciaticaColumbaPorId($_GET['idAtividadeIniciaticaColumba']);
?>

<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Columbas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeIniciaticaColumba">Columbas Cadastradas</a>
            </li>
            <li class="active">
                <a>Alteração de Dados de Columba</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Alteração de dados da Columba</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciaticaColumba">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="form_columba" onsubmit="return verificaCamposPreenchidosColumbaSubmit();" id="form_columba" class="form-horizontal" enctype="multipart/form-data" method="post" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="10000000" />
                        <div class="form-group form-inline"><label class="col-sm-3 control-label">Organismo Afiliado: </label>
                          <div class="col-sm-9">
                              <div class="form-control-static" style="padding-top: 5px; padding-bottom: 10px;">
                                  <?php
                                  $nomeOrganismo = retornaNomeCompletoOrganismoAfiliado($dados->getFkIdOrganismoAfiliado());
                                  echo $nomeOrganismo;
                                  ?>
                              </div>
                              <input type="hidden" id="fk_idOrganismoAfiliado" name="fk_idOrganismoAfiliado" value="<?php echo $dados->getFkIdOrganismoAfiliado(); ?>">
                          </div>
                        </div>

                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Columba:</label>
                            <div class="col-sm-9">
                                <?php echo "[" . $dados->getCodAfiliacaoAtividadeIniciaticaColumba() . "] " . $dados->getNomeAtividadeIniciaticaColumba(); ?>
                            </div>
                        </div>

                        <div class="form-group" id="anotacoesAtivInic">
                            <label class="col-sm-3 control-label">Descrição: </label>
                            <div class="col-sm-9">
                                <div style="border: #ccc solid 1px; max-width: 800px">
                                    <div class="mail-text h-200">
                                        <textarea class="summernote" id="descricaoAtividadeIniciaticaColumba" name="descricaoAtividadeIniciaticaColumba"><?php echo $dados->getDescricaoAtividadeIniciaticaColumba();?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br>
<!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Endereço: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" value="<?php echo $dados->getEnderecoAtividadeIniciaticaColumba(); ?>" name="enderecoAtividadeIniciaticaColumba" id="enderecoAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CEP: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" value="<?php echo $dados->getCepAtividadeIniciaticaColumba(); ?>" name="cepAtividadeIniciaticaColumba" id="cepAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Naturalidade: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" value="<?php echo $dados->getNaturalidadeAtividadeIniciaticaColumba(); ?>" name="naturalidadeAtividadeIniciaticaColumba" id="naturalidadeAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_atividade_iniciatica">
                            <label class="col-sm-3 control-label">Data de Nascimento: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input value="<?php echo $dados->getDataNascimentoAtividadeIniciaticaColumba(); ?>" onkeypress="valida_data(this,'dataNascimentoAtividadeIniciaticaColumba')" maxlength="10" name="dataNascimentoAtividadeIniciaticaColumba" id="dataNascimentoAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataNascimentoAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Escola que frequenta: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" value="<?php echo $dados->getEscolaAtividadeIniciaticaColumba(); ?>" name="escolaAtividadeIniciaticaColumba" id="escolaAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" value="<?php echo $dados->getEmailAtividadeIniciaticaColumba(); ?>" name="emailAtividadeIniciaticaColumba" id="emailAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Endereço de Facebook, blog ou site pessoal(opcional): </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" value="<?php echo $dados->getRedeSocialAtividadeIniciaticaColumba(); ?>" name="redeSocialAtividadeIniciaticaColumba" id="redeSocialAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group datapicker">
                            <label class="col-sm-3 control-label">Data de Admissão: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input value="<?php echo $dados->getDataAdmissaoAtividadeIniciaticaColumba(); ?>" onkeypress="valida_data(this,'dataAdmissaoAtividadeIniciaticaColumba')" maxlength="10" name="dataAdmissaoAtividadeIniciaticaColumba" id="dataAdmissaoAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataAdmissaoAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group datapicker">
                            <label class="col-sm-3 control-label">Data de Instalação: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input value="<?php echo $dados->getDataInstalacaoAtividadeIniciaticaColumba(); ?>" onkeypress="valida_data(this,'dataInstalacaoAtividadeIniciaticaColumba')" maxlength="10" name="dataInstalacaoAtividadeIniciaticaColumba" id="dataInstalacaoAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataInstalacaoAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group datapicker">
                            <label class="col-sm-3 control-label">Ativa até: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input value="<?php echo $dados->getDataAtivaAteAtividadeIniciaticaColumba(); ?>" onkeypress="valida_data(this,'dataAtivaAteAtividadeIniciaticaColumba')" maxlength="10" name="dataAtivaAteAtividadeIniciaticaColumba" id="dataAtivaAteAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataAtivaAteAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Pai ou tutor: </label>
                            <div class="col-sm-9">
                                <input value="<?php if($dados->getCodAfiliacaoTutorAtividadeIniciaticaColumba() != 0){ echo $dados->getCodAfiliacaoTutorAtividadeIniciaticaColumba();} ?>" placeholder="Código" class="form-control" id="codAfiliacaoTutorAtividadeIniciaticaColumba" name="codAfiliacaoTutorAtividadeIniciaticaColumba" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input value="<?php echo $dados->getNomeTutorAtividadeIniciaticaColumba(); ?>" placeholder="Nome Completo" class="form-control" id="nomeTutorAtividadeIniciaticaColumba" name="nomeTutorAtividadeIniciaticaColumba" type="text" maxlength="100" value="" style="min-width: 320px">
                                <small>Por favor, caso preencha esse campo informe o nome completo</small>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Mãe ou tutora: </label>
                            <div class="col-sm-9">
                                <input value="<?php if($dados->getCodAfiliacaoTutoraAtividadeIniciaticaColumba() != 0){ echo $dados->getCodAfiliacaoTutoraAtividadeIniciaticaColumba();} ?>" placeholder="Código" class="form-control" id="codAfiliacaoTutoraAtividadeIniciaticaColumba" name="codAfiliacaoTutoraAtividadeIniciaticaColumba" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input value="<?php echo $dados->getNomeTutoraAtividadeIniciaticaColumba(); ?>" placeholder="Nome Completo" class="form-control" id="nomeTutoraAtividadeIniciaticaColumba" name="nomeTutoraAtividadeIniciaticaColumba" type="text" maxlength="100" value="" style="min-width: 320px">
                                <small>Por favor, caso preencha esse campo informe o nome completo</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Qual dos dois é responsável pleno? </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="responsavelPlenoAtividadeIniciaticaColumba" id="responsavelPlenoAtividadeIniciaticaColumba" style="max-width: 320px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1" <?php if ($dados->getResponsavelPlenoAtividadeIniciaticaColumba()==1){ echo 'selected';};?>>Pai ou tutor</option>
                                    <option value="2" <?php if ($dados->getResponsavelPlenoAtividadeIniciaticaColumba()==2){ echo 'selected';};?>>Mãe ou tutora</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="seqCadastResponsavelPlenoAtividadeIniciaticaColumba" name="seqCadastResponsavelPlenoAtividadeIniciaticaColumba" value="999999">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail do Responsável Legal: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" value="<?php echo $dados->getEmailResponsavelAtividadeIniciaticaColumba(); ?>" name="emailResponsavelAtividadeIniciaticaColumba" id="emailResponsavelAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-inline" id="columbaRgAnexo">
                            <label class="col-sm-3 control-label">
                                Anexar RG(caso possua): <br>
                                <?php if($dados->getRgAnexoAtividadeIniciaticaColumba() != ""){ ?>
                                    <small class="text-navy">
                                        <a href="<?php echo $dados->getRgAnexoAtividadeIniciaticaColumba(); ?>" target="_blank">Clique para visulizar anexo atual.</a>
                                    </small>
                                <?php } ?>
                            </label>
                            <div class="col-sm-9">
                                <input name="RgAnexoAtividadeIniciaticaColumba" id="RgAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="columbaRgAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div><br>
                        </div>
                        <div class="form-group form-inline" id="columbaCertidaoNascimentoAnexo">
                            <label class="col-sm-3 control-label">
                                Anexar Certidão de Nascimento: <br>
                                <?php if($dados->getCertidaoNascimentoAnexoAtividadeIniciaticaColumba() != ""){ ?>
                                    <small class="text-navy">
                                        <a href="<?php echo $dados->getCertidaoNascimentoAnexoAtividadeIniciaticaColumba(); ?>" target="_blank">Clique para visulizar anexo atual.</a>
                                    </small>
                                <?php } ?>
                            </label>
                            <div class="col-sm-9">
                                <input name="certidaoNascimentoAnexoAtividadeIniciaticaColumba" id="certidaoNascimentoAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="columbaCertidaoNascimentoAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <div class="form-group form-inline" id="fotoCorpoInteirAnexo">
                            <label class="col-sm-3 control-label">
                                Anexar Foto de Corpo de Inteiro: <br>
                                <?php if($dados->getFotoCorpoInteiroAnexoAtividadeIniciaticaColumba() != ""){ ?>
                                    <small class="text-navy">
                                        <a href="<?php echo $dados->getFotoCorpoInteiroAnexoAtividadeIniciaticaColumba(); ?>" target="_blank">Clique para visulizar anexo atual.</a>
                                    </small>
                                <?php } ?>
                            </label>
                            <div class="col-sm-9">
                                <input name="fotoCorpoInteiroAnexoAtividadeIniciaticaColumba" id="fotoCorpoInteiroAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="fotoCorpoInteirAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <div class="form-group form-inline" id="cartaoMembroResponsavelAnexo">
                            <label class="col-sm-3 control-label">
                                Cartão de Membro do Responsável: <br>
                                <?php if($dados->getCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba() != ""){ ?>
                                    <small class="text-navy">
                                        <a href="<?php echo $dados->getCartaoMembroResponsavelAnexoAtividadeIniciaticaColumba(); ?>" target="_blank">Clique para visulizar anexo atual.</a>
                                    </small>
                                <?php } ?>
                            </label>
                            <div class="col-sm-9">
                                <input name="cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba" id="cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="cartaoMembroResponsavelAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <hr>
-->
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input type="hidden" id="idAtividadeIniciaticaColumba" name="idAtividadeIniciaticaColumba" value="<?php echo $_GET['idAtividadeIniciaticaColumba']; ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar"" />
                                <a class="btn btn-white" href="?corpo=buscaAtividadeIniciaticaColumba" id="cancelar" name="cancelar">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo de Modal Início -->

<!-- Modal Cadastrar Tipo de Escritura Início -->
<div class="modal inmodal fade" id="modalTipoDeEscritura" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Cadastrar Novo Tipo de Escritura</h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label class="col-sm-2 control-label">Tipo de Escritura: </label>
                        <div class="col-sm-10">
                            <input type="text" name="tipoEscritura" id="tipoEscritura" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12" id="alertTipoDeEscritura">
                        <div class="alert alert-info" style="text-align: justify">
                            <center>
                            Contribua com um novo <a class="alert-link">Tipo de Escritura</a>.
                            O qual será visível por todos!
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal" onclick="trocaAlertTiposDeEscritura();">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="cadastraTipoDeEscritura();">Salvar nova opção</button>
            </div>
        </div>
    </div>
</div>

<!-- Conteúdo de Modal Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
