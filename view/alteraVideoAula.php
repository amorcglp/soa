<?php
include_once("controller/videoAulaController.php");

$v = new videoAulaController();
$dados = $v->buscaVideoAula($_GET['id']);
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gerenciamento de Vídeo Aulas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaGerenciamentoVideoAula">Gerenciamento de Vídeo Aulas</a>
            </li>
            <li class="active">
                <a>Edição de Video Aula</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição de Video Aula</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <form id="cadastro" method="POST" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idVideoAula" id="idVideoAula" value="<?php echo $_GET['id']; ?>" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Título</label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloVideoAula" id="tituloVideoAula" class="form-control" value="<?php echo $dados->getTitulo(); ?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Descrição</label>
                            <div class="col-sm-10">
                                <textarea rows="7" name="descricaoVideoAula" id="descricaoVideoAula" class="form-control" style="max-width: 500px"><?php echo $dados->getDescricao(); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-2 control-label">Data Inicial:</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataInicial" maxlength="10" id="dataInicial" type="text" class="form-control" style="max-width: 102px" value="<?php echo $dados->getDataInicial(); ?>"  required="required">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-2 control-label">Data Final:</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataFinal" maxlength="10" id="dataFinal" type="text" class="form-control" style="max-width: 102px" value="<?php echo $dados->getDataFinal(); ?>" required="required">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Link</label>
                            <div class="col-sm-10">
                                <input type="text" name="link" id="link" class="form-control" value="<?php echo $dados->getLink(); ?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <br>
                        <hr>
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>