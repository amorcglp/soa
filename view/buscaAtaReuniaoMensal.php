<?php

@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("controller/usuarioController.php");
$um = new Usuario();
require_once ("controller/organismoController.php");
$oc = new organismoController();
include_once("model/ataReuniaoMensalAssinadaClass.php");
$arma = new ataReuniaoMensalAssinada();

include_once('model/ticketItemFuncionalidadeClass.php');
$tif = new TicketItemFuncionalidade();

require_once 'model/usuarioClass.php';

include_once("model/ataReuniaoMensalClass.php");
$at = new ataReuniaoMensal();

$anexo = isset($_FILES['anexo']['name']) ? $_FILES['anexo']['name'] : null;

/**
 * Upload da Ata Assinada
 */
$ataAssinada = 0;
$extensaoNaoValida = 0;
if ($anexo != "") {
    if ($_FILES['anexo']['name'] != "") {
        if(substr_count($_FILES['anexo']['name'],".")==1)
        {
            $extensao = strstr($_FILES['anexo']['name'], '.');
            if ($extensao == ".pdf" ||
                $extensao == ".png" ||
                $extensao == ".jpg" ||
                $extensao == ".jpeg" ||
                $extensao == ".PDF" ||
                $extensao == ".PNG" ||
                $extensao == ".JPG" ||
                $extensao == ".JPEG"
            ) {
                $proximo_id = $arma->selecionaUltimoId();
                $caminho = "img/ata_reuniao_mensal/" . basename($_POST["idAtaUpload"] . ".ata.reuniao.mensal" . $proximo_id);
                $arma->setFkIdAtaReuniaoMensal($_POST["idAtaUpload"]);
                $arma->setCaminhoAtaReuniaoMensalAssinada($caminho);
                if ($arma->cadastroAtaReuniaoMensalAssinada()) {
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/ata_reuniao_mensal/';
                    $uploadfile = $uploaddir . basename($_POST["idAtaUpload"] . ".ata.reuniao.mensal" . $proximo_id);
                    if (move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile)) {
                        $ataAssinada = 1;
                    } else {
                        echo "<script type='text/javascript'>alert('Erro ao enviar a ata assinada!');</script>";
                    }
                }
            } else {
                $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}

$liberaGLP=false;
if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
    $liberaGLP = true;
}
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<?php
$salvo = isset($_REQUEST['salvo']) ? $_REQUEST['salvo'] : null;
if ($salvo == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Ata salva com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php } ?>
<?php
$jaCadastrado = isset($_REQUEST['jaCadastrado']) ? $_REQUEST['jaCadastrado'] : null;
if ($jaCadastrado == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Ata já cadastrada anteriormente!",
                type: "error",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if ($ataAssinada == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Ata Assinada enviada com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
$seqCadastSolicitanteToken=0;
$liberarEmMassa=0;
//Se tem cookie que libera token
$liberaTokenUsuarioIdItemFuncionalidade=false;
if (isset($_COOKIE['liberarTokenUsuario'])) {
    //Verificar se token é da ata de reunião mensal

    $u = new Usuario();
    $resultado = $u->retornaTokenIntegridade($_COOKIE['liberarTokenUsuario'],2);
    if($resultado)
    {
        foreach($resultado as $vetor)
        {
            $liberaTokenUsuarioIdItemFuncionalidade=$vetor['idItemFuncionalidade'];
            $liberarEmMassa = $vetor['liberarEmMassa'];
               
                
            //$timelife=$vetor['timelife'];
            //$timelifeExpira = $timelife + (60 * 60 );
            $date = new DateTime($vetor['dataInicial'].' 23:59:59');
            $timelife = $date->getTimestamp();
            $date2 = new DateTime($vetor['dataFinal'].' 23:59:59');
            $timelifeExpira = $date2->getTimestamp();
            $seqCadastSolicitanteToken=$vetor['seqCadast'];
        }
    }
    if($liberarEmMassa==1)
    {
        $bloqueia=false;
    } 
    ?>
<?php }

//Libera por login
$u = new Usuario();
$resultado77 = $u->retornaTokenPorLogin($_SESSION['loginUsuario'],date('Y-m-d'),2);
if($resultado77)
{
    foreach($resultado77 as $vetor77)
    {
        //echo "token:".$vetor77['liberarEmMassa'];
        $liberarEmMassa = $vetor77['liberarEmMassa'];
        if($vetor77['mes']==$mesAtual&&$vetor77['ano']==$anoAtual)
        {
            $liberaTokenUsuarioIdItemFuncionalidade=$vetor77['idItemFuncionalidade'];
            $liberarEmMassa = $vetor77['liberarEmMassa'];


            //$timelife=$vetor['timelife'];
            //$timelifeExpira = $timelife + (60 * 60 );
            $date = new DateTime($vetor77['dataInicial'].' 23:59:59');
            $timelife = $date->getTimestamp();
            $date2 = new DateTime($vetor77['dataFinal'].' 23:59:59');
            $timelifeExpira = $date2->getTimestamp();
            $seqCadastSolicitanteToken=$vetor77['seqCadast'];
        }
    }
    if($liberarEmMassa==1)
    {
        $bloqueia=false;
    }
}
//Fim libera por login

//Se for mes atual ou mes anterior liberar entradas

$mesAnteriorDaDataAtual=date('m', strtotime('-1 months', strtotime(date('Y')."-".date('m')."-01")));
$mesProximoDaDataAtual=date('m', strtotime('+1 months', strtotime(date('Y')."-".date('m')."-01")));

if(date('m')==1)
{
    $anoAnteriorDaDataAtual=$anoAtual-1;
}else{
    $anoAnteriorDaDataAtual=$anoAtual;
}
if(date('m')==12)
{
    $anoProximoDaDataAtual=$anoAtual+1;
}else{
    $anoProximoDaDataAtual=$anoAtual;
}

$liberaMesAtualOuAnterior=false;
if(
    ($_REQUEST['mesAtual']==date('m')&&$_REQUEST['anoAtual']==date('Y'))
    ||
    ($_REQUEST['mesAtual']==$mesAnteriorDaDataAtual&&$_REQUEST['anoAtual']==$anoAnteriorDaDataAtual)
)
{
    $liberaMesAtualOuAnterior=true;
}

if ($liberaTokenUsuarioIdItemFuncionalidade != false&&$seqCadastSolicitanteToken==$_SESSION['seqCadast']) {
    
    if($liberarEmMassa == 0)
    {    
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "O Token solicitado para a Ata Cod. <?php echo $liberaTokenUsuarioIdItemFuncionalidade;?> expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php 
    }else{
        ?>
        <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "O Token solicitado para todas as Atas expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
        <?php
    }
}
?>
<style>
    textarea {
        resize: none;
    }
</style>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Área de ATAs</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaReuniaoMensal">ATA</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaReuniaoMensal">Organismo Afiliado</a>
            </li>
            <li class="active">
                <strong>
                    <a>Reunião Mensal</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!-- Caminho de Migalhas Fim -->



<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Ata de Reunião Mensal do Organismo <?php echo $nomeOrganismoAfiliadoCompleto; ?></h5>
                    <div class="ibox-tools">
                        <?php //if($sessao->getValue("seqCadast") == 550110){ ?>
                        <?php if (in_array("1", $arrNivelUsuario)) { ?>
                            <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtaReuniaoMensal">
                                <i class="fa fa-plus"></i>Nova ATA
                            </a>
                        <?php } ?>
                        <?php //} ?>
                    </div>
                    <?php if ($liberaGLP) { ?>
                    <button class="btn btn-sm btn-danger" type="button" onclick="document.getElementById('idFuncionalidade').value='2';document.getElementById('idItemFuncionalidade').value='0';document.getElementById('idOrganismoAfiliadoLiberarToken').value='<?php echo $idOrga2;?>';" data-target="#mySeeToken" data-toggle="modal" data-placement="left">
                        <i class="fa fa-key"></i>
                    </button>
                    <?php }?> 
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                        <tr>
                            <th>Cód.</th>
                            <th>
                                <center>Mês/Ano Competência</center>
                            </th>
                            <th>
                                <center>Data da Reunião</center>
                            </th>
                            <!--
                            <th>
                                <center>Data da Entrega</center>
                            </th>-->
                            <th>
                                <center>Arquivos entregues</center>
                            </th>
                            <th>
                                <center>Data de entrega (Ass. Eletrônica) </center>
                            </th>
                            <th>
                                <center>Assinaturas</center>
                            </th>
                            <th>
                                <center>Ações</center>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //echo 'teste braz'.$_SESSION['seqCadast'];
                        $liberaGLP=false;
                        if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
                            $liberaGLP = true;
                        }
                        
                        include_once("controller/ataReuniaoMensalController.php");
                        
                        $armc = new ataReuniaoMensalController();
                        $resultado = $armc->listaAtaReuniaoMensal($idOrganismoAfiliado);
                        
                        if ($resultado) {
                            foreach ($resultado as $vetor) {

                                if(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA"))) {

                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }

                                    $resposta3 = $arma->listaAtaReuniaoMensalAssinada($vetor['idAtaReuniaoMensal']);
                                    if ($resposta3) {
                                        $total = count($resposta3);
                                    } else {
                                        $total = 0;
                                    }

                                    //Verificar se precisa bloquear registro
                                    $bloqueia = false;
                                    $data_ini = $vetor['anoCompetencia'] . "-" . $vetor['mesCompetencia'] . "-01";
                                    $data_end = date("Y-m-d");

                                    $meses = date_diffe($data_ini, $data_end);
                                    //$meses=7;

                                    if ($meses > 2) {
                                        $bloqueia = true;
                                    }
                                    if ($liberarEmMassa == 1) {
                                        $bloqueia = false;
                                    }
                                    //echo "liiiiiiiiberrrrrraaaaaaaa".$liberaEmMassa;
                                    //Amanhã
                                    $dataEntrega = substr($vetor['data'], 0, 10);
                                    $amanha = dataAmanha($dataEntrega);

                                    $naoEnviada = false;

                                    ?>
                                    <?php if ((strtoupper($vetor["siglaOrganismoAfiliado"]) == strtoupper($sessao->getValue("siglaOA"))) || (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2))) { ?>
                                        <tr>
                                            <td>
                                                <?php echo $vetor['idAtaReuniaoMensal']; ?>
                                            </td>
                                            <td>
                                                <center>
                                                    <?php echo $vetor['mesCompetencia'] . "/" . $vetor['anoCompetencia']; ?>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <?php echo substr($vetor['dataAtaReuniaoMensal'], 8, 2) . "/" . substr($vetor['dataAtaReuniaoMensal'], 5, 2) . "/" . substr($vetor['dataAtaReuniaoMensal'], 0, 4); ?>
                                                </center>
                                            </td>
                                            <!--
                                            <td>
                                                <center>
                                                    <?php //echo substr($vetor['data'], 8, 2) . "/" . substr($vetor['data'], 5, 2) . "/" . substr($vetor['data'], 0, 4); ?>
                                                    <?php //echo "<br>".$amanha;?>
                                                </center>
                                            </td>-->
                                            <td>
                                                <center>
                                            <span id="anexos<?php echo $vetor['idAtaReuniaoMensal']; ?>">
                                                <?php if ($total > 0) { ?>
                                                    <?php if (!$bloqueia || $liberaGLP||$liberarEmMassa==1||$liberaMesAtualOuAnterior || $liberaTokenUsuarioIdItemFuncionalidade == $vetor['idAtaReuniaoMensal']) { ?>
                                                        <button class="btn btn-info  dim" type="button"
                                                                onclick="listaUploadAtaReuniaoMensal('<?php echo $vetor['idAtaReuniaoMensal']; ?>')"
                                                                data-target="#mySeeListaUpload" data-toggle="modal"
                                                                data-placement="left">
                                                    <i class="fa fa-paste"></i>
                                                </button>
                                                    <?php } else { ?>
                                                        <?php if (verificaPrazo(date("Y-m-d"), $amanha)) { ?>
                                                            <?php if ($tif->verificaVinculoItemTicket(2, $vetor['idAtaReuniaoMensal'], $_SESSION['seqCadast'], 3)) { ?>
                                                                <button class="btn btn-sm btn-success" type="button"
                                                                        data-target="#mySeeIntegridadeTicketAberto"
                                                                        data-toggle="modal" data-placement="left">
                                                    <i class="fa fa-hourglass-start"></i>
                                                </button>
                                                            <?php } else { ?>
                                                                <button class="btn btn-sm btn-success" type="button"
                                                                        data-target="#mySeeIntegridade"
                                                                        data-toggle="modal" data-placement="left"
                                                                        onclick="document.getElementById('idFuncionalidadeTicket').value='2';document.getElementById('idItemFuncionalidadeTicket').value='<?php echo $vetor['idAtaReuniaoMensal']; ?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $vetor["idOrganismoAfiliado"]; ?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"]; ?>';document.getElementById('codigoItem').innerHTML='<?php echo $vetor['idAtaReuniaoMensal']; ?>';listaUploadAtaReuniaoMensalJaEntregue(<?php echo $vetor['idAtaReuniaoMensal']; ?>);">
                                                    <i class="fa fa-lock"></i>
                                                </button>
                                                            <?php } ?>
                                                        <?php } else {
                                                            echo "Documento assinado eletronicamente";
                                                            $naoEnviada = true;
                                                        }
                                                        ?>
                                                    <?php } ?>
                                                    <?php
                                                } else {
                                                    echo "Documento assinado eletronicamente";
                                                    $naoEnviada = true;
                                                } ?>
                                            </span>
                                                </center>
                                            </td>
                                            <td>
                                                <div id="pronto<?php echo $vetor['idAtaReuniaoMensal']; ?>">
                                                <center>
                                                    <?php if($vetor['entregue']==1){?>
                                                        <?php echo substr($vetor['dataEntrega'],8,2);?>/<?php echo substr($vetor['dataEntrega'],5,2);?>/<?php echo substr($vetor['dataEntrega'],0,4);?> às <?php echo substr($vetor['dataEntrega'],11,8);?>
                                                    <?php }else{?>
                                                        <b>Não Informado</b>
                                                    <?php }?>
                                                </center>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <center>
                                                        <?php if($vetor['entregueCompletamente']==1){?>
                                                            <?php echo substr($vetor['dataEntregouCompletamente'],8,2);?>/<?php echo substr($vetor['dataEntregouCompletamente'],5,2);?>/<?php echo substr($vetor['dataEntregouCompletamente'],0,4);?> às <?php echo substr($vetor['dataEntregouCompletamente'],11,8);?>
                                                        <?php }else{?>
                                                            <b>Faltam assinaturas</b>
                                                        <?php }?>
                                                    </center>
                                                </div>
                                            </td>
                                            <td>
                                                <?php
                                                $mestre = $at->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($vetor['idAtaReuniaoMensal'],365);
                                                $secretario = $at->buscarAssinaturasEmAtaReuniaoMensalPorDocumento($vetor['idAtaReuniaoMensal'],367);

                                                //Verificar se houve uma assinatura
                                                $primeiraAssinatura=false;
                                                if($mestre||$secretario){
                                                    $primeiraAssinatura=true;
                                                }


                                                //Montar Faltam assinar
                                                $arr=array();
                                                if(!$mestre)
                                                {
                                                    $arr[]="MESTRE DO ORGANISMO AFILIADO";
                                                }
                                                if(!$secretario)
                                                {
                                                    $arr[]="SECRETÁRIO DO ORGANISMO AFILIADO";
                                                }

                                                $oficiaisFaltamAssinar = implode(",",$arr);
                                                ?>
                                                <center>
                                                    <button type="button" class="btn btn-sm btn-info"
                                                            onclick="mostraDetalhesAtaReuniaoMensal('<?php echo $vetor['idAtaReuniaoMensal']; ?>');"
                                                            data-toggle="modal" data-target="#mySeeDetalhes"
                                                            data-toggle="tooltip" data-placement="left"
                                                            title="Detalhes da ATA de Reunião Mensal dos OA's">
                                                        <i class="fa fa-search-plus fa-white"></i>&nbsp;
                                                        Detalhes
                                                    </button>
                                                    <?php if($vetor['entregue']==0){?>
                                                    <?php if (in_array("2", $arrNivelUsuario)) { ?>
                                                        <span id="editar<?php echo $vetor['idAtaReuniaoMensal']; ?>">
                                                <?php if (!$bloqueia || $liberaGLP ||$liberarEmMassa==1|| !$primeiraAssinatura || $liberaTokenUsuarioIdItemFuncionalidade == $vetor['idAtaReuniaoMensal']) { ?>
                                                    <a href="?corpo=alteraAtaReuniaoMensal&idata_reuniao_mensal=<?php echo $vetor['idAtaReuniaoMensal']; ?>"
                                                       class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                       data-placement="left"
                                                       title="Editar ATA de Reunião Mensal dos OA's">
                                                    <i class="fa fa-edit fa-white"></i>&nbsp;
                                                        Editar
                                                </a>
                                                <?php } else { ?>
                                                    <?php if (verificaPrazo(date("Y-m-d"), $amanha)) { ?>
                                                        <?php if ($tif->verificaVinculoItemTicket(2, $vetor['idAtaReuniaoMensal'], $_SESSION['seqCadast'], 3)) { ?>
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                    data-target="#mySeeIntegridadeTicketAberto"
                                                                    data-toggle="modal" data-placement="left">
                                                    <i class="fa fa-hourglass-start"></i>Editar
                                                </button>
                                                        <?php } else { ?>
                                                            <button class="btn btn-sm btn-primary" type="button"
                                                                    data-target="#mySeeIntegridade" data-toggle="modal"
                                                                    data-placement="left"
                                                                    onclick="document.getElementById('idFuncionalidadeTicket').value='2';document.getElementById('idItemFuncionalidadeTicket').value='<?php echo $vetor['idAtaReuniaoMensal']; ?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $vetor["idOrganismoAfiliado"]; ?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"]; ?>';document.getElementById('codigoItem').innerHTML='<?php echo $vetor['idAtaReuniaoMensal']; ?>';">
                                                    <i class="fa fa-lock"></i>Editar
                                                </button>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <a href="?corpo=alteraAtaReuniaoMensal&idata_reuniao_mensal=<?php echo $vetor['idAtaReuniaoMensal']; ?>"
                                                           class="btn btn-sm btn-primary" data-toggle="tooltip"
                                                           data-placement="left"
                                                           title="Editar ATA de Reunião Mensal dos OA's">
                                                    <i class="fa fa-edit fa-white"></i>&nbsp;
                                                                Editar
                                                </a>
                                                    <?php } ?>
                                                <?php } ?>
                                            </span>
                                                    <?php } ?>
                                                    <?php }?>
                                                    <button type="button" class="btn btn-sm btn-success"
                                                            onclick="abrirPopUpAtaReuniaoMensal('<?php echo $vetor['idAtaReuniaoMensal']; ?>');"
                                                            data-toggle="tooltip" data-placement="left"
                                                            title="Imprimir ATA de Reunião Mensal dos OA's">
                                                        <i class="fa fa-print fa-white"></i>&nbsp;
                                                        Imprimir
                                                    </button>
                                                    <!--
                                                    <?php //if (in_array("4", $arrNivelUsuario)) { ?>
                                                        <span id="enviar<?php //echo $vetor['idAtaReuniaoMensal']; ?>">
                                                <?php //if (!$bloqueia || $liberaGLP || $liberaTokenUsuarioIdItemFuncionalidade == $vetor['idAtaReuniaoMensal']) { ?>
                                                    <button type="button" class="btn btn-sm btn-warning"
                                                            onclick="document.getElementById('idAtaUpload').value =<?php //echo $vetor['idAtaReuniaoMensal']; ?>"
                                                            data-target="#mySeeUpload" data-toggle="modal"
                                                            data-placement="left" title="Upload">
                                                    <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                                    Enviar
                                                </button>
                                                <?php //} else { ?>
                                                    <?php //if (verificaPrazo(date("Y-m-d"), $amanha) && $naoEnviada == false) { ?>
                                                        <?php //if ($tif->verificaVinculoItemTicket(2, $vetor['idAtaReuniaoMensal'], $_SESSION['seqCadast'], 3)) { ?>
                                                            <button class="btn btn-sm btn-warning" type="button"
                                                                    data-target="#mySeeIntegridadeTicketAberto"
                                                                    data-toggle="modal" data-placement="left">
                                                    <i class="fa fa-hourglass-start"></i>Enviar
                                                </button>
                                                        <?php //} else { ?>
                                                            <button class="btn btn-sm btn-warning" type="button"
                                                                    data-target="#mySeeIntegridade" data-toggle="modal"
                                                                    data-placement="left"
                                                                    onclick="document.getElementById('idFuncionalidadeTicket').value='2';document.getElementById('idItemFuncionalidadeTicket').value='<?php //echo $vetor['idAtaReuniaoMensal']; ?>';document.getElementById('idOrganismoAfiliado').value='<?php //echo $vetor["idOrganismoAfiliado"]; ?>';document.getElementById('seqCadastTicket').value='<?php //echo $_SESSION["seqCadast"]; ?>';document.getElementById('codigoItem').innerHTML='<?php //echo $vetor['idAtaReuniaoMensal']; ?>';">
                                                    <i class="fa fa-lock"></i>Enviar
                                                </button>
                                                        <?php //} ?>
                                                    <?php //} else { ?>
                                                        <button type="button" class="btn btn-sm btn-warning"
                                                                onclick="document.getElementById('idAtaUpload').value =<?php //echo $vetor['idAtaReuniaoMensal']; ?>"
                                                                data-target="#mySeeUpload" data-toggle="modal"
                                                                data-placement="left" title="Upload">
                                                    <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                                                Enviar
                                                </button>
                                                    <?php //} ?>
                                                <?php //} ?>
                                            </span>
                                                    <?php //} ?>
                                                    -->
                                                    <?php if($vetor['entregue']==0&&!$usuarioApenasLeitura){?>
                                                        <a class="btn btn-sm btn-warning" href="#" id="entregar<?php echo $vetor['idAtaReuniaoMensal']; ?>"
                                                           onclick="entregarAtaReuniaoMensal('<?php echo $vetor['idAtaReuniaoMensal']; ?>','<?php echo $_SESSION['seqCadast']; ?>')">
                                                            <i class="fa fa-location-arrow icon-white"></i>
                                                            Entregar com Ass. Eletrônica
                                                        </a>
                                                    <?php }?>
                                                    <?php
                                                    if($vetor['entregueCompletamente']==0) {
                                                        ?>
                                                        <button type="button"
                                                                id="faltaAssinar<?php echo $vetor['idAtaReuniaoMensal']; ?>"
                                                                class="btn btn-default" data-container="body"
                                                                data-toggle="popover" data-placement="top"
                                                                data-content="<?php if($mestre&&$secretario){?>Todos Assinaram. Processo de entrega finalizado!<?php } else { ?>Faltam assinar: <?php echo $oficiaisFaltamAssinar;}?>
                                                                " data-original-title="" title=""
                                                                aria-describedby="popover408083"
                                                                style="display: <?php if ($vetor['entregue'] == 0) { ?>none<?php } else { ?>block<?php } ?>">
                                                            <i class="fa fa-pencil"></i> Acompanhar assinaturas
                                                        </button>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <span class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Documento 100% entregue</span>
                                                        <?php
                                                    }
                                                        if ($liberaGLP) { ?>
                                                        <button class="btn btn-sm btn-danger" type="button"
                                                                onclick="document.getElementById('idFuncionalidade').value='2';document.getElementById('idItemFuncionalidade').value='<?php echo $vetor['idAtaReuniaoMensal']; ?>';"
                                                                data-target="#mySeeToken" data-toggle="modal"
                                                                data-placement="left">
                                                            <i class="fa fa-key"></i>
                                                        </button>
                                                    <?php } ?>

                                                </center>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <!--
                        <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                        </tr>
                        -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes da Ata de Reunião</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Ata:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="dataAtaReuniaoMensal"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora inicial:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="horaInicialAtaReuniaoMensal"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora do encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="horaFinalAtaReuniaoMensal"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número de membros:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="numeroMembrosAtaReuniaoMensal"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Membro que presidiu:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="nomePresidenteAtaReuniaoMensal"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Oficiais Administrativos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="oficiaisAdministrativos"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura do relatório mensal anterior com as seguintes restrições:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_um"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura da ata anterior:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_dois"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos Pendentes:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_quatro"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos novos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_cinco"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Expansão da Ordem:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_seis"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações da Suprema Grande Loja, da GLP e Grande Conselheiro(a):</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_oito"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações Gerais:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_nove"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Relatório das Comissões:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_tres"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Palavra livre:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_sete"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="topico_dez"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data Cadastro ou Atualização:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="data"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cadastrado por:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="cadastrado_por"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Atualizado por:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="atualizado_por"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Entrega (Ass. Eletrônica):</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="data_entrega"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Quem Entregou (Ass. Eletrônica):</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="quem_entregou"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Oficiais que assinaram esse documento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="oficiais_que_assinaram"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assinaturas:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="assinaturas"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Faltam assinar:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="faltam_assinar"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Código da Assinatura Eletrônica:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="numero_assinatura"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio da Ata Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <!--
                        <div class="alert alert-danger">
       <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
</div>
                        -->
                        <div class="col-sm-10">
                            <input name="anexo"
                                   id="anexo" type="file" />
                        </div>
                        <div id="anexoAlerta" class="col-sm-10"
                             style="color: #4F5B93; font-weight: normal; margin-top: 7px;">
                            &nbsp;&nbsp;&nbsp;Tamanho
                            máximo de 10MB.
                        </div>

                    </div>
                    <input type="hidden" id="idAtaUpload" name="idAtaUpload" value="0" />
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Enviar" onclick="document.ata.submit();" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Ata Assinada Manualmente</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUpload"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeIntegridade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-lock modal-icon"></i>
                <h4 class="modal-title">
                    Solicitação de Token
                    <input type="hidden" name="codigoItem" id="codigoItem">
                </h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUploadJaEntregue" style="background-color:white"></div>
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Lembre-se: você tem até 1 mês para enviar todos os Relatórios Assinados para a GLP.
                        <br />
                        Deseja ter acesso a esse item e regularizar as informações? Descreva o motivo e solicite seu Token.
                        <br />
                        Caso seja aprovada sua solicitação, você receberá uma notificação através do SOA e por e-mail.
                        <br />
                        Atenção: A liberação do Token depende diretamente da análise a ser realizada pelos colaboradores da GLP, portanto depende do horário de funcionamento da Grande Loja, por este motivo, pedimos um prazo de até 48h para as liberações.
                        <br />
                        <br />
                        <b>Motivo da liberação do token:</b>
                        <br />
                        <br />
                        <textarea rows="5" cols="50" name="motivoTicket" id="motivoTicket"></textarea>
                        <br />
                        <button type="button" class="btn btn-sm btn-info" data-dismiss="modal" onclick="abrirTicketParaLiberarFuncionalidade();">Abrir Ticket</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="seqCadastTicket" id="seqCadastTicket" value="<?php $_SESSION['seqCadast'];?>" />
                <input type="hidden" name="idFuncionalidadeTicket" id="idFuncionalidadeTicket" />
                <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" value=""/>
                <input type="hidden" name="idItemFuncionalidadeTicket" id="idItemFuncionalidadeTicket" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeToken" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-unlock modal-icon"></i>
                <h4 class="modal-title">Liberar Token para Usuário</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <table>
                            <tr>
                                <td>Organismo:</td>
                                <td><span id="nomeOrganismoToken"><?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);?></span> </td>
                            </tr>
                            <tr>
                                <td></td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Login no SOA:</td>
                                <td><input type="text" name="usuarioToken" id="usuarioToken" /></td>
                            </tr>
                            <tr>
                                <td><br>Data Inicial:</td>
                                <td><br><input type="text" class="data" name="dataInicial" id="dataInicial" value="<?php echo date('d/m/Y');?>"></td>
                            </tr>
                            <tr>
                                <td><br>Data Final:</td>
                                <td>
                                    <br><input type="text" class="data" name="dataFinal" id="dataFinal">
                                </td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Liberar em massa:</td>
                                <td>
                                    <br><input type="checkbox" name="liberarEmMassa" id="liberarEmMassa" onclick="zerarMesAno();">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <br>
                                    <input type="hidden" name="mesToken" id="mesToken" value="0" />
                                    <input type="hidden" name="anoToken" id="anoToken" value="0" />
                                    <input type="hidden" name="idFuncionalidade" id="idFuncionalidade" />
                                    <input type="hidden" name="idItemFuncionalidade" id="idItemFuncionalidade" />
                                    <input type="hidden" name="idOrganismoAfiliadoLiberarToken" id="idOrganismoAfiliadoLiberarToken" value=""/>
                                    <button class="btn btn-sm btn-danger" type="button" onclick="liberarTokenUsuario();">
                                        <i class="fa fa-key"></i>Liberar
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeIntegridadeTicketAberto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-hourglass-start modal-icon"></i>
                <h4 class="modal-title">Ticket já está aberto para esse item aguarde uma resposta</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Você já abriu um ticket para esse item, aguarde resposta da GLP. Enquanto a GLP não liberar você não tem acesso a ele. Caso a GLP libere o acesso a esse item, você receberá um e-mail com o token para alteração.
                        <br />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
modalNotificacao($sessao->getValue("seqCadast"));
?>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->