<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades das Columbas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaAtividadesColumbas">Atividades das Columbas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de Cadastro de Atividades das Columbas Atuantes do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadesColumbas">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="formulario" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onsubmit="return validaCadastroAtividadeColumba();">
                        <div class="row">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2" required="required">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>  
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Columba Atuante:</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="codAfiliacaoColumba" id="codAfiliacaoColumba" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" name="nomeColumba" id="nomeColumba" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoColumba','nomeColumba','codAfiliacaoColumba','nomeColumba','h_seqCadastColumba','h_nomeColumba','myModalPesquisa','informacoesPesquisa','S');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Digite o código de afiliação do membro e no outro campo acima o primeiro nome e pressione TAB
                                        <!--<input type="checkbox" value="" name="companheiroMembroPresidente" id="companheiroMembroPresidente"> Companheiro-->
                                   
                            </div>
                        </div>
                        <input type="hidden" id="tipo" name="tipo" value="2" >
                        <input type="hidden" name="h_seqCadastColumba" id="h_seqCadastColumba">
                        <input type="hidden" name="h_nomeColumba" id="h_nomeColumba">    
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Trimestre de Competência:</label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="trimestre" id="trimestre" style="max-width: 300px" required="required">
                                    <option value="0">Selecione</option>
                                    <option value="1">1º</option> 
                                    <option value="2">2º</option> 
                                    <option value="3">3º</option> 
                                    <option value="4">4º</option> 
                                </select>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano de Competência: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="ano" name="ano" type="text" value="" style="max-width: 86px" required="required">
                                (Ex. Referente à 2015, então preencha 2015)
                            </div>
                        </div>                         
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Pais ativos no OA:</label>
                            <div class="col-sm-9">
                                <input type="radio" name="paisAtivosOA" id="sim" value="1" checked>Sim
                                <input type="radio" name="paisAtivosOA" id="nao" value="2">Não
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Reuniões: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="reunioes" name="reunioes" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Ensaios: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="ensaios" name="ensaios" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Iniciações de Loja/Capítulo: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="iniciacoesLojaCapitulo" name="iniciacoesLojaCapitulo" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Aposições de Nome: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="aposicaoNome" name="aposicaoNome" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Casamentos: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="casamento" name="casamento" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Convocações Ritualísticas: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="convocacoesRitualisticas" name="convocacoesRitualisticas" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Iniciações de Graus: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="iniciacoesGraus" name="iniciacoesGraus" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Instalações: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="instalacao" name="instalacao" type="text" value="" style="max-width: 43px">
                            </div>
                        </div> 
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Avaliação:</label>
                            <div class="col-sm-9">
                                <select name="avaliacaoCoordenadora" id="avaliacaoCoordenadora" class="form-control col-sm-3">
                                    <option value="0">Selecione</option>
                                    <option value="1">Regular</option> 
                                    <option value="2">Bom</option> 
                                    <option value="3">Ótima</option> 
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Comentários da Orientadora:</label>
                            <div class="col-sm-9">
                                <textarea rows="5" cols="50" name="comentarios" id="comentarios"></textarea>
                            </div>
                        </div>    
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Salvar"
									\>
									<i class="fa fa-check fa-white"></i>&nbsp; Salvar
								</button>
								&nbsp; <a href="?corpo=buscaAtividadesColumbas"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
					</form>
                    
                </div>
            </div>
        </div>
	</div>
</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeLoadingAjax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
            <div class="cssload-box-loading" style="margin: 30% 50% 50% 50%;">
			</div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	