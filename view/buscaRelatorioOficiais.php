<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório de Oficiais</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Relatório</a>
            </li>
            <li class="active">
                <strong><a>Oficiais</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Relatório de Oficiais</h5>
                </div>
                <div class="ibox-content">
                    <form name="extratoFinanceiro" class="form-horizontal" method="post"  onSubmit="" >
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Região:</label>
                            <div class="col-sm-9">
                                <select name="fk_idRegiaoRosacruz" id="fk_idRegiaoOrdemRosacruz" style="width:350px;" onchange="carregaOrganismoMailist(this.value);">
                                    <option value="0">Todas</option>
                                    <?php
                                    include_once 'controller/regiaoRosacruzController.php';
                                    $rr = new regiaoRosaCruzController();
                                    $rr->criarComboBox(0);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" style="width:350px;">
                                    <option value="0">Todos</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo: </label>
                            <div class="col-sm-9">
                                <select name="tipo" id="tipo" onChange="carregaFuncoesCheckbox();">
                                	<option value="0">Selecione</option>
                                	<option value="1">Atuantes</option>
                                	<option value="2">Não Atuantes</option>
                                	<option value="3">Atuantes e Não Atuantes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Termino do Mandato: </label>
                            <div class="col-sm-9">
                                <select name="terminoMandato" id="terminoMandato">
                                	<option value="0">Qualquer</option>
                                        <?php for($i=2016;$i<(date('Y')+10);$i++){?>
                                	<option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo de Função: </label>
                            <div class="col-sm-9">
                                <input type="checkbox" name="tipoFuncao[]" id="oficiaisAdministrativos" value="1" onclick="carregaFuncoesCheckbox();"> Oficiais Administrativos<br>
                                <input type="checkbox" name="tipoFuncao[]" id="dignitarios" value="2" onclick="carregaFuncoesCheckbox();"> Dignitários<br>
                                <input type="checkbox" name="tipoFuncao[]" id="oficiaisRitualisticos" value="3" onclick="carregaFuncoesCheckbox();"> Oficiais Ritualísticos<br>
                                <input type="checkbox" name="tipoFuncao[]" id="oficiaisComissao" value="4" onclick="carregaFuncoesCheckbox();"> Oficiais de Comissão<br>
                                <input type="checkbox" name="tipoFuncao[]" id="oficiaisIniciaticos" value="5" onclick="carregaFuncoesCheckbox();"> Oficiais Iniciáticos<br>
                            </div>
                        </div>
                        <div class="form-group" id="linhaFuncao" style="display:none">
                            <label class="col-sm-3 control-label">Função: </label>
                            <div class="col-sm-9">
                                <input type="checkbox" name='todos' id='todos' checked="checked" onchange="marcardesmarcar();"/> Todos<br>
                                <div id="funcoesSelecionadas"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano Rosacruz (certificado): </label>
                            <div class="col-sm-9">
                                <select name="anoRosacruz" id="anoRosacruz">
                                	<option value="0">Selecionar</option>
                                        <?php for($i=2016;$i<(date('Y')+10);$i++){?>
                                	<option value="<?php echo anoRC($i, date('m'),date('d'));?>"><?php echo anoRC($i, date('m'),date('d'));?></option>
                                        <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data de Emissão (certificado):</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataEmissao" maxlength="10" id="dataEmissao" type="text" value="" class="form-control" style="max-width: 102px">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <a href="#" onclick="abrirPopupRelatorioOficiais(document.getElementById('fk_idOrganismoAfiliado').value,document.getElementById('tipo').value);" class="btn btn-sm btn-success" data-placement="left" title="Salvar">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Gerar Relatório
                                </a>
                                &nbsp;
                                <a href="#" onclick="abrirPopupRelatorioOficiaisMailistInovad(document.getElementById('fk_idRegiaoOrdemRosacruz').value,document.getElementById('fk_idOrganismoAfiliado').value,document.getElementById('tipo').value);" class="btn btn-sm btn-success" data-placement="left" title="Salvar">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Gerar Mailist
                                </a>
                                &nbsp;
                                <a href="#" onclick="abrirPopupRelatorioOficiaisCertificadoRetrato(document.getElementById('fk_idRegiaoOrdemRosacruz').value,document.getElementById('fk_idOrganismoAfiliado').value,document.getElementById('tipo').value,document.getElementById('anoRosacruz').value,document.getElementById('dataEmissao').value);" class="btn btn-sm btn-success" data-placement="left" title="Certificado">
                                    <i class="fa fa-magic fa-white"></i>&nbsp;
                                    Gerar Certificado Retrato
                                </a>
                                &nbsp;
                                <a href="#" onclick="abrirPopupRelatorioOficiaisCertificadoPaisagem(document.getElementById('fk_idRegiaoOrdemRosacruz').value,document.getElementById('fk_idOrganismoAfiliado').value,document.getElementById('tipo').value,document.getElementById('anoRosacruz').value,document.getElementById('dataEmissao').value);" class="btn btn-sm btn-success" data-placement="left" title="Certificado">
                                    <i class="fa fa-magic fa-white"></i>&nbsp;
                                    Gerar Certificado Paisagem
                                </a>
                                &nbsp;
                                <a href="#" onclick="abrirPopupRelatorioOficiaisEtiquetas(document.getElementById('fk_idRegiaoOrdemRosacruz').value,document.getElementById('fk_idOrganismoAfiliado').value,document.getElementById('tipo').value);" class="btn btn-sm btn-success" data-placement="left" title="Certificado">
                                    <i class="fa fa-magic fa-white"></i>&nbsp;
                                    Gerar Etiquetas
                                </a>
                                &nbsp;
                                <a href="exportacao/gcs_emeritos.php" class="btn btn-sm btn-success" data-placement="left" title="GCs Eméritos">
                                    <i class="fa fa-file-excel-o fa-white"></i>&nbsp;
                                    GCs Eméritos
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-times-circle modal-icon"></i>
                <h4 class="modal-title">Edição de ATA de Reunião Mensal do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <label>Motivo da edição:</label>
                <div style="border: #ccc solid 1px">
                    <div class="summernote"></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="atualizaMotivoEdicaoATA();">Editar ATA</button>
            </div>
        </div>
    </div>
</div>

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	