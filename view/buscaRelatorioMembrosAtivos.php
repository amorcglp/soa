<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório de Membros Ativos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="#">Relatórios</a>
            </li>
            <li class="active">
                <strong><a>Relatório de Membros Ativos</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Relatório de Membros Ativos</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <form name="extratoFinanceiro" class="form-horizontal" method="post" action="impressao/excelMembrosAtivosOA.php">
                <div class="ibox-content">
                    <div class="form-group" id="datapicker_ata">
                        <label class="col-sm-3 control-label">Mês/Ano Inicial:</label>
                        <div class="col-sm-9">
                            <select name="mesInicial" id="mesInicial" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                <option value="0">Selecione</option>
                                <?php
                                for($i=1;$i<=12;$i++)
                                {
                                    echo "<option value=\"".str_pad($i, 2, "0", STR_PAD_LEFT)."\">".str_pad($i, 2, "0", STR_PAD_LEFT)."</option>";
                                }
                                ?>
                            </select>
                            /
                            <select name="anoInicial" id="anoInicial" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                <option value="0">Selecione</option>
                                <?php
                                $tAno = date('Y')+4;
                                for($i=2016;$i<=$tAno;$i++)
                                {
                                    echo "<option value=\"".$i."\">".$i."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="datapicker_ata">
                        <label class="col-sm-3 control-label">Mês/Ano Final:</label>
                        <div class="col-sm-9">
                            <select name="mesFinal" id="mesFinal" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                <option value="0">Selecione</option>
                                <?php
                                for($i=1;$i<=12;$i++)
                                {
                                    echo "<option value=\"".str_pad($i, 2, "0", STR_PAD_LEFT)."\">".str_pad($i, 2, "0", STR_PAD_LEFT)."</option>";
                                }
                                ?>
                            </select>
                            /
                            <select name="anoFinal" id="anoFinal" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                <option value="0">Selecione</option>
                                <?php
                                $tAno = date('Y')+4;
                                for($i=2016;$i<=$tAno;$i++)
                                {
                                    echo "<option value=\"".$i."\">".$i."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="datapicker_ata">
                        <label class="col-sm-3 control-label">Relatório com:</label>
                        <div class="col-sm-9">
                            <input type="radio" name="tipoRelatorio" id="1" checked="checked" value="1"> Organismos que preencheram<br>
                            <input type="radio" name="tipoRelatorio" id="2" value="2"> Organismos que não preencheram<br>
                        </div>
                    </div>
                <?php if(!isset($regiaoUsuario)&&$_SESSION['fk_idDepartamento'] != 2 && $_SESSION['fk_idDepartamento'] != 3){ 
                                            $oaUsuario = $idOrganismoAfiliado;
                                            ?>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                        <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                            <option value="0">Selecione</option>
                                            <?php
                                            include_once 'controller/organismoController.php';
                                            $oc = new organismoController();
                                            $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                            ?>
                                        </select>
                            </div>    
                        </div>    
                                            <?php
                                        }else{ 
                                            $oaUsuario = "";
                                            ?>
                            <div class="form-group form-inline">
                                <label class="col-sm-3 control-label">Região:</label>
                                
                                <div class="col-sm-9">
                                        <select name="regiao" id="regiao" data-placeholder="Selecione uma regiao..." style="width:350px;" tabindex="2" onchange="carregaOrganismoMailist(this.value);">
                                            <option value="0">TODAS</option>
                                            <?php
                                            include_once 'controller/regiaoRosacruzController.php';
                                            $rrc = new regiaoRosacruzController();
                                            $rrc->criarComboBox(0,$regiaoUsuario);
                                            ?>
                                        </select>
                                </div>    
                            </div>
                            <div class="form-group form-inline">
                        <label class="col-sm-3 control-label">Organismo:</label>
                                <div class="col-sm-9">
                                        <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" style="width:350px;">
                                            
                                        </select>
                                </div>    
                        </div>
                                <?php }?>
                            
                        
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="ano" name="ano" type="text" value="" style="max-width: 83px">
                            </div>
                        </div>
                        -->

                </div>
                <div class="ibox-content">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <input type="submit" class="btn btn-sm btn-success" title="Calcular" value="Gerar Relatório de Membros Ativos">&nbsp;
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>	