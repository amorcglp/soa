<?php 

$arrOficial = array();
$arrOficial['nome'][0]							= "VILMA PEGORETTI";
$arrOficial['cod_afiliacao'][0]					= "41087";
$arrOficial['perfil'][0]						= "Rosacruz";
$arrOficial['obrigacao'][0]						= "6º Grau do Templo";
$arrOficial['data'][0]							= "17/10/14";
$arrOficial['hora_inicio'][0]					= "15:00";
$arrOficial['oa'][0]							= "PRONAOS R+C JARAGUA DO SUL - AMORC - SC107";
$arrOficial['imprime'][0]						= "Confirmados";
$arrOficial['status'][0]						= "1";

$arrOficial['nome'][1]							= "CLAUDIO FURLAN";
$arrOficial['cod_afiliacao'][1]					= "122227";
$arrOficial['perfil'][1]						= "Rosacruz";
$arrOficial['obrigacao'][1]						= "4º Grau do Templo";
$arrOficial['data'][1]							= "26/02/2015";
$arrOficial['hora_inicio'][1]					= "14:00";
$arrOficial['oa'][1]							= "LOJA R+C SAO PAULO-AMORC - SP101";
$arrOficial['imprime'][1]						= "Agendados";
$arrOficial['status'][1]						= "2";

$arrOficial['nome'][2]							= "JOSE RAMOS DE ASSUMPCAO";
$arrOficial['cod_afiliacao'][2]					= "132011";
$arrOficial['perfil'][2]						= "TOM";
$arrOficial['obrigacao'][2]						= "2º Grau do Templo";
$arrOficial['data'][2]							= "17/12/14";
$arrOficial['hora_inicio'][2]					= "11:00";
$arrOficial['oa'][2]							= "LOJA R+C CAMPINAS - AMORC - SP301";
$arrOficial['imprime'][2]						= "Confirmados";
$arrOficial['status'][2]						= "1";

$arrOficial['nome'][3]							= "DAMIANA SANTOS CABRAL";
$arrOficial['cod_afiliacao'][3]					= "144575";
$arrOficial['perfil'][3]						= "Rosacruz";
$arrOficial['obrigacao'][3]						= "9º Grau do Templo";
$arrOficial['data'][3]							= "17/04/15";
$arrOficial['hora_inicio'][3]					= "09:00";
$arrOficial['oa'][3]							= "LOJA R+C BELEM-AMORC - PA101";
$arrOficial['imprime'][3]						= "Agendados";
$arrOficial['status'][3]						= "2";

$arrOficial['nome'][4]							= "ADRIANA WESTPHALEN VESCIA";
$arrOficial['cod_afiliacao'][4]					= "333102";
$arrOficial['perfil'][4]						= "TOM";
$arrOficial['obrigacao'][4]						= "3º Grau do Templo";
$arrOficial['data'][4]							= "05/11/14";
$arrOficial['hora_inicio'][4]					= "07:00";
$arrOficial['oa'][4]							= "LOJA R+C PORTO ALEGRE-AMORC - RS101";
$arrOficial['imprime'][4]						= "Confirmados";
$arrOficial['status'][4]						= "3";

?>
			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Área de Iniciações</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>
	                        <li>
	                            <a href="index.html">Iniciações</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Iniciações Realizadas</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
			
				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Iniciações Realizadas</h5>
									<div class="ibox-tools">
										<!--
										<a class="btn btn-xs btn-primary" href="http://localhost/soa/includes/ritualistica_nova.php">
											<i class="fa fa-plus"></i> Agendar Nova
										</a>
										-->
									</div>
								</div>
								<div class="ibox-content">
									<table class="table table-striped table-bordered table-hover dataTables-example" >
										<thead>
											<tr>
												<th>Nome</th>
												<th><center>Cód. Afiliação</center></th>
												<th><center>Perfil</center></th>
												<th><center>Obrigação</center></th>
												<th><center>Data</center></th>
												<th><center>Organismo Afiliado</center></th>
												<th><center>Ações</center></th>
											</tr>
										</thead>
										<tbody>
										<?php if(count($arrOficial)>0)
									  		{ 
										  		for($i=0;$i<count($arrOficial['perfil']);$i++){
										  	?>
										 <tr>
											<td>
												<?php echo utf8_encode($arrOficial['nome'][$i]);?>
											</td>
											<td>
												<center>
													<?php echo $arrOficial['cod_afiliacao'][$i];?>
												</center>
											</td>
											<td>
												<?php echo $arrOficial['perfil'][$i];?>
											</td>
											<td>
												<center>
													<?php echo $arrOficial['obrigacao'][$i];?>
												</center>
											</td>
											<td>
												<?php echo $arrOficial['data'][$i];?>
											</td>
											<td>
												<center>
													<?php echo utf8_encode($arrOficial['oa'][$i]);?>
												</center>
											</td>
											<td>
												<center>
													<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
					                                    <i class="fa fa-search-plus fa-white"></i>&nbsp;
					                                    Detalhes
					                                </button>
					                                <br>
													<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#mySeeMotivoEdicao" data-toggle="tooltip" data-placement="left" title="Ao editar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
					                                    <i class="fa fa-edit fa-white"></i>&nbsp;
					                                    Editar
					                                </button>
													<!-- <div class="espacoAcoes">&nbsp;</div> -->
													<!--<div id="acoes_confirma_cancela"></div>-->
												</center>
											</td>
										</tr>
										<?php }
									  	}
									  	?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->
				
				<!-- Window MODAL Início -->
				
				<!-- Edição de Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-edit modal-icon"></i>
								<h4 class="modal-title">Edição de Iniciação Ritualística</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<!--
								<div class="alert alert-danger">
	                                <a class="alert-link" href="#">Atenção! </a>Você têm certeza que deseja cancelar este agendamento? Ou talvez reagendar? Se for este o caso feche a janela atual e clique na opção <b>Reagendar</b> que se encontra abaixo da <b>Data/Hora de início</b> na lista.
	                            </div>
								-->
								<label>Motivo da Edição:</label>
								<div style="border: #ccc solid 1px">
									<div class="summernote"></div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary">Seguir para tela de edição</button>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Detalhes da Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-search-plus modal-icon"></i>
								<h4 class="modal-title">Detalhes da Iniciação Ritualística</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<form action="" class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-3 control-label">Nome:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">ADRIANA WESTPHALEN VESCIA</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Cód. Afiliação:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">333102</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Perfil:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">TOM</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Obrigação:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">3º Grau do Templo</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Data:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">05/11/14</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Organismo Afiliado:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">LOJA R+C PORTO ALEGRE-AMORC - RS101</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Registrado por:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">CLAUDIO FURLAN</div>
	                                    </div>
	                               	</div>
	                            </form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary" onclick="window.print();">Imprimir</button>
							</div>
						</div>
					</div>
				</div>
				<!-- Window MODAL Fim -->
				
				<!-- Conteúdo DE INCLUDE FIM -->