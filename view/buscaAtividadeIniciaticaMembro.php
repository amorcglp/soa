<script src="js/jquery-2.1.1.js"></script>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades Iniciáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Membros com Atividades Iniciáticas Pendentes</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<?php // echo "<script type='text/javascript'> $(document).ready(function(){ startProgress() }); </script>";

@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>
<?php
flush();
    include_once("controller/atividadeIniciaticaController.php");
    include_once("controller/atividadeIniciaticaOficialController.php");
    include_once("controller/organismoController.php");
    include_once("model/membroPotencialOAClass.php");

    $aic = new atividadeIniciaticaController();
    $ai = new atividadeIniciatica();
    $aioc = new atividadeIniciaticaOficialController();
    $aiom = new atividadeIniciaticaOficial();
    $oc = new organismoController();
    $o = new organismo();
    $mp = new membroPotencialOA();

    $sessao = new criaSessao();

    $idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';

    if($idOrganismo == ''){
        echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaAtividadeIniciatica';</script>";
    }

    if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
        $idOrg = $idOrganismo;
    } else {
        $idOrg = $idOrganismoAfiliado;
    }
?>
<?php
flush();
$resultado = $mp->listaMembrosPorIdOrganismoFiltradoPorData($idOrg);

$arrayMembros=array();

$ocultar_json=1;

$b=0;
$erroPote01=0;
$erroPote02=0;
if ($resultado) {
    foreach ($resultado as $vetor) {
        $seqCadast = $vetor['seqCadastMembroOa'];

        $arrayMembros[$b]['seqCadast']                  = $seqCadast;
        $arrayMembros[$b]['nome']                       = $vetor['fNomCliente'];

        $arrayMembros[$b]['email']                      = $vetor['fDesEmail'] != '' ? $vetor['fDesEmail'] : 'Nenhum e-mail cadastrado!';
        $arrayMembros[$b]['telefoneFixo']               = $vetor['fNumTelefoFixo'] != '' ? $vetor['fNumTelefoFixo'] : 'Telefone fixo não cadastrado!';
        $arrayMembros[$b]['telefoneCel']                = $vetor['fNumCelula'] != '' ? $vetor['fNumCelula'] : 'Celular não cadastrado!';
        $arrayMembros[$b]['telefoneOutro']              = $vetor['fNumOutroTelefo'] != '' ? $vetor['fNumOutroTelefo'] : 'Outro telefone não cadastrado!';

        $arrayMembros[$b]['codAfiliacao']               = $vetor['fCodRosacruz'];
        $arrayMembros[$b]['lote']                       = $vetor['fNumLoteAtualRosacr'];
        $arrayMembros[$b]['tempoDeMembro']              = $vetor['fDatAdmissRosacr'];
        $arrayMembros[$b]['situacaoRemessa']            = $vetor['fIdeTipoSituacRemessRosacr'];

        include './js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
        $obj2 = json_decode(json_encode($return),true);

        if (count($obj2['result'][0]['fields']['fArrayObrigacoes']) > 0) {
            $c = 0;
            foreach ($obj2['result'][0]['fields']['fArrayObrigacoes'] as $vetorObrigacoes) {
                //echo "<pre>";print_r($vetorObrigacoes);echo "</pre>";

                if(($vetorObrigacoes['fields']['fSeqTipoObriga'] == 0) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 1) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 3) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 5) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 7) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 9) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 11) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 13) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 15) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 17) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 19) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 32) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 33) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 34))
                {
                    $arrayMembros[$b]['atividades'][] = $vetorObrigacoes['fields']['fSeqTipoObriga'];
                    $c++;
                }
            }
            if($c==0){
                $arrayMembros[$b]['atividades'][] = 0;
            }
        } else {
            $arrayMembros[$b]['atividades'][] = 0;
        }

        $b++;
    }
}

/*
 * Regras de Negócio
 */

flush();
for($i=0; $i < count($arrayMembros); $i++){
    if(!empty($arrayMembros[$i]['atividades'])){
        if (!sort($arrayMembros[$i]['atividades'])) {
            exit();
        }
    }
}

$poteGrauUm         = 0;
$poteGrauDois       = 0;
$poteGrauTres       = 0;
$poteGrauQuatro     = 0;
$poteGrauCinco      = 0;
$poteGrauSeis       = 0;
$poteGrauSete       = 0;
$poteGrauOito       = 0;
$poteGrauNove       = 0;
$poteGrauDez        = 0;
$poteGrauOnze       = 0;
$poteGrauDoze       = 0;

$poteGrauDozeFeito  = 0;
$poteIndisponivel   = 0;

if(!empty($arrayMembros)) {
    for ($i = 0; $i < count($arrayMembros); $i++) {

        if(isset($arrayMembros[$i]['tempoDeMembro'])) {
            $membroDesde = $arrayMembros[$i]['tempoDeMembro'];

            $anoEntrada = substr($membroDesde, 0, 4);

            $anoAtual = date('Y');

            $meses = (($anoAtual - $anoEntrada) - 1) * 12;

            $meses = $meses < 0 ? 0 : $meses;

            $mesEntrada = substr($membroDesde, 5, 2);

            $mesAtual = date('m');

            if($anoEntrada != $anoAtual) {
                $meses = $meses + ($mesAtual + (12 - ($mesEntrada - 1)));
            } else {
                $meses = $meses + $mesAtual;
            }
            $arrayMembros[$i]['tempoDeMembro'] = $meses;

        }

        if (!empty($arrayMembros[$i]['atividades'])) {
            for ($z = 0; $z < count($arrayMembros[$i]['atividades']); $z++) {

                if(!empty($arrayMembros[$i]['lote'])) {
                    if ($arrayMembros[$i]['lote'] >= 6) {

                        $arrayMembros[$i]['tempoDeMembro'];

                    }
                }
                $grauAtual = $arrayMembros[$i]['atividades'][$z];
            }

            switch($grauAtual){
                case 0: $grau = 0; break;
                case 1: $grau = 0; break;
                case 3: $grau = 1; break;
                case 5: $grau = 2; break;
                case 7: $grau = 3; break;
                case 9: $grau = 4; break;
                case 11: $grau = 5; break;
                case 13: $grau = 6; break;
                case 15: $grau = 7; break;
                case 17: $grau = 8; break;
                case 19: $grau = 9; break;
                case 32: $grau = 10; break;
                case 33: $grau = 11; break;
                case 34: $grau = 12; break;
            }

            if(isset($arrayMembros[$i]['tempoDeMembro']) && isset($grau) && isset($arrayMembros[$i]['lote'])){
                //echo "grau: ".$grau." | ".$arrayMembros[$i]['tempoDeMembro']." | ".$arrayMembros[$i]['lote']."<br>";
                if(($grau == 0) && ($arrayMembros[$i]['tempoDeMembro'] >= 18) && ($arrayMembros[$i]['lote'] >= 6)){

                    $arrayMembros[$i]['grauDisponivel'] = 1;

                    $poteGrauUm++;
                } else if(($grau == 1) && ($arrayMembros[$i]['tempoDeMembro'] >= 21) && ($arrayMembros[$i]['lote'] >= 7)){

                    $arrayMembros[$i]['grauDisponivel'] = 2;
                    $poteGrauDois++;
                } else if(($grau == 2) && ($arrayMembros[$i]['tempoDeMembro'] >= 24) && ($arrayMembros[$i]['lote'] >= 8)){

                    $arrayMembros[$i]['grauDisponivel'] = 3;
                    $poteGrauTres++;
                } else if(($grau == 3) && ($arrayMembros[$i]['tempoDeMembro'] >= 27) && ($arrayMembros[$i]['lote'] >= 9)){

                    $arrayMembros[$i]['grauDisponivel'] = 4;
                    $poteGrauQuatro++;
                } else if(($grau == 4) && ($arrayMembros[$i]['tempoDeMembro'] >= 30) && ($arrayMembros[$i]['lote'] >= 10)){

                    $arrayMembros[$i]['grauDisponivel'] = 5;
                    $poteGrauCinco++;
                } else if(($grau == 5) && ($arrayMembros[$i]['tempoDeMembro'] >= 33) && ($arrayMembros[$i]['lote'] >= 11)){

                    $arrayMembros[$i]['grauDisponivel'] = 6;
                    $poteGrauSeis++;
                } else if(($grau == 6) && ($arrayMembros[$i]['tempoDeMembro'] >= 39) && ($arrayMembros[$i]['lote'] >= 13)){

                    $arrayMembros[$i]['grauDisponivel'] = 7;
                    $poteGrauSete++;
                } else if(($grau == 7) && ($arrayMembros[$i]['tempoDeMembro'] >= 48) && ($arrayMembros[$i]['lote'] >= 16)){

                    $arrayMembros[$i]['grauDisponivel'] = 8;
                    $poteGrauOito++;
                } else if(($grau == 8) && ($arrayMembros[$i]['tempoDeMembro'] >= 57) && ($arrayMembros[$i]['lote'] >= 19)){

                    $arrayMembros[$i]['grauDisponivel'] = 9;
                    $poteGrauNove++;
                } else if(($grau == 9) && ($arrayMembros[$i]['tempoDeMembro'] >= 69) && ($arrayMembros[$i]['lote'] >= 23)){

                    $arrayMembros[$i]['grauDisponivel'] = 10;
                    $poteGrauDez++;
                } else if(($grau == 10) && ($arrayMembros[$i]['tempoDeMembro'] >= 96) && ($arrayMembros[$i]['lote'] >= 32)){

                    $arrayMembros[$i]['grauDisponivel'] = 11;
                    $poteGrauOnze++;
                } else if(($grau == 11) && ($arrayMembros[$i]['tempoDeMembro'] >= 135) && ($arrayMembros[$i]['lote'] >= 45)){

                    $arrayMembros[$i]['grauDisponivel'] = 12;
                    $poteGrauDoze++;
                }  else if($grau == 12){

                    $arrayMembros[$i]['grauDisponivel'] = null;
                    $poteGrauDozeFeito++;
                } else {
                    $arrayMembros[$i]['grauDisponivel'] = null;

                    $poteIndisponivel++;
                }
            }
        }
    }
}

//echo "Número de erros retornaDadosMembroPorSeqCadast: ".$erroPote01."<br>";
//echo "Número de erros retornaObrigacoesRitualisticasDoMembro: ".$erroPote02."<br>";
//echo "Números de membros: ".count($arrayMembros)."<br>";

$arrayFinal=array();
if(!empty($arrayMembros)) {
    for ($i = 0; $i < count($arrayMembros); $i++) {
        if(!empty($arrayMembros[$i]['grauDisponivel']) && $arrayMembros[$i]['grauDisponivel'] != null){
            $arrayFinal[] = $arrayMembros[$i];
        }
    }
}
//echo "Números de membros: ".count($arrayFinal)."<br>";

//echo "<pre>";print_r($arrayFinal);echo "</pre>";
//echo "Quantidade de Registros: ".$pote;
?>

<?php // echo "<script type='text/javascript'> $(document).ready(function(){ stopProgress() }); </script>"; ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        INFO
                        <?php
                        if($idOrganismo!=''){
                            $nomeOa = retornaNomeCompletoOrganismoAfiliado($idOrganismo);
                            echo ' - '.$nomeOa;
                        }
                        ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="alert alert-success">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <center>
                                            Membros com atividades iniciáticas disponíveis:
                                        </center>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <ul>
                                            <li><a class="alert-link">1º Grau de Templo: </a><?php echo $poteGrauUm; ?>.</li>
                                            <li><a class="alert-link">2º Grau de Templo: </a><?php echo $poteGrauDois; ?>.</li>
                                            <li><a class="alert-link">3º Grau de Templo: </a><?php echo $poteGrauTres; ?>.</li>
                                            <li><a class="alert-link">4º Grau de Templo: </a><?php echo $poteGrauQuatro; ?>.</li>
                                            <li><a class="alert-link">5º Grau de Templo: </a><?php echo $poteGrauCinco; ?>.</li>
                                            <li><a class="alert-link">6º Grau de Templo: </a><?php echo $poteGrauSeis; ?>.</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">
                                        <ul>
                                            <li><a class="alert-link">7º Grau de Templo: </a><?php echo $poteGrauSete; ?>.</li>
                                            <li><a class="alert-link">8º Grau de Templo: </a><?php echo $poteGrauOito; ?>.</li>
                                            <li><a class="alert-link">9º Grau de Templo: </a><?php echo $poteGrauNove; ?>.</li>
                                            <li><a class="alert-link">10º Grau de Templo: </a><?php echo $poteGrauDez; ?>.</li>
                                            <li><a class="alert-link">11º Grau de Templo: </a><?php echo $poteGrauOnze; ?>.</li>
                                            <li><a class="alert-link">12º Grau de Templo: </a><?php echo $poteGrauDoze; ?>.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="alert alert-warning">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <center>
                                            Membros com atividades iniciáticas indisponíveis:
                                        </center>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <ul>
                                        <li><a class="alert-link">Membros já no 12º Grau de Templo: </a><?php echo $poteGrauDozeFeito; ?>.</li>
                                        <li><a class="alert-link">Membros que não podem fazer iniciações: </a><?php echo $poteIndisponivel; ?>.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php flush(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        Membros com Atividades Iniciáticas Pendentes
                    </h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciatica">
                            <i class="fa fa-reply"></i> Voltar para área de Atividades
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-membros-iniciaticos" >
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Cód. de Afiliação</th>
                                <th>Grau Disponível</th>
                                <th><center>Status</center></th>
                                <th>Ações</th>
                        	</tr>
                        </thead>
                        <tbody>
                        <?php
                            if(!empty($arrayFinal)){
                                for($i=0; $i < count($arrayFinal); $i++){
                                ?>
                                    <tr>
                                        <td>
                                            <?php
                                            $arrNome				= explode(" ", $arrayFinal[$i]['nome']);
                                            $nome = ucfirst(mb_strtolower($arrNome[0],'UTF-8'))
                                                    ." ". ucfirst(mb_strtolower($arrNome[1],'UTF-8'));
                                            if(array_key_exists(2, $arrNome)){
                                                $nome .= " " . ucfirst(mb_strtolower($arrNome[2],'UTF-8'));
                                            }
                                            if(array_key_exists(3, $arrNome)){
                                                $nome .= " " . ucfirst(mb_strtolower($arrNome[3],'UTF-8'));
                                            }
                                            if(array_key_exists(4, $arrNome)){
                                                $nome .= " " . ucfirst(mb_strtolower($arrNome[4],'UTF-8'));
                                            }
                                            if(array_key_exists(5, $arrNome)){
                                                $nome .= " " . ucfirst(mb_strtolower($arrNome[5],'UTF-8'));
                                            }
                                            if(array_key_exists(6, $arrNome)){
                                                $nome .= " " . ucfirst(mb_strtolower($arrNome[6],'UTF-8'));
                                            }
                                            echo $nome;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $arrayFinal[$i]['codAfiliacao']; ?>
                                        </td>
                                        <td>
                                            <?php echo $arrayFinal[$i]['grauDisponivel']; ?>º Grau de Templo
                                        </td>
                                        <td>
                                            <center>
                                            <?php
                                            $resultadoAgendado = $ai->verificaMembroAgendado($arrayFinal[$i]['seqCadast'], $arrayFinal[$i]['grauDisponivel']);
                                            if($resultadoAgendado){
                                            ?>
                                                <div id="statusMembro<?php echo $arrayFinal[$i]['seqCadast']; ?>">
                                                    <span class="badge badge-primary">Agendado</span>
                                                </div>
                                            <?php
                                            } else {
                                            ?>
                                                <div id="statusMembro<?php echo $arrayFinal[$i]['seqCadast']; ?>">
                                                    <span class='badge badge-danger'>Pendente</span>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                $arrNome				= explode(" ",$arrayFinal[$i]['nome']);
                                                $arrNomeDecrescente		= $arrNome[count($arrNome)-1];
                                                ?>
                                                <input type="hidden" name="nomeAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" id="nomeAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" value="<?php echo ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNomeDecrescente,'UTF-8')) ; ?>">

                                                <input type="hidden" name="emailAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" id="emailAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" value="<?php echo $arrayFinal[$i]['email']; ?>">
                                                <input type="hidden" name="telFixoAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" id="telFixoAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" value="<?php echo $arrayFinal[$i]['telefoneFixo']; ?>">
                                                <input type="hidden" name="telCelAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" id="telCelAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" value="<?php echo $arrayFinal[$i]['telefoneCel']; ?>">
                                                <input type="hidden" name="telOutroAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" id="telOutroAtividadeIniciatica<?php echo $arrayFinal[$i]['seqCadast']; ?>" value="<?php echo $arrayFinal[$i]['telefoneOutro']; ?>">

                                                <div id="botoesMembro<?php echo $arrayFinal[$i]['seqCadast']; ?>">
                                                    <a class="btn btn-sm btn-info" onclick="abreModalInfoContato('<?php echo $arrayFinal[$i]['seqCadast']; ?>');" data-target="#mySeeInfoMembro">
                                                        <i class="fa fa-envelope fa-white"></i>
                                                        Contato
                                                    </a>
                                                    <?php
                                                    if($resultadoAgendado){
                                                        foreach($resultadoAgendado as $vetorAgendado){
                                                            $idAtividadeIniciatica = $vetorAgendado['fk_idAtividadeIniciatica'];
                                                        }
                                                    ?>
                                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" onclick="abreModalAtividadeViaAjax(<?php echo $idAtividadeIniciatica; ?>);" data-target="#mySeeAtividadeIniciatica">
                                                            <i class="fa fa-search fa-white"></i>
                                                            Visualizar
                                                        </a>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <a class="btn btn-sm btn-primary" href="" data-toggle="modal" onclick="abreModalAtividadesDisponiveis('<?php echo $arrayFinal[$i]['grauDisponivel']; ?>','<?php echo $idOrg; ?>','<?php echo $arrayFinal[$i]['seqCadast']; ?>');" data-target="#mySeeAgendamento">
                                                            <i class="fa fa-plus fa-white"></i>
                                                            Agendar
                                                        </a>
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<!-- OnLoad Progress -->
<div class="modal inmodal" id="mySeeLoading" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="cssload-box-loading" style="margin: 30% 50% 50% 50%;">
        </div>
    </div>
</div>

<!-- Agendamento e Reagendamento de Iniciação Ritualística -->
<div class="modal inmodal" id="mySeeAgendamento" name="mySeeAgendamento" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-calendar modal-icon"></i>
                <h4 class="modal-title">Agendamento de Iniciação</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="conteudoModalIniciacoes"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="iniciacaoDisponivelButton"></div>
            </div>
        </div>
    </div>
</div>

<!-- Info Iniciação Ritualística -->
<div class="modal inmodal" id="mySeeAtividadeIniciatica" name="mySeeAtividadeIniciatica" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-calendar modal-icon"></i>
                <h4 class="modal-title">Atividade Iniciática</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="conteudoModalAtividade"></div>
                </div>
            </div>
            <div class="modal-footer">
                <div id="atividadeButton">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Info de Contato do membro -->
<div class="modal inmodal" id="mySeeInfoMembro" name="mySeeInfoMembro" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-calendar modal-icon"></i>
                <h4 class="modal-title">Contatos do Membro</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="conteudoModalInfoContato"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->
