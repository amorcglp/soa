<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atualizações Servidor</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaNotificacaoAtualizacaoServer">Atualização Servidor</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Nova Notificação de Atualização</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaNotificacaoAtualizacaoServer">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="post" action="acoes/acaoCadastrar.php">

                        <div class="form-group"><label class="col-sm-2 control-label">Título da Notificação</label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloNotificacao" id="tituloNotificacao" maxlength="45" class="form-control" style="max-width: 320px" required="required">
                                <p class="small">(45 caracteres)</p>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10">
                                <div style="border: 1px solid #ccc">
                                    <textarea class="summernote" id="descricaoNotificacao" name="descricaoNotificacao" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Típo de Notificação:</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="tipoNotificacao" id="tipoNotificacao" style="max-width: 150px" required="required">
                                    <option value="0">Selecione</option>
                                    <option value="1">Melhoria</option>
                                    <option value="2">Corrigido</option>
                                    <option value="3">Novidade</option>
                                    <option value="4">Erro</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-2 control-label">Data Agendada para avisar todos:</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAgendada" maxlength="10" id="dataAgendada" type="text" class="form-control" style="max-width: 102px"  required="required">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" />
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>