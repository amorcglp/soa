<?php
include_once 'controller/organismoController.php';
include_once 'controller/ticketController.php';
include_once 'controller/notificacaoController.php';
include_once 'controller/usuarioController.php';
include_once 'controller/perfilUsuarioController.php';
include_once 'model/ticketClass.php';
include_once('model/ticketItemFuncionalidadeClass.php');
include_once 'model/usuarioClass.php';


$tif = new TicketItemFuncionalidade();
$oc = new organismoController();
$tc = new ticketController();
$nc = new notificacaoController();
$uc = new usuarioController();
$uc2 = new usuarioController();
$puc = new perfilController();
$tm = new Ticket();
$u = new Usuario();


@usuarioOnline();

$id	                = isset($_GET['d']) ? json_decode($_GET['d']) : '';
$seqCadast          = $sessao->getValue("seqCadast");

if($id=='')
    echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaTicket';</script>";

if(!$tm->hasAuthorization($seqCadast,$id))
    echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaTicket';</script>";

//$tm->alteraStatusTicketUsuario(1,$id,$seqCadast);

$liberaGLP=false;
if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
    $liberaGLP = true;
    //Se for alguém da GLP então decrementar ticket como visto para todos
    $resultadoUsuarios = $u->listaUsuario(null,"2,3");
    if($resultadoUsuarios)
    {
            foreach ($resultadoUsuarios as $vetor)
            {
                $tm->alteraStatusTicketUsuario(1,$id,$vetor['idUsuario']);
                    
            }
    }
}

$dados = $tc->buscaTicket($id);
//echo "<pre>";print_r();
$idFinalidade= $dados->getFkIdTicketFinalidade();

$dados = $tc->buscaFuncionalidade($dados->getFkIdFuncionalidade());

switch($dados->getStatusTicket()){
    case 0: $botao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-default btn-xs dropdown-toggle'>Aberto <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$id.",1);'>Em andamento</a></li><li><a onClick='mudaStatusTicket(".$id.",3);'>Indeferido!</a></li><li><a onClick='mudaStatusTicket(".$id.",2);'>Fechado</a></li></ul></div>"; break;
    case 1: $botao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-warning btn-xs dropdown-toggle'>Em andamento <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$id.",0);'>Aberto</a></li><li><a onClick='mudaStatusTicket(".$id.",3);'>Indeferido!</a></li><li><a onClick='mudaStatusTicket(".$id.",2);'>Fechado</a></li></ul></div>"; break;
    case 2: $botao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-primary btn-xs dropdown-toggle'>Fechado <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$id.",0);'>Aberto</a></li><li><a onClick='mudaStatusTicket(".$id.",1);'>Em andamento</a></li><li><a onClick='mudaStatusTicket(".$id.",3);'>Indeferido!</a></li></ul></div>"; break;
    case 3: $botao = "<div class='btn-group'><button data-toggle='dropdown' class='btn btn-danger btn-xs dropdown-toggle'>Indeferido! <span class='caret'></span></button><ul class='dropdown-menu'><li><a onClick='mudaStatusTicket(".$id.",0);'>Aberto</a></li><li><a onClick='mudaStatusTicket(".$id.",1);'>Em andamento</a></li><li><a onClick='mudaStatusTicket(".$id.",2);'>Fechado</a></li></ul></div>"; break;
    default: $botao = "<div class='btn-group'><button class='btn btn-danger btn-xs dropdown-toggle'>Erro! Atualize a página. </button></div>";
}

switch($dados->getStatusTicket()){
    case 0: $botaoMembro = "<div class='btn-group'><button class='btn btn-default btn-xs dropdown-toggle'>Aberto </button></div>"; break;
    case 1: $botaoMembro = "<div class='btn-group'><button class='btn btn-warning btn-xs dropdown-toggle'>Em andamento </button></div>"; break;
    case 2: $botaoMembro = "<div class='btn-group'><button class='btn btn-primary btn-xs dropdown-toggle'>Fechado </button></div>"; break;
    case 3: $botaoMembro = "<div class='btn-group'><button class='btn btn-danger btn-xs dropdown-toggle'>Indeferido! </button></div>"; break;
    default: $botaoMembro = "<div class='btn-group'><button class='btn btn-danger btn-xs dropdown-toggle'>Erro! Atualize a página. </button></div>";
}

switch($dados->getPrioridadeTicket()) {
    case 1: $prioridadeTicket = "<span><i class='fa fa-circle text-success'></i> Prioridade Baixa</span>"; break;
    case 2: $prioridadeTicket = "<span><i class='fa fa-circle text-warning'></i> Prioridade Média</span>"; break;
    case 3: $prioridadeTicket = "<span><i class='fa fa-circle text-danger'></i> Prioridade Alta</span>"; break;
    default: $prioridadeTicket = "<span><i class='fa fa-circle'></i> Erro!</span>";
}

if($dados->getCriadoPorTicket() != 0){
    $dadosUsuarioCriador    = $uc->buscaUsuario($dados->getCriadoPorTicket());
    $dadosAvatar            = $puc->listaAvatarUsuario($dados->getCriadoPorTicket());
}

if($dados->getAtribuidoTicket() != 0) {
    $dadosUsuarioAtribuido      = $uc2->buscaUsuario($dados->getAtribuidoTicket());
}


$glp = ($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2) ? true : false;
 
?>

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2>Ticket <?php echo "[ID: ".$id."]"; ?></h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="painelDeControle.php">Home</a>
                    </li>
                    <li class="active">
                        <strong>Detalhes do Ticket</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="wrapper wrapper-content animated fadeInUp">
                    <div class="ibox">
                        <div class="ibox-content">

                            <!-- Cabeçalho do Ticket INÍCIO -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="m-b-md">
                                        <a href="?corpo=buscaTicket" class="btn btn-white btn-xs pull-right">Voltar aos Tickets</a>
                                        <!--
                                        <a href="#" class="btn btn-white btn-xs pull-right">Editar Ticket</a>
                                        -->
                                        <input type="hidden" name="idFunc" id="idFunc" value="<?php echo $dados->getFkIdFuncionalidade() ;?>">
                                        <input type="hidden" name="idIdentificadorTicket" id="idIdentificadorTicket" value="<?php echo $dados->getIdentificadorTicket() ;?>">
                                        <input type="hidden" name="nomeFuncionalidade" id="nomeFuncionalidade" value="<?php echo $dados->getNomeFuncionalidade(); ?>">
                                        <h2><?php echo $dados->getTituloTicket(); ?> 
                                            <?php if($dados->getFkIdFuncionalidade()!=1&&$dados->getFkIdFuncionalidade()!=4){?>
                                            <a href="#" onclick="carregaUploadsDaFuncionalidade();"data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"><i class="fa fa-download"></i></a>
                                            <?php }?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <dl class="dl-horizontal">
                                        <dt>Status:</dt>
                                        <dd id="statusTicket">
                                            <?php echo $glp ? $botao : $botaoMembro; ?>
                                        </dd>
                                        <dt style="margin-top: 5px">Criado Por:</dt>
                                        <dd style="margin-top: 5px">
                                        <?php echo retornaNomeFormatado($dadosUsuarioCriador->getNomeUsuario(),3); ?>
                                        </dd>
                                        <dt>Criado em:</dt> <dd><?php if($dados->getDataCriacaoTicket() != "00/00/0000 às 00:00"){ echo $dados->getDataCriacaoTicket();} else { echo "--"; } ?></dd>
                                        <dt>Notificações:</dt> <dd>  <?php echo $nc->retornaNumeroNotificacao($id) ?></dd>
                                        <dt>Funcionalidade:</dt> <dd><a target="_blank" href="painelDeControle.php?corpo=<?php echo $dados->getCorpoFuncionalidade(); ?>" class="text-navy"> <?php echo $dados->getNomeFuncionalidade(); ?></a> </dd>
                                        <dt>Identificador:</dt> <dd> <?php echo $dados->getIdentificadorTicket() ;?> </dd>
                                        <dt>Organismo Afiliado:</dt> <dd> <?php echo retornaNomeCompletoOrganismoAfiliado($dados->getFkIdOrganismoAfiliado()) ;?> </dd>
                                        <?php if($glp){ ?>
                                        <dt style="margin-top: 10px">Atribuído para:</dt>
                                        <dd style="margin-top: 10px">
                                            <select class="select2 form-control" style="max-width: 200px" id="atribuicao" onchange="alterarAtribuicaoTicket(this.value);">
                                                <option value="0">Aguardando atribuição!</option>
                                                <?php $tc->criarComboBoxUsuarioAtribuido($dados->getAtribuidoTicket()); ?>
                                            </select>
                                            <i id="check" class="fa fa-check" style="display: none"></i>
                                            <i id="erro" class="fa fa-refresh" style="display: none"></i>
                                        </dd>
                                        <?php } else { ?>
                                        <dt>Atribuído para:</dt>
                                        <dd><?php echo $dados->getAtribuidoTicket() != 0 ? retornaNomeFormatado($dadosUsuarioAtribuido->getNomeUsuario(),3) : "Aguardando atribuição!"; ?></dd>
                                        <?php } ?>
                                    </dl>
                                </div>
                                <div class="col-lg-6" id="cluster_info">
                                    <dl class="dl-horizontal" >
                                        <?php 
                                        //echo "--->".$idFinalidade;
                                        if ($liberaGLP&&$dados->getFkIdTicketFinalidade() == 3) { 
                                            //Pesquisar informações do vinculo do ticket com a solicitação do token
                                            $arrVinculo = $tif->listaVinculoItemTicket($dados->getIdFuncionalidade(),null,null,3,$id);
                                            //echo "<pre>";print_r($arrVinculo);
                                            if($arrVinculo[0]['mes']!=0&&$arrVinculo[0]['ano']!=0)
                                            {    
                                                $referenteMesAno = ' Mês: '. mesExtensoPortugues($arrVinculo[0]['mes']).' Ano: '.$arrVinculo[0]['ano'];
                                                $mes = $arrVinculo[0]['mes'];
                                                $ano = $arrVinculo[0]['ano'];
                                            }else{
                                                $referenteMesAno = '';
                                                $mes=0;
                                                $ano=0;
                                            } 
                                            //Funcões do Oficial
                                            include_once 'model/funcaoUsuarioClass.php';
                                            $fuOficial = new FuncaoUsuario();
                                            $funcoesOficial="";
                                            $x=0;
                                            $loginUsuario=$arrVinculo[0]['loginUsuario'];
                                            $respostaFuncoes = $fuOficial->buscaPorFuncaoPorLogin($loginUsuario);
                                            if($respostaFuncoes)
                                            {
                                                foreach ($respostaFuncoes as $vet)
                                                {
                                                    if($x==0)
                                                    {
                                                        $funcoesOficial = $vet['nomeFuncao'];
                                                    }else{
                                                        $funcoesOficial .= ", ".$vet['nomeFuncao'];
                                                    }
                                                    $x++;
                                                }
                                            }
                                            ?>
                                        <dt>Liberar Token:
                                        </dt>
                                        <dd>
                                            <a href="#" onclick="document.getElementById('funcoesOficial').innerHTML='<?php echo $funcoesOficial;?>';document.getElementById('motivo').innerHTML='<?php echo $arrVinculo[0]['motivo'];?>';document.getElementById('idFuncionalidade').value='<?php echo $arrVinculo[0]['fk_idFuncionalidade'];?>';document.getElementById('idItemFuncionalidade').value='<?php echo $arrVinculo[0]['fk_idItemFuncionalidade'];?>';document.getElementById('usuarioToken').value='<?php echo $arrVinculo[0]['loginUsuario'];?>';document.getElementById('documento').innerHTML='<?php echo $dados->getTituloTicket(); ?>';document.getElementById('oaSolicitante').innerHTML='<?php echo retornaNomeCompletoOrganismoAfiliado($dados->getFkIdOrganismoAfiliado()); ?>';document.getElementById('solicitante').innerHTML='<?php echo retornaNomeFormatado($arrVinculo[0]['nomeUsuario']);?>';document.getElementById('referenteMesAno').innerHTML='<?php echo $referenteMesAno;?>';document.getElementById('mesToken').value='<?php echo $mes;?>';document.getElementById('anoToken').value='<?php echo $ano;?>';document.getElementById('idOrganismoAfiliadoLiberarToken').value='<?php echo $dados->getFkIdOrganismoAfiliado();?>';escondeCamposTicket(<?php echo $arrVinculo[0]['fk_idFuncionalidade'];?>);" data-target="#mySeeToken" data-toggle="modal" data-placement="left">
                                                <span class="badge badge-info"><i class="fa fa-key"></i> Liberar token</span>
                                            </a>
                                        </dd>
                                        <?php }?>
                                        <dt>Última atualização:</dt> <dd><?php if($dados->getDataAtualizacaoTicket() != "00/00/0000 às 00:00"){ echo $dados->getDataAtualizacaoTicket();} else { echo "--"; } ?></dd>
                                        <dt><?php
                                            if($dados->getCriadoPorDepartTicket() == 1){
                                                echo "Organismo Afiliado";
                                            } else {
                                                echo "Depto de O.A. - GLP";
                                            }
                                            ?>:
                                        </dt>
                                        <dd class="project-people">
                                            <?php
                                            $resultadosCriadoPorDepart = $tc->listaEnvolvidos($id,$dados->getCriadoPorDepartTicket());
                                            if($resultadosCriadoPorDepart){
                                                foreach($resultadosCriadoPorDepart as $vetorCriadoPorDepart){
                                                    echo "<img alt='".$vetorCriadoPorDepart['nomeUsuario']."' title='".retornaNomeFormatado($vetorCriadoPorDepart['nomeUsuario'])."' class='img-circle' src='".validaAvatarUsuario($vetorCriadoPorDepart['avatarUsuario'])."'>";
                                                }
                                            } else { echo "--"; }
                                            ?>
                                        </dd>
                                        <dt><?php
                                            if($dados->getAtribuidoDepartTicket() == 1){
                                                echo "Organismo Afiliado";
                                            } else {
                                                echo "Depto de O.A. - GLP";
                                            }
                                            ?>:
                                        </dt>
                                        <dd class="project-people">
                                            <?php
                                            $resultadosAtribuidoDepart = $tc->listaEnvolvidos($id,$dados->getAtribuidoDepartTicket());
                                            if($resultadosAtribuidoDepart){
                                                foreach($resultadosAtribuidoDepart as $vetorAtribuidoDepart){
                                                    echo "<img alt='".$vetorAtribuidoDepart['nomeUsuario']."' title='".retornaNomeFormatado($vetorAtribuidoDepart['nomeUsuario'])."' class='img-circle' src='".validaAvatarUsuario($vetorAtribuidoDepart['avatarUsuario'])."'>";
                                                }
                                            } else { echo "--"; }
                                            ?>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <!-- Cabeçalho do Ticket FIM -->

                            <div class="row m-t-sm">
                                <div class="col-lg-12">

                                    <!-- Abas INÍCIO -->

                                    <div class="panel blank-panel">
                                        <div class="panel-heading">
                                            <div class="panel-options">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab-1" data-toggle="tab">Notificações</a></li>
                                                    <li><a href="#tab-2" data-toggle="tab">Enviar Notificação</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="panel-body">

                                            <div class="tab-content">

                                                <!-- Notificações do Ticket 01 INÍCIO -->
                                                <div class="tab-pane active" id="tab-1">
                                                    <div class="feed-activity-list">
                                                        <?php
                                                        $listaNotificacoes = $nc->retornaListaNotificacao($id);
                                                        if ($listaNotificacoes) {
                                                            foreach ($listaNotificacoes as $vetorNotificacoes) {
                                                                $dadosUsuarioNotificacao    = $puc->buscaUsuario($vetorNotificacoes['fk_seqCadastRemetente']);
                                                                $avatarUsuarioNotificacao   = $puc->listaAvatarUsuario($vetorNotificacoes['fk_seqCadastRemetente']);
                                                        ?>
                                                        <div class="feed-element">
                                                            <a class="pull-left">
                                                                <img alt="image" class="img-circle" src="<?php echo $avatarUsuarioNotificacao->getAvatarUsuario(); ?>">
                                                            </a>
                                                            <div class="media-body ">
                                                                <small class="pull-right text-navy"><?php echo timeAgo($vetorNotificacoes['dataNotificacao']); ?></small>
                                                                <strong>
                                                                    <?php echo retornaNomeFormatado($dadosUsuarioNotificacao->getNomeUsuario(),2); ?>
                                                                </strong>
                                                                publicou uma notificação para
                                                                <strong>
                                                                    <?php
                                                                        if($dadosUsuarioNotificacao->getFkIdDepartamento() == 1){
                                                                            echo "Departamento de Organismos Afiliados - GLP";
                                                                        } else {
                                                                            echo retornaNomeCompletoOrganismoAfiliado($dados->getFkIdOrganismoAfiliado());
                                                                        }
                                                                    ?>
                                                                </strong>.<br>
                                                                <small class="text-muted">
                                                                    <?php echo substr($vetorNotificacoes['dataNotificacao'],8,2)."/".
                                                                                substr($vetorNotificacoes['dataNotificacao'],5,2)."/".
                                                                                substr($vetorNotificacoes['dataNotificacao'],0,4) . " às " .
                                                                                substr($vetorNotificacoes['dataNotificacao'],11,5); ?>
                                                                </small>
                                                                <div class="well">
                                                                    <?php echo $vetorNotificacoes['mensagemNotificacao']; ?>
                                                                </div>
                                                                <div class="actions">
                                                                    <!--<a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> Ok </a>
                                                                    <a class="btn btn-xs btn-white"><i class="fa fa-mail-reply"></i> Responder</a>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <!-- Notificações do Ticket 01 FIM -->

                                                <!-- Cadastro de Notificações do Ticket 01 INÍCIO -->
                                                <div class="tab-pane" id="tab-2">
                                                    <!--
                                                    <div class="row">
                                                        <div class="form-group" style="margin-top: 10px;">
                                                            <label>Imóvel Avaliado? </label>
                                                            <div>
                                                                <select class="form-control col-sm-3" name="avaliacaoImovel" id="avaliacaoImovel" style="max-width: 500px">
                                                                    <option>Selecione...</option>
                                                                    <option value="0">Sim</option>
                                                                    <option value="1">Não</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group" style="margin-top: 10px;">
                                                            <label>Incluir outros oficiais desse organismo? </label>
                                                            <div>
                                                                <select class="form-control chosen-select col-sm-3" data-placeholder="Selecione novos participantes" multiple name="oficial-mensagem[]" id="oficial-mensagem" style="max-width: 500px; min-width: 500px">
                                                                    <option value="540135">Ewerton Schaedler Rosa [Mestre]</option>
                                                                    <option value="540135">Ewerton Schaedler Rosa [Mestre]</option>
                                                                    <option value="540135">Ewerton Schaedler Rosa [Mestre]</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    -->
                                                    <div class="row">
                                                        <div class="form-group" style="margin-top: 10px;">
                                                            <label>Mensagem:</label>
                                                            <div style="border: #ccc solid 1px">
                                                                <div class="summernote" id="mensagemNotificacao" name="mensagemNotificacao"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <input type="hidden" id="fk_seqCadastRemetente" name="fk_seqCadastRemetente" value="<?php echo $sessao->getValue("seqCadast"); ?>">
                                                        <input type="hidden" id="fk_idTicket" name="fk_idTicket" value="<?php echo $id; ?>">
                                                        <div class="text-center m-t-md">
                                                            <a onclick="cadastraNovaNotificacao()" class="btn btn-xs btn-primary">Enviar Notificação</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Cadastro de Notificações do Ticket 01 FIM -->

                                            </div>
                                        </div>
                                    </div>
                                    <!-- Abas FIM -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Detalhes do Ticket INÍCIO -->
            <div class="col-lg-3">
                <div class="wrapper wrapper-content project-manager">
                    <center><h4>Descrição do Ticket <?php echo "[ID: ".$id."]"; ?></h4></center>
                    <p class="small">
                        <?php echo $dados->getDescricaoTicket(); ?>
                    </p>
                    <br>
                    <center>
                    <p class="small font-bold">
                        <?php echo $prioridadeTicket; ?>
                    </p>
                    </center>
                    <br>
                    <center><h5>Tags</h5></center>
                    <ul class="tag-list" style="padding: 0; height: 60px">
                        <?php
                        $tags = [];
                        $tags = explode(" ", $dados->getTagTicket());
                        if($tags){
                            foreach($tags as $tagsVetor){
                                echo "<li><a><i class='fa fa-tag'></i> ".$tagsVetor."</a></li> ";
                            }
                        }
                        ?>
                    </ul>
                    <center><h5>Arquivos Anexados</h5></center>
                    <ul class="list-unstyled project-files" name="listaAnexos" id="listaAnexos">
                        <?php
                        $anexo = $tm->listaAnexo($id);
                        if($anexo){
                            foreach($anexo as $vetorAnexo){
                                $nameFile = str_split($vetorAnexo['nome_original'],30);
                                $nameAnexo = strlen($nameFile[0]) == 30 ? $nameFile[0]."..." : $nameFile[0];
                                echo "<a target='_BLANK' title='".$vetorAnexo['nome_original']."' href='" . $vetorAnexo['full_path'] . "'>". retornaIconeAnexo($vetorAnexo['ext']) . " " . $nameAnexo . "</a><a style='margin-right: 25px' onclick='excluiTicketAnexo(" . $vetorAnexo['idTicketAnexo'] . "," . $vetorAnexo['fk_idTicket'] . ");'>  <i class='fa fa-trash'></i></a><br>";
                            }
                        } else {
                            //echo "Nenhum anexo enviado!";
                        }
                        ?>
                    </ul>
                    <div class="text-center m-t-md">
                        <center>
                            <input name="file_upload_ticket_anexo" id="file_upload_ticket_anexo" type="file" />
                        </center>
                        <!--
                        <br><a href="#" class="btn btn-xs btn-primary">Adicionar Arquivo</a>
                        -->
                    </div>
                </div>
            </div>
            <!-- Detalhes do Ticket FIM -->

        </div>
<div class="modal inmodal" id="mySeeToken" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-unlock modal-icon"></i>
                <h4 class="modal-title">Liberar Token para Usuário</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-danger alert-dismissable">
                        <b>Solicitante:</b> <span id="solicitante"></span> <br>
                        <b>Funções:</b> <span id="funcoesOficial"></span> <br>
                        <b>Organismo:</b> <span id="oaSolicitante"></span> <br>
                        <b>Documento:</b> <span id="documento"></span> <span id="referenteMesAno"></span><br>
                        <b>Motivo:</b> <span id="motivo"></span> 
                    </div>
                    <div class="alert alert-warning alert-dismissable">
                       <table>
                            <tr id="tr_campos_mes">
                                <td width="90px"><br>Mês:</td>
                                <td>
                                    <br><input type="text" name="mesToken" id="mesToken">
                                </td>
                            </tr>
                            <tr id="tr_campos_ano">
                                <td width="90px"><br>Ano:</td>
                                <td>
                                    <br><input type="text" name="anoToken" id="anoToken">
                                </td>
                            </tr>
                       </table>
                        <br>
                        <table>
                            <tr>
                                <td width="90px">Login no SOA:</td>
                                <td><input type="text" name="usuarioToken" id="usuarioToken" /></td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Data Inicial:</td>
                                <td><br><input type="text" class="data" name="dataInicial" id="dataInicial" value="<?php echo date('d/m/Y');?>"></td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Data Final:</td>
                                <td>
                                    <br><input type="text" class="data" name="dataFinal" id="dataFinal">***Não esquecer de preencher
                                </td>
                            </tr>
                            
                            <tr>
                                <td></td>
                                <td>
                                    <br>
                                    <input type="hidden" name="idOrganismoAfiliadoLiberarToken" id="idOrganismoAfiliadoLiberarToken" value="<?php echo $idOrganismoAfiliado;?>"/>
                                    <input type="hidden" name="idFuncionalidade" id="idFuncionalidade" />
                                    <input type="hidden" name="idItemFuncionalidade" id="idItemFuncionalidade" />
                                    <button class="btn btn-sm btn-danger" type="button" onclick="liberarTokenUsuario();">
                                        <i class="fa fa-key"></i>Liberar
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title"><span id="tituloModal"></span> [ID: <span id="idDoItem"></span>]</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUploadJaEntregue"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

