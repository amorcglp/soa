<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
		<div class="col-lg-10">
			<h2>Recebimentos</h2>
			<ol class="breadcrumb">
				<li><a href="painelDeControle.php">Home</a>
				</li>
				<li><a href="painelDeControle.php">Financeiro</a>
				</li>
				<li class="active"><strong><a>Recebimentos</a> </strong>
				</li>
			</ol>
		</div>
	</div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de cadastro de Recebimentos</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaRecebimentos">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaAtaReuniaoMensal()" >
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAtaReuniaoMensal" onBlur="validaData(this.value, this);" maxlength="10" id="dataAtaReuniaoMensal" type="text" class="form-control" style="max-width: 102px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Valor R$: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="10" onkeypress="return SomenteNumero(event)" id="valorRecebimento" name="valorRecebimento" type="text" value="" style="max-width: 243px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Discriminação</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="discriminacaoRecebimento" id="discriminacaoRecebimento" style="max-width: 300px" required="required">
                                    <option value="">Selecione...</option>
                                    <option value="1">Mensalidade do Organismo</option>
                                    <option value="2">Donativos e Lei de AMRA</option>
                                    <option value="3">Bazar e Suprimentos</option>
                                    <option value="4">Comissões</option>
                                    <option value="5">Atividades Sociais</option>
                                    <option value="6">Recebimentos Diversos</option>
                                    <option value="7">Construção</option>
                                    <option value="8">Convenções e Jornadas</option>
                                    <option value="9">Receitas Financeiras</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Membro:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembroPresidente" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroPresidente" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="retornaDadosMembroPresidenteECompanheiro();">
                                
                                        <input type="checkbox" value="" name="companheiroMembroPresidente" id="companheiroMembroPresidente"> Companheiro
                                   
                            </div>
                        </div>
                        <input type="hidden" name="h_seqCadastMembroPresidente" id="h_seqCadastMembroPresidente">
                        <input type="hidden" name="h_nomeMembroPresidente" id="h_nomeMembroPresidente">
                        <input type="hidden" name="h_seqCadastCompanheiroMembroPresidente" id="h_seqCadastCompanheiroMembroPresidente">
                        <input type="hidden" name="h_nomeCompanheiroMembroPresidente" id="h_nomeCompanheiroMembroPresidente">
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar a Ata" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaAtaReuniaoMensal" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-times-circle modal-icon"></i>
                <h4 class="modal-title">Edição de ATA de Reunião Mensal do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <label>Motivo da edição:</label>
                <div style="border: #ccc solid 1px">
                    <div class="summernote"></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="atualizaMotivoEdicaoATA();">Editar ATA</button>
            </div>
        </div>
    </div>
</div>

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	