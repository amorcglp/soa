<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Informações do Organismo salvas com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Organismo já cadastrado anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Organismos</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Organismos Cadastrados</h5>
                    <div class="ibox-tools">
                    <?php if(in_array("1",$arrNivelUsuario)&&(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2))){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroOrganismo">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód. no SOA</th>
                                <th>Organismo</th>
                                <th><center>Região</center></th>
                                <th><center>Sigla</center></th>
                                <th><center>Pais</center></th>
                                <th><center>Status</center></th>
                                <th><center>CEP</center></th>
                                <th><center>Manual</center></th>
                                <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/organismoController.php");

                            $rrc = new organismoController();
                            $resultado = $rrc->listaOrganismo();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                	if((strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA")))||(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)))
                                	{
                                    ?>
                                    <tr>
                                        <td>
                                                <?php echo $vetor['idOrganismoAfiliado']; ?>
                                        </td>
                                        <td>
                                            <?php
                                            switch ($vetor['classificacaoOrganismoAfiliado']) {
                                                case 1:
                                                    echo "Loja";
                                                    $classificacao="Loja";
                                                    break;
                                                case 2:
                                                    echo "Pronaos";
                                                    $classificacao="Pronaos";
                                                    break;
                                                case 3:
                                                    echo "Capítulo";
                                                    $classificacao="Capítulo";
                                                    break;
                                                case 4:
                                                    echo "Heptada";
                                                    $classificacao="Heptada";
                                                    break;
                                                case 5:
                                                    echo "Atrium";
                                                    $classificacao="Atrium";
                                                    break;
                                            }
                                            ?>
                                            <?php
                                            switch ($vetor['tipoOrganismoAfiliado']) {
                                                case 1:
                                                    echo "R+C";
                                                    $tipo="R+C";
                                                    break;
                                                case 2:
                                                    echo "TOM";
                                                    $tipo="TOM";
                                                    break;
                                            }
                                            ?>
        									<?php echo $vetor['nomeOrganismoAfiliado']; ?>
                                        </td>
                                        <td>
                                            <center>
                                                <?php echo $vetor['regiaoRosacruz']; ?>
                                            </center>
                                        </td>
                                        <td>
			                                <center>
			        							<?php echo $vetor['siglaOrganismoAfiliado']; ?>
			                                </center>
		                                </td>
		                                <td>
                                            <center>
		                                    <?php
		                                    switch ($vetor['paisOrganismoAfiliado']) {
		                                        case 1:
		                                            echo "Brasil";
		                                            $pais="Brasil";
		                                            break;
		                                        case 2:
		                                            echo "Portugal";
		                                            $pais="Portugal";
		                                            break;
		                                        case 3:
		                                            echo "Angola";
		                                            $pais="Angola";
		                                            break;
		                                    }
		                                    ?>
                                            </center>
		                                </td>
		                                <td>
		                                <center>
		                                    <div id="statusTarget<?php echo $vetor['idOrganismoAfiliado'] ?>">
		                                        <?php
		                                        switch ($vetor['statusOrganismoAfiliado']) {
		                                            case 0:
		                                                echo "<span class='badge badge-primary'>Ativo</span>";
		                                                break;
		                                            case 1:
		                                                echo "<span class='badge badge-danger'>Inativo</span>";
		                                                break;
		                                        }
		                                        ?>
		                                    </div>
		                                </center>
		                                </td>
		                                <td>
		                                <center>
		        							<?php echo $vetor['cepOrganismoAfiliado'] ; ?>
		                                </center>
		                                </td>
                                        <?php
                                        if ((strtoupper($vetor["siglaOrganismoAfiliado"]) == strtoupper($sessao->getValue("siglaOA"))) || (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2))) {
                                            ?>
                                            <td>
                                                <center>
                                                    <a href="testes/gerarMarcaDagua.php?id=<?php echo $vetor['idOrganismoAfiliado'];?>&classificacao=<?php echo $classificacao;?>&nome=<?php echo $classificacao . " R+C " . $vetor['nomeOrganismoAfiliado']; ?>&enviaRequisicao=true">
                                                        <button class="btn btn-outline btn-primary dim" type="button"><i
                                                                    class="fa fa-file-pdf-o"></i></button>
                                                    </a>
                                                </center>
                                            </td>
                                            <?php
                                        }
                                        ?>
		                                <td><center>
		                                    <div id="acoes_confirma_cancela">
		                                        <a class="btn btn-sm btn-success" href="#" onclick="mostraDetalhesOrganismo('<?php echo $vetor['idOrganismoAfiliado']; ?>','<?php echo $classificacao." ".$tipo." ".$vetor['nomeOrganismoAfiliado'];?>','<?php echo $vetor['siglaOrganismoAfiliado']; ?>','<?php echo $vetor['paisOrganismoAfiliado'];?>','<?php echo $pais;?>');" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Detalhes dos OA's">
		                                                <i class="icon-edit icon-white"></i>  
		                                                Detalhes                                            
		                                        </a>
		                                        <?php if(in_array("2",$arrNivelUsuario)){?>
		                                        <a class="btn btn-sm btn-info" href="?corpo=alteraOrganismo&idOrganismoAfiliado=<?php echo $vetor['idOrganismoAfiliado']; ?>" data-rel="tooltip" title="">
		                                            <i class="icon-edit icon-white"></i>  
		                                            Editar                                            
		                                        </a>
		                                        <?php }?>
		                                        <?php if(in_array("3",$arrNivelUsuario)){?>
		                                        <span id="status<?php echo $vetor['idOrganismoAfiliado'] ?>">
		                                            <?php if ($vetor['statusOrganismoAfiliado'] == 1) { ?>
		                                                <a class="btn btn-sm btn-primary" onclick="mudaStatus('<?php echo $vetor['idOrganismoAfiliado'] ?>', '<?php echo $vetor['statusOrganismoAfiliado'] ?>', 'organismo')" data-rel="tooltip" title="Ativar"> 
		                                                    Ativar                                            
		                                                </a>
		                                            <?php } else { ?>
		                                                <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idOrganismoAfiliado'] ?>', '<?php echo $vetor['statusOrganismoAfiliado'] ?>', 'organismo')" data-rel="tooltip" title="Inativar">
		                                                    Inativar                                            
		                                                </a>
		        									<?php } ?>
		                                        </span>
		                                        <?php }?>
		                                    </div>
		                                </center>
		                                </td>
                                	</tr>
    <?php
                                }
    }
}
?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes do Organismo</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Organismo:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="nomeOrganismoAfiliado"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Situação:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="situacao"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Sigla:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="siglaOrganismoAfiliado"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Código de Afiliação:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="codigoAfiliacao"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Região:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="regiao"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">País:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="pais"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Estado:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="estado"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cidade:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="cidade"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bairro:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="bairro"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">CEP:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="cep"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Endereço:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="endereco"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nº:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="numero"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Complemento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="complemento"></span></div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">E-mail:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="email"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">CNPJ:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="cnpj"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Celular:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="celular"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Telefone Fixo:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="telefoneFixo"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Outro Telefone:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="outroTelefone"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Fax:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="fax"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Caixa Postal:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="caixaPostal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cep Caixa Postal:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="cepCaixaPostal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Endereço para Correspondência:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="enderecoCorrespondencia"></span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Conteúdo DE INCLUDE FIM -->

<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

        /* Init DataTables */
        var oTable = $('#editable').dataTable();

        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });


    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row"]);

    }
</script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>		