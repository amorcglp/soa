<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Galeria de Imagens dos Imóveis</h2>
        <ol class="breadcrumb">
            <li>
                <a href="?corpo=buscaImovel">Início</a>
            </li>
            <li>
                <a>Imóveis</a>
            </li>
            <li class="active">
                <a href=""><strong>Galeria de Imagens dos Imóveis</strong></a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <br>
                    <center>
                        <div class="center">
                            <span class="alert alert-info">Essa galeria só funciona nos navegadores: Internet Explorer e Google Chrome</span>
                        </div>
                    </center>
                    <br><br>
                    <center>
                        <?php
                        include_once("controller/imovelController.php");
                        require_once ("model/imovelClass.php");

                        $ic = new imovelController();
                        $dadosImovel = $ic->buscaImovel($_GET['idImovel']);
                        ?>
                        <h3>
                            Organismo Afiliado
                            <?php
                            include_once("controller/organismoController.php");
                            require_once ("model/organismoClass.php");

                            $oc = new organismoController();
                            $dadosOrganismo = $oc->buscaOrganismo($dadosImovel->getFk_idOrganismoAfiliado());
                            switch ($dadosOrganismo->getClassificacaoOrganismoAfiliado()) {
                                case 1: echo "Loja ";
                                    break;
                                case 2: echo "Pronaos ";
                                    break;
                                case 3: echo "Capítulo ";
                                    break;
                                case 4: echo "Heptada ";
                                    break;
                                case 5: echo "Atrium ";
                                    break;
                            }
                            switch ($dadosOrganismo->getTipoOrganismoAfiliado()) {
                                case 1: echo "R+C ";
                                    break;
                                case 2: echo "TOM ";
                                    break;
                            }
                            echo $dadosOrganismo->getNomeOrganismoAfiliado() . " - " . $dadosOrganismo->getSiglaOrganismoAfiliado();
                            ?>
                        </h3>
                        <h2>
                            Galeria de Imagens do Imóvel
                            <strong>
                            <?php echo $dadosImovel->getEnderecoImovel() . ' - ' . $dadosImovel->getNumeroImovel() . ' - ' . $dadosImovel->getComplementoImovel(); ?>
                            </strong>
                        </h2>
                    </center>
                    <br>
                    <br>
                    <center>
                        <div class="lightBoxGallery" id="lightBoxGallery">
                            <?php
                            include_once("model/imovelClass.php");

                            $im = new imovel();
                            $resultado = $im->listaImovelGaleria($_GET['idImovel']);

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <a style="margin: 5px" href="<?php echo $vetor['full_path'] ?>" title="teste" data-gallery="">
                                        <img  style="width: 150px; margin-bottom: 20px" src="img/imovel/galeria/<?php echo $vetor['nome_arquivo'] ?>">
                                        <a style="margin-right: 25px" onclick="excluiImovelGaleria(<?php echo $vetor['idImovelGaleria']; ?>,<?php echo $_GET['idImovel']; ?>);">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </a>
                                    <?php
                                }
                            } else {
                                ?>
                                <center>
                                    <i>Nenhuma imagem cadastrada</i>
                                </center>
                                <?php
                            }
                            ?>
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>
                        </div>
                    </center>
                    <br>
                    <br>
                    <div class="row">
                        <center>
                        <?php if (in_array("4", $arrNivelUsuario)) { ?>
                                <div class="col-sm-6">
                                    <a class="btn btn-primary btn-rounded btn-block" href="" data-toggle="modal" data-target="#myModaCadastrarNovasFotos"><i class="fa fa-plus"></i> Publicar Fotos</a>
                                </div>
                        <?php } ?>
                            <div class="col-sm-<?php if (in_array("4", $arrNivelUsuario)) {
                            echo 6;
                        } else {
                            echo 12;
                        } ?>" >
                                <a class="btn btn-warning btn-rounded btn-block" href="?corpo=buscaImovel"><i class="fa fa-reply"></i> Voltar para a área de Imóveis</a>
                            </div>
                        </center>
                    </div>
                </div>
                <input type="hidden" name="idImovel" id="idImovel" value="<?php echo $_GET['idImovel']; ?>" />
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->
<!-- Modal de detalhes do usuário -->
<div class="modal inmodal" id="myModaCadastrarNovasFotos" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Publicar Novas Fotos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <center>
                        <div class="center"><br>
                            <span class="alert alert-danger">Depois de carregar as imagens feche essa janela para visualizar o resultado!</span>
                        </div>
                    </center>
                </div>
                <div class="row">
                    <center>
                        <div class="center"><br><br><br>
                            <span class="alert alert-info">***Obs. Não é preciso clicar em "salvar" ou "upload"</span>
                        </div>
                    </center>
                </div>
                <div class="row">
                    <div align="center"><br><br>
                        <input name="file_upload_imovel_galeria" id="file_upload_imovel_galeria" type="file"/>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<!-- Conteúdo DE INCLUDE FIM -->
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
				