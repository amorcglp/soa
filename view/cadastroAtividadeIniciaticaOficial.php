<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades Iniciáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeIniciaticaEquipe">Equipes de Atividades Iniciáticas</a>
            </li>
            <li class="active">
                <a>Cadastro de Equipes de Atividades Iniciáticas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php $idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';?>
                    <h5>Cadastro de Equipes de Atividades Iniciáticas</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciaticaOficial<?php if($idOrganismo!=''){ echo '&idOrganismoAfiliado='.$idOrganismo;} ?>">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <!--
                    <div class="row">
                        <div class="alert alert-success col-sm-offset-3 col-sm-6">
                            <center>
                            Olá, essa funcionalidade serve para alertar a existência dessa equipe para o sistema.
                            </center>
                        </div>
                    </div>
                    -->
                    <form class="form-horizontal" onsubmit="return verificaCamposPreenchidosAtivOficiaisSubmit();" enctype="multipart/form-data" method="post" action="acoes/acaoCadastrar.php">
                        <div class="form-group form-inline"><label class="col-sm-3 control-label">Organismo Afiliado: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-5 chosen-select" onchange="mudaOrganismoGetAno(this.value);" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." style="max-width: 350px" required="required">
                                    <option value="0">Selecione...</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    require_once("model/criaSessaoClass.php");
                                    $oc = new organismoController();
                                    $sessao = new criaSessao();

                                    if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){
                                        $oc->criarComboBox($idOrganismo);
                                    } else {
                                        $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    }
                                    ?>
                                	</select>
                            </div>
                        </div>
                        <div class="form-group" id="anoAtivInicOficial">
                            <label class="col-sm-3 control-label">Ano R+C: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3 chosen-select" onchange="mudaAnoGetEquipe(this.value);" name="anoAtividadeIniciaticaOficial" id="anoAtividadeIniciaticaOficial" style="max-width: 250px" required="required">
                                    <option value="0">Selecione...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="tipoAtivInicOficial">
                            <label class="col-sm-3 control-label">Equipe: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" onchange="mudaTipoEquipeGetEquipe(this.value);" name="tipoAtividadeIniciaticaOficial" id="tipoAtividadeIniciaticaOficial" style="max-width: 250px" required="required">
                                    <option value="0">Selecione...</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Mestre: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoMestre" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeMestre" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Mestre','mestreAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMestre','nomeMestre','codAfiliacaoMestre','nomeMestre','mestreAtividadeIniciaticaOficial','h_nomeMestre','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" name="tipo" id="tipo" value="1">
                            <input type="hidden" maxlength="50" name="mestreAtividadeIniciaticaOficial" id="mestreAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastMestre" id="h_seqCadastMestre">
                            <input type="hidden" name="h_nomeMestre" id="h_nomeMestre">
                            <input type="hidden" name="h_seqCadastCompanheiroMestre" id="h_seqCadastCompanheiroMestre">
                            <input type="hidden" name="h_nomeCompanheiroMestre" id="h_nomeCompanheiroMestre">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Mestre Auxiliar: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoMestreAux" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeMestreAux" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('MestreAux','mestreAuxAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMestreAux','nomeMestreAux','codAfiliacaoMestreAux','nomeMestreAux','mestreAuxAtividadeIniciaticaOficial','h_nomeMestreAux','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="mestreAuxAtividadeIniciaticaOficial" id="mestreAuxAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastMestreAux" id="h_seqCadastMestreAux">
                            <input type="hidden" name="h_nomeMestreAux" id="h_nomeMestreAux">
                            <input type="hidden" name="h_seqCadastCompanheiroMestreAux" id="h_seqCadastCompanheiroMestreAux">
                            <input type="hidden" name="h_nomeCompanheiroMestreAux" id="h_nomeCompanheiroMestreAux">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Arquivista: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoArquivista" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeArquivista" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Arquivista','arquivistaAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoArquivista','nomeArquivista','codAfiliacaoArquivista','nomeArquivista','arquivistaAtividadeIniciaticaOficial','h_nomeArquivista','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="arquivistaAtividadeIniciaticaOficial" id="arquivistaAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastArquivista" id="h_seqCadastArquivista">
                            <input type="hidden" name="h_nomeArquivista" id="h_nomeArquivista">
                            <input type="hidden" name="h_seqCadastCompanheiroArquivista" id="h_seqCadastCompanheiroArquivista">
                            <input type="hidden" name="h_nomeCompanheiroArquivista" id="h_nomeCompanheiroArquivista">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Capelão: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoCapelao" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeCapelao" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Capelao','capelaoAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoCapelao','nomeCapelao','codAfiliacaoCapelao','nomeCapelao','capelaoAtividadeIniciaticaOficial','h_nomeCapelao','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="capelaoAtividadeIniciaticaOficial" id="capelaoAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastCapelao" id="h_seqCadastCapelao">
                            <input type="hidden" name="h_nomeCapelao" id="h_nomeCapelao">
                            <input type="hidden" name="h_seqCadastCompanheiroCapelao" id="h_seqCadastCompanheiroCapelao">
                            <input type="hidden" name="h_nomeCompanheiroCapelao" id="h_nomeCompanheiroCapelao">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Matre: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoMatre" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeMatre" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Matre','matreAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMatre','nomeMatre','codAfiliacaoMatre','nomeMatre','matreAtividadeIniciaticaOficial','h_nomeMatre','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="matreAtividadeIniciaticaOficial" id="matreAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastMatre" id="h_seqCadastMatre">
                            <input type="hidden" name="h_nomeMatre" id="h_nomeMatre">
                            <input type="hidden" name="h_seqCadastCompanheiroMatre" id="h_seqCadastCompanheiroMatre">
                            <input type="hidden" name="h_nomeCompanheiroMatre" id="h_nomeCompanheiroMatre">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Grande Sacerdotisa: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoGrandeSacerdotisa" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeGrandeSacerdotisa" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('GrandeSacerdotisa','grandeSacerdotisaAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGrandeSacerdotisa','nomeGrandeSacerdotisa','codAfiliacaoGrandeSacerdotisa','nomeGrandeSacerdotisa','grandeSacerdotisaAtividadeIniciaticaOficial','h_nomeGrandeSacerdotisa','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="grandeSacerdotisaAtividadeIniciaticaOficial" id="grandeSacerdotisaAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastGrandeSacerdotisa" id="h_seqCadastGrandeSacerdotisa">
                            <input type="hidden" name="h_nomeGrandeSacerdotisa" id="h_nomeGrandeSacerdotisa">
                            <input type="hidden" name="h_seqCadastCompanheiroGrandeSacerdotisa" id="h_seqCadastCompanheiroGrandeSacerdotisa">
                            <input type="hidden" name="h_nomeCompanheiroGrandeSacerdotisa" id="h_nomeCompanheiroGrandeSacerdotisa">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Guia: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoGuia" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeGuia" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Guia','guiaAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGuia','nomeGuia','codAfiliacaoGuia','nomeGuia','guiaAtividadeIniciaticaOficial','h_nomeGuia','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="guiaAtividadeIniciaticaOficial" id="guiaAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastGuia" id="h_seqCadastGuia">
                            <input type="hidden" name="h_nomeGuia" id="h_nomeGuia">
                            <input type="hidden" name="h_seqCadastCompanheiroGuia" id="h_seqCadastCompanheiroGuia">
                            <input type="hidden" name="h_nomeCompanheiroGuia" id="h_nomeCompanheiroGuia">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Guardião Interno: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoGuardiaoInterno" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeGuardiaoInterno" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('GuardiaoInterno','guardiaoInternoAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGuardiaoInterno','nomeGuardiaoInterno','codAfiliacaoGuardiaoInterno','nomeGuardiaoInterno','guardiaoInternoAtividadeIniciaticaOficial','h_nomeGuardiaoInterno','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="guardiaoInternoAtividadeIniciaticaOficial" id="guardiaoInternoAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastGuardiaoInterno" id="h_seqCadastGuardiaoInterno">
                            <input type="hidden" name="h_nomeGuardiaoInterno" id="h_nomeGuardiaoInterno">
                            <input type="hidden" name="h_seqCadastCompanheiroGuardiaoInterno" id="h_seqCadastCompanheiroGuardiaoInterno">
                            <input type="hidden" name="h_nomeCompanheiroGuardiaoInterno" id="h_nomeCompanheiroGuardiaoInterno">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Guardião Externo: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoGuardiaoExterno" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeGuardiaoExterno" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('GuardiaoExterno','guardiaoExternoAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGuardiaoExterno','nomeGuardiaoExterno','codAfiliacaoGuardiaoExterno','nomeGuardiaoExterno','guardiaoExternoAtividadeIniciaticaOficial','h_nomeGuardiaoExterno','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="guardiaoExternoAtividadeIniciaticaOficial" id="guardiaoExternoAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastGuardiaoExterno" id="h_seqCadastGuardiaoExterno">
                            <input type="hidden" name="h_nomeGuardiaoExterno" id="h_nomeGuardiaoExterno">
                            <input type="hidden" name="h_seqCadastCompanheiroGuardiaoExterno" id="h_seqCadastCompanheiroGuardiaoExterno">
                            <input type="hidden" name="h_nomeCompanheiroGuardiaoExterno" id="h_nomeCompanheiroGuardiaoExterno">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Portador do Archote: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoPortadorArchote" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomePortadorArchote" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('PortadorArchote','portadorArchoteAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoPortadorArchote','nomePortadorArchote','codAfiliacaoPortadorArchote','nomePortadorArchote','portadorArchoteAtividadeIniciaticaOficial','h_nomePortadorArchote','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="portadorArchoteAtividadeIniciaticaOficial" id="portadorArchoteAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastPortadorArchote" id="h_seqCadastPortadorArchote">
                            <input type="hidden" name="h_nomePortadorArchote" id="h_nomePortadorArchote">
                            <input type="hidden" name="h_seqCadastCompanheiroPortadorArchote" id="h_seqCadastCompanheiroPortadorArchote">
                            <input type="hidden" name="h_nomeCompanheiroPortadorArchote" id="h_nomeCompanheiroPortadorArchote">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Medalhista: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoMedalhista" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeMedalhista" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Medalhista','medalhistaAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMedalhista','nomeMedalhista','codAfiliacaoMedalhista','nomeMedalhista','medalhistaAtividadeIniciaticaOficial','h_nomeMedalhista','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="medalhistaAtividadeIniciaticaOficial" id="medalhistaAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastMedalhista" id="h_seqCadastMedalhista">
                            <input type="hidden" name="h_nomeMedalhista" id="h_nomeMedalhista">
                            <input type="hidden" name="h_seqCadastCompanheiroMedalhista" id="h_seqCadastCompanheiroMedalhista">
                            <input type="hidden" name="h_nomeCompanheiroMedalhista" id="h_nomeCompanheiroMedalhista">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Arauto: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoArauto" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeArauto" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Arauto','arautoAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoArauto','nomeArauto','codAfiliacaoArauto','nomeArauto','arautoAtividadeIniciaticaOficial','h_nomeArauto','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="arautoAtividadeIniciaticaOficial" id="arautoAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastArauto" id="h_seqCadastArauto">
                            <input type="hidden" name="h_nomeArauto" id="h_nomeArauto">
                            <input type="hidden" name="h_seqCadastCompanheiroArauto" id="h_seqCadastCompanheiroArauto">
                            <input type="hidden" name="h_nomeCompanheiroArauto" id="h_nomeCompanheiroArauto">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Adjutor: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoAdjutor" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeAdjutor" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Adjutor','adjutorAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoAdjutor','nomeAdjutor','codAfiliacaoAdjutor','nomeAdjutor','adjutorAtividadeIniciaticaOficial','h_nomeAdjutor','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="adjutorAtividadeIniciaticaOficial" id="adjutorAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastAdjutor" id="h_seqCadastAdjutor">
                            <input type="hidden" name="h_nomeAdjutor" id="h_nomeAdjutor">
                            <input type="hidden" name="h_seqCadastCompanheiroAdjutor" id="h_seqCadastCompanheiroAdjutor">
                            <input type="hidden" name="h_nomeCompanheiroAdjutor" id="h_nomeCompanheiroAdjutor">
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Sonoplasta: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoSonoplasta" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeSonoplasta" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="getOficialIniciaticoResponsavel('Sonoplasta','sonoplastaAtividadeIniciaticaOficial');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoSonoplasta','nomeSonoplasta','codAfiliacaoSonoplasta','nomeSonoplasta','sonoplastaAtividadeIniciaticaOficial','h_nomeSonoplasta','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" maxlength="50" name="sonoplastaAtividadeIniciaticaOficial" id="sonoplastaAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastSonoplasta" id="h_seqCadastSonoplasta">
                            <input type="hidden" name="h_nomeSonoplasta" id="h_nomeSonoplasta">
                            <input type="hidden" name="h_seqCadastCompanheiroSonoplasta" id="h_seqCadastCompanheiroSonoplasta">
                            <input type="hidden" name="h_nomeCompanheiroSonoplasta" id="h_nomeCompanheiroSonoplasta">
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Anotações: </label>
                            <div class="col-sm-9">
                                <div style="border: #ccc solid 1px; max-width: 800px">
                                    <div class="mail-text h-200">
                                        <textarea class="summernote" id="anotacoesAtividadeIniciaticaOficial" name="anotacoesAtividadeIniciaticaOficial"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar"" />
                                <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {?>
                                    <?php if($idOrganismo!=''){ ?>
                                        <a class="btn btn-white" href="?corpo=buscaAtividadeIniciaticaOficial&idOrganismoAfiliado=<?php echo $idOrganismo ?>" id="cancelar" name="cancelar">Cancelar</a>
                                    <?php } else { ?>
                                        <a class="btn btn-white" href="?corpo=buscaAtividadeIniciatica" id="cancelar" name="cancelar">Cancelar</a>
                                    <?php } ?>
                                <?php } else { ?>
                                    <a class="btn btn-white" href="?corpo=buscaAtividadeIniciaticaOficial" id="cancelar" name="cancelar">Cancelar</a>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo de Modal Início -->

<!-- Modal Cadastrar Tipo de Escritura Início -->
<div class="modal inmodal fade" id="modalTipoDeEscritura" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Cadastrar Novo Tipo de Escritura</h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label class="col-sm-2 control-label">Tipo de Escritura: </label>
                        <div class="col-sm-10">
                            <input type="text" name="tipoEscritura" id="tipoEscritura" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12" id="alertTipoDeEscritura">
                        <div class="alert alert-info" style="text-align: justify">
                            <center>
                            Contribua com um novo <a class="alert-link">Tipo de Escritura</a>.
                            O qual será visível por todos!
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal" onclick="trocaAlertTiposDeEscritura();">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="cadastraTipoDeEscritura();">Salvar nova opção</button>
            </div>
        </div>
    </div>
</div>

<!-- Conteúdo de Modal Fim -->
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>		