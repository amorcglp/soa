<!-- ConteÃºdo DE INCLUDE INÃ�CIO -->
<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once ('model/isencaoOAClass.php');
include_once ('model/mensalidadeOAClass.php');
include_once ('model/membroOAClass.php');
include_once ('model/organismoClass.php');
include_once ('lib/functions.php');

//Atualizar membro como falecido
$membroAindaVivo    = isset($_GET['membroAindaVivo'])?$_GET['membroAindaVivo']:NULL;
$falecimento        = isset($_GET['falecimento'])?$_GET['falecimento']:NULL;
$seqCadastMembroOa  = isset($_GET['seqCadastMembroOa'])?$_GET['seqCadastMembroOa']:NULL;
$salvo              = 0;
$salvoMembroAindaVivo=0;

if($falecimento==1)
{
    $membroOa = new membroOA();
    $membroOa->setSeqCadastMembroOa($seqCadastMembroOa);
    if($membroOa->atualizaFalecimento()&&$membroOa->verificaSeJaCadastrado($seqCadastMembroOa))
    {
        $salvo=1;
    }
}

if($membroAindaVivo==1)
{
    $membroOa = new membroOA();
    $membroOa->setSeqCadastMembroOa($seqCadastMembroOa);
    if($membroOa->atualizaNaoFalecimento())
    {
        $salvoMembroAindaVivo=1;
    }
}



if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Membro Falecido incluído com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($salvoMembroAindaVivo==1){?>
    <script>
        window.onload = function(){
            swal({
                title: "Sucesso!",
                text: "Membro reincluído na lista de membros do OA com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if($seqCadastMembroOa!=null) {
    if (!$membroOa->verificaSeJaCadastrado($seqCadastMembroOa)&&$salvo==1) {
        ?>
        <script>
            window.onload = function () {
                swal({
                    title: "Aviso!",
                    text: "Membro Falecido não cadastrado!",
                    type: "warning",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
    <?php } ?>

    <?php
    if ($salvo == 2) {
        ?>
        <script>
            window.onload = function () {
                swal({
                    title: "Aviso!",
                    text: "Membro não possui afiliação R+C!",
                    type: "warning",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
    <?php }
}


?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( document ).tooltip();
  });
  </script>

<!-- Caminho de Migalhas InÃ­cio -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Membros Falecidos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaFalecimento">Membros Falecidos</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela InÃ­cio -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Membros Falecidos</h5>
                    <div class="ibox-tools">
                   
                    <a class="btn btn-xs btn-success" style="color: white" href="" data-toggle="modal" data-target="#myModal2">
                         <i class="fa fa-search-plus"></i> Legenda dos Status de Situação
                    </a>

                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroFalecimento">
                            <i class="fa fa-plus"></i> Falecimento
                        </a>
                    <?php }?>
                    </div>
                </div>

                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód. de Afiliação</th>
                                <th>Nome do Membro</th>
                                <th>Organismo</th>
                                <th>Situação no OA</th>
                                <th>Situação Cadastral na GLP</th>
                                <th>Envio de Monografia</th>
		                        <th>Data de Quitação</th>
		                        <th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/membroOAController.php");

                            $m = new membroOAController();
                            $resultado = $m->listaMembroOA(null,1);
							
                            if ($resultado) {
                                foreach ($resultado as $vetor) {

                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                            ?>
                               <?php //if(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA"))){
                                         //Verificar se estÃ¡ isento, inativo ou ativo no OA
                                         $isencao = new isencaoOA();
                                         $resultadoIsencao = $isencao->verificaIsencaoOaMembroHoje($vetor["seqCadastMembroOa"],$vetor["fk_idOrganismoAfiliado"]);
                                         $totalIsencao=0;
                                         if($resultadoIsencao)
                                         {
                                             $totalIsencao = count($resultadoIsencao);
                                         }
                                         //echo $totalIsencao;
                                         $mensalidade = new mensalidadeOA();
                                         /*
                                         $dataAnterior = date('d/m/Y', strtotime('-1 months', strtotime(date('Y-m-d'))));
                                         $mesAnterior = substr($dataAnterior,3,2);
                                         $anoAnterior = substr($dataAnterior,6,4);
                                          */
                                         $mesAnterior = date('m');
                                         $anoAnterior = date('Y');
                                         $resultadoSituacao = $mensalidade->verificaMensalidadeOA($vetor["seqCadastMembroOa"],$mesAnterior,$anoAnterior,$vetor["fk_idOrganismoAfiliado"]);
                                         
                                         //Extrair primeiro nome
                                         $arrNome = explode(" ",$vetor['nomeMembroOa']);
                                         $primeiroNome = $arrNome[0];
                                         
                                         $ocultar_json=1;
                                         $codigoAfiliacao=$vetor['codigoAfiliacao'];
                                         $nomeMembro=$vetor['nomeMembroOa'];
                                         $tipoMembro=1;
                                         if(!isset($_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome]))
                                         {
                                             $_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome]=array();
                                             //Verificar situaÃ§Ã£o na GLP
                                             
                                             include './js/ajax/retornaDadosMembro.php';
                                             $_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome] = json_decode(json_encode($return),true);
                                         }
                                         //echo "<pre>";print_r($_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome]);
                               ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['codigoAfiliacao']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['nomeMembroOa']; ?>
                                        </td>

                                        <td>
                                            <?php echo $classificacao." ".$tipo." ".$vetor['nomeOrganismoAfiliado'];?>
                                        </td>

                                        <td>
                                        	<?php 
                                         $ativoAte="";
                                         if($totalIsencao>0){
                                             $dataInicial="";
                                             $dataFinal="";
                                             foreach($resultadoIsencao as $vetorIsencao)
                                             {
                                                 $dataInicial = substr($vetorIsencao['dataInicial'],8,2)."/".substr($vetorIsencao['dataInicial'],5,2)."/".substr($vetorIsencao['dataInicial'],0,4);
                                                 $dataFinal = substr($vetorIsencao['dataFinal'],8,2)."/".substr($vetorIsencao['dataFinal'],5,2)."/".substr($vetorIsencao['dataFinal'],0,4);
                                             }	
                                            ?>
                                        				<a href="#" style="color: white" data-toggle="tooltip"
														data-placement="bottom" data-html="true" title="Desde <?php echo $dataInicial;?> até <?php echo $dataFinal;?>"
														data-original-title="Tooltip on bottom">
                                        				<?php  
                                             echo "<span class=\"badge badge-plain\">Isento</span>";
                                                        ?>
                                        				</a>
                                        				<?php 
                                         }else{
                                             if($resultadoSituacao){ 
                                                 $resultadoAtivoAte = $mensalidade->verificaAtivoAte($vetor["seqCadastMembroOa"],$vetor["fk_idOrganismoAfiliado"]);
                                                 
                                                 if($resultadoAtivoAte)
                                                 {
                                                     foreach($resultadoAtivoAte as $vetorAtivoAte)
                                                     {
                                                         $ativoAte = mesExtensoPortugues($vetorAtivoAte['mes'])."/".$vetorAtivoAte['ano'];
                                                     }
                                                 }	
                                                        ?>
	                                        				<a href="#" style="color: white" data-toggle="tooltip"
															data-placement="bottom" data-html="true" title="Até <?php echo $ativoAte;?>"
															data-original-title="Tooltip on bottom">
	                                        				<?php  
                                                 echo "<span class=\"badge badge-primary\">Ativo</span>";
                                                            ?>
	                                        				</a>
	                                        				<?php 
                                             }else{
                                                 $resultadoAtivoAte = $mensalidade->verificaAtivoAte($vetor["seqCadastMembroOa"],$vetor["fk_idOrganismoAfiliado"]);
                                                 //echo "<pre>";print_r($resultadoAtivoAte);
                                                 $inativoDesde="";
                                                 $tM=0;
                                                 if($resultadoAtivoAte)
                                                 {
                                                     foreach($resultadoAtivoAte as $vetorAtivoAte)
                                                     {
                                                         if($vetorAtivoAte['mes']==12)
                                                         {
                                                             $tM = 1;
                                                             $tA=$vetorAtivoAte['ano']+1;
                                                         }else{    
                                                            $tM = $vetorAtivoAte['mes']+1;
                                                            $tA=$vetorAtivoAte['ano'];
                                                         }
                                                         $inativoDesde = mesExtensoPortugues($tM)."/".$tA;
                                                     }
                                                 }else{
                                                     $inativoDesde="Sempre";
                                                 }
                                                 if($resultadoAtivoAte)
                                                 {
                                                     if(count($resultadoAtivoAte))
                                                     {
                                                         if(($vetorAtivoAte['mes']+1)!=date('m')){
                                                            ?>
                                                                                        <a href="#" style="color: white" data-toggle="tooltip"
                                                                                                                                data-placement="bottom" data-html="true" title="Desde <?php echo $inativoDesde;?>"
                                                                                                                                data-original-title="Tooltip on bottom">
                                                                                        <?php 
                                                                                        echo "<span class=\"badge badge-warning\">Aguardando Pagamento</span>";
                                                                                        ?>
                                                                                        </a>
                                                                                        <?php
                                                         }else{
                                                                                        ?>
                                                                                        <a href="#" style="color: white" data-toggle="tooltip"
                                                                                                                                data-placement="bottom" data-html="true" title="Desde <?php echo $inativoDesde;?>"
                                                                                                                                data-original-title="Tooltip on bottom">
                                                                                        <?php 
                                                                                        echo "<span class=\"badge badge-danger\">Inativo</span>";
                                                                                        ?>
                                                                                        </a>
                                                                                        
                                                                                        <?php
                                                         }
                                                     }
                                                 }else{
                                                     echo "<span class=\"badge badge-warning\">Aguardando registro de quitação</span>";
                                                 }
                                                 
                                             }
                                         }?>
                                        			
                                        </td>
                                        <td>
 	                                       	<?php 
                                         if(isset($_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome])){
                                             echo situacaoCadastral($_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome]['result'][0]['fields']['fIdeTipoSituacMembroRosacr']);	
                                         }   		                                    	
                                                ?>
                                        </td>
                                        <td>
 	                                       	<?php 
                                         if(isset($_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome])){
                                             echo situacaoRemessa($_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome]['result'][0]['fields']['fIdeTipoSituacRemessRosacr']);	
                                         }   		                                    	
                                                ?>
                                        </td>
                                        <td>
                                            <?php 
                                         $dataTemp = $_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome]['result'][0]['fields']['fDatQuitacRosacr']; 
                                         echo substr($dataTemp,8,2)."/".substr($dataTemp,5,2)."/".substr($dataTemp,0,4);
                                            ?>
                                        </td>
		                                <td>
			                                <center>
			                                    <button type="button" class="btn btn-sm btn-info" onclick="consultaMembroOA('<?php echo $primeiroNome;?>','<?php echo $vetor['codigoAfiliacao']; ?>');" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Detalhes">
			                                        <i class="fa fa-search-plus fa-white"></i>&nbsp;
			                                        Detalhes
			                                    </button>
                                                <a href="?corpo=buscaFalecimento&membroAindaVivo=1&seqCadastMembroOa=<?php echo $vetor['seqCadastMembroOa']; ?>" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-minus fa-white"></i>&nbsp;
                                                    Excluir membro falecido
                                                </a>
			                                </center>
		                                </td>
                                	</tr>
                            	<?php
                                     }
                                //}
                            }
                                ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
            $(document).on('ready', function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
</script>          

<!-- Tabela Fim -->

<!-- Window MODAL InÃ­cio -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes do Membro do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <table cellpadding="10">
						<tbody>
							<tr>
								<td width="35%">Nome:</td><td><span id="m_nome"></span></td>
							</tr>
							<tr>
								<td>Cód. Afiliação:</td><td><span id="m_codigoAfiliacao"></span></td>
							</tr>
							
							<tr>
								<td>Cidade:</td><td><span id="cidade"></span></td>
							</tr>

							<tr>
								<td>Data de Nascimento:</td><td><span id="dataNascimento"></span></td>
							</tr>

							<tr>
								<td>Profissão:</td><td><span id="profissao"></span></td>
							</tr>

							<tr>
								<td>Ocupação:</td><td><span id="ocupacao"></span></td>
							</tr>

							<tr>
								<td>Admissão:</td><td> <span id="admissao"></span></td>
							</tr>
							<tr>
								<td>Afiliação R+C:</td><td> <span id="tipoAfiliacao"></span> <span id="dual"></span></td>
							</tr>
                                                        <tr>
								<td>Afiliação TOM:</td><td> <span id="tom"></span> <span id="tipoAfiliacaoTom"></span> <span id="dualTom"></span></td>
							</tr> 
						</tbody>
					</table>
					<div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>R+C<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
				<table cellpading="5">	
					<tr>
						<td>Sit. Cadastral:</td><td> <span id="situacaoCadastralRC"></span></td>
					</tr>
					<tr>
						<td>Sit. Remessa:</td><td> <span id="situacaoRemessaRC"></span></td>
					</tr>
					<tr>
						<td>Data da Quitação:</td><td> <span id="dataQuitacaoRC"></span></td>
					</tr>
					<tr>
						<td>Grau:</td><td> <span id="grauRC"></span></td>
					</tr>
					<tr>
						<td>Lote:</td><td> <span id="loteRC"></span></td>
					</tr>
					<tr>
						<td>Iniciação pendentes:</td><td><span id="iniciacoesPendentesRC"></span></td>
					</tr>
					<tr>
						<td>Concluiu os ensinamentos:</td><td> <span id="reiniciouRC"></span></td>
					</tr>
				</table>	
				</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><center>TOM</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
				<table cellpading="5">	
					<tr>
						<td>Sit. Cadastral:</td><td> <span id="situacaoCadastralTOM"></span></td>
					</tr>
					<tr>
						<td>Sit. Remessa:</td><td> <span id="situacaoRemessaTOM"></span></td>
					</tr>
					<tr>
						<td>Data da Quitação:</td><td> <span id="dataQuitacaoTOM"></span></td>
					</tr>
					<tr>
						<td>Lote:</td><td> <span id="loteTOM"></span></td>
					</tr>
					<tr>
						<td>Concluiu os ensinamentos:</td><td> <span id="reiniciouTOM"></span></td>
					</tr>
				</table>	
				</div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><center>OGG</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
				<table cellpading="5">	
					<tr>
						<td>Sit. Cadastral:</td><td> <span id="situacaoCadastralOGG"></span></td>
					</tr>
					<tr>
						<td>Sit. Remessa:</td><td> <span id="situacaoRemessaOGG"></span></td>
					</tr>
					<tr>
						<td>Data da Quitação:</td><td> <span id="dataQuitacaoOGG"></span></td>
					</tr>
					<tr>
						<td>Lote:</td><td> <span id="loteOGG"></span></td>
					</tr>
					<tr>
						<td>Grau:</td><td> <span id="grauOGG"></span></td>
					</tr>
				</table>
				</div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><center>Iniciações R+C</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                               
				<table>
					<tr>
					<td>&nbsp;</td><td></td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="iniciacoes">
							</div>
						 </td>
					</tr>
					</table>
					</div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><center>Iniciações TOM</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                               
				<table>
					<tr>
					<td>&nbsp;</td><td></td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="iniciacoesTOM">
							</div>
						 </td>
					</tr>
					</table>
					</div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen"><center>Iniciações OGG</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseTen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                               
				<table>
					<tr>
					<td>&nbsp;</td><td></td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="iniciacoesOGG">
							</div>
						 </td>
					</tr>
					</table>
					</div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><center>Cargos Atuantes</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
					<table>
					<tr>
						<td colspan="2">
							<div id="cargos">
								
							 </div>
						 </td>
					</tr>
					</tbody>	
					</table>
            	</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><center>Cargos Exercídos</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
					<table>
					<tr>
						<td colspan="2">
							<div id="cargosNaoAtuantes">
								
							 </div>
						 </td>
					</tr>
					</tbody>	
					</table>
            	</div>
                                        </div>
                                    </div>
                                </div>
					 </div>
                        </div>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Legenda dos Status de Situação </h4>
            </div>
            <div class="modal-body">
            <b>-> Situação no OA</b>
            <ul>
                <li>Aguardando registro de quitação = (Não foi encontrado registro de quitação no último mês, antes de ele ficar inativo, está sendo aguardado o registro de quitação)</li>
                <li>Aguardando Pagamento = (O membro...)</li>
                <li>Inativo (Não foi verificado nenhum pagamento do membro para o O.A em 3 meses)</li>
                <li>Ativo (O membro está com a quitação em seu O.A em dia)</li>
            </ul>
            
            <b>-> Situação Cadastral na GLP</b>
            <ul>
                <li>Ativo (Pagamento em dia na R+C)</li>
                <li>Inativo (Não foi verificado nenhum pagamento R+C do membro pela GLP)</li>
                <li>Estudos paralisados (O membro solicitou uma pausa no recebimento dos materiais)</li>
                <li>Não encontrado. Avisar T.I (Ocorreu algum erro interno, por favor tente mais tarde!)</li>
            </ul>

            <b>-> Envio de Monografia</b>
            <ul>
                <li>Envio de Lote - Normal (O envio das monografias para o membro está ativo)</li>
                <li>Estudos paralisados (O membro solicitou uma pausa no recebimento dos materiais)</li>
                <li>Não encontrado (Ocorreu algum erro interno, por favor tente mais tarde!)</li>
            </ul>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
        </div>
        </div>
    </div>
</div>

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
<!-- Window MODAL Fim -->

<!-- ConteÃºdo DE INCLUDE FIM -->
