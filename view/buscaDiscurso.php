<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
/*
$textoImpressao = "
Saudações em Todas as Pontas do nosso Sagrado Triangulo!
    
O presente Manual Administrativo é compartilhado com o compromisso especial
de sua execução exatamente de acordo com as diretrizes nele contidas.

Lembro que este documento está protegido por lei e registrado nas instâncias
competentes da República Federativa do Brasil, sendo interditada sua reprodução
por qualquer meio – cópia, fotocópia e digitalização – cabendo aos Oficiais se
organizarem de forma a dispensarem estas medidas.

É vedada também a retirada deste Manual (se impresso) das dependências do
Organismo Afiliado, não podendo qualquer Oficial ou Membro transportá-lo
consigo para sua residência ou outro local.

A GLP está tomando medidas para coibir o descumprimento destas diretrizes e
identificará por rastreamento quaisquer conteúdos privativos que por ventura
apareçam em outros locais que não os Organismos Afiliados.

O descumprimento gerará, além das inevitáveis compensações cármicas,
pela quebra do juramento de sigilo, em sanções administrativas.

Na certeza de que saberão valorizar este Sagrado Legado,
confio-lhes!

Nos Laços de Nossa Ordem,
Assim Seja!
Hélio de Moraes e Marques
Grande Mestre";
*/

$textoImpressao = "
Caros Oficiais!
Saudações em Todas as Pontas do nosso Sagrado Triangulo!

Os presentes discursos são disponibilizados a você com o compromisso especial
de seu melhor aproveitamento, como um Sagrado Legado cujo conteúdo deve ser
mantido confidencial. Em outras palavras, estes conteúdos devem ser objetos de
todo o seu cuidado e discrição.

Não é permitido a você que o repasse a pessoas não pertencentes à AMORC.
Sendo interditada sua reprodução por qualquer meio – cópia, fotocópia e
digitalização. Saiba que o descumprimento gerará inevitáveis compensações
cármicas, pela quebra do juramento de sigilo, e em sanções administrativas que
podem chegar à exclusão do estudante dos quadros de nossa Venerável Ordem.

Na certeza de que saberá valorizar este Sagrado Legado,
Confio-lhe!
Nos Laços de Nossa Ordem,
Assim Seja!
Hélio de Moraes e Marques
Grande Mestre";

$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Discurso salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Já existe um Discurso cadastrado com esse número. Por favor, informe outro número!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$excluido = isset($_REQUEST['excluido'])?$_REQUEST['excluido']:null;
if($excluido==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Arquivo excluído com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php } else if ($excluido == 2) { ?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Discurso excluído com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php } ?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Buscar Discurso</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li class="active">
                <strong>
                    <a>Discursos</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<script src='js/lightbox.js'></script>

<?php
include_once 'controller/documentoController.php';
include_once 'controller/discursoController.php';
include_once 'controller/usuarioController.php';
include_once 'controller/impressaoController.php';
include_once 'controller/impressaoDiscursoController.php';

include_once 'lib/libImpressao.php';

$documento            =         new documentoController();
$discurso             =         new DiscursoController();
$usuario              =         new usuarioController();
$impressao            =         new impressaoController();
$impressaoDiscurso    =         new ImpressaoDiscursoController();

$id = isset($_SESSION['seqCadast']) ? $_SESSION['seqCadast'] : null;

if (isset($_REQUEST['idDocumentoImpressao']) && isset($_REQUEST['idDiscursoImpressao'])) {
    $doc = $_REQUEST['idDocumentoImpressao'] != "" ? $_POST['idDocumentoImpressao'] : null;
    $idDiscursoImpressao = $_REQUEST['idDiscursoImpressao'] ? $_REQUEST['idDiscursoImpressao'] : null;
} else {
    $doc = null;
    $idDiscursoImpressao = NULL;
}

if (isset($_REQUEST['opcoesImp'])) {
    $opcaoImp = $_REQUEST['opcoesImp'] != "" ? $_POST['opcoesImp'] : null;
} else {
    $opcaoImp = null;
}

$anexo = isset($_FILES['anexo']['name']) ? true : false;
$delfile = isset($_REQUEST['delfile']) ? $_REQUEST['delfile'] : null;
$delspeech = isset($_REQUEST['delspeech']) ? $_REQUEST['delspeech'] : null;
$desativado = "";

if ($id != null) {
    $dadosUsuario = $usuario->buscaUsuario($id);
   
    $impressao->setUsuario($dadosUsuario);
    if ($doc != null) {
        $dadosDocumento = $documento->buscarDadosId($doc);
        if ($dadosDocumento) {
            foreach ($dadosDocumento as $vetor) {
                $documento->setIdDocumento($vetor['idDocumento']);
                $documento->setDocumentoExtensao($vetor['extensao']);
                $documento->setPaginaImpressao(
                        empty($vetor['paginaImpressao']) ? null : $vetor['paginaImpressao']
                );
                $documento->setDocumentoTipo($vetor['tipo']);
            }
            $impressao->setDocumento($documento);
        }
    }
} else {
    $desativado = "disabled=\"\"";
}



/**
 * Upload Início
 */
$arquivoEnviado=0;
if ($anexo) {
    if ($_FILES['anexo']['name'] != "") {
        $proximo_id = $documento->somaUltimaId();
        //$caminho = "impress/Discurso/";
        $delfile = null;
        $partes = explode('.', $_FILES['anexo']['name']);
        $extensao = strtolower($partes[count($partes) - 1]);

      //  $nomeBruto = strtolower(basename(str_replace(' ', '_', $_FILES['anexo']['name']), '.' . $extensao));
        $inMd5 = md5(aleatorio() . '.' . $extensao);

        if ($extensao != "pdf") {
            echo "<script type=\"text/javascript\">alert(\"Envie arquivos no formato PDF!\");";
            echo "window.location='painelDeControle.php?corpo=buscaDiscurso';</script>";
            return;
        }
        /*
        if ($documento->buscarDados($nomeBruto)) {
            echo "<script type=\"text/javascript\">alert(\"Já existe um arquivo cadastrado com este nome!\");";
            echo "window.location='painelDeControle.php?corpo=buscaDiscurso';</script>";
            return;
        }
        */
        $documento->setIdDocumento($proximo_id);
        $documento->setDocumentoArquivo($inMd5);
        $documento->setDocumentoTipo("Discurso");
        $documento->setDocumentoExtensao($extensao);
        $documento->setPaginaImpressao(null);

        $resultado = $discurso->listaDiscursoId($_POST['idDiscursoUpload']);

        if ($resultado) {
            foreach ($resultado as $vetor) {
                $documento->setDocumentoDescricao($vetor['numeroMensagem'] . ' - ' . $vetor['tituloMensagem'] . ($vetor['autorMensagem'] == "" ? "" : " (" . $vetor['autorMensagem'] . ")"));
            }
        } else {
            $documento->setDocumentoDescricao(null);
        }

        $discurso->setIdDocumento($proximo_id);
        if ($documento->cadastroDocumento(false) && $discurso->insereIdDocumento($_POST['idDiscursoUpload'])) {
            //$uploaddir = 'C:\/xampp\/htdocs\/img\/ata_reuniao_mensal\/';
            $uploaddir = "impress/Discurso/";
            $uploadfile = $uploaddir . $inMd5 . '.' . $extensao;
            //echo "$uploadfile";
            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
            { 
                    $arquivoEnviado = 1;  
            }else{
                echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo!');</script>";
            }    
        }
    }
}

if($arquivoEnviado==1){
?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Arquivo enviado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php 
}
/*
 * Upload Fim
 */

if ($delfile != null) {
    $resultado = $discurso->listaDiscurso();
    if ($resultado) {
        foreach ($resultado as $vetor) {
            if ($vetor['idDiscurso'] == $delfile) {
                if ($vetor['fk_seqCadast'] == $id && $vetor['fk_idDocumento'] != null) {
                    if ($discurso->removerDocumento($delfile)) {
                        echo "<script type=\"text/javascript\">";
                        //echo "alert(\"Arquivo excluído com sucesso!\");";
                        echo "window.location='painelDeControle.php?corpo=buscaDiscurso&excluido=1';</script>";
                    }
                } else {
                    echo "<script type=\"text/javascript\">";
                    echo "alert(\"Você não tem permissão para deletar este arquivo, ou o arquivo já foi excluído!\");";
                    echo "window.location='painelDeControle.php?corpo=buscaDiscurso';</script>";
                }
            }
        }
    }
}

if ($delspeech != null) {
    $resultado = $discurso->listaDiscursoId($delspeech);
    if ($resultado) {
        $resultado2 = $impressaoDiscurso->buscarIdDiscurso($delspeech);
        if($resultado2) {
            foreach ($resultado2 as $vetor2) {
                if($impressaoDiscurso->buscarIdDiscurso($delspeech) && $impressaoDiscurso->excluirImpressaoDiscurso($delspeech)) {
                    $discurso->excluirDiscurso($delspeech);
                } 
            }
        } else {
            $discurso->excluirDiscurso($delspeech);
        }
    }
}
?>

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Discursos</h5>
                    <div class="ibox-tools">
                        <button type="button" style="display: none" class="btn btn-xs btn-primary" onclick="window.location = 'painelDeControle.php?corpo=relatorioDiscurso'">
                            <i class="fa fa-search"></i>
                            Relatório de Impressão
                        </button>
                        <?php if(in_array("1",$arrNivelUsuario)){?>
                        <button type="button" <?php echo $desativado; ?> class="btn btn-xs btn-primary" onclick="window.location = 'painelDeControle.php?corpo=cadastroDiscurso'" title="Cadastra discurso">
                            <i class="fa fa-plus"></i>
                            Cadastrar Discurso
                        </button>
                        <?php } ?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th><center>Número da Mensagem</center></th>
                        <th><center>Título da Mensagem</center></th>
                        <th><center>Tipo</center></th>
                        <th><center>Autor da Mensagem</center></th>
                        <th><center>Data de Criação do Discurso</center></th>
                        <th><center>Experimento</center></th>
                        <th><center>Arquivo</center></th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
<?php
if ($id == null) {
    echo "<script type=\"text/javascript\">";
    echo "alert(\"Impressão desativada! Usuário não identificado.\");";
    echo "</script>";
} else {
    if (!empty($doc)) {
        if (($resultado = $documento->buscarDadosId($doc)) == false) {
            echo "<script type=\"text/javascript\">";
            echo "alert(\"Arquivo não encontrado pelo ID: '" . $doc . "'\");";
            echo "</script>";
        } else {
            include_once 'controller/impressaoDiscursoController.php';
            $impressaoDiscurso = new ImpressaoDiscursoController();

            switch ($opcaoImp) {
                case 0:
                    $motivo = "Atividade Ritualística";
                    break;
                case 1:
                    $motivo = "Consulta";
                    break;
                case 2:
                    $motivo = $_REQUEST['motivoImpressao'];
                    break;
            }

            $impressaoDiscurso->setMotivoDiscursoImpressao($motivo);
            $impressaoDiscurso->setFkIdDiscurso($idDiscursoImpressao);
            $impressao->setImpressaoDiscurso($impressaoDiscurso);

            foreach ($resultado as $vetor) {
                $arquivo = $vetor['arquivo'];
            }
            
            $impressao->cadastroImpressao();
            
            $resultado2 = $documento->buscarDadosId($_POST['idDocumentoImpressao']);
            $arquivo2 = $resultado2[0]['arquivo'];

            $caminhoCompleto = "../impress/Discurso/".$arquivo2.".pdf";
            //echo "<script>alert(\"" . $caminhoCompleto . "\");</script>";
            echo "<script type=\"text/javascript\">var p=window.open(\"" . $caminhoCompleto . "\");p.window.focus();p.print();</script>";
            /*
            try {
                $token = impPdf($arquivo, $impressao, $documento, $usuario, $id, "topo&meio", "Discurso/");
                if ($token != null) {
                    echo "<script type=\"text/javascript\">var p=window.open(\"output/" . $token . ".pdf\");p.window.focus();p.print();";
                    echo "window.location='painelDeControle.php?corpo=buscaDiscurso';</script>";
                } else {
                    echo "<script type=\"text/javascript\">alert(\"" . $mensagemErro . "\");";
                    echo "window.location='painelDeControle.php?corpo=buscaDiscurso';</script>";
                }
            } catch (Exception $e) {
                $mensagemErro .= "\\nProvável motivo: versão do arquivo PDF maior ou igual a 1.5";
                echo "<script type=\"text/javascript\">alert(\"" . $mensagemErro . "\");";
                echo "window.location='painelDeControle.php?corpo=buscaDiscurso';</script>";
            }
             */
        }
    }
}

$resultado = $discurso->listaDiscurso();

if ($resultado) {
    foreach ($resultado as $vetor) {
        $data = explode('-', $vetor['dataCriacao']);
        $desativado = ($vetor['fk_seqCadast'] != $id) ? "disabled=\"\"" : "";

        $valorData = ($data[0] == 0 || $data[1] == 0 || $data[2] == 0) ? "Não informado" : $data[2] . '/' . $data[1] . '/' . $data[0];

        $arquivoExiste = false;

        if ($vetor['fk_idDocumento'] != null) {
            $arquivoExiste = $documento->buscarDadosId($vetor['fk_idDocumento']) ? true : $arquivoExiste;
        }
        ?>
                                    <tr>
                                        <td><center><?php echo $vetor['numeroMensagem']; ?></center></td>
                                <td><center><?php echo $vetor['tituloMensagem']; ?></center></td>
                                <td><center><?php echo ($vetor['tipo'] == 2 ? "OGG" : "R+C"); ?></center></td>
                                <td><center><?php echo ($vetor['autorMensagem'] == "" ? "Não informado" : $vetor['autorMensagem']); ?></center></td>
                                <td><center><?php echo $valorData; ?></center></td>
                                <td><center><?php echo ($vetor['temExperimento'] == 1 ? "Sim" : "Não"); ?></center></td>
                                <td><center><?php
                                    if ($arquivoExiste) {
                                           $resultado2 = $documento->buscarDadosId($vetor['fk_idDocumento']);
                                           foreach ($resultado2 as $vetor2) {
                                               if($vetor2['linkVisualizacao'] != "") {
                                                   $linkVisualizacao = $vetor2['linkVisualizacao'];
                                               }
                                               else {
                                                   $linkVisualizacao = null;
                                               }
                                           }
                                           if($linkVisualizacao != null) {
                                            ?>
                                    <!--
                                            <a class="btn btn-sm btn-info" data-rel='fh5-light-box-demo' data-href='<?php //echo $linkVisualizacao; ?>' data-width='1000' data-height='600'
                                                data-title='<?php //echo $vetor['tituloMensagem']; ?>'>
                                                Visualizar
                                            </a>-->
                                        <?php
                                           }
                                           else
                                           {
                                           ?>
                                    <!--
                                            <a class="btn btn-sm btn-info" disabled="">
                                                Visualizar
                                            </a>
                                    -->
                                           <?php
                                           }
                                           if($vetor['fk_idDocumento']!=0)
                                           {    
                                        ?>
                                        <?php //if(in_array("3",$arrNivelUsuario)){?>
                                        <button class="btn btn-sm btn-danger" <?php echo $desativado; ?> onclick="excluirUploadDiscurso('<?php echo $vetor['idDiscurso']; ?>');" type="button" data-toggle="tooltip" data-placement="left" title="Excluir Arquivo">
                                            Excluir
                                        </button>
                                        <?php //} ?>
                                        <?php
                                           }else{
                                                    $resultado4 = $discurso->listaDiscurso();
                                                    if ($resultado4) {
                                                        foreach ($resultado4 as $vetor4) {
                                                            if ($vetor4['idDiscurso'] == $vetor['idDiscurso']) {
                                                                if ($vetor4['fk_seqCadast'] == $id) {
                                                                    if ($discurso->removerDocumento($vetor4['idDiscurso'])) {
                                                                        
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                
                                               ?>
                                                <button type="button" class="btn btn-sm btn-warning" <?php echo $desativado; ?> onclick="document.getElementById('idDiscursoUpload').value =<?php echo $vetor['idDiscurso']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Enviar arquivo">
                                                    <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                                    Enviar discurso
                                                </button>
                                               <?php
                                           }
                                        } else {
                                        ?>
                                        <?php //if(in_array("4",$arrNivelUsuario)){?>
                                        <button type="button" class="btn btn-sm btn-warning" <?php echo $desativado; ?> onclick="document.getElementById('idDiscursoUpload').value =<?php echo $vetor['idDiscurso']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Enviar arquivo">
                                            <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                            Enviar discurso
                                        </button>
                                        <?php //} ?>
            <?php
        }
        ?></center></td>
                                <td><center>
                                    <!--
                                    <a href="painelDeControle.php?corpo=alteraDiscurso&iddiscurso=<?php // echo $vetor['idDiscurso']; ?>"
                                       class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar dados do Discurso">
                                        <i class="fa fa-edit fa-white"></i>
                                        Editar
                                    </a>
                                    -->
                                    <?php if(in_array("2",$arrNivelUsuario)){?>
                                    <button type="button" class="btn btn-sm btn-primary" <?php echo $desativado; ?> onclick="window.location = 'painelDeControle.php?corpo=alteraDiscurso&iddiscurso=<?php echo $vetor['idDiscurso']; ?>'" data-toggle="tooltip" data-placement="left" title="Editar Cadastro">
                                        <i class="fa fa-edit fa-white"></i>
                                        Editar
                                    </button>
                                    <?php } ?>
                                    <?php $desativado = ($id != null && $arquivoExiste) ? "" : "disabled=\"\""; ?>
                                    <button type="button" class="btn btn-sm btn-success" <?php echo $desativado; ?> onclick="document.getElementById('idDocumentoImpressao').value =<?php echo $vetor['fk_idDocumento']; ?>;
                                            document.getElementById('idDiscursoImpressao').value =<?php echo $vetor['idDiscurso']; ?>" data-target="#mySeeImpress" data-toggle="modal" data-placement="left" title="Impressão"> 
                                        <i class="fa fa-sign-in fa-white"></i>&nbsp;
                                        Visualizar discurso
                                    </button>
                                    <?php if(in_array("3", $arrNivelUsuario)) { ?>
                                    <button class="btn btn-sm btn-danger" onclick="excluirDiscurso('<?php echo $vetor['idDiscurso']; ?>');" type="button" data-toggle="tooltip" data-placement="left" title="Excluir Discurso">
                                            Excluir
                                    </button>
                                    <?php } ?>
                                </center></td>
                                </tr>
        <?php
    }
}
?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal Início -->

<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Enviar arquivo de discurso</h4>
            </div>
            <div class="modal-body">
                <form id="discurso" name="discurso" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input name="anexo"
                                   id="anexo" type="file" />
                        </div>
                        <div id="anexoAlerta" class="col-sm-10"
                             style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
                            máximo de 10MB.</div>

                    </div>
                    <input type="hidden" id="idDiscursoUpload" name="idDiscursoUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Enviar" onclick="document.discurso.submit()">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Fim -->

<!-- Modal Início -->

<div class="modal inmodal" id="mySeeImpress" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 50%">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-print modal-icon"></i>
                <h4 class="modal-title">Visualização</h4>
            </div>
            <div class="modal-body">
                <form id="imp2" name="imp2" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <h1>Solene Advertência</h1>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <textarea name="textoTermo" id="textoTermo" class="form-control" style="width: 100%; background-color: white; font-family: cursive"
                                      rows="20" readonly=""><?php echo $textoImpressao; ?></textarea><br>
                            <input type="radio" name="termo" id="termo" value="1" />Eu concordo  
                            <input type="radio" name="termo" id="termo" value="0" checked="" />Eu não concordo
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <h2>Selecione o motivo da Visualização</h2>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <select class="form-control col-sm-3" name="opcoesImp" id="opcoesImp" style="max-width: 300px">
                                    <option>Selecione...</option>
                                    <option value="0">Utilizar para Atividade Ritualística</option>
                                    <option value="1">Utilizar para Consulta</option>
                                    <option value="2">Outro...</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="idDocumentoImpressao" name="idDocumentoImpressao" value="">
                        <input type="hidden" id="idDiscursoImpressao" name="idDiscursoImpressao" value="">
                    </div>
                    <div class="form-group form-inline" name="outroMotivo" id="outroMotivo">  
                        <label class="col-sm-2 control-label">Especifique o motivo: </label>
                        <div class="col-sm-10">
                            <div class="form-control-static">
                                <input type="text" name="motivoImpressao" id="motivoImpressao" class="form-control form-inline" value="" style="width: 300px">
                            </div>
                        </div>
                    </div>
                   <!-- <input type="hidden" name="motivoImpressao" id="motivoImpressao" value=""> -->
                </form>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-warning" name="okImprimir" id="okImprimir" value="Visualizar" onclick="validaCampoMotivo()">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Fim -->



<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
