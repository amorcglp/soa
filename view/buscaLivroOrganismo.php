<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("model/livroOrganismoArquivoClass.php");
$loa = new livroOrganismoArquivo();

$arquivo=0;
$extensaoNaoValida=0;
if(isset($_FILES['anexo']))
{
	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo']['name'], '.');
                if($extensao==".pdf"||
                        $extensao==".png"||
                        $extensao==".jpg"||
                        $extensao==".jpeg"||
                        $extensao==".PDF"||
                        $extensao==".PNG"||
                        $extensao==".JPG"||
                        $extensao==".JPEG"||
                        $extensao==".rar"||
                        $extensao==".zip"
                        )
                {
                    $proximo_id = $loa->selecionaUltimoId();
                    $caminho = "img/livro_organismo/" . basename($_POST["idLivroUpload"].".livro.organismo.".$proximo_id);
                    $loa->setFkIdLivroOrganismo($_POST["idLivroUpload"]);
                    $loa->setCaminho($caminho);
                    if($loa->cadastroLivroOrganismoArquivo())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/livro_organismo/';
                            //$uploaddir = 'C:\/wamp\/www\/soa_mvc\/img\/livro_organismo\/';
                            $uploadfile = $uploaddir . basename($_POST["idLivroUpload"].".livro.organismo.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            {
                                    $arquivo = 1;
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo!');</script>";
                            }
                    }
                }else{
                    $extensaoNaoValida=1;
                }
            }else{
                    $extensaoNaoValida=2;
            }
	}
}

$liberaGLP=false;
if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
    $liberaGLP=true;
}

?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Material salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Material já cadastrado anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($arquivo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Arquivo enviado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php 
}

if($extensaoNaoValida==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Extensão do arquivo inválida! O sistema só aceita as extensões: ZIP, RAR, JPG, JPEG, PNG ou PDF!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Materiais do Organismo para GLP</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li class="active">
                <strong><a>Materiais do Organismo para GLP</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Materiais do Organismo para GLP</h5>
                    <div class="ibox-tools">
                    	<?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroLivroOrganismo">
                            <i class="fa fa-plus"></i> Novo Material
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>

                                <th width="5%"><center>Cod.</center></th>
                                <th width="20%">Título</th>
                                <th width="20%">Organismo</th>
                                <th width="10%"><center>Autor</center></th>
                                <th width="3%"><center>Ler</center></th>
                                <th width="10%"><center>Palavras-chave</center></th>
                        		<th width="8%"><center>Data de Cadastro</center></th>
                        		<th width="3%"><center>Arquivos</center></th>
                        		<th width="20%"><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php


                            include_once("controller/livroOrganismoController.php");

                            $loc = new livroOrganismoController();
                            $resultado = $loc->listaLivroOrganismo();

                            $i=1;
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <?php if(($liberaGLP==true)||(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA")))){?>
                                    <tr>
                                        <td>
                                            <center><?php echo $vetor['idLivroOrganismo']; ?></center>
                                        </td>
                                        <td>
                                            <?php echo $vetor['titulo']; ?>
                                        </td>
                                        <td>
                                            <?php echo retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']); ?>
                                        </td>
                                        <td>
			                                <center>
			                                    <?php echo $vetor['nomeUsuario']; ?>
			                                </center>
		                                </td>
		                                <td>
		                                    <center><button onclick="lerCapituloLivroOrganismo('<?php echo $vetor['idLivroOrganismo']?>','<?php echo $i;?>');" class="btn btn-info  dim" type="button" data-target="#mySee" data-toggle="modal" data-placement="left"><i class="fa fa-book"></i></button></center>
		                                </td>
		                                <td>
			                                <center>
			                                	<ul class="tag-list" style="padding: 5px">
			                                    <?php 
			                                    $arrPC = explode(",",$vetor['palavraChave']); 
			                                    foreach($arrPC as $v)
			                                    {
			                                    ?>
						                                <li style="margin-left: 10px"><i class="fa fa-tag"></i> <?php echo $v;?></li>
			                                    <?php 
			                                    }
			                                    ?>
			                                    </ul>
			                                </center>
		                                </td>
		                                <td>
			                                <center>
			                                    <?php echo substr($vetor['dataCadastro'], 8, 2) . "/" . substr($vetor['dataCadastro'], 5, 2) . "/" . substr($vetor['dataCadastro'], 0, 4); ?>
			                                </center>
		                                </td>
		                                <td>
		                                    <center><button onclick="listaUploadLivroOrganismo('<?php echo $vetor['idLivroOrganismo']?>','<?php echo $i;?>');" class="btn btn-info  dim" type="button" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"><i class="fa fa-paste"></i></button></center>
		                                </td>
		                                <td>
		                                <center>
		                                	<?php if(in_array("2",$arrNivelUsuario)){?>
		                                    <a href="?corpo=alteraLivroOrganismo&idlivro_organismo=<?php echo $vetor['idLivroOrganismo']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left">
		                                        <i class="fa fa-edit fa-white"></i>&nbsp;
		                                        Editar
		                                    </a>
		                                    <?php }?>
		                                    <?php if(in_array("4",$arrNivelUsuario)){?>
		                                    <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('idLivroUpload').value=<?php echo $vetor['idLivroOrganismo']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload">
		                                        <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
		                                        Envio de Arquivos
		                                    </button>
		                                    <?php }?>
		                                </center>
		                                </td>
                                </tr>
                            <?php
                            			$i++;
                                    }
                              }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<div class="modal inmodal" id="mySee" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-book modal-icon"></i>
                <h4 class="modal-title">Material Cadastrado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="texto">
						</div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio de Arquivos</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="idLivroUpload" name="idLivroUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.ata.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Arquivos do Material</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload">
						</div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
