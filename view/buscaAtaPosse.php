<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("controller/usuarioController.php");
$um = new Usuario();
require_once ("controller/organismoController.php");
$oc = new organismoController();

include_once("model/ataPosseAssinadaClass.php");
$apa = new ataPosseAssinada();

$anexo = isset($_FILES['anexo']['name']) ? $_FILES['anexo']['name'] : null;

/**
 * Upload da Ata Assinada
 */
$ataAssinada = 0;
$extensaoNaoValida = 0;
if ($anexo != "") {
    if ($_FILES['anexo']['name'] != "") {
        if(substr_count($_FILES['anexo']['name'],".")==1)
        {

            $extensao = strstr($_FILES['anexo']['name'], '.');
            if ($extensao == ".pdf" ||
                    $extensao == ".png" ||
                    $extensao == ".jpg" ||
                    $extensao == ".jpeg" ||
                    $extensao == ".PDF" ||
                    $extensao == ".PNG" ||
                    $extensao == ".JPG" ||
                    $extensao == ".JPEG"
            ) {
                $proximo_id = $apa->selecionaUltimoId();
                $caminho = "img/ata_posse/" . basename($_POST["idAtaUpload"] . ".ata.posse" . $proximo_id);
                $apa->setFkIdAtaPosse($_POST["idAtaUpload"]);
                $apa->setCaminhoAtaPosseAssinada($caminho);
                if ($apa->cadastroAtaPosseAssinada()) {
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/ata_posse/';
                    $uploadfile = $uploaddir . basename($_POST["idAtaUpload"] . ".ata.posse" . $proximo_id);
                    if (move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile)) {
                        $ataAssinada = 1;
                    } else {
                        echo "<script type='text/javascript'>alert('Erro ao enviar a ata assinada!');</script>";
                    }
                }
            } else {
                $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php
$salvo = isset($_REQUEST['salvo']) ? $_REQUEST['salvo'] : null;
if ($salvo == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Ata salva com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php } ?>
<?php
$jaCadastrado = isset($_REQUEST['jaCadastrado']) ? $_REQUEST['jaCadastrado'] : null;
if ($jaCadastrado == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Ata já cadastrada anteriormente!",
                type: "error",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if ($ataAssinada == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Ata Assinada enviada com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
    <?php
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Área de ATAs</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaPosse">ATA</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaPosse">Organismo Afiliado</a>
            </li>
            <li class="active">
                <strong><a>Posse</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de ATA de Posse</h5>
                    <div class="ibox-tools">
<?php if (in_array("1", $arrNivelUsuario)) { ?>
                            <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtaPosse">
                                <i class="fa fa-plus"></i> Nova ATA
                            </a>
<?php } ?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód.</th>
                                <th>Organismo</th>
                                <th><center>Data da Posse</center></th>
                        <th><center>Data da Entrega</center></th>
                        <th><center>Ata Assinada</center></th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
<?php
include_once("controller/ataPosseController.php");

$apc = new ataPosseController();
$resultado = $apc->listaAtaPosse($idOrganismoAfiliado);

if ($resultado) {
    foreach ($resultado as $vetor) {

        switch ($vetor['classificacaoOrganismoAfiliado']) {
            case 1:
                $classificacao = "Loja";
                break;
            case 2:
                $classificacao = "Pronaos";
                break;
            case 3:
                $classificacao = "Capítulo";
                break;
            case 4:
                $classificacao = "Heptada";
                break;
            case 5:
                $classificacao = "Atrium";
                break;
        }
        switch ($vetor['tipoOrganismoAfiliado']) {
            case 1:
                $tipo = "R+C";
                break;
            case 2:
                $tipo = "TOM";
                break;
        }

        $resposta3 = $apa->listaAtaPosseAssinada($vetor['idAtaPosse']);
        if ($resposta3) {
            $total = count($resposta3);
        } else {
            $total = 0;
        }
        ?>
                                    <?php if ((strtoupper($vetor["siglaOrganismoAfiliado"]) == strtoupper($sessao->getValue("siglaOA"))) ) { ?>
                                        <tr>
                                            <td>
                                        <?php echo $vetor['idAtaPosse']; ?>
                                            </td>
                                            <td>
                                                <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                            </td>
                                            <td>
                                    <center>
            <?php echo substr($vetor['dataPosse'], 8, 2) . "/" . substr($vetor['dataPosse'], 5, 2) . "/" . substr($vetor['dataPosse'], 0, 4); ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
            <?php echo substr($vetor['data'], 8, 2) . "/" . substr($vetor['data'], 5, 2) . "/" . substr($vetor['data'], 0, 4); ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center><?php if ($total > 0) { ?><button class="btn btn-info  dim" type="button" onclick="listaUploadAtaPosse('<?php echo $vetor['idAtaPosse']; ?>')" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"><i class="fa fa-paste"></i> </button><?php } else {
                echo "Não enviada";
            } ?></center>
                                    </td>
                                    <td>
                                    <center>
                                        <button type="button" class="btn btn-sm btn-info" onclick="mostraDetalhesAtaPosse('<?php echo $vetor['idAtaPosse']; ?>');" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Detalhes da ATA de Reunião Mensal dos OA's">
                                            <i class="fa fa-search-plus fa-white"></i>&nbsp;
                                            Detalhes
                                        </button>
                                        <?php if (in_array("2", $arrNivelUsuario)) { ?>
                                            <a href="?corpo=alteraAtaPosse&idata_posse=<?php echo $vetor['idAtaPosse']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar ATA dos OA's">
                                                <i class="fa fa-edit fa-white"></i>&nbsp;
                                                Editar
                                            </a>
                                        <?php } ?>
                                        <button type="button" class="btn btn-sm btn-success" onclick="abrirPopUpAtaPosse('<?php echo $vetor['idAtaPosse']; ?>');" data-toggle="tooltip" data-placement="left" title="Imprimir ATA">
                                            <i class="fa fa-print fa-white"></i>&nbsp;
                                            Imprimir
                                        </button>
                                        <?php if (in_array("4", $arrNivelUsuario)) { ?>
                                            <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('idAtaUpload').value =<?php echo $vetor['idAtaPosse']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload">
                                                <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                                Enviar
                                            </button>
                                        <?php } ?>
                                        <?php if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) { ?>
                                            <a class="btn btn-sm btn-danger" href="#" onclick="abreModalNotificacao(<?php echo $vetor['fk_idOrganismoAfiliado']; ?>, null)">
                                                <i class="fa fa-envelope icon-white"></i>
                                                Notificar
                                            </a>
                                        <?php } ?>
                                    </center>
                                    </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes da Ata de Posse</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Posse:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="dataPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora de inicio:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaInicioPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Endereço:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="enderecoPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="numeroPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bairro:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="bairroPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cidade:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="cidadePosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mestre Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="mestreRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Secretário Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="secretarioRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Presidente Junta Depositária Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="presidenteJuntaDepositariaRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Detalhes Adicionais:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="detalhesAdicionaisPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data Cadastro ou Atualização:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="data"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cadastrado por:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="cadastrado_por"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Atualizado por:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="atualizado_por"></span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio da Ata Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="10000000">
                    <div class="form-group">
                        <!--  
                        <div class="alert alert-danger">
       <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
</div>
                        -->
                        <div class="col-sm-10">
                            <input name="anexo"
                                   id="anexo" type="file" />
                        </div>
                        <div id="anexoAlerta" class="col-sm-10"
                             style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
                            máximo de 10MB.</div>

                    </div>
                    
                    <input type="hidden" id="idAtaUpload" name="idAtaUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Enviar" onclick="document.ata.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Ata Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUpload"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
modalNotificacao($sessao->getValue("seqCadast"));
?>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
