<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

require_once ("model/criaSessaoClass.php");
include_once ("controller/regiaoRosacruzController.php");
include_once ("controller/organismoController.php");

$rrc = new regiaoRosaCruzController();
$oc = new organismoController();

?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Publicações | Administração</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Início</a>
            </li>
            <li>
                <a>Publicações</a>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">

                    <div class="m-b-lg">
                        <!--
                        <div class="input-group">
                            <input type="text" placeholder="Digite o que deseja buscar..." class=" form-control">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-white"> Procurar</button>
                            </span>
                        </div>
                        -->
                        <div class="m-t-md">
                            <strong>Filtros</strong>
                        </div>
                        <div class="input-group inline col-lg-12">
                            <div class="col-lg-3" style="margin-top: 8px">
                                <div>
                                    <select id="filtroRegiao" name="filtroRegiao" data-placeholder="Região" class="chosen-select col-lg-12" multiple tabindex="4">
                                        <?php $rrc->criarComboBox(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3" style="margin-top: 8px">
                                <div>
                                    <select id="filtroOrganismo" name="filtroOrganismo" data-placeholder="Organismo" class="chosen-select col-lg-12" multiple tabindex="4">
                                        <?php $oc->criarComboBox(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3" style="margin-top: 8px">
                                <div>
                                    <select id="filtroCategoria" name="filtroCategoria" data-placeholder="Categoria" class="chosen-select col-lg-12" multiple tabindex="4">
                                        <option value="1">Atividades</option>
                                        <option value="2">Eventos</option>
                                        <option value="3">Notícias</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3" style="margin-top: 8px">
                                <div>
                                    <select id="filtroPublico" name="filtroPublico" data-placeholder="Público" class="chosen-select col-lg-12" multiple tabindex="4">
                                        <option value="1">Rosacruz</option>
                                        <option value="2">Martinista</option>
                                        <option value="3">OGG</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 8px">
                            <div class="btn-group col-lg-offset-3 col-lg-6 center">
                                <button onClick="aplicarFiltroMuralAdmin();" 
                                        class="btn btn-primary btn-sm col-lg-6" 
                                        type="button">
                                    Aplicar Filtro
                                </button>
                                <button onClick="limparFiltroMuralAdmin();" 
                                        class="btn btn-white btn-sm col-lg-6" 
                                        type="button">
                                    Limpar Filtro
                                </button>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 8px">
                            <div class="btn-group col-lg-offset-3 col-lg-6 center">
                                <button onClick="alteraFiltroFavoritoAdmin();" 
                                        id="filtroFavoritoBtn" 
                                        name="filtroFavoritoBtn" 
                                        class="btn btn-white btn-sm col-lg-12" 
                                        type="button">
                                        Favoritos
                                </button>
                                <input type="hidden" id="filtroFavorito" 
                                        name="filtroFavorito" value="0">
                            </div>
                        </div>

                        <div class="m-t-md">
                            <!--
                            <div class="pull-right">
                                <button type="button" class="btn btn-sm btn-white" onclick="window.location.href='?corpo=muralDigitalEstatisticas'" formtarget="_blank"> <i class="fa fa-area-chart"></i></button>
                            </div>
                            -->
                            <strong>Listagem das Postagens</strong>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Status</th>
                                        <th>Tipo de Postagem </th>
                                        <th>Público </th>
                                        <th>Quem </th>
                                        <th>Quando</th>
                                        <th>Postagem Completa</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody class="muralAdmin"></tbody>
                            </table>
                            <div id="linhaTracejadaMural" class="hr-line-dashed"></div>
                            <div class="text-center"><div class="btn-group" id="paginadorMural"></div></div>
                        </div>
                    </div>
                    <input type="hidden" id="seqCadastUsuarioLogado" name="seqCadastUsuarioLogado" value="<?php echo $sessao->getValue("seqCadast");?>">
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL -->

    <div class="modal inmodal fade" id="modalPublicacaoCompleta" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 900px !important;">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 id="header-titulo" class="modal-title">Organismo Afiliado: | Publicado em: 00/00/0000</h4>
                    <h3 id="header-dados" class="font-bold">Tipo da Postagem: Evento | Para: Região</h3>
                </div>

                <div class="modal-body">

                    <!--<h3>Postagem Completa</h3>-->

                    <div class="feed-activity-list">


                        <div class="feed-element">

                            <a class="pull-left col-lg-1" id="publicacao-avatar">
                                <img alt="image" class="img-circle" src="">
                            </a>
                            
                            <div class="media-body col-lg-11">

                                <strong id="publicacao-nome">John Doe</strong><br>
                                <span id="publicacao-data" class="date">00/00/0000 - 00:00</span>

                                <div id="publicacao-mensagem" class="well" style="width: 100%">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique cursus nulla, in egestas sem ultricies vitae. Nunc elementum finibus turpis nec commodo. Mauris vitae mollis dui. Donec dapibus, dolor sit amet aliquam auctor, felis magna auctor eros, at ullamcorper justo nibh ut diam. Duis consectetur eget tortor vel varius. Cras nisi felis, efficitur eget varius at, rutrum molestie velit. Vivamus ut vehicula metus, sed rhoncus massa. Proin congue tellus vel eleifend venenatis. Sed ultrices, purus eu imperdiet mattis, risus ipsum tincidunt nisl, vel malesuada ligula ligula ac sem. Suspendisse rutrum ultricies erat, a rhoncus sem venenatis non. Quisque ante erat, laoreet id nibh et, consequat ullamcorper turpis. Cras sed enim a quam pellentesque faucibus vel in tortor. Nam in massa sodales, lacinia libero nec, condimentum elit. Aenean lacinia aliquam ligula, a commodo eros. Ut eu sollicitudin risus. Aliquam pellentesque est purus, vel pretium mauris consequat rhoncus. 
                                </div>

                                <div class="photos" id="publicacao-imagem">
                                    <img alt="image" class="feed-photo" src="">
                                </div>

                            </div>

                        </div>


                        <div class="panel-body">
                            <h3>Comentários</h3>
                            <br>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">
                                    <div class="feed-activity-list" id="comentarios">

                                        <div class="feed-element">
                                            <a class="pull-left">
                                                <img alt="image" class="img-circle" src="">
                                            </a>
                                            <div class="media-body">
                                                <small class="pull-right">
                                                    <button type="button" class="btn btn-danger btn-xs">Excluir</button>
                                                </small>
                                                <strong>John Doe</strong>
                                                <small class="text-muted">00/00/0000 - 00:00:00</small>
                                                <div class="well">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                                                    Over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <input type="hidden" id="idPublicacaoModal" name="idPublicacaoModal" value="0">

                    <div class="modal-footer" id="btnsPublicacaoModal">
                        <button type="button" class="btn btn-outline btn-warning" id="btnFavoritoModal" onClick="">Erro</button>
                        <button type="button" class="btn btn-outline btn-warning" id="btnExclusao" onClick="">Erro</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
