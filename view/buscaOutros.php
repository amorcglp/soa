<?php

require_once 'controller/usuarioController.php';
require_once 'controller/impressaoController.php';
require_once 'controller/documentoController.php';
require_once 'controller/visualizacaoDocumentoController.php';

@include_once 'lib/libImpressao.php';

$usuario = new usuarioController();
$impressao = new impressaoController();
$documento = new documentoController();
$visualizacao = new VisualizacaoDocumentoController();

$id = isset($_SESSION['seqCadast']) ? $_SESSION['seqCadast'] : null;
$doc = isset($_REQUEST['doc']) ? $_REQUEST['doc'] : null;
$impMode = isset($_REQUEST['impmode']) ? $_REQUEST['impmode'] : null;

$textoImpressao = "
Saudações em Todas as Pontas do nosso Sagrado Triangulo!
    
O presente documento é compartilhado com o compromisso especial
de sua execução exatamente de acordo com as diretrizes nele contidas.

Lembro que este documento está protegido por lei e registrado nas instâncias
competentes da República Federativa do Brasil, sendo interditada sua reprodução
por qualquer meio – cópia, fotocópia e digitalização – cabendo aos Oficiais se
organizarem de forma a dispensarem estas medidas.

É vedada também a retirada deste Manual (se impresso) das dependências do
Organismo Afiliado, não podendo qualquer Oficial ou Membro transportá-lo
consigo para sua residência ou outro local.

A GLP está tomando medidas para coibir o descumprimento destas diretrizes e
identificará por rastreamento quaisquer conteúdos privativos que por ventura
apareçam em outros locais que não os Organismos Afiliados.

O descumprimento gerará, além das inevitáveis compensações cármicas,
pela quebra do juramento de sigilo, em sanções administrativas.

Na certeza de que saberão valorizar este Sagrado Legado,
confio-lhes!

Nos Laços de Nossa Ordem,
Assim Seja!
Hélio de Moraes e Marques
Grande Mestre";

if ($id != null) {
    $dadosUsuario = $usuario->buscaUsuario($id);
    $impressao->setUsuario($dadosUsuario);
    
    $nomeUsuario = $dadosUsuario->getNomeUsuario();
    $codigoDeAfiliacao = $dadosUsuario->getCodigoDeAfiliacaoUsuario();
    $emailUsuario = $dadosUsuario->getEmailUsuario();
    
    $arquivo = "";
    
    if($doc != null && $impMode != null) {
        $dadosDocumento = $documento->buscarDadosId($doc);
        if($dadosDocumento) {
            foreach ($dadosDocumento as $vetor) {
                $documento->setIdDocumento($vetor['idDocumento']);
                $documento->setDocumentoExtensao($vetor['extensao']);
                $documento->setPaginaImpressao(
                    empty($vetor['paginaImpressao']) ? null : $vetor['paginaImpressao']
                );
                $arquivo = $vetor['arquivo'];
            }
            if($impMode == 0) {
                $impressao->setDocumento($documento);
            }
        }
    }
}

// Impressão
if(!empty($doc)) {
    if($documento->buscarDados($arquivo)==false) {
        echo "<script type=\"text/javascript\">";
        echo "swal(\"Erro\", \"Arquivo não encontrado: '" . $arquivo . "'\", \"error\");";
        echo "</script>";
    }
    else {
        if($impMode == 0) {
            $token = impPdf($arquivo, $impressao, $documento, $usuario, $id, "topo&meio", "Outros/");
            if ($token != null) {
                echo "<script type=\"text/javascript\">var p=window.open(\"output/". $token .".pdf\");p.window.focus();p.print();";
                echo "window.location='painelDeControle.php?corpo=buscaOutros';</script>";
            }
            else {
                echo "<script type=\"text/javascript\">swal(\"PDF\", \"Erro ao gerar PDF!\", \"error\");";
                echo "window.location='painelDeControle.php?corpo=buscaOutros';</script>";
            }
        } else if($impMode == 1) {
            $token = impPdfVisualizacao($arquivo, $visualizacao, $documento, $usuario, $id, "topo&meio", "Outros/");
            if($token!=null) {
                echo "<script type=\"text/javascript\">var p=window.open(\"output/". $token .".pdf\");p.window.focus();";
                echo "window.location='painelDeControle.php?corpo=buscaOutros';</script>";
            }
            else {
                echo "<script type=\"text/javascript\">swal(\"PDF\", \"Erro ao gerar PDF!\", \"error\");";
                echo "window.location='painelDeControle.php?corpo=buscaOutros';</script>";
            }
        }
    }
}

?>

<link href="css/biblioteca.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src='js/lightbox.js'></script>

<body>
    <h1>Biblioteca de Arquivos SOA</h1>
    <p>Qualquer dúvida em relação ao SOA, sempre terá um documento nessa seção que irá lhe esclarecer suas dúvidas!</p><br>
    <input type="hidden" name="arquivo" id="arquivo" value="">
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Outros Documentos</h5>
                    <div class="ibox-tools">
                       <!-- Opções da lista -->
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th><center>Cód.</center></th>
                                <th><center>Título</center></th>
                                <th><center>Ações</center></th>
                            </tr>
                        </thead>
                        <tbody>
            <?php
                $resultado = $documento->listaDocumento("Outros");
                $contador = 0;
                if($resultado) {
                    foreach ($resultado as $vetor) {
                          $contador++;
                        ?>
            <tr>
                <td><?php echo $contador; ?></td>
                <td><?php echo $vetor['descricao']; ?></td>
            <td><center>
                <button name="btnVisualizarManualCompleto" id="btnVisualizarManualCompleto" class="btn btn-sm btn-primary" <?php if($id==null) { echo "disabled=\"\""; } ?> onclick="window.location.href='painelDeControle.php?corpo=buscaOutros&doc=<?php echo $vetor['idDocumento'] . "&impmode=1" . "&fk_seqCadast=" . $id; ?>'" title="Visualizar documento">
                    Visualizar
                </button>
                <button class='btn btn-sm btn-success' <?php if($id==null) { echo "disabled=\"\""; } ?> data-toggle="modal" data-target="#ModalImpressao"
                    data-toggle="tooltip" data-placement="left" onclick="document.getElementById('doc').value=<?php echo $vetor['idDocumento']; ?>; document.getElementById('impmode').value=0" title="Impressão">
                    Imprimir
                </button>
            </center></td>
            </tr>
            <?php
                    }
                }
            ?>
                    </tbody>
        </table>
                </div>
               </div>
        </div>
    </div>
 </div>
    
<!-- MODAL Início -->
<div class="modal inmodal" id="ModalImpressao" tabindex="-1" role="dialog"  aria-hidden="true">
     <div class="modal-dialog modal-lg" style="width: 50%">
        <div class="modal-content animated bounceInUp">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <i class="fa fa-print modal-icon"></i>
              <h4 class="modal-title">Impressão</h4>
          </div>
          <div class="modal-body">
            <form id="imp" name="imp" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="form-group">
              <div class="col-sm-10">
                  <h1>Solene Advertência</h1>
              </div>
              </div>
              <div class="form-group">
                    <div class="col-sm-12">
                        <textarea name="textoTermo" id="textoTermo" class="form-control" style="width: 100%; background-color: white; font-family: cursive"
                                rows="28" readonly=""><?php echo $textoImpressao; ?></textarea><br>
                        <input type="radio" name="termo" id="termo" value="1" />Eu concordo  
                        <input type="radio" name="termo" id="termo" value="0" checked="" />Eu não concordo
                    </div>
             </div>
            <input type="hidden" name="doc" id="doc" value="">
            <input type="hidden" name="impmode" id="impmode" value="">
            </form>
                <div class="modal-footer">
                    <input type="submit" name="okImprimir" id="okImprimir" class="btn btn-warning" value="Imprimir" onclick="return validaConcordanciaImpressao(true)">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
 </div>
<!-- MODAL Fim -->
    
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
</body>
</html>