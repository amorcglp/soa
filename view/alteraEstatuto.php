<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();

        if($idClassificacaoOa==1)
        {
            $arrTipos=['P','C','L'];
            $tipos="P,C,L";
        }elseif($idClassificacaoOa==2)
        {
            $arrTipos=['P'];
            $tipos="P";
        }elseif($idClassificacaoOa==3)
        {
            $arrTipos=['P','C'];
            $tipos="P,C";
        }
      ?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>
$(document).ready(function(){
	$("#dataNascimento1").mask("99/99/9999");
});
</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Estatuto</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaEstatuto">Cadastros</a>
            </li>
            <li class="active">
                <strong><a>Estatuto</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<?php
include_once("controller/estatutoController.php");
$apc = new estatutoController();
$dados = $apc->buscaEstatuto($_GET['idestatuto']);

?>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de edição de Estatuto para Organismo</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaEstatuto">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoAlterar.php" onSubmit="return validaEstatuto()" >
                        <input type="hidden" name="fk_idEstatuto" id="fk_idEstatuto" value="<?php echo $_GET['idestatuto'];?>">
                        <div class="row">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">                              
                                <?php echo retornaNomeCompletoOrganismoAfiliado($dados->getFkidOrganismoAfiliado());?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número do Ofício: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="10" id="numeroOficio" onkeypress="return SomenteNumero(event)" name="numeroOficio" type="text" value="<?php if($dados->getNumeroOficio()!=0){echo $dados->getNumeroOficio();}else{ echo "";}?>" style="max-width: 83px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comarca do Ofício: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="cidadeOficio" name="cidadeOficio" type="text" value="<?php echo $dados->getCidadeOficio();?>" onkeypress="aumentarLetra('cidadeOficio',this.value);" onBlur="aumentarLetra('cidadeOficio',this.value);" style="max-width: 343px"  required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">UF do Ofício: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="ufOficio" maxlength="2" name="ufOficio" type="text" value="<?php echo $dados->getUfOficio();?>" onkeypress="aumentarLetra('ufOficio',this.value);" onBlur="aumentarLetra('ufOficio',this.value);" style="max-width: 53px"  required="required">
                            </div>
                        </div>
                        <div class="form-group datapicker">
                            <label class="col-sm-3 control-label">Data da Assembléia Geral:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAssembleiaGeral" maxlength="10" id="dataAssembleiaGeral" type="text" value="<?php echo substr($dados->getDataAssembleiaGeral(),8,2);?>/<?php echo substr($dados->getDataAssembleiaGeral(),5,2);?>/<?php echo substr($dados->getDataAssembleiaGeral(),0,4);?>" class="form-control" style="max-width: 102px"  required="required"  onkeypress="mascaraData(this)" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número de Presentes: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="10" id="numeroPresentes" onkeypress="return SomenteNumero(event)" name="numeroPresentes" type="text" value="<?php echo $dados->getNumeroPresentes();?>" style="max-width: 83px" required="required">
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nome do Advogado: </label>
                                <div class="col-sm-9">
                                    <input class="form-control" id="nomeAdvogado" name="nomeAdvogado" type="text" value="<?php echo $dados->getNomeAdvogado();?>" onkeypress="aumentarLetra('nomeAdvogado',this.value);" onBlur="aumentarLetra('nomeAdvogado',this.value);" style="max-width: 300px" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Número da OAB: </label>
                                <div class="col-sm-9">
                                    <input class="form-control" maxlength="10" id="numeroOAB" onkeypress="return SomenteNumero(event)" name="numeroOAB" type="text" value="<?php echo $dados->getNumeroOAB();?>" style="max-width: 83px" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php if(in_array("P",$arrTipos)){?>
                        	<div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>REGISTRO DE PRONAOS</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Cartório:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cartorioPronaos" name="cartorioPronaos" type="text" maxlength="100" value="<?php echo $dados->getCartorioPronaos();?>" style="min-width: 320px" onkeypress="aumentarLetra('cartorioPronaos',this.value);" onBlur="aumentarLetra('cartorioPronaos',this.value);">
					                            </div>
				                        </div>
					                <div class="form-group">
					                            <label class="col-sm-3 control-label">Comarca:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="comarcaPronaos" name="comarcaPronaos" type="text" maxlength="100" value="<?php echo $dados->getComarcaPronaos();?>" style="min-width: 320px" onkeypress="aumentarLetra('comarcaPronaos',this.value);" onBlur="aumentarLetra('comarcaPronaos',this.value);">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Número do Estatuto:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroPronaos" name="numeroPronaos" type="text" maxlength="10" value="<?php echo $dados->getNumeroPronaos();?>" style="min-width: 320px">
					                            </div>
					                </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Número de Averbação:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="numeroAverbacaoPronaos" name="numeroAverbacaoPronaos" type="text" maxlength="10" value="<?php echo $dados->getNumeroAverbacaoPronaos();?>" style="min-width: 320px">
                                            </div>
                                        </div>
					                <div class="form-group">
					                            <label class="col-sm-3 control-label">Número da Lei Municipal de Utilidade Pública:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroLeiMunicipalPronaos" name="numeroLeiMunicipalPronaos" type="text" maxlength="10" value="<?php echo $dados->getNumeroLeiMunicipalPronaos();?>" style="min-width: 320px">
					                            </div>
					                </div>
					                <div class="form-group datapicker">
				                            <label class="col-sm-3 control-label">Data da Lei Municipal:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input maxlength="10" id="dataLeiMunicipalPronaos" name="dataLeiMunicipalPronaos" type="text" class="form-control" value="<?php echo $dados->getDataLeiMunicipalPronaos();?>" style="max-width: 102px" onkeypress="mascaraData(this)" >
				                            </div>
				                        </div>
                                                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Número da Lei Estadual de Utilidade Pública:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroLeiEstadualPronaos" name="numeroLeiEstadualPronaos" type="text" maxlength="10" value="<?php echo $dados->getNumeroLeiEstadualPronaos();?>" style="min-width: 320px">
					                            </div>
					                </div>
					                <div class="form-group datapicker">
				                            <label class="col-sm-3 control-label">Data da Lei Estadual:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input maxlength="10" id="dataLeiEstadualPronaos" name="dataLeiEstadualPronaos" type="text" class="form-control" value="<?php echo $dados->getDataLeiEstadualPronaos();?>" style="max-width: 102px" onkeypress="mascaraData(this)" >
				                            </div>
				                        </div>    
					               </div>
                                    <?php if(in_array("C",$arrTipos)){?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">&nbsp;</label>
                                        <div class="col-sm-9">
                                            <a href="#" onclick="copiarInformacoesEstatuto('Pronaos','Capitulo');">(Copiar informações acima para registro de capítulo)</a>
                                        </div>
                                    </div>
                                    <?php }?>
					            </div>
					        </div>
                            <?php }?>
                            <?php if(in_array("C",$arrTipos)){?>
					        <div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>REGISTRO DE CAPÍTULO</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Cartório:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cartorioCapitulo" name="cartorioCapitulo" type="text" maxlength="100" value="<?php echo $dados->getCartorioCapitulo();?>" style="min-width: 320px" onkeypress="aumentarLetra('cartorioCapitulo',this.value);" onBlur="aumentarLetra('cartorioCapitulo',this.value);">
					                            </div>
				                        </div>
					                <div class="form-group">
					                            <label class="col-sm-3 control-label">Comarca:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="comarcaCapitulo" name="comarcaCapitulo" type="text" maxlength="100" value="<?php echo $dados->getComarcaCapitulo();?>" style="min-width: 320px" onkeypress="aumentarLetra('comarcaCapitulo',this.value);" onBlur="aumentarLetra('comarcaCapitulo',this.value);">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Número do Estatuto:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroCapitulo" name="numeroCapitulo" type="text" maxlength="10" value="<?php echo $dados->getNumeroCapitulo();?>" style="min-width: 320px">
					                            </div>
					                </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Número de Averbação:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="numeroAverbacaoCapitulo" name="numeroAverbacaoCapitulo" type="text" maxlength="10" value="<?php echo $dados->getNumeroAverbacaoCapitulo();?>" style="min-width: 320px">
                                            </div>
                                        </div>
					                <div class="form-group">
					                            <label class="col-sm-3 control-label">Número da Lei Municipal de Utilidade Pública:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroLeiMunicipalCapitulo" name="numeroLeiMunicipalCapitulo" type="text" maxlength="10" value="<?php echo $dados->getNumeroLeiMunicipalCapitulo();?>" style="min-width: 320px">
					                            </div>
					                </div>
					                <div class="form-group datapicker">
				                            <label class="col-sm-3 control-label">Data da Lei Municipal:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input maxlength="10" id="dataLeiMunicipalCapitulo" name="dataLeiMunicipalCapitulo" type="text" class="form-control" value="<?php echo $dados->getDataLeiMunicipalCapitulo();?>" style="max-width: 102px" onkeypress="mascaraData(this)" >
				                            </div>
				                        </div>
                                                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Número da Lei Estadual de Utilidade Pública:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroLeiEstadualCapitulo" name="numeroLeiEstadualCapitulo" type="text" maxlength="10" value="<?php echo $dados->getNumeroLeiEstadualCapitulo();?>" style="min-width: 320px">
					                            </div>
					                </div>
					                <div class="form-group datapicker">
				                            <label class="col-sm-3 control-label">Data da Lei Estadual:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input maxlength="10" id="dataLeiEstadualCapitulo" name="dataLeiEstadualCapitulo" type="text" class="form-control" value="<?php echo $dados->getDataLeiEstadualCapitulo();?>" style="max-width: 102px" onkeypress="mascaraData(this)" >
				                            </div>
				                        </div>    
					               </div>
                                    <?php if(in_array("L",$arrTipos)){?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">&nbsp;</label>
                                            <div class="col-sm-9">
                                                <a href="#" onclick="copiarInformacoesEstatuto('Capitulo','Loja');">(Copiar informações acima para registro de loja)</a>
                                            </div>
                                        </div>
                                    <?php }?>
					            </div>
					        </div>
                            <?php }?>
                        </div>
                        <?php if(in_array("L",$arrTipos)){?>
                        <div class="row">
                        	<div class="col-lg-6">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>REGISTRO DE LOJA</h5>
					                    <div class="ibox-tools">
					                            <i class="fa fa-pencil-square-o"></i>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                	<div class="form-group">
					                            <label class="col-sm-3 control-label">Cartório:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="cartorioLoja" name="cartorioLoja" type="text" maxlength="100" value="<?php echo $dados->getCartorioLoja();?>" style="min-width: 320px" onkeypress="aumentarLetra('cartorioLoja',this.value);" onBlur="aumentarLetra('cartorioLoja',this.value);">
					                            </div>
				                        </div>
					                <div class="form-group">
					                            <label class="col-sm-3 control-label">Comarca:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="comarcaLoja" name="comarcaLoja" type="text" maxlength="100" value="<?php echo $dados->getComarcaLoja();?>" style="min-width: 320px" onkeypress="aumentarLetra('comarcaLoja',this.value);" onBlur="aumentarLetra('comarcaLoja',this.value);">
					                            </div>
				                        </div>
				                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Número do Estatuto:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroLoja" name="numeroLoja" type="text" maxlength="10" value="<?php echo $dados->getNumeroLoja();?>" style="min-width: 320px">
					                            </div>
					                </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Número de Averbação:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="numeroAverbacaoLoja" name="numeroAverbacaoLoja" type="text" maxlength="10" value="<?php echo $dados->getNumeroAverbacaoLoja();?>" style="min-width: 320px">
                                            </div>
                                        </div>
					                <div class="form-group">
					                            <label class="col-sm-3 control-label">Número da Lei Municipal de Utilidade Pública:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroLeiMunicipalLoja" name="numeroLeiMunicipalLoja" type="text" maxlength="10" value="<?php echo $dados->getNumeroLeiMunicipalLoja();?>" style="min-width: 320px">
					                            </div>
					                </div>
					                <div class="form-group datapicker">
				                            <label class="col-sm-3 control-label">Data da Lei Municipal:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input maxlength="10" id="dataLeiMunicipalLoja" name="dataLeiMunicipalLoja" type="text" class="form-control" value="<?php echo $dados->getDataLeiMunicipalLoja();?>" style="max-width: 102px" onkeypress="mascaraData(this)" >
				                            </div>
				                        </div>
                                                        <div class="form-group">
					                            <label class="col-sm-3 control-label">Número da Lei Estadual de Utilidade Pública:</label>
					                            <div class="col-sm-9">
					                                <input class="form-control" id="numeroLeiEstadualLoja" name="numeroLeiEstadualLoja" type="text" maxlength="10" value="<?php echo $dados->getNumeroLeiEstadualLoja();?>" style="min-width: 320px">
					                            </div>
					                </div>
					                <div class="form-group datapicker">
				                            <label class="col-sm-3 control-label">Data da Lei Estadual:</label>
				                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
				                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                <input maxlength="10" id="dataLeiEstadualLoja" name="dataLeiEstadualLoja" type="text" class="form-control" value="<?php echo $dados->getDataLeiEstadualLoja();?>" style="max-width: 102px" onkeypress="mascaraData(this)" >
				                            </div>
				                        </div>    
					               </div>
					            </div>
					        </div>
                        </div>
                        <?php }?>
                        <input type="hidden" name="tipos" id="tipos" value="<?php echo $tipos;?>">
                        <?php 
                        	include_once 'model/organismoClass.php';
                            $o = new organismo();
                        	$nomeOrganismo="";
                        	$resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
		                    if($resultado2)
		                    {
			                    	foreach($resultado2 as $vetor2)
			                    	{
			                    		switch($vetor2['classificacaoOrganismoAfiliado']){
											case 1:
												$classificacao =  "Loja";
												break;
											case 2:
												$classificacao =  "Pronaos";
												break;
											case 3:
												$classificacao =  "Capítulo";
												break;
											case 4:
												$classificacao =  "Heptada";
												break;
											case 5:
												$classificacao =  "Atrium";
												break;
										}
										switch($vetor2['tipoOrganismoAfiliado']){
											case 1:
												$tipo = "R+C";
												break;
											case 2:
												$tipo = "TOM";
												break;
										}
											
										$nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];
                                                                                $siglaOrganismo = $vetor2["siglaOrganismoAfiliado"];
		                    		}
		                    }
                        ?>
                        <input type="hidden" name="siglaOrganismoAta" id="siglaOrganismoAta" value="<?php echo $siglaOrganismo;?>">
                        <input type="hidden" name="nomeOrganismoAta" id="nomeOrganismoAta" value="<?php echo $nomeOrganismo;?>">
                        <input type="hidden" name="nomeUsuarioAta" id="nomeUsuarioAta" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
                        <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-12">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Salvar o Estatuto"
									>
									<i class="fa fa-check fa-white"></i>&nbsp; Salvar
								</button>
								&nbsp; <a href="?corpo=buscaEstatuto"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
	
					</form>
                </div>
            </div>
        </div>
	</div>
	
	

</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->


<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	