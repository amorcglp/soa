<?php
      @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
      
    include_once("controller/atividadeAgendaTipoController.php");
    require_once ("model/atividadeAgendaTipoClass.php");

    $idAtividadeAgendaTipo	= isset($_GET['idAtividadeAgendaTipo']) ? json_decode($_GET['idAtividadeAgendaTipo']) : '';

    if($idAtividadeAgendaTipo==''){
        echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaAtividadeAgendaTipo';</script>";
    }

    $a = new atividadeAgendaTipoController();
    $dados = $a->buscaAtividadeAgendaTipo($idAtividadeAgendaTipo);
    //echo "<pre>";print_r($dados);
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividade Para Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeAgendaTipo">Tipos de Atividade</a>
            </li>
            <li class="active">
                <strong>
                    <a>Edição de Tipos de Atividades</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição de Tipos de Atividade Agenda</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeAgendaTipo">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="fk_idAtividadeAgendaTipo" id="fk_idAtividadeAgendaTipo" value="<?php echo $idAtividadeAgendaTipo;?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Atividade: </label>
                            <div class="col-sm-10">
                                <input type="text" name="nome" id="nome" class="form-control" style="max-width: 320px" required="required" value="<?php echo $dados->getNome();?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; max-width: 850px">
	                            	<div class="mail-text h-200">
										<textarea class="summernote" id="descricao" name="descricao"><?php echo $dados->getDescricao();?></textarea>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Destinado ao OA: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="classiOaTipoAtividadeAgenda" id="classiOaTipoAtividadeAgenda" style="max-width: 250px">
                                    <option value="0">Selecione</option>
                                    <option value="1" <?php if($dados->getClassiOaTipoAtividadeAgenda()==1){ echo "selected";}?>>Loja</option>
                                    <option value="2" <?php if($dados->getClassiOaTipoAtividadeAgenda()==2){ echo "selected";}?>>Pronaos</option>
                                    <option value="3" <?php if($dados->getClassiOaTipoAtividadeAgenda()==3){ echo "selected";}?>>Capítulo</option>
                                    <!--<option value="4">Todos</option>-->
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" id="statusTipoAtividadeAgenda" name="statusTipoAtividadeAgenda" value="<?php echo $dados->getStatusTipoAtividadeAgenda();?>">
                                <input type="hidden" id="usuario" name="usuario" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <a class="btn btn-white" href="?corpo=buscaAtividadeAgendaTipo"> Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>