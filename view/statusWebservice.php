<?php 

ini_set('max_execution_time', 0);
session_start();
//error_reporting(E_ALL);
ini_set("display_errors", 1 );


require_once 'webservice/wsInovad.php';

$seqCadast = 550110;//Braz

$array = [0 => $seqCadast];
 $teste = '';
foreach ($array as $key => $value){
    if($teste != ''){
        $teste = $teste.', '.$value;
    }else{
        $teste = $value;
    }
}

//Buscar Nome do Usuário vindo do ORCZ
$vars = array('seq_cadast' => $teste);
$resposta = json_decode(json_encode(restAmorc("membros",$vars)),true);;
$obj2 = json_decode(json_encode($resposta),true);
//echo "<pre>";print_r($obj2);exit();
$status=0;

if(isset($obj2['data'][0]['nom_client']))
{
    $status=1;
    $nomeOficial = $obj2['data'][0]['nom_client'];
    $emailOficial = $obj2['data'][0]['des_email'];
    //$celularOficial = "(0".substr($obj2['data'][0]['num_celula'],0,2).") ".substr($obj2['data'][0]['num_celula'],2,5)."-".substr($obj2['data'][0]['num_celula'],7,4);
    $celularOficial = $obj2['data'][0]['num_celula'];
}

?>

<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
    $(function() {
        $( document ).tooltip();
    });
</script>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Status Webservice</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Administração</a>
            </li>
            <li class="active">
                <a href="?corpo=statusWebservice">Status Webservice</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<br>
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Status Webservice</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <div style="font-size: 32px;">
                        <?php if($status==1){
                                ?>
                                    <i class="fa fa-thumbs-o-up modal-icon text-info"></i>
                                <?php
                            }else{
                                ?>
                                    <i class="fa fa-thumbs-o-down modal-icon text-danger"></i>
                                <?php
                        }?>
                    </div>
                    Nome retornado para o seqCadast (550110) => <?php echo $nomeOficial;?><br>
                    E-mail => <?php echo $emailOficial;?><br>
                    Celular => <?php echo $celularOficial;?><br>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('ready', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
