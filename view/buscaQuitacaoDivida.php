<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("model/quitacaoDividaClass.php");
include_once("lib/functions.php");

$d = new QuitacaoDivida();

$liberaGLP=false;
if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
    $liberaGLP = true;
}
?>
<?php 
$seqCadastSolicitanteToken=0;

//Prazo
$inicio=date('Y-m')."-01";
$parcelas=1;
$data_termino = new DateTime($inicio);
$data_termino->sub(new DateInterval('P'.$parcelas.'M'));
$prazo=$data_termino->format('Y-m-d');
$anoPrazo = $data_termino->format('Y');
$mesPrazo = $data_termino->format('m');
$diaPrazo = $data_termino->format('d');

//Se for mes atual ou mes anterior liberar entradas

$mesAnteriorDaDataAtual=date('m', strtotime('-1 months', strtotime(date('Y')."-".date('m')."-01")));
$mesProximoDaDataAtual=date('m', strtotime('+1 months', strtotime(date('Y')."-".date('m')."-01")));

if(date('m')==1)
{
    $anoAnteriorDaDataAtual=$anoAtual-1;
}else{
    $anoAnteriorDaDataAtual=$anoAtual;
}
if(date('m')==12)
{
    $anoProximoDaDataAtual=$anoAtual+1;
}else{
    $anoProximoDaDataAtual=$anoAtual;
}

$liberaMesAtualOuAnterior=false;
if(
    ($_REQUEST['mesAtual']==date('m')&&$_REQUEST['anoAtual']==date('Y'))
    ||
    ($_REQUEST['mesAtual']==$mesAnteriorDaDataAtual&&$_REQUEST['anoAtual']==$anoAnteriorDaDataAtual)
)
{
    $liberaMesAtualOuAnterior=true;
}
?>

<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Pagamento salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$excluir = isset($_REQUEST['excluir'])?$_REQUEST['excluir']:null;
if($excluir==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Seu registro de quitação foi deletado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>

<?php $liberarEmMassa = 1;?>


<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Quitação de Dívidas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Financeiro</a>
            </li>
            <li class="active">
                <strong><a>Quitação de Dívidas</a></strong>
            </li>
        </ol>
    </div>

</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Quitação de Dívidas</h5>									
                </div>
                <div class="ibox-content">
                       <div class="alert alert-warning alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link">Atenção:</a> Instruções sobre como funciona <a class="alert-link">Quitação de Dívidas</a>.<br>
                            A cada novo registro de quitação/pagamento da dívida, <a class="alert-link">é decrementado o valor do total da dívida</a> e é mostrado o resultado na coluna do valor restante.<br>
                        </div>
                    <div class="" style="width:100%;">
                        <table width="100%">
                            <td>
                                <?php if (in_array("1", $arrNivelUsuario)&&!$usuarioApenasLeitura) { ?>
                                    <a onclick="#" href="?corpo=cadastroQuitacaoDivida" class="btn btn-primary ">Adicionar Pagamento</a>
                                <?php } ?>
                                <?php if ($liberaGLP) { ?>
                                <button class="btn btn-sm btn-danger" type="button" onclick="document.getElementById('idFuncionalidade').value='9';document.getElementById('idItemFuncionalidade').value='0';document.getElementById('idOrganismoAfiliadoLiberarToken').value='<?php echo $idOrganismoAfiliado;?>';" data-target="#mySeeToken" data-toggle="modal" data-placement="left">
                                    <i class="fa fa-key"></i>
                                </button>
                                <?php }?>     
                            </td>
                        </table>
                    </div>
                    <br>
                    <form>
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="editable2" >
                            <thead>
                                <tr id="linha0">
                                    <th width="80">Dívida</th>
                                    <th width="100">Data do Pagamento</th>
                                    <th width="100">Valor Integral</th>
                                    <th width="100">Valor Pago</th>
                                    <th width="100">Valor Restante</th>
                                    <th width="100">Observação</th>
                                    <th width="100">Ações</th>
                                </tr>
                            </thead>
                            <tbody id="tabela">
                                <?php
                                $totalGeral = 0;
                                $resultado = $d->listaQuitacaoDivida($idOrganismoAfiliado);
                                $i = 0;
                                if ($resultado) {
                                    foreach ($resultado as $vetor) {
                                        $valorDivida = (float) str_replace(",",".",str_replace(".","",$vetor['valorDivida']));
                                        $valorPagamento = (float) str_replace(",",".",str_replace(".","",$vetor['valorPagamento']));
                                        if($i==0||$fk_idDivida!=$vetor['fk_idDivida'])
                                        {    
                                            $valorRestante = $valorDivida - $valorPagamento;
                                        }else{
                                            $valorRestante = $valorRestante - $valorPagamento;
                                        }
                                        
                                        //Verificar se precisa bloquear registro
                                        $bloqueia=false;
                                        
                                        $data_ini  = substr($vetor['dataPagamento'],0,4)."-".substr($vetor['dataPagamento'],5,2)."-01";
                                        $data_end  = date("Y-m-d");
                                        $anoAtual  = (int) substr($vetor['dataPagamento'],0,4);
                                        $mesAtual  = (int) substr($vetor['dataPagamento'],5,2);

                                        $meses = date_diffe($data_ini, $data_end);
                                        //$meses=7;

                                        //Verificar se houve uma assinatura
                                        include_once('model/financeiroMensalClass.php');
                                        $f = new financeiroMensal();

                                        $mestre = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
                                        $secretario = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);
                                        $guardiao = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,369);
                                        $pjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,371);
                                        $sjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,373);
                                        $tjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,375);
                                        $ma = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,377);
                                        $vdc = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,533);

                                        //Verificar se houve uma assinatura
                                        if($mestre||$secretario||$guardiao||$pjd||$sjd||$tjd||$ma||$vdc){
                                            $bloqueia=true;
                                        }

                                        if($meses>2)
                                        {
                                            $bloqueia=true;
                                        }
                                        
                                        //Se tem cookie que libera token
                                        $liberaTokenUsuarioIdItemFuncionalidade=false;
                                        $liberarEmMassa=0;
                                        $mes=0;
                                        $ano=0;
                                        if (isset($_COOKIE['liberarTokenUsuario'])) {
                                            //Verificar se token é da ata de reunião mensal
                                            //echo "tk=>";
                                            $u = new Usuario();
                                            $resultado7 = $u->retornaTokenIntegridade($_COOKIE['liberarTokenUsuario'],9);
                                            //echo "<pre>";print_r($resultado7);
                                            if($resultado7)
                                            {
                                                foreach($resultado7 as $vetor7)
                                                {
                                                    $liberarEmMassa = $vetor7['liberarEmMassa'];
                                                    //if($vetor7['mes']==$mesAtual&&$vetor7['ano']==$anoAtual)
                                                    //{    
                                                        $liberaTokenUsuarioIdItemFuncionalidade=true;
                                                        //$timelife=$vetor['timelife'];
                                                        //$timelifeExpira = $timelife + (60 * 60 );
                                                        $date = new DateTime($vetor7['dataInicial'].' 23:59:59');
                                                        $timelife = $date->getTimestamp();
                                                        $date2 = new DateTime($vetor7['dataFinal'].' 23:59:59');
                                                        $timelifeExpira = $date2->getTimestamp();
                                                        $seqCadastSolicitanteToken=$vetor7['seqCadast'];
                                                        $mesTexto = mesExtensoPortugues($vetor7['mes']);
                                                        $mes = $vetor7['mes'];
                                                        $ano = $vetor7['ano'];
                                                   //}
                                                }
                                            }
                                            if($liberarEmMassa==1)
                                            {
                                                $bloqueia=false;
                                            } 
                                        ?>
                                        <?php }

                                            //Libera por login
                                            $u = new Usuario();
                                            $resultado77 = $u->retornaTokenPorLogin($_SESSION['loginUsuario'],date('Y-m-d'),9);
                                            if($resultado77)
                                            {
                                                foreach($resultado77 as $vetor77)
                                                {
                                                    $liberarEmMassa = $vetor77['liberarEmMassa'];
                                                    if($vetor77['mes']==$mesAtual&&$vetor77['ano']==$anoAtual)
                                                    {
                                                        $liberaTokenUsuarioIdItemFuncionalidade=true;

                                                        //$timelife=$vetor['timelife'];
                                                        //$timelifeExpira = $timelife + (60 * 60 );
                                                        $date = new DateTime($vetor77['dataInicial'].' 23:59:59');
                                                        $timelife = $date->getTimestamp();
                                                        $date2 = new DateTime($vetor77['dataFinal'].' 23:59:59');
                                                        $timelifeExpira = $date2->getTimestamp();
                                                        $seqCadastSolicitanteToken=$vetor77['seqCadast'];
                                                        $mesTexto = mesExtensoPortugues($vetor77['mes']);
                                                        $ano = $vetor77['ano'];
                                                    }
                                                }
                                                if($liberarEmMassa==1)
                                                {
                                                    $bloqueia=false;
                                                }
                                            }
                                            //Fim libera por login

                                        //echo "<br>==>TokenMês:".$mes;
                                        //echo "<br>==>TokenAno:".$ano;
                                        if ($liberaTokenUsuarioIdItemFuncionalidade != false&&$seqCadastSolicitanteToken==$_SESSION['seqCadast']) {
                                            if($liberarEmMassa == 0)
                                            {
                                        ?>
                                        <script>
                                            window.onload = function () {
                                                swal({
                                                    title: "Aviso!",
                                                    text: "O Token solicitado para a as Quitações de Dividas com pagamento no Mês de <?php echo $mesTexto;?>, Ano <?php echo $ano;?> expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
                                                    type: "warning",
                                                    confirmButtonColor: "#1ab394"
                                                });
                                            }
                                        </script>
                                        <?php }else{
                                                ?>
                                                <script>
                                                    window.onload = function () {
                                                        swal({
                                                            title: "Aviso!",
                                                            text: "O Token solicitado para alterar todas as Quitações de Dividas expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
                                                            type: "warning",
                                                            confirmButtonColor: "#1ab394"
                                                        });
                                                    }
                                                </script>
                                        <?php 
                                            }
                                        }
                                        $fimDividaMes = (int) substr($vetor['dataPagamento'],5,2);
                                        $fimDividaAno = (int) substr($vetor['dataPagamento'],0,4);
                                        //echo "<br>==>ListMês:".$fimDividaMes;
                                        //echo "<br>==>ListAno:".$fimDividaAno;
                                        ?>
                                        <tr>
                                            <td id=""><?php echo $vetor['descricaoDivida']; ?></td>
                                            <td id=""><?php echo substr($vetor['dataPagamento'], 8, 2) . "/" . substr($vetor['dataPagamento'], 5, 2) . "/" . substr($vetor['dataPagamento'], 0, 4); ?></td>
                                            <td id=""><?php echo $vetor['valorDivida']; ?></td>
                                            <td id=""><?php echo $vetor['valorPagamento']; ?></td>
                                            <td id=""><?php echo number_format($valorRestante, 2, ',', '.'); ?></td>
                                            <td id=""><?php echo $vetor['observacao']; ?></td>
                                            <td>
                                                <div style='width:100'>
                                                    <?php
                                                    if(!$usuarioApenasLeitura) {
                                                        if ((in_array("2", $arrNivelUsuario) && !$bloqueia) || $liberaGLP||$liberarEmMassa==1||$liberaMesAtualOuAnterior || ($liberaTokenUsuarioIdItemFuncionalidade == true && $mes == $fimDividaMes && $ano == $fimDividaAno)) {
                                                            ?>
                                                            <a href="?corpo=alteraQuitacaoDivida&id=<?php echo $vetor['idQuitacaoDivida']; ?>"
                                                               class='btn btn-xs btn-primary'> Editar</a>
                                                        <?php } else { ?>
                                                            <a href="#" class='btn btn-xs btn-primary'
                                                               data-target="#mySeeIntegridade" data-toggle="modal"
                                                               data-placement="left"
                                                               onclick="document.getElementById('idFuncionalidadeTicket').value='9';document.getElementById('mesTicket').value='<?php echo $mesAtual; ?>';document.getElementById('anoTicket').value='<?php echo $anoAtual; ?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado; ?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"]; ?>';"><i
                                                                        class="fa fa-lock"></i> Editar</a>
                                                        <?php } ?>
                                                        <?php if ((in_array("3", $arrNivelUsuario) && !$bloqueia) || $liberaGLP||$liberarEmMassa==1||$liberaMesAtualOuAnterior || ($liberaTokenUsuarioIdItemFuncionalidade == true && $mes == $fimDividaMes && $ano == $fimDividaAno)) { ?>
                                                            <a href="#" class='btn btn-xs btn-danger'
                                                               onclick="excluirQuitacaoDivida('<?php echo $vetor['idQuitacaoDivida']; ?>');">
                                                                Excluir</a>
                                                        <?php } else { ?>
                                                            <a href="#" class='btn btn-xs btn-danger'
                                                               data-target="#mySeeIntegridade" data-toggle="modal"
                                                               data-placement="left"
                                                               onclick="document.getElementById('idFuncionalidadeTicket').value='9';document.getElementById('mesTicket').value='<?php echo $mesAtual; ?>';document.getElementById('anoTicket').value='<?php echo $anoAtual; ?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado; ?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"]; ?>';"><i
                                                                        class="fa fa-lock"></i> Excluir</a>
                                                        <?php }
                                                    }else{
                                                        echo "<span class=\"btn btn-default btn-rounded\">Apenas Leitura</span>";
                                                    }
                                                    ?>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php 
                                    $fk_idDivida = $vetor['fk_idDivida'];
                                    $i++;
                                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function () {

    });
</script>	
<div class="modal inmodal" id="mySeeIntegridade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-lock modal-icon"></i>
                <h4 class="modal-title">
                    Solicitação de Token
                    <input type="hidden" name="codigoItem" id="codigoItem">
                </h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUploadJaEntregue" style="background-color:white"></div>
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Lembre-se: você tem até 1 mês para enviar todos os Relatórios Assinados para a GLP.
                        <br />
                        Deseja ter acesso a esse item e regularizar as informações? Descreva o motivo e solicite seu Token.
                        <br />
                        Caso seja aprovada sua solicitação, você receberá uma notificação através do SOA e por e-mail.
                        <br />
                        Atenção: A liberação do Token depende diretamente da análise a ser realizada pelos colaboradores da GLP, portanto depende do horário de funcionamento da Grande Loja, por este motivo, pedimos um prazo de até 48h para as liberações.
                        <br />
                        <br />
                        <b>Motivo da liberação do token:</b>
                        <br />
                        <br />
                        <textarea rows="5" cols="50" name="motivoTicket" id="motivoTicket"></textarea>
                        <br />
                        <button type="button" class="btn btn-sm btn-info" data-dismiss="modal" onclick="abrirTicketParaLiberarFuncionalidade();">Abrir Ticket</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="seqCadastTicket" id="seqCadastTicket" value="<?php $_SESSION['seqCadast'];?>" />
                <input type="hidden" name="idFuncionalidadeTicket" id="idFuncionalidadeTicket" />
                <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" />
                <input type="hidden" name="mesTicket" id="mesTicket" />
                <input type="hidden" name="anoTicket" id="anoTicket" />
                <input type="hidden" name="idItemFuncionalidadeTicket" id="idItemFuncionalidadeTicket" value="0"/>
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeToken" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-unlock modal-icon"></i>
                <h4 class="modal-title">Liberar Token para Usuário</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <table>
                            <tr>
                                <td>Organismo:</td>
                                <td><span id="nomeOrganismoToken"><?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);?></span> </td>
                            </tr>
                            <tr>
                                <td></td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Login no SOA:</td>
                                <td><input type="text" name="usuarioToken" id="usuarioToken" /></td>
                            </tr>
                            <tr>
                                <td><br>Data Inicial:</td>
                                <td><br><input type="text" class="data" name="dataInicial" id="dataInicial" value="<?php echo date('d/m/Y');?>"></td>
                            </tr>
                            <tr>
                                <td><br>Data Final:</td>
                                <td>
                                    <br><input type="text" class="data" name="dataFinal" id="dataFinal">
                                </td>
                            </tr>
                            <tr>
                                <td><br>Mês da Despesa:</td>
                                <td><br><input type="text" name="mesToken" id="mesToken" /></td>
                            </tr>
                            <tr>
                                <td><br>Ano da Despesa:</td>
                                <td><br><input type="text" name="anoToken" id="anoToken" value="<?php echo date('Y');?>" /></td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Liberar em massa:</td>
                                <td>
                                    <br><input type="checkbox" name="liberarEmMassa" id="liberarEmMassa" onclick="zerarMesAno();">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <br>
                                    <input type="hidden" name="idFuncionalidade" id="idFuncionalidade" />
                                    <input type="hidden" name="idOrganismoAfiliadoLiberarToken" id="idOrganismoAfiliadoLiberarToken" />
                                    <input type="hidden" name="idItemFuncionalidade" id="idItemFuncionalidade" value="<?php echo $anoAtual.$mesAtual.$idOrganismoAfiliado;?>"/>
                                    <button class="btn btn-sm btn-danger" type="button" onclick="liberarTokenUsuario();">
                                        <i class="fa fa-key"></i>Liberar
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeIntegridadeTicketAberto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-hourglass-start modal-icon"></i>
                <h4 class="modal-title">Ticket já está aberto para esse item aguarde uma resposta</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Você já abriu um ticket para esse item, aguarde resposta da GLP. Enquanto a GLP não liberar você não tem acesso a ele. Caso a GLP libere o acesso a esse item, você receberá um e-mail com o token para alteração.
                        <br />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>              
