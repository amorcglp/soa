<?php

@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("lib/webservice/retornaInformacoesMembro.php");
include_once("model/perfilUsuarioClass.php");
include_once("controller/planoAcaoOrganismoController.php");
include_once("model/planoAcaoOrganismoParticipanteClass.php");

$parc = new planoAcaoOrganismoController();

$pesquisar = (isset($_GET['pesquisar'])) ? $_GET['pesquisar'] : null;   

/*PAGINACAO*/

define('QTDE_REGISTROS', 5);   
define('RANGE_PAGINAS', 3); 

/* Recebe o número da página via parâmetro na URL */  
 $pagina_atual = (isset($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 1;   
   
 /* Calcula a linha inicial da consulta */  
 $linha_inicial = ($pagina_atual -1) * QTDE_REGISTROS; 
 
 /* Conta quantos registos existem na tabela */  
 $total_registros = $resultado = $parc->totalPlanoAcaoOrganismo($pesquisar);
 
 /* Idêntifica a primeira página */  
 $primeira_pagina = 1;   
   
 /* Cálcula qual será a última página */  
 $ultima_pagina  = ceil($total_registros / QTDE_REGISTROS);   
   
 /* Cálcula qual será a página anterior em relação a página atual em exibição */   
 $pagina_anterior = ($pagina_atual > 1) ? $pagina_atual -1 : 0 ;   
   
 /* Cálcula qual será a pŕoxima página em relação a página atual em exibição */   
 $proxima_pagina = ($pagina_atual < $ultima_pagina) ? $pagina_atual +1 : 0 ;  
   
 /* Cálcula qual será a página inicial do nosso range */    
 $range_inicial  = (($pagina_atual - RANGE_PAGINAS) >= 1) ? $pagina_atual - RANGE_PAGINAS : 1 ;   
   
 /* Cálcula qual será a página final do nosso range */    
 $range_final   = (($pagina_atual + RANGE_PAGINAS) <= $ultima_pagina ) ? $pagina_atual + RANGE_PAGINAS : $ultima_pagina ;   
   
 /* Verifica se vai exibir o botão "Primeiro" e "Pŕoximo" */   
 $exibir_botao_inicio = ($range_inicial < $pagina_atual) ? 'mostrar' : 'esconder'; 
   
 /* Verifica se vai exibir o botão "Anterior" e "Último" */   
 $exibir_botao_final = ($range_final > $pagina_atual) ? 'mostrar' : 'esconder';  
?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Plano salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Plano já cadastrado anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Planos de Ação do Organismo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Planos de Ação do Organismo</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInUp">
					<?php 
					$nomeOrganismo="";
					include_once ('model/organismoClass.php');
					$o = new organismo();
					$resultado = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
					if($resultado)
					{
						foreach($resultado as $vetor)
						{
							switch ($vetor['classificacaoOrganismoAfiliado']) {
								case 1:
									$classificacao = "Loja";
									break;
								case 2:
									$classificacao = "Pronaos";
									break;
								case 3:
									$classificacao = "Capítulo";
									break;
								case 4:
									$classificacao = "Heptada";
									break;
								case 5:
									$classificacao = "Atrium";
									break;
							}
							switch ($vetor['tipoOrganismoAfiliado']) {
								case 1:
									$tipo = "R+C";
									break;
								case 2:
									$tipo = "TOM";
									break;
							}

							$nomeOrganismo = $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor["nomeOrganismoAfiliado"];
							
						}
					}
					?>
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Planos de Ação do Organismo <?php if(($sessao->getValue("fk_idDepartamento")!=3) && ($sessao->getValue("fk_idDepartamento")!=2) ){ ?><?php echo $nomeOrganismo;?><?php }?></h5>
                            <div class="ibox-tools">
                                <?php
                                if(in_array("1",$arrNivelUsuario)&&!$usuarioApenasLeitura) {
                                ?>
                                <a href="?corpo=cadastroPlanoAcaoOrganismo" class="btn btn-primary btn-xs">
                                	<i class="fa fa-plus"></i>
                                	Novo Plano de Ação
                                </a>
                                <?php }?>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div style="text-align:right">
                                Pesquisar: <input type="text" name="pesquisar" id="pesquisar" onkeyup="pesquisarPlanoAcaoOrganismo(this.value);" value="<?=$pesquisar?>">
                            </div> 
                            <br>
                            <div class="project-list" id="resultadoPesquisa">

                                <table class="table table-hover">
                                    <tbody>
                                    	<?php
                                    	
			                            $resultado = $parc->listaPlanoAcaoOrganismo($pesquisar,$linha_inicial,QTDE_REGISTROS);
			                            
			                            include_once("model/planoAcaoOrganismoMetaClass.php");
										
			
			                            if ($resultado) {
			                                foreach ($resultado as $vetor) {
			                                	
			                                	if(($vetor['fk_idOrganismoAfiliado']==$idOrganismoAfiliado)||($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2))
			                                	{
			                                	
				                                	$parm = new planoAcaoOrganismoMeta();
				                                	
				                                	$resultadoMeta = $parm->listaMeta($vetor['idPlanoAcaoOrganismo']);
														
													if($resultadoMeta)
													{
														$totalMeta = count($resultadoMeta);
													}else{
														$totalMeta = 0;
													}
				                                	
													$resultadoMetaConcluida = $parm->listaMeta($vetor['idPlanoAcaoOrganismo'],1);
														
													if($resultadoMetaConcluida)
													{
														$totalMetaConcluida = count($resultadoMetaConcluida);
													}else{
														$totalMetaConcluida = 0;
													}
													
				                                	/*
													 * Cálculo da percentagem concluída
													 */
													if($totalMetaConcluida>0)
													{
														$percentual = round(($totalMetaConcluida*100)/$totalMeta);
													}else{
														$percentual = 0;
													}
                                    	?>
	                                    <tr>
	                                        <td class="project-status">
	                                            
	                                            <?php 
	                                            if($vetor['statusPlano']==1)
	                                            {
	                                            	echo "<div id=\"statusPlano".$vetor['idPlanoAcaoOrganismo']."\"><a href=\"#\" ";
	                                            	if(in_array("2",$arrNivelUsuario)){
	                                            		echo "onclick=\"mudaStatusPlanoAcaoOrganismo('".$vetor['idPlanoAcaoOrganismo']."','1')\"";
	                                            	}
	                                            	echo "><span class=\"label label-primary\">Ativo</span></a></div>";
	                                            }else{
	                                            	echo "<div id=\"statusPlano".$vetor['idPlanoAcaoOrganismo']."\"><a href=\"#\" ";
	                                            	if(in_array("2",$arrNivelUsuario)){
	                                            		echo "onclick=\"mudaStatusPlanoAcaoOrganismo('".$vetor['idPlanoAcaoOrganismo']."','0')\"";
	                                            	}
	                                            	echo "><span class=\"label label-default\">Inativo</span></a></div>";
	                                            }
	                                            ?>
	                                            
	                                        </td>
	                                        <td class="project-title">
	                                            <a href="?corpo=buscaPlanoAcaoOrganismoDetalhe&idPlanoAcaoOrganismo=<?php echo $vetor['idPlanoAcaoOrganismo'];?>"><?php echo $vetor['tituloPlano'];?></a>
	                                            <br/>
	                                            <small>Palavras-chave: <?php echo $vetor['palavrasChave'];?></small>
	                                        </td>
	                                        <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2) ){
		                                       		$resultado2 = $o->listaOrganismo(null,null,null,null,null,$vetor['fk_idOrganismoAfiliado']);
													if($resultado2)
													{
														foreach($resultado2 as $vetor2)
														{
															switch ($vetor2['classificacaoOrganismoAfiliado']) {
																case 1:
																	$classificacao = "Loja";
																	break;
																case 2:
																	$classificacao = "Pronaos";
																	break;
																case 3:
																	$classificacao = "Capítulo";
																	break;
																case 4:
																	$classificacao = "Heptada";
																	break;
																case 5:
																	$classificacao = "Atrium";
																	break;
															}
															switch ($vetor2['tipoOrganismoAfiliado']) {
																case 1:
																	$tipo = "R+C";
																	break;
																case 2:
																	$tipo = "TOM";
																	break;
															}
								
															$nomeOrganismo2 = $vetor2["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor2["nomeOrganismoAfiliado"];
															
														}
													}
	                                        	?>
		                                        <td>
		                                        	<?php echo $nomeOrganismo2;?>
		                                        </td>
	                                        <?php }?>
	                                        <td class="project-completion">
	                                                <small>Progresso: <?php echo $percentual;?>%</small>
	                                                <div class="progress progress-mini">
	                                                    <div style="width: <?php echo $percentual;?>%;" class="progress-bar"></div>
	                                                </div>
	                                        </td>
	                                        <?php 
	                                        $parp = new planoAcaoOrganismoParticipante();
	                                        $resultado2 = $parp->listaParticipantes($vetor['idPlanoAcaoOrganismo']);
	                                        ?>
	                                        <td class="project-people">
	                                        	<?php 
	                                        	if($resultado2)
	                                        	{
	                                        		foreach($resultado2 as $vetor2)
	                                        		{
	                                        			$pu = new perfilUsuario();
	                                        			$resultado3 = $pu->listaAvatarUsuario($vetor2['seqCadast']);
	                                        			if($resultado3)
	                                        			{
		                                        			foreach($resultado3 as $vetor3)
		                                        			{
		                                        				
			                                        			if($vetor3['avatarUsuario']!="")
			                                        			{
			                                        			
			                                        	?>
			                                            			<a href="#" title="<?php echo $vetor3['nomeUsuario'];?>"><img alt="image" class="img-circle" src="<?php echo $vetor3['avatarUsuario'];?>"></a>
			                                            <?php 
			                                        			}else{
			                                        			?>
			                                        				<a href="#" title="<?php echo retornaNomeCompleto($vetor3['seqCadast']);?>"><img alt="image" class="img-circle" src="img/default-user.png"></a>
			                                        			<?php 	
			                                        			}
		                                        			}	
	                                        			}
	                                        		}
	                                        	}
	                                            ?>	
	                                        </td>
	                                        <td class="project-actions">
	                                            <a href="?corpo=buscaPlanoAcaoOrganismoDetalhe&idPlanoAcaoOrganismo=<?php echo $vetor['idPlanoAcaoOrganismo'];?>" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> Visualizar </a>
	                                            <?php if(in_array("2",$arrNivelUsuario)&&!$usuarioApenasLeitura){?>
	                                            <a href="?corpo=alteraPlanoAcaoOrganismo&idPlanoAcaoOrganismo=<?php echo $vetor['idPlanoAcaoOrganismo'];?>" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Editar </a>
	                                            <?php }?>
	                                            <a href="#" onclick="imprimirPlanoAcaoOrganismo('<?php echo $vetor['idPlanoAcaoOrganismo'];?>','<?php echo $idOrganismoAfiliado;?>');" class="btn btn-white btn-sm"><i class="fa fa-print"></i> Imprimir </a>
                                                <?php if(!$usuarioApenasLeitura){?>
                                                    <a href="#" onclick="excluirPlanoAcaoOrganismo('<?php echo $vetor['idPlanoAcaoOrganismo'];?>');" class="btn btn-white btn-sm"><i class="fa fa-trash"></i> Excluir </a>
                                                <?php }?>
	                                        </td>
	                                    </tr>
	                                    <?php 
			                                	}
			                                }
			                            }
	                                    ?>
                                    </tbody>
                                </table>
                                <?php if ($resultado) { ?>  
                                <div style="text-align:right">
                                    <ul class="pagination">
                                        <?php if($exibir_botao_inicio=='mostrar'){ ?>
                                        <li class="paginate_button previous">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$primeira_pagina?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">
                                                Primeira
                                            </a>
                                        </li>
                                        <li class="paginate_button previous">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$pagina_anterior?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">
                                                Anterior
                                            </a>
                                        </li>
                                        <?php  
                                        }
                                        
                                            /* Loop para montar a páginação central com os números */   
                                            $r=2;
                                            for ($k=$range_inicial; $k <= $range_final; $k++):   
                                              $destaque = ($k == $pagina_atual) ? 'destaque' : '' ;  
                                              ?>
                                                    <li class="paginate_button <?php if($destaque=='destaque'){ echo "active";}?>">
                                                        <?php if($destaque=='destaque'){ ?>
                                                        <a href="#">
                                                        <?=$k?>
                                                        </a>    
                                                        <?php }else{ ?>    
                                                        <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$k?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="<?=$r?>" tabindex="0">
                                                            <?=$k?>
                                                        </a>
                                                        <?php }?>    
                                                            
                                                    </li>
                                            <?php 
                                            $r++;
                                            endfor; ?>
                                        <?php if($exibir_botao_final=='mostrar'){?>            
                                        <li class="paginate_button next">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$proxima_pagina?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="<?=$r++?>" tabindex="0">
                                                Próxima
                                            </a>
                                        </li>
                                        <li class="paginate_button next">
                                            <a href="?corpo=buscaPlanoAcaoOrganismo&page=<?=$ultima_pagina?>&pesquisar=<?=$pesquisar?>" aria-controls="DataTables_Table_0" data-dt-idx="<?=$r++?>" tabindex="0">
                                                Última
                                            </a>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>
                                <?php } ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	