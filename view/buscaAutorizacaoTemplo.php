<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>
			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Autorização de Templo</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="painelDeControle.php">Home</a>
                        	</li>
	                        <li>
	                            <a href="?corpo=buscaAutorizacaoTemplo">Consulta</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Autorização de Templo</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
	            
	            <!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Autorização de Templo</h5>
									<div class="ibox-tools">
										<!--  
										<a class="btn btn-xs btn-primary" href="?opcao=oficial_form.php">
											<i class="fa fa-plus"></i> Novo Membro
										</a>
										-->
									</div>
								</div>
								<div class="ibox-content">
									<table style="border-spacing: 10px;border-collapse: separate;">
										<tr>
											<td>
												Código de Afiliação: 
											</td>
											<td>	
												<input type="text" name="codigoAfiliacao" id="codigoAfiliacao" onkeypress="return SomenteNumero(event)">
											</td>
										</tr>
                                                                                <tr>
											<td>
												ou Nome: 
											</td>
											<td>	
												<input type="text" name="nome" id="nome">
											</td>
										</tr>
										<tr>
                                                                                        <td>
												<input type="hidden" id="fk_idOrganismoAfiliado" name="fk_idOrganismoAfiliado" value="<?php echo $idOrganismoAfiliado;?>">
												<input type="hidden" id="usuario" name="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
											</td>
											<td>	
												<a href="#" class="btn btn-outline btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codigoAfiliacao','nome','codigoAfiliacao','nome','','','myModalPesquisa','informacoesPesquisa','N','autorizacaoTemplo');">Imprimir autorização</a>
                                                                                                &nbsp;
                                                                                                <a href="#" class="btn btn-outline btn-primary" onclick="limparCamposNomeCodigoAfiliacao()">Limpar campos</a>
											</td>
										</tr>
									</table>
								</div>
								<br>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->
	
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>