<?php
//Pegar as permiss�es dos grupos e subgrupos que podem ver os dashboars

include_once('model/departamentoObjetoDashboardClass.php');
$dod = new DepartamentoObjetoDashboard();
$dod->setFkIdDepartamento($fk_idDepartamento);
$resOpcoesMarcadas = $dod->buscaPorDepartamento();
$arrPermissaoDashboardsDepartamentos = array();
$arrPermissaoDashboardsDepartamentos['fk_idObjetoDashboard'][] = "";
if ($resOpcoesMarcadas) {
    foreach ($resOpcoesMarcadas as $vetor) {
        $arrPermissaoDashboardsDepartamentos['fk_idDepartamento'][] = $vetor['fk_idDepartamento'];
        $arrPermissaoDashboardsDepartamentos['fk_idObjetoDashboard'][] = $vetor['fk_idObjetoDashboard'];
    }
}

include_once('model/funcaoObjetoDashboardClass.php');
$fod = new FuncaoObjetoDashboard();
$arrPermissaoDashboardsFuncoes = array();
$resOpcoesMarcadasFuncoes = $fod->buscaPorFuncao($funcoesUsuarioString);
$arrPermissaoDashboardsFuncoes = array();
$arrPermissaoDashboardsFuncoes['fk_idObjetoDashboard'][] = "";
if ($resOpcoesMarcadasFuncoes) {
    foreach ($resOpcoesMarcadasFuncoes as $vetor) {
        $arrPermissaoDashboardsFuncoes['fk_idFuncao'][] = $vetor['fk_idFuncao'];
        $arrPermissaoDashboardsFuncoes['fk_idObjetoDashboard'][] = $vetor['fk_idObjetoDashboard'];
    }
}

include_once('model/objetoDashboardClass.php');
$objeto_dashboard = new ObjetoDashboard();

//Inicializar vari�veis para mostrar os dashboards
$arrObjetoDashboard = array();

$arrIbox = $objeto_dashboard->listaObjetoDashboardInicio("ibox", $arrPermissaoDashboardsDepartamentos, $arrPermissaoDashboardsFuncoes);
//echo "<pre>";print_r($arrIbox);
$arrOrders = $objeto_dashboard->listaObjetoDashboardInicio("orders", $arrPermissaoDashboardsDepartamentos, $arrPermissaoDashboardsFuncoes);

$arrWidget1 = $objeto_dashboard->listaObjetoDashboardInicio("widget1", $arrPermissaoDashboardsDepartamentos, $arrPermissaoDashboardsFuncoes);

$arrWidget2 = $objeto_dashboard->listaObjetoDashboardInicio("widget2", $arrPermissaoDashboardsDepartamentos, $arrPermissaoDashboardsFuncoes);

$arrWidget3 = $objeto_dashboard->listaObjetoDashboardInicio("widget3", $arrPermissaoDashboardsDepartamentos, $arrPermissaoDashboardsFuncoes);

$arrWidget4 = $objeto_dashboard->listaObjetoDashboardInicio("widget4", $arrPermissaoDashboardsDepartamentos, $arrPermissaoDashboardsFuncoes);
?>

<!-- Conteúdo DE INCLUDE INICIO OBJETOS DASHBOARD-->

<div class="wrapper wrapper-content">

<!-- orders -->
<?php
if (count($arrOrders) > 0) {
	?>
	<div class="row">
	<?php
	for ($i = 0; $i < count($arrOrders['idobjeto_dashboard']); $i++) {

	include("dashboards/" . $arrOrders['arquivo'][$i]);

	}
	?>
	</div>
	<?php
}
?>

<!-- Widget3 -->
<?php
if (count($arrWidget3) > 0) {

	?>
	<div class="row">
	<?php
	for ($i = 0; $i < count($arrWidget3['idobjeto_dashboard']); $i++) {

	$total = (int) $arrWidget3['proporcao'][$i];

	for ($y = 0; $y < $total; $y++) {
		if ($i < count($arrWidget3['idobjeto_dashboard'])) {
			if ($arrWidget3['objeto_dashboard'][$i] != "") {
				include("dashboards/" . $arrWidget3['arquivo'][$i]);
				$i++;
			}
		}
	}
	$i--;

	}
	?>
	</div>
	<?php
}
?>

<!-- Widget1 -->
<?php
if (count($arrWidget1) > 0) {

	?>
	<div class="row">
	<?php
	for ($i = 0; $i < count($arrWidget1['idobjeto_dashboard']); $i++) {

	$total = (int) $arrWidget1['proporcao'][$i];

	for ($y = 0; $y < $total; $y++) {
		if ($i < count($arrWidget1['idobjeto_dashboard'])) {
			if ($arrWidget1['objeto_dashboard'][$i] != "") {
				include("dashboards/" . $arrWidget1['arquivo'][$i]);
				$i++;
			}
		}
	}
	$i--;

	}
	?>
	</div>
	<?php
}
?>

<!-- Widget2 -->
<?php

if (count($arrWidget2)) {
?>
	<div class="row">
	<?php
	for ($i = 0; $i < count($arrWidget2['idobjeto_dashboard']); $i++) {

	$total = (int) $arrWidget2['proporcao'][$i];

	for ($y = 0; $y < $total; $y++) {
		if ($y < count($arrWidget2['idobjeto_dashboard'])) {

			if ($arrWidget2['objeto_dashboard'][$y] != "") {
				include("dashboards/" . $arrWidget2['arquivo'][$y]);
				$i++;
			}
		}
	}
	$i--;

	}
	?>
	</div>
	<?php
}
?>

<!-- Widget4 -->
<?php
if (count($arrWidget4)) {

	?>
	<div class="row">
	<?php
	for ($i = 0; $i < count($arrWidget4['idobjeto_dashboard']); $i++) {

	$total = (int) $arrWidget4['proporcao'][$i];

	for ($y = 0; $y < $total; $y++) {
		if ($i < count($arrWidget4['idobjeto_dashboard'])) {
			if ($arrWidget4['objeto_dashboard'][$i] != "") {
				include("dashboards/" . $arrWidget4['arquivo'][$i]);
				$i++;
			}
		}
	}
	$i--;

	}
	?>
	</div>
	<?php
}
?>

<!-- ibox -->
<?php
if (count($arrIbox)) {

	?>
	<div class="row">
	<?php
	for ($i = 0; $i < count($arrIbox['idobjeto_dashboard']); $i++) {

	$total = (int) $arrIbox['proporcao'][$i];

	for ($y = 0; $y < $total; $y++) {
		if ($i < count($arrIbox['idobjeto_dashboard'])) {
			if ($arrIbox['objeto_dashboard'][$i] != "") {
				include("dashboards/" . $arrIbox['arquivo'][$i]);
				$i++;
			}
		}
	}
	$i--;

	}
	?>
	</div>
	<?php
}
?>

<!-- ibox2 -->
<?php
if (count($arrIbox2) > 0) {

	?>

        <div class="row">
	<?php
	for ($i = 0; $i < count($arrIbox2['idobjeto_dashboard']); $i++) {

	$total = (int) $arrIbox2['proporcao'][$i];

	for ($y = 0; $y < $total; $y++) {
		if ($i < count($arrIbox2['idobjeto_dashboard'])) {
			if ($arrIbox2['objeto_dashboard'][$i] != "") {
				include("dashboards/" . $arrIbox2['arquivo'][$i]);
				$i++;
			}
		}
	}
	$i--;

	}
	?>
	</div>
	<?php
}
?>


</div>
        <?php
        require_once 'controller/logController.php';
        $registro = new LogSistemaController();
        $registro->Registro()
        ?>
<!-- Conteúdo DE INCLUDE FIM OBJETOS DASHBOARD-->