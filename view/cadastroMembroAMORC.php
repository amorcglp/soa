<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
                         @usuarioOnline();?>

<?php 
ini_set("soap.wsdl_cache_enabled", "0"); 

include_once 'lib/functions.php';

$arrDesejaColaborar=array();
$arrDesejaColaborar=explode(" ",$_REQUEST['desejaColaborar']);


?>

<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atualização do Cadastro do Membro na AMORC-GLP</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaMembroAMORC">Cadastro</a>
            </li>
            <li class="active">
                <a>Atualização do Cadastro do Membro na AMORC-GLP</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Atualização do Cadastro do Membro na AMORC-GLP</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal">
                        <!--
                    	<div class="alert alert-warning alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link">Atenção:</a> Instruções sobre a <a class="alert-link">Atualização</a>.<br>
                            Os dados estão sendo atualizados em nosso <a class="alert-link">servidor de teste</a>, isso quer dizer que a atualização<br> 
                            não está acontecendo realmente. <br>
                            Os dados apenas serão realmente atualizados em Janeiro de 2016 quando o sistema entrar no ar de fato.
                        </div>
                        -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Nome</label>
                            <div class="col-sm-10">
                                <input type="text" name="NomMembro" id="NomMembro" class="form-control" value="<?php echo $_REQUEST['nome'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Sexo</label>
                            <div class="col-sm-10">
                                <input type="radio" name="IdeSexo" id="IdeSexo" value="M" <?php if($_REQUEST['sexo']=="M"){ echo "checked";}?>> M 
                                <input type="radio" name="IdeSexo" id="IdeSexo" value="F" <?php if($_REQUEST['sexo']=="F"){ echo "checked";}?>> F 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Conjuge</label>
                            <div class="col-sm-10">
                                <input type="text" name="NomConjuge" id="NomConjuge" class="form-control" value="<?php echo $_REQUEST['conjuge'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Código de Afiliação</label>
                            <div class="col-sm-10">
                                <input type="text" name="CodigoAfiliacao" id="CodigoAfiliacao" class="form-control" value="<?php echo $_REQUEST['codigoAfiliacao'];?>" style="max-width: 320px" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*CEP</label>
                            <div class="col-sm-10">
                                <input type="text" name="CodCep" id="CodCep" class="form-control" value="<?php echo $_REQUEST['cep'];?>" style="max-width: 320px" required="required" onblur="buscaPeloCep('CodCep','DesLograd','NomBairro','NomLocali','SigUf');">
                                <br>
                                <a href="https://www.buscacep.correios.com.br/" target="_blank"><span class='badge badge-primary'>Membro não lembra o CEP</span></a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Logradouro</label>
                            <div class="col-sm-10">
                                <input type="text" name="DesLograd" id="DesLograd" class="form-control" value="<?php echo $_REQUEST['logradouro'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Número</label>
                            <div class="col-sm-10">
                                <input type="text" name="NumEndere" id="NumEndere" class="form-control" value="<?php echo $_REQUEST['numero'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Complemento</label>
                            <div class="col-sm-10">
                                <input type="text" name="DesCompleLograd" id="DesCompleLograd" class="form-control" value="<?php echo $_REQUEST['complemento'];?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Bairro</label>
                            <div class="col-sm-10">
                                <input type="text" name="NomBairro" id="NomBairro" class="form-control" value="<?php echo $_REQUEST['bairro'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Cidade</label>
                            <div class="col-sm-10">
                                <input type="text" name="NomLocali" id="NomLocali" class="form-control" value="<?php echo $_REQUEST['cidade'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*UF</label>
                            <div class="col-sm-10">
                                <input type="text" name="SigUf" id="SigUf" class="form-control" value="<?php echo $_REQUEST['uf'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">RG</label>
                            <div class="col-sm-10">
                                <input type="text" name="CodRg" id="CodRg" class="form-control" value="<?php echo $_REQUEST['rg'];?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Número da Caixa Postal</label>
                            <div class="col-sm-10">
                                <input type="text" name="NumCaixaPostal" id="NumCaixaPostal" class="form-control" value="<?php echo $_REQUEST['numeroCaixaPostal'];?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Código do Cep da Caixa Postal</label>
                            <div class="col-sm-10">
                                <input type="text" name="CodCepCaixaPostal" id="CodCepCaixaPostal" class="form-control" style="max-width: 320px" value="<?php echo $_REQUEST['codCepCaixaPostal'];?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Endereço para Correspondência</label>
                            <div class="col-sm-10">
                                <input type="radio" name="IdeEnderecoCorrespondencia" id="IdeEnderecoCorrespondencia" value="1" <?php if($_REQUEST['enderecoCorrespondencia']==1){ echo "checked";}?>> Logradouro 
                                <input type="radio" name="IdeEnderecoCorrespondencia" id="IdeEnderecoCorrespondencia" value="2" <?php if($_REQUEST['enderecoCorrespondencia']==2){ echo "checked";}?>> Caixa Postal 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Sigla do País</label>
                            <div class="col-sm-10">
                                <input type="text" name="SigPais" id="SigPais" class="form-control" value="<?php echo $_REQUEST['pais'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*E-mail</label>
                            <div class="col-sm-10">
                                <input type="text" name="DesEmail" id="DesEmail" class="form-control" value="<?php echo $_REQUEST['email'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Estado Civil</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idEstadoCivil" id="fk_idEstadoCivil" style="max-width: 300px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1" <?php if($_REQUEST['estadoCivil']==1){ echo "selected";}?>>Casado</option>
                                    <option value="2" <?php if($_REQUEST['estadoCivil']==2){ echo "selected";}?>>Desquitado</option>
                                    <option value="3" <?php if($_REQUEST['estadoCivil']==3){ echo "selected";}?>>Divorciado</option>
                                    <option value="4" <?php if($_REQUEST['estadoCivil']==4){ echo "selected";}?>>Separado</option>
                                    <option value="5" <?php if($_REQUEST['estadoCivil']==5){ echo "selected";}?>>Solteiro</option>
                                    <option value="6" <?php if($_REQUEST['estadoCivil']==6){ echo "selected";}?>>Viúvo</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Grau de Instrução</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idGrauInstrucao" id="fk_idGrauInstrucao" style="max-width: 300px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1" <?php if($_REQUEST['grauInstrucao']==1){ echo "selected";}?>>1º grau incompleto</option>
                                    <option value="2" <?php if($_REQUEST['grauInstrucao']==2){ echo "selected";}?>>1º grau completo</option>
                                    <option value="3" <?php if($_REQUEST['grauInstrucao']==3){ echo "selected";}?>>2º grau incompleto</option>
                                    <option value="4" <?php if($_REQUEST['grauInstrucao']==4){ echo "selected";}?>>2º grau completo</option>
                                    <option value="5" <?php if($_REQUEST['grauInstrucao']==5){ echo "selected";}?>>Superior incompleto</option>
                                    <option value="6" <?php if($_REQUEST['grauInstrucao']==6){ echo "selected";}?>>Superior completo</option>
                                    <option value="7" <?php if($_REQUEST['grauInstrucao']==7){ echo "selected";}?>>Superior com mestrado</option>
                                    <option value="8" <?php if($_REQUEST['grauInstrucao']==8){ echo "selected";}?>>Superior com especialização</option>
                                    <option value="9" <?php if($_REQUEST['grauInstrucao']==9){ echo "selected";}?>>Superior com doutorado</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Especialização</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idEspecializacao" id="fk_idEspecializacao" style="max-width: 300px">
                                    <option value="0">Selecione...</option>
                                    <?php
                                    include_once 'controller/especializacaoController.php';
                                    $ec = new especializacaoController();
                                    $ec->criarComboBox($_REQUEST['seqEspecializacao']);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Formação</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idFormacao" id="fk_idFormacao" style="max-width: 300px">
                                    <option value="0">Selecione...</option>
                                    <?php
                                    include_once 'controller/formacaoController.php';
                                    $fc = new formacaoController();
                                    $fc->criarComboBox($_REQUEST['seqFormacao']);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ocupação</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idOcupacao" id="fk_idOcupacao" style="max-width: 300px">
                                    <option value="0">Selecione...</option>
                                    <?php
                                    include_once 'controller/ocupacaoController.php';
                                    $oc = new ocupacaoController();
                                    $oc->criarComboBox($_REQUEST['seqOcupacao']);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Moeda:</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idTipoMoeda" id="fk_idTipoMoeda" style="max-width: 300px" required="required">
                                    <option value="0">Selecione...</option>
                                    <?php
                                    include_once 'controller/tipoMoedaController.php';
                                    $tmc = new tipoMoedaController();
                                    $tmc->criarComboBox($_REQUEST['seqMoeda']);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Telefone Fixo</label>
                            <div class="col-sm-10">
                                <input type="text" name="NumTelefoFixo" id="NumTelefoFixo" class="form-control" value="<?php echo $_REQUEST['telefoneFixo'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Celular</label>
                            <div class="col-sm-10">
                                <input type="text" name="NumCelula" id="NumCelula" class="form-control" value="<?php echo $_REQUEST['celular'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Outro Telefone</label>
                            <div class="col-sm-10">
                                <input type="text" name="NumOutroTelefo" id="NumOutroTelefo" class="form-control" value="<?php echo $_REQUEST['outroTelefone'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Descrição Outro Telefone</label>
                            <div class="col-sm-10">
                                <input type="text" name="DesOutroTelefo" id="DesOutroTelefo" class="form-control" value="<?php echo $_REQUEST['descricaoOutroTelefone'];?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Aceito Contato</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="AceitoContato[]" id="telefone" <?php if(substr($_REQUEST['aceitoContato'],0,1)=="S"){ echo "checked";}?>> Telefone <br>
                                <input type="checkbox" name="AceitoContato[]" id="carta" <?php if(substr($_REQUEST['aceitoContato'],1,1)=="S"){ echo "checked";}?>> Carta <br>
                                <input type="checkbox" name="AceitoContato[]" id="email" <?php if(substr($_REQUEST['aceitoContato'],2,1)=="S"){ echo "checked";}?>> Email <br>
                                <input type="checkbox" name="AceitoContato[]" id="sms" <?php if(substr($_REQUEST['aceitoContato'],3,1)=="S"){ echo "checked";}?>> SMS 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Período Ideal de Contato</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="IdePeriodContatIdeal[]" id="manha" value="M" <?php if(substr($_REQUEST['periodoContato'],0,1)=="S"){ echo "checked";}?>> Manhã <br>
                                <input type="checkbox" name="IdePeriodContatIdeal[]" id="tarde" value="T" <?php if(substr($_REQUEST['periodoContato'],1,1)=="S"){ echo "checked";}?>> Tarde <br>
                                <input type="checkbox" name="IdePeriodContatIdeal[]" id="noite" value="N" <?php if(substr($_REQUEST['periodoContato'],2,1)=="S"){ echo "checked";}?>> Noite <br>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Forma Ideal de Contato</label>
                            <div class="col-sm-10">
                                <input type="radio" name="IdeFormaContatIdeal" id="IdeFormaContatIdeal" value="EM" <?php if($_REQUEST['formaIdealContato']=="EM"){ echo "checked";}?>> E-mail <br>
                                <input type="radio" name="IdeFormaContatIdeal" id="IdeFormaContatIdeal" value="CT" <?php if($_REQUEST['formaIdealContato']=="CT"){ echo "checked";}?>> Carta <br>
                                <input type="radio" name="IdeFormaContatIdeal" id="IdeFormaContatIdeal" value="TF" <?php if($_REQUEST['formaIdealContato']=="TF"){ echo "checked";}?>> Telefone fixo <br>
                                <input type="radio" name="IdeFormaContatIdeal" id="IdeFormaContatIdeal" value="TC" <?php if($_REQUEST['formaIdealContato']=="TC"){ echo "checked";}?>> Telefone celular <br>
                                <input type="radio" name="IdeFormaContatIdeal" id="IdeFormaContatIdeal" value="TM" <?php if($_REQUEST['formaIdealContato']=="TM"){ echo "checked";}?>> Telefone comercial <br>
                                <input type="radio" name="IdeFormaContatIdeal" id="IdeFormaContatIdeal" value="SM" <?php if($_REQUEST['formaIdealContato']=="SM"){ echo "checked";}?>> SMS
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Possui Computador</label>
                            <div class="col-sm-10">
                                <input type="radio" name="possuiComputador" id="possuiComputador" value="S" <?php if($_REQUEST['possuiComputador']=="S"){ echo "checked";}?>> Sim 
                                <input type="radio" name="possuiComputador" id="possuiComputador" value="N" <?php if($_REQUEST['possuiComputador']=="N"){ echo "checked";}?>> Não 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Possui Internet</label>
                            <div class="col-sm-10">
                                <input type="radio" name="possuiInternet" id="possuiInternet" value="S" <?php if($_REQUEST['possuiInternet']=="S"){ echo "checked";}?>> Sim 
                                <input type="radio" name="possuiInternet" id="possuiInternet" value="N" <?php if($_REQUEST['possuiInternet']=="N"){ echo "checked";}?>> Não 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Deseja Colaborar nas Equipes:</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="desejaColaborar[]" id="administrativa" value="1" <?php if(in_array("1",$arrDesejaColaborar)){ echo "checked";}?>> Administrativa <br>
                                <input type="checkbox" name="desejaColaborar[]" id="ritualistica" value="2" <?php if(in_array("2",$arrDesejaColaborar)){ echo "checked";}?>> Ritualística <br>
                                <input type="checkbox" name="desejaColaborar[]" id="iniciatica" value="3" <?php if(in_array("3",$arrDesejaColaborar)){ echo "checked";}?>> Iniciática <br>
                                <input type="checkbox" name="desejaColaborar[]" id="cultural" value="4" <?php if(in_array("4",$arrDesejaColaborar)){ echo "checked";}?>> Cultural <br>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Experiência Profissional:</label>
                            <div class="col-sm-10">
                                <textarea name="experienciaProfissional" id="experienciaProfissional" rows="5" cols="45"><?php echo $_REQUEST['experienciaProfissional'];?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Membro URCI</label>
                            <div class="col-sm-10">
                                <input type="radio" name="membroURCI" id="membroURCI" value="S" <?php if($_REQUEST['membroURCI']=="S"){ echo "checked";}?>> Sim 
                                <input type="radio" name="membroURCI" id="membroURCI" value="N" <?php if($_REQUEST['membroURCI']=="N"){ echo "checked";}?>> Não 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Receber Revistas Impressas:</label>
                            <div class="col-sm-10">
                                <input type="checkbox" name="IdeAmorcgDigita" id="IdeAmorcgDigita" <?php if($_REQUEST['IdeAmorcgDigita']=="N"){ echo "checked";}?>> AMORC GLP <br>
                                <input type="checkbox" name="IdeForumrDigita" id="IdeForumrDigita" <?php if($_REQUEST['IdeForumrDigita']=="N"){ echo "checked";}?>> Fórum  <br>
                                <input type="checkbox" name="IdeOrosacDigita" id="IdeOrosacDigita" <?php if($_REQUEST['IdeOrosacDigita']=="N"){ echo "checked";}?>> O Rosacruz  <br>
                                <input type="checkbox" name="IdeOpentaDigita" id="IdeOpentaDigita" <?php if($_REQUEST['IdeOpentaDigita']=="N"){ echo "checked";}?>> O Pantáculo <br>
                                <input type="checkbox" name="IdeOGGDigita" id="IdeOGGDigita" <?php if($_REQUEST['IdeOGGDigita']=="N"){ echo "checked";}?>> Ordem Guias do Graal <br>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                                <input type="hidden" name="SeqCadastAtendimento" id="SeqCadastAtendimento" value="<?php echo $_SESSION['seqCadast'];?>">
                                <input type="hidden" name="CodUsuario" id="CodUsuario" value="<?php echo $dadosUsuario->getLoginUsuario();?>">
                        	<input type="hidden" name="TipoMembro" id="TipoMembro" value="<?php echo $_REQUEST['tipoMembro'];?>">
                        	<input type="hidden" name="DatNascim" id="DatNascim" value="<?php echo $_REQUEST['dataNascimento'];?>">
                                <input type="hidden" name="SeqCadast" id="SeqCadast" value="<?php echo $_REQUEST['seqCadast'];?>">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="button" onclick="atualizarCadastroMembroAMORC();" id="atualizar" name="atualizar" value="Atualizar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>