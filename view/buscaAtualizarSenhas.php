<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once ('model/organismoClass.php');
$o = new organismo();

$anexo = isset($_FILES['anexoSenhas']['name']) ? $_FILES['anexoSenhas']['name'] : null;

date_default_timezone_set("Brazil/East"); //Definindo timezone padrão

/**
 * Upload da Ata Assinada
 */
$uploadRealizado=0;
$extensaoNaoValida = 0;
if ($anexo != "") {
    if ($_FILES['anexoSenhas']['name'] != "") {

        if ($o->cadastroPlanilhaSenhasAtualizadas()) {

            $ultimo_id = $o->selecionaUltimoIdCadastradoPlanilhaSenhasAtualizadas();
            $arquivo = $ultimo_id.".xls";

            //echo "arq=>".$arquivo;

            $ext = strtolower(substr($_FILES['anexoSenhas']['name'],-4)); //Pegando extensão do arquivo
            $new_name = $ultimo_id . $ext; //Definindo um novo nome para o arquivo
            $dir = $_SERVER['DOCUMENT_ROOT'] .'/importacao/planilha/'.$new_name; //Diretório para uploads

            //echo "==>Dir:".$dir;
            if ( move_uploaded_file($_FILES['anexoSenhas']['tmp_name'], $dir)) {
                $uploadRealizado = 1;
            } else {
                echo "<script type='text/javascript'>alert('Erro ao enviar a planilha!');</script>";
            }
        }

    }
}



if($uploadRealizado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Planilha enviada com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($extensaoNaoValida==1||$extensaoNaoValida==2){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Extensão não válida!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( document ).tooltip();
  });
  </script>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atualizar Senhas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Senhas</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaAgendaAtividade">Atualizar Senhas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<br>
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload da planilha com os emails e senhas atualizadas</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" action="#" method="post" enctype="multipart/form-data">
                        <input type="file" name="anexoSenhas"><br>
                        <input type="submit" class="btn btn-primary" value="Upload">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
            $(document).on('ready', function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
</script>          

<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->