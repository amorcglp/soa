<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

require_once ("model/criaSessaoClass.php");
include_once ("controller/organismoController.php");

$oc = new organismoController();

?>
<!-- Conteúdo DE INCLUDE INICIO -->

<link rel="stylesheet" href="js/plugins/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Social
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Mural Digital</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

        <div class="col-lg-3">
            <div class="form-group">
                <div class="row">
                    <label class="font-normal">Selecione filtros: </label>
                </div>
                <div class="row" style="margin-top: 8px">
                    <div>
                        <select id="filtroCategoria" name="filtroCategoria" data-placeholder="Categoria" class="chosen-select col-lg-12" multiple tabindex="4">
                            <option value="1">Atividades</option>
                            <option value="2">Eventos</option>
                            <option value="3">Notícias</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-top: 8px">
                    <div>
                        <select id="filtroPublico" name="filtroPublico" data-placeholder="Público" class="chosen-select col-lg-12" multiple tabindex="3">
                            <option value="1">Rosacruz</option>
                            <option value="2">Martinista</option>
                            <option value="3">OGG</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-top: 8px">
                    <div>
                        <select id="filtroOrganismo" name="filtroOrganismo" data-placeholder="Organismo" class="chosen-select col-lg-12" multiple tabindex="4">
                            <?php $oc->criarComboBox(); ?>
                        </select>
                    </div>
                </div>
                <div class="row" style="margin-top: 8px">
                    <div class="btn-group col-lg-12">
                        <button onClick="aplicarFiltroMural();" class="btn btn-primary btn-sm col-lg-6" type="button">Aplicar Filtro</button>
                        <button onClick="limparFiltroMural();" class="btn btn-white btn-sm col-lg-6" type="button">Limpar Filtro</button>
                    </div>
                </div>
                <div class="row" style="margin-top: 8px">
                    <div class="btn-group col-lg-12">
                        <button onClick="alteraFiltroFavorito();" id="filtroFavoritoBtn" name="filtroFavoritoBtn" class="btn btn-white btn-sm col-lg-12" type="button">Favoritos</button>
                        <input type="hidden" id="filtroFavorito" name="filtroFavorito" value="0">
                    </div>
                </div>
                <div class="row" style="margin-top: 8px">
                    <div class="btn-group col-lg-12">
                        <button onClick="alteraFiltroPropriasPublicacoes();" id="filtroPropriasPublicacoesBtn" name="filtroPropriasPublicacoesBtn" class="btn btn-white btn-sm col-lg-12" type="button">Minhas Publicações</button>
                        <input type="hidden" id="filtroPropriasPublicacoes" name="filtroPropriasPublicacoes" value="0">
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 8px">
                <div id="queue"></div>
            </div>
        </div>

        <div class="col-lg-8">

            <!-- CABEÇALHO DO FEED-->
            <div class="ibox float-e-margins" style="margin-bottom: 0px">
                <div class="ibox-content text-center">
                    <h1 class="m-b-xxs">Mural Digital O.A's</h1>
                    <small>Publique notícias, eventos e atividades</small>
                </div>
            </div>
            <!--
            <div class="ibox" style="margin-bottom: 0px">
                <div class="ibox-content" style="height: 30px">
                    <button class="btn btn-primary btn-xs col-lg-offset-10 col-lg-2" type="button">Nova publicação </button>
                </div>
            </div>
            -->
            <?php 
                $funcoesUsuarioString = isset($_SESSION['funcoes']) ? $_SESSION['funcoes'] : null;
                $arrFuncoes = explode(",", $funcoesUsuarioString);

                //echo "Departamento: " . $sessao->getValue("fk_idDepartamento") . "<br>";
                //echo "Funções: <pre>"; print_r($arrFuncoes); echo "</pre>";

                if($sessao->getValue("seqCadast")==431328 //EMANUELLE SPACK - Assessoria de Comunicação -AMORC
                    ||in_array("329",$arrFuncoes) //V. Ext. 151
                    ||in_array("365",$arrFuncoes) //V. Ext. 201
                    ||in_array("367",$arrFuncoes) //V. Ext. 203
                    ||in_array("369",$arrFuncoes) //V. Ext. 205
                    ||in_array("341",$arrFuncoes) //V. Ext. 163
                    ||in_array("1",$arrFuncoes) //V. Ext. 187
                    ||($sessao->getValue("fk_idDepartamento")==2)
                    || ($sessao->getValue("fk_idDepartamento")==3)){
            ?>
            <div class="social-feed-box" style="height: 175px">
                <div class="social-footer col-lg-12" style="height: 350px">
                    <div class="social-comment col-lg-12">
                        <div class="media-body col-lg-12">
                            <div class="row" style="">
                                <!--<textarea id="mensagemMural2" onkeyup="diminuirCaracteresMensagemMural('mensagemMural2','caracteresMensagemMural',1750)" cols="50" maxlength="1750" placeholder="Escreva uma publicação..."></textarea>
                                -->

                                <textarea class="ckeditor" name="editor" id="editor"></textarea>

                                <div id="caracteresMensagemMural" style="margin-top: 5px">Máx. 1750 caracteres permitidos!</div>

                            </div>
                            <div class="row form-inline" style="margin-top: 16px">
                                <div class="form-group col-lg-3 col-lg-offset-2">
                                    <input id="imagemPublicacaoMural" name="imagemPublicacaoMural" type="file">
                                    <input type="hidden" name="fk_idMuralImagem" id="fk_idMuralImagem" value="0">
                                    <input type="hidden" name="imagemCarregada" id="imagemCarregada" value="0">
                                </div>
                                <div>
                                    <select id="categoriaPublicacaoMural" name="categoriaPublicacaoMural" class="form-control"> <!-- select2_demo_3 -->
                                        <option value="0">Categoria</option>
                                        <option value="1" selected>Atividades</option>
                                        <option value="2">Eventos</option>
                                        <option value="3">Notícias</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <select id="publicoPublicacaoMural" name="publicoPublicacaoMural" class="form-control"> <!-- select2_demo_3 -->
                                        <option value="0">Público</option>
                                        <option value="1" selected>Rosacruz</option>
                                        <option value="2">Martinista</option>
                                        <option value="3">OGG</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <select id="exibicaoPublicacaoMural" name="exibicaoPublicacaoMural" class=" form-control"> <!-- select2_demo_3 -->
                                        <option value="0">Exibido para</option>
                                        <option value="1">Todos</option>
                                        <option value="2">Região</option>
                                        <option value="3">O.A.</option>
                                    </select>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <button onClick="uploadImagemPublicacaoMural();" class="btn btn-white btn-sm">
                                        <i class="fa fa-comments"></i> Publicar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <?php
            } 
            ?>

            <div class="mural"></div>

            <div id="linhaTracejadaMural" class="hr-line-dashed"></div>

            <div class="text-center"><div class="btn-group" id="paginadorMural"></div></div>
            
            <input type="hidden" id="seqCadastUsuarioLogado" name="seqCadastUsuarioLogado" value="<?php echo $sessao->getValue("seqCadast");?>">
            <input type="hidden" id="fk_idRegiaoRosacruzUsuarioLogado" name="fk_idRegiaoRosacruzUsuarioLogado" value="<?php echo $fk_idRegiaoPainel;?>">
            <input type="hidden" id="fk_idOrganismoAfiliadoUsuarioLogado" name="fk_idOrganismoAfiliadoUsuarioLogado" value="<?php echo $idOrganismoAfiliado; ?>">
            <input type="hidden" id="avatarUsuarioUsuarioLogado" name="avatarUsuarioUsuarioLogado" value="<?php echo $_COOKIE["av"];?>">

        </div>
    </div>
</div>



<!-- BOX FEED-->
<!--<div class="social-feed-box">-->

    <!-- CONFIG PUBLICAÇÃO-->
    <!--<div class="pull-right social-action dropdown"></div>-->


    <!-- USUÁRIO E DATA DE PUBLICAÇÃO -->
    <!--<div class="social-avatar"></div>-->


    <!-- MENSAGEM -->
    <!--<div class="social-body"></div>-->


    <!-- RODAPÉ DO FEED-->    <!-- COMENTÁRIO-->   <!-- PUBLICAÇÃO DE COMENTÁRIO-->
    <!--<div class="social-footer"></div>-->
<!--</div>-->


<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<div class="modal inmodal" id="modalSubstituirImagem" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <i class="fa fa-file-image-o modal-icon"></i>
                <h4 class="modal-title">Incluir/Substituir Imagem</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" style="text-align: right">
                            Selecione uma imagem
                        </label>
                        <div class="col-sm-8">
                            <div class="col-sm-12" style="max-width: 320px">
                                <div class="row">
                                    <input type="file" 
                                           id="imagemPublicacaoMuralAlterar" 
                                           name="imagemPublicacaoMuralAlterar">
                                </div>
                                <div class="row">
                                    <div id="queueAlterar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="idPublicacaoAlterarImagem" name="idPublicacaoAlterarImagem" value="0">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onClick="uploadImagemPublicacaoMuralalterar();">Salvar Imagem</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->
