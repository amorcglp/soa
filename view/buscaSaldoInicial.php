<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once 'model/saldoInicialClass.php';
$si = new saldoInicial();

include_once('model/ticketItemFuncionalidadeClass.php');
$tif = new TicketItemFuncionalidade();

$idSaldoInicial = "";
$dataSaldoInicial = "";
$saldoInicial = "";
$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if ($resultado) {
    foreach ($resultado as $vetor) {
        $idSaldoInicial = $vetor['idSaldoInicial'];
        $dataSaldoInicial = substr($vetor['dataSaldoInicial'], 8, 2) . "/" . substr($vetor['dataSaldoInicial'], 5, 2) . "/" . substr($vetor['dataSaldoInicial'], 0, 4);
        $saldoInicial = $vetor['saldoInicial'];
    }
}
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php
$salvo = isset($_REQUEST['salvo']) ? $_REQUEST['salvo'] : null;
if ($salvo == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Saldo Inicial salvo com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php } ?>
<?php
$jaCadastrado = isset($_REQUEST['jaCadastrado']) ? $_REQUEST['jaCadastrado'] : null;
if ($jaCadastrado == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "O Saldo Inicial já foi cadastrado anteriormente!",
                type: "error",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php } ?>
<?php
$limpou = isset($_REQUEST['limpou']) ? $_REQUEST['limpou'] : null;
if ($limpou == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Você já pode inserir o saldo inicial novamente!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
$seqCadastSolicitanteToken=0;
$liberarEmMassa=0;
//Se tem cookie que libera token
$liberaTokenUsuarioIdItemFuncionalidade=false;
if (isset($_COOKIE['liberarTokenUsuario'])) {
    //Verificar se token é da ata de reunião mensal
    require_once 'model/usuarioClass.php';
    $u = new Usuario();
    $resultado = $u->retornaTokenIntegridade($_COOKIE['liberarTokenUsuario'],10);
    if($resultado)
    {
        foreach($resultado as $vetor)
        {
            $liberarEmMassa = $vetor['liberarEmMassa'];
            //$liberaTokenUsuarioIdItemFuncionalidade=true;
            $liberaTokenUsuarioIdItemFuncionalidade=$vetor['idItemFuncionalidade'];
            //$timelife=$vetor['timelife'];
            //$timelifeExpira = $timelife + (60 * 60 );
            $date = new DateTime($vetor['dataInicial'].' 23:59:59');
            $timelife = $date->getTimestamp();
            $date2 = new DateTime($vetor['dataFinal'].' 23:59:59');
            $timelifeExpira = $date2->getTimestamp();
            $seqCadastSolicitanteToken=$vetor['seqCadast'];
        }
    }
?>
<?php }
if ($liberaTokenUsuarioIdItemFuncionalidade != false&&$seqCadastSolicitanteToken==$_SESSION['seqCadast']) {
    if($liberarEmMassa == 0)
    {
?>
<script>
    window.onload = function () {
        swal({
            title: "Aviso!",
            text: "O Token solicitado para a Saldo Cod. <?php echo $liberaTokenUsuarioIdItemFuncionalidade;?> expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
</script>
<?php 
    }else{
       ?>
<script>
    window.onload = function () {
        swal({
            title: "Aviso!",
            text: "O Token solicitado para o Saldo expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
</script>
<?php  
    }
}
?>
<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Saldo Inicial</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaSaldoInicial">Financeiro</a>
            </li>
            <li class="active">
                <strong><a>Saldo Inicial</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<?php
include_once 'model/organismoClass.php';
$o = new organismo();
$nomeOrganismo = "";
$resultado2 = $o->listaOrganismo(null, $sessao->getValue("siglaOA"));
if ($resultado2) {
    foreach ($resultado2 as $vetor2) {
        switch ($vetor2['classificacaoOrganismoAfiliado']) {
            case 1:
                $classificacao = "Loja";
                break;
            case 2:
                $classificacao = "Pronaos";
                break;
            case 3:
                $classificacao = "Capítulo";
                break;
            case 4:
                $classificacao = "Heptada";
                break;
            case 5:
                $classificacao = "Atrium";
                break;
        }
        switch ($vetor2['tipoOrganismoAfiliado']) {
            case 1:
                $tipo = "R+C";
                break;
            case 2:
                $tipo = "TOM";
                break;
        }

        $nomeOrganismo = $classificacao . " " . $tipo . " " . $vetor2["nomeOrganismoAfiliado"] . " - " . $vetor2["siglaOrganismoAfiliado"];
        $idOrganismo = $vetor2['idOrganismoAfiliado'];
    }
}
?>
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Saldo Inicial - <?php echo $nomeOrganismo; ?>  &nbsp;<span id="spaceBalance">&nbsp;</span> <center><div id="load"></div></center>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="saldoInicial" class="form-horizontal" method="post">
                        <div class="alert alert-warning alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link">Atenção:</a> Instruções para preenchimento do <a class="alert-link">Saldo inicial</a>.<br>
                            Este campo inicializa o módulo financeiro e <a class="alert-link">não é possível alteração desta informação</a>.<br>
                            Insira no campo SALDO na data de <a class="alert-link">01/01/2016</a> os valores contabilizados como SALDO ATUAL ao final do ano anterior.
                        </div>
                        <div><center><div id="loadSI"></div></center></h5></div>

                        <?php if ($dataSaldoInicial == "") { ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Data:</label>
                                <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="dataSaldoInicial" maxlength="10" id="dataSaldoInicial" type="text" value="01/01/2016" class="form-control" style="max-width: 102px" readonly="readonly">
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Data:</label>
                                <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="dataSaldoInicial" maxlength="10" id="dataSaldoInicial" type="text" value="<?php echo $dataSaldoInicial; ?>" <?php
                                    if ($dataSaldoInicial != "") {
                                        echo "readonly";
                                    }
                                    ?> class="form-control" style="max-width: 102px">
                                </div>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Saldo:</label>
                            <div class="col-sm-9 input-group" style="padding: 0px 0px 0px 15px">
                                <input type="text" class="form-control currency" id="saldoInicial" name="saldoInicial" value="<?php echo $saldoInicial; ?>" <?php
                                if ($dataSaldoInicial != "" && !$liberaTokenUsuarioIdItemFuncionalidade) {
                                    echo "readonly";
                                }
                                ?> style="max-width: 150px">
                            </div>
                        </div>
                        <input type="hidden" name="idSaldoInicial" id="idSaldoInicial" value="<?php echo $idSaldoInicial; ?>">
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast']; ?>">
                        <input type="hidden" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" value="<?php echo $idOrganismoAfiliado; ?>">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <?php if (in_array("1", $arrNivelUsuario)) { ?>	
                                    <?php if ($dataSaldoInicial == ""|| $liberaTokenUsuarioIdItemFuncionalidade) { ?>
                                        <a href="#" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar" onclick="salvarSaldoInicial();">
                                            <i class="fa fa-check fa-white"></i>&nbsp;
                                            Salvar
                                        </a>
                                    <?php }?>
                                <?php } ?>
<!--                                <a href="?corpo=buscaSaldoInicial" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">-->
<!--                                    <i class="fa fa-times fa-white"></i>&nbsp;-->
<!--                                    Cancelar-->
<!--                                </a>-->
                                <?php if((($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2))&&$saldoInicial!=""){?>    
                                <a href="#" onclick="limparSaldoInicial('<?php echo $idOrganismo;?>');" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="left" title="Limpar!">
                                    <i class="fa fa-caret-square-o-right fa-white"></i>&nbsp;
                                    Limpar saldo inicial
                                </a>   
                                <button class="btn btn-sm btn-danger" type="button" onclick="document.getElementById('idFuncionalidade').value='10';document.getElementById('idItemFuncionalidade').value='<?php echo $idSaldoInicial;?>';" data-target="#mySeeToken" data-toggle="modal" data-placement="left">
                                    <i class="fa fa-key"></i>
                                </button>    
                                <?php }?>
                                <?php if(verificaPrazo(date("Y-m-d"), $amanha) && !$liberaTokenUsuarioIdItemFuncionalidade){?>
                                    <?php if($tif->verificaVinculoItemTicket(10,$idSaldoInicial,$_SESSION['seqCadast'],3)){?>
                                    <button class="btn btn-sm btn-success" type="button" data-target="#mySeeIntegridadeTicketAberto" data-toggle="modal" data-placement="left">
                                        <i class="fa fa-hourglass-start"></i>
                                    </button>
                                    <?php }else{
                                            
                                        ?>
                                    <button class="btn btn-sm btn-success" type="button" data-target="#mySeeIntegridade" data-toggle="modal" data-placement="left" onclick="document.getElementById('idFuncionalidadeTicket').value='10';document.getElementById('idItemFuncionalidadeTicket').value='<?php echo $idSaldoInicial;?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $vetor["idOrganismoAfiliado"];?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"];?>';document.getElementById('codigoItem').innerHTML='<?php echo $vetor['idAtaReuniaoMensal'];?>';">
                                        <i class="fa fa-lock"></i>
                                    </button>
                                    <?php 
                                            
                                        }?>  
                                <?php }?>    
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<div class="modal inmodal" id="mySeeIntegridade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-lock modal-icon"></i>
                <h4 class="modal-title">
                    Solicitação de Token
                    <input type="hidden" name="codigoItem" id="codigoItem">
                </h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Lembre-se: você tem até 1 mês para enviar todos os Relatórios Assinados para a GLP.
                        <br />
                        Deseja ter acesso a esse item e regularizar as informações? Descreva o motivo e solicite seu Token.
                        <br />
                        Caso seja aprovada sua solicitação, você receberá uma notificação através do SOA e por e-mail.
                        <br />
                        <br />
                        <b>Motivo da liberação do token:</b>
                        <br />
                        <br />
                        <textarea rows="5" cols="50" name="motivoTicket" id="motivoTicket"></textarea>
                        <br />
                        <?php if($liberaTokenUsuarioIdItemFuncionalidade==false){?>
                        <button type="button" class="btn btn-sm btn-info" data-dismiss="modal" onclick="abrirTicketParaLiberarFuncionalidade();">Abrir Ticket</button>
                        <?php }?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="seqCadastTicket" id="seqCadastTicket" value="<?php $_SESSION['seqCadast'];?>" />
                <input type="hidden" name="idFuncionalidadeTicket" id="idFuncionalidadeTicket" />
                <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" />
                <input type="hidden" name="mesTicket" id="mesTicket" value="1" />
                <input type="hidden" name="anoTicket" id="anoTicket" value="2016" />
                <input type="hidden" name="idItemFuncionalidadeTicket" id="idItemFuncionalidadeTicket" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeToken" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-unlock modal-icon"></i>
                <h4 class="modal-title">Liberar Token para Usuário</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <table>
                            <tr>
                                <td>Organismo:</td>
                                <td><span id="nomeOrganismoToken"><?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);?></span> </td>
                            </tr>
                            <tr>
                                <td></td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Login no SOA:</td>
                                <td><input type="text" name="usuarioToken" id="usuarioToken" /></td>
                            </tr>
                            <tr>
                                <td><br>Data Inicial:</td>
                                <td><br><input type="text" class="data" name="dataInicial" id="dataInicial" value="<?php echo date('d/m/Y');?>"></td>
                            </tr>
                            <tr>
                                <td><br>Data Final:</td>
                                <td>
                                    <br><input type="text" class="data" name="dataFinal" id="dataFinal">
                                </td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Liberar em massa:</td>
                                <td>
                                    <br><input type="checkbox" name="liberarEmMassa" id="liberarEmMassa">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <br>
                                    <input type="hidden" name="mesToken" id="mesToken" value="1" />
                                    <input type="hidden" name="anoToken" id="anoToken" value="2016" />
                                    <input type="hidden" name="idFuncionalidade" id="idFuncionalidade" />
                                    <input type="hidden" name="idItemFuncionalidade" id="idItemFuncionalidade" />
                                    <button class="btn btn-sm btn-danger" type="button" onclick="liberarTokenUsuario();">
                                        <i class="fa fa-key"></i>Liberar
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeIntegridadeTicketAberto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-hourglass-start modal-icon"></i>
                <h4 class="modal-title">Ticket já está aberto para esse item aguarde uma resposta</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Você já abriu um ticket para esse item, aguarde resposta da GLP. Enquanto a GLP não liberar você não tem acesso a ele. Caso a GLP libere o acesso a esse item, você receberá um e-mail com o token para alteração.
                        <br />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>	