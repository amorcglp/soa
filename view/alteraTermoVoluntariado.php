<?php
include_once("controller/termoVoluntariadoController.php");
$tvc = new termoVoluntariadoController();
$dados = $tvc->buscaTermoVoluntariado($_GET['idtermo_voluntariado']);
//echo "<pre>";print_r($dados);
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Termo de Voluntariado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaTermoVoluntariado">Administração</a>
            </li>
            <li class="active">
                <strong><a>Termo de Voluntariado</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de criação do Termo de Voluntariado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaTermoVoluntariado">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoAlterar.php" onSubmit="return validaAtaReuniaoMensal()" >
                        <input type="hidden" name="fk_idTermoVoluntariado" id="fk_idTermoVoluntariado" value="<?php echo $_REQUEST['idtermo_voluntariado']; ?>">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <?php echo retornaNomeCompletoOrganismoAfiliado($dados->getFk_idOrganismoAfiliado());?>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data do Termo de Voluntariado:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataTermoVoluntariado" maxlength="10" value="<?php echo $dados->getDataTermoVoluntariado();?>" id="dataTermoVoluntariado" type="text" class="form-control" style="max-width: 102px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Afiliação: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="codigoAfiliacao" name="codigoAfiliacao" type="text" value="<?php echo $dados->getCodigoAfiliacao();?>" style="max-width: 150px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nome do Voluntário: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" onblur="retornaDadosMembroNoTermoVoluntariado()" id="nomeVoluntario" name="nomeVoluntario" type="text" value="<?php echo $dados->getNomeVoluntario();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CPF: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="cpf" name="cpf" type="text" value="<?php echo $dados->getCpf();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">RG/BI: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="rg" name="rg" type="text" value="<?php echo $dados->getRg();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Profissão: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="profissao" name="profissao" type="text" value="<?php echo $dados->getProfissao();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nacionalidade: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="nacionalidade" name="nacionalidade" type="text" value="<?php echo $dados->getNacionalidade();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="email" name="email" type="text" value="<?php echo $dados->getEmail();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Rua: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="logradouro" name="logradouro" type="text" value="<?php echo $dados->getLogradouro();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="numero" name="numero" type="text" value="<?php echo $dados->getNumero();?>" style="max-width: 143px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Complemento: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="complemento" name="complemento" type="text" value="<?php echo $dados->getComplemento();?>" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Bairro: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="bairro" name="bairro" type="text" value="<?php echo $dados->getBairro();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cep: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="cep" name="cep" type="text" value="<?php echo $dados->getCep();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cidade: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="cidade" name="cidade" type="text" value="<?php echo $dados->getCidade();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">UF: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" id="uf" name="uf" type="text" value="<?php echo $dados->getUf();?>" style="max-width: 70px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telefone Residencial: </label>
                            <div class="col-sm-9">
                                <input class="form-control telefone" maxlength="100" id="telefoneResidencial" name="telefoneResidencial" type="text" value="<?php echo $dados->getTelefoneResidencial();?>" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telefone Comercial: </label>
                            <div class="col-sm-9">
                                <input class="form-control telefone" maxlength="100" id="telefoneComercial" name="telefoneComercial" type="text" value="<?php echo $dados->getTelefoneComercial();?>" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Celular: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="celular" name="celular" type="tel" value="<?php echo $dados->getCelular();?>" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Trabalho voluntário na área/setor de: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="areaTrabalhoVoluntario" name="areaTrabalhoVoluntario" type="text" value="<?php echo $dados->getAreaTrabalhoVoluntario();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tarefa específica: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="tarefaEspecifica" name="tarefaEspecifica" type="text" value="<?php echo $dados->getTarefaEspecifica();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dia(s) da semana do voluntariado: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="diasSemanaVoluntariado" name="diasSemanaVoluntariado" type="text" value="<?php echo $dados->getDiasSemanaVoluntariado();?>" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano de inicio do voluntariado: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="anoInicial" name="anoInicial" type="text" value="<?php echo $dados->getAnoInicial();?>" style="max-width: 143px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano de fim do voluntariado: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="anoFinal" name="anoFinal" type="text" value="<?php echo $dados->getAnoFinal();?>" style="max-width: 143px" required="required">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <?php 
                        	include_once 'model/organismoClass.php';
                            $o = new organismo();
                        	$nomeOrganismo="";
                        	$resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
		                    if($resultado2)
		                    {
			                    	foreach($resultado2 as $vetor2)
			                    	{
			                    		switch($vetor2['classificacaoOrganismoAfiliado']){
											case 1:
												$classificacao =  "Loja";
												break;
											case 2:
												$classificacao =  "Pronaos";
												break;
											case 3:
												$classificacao =  "Capítulo";
												break;
											case 4:
												$classificacao =  "Heptada";
												break;
											case 5:
												$classificacao =  "Atrium";
												break;
										}
										switch($vetor2['tipoOrganismoAfiliado']){
											case 1:
												$tipo = "R+C";
												break;
											case 2:
												$tipo = "TOM";
												break;
										}
											
										$nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];
		                    	
		                    		}
		                    }
                        ?>
                        <input type="hidden" name="nomeOrganismoTermo" id="nomeOrganismoTermo" value="<?php echo $nomeOrganismo;?>">
                        <input type="hidden" name="nomeUsuarioTermo" id="nomeUsuarioTermo" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaTermoVoluntariado" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-times-circle modal-icon"></i>
                <h4 class="modal-title">Edição de ATA de Reunião Mensal do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <label>Motivo da edição:</label>
                <div style="border: #ccc solid 1px">
                    <div class="summernote"></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="atualizaMotivoEdicaoATA();">Editar ATA</button>
            </div>
        </div>
    </div>
</div>

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	