<?php
include_once 'controller/organismoController.php';
include_once 'controller/ticketController.php';
$oc = new organismoController();
$tc = new ticketController();

?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Ticket</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaTicket">Ticket</a>
            </li>
            <li class="active">
                <a>Cadastro de Novo Ticket</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Novo Imóvel</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaTicket">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="acoes/acaoCadastrar.php">

                        <div class="form-group form-inline">
                            <label class="col-sm-2 control-label">Organismo Afiliado </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-5 chosen-select" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." style="max-width: 350px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php $oc->criarComboBox(); ?>
                                	</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título </label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloTicket" id="tituloTicket" class="form-control" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Funcionalidade </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idFuncionalidade" id="fk_idFuncionalidade" style="max-width: 250px">
                                    <option value="0">Selecione...</option>
                                    <?php $tc->criarComboBoxFuncionalidade(); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Identificador </label>
                            <div class="col-sm-10">
                                <input type="text" name="identificadorTicket" id="identificadorTicket" class="form-control" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-2 control-label">Prioridade: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="prioridadeTicket" id="prioridadeTicket" style="max-width: 250px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">Baixa</option>
                                    <option value="2">Média</option>
                                    <option value="3">Alta</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-2 control-label">Tags: </label>
                            <div class="col-sm-10">
                                <select class="chosen-select col-sm-3" multiple name="tagTicket[]" id="tagTicket" style="max-width: 250px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="ata">ata</option>
                                    <option value="imovel">imovel</option>
                                    <option value="reuniao">reuniao</option>
                                    <option value="posse">posse</option>
                                    <option value="organismo">organismo</option>
                                    <option value="anual">anual</option>
                                    <option value="mensal">mensal</option>
                                    <option value="atividade">atividade</option>
                                    <option value="regiao">regiao</option>
                                    <option value="iniciatica">iniciatica</option>
                                    <option value="livro">livro</option>
                                    <option value="planodeacao">planodeacao</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; width: 500px">
                                    <div class="mail-text h-200">
                                        <textarea class="summernote" id="descricaoTicket" name="descricaoTicket"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="text" id="criadoPorTicket" name="criadoPorTicket" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input type="text" id="criadoPorDepartTicket" name="criadoPorDepartTicket" value="<?php echo $sessao->getValue("fk_idDepartamento") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" onclick="startProgress();" />
                                <a class="btn btn-white" href="?corpo=buscaImovel" id="cancelar" name="cancelar">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo de Modal Início -->

<!-- Conteúdo de Modal Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>		