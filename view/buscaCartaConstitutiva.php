<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Carta Constitutiva</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaSaldoInicial">Financeiro</a>
            </li>
            <li class="active">
                <strong><a>Carta Constitutiva</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Carta Constitutiva</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="saldoInicial" class="form-horizontal" method="post">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano:</label>
                            <div class="col-sm-9">
                                <select name="ano" id="ano" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2" onchange="escolheAnoCartaConstitutiva(this.value);">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        $tAno = date('Y')+4;
                                        for($i=2016;$i<=$tAno;$i++)
                                        {
                                            echo "<option value=\"".$i."\">".$i."</option>";
                                        }
                                    ?>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6" id="botaoCalculaCartaConstitutiva">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>	