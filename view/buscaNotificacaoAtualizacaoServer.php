<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
include_once("controller/notificacaoAtualizacaoServerController.php");
$nc = new notificacaoServerController();
                            
$idNotificacaoAtualizacaoServer = isset($_REQUEST['idNotificacaoAtualizacaoServer'])?$_REQUEST['idNotificacaoAtualizacaoServer']:null;
$excluir = isset($_REQUEST['excluir'])?$_REQUEST['excluir']:null;
$excluiu=0;
if($excluir==1)
{
    $excluiu = $nc->removeNotificacaoServer($idNotificacaoAtualizacaoServer);
}    

$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Notificação de Atualização do Servidor salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
$excluiu = isset($_REQUEST['excluiu'])?$_REQUEST['excluiu']:null;
if($excluiu==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Notificação de Atualização do Servidor excluída com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Notificações de Atualização Servidor</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Notificações de Atualização Servidor</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Notificações de Atualização no Servidor</h5>
                    <div class="ibox-tools">
                    	<?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroNotificacoesAtualizacaoServer">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th><center>Data Cadastrada</center></th>
                                <th><center>Data Agendada</center></th>
                        <th><center>Tipo</center></th>
                        <th><center>Status</center></th>
                    <!--
                        <th><center>Enviar por e-mail</center></th>
                    -->
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            $resultado = $nc->listaNotificacaoServer();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['tituloNotificacao']; ?>
                                        </td>
                                        <td>
                                            <center>
                                            <?php
                                            $data = date_create($vetor['dataNotificacao']);
                                            echo date_format($data, 'd/m/Y H:i:s');
                                            ?>
                                            </center>    
                                        </td>
                                        <td>
                                            <center>
                                            <?php
                                            if($vetor['dataAgendada']!="0000-00-00")
                                            {    
                                                $data2 = date_create($vetor['dataAgendada']);
                                                echo date_format($data2, 'd/m/Y');
                                            }else{
                                                echo "--";
                                            }
                                            ?>
                                            </center>
                                        </td>
                                        <td>
                                <center>
                                    <?php
                                    switch ($vetor['tipoNotificacao']) {
                                        case 1:
                                            echo "<span class='badge badge-success'>Melhoria</span>";
                                            break;
                                        case 2:
                                            echo "<span class='badge badge-warning'>Corrigido</span>";
                                            break;
                                        case 3:
                                            echo "<span class='badge badge-info'>Novidade</span>";
                                            break;
                                        case 4:
                                            echo "<span class='badge badge-danger'>Erro</span>";
                                            break;
                                    }
                                    ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <div id="statusTarget<?php echo $vetor['idNotificacao'] ?>">
                                        <?php
                                        switch ($vetor['statusNotificacao']) {
                                            case 0:
                                                echo "<span class='badge badge-primary'>Ativo</span>";
                                                break;
                                            case 1:
                                                echo "<span class='badge badge-danger'>Inativo</span>";
                                                break;
                                        }
                                        ?>
                                    </div>
                                </center>
                                </td>
                                <!--
                                <td>
                                <center>
                                    
                                    <div id="load<?php //echo $vetor['idNotificacao'] ?>">
                                        <a href="#" onclick="notificaNovaAtualizacao(<?php //echo $vetor['idNotificacao'] ?>);"><span class="fa fa-send-o"></span></a>
                                    </div>
                                </center>
                                </td>
                                -->
                                <td>
                                <center>
                                    <div id="acoes_confirma_cancela">
                                    	<?php if(in_array("2",$arrNivelUsuario)){?>
                                        <a class="btn btn-sm btn-info" href="painelDeControle.php?corpo=alteraNotificacaoAtualizacaoServer&id=<?php echo $vetor['idNotificacao'] ?>" data-rel="tooltip" title="">
                                            <i class="icon-edit icon-white"></i>  
                                            Editar                                            
                                        </a>
                                        <?php }?>
                                        <?php if(in_array("3",$arrNivelUsuario)){?>
                                        <span id="status<?php echo $vetor['idNotificacao'] ?>">
                                            <?php if ($vetor['statusNotificacao'] == 1) { ?>
                                                <a class="btn btn-sm btn-success" href="#" onclick="mudaStatus('<?php echo $vetor['idNotificacao'] ?>', '<?php echo $vetor['statusNotificacao'] ?>', 'notificacaoServer')" data-rel="tooltip" title="Ativar"> 
                                                    Ativar                                            
                                                </a>
                                            <?php } else { ?>
                                                <a class="btn btn-sm btn-danger" href="#" onclick="mudaStatus('<?php echo $vetor['idNotificacao'] ?>', '<?php echo $vetor['statusNotificacao'] ?>', 'notificacaoServer')" data-rel="tooltip" title="Inativar">
                                                    Inativar                                            
                                                </a>
                                            <?php } ?>
                                        </span>
                                        <a class="btn btn-sm btn-danger" href="?corpo=buscaNotificacaoAtualizacaoServer&excluir=1&idNotificacaoAtualizacaoServer=<?php echo $vetor['idNotificacao'] ?>" data-rel="tooltip" title="">
                                            Excluir                                            
                                        </a>
										<?php }?>
                                        </span>
                                    </div>
                                </center>
                                </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

