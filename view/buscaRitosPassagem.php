<!-- Conteúdo DE INCLUDE INICIO -->
<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("model/ritoPassagemOGGClass.php");
include_once("model/ritoPassagemOGGMembroClass.php");
include_once("model/ritoPassagemOGGMembroTestemunhaClass.php");
include_once("model/ritoPassagemOGGNaoMembroClass.php");

$excluir = isset($_REQUEST['excluirRitoPassagem'])?$_REQUEST['excluirRitoPassagem']:null;
$idExcluir = isset($_REQUEST['id'])?$_REQUEST['id']:null;

if ($excluir == 1) 
{
    
    //Excluir informação
    $convocacao = new ritoPassagemOGG();
    if($convocacao->remove($idExcluir))
    {  
        $membros = new ritoPassagemOGGMembro();
        $membros->remove($idExcluir);
        
        $testemunhas = new ritoPassagemOGGMembroTestemunha();
        $testemunhas->remove($idExcluir);
        
        $nao_membros = new ritoPassagemOGGNaoMembro();
        $nao_membros->remove($idExcluir);
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Item excluido com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php 
    }
}
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Ritos de Passagem - OGG</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Ritos de Passagem - OGG</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<?php



?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ritos de Passagem - OGG
                    </h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroRitoPassagem">
                            <i class="fa fa-plus"></i> Incluir Novo
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                    <th>Organismo</th>
                                    <th><center>Rito</center></th>
                                    <th><center>Data</center></th>
                                    <th><center>Buscadores Jovens</center></th>
                                    <th><center>Buscadores Adultos</center></th>
                                    <th><center>Testemunhas</center></th>
                                    <th><center>Total</center></th>
                                    <th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                        <?php
                        
                        include_once("controller/ritoPassagemOGGController.php");
                        $cr = new ritoPassagemOGGController();
                        $resultado = $cr->lista($idOrganismoAfiliado);
 
                        if($resultado)
                        {
                            foreach ($resultado as $vetor)
                            {    
                                $data = substr($vetor['data'],8,2)."/".substr($vetor['data'],5,2)."/".substr($vetor['data'],0,4). " ".$vetor['hora'];
                                
                                $membros = new ritoPassagemOGGMembro();
                                $totalMembrosAdulto = $membros->total($vetor['idRitoPassagemOGG'],null,1);
                                $totalMembrosJovem = $membros->total($vetor['idRitoPassagemOGG'],null,2);
                                
                                $testemunhas = new ritoPassagemOGGMembroTestemunha();
                                $totalMembrosTestemunhaAdulto = $testemunhas->total($vetor['idRitoPassagemOGG'],null,1);
                                $totalMembrosTestemunhaJovem = $testemunhas->total($vetor['idRitoPassagemOGG'],null,2);
                                
                                $nao_membros = new ritoPassagemOGGNaoMembro();
                                $totalNaoMembrosAdulto = $nao_membros->total($vetor['idRitoPassagemOGG'],null,1);
                                $totalNaoMembrosJovem = $nao_membros->total($vetor['idRitoPassagemOGG'],null,2);
                                
                                $totalJovem = $totalMembrosJovem+$totalNaoMembrosJovem;
                                $totalAdulto = $totalMembrosAdulto+$totalNaoMembrosAdulto;
                                
                                $totalTestemunha = $totalMembrosTestemunhaJovem+$totalMembrosTestemunhaAdulto;
                                
                                $total = $totalJovem+$totalAdulto+$totalTestemunha;
                                
                        ?>
                            <tr>
                                    <td><?php echo retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);?></td>
                                    <td><center><?php 
                                                        switch ($vetor['rito']){
                                                            case 1:
                                                                echo "O Chamado";
                                                                break;
                                                            case 2:
                                                                echo "Guardião do Castelo";
                                                                break;
                                                            case 3:
                                                                echo "Escudeiro da Verdade";
                                                                break;
                                                            case 4:
                                                                echo "Fiel Servidor";
                                                                break;
                                                            case 5:
                                                                echo "Cavaleiro da Perseverança";
                                                                break;
                                                            case 6:
                                                                echo "Guias do Graal";
                                                                break;
                                                        }
?></center></td>
                                    <td><center><?php echo $data;?></center></td>
                                    <td><center><?php echo $totalJovem;?></center></td>
                                    <td><center><?php echo $totalAdulto;?></center></td>
                                    <td><center><?php echo $totalTestemunha;?></center></td>
                                    <td><center><?php echo $total;?></center></td>
                                    <td>
                                                <center>
                                                    <?php if (in_array("2", $arrNivelUsuario)) { ?>
                                                        <a href="?corpo=alteraRitoPassagem&id=<?php echo $vetor['idRitoPassagemOGG']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar">
                                                            <i class="fa fa-edit fa-white"></i>&nbsp;
                                                            Editar
                                                        </a>
                                                    <?php } ?>
                                                    <?php if (in_array("5", $arrNivelUsuario)) { ?>
                                                    <button type="button" class="btn btn-danger" onclick="location.href='painelDeControle.php?corpo=buscaRitosPassagem&id=<?php echo $vetor['idRitoPassagemOGG'];?>&excluirRitoPassagem=1';">
                                                        <i class="fa fa-times fa-white"></i>&nbsp;
                                                        Excluir</button>
                                                    <?php } ?>
                                                </center>
                                    </td>
                            </tr>
                        <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->


