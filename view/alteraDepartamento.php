<?php
include_once("controller/departamentoController.php");
require_once ("model/criaSessaoClass.php");

$dc = new departamentoController();
$dados = $dc->buscaDepartamento($_GET['id']);
?>


<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Departamento</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaDepartamento">Departamento</a>
            </li>
            <li class="active">
                <a>Cadastro de Novo Departamento</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Novo Departamento</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idDepartamento" id="idDepartamento" value="<?php echo $_GET['id']; ?>" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Departamento</label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeDepartamento" id="nomeDepartamento" class="form-control" value="<?php echo $dados->getNomeDepartamento(); ?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Vínculo Externo</label>
                            <div class="col-sm-10">
                                <input type="text" name="vinculoExterno" id="vinculoExterno" class="form-control" value="<?php echo $dados->getVinculoExterno() ?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10">
                                <textarea rows="7" name="descricaoDepartamento" id="descricaoDepartamento" class="form-control" style="max-width: 500px" ><?php echo $dados->getDescricaoDepartamento() ?></textarea>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>

<!-- Conteúdo DE INCLUDE FIM -->

