<?php
include_once("controller/isencaoOAController.php");

$ic = new isencaoOAController();
$dados = $ic->buscaIsencaoOa($_GET['idIsencaoOa']);
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Isenção no Organismo Afiliado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Financeiro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaMembroOA">Isenção no Organismo Afiliado</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de Alteração da Isenção de Membros no Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaIsencaoOA">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoAlterar.php">
                        <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Afiliação: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="codigoAfiliacao" name="codigoAfiliacao" type="text"  value="<?php echo $dados->getCodigoAfiliacao();?>" style="max-width: 83px"  required="required" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nome do Membro: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="nomeMembroOa" name="nomeMembroOa" type="text" value="<?php echo $dados->getNomeMembroOa();?>" style="max-width: 343px" required="required" onblur="retornaDadosMembroOa()">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <?php echo retornaNomeCompletoOrganismoAfiliado($dados->getFk_idOrganismoAfiliado());?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Interno: </label>
                            <div class="col-sm-9">
                                <div id="seqCadastMembroOaDiv" style="margin-top: 7px"><?php echo $dados->getSeqCadastMembroOa();?></div>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data Inicial:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataInicial" maxlength="10" id="dataInicial" type="text" class="form-control" value="<?php echo $dados->getDataInicial();?>" style="max-width: 102px"  required="required">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data Final:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataFinal" maxlength="10" id="dataFinal" type="text" class="form-control" value="<?php echo $dados->getDataFinal();?>" style="max-width: 102px"  required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Motivo</label>
                            <div class="col-sm-9">
                                <textarea rows="7" name="motivo" id="motivo" class="form-control" style="max-width: 500px"><?php echo $dados->getMotivo();?></textarea>
                            </div>
                        </div>
                        <input type="hidden" id="principalCompanheiro" name="principalCompanheiro" value="">
                        <input type="hidden" id="idIsencaoOa" name="idIsencaoOa" value="<?php echo $dados->getIdIsencaoOa();?>">
                        <input type="hidden" id="seqCadastMembroOa" name="seqCadastMembroOa" value="<?php echo $dados->getSeqCadastMembroOa();?>">
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Salvar"
									OnMouseOver="retornaDadosMembroOa();">
									<i class="fa fa-check fa-white"></i>&nbsp; Salvar
								</button>
								&nbsp; <a href="?corpo=buscaIsencaoOA"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
	
					</form>
                </div>
            </div>
        </div>
	</div>
</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	