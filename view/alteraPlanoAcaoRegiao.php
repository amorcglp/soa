<?php
include_once("controller/planoAcaoRegiaoController.php");
include_once("lib/webservice/retornaInformacoesMembro.php");
$parc = new planoAcaoRegiaoController();
$dados = $parc->buscaPlanoAcaoRegiao($_GET['idPlanoAcaoRegiao']);
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php 
$excluido = isset($_REQUEST['excluido'])?$_REQUEST['excluido']:null;
if($excluido==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Participante excluído com sucesso da lista!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Planos de Ação da Região</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Planos de Ação da Região</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de alteração do Plano de Ação da Região</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaPlanoAcaoRegiao">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoAlterar.php" onSubmit="return validaLivroRegiao()" >
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Regiao:</label>
                            <div class="col-sm-9">
                                <?php echo $dados->getRegiao();?>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Título: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="tituloPlano" name="tituloPlano" type="text" value="<?php echo $dados->getTituloPlano();?>" style="max-width: 450px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Prioridade: </label>
                            <div class="col-sm-3">
                                <select class="form-control col-sm-3" name="prioridade" id="prioridade">
                                	<option>Selecione</option>
                                	<option value="1" <?php if($dados->getPrioridade()==1){ echo "selected";}?>>Alta</option>
                                	<option value="2" <?php if($dados->getPrioridade()==2){ echo "selected";}?>>Média</option>
                                	<option value="3" <?php if($dados->getPrioridade()==3){ echo "selected";}?>>Baixa</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Participantes:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembroOficial" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroOficial" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="retornaDadosMembroOficialECompanheiro();">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroOficial','nomeMembroOficial','codAfiliacaoMembroOficial','nomeMembroOficial','h_seqCadastMembroOficial','h_nomeMembroOficial','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Digite o código de afiliação do membro e confira o nome antes de clicar em Incluir<br>
                                <input type="hidden" id="h_seqCadastMembroOficial">
                                <input type="hidden" id="h_nomeMembroOficial">
                                <input type="hidden" id="h_seqCadastCompanheiroMembroOficial">
                                <input type="hidden" id="h_nomeCompanheiroMembroOficial">
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiOficiaisAtaReuniaoMensal();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="ata_oa_mensal_oficiais[]" id="ata_oa_mensal_oficiais" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                        <?php
                                            include_once 'model/planoAcaoRegiaoParticipanteClass.php';
                                            $parp = new planoAcaoRegiaoParticipante();
                                            $resultado = $parp->listaParticipantes($dados->getIdPlanoAcaoRegiao());

                                            foreach ($resultado as $vetor) {
                                                ?>
                                                <option value="<?php echo $vetor['seqCadast']; ?>">[<?php echo retornaCodigoAfiliacao($vetor['seqCadast']) ?>] <?php echo retornaNomeCompleto($vetor['seqCadast']) ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="excluirParticipantePlanoAcaoRegiao('<?php echo $_REQUEST['idPlanoAcaoRegiao'];?>');"> Excluir Participante da Lista</a><br>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Descreva em poucas palavras o Plano de Ação aqui<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="descricaoPlano" name="descricaoPlano"><?php echo $dados->getDescricaoPlano();?></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Palavras-chave (separe-as com vírgula): </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="palavraChave" name="palavraChave" type="text" value="<?php echo $dados->getPalavraChave();?>" style="max-width: 450px">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                        	<input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        	<input type="hidden" name="fk_idPlanoAcaoRegiao" id="fk_idPlanoAcaoRegiao" value="<?php echo $_REQUEST['idPlanoAcaoRegiao'];?>">
                                <input type="hidden" name="regiao" id="regiao" value="<?php echo $dados->getRegiao();?>">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar a Ata" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaPlanoAcaoRegiao" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	