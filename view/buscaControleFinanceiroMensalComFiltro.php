<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/usuarioController.php");
$um     = new Usuario();
require_once ("controller/organismoController.php");
$oc     = new organismoController();

 include_once("model/organismoClass.php");
 include_once("model/relatorioFinanceiroMensalClass.php");
 include_once("lib/functions.php");
 
 $anoAtual = isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');
 
 /*
 * Montar anterior e próximo
 */

$anoAnterior=$anoAtual-1;
$anoProximo=$anoAtual+1;
 	
 $o = new organismo();

 $resultado = $o->buscaIdOrganismo($_REQUEST['idOrganismoAfiliado']);


 if ($resultado) {
 	foreach ($resultado as $vetor) {
 		$idOrganismoAfilialdo = $vetor['idOrganismoAfiliado'];
 		switch ($vetor['classificacaoOrganismoAfiliado']) {
 			case 1:
 				$classificacao = "Loja";
 				break;
 			case 2:
 				$classificacao = "Pronaos";
 				break;
 			case 3:
 				$classificacao = "Capítulo";
 				break;
 			case 4:
 				$classificacao = "Heptada";
 				break;
 			case 5:
 				$classificacao = "Atrium";
 				break;
 		}
 		switch ($vetor['tipoOrganismoAfiliado']) {
 			case 1:
 				$tipo = "R+C";
 				break;
 			case 2:
 				$tipo = "TOM";
 				break;
 		}
 	}
 }
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Controle Financeiro Mensal</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Financeiro</a>
            </li>
            <li>
                <a href="painelDeControle.php">Controle Financeiro</a>
            </li>
            <li class="active">
                <strong><a>Mensal</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                	<table width="100%">
                		<tr>
							<td>
                    			<h5>Controle Financeiro Mensal - <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?></h5>
                    		</td>
                    		<td align="right">
                    			
		            				<a class="btn btn-xs btn-info" href="?corpo=buscaControleFinanceiroMensalComFiltro&anoAtual=<?php echo $anoAnterior;?>&idOrganismoAfiliado=<?php echo $_REQUEST['idOrganismoAfiliado']?>"> <i class="fa fa-chevron-left"></i> <?php echo $anoAnterior;?></a>
		            				<a class="btn btn-xs btn-info"	href="#"><b><?php echo $anoAtual;?> </b> </a> 
		            				<a class="btn btn-xs btn-info"	href="?corpo=buscaControleFinanceiroMensalComFiltro&anoAtual=<?php echo $anoProximo;?>&idOrganismoAfiliado=<?php echo $_REQUEST['idOrganismoAfiliado']?>"><?php echo $anoProximo;?>  <i class="fa fa-chevron-right"></i> </a>
									<a class="btn btn-xs btn-warning" href="?corpo=buscaControleFinanceiroMensal"> Voltar </a>
		            		</td>
		            	</tr>
		            </table>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
		                        <th><center>Data de Entrega</center></th>
		                        <th><center>Entregue por</center></th>
		                        <th><center>Relatório Assinado</center></th>
		                        <th><center>Notificar</center></th>
		                    </tr>
                        </thead>
                        <tbody>
                            <?php
                           		for($i=1;$i<13;$i++)
                           		{
                           				$rfm = new RelatorioFinanceiroMensal();
								 		$resultado2 = $rfm->listaRelatorioFinanceiroMensal($i,$anoAtual,$_REQUEST['idOrganismoAfiliado']);
								 		$total=0;
								 		if($resultado2)
								 		{
								 			foreach($resultado2 as $vetor2)
								 			{
								 				$dataCadastro = $vetor2['dataCadastro'];
								 				$nomeUsuario = $vetor2['nomeUsuario'];
								 			}
								 			$total = count($resultado2);
								 		}
                                    ?>
                                    
                                    <tr>
                                        <td>
                                            <?php echo str_pad($i, 2, "0", STR_PAD_LEFT)."/".$anoAtual; ?>
                                        </td>
                                       
                                        <td>
		                                <center>
		                                    <?php if($total>0){ echo substr($dataCadastro, 8, 2) . "/" . substr($dataCadastro, 5, 2) . "/" . substr($dataCadastro, 0, 4)." às ".substr($dataCadastro, 11, 8); }else{ echo "Não entregou este mês"; }?> 
		                                </center>
		                                </td>
		                                <td>
		                                	<center>
		                                            <?php if($total>0){ echo $nomeUsuario;}else{ echo "--";} ?>
		                                    </center>
		                                </td>
		                                <td>
		                                    <center>
		                                    	<?php  
		                                    		if($total>0){?>
		                                    			<a href="#" 
		                                    			onClick="listaUploadRelatorioFinanceiroMensal('<?php echo $i; ?>','<?php echo $anoAtual; ?>','<?php echo $vetor['idOrganismoAfiliado'];?>')"
		                                    			data-target="#mySeeListaUpload" data-toggle="modal"
		                                    			target="_blank">
		                                    				<button class="btn btn-info  dim" type="button"><i class="fa fa-paste"></i> </button>
		                                    			</a>
		                                    			<?php }else{ ?>
		                                    			<button class="btn btn-warning " type="button"><i class="fa fa-warning"></i> <span class="bold">Não enviado</span></button>
		                                    	<?php }?>
		                                    </center>
		                                </td>
		                                <td>
		                                	<center>
			                                	<?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
			                                            <a class="btn btn-sm btn-danger" href="" data-toggle="modal" data-target="#mySeeNotificacao<?php echo $idOrganismoAfilialdo;?>">
			                                                <i class="fa fa-envelope icon-white"></i>
			                                                Notificar
			                                            </a>
			                                    <?php } ?>
                                    		</center>
		                                </td>
                               		 </tr>
                            <?php
                                
                            }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes da Ata de Reunião</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Ata:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="dataAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora inicial:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaInicialAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora do encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaFinalAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número de membros:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="numeroMembrosAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Membro que presidiu:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="nomePresidenteAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Oficiais Administrativos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="oficiaisAdministrativos"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura do relatório mensal com as seguintes restrições:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_um"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura da ata anterior:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_dois"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Relatório das Comissões:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_tres"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos Pendentes:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_quatro"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos novos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_cinco"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Expansão da Ordem:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_seis"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Palavra livre:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_sete"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações da Suprema Grande Loja, da GLP e Grande Conselheiro(a):</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_oito"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações Gerais:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_nove"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_dez"></span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Upload da Ata Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> é preciso enviar a ata original para GLP por correio, mesmo fazendo o upload da ata assinada. Caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="idAtaUpload" name="idAtaUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Upload" onclick="document.ata.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Relatório Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Notificar Responsáveis -->
<div class="modal inmodal"
	id="mySeeNotificacao<?php echo $idOrganismoAfiliado;?>"
	tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span>
				</button>
				<i class="fa fa-envelope modal-icon"></i>
				<h4 class="modal-title">Notificar Responsáveis</h4>
			</div>
			<div class="modal-body">

			<?php
                       
                        $dadosOrganismo = $oc->buscaOrganismo($idOrganismoAfiliado);
                        $nomeOa = organismoAfiliadoNomeCompleto($dadosOrganismo->getClassificacaoOrganismoAfiliado(),$dadosOrganismo->getTipoOrganismoAfiliado(),$dadosOrganismo->getNomeOrganismoAfiliado(),$dadosOrganismo->getSiglaOrganismoAfiliado());
                        echo '<h4><center>'.$nomeOa.'</center></h4>'.'<br>';

                        $sigla = $oc->buscaOrganismo($idOrganismoAfiliado);

                        $listarAtuantes 	= isset($_REQUEST['listarAtuantes'])?$_REQUEST['listarAtuantes']:null;
                        $listarNaoAtuantes 	= isset($_REQUEST['listarNaoAtuantes'])?$_REQUEST['listarNaoAtuantes']:null;
                        $ocultar_json=1;
                        $siglaOA=strtoupper($sigla->getSiglaOrganismoAfiliado());
                        $atuantes='S';
                        $naoAtuantes='N';

                        include './js/ajax/retornaFuncaoMembro.php';
                        $oficial = json_decode(json_encode($return),true);
                        $b=0;
                        $semRegistro=array();
                        
                        foreach ($oficial['result'][0]['fields']['fArrayOficiais'] as $vetorOficial) {
                            if((($vetorOficial['fields']['fDesTipoFuncao']=='ADMINISTRATIVOS') || ($vetorOficial['fields']['fDesTipoFuncao']=='DIGNITÁRIOS')) && (($vetorOficial['fields']['fCodFuncao']==201) || ($vetorOficial['fields']['fCodFuncao']==203) || ($vetorOficial['fields']['fCodFuncao']==213) || ($vetorOficial['fields']['fCodFuncao']==207) || ($vetorOficial['fields']['fCodFuncao']==209) || ($vetorOficial['fields']['fCodFuncao']==211) || ($vetorOficial['fields']['fCodFuncao']==151))){
                                //echo "<pre>";print_r($vetorOficial);echo "</pre>";
                                if($vetorOficial['fields']['fCodMembro']!=0){
                                    $temCadastro = $um->temUsuario($vetorOficial['fields']['fSeqCadast']);
                                    if($temCadastro){
                                        ?>
                                        <div class="form-group form-inline">
                                            <label class="col-sm-6 col-sm-offset-1 control-label" style="padding: 7px 0px 7px 0px">
                                                <div class="checkbox i-checks">
                                                    <input type="checkbox" id="oficial<?php echo $idOrganismoAfiliado;?><?php echo $b; ?>" name="oficial<?php echo $idOrganismoAfiliado;?><?php echo $b; ?>[]" value="<?php echo $vetorOficial['fields']['fSeqCadast']; ?>" style="margin-top: 10px">
                                                </div>
                                                &nbsp;
                                                <span><?php echo ucfirst(mb_strtolower($vetorOficial['fields']['fDesFuncao'],'UTF-8')).": "; ?></span>
                                            </label>
                                            <div class="col-sm-5">
                                                <p class="form-control-static" style="margin-left: 15px; padding-top: 7px">
                                                    <?php
                                                    $arrNome				= explode(" ",$vetorOficial['fields']['fNomClient']);
                                                    $arrNomeDecrescente		= $arrNome[count($arrNome)-1];
                                                    ?>
                                                    <?php echo ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNomeDecrescente,'UTF-8'));?>
                                                </p>
                                            </div>
                                        </div>
                                        <br><br>
                                        <?php
                                        $b++;
                                    } else {
                                        $arrNome				= explode(" ",$vetorOficial['fields']['fNomClient']);
                                        $arrNomeDecrescente		= $arrNome[count($arrNome)-1];
                                        $semRegistro['alerts'][] = ucfirst(mb_strtolower($vetorOficial['fields']['fDesFuncao'],'UTF-8').': '.ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNomeDecrescente,'UTF-8')));
                                    }
                                }
                            }
                        }
                        ?>
                        <input type="hidden" id="qntOficiais<?php echo $idOrganismoAfiliado;?>" name="qntOficiais<?php echo $idOrganismoAfiliado;?>" value="<?php echo $b; ?>">
                        <?php if($b==0){ ?>
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    <center><a class="alert-link" href="">Atenção! </a>Não há nenhum oficial cadastrado nesse Organismo!</center>
                                </div>
                            </div>
                            <?php
                        } else { ?>
                            <div class="form-group">
                                <div class="alert alert-warning">
                                    <center>Alguns oficiais deste Organismo ainda não realizaram registro no sistema:</center>
                                    <br>
                                    <ul>
                                        <?php
                                        for($i=0;$i<count($semRegistro['alerts']);$i++){
                                            echo '<li>'.$semRegistro['alerts'][$i].'</li>';
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <?php if($b>0){ ?>
                            <br>
                            <div class="form-group">
                                <label>Mensagem do Tipo: </label>
                                <div>
                                    <select class="form-control col-sm-3" name="tipoNotificacaoGlp<?php echo $idOrganismoAfiliado;?>" id="tipoNotificacaoGlp<?php echo $idOrganismoAfiliado;?>" required="required">
                                        <option value="0">Selecione...</option>
                                        <option value="2">Falta Documento</option>
                                        <option value="3">Erro de Documentação</option>
                                        <option value="4">Deve Atualizar</option>
                                        <option value="5">Outro</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label>Título:</label>
                                <div>
                                    <input class="form-control" id="tituloNotificacaoGlp<?php echo $idOrganismoAfiliado;?>" name="tituloNotificacaoGlp<?php echo $idOrganismoAfiliado;?>" type="text" value="Relatório Financeiro Mensal" style="min-width: 320px" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Mensagem:</label>
                                <div style="border: #ccc solid 1px">
                                    <div class="summernote" id="mensagemNotificacaoGlp<?php echo $idOrganismoAfiliado;?>" name="mensagemNotificacaoGlp<?php echo $idOrganismoAfiliado;?>"></div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="fk_seqCadastRemetente<?php echo $idOrganismoAfiliado;?>" value="<?php echo $sessao->getValue("seqCadast"); ?>" />
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                        <?php if ($b!=0) { ?>
                            <button type="button" class="btn btn-primary" onclick="enviaNotificacao(<?php echo $idOrganismoAfiliado;?>);">Enviar Notificação</button>
                        <?php } ?>
                    </div>
		</div>
	</div>
</div>

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
