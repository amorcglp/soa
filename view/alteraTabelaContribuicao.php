<?php
include_once("controller/tabelaContribuicaoController.php");

$t = new tabelaContribuicaoController();
$dados = $t->buscaTabelaContribuicao($_GET['id']);
//echo "<pre>";print_r($dados);
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Tabela de Contribuição</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaTabelaContribuicao">Tabela de Contribuição</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Registro na Tabela de Contribuição</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idTabelaContribuicao" id="idTabelaContribuicao" value="<?php echo $_GET['id'];?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo de Monografia</label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="tipo" id="tipo" style="max-width: 300px" required="required">
                                    <option value="">Selecione...</option>
                                    <option value="1" <?php if($dados->getTipo()==1){ echo "selected";}?>>R+C Impressa</option>
                                    <option value="2" <?php if($dados->getTipo()==2){ echo "selected";}?>>R+C Virtual</option>
                                    <option value="3" <?php if($dados->getTipo()==3){ echo "selected";}?>>R+C Virtual+Impressa</option>
                                    <option value="4" <?php if($dados->getTipo()==4){ echo "selected";}?>>TOM</option>
                                    <option value="5" <?php if($dados->getTipo()==5){ echo "selected";}?>>OGG Impressa</option>
                                    <option value="6" <?php if($dados->getTipo()==6){ echo "selected";}?>>OGG Virtual</option>
                                    <option value="7" <?php if($dados->getTipo()==7){ echo "selected";}?>>OGG Virtual+Impressa</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Mensal Individual</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeMensalIndividual" id="modalidadeMensalIndividual" class="form-control" value="<?php echo $dados->getModalidadeMensalIndividual();?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Trim. Individual Real (R$)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeTrimestralIndividualReal" id="modalidadeTrimestralIndividualReal" class="form-control" value="<?php echo $dados->getModalidadeTrimestralIndividualReal();?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Trim. Individual Dolar ($)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeTrimestralIndividualDolar" id="modalidadeTrimestralIndividualDolar" class="form-control" value="<?php echo $dados->getModalidadeTrimestralIndividualDolar();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalide Trim. Individual Euro (&euro;)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeTrimestralIndividualEuro" id="modalidadeTrimestralIndividualEuro" class="form-control" value="<?php echo $dados->getModalidadeTrimestralIndividualEuro();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Anual Individual Real (R$)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeAnualIndividualReal" id="modalidadeAnualIndividualReal" class="form-control" value="<?php echo $dados->getModalidadeAnualIndividualReal();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Anual Individual CFD</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeAnualIndividualCFD" id="modalidadeAnualIndividualCFD" class="form-control" value="<?php echo $dados->getModalidadeAnualIndividualCFD();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Mensal Dual</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeMensalDual" id="modalidadeMensalDual" class="form-control" value="<?php echo $dados->getModalidadeMensalDual();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Trimestral Dual Real (R$)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeTrimestralDualReal" id="modalidadeTrimestralDualReal" class="form-control" value="<?php echo $dados->getModalidadeTrimestralDualReal();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Trimestral Dual Dolar ($)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeTrimestralDualDolar" id="modalidadeTrimestralDualDolar" class="form-control" value="<?php echo $dados->getModalidadeTrimestralDualDolar();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalide Trimestral Dual Euro (&euro;)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeTrimestralDualEuro" id="modalidadeTrimestralDualEuro" class="form-control" value="<?php echo $dados->getModalidadeTrimestralDualEuro();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Anual Dual Real (R$)</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeAnualDualReal" id="modalidadeAnualDualReal" class="form-control" value="<?php echo $dados->getModalidadeAnualDualReal();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Modalidade Anual Dual CFD</label>
                            <div class="col-sm-9">
                                <input type="text" name="modalidadeAnualDualCFD" id="modalidadeAnualDualCFD" class="form-control" value="<?php echo $dados->getModalidadeAnualDualCFD();?>" style="max-width: 320px" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Taxa de Inscrição Real (R$)</label>
                            <div class="col-sm-9">
                                <input type="text" name="taxaInscricaoReal" id="taxaInscricaoReal" class="form-control" value="<?php echo $dados->getTaxaInscricaoReal();?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Taxa de Inscrição Dolar ($)</label>
                            <div class="col-sm-9">
                                <input type="text" name="taxaInscricaoDolar" id="taxaInscricaoDolar" class="form-control" value="<?php echo $dados->getTaxaInscricaoDolar();?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">*Taxa de Inscrição Euro (&euro;)</label>
                            <div class="col-sm-9">
                                <input type="text" name="taxaInscricaoEuro" id="taxaInscricaoEuro" class="form-control" value="<?php echo $dados->getTaxaInscricaoEuro();?>" style="max-width: 320px">
                            </div>
                        </div>
                        <hr>
                        <input type="hidden" id="usuario" name="usuarioAlteracao" value="<?php echo $sessao->getValue("seqCadast") ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">&nbsp;</label>
                            <div class="col-sm-9">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>