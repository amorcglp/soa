<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Liberação de Tokens</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Liberação de Tokens</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tokens liberados</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th><center>Protocolo de Liberação</center></th>
                                <th><center>Link do Token</center></th>
                                <th><center>Data Inicial</center></th>
                                <th><center>Data Final</center></th>
                                <th><center>Funcionalidade</center></th>
                                <th><center>Documento</center></th>
                                <th><center>Mês/Ano</center></th>
                                <th><center>Usuário</center></th>
                                <th><center>Organismo</center></th>
                                <th><center>Liberação em massa</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("model/usuarioClass.php");

                            $u = new Usuario();
                            $resultado = $u->retornaTokenIntegridade();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['idIntegridadeTokenUsuario']; ?>
                                        </td>
                                        <td>
                                            <center>
                                                <div id="link<?php echo $vetor['token']; ?>">https://soa.amorc.org.br/login.php?liberarTokenUsuario=<?php echo $vetor['token']; ?></div>
                                                <a href="#" onclick="copy('link<?php echo $vetor['token']; ?>');" class="badge badge-primary">COPIAR</a>
                                            </center>
                                        </td>
                                        <td>
                                        <center>    
                                            <?php echo substr($vetor['dataInicial'],8,2)."/".substr($vetor['dataInicial'],5,2)."/".substr($vetor['dataInicial'],0,4); ?>
                                        </center>    
                                        </td>
                                        <td>
                                        <center>    
                                            <?php echo substr($vetor['dataFinal'],8,2)."/".substr($vetor['dataFinal'],5,2)."/".substr($vetor['dataFinal'],0,4); ?>
                                        </center>    
                                        </td>
                                        <td>
                                        <center>
                                            <?php
                                                switch ($vetor['idFuncionalidade'])
                                                {
                                                    case 2:
                                                        echo "Ata de Reunião Mensal";
                                                        break;
                                                    case 3:
                                                        echo "Ata de Posse";
                                                        break;
                                                    case 4:
                                                        echo "Recebimentos";
                                                        break;
                                                    case 5:
                                                        echo "Despesas";
                                                        break;
                                                    case 6:
                                                        echo "Saldo";
                                                        break;
                                                    case 7:
                                                        echo "Membros Ativos";
                                                        break;
                                                    case 8:
                                                        echo "Dívidas";
                                                        break;
                                                    case 9:
                                                        echo "Quitação de Dívidas";
                                                        break;
                                                    case 10:
                                                        echo "Saldo Inicial";
                                                        break;
                                                    default:
                                                        echo "--";
                                                        break;
                                                }
                                            ?>
                                        </center>
                                        </td>
                                        <td>
                                        <center>
                                            <?php
                                                if($vetor['idItemFuncionalidade']!=0)
                                                {    
                                                    echo $vetor['idItemFuncionalidade'];
                                                }else{
                                                    echo "--";
                                                }
                                            ?>
                                        </center>
                                        </td>
                                        <td>
                                        <center>
                                            <?php
                                                if($vetor['mes']!=0&&$vetor['ano']!=0)
                                                {    
                                                    echo $vetor['mes']."/".$vetor['ano'];
                                                }else{
                                                    echo "--";
                                                }
                                            ?>
                                        </center>
                                        </td>
                                        <td>
                                        <center>
                                            <?php
                                                echo $vetor['usuarioToken'];
                                            ?>
                                        </center>
                                        </td>
                                        <td>
                                        <center>
                                            <?php
                                                if($vetor['fk_idOrganismoAfiliado']!=0)
                                                {    
                                                    echo retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
                                                }else{
                                                    echo "--";
                                                }
                                            ?>
                                        </center>    
                                        </td>
                                        <td>
                                        <center>
                                            <?php
                                                if($vetor['liberarEmMassa']==1){ echo "Sim";}else{ echo "Não";}
                                            ?>
                                        </center>
                                        </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

