
<!-- Conteúdo DE INCLUDE INICIO -->
<script type="text/javascript">

$(function () {
    //Máscaras
    $("#telefoneFixoOrganismoAfiliado").mask("(999) 9999-9999");
    //$("#outroTelefoneOrganismoAfiliado").mask("(999) 9999-9999");
    //$("#celularUsuario").mask("(999) 9999-9999");
    $("#faxOrganismoAfiliado").mask("(999) 9999-9999");
    
    jQuery('input[type=tel]').mask("(999) 9999-9999?9").ready(function(event) {
    var target, phone, element;
    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
    phone = target.value.replace(/\D/g, '');
    element = $(target);
    element.unmask();
    if(phone.length > 10) {
    element.mask("(999) 99999-999?9");
    } else {
    element.mask("(999) 9999-9999?9");
    }
    });
})

</script>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaOrganismo">Organismos</a>
            </li>
            <li class="active">
                <a>Cadastro de Novo Organismo</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Novo Organismo</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaOrganismo">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="post" action="acoes/acaoCadastrar.php">
                    	<div class="form-group"><label class="col-sm-2 control-label">País</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" onchange="selecionaPais(this.value)" name="paisOrganismoAfiliado" id="paisOrganismoAfiliado" style="max-width: 150px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">Brasil</option>
                                    <option value="2">Portugal</option>
                                    <option value="3">Angola</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Sigla</label>
                            <div class="col-sm-10"><input type="text" name="siglaOrganismoAfiliado" maxlength="7" onChange="this.value = this.value.toUpperCase()" onBlur="retornaDadosOrganismo();" id="siglaOrganismoAfiliado" class="form-control" style="max-width: 160px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Era sigla</label>
                            <div class="col-sm-10"><input type="text" name="eraSiglaOrganismoAfiliado" maxlength="7" onChange="this.value = this.value.toUpperCase()" id="eraSiglaOrganismoAfiliado" class="form-control" style="max-width: 160px"></div>
                        </div>
						<div class="form-group"><label class="col-sm-2 control-label">Código Afiliação</label>
                            <div class="col-sm-10"><input type="text" name="codigoAfiliacaoOrganismoAfiliado" maxlength="7" onkeypress="return SomenteNumero(event)" id="codigoAfiliacaoOrganismoAfiliado" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Organismo</label>
                            <div class="col-sm-10"><input type="text" name="nomeOrganismoAfiliado" onChange="this.value = this.value.toUpperCase()" id="nomeOrganismoAfiliado" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Região</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idRegiaoOrdemRosacruz" id="fk_idRegiaoOrdemRosacruz" style="max-width: 150px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'controller/regiaoRosacruzController.php';
                                    $rc = new regiaoRosaCruzController();
                                    $rc->criarComboBox();
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Tipo</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="tipoOrganismoAfiliado" id="tipoOrganismoAfiliado" style="max-width: 150px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">R+C</option>
                                    <option value="2">TOM</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Classificação</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="classificacaoOrganismoAfiliado" id="classificacaoOrganismoAfiliado" style="max-width: 150px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">Loja</option>
                                    <option value="2">Pronaos</option>
                                    <option value="3">Capítulo</option>
                                    <option value="4">Heptada</option>
                                    <option value="5">Atrium</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">CEP</label>
                            <div class="col-sm-10"><input type="text" name="cepOrganismoAfiliado" maxlength="9" id="cepOrganismoAfiliado" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Endereço</label>
                            <div class="col-sm-10"><input type="text" name="enderecoOrganismoAfiliado" id="enderecoOrganismoAfiliado" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Nº</label>
                            <div class="col-sm-10"><input type="text" name="numeroOrganismoAfiliado" id="numeroOrganismoAfiliado" maxlength="6" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Complemento</label>
                            <div class="col-sm-10"><input type="text" name="complementoOrganismoAfiliado" id="complementoOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Bairro</label>
                            <div class="col-sm-10"><input type="text" name="bairroOrganismoAfiliado" id="bairroOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Cidade</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idCidade" id="fk_idCidade" style="max-width: 300px" required="required">
                                    <option value="0">Selecione...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Estado</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idEstado" id="fk_idEstado" onChange="carregaCombo(this.value, 'fk_idCidade');" style="max-width: 300px" required="required">
                                    <option value="0">Selecione...</option>
                                    <?php
	                                    include_once 'controller/estadoController.php';
	                                    $ec = new estadoController();
	                                    $ec->criarComboBox();
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">E-mail</label>
                            <div class="col-sm-10"><input type="text" name="emailOrganismoAfiliado" id="emailOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Senha do E-mail</label>
                            <div class="col-sm-10" id="campoSenha"><input type="password" name="senhaEmailOrganismoAfiliado" id="senhaEmailOrganismoAfiliado" class="form-control" style="max-width: 320px"><label><input id="mostraSenha" type="checkbox" onclick="mostrarSenha()"> Mostrar senha</label></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Situação</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="situacaoOrganismoAfiliado" id="situacaoOrganismoAfiliado" style="max-width: 300px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">Recesso</option>
                                    <option value="2">Fechou</option>
                                    <option value="3" selected="selected">Aberto</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">CNPJ</label>
                            <div class="col-sm-10"><input type="text" name="cnpjOrganismoAfiliado" id="cnpjOrganismoAfiliado" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Celular</label>
                        	<div class="col-sm-10">
	                        	<select class="form-control col-sm-3" name="operadoraCelularOrganismoAfiliado" id="operadoraCelularOrganismoAfiliado" style="max-width: 120px">
	                                    <option value="0">Selecione...</option>
	                                    <option value="1">TIM</option>
	                                    <option value="2">Vivo</option>
	                                    <option value="3">Claro</option>
	                                    <option value="4">Oi</option>
	                            </select>
                            	<input type="tel" name="celularOrganismoAfiliado" id="celularOrganismoAfiliado" class="form-control" style="max-width: 200px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Telefone Fixo</label>
                            <div class="col-sm-10"><input type="text" name="telefoneFixoOrganismoAfiliado" id="telefoneFixoOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Outro Telefone</label>
                            <div class="col-sm-10"><input type="tel" name="outroTelefoneOrganismoAfiliado" id="outroTelefoneOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Fax</label>
                            <div class="col-sm-10"><input type="text" name="faxOrganismoAfiliado" id="faxOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Caixa Postal</label>
                            <div class="col-sm-10"><input type="text" name="caixaPostalOrganismoAfiliado" id="caixaPostalOrganismoAfiliado" class="form-control" style="max-width: 320px" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Cep da Caixa Postal</label>
                            <div class="col-sm-10"><input type="text" name="cepCaixaPostalOrganismoAfiliado" maxlength="9" id="cepCaixaPostalOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Endereço para Correspondência</label>
                        	<div class="col-sm-10">
                            	<div class="radio i-checks"><label class=""> <div class="iradio_square-green" style="position: relative;"><input type="radio" value="1" name="enderecoCorrespondenciaOrganismoAfiliado" id="EC_caixaPostal" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> <i></i> Logradouro </label></div>
                            	<div class="radio i-checks"><label class=""> <div class="iradio_square-green" style="position: relative;"><input type="radio" value="2" name="enderecoCorrespondenciaOrganismoAfiliado" id="EC_logradouro" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> <i></i> Caixa Postal </label></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Seq Cadast</label>
                            <div class="col-sm-10"><input type="text" name="seq_cadast" id="seq_cadast" value="0" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Sigla do Pais</label>
                            <div class="col-sm-10">
                                <input type="text" name="paisOa" id="paisOa" value="" />
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Não cobrar relatórios</label>
                        	<div class="col-sm-10">
                            	<div class="radio i-checks"><label class=""> <div class="iradio_square-green" style="position: relative;"><input type="radio" value="1" name="naoCobrarDashboard" id="naoCobrarDashboard_sim" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> <i></i> Sim </label></div>
                            	<div class="radio i-checks"><label class=""> <div class="iradio_square-green" style="position: relative;"><input type="radio" value="0" name="naoCobrarDashboard" id="naoCobrarDashboard_nao" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> <i></i> Não </label></div>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <!--  
                        <input type="hidden" name="seq_cadast" id="seq_cadast" value="0" />
                        -->
                        <input type="hidden" name="latitude" id="latitude" value="" />
                        <input type="hidden" name="longitude" id="logitude" value="" />
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" />
                                <a href="?corpo=buscaOrganismo" class="btn btn-white" id="cancelar" name="cancelar" />Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>	