<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?><!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cadastro de Resposta FAQ</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaFaq">Cadastro de Resposta FAQ</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Resposta FAQ</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=faq">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="post" action="acoes/acaoCadastrar.php">
                        <input type="hidden" name="idRespostaFaq" id="idRespostaFaq">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título da Resposta</label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloRespostaFaq" id="tituloRespostaFaq" class="form-control" required="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Responsável pela Resposta</label>
                            <div class="col-sm-10">
                                <input type="text" name="responsavelRespostaFaq" id="responsavelRespostaFaq" class="form-control" required="" value="TI - Desenvolvimento" readonly="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Resposta</label>
                            <div class="col-sm-10">
                                <textarea rows="7" name="respostaFaq" id="respostaFaq" class="form-control" style="max-width: 500px"></textarea>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" />
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>