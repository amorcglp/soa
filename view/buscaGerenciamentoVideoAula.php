<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Vídeo-aula salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Gerenciamento de Vídeo-aulas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Gerenciamento de Vídeo-aulas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Vídeo-aulas Cadastradas</h5>
                    <div class="ibox-tools">
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroVideoAula">
                            <i class="fa fa-plus"></i> Cadastrar Nova
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód.</th>
                                <th>Título</th>
                                <th>Data Inicial</th>
                                <th>Data Final</th>
                                <th>Status</th>
                                
                                <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/videoAulaController.php");

                            $c = new videoAulaController();
                            $resultado = $c->listaVideoAula();
//echo "<pre>";print_r($resultado);
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['idVideoAula']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['titulo']; ?>
                                        </td>
                                        <td>
                                            <?php echo substr($vetor['dataInicial'],8,2)."/".substr($vetor['dataInicial'],5,2)."/".substr($vetor['dataInicial'],0,4); ?>
                                        </td>
                                        <td>
                                            <?php echo substr($vetor['dataFinal'],8,2)."/".substr($vetor['dataFinal'],5,2)."/".substr($vetor['dataFinal'],0,4); ?>
                                        </td>
                                        <td>
                                <center>
                                    <div id="statusTarget<?php echo $vetor['idVideoAula'] ?>">
                                        <?php
                                        switch ($vetor['status']) {
                                            case 0:
                                                echo "<span class='badge badge-primary'>Ativo</span>";
                                                break;
                                            case 1:
                                                echo "<span class='badge badge-danger'>Inativo</span>";
                                                break;
                                        }
                                        ?>
                                    </div>
                                </center>
                                
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                    <?php if(in_array("2",$arrNivelUsuario)){?>
                                        <a class="btn btn-sm btn-info" href="painelDeControle.php?corpo=alteraVideoAula&id=<?php echo $vetor['idVideoAula'] ?>" data-rel="tooltip" title="">
                                            <i class="icon-edit icon-white"></i>  
                                            Editar                                            
                                        </a>
                                    <?php }?>
                                    <?php if(in_array("3",$arrNivelUsuario)){?>
                                        <span id="status<?php echo $vetor['idVideoAula'] ?>">
        <?php if ($vetor['status'] == 1) { ?>
                                                <a class="btn btn-sm btn-primary" href="#" onclick="mudaStatus('<?php echo $vetor['idVideoAula'] ?>', '<?php echo $vetor['status'] ?>', 'videoAula')" data-rel="tooltip" title="Ativar"> 
                                                    Ativar                                            
                                                </a>
        <?php } else { ?>
                                                <a class="btn btn-sm btn-danger" href="#" onclick="mudaStatus('<?php echo $vetor['idVideoAula'] ?>', '<?php echo $vetor['status'] ?>', 'videoAula')" data-rel="tooltip" title="Inativar">
                                                    Inativar                                            
                                                </a>
        <?php } ?>
                                        </span>
                                    <?php }?>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

