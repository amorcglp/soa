<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Imóveis</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeEstatutoTipo">Tipos de Anexo</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Tipos de Anexo</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Tipos de Atividade</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaImovelAnexoTipo">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoCadastrar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nome: </label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeImovelAnexoTipo" id="nomeImovelAnexoTipo" class="form-control" value="" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; max-width: 850px">
	                            	<div class="mail-text h-200">
										<textarea class="summernote" id="descricaoImovelAnexoTipo" name="descricaoImovelAnexoTipo"></textarea>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Entrega Anual? </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="anualImovelAnexoTipo" id="anualImovelAnexoTipo" style="max-width: 250px">
                                    <option value="0">Selecione</option>
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <a class="btn btn-white" href="?corpo=buscaImovelAnexoTipo"> Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>