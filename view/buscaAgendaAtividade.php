<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once ('model/organismoClass.php');
include_once ('model/agendaAtividadeClass.php');
include_once ('lib/functions.php');

$idAgendaAtividade      = isset($_REQUEST['idAgendaAtividade'])?$_REQUEST['idAgendaAtividade']:null;
$excluirAgendaAtividade = isset($_REQUEST['excluirAgendaAtividade'])?$_REQUEST['excluirAgendaAtividade']:null;
$removerAtividadesRecorrentes = isset($_REQUEST['removerAtividadesRecorrentes'])?$_REQUEST['removerAtividadesRecorrentes']:null;
$idOa = isset($_GET['idOa'])?$_GET['idOa']:null;

$excluirRecorrentes=0;
if($removerAtividadesRecorrentes)
{
    $aa = new AgendaAtividade();
    if($aa->excluirAtividadesRecorrentes($idOa))
    {
        $excluirRecorrentes=1;
    }
}

$excluir=0;

//Instalação de Columbas

if($idAgendaAtividade!=null&&$idAgendaAtividade!=0&&$excluirAgendaAtividade==1)
{
    $aa = new AgendaAtividade();
    if($aa->excluirAgendaAtividade($idAgendaAtividade))
    {
        $excluir=1;
    }
}

$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Atividade salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($excluirRecorrentes==1){?>
    <script>
        window.onload = function(){
            swal({
                title: "Sucesso!",
                text: "Atividades recorrentes excluídas com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Atividade já cadastrada anteriormente!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
if($excluir==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Atividade excluída com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( document ).tooltip();
  });
  </script>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Agenda de Atividades</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaAgendaAtividade">Agenda de Atividades</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<br>
<div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <a class="alert-link">Atenção:</a>
                        <br />
                        A Agenda de Atividades do Organismo é cobrada anualmente, mas deve ser <a class="alert-link">revisada mensalmente</a>, uma vez que as informações aqui postadas, são exibidas no Portal da AMORC (<a class="alert-link">www.amorc.org.br</a>)
                    </div>

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Agenda de Atividades do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                    <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2) || in_array("151",$arrFuncoes)){ ?>    
                        <a class="btn btn-xs btn-primary" href="#" onclick="abrirPopUpAgendaAtividadeTodas();">
                            <i class="fa fa-print"></i> Imprimir relatório da agenda de todas atividades
                        </a>
                        <a class="btn btn-xs btn-danger" href="#" onclick="removerTodasAsAtividadesRecorrentesAgendaPortal('<?php echo $idOrganismoAfiliado; ?>');">
                            <i class="fa fa-trash-o"></i> Remover todas as atividades recorrentes desse Organismo
                        </a>
                    <?php }?>
                        <a class="btn btn-xs btn-primary" href="#" onclick="abrirPopUpAgendaAtividade('<?php echo $idOrganismoAfiliado; ?>');">
                            <i class="fa fa-print"></i> Imprimir relatório da agenda de atividades
                        </a>
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAgendaAtividade">
                            <i class="fa fa-plus"></i> Cadastrar novo item na agenda
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Atividade</th>
                                <th>Tag</th>
                                <!--<th><center>Ativ. recorrente sem data definida?</center></th>-->
                                <th>Quando?</th>
                                <th>Local</th>
                                <th>Ano</th>
                                <th>Público</th>
                                <th>Cadastrado em</th>
                                <th>Cadastrado por</th>
                                <th>Status</th>
                                <th>Ultimo a Atualizar</th>
		                <th><center>Ações</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("model/agendaAtividadeClass.php");

                            $aa = new AgendaAtividade();
                            $resultado = $aa->listaAgendaAtividade($idOrganismoAfiliado);
							
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['nomeTipoAtividadeEstatuto'];
                                            if($vetor['complementoNomeAtividade']!="")
                                            {
                                                echo " - ".$vetor['complementoNomeAtividade'];
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
                                            
                                            switch ($vetor['tag']){ 
                                                case 0:
                                                    echo "Eventos";
                                                    break;
                                                case 1:
                                                    echo "Erin";
                                                    break;
                                                case 2:
                                                    echo "Iniciações";
                                                    break;
                                                case 3:
                                                    echo "Atividade do Organismo";
                                                    break;
                                            }
?>
                                        </td>
                                        <!--
                                        <td><center>
                                                <?php
                                                /*
                                                switch ($vetor['recorrente'])
                                                {
                                                    case 1:
                                                        echo "<span class='badge badge-primary'>Sim</span>";
                                                        break;
                                                    default:
                                                        echo "<span class='badge badge-danger'>Não</span>";
                                                        break;
                                                }*/
                                                ?></center>
                                        </td>-->
                                        <td>
                                            <?php if($vetor['recorrente']==0){?>
                                            Data Inicial: <?php echo substr($vetor['dataInicial'],8,2)."/".substr($vetor['dataInicial'],5,2)."/".substr($vetor['dataInicial'],0,4); ?> às Hora Inicial: <?php echo $vetor['horaInicial']; ?><br>
                                            Data Final: <?php echo substr($vetor['dataFinal'],8,2)."/".substr($vetor['dataFinal'],5,2)."/".substr($vetor['dataFinal'],0,4); ?>  às Hora Final: <?php echo $vetor['horaFinal']; ?><br>
                                            <?php }?>
                                            <?php if($vetor['recorrente']==1){?>
                                            <?php echo $vetor['diasHorarios']; ?>
                                            <?php }?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['local']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['anoCompetencia']; ?>
                                        </td>
                                        <td>
                                            <?php
                                                switch ($vetor['fk_idPublico'])
                                                {
                                                    case 1:
                                                        echo "Membros e não membros";
                                                        break;
                                                    case 2:
                                                        echo "Apenas membros";
                                                        break;
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo substr($vetor['dataCadastro'],8,2)."/".substr($vetor['dataCadastro'],5,2)."/".substr($vetor['dataCadastro'],0,4); ?>
                                        </td>
                                        <td>
                                            <center><?php echo $vetor['loginUsuario']; ?></center>
                                        </td>
                                        <td>
                                            <center>
                                    <div id="statusTarget<?php echo $vetor['idAgendaAtividade'] ?>">
                                        <?php
                                        switch ($vetor['statusAgenda']) {
                                            case 0:
                                                echo "<span class='badge badge-primary'>Ativo</span>";
                                                break;
                                            case 1:
                                                echo "<span class='badge badge-danger'>Inativo</span>";
                                                break;
                                        }
                                        ?>
                                    </div>
                                </center>
                                        </td>
                                        <td>
                                        <center><?php echo retornaLogin($vetor['ultimoAtualizar']); ?></center>
                                        </td>
                                        <td>
                                                <center>
                                                    <?php
                                    if(!$usuarioApenasLeitura) {

                                        if (in_array("2", $arrNivelUsuario)) { ?>
                                            <a href="?corpo=alteraAgendaAtividade&idagenda_atividade=<?php echo $vetor['idAgendaAtividade']; ?>"
                                               class="btn btn-sm btn-primary" data-toggle="tooltip"
                                               data-placement="left" title="Editar">
                                                <i class="fa fa-edit fa-white"></i>&nbsp;
                                                Editar
                                            </a>
                                        <?php } ?>
                                        <?php if (in_array("3", $arrNivelUsuario)) { ?>
                                            <span id="status<?php echo $vetor['idAgendaAtividade'] ?>">
        <?php if ($vetor['statusAgenda'] == 1) { ?>
            <a class="btn btn-sm btn-primary" href="#"
               onclick="mudaStatus('<?php echo $vetor['idAgendaAtividade'] ?>', '<?php echo $vetor['statusAgenda'] ?>', 'agendaAtividade')"
               data-rel="tooltip" title="Ativar">
                                                    Ativar                                            
                                                </a>
        <?php } else { ?>
            <a class="btn btn-sm btn-danger" href="#"
               onclick="mudaStatus('<?php echo $vetor['idAgendaAtividade'] ?>', '<?php echo $vetor['statusAgenda'] ?>', 'agendaAtividade')"
               data-rel="tooltip" title="Inativar">
                                                    Inativar                                            
                                                </a>
        <?php } ?>
                                        </span>
                                        <?php }
                                    }else{
                                        echo "<span class=\"btn btn-default btn-rounded\">Apenas Leitura</span>";
                                    }
                                        ?>
                                                </center>
                                        </td>
                                </tr>
                            	<?php
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
            $(document).on('ready', function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
</script>          

<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->