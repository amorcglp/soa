<link href="css/plugins/cropper/cropper.min.css" rel="stylesheet">

<script type="text/javascript">


</script>

<script type="text/javascript">

    //setInterval(function(){alert("entrou");},6000);

</script>

<?php
include_once("controller/perfilUsuarioController.php");
require_once ("model/perfilUsuarioClass.php");
require_once ("model/criaSessaoClass.php");

$pc = new perfilController();
$sessao = new criaSessao();
$seqCadastUsuario = $sessao->getValue("seqCadast");
$dados = $pc->buscaUsuario($sessao->getValue("seqCadast"));
$avatar = $pc->listaAvatarUsuario($sessao->getValue("seqCadast"));
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            <?php echo $dados->getNomeUsuario(); ?>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <strong>
                    Perfil do Membro
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        Perfil do Membro
                    </h5>
                </div>
                <div>
                    <div class="ibox-content no-padding border-left-right">
                        <center>
                            <img alt="image" class="img-responsive" src="<?php echo $avatar->getAvatarUsuario(); ?>">
                        </center>
                    </div>
                    <div class="ibox-content profile-content">
                        <h3>
                            <strong>
                                <?php echo $dados->getNomeUsuario(); ?>
                            </strong>
                        </h3>
                        <h4>
                            <i>
                                <?php
                                require_once ("model/funcaoUsuarioClass.php");
                                require_once ("model/funcaoClass.php");
                                $fum = new FuncaoUsuario();
                                $fm = new funcao();

                                $funcao = $fum->buscaPorFuncao($seqCadastUsuario);
                                if ($funcao) {
                                    foreach ($funcao as $vetorFuncao) {
                                        $vFuncao = $vetorFuncao['fk_idFuncao'];
                                    }
                                }
                                if(isset($vFuncao)){

                                    if($fum->verificaSeJaExistePorParametro($seqCadastUsuario,$vFuncao)){
                                        $funcaoNome = $fm->buscarIdFuncao($vFuncao);
                                        if ($funcaoNome) {
                                            foreach ($funcaoNome as $vetorFuncaoNome) {
                                                $vFuncaoNome = $vetorFuncaoNome['nomeFuncao'];
                                            }
                                            echo ucfirst(mb_strtolower($vFuncaoNome,'UTF-8'));
                                        }
                                    }else {
                                        echo "Erro ao identificar a função";
                                    }
                                } else {
                                    echo "Erro ao identificar a função";
                                }
                                ?>
                            </i>
                        </h4>
                        <p><i style="width: 16px" class="fa fa-institution"></i>
                            <?php
                            include_once("model/organismoClass.php");
                            $o = new organismo();
                            $nomeOrganismo = " LOJA R+C CURITIBA - PR101";//Default
                            if($sessao->getValue("siglaOA")!="")
                            {
                                $resultadoOA = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));

                                if ($resultadoOA) {
                                    foreach ($resultadoOA as $vetorOA) {
                                        $sessao->setValue("idOrganismoAfiliado", $vetorOA["idOrganismoAfiliado"]);//Guardar Id do Organismo na Sessão
                                        switch($vetorOA['classificacaoOrganismoAfiliado']){
                                            case 1:
                                                $classificacao =  "Loja";
                                                break;
                                            case 2:
                                                $classificacao =  "Pronaos";
                                                break;
                                            case 3:
                                                $classificacao =  "Capítulo";
                                                break;
                                            case 4:
                                                $classificacao =  "Heptada";
                                                break;
                                            case 5:
                                                $classificacao =  "Atrium";
                                                break;
                                        }
                                        switch($vetorOA['tipoOrganismoAfiliado']){
                                            case 1:
                                                $tipo = "R+C";
                                                break;
                                            case 2:
                                                $tipo = "TOM";
                                                break;
                                        }

                                        $nomeOrganismo = $classificacao . " " . $tipo . " " .$vetorOA["nomeOrganismoAfiliado"]. " - ".$vetorOA["siglaOrganismoAfiliado"];
                                    }

                                }
                            }
                            echo $nomeOrganismo;
                            ?>
                        </p>
                        <?php if ($dados->getEmailUsuario() != "") { ?>
                            <p><i style="width: 16px" class="fa fa-envelope"></i> <?php echo $dados->getEmailUsuario(); ?></p>
                        <?php } ?>
                        <?php if ($dados->getTelefoneResidencialUsuario() != "") { ?>
                            <p><i style="width: 16px" class="fa fa-phone"></i> <?php echo $dados->getTelefoneResidencialUsuario(); ?></p>
                        <?php } ?>
                        <?php if ($dados->getTelefoneComercialUsuario() != "") { ?>
                            <p><i style="width: 16px" class="fa fa-phone"></i> <?php echo $dados->getTelefoneComercialUsuario(); ?></p>
                        <?php } ?>
                        <?php if ($dados->getCelularUsuario() != "") { ?>
                            <p><i style="width: 16px" class="fa fa-mobile-phone"></i> <?php echo $dados->getCelularUsuario(); ?></p>
                        <?php } ?>
                        <?php if ($dados->getCodigoDeAfiliacao() != "") { ?>
                            <p><i style="width: 16px" class="fa fa-credit-card"></i> <?php echo $dados->getCodigoDeAfiliacao(); ?></p>
                        <?php } ?>
                        <?php if ($dados->getCienteAssinaturaDigital() != 0) { ?>
                            <p><i style="width: 16px" class="fa fa-check-square"></i> Ciente da Assinatura Eletrônica: <b>sim</b><p><i class="fa fa-calendar" style="width: 16px"></i> Data que ficou ciente: <b><?php echo substr($dados->getDataAssinaturaDigital(),8,2);?>/<?php echo substr($dados->getDataAssinaturaDigital(),5,2);?>/<?php echo substr($dados->getDataAssinaturaDigital(),0,4);?> às <?php echo substr($dados->getDataAssinaturaDigital(),11,8);?></b></p>
                        <?php }else{ ?>
                            <p><i style="width: 16px" class="fa fa-check-square"></i> Ciente da Assinatura Eletrônica: <b>não</b>
                        <?php }?>
                    </div>
                    <div class="ibox-content">
                        <center style="margin-top: 5px">
                            <a data-toggle="modal" data-target="#myModaAlteraInfo" class="btn btn-primary"> Editar informações pessoais e foto</a>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Contatos do Organismo</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaContatos">
                            <i class="fa fa-mail-forward"></i> Ir para a página de todos os contatos
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <!--
                    <div class="row" style="margin-bottom: 20px">
                        <div class="col-sm-offset-6 col-sm-6">
                            <input type="text" id="pesquisar" name="pesquisar" class="form-control" placeholder="Pesquisar contato">
                        </div>
                    </div>
                    <div class="row" id="conteudo"></div>
                    <div id="linha_tracejada" class="hr-line-dashed"></div>
                    <div class="text-center">
                        <div class="btn-group" id="paginador">
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal's da página INICIO -->

<!-- Modal de atualização de informações pessoais -->
<div class="modal inmodal" id="myModaAlteraInfo" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php" enctype="multipart/form-data">
                <input id="form_perfilUsuario" name="form_perfilUsuario" type="hidden">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Atualizar dados cadastrais</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <!-- DIV de avatar INÍCIO -->
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Imagem de perfil</label>
                            <div class="col-sm-8" style="color: red">Temporariamente indisponível!</div>
                        </div>
                        <!-- DIV de avatar FIM -->

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nome Completo</label>
                            <div class="col-sm-8">
                                <input type="text" name="nomeUsuario" id="nomeUsuario" class="form-control" value="<?php echo $dados->getNomeUsuario(); ?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">E-mail</label>
                            <div class="col-sm-8">
                                <input type="text" name="emailUsuario" id="emailUsuario" class="form-control" value="<?php echo $dados->getEmailUsuario(); ?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Telefone Residêncial</label>
                            <div class="col-sm-8">
                                <input type="text" name="telefoneResidencialUsuario" id="telefoneResidencialUsuario" class="form-control" value="<?php echo $dados->getTelefoneResidencialUsuario(); ?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Telefone Comercial</label>
                            <div class="col-sm-8">
                                <input type="text" name="telefoneComercialUsuario" id="telefoneComercialUsuario" class="form-control" value="<?php echo $dados->getTelefoneComercialUsuario(); ?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Celular</label>
                            <div class="col-sm-8">
                                <input type="text" name="celularUsuario" id="celularUsuario" class="form-control" value="<?php echo $dados->getCelularUsuario(); ?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Login</label>
                            <div class="col-sm-8">
                                <input type="text" name="loginUsuario" id="loginUsuario" class="form-control" readonly value="<?php echo $dados->getLoginUsuario(); ?>" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Senha</label>
                            <div class="col-sm-8">
                                <input type="password" name="senhaUsuario" id="senhaUsuario"  maxlength="8" class="form-control" value="<?php echo $dados->getSenhaUsuario(); ?>" style="max-width: 320px">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="seqCadast" id="seqCadast" class="form-control" value="<?php echo $sessao->getValue("seqCadast"); ?>">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Atualizar">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal's da página FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>