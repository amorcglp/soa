
<!-- Conteúdo DE INCLUDE INÍCIO -->

<?php 
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório Gerencial Atas/Financeiro/Membros Ativos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Relatório</a>
            </li>
            <li class="active">
                <strong><a>Relatório Gerencial Atas/Financeiro/Membros Ativos</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Relatório Gerencial Atas/Financeiro/Membros Ativos</h5>
                </div>
                <div class="ibox-content">
                    <form name="extratoFinanceiro" class="form-horizontal" method="post"  onSubmit="return validaRelatorioGerencialFinanceiroGC()" >
                        <div class="form-group form-inline">
                            
                                <?php if(!isset($regiaoUsuario)&&$_SESSION['fk_idDepartamento'] != 2 && $_SESSION['fk_idDepartamento'] != 3){ 
                                            $oaUsuario = $idOrganismoAfiliado;
                                            ?>
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                        <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                            <option value="0">Selecione</option>
                                            <?php
                                            include_once 'controller/organismoController.php';
                                            $oc = new organismoController();
                                            $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                            ?>
                                        </select>
                                            <?php
                                        }else{ 
                                            $oaUsuario = "";
                                            ?>
                                <label class="col-sm-3 control-label">Região:</label>
                                <div class="col-sm-9">
                                        <select name="regiao" id="regiao" data-placeholder="Selecione uma regiao..." class="chosen-select" style="width:350px;" tabindex="2">
                                            <option value="0">Selecione</option>
                                            <?php
                                            include_once 'controller/regiaoRosacruzController.php';
                                            $rrc = new regiaoRosacruzController();
                                            $rrc->criarComboBox(0,$regiaoUsuario);
                                            ?>
                                        </select>
                                <?php }?>
                            </div>
                        </div>
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="ano" name="ano" type="text" value="" style="max-width: 83px">
                            </div>
                        </div>
                        -->
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label"><span id="txtMesInicial">Mês/</span>Ano Inicial:</label>
                            <div class="col-sm-9">
                                <select name="mesInicial" id="mesInicial" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        for($i=1;$i<=12;$i++)
                                        {
                                            echo "<option value=\"".str_pad($i, 2, "0", STR_PAD_LEFT)."\">".str_pad($i, 2, "0", STR_PAD_LEFT)."</option>";
                                        }
                                    ?>    
                                </select>
                                <span id="barra">/</span>
                                <select name="anoInicial" id="anoInicial" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        $tAno = date('Y')+4;
                                        for($i=2016;$i<=$tAno;$i++)
                                        {
                                            echo "<option value=\"".$i."\">".$i."</option>";
                                        }
                                    ?>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label"><span id="txtMesFinal">Mês/</span>Ano Final:</label>
                            <div class="col-sm-9">
                                <select name="mesFinal" id="mesFinal" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        for($i=1;$i<=12;$i++)
                                        {
                                            echo "<option value=\"".str_pad($i, 2, "0", STR_PAD_LEFT)."\">".str_pad($i, 2, "0", STR_PAD_LEFT)."</option>";
                                        }
                                    ?>    
                                </select>
                                <span id="barra2">/</span>
                                <select name="anoFinal" id="anoFinal" data-placeholder="" class="chosen-select" style="width:70px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php 
                                        $tAno = date('Y')+4;
                                        for($i=2016;$i<=$tAno;$i++)
                                        {
                                            echo "<option value=\"".$i."\">".$i."</option>";
                                        }
                                    ?>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mostrar:</label>
                            <div class="col-sm-9">
                                <input type="checkbox" name='todos' checked="checked" onchange="marcardesmarcar();"/> Todos<br>
                                <input type="checkbox" name="linha1" id="linha1" class="marcar" checked="checked"> RAM<br>
                                <input type="checkbox" name="linha2" id="linha2" class="marcar" checked="checked"> RAM - Lançamento no SOA<br>
                                <input type="checkbox" name="linha3" id="linha3" class="marcar" checked="checked"> RAM - Upload<br>
                                <input type="checkbox" name="linha11" id="linha11" class="marcar" checked="checked"> RAM - Monitor Regional Presente?<br>
                                <input type="checkbox" name="linha4" id="linha4" class="marcar" checked="checked"> M. FIN - Saldo Anterior<br>
                                <input type="checkbox" name="linha9" id="linha9" class="marcar" checked="checked"> M. FIN - Entradas<br>
                                <input type="checkbox" name="linha10" id="linha10" class="marcar" checked="checked"> M. FIN - Saídas<br>
                                <input type="checkbox" name="linha5" id="linha5" class="marcar" checked="checked"> M. FIN - Saldo Atual<br>
                                <input type="checkbox" name="linha6" id="linha6" class="marcar" checked="checked"> M. FIN - Divida Atual GLP<br>
                                <input type="checkbox" name="linha7" id="linha7" class="marcar" checked="checked"> M. FIN - Dívida Atual Outros<br>
                                <input type="checkbox" name="linha8" id="linha8" class="marcar" checked="checked"> Membros Ativos<br>
                                
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <input type="hidden" name="oaUsuario" id="oaUsuario" value="<?php echo $oaUsuario;?>">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <a href="#" onclick="abrirPopupRelatorioGerencialFinanceiro();" class="btn btn-sm btn-success" data-placement="left" title="Salvar">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Gerar Relatório
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	