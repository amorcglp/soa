<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php

@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once ('model/columbaRelatorioTrimestralClass.php');
include_once ('model/organismoClass.php');
include_once ('lib/functions.php');

$idColumba      = isset($_REQUEST['idColumba'])?$_REQUEST['idColumba']:null;
$dataInstalacao = isset($_REQUEST['dataInstalacao'])?$_REQUEST['dataInstalacao']:null;

$instalou=0;

//Instalação de Columbas
if($idColumba!=null&&$idColumba!=0)
{
    $dataInstalacao = substr($dataInstalacao,6,4)."-".substr($dataInstalacao,3,2)."-".substr($dataInstalacao,0,2);
    $columba = new ColumbaRelatorioTrimestral();
    if($columba->instalarColumba($idColumba,$dataInstalacao))
    {
        $instalou=1;
    }
}

if($instalou==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Columba instalada com sucesso! Agora essa columba aparece na lista de columbas atuantes!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Columba incluida com sucesso na lista de Columbas Não Instaladas!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Columba já encontra-se na lista de columbas não instaladas!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( document ).tooltip();
  });
  </script>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Columbas Não Instaladas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaColumbass">Columbas Não Instaladas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Columbas não instaladas do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroColumbas">
                            <i class="fa fa-plus"></i> Nova Columba à ser instalada
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód. de Afiliação</th>
                                <th>Nome</th>
                                <th width="100">Idade</th>
                                <th>Data de Nascimento</th>
                                <th>Data Prevista para Instalação</th>
		                <th><center>Ações</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/columbaRelatorioTrimestralController.php");

                            $m = new columbaRelatorioTrimestralController();
                            $resultado = $m->listaColumba($idOrganismoAfiliado,1);
							
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                               <?php if(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA"))){
                               		                              		
                               		//Extrair primeiro nome
                               		$arrNome = explode(" ",$vetor['nomeColumba']);
                               		$primeiroNome = $arrNome[0];
                               		
                               		$ocultar_json=1;
                               		$codigoAfiliacao=$vetor['codigoAfiliacao'];
                               		$nomeMembro=$vetor['nomeColumba'];
                               		$tipoMembro=2;
	                               	include './js/ajax/retornaDadosMembro.php';
                                        $obj = json_decode(json_encode($return),true);
                               		//echo "<pre>";print_r($_SESSION['situacaoMembro'][$codigoAfiliacao][$primeiroNome]);
                                        $dataNascimento = substr($obj['result'][0]['fields']['fDatNascimento'],0,10);
                               	?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['codigoAfiliacao']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['nomeColumba']; ?>
                                        </td>
                                        <td>
                                            <?php echo getIdade($dataNascimento); ?>
                                        </td>
                                        <td>
 	                                       	<?php 
		 	                            echo substr($dataNascimento,8,2)."/".substr($dataNascimento,5,2)."/".substr($dataNascimento,0,4);	  		                                    	
                                        	?>
                                        </td>
                                        <td>
 	                                       	<?php 
 	                                       		echo substr($vetor['dataPrevistaInstalacao'],8,2)."/".substr($vetor['dataPrevistaInstalacao'],5,2)."/".substr($vetor['dataPrevistaInstalacao'],0,4);  		                                    	
                                        	?>
                                        </td>                    
                                        <td>
                                                <center>
                                                    <button type="button" class="btn btn-sm btn-info" data-target="#mySeeInstalar" data-toggle="modal" data-placement="left" onclick="document.getElementById('idColumba').value=<?php echo $vetor['idColumbaRelatorioTrimestral']; ?>;document.getElementById('nomeColumba').innerHTML='<?php echo str_replace("."," ",$vetor['nomeColumba']); ?>';document.getElementById('codigoAfiliacao').innerHTML=<?php echo $vetor['codigoAfiliacao']; ?>;">
                                                        <i class="fa fa-check-square-o fa-white"></i>&nbsp;
                                                        Registrar Instalação de Columba
                                                    </button>
                                                </center>
                                        </td>
                                </tr>
                            	<?php
                                    }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
            $(document).on('ready', function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
</script>          

<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeInstalar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-check-square-o modal-icon"></i>
                <h4 class="modal-title">Instalação de Columba</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="instalarColumba" name="instalarColumba" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Afiliação: </label>
                            <div class="col-sm-9">
                                <div id="codigoAfiliacao"></div>
                            </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label">Nome da Columba: </label>
                            <div class="col-sm-9">
                                <div id="nomeColumba"></div>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Instalação:</label>
                        <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                            <input name="dataInstalacao" maxlength="10" class="data" onkeypress="return SomenteNumero(event)" id="dataInstalacao" type="text" value="" class="form-control" style="max-width: 102px"  required="required">
                        </div>
                    </div>
                    <input type="hidden" id="idColumba" name="idColumba" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Instalar Columba" onclick="document.instalarColumba.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
