<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Cadastro de Discurso</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php?corpo=buscaDiscurso">Discursos</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Discurso</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<script type="text/javascript">
    function validaCampoExperimento() {
        if(document.getElementById("temExperimento").value === "") {
            alert("Preencha corretamente o campo Experimento!");
            document.getElementById("temExperimento").focus();
            return false;
        }
        
        return true;
    }
</script>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Discurso para Convocação Ritualística</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaDiscurso">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="formCadastro" id="formCadastro" enctype="multipart/form-data" method="post" class="form-horizontal" action="acoes/acaoCadastrar.php">
                        <input type="hidden" name="seqCadast" id="seqCadast" value="<?php echo $_SESSION['seqCadast']; ?>">
                        <input type="hidden" name="idDocumento" id="idDocumento" value="null">
                        <input type="hidden" name="qtdeImpressao" id="qtdeImpressao" value="0">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Número da Mensagem</label>
                            <div class="col-sm-10">
                                <input class="form-control fn" type="number" name="numeroMensagem" id="numeroMensagem" min="0" step="any" class="form-control" style="max-width: 80px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Autor da Mensagem</label>
                            <div class="col-sm-10">
                                <input type="text" name="autorMensagem" id="autorMensagem" class="form-control" style="max-width: 200px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Título do Discurso</label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloMensagem" id="tituloMensagem" class="form-control" style="max-width: 200px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Tipo</label>
                            <div class="col-sm-10">
                                <select name="tipo" id="tipo">
                                    <option value="0">Selecione</option>
                                    <option value="1">R+C</option>
                                    <option value="2">OGG</option>
                                </select>    
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_discurso">
                            <label class="col-sm-2 control-label">Data da Criação do Discurso</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataCriacao" maxlength="10" id="dataCriacao" type="text" class="form-control" style="max-width: 102px">
                                <div hidden id="data_invalida_dataCriacao" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Tem experimento?</label>
                            <div class="col-sm-10">
                                <select name="temExperimento" id="temExperimento" required="required">
                                    <option value="">Selecione...</option>
                                    <option value="0">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Cadastrar" onclick="return validaCampoExperimento()">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar" onclick="window.location='painelDeControle.php?corpo=buscaDiscurso'">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
