<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Notificações</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li class="active">
                <strong>Notificações</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Notificações</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover issue-tracker">
                            <tbody>
                                <?php
                                include_once("controller/notificacaoGlpController.php");
                                include_once("controller/usuarioController.php");

                                $pagina	    = isset($_GET['pagina']) ? $_GET['pagina'] : null;
                                $pagina     = ($pagina <= 0) ? 1 : $pagina;

                                $ngc = new notificacaoGlpController();
                                $resultado          = $ngc->listaNotificacaoOficialGlp($_SESSION['seqCadast'],null,$pagina);

                                $nroRegistros       = $ngc->retornaNumeroNotificacaoOficialGlp($_SESSION['seqCadast']);
                                //echo "<br>";
                                //echo "nroRegistros: " . $nroRegistros."<br>";

                                $nroPaginas         = ceil($nroRegistros/10);
                                //echo "nroPaginas: " . ceil($nroPaginas);

                                $notificacoes = array();
                                
                                if ($resultado) {
                                    foreach ($resultado as $vetorOficial) {
                                    	$notificacoes['notificacoes'][] = $vetorOficial['fk_idNotificacaoGlp'];
                                    }
                                }
                                if ($notificacoes){ ?>

                                    <center>
                                        <div class="btn-group">
                                            <?php
                                            if(($pagina-1) >= 1){
                                                if(($pagina - 1) != 1) { ?>
                                                    <a type="button" href='?corpo=buscaNotificacoes&pagina=1' class="btn btn-white">
                                                        <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>
                                                    </a>
                                                <?php } ?>
                                                <a type="button" href='?corpo=buscaNotificacoes&pagina=<?php echo $pagina - 1 ?>' class="btn btn-white">
                                                    <i class="fa fa-chevron-left"></i>
                                                </a>
                                            <?php
                                            }
                                            for($i=1; $i < ($nroPaginas+1); $i++){
                                                if((($i - 3) <= $pagina) && (($i + 3) >= $pagina)){
                                                    $active = ($i == $pagina) ? "btn btn-white active" : "btn btn-white";
                                                    echo "<a href='?corpo=buscaNotificacoes&pagina=".$i."' class='".$active."'>".$i."</a>";
                                                }
                                            }
                                            if(($pagina) < $nroPaginas){ ?>
                                                <a type="button" href='?corpo=buscaNotificacoes&pagina=<?php echo $pagina+1 ?>' class="btn btn-white">
                                                    <i class="fa fa-chevron-right"></i>
                                                </a>
                                            <?php
                                                if(($pagina + 1) !=  $nroPaginas) { ?>
                                                    <a type="button" href='?corpo=buscaNotificacoes&pagina=<?php echo $nroPaginas ?>' class="btn btn-white">
                                                        <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i>
                                                    </a>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </center>
                                    <br>
                                    <div>
                                        <input style='margin-left: 8px' type='checkbox' id="marca_e_desmarca_notificacao" onclick="marcaDesmarcaNotificacao()">&nbsp;<i class='fa fa-arrow-left'></i>&nbsp;Selecionar Todos!
                                        <a style="margin-left: 10px" class="btn btn-xs btn-primary" onclick="mudaStatusNotificacao(1,<?php echo $pagina; ?>);">
                                            <i class="fa fa-eye"></i> Marcar como lido
                                        </a>
                                        <a style="margin-left: 10px" class="btn btn-xs btn-warning" onclick="mudaStatusNotificacao(0,<?php echo $pagina; ?>);">
                                            <i class="fa fa-eye-slash"></i> Marcar como não lido
                                        </a>
                                    </div>
                                    <br>
	                                <?php
                                    for($i=0;$i < count($notificacoes['notificacoes']);$i++) {
		                                $resultadoNotificacao = $ngc->listaNotificacaoGlp($notificacoes['notificacoes'][$i]);
		                                //echo "<pre>";print_r($resultadoNotificacao);
		                                if ($resultadoNotificacao) {
		                                	//$e=0;
		                                    foreach ($resultadoNotificacao as $vetor) {
		                                        ?>
		                                        <tr>
                                                    <td>
                                                        <input type="checkbox" name="notificacoes[]" id="<?php echo $vetor['idNotificacaoGlp']; ?>">
                                                    </td>
		                                            <td>
		                                                <?php
		                                                switch ($vetor['tipoNotificacaoGlp']) {
		                                                    case 1: echo "<span class='badge badge-danger'>Não Entregue</span>"; break;
		                                                    case 2: echo "<span class='badge badge-warning'>Falta Documento</span>"; break;
		                                                    case 3: echo "<span class='badge badge-info'>Erro de Documentação</span>"; break;
                                                            case 4: echo "<span class='badge badge-danger'>Deve Atualizar</span>"; break;
                                                            case 5: echo "<span class='badge badge-success'>Outro</span>"; break;
                                                            case 6: echo "<span class='badge badge-success'>Entrega</span>"; break;
                                                            case 7: echo "<span class='badge badge-success'>Transferência</span>"; break;
		                                                }
		                                                ?>
		                                            </td>
                                                    <!--
                                                    <td>
                                                        Loja R+C CURITIBA - PR101
                                                    </td>
                                                    -->
		                                            <td>
                                                        <center>
		                                        		<?php 
		                                            	include_once("model/notificacaoAtualizacaoServerClass.php");
		                                				$nglp2 = new NotificacaoGlp();
		                                				$resultado8 = $nglp2->lista($sessao->getValue("seqCadast"),$vetor['idNotificacaoGlp']);
		                                				
                                                        if($resultado8) {
                                                            foreach($resultado8 as $vetor8) {
                                                                if($vetor8['statusNotificacaoGlpUsuario']==1) {
                                                                    echo "<span class='badge badge-primary'>Visualizada</span>";
                                                                }else{
                                                                    echo "<span class='badge badge-warning'>Não visualizada</span>";
                                                                }
                                                            }
                                                        }
		                                            	?>
                                                        </center>
		                                        	</td>
		                                            <td class="issue-info">
		                                                <a href="?corpo=detalheNotificacao&idNotificacaoGlp=<?php echo $vetor['idNotificacaoGlp']; ?>">
		                                                    <?php echo $vetor['tituloNotificacaoGlp']; ?>
		                                                </a>
		                                                <br>
		                                                <small>
		                                                    <?php echo substr(str_replace("<p>", "", str_replace("</p>", "", $vetor['mensagemNotificacaoGlp'])), 0, 100); ?>...
		                                                    <a href="?corpo=detalheNotificacao&idNotificacaoGlp=<?php echo $vetor['idNotificacaoGlp']; ?>"><br>+saiba mais</a>
		                                                </small>
		                                            </td>
		                                            <td>
		                                                <i class="fa fa-institution"></i>
		                                                <?php 
		                                                	$um = new Usuario();
                               								$resultadoRemetente = $um->buscaUsuario($vetor['fk_seqCadastRemetente']);
									                        if ($resultadoRemetente) {
							                                    foreach ($resultadoRemetente as $vetorRemetente) {

                                                                    $arrNome				= explode(" ", $vetorRemetente['nomeUsuario']);
                                                                    $sobrenome = isset($arrNome[1]) ? ucfirst(mb_strtolower($arrNome[1], 'UTF-8')) : "";
                                                                    echo ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ". $sobrenome;

									                            	$resultDepartamento = $ngc->buscaDepartamento($vetorRemetente['fk_idDepartamento']);
										                            if ($resultDepartamento) {
									                                    foreach ($resultDepartamento as $vetorDepartamento) {
									                                		echo " [".$vetorDepartamento['nomeDepartamento']."]";
									                                    }
										                            }
							                                    }
							                                }
		                                                ?>
		                                            </td>
		                                            <td>
                                                        <center>
		                                                <?php
		                                                $data = date_create($vetor['dataNotificacaoGlp']);
		                                                echo date_format($data, 'd/m/Y')."<br>".date_format($data, 'H:i:s');
		                                                ?>
                                                        </center>
		                                            </td>
		                                        </tr>
		                                        <?php
		                                    }
		                                } else {
		                                ?>
		                                		<tr>
		                                            <td>
			                                            <center>
			                                                Nenhuma Notificação Recebida
														</center>
		                                            </td>
		                                        </tr>
		                                <?php 
		                                }
	                                }
                                } else {
                                ?>
                                		<tr>
                                            <td>
	                                            <center>
	                                                Nenhuma Notificação Recebida
												</center>
                                            </td>
                                        </tr>
                                <?php 
                                }
	                            ?>
                                <input type="hidden" id="seqCadastUsuario" name="seqCadastUsuario" value="<?php echo $_SESSION['seqCadast']; ?>" >
                            </tbody>
                        </table>
                        <center>
                            <div class="btn-group">
                                <?php
                                if(($pagina-1) >= 1){
                                    if(($pagina - 1) != 1) { ?>
                                        <a type="button" href='?corpo=buscaNotificacoes&pagina=1' class="btn btn-white">
                                            <i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i>
                                        </a>
                                    <?php } ?>
                                    <a type="button" href='?corpo=buscaNotificacoes&pagina=<?php echo $pagina - 1 ?>' class="btn btn-white">
                                        <i class="fa fa-chevron-left"></i>
                                    </a>
                                    <?php
                                }
                                for($i=1; $i < ($nroPaginas+1); $i++){
                                    if((($i - 3) <= $pagina) && (($i + 3) >= $pagina)){
                                        $active = ($i == $pagina) ? "btn btn-white active" : "btn btn-white";
                                        echo "<a href='?corpo=buscaNotificacoes&pagina=".$i."' class='".$active."'>".$i."</a>";
                                    }
                                }
                                if(($pagina) < $nroPaginas){ ?>
                                    <a type="button" href='?corpo=buscaNotificacoes&pagina=<?php echo $pagina+1 ?>' class="btn btn-white">
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                    <?php
                                    if(($pagina + 1) !=  $nroPaginas) { ?>
                                        <a type="button" href='?corpo=buscaNotificacoes&pagina=<?php echo $nroPaginas ?>' class="btn btn-white">
                                            <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right"></i>
                                        </a>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </center>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>