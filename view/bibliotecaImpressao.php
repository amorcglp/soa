<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Impressão</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php?corpo=biblioteca">Biblioteca</a>
            </li>
            <li class="active">
                <a href="painelDeControle.php?corpo=bibliotecaImpressao">Impressao</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<?php
require_once 'controller/usuarioController.php';
require_once 'controller/impressaoController.php';

$usuario = new usuarioController();
$impressao = new impressaoController();

$doc = isset($_GET['doc']) ? $_GET['doc'] : null;

function impPdf($arq) {
    global $impressao;
    if($arq != null && $impressao->cadastroImpressao()) {
        $arq = "impress/" . $arq . ".pdf";
        echo "<script type='text/javascript'>
              var p = window.open('$arq');
                  p.window.focus();
                  p.window.print();
              </script>";
    }
}

if($doc == null) {
   echo <<< EOT
<script type="text/javascript">
  alert("Nenhum documento selecionado!");
  window.location = 'painelDeControle.php';
</script>
EOT;
} else {
    $url = "painelDeControle.php?corpo=bibliotecaImpressao&doc=$doc";
    $dadosUsuario = $usuario->buscaUsuario($_SESSION['seqCadast']);
    $_SESSION['ImpDoc'] = $doc;
    
    if($dadosUsuario != false) {
        $impressao->setUsuario($dadosUsuario);
        if($doc != null && isset($_GET['impress'])) {
            impPdf($doc);
            return;
        }
    ?>
        <button type="button" class="btn btn-sm btn-info" onclick="" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Detalhes da ATA de Reunião Mensal dos OA's">
                                        <i class="fa fa-search-plus fa-white"></i>&nbsp;
                                        Detalhes
                                    </button>
    <?php
    } else {
        echo "<h2>Erro! Não foi possível coletar dados do usuário!</h2>";
        return ;
    }
} 
?>

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->

<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes da Ata de Reunião</h4>
                
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Ata:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="dataAtaReuniaoMensal">pppppp</span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora inicial:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaInicialAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora do encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaFinalAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número de membros:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="numeroMembrosAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Membro que presidiu:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="nomePresidenteAtaReuniaoMensal"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Oficiais Administrativos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static">
                                <span id="oficiaisAdministrativos"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura do relatório mensal com as seguintes restrições:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_um"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura da ata anterior:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_dois"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Relatório das Comissões:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_tres"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos Pendentes:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_quatro"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Assuntos novos:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_cinco"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Expansão da Ordem:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_seis"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Palavra livre:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_sete"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações da Suprema Grande Loja, da GLP e Grande Conselheiro(a):</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_oito"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Leitura das Comunicações Gerais:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_nove"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Encerramento:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="topico_dez"></span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!--
<div class="modal inmodal" id="myModa" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Concordância</h4>
            </div>
            <div class="body">
              <form action="" class="form-horizontal">
                <div class="form-group">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget faucibus elit. Aliquam elementum porta nisl, consectetur commodo mauris
                dignissim eu. Cras diam arcu, imperdiet eu libero sed, venenatis interdum velit. Class aptent taciti sociosqu ad litora torquent per conubia
                nostra, per inceptos himenaeos. Vivamus tincidunt nulla nec est aliquet, dictum accumsan mauris pulvinar. Cum sociis natoque penatibus et
                magnis dis parturient montes, nascetur ridiculus mus. Curabitur ac mi nulla. Aliquam vel nunc tincidunt arcu venenatis dignissim. Sed posuere
                luctus semper. Morbi congue odio eget euismod bibendum.

                Aliquam tempus pellentesque dolor non dignissim. Sed eu nunc nunc. Mauris et orci at tortor maximus consectetur eu quis justo.
                Praesent nec aliquam magna, et sollicitudin nisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas pretium, tortor ac
                lobortis iaculis, lectus ex cursus orci, ac dapibus elit nisi a orci. Suspendisse convallis velit non eros bibendum sodales. In felis erat,
                vestibulum vel auctor id, scelerisque et velit. Quisque tempus posuere velit ut pretium. In hac habitasse platea dictumst. Quisque quis ex
                porttitor neque consequat suscipit vitae quis nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis non lorem sodales, luctus
                turpis sed, convallis risus.
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white"
                        data-dismiss="modal"
                        onclick="window.location = 'painelDeControle.php?corpo=biblioteca&doc=<?php echo $vetor['arquivo']; ?>&impress'">Ok! Concordo!</button>
            </div>
        </div>
    </div>
 </div>
-->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
</body>
</html>
