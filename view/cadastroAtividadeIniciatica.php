<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();

include_once("controller/organismoController.php");
$oc                                 = new organismoController();
$dadosOrganismo                     = $oc->buscaOrganismo($idOrganismoAfiliado);
$classificacaoOrganismoAfiliado     = $dadosOrganismo->getClassificacaoOrganismoAfiliado();

if($classificacaoTemporaria!="")
{
    $classificacao=$classificacaoTemporaria;
}
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades Iniciáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeIniciatica">Relatório de Atividades Iniciáticas</a>
            </li>
            <li class="active">
                <a>Agendamento de Atividade Iniciática</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Agendamento de Atividade Iniciática</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciatica">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form id="form_atividade" name="form_atividade" onsubmit="return verificaCamposPreenchidosAtividadeIniciaticaSubmit();" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php">
                        <div class="form-group form-inline"><label class="col-sm-3 control-label">Organismo Afiliado: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-5 chosen-select" onchange="mudaOrganismoGetEquipes(this.value);" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." style="max-width: 350px">
                                    <option value="0">Selecione...</option>
                                    <?php
                                        include_once 'controller/organismoController.php';
                                        require_once("model/criaSessaoClass.php");
                                        $oc = new organismoController();
                                        $sessao = new criaSessao();

                                        if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){
                                            $oc->criarComboBox();
                                        } else {
                                            $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                        }
                                    ?>
                                	</select>
                            </div>
                        </div>
                        <div class="form-group form-inline" id="atividadeIniciaticaOficial">
                            <label class="col-sm-3 control-label">Equipe Iniciática: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-5" name="fk_idAtividadeIniciaticaOficial" id="fk_idAtividadeIniciaticaOficial" data-placeholder="Selecione uma equipe responsável..." style="max-width: 350px" required="required">
                                    <option value="0">Selecione...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline" id="atividadeIniciaticaColumba">
                            <label class="col-sm-3 control-label">Columba: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-5" name="fk_idAtividadeIniciaticaColumba" id="fk_idAtividadeIniciaticaColumba" data-placeholder="Selecione a columba escalada..." style="max-width: 350px" required="required">
                                    <option value="0">Selecione...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Secretário: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoRecepcao" name="codAfiliacaoRecepcao" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome completo" class="form-control" id="nomeRecepcao" name="nomeRecepcao" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoRecepcao','nomeRecepcao','codAfiliacaoRecepcao','nomeRecepcao','h_seqCadastRecepcao','h_nomeRecepcao','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Após inserir o nome pressione TAB
                            </div>
                            <input type="hidden" name="tipo" id="tipo" value="1">
                            <input type="hidden" maxlength="50" name="recepcaoAtividadeIniciaticaOficial" id="recepcaoAtividadeIniciaticaOficial" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="h_seqCadastRecepcao" id="h_seqCadastRecepcao">
                            <input type="hidden" name="h_nomeRecepcao" id="h_nomeRecepcao">
                            <input type="hidden" name="h_seqCadastCompanheiroRecepcao" id="h_seqCadastCompanheiroRecepcao">
                            <input type="hidden" name="h_nomeCompanheiroRecepcao" id="h_nomeCompanheiroRecepcao">
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Grau de Iniciação: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="tipoAtividadeIniciatica" id="tipoAtividadeIniciatica" style="max-width: 320px" required="required">
                                    <option value="0">Selecione...</option>
                                    <?php if($classificacaoOrganismoAfiliado == 1) { ?>
                                    <option value="1">1º Grau de Templo</option>
                                    <option value="2">2º Grau de Templo</option>
                                    <option value="3">3º Grau de Templo</option>
                                    <option value="4">4º Grau de Templo</option>
                                    <option value="5">5º Grau de Templo</option>
                                    <option value="6">6º Grau de Templo</option>
                                    <option value="7">7º Grau de Templo</option>
                                    <option value="8">8º Grau de Templo</option>
                                    <option value="9">9º Grau de Templo</option>
                                    <option value="10">10º Grau de Templo</option>
                                    <option value="11">11º Grau de Templo</option>
                                    <option value="12">12º Grau de Templo</option>
                                    <option value="13">Iniciação a Loja</option>
                                    <?php } ?>
                                    <?php if($classificacaoOrganismoAfiliado == 2) { ?>
                                    <option value="14">Iniciação ao Pronaos</option>
                                    <?php } ?>
                                    <?php if($classificacaoOrganismoAfiliado == 3) { ?>
                                    <option value="16">Iniciação ao Capítulo</option>
                                    <?php } ?>
                                    <option value="15">Discurso de Orientação</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_atividade_iniciatica">
                            <label class="col-sm-3 control-label">Data: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input onkeypress="valida_data(this,'dataRealizadaAtividadeIniciatica')" maxlength="10" name="dataRealizadaAtividadeIniciatica" id="dataRealizadaAtividadeIniciatica" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataRealizadaAtividadeIniciatica" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Hora de Início: </label>
                            <div class="col-sm-9">
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                    <input type="text" class="form-control" id="horaRealizadaAtividadeIniciatica" name="horaRealizadaAtividadeIniciatica" value="00:00" style="max-width: 70px"  required="required">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Local de Realização: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="29" name="localAtividadeIniciatica" id="localAtividadeIniciatica" class="form-control" style="max-width: 320px"  required="required">
                            </div>
                        </div>
                        <div class="form-group" id="anotacoesAtivInic">
                            <label class="col-sm-3 control-label">Observações: </label>
                            <div class="col-sm-9">
                                <div style="border: #ccc solid 1px; max-width: 800px">
                                    <div class="mail-text h-200">
                                        <textarea class="summernote" id="anotacoesAtividadeIniciatica" name="anotacoesAtividadeIniciatica"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input type="hidden" id="loginAtualizadoPor" name="loginAtualizadoPor" value="<?php echo $sessao->getValue("loginUsuario") ?>">
                                <input type="hidden" id="classificacaoOa" name="classificacaoOa" value="<?php echo $classificacao;?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar"" />
                                <a class="btn btn-white" href="?corpo=buscaAtividadeIniciatica" id="cancelar" name="cancelar">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo de Modal Início -->

<!-- Modal Cadastrar Tipo de Escritura Início -->
<div class="modal inmodal fade" id="modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Modal</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Conteúdo de Modal Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
