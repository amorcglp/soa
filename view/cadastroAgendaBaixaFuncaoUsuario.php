
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>
$(document).ready(function(){
	$("#dataNascimento1").mask("99/99/9999");
});
</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Área de ATAs</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="">Administração</a>
            </li>
            <li class="active">
                <strong><a>Agenda de Baixas Automáticas</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de agendamento da Baixa Automática</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAgendaBaixaFuncaoUsuario">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaAtaReuniaoMensal()" >
                        <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Quantidade de Dias da Prorrogação da Baixa após término do mandato: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="quantidadeDiasProrrogacaoBaixa" name="quantidadeDiasProrrogacaoBaixa" type="text" value="0" style="max-width: 343px" required="required">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data Inicial:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAgendadaInicial" maxlength="10" id="dataAgendadaInicial" type="text" class="form-control" style="max-width: 102px"  required="required">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data Final:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAgendadaFinal" maxlength="10" id="dataAgendadaFinal" type="text" class="form-control" style="max-width: 102px"  required="required">
                            </div>
                        </div>    
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-12">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Agendar Baixa"
									>
									<i class="fa fa-check fa-white"></i>&nbsp; Salvar
								</button>
								&nbsp; <a href="?corpo=buscaAgendaBaixaFuncaoUsuario"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
	
					</form>
                </div>
            </div>
        </div>
	</div>
	
	

</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-times-circle modal-icon"></i>
                <h4 class="modal-title">Edição de ATA de Reunião Mensal do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <label>Motivo da edição:</label>
                <div style="border: #ccc solid 1px">
                    <div class="summernote"></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="atualizaMotivoEdicaoATA();">Editar ATA</button>
            </div>
        </div>
    </div>
</div>

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	