<?php
exit();
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once 'controller/emailController.php';
$ec = new emailController();

include_once 'model/emailDeParaClass.php';
include_once 'model/usuarioClass.php';
/**
 * Limpar e-mails inacabados
 */
//$retorno2 = $ec->limparEmails();

/**
 * Gerar Id para o próximo email
 */
$_SESSION['ultimoIdNovoEmail']	= 0;//Iniciar ultimo Id novo email
include_once 'model/emailClass.php';
$e = new Email();
$ultimo_id=0;
$e->setDe($sessao->getValue("seqCadast"));
$ultimo_id = $e->cadastraEmail();
$_SESSION['ultimoIdNovoEmail']=$ultimo_id;

$pastaInterna = isset($_REQUEST['pastaInterna'])?$_REQUEST['pastaInterna']:1;
$search = isset($_REQUEST['search'])?$_REQUEST['search']:null;
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "E-mail enviado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
include_once 'model/emailAnexoClass.php';

$retorno = $ec->listaEmail($pastaInterna,$_SESSION['seqCadast']);
$totalEmails=0;
$totalEmailsNovos=0;
if($retorno)
{
	$totalEmails = count($resultado);
}
if($retorno)
{
	foreach($retorno as $vetor)
	{

		if($vetor['nova']==1)
		{
			$totalEmailsNovos++;
		}
	}
}


?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>E-mail Interno</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaEmail">E-mail Interno</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="?corpo=comporEmail">Escrever e-mail</a>
                            <div class="space-25"></div>
                            <h5>Pastas</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li><a href="?corpo=buscaEmail"> <i class="fa fa-inbox "></i> Caixa de Entrada <?php if($totalEmailsNovos>0){?><span class="label label-warning pull-right"><?php echo $totalEmailsNovos;?></span><?php }?> </a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=2"> <i class="fa fa-envelope-o"></i> E-mails enviados</a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=3"> <i class="fa fa-exclamation-circle"></i> Importante</a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=4"> <i class="fa fa-trash-o"></i> Lixo</a></li>
                            </ul>
                            
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">

                <div class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" id="search" name="search" value="<?php if($search!=null){ echo $search;}?>" onkeypress="pesquisarEmailAoDigitar(event)" placeholder="Procurar no e-mail">
                        <div class="input-group-btn">
                            <a class="btn btn-sm btn-primary" onclick="pesquisarEmail();">
                                Procurar
                            </a>
                        </div>
                    </div>
                </div>
                
                <h2>
                    <?php 
                    switch ($pastaInterna)
                    {
                    	case 1:
                    		echo "Caixa de Entrada";
                    		break;
                    	case 2:
                    		echo "E-mails enviados";
                    		break;
                    	case 3:
                    		echo "Importante";
                    		break;
                    	case 4:
                    		echo "Lixo";
                    		break;
                    }
                    ?> 
                    (<?php echo $totalEmailsNovos;?>)
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="btn-group pull-right">
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>
                    </div>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Atualizar e-mails" onclick="location.href='?corpo=buscaEmail'"><i class="fa fa-refresh"></i> Atualizar</button>
                    <?php if($pastaInterna==1){?>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Marcar como lido" onclick="marcarMensagemComoLida(<?php echo $_SESSION['seqCadast']?>);"><i class="fa fa-eye"></i> </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Marcar como importante" onclick="marcarMensagemComoImportante(<?php echo $_SESSION['seqCadast']?>);"><i class="fa fa-exclamation"></i> </button>
                    <?php }?>
                    <?php if($pastaInterna!=4){?>
                    	<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mover para a lixeira" onclick="moverMensagemParaLixeira(<?php echo $_SESSION['seqCadast']?>,'<?php echo $pastaInterna;?>');"><i class="fa fa-trash-o"></i> </button>
                    <?php }else{?>
                    	<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Excluir definitivamente" onclick="excluirEmailDefinitivamente(<?php echo $_SESSION['seqCadast']?>,'<?php echo $pastaInterna;?>');"><i class="fa fa-trash-o"></i> </button>
                    <?php }?>
                    

                </div>
            </div>
                <div class="mail-box">

                <table class="table table-hover table-mail">
                <tbody>
                <?php 
                $ec = new emailController();
				$retorno = $ec->listaEmail($pastaInterna,$_SESSION['seqCadast'],$search);
                if($retorno)
                {
	                foreach($retorno as $vetor)
	                {
	                	
	                	if($vetor['nova']==1)
	                	{
	                		
			                ?>
			                <tr class="unread">
			                    <td class="check-mail">
			                        <input type="checkbox" class="i-checks" name="emails[]" id="<?php echo $vetor['idEmail'];?>" value="<?php echo $vetor['idEmail'];?>">
			                    </td>
			                    <td class="mail-ontact">
			                    	<a href="?corpo=emailDetalhe&idEmail=<?php echo $vetor['idEmail'];?>&vinculoEmailEnviado=<?php echo $vetor['vinculoEmailEnviado'];?>">
			                    		<?php
			                    			if($pastaInterna!=2)
			                    			{ 
			                    				echo $vetor['nomeUsuario'];
			                    			}else{
			                    				
			                    				$destinatarios="";
			                    				$edp = new EmailDePara();
			                    				$retorno5 = $edp->selecionarDestinatarios($vetor['vinculoEmailEnviado']);
			                    				$i=0;
									            if($retorno5)
						                        {
						                        	foreach($retorno5 as $vetor5)
						                        	{
						                        		$u = new Usuario();
						                        		$retorno6 = $u->buscarIdUsuario($vetor5['para']);
						                        		if($retorno6)
						                        		{
						                        			
						                        			foreach($retorno6 as $vetor6)
						                        			{
						                        				if($i==0)
						                        				{
						                        					$destinatarios = $vetor6['nomeUsuario'];
						                        				}else{
						                        					if($i==1)
						                        					{
						                        						$destinatarios .= "...";
						                        					}
						                        				}
						                        				$i++;
						                        			}
						                        		}
						                        	}
						                        }
						                        echo $destinatarios;
						                        		
						                      
			                    			}
			                    		?>
			                    	</a>
			                    </td>
			                    <td class="mail-subject"><a href="?corpo=emailDetalhe&idEmail=<?php echo $vetor['idEmail'];?>&vinculoEmailEnviado=<?php echo $vetor['vinculoEmailEnviado'];?>"><?php echo $vetor['assunto'];?></a></td>
			                    <td class="">
			                    <?php 
			                    $ea = new emailAnexo();
			                    $retorno3 = $ea->listaEmailAnexo($vetor['idEmail']);
			                    if($retorno3)
			                    {
			                    ?>
			                    <i class="fa fa-paperclip"></i>
			                    <?php 
			                    }
			                    ?>
			                    </td>
			                    <td class="text-right mail-date"><?php echo substr($vetor['dataCad'],8,2)."/".substr($vetor['dataCad'],5,2)."/".substr($vetor['dataCad'],0,4);?> <?php echo substr($vetor['dataCad'],11,8);?></td>
			                </tr>
			            <?php 
	                	}else{
			            ?>
			                <tr class="read">
			                    <td class="check-mail">
			                        <input type="checkbox" class="i-checks" name="emails[]" id="<?php echo $vetor['idEmail'];?>" value="<?php echo $vetor['idEmail'];?>">
			                    </td>
			                    <td class="mail-ontact">
			                    	<a href="?corpo=emailDetalhe&idEmail=<?php echo $vetor['idEmail'];?>&vinculoEmailEnviado=<?php echo $vetor['vinculoEmailEnviado'];?>">
			                    		<?php 
			                    			if($pastaInterna!=2)
			                    			{ 
			                    				echo $vetor['nomeUsuario'];
			                    			}else{
			                    				$destinatarios="";
			                    				$edp = new EmailDePara();
			                    				$retorno5 = $edp->selecionarDestinatarios($vetor['vinculoEmailEnviado']);
			                    				$i=0;
									            if($retorno5)
						                        {
						                        	foreach($retorno5 as $vetor5)
						                        	{
						                        		$u = new Usuario();
						                        		$retorno6 = $u->buscarIdUsuario($vetor5['para']);
						                        		if($retorno6)
						                        		{
						                        			
						                        			foreach($retorno6 as $vetor6)
						                        			{
						                        				if($i==0)
						                        				{
						                        					$destinatarios = $vetor6['nomeUsuario'];
						                        				}else{
						                        					if($i==1)
						                        					{
						                        						$destinatarios .= "...";
						                        					}
						                        				}
						                        				$i++;
						                        			}
						                        		}
						                        	}
						                        }
						                        echo $destinatarios;
			                    			}
			                    		?>
			                    	</a>
			                    </td>
			                    <td class="mail-subject"><a href="?corpo=emailDetalhe&idEmail=<?php echo $vetor['idEmail'];?>&vinculoEmailEnviado=<?php echo $vetor['vinculoEmailEnviado'];?>"><?php echo $vetor['assunto'];?></a></td>
			                    <td class="">
			                    <?php 
			                    $ea = new emailAnexo();
			                    $retorno3 = $ea->listaEmailAnexo($vetor['idEmail']);
			                    if($retorno3)
			                    {
			                    ?>
			                    <i class="fa fa-paperclip"></i>
			                    <?php 
			                    }
			                    ?>
			                    </td>
			                    <td class="text-right mail-date"><?php echo substr($vetor['dataCad'],8,2)."/".substr($vetor['dataCad'],5,2)."/".substr($vetor['dataCad'],0,4);?> <?php echo substr($vetor['dataCad'],11,8);?></td>
			                </tr>
			            <?php 
	                	}
	                }
                }
			            ?>
                </tbody>
                </table>


                </div>
            </div>
        </div>
        </div>