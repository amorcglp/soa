<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Columbas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeIniciaticaColumba">Columbas Cadastradas</a>
            </li>
            <li class="active">
                <a>Cadastro de Columba</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Columba</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciaticaColumba">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" onsubmit="return verificaCamposPreenchidosColumbaSubmit();" enctype="multipart/form-data" method="post" action="acoes/acaoCadastrar.php">
                        <input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="10000000" />
                        <div class="form-group form-inline"><label class="col-sm-3 control-label">Organismo Afiliado: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-5 chosen-select" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." style="max-width: 350px" required="required">
                                    <option value="0">Selecione...</option>
                                    <?php
                                        include_once 'controller/organismoController.php';
                                        require_once("model/criaSessaoClass.php");
                                        $oc = new organismoController();
                                        $sessao = new criaSessao();

                                        if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){
                                            $oc->criarComboBox();
                                        } else {
                                            $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                        }
                                    ?>
                                	</select>
                            </div>
                        </div>

                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Columba:</label>
                            <div class="col-sm-9">
                                <input class="form-control" name="codAfiliacaoAtividadeIniciaticaColumba" id="codAfiliacaoAtividadeIniciaticaColumba" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" name="nomeAtividadeIniciaticaColumba" id="nomeAtividadeIniciaticaColumba" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="retornaDadosMembroColumba('codAfiliacaoAtividadeIniciaticaColumba', 'nomeAtividadeIniciaticaColumba');">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoAtividadeIniciaticaColumba','nomeAtividadeIniciaticaColumba','codAfiliacaoAtividadeIniciaticaColumba','nomeAtividadeIniciaticaColumba','h_seqCadastColumba','h_nomeColumba','myModalPesquisa','informacoesPesquisa','S');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br> Digite o código de afiliação da columba e no outro campo acima o primeiro nome e pressione TAB
                            </div>
                        </div>
                        <input type="hidden" name="h_seqCadastColumba" id="h_seqCadastColumba">
                        <input type="hidden" name="h_nomeColumba" id="h_nomeColumba">
                        <input type="hidden" name="tipo" id="tipo" value="2">

                        <div class="form-group" id="anotacoesAtivInic">
                            <label class="col-sm-3 control-label">Descrição: </label>
                            <div class="col-sm-9">
                                <div style="border: #ccc solid 1px; max-width: 800px">
                                    <div class="mail-text h-200">
                                        <textarea class="summernote" id="descricaoAtividadeIniciaticaColumba" name="descricaoAtividadeIniciaticaColumba"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Endereço: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" name="enderecoAtividadeIniciaticaColumba" id="enderecoAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CEP: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" name="cepAtividadeIniciaticaColumba" id="cepAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Naturalidade: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" name="naturalidadeAtividadeIniciaticaColumba" id="naturalidadeAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_atividade_iniciatica">
                            <label class="col-sm-3 control-label">Data de Nascimento: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input onkeypress="valida_data(this,'dataNascimentoAtividadeIniciaticaColumba')" maxlength="10" name="dataNascimentoAtividadeIniciaticaColumba" id="dataNascimentoAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataNascimentoAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Escola que frequenta: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" name="escolaAtividadeIniciaticaColumba" id="escolaAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" name="emailAtividadeIniciaticaColumba" id="emailAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Endereço de Facebook, blog ou site pessoal(opcional): </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" name="redeSocialAtividadeIniciaticaColumba" id="redeSocialAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group datapicker">
                            <label class="col-sm-3 control-label">Data de Admissão: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input onkeypress="valida_data(this,'dataAdmissaoAtividadeIniciaticaColumba')" maxlength="10" name="dataAdmissaoAtividadeIniciaticaColumba" id="dataAdmissaoAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataAdmissaoAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group datapicker">
                            <label class="col-sm-3 control-label">Data de Instalação: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input onkeypress="valida_data(this,'dataInstalacaoAtividadeIniciaticaColumba')" maxlength="10" name="dataInstalacaoAtividadeIniciaticaColumba" id="dataInstalacaoAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataInstalacaoAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <div class="form-group datapicker">
                            <label class="col-sm-3 control-label">Ativa até: </label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input onkeypress="valida_data(this,'dataAtivaAteAtividadeIniciaticaColumba')" maxlength="10" name="dataAtivaAteAtividadeIniciaticaColumba" id="dataAtivaAteAtividadeIniciaticaColumba" type="text" class="form-control" style="max-width: 105px" required="required">
                                <div hidden id="data_invalida_dataAtivaAteAtividadeIniciaticaColumba" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                            </div>
                        </div>
                        <hr>
                        -->
                        <!--
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Pai ou tutor: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoTutorAtividadeIniciaticaColumba" name="codAfiliacaoTutorAtividadeIniciaticaColumba" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome Completo" class="form-control" id="nomeTutorAtividadeIniciaticaColumba" name="nomeTutorAtividadeIniciaticaColumba" type="text" maxlength="100" value="" style="min-width: 320px">
                                -->
                                <!--<input type="checkbox" value="" name="companheiroTutorAtividadeIniciaticaColumba" id="companheiroTutorAtividadeIniciaticaColumba" > Companheiro -->
                                <!--
                                <small>Por favor, caso preencha esse campo informe o nome completo</small>
                            </div>
                        </div>
                        -->
                        <!--
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Mãe ou tutora: </label>
                            <div class="col-sm-9">
                                <input placeholder="Código" class="form-control" id="codAfiliacaoTutoraAtividadeIniciaticaColumba" name="codAfiliacaoTutoraAtividadeIniciaticaColumba" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input placeholder="Nome Completo" class="form-control" id="nomeTutoraAtividadeIniciaticaColumba" name="nomeTutoraAtividadeIniciaticaColumba" type="text" maxlength="100" value="" style="min-width: 320px">
                                -->
                                <!-- <input type="checkbox" value="" name="companheiroTutoraAtividadeIniciaticaColumba" id="companheiroTutoraAtividadeIniciaticaColumba" > Companheiro -->
                                <!--
                                <small>Por favor, caso preencha esse campo informe o nome completo</small>
                            </div>
                        </div>
                        -->
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Qual dos dois é responsável pleno? </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="responsavelPlenoAtividadeIniciaticaColumba" id="responsavelPlenoAtividadeIniciaticaColumba" style="max-width: 320px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">Pai ou tutor</option>
                                    <option value="2">Mãe ou tutora</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="seqCadastResponsavelPlenoAtividadeIniciaticaColumba" name="seqCadastResponsavelPlenoAtividadeIniciaticaColumba" value="">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail do Responsável Legal: </label>
                            <div class="col-sm-9">
                                <input type="text" maxlength="149" name="emailResponsavelAtividadeIniciaticaColumba" id="emailResponsavelAtividadeIniciaticaColumba" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-inline" id="columbaRgAnexo">
                            <label class="col-sm-3 control-label">Anexar RG(caso possua): </label>
                            <div class="col-sm-9">
                                <input name="RgAnexoAtividadeIniciaticaColumba" id="RgAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="columbaRgAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <div class="form-group form-inline" id="columbaCertidaoNascimentoAnexo">
                            <label class="col-sm-3 control-label">Anexar Certidão de Nascimento: </label>
                            <div class="col-sm-9">
                                <input name="certidaoNascimentoAnexoAtividadeIniciaticaColumba" id="certidaoNascimentoAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="columbaCertidaoNascimentoAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <div class="form-group form-inline" id="fotoCorpoInteirAnexo">
                            <label class="col-sm-3 control-label">Anexar Foto de Corpo de Inteiro: </label>
                            <div class="col-sm-9">
                                <input name="fotoCorpoInteiroAnexoAtividadeIniciaticaColumba" id="fotoCorpoInteiroAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="fotoCorpoInteirAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <div class="form-group form-inline" id="cartaoMembroResponsavelAnexo">
                            <label class="col-sm-3 control-label">Cartão de Membro do Responsável: </label>
                            <div class="col-sm-9">
                                <input name="cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba" id="cartaoMembroResponsavelAnexoAtividadeIniciaticaColumba" type="file" />
                            </div>
                            <div id="cartaoMembroResponsavelAnexoAlerta" class="col-sm-9" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <hr>
                        -->
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar"" />
                                <a class="btn btn-white" href="?corpo=buscaAtividadeIniciaticaColumba" id="cancelar" name="cancelar">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo de Modal Início -->
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Conteúdo de Modal Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>		