<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Auxiliar Web</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAuxiliarWeb">Cadastros</a>
            </li>
            <li class="active">
                <strong><a>Auxiliar Web</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de criação do Termo de Compromisso</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAuxiliarWeb">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaAuxiliarWeb()" >
                        <input type="hidden" name="idAuxiliarWeb" id="idAuxiliarWeb" value="1">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data do Termo de Compromisso:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataTermoCompromisso" maxlength="10" id="dataTermoCompromisso" type="text" class="form-control" style="max-width: 102px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Afiliação: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="codigoAfiliacao" name="codigoAfiliacao" type="text" value="" style="max-width: 150px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nome do Auxiliar: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" onblur="retornaDadosMembroNoAuxiliarWeb()" id="nome" name="nome" type="text" value="" style="max-width: 350px" required="required">
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CPF: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="cpf" name="cpf" type="text" value="" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="email" name="email" type="text" value="" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Rua: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="logradouro" name="logradouro" type="text" value="" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="numero" name="numero" type="text" value="" style="max-width: 143px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Complemento: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="complemento" name="complemento" type="text" value="" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Bairro: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="bairro" name="bairro" type="text" value="" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cep: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="cep" name="cep" type="text" value="" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cidade: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="cidade" name="cidade" type="text" value="" style="max-width: 350px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">UF: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" id="uf" name="uf" type="text" value="" style="max-width: 70px">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label">País</label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="pais" id="pais" style="max-width: 150px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">Brasil</option>
                                    <option value="2">Portugal</option>
                                    <option value="3">Angola</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telefone Residencial: </label>
                            <div class="col-sm-9">
                                <input class="form-control telefone" maxlength="100" id="telefoneResidencial" name="telefoneResidencial" type="text" value="" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telefone Comercial: </label>
                            <div class="col-sm-9">
                                <input class="form-control telefone" maxlength="100" id="telefoneComercial" name="telefoneComercial" type="text" value="" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Celular: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="100" id="celular" name="celular" type="tel" value="" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaAuxiliarWeb" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	