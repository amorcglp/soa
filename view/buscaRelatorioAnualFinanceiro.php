<?php
include_once("../lib/functions.php");
@usuarioOnline();

include_once('model/relatorioFinanceiroAnualClass.php');
include_once('model/usuarioClass.php');
include_once('model/notificacaoGlpClass.php');

$rfa        = new RelatorioFinanceiroAnual();
$usu        = new Usuario();
$u          = new NotificacaoGlp();

//$anoAtual                 = isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');
$mesAtual                   = date('m');

if(isset($_REQUEST['anoAtual']))
{
    $anoAtual = $_REQUEST['anoAtual'];
}else{

    if(isset($_SESSION['anoAtualRelatorioFinanceiroAnual']))
    {
        $anoAtual = $_SESSION['anoAtualRelatorioFinanceiroAnual'];
    }else{
        $anoAtual = date('Y');
        $_SESSION['anoAtualRelatorioFinanceiroAnual'] = $anoAtual;
    }
}
//echo "anoatual=>".$anoAtual;
$_SESSION['anoAtualRelatorioFinanceiroAnual'] = $anoAtual;

//$idOrganismoAfiliado  = isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;
$usuario                = isset($_REQUEST['usuario'])?$_REQUEST['usuario']:null;
$anexo                  = isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;
/**
 * Upload do Relatório Assinado
 */
$relatorio=0;
$extensaoNaoValida=0;
if($anexo != "")
{
    //Verificar se já está cadastrado
    $rfa->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
    $rfa->setAno($anoAtual);
    $rfa->setCaminhoRelatorioFinanceiroAnual('');
    $rfa->setUsuario($usuario);

    $idRelatorioFinanceiro = $rfa->cadastraRelatorioFinanceiroAnual();

    if ($_FILES['anexo']['name'] != "") {
        if(substr_count($_FILES['anexo']['name'],".")==1)
        {
            $extensao = strstr($_FILES['anexo']['name'], '.');
            if($extensao==".pdf"||
                    $extensao==".png"||
                    $extensao==".jpg"||
                    $extensao==".jpeg"||
                    $extensao==".PDF"||
                    $extensao==".PNG"||
                    $extensao==".JPG"||
                    $extensao==".JPEG"
                    )
            {
                $caminho = "img/relatorio_financeiro_anual/" . basename($idRelatorioFinanceiro.".relatorio.financeiro.anual.".$anoAtual.$extensao);
                //Guardar Link do Caminho no Banco de Dados
                
                //echo "caminho: " . $caminho;
                
                $rfa->setCaminhoRelatorioFinanceiroAnual($caminho);
                if($rfa->atualizaCaminhoRelatorioFinanceiroAnualAssinado($idRelatorioFinanceiro))
                {
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/relatorio_financeiro_anual/';
                    //$uploaddir = 'C:\/wamp\/www\/soa_mvc\/img\/relatorio_financeiro_anual\/';
                    $uploadfile = $uploaddir . basename($idRelatorioFinanceiro.".relatorio.financeiro.anual.".$anoAtual.$extensao);
                    if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                    {
                        $relatorio = 1;
                        /**
                         * Notificar GLP que o relatório assinado foi inserido
                         */
                        date_default_timezone_set('America/Sao_Paulo');
                        $tituloNotificacao="Novo Relatorio Financeiro Anual Entregue";
                        $tipoNotificacao=6;//Entregue
                        $remetenteNotificacao=$sessao->getValue("seqCadast");
                        $mensagemNotificacao="Relatorio Financeiro Anual Entregue pelo Organismo ".$_POST['nomeOrganismo'].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuario'];

                        //Selecionar todos os usuários com departamento 2 e 3
                        $resultadoUsuarios = $usu->listaUsuario(null,"2,3");
                        $arrOficiais=array();
                        if($resultadoUsuarios)
                        {
                            foreach ($resultadoUsuarios as $vetor)
                            {
                                $arrOficiais[] = $vetor['idUsuario'];
                            }
                        }

                        $resultado = $u->cadastroNotificacao(utf8_encode($tituloNotificacao),utf8_encode($mensagemNotificacao),$tipoNotificacao,$remetenteNotificacao);

                        if ($resultado==true) {
                            for ($i=0; $i<count($arrOficiais); $i++){
                                $ultimoId = $u->selecionaUltimoId();
                                $resultadoOficiais = $u->cadastroNotificacaoGlp($ultimoId,$arrOficiais[$i]);
                            }
                        }
                    }else{
                        echo "<script type='text/javascript'>alert('Erro ao enviar o relatório assinado!');</script>";
                    }
                }
            }else{
                $extensaoNaoValida=1;
            }
        }else{
            $extensaoNaoValida=2;
        }
    }
}

if($relatorio==1){?>
<script>
window.onload = function(){
    swal({
        title: "Sucesso!",
        text: "Relatório Assinado Enviado com Sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($extensaoNaoValida==1){?>
<script>
window.onload = function(){
    swal({
        title: "Aviso!",
        text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
if ($extensaoNaoValida == 2) {
?>
<script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
</script>
<?php }
/*
 * Verificar se já foi feito o upload do relatório assinado
 */

$caminhoRelatorioFinanceiroAnual="";
if($rfa->verificaSeJaCadastrado($anoAtual,$idOrganismoAfiliado))
{
    $resultado = $rfa->listaRelatorioFinanceiroAnual($anoAtual,$idOrganismoAfiliado);
    if($resultado)
    {
        foreach($resultado as $vetor)
        {
            $caminhoRelatorioFinanceiroAnual = $vetor['caminhoRelatorioFinanceiroAnual'];
        }
    }
}

?>
<?php
include_once('model/recebimentoClass.php');
include_once('model/despesaClass.php');
include_once("model/rendimentoClass.php");
include_once('model/membrosRosacruzesAtivosClass.php');
include_once("lib/functions.php");


/****
 * Montar navegação por anos
 *****/

/*
 * Montar anterior e próximo
 */
$tirarBotao=false;
$anoProximo=$anoAtual-1; //realizei alteração aqui para aparecer o ano de 2016
if($anoAtual==date('Y'))
{
    $tirarBotao=true;
}else{    
    $anoAnterior=$anoAtual+1; //realizei alteração aqui para aparecer o ano de 2016
}




$r = new Recebimento();
$d = new Despesa();
$rendimento = new Rendimento();
$membrosRosacruzesAtivos = new MembrosRosacruzesAtivos();
//echo "idOrganismo=>".$idOrganismoAfiliado;
/*
 * Calcular saldo dos anos anteriores
 */

//Encontrar Saldo e Data Inicial
include_once('model/saldoInicialClass.php');
$si = new saldoInicial();
$mesSaldoInicial = "01";
$anoSaldoInicial = "2016";
$saldoInicial=0;
$temSaldoInicial=0;
$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if($resultado)
{
    foreach($resultado as $vetor)
    {
        $mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
        $anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
        $saldoInicial = (float) str_replace(",",".",str_replace(".","",$vetor['saldoInicial']));
        $temSaldoInicial=1;
    }
}
/*
//Encontrar Saldo dos Meses Anteriores
$mes=intval($mesSaldoInicial);
$ano=intval($anoSaldoInicial);
$saldoMesesAnteriores=0;
$saldoGeral=0;

//echo "<br>mesSaldoInicial=>".$mes;
//echo "<br>mesAtual=>".$mesAtual;
//echo "<br>anoSaldoInicial=>".$ano;
//echo "<br>anoAtual=>".$anoAtual;

$y=0;
if($anoAtual==$anoSaldoInicial)
{
		$saldoGeral = $saldoInicial;
		$saldoAnosAnteriores = $saldoGeral;
}else{
    $i=0;
    while($ano<$anoAtual)
    {
        if($mes<=12)
        {
                if($mes==$mesSaldoInicial&&$ano==$anoSaldoInicial)
                {
                        $saldoAnosAnteriores = $saldoInicial;
                }
                //echo "i:".$i."-mes".$mes."<br>";
                //echo "i:".$i."-ano".$ano."<br>";
                //echo "saldoDoMesAnteriorParaSerCalculado:".$saldoMesesAnteriores."<br>";
                $saldoGeral = retornaSaldo($r,$d,$mes,$ano,$idOrganismoAfiliado,$saldoAnosAnteriores);
                $saldoAnosAnteriores = $saldoGeral;
                //echo "saldoGeral".$saldoAnosAnteriores."<br>";
                $mes++;	
        }else{
                $ano++;
                $mes=1;
        }	
			
        if($ano>=$anoAtual&&$mes>=$mesAtual)
        {
            break;
        }            
	$i++;
    }
}
*/
//echo "SALDO ANOS ANTERIORES=>".$saldoAnosAnteriores;

/*
 * Gráfico Financeiro Anual
 */
$meses = 12;
$arrRecebimentos=array();
for($i=1;$i<=$meses;$i++)
{
    if($r->retornaEntrada($i,$anoAtual,$idOrganismoAfiliado)!="")
    {
        $arrRecebimentos['total'][$i] = number_format($r->retornaEntrada($i,$anoAtual,$idOrganismoAfiliado), 2, '.', '');;
    }else{
        $arrRecebimentos['total'][$i] = 0;
    }
}

$arrDespesas=array();
for($i=1;$i<=$meses;$i++)
{
    if($d->retornaSaida($i,$anoAtual,$idOrganismoAfiliado)!="")
    {
        $arrDespesas['total'][$i] = number_format($d->retornaSaida($i,$anoAtual,$idOrganismoAfiliado), 2, '.', '');;
    }else{
        $arrDespesas['total'][$i] = 0;
    }
}

include_once('model/membrosRosacruzesAtivosClass.php');
include_once('lib/functions.php');

$mra = new MembrosRosacruzesAtivos();

//Dados do Gráfico Membros Ativos
$arrMembrosAtivos = array();
for($i=1;$i<=12;$i++)
{
    if($mra->retornaMembrosRosacruzesAtivos($i,$anoAtual,$idOrganismoAfiliado)!="")
    {
        $arrM = $mra->retornaMembrosRosacruzesAtivos($i,$anoAtual,$idOrganismoAfiliado);
        $arrMembrosAtivos['d1'][$i] = $arrM['numeroAtualMembrosAtivos'];
    }else{
        $arrMembrosAtivos['d1'][$i] = 0;
    }
}
/*
for($i=1;$i<=12;$i++)
{
$arrMembrosAtivos['d1'][$i] = 0;
}
 */

?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Meses');
      data.addColumn('number', 'Recebimentos');
      data.addColumn('number', 'Despesas');

      data.addRows([
    <?php
    for($i=1;$i<=$meses;$i++)
    {
    ?>
        <?php if($i==1){?>
            ['<?php echo mesExtensoPortugues($i);?>',<?php echo $arrRecebimentos['total'][$i];?>,<?php echo $arrDespesas['total'][$i];?>]
        <?php }else{?>
            ,['<?php echo mesExtensoPortugues($i);?>',<?php echo $arrRecebimentos['total'][$i];?>,<?php echo $arrDespesas['total'][$i];?>]
        <?php }
        ?>
    <?php }?>
      ]);

      var options = {
                hAxis: {
                  title: 'Tempo'
                },
                vAxis: {
                  title: 'Entradas/Saidas'
                }
              };

              var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

              chart.draw(data, options);
            }
</script>
<script type="text/javascript">
google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Meses');
      data.addColumn('number', 'Nº de Membros');


      data.addRows([
    <?php
    for($i=1;$i<=$meses;$i++)
    {
    ?>
        <?php if($i==1){?>
            ['<?php echo mesExtensoPortugues($i);?>',<?php echo $arrMembrosAtivos['d1'][$i];?>]
        <?php }else{?>
            ,['<?php echo mesExtensoPortugues($i);?>',<?php echo $arrMembrosAtivos['d1'][$i];?>]
        <?php }
        ?>
    <?php }?>
      ]);

      var options = {
                hAxis: {
                  title: 'Tempo'
                },
                vAxis: {
                  title: 'Nº de Membros Ativos'
                }
              };

              var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));

              chart.draw(data, options);
            }
</script>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório Financeiro Anual</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Financeiro</a>
            </li>
            <li class="active">
                <strong>Relatório Financeiro Anual</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Relatório Financeiro Anual</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row"></div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="widget lazur-bg p-xl">
                                    <ul class="list-unstyled m-t-md">
                                        <h2>
                                            <i class="fa fa-exclamation-triangle"></i>Avisos
                                        </h2>
                                        <?php
                                        $k=0;
                                        for($i=1;$i<=12;$i++)
                                        {
                                            if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                $totalRecebimentos = $r->retornaEntrada($i, $anoAtual, $idOrganismoAfiliado);
                                                if ($totalRecebimentos == 0) {
                                                    $k++;
                                                }
                                            }
                                        }
                                        if($k>0)
                                        {
                                        ?>
                                        <br />
                                        <li>
                                            <span class="fa fa-thumbs-down"></span>Não houve 
                                            <label>RECEBIMENTOS</label>para o(s) mes(es) de
                                            <?php
                                            for($i=1;$i<=12;$i++)
                                            {
                                                $totalRecebimentos = $r->retornaEntrada($i,$anoAtual,$idOrganismoAfiliado);
                                                if($totalRecebimentos==0)
                                                {
                                                    if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                        ?>
                                                        <b>
                                                            <?php echo mesExtensoPortugues($i); ?>
                                                        </b>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                            !
                                        </li>
                                        <?php
                                        }
                                        $l=0;
                                        for($i=1;$i<=12;$i++)
                                        {
                                            if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                $totalDespesas = $d->retornaSaida($i, $anoAtual, $idOrganismoAfiliado);
                                                if ($totalDespesas == 0) {
                                                    $l++;
                                                }
                                            }
                                        }
                                        if($l>0)
                                        {
                                        ?>
                                        <br />
                                        <li>
                                            <span class="fa fa-thumbs-down"></span>Não houve 
                                            <label>DESPESAS</label>para o(s) mes(es) de
                                            <?php
                                            for($i=1;$i<=12;$i++)
                                            {
                                            ?>
                                            <?php
                                                $totalDespesas = $d->retornaSaida($i,$anoAtual,$idOrganismoAfiliado);
                                                if($totalDespesas==0)
                                                {
                                                    if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                        ?>
                                                        <b>
                                                            <?php echo mesExtensoPortugues($i); ?>
                                                        </b>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            <?php
                                            }
                                            ?>
                                            !
                                        </li>
                                        <?php
                                        }
                                        $m=0;
                                        for($i=1;$i<=12;$i++)
                                        {
                                            if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                $resultado = $rendimento->listaRendimento($i, $anoAtual, $idOrganismoAfiliado);
                                                if (!$resultado) {
                                                    $m++;
                                                }
                                            }
                                        }
                                        if($m>0)
                                        {
                                        ?>
                                        <br />
                                        <li>
                                            <span class="fa fa-thumbs-down"></span>Não houve 
                                            <label>SALDO</label>para o(s) mes(es) de
                                            <?php
                                            for($i=1;$i<=12;$i++)
                                            {
                                                $resultado = $rendimento->listaRendimento($i,$anoAtual,$idOrganismoAfiliado);
                                                if(!$resultado)
                                                {
                                                    if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                        ?>
                                                        <b>
                                                            <?php echo mesExtensoPortugues($i); ?>
                                                        </b>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                            <?php
                                            }
                                            ?>
                                            !
                                        </li>
                                        <?php
                                        }
                                        $n=0;
                                        for($i=1;$i<=12;$i++)
                                        {
                                            if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                $resultado = $membrosRosacruzesAtivos->retornaMembrosRosacruzesAtivos($i, $anoAtual, $idOrganismoAfiliado);
                                                if ($resultado['idMembrosRosacruzesAtivos'] == "") {
                                                    $n++;
                                                }
                                            }
                                        }
                                        if($n>0)
                                        {
                                        ?>
                                        <br />
                                        <li>
                                            <span class="fa fa-thumbs-down"></span>Não houve nenhuma entrada nos
                                            <label>MEMBROS ATIVOS OA</label>para o(s) mes(es) de
                                            <?php
                                            for($i=1;$i<=12;$i++)
                                            {
                                                $resultado = $membrosRosacruzesAtivos->retornaMembrosRosacruzesAtivos($i,$anoAtual,$idOrganismoAfiliado);
                                                if($resultado['idMembrosRosacruzesAtivos']=="")
                                                {
                                                    if($i>=$mesSaldoInicial&&$anoAtual>=$anoSaldoInicial) {
                                                        ?>
                                                        <b>
                                                            <?php echo mesExtensoPortugues($i); ?>
                                                        </b>
                                                        <?php
                                                    }
                                                }

                                            }
                                            ?>
                                            !
                                        </li>
                                        <?php
                                        }
                                        if($k==0&&$l==0&&$m==0&&$n==0)
                                        {
                                        ?>
                                        <br />
                                        <li>
                                            <span class="fa fa-thumbs-up"></span>Foram realizadas
                                            <label>todas as entradas</label>para a geração do
                                            <label>Relatório Anual</label>.
                                            <label>Parabéns</label>!
                                        </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            
                                            <?php if(!$tirarBotao){ ?>
                                            <a class="btn btn-info" href="?corpo=buscaRelatorioAnualFinanceiro&anoAtual=<?php echo $anoAnterior;?>">
                                                <i class="fa fa-calendar-o"></i><?php echo $mesAnteriorTexto;?><?php echo $anoAnterior;?>
                                            </a>
                                            <?php }?>
                                            <a class="btn btn-info">
                                                <b>
                                                    <i class="fa fa-calendar-o"></i><?php echo $mesAtualTexto;?><?php echo $anoAtual;?>
                                                </b>
                                            </a>
                                            <a class="btn btn-info" href="?corpo=buscaRelatorioAnualFinanceiro&anoAtual=<?php echo $anoProximo;?>">
                                                <i class="fa fa-calendar-o"></i><?php echo $mesProximoTexto;?><?php echo $anoProximo;?>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <?php // echo "saldoAnosAnteriores: " . $saldoAnosAnteriores;?>
                                            <?php //echo "================================<pre>";print_r($arrNivelUsuario);?>
                                            <a href="#"
                                                class="btn btn-info  dim btn-large-dim btn-outline"
                                                onclick="abrirPopupRelatorioFinanceiroAnual('<?php echo $anoAtual;?>','<?php echo $idOrganismoAfiliado;?>','<?php echo $saldoAnosAnteriores;?>');">
                                                <i
                                                    class="fa fa-print" value="Impressão"></i>
                                            </a>
                                            <?php
                                            if($temSaldoInicial==1){
                                            if($n==0)
                                            {
                                                if(in_array("4",$arrNivelUsuario)){?>
                                            &nbsp;
                                            <a href="#"
                                                class="btn btn-info  dim btn-large-dim btn-outline"
                                                data-target="#mySeeUpload" data-toggle="modal">
                                                <i class="fa fa-cloud-upload"></i>
                                            </a>
                                            <?php }?>
                                            &nbsp;
                                            <a href="#" target="_blank"
                                                class="btn btn-info  dim btn-large-dim btn-outline"
                                                onclick="listaUploadRelatorioFinanceiroAnual('<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>')"
                                                data-target="#mySeeListaUpload" data-toggle="modal">
                                                <i class="fa fa-paste"></i>
                                            </a>                                         
                                            <?php }?>
                                            <?php }else{?>
                                            <br><br><div class="alert alert-danger">
                                            Como o Organismo não cadastrou o <a class="alert-link" href="#">Saldo Inicial</a>, não são liberados os botões para upload de relatórios assinados.
                                            </div>
                                            <?php }?>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Gráfico Financeiro Anual</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div id="chart_div"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Gráfico de Membros Ativos no OA</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div id="chart_div2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Relatório Financeiro Anual Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="relatorioFinanceiro" name="relatorioFinanceiro" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <!--  
                        <div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> é preciso enviar o relatório original para GLP por correio, mesmo fazendo o upload do relatório assinado. Caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
                        <div class="col-sm-10">
                            <input name="anexo" id="anexo" type="file" />
                        </div>
                        <div id="anexoAlerta" class="col-sm-10"
                            style="color: #4F5B93; font-weight: normal; margin-top: 7px;">
                            &nbsp;&nbsp;&nbsp;Tamanho
                            máximo de 10MB.
                        </div>

                    </div>
                    <?php
                    include_once 'model/organismoClass.php';
                    $o = new organismo();
                    $nomeOrganismo="";
                    $resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
                    if($resultado2)
                    {
                        foreach($resultado2 as $vetor2)
                        {
                            switch($vetor2['classificacaoOrganismoAfiliado']){
                                case 1:
                                    $classificacao =  "Loja";
                                    break;
                                case 2:
                                    $classificacao =  "Pronaos";
                                    break;
                                case 3:
                                    $classificacao =  "Capítulo";
                                    break;
                                case 4:
                                    $classificacao =  "Heptada";
                                    break;
                                case 5:
                                    $classificacao =  "Atrium";
                                    break;
                            }
                            switch($vetor2['tipoOrganismoAfiliado']){
                                case 1:
                                    $tipo = "R+C";
                                    break;
                                case 2:
                                    $tipo = "TOM";
                                    break;
                            }

                            $nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];

                        }
                    }
                    ?>
                    <input type="hidden" name="nomeOrganismo" id="nomeOrganismo" value="<?php echo $nomeOrganismo;?>" />
                    <input type="hidden" name="nomeUsuario" id="nomeUsuario" value="<?php echo $dadosUsuario->getNomeUsuario();?>" />
                    <input type="hidden" name="anoAtual" id="anoAtual" value="<?php echo $anoAtual;?>" />
                    <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>" />
                    <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" value="<?php echo $_SESSION['idOrganismoAfiliado'];?>" />
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Enviar" onclick="document.relatorioFinanceiro.submit();" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Relatório Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUpload"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


