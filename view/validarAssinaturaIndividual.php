<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();

include_once("model/ataReuniaoMensalClass.php");
$a = new ataReuniaoMensal();

include_once("model/financeiroMensalClass.php");
$f = new financeiroMensal();

include_once("model/atividadeEstatutoMensalClass.php");
$aem = new atividadeEstatutoMensal();

include_once("model/oggMensalClass.php");
$o = new oggMensal();

include_once("model/columbaTrimestralClass.php");
$ct = new columbaTrimestral();

include_once("model/relatorioClasseArtesaosClass.php");
$rca = new relatorioClasseArtesaos();

include_once("model/relatorioGrandeConselheiroClass.php");
$rgc = new relatorioGrandeConselheiro();

$documentoValido=false;
$nomeUsuario="";

if($_POST['validar']==true){
    if($_POST['tipo']==1)//Ata de Reunião Mensal
    {
        if(trim($_POST['codigo'])!="") {
            if ($a->buscaAssinaturaIndividual($_POST['codigo'],$_POST['codigoIndividual'])) {
                $assinaturaValida = true;
                $resultadoUsuario = $a->buscarAssinaturasEmAtaReuniaoMensal(null,null,$_POST['codigoIndividual']);
                if($resultadoUsuario)
                {
                    foreach($resultadoUsuario as $v)
                    {
                        $nomeUsuario = $v['nomeUsuario'];
                        $codigoAfiliacaoUsuario = $v['codigoDeAfiliacao'];
                    }
                }
            }
        }
    }

    if($_POST['tipo']==2)//Financeiro Mensal
    {
        if(trim($_POST['codigo'])!="") {
            if ($f->buscaAssinaturaIndividual($_POST['codigo'],$_POST['codigoIndividual'])) {
                $assinaturaValida = true;
                $resultadoUsuario = $f->buscarAssinaturasEmFinanceiroMensal(null,null,$_POST['codigoIndividual']);
                if($resultadoUsuario)
                {
                    foreach($resultadoUsuario as $v)
                    {
                        $nomeUsuario = $v['nomeUsuario'];
                        $codigoAfiliacaoUsuario = $v['codigoDeAfiliacao'];
                    }
                }
            }
        }
    }

    if($_POST['tipo']==3)//Atividade (Acordo de Afiliação) Mensal
    {
        if(trim($_POST['codigo'])!="") {
            if ($aem->buscaAssinaturaIndividual($_POST['codigo'],$_POST['codigoIndividual'])) {
                $assinaturaValida = true;
                $resultadoUsuario = $aem->buscarAssinaturasEmAtividadeMensal(null,null,$_POST['codigoIndividual']);
                if($resultadoUsuario)
                {
                    foreach($resultadoUsuario as $v)
                    {
                        $nomeUsuario = $v['nomeUsuario'];
                        $codigoAfiliacaoUsuario = $v['codigoDeAfiliacao'];
                    }
                }
            }
        }
    }

    if($_POST['tipo']==4)//OGG
    {
        if(trim($_POST['codigo'])!="") {
            if ($o->buscaAssinaturaIndividual($_POST['codigo'],$_POST['codigoIndividual'])) {
                $assinaturaValida = true;
                $resultadoUsuario = $o->buscarAssinaturasEmOggMensal(null,null,$_POST['codigoIndividual']);
                if($resultadoUsuario)
                {
                    foreach($resultadoUsuario as $v)
                    {
                        $nomeUsuario = $v['nomeUsuario'];
                        $codigoAfiliacaoUsuario = $v['codigoDeAfiliacao'];
                    }
                }
            }
        }
    }

    if($_POST['tipo']==5)//Columba
    {
        if(trim($_POST['codigo'])!="") {
            if ($ct->buscaAssinaturaIndividual($_POST['codigo'],$_POST['codigoIndividual'])) {
                $assinaturaValida = true;
                $resultadoUsuario = $ct->buscarAssinaturasEmColumbaTrimestral(null,null,$_POST['codigoIndividual']);
                if($resultadoUsuario)
                {
                    foreach($resultadoUsuario as $v)
                    {
                        $nomeUsuario = $v['nomeUsuario'];
                        $codigoAfiliacaoUsuario = $v['codigoDeAfiliacao'];
                    }
                }
            }
        }
    }

    if($_POST['tipo']==6)//Classe de Artesãos
    {
        if(trim($_POST['codigo'])!="") {
            if ($rca->buscaAssinaturaIndividual($_POST['codigo'],$_POST['codigoIndividual'])) {
                $assinaturaValida = true;
                $resultadoUsuario = $rca->buscarAssinaturasEmRelatorioClasseArtesaos(null,null,$_POST['codigoIndividual']);
                if($resultadoUsuario)
                {
                    foreach($resultadoUsuario as $v)
                    {
                        $nomeUsuario = $v['nomeUsuario'];
                        $codigoAfiliacaoUsuario = $v['codigoDeAfiliacao'];
                    }
                }
            }
        }
    }

    if($_POST['tipo']==7)//Grande Conselheiros
    {
        if(trim($_POST['codigo'])!="") {
            if ($rgc->buscaAssinaturaIndividual($_POST['codigo'],$_POST['codigoIndividual'])) {
                $assinaturaValida = true;
                $resultadoUsuario = $rgc->buscarAssinaturasEmRelatorioGrandeConselheiro(null,null,$_POST['codigoIndividual']);
                if($resultadoUsuario)
                {
                    foreach($resultadoUsuario as $v)
                    {
                        $nomeUsuario = $v['nomeUsuario'];
                        $codigoAfiliacaoUsuario = $v['codigoDeAfiliacao'];
                    }
                }
            }
        }
    }
}

      ?>

<!-- Conteúdo DE INCLUDE INÍCIO -->
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Validar Assinatura Eletrônica Individual</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li class="active">
                <strong><a>Validar Assinatura Eletrônica Individual</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!--
<div class="alert alert-warning">
                                Para fazer a pesquisa digite o <a class="alert-link" href="#"> código de afiliação e o nome do membro</a>.
                    </div>-->
<!-- Caminho de Migalhas Fim -->
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Validar Assinatura Eletrônica Individual</h5>
                    <div class="ibox-tools">
                        <!--
                        <a class="btn btn-xs btn-primary" href="?opcao=oficial_form.php">
                            <i class="fa fa-plus"></i> Novo Membro
                        </a>
                        -->
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="validacaoDocumentos" method="post">
                        <table style="border-spacing: 10px;border-collapse: separate;">
                            <tr>
                                <td></td>
                                <td>
                                    <?php if($assinaturaValida==true){?>
                                        <?php if(trim($_POST['codigo'])!=""){?>
                                            <?php if($_POST['validar']==true){?>
                                                <div class="alert alert-success alert-dismissable">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                    <a class="alert-link">Assinatura válida</a>. Essa assinatura foi realizada por <?php echo $nomeUsuario;?> [<?php echo $codigoAfiliacaoUsuario;?>] e é válida nesse documento que foi gerado pelo SOA.
                                                </div>
                                            <?php }else{

                                            }?>
                                        <?php }?>
                                    <?php }else{
                                             if($_REQUEST['validar']==true) {
                                                 if (trim($_POST['codigo']) == "") {
                                                     ?>
                                                     <div class="alert alert-danger alert-dismissable">
                                                         <button aria-hidden="true" data-dismiss="alert" class="close"
                                                                 type="button">×
                                                         </button>
                                                         <a class="alert-link">O campo está vazio</a>. Preencha o código do documento
                                                         para ser validado.
                                                     </div>
                                                 <?php }
                                                 if (trim($_POST['codigoIndividual']) == "") {
                                                     ?>
                                                     <div class="alert alert-danger alert-dismissable">
                                                         <button aria-hidden="true" data-dismiss="alert" class="close"
                                                                 type="button">×
                                                         </button>
                                                         <a class="alert-link">O campo está vazio</a>. Preencha o código da assinatura
                                                         para ser validada.
                                                     </div>
                                                 <?php }
                                             }
                                    }?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tipo de Documento:
                                </td>
                                <td>
                                    <select id="tipo" name="tipo" class="form-control col-sm-3">
                                        <option value="0">Selecione</option>
                                        <option value="1" <?php if($_POST['tipo']==1){ echo "selected";}?>>Ata de Reunião Mensal</option>
                                        <option value="2" <?php if($_POST['tipo']==2){ echo "selected";}?>>Relatório Financeiro Mensal</option>
                                        <option value="3" <?php if($_POST['tipo']==3){ echo "selected";}?>>Acordo de Afiliação Mensal</option>
                                        <option value="4" <?php if($_POST['tipo']==4){ echo "selected";}?>>Relatório OGG Mensal</option>
                                        <option value="5" <?php if($_POST['tipo']==5){ echo "selected";}?>>Relatório de Columbas Trimestral</option>
                                        <option value="6" <?php if($_POST['tipo']==6){ echo "selected";}?>>Relatório da Classe de Artesãos</option>
                                        <option value="7" <?php if($_POST['tipo']==7){ echo "selected";}?>>Relatório Semestral dos Grandes Conselheiros</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Código da Assinatura Eletrônica do Documento:
                                </td>
                                <td>
                                    <input type="text" name="codigo" maxlength="15" id="codigo" onkeypress="return SomenteNumero(event)" value="<?php echo $_POST['codigo'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Código da Assinatura Eletrônica Individual:
                                </td>
                                <td>
                                    <input type="text" name="codigoIndividual" maxlength="15" id="codigoIndividual" onkeypress="return SomenteNumero(event)" value="<?php echo $_POST['codigoIndividual'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="submit" name="validar" id="validar" value="Validar Assinatura" class="btn btn-primary">
                                </td>
                            </tr>

                        </table>
                    </form>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->


