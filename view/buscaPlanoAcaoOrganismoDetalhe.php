<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("model/planoAcaoOrganismoArquivoClass.php");
$arq = new planoAcaoOrganismoArquivo();

$anexo = isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;

/**
 * Upload dos Arquivos
 */
$arquivo=0;
$extensaoNaoValida=0;
if($anexo != "")
{
	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo']['name'], '.');
                if($extensao==".pdf"||
                        $extensao==".png"||
                        $extensao==".jpg"||
                        $extensao==".jpeg"||
                        $extensao==".PDF"||
                        $extensao==".PNG"||
                        $extensao==".JPG"||
                        $extensao==".JPEG"||
                        $extensao==".DOC"||
                        $extensao==".doc"||
                        $extensao==".DOCX"||
                        $extensao==".docx"
                        )
                {
                    $proximo_id = $arq->selecionaUltimoId();
                    $caminho = "img/plano_acao_organismo/" . basename($_REQUEST["idPlanoAcaoOrganismoUpload"].".plano.acao.organismo.".$proximo_id);
                    $arq->setFkIdPlanoAcaoOrganismo($_REQUEST["idPlanoAcaoOrganismoUpload"]);
                    $arq->setCaminhoPlanoOrganismoArquivo($caminho);
                    if($arq->cadastroPlanoOrganismoArquivo())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/plano_acao_organismo/';
                            $uploadfile = $uploaddir . basename($_REQUEST["idPlanoAcaoOrganismoUpload"].".plano.acao.organismo.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            { 
                                    $arquivo = 1;  
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o arquivo!');</script>";
                            }
                    }
                }else{
                    $extensaoNaoValida=1;
                }
            }else{
                $extensaoNaoValida=2;
            }
	}
}

if($arquivo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Arquivo enviado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php 
}

if($extensaoNaoValida==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php 
}

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

/**
 * Exclusão dos Arquivos
 */
$excluirArquivo = isset($_REQUEST['excluirArquivo'])?$_REQUEST['excluirArquivo']:null;
//echo "excluirArquivo:".$excluirArquivo;
if($excluirArquivo!= "")
{
	$arq = new planoAcaoOrganismoArquivo();
	$resultado = $arq->removePlanoAcaoOrganismoArquivo($excluirArquivo);
	
	//Mostrar alert
	?>
	<script>
	window.onload = function(){
		swal({title:"Sucesso!",text:"Seu arquivo foi deletado com sucesso.",type:"success",confirmButtonColor:"#1ab394"});
	}
	</script>
	<?php 
}

include_once("controller/planoAcaoOrganismoController.php");
include_once("lib/webservice/retornaInformacoesMembro.php");
$parc = new planoAcaoOrganismoController();
$dados = $parc->buscaPlanoAcaoOrganismo($_GET['idPlanoAcaoOrganismo']);

/**
 * Atualização
 */
include_once("model/planoAcaoOrganismoAtualizacaoClass.php");
$para = new planoAcaoOrganismoAtualizacao();
if(isset($_GET['idPlanoAcaoOrganismo']))
{
	$resultadoAtualizacao = $para->listaAtualizacao($_GET['idPlanoAcaoOrganismo']);
}else{
	$resultadoAtualizacao = $para->listaAtualizacao($_REQUEST['idPlanoAcaoOrganismoModalAtualizacao']);	
}
if($resultadoAtualizacao)
{
	$totalAtualizacao = count($resultadoAtualizacao);
}else{
	$totalAtualizacao = 0;
}
if(isset($_REQUEST['mensagem'])&&$_REQUEST['mensagem']!="")
{
	$para->setFk_idPlanoAcaoOrganismo($_REQUEST['idPlanoAcaoOrganismoModalAtualizacao']);
	$para->setSeqCadast($_REQUEST['usuario']);
	$para->setMensagem($_REQUEST['mensagem']);
	if($_REQUEST['idPlanoAcaoOrganismoAtualizacao']==0)
	{
		$para->cadastroPlanoAcaoOrganismoAtualizacao();
	}else{
		$para->setIdPlanoAcaoOrganismoAtualizacao($_REQUEST['idPlanoAcaoOrganismoAtualizacao']);
		$para->removeAtualizacao();	
		$para->cadastroPlanoAcaoOrganismoAtualizacao();
	}
	echo "<script>location.href='?corpo=buscaPlanoAcaoOrganismoDetalhe&idPlanoAcaoOrganismo=".$_REQUEST['idPlanoAcaoOrganismoModalAtualizacao']."&salvoAtualizacao=1'</script>";
}

/**
 * Meta
 */
include_once("model/planoAcaoOrganismoMetaClass.php");
$parm = new planoAcaoOrganismoMeta();
if(isset($_GET['idPlanoAcaoOrganismo']))
{
	$resultadoMeta = $parm->listaMeta($_GET['idPlanoAcaoOrganismo']);
}else{
	$resultadoMeta = $parm->listaMeta($_REQUEST['idPlanoAcaoOrganismoMeta']);	
}
if($resultadoMeta)
{
	$totalMeta = count($resultadoMeta);
}else{
	$totalMeta = 0;
}
if(isset($_GET['idPlanoAcaoOrganismo']))
{
	$resultadoMetaConcluida = $parm->listaMeta($_GET['idPlanoAcaoOrganismo'],1);
}else{
	$resultadoMetaConcluida = $parm->listaMeta($_REQUEST['idPlanoAcaoOrganismoModalMeta'],1);	
}
if($resultadoMetaConcluida)
{
	$totalMetaConcluida = count($resultadoMetaConcluida);
}else{
	$totalMetaConcluida = 0;
}
if(isset($_REQUEST['tituloMeta'])&&$_REQUEST['tituloMeta']!="")
{
	$parm->setFk_idPlanoAcaoOrganismo($_REQUEST['idPlanoAcaoOrganismoModalMeta']);
	$parm->setSeqCadast($_REQUEST['usuario']);
	$parm->setStatusMeta(0);
	$parm->setTituloMeta($_REQUEST['tituloMeta']);
	$parm->setInicio(substr($_REQUEST['inicio'],6,4)."-".substr($_REQUEST['inicio'],3,2)."-".substr($_REQUEST['inicio'],0,2));
	$parm->setFim(substr($_REQUEST['fim'],6,4)."-".substr($_REQUEST['fim'],3,2)."-".substr($_REQUEST['fim'],0,2));
	$parm->setOque($_REQUEST['oque']);
	$parm->setPorque($_REQUEST['porque']);
	$parm->setQuem($_REQUEST['quem']);
	$parm->setOnde($_REQUEST['onde']);
	$parm->setQuando($_REQUEST['quando']);
	$parm->setComo($_REQUEST['como']);
	$parm->setQuanto($_REQUEST['quanto']);
	$parm->setOrdenacao($_REQUEST['ordenacao']);
	if($_REQUEST['idPlanoAcaoOrganismoMeta']==0)
	{
		$parm->cadastroPlanoAcaoOrganismoMeta();
	}else{
		$parm->setIdPlanoAcaoOrganismoMeta($_REQUEST['idPlanoAcaoOrganismoMeta']);
		$parm->removeMeta();	
		$parm->cadastroPlanoAcaoOrganismoMeta();
	}
	echo "<script>location.href='?corpo=buscaPlanoAcaoOrganismoDetalhe&idPlanoAcaoOrganismo=".$_REQUEST['idPlanoAcaoOrganismoModalMeta']."&salvoMeta=1'</script>";
}

/*
 * Cálculo da percentagem concluída
 */
if($totalMetaConcluida>0)
{
	$percentual = round(($totalMetaConcluida*100)/$totalMeta);
}else{
	$percentual = 0;
}
?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvoAtualizacao = isset($_REQUEST['salvoAtualizacao'])?$_REQUEST['salvoAtualizacao']:null;
if($salvoAtualizacao==1&&$arquivo==0){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Atualização salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$salvoMeta = isset($_REQUEST['salvoMeta'])?$_REQUEST['salvoMeta']:null;
if($salvoMeta==1&&$arquivo==0){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Meta salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Planos de Ação do Organismo (Detalhes)</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaPlanoAcaoOrganismo">Planos de Ação do Organismo</a>
            </li>
            <li class="active">
            	<b>
                	<a href="#">Detalhes do Plano de Ação</a>
                </b>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
            <div class="col-lg-9">
                <div class="wrapper wrapper-content animated fadeInUp">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="m-b-md">
                                    	<table cellpadding="10" align="right" border="0" width="185">
                                    		<tr>
                                    			<td><a href="?corpo=buscaPlanoAcaoOrganismo" class="btn btn-warning btn-xs pull-right">Voltar</a></td>
                                    			<?php if(in_array("2",$arrNivelUsuario)&&!$usuarioApenasLeitura){?>
                                    			<td><a href="?corpo=alteraPlanoAcaoOrganismo&idPlanoAcaoOrganismo=<?php echo $_REQUEST['idPlanoAcaoOrganismo'];?>" class="btn btn-info btn-xs pull-right">Editar Plano de Ação</a></td>
                                    			<?php }?>
                                    		</tr>
                                    	</table>
                                        <h2>Plano: <?php echo $dados->getTituloPlano();?></h2>
                                    </div>
                                    <dl class="dl-horizontal">
                                        <dt>Status:</dt> 
                                        	<dd>
                                        		<?php 
	                                            if($dados->getStatusPlano()==1)
	                                            {
	                                            	echo "<div id=\"statusPlano".$dados->getIdPlanoAcaoOrganismo()."\"><a href=\"#\" ";
	                                            	if(in_array("2",$arrNivelUsuario)){
	                                            		echo "onclick=\"mudaStatusPlanoAcaoOrganismo('".$dados->getIdPlanoAcaoOrganismo()."','1')\"";
	                                            	}
	                                            	echo "><span class=\"label label-primary\">Ativo</span></a></div>";
	                                            }else{
	                                            	echo "<div id=\"statusPlano".$dados->getIdPlanoAcaoOrganismo()."\"><a href=\"#\" ";
	                                            	if(in_array("2",$arrNivelUsuario)){
	                                            		echo "onclick=\"mudaStatusPlanoAcaoOrganismo('".$dados->getIdPlanoAcaoOrganismo()."','0')\"";
	                                            	}
	                                            	echo "><span class=\"label label-default\">Inativo</span></a></div>";
	                                            }
	                                            ?>
                                        	</dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">

                                        <dt>Criado por:</dt> <dd><?php echo $dados->getCriadoPor();?></dd>
                                        <dt>Atualizações:</dt> <dd><?php echo $totalAtualizacao;?></dd>
                                        <dt>Metas:</dt> <dd><?php echo $totalMeta;?></dd>
                                        <dt>Organismo:</dt> 
                                        <dd>
                                        	<a href="#" class="text-navy"> 
                                        		<?php 
                                        		include_once ('model/organismoClass.php');
                                        		$o = new organismo();
                                        		$resultado = $o->listaOrganismo(null,null,null,null,null,$dados->getFkIdOrganismoAfiliado());
                                        		if($resultado)
                                        		{
                                        			foreach($resultado as $vetor)
                                        			{
                                        				switch ($vetor['classificacaoOrganismoAfiliado']) {
												            case 1:
												                $classificacao = "Loja";
												                break;
												            case 2:
												                $classificacao = "Pronaos";
												                break;
												            case 3:
												                $classificacao = "Capítulo";
												                break;
												            case 4:
												                $classificacao = "Heptada";
												                break;
												            case 5:
												                $classificacao = "Atrium";
												                break;
												        }
												        switch ($vetor['tipoOrganismoAfiliado']) {
												            case 1:
												                $tipo = "R+C";
												                break;
												            case 2:
												                $tipo = "TOM";
												                break;
												        }
												
												        $nomeOrganismo = $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor["nomeOrganismoAfiliado"];
                                        				echo $nomeOrganismo;
                                        			}
                                        		}
                                        		?>
                                        	</a> 
                                        </dd>
                                        
                                    </dl>
                                </div>
                                <div class="col-lg-7" id="cluster_info">
                                    <dl class="dl-horizontal" >

                                        <dt>Ultima atualização:</dt> 
                                        <dd>
                                        	<?php
                                        	$ultimaAtualizacao = $para->retornaUltimaAtualizacao($_REQUEST['idPlanoAcaoOrganismo']); 
                                        	if($ultimaAtualizacao!="")
                                        	{
                                        		echo substr($ultimaAtualizacao,8,2)."/".substr($ultimaAtualizacao,5,2)."/".substr($ultimaAtualizacao,0,4)." ".substr($ultimaAtualizacao,11,8);
                                        	}else{
                                        		echo substr($dados->getDataCriacao(),8,2)."/".substr($dados->getDataCriacao(),5,2)."/".substr($dados->getDataCriacao(),0,4)." ".substr($dados->getDataCriacao(),11,8);
                                        	}
                                        	?>
                                        </dd>
                                        <dt>Criado em:</dt> <dd> <?php echo substr($dados->getDataCriacao(),8,2)."/".substr($dados->getDataCriacao(),5,2)."/".substr($dados->getDataCriacao(),0,4)." ".substr($dados->getDataCriacao(),11,8);?> </dd>
                                        <?php 
                                        	$parp = new planoAcaoOrganismoParticipante();
                                        	$resultado2 = $parp->listaParticipantes($_REQUEST['idPlanoAcaoOrganismo']);
                                        ?>
                                        <dt>Participantes:</dt>
                                        <dd class="project-people">
                                        	<?php 
	                                        	if($resultado2)
	                                        	{
	                                        		foreach($resultado2 as $vetor2)
	                                        		{
	                                        			$pu = new perfilUsuario();
	                                        			$resultado3 = $pu->listaAvatarUsuario($vetor2['seqCadast']);
	                                        			if($resultado3)
	                                        			{
		                                        			foreach($resultado3 as $vetor3)
		                                        			{
		                                        				
			                                        			if($vetor3['avatarUsuario']!="")
			                                        			{
			                                        			
			                                        	?>
			                                            			<a href="#" title="<?php echo $vetor3['nomeUsuario'];?>"><img alt="image" class="img-circle" src="<?php echo $vetor3['avatarUsuario'];?>"></a>
			                                            <?php 
			                                        			}else{
			                                        			?>
			                                        				<a href="#" title="<?php echo retornaNomeCompleto($vetor3['seqCadast']);?>"><img alt="image" class="img-circle" src="img/default-user.png"></a>
			                                        			<?php 	
			                                        			}
		                                        			}	
	                                        			}
	                                        		}
	                                        	}
	                                        ?>	
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <dl class="dl-horizontal">
                                        <dt>Progresso:</dt>
                                        <dd>
                                            <div class="progress progress-striped active m-b-sm">
                                                <div id="percentual" style="width: <?php echo $percentual;?>%;" class="progress-bar"></div>
                                            </div>
                                            <small><strong><span id="textoPercentual"><?php echo $percentual;?></span>%</strong>. Essa percentagem é formada pelo percentual de conclusão das metas.</small>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                            	<div style="text-align: right">
                            		<?php if(in_array("1",$arrNivelUsuario)&&!$usuarioApenasLeitura){?>
                            		<span class="label label-info"><a href="#" onclick="document.getElementById('idPlanoAcaoOrganismoAtualizacao').value=<?php echo $_REQUEST['idPlanoAcaoOrganismo']; ?>" style="color:white;" data-target="#mySeeAtualizacao" data-toggle="modal" data-placement="left"><i class="fa fa-plus"></i> Adicionar Atualização</a></span>
                            		<span class="label label-warning"><a href="#" onclick="document.getElementById('idPlanoAcaoOrganismoMeta').value=<?php echo $_REQUEST['idPlanoAcaoOrganismo']; ?>" style="color:white;" data-target="#mySeeMeta" data-toggle="modal" data-placement="left"><i class="fa fa-plus"></i> Adicionar Meta</a></span>
                            		<?php }?>
                            	</div>
                            </div>
                            <div class="row m-t-sm">
                                <div class="col-lg-12">
                                <div class="panel blank-panel">
                                <div class="panel-heading">
                                    <div class="panel-options">
                                        <ul class="nav nav-tabs">
                                            <li class="<?php if($salvoMeta!=1){?>active<?php }?>"><a href="#tab-1" data-toggle="tab">Atualizações</a></li>
                                            <li class="<?php if($salvoMeta==1){?>active<?php }?>"><a href="#tab-2" data-toggle="tab">Metas</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">

                                <div class="tab-content">
                                <div class="tab-pane <?php if($salvoMeta!=1){?>active<?php }?>" id="tab-1">
                                    <div class="feed-activity-list">
                                    	<?php 
                                    	if($resultadoAtualizacao)
                                    	{
                                    			foreach($resultadoAtualizacao as $vetor4)
                                    			{
			                                                $pu = new perfilUsuario();
		                                        			$resultado5 = $pu->listaAvatarUsuario($vetor4['seqCadast']);
		                                        			if($resultado5)
		                                        			{
			                                        			foreach($resultado5 as $vetor5)
			                                        			{
			                                        				$nomeUsuario = $vetor5['nomeUsuario'];
				                                        			if($vetor5['avatarUsuario']!="")
				                                        			{?>
								                                       <div class="feed-element">
								                                            <a href="#" class="pull-left">
								                                                <img alt="image" class="img-circle" src="<?php echo $vetor5['avatarUsuario'];?>">
								                                            </a>
								                                            <div class="media-body ">
								                                                <small class="pull-right">&nbsp;</small>
								                                                <strong><?php echo $nomeUsuario;?></strong> postou a mensagem. <a href="#" onclick="visualizarAtualizacaoPlanoAcaoOrganismo('<?php echo $vetor4['idPlanoAcaoOrganismoAtualizacao']?>');" data-target="#mySeeAtualizacao" data-toggle="modal" data-placement="left"><i class="fa fa-pencil"></i></a><br>
								                                                <small class="text-muted"><?php echo substr($vetor4['dataCadastro'],8,2)."/".substr($vetor4['dataCadastro'],5,2)."/".substr($vetor4['dataCadastro'],0,4);?> às <?php echo substr($vetor4['dataCadastro'],11,8)?></small>
								                                                <div class="well">
								                                                    <?php echo $vetor4['mensagem'];?>
								                                                </div>
								                                            </div>
								                                        </div> 
								                                   <?php 
					                                    			}else{?>
					                                    				<div class="feed-element">
								                                            <a href="#" class="pull-left">
								                                                <img alt="image" class="img-circle" src="img/default-user.png">
								                                            </a>
								                                            <div class="media-body ">
								                                                <small class="pull-right">&nbsp;</small>
								                                                <strong><?php echo $nomeUsuario;?></strong> postou a mensagem. <a href="#" onclick="visualizarAtualizacaoPlanoAcaoOrganismo('<?php echo $vetor4['idPlanoAcaoOrganismoAtualizacao']?>');" data-target="#mySeeAtualizacao" data-toggle="modal" data-placement="left"><i class="fa fa-pencil"></i></a><br>
								                                                <small class="text-muted"><?php echo substr($vetor4['dataCadastro'],8,2)."/".substr($vetor4['dataCadastro'],5,2)."/".substr($vetor4['dataCadastro'],0,4);?> às <?php echo substr($vetor4['dataCadastro'],11,8)?></small>
								                                                <div class="well">
								                                                    <?php echo $vetor4['mensagem'];?>
								                                                </div>
								                                            </div>
								                                        </div> 
					                                    			<?php 
                                    							}
		                                        			}
		                                        		}
                                    			}
                                    		       
                                    	}
                                    	
			                         	?>
                                    </div>

                                </div>
                                <div class="tab-pane <?php if($salvoMeta==1){?>active<?php }?>" id="tab-2">

                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Status</th>
                                            <th>Titulo</th>
                                            <th>Início</th>
                                            <th>Fim</th>
                                            <th>5W 2H</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        
                                    	if($resultadoMeta)
                                    	{
                                    			foreach($resultadoMeta as $vetor7)
                                    			{
			                                    	?>
				                                        <tr>
				                                            <td>
				                                            
				                                            	<?php 
					                                            if($vetor7['statusMeta']==1)
					                                            {
					                                            	echo "<div id=\"statusMeta".$vetor7['idPlanoAcaoOrganismoMeta']."\"><a href=\"#\" ";
					                                            	if(in_array("2",$arrNivelUsuario)){
					                                            		echo "onclick=\"mudaStatusMetaOrganismo('".$vetor7['idPlanoAcaoOrganismoMeta']."','1')\"";
					                                            	}
					                                            	echo "><span class=\"label label-primary\"><i class=\"fa fa-check\"></i> Completa</span></a></div>";
					                                            }else{
					                                            	echo "<div id=\"statusMeta".$vetor7['idPlanoAcaoOrganismoMeta']."\"><a href=\"#\" ";
					                                            	if(in_array("2",$arrNivelUsuario)){
					                                            		echo "onclick=\"mudaStatusMetaOrganismo('".$vetor7['idPlanoAcaoOrganismoMeta']."','0')\"";
					                                            	}
					                                            	echo "><span class=\"label label-default\"><i class=\"fa fa-thumbs-o-down\"></i> Incompleta</span></a></div>";
					                                            }
					                                            ?>
				                                                
				                                            </td>
				                                            <td>
				                                               <?php echo $vetor7['tituloMeta'];?>
				                                            </td>
				                                            <td>
				                                               <?php echo substr($vetor7['inicio'],8,2)."/".substr($vetor7['inicio'],5,2)."/".substr($vetor7['inicio'],0,4);?>
				                                            </td>
				                                            <td>
				                                               <?php echo substr($vetor7['fim'],8,2)."/".substr($vetor7['fim'],5,2)."/".substr($vetor7['fim'],0,4);?>
				                                            </td>
				                                            <td>
				                                            <p class="small">
				                                               <span class="label label-warning"><a href="#" style="color:white;" onclick="visualizar5w2hOrganismo('<?php echo $vetor7['idPlanoAcaoOrganismoMeta'];?>');" data-target="#mySee5w2h" data-toggle="modal" data-placement="left"><i class="fa fa-check"></i> 5W 2H</a></span>
				                                            </p>
				                                            </td>
				                                            <td>
																<a href="#" onclick="visualizarMetaPlanoAcaoOrganismo('<?php echo $vetor7['idPlanoAcaoOrganismoMeta']?>');" data-target="#mySeeMeta" data-toggle="modal" data-placement="left"><i class="fa fa-pencil"></i></a>
															</td>
				                                        </tr>
				                                   <?php 
                                    			}
                                    	}    
				                               ?>
                                        </tbody>
                                    </table>

                                </div>
                                </div>
                                
                                

                                </div>

                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="wrapper wrapper-content project-manager">
                    <h4>Descrição do Plano de Ação</h4>
                    <i class="fa fa-paste fa-5x"></i>
                    <p class="small">
                        <?php echo $dados->getDescricaoPlano();?>
                    </p>
                    <p class="small font-bold">
                    	<?php if($dados->getPrioridade()==1){?>
                       		<span><i class="fa fa-circle text-danger"></i> Alta prioridade</span>
                       	<?php }?>
                       	<?php if($dados->getPrioridade()==2){?>
                       		<span><i class="fa fa-circle text-warning"></i> Prioridade média</span>
                       	<?php }?>
                       	<?php if($dados->getPrioridade()==3){?>
                       		<span><i class="fa fa-circle text-info"></i> Baixa prioridade</span>
                       	<?php }?>
                    </p>
                    <div class="row">
                    	<h5>Palavras-chave:</h5>
	                    <ul class="tag-list" style="padding: 0">
	                    	<?php 
	                    	$arrPalavraChave = explode(",",$dados->getPalavraChave());
	                    	if(count($arrPalavraChave)>0)
	                    	{
	                    		foreach($arrPalavraChave as $v)
	                    		{
	                    	?>
	                        		<li><a href=""><i class="fa fa-tag"></i> <?php echo $v;?></a></li>
	                        <?php 
	                    		}
	                    	}
	                        ?>
	                    </ul>
                    </div>
                    <div class="row">
                    	<h5>Arquivos do Plano</h5>
	                    <ul class="list-unstyled project-files">
	                    <?php

							$resultado = $arq->listaPlanoOrganismoArquivo($_REQUEST['idPlanoAcaoOrganismo']);
							$i=1;
							if($resultado)
							{
								if (count($resultado)>0) {
									
									foreach($resultado as $vetor)
									{
										?>
	                        				<li>
	                        					<a href="<?php echo $vetor['caminho'];?>" target="_blank"><i class="fa fa-file"></i> Arquivo <?php echo $i;?></a>
	                        					<?php if(in_array("3",$arrNivelUsuario)){?>
													<a onclick="excluirArquivo('?corpo=buscaPlanoAcaoOrganismoDetalhe&idPlanoAcaoOrganismo=<?php echo $_REQUEST['idPlanoAcaoOrganismo'];?>&excluirArquivo=<?php echo $vetor['idPlanoAcaoOrganismoArquivo'];?>');"><i class="fa fa-trash"></i></a>
												<?php }?>
	                        				</li>
	                        			<?php
										$i++; 
									}
								}
							}
						?>	
	                    </ul>
                    </div>
                    <div class="row">
	                    <div class="text-center m-t-md">
	                    	<?php if(in_array("4",$arrNivelUsuario)&&!$usuarioApenasLeitura){?>
	                        	<a href="#" class="btn btn-xs btn-primary" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload">Adicionar Arquivos</a>
	                        <?php }?>
	                    </div>
                    </div>
                </div>
            </div>
</div>
<!-- Tabela Fim -->
<input type="hidden" name="idPlanoAcaoOrganismo" id="idPlanoAcaoOrganismo" value="<?php echo $_REQUEST['idPlanoAcaoOrganismo'];?>">
<!-- Conteúdo DE INCLUDE FIM -->

<div class="modal inmodal" id="mySeeAtualizacao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-refresh modal-icon"></i>
                <h4 class="modal-title">Atualização</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="atualizacao" name="atualizacao" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea rows="7" name="mensagem" id="mensagem" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<input type="hidden" id="idPlanoAcaoOrganismoModalAtualizacao" name="idPlanoAcaoOrganismoModalAtualizacao" value="<?php echo $_REQUEST['idPlanoAcaoOrganismo'];?>">
					<input type="hidden" id="idPlanoAcaoOrganismoAtualizacao" name="idPlanoAcaoOrganismoAtualizacao" value="0">
					<input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Salvar Atualização" onclick="document.atualizacao.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="mySeeMeta" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-bullseye modal-icon"></i>
                <h4 class="modal-title">Meta</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="meta" name="meta" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Título</label>
                            <div class="col-sm-10"><input type="text" name="tituloMeta" id="tituloMeta" class="form-control" style="max-width: 500px" required="required"></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Dt. Inicial</label>
                            <div class="col-sm-10"><input type="text" name="inicio" id="inicio" class="form-control data" onblur="validaDataZerada(this.value)" style="max-width: 150px" required="required"></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Dt. Final</label>
                            <div class="col-sm-10"><input type="text" name="fim" id="fim" class="form-control data" style="max-width: 150px" onblur="validaDataZerada(this.value)" required="required"></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Posição na Lista</label>
                            <div class="col-sm-10"><input type="text" name="ordenacao" id="ordenacao" class="form-control" value="0" style="max-width: 150px" required="required"></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">O que?</label>
                            <div class="col-sm-10"><textarea rows="7" name="oque" id="oque" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Por que?</label>
                            <div class="col-sm-10"><textarea rows="7" name="porque" id="porque" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Quem?</label>
                            <div class="col-sm-10"><textarea rows="7" name="quem" id="quem" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Onde?</label>
                            <div class="col-sm-10"><textarea rows="7" name="onde" id="onde" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Quando?</label>
                            <div class="col-sm-10"><textarea rows="7" name="quando" id="quando" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Como?</label>
                            <div class="col-sm-10"><textarea rows="7" name="como" id="como" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Quanto?</label>
                            <div class="col-sm-10"><textarea rows="7" name="quanto" id="quanto" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>   	
					</div>
					<input type="hidden" id="idPlanoAcaoOrganismoModalMeta" name="idPlanoAcaoOrganismoModalMeta" value="<?php echo $_REQUEST['idPlanoAcaoOrganismo'];?>">
					<input type="hidden" id="idPlanoAcaoOrganismoMeta" name="idPlanoAcaoOrganismoMeta" value="0">
					<input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                </form>
            </div>
            <div class="modal-footer">
            	<a href="#" class="btn btn-info" onclick="cadastrarMetaModal();">Salvar Meta</a>
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="mySee5w2h" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-bullseye modal-icon"></i>
                <h4 class="modal-title">5W 2H da Meta</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Meta</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_tituloMeta"></span></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">O que?</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_oque"></span></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Por que?</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_porque"></span></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Quem?</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_quem"></span></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Onde?</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_onde"></span></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Quando?</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_quando"></span></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Como?</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_como"></span></div>
                        </div>   	
					</div>
					<div class="form-group">
						<div class="form-group"><label class="col-sm-2 control-label">Quanto?</label>
                            <div class="col-sm-10" style="margin-top:8px"><span id="v_quanto"></span></div>
                        </div>   	
					</div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio de Arquivos do Plano de Ação</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="plano" name="plano" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" name="idPlanoAcaoOrganismoUpload" id="idPlanoAcaoOrganismoUpload" value="<?php echo $_REQUEST['idPlanoAcaoOrganismo'];?>">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.plano.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	