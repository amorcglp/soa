<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Informações do Organismo salvas com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Organismo já cadastrado anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Histórico de Assinaturas Eletrônicas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Histórico de Assinaturas Eletrônicas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Assinaturas Realizadas por <?php echo $sessao->getValue("nomeUsuario");?></h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Código Indivi. da Ass.</th>
                                <th><center>Nome</center></th>
                                <th><center>IP</center></th>
                                <th><center>Data e Hora</center></th>
                                <th><center>Função</center></th>
                                <th><center>Cód. do Doc.</th>
                                <th><center>Cód. Ass. Eletro. do Doc.</th>
                                <th><center>Mês/Trimestre/Semestre</center></th>
                                <th><center>Ano</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //Atas de Reuniões Mensais
                            include_once("model/ataReuniaoMensalClass.php");
                            $arm = new ataReuniaoMensal();
                            $resultado = $arm->buscarAssinaturasEmAtaReuniaoMensal($sessao->getValue("seqCadast"),$idOrga2);
                            //echo "<pre>";print_r($resultado);
                            if ($resultado) {
                                foreach ($resultado as $vetor) {

                                    ?>
                                    <tr>
                                        <td>Ata de Reunião Mensal</td>
                                        <td><?php echo $vetor["codigoAssinaturaIndividual"];?></td>
                                        <td><?php echo $vetor["nomeUsuario"];?></td>
                                        <td>
			                                <center>
			        							<?php echo $vetor['ip']; ?>
			                                </center>
		                                </td>
                                        <td>
                                            <center>
                                                <?php
                                                if(isset($vetor['dataEntrega']))
                                                {
                                                    $dataHora = substr($vetor['dataEntrega'],8,2)."/".substr($vetor['dataEntrega'],5,2)."/".substr($vetor['dataEntrega'],0,4)." <br>às ".substr($vetor['dataEntrega'],11,10);
                                                    echo $dataHora;
                                                }
                                                ?>
                                            </center>
                                        </td>
		                                <td>
                                            <center>
		                                    <?php
		                                        echo $vetor['nomeFuncao'];
		                                    ?>
                                            </center>
		                                </td>
                                        <td><?php echo $vetor["idAtaReuniaoMensal"];?></td>
                                        <td><?php echo $vetor["numeroAssinatura"];?></td>
		                                <td>
		                                <center>
                                            <?php
                                                echo $vetor['mesCompetencia'];
                                            ?>
		                                </center>
		                                </td>
                                        <td>
                                        <center>
                                            <?php
                                                echo $vetor['anoCompetencia'];
                                            ?>
                                        </center>
                                        </td>
                                	</tr>
                                <?php
                                    }

                            }
                            ?>
                            <?php
                            //Financeiro
                            include_once("model/financeiroMensalClass.php");
                            $fm = new financeiroMensal();
                            $resultado2 = $fm->buscarAssinaturasEmFinanceiroMensal($sessao->getValue("seqCadast"),$idOrga2);
                            //echo "<pre>";print_r($resultado2);
                            if ($resultado2) {
                                foreach ($resultado2 as $vetor2) {

                                        ?>
                                        <tr>
                                            <td>Relatório Financeiro Mensal</td>
                                            <td><?php echo $vetor2["codigoAssinaturaIndividual"];?></td>
                                            <td><?php echo $vetor2["nomeUsuario"];?></td>
                                            <td>
                                                <center>
                                                    <?php echo $vetor2['ip']; ?>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <?php
                                                    if(isset($vetor2['data']))
                                                    {
                                                        $dataHora = substr($vetor2['data'],8,2)."/".substr($vetor2['data'],5,2)."/".substr($vetor2['data'],0,4)." <br>às ".substr($vetor2['data'],11,10);
                                                        echo $dataHora;
                                                    }
                                                    ?>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <?php
                                                    echo $vetor2['nomeFuncao'];
                                                    ?>
                                                </center>
                                            </td>
                                            <td><?php echo $vetor2["idFinanceiroMensal"];?></td>
                                            <td><?php echo $vetor2["numeroAssinatura"];?></td>
                                            <td>
                                                <center>
                                                    <?php
                                                        echo $vetor2['mes'];
                                                    ?>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <?php
                                                        echo $vetor2['ano'];
                                                    ?>
                                                </center>
                                            </td>
                                        </tr>
                                        <?php
                                    }

                            }
                            ?>
                            <?php
                            //Atividade
                            include_once("model/atividadeEstatutoMensalClass.php");
                            $aem = new atividadeEstatutoMensal();
                            $resultado3 = $aem->buscarAssinaturasEmAtividadeMensal($sessao->getValue("seqCadast"),$idOrga2);
                            //echo "<pre>";print_r($resultado2);
                            if ($resultado3) {
                                foreach ($resultado3 as $vetor3) {

                                    ?>
                                    <tr>
                                        <td>Relatório de Acordo de Afiliação</td>
                                        <td><?php echo $vetor3["codigoAssinaturaIndividual"];?></td>
                                        <td><?php echo $vetor3["nomeUsuario"];?></td>
                                        <td>
                                            <center>
                                                <?php echo $vetor3['ip']; ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                if(isset($vetor3['data']))
                                                {
                                                    $dataHora = substr($vetor3['data'],8,2)."/".substr($vetor3['data'],5,2)."/".substr($vetor3['data'],0,4)." <br>às ".substr($vetor3['data'],11,10);
                                                    echo $dataHora;
                                                }
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor3['nomeFuncao'];
                                                ?>
                                            </center>
                                        </td>
                                        <td><?php echo $vetor3["idAtividadeEstatutoMensal"];?></td>
                                        <td><?php echo $vetor3["numeroAssinatura"];?></td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor3['mes'];
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor3['ano'];
                                                ?>
                                            </center>
                                        </td>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>
                            <?php
                            //OGG
                            include_once("model/oggMensalClass.php");
                            $om = new oggMensal();
                            $resultado4 = $om->buscarAssinaturasEmOggMensal($sessao->getValue("seqCadast"),$idOrga2);
                            //echo "<pre>";print_r($resultado2);
                            if ($resultado4) {
                                foreach ($resultado4 as $vetor4) {

                                    ?>
                                    <tr>
                                        <td>Relatório OGG Mensal</td>
                                        <td><?php echo $vetor4["codigoAssinaturaIndividual"];?></td>
                                        <td><?php echo $vetor4["nomeUsuario"];?></td>
                                        <td>
                                            <center>
                                                <?php echo $vetor4['ip']; ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                if(isset($vetor4['data']))
                                                {
                                                    $dataHora = substr($vetor4['data'],8,2)."/".substr($vetor4['data'],5,2)."/".substr($vetor4['data'],0,4)." <br>às ".substr($vetor4['data'],11,10);
                                                    echo $dataHora;
                                                }
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor4['nomeFuncao'];
                                                ?>
                                            </center>
                                        </td>
                                        <td><?php echo $vetor4["idOggMensal"];?></td>
                                        <td><?php echo $vetor4["numeroAssinatura"];?></td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor4['mes'];
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor4['ano'];
                                                ?>
                                            </center>
                                        </td>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>

                            <?php
                            //Columba
                            include_once("model/columbaTrimestralClass.php");
                            $ct = new columbaTrimestral();
                            $resultado5 = $ct->buscarAssinaturasEmColumbaTrimestral($sessao->getValue("seqCadast"),$idOrga2);
                            //echo "<pre>";print_r($resultado2);
                            if ($resultado5) {
                                foreach ($resultado5 as $vetor5) {

                                    ?>
                                    <tr>
                                        <td>Relatório de Columbas Trimestral</td>
                                        <td><?php echo $vetor5["codigoAssinaturaIndividual"];?></td>
                                        <td><?php echo $vetor5["nomeUsuario"];?></td>
                                        <td>
                                            <center>
                                                <?php echo $vetor5['ip']; ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                if(isset($vetor5['data']))
                                                {
                                                    $dataHora = substr($vetor5['data'],8,2)."/".substr($vetor5['data'],5,2)."/".substr($vetor5['data'],0,4)." <br>às ".substr($vetor5['data'],11,10);
                                                    echo $dataHora;
                                                }
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor5['nomeFuncao'];
                                                ?>
                                            </center>
                                        </td>
                                        <td><?php echo $vetor5["idOggMensal"];?></td>
                                        <td><?php echo $vetor5["numeroAssinatura"];?></td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor5['trimestre'];
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor5['ano'];
                                                ?>
                                            </center>
                                        </td>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>
                            <?php
                            //Artesãos
                            include_once("model/relatorioClasseArtesaosClass.php");
                            $rca = new relatorioClasseArtesaos();
                            $resultado6 = $rca->buscarAssinaturasEmRelatorioClasseArtesaos($sessao->getValue("seqCadast"),$idOrga2);
                            //echo "<pre>";print_r($resultado);
                            if ($resultado6) {
                                foreach ($resultado6 as $vetor6) {

                                    ?>
                                    <tr>
                                        <td>Relatório Classe de Artesãos</td>
                                        <td><?php echo $vetor6["codigoAssinaturaIndividual"];?></td>
                                        <td><?php echo $vetor6["nomeUsuario"];?></td>
                                        <td>
                                            <center>
                                                <?php echo $vetor6['ip']; ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                if(isset($vetor6['dataEntrega']))
                                                {
                                                    $dataHora = substr($vetor6['dataEntrega'],8,2)."/".substr($vetor6['dataEntrega'],5,2)."/".substr($vetor6['dataEntrega'],0,4)." <br>às ".substr($vetor6['dataEntrega'],11,10);
                                                    echo $dataHora;
                                                }
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor6['nomeFuncao'];
                                                ?>
                                            </center>
                                        </td>
                                        <td><?php echo $vetor6["idRelatorioClasseArtesaos"];?></td>
                                        <td><?php echo $vetor6["numeroAssinatura"];?></td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor6['mesCompetencia'];
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor6['anoCompetencia'];
                                                ?>
                                            </center>
                                        </td>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>
                            <?php
                            //Artesãos
                            include_once("model/relatorioGrandeConselheiroClass.php");
                            $rgc = new relatorioGrandeConselheiro();
                            $resultado7 = $rgc->buscarAssinaturasEmRelatorioGrandeConselheiro($sessao->getValue("seqCadast"),$idRegiao);
                            //echo "<pre>";print_r($resultado);
                            if ($resultado7) {
                                foreach ($resultado7 as $vetor7) {

                                    ?>
                                    <tr>
                                        <td>Relatório Semestral de Grande Conselheiros</td>
                                        <td><?php echo $vetor7["codigoAssinaturaIndividual"];?></td>
                                        <td><?php echo $vetor7["nomeUsuario"];?></td>
                                        <td>
                                            <center>
                                                <?php echo $vetor7['ip']; ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                if(isset($vetor7['dataEntrega']))
                                                {
                                                    $dataHora = substr($vetor7['dataEntrega'],8,2)."/".substr($vetor7['dataEntrega'],5,2)."/".substr($vetor7['dataEntrega'],0,4)." <br>às ".substr($vetor7['dataEntrega'],11,10);
                                                    echo $dataHora;
                                                }
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor7['nomeFuncao'];
                                                ?>
                                            </center>
                                        </td>
                                        <td><?php echo $vetor7["idRelatorioGrandeConselheiro"];?></td>
                                        <td><?php echo $vetor7["numeroAssinatura"];?></td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor7['semestre'];
                                                ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php
                                                echo $vetor7['ano'];
                                                ?>
                                            </center>
                                        </td>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->


<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>		