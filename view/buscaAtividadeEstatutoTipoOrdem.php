<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/atividadeEstatutoTipoController.php");
$aetc = new atividadeEstatutoTipoController();

$classiOaTipoAtividadeEstatuto = isset($_GET['classiOa']) ? stripslashes($_GET['classiOa']) : 1;
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Tipos de Atividade - Organização de Exibição</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="?corpo=cadastroAtividadeEstatutoTipo">Tipos de Atividade - Organização de Exibição</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tipos de Atividades - Organização de Exibição</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeEstatutoTipo">
                            <i class="fa fa-reply fa-white"></i> Tipos de Atividade
                        </a>
                        <!--
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeEstatutoTipo">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                        -->
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                      <a href="?corpo=buscaAtividadeEstatutoTipoOrdem" class="col-lg-2 col-lg-offset-2 btn btn-primary <?php if($classiOaTipoAtividadeEstatuto==1){ echo 'btn-outline';};?>">Loja</a>
                      <a href="?corpo=buscaAtividadeEstatutoTipoOrdem&classiOa=2" class="col-lg-2 col-lg-offset-1 btn btn-primary <?php if($classiOaTipoAtividadeEstatuto==2){ echo 'btn-outline';};?>">Pronaos</a>
                      <a href="?corpo=buscaAtividadeEstatutoTipoOrdem&classiOa=3" class="col-lg-2 col-lg-offset-1 btn btn-primary <?php if($classiOaTipoAtividadeEstatuto==3){ echo 'btn-outline';};?>">Capítulo</a>
                    </div>
                    <div class="row" style="margin-top: 20px">
                      <div class="dd" id="nestableAtividadeTipo" style="width: 500px; margin-left:-250px; left:50%">
                          <ol class="dd-list">
                              <?php
                              $resultado = $aetc->listaAtividadeEstatutoTipo($classiOaTipoAtividadeEstatuto,1,true);
                              if ($resultado) {
                                  foreach ($resultado as $vetor) {
                                    ?>
                                    <li class="dd-item" data-id="<?php echo $vetor['idTipoAtividadeEstatuto'];?>">
                                        <div class="dd-handle">
                                          <?php echo "[" . $vetor['idTipoAtividadeEstatuto'] . "] " . $vetor['nomeTipoAtividadeEstatuto'];?>
                                        </div>
                                    </li>
                                    <?php
                                  }
                              }
                              ?>
                          </ol>
                      </div>
                    </div>
                    <!--<textarea id="nestable-output" class="form-control"></textarea>-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->


<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
