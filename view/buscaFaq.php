<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php
$salvo = isset($_REQUEST['salvo']) ? $_REQUEST['salvo'] : null;
if ($salvo == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Sua pergunta foi enviada com sucesso! Estamos verificando uma solução.",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php } ?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Perguntas do FAQ</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Perguntas FAQ</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Perguntas FAQ</h5>
                    <div class="ibox-tools">
                        <?php if (in_array("1", $arrNivelUsuario)) { ?>
                            <a class="btn btn-xs btn-primary" href="?corpo=cadastroRespostaFaq">
                                <i class="fa fa-plus"></i> Cadastrar Nova Resposta
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Título Notificação</th>
                                <th><center>Pergunta</center></th>
                        <th><center>Nome</center></th>
                        <th><center>Data</center></th>
                        <th><center>Status</center></th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/faqController.php");

                            $fc = new faqController();
                            $resultado = $fc->listaFaq();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['tituloPerguntaFaq']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['perguntaFaq']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['nomeUsuarioFaq']; ?>
                                        </td>
                                        <td>
                                            <?php
                                            $data = date_create($vetor['dataFaq']);
                                            echo date_format($data, 'd/m/Y H:i:s');
                                            ?>
                                        </td>
                                        <td>
                                            <div id="statusTarget<?php echo $vetor['idFaq'] ?>">
                                                <?php
                                                switch ($vetor['statusFaq']) {
                                                    case 0:
                                                        echo "<span class='btn btn-primary btn-xs'>Respondida</span>";
                                                        break;
                                                    case 1:
                                                        echo "<span class='btn btn-danger btn-xs'>Não Respondida</span>";
                                                        break;
                                                }
                                                ?>
                                            </div>
                                        </td>

                                        <td>
                                            <span id="status<?php echo $vetor['idFaq'] ?>">
                                                <?php if ($vetor['statusFaq'] == 1) { ?>
                                                <a class="btn btn-primary btn-xs" onclick="mudaStatusFaq('<?php echo $vetor['idFaq'] ?>', '<?php echo $vetor['statusFaq'] ?>', 'faq')" data-rel="tooltip" title="Respondida"> 
                                                        Respondida                                            
                                                    </a>
                                                <?php } else { ?>
                                                <a class="btn btn-danger btn-xs" onclick="mudaStatusFaq('<?php echo $vetor['idFaq'] ?>', '<?php echo $vetor['statusFaq'] ?>', 'faq')" data-rel="tooltip" title="Não Respondida">
                                                        Não Respondido                                            
                                                    </a>
                                                <?php } ?>
                                            </span>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

