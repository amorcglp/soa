<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->
<script>
function toggleRecorrente(cod)
{
    //alert(cod);
    if(cod==0)
    {
        document.getElementById("naoRecorrente").style.display="block";
        document.getElementById("atividadeRecorrente").style.display="none";
    }else{
        document.getElementById("naoRecorrente").style.display="none";
        document.getElementById("atividadeRecorrente").style.display="block";
    } 
    
}
</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Agenda de Atividades</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaAgendaAtividade">Agenda de Atividades</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de Cadastro da Agenda de Atividades do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAgendaAtividade">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="formulario" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onsubmit="return validaCadastroAgendaAtividade();">
                        <div class="row">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">&nbsp;</label>
                            <div class="col-sm-9"><br>
                                <span class="alert alert-danger">Assista ao video de instruções de preenchimento da agenda antes de começar,
                                <a href="https://www.youtube.com/watch?time_continue=1&v=r3X25LiHG8U" target="_blank">clicando aqui</a>.</span><br>&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2" required="required">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Atividade:</label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-5 chosen-select" onchange="retornaPeriodicidadeTipoAtividade(this.value);" name="fk_idTipoAtividadeEstatuto" id="fk_idTipoAtividadeEstatuto" data-placeholder="Selecione um organismo..." style="max-width: 500px" required="required">
                                    <option value="0">Selecione...</option>
                                    <?php
                                    include_once 'controller/atividadeEstatutoTipoController.php';
                                    $aetc   = new atividadeEstatutoTipoController();
                                    $oc     = new organismoController();
                                    $dadosOrganismo = $oc->buscaOrganismo($idOrganismoAfiliado);
                                    $aetc->criarComboBox(0, $dadosOrganismo->getClassificacaoOrganismoAfiliado(),false,false,true);
                                    ?>
                                </select>&nbsp; <p id="spanPeriodicidade" style="margin-top: 10px">Periodicidade: -- <p>
                            </div>
                        </div>
                          
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Complemento do Nome: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="250" id="complementoNomeAtividade" name="complementoNomeAtividade" type="text" value="" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Público da Atividade:</label>
                            <div class="col-sm-9">
                                <select name="fk_idPublico" id="fk_idPublico" data-placeholder="Selecione um público..." class="chosen-select" style="width:350px;" tabindex="2" required="required">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/publicoController.php';
                                    $p = new publicoController();
                                    $p->criarComboBox(0);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tag </label>
                            <div class="col-sm-9">
                                <input type="radio" name="tag" value="0" checked="checked">Eventos
                                <input type="radio" name="tag" value="1">Erin
                                <input type="radio" name="tag" value="2">Iniciações
                                <input type="radio" name="tag" value="3">Atividades do Organismo
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano de Competência: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="anoCompetencia" name="anoCompetencia" type="text" value="" style="max-width: 86px">
                                (Ex. Referente à 2015, então preencha 2015)
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Local da Atividade: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="250" id="local" name="local" type="text" value="" style="max-width: 350px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Recorrente? </label>
                            <div class="col-sm-9">
                                <input type="radio" name="recorrente" value="0" onchange="toggleRecorrente(this.value);">Não
                                <input type="radio" name="recorrente" value="1" onchange="toggleRecorrente(this.value);">Sim
                                &nbsp;&nbsp;
                                <button type="button" class="btn btn-info btn-rounded" data-toggle="popover" data-placement="auto top" data-content="
                                Marque SIM: se a atividade acontece com frequência no seu organismo nos dias de semana, por exemplo: Convocação Ritualística que é realizada “Toda quarta-feira ás 19 horas”, ou “Todo sábado ás 15h30” etc.
                                Marque NÃO: se a atividade tem uma “dia e hora” no calendário para acontecer, por exemplo todas as iniciações, neste caso uma iniciação do  “10º Grau de Templo”, marcada para o dia início 16/03/2019, hora início ás 20 horas, dia fim 16/03/2019 hora fim 21 horas.
                                " data-original-title="" title="">
                                    Tem dúvida sobre esse item? <b>Clique aqui</b>
                                </button>
                            </div>
                        </div>
                        <span id="naoRecorrente" style="display: none">
                            <div class="form-group" id="datapicker_ata">
                                    <label class="col-sm-3 control-label">Data Inicial:</label>
                                    <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input name="dataInicial" maxlength="10" id="dataInicial" type="text" class="form-control" style="max-width: 102px">
                                    </div>
                            </div>    
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Hora Inicial:</label>
                                <div class="col-sm-2 input-group" style="padding: 0px 0px 0px 15px">
                                    <div class="input-group clockpicker"  data-autoclose="true">
                                    <input type="text" class="form-control" onBlur="validaHora(this);" maxlength="5" id="horaInicial" name="horaInicial" value="">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="datapicker_ata">
                                    <label class="col-sm-3 control-label">Data Final:</label>
                                    <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input name="dataFinal" maxlength="10" id="dataFinal" type="text" class="form-control" style="max-width: 102px">
                                    </div>
                            </div>        
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Hora Final:</label>
                                <div class="col-sm-2 input-group" style="padding: 0px 0px 0px 15px">
                                    <div class="input-group clockpicker"  data-autoclose="true">
                                    <input type="text" class="form-control" onBlur="validaHora(this);" maxlength="5" id="horaFinal" name="horaFinal" value="">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                    </div>
                                </div>
                            </div>        
                        </span>    
                        <span class="form-group" id="atividadeRecorrente" style="display: none">
                            <label class="col-sm-3 control-label">Dias e horários: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="250" id="diasHorarios" name="diasHorarios" type="text" value="" style="max-width: 350px" placeholder="Ex: Segunda e Quarta às 19h">
                            </div>
                        </span>    
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Observações:</label>
                            <div class="col-sm-9">
                                <textarea rows="5" cols="50" name="observacoes" id="observacoes" placeholder="Cadastre os detalhes desta atividade para informar o publico no Portal"></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="idAgendaAtividade" id="idAgendaAtividade" value="<?php echo $_SESSION['seqCadast'];?>">
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-sm btn-success"
                                                data-toggle="tooltip" data-placement="left" title="Salvar">
                                                <i class="fa fa-check fa-white"></i>&nbsp; Salvar
                                        </button>
                                        &nbsp; <a href="?corpo=buscaAgendaAtividade"
                                                class="btn btn-sm btn-danger" data-toggle="tooltip"
                                                data-placement="left" title="Cancelar e voltar!"> <i
                                                class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
                                </div>
                        </div>
                        </div>
		</form>

                </div>
            </div>
        </div>
	</div>
</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeLoadingAjax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
            <div class="cssload-box-loading" style="margin: 30% 50% 50% 50%;">
			</div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
