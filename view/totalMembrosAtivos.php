
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Total de Membros Ativos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Relatórios</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaSubMenu">Total de Membros Ativos</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Total de Membros Ativos em <?php echo date('Y');?></h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
<?php //if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) { ?>
<?php
require_once('model/membrosRosacruzesAtivosClass.php');
$mra = new MembrosRosacruzesAtivos();
$o = new organismo();
$resultado20 = $o->listaOrganismo(null,null,null,null,null,null,null,1);
if($resultado20)
{
foreach($resultado20 as $vetor20)
{
$total=0;
$anoAtual=date('Y');
$mesAtual=date('m');
$idOrganismoAfiliado = $vetor20['idOrganismoAfiliado'];

if($mesAtual>=12)
{
$mJan						= $mra->retornaMembrosRosacruzesAtivos(12,$anoAtual,$idOrganismoAfiliado);
  if($mJan['numeroAtualMembrosAtivos']>0)
  {
    $tMembrosAtivos						= $mJan['numeroAtualMembrosAtivos'];
  }
}
if($mesAtual>=11)
{
$mFev						= $mra->retornaMembrosRosacruzesAtivos(11,$anoAtual,$idOrganismoAfiliado);
if($mFev['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mFev['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=10)
{
$mMar						= $mra->retornaMembrosRosacruzesAtivos(10,$anoAtual,$idOrganismoAfiliado);
if($mMar['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mMar['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=9)
{
$mAbr						= $mra->retornaMembrosRosacruzesAtivos(9,$anoAtual,$idOrganismoAfiliado);
if($mAbr['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mAbr['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=8)
{
$mMai						= $mra->retornaMembrosRosacruzesAtivos(8,$anoAtual,$idOrganismoAfiliado);
if($mMai['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mMai['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=7)
{
$mJun						= $mra->retornaMembrosRosacruzesAtivos(7,$anoAtual,$idOrganismoAfiliado);
if($mJun['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mJun['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=6)
{
$mJul						= $mra->retornaMembrosRosacruzesAtivos(6,$anoAtual,$idOrganismoAfiliado);
if($mJul['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mJul['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=5)
{
$mAgo						= $mra->retornaMembrosRosacruzesAtivos(5,$anoAtual,$idOrganismoAfiliado);
if($mAgo['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mAgo['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=4)
{
$mSet						= $mra->retornaMembrosRosacruzesAtivos(4,$anoAtual,$idOrganismoAfiliado);
if($mSet['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mSet['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=3)
{
$mOut						= $mra->retornaMembrosRosacruzesAtivos(3,$anoAtual,$idOrganismoAfiliado);
if($mOut['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mOut['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=2)
{
$mNov						= $mra->retornaMembrosRosacruzesAtivos(2,$anoAtual,$idOrganismoAfiliado);
if($mNov['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mNov['numeroAtualMembrosAtivos'];
}

}
if($mesAtual>=1)
{
$mDez						= $mra->retornaMembrosRosacruzesAtivos(1,$anoAtual,$idOrganismoAfiliado);
if($mDez['numeroAtualMembrosAtivos']>0)
{
  $tMembrosAtivos						= $mDez['numeroAtualMembrosAtivos'];
}

}
$mediaMembrosAtivos += $tMembrosAtivos;
}
}
?>
<!-- TOTAL DE MEMBROS ATIVOS NA R+C -->
<h1><?php echo $mediaMembrosAtivos;?> membros</h1>
<?php //}?>
</div>
</div>
</div>
</div>
</div>
</div>
