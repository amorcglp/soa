<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Usuário</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaUsuario">Usuários</a>
            </li>
            <li class="active">
                <a>Cadastro de Novo Usuário</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Novo Usuário</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaUsuario">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nome</label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeUsuario" id="nomeUsuario" class="form-control" required="" onChange="this.value = this.value.toUpperCase()" onblur="geraLoginUsuario()" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Código de Afiliação</label>
                            <div class="col-sm-10">
                                <input type="text" name="codigoAfiliacaoUsuario" id="codigoAfiliacaoUsuario" class="form-control" required="" onkeypress="return SomenteNumero(event)" onblur="retornaDados()" value="" style="max-width: 75px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">E-mail</label>
                            <div class="col-sm-10">
                                <input type="text" name="emailUsuario" id="emailUsuario" class="form-control" required="" onChange="this.value = this.value.toLowerCase()" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Telefone Residencial</label>
                            <div class="col-sm-10">
                                <input type="text" name="telefoneResidencialUsuario" id="telefoneResidencialUsuario" required="" class="form-control" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Telefone Comercial</label>
                            <div class="col-sm-10">
                                <input type="text" name="telefoneComercialUsuario" id="telefoneComercialUsuario" required="" class="form-control" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Celular</label>
                            <div class="col-sm-10">
                                <input type="tel" name="celularUsuario" id="celularUsuario" required="" class="form-control" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Departamento</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idDepartamento" id="fk_idDepartamento" style="max-width: 150px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'controller/departamentoController.php';
                                    $dc = new departamentoController();
                                    $dc->criarComboBox();
                                    ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" name="jaCadastrado" id="jaCadastrado">
                                <input type="hidden" name="seq_cadast" id="seq_cadast">
                                <input type="hidden" name="loginUsuario" id="loginUsuario">
                                <input type="hidden" name="senhaUsuario" id="senhaUsuario">
                                <input type="hidden" name="regiaoUsuario" id="regiaoUsuario">
                                <input type="hidden" name="totalFuncoes" id="totalFuncoes" value="0">
                                <button type="button" class="btn btn-primary" id="cadastraUser" onclick="cadastraUsuarioInterno()" type="submit">Cadastrar</button>
                                <a class="btn btn-white" type="submit">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<script type="text/javascript">
    /*
     $(function () {
     //Máscaras
     $("#telefoneResidencialUsuario").mask("(999) 9999-9999");
     $("#telefoneComercialUsuario").mask("(999) 9999-9999");
     //$("#celularUsuario").mask("(999) 9999-9999");
     
     jQuery('input[type=tel]').mask("(999) 9999-9999?9").ready(function(event) {
     var target, phone, element;
     target = (event.currentTarget) ? event.currentTarget : event.srcElement;
     phone = target.value.replace(/\D/g, '');
     element = $(target);
     element.unmask();
     if(phone.length > 10) {
     element.mask("(999) 99999-999?9");
     } else {
     element.mask("(999) 9999-9999?9");
     }
     });
     
     var i = -1;
     var toastCount = 0;
     var $toastlast;
     var getMessage = function () {
     var msg = 'Hi, welcome to Inspinia. This is example of Toastr notification box.';
     return msg;
     };
     
     toastr.options = {
     "closeButton": true,
     "debug": false,
     "progressBar": false,
     "positionClass": "toast-top-center",
     "onclick": null,
     "showDuration": "400",
     "hideDuration": "1000000",
     "timeOut": "7000000",
     "extendedTimeOut": "1000000",
     "showEasing": "swing",
     "hideEasing": "linear",
     "showMethod": "fadeIn",
     "hideMethod": "fadeOut"
     }
     
     function getLastToast(){
     return $toastlast;
     }
     $('#clearlasttoast').click(function () {
     toastr.clear(getLastToast());
     });
     $('#cleartoasts').click(function () {
     toastr.clear();
     });
     })
     */
</script>

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>		