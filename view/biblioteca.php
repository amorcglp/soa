<?php

require_once 'controller/usuarioController.php';
require_once 'controller/impressaoController.php';
require_once 'controller/documentoController.php';

@include_once 'lib/libImpressao.php';

$usuario = new usuarioController();
$impressao = new impressaoController();
$documento = new documentoController();

$id = isset($_SESSION['seqCadast']) ? $_SESSION['seqCadast'] : null;
$doc = isset($_POST['doc']) ? $_POST['doc'] : null;

$textoImpressao = "
Saudações em Todas as Pontas do nosso Sagrado Triangulo!
    
O presente Manual Administrativo é compartilhado com o compromisso especial
de sua execução exatamente de acordo com as diretrizes nele contidas.

Lembro que este documento está protegido por lei e registrado nas instâncias
competentes da República Federativa do Brasil, sendo interditada sua reprodução
por qualquer meio – cópia, fotocópia e digitalização – cabendo aos Oficiais se
organizarem de forma a dispensarem estas medidas.

É vedada também a retirada deste Manual (se impresso) das dependências do
Organismo Afiliado, não podendo qualquer Oficial ou Membro transportá-lo
consigo para sua residência ou outro local.

A GLP está tomando medidas para coibir o descumprimento destas diretrizes e
identificará por rastreamento quaisquer conteúdos privativos que por ventura
apareçam em outros locais que não os Organismos Afiliados.

O descumprimento gerará, além das inevitáveis compensações cármicas,
pela quebra do juramento de sigilo, em sanções administrativas.

Na certeza de que saberão valorizar este Sagrado Legado,
confio-lhes!

Nos Laços de Nossa Ordem,
Assim Seja!
Hélio de Moraes e Marques
Grande Mestre";

if ($id != null) {
    $dadosUsuario = $usuario->buscaUsuario($id);
    $impressao->setUsuario($dadosUsuario);
    
    $nomeUsuario = $dadosUsuario->getNomeUsuario();
    $codigoDeAfiliacao = $dadosUsuario->getCodigoDeAfiliacaoUsuario();
    $emailUsuario = $dadosUsuario->getEmailUsuario();
    
    $arquivo = "";
    
    if($doc != null) {
        $dadosDocumento = $documento->buscarDadosId($doc);
        if($dadosDocumento) {
            foreach ($dadosDocumento as $vetor) {
                $documento->setIdDocumento($vetor['idDocumento']);
                $documento->setDocumentoExtensao($vetor['extensao']);
                $documento->setPaginaImpressao(
                    empty($vetor['paginaImpressao']) ? null : $vetor['paginaImpressao']
                );
                $arquivo = $vetor['arquivo'];
            }
         $impressao->setDocumento($documento);
        }
    }
}

// Impressão
if(!empty($doc)) {
    if($documento->buscarDados($arquivo)==false) {
        echo "<script type=\"text/javascript\">";
        echo "alert(\"Arquivo não encontrado: '" . $arquivo . "'\");";
        echo "</script>";
    }
    else {
        $token = impPdf($arquivo, $impressao, $documento, $usuario, $id, "topo&meio", "ManualAdministrativo/");
        if ($token != null) {
            echo "<script type=\"text/javascript\">var p=window.open(\"output/". $token .".pdf\");p.window.focus();p.print();";
            echo "window.location='painelDeControle.php?corpo=biblioteca';</script>";
        }
        else {
            echo "<script type=\"text/javascript\">alert(\"Erro ao gerar PDF!\");";
            echo "window.location='painelDeControle.php?corpo=biblioteca';</script>";
        }
    }
}

?>

<link href="css/biblioteca.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src='js/lightbox.js'></script>
<body>
    <h1>Biblioteca de Arquivos SOA</h1>
    <p>Qualquer dúvida em relação ao SOA, sempre terá um documento nessa seção que irá lhe esclarecer suas dúvidas!</p><br>
    <input type="hidden" name="arquivo" id="arquivo" value="">
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Manual Administrativo</h5>
                    <div class="ibox-tools">
                        <?php 
                            $resultado = $documento->buscarDados("manual_administrativo");
                            if($resultado) {
                                foreach ($resultado as $vetor) {
                                   //echo "<a class='btn btn-xs btn-primary' " . $vetor['linkVisualizacao'] . " title=\"Visualizar manual completo\">";
                                   //echo "<i class=\"fa fa-file-archive-o fa-white\"></i>&nbsp; Manual Completo</a>&nbsp;";
                                   $desativado = ($id==null) ? "disabled=\"\"" : "";
                                   echo "<button class=\"btn btn-xs btn-success\" " . $desativado . " data-toggle=\"modal\" data-target=\"#ModalImpressao\" ";
                                   echo "data-toggle=\"tooltip\" data-placement=\"left\" onclick=\"document.getElementById('doc').value='" . $vetor['idDocumento'] . "'\" ";
                                   echo "title=\"Imprimir manual completo - Loja/Capítulo\"><i class=\"fa fa-print fa-white\"></i>&nbsp; Visualizar/Imprimir - [Loja/Capítulo]</button>";
                                }
                            }
                        ?>
                        <?php
                        $resultado = $documento->buscarDados("manual_administrativo_pronaos");
                        if($resultado) {
                            foreach ($resultado as $vetor) {
                                //echo "<a class='btn btn-xs btn-primary' " . $vetor['linkVisualizacao'] . " title=\"Visualizar manual completo\">";
                                //echo "<i class=\"fa fa-file-archive-o fa-white\"></i>&nbsp; Manual Completo</a>&nbsp;";
                                $desativado = ($id==null) ? "disabled=\"\"" : "";
                                echo "<button class=\"btn btn-xs btn-success\" " . $desativado . " data-toggle=\"modal\" data-target=\"#ModalImpressao\" ";
                                echo "data-toggle=\"tooltip\" data-placement=\"left\" onclick=\"document.getElementById('doc').value='" . $vetor['idDocumento'] . "'\" ";
                                echo "title=\"Imprimir manual completo - Pronaos\"><i class=\"fa fa-print fa-white\"></i>&nbsp; Visualizar/Imprimir - [Pronaos]</button>";
                            }
                        }
                        ?>
                    </div>
                </div>
                <!--
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th><center>Cód.</center></th>
                                <th><center>Título</center></th>
                                <th><center>Ações</center></th>
                            </tr>
                        </thead>
                        <tbody>
            <?php
                /*$resultado = $documento->listaDocumento("Administrativo");
                $contador = 0;
                if($resultado) {
                    foreach ($resultado as $vetor) {
                          if($vetor['arquivo'] == "manual_administrativo") {
                              continue;
                          }
                          $contador++;
                        ?>
            <tr>
                <td><?php echo $contador; ?></td>
                <td><?php echo $vetor['descricao']; ?></td>
            <td><center>
            <?php
            /*
                if($vetor['linkVisualizacao'] != "") {
                    echo "<a class='btn btn-sm btn-primary btn-sucess' " . $vetor['linkVisualizacao'] . ">Visualizar</a>";
                }*/
            ?>
                <button class='btn btn-sm btn-success' <?php if($id==null) { echo "disabled=\"\""; } ?> data-toggle="modal" data-target="#ModalImpressao" data-toggle="tooltip" data-placement="left" onclick="document.getElementById('doc').value=<?php echo $vetor['idDocumento']; ?>" title="Impressão">
                    Visualizar
                </button>
            </center></td>
            </tr>
            <?php/*
                    }
                }*/
            ?>
                    </tbody>
        </table>
                </div>-->
               </div>
        </div>
    </div>
 </div>
    <?php
    /*
        if($id == null) {
            echo "<script type=\"text/javascript\">";
            echo "alert(\"Impressão desativada! Usuário não identificado.\");";
            echo "</script>";
        }
        else {
            if(!empty($doc)) {
                if($documento->buscarDados($doc)==false) {
                    echo "<script type=\"text/javascript\">";
                    echo "alert(\"Arquivo não encontrado: '" . $doc . "'\");";
                    echo "</script>";
                }
                else {
                    $token = impPdf($doc, $impressao, $documento, $usuario, $id, "topo&meio&rodape");
                    if ($token != null) {
                        echo "<script type=\"text/javascript\">var p=window.open(\"output/". $token .".pdf\");p.window.focus();p.print();";
                        echo "window.location='painelDeControle.php?corpo=biblioteca';</script>";
                    }
                    else {
                        echo "<script type=\"text/javascript\">alert(\"Erro ao gerar PDF!\");";
                        echo "window.location='painelDeControle.php?corpo=biblioteca';</script>";
                    }
                }
            }
        }
     */
    ?>
    <!-- Biblioteca INICIO
    <div class="css-treeview">
        <ul>
            <li><input type="checkbox" id="item-0" />
                <label for="item-0">Manual Administrativo</label>
                <ul>
                    <li><input type="checkbox" id="item-0-0" /><label for="item-0-0">Seção 01 - 010</label>
                        <ul>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/sljb/' data-width='1000' data-height='600'
                                   data-title='Seção 001 - Introdução'>
                                    Seção 001 - Introdução
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/cnzy/' data-width='1000' data-height='600' 
                                   data-title='Seção 002 - Administração dos Organismos Afiliados'>
                                    Seção 002 - Administração dos Organismos Afiliados
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/wpan/' data-width='1000' data-height='600' data-title='Seção 002 - Serviço Voluntário - Termo de Adesão - Anexo B'>
                                    Seção 002 - Serviço Voluntário - Termo de Adesão - Anexo B
                                </a>
                                <?php
                                /*
                                    if($id != null) {
                                        echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao0\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                                        echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                                    }
                                 *
                                 */
                                ?>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/iumk/' data-width='1000' data-height='600'
                                   data-title='Seção 003 - Lei de AMRA - Doações em geral'>
                                    Seção 003 - Lei de AMRA - Doações em geral
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/jsvp/' data-width='1000' data-height='600'
                                   data-title='Seção 004 - Autoridade'>
                                    Seção 004 - Autoridade
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/kxga/' data-width='1000' data-height='600'
                                   data-title='Seção 005 - Columbas'>
                                    Seção 005 - Columbas
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/qdul/' data-width='1000' data-height='600'
                                   data-title='Seção 006 - Comissões'>
                                    Seção 006 - Comissões
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/mxao/' data-width='1000' data-height='600'
                                   data-title='Seção 007 - Convocação Ritualística'>
                                    Seção 007 - Convocação Ritualística
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ijui/' data-width='1000' data-height='600'
                                   data-title='Seção 008 - Decoro'>
                                    Seção 008 - Decoro
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/onnm/' data-width='1000' data-height='600'
                                   data-title='Seção 009 - Arquivo e Documentação'>
                                    Seção 009 - Arquivo e Documentação
                                </a>
                            <?php
                            /*
                                if($id != null) {
                                     echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao22\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                                     echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                                }
                             * 
                             */
                             ?>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/bgco/' data-width='1000' data-height='600'
                                   data-title='Seção 010 - Elevação do Organismo Afiliado'>
                                    Seção 010 - Elevação do Organismo Afiliado
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><input type="checkbox" id="item-0-1" /><label for="item-0-1">Seção 011 - 023</label>
                        <ul>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/mres/' data-width='1000' data-height='600'
                                   data-title='Seção 011 - Eventos Rosacruzes Especiais'>
                                    Seção 011 - Eventos Rosacruzes Especiais
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/boge/' data-width='1000' data-height='600'
                                   data-title='Seção 012 - Expansão dos Organismos Afiliados'>
                                    Seção 012 - Expansão dos Organismos Afiliados
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/zzjq/' data-width='1000' data-height='600'
                                   data-title='Seção 013 - Jurisdição da Grande Loja'>
                                    Seção 013 - Jurisdição da Grande Loja
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/gjmb/' data-width='1000' data-height='600' 
                                   data-title='Seção 014 - Associados'>
                                    Seção 014 - Associados (Membros)
                                </a>
                            </li>
                            <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/xppn/' data-width='1000' data-height='600'
                                   data-title='Seção 015 - Oficiais'>
                                    Seção 015 - Oficiais
                                </a>
                            </li>
                            <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/bnrs/' data-width='1000' data-height='600' 
                                   data-title='Seção 016 - Calendário Anual das Atividades'>
                                    Seção 016 - Calendário Anual das Atividades
                                </a>
                            </li>
                            <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/kegt/' data-width='1000' data-height='600' 
                                   data-title='Seção 017 - Rituais'>
                                    Seção 017 - Rituais
                                </a>
                            </li>
                            <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/hbwz/' data-width='1000' data-height='600' 
                                   data-title='Seção 018 - Sede dos Organismos Afiliados'>
                                    Seção 018 - Sede dos Organismos Afiliados
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/qvoc/' data-width='1000' data-height='600' 
                                   data-title='Seção 019 - Suprimentos'>
                                    Seção 019 - Suprimentos
                                </a>
                            </li>
                            <li>
                                <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/zyef/' data-width='1000' data-height='600' 
                                   data-title='Seção 020 - Templo Rosacruz'>
                                    Seção 020 - Templo Rosacruz
                                </a>
                            </li>
                            <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/nbcy/' data-width='1000' data-height='600'
                                   data-title='Seção 021 - Atividades de Pronaos em Loja e Capítulo'>
                                    Seção 021 - Atividades de Pronaos em Loja e Capítulo
                                </a>
                            </li>
                            FIXME: Documento em Revisão
                            <li><a id="desativado" data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/uwai/' data-width='1000' data-height='600'
                                   data-title='Seção 022 - Ordem Guias do Grau (EM REVISÂO)'>
                            
                                    Seção 022 - Ordem Guias do Grau (EM REVISÂO)
                                </a>
                            
                            Seção 022 - Ordem Guias do Grau (EM REVISÂO)
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/eihc/' data-width='1000' data-height='600' 
                           data-title='Seção 023 - Artesão - Atividade da Classe dos Artesãos'>
                            Seção 023 - Artesão - Atividade da Classe dos Artesãos
                        </a>
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-0-2" /><label for="item-0-2">Seção 100 - 110</label>
                <ul>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/boed/' data-width='1000' data-height='600'
                           data-title='Seção 100 Construção da Sede Própria'>
                            Seção 100 - Contrução da Sede Própria
                        </a>
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/gine/' data-width='1000' data-height='600'
                           data-title='Seção 101 - Convenções Regionais e Encontros Rosacruzes'>
                            Seção 101 - Convenções Regionais e Encontros Rosacruzes
                        </a>
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/rnse/' data-width='1000' data-height='600'
                           data-title='Seção 102 - Instruções para a comissão de Verificação de Contas'>
                            Seção 102 - Instruções para a comissão de Verificação de Contas
                        </a>
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/mgpd/' data-width='1000' data-height='600'
                           data-title='Seção 103 - Grupo R+C de Divulgação'>
                            Seção 103 - Grupo R+C de Divulgação
                        </a>
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/qfum/' data-width='1000' data-height='600'
                           data-title='Seção 103 - Grupo R+C de Divulgação - Anexo'>
                            Seção 103 - Grupo R+C de Divulgação - Anexo
                        </a>
                        <?php
                        /*
                                if($id != null) {
                                    echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao1\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                                    echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                                }
                         * 
                         */
                        ?>
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/jdme/' data-width='1000' data-height='600'
                           data-title='Seção 104 - Coordenadores da Ordem Guias do Graal'>
                            Seção 104 - Coordenadores da Ordem Guias do Graal
                        </a>
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/tasn/' data-width='1000' data-height='600' 
                           data-title='Seção 105 - Manual da Columba'>
                            Seção 105 - Manual da Columba
                        </a>
                    </li>
                    <li><a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ixuv/' data-width='1000' data-height='600'
                           data-title='Seção 106 - Columbas - Instruções gerais para a Orientadora'>
                            Seção 106 - Columbas - Instruções gerais para a Orientadora
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/prtd/' data-width='1000' data-height='600'
                           data-title='Seção 107 - Boletim - Confecção'>
                            Seção 107 - Boletim - Confecção
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/tdjj/' data-width='1000' data-height='600' 
                           data-title='Seção 108 - Conforto - Instrução para o trabalho da Comissão'>
                            Seção 108 - Conforto - Instrução para o trabalho da Comissão
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/capb/' data-width='1000' data-height='600' 
                           data-title='Seção 109 - Providências Legais e Fiscais'>
                            Seção 109 - Providências Legais e Fiscais
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/btll/' data-width='1000' data-height='600'
                           data-title='Seção 110 - Palestras Públicas - Orientação'>
                            Seção 110 - Palestras Públicas - Orientação
                        </a>
                    </li>
                </ul>
            <li><input type="checkbox" id="item-0-3"><label for="item-0-3">Seção 111 - 120</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/gpxp/' data-width='1000' data-height='600'
                           data-title='Seção 111 - Recepção - Orientação para a comissão de recepção'>
                            Seção 111 - Recepção - Orientação para a comissão de recepção
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/bnik/' data-width='1000' data-height='600'
                           data-title='Seção 112 - Consultoria de Estudos Rosacruzes'>
                            Seção 112 - Consultoria de Estudos Rosacruzes
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/emkd/' data-width='1000' data-height='600'
                           data-title='Seção 113 - Monitores e Oradores Regionais'>
                            Seção 113 - Monitores e Oradores Regionais   
                        </a>
                    </li>
                    <li>
                        FIXME: Documento em revisão 
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/goxf/' data-width='1000' data-height='600'
                           data-title='Seção 114 - Instruções e Práticas Para a Ordem Guias do Graal (EM REVISÂO)'>
                        
                            Seção 114 - Instruções e Práticas Para a Ordem Guias do Graal (EM REVISÂO)
                        </a>
                       
                        Seção 114 - Instruções e Práticas Para a Ordem Guias do Graal (EM REVISÂO) 
                    </li>
                    <li>
                        <a name="desativado" data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/bwtm/' data-width='1000' data-height='600'
                           data-title='Seção 115 - ERIN'>
                            Seção 115 - ERIN
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/odgi/' data-width='1000' data-height='600'
                           data-title='Seção 115 -  ERIN / Anexo'>
                            Seção 115 -  ERIN / Anexo   
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao2\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    <li>
                        FIXME: Documento PDF em falta
                        Seção 116 - Centros Culturais (EM REVISÂO)
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/nfuh/' data-width='1000' data-height='600'
                           data-title='Seção 117.3a - Acordo de Afiliação - Modelo para Loja'>
                            Seção 117.3a - Acordo de Afiliação - Modelo para Loja
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/hqea/' data-width='1000' data-height='600'
                           data-title='Seção 117.3a - Anexo - Termo de Adesão e Concordância'>
                            Seção 117.3a - Anexo - Termo de Adesão e Concordância
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao3\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/yjfw/' data-width='1000' data-height='600'
                           data-title='Seção 117.3b - Acordo de Afiliação - Modelo para Capítulo'>
                            Seção 117.3b - Acordo de Afiliação - Modelo para Capítulo    
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/acoa/' data-width='1000' data-height='600'
                           data-title='Seção 117.3b - Anexo - Termo de Adesão e Concordância'>
                            Seção 117.3b - Anexo - Termo de Adesão e Concordância
                        </a>
                     <?php
                     /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao4\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                      * 
                      */
                    ?>
                    </li>
                    <li> FIXME: impressão
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/zfsq/' data-width='1000' data-height='600'
                           data-title='Seção 117.3c - Acordo de Afiliação - Modelo para Pronaos com CNPJ'>
                            Seção 117.3c - Acordo de Afiliação - Modelo para Pronaos com CNPJ
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao5\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/nekk/' data-width='1000' data-height='600'
                           data-title='Seção 118 - Estatuto para Pronaos (com CNPJ / Acordo)'>
                            Seção 118 - Estatuto para Pronaos (com CNPJ / Acordo)    
                        </a>
                    </li>
                    <li> FIXME: Falta o arquivo .pdf 120
                        Seção 120 - Incentivo Fiscal ao desenvolvimento da Cultura L.C.P (EM REVISÂO)
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-0-4"><label for="item-0-4">Seção 121 - 130</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/hbev/' data-width='1000' data-height='600'
                           data-title='Seção 121 - Cartões de Afiliação e Identificação'>
                            Seção 121 - Cartões de Afiliação e Identificação
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/djol/' data-width='1000' data-height='600'
                           data-title='Seção 122 - Imagem e Uso da Internet'>
                            Seção 122 - Imagem e Uso da Internet
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/muyv/' data-width='1000' data-height='600'
                           data-title='Seção 123 - Cursos Rosacruz'>
                            Seção 123 - Cursos Rosacruz    
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/xqjq/' data-width='1000' data-height='600'
                           data-title='Seção 125 - Espaço Físico - Construção e Reforma'>
                            Seção 125 - Espaço Físico - Construção e Reforma   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/hmfm/' data-width='1000' data-height='600'
                           data-title='Seção 126 - Manutenção no Organismo Afiliado'>
                            Seção 126 - Manutenção no Organismo Afiliado
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/eokh/' data-width='1000' data-height='600'
                           data-title='Seção 127 - Segurança no Organismo Afiliado'>
                            Seção 127 - Segurança no Organismo Afiliado   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/mmwq/' data-width='1000' data-height='600'
                           data-title='Seção 128 - Atividades Motivadoras'>
                            Seção 128 - Atividades Motivadoras
                        </a>
                    </li>
                </ul>

            </li>
            <li><input type="checkbox" id="item-0-5"><label for="item-0-5">Seção 170 - 209A</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/wzeh/' data-width='1000' data-height='600'
                           data-title='Seção 170 - Expansão Rosacruz'>
                            Seção 170 - Expansão Rosacruz 
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ifwa/' data-width='1000' data-height='600'
                           data-title='Seção 200 - Relatório de Atividades da Classe dos Artesãos'>
                            Seção 200 - Relatório de Atividades da Classe dos Artesãos
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ppvn/' data-width='1000' data-height='600'
                           data-title='Seção 201 - Ficha de Inscrição ao Organismo Afiliado - OGG'>
                            Seção 201 - Ficha de Inscrição ao Organismo Afiliado - Ordem Guias do Graal
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao6\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/tqif/' data-width='1000' data-height='600'
                           data-title='Seção 203 - Discriminação de Remessa e novo endereço'>
                            Seção 203 - Discrimação de Remessa e novo endereço
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ekjd/' data-width='1000' data-height='600'
                           data-title='Seção 204 - Ata de Reunião Administrativa Mensal'>
                            Seção 204 - Ata de Reunião Admistrativa Mensal
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao7\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/piyi/' data-width='1000' data-height='600'
                           data-title='Seção 205 - Relatório Mensal/Movimento Financeiro p/Loja, Capítulo e Pronaos'>
                            Seção 205 - Relatório Mensal/Movimento Financeiro p/Loja, Capítulo e Pronaos
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/buld/' data-width='1000' data-height='600'
                           data-title='Seção 206 - Ata de Posse - Loja e Capítulo'>
                            Seção 206 - Ata de Posse - Loja e Capítulo  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/yhuy/' data-width='1000' data-height='600'
                           data-title='Seção 207 - Ata de Posse - Pronaos Organismo (com CNPJ)'>
                            Seção 207 - Ata de Posse - Pronaos Organismo (com CNPJ)
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/mtsh/' data-width='1000' data-height='600'
                           data-title='Seção 208 - Certificado de Devolução e Reciclagem de Monografias'>
                            Seção 208 - Certificado de Devolução e Reciclagem de Monografias 
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/jhak/' data-width='1000' data-height='600'
                           data-title='Seção 209 - Questionário de Recomendação para Oficial Adm'>
                            Seção 209 - Questionário de Recomendação para Oficial Adm
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao8\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ifbd/' data-width='1000' data-height='600'
                           data-title='Seção 209A - LISTA TRÍPLICE - Nominata dos Candidatos a Oficiais adm'>
                            Seção 209A - LISTA TRÍPLICE - Nominata dos Candidatos a Oficiais adm  
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao9\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-0-6"><label for="item-0-6">Seção 210 - 222B</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/fwyr/' data-width='1000' data-height='600'
                           data-title='Seção 210 - Relação dos Oficiais em Exercício'>
                            Seção 210 - Relação dos Oficiais em Exercício 
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao10\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/szaj/' data-width='1000' data-height='600'
                           data-title='Seção 211 - Declaração I, II, III, IV e V (Relatório Anual)'>
                            Seção 211 - Declaração I, II, III, IV e V (Relatório Anual)
                        </a>
                   <?php
                   /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao11\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                    * 
                    */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/gwoo/' data-width='1000' data-height='600'
                           data-title='Seção 212 - Comissões - Relatório de Atividades'>
                            Seção 212 - Comissões - Relatório de Atividades 
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao12\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/gadk/' data-width='1000' data-height='600'
                           data-title='Seção 213 - Proposta de Afiliação ao Organismo Afiliado'>
                            Seção 213 - Proposta de Afiliação ao Organismo Afiliado
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao13\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/qphr/' data-width='1000' data-height='600'
                           data-title='Seção 214 - Columbas - Relatório Trimestral / Orientadora de Columba'>
                            Seção 214 - Columbas - Relatório Trimestral / Orientadora de Columba  
                        </a>
                   <?php
                   /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao14\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                    * 
                    */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/xlns/' data-width='1000' data-height='600'
                           data-title='Seção 215 - Columba - Relatório Anual'>
                            Seção 215 - Columba - Relatório Anual   
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao15\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/vtyt/' data-width='1000' data-height='600'
                           data-title='Seção 217 - Transferência de Membros - Associados'>
                            Seção 217 - Transferência de Membros - Associados 
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao16\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/zudr/' data-width='1000' data-height='600'
                           data-title='Seção 218 - Questionário de Recomendação de Monitor e Orador Regional'>
                            Seção 218 - Questionário de Recomendação de Monitor e Orador Regional   
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao17\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/qtgx/' data-width='1000' data-height='600'
                           data-title='Seção 222A - Relação Mensal dos Admitidos ao Pronaos em Lojas e Capítulos'>
                            Seção 222A - Relação Mensal dos Admitidos ao Pronaos em Lojas e Capítulos
                        </a>
                   <?php
                   /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao18\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                    * 
                    */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/etic/' data-width='1000' data-height='600'
                           data-title='Seção 222B - Relação Mensal dos Iniciados 1º a º12 Grau de Templo'>
                            Seção 222B - Relação Mensal dos Iniciados 1º a º12 Grau de Templo 
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao19\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-0-7"><label for="item-0-7">Seção 224 - 233</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/pfbw/' data-width='1000' data-height='600'
                           data-title='Seção 224 - Relatório Mensal - Ordem Guias do Graal'>
                            Seção 224 - Relatório Mensal - Ordem Guias do Graal 
                        </a>
                     <?php
                     /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao20\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                      * 
                      */
                    ?>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ovri/' data-width='1000' data-height='600'
                           data-title='Seção 230 - URCI - Universidade Rosecroix Internacional'>
                            Seção 230 - URCI - Universidade Rosecroix Internacional  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/qfqh/' data-width='1000' data-height='600'
                           data-title='Seção 233 - Relatório Mensal de Atividades de Pronaos'>
                            Seção 233 - Relatório Mensal de Atividades de Pronaos   
                        </a>
                    <?php
                    /*
                        if($id != null) {
                            echo "<button class=\"btn btn-sm btn-link\" data-toggle=\"modal\" data-target=\"#ModalImpressao21\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Impressão\">";
                            echo "<i class=\"fa fa-print fa-white\"></i>&nbsp;</button>";
                        }
                     * 
                     */
                    ?>
                    </li>
                </ul>
            </li>
        </ul>

    </li>
    <li><input type="checkbox" id="item-1" /><label for="item-1">Manual Ritualístico</label>
        <ul>
            <li><input type="checkbox" id="item-1-0"><label for="item-1-0">Seção 303 - 320</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/oakj/' data-width='1000' data-height='600'
                           data-title='Seção 303 - Montagem de Pronaos'>
                            Seção 303 - Montagem de Pronaos
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/oixd/' data-width='1000' data-height='600'
                           data-title='Seção 310 - Cerimônia de Ano Novo R+C e Instalação de Novos Oficiais (Pronaos)'>
                            Seção 310 - Cerimônia de Ano Novo R+C e Instalação de Novos Oficiais (Pronaos) 
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/lylo/' data-width='1000' data-height='600'
                           data-title='Seção 311 - Cerimônia de Ano Novo R+C para Comissão de Pronaos em Capítulo e Loja'>
                            Seção 311 - Cerimônia de Ano Novo R+C para Comissão de Pronaos em Capítulo e Loja 
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/yfpg/' data-width='1000'
                           data-height='600' data-title='Seção 312 - Fórum de Grau para Pronaos'>
                            Seção 312 - Fórum de Grau para Pronaos  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/jmvt/' data-width='1000' data-height='600'
                           data-title='Seção 313 - Ritual de Convocação para Pronaos'>
                            Seção 313 - Ritual de Convocação para Pronaos
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/tcbt/' data-width='1000' data-height='600'
                           data-title='Seção 314 - Discurso de Admissão para Pronaos'>
                            Seção 314 - Discurso de Admissão para Pronaos 
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/yjgx/' data-width='1000' data-height='600'
                           data-title='Seção 315 - Festa da Luz para Pronaos'>
                            Seção 315 - Festa da Luz para Pronaos  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/xkts/' data-width='1000' data-height='600'
                           data-title='Seção 316 - Instalação de Oficiais no Decurso do Ano para Comissão de Pronaos'>
                            Seção 316 - Instalação de Oficiais no Decurso do Ano para Comissão de Pronaos em Lojas e Capítulos 
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/yuqs/' data-width='1000' data-height='600'
                           data-title='Seção 317 - Ritual Fúnebre R+C nº 1 para Pronaos'>
                            Seção 317 - Ritual Fúnebre R+C nº 1 para Pronaos  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/kuju/' data-width='1000' data-height='600'
                           data-title='Seção 318 - Ritual Fúnebre R+C nº 2 para Pronaos'>
                            Seção 318 - Ritual Fúnebre R+C nº 2 para Pronaos 
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/lvlq/' data-width='1000' data-height='600'
                           data-title='Seção 319 - Ritual Fúnebre R+C nº 3 para Pronaos'>
                            Seção 319 - Ritual Fúnebre R+C nº 3 para Pronaos  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/chcx/' data-width='1000' data-height='600'
                           data-title='Seção 320 - Ritual de Inumação ou Cremação para Loja / Capítulo e Pronaos'>
                            Seção 320 - Ritual de Inumação ou Cremação para Loja / Capítulo e Pronaos  
                        </a>
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-1-1"><label for="item-1-1">Seção 334 - 363</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/cwds/' data-width='1000' data-height='600'
                           data-title='Seção 334 - Conscientização Ecológica na OGG'>
                            Seção 334 - Conscientização Ecológica na OGG  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/eqye/' data-width='1000' data-height='600'
                           data-title='Seção 343 - Montagem do Templo - Loja e Capítulo'>
                            Seção 343 - Montagem do Templo - Loja e Capítulo  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/hnmn/' data-width='1000' data-height='600'
                           data-title='Seção 352 - Inst. Conf. Vestes e Insígnias de GCS e Monitores'>
                            Seção 352 - Inst. Conf. Vestes e Insígnias de GCS e Monitores  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/tfds/' data-width='1000' data-height='600'
                           data-title='Seção 354 - Instrução para Confecção das Vestes dos Oficiais - Loja/Capítulo'>
                            Seção 354 - Instrução para Confecção das Vestes dos Oficiais - Loja/Capítulo
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/grye/' data-width='1000' data-height='600'
                           data-title='Seção 359 - Sons Vocálicos para Loja e Capítulo'>
                            Seção 359 - Sons Vocálicos para Loja e Capítulo  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/epqh/' data-width='1000' data-height='600'
                           data-title='Seção 360 - Sons Vocálicos para Pronaos'>
                            Seção 360 - Sons Vocálicos para Pronaos
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/bkip/' data-width='1000' data-height='600'
                           data-title='Seção 361 - Cerimônia Anual da Columba - Loja e Capítulo'>
                            Seção 361 - Cerimônia Anual da Columba - Loja e Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/swce/' data-width='1000' data-height='600'
                           data-title='Seção 362 - Ritual da Classe dos Artesãos'>
                            Seção 362 - Ritual da Classe dos Artesãos   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/raco/' data-width='1000' data-height='600'
                           data-title='Seção 363 - Cerimônia de Ano Novo R+C e Instalação de Novos Oficiais para Loja e Capítulo'>
                            Seção 363 - Cerimônia de Ano Novo R+C e Instalação de Novos Oficiais para Loja e Capítulo  
                        </a>
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-1-2"><label for="item-1-2">Seção 364 - 374</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ocah/' data-width='1000' data-height='600'
                           data-title='Seção 364 - Cerimônia de Aposição de Nome - Loja e Capítulo'>
                            Seção 364 - Cerimônia de Aposição de Nome - Loja e Capítulo  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/zozb/' data-width='1000' data-height='600'
                           data-title='Seção 365 - Cerimônia de Casamento Rosacruz - Loja e Capítulo'>
                            Seção 365 - Cerimônia de Casamento Rosacruz - Loja e Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/oejw/' data-width='1000' data-height='600'
                           data-title='Seção 366 - Cerimônia &quot;In Memoriam&quot; - para Loja / Capítulo e Pronaos'>
                            Seção 366 - Cerimônia &quot;In Memoriam&quot; - para Loja / Capítulo e Pronaos   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/lodv/' data-width='1000' data-height='600'
                           data-title='Seção 367 - Festa da Luz para Loja e Capítulo'>
                            Seção 367 - Festa da Luz para Loja e Capítulo  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/scny/' data-width='1000' data-height='600'
                           data-title='Seção 368 - Invocação para Loja, Capítulo e Pronaos'>
                            Seção 368 - Invocação para Loja, Capítulo e Pronaos  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/kded/' data-width='1000' data-height='600'
                           data-title='Seção 369 - Ritual de Convocação para Loja e Capítulo'>
                            Seção 369 - Ritual de Convocação para Loja e Capítulo
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/lxgw/' data-width='1000' data-height='600'
                           data-title='Seção 370 - Discurso de Orientação de Loja e Capítulo'>
                            Seção 370 - Discurso de Orientação de Loja e Capítulo
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/wdnf/' data-width='1000' data-height='600'
                           data-title='Seção 371 - Iniciação à Loja ou Capítulo'>
                            Seção 371 - Iniciação à Loja ou Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/jmek/' data-width='1000' data-height='600'
                           data-title='Seção 372 - Instalação de uma Columba para Loja e Capítulo'>
                            Seção 372 - Instalação de uma Columba para Loja e Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/okep/' data-width='1000' data-height='600'
                           data-title='Seção 373 - Instalação de Várias Columbas para Loja e Capítulo'>
                            Seção 373 - Instalação de Várias Columbas para Loja e Capítulo    
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/azux/' data-width='1000' data-height='600'
                           data-title='Seção 374 - Instalação de um Oficial no Decurso do Ano R+C'>
                            Seção 374 - Instalação de um Oficial no Decurso do Ano R+C    
                        </a>
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-1-3"><label for="item-1-3">Seção 375 - 397</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ekbf/' data-width='1000' data-height='600'
                           data-title='Seção 375 - Ritual Fúnebre R+C Nº 1 para Loja e Capítulo (CORPO PRESENTE NO ORGANISMO)'>
                            Seção 375 - Ritual Fúnebre R+C Nº 1 para Loja e Capítulo  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/sent/' data-width='1000' data-height='600'
                           data-title='Seção 376 - Ritual Fúnebre R+C Nº 2 para Loja e Capítulo (CORPO PRESENTE FORA DO ORGANISMO)'>
                            Seção 376 - Ritual Fúnebre R+C Nº 2 para Loja e Capítulo    
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/zocy/' data-width='1000' data-height='600'
                           data-title='Seção 377 - Ritual Fúnebre R+C Nº 3 para Loja e Capítulo (APÓS MORTE ATÉ 1 MÊS C/ FOTO)'>
                            Seção 377 - Ritual Fúnebre R+C Nº 3 para Loja e Capítulo
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/sksx/' data-width='1000' data-height='600'
                           data-title='Seção 378 - Cerimônia de Ação de Graças para Loja e Capítulo'>
                            Seção 378 - Cerimônia de Ação de Graças para Loja e Capítulo    
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/cmzl/' data-width='1000' data-height='600'
                           data-title='Seção 379 - Ritual Pitagórico. Loja e Capítulo'>
                            Seção 379 - Ritual Pitagórico. Loja e Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/cpaa/' data-width='1000' data-height='600'
                           data-title='Seção 380 - Incensamento Duplo para Loja e Capítulo'>
                            Seção 380 - Incensamento Duplo para Loja e Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/tsog/' data-width='1000' data-height='600'
                           data-title='Seção 381 - Atuação de Quatro Columbas em Ritual Conv. e Convenções'>
                            Seção 381 - Atuação de Quatro Columbas em Ritual Conv. e Convenções
                        </a>    
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/ixnd/' data-width='1000' data-height='600'
                           data-title='Seção 382 - Fórum de Grau para Loja e Capítulo'>
                            Seção 382 - Fórum de Grau para Loja e Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/idgb/' data-width='1000' data-height='600'
                           data-title='Seção 386 - Cerimônia de Despedida de Columba'>
                            Seção 386 - Cerimônia de Despedida de Columba   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/sqjj/' data-width='1000' data-height='600'
                           data-title='Seção 387 - Instalação MONITOR'>
                            Seção 387 - Instalação MONITOR  
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/wdao/' data-width='1000' data-height='600'
                           data-title='Seção 392 - Meditação &quot;Luz, Vida e Amor&quot; - Loja e Capítulo'>
                            Seção 392 - Meditação &quot;Luz, Vida e Amor&quot; - Loja e Capítulo   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/oegi/' data-width='1000' data-height='600'
                           data-title='Seção 392A - Meditação &quot;Luz, Vida, Amor&quot; - Pronaos'>
                            Seção 392A - Meditação &quot;Luz, Vida, Amor&quot; - Pronaos   
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/kapc/' data-width='1000' data-height='600'
                           data-title='Seção 395 - Ritual de Harmonização c/ Conselho de Solace da GLP (Loja e Capítulo)'>
                            Seção 395 - Ritual de Harmonização c/ Conselho de Solace da GLP (Loja e Capítulo)    
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/wrom/' data-width='1000' data-height='600'
                           data-title='Seção 396 - Instruções para Confecção da Veste de Columba'>
                            Seção 396 - Instruções para Confecção da Veste de Columba
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/nsoi/' data-width='1000' data-height='600'
                           data-title='Seção 397 - Meditação para a Paz - Loja, Capítulo e Pronaos'>
                            Seção 397 - Meditação para a Paz - Loja, Capítulo e Pronaos   
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li><input type="checkbox" id="item-2" />
        <label for="item-2">Manual Iniciático</label>
        <ul>
            <li><input type="checkbox" id="item-2-0"><label for="item-2-0">Introdução</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/segi/' data-width='1000' data-height='600'
                           data-title='Seção 400 - Processo Iniciático Rosacruz - Introdução'>
                            Seção 400 - Processo Iniciático Rosacruz   
                        </a>
                    </li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-2-0"><label for="item-2-0">Seções - 1º ao 6º Grau</label>
                <ul>

                </ul>
            </li>
            <li><input type="checkbox" id="item-2-1"><label for="item-2-0">Seções - 7º ao 12º Grau</label>
                <ul>

                </ul>
            </li>
        </ul>
    </li>
    <li><input type="checkbox" id="item-0" />
        <label for="item-0">Manual Financeiro</label>
        <ul>
            <li><input type="checkbox" id="item-0-0" /><label for="item-0-0">Seção 01 - 010</label>
                <ul>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/sljb/' data-width='1000' data-height='600' data-title='seção 001'>
                            Seção 001
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/cnzy/' data-width='1000' data-height='600' data-title='seção 002'>
                            Seção 002
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/iumk/' data-width='1000' data-height='600' data-title='seção 003'>
                            Seção 003
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/jsvp/' data-width='1000' data-height='600' data-title='seção 004'>
                            Seção 004
                        </a>
                    </li>
                    <li>
                        <a data-rel='fh5-light-box-demo' data-href='http://online.pubhtml5.com/umzz/kxga/' data-width='1000' data-height='600' data-title='seção 005'>
                            Seção 005
                        </a>
                    </li>

                </ul>
            </li>
            <li><input type="checkbox" id="item-0-1" /><label for="item-0-2">Seção 011 - 020</label>
                <ul>
                    <li><a >Seção 011</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                    <li><a href="">item</a></li>
                </ul>
            </li>
            <li><input type="checkbox" id="item-0-2" disabled="disabled" /><label for="item-0-2">Seção</label>
                <ul>
                    <li><a href="./">item</a></li>
                    <li><a href="./">item</a></li>
                    <li><a href="./">item</a></li>
                    <li><a href="./">item</a></li>
                    <li><a href="./">item</a></li>
                    <li><a href="./">item</a></li>
                    <li><a href="./">item</a></li>
                    <li><a href="./">item</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li><input type="checkbox" id="item-0-2" /><label for="item-0-2">Manual SOA</label>
        <ul>
            <li><a href="./">item</a></li>
            <li><a href="./">item</a></li>
            <li><a href="./">item</a></li>
            <li><a href="./">item</a></li>
            <li><a href="./">item</a></li>
            <li><a href="./">item</a></li>
            <li><a href="./">item</a></li>
            <li><a href="./">item</a></li>
        </ul>
    </li>
</ul>
</div>
  Biblioteca FIM -->
    
<!-- MODAL Início -->
<div class="modal inmodal" id="ModalImpressao" tabindex="-1" role="dialog"  aria-hidden="true">
     <div class="modal-dialog modal-lg" style="width: 50%">
        <div class="modal-content animated bounceInUp">
          <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <i class="fa fa-print modal-icon"></i>
              <h4 class="modal-title">Impressão</h4>
          </div>
          <div class="modal-body">
            <form id="imp3" name="imp3" class="form-horizontal" method="post" enctype="multipart/form-data">
              <div class="form-group">
              <div class="col-sm-10">
                  <h1>Solene Advertência</h1>
              </div>
              </div>
              <div class="form-group">
                    <div class="col-sm-12">
                        <textarea class="form-control" style="width: 100%; background-color: white; font-family: cursive"
                                rows="28" readonly=""><?php echo $textoImpressao; ?></textarea><br>
                        <input type="radio" name="termo" id="termo" value="1" />Eu concordo  
                        <input type="radio" name="termo" id="termo" value="0" checked="" />Eu não concordo
                    </div>
             </div>
            <input type="hidden" name="doc" id="doc" value="">
            </form>
                <div class="modal-footer">
                    <input type="submit" name="okImprimir" id="okImprimir" class="btn btn-warning" value="Imprimir" onclick="validaConcordanciaImpressao()">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
 </div>
<!-- MODAL Fim -->
<?php
/*
  $resultado = $documento->listaDocumento("Administrativo");
  if($id != null && $resultado) {
      $dadosUsuario = $usuario->buscaUsuario($id);
      if($dadosUsuario) {
            $nomeUser = $dadosUsuario->getNomeUsuario();
            $emailUser = $dadosUsuario->getEmailUsuario();
            $codigoDeAfiliacaoUser = $dadosUsuario->getCodigoDeAfiliacaoUsuario();
      }
      foreach ($resultado as $vetor) {
          ?>
          <div class="modal inmodal" id="ModalImpressao<?php echo $vetor['idDocumento']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-print modal-icon"></i>
                <h4 class="modal-title">Impressão</h4>
            </div>
            <div class="body">
                <center>
                    <h1 class="modal-title">Dados do Usuário</h1>
                </center><br>
              <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nome:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><?php echo $nomeUser; ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email: </label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><?php echo $emailUser; ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">IP: </label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><?php echo $_SERVER['REMOTE_ADDR']; ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Código de Afiliação: </label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><?php echo $codigoDeAfiliacaoUser; ?></div>
                        </div>
                    </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Documento: </label>
                      <div class="col-sm-9">
                          <div class="form-control-static"><?php echo $vetor['arquivo']; ?></div>
                      </div>
                  </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Termos de Uso: </label>
                    <div class="form-control-static"></div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white"
                        data-dismiss="modal"
                        onclick="window.location = 'painelDeControle.php?corpo=biblioteca&doc=<?php echo $vetor['arquivo']; ?>'">Ok! Concordo!</button>
            </div>
        </div>
    </div>
 </div>
<?php
      }
  }
 * 
 */
?>
    

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
</body>
</html>