<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
					<!-- Conteúdo DE INCLUDE INICIO -->
					<?php 
					$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
					if($salvo==1){?>
					<script>
					window.onload = function(){
						swal({
					        title: "Sucesso!",
					        text: "Cadastro do Oficial realizado com sucesso!",
					        type: "success",
					        confirmButtonColor: "#1ab394"
					    });
					}
					</script>
					<?php }
                                        $erroWebservice = isset($_REQUEST['erroWebservice'])?$_REQUEST['erroWebservice']:null;
                                        if($erroWebservice!=null){?>
					<script>
					window.onload = function(){
						swal({
					        title: "Aviso!",
					        text: "Membro não atua no Organismo Afiliado indicado!",
					        type: "warning",
					        confirmButtonColor: "#1ab394"
					    });
					}
					</script>
					<?php }
					
					$funcoesUsuarioString = isset($_SESSION['funcoes']) ? $_SESSION['funcoes'] : null;
					$arrFuncoes = explode(",", $funcoesUsuarioString);
					//echo "<pre>";print_r($arrFuncoes);
					?>
					<!-- Caminho de Migalhas Início -->
					<div class="row wrapper border-bottom white-bg page-heading">
					    <div class="col-lg-10">
					        <h2>Oficiais</h2>
					        <ol class="breadcrumb">
					            <li>
					                <a href="">Home</a>
					            </li>
					            <li class="active">
					                <a href="">Oficiais</a>
					            </li>
					        </ol>
					    </div>
					    <div class="col-lg-2">
					    </div>
					</div>
					<!-- Caminho de Migalhas Fim -->
					
					<!-- Tabela Início -->
					<div class="wrapper wrapper-content animated fadeInRight">
					    <div class="row">
					        <div class="col-lg-12">
					            <div class="ibox float-e-margins">
					                <div class="ibox-title">
					                    <h5>Consulta de Oficiais</h5>
					                    <div class="ibox-tools">
					                    	<?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2) || in_array("151",$arrFuncoes)){ ?>
					                    	<a class="btn btn-xs btn-info" href="?corpo=buscaOficial&listarGrandesConselheirosAtuantes=1" style="color:white;">
					                            <i class="fa fa-mail-forward"></i> Listar Grandes Conselheiros Atuantes
					                        </a>
					                        &nbsp;
					                    	<a class="btn btn-xs btn-danger" href="?corpo=buscaOficial&listarMonitoresAtuantes=1" style="color:white;">
					                            <i class="fa fa-mail-forward"></i> Listar Monitores Atuantes
					                        </a>
					                        &nbsp;
					                    	<a class="btn btn-xs btn-primary" href="?corpo=buscaOficial&listarOradoresAtuantes=1" style="color:white;">
					                            <i class="fa fa-mail-forward"></i> Listar Oradores Atuantes
					                        </a>
					                        &nbsp;
					                        <?php }?>
					                    	<a class="btn btn-xs btn-warning" href="?corpo=buscaOficial&listarAtuantes=1" style="color:white;">
					                            <i class="fa fa-mail-forward"></i> Listar Atuantes
					                        </a>
					                        &nbsp;
					                    	<a class="btn btn-xs btn-danger" href="?corpo=buscaOficial&listarNaoAtuantes=1" style="color:white;">
					                            <i class="fa fa-mail-forward"></i> Listar Não Atuantes
					                        </a>
					                        <?php if(in_array("1",$arrNivelUsuario)){?>
					                        &nbsp;
					                        <a class="btn btn-xs btn-primary" href="?corpo=atualizaQuadroOficiais">
					                            <i class="fa fa-plus"></i> Cadastrar Oficial
					                        </a>
					                        <?php }?>
					                    </div>
					                </div>
					                <div class="ibox-content">
					                    <table class="table table-striped table-bordered table-hover dataTables-example" >
					                        <thead>
					                            <tr>
					                                <th>Cod.</th>
													<th>Nome</th>
													<th><center>Função</center></th>
													<th><center>Entrada</center></th>
													<th><center>Saída</center></th>
													<th><center>Término de Mandato</center></th>
													<th><center>Região</center></th>
													<!--<th><center>Organismo Afiliado</center></th>-->
													<th><center>Info</center></th>
						                        </tr>
					                        </thead>
					                        <?php 
					                        $nomeParcialFuncao="";
					                        $gc =isset($_REQUEST['listarGrandesConselheirosAtuantes'])?'GRANDE CONSELHEIRO REGIONAL':null;
					                        $monitor=isset($_REQUEST['listarMonitoresAtuantes'])?'MONITOR':null;
					                        $orador = isset($_REQUEST['listarOradoresAtuantes'])?'ORADOR':null;
					                        $listarAtuantes 	= isset($_REQUEST['listarAtuantes'])?$_REQUEST['listarAtuantes']:null;
					                        $listarNaoAtuantes 	= isset($_REQUEST['listarNaoAtuantes'])?$_REQUEST['listarNaoAtuantes']:null;
					                        $ocultar_json=1;
					                        
					                        if($gc==null&&$monitor==null&&$orador==null)
					                        {
						                        if($listarAtuantes==null&&$listarNaoAtuantes==null)
						                        {
						                        	$naoAtuantes='S';
						                        }else{
						                        	if($listarAtuantes!=null)
						                        	{
						                        		$atuantes='S';
						                        		$naoAtuantes='N';
						                        	}else{
						                        		if($listarNaoAtuantes!=null)
						                        		{
						                        			$atuantes='N';
						                        			$naoAtuantes='S';
						                        		}
						                        	}
						                        }
					                        }else{ 
						                        if($gc!=null)
						                        {
						                        	$nomeParcialFuncao 	= $gc;
						                        	$atuantes='S';
						                        	$naoAtuantes='N';
						                        	$seqFuncao=151;
						                        }
						                        if($monitor!=null)
						                        {
						                        	$nomeParcialFuncao 	= $monitor;
						                        	$atuantes='S';
						                        	$naoAtuantes='N';
						                        } 
						                        if($orador!=null)
						                        {
						                        	$nomeParcialFuncao 	= $orador;
						                        	$atuantes='S';
						                        	$naoAtuantes='N';
						                        	$seqFuncao=177;
						                        }
					                        }
					                        if($nomeParcialFuncao=="")
					                        {
					                        	$siglaOA=strtoupper($sessao->getValue("siglaOA"));
					                        }
					                        ?>
						                        <tbody>
						                        	<?php
					                        //echo "---->".$nomeParcialFuncao;
					                       // if($listarNaoAtuantes==1||$listarNaoAtuantes==1||$gc!=null||$monitor!=null||$orador!=null)
					                        //{
						                        include './js/ajax/retornaFuncaoMembro.php';
						                        $obj = json_decode(json_encode($return),true);
						                        //echo "<pre>";print_r($obj);
						                         
						                        	
						                        	foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor)
						                        	{
						                        		if($vetor['fields']['fCodMembro']!=0)
						                        		{
						                        			
								                        	?>
															<tr>
																<td>
																	<?php echo $vetor['fields']['fCodMembro'];?>
																</td>
																<td>
																	<?php echo $vetor['fields']['fNomClient'];?>
																</td>
																<td>
																	<?php echo $vetor['fields']['fDesFuncao'];?>
																</td>
																<td><center>
																	<?php if($vetor['fields']['fDatEntrad']!=""){echo substr($vetor['fields']['fDatEntrad'],8,2)."/".substr($vetor['fields']['fDatEntrad'],5,2)."/".substr($vetor['fields']['fDatEntrad'],0,4); }else{ echo "--";}?>
																	</center>
																</td>
																<td>
																	<center>
																	<?php if($vetor['fields']['fDatSaida']!=""){ echo substr($vetor['fields']['fDatSaida'],8,2)."/".substr($vetor['fields']['fDatSaida'],5,2)."/".substr($vetor['fields']['fDatSaida'],0,4); }else{ echo "--";}?>
																	</center>
																</td>
																<td>
																	<center>
																	<?php if($vetor['fields']['fDatTerminMandat']!=""){ echo substr($vetor['fields']['fDatTerminMandat'],8,2)."/".substr($vetor['fields']['fDatTerminMandat'],5,2)."/".substr($vetor['fields']['fDatTerminMandat'],0,4); }else{ echo "--";}?>
																	</center>
																</td>
																<td>
																	<center>
																	<?php if($vetor['fields']['fSigOrgafi']!=""){ echo substr($vetor['fields']['fSigOrgafi'],0,3); }else{ echo "--";}?>
																	</center>
																</td>
																<!--  
																<td>
																	<?php //echo $nomeOrganismo;?>
																</td>
																-->
																<td><center>
																	<div id="acoes_confirma_cancela">
																		<a class="btn btn-xs btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['fields']['fSeqCadast'];?>_<?php echo $vetor['fields']['fCodFuncao'];?>">
																			<i class="icon-edit icon-white"></i>  
																			Detalhes                                            
																		</a>
																	</div>
																	</center>
																</td>
															</tr>
													<?php 	
                        							                        		}
                        							                        	}?>
					                        </tbody>
					                    </table>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
					<!-- Tabela Fim -->
					
					<!-- Modal's da página INICIO -->
				<?php 
				foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor)
				{
                	if($vetor['fields']['fCodMembro']!=0)
                	{
						
				?>	
					<!-- Modal de detalhes do oficial -->
					<div class="modal inmodal" id="myModaInfo<?php echo $vetor['fields']['fSeqCadast'];?>_<?php echo $vetor['fields']['fCodFuncao'];?>" tabindex="-1" role="dialog"  aria-hidden="true">
					    <div class="modal-dialog modal-lg">
					        <div class="modal-content animated fadeIn">
				                <div class="modal-header">
				                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				                    <h4 class="modal-title"><i class="fa fa-user"></i> Detalhes do Oficial</h4>
				                </div>
				                <div class="modal-body">
				                    <div class="row">
				                    	<div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Organismo: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fNomeOa'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Nome: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fNomClient'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Código de Afiliação: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fCodMembro'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Função Exercida: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fDesFuncao'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Data de Entrada: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php if($vetor['fields']['fDatEntrad']!=""){echo substr($vetor['fields']['fDatEntrad'],8,2)."/".substr($vetor['fields']['fDatEntrad'],5,2)."/".substr($vetor['fields']['fDatEntrad'],0,4); }else{ echo "--";}?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Data de Saída: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php if($vetor['fields']['fDatSaida']!=""){ echo substr($vetor['fields']['fDatSaida'],8,2)."/".substr($vetor['fields']['fDatSaida'],5,2)."/".substr($vetor['fields']['fDatSaida'],0,4); }else{ echo "--";}?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Término do Mandato: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php if($vetor['fields']['fDatTerminMandat']!=""){ echo substr($vetor['fields']['fDatTerminMandat'],8,2)."/".substr($vetor['fields']['fDatTerminMandat'],5,2)."/".substr($vetor['fields']['fDatTerminMandat'],0,4); }else{ echo "--";}?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">E-mail: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fDesEmail'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
                                                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Telefone: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fNumTelefo'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
                                                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Telefone Fixo: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fNumTelefoFixo'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
                                                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Telefone Celular: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fNumCelula'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
                                                        <div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Outro Telefone: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['fields']['fNumOutroTelefo'];?>
                                                                                        <?php echo $vetor['fields']['fDesOutroTelefo'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
                                                        
				                        <?php 
				                        include_once("./model/usuarioClass.php");
										$u = new Usuario();
										$resultado = $u->buscaUsuario($vetor['fields']['fSeqCadast']);
										if($resultado)
										{
											foreach ($resultado as $vetor)
											{
				                        ?>
										<div class="row">
					                        <div class="form-group">
					                            <label class="col-sm-4 control-label" style="text-align: right">Telefones no SOA: </label>
					                            <div class="col-sm-8">
					                                <div class="col-sm-12" style="max-width: 320px">
						                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
						                                	<?php echo $vetor['telefoneResidencialUsuario'];?>
                                                                                        <?php echo $vetor['telefoneComercialUsuario'];?>
                                                                                        <?php echo $vetor['celularUsuario'];?>
						                                </div>
					                                </div>
					                            </div>
					                        </div>
				                        </div>
				                        <?php 
											}
                						}
				                        ?>             
				                    </div>
				                </div>
				                <div class="modal-footer">
				                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
				                </div>
					        </div>
					    </div>
					</div>
					<!-- Modal's da página FIM -->
				<?php 
                	}
				}
				?>
				<!-- Conteúdo DE INCLUDE FIM -->

<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

        /* Init DataTables */
        var oTable = $('#editable').dataTable();

        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });


    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row"]);

    }
</script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>		