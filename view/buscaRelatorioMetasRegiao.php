<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório de Metas por Região</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Relatório de Metas por Região</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Relatório de Metas por Região</h5>
                    <div class="ibox-tools">
                   
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Região</th>
                                <th>Total de Metas Estabelecidas</th>
                                <th>Total de Metas Alcançadas</th>
                                <th>Total de Metas Não Alcançadas</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                        
set_time_limit(0);
//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );

include_once('lib/functions.php');
include_once('lib/phpmailer/class.phpmailer.php');
include_once('model/regiaoRosacruzClass.php');
include_once("model/planoAcaoRegiaoMetaClass.php");
include_once('model/usuarioClass.php');
																				
							$regiaoRosacruz = new regiaoRosacruz();
					
							$resultadoReg = $regiaoRosacruz->listaRegiaoRosacruz();
							
							if($resultadoReg)
							{
								foreach($resultadoReg as $vetorReg)
								{
									$regiao = $vetorReg['idRegiaoRosacruz'];
									$regiaoRosacruz = $vetorReg['regiaoRosacruz'];
									$total=0;//Contador de relatórios NÃO entregues
									
									/**
									 * Total de Metas Estabelecidas
									 */
									$parm = new planoAcaoRegiaoMeta();
									$resultadoMeta = $parm->listaMetaRegiao($regiao,0);
									if($resultadoMeta)
									{
										$totalMetasEstabelecidas = count($resultadoMeta);
									}else{
										$totalMetasEstabelecidas = 0;
									}
									
									/**
									 * Total de Metas Alcançadas
									 */
									$parm = new planoAcaoRegiaoMeta();
									$resultadoMeta = $parm->listaMetaRegiao($regiao,1);
									if($resultadoMeta)
									{
										$totalMetasAlcancadas = count($resultadoMeta);
									}else{
										$totalMetasAlcancadas = 0;
									}
                                                                        
                                                                        /**
									 * Total de Metas Não Alcançadas
									 */
									$parm = new planoAcaoRegiaoMeta();
									$resultadoMeta2 = $parm->listaMetaRegiao($regiao,null,null,1);
									if($resultadoMeta2)
									{
										$totalMetasNaoAlcancadas = count($resultadoMeta2);
									}else{
										$totalMetasNaoAlcancadas = 0;
									}
                            
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $regiaoRosacruz; ?>
                                        </td>
                                        <td>
                                            <?php 
                                            	echo $totalMetasEstabelecidas; 
											?>
                                        </td>
                                        <td>
                                            <?php 
												echo $totalMetasAlcancadas;
                                            ?>
                                        </td>
                                        <td>
                                            <?php 
												echo $totalMetasNaoAlcancadas;
                                            ?>
                                        </td>
                                </tr>
                            <?php
                            }
                        }
		
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

