<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>

<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Isenção concedida com sucesso ao membro!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Isenção já cadastrada anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$excluido = isset($_REQUEST['excluido'])?$_REQUEST['excluido']:null;
if($excluido==1){?>
<script>
window.onload = function(){
	swal({title:"Sucesso!",text:"Isenção excluída com sucesso.",type:"success",confirmButtonColor:"#1ab394"});
}
</script>
<?php }?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Isenção dos Membros do Organismo Afiliado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaMembroOA">Isenção dos Membros do Organismo Afiliado</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Isenção dos Membros do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroIsencaoOA">
                            <i class="fa fa-plus"></i> Conceder Isenção
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód. de Afiliação</th>
                                <th>Nome</th>
                                <th><center>Inicio da Isenção</center></th>
                                <th><center>Fim da Isenção</center></th>
                                <th><center>Motivo</center></th>
		                        <th>Organismo Afiliado</th>
		                        <th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/isencaoOAController.php");

                            $i = new isencaoOAController();
                            $resultado = $i->listaIsencaoOA();
							
                            if ($resultado) {
                                foreach ($resultado as $vetor) {

                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                                    ?>
                               <?php if(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA"))){?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['codigoAfiliacao']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['nomeMembroOa']; ?>
                                        </td>
                                        <td>
                                        	<center>
                                            	<?php echo substr($vetor['dataInicial'],8,2)."/".substr($vetor['dataInicial'],5,2)."/".substr($vetor['dataInicial'],0,4);?>
                                            </center>
                                        </td>
                                        <td>
                                        	<center>
                                            	<?php echo substr($vetor['dataFinal'],8,2)."/".substr($vetor['dataFinal'],5,2)."/".substr($vetor['dataFinal'],0,4);?>
                                            </center>
                                        </td>
                                        <td>
                                        	<center>
                                        		<a href="#" onclick="getMotivoIsencao('<?php echo $vetor['idIsencaoOa'];?>')" data-target="#mySeeMotivo" data-toggle="modal" data-placement="left">
                                        				<button class="btn btn-danger btn-circle btn-lg btn-outline"><i class="fa fa-list"></i>
                            							</button>
                            					</a>
                            				</center>
                                        </td>
                                        <td>
                                            <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                        </td>
		                                <td>
                                            <center>
                                            <?php
                                            if(!$usuarioApenasLeitura) {
                                            ?>
                                            ?>

			                                	<a href="?corpo=alteraIsencaoOA&idIsencaoOa=<?php echo $vetor['idIsencaoOa'];?>" class="btn btn-sm btn-info">
			                                        <i class="fa fa-pencil fa-white"></i>&nbsp;
			                                        Editar
			                                    </a>
			                                    &nbsp;
			                                    <button type="button" class="btn btn-sm btn-danger" onclick="excluirIsencaoOa('<?php echo $vetor['idIsencaoOa']; ?>');" >
			                                        <i class="fa fa-trash fa-white"></i>&nbsp;
			                                        Excluir
			                                    </button>

                                            <?php }else{
                                                echo "<span class=\"btn btn-default btn-rounded\">Apenas Leitura</span>";
                                            }
                                            ?>
                                            </center>
		                                </td>
                                	</tr>
                            	<?php
                                    }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<div class="modal inmodal" id="mySeeMotivo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-list modal-icon"></i>
                <h4 class="modal-title">Motivo</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="motivo"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
