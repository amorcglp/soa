<?php
include_once("controller/livroOrganismoController.php");
$loc = new livroOrganismoController();
$dados = $loc->buscaLivroOrganismo($_GET['idlivro_organismo']);
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Materiais do Organismo para GLP</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li class="active">
                <strong><a>Materiais do Organismo para GLP</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de alteração de Materiais do Organismo para GLP</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaLivroOrganismo">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <?php 
                
                ?>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoAlterar.php" onSubmit="return validaLivroOrganismo()" >
                    	<input type="hidden" name="fk_idLivroOrganismo" id="fk_idLivroOrganismo" value="<?php echo $_GET['idlivro_organismo'];?>">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <?php echo retornaNomeCompletoOrganismoAfiliado($dados->getFk_idOrganismoAfiliado());?>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Título: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="titulo" name="titulo" type="text" style="max-width: 450px" value="<?php echo $dados->getTitulo();?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Assunto: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="assunto" name="assunto" type="text" value="<?php echo $dados->getAssunto();?>" style="max-width: 450px">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Escreva a notícia aqui<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="descricao" name="descricao"><?php echo $dados->getDescricao();?></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Palavras-chave (separe-as com vírgula): </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="200" id="palavraChave" name="palavraChave" type="text" value="<?php echo $dados->getPalavraChave();?>" style="max-width: 450px">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                        	<input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                                <input type="hidden" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" value="<?php echo $dados->getFk_idOrganismoAfiliado();?>">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar a Ata" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaLivroOrganismo" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	