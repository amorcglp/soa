<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
                         @usuarioOnline();?>
<?php 
//echo "<pre>";print_r($_SESSION);
include_once("model/emailAnexoClass.php");
$ea = new emailAnexo();

$listaDestinatarios 			= isset($_REQUEST['listaDestinatarios'])?$_REQUEST['listaDestinatarios']:null;
$anexo 							= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;
$paraAnexo 						= isset($_REQUEST['paraAnexo'])?$_REQUEST['paraAnexo']:null;


if($paraAnexo!=null)
{
	$var = $_REQUEST['paraAnexo'];
	$listaDestinatarios = explode(",",$var);
}

/**
 * Upload de Anexos
 */
$arquivoAnexo=0;
if($anexo != "")
{
	if ($_FILES['anexo']['name'] != "") {
		//$proximo_id = $ea->selecionaUltimoId();
		$extensao = strstr($_FILES['anexo']['name'], '.');
		$caminho = "img/email/" . basename($_POST["idEmailAnexo"].".email.".$_SESSION['ultimoIdNovoEmail'].$extensao);
		$ea->setFkIdEmail($_POST["idEmailAnexo"]);
		$ea->setCaminho($caminho);
		$ea->setExtensao($extensao);
		$ea->setNomeArquivo($_FILES['anexo']['name']);
		if($ea->cadastroEmailAnexo())
		{
			$uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/email/';
			$uploadfile = $uploaddir . basename($_POST["idEmailAnexo"].".email.".$_SESSION['ultimoIdNovoEmail'].$extensao);
			if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
			{ 
				$arquivoAnexo = 1;  
			}else{
				echo "<script type='text/javascript'>alert('Erro ao anexar arquivo!');</script>";
			}
		}
	}
}

if($arquivoAnexo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Arquivo anexado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php 
}

$responderEmail 	= isset($_REQUEST['responderEmail'])?$_REQUEST['responderEmail']:null;
$encaminharEmail 	= isset($_REQUEST['encaminharEmail'])?$_REQUEST['encaminharEmail']:null;
$idEmail 			= isset($_REQUEST['idEmail'])?$_REQUEST['idEmail']:null;

include_once 'model/emailClass.php';
$e = new Email();

include_once 'model/usuarioClass.php';

if(count($listaDestinatarios)==null&&$responderEmail==null)
{
/**
 * Ao encaminhar copiar os anexos para o novo email
 */
if($encaminharEmail!=null)
{
	//Selecionar os anexos do email encaminhado
	$ea = new emailAnexo();
	$resultado = $ea->listaEmailAnexo($idEmail);
	if($resultado)
	{
		foreach($resultado as $vetor)
		{
			$ea2 = new emailAnexo();
			$ea2->setFkIdEmail($_SESSION['ultimoIdNovoEmail']);
			$ea2->setCaminho($vetor['caminho']);
			$ea2->setNomeArquivo($vetor['nome_arquivo']);
			$ea2->setExtensao($vetor['extensao']);
			$ea2->cadastroEmailAnexo();
		}
	}
}
?>
	<script>
	window.onload = function(){
		$('#mySeeLista').modal({
	    });
	}
	</script>
<?php 
}else{
	if($anexo=="")
	{
		
	}
}

include_once 'controller/emailController.php';
$ec = new emailController();
$retorno = $ec->listaEmail(1,$_SESSION['seqCadast']);
$totalEmails=0;
$totalEmailsNovos=0;
if($retorno)
{
	$totalEmails = count($retorno);
}
if($retorno)
{
	foreach($retorno as $vetor)
	{

		if($vetor['nova']==1)
		{
			$totalEmailsNovos++;
		}
	}
}

$assunto =isset($_REQUEST['assuntoAnexo'])?$_REQUEST['assuntoAnexo']:null;
$mensagem=isset($_REQUEST['mensagemAnexo'])?$_REQUEST['mensagemAnexo']:null;
if($responderEmail!=null||$encaminharEmail!=null)
{
	$resultado4 = $e->buscarEmailPorId($idEmail);
	if($resultado4)
	{
		foreach($resultado4 as $vetor4)
		{
			if($responderEmail!=null)
			{
				$assunto = "RE: ".$vetor4['assunto'];
			}
			if($encaminharEmail!=null)
			{
				$assunto = "FW: ".$vetor4['assunto'];
			}
			$mensagem = "<br><br>---------------------------------------<br>".$vetor4['mensagem'];
		}
	}
}
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>E-mail Interno</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaEmail">E-mail Interno</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="?corpo=comporEmail">Escrever e-mail</a>
                            <div class="space-25"></div>
                            <h5>Pastas</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li><a href="?corpo=buscaEmail"> <i class="fa fa-inbox "></i> Caixa de Entrada <?php if($totalEmailsNovos>0){?><span class="label label-warning pull-right"><?php echo $totalEmailsNovos;?></span><?php }?> </a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=2"> <i class="fa fa-envelope-o"></i> E-mails enviados</a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=3"> <i class="fa fa-exclamation-circle"></i> Importante</a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=4"> <i class="fa fa-trash-o"></i> Lixo</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="#" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Descartar email" onclick="descartarEmail();"><i class="fa fa-times"></i> Descartar</a>
                </div>
                <h2>
                    Escrever e-mail
                </h2>
            </div>
                <div class="mail-box">


                <div class="mail-body">

                    <form name="email" id="email" class="form-horizontal" method="post" action="acoes/acaoAlterar.php">
                        <div class="form-group">
                        
                        <label class="col-sm-2 control-label"><a href="#" data-target="#mySeeLista" data-toggle="modal" data-placement="left">Para:</a>
                        </label>

                            <div class="col-sm-10">
                            	<?php //echo "<pre>";print_r($listaDestinatarios);?>
                            	<select name="paraView" id="paraView" data-placeholder="Escolha um destinatário..." class="chosen-select" onChange="alteraPara()" multiple style="width:100%;" tabindex="4">
					                <option value="">Selecione</option>
					                <?php 
					                $u = new Usuario();
					                $resultado3 = $u->listaUsuario(null,null,null,null,"u.seqCadast",null,1);
					                if($resultado3)
					                {
					                	foreach($resultado3 as $vetor3)
					                	{
					                		//echo "-->".$vetor['seqCadast'];
					                	?>
					                		<option value="<?php echo $vetor3['idUsuario'];?>" 
											<?php 
												if(count($listaDestinatarios)>0){
													if (in_array($vetor3['idUsuario'],$listaDestinatarios)){
														echo "selected";
													}
												}
												if($responderEmail!=null){
													if($responderEmail==$vetor3['idUsuario'])
													{
														echo "selected";
													}
												}
											?>
											><?php echo $vetor3['nomeUsuario'];?></option>
					                	<?php 
					                	}
					                }
					                ?>
				                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Assunto:</label>

                            <div class="col-sm-10"><input name="assunto" id="assunto" type="text" class="form-control" value="<?php echo $assunto;?>"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Anexar:</label>

                            <div class="col-sm-10">
                            	<a href="#" class="btn btn-warning btn-sm" data-target="#mySeeUpload" data-toggle="modal" data-placement="left"><i class="fa fa-paperclip"></i> Anexar</a>
                            	<a href="#" class="btn btn-info btn-sm" data-target="#mySeeListaUpload" onclick="listaUploadEmailAnexo('<?php echo $_SESSION['ultimoIdNovoEmail'];?>');" data-toggle="modal" data-placement="left"><i class="fa fa-paperclip"></i> Lista de Anexos</a>
							</div>
                        </div>
                        
					
                </div>
                    <div class="mail-text h-200">
                        <textarea class="summernote" id="mensagem" name="mensagem"><?php echo $mensagem;?></textarea>
						<div class="clearfix"></div>
                        </div>
                    <div class="mail-body text-right tooltip-demo">
                    	<input type="hidden" id="para" name="para" value="<?php if(count($listaDestinatarios)>0){
								for($i=0;$i<count($listaDestinatarios);$i++)
								{
									if($i==0)
									{
										echo $listaDestinatarios[$i];
									}else{
										echo ",".$listaDestinatarios[$i];
									}
								}		
							}
							if($responderEmail!=null)
							{
								echo $responderEmail;
							}
                    	?>">
                    	<input type="hidden" name="fk_idEmail" id="fk_idEmail" value="<?php echo $_SESSION['ultimoIdNovoEmail'];?>">
                    	<input type="hidden" name="mensagemEmail" id="mensagemEmail" value="">
                        <input type="submit" class="btn btn-primary" value="Enviar" onclick="enviarEmail();">
                        <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Descartar e-mail" onclick="descartarEmail();"><i class="fa fa-times"></i> Descartar</a>
                    </div>
                    <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<div class="modal inmodal" id="mySeeLista" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-envelope modal-icon"></i>
                <h4 class="modal-title">Escolha as Pessoas para quem você quer mandar e-mail</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group" id="checkboxes">
						<form name="destinatarios" id="destinatarios" method="post" enctype="multipart/form-data">
						<center>
							<div>
								Pesquisar: <input type="text" name="search" id="search"
									onkeypress="filtraPessoasParaQuemVoceQuerMandarEmail(this.value);"
									onkeyUp="filtraPessoasParaQuemVoceQuerMandarEmail(this.value);">
							</div>
						</center>
						<br>
						<input type="checkbox" name="checkboxCheckAll" id="checkboxCheckAll" onclick="checkAll();"> <b>Marcar todos</b>
							<br><br>
							<div id="pessoasParaQuemVoceQuerMandarEmail">
							<?php 
								$u27 = new Usuario();
								$resultado27 = $u27->listaUsuario(null,null,null,null,"idOrganismoAfiliado",null,true);
                                                                //echo "<pre>".print_r($resultado27);
								if($resultado27)
								{
                                                                    
									foreach($resultado27 as $vetor)
									{
										?>
										<input type="checkbox" class="marcar" name="listaOA[]" id="marcarOA<?php echo $vetor['siglaOrganismoAfiliado'];?>" onClick="checkAllOA('<?php echo $vetor['siglaOrganismoAfiliado'];?>',this)">
										<?php 
										echo organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'],$vetor['tipoOrganismoAfiliado'],$vetor["nomeOrganismoAfiliado"])."<br><br>";
                                                                                
										?>
										<div id="checkboxes<?php echo $vetor['siglaOrganismoAfiliado'];?>">
										<?php 
                                                                                
										$u28 = new Usuario();
										$resultado28 = $u28->listaUsuario($vetor['siglaOrganismoAfiliado'],null,null,null,"u.seqCadast","u.nomeUsuario",1);
										
										if($resultado28)
										{
											foreach ($resultado28 as $vetor28)
											{
												?>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="checkbox" class="marcar" name="listaDestinatarios[]" id="<?php echo $vetor28['idUsuario'];?>" value="<?php echo $vetor28['idUsuario'];?>">
												<?php
												echo $vetor28['nomeUsuario']."<br><br>"; 
											}
										}
                                                                                 
										?>
                                                                                </div>
										<?php 
									}
                                                                     
								}
							?>
							</div>
						</form>
                                        </div>
					
                </div>
            
            <div class="modal-footer">
            	<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <input type="submit" class="btn btn-info" value="Confirmar" onclick="document.destinatarios.submit();">
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Anexar Arquivos</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
            	<form id="arquivo" name="arquivo" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="paraAnexo" name="paraAnexo" value="<?php if(count($listaDestinatarios)>0){
								for($i=0;$i<count($listaDestinatarios);$i++)
								{
									if($i==0)
									{
										echo $listaDestinatarios[$i];
									}else{
										echo ",".$listaDestinatarios[$i];
									}
								}		
							}
							if($responderEmail!=null)
							{
								echo $responderEmail;
							}
                    	?>">
                    <input type="hidden" name="mensagemAnexo" id="mensagemAnexo" value="">
                    <input type="hidden" name="assuntoAnexo" id="assuntoAnexo" value="">	
					<input type="hidden" name="idEmailAnexo" id="idEmailAnexo" value="<?php echo $_SESSION['ultimoIdNovoEmail'];?>">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Anexar" onclick="anexar();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-paperclip modal-icon"></i>
                <h4 class="modal-title">Lista de Anexos</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>