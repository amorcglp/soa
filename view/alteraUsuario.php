<?php
include_once("controller/usuarioController.php");
require_once ("model/usuarioClass.php");

$uc = new usuarioController();
$dados = $uc->buscaUsuario($_GET['seqCadast']);
?>

<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Usuário</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaUsuario">Usuários</a>
            </li>
            <li class="active">
                <a>Alterar Usuário</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Alterar Usuário</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaUsuario">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nome</label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeUsuario" id="nomeUsuario" value="<?php echo $dados->getNomeUsuario(); ?>" class="form-control" required="" onChange="this.value = this.value.toUpperCase()" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Login</label>
                            <div class="col-sm-10">
                                <input type="text" name="loginUsuario" id="loginUsuario" required="" value="<?php echo $dados->getLoginUsuario(); ?>" class="form-control" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mudar Senha</label>
                            <div class="col-sm-10">
                                <input type="text" name="senhaUsuario" id="senhaUsuario" value="" class="form-control" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">E-mail</label>
                            <div class="col-sm-10">
                                <input type="text" name="emailUsuario" id="emailUsuario" class="form-control" value="<?php echo $dados->getEmailUsuario(); ?>" required="" onChange="this.value = this.value.toLowerCase()" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Telefone Residencial</label>
                            <div class="col-sm-10">
                                <input type="text" name="telefoneResidencialUsuario" id="telefoneResidencialUsuario" value="<?php echo $dados->getTelefoneResidencialUsuario(); ?>"  class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Telefone Comercial</label>
                            <div class="col-sm-10">
                                <input type="text" name="telefoneComercialUsuario" id="telefoneComercialUsuario" value="<?php echo $dados->getTelefoneComercialUsuario(); ?>"  class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Celular</label>
                            <div class="col-sm-10">
                                <input type="tel" name="celularUsuario" id="celularUsuario" value="<?php echo $dados->getCelularUsuario(); ?>" class="form-control" value="" style="max-width: 320px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Departamento</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idDepartamento" id="fk_idDepartamento" style="max-width: 150px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'controller/departamentoController.php';
                                    $dc = new departamentoController();
                                    $dc->criarComboBox($dados->getFk_idDepartamento());
                                    ?>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <!--<input type="hidden" name="jaCadastrado" id="jaCadastrado">-->
                                <input type="hidden" name="seqCadast" id="seqCadast" value="<?php echo $_GET['seqCadast']; ?>" />
                                <input type="hidden" name="letra" id="letra" value="<?php echo $_GET['letra']; ?>" />
                                <input type="hidden" name="letraNome" id="letraNome" value="<?php echo $_GET['letraNome']; ?>" />
                                <!--<input type="hidden" name="regiaoUsuario" id="regiaoUsuario">-->
                                <!--<input type="hidden" name="totalFuncoes" id="totalFuncoes" value="0">-->
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->


<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
				
