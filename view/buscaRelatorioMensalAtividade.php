<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once('model/relatorioAtividadeMensalClass.php');
include_once('model/usuarioClass.php');
include_once('model/notificacaoGlpClass.php');
include_once 'model/organismoClass.php';
include_once 'model/atividadeIniciaticaClass.php';
include_once 'model/atividadeEstatutoClass.php';
include_once('model/membrosRosacruzesAtivosClass.php');

include_once("lib/functions.php");
include_once("controller/atividadeIniciaticaController.php");
include_once("controller/atividadeEstatutoController.php");
include_once("controller/organismoController.php");

$ram 		= new RelatorioAtividadeMensal();
$usu		= new Usuario();
$u 			= new NotificacaoGlp();
$o          = new organismo();
$oc         = new organismoController();
$aim        = new atividadeIniciatica();
$aic        = new atividadeIniciaticaController();
$aem        = new atividadeEstatuto();
$aec        = new atividadeEstatutoController();

if(isset($_REQUEST['mesAtual'])) {
	$mesAtual = $_REQUEST['mesAtual'];
} else {
    if(isset($_SESSION['mesAtualRelatorioAtividadeMensal'])) {
        $mesAtual = $_SESSION['mesAtualRelatorioAtividadeMensal'];
    } else {
        $mesAtual = date('m');
        $_SESSION['mesAtualRelatorioAtividadeMensal'] = $mesAtual;
    }
}
//echo $mesAtual;
$_SESSION['mesAtualRelatorioAtividadeMensal'] = $mesAtual;

if(isset($_REQUEST['anoAtual'])) {
	$anoAtual = $_REQUEST['anoAtual'];
} else {
	if(isset($_SESSION['anoAtualRelatorioAtividadeMensal'])) {
		$anoAtual = $_SESSION['anoAtualRelatorioAtividadeMensal'];
	} else {
		$anoAtual = date('Y');
		$_SESSION['anoAtualRelatorioAtividadeMensal'] = $anoAtual;
	}
}
//echo "anoatual=>".$anoAtual;
$_SESSION['anoAtualRelatorioAtividadeMensal'] = $anoAtual;

/**
 * #####################################################################################################################################################
 */

//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;
$usuario 				= isset($_REQUEST['usuario'])?$_REQUEST['usuario']:null;
$anexo 					= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;
/**
 * Upload do Relatório Assinado
 */
$relatorio=0;
$extensaoNaoValida=0;
if($anexo != "")
{
    //Verificar se já está cadastrado
    $ram->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
    $ram->setMes($mesAtual);
    $ram->setAno($anoAtual);
    $ram->setUsuario($usuario);

    $idRelatorioAtividade = $ram->cadastraRelatorioAtividadeMensal();

    if ($_FILES['anexo']['name'] != "") {
        if(substr_count($_FILES['anexo']['name'],".")==1)
        {
            $extensao = strstr($_FILES['anexo']['name'], '.');
            if($extensao==".pdf"||
                $extensao==".png"||
                $extensao==".jpg"||
                $extensao==".jpeg"||
                $extensao==".PDF"||
                $extensao==".PNG"||
                $extensao==".JPG"||
                $extensao==".JPEG"
            )
            {
                $caminho = "img/relatorio_atividade_mensal/" . basename($idRelatorioAtividade.".relatorio.atividade.mensal.".$mesAtual.".".$anoAtual);
                //Guardar Link do Caminho no Banco de Dados
                $ram->setCaminhoRelatorioAtividadeMensal($caminho);
                if($ram->atualizaCaminhoRelatorioAtividadeMensalAssinado($idRelatorioAtividade))
                {
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/relatorio_atividade_mensal/';
                    $uploadfile = $uploaddir . basename($idRelatorioAtividade.".relatorio.atividade.mensal.".$mesAtual.".".$anoAtual);
                    if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                    {
                        $relatorio = 1;
                        /**
                         * Notificar GLP que o relatório assinado foi inserido
                         */
                        date_default_timezone_set('America/Sao_Paulo');
                        $tituloNotificacao="Novo Relatorio Atividade Mensal Entregue";
                        $tipoNotificacao=6;//Entregue
                        $remetenteNotificacao=$sessao->getValue("seqCadast");
                        $mensagemNotificacao="Relatorio Atividade Mensal Entregue pelo Organismo ".$_POST['nomeOrganismo'].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuario'];

                        //Selecionar todos os usuários com departamento 2 e 3
                        $resultadoUsuarios = $usu->listaUsuario(null,"2,3");
                        $arrOficiais=array();
                        if($resultadoUsuarios)
                        {
                            foreach ($resultadoUsuarios as $vetor)
                            {
                                $arrOficiais[] = $vetor['idUsuario'];
                            }
                        }

                        $resultado = $u->cadastroNotificacao(utf8_encode($tituloNotificacao),utf8_encode($mensagemNotificacao),$tipoNotificacao,$remetenteNotificacao);

                        if ($resultado==true) {
                            for ($i=0; $i<count($arrOficiais); $i++){
                                $ultimoId = $u->selecionaUltimoId();
                                $resultadoOficiais = $u->cadastroNotificacaoGlp($ultimoId,$arrOficiais[$i]);
                            }
                        }
                    }else{
                        echo "<script type='text/javascript'>alert('Erro ao enviar o relatório assinado!');</script>";
                    }
                }
            }else{
                $extensaoNaoValida=1;
            }
        }else{
            $extensaoNaoValida=2;
        }
    }
}

if($relatorio==1){?>
<script>
    window.onload = function () {
        swal({
            title: "Sucesso!",
            text: "Relatório Assinado Enviado com Sucesso!",
            type: "success",
            confirmButtonColor: "#1ab394"
        });
    }
</script>
<?php }

if($extensaoNaoValida==1){?>
<script>
    window.onload = function () {
        swal({
            title: "Aviso!",
            text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
</script>
<?php }
if ($extensaoNaoValida == 2) {
?>
<script>
    window.onload = function () {
        swal({
            title: "Aviso!",
            text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
</script>
<?php }
/*
 * Verificar se já foi feito o upload do relatório assinado
 */
$caminhoRelatorioAtividadeMensal="";
if($ram->verificaSeJaCadastrado($mesAtual,$anoAtual,$idOrganismoAfiliado))
{
    $resultado = $ram->listaRelatorioAtividadeMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
    if($resultado)
    {
        foreach($resultado as $vetor)
        {
            $caminhoRelatorioAtividadeMensal = $vetor['caminhoRelatorioAtividadeMensal'];
        }
    }
}
/**
 * #####################################################################################################################################################
 */

?>
<?php

/****
 * Montar navegação por meses
 *****/

/*
 * Montar anterior e próximo
 */

$mesAnterior=date('m', strtotime('-1 months', strtotime($anoAtual."-".$mesAtual."-01")));
$mesProximo=date('m', strtotime('+1 months', strtotime($anoAtual."-".$mesAtual."-01")));

if($mesAtual==1)
{
	$anoAnterior=$anoAtual-1;
}else{
	$anoAnterior=$anoAtual;
}
if($mesAtual==12)
{
	$anoProximo=$anoAtual+1;
}else{
	$anoProximo=$anoAtual;
}

/*
 * Montar texto para ser exibido
 */
$mesAtualTexto = mesExtensoPortugues($mesAtual);
$mesProximoTexto = mesExtensoPortugues($mesProximo);
$mesAnteriorTexto = mesExtensoPortugues($mesAnterior);

$membrosRosacruzesAtivos = new MembrosRosacruzesAtivos();


/*
 * Seleção dos rosacruzes ativos no OA
 */
$membrosAtivosMesAnterior   = $membrosRosacruzesAtivos->retornaMembrosRosacruzesAtivos($mesAnterior, $anoAnterior, $idOrganismoAfiliado);
$arr                        = $membrosRosacruzesAtivos->retornaMembrosRosacruzesAtivos($mesAtual, $anoAtual, $idOrganismoAfiliado);
$numeroMembrosAtivos    = ($membrosAtivosMesAnterior['numeroAtualMembrosAtivos'] + $arr['novasAfiliacoes'] + $arr['reintegrados']) - $arr['desligamentos'];


/*
 * Dados do Organismo Afiliado
 */
$dadosOrganismo = $oc->buscaOrganismo($idOrganismoAfiliado);
$nomeOrganismo                      = retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);


$classificacaoOrganismoAfiliado     = $dadosOrganismo->getClassificacaoOrganismoAfiliado();
$atuacaoTemporaria                  = $dadosOrganismo->getAtuacaoTemporaria();

//Buscar informações no webservice para pegar atualizações vindas do ORCZ
$ocultar_json=1;
$siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
switch ($dadosOrganismo->getPaisOrganismoAfiliado())
{
    case 1:
        $pais="BR";
        break;
    case 2:
        $pais="PT";
        break;
    case 3:
        $pais="AO";
        break;
    default :
        $pais="BR";
        break;
}
$paisDoOrganismo = $pais;
//echo $pais;
include 'js/ajax/retornaDadosOrganismo.php';
//echo "<pre>";print_r($return);
$elevacaoLojaWS = $return->result[0]->fields->fArrayOA[0]->fields->fDatElevacLoja;
$elevacaoCapituloWS = $return->result[0]->fields->fArrayOA[0]->fields->fDatElevacCapitu;
$elevacaoPronaosWS = $return->result[0]->fields->fArrayOA[0]->fields->fDatElevacPronau;

//Tratamento das datas
$elevacaoLoja                   = substr($elevacaoLojaWS,0,7);
$elevacaoCapitulo               = substr($elevacaoCapituloWS,0,7);
$elevacaoPronaos                = substr($elevacaoPronaosWS,0,7);

if($elevacaoLoja==""&&$elevacaoCapitulo==""&&$elevacaoPronaos=="")
{
    $classificacao = $classificacaoOrganismoAfiliado;
    $classificacaoNoSoa = $classificacaoOrganismoAfiliado;
}    

$elevacaoLoja                   = $elevacaoLoja."-01";
$elevacaoCapitulo               = $elevacaoCapitulo."-01";
$elevacaoPronaos                = $elevacaoPronaos."-01";
$dataAtual                      = $anoAtual."-".$mesAtual."-01";

include_once('model/atividadeEstatutoMensalClass.php');
$a = new atividadeEstatutoMensal();
$resultado7 = $a->listaAtividadeMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
//echo "<pre>";print_r($resultado7);
$entregue=0;
$entregueCompletamente=0;
$dataEntrega="";
$dataEntregaCompletamente="";
$statusEntrega=1;
if($resultado7)
{
    $entregue=1;
    $statusEntrega=2;
    foreach ($resultado7 as $v)
    {
        $codigoAssinatura = $v['numeroAssinatura'];
        $quemEntregou = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
        $dataEntrega = substr($v['dataEntrega'],8,2)."/".substr($v['dataEntrega'],5,2)."/".substr($v['dataEntrega'],0,4)." às ".substr($v['dataEntrega'],11,8);
        //echo "dataEntrega:".$dataEntrega;
        if($v['entregueCompletamente']==1)
        {
            $entregueCompletamente=1;
            $statusEntrega=3;
            $dataEntregaCompletamente = substr($v['dataEntregouCompletamente'],8,2)."/".substr($v['dataEntregouCompletamente'],5,2)."/".substr($v['dataEntregouCompletamente'],0,4)." às ".substr($v['dataEntregouCompletamente'],11,8);
        }

    }
}

switch ($statusEntrega)
{
    case 1:
        $class = "warning";
        $icon = "fa-location-arrow";
        $tituloBox = "Assinatura Eletrônica";
        break;
    case 2:
        $class = "default";
        $icon = "fa-pencil";
        $tituloBox = "O Documento não foi entregue completamente. Faltam assinaturas!";
        break;
    case 3:
        $class = "info";
        $icon = "fa-thumbs-up";
        $tituloBox = "O Documento foi 100% entregue";
        break;
}

//Ver se tem upload criado
$resultado = $ram->listaRelatorioAtividadeMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
$upload = "não";
if($resultado) {
    if (count($resultado) > 0) {
        $upload = "sim";
    }
}

//Ver data mais antiga da criação desse relatório
$dataCriacao = $aem->dataCriacao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$dataCriacaoDocumento = substr($dataCriacao,8,2)."/".substr($dataCriacao,5,2)."/".substr($dataCriacao,0,4);

//Ver data da ultima alteração
$dataAlteracao = $aem->dataUltimaAlteracao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$dataUltimaAlteracaoDocumento = substr($dataAlteracao,8,2)."/".substr($dataAlteracao,5,2)."/".substr($dataAlteracao,0,4);

//Responsáveis pela construção do documento
$arrResp = $aem->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);

//Responsáveis pela alteracao do documento
$arrRespAlteracao = $aem->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);
foreach ($arrRespAlteracao['seq'] as $k => $v)
{
    if(!in_array($v,$arrRespAlteracao['seq']))
    {
        $arrResp['seq'][$i] = $v;
        $arrResp['nome'][$i] = $arrRespAlteracao['nome'][$k];
        $i++;
    }
}

//Ordernar responsáveis por ordem do inicio de criação
/*
rsort ($arrResp['nome']);
rsort ($arrResp['seq']);
rsort ($arrResp['data']);
*/
$arrResp = unique_multidim_array_vals($arrResp);

sort($arrResp['nome'], SORT_NATURAL | SORT_FLAG_CASE);


//echo "<pre>";print_r($arrResp['nome']);


//Faltam assinar
include_once('model/atividadeEstatutoMensalClass.php');
$a = new atividadeEstatutoMensal();
$mestre = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
$secretario = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);

//Montar Faltam assinar
$arrOficiaisFaltamAssinar=array();
if(!$mestre)
{
    $arrOficiaisFaltamAssinar[]="MESTRE DO ORGANISMO AFILIADO";
}
if(!$secretario)
{
    $arrOficiaisFaltamAssinar[]="SECRETÁRIO DO ORGANISMO AFILIADO";
}

//$oficiaisFaltamAssinar = implode(", ",$arr);
$arrOficiaisQueAssinaram = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado);


?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Atividades</a>
            </li>
            <li class="active">
                <strong>Relatório Mensal de Atividades</strong>
            </li>
        </ol>
    </div>
</div>
<?php
/*
if($sessao->getValue("fk_idDepartamento")==1) {
?>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
<div class="col-lg-12">
<div class="ibox float-e-margins">
<div class="ibox-title">
<h5>Atividades Iniciáticas</h5>
<div class="ibox-tools">
</div>
</div>
<div class="ibox-content">
<div class="alert alert-success">
<center>
Módulo ainda não disponível. <br>
<a class="alert-link">Calma! Falta pouco :]</a>
</center>
</div>
</div>
</div>
</div>
</div>
</div>
<?php
} else {
 */
//teste
?>
<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Relatório Mensal de Atividades</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row"></div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="widget lazur-bg p-xl">
                                    <ul class="list-unstyled m-t-md">
                                        <h2>
                                            <i class="fa fa-exclamation-triangle"></i>Avisos
                                        </h2>
                                        <br />

                                        <?php
                                        $elevacao="";    
                                        if($elevacaoPronaosWS!=""&&$dataAtual>=$elevacaoPronaos) {
                                            $elevacao=2;
                                            $classificacao=2;
                                            $textoClassificacao="Pronaos";
                                        }
                                        if($elevacaoCapituloWS!=""&&$dataAtual>=$elevacaoCapitulo) {
                                            $elevacao=3;
                                            $classificacao=3;
                                            $textoClassificacao="Capítulo";
                                        }
                                        if($elevacaoLojaWS!=""&&$dataAtual>=$elevacaoLoja) {
                                            $elevacao=1;
                                            $classificacao=1;
                                            $textoClassificacao="Loja";
                                        }
                                        if($atuacaoTemporaria!=0)
                                        {
                                            $classificacao=$atuacaoTemporaria;
                                            switch ($atuacaoTemporaria) {
                                                case 1:
                                                    $textoClassificacao = "Loja";
                                                    break;
                                                case 2:
                                                    $textoClassificacao = "Pronaos";
                                                    break;
                                                case 3:
                                                    $textoClassificacao = "Capítulo";
                                                    break;
                                                case 4:
                                                    $textoClassificacao = "Heptada";
                                                    break;
                                                case 5:
                                                    $textoClassificacao = "Atrium";
                                                    break;
                                                default:
                                                    $textoClassificacao = "Loja";
                                                    break;
                                            }
                                        }

                                        $idClassificacaoOa = $classificacao;//Forçar pegar a classificação do organismo para este mês
                                        
                                        $qntAtividadesIniciaticas = $aim->retornaNumeroDeAtividadesParaRelatorio($idOrganismoAfiliado,$mesAtual,$anoAtual);
                                        
                                        $classificacao = $aem->listaAtividadeEstatutoParaRelatorioGetClassificacao($idOrganismoAfiliado,$mesAtual,$anoAtual);
                                        
                                        $retornoEstatutoTipo = $aem->listaAtividadeEstatutoTipoPelaClassificacaoOa($classificacao,true,$mesAtual);
                                        
                                        echo "Você preencheu esse relatório do mês ".$mesAtualTexto." de ".$anoAtual." como: ";
                                        switch ($classificacao)
                                        {
                                            case 2:
                                                echo "Pronaos";
                                                break;
                                            case 3:
                                                echo "Capítulo";
                                                break;
                                            case 1:
                                                echo "Loja";
                                                break;
                                        }
                                        echo "<br><br>";
                                        if($retornoEstatutoTipo){
                                            foreach($retornoEstatutoTipo as $vetorEstatutoTipo){
                                                $idTipoAtividadeEstatuto        = $vetorEstatutoTipo['idTipoAtividadeEstatuto'];
                                                $nomeTipoAtividadeEstatuto      = $vetorEstatutoTipo['nomeTipoAtividadeEstatuto'];
                                                $qntTipoAtividadeEstatuto       = $vetorEstatutoTipo['qntTipoAtividadeEstatuto'];
                                                $qntAtividades = $aem->listaAtividadeEstatutoParaRelatorio($idOrganismoAfiliado,$mesAtual,$anoAtual,$idTipoAtividadeEstatuto);

                                                if($qntTipoAtividadeEstatuto == 0){
                                                    if($qntAtividades > 0){
                                                        echo "<li><span class='fa fa-thumbs-up'></span> Atividade <label>Cód. " . $idTipoAtividadeEstatuto."-".$nomeTipoAtividadeEstatuto . "</label> (".$qntAtividades."/".$qntTipoAtividadeEstatuto.")</li>";
                                                    }
                                                } else {
                                                    if($qntAtividades >= $qntTipoAtividadeEstatuto){
                                                        echo "<li><span class='fa fa-thumbs-up'></span> Atividade <label> Cód. " . $idTipoAtividadeEstatuto."-". $nomeTipoAtividadeEstatuto . "</label> (".$qntAtividades."/".$qntTipoAtividadeEstatuto.")</li>";
                                                    } else {
                                                        echo "<li><span class='fa fa-thumbs-down'></span> Atividade <label>Cód. " . $idTipoAtividadeEstatuto."-". $nomeTipoAtividadeEstatuto . "</label> (".$qntAtividades."/".$qntTipoAtividadeEstatuto.")</li>";
                                                    }
                                                }
                                            }
                                        }
                                        if($classificacaoOrganismoAfiliado == 1){
                                            echo "<li><span class='fa fa-thumbs-up'></span> <label>Iniciação de Graus</label>: ".$qntAtividadesIniciaticas."</li>";
                                        }
                                        echo "<li><span class='fa fa-thumbs-up'></span> <label>Rosacruzes Ativos no Organismo Afiliado</label>: ".$numeroMembrosAtivos."</li>";
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <table width="100%">
                                    <tr>
                                        <td align="center">
                                            <div class="form-group" id="data_4">
                                                <div class="input-group date">
                                                    <table width="100">
                                                        <tr>
                                                            <td>
                                                                <span class="btn btn-w-m btn-primary" style="cursor: pointer; color:#fff;">
                                                                    <i class="fa fa-calendar-o" style="color:#fff"></i>

                                                                    <input type="hidden" id="calendario" class="form-control" value="<?php echo $mesAtual;?>/<?php echo date('d');?>/<?php echo $anoAtual;?>" />
                                                                    <span style="color:#fff">
                                                                        <?php echo $mesAtualTexto;?><?php echo $anoAtual;?>
                                                                    </span>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <input type="hidden" id="paginaAtual" class="form-control" value="<?php echo $corpo;?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table cellspacing="10" width="<?php if(!$usuarioApenasLeitura){?>300<?php }else{?>200<?php }?>">
                                                <tr>
                                                    <td>
                                            <a href="#" class="btn btn-info  dim btn-large-dim btn-outline"
                                                onclick="abrirPopupRelatorioMensalAtividade('<?php echo $idOrganismoAfiliado;?>','<?php echo $mesAtual;?>','<?php echo $anoAtual;?>','<?php echo $numeroMembrosAtivos?>','<?php echo $elevacaoLojaWS;?>','<?php echo $elevacaoCapituloWS;?>','<?php echo $elevacaoPronaosWS;?>');">
                                                <i class="fa fa-print" value="Impressão"></i>
                                            </a>
                                                    </td>
                                                    <td>
                                            <?php //if(in_array("4",$arrNivelUsuario)){?>
                                            <!--    &nbsp;
                                            <a href="#"
                                                class="btn btn-info  dim btn-large-dim btn-outline"
                                                data-target="#mySeeUpload" data-toggle="modal">
                                                <i class="fa fa-cloud-upload"></i>
                                            </a>-->
                                            <?php //}?>
                                                    </td>
                                                    &nbsp;       <td>
											&nbsp;
                                            <a href="#" target="_blank"
                                                class="btn btn-info  dim btn-large-dim btn-outline"
                                                onclick="listaUploadRelatorioAtividadeMensal('<?php echo $mesAtual; ?>','<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>')"
                                                data-target="#mySeeListaUpload" data-toggle="modal">
                                                <i class="fa fa-paste"></i>
                                            </a>
                                                    </td>
                                                    <td>
                                                        <div id="entregarX">
                                                            <?php
                                                            if($statusEntrega==1&&!$usuarioApenasLeitura){
                                                                ?>
                                                                <a href="#" id="entregar"
                                                                   class="btn btn-info dim btn-large-dim btn-outline"
                                                                   onClick="entregarAtividadeMensal('<?php echo $mesAtual; ?>','<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>','<?php echo $_SESSION['seqCadast'];?>')"><i class="fa fa-location-arrow"></i> </a>
                                                                <?php
                                                            }
                                                            $mestre = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
                                                            $secretario = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);

                                                            //$pjd = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,371);
                                                            //$sjd = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,373);
                                                            //$tjd = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,375);
                                                            //$ma = $a->buscarAssinaturasEmAtividadeMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,377);

                                                            //Montar Faltam assinar
                                                            $arr=array();
                                                            if(!$mestre)
                                                            {
                                                                $arr[]="MESTRE DO ORGANISMO AFILIADO";
                                                            }
                                                            if(!$secretario)
                                                            {
                                                                $arr[]="SECRETÁRIO DO ORGANISMO AFILIADO";
                                                            }


                                                            /*
                                                            if(!$sjd)
                                                            {
                                                                $arr[]="SECRETÁRIO DA JUNTA DEPOSITÁRIA";
                                                            }
                                                            if(!$tjd)
                                                            {
                                                                $arr[]="TESOUREIRO DA JUNTA DEPOSITÁRIA";
                                                            }
                                                            if(!$ma)
                                                            {
                                                                $arr[]="MESTRE AUXILIAR DO ORGANISMO AFILIADO";
                                                            }
                                                            */
                                                            $oficiaisFaltamAssinar = implode(",",$arr);
                                                            if($statusEntrega==2)
                                                            {
                                                                ?>

                                                                <a href="#" data-container="body" data-toggle="popover" data-placement="top" data-content="<?php if($mestre&&$secretario){?>Todos Assinaram. Processo de entrega finalizado!<?php }else{?>Faltam assinar: <?php echo $oficiaisFaltamAssinar;}?>" data-original-title="" title="" aria-describedby="popover408083" style="display: <?php if($entregue==0){?>none<?php }else{?>block<?php }?>;" id="faltaAssinar"
                                                                   class="btn btn-info dim btn-large-dim btn-outline"
                                                                ><i class="fa fa-pencil"></i> </a>
                                                                <?php
                                                            }
                                                            if($statusEntrega==3)
                                                            {
                                                                ?>
                                                                <a href="#" data-container="body" data-toggle="popover" data-placement="top" data-content="Todos Assinaram. Processo de entrega finalizado!" data-original-title="" title="" aria-describedby="popover408083" style="display: <?php if($entregue==0){?>none<?php }else{?>block<?php }?>;" id="faltaAssinar"
                                                                   class="btn btn-info dim btn-large-dim btn-outline"
                                                                ><i class="fa fa-thumbs-up"></i> </a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <i class="fa fa-info-circle"></i> <b>Dados do Relatório</b>
                                    </div>
                                    <div class="panel-body">
                                        <p>
                                        <ul>

                                            <li><b>Arquivos enviados: <?php echo $upload; if($upload=="sim"){ echo " - Não precisa necessariamente de assinatura eletrônica!";}?></b></li>
                                            <li>Data de Início da criação: <?php if($dataCriacaoDocumento!="//"){ $criado=1; echo $dataCriacaoDocumento;}else{ $criado=0; echo " <b>não começou a ser criado</b>";}?></li>
                                            <li>Última alteração: <?php if($dataUltimaAlteracaoDocumento!="//"&&$dataUltimaAlteracaoDocumento!="00/00/0000"){ echo $dataUltimaAlteracaoDocumento;}else{ echo "--";}?></li>
                                            <li>Responsável(is) pela elaboração desse documento:<?php if(count($arrResp['nome'])==0){ echo "--";}?></li>
                                            <ol>
                                                <?php if(count($arrResp['nome'])>0){?>
                                                    <?php foreach ($arrResp['nome'] as $k => $v){?>
                                                        <li><?php echo $v;?></li>
                                                    <?php }?>
                                                <?php }?>
                                            </ol>
                                            <li>Oficiais que precisam assinar esse documento se for entregue eletronicamente:</li>
                                            <ol>
                                                <li>MESTRE DO ORGANISMO AFILIADO</li>
                                                <li>SECRETÁRIO DO ORGANISMO AFILIADO</li>
                                                <?php if($idClassificacaoOa==1||$idClassificacaoOa==3){?><li>TESOUREIRO DA JUNTA DEPOSITÁRIA</li><?php }?>
                                            </ol>
                                            <input type="hidden" name="idClassificacaoOa" id="idClassificacaoOa" value="<?php echo $idClassificacaoOa;?>">
                                            <input type="hidden" name="criado" id="criado" value="<?php echo $criado;?>">
                                        </ul>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-<?php echo $class;?>">
                                    <div class="panel-heading">
                                        <i class="fa <?php echo $icon;?>"></i> <b><?php echo $tituloBox;?></b>
                                    </div>
                                    <div class="panel-body">
                                        <?php

                                        ?>
                                        <p>
                                        <ul>
                                            <li>Data da Entrega (Ass. Eletrônica): <?php if($dataEntrega!=""){ echo $dataEntrega;}else{ echo "<span id='dataEntrega'>--</span>";}?></li>
                                            <li>Quem Entregou (Ass. Eletrônica): <?php if($quemEntregou!=""){ echo $quemEntregou;}else{ echo "<span id='quemEntregou'>--</span>";}?></li>
                                            <li>Oficiais que assinaram esse documento: <?php if($entregue==0||$arrOficiaisQueAssinaram==false){?>--<?php }?></li>
                                            <?php if($entregue==1){?>
                                                <ol>
                                                    <?php
                                                    if(count($arrOficiaisQueAssinaram)>0) {
                                                        foreach ($arrOficiaisQueAssinaram as $v) {
                                                            ?>
                                                            <li><?php echo $v['nomeUsuario']; ?> - <?php echo $v['nomeFuncao']; ?></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ol>
                                            <?php }?>
                                            <li>Assinaturas: <?php if($dataEntregaCompletamente!=""){ echo $dataEntregaCompletamente;}else{ echo "--";}?></li>
                                            <li>Faltam assinar: <?php if($entregue==0||count($arrOficiaisFaltamAssinar)==0){ echo "<span id='faltamAssinar'>--</span>";}?></li>
                                            <?php if($entregue==1){?>
                                                <ol>
                                                    <?php
                                                    if(count($arrOficiaisFaltamAssinar)>0) {
                                                        foreach ($arrOficiaisFaltamAssinar as $v) {
                                                            ?>
                                                            <li><?php echo $v; ?></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ol>
                                            <?php }?>
                                            <li>Código da Assinatura Eletrônica: <?php if($codigoAssinatura!=""){ echo $codigoAssinatura;}else{ echo "<span id='numeroAssinatura'>--</span>";}?></li>
                                        </ul>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Relatório Atividade Mensal Assinado</h4>
            </div>
            <div class="modal-body">
                <form id="relatorioAtividade" name="relatorioAtividade" class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return validaUpload()">
                    <div class="form-group">
                        <div class="col-sm-10">
                            <input name="anexo" id="anexo" type="file" />
                        </div>
                        <div id="anexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                    </div>
                    <?php echo $nomeOrganismo; ?>
                    <input type="hidden" name="nomeOrganismo" id="nomeOrganismo" value="<?php echo $nomeOrganismo;?>" />
                    <input type="hidden" name="nomeUsuario" id="nomeUsuario" value="<?php echo $dadosUsuario->getNomeUsuario();?>" />
                    <input type="hidden" name="mesAtual" id="mesAtual" value="<?php echo $mesAtual;?>" />
                    <input type="hidden" name="anoAtual" id="anoAtual" value="<?php echo $anoAtual;?>" />
                    <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>" />
                    <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" value="<?php echo $_SESSION['idOrganismoAfiliado'];?>" />
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Enviar" onclick="document.relatorioAtividade.submit();" />
                <input type="submit" class="btn btn-info" value="Enviar" onclick="document.relatorioAtividade.submit();" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Relatório Assinado</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUpload"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<?php //} ?>
