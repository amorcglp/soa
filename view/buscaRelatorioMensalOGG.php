<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once('model/relatorioOGGMensalClass.php');
include_once('model/oggMensalClass.php');
include_once('model/usuarioClass.php');
include_once 'model/organismoClass.php';
include_once("lib/functions.php");
include_once("controller/organismoController.php");

$ram 		= new RelatorioOGGMensal();
$rom 		= new oggMensal();
$usu		= new Usuario();
$o          = new organismo();
$oc         = new organismoController();


if(isset($_REQUEST['mesAtual'])) {
    $mesAtual = $_REQUEST['mesAtual'];
} else {
    if(isset($_SESSION['mesAtualRelatorioAtividadeMensal'])) {
        $mesAtual = $_SESSION['mesAtualRelatorioAtividadeMensal'];
    } else {
        $mesAtual = date('m');
        $_SESSION['mesAtualRelatorioAtividadeMensal'] = $mesAtual;
    }
}
//echo $mesAtual;
$_SESSION['mesAtualRelatorioAtividadeMensal'] = $mesAtual;

if(isset($_REQUEST['anoAtual'])) {
    $anoAtual = $_REQUEST['anoAtual'];
} else {
    if(isset($_SESSION['anoAtualRelatorioAtividadeMensal'])) {
        $anoAtual = $_SESSION['anoAtualRelatorioAtividadeMensal'];
    } else {
        $anoAtual = date('Y');
        $_SESSION['anoAtualRelatorioAtividadeMensal'] = $anoAtual;
    }
}
//echo "anoatual=>".$anoAtual;
$_SESSION['anoAtualRelatorioAtividadeMensal'] = $anoAtual;

/**
 * #####################################################################################################################################################
 */

//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;
$usuario 				= isset($_REQUEST['usuario'])?$_REQUEST['usuario']:null;
$anexo 					= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;
/**
 * Upload do Relatório Assinado
 */
$relatorio=0;
$extensaoNaoValida=0;
if($anexo != "")
{

    //Verificar se já está cadastrado
    $ram->setFk_idOrganismoAfiliado($idOrganismoAfiliado);
    $ram->setMes($mesAtual);
    $ram->setAno($anoAtual);
    $ram->setUsuario($usuario);

    $idRelatorioOGG = $ram->cadastraRelatorioOGGMensal();

    if ($_FILES['anexo']['name'] != "") {
        if(substr_count($_FILES['anexo']['name'],".")==1)
        {

            $extensao = strstr($_FILES['anexo']['name'], '.');
            if($extensao==".pdf"||
                $extensao==".png"||
                $extensao==".jpg"||
                $extensao==".jpeg"||
                $extensao==".PDF"||
                $extensao==".PNG"||
                $extensao==".JPG"||
                $extensao==".JPEG"
            )
            {

                $proximo_id = $ram->selecionaUltimoId();
                $caminho = "img/relatorio_ordem_guias_graal_mensal/" . basename($idRelatorioOGG . ".relatorio.ordem.guias.graal.mensal." . $mesAtual.".".$anoAtual);
                $ram->setIdRelatorioOGGMensal($idRelatorioOGG);
                $ram->setCaminhoRelatorioOGGMensal($caminho);
                if ($ram->atualizaCaminhoRelatorioOGGMensalAssinado()) {
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/relatorio_ordem_guias_graal_mensal/';
                    $uploadfile = $uploaddir . basename($idRelatorioOGG . ".relatorio.ordem.guias.graal.mensal." . $mesAtual.".".$anoAtual);
                    //echo $uploadfile;
                    if (move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile)) {
                        $relatorio = 1;
                    } else {
                        echo "<script type='text/javascript'>alert('Erro ao enviar o relatório assinado!');</script>";
                    }
                }
            }else{
                $extensaoNaoValida=1;
            }
        }else{
            $extensaoNaoValida=2;
        }
    }
}

if($relatorio==1){?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Relatório Assinado Enviado com Sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if($extensaoNaoValida==1){?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
/*
 * Verificar se já foi feito o upload do relatório assinado
 */
$caminhoRelatorioOGGMensal="";
if($ram->verificaSeJaCadastrado($mesAtual,$anoAtual,$idOrganismoAfiliado))
{
    $resultado = $ram->listaRelatorioOGGMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
    if($resultado)
    {
        foreach($resultado as $vetor)
        {
            $caminhoRelatorioOGGMensal = $vetor['caminhoRelatorioOGGMensal'];
        }
    }
}
/**
 * #####################################################################################################################################################
 */

?>
<?php

/****
 * Montar navegação por meses
 *****/

/*
 * Montar anterior e próximo
 */

$mesAnterior=date('m', strtotime('-1 months', strtotime($anoAtual."-".$mesAtual."-01")));
$mesProximo=date('m', strtotime('+1 months', strtotime($anoAtual."-".$mesAtual."-01")));

if($mesAtual==1)
{
    $anoAnterior=$anoAtual-1;
}else{
    $anoAnterior=$anoAtual;
}
if($mesAtual==12)
{
    $anoProximo=$anoAtual+1;
}else{
    $anoProximo=$anoAtual;
}

/*
 * Montar texto para ser exibido
 */
$mesAtualTexto = mesExtensoPortugues($mesAtual);
$mesProximoTexto = mesExtensoPortugues($mesProximo);
$mesAnteriorTexto = mesExtensoPortugues($mesAnterior);

/*
 * Dados do Organismo Afiliado
 */
$dadosOrganismo = $oc->buscaOrganismo($idOrganismoAfiliado);
$nomeOrganismo                      = retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);
$classificacaoOrganismoAfiliado     = $dadosOrganismo->getClassificacaoOrganismoAfiliado();

include_once('model/oggMensalClass.php');
$rom = new oggMensal();
$resultado7 = $rom->listaOggMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
//echo "<pre>";print_r($resultado7);
$entregue=0;
$entregueCompletamente=0;
$dataEntrega="";
$dataEntregaCompletamente="";
$statusEntrega=1;
if($resultado7)
{
    $entregue=1;
    $statusEntrega=2;
    foreach ($resultado7 as $v)
    {
        $codigoAssinatura = $v['numeroAssinatura'];
        $quemEntregou = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
        $dataEntrega = substr($v['dataEntrega'],8,2)."/".substr($v['dataEntrega'],5,2)."/".substr($v['dataEntrega'],0,4)." às ".substr($v['dataEntrega'],11,8);
        //echo "dataEntrega:".$dataEntrega;
        if($v['entregueCompletamente']==1)
        {
            $entregueCompletamente=1;
            $statusEntrega=3;
            $dataEntregaCompletamente = substr($v['dataEntregouCompletamente'],8,2)."/".substr($v['dataEntregouCompletamente'],5,2)."/".substr($v['dataEntregouCompletamente'],0,4)." às ".substr($v['dataEntregouCompletamente'],11,8);
        }

    }
}

switch ($statusEntrega)
{
    case 1:
        $class = "warning";
        $icon = "fa-location-arrow";
        $tituloBox = "Assinatura Eletrônica";
        break;
    case 2:
        $class = "default";
        $icon = "fa-pencil";
        $tituloBox = "O Documento não foi entregue completamente. Faltam assinaturas!";
        break;
    case 3:
        $class = "info";
        $icon = "fa-thumbs-up";
        $tituloBox = "O Documento foi 100% entregue";
        break;
}

//Ver se tem upload criado
include_once('model/columbaTrimestralClass.php');
$r = new RelatorioOGGMensal();
$resultado = $r->listaRelatorioOGGMensal($mesAtual,$anoAtual,$idOrganismoAfiliado);
$upload = "não";
if($resultado) {
    if (count($resultado) > 0) {
        $upload = "sim";
    }
}

//Ver data mais antiga da criação desse relatório
include_once('model/convocacaoRitualisticaOGGClass.php');
$cro = new convocacaoRitualisticaOGG();
$dataCriacao = $cro->dataCriacao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$dataCriacaoDocumento = substr($dataCriacao,8,2)."/".substr($dataCriacao,5,2)."/".substr($dataCriacao,0,4);

//Ver data da ultima alteração
$dataAlteracao = $cro->dataUltimaAlteracao($mesAtual,$anoAtual,$idOrganismoAfiliado);
$dataUltimaAlteracaoDocumento = substr($dataAlteracao,8,2)."/".substr($dataAlteracao,5,2)."/".substr($dataAlteracao,0,4);

//Responsáveis pela construção do documento
$arrResp = $cro->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);

//Responsáveis pela alteracao do documento
$arrRespAlteracao = $cro->responsaveis($mesAtual,$anoAtual,$idOrganismoAfiliado);
foreach ($arrRespAlteracao['seq'] as $k => $v)
{
    if(!in_array($v,$arrRespAlteracao['seq']))
    {
        $arrResp['seq'][$i] = $v;
        $arrResp['nome'][$i] = $arrRespAlteracao['nome'][$k];
        $i++;
    }
}

//Ordernar responsáveis por ordem do inicio de criação
/*
rsort ($arrResp['nome']);
rsort ($arrResp['seq']);
rsort ($arrResp['data']);
*/
$arrResp = unique_multidim_array_vals($arrResp);

sort($arrResp['nome'], SORT_NATURAL | SORT_FLAG_CASE);


//echo "<pre>";print_r($arrResp['nome']);


//Faltam assinar
$rom = new oggMensal();
$mestre = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
$secretario = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);
//$tjd = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,375);

//Montar Faltam assinar
$arrOficiaisFaltamAssinar=array();
if(!$mestre)
{
    $arrOficiaisFaltamAssinar[]="MESTRE DO ORGANISMO AFILIADO";
}
if(!$secretario)
{
    $arrOficiaisFaltamAssinar[]="SECRETÁRIO DO ORGANISMO AFILIADO";
}
//$oficiaisFaltamAssinar = implode(", ",$arr);
$arrOficiaisQueAssinaram = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado);

?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Relatório Mensal da OGG</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Atividades</a>
                </li>
                <li class="active">
                    <strong>Relatório Mensal da OGG</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Relatório Mensal da OGG</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row"></div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="widget lazur-bg p-xl">
                                        <ul class="list-unstyled m-t-md">
                                            <h2>
                                                <i class="fa fa-exclamation-triangle"></i> Avisos
                                            </h2>
                                            <br />
                                            <?php
                                            //Convocação Ritualística
                                            include_once('model/convocacaoRitualisticaOGGMembroClass.php');
                                            $c = new convocacaoRitualisticaOGGMembro();
                                            $totalConvocacaoMembros = $c->total(null, null, null, $mesAtual, $anoAtual,$idOrganismoAfiliado);
                                            //echo $totalConvocacaoMembros;
                                            include_once('model/convocacaoRitualisticaOGGNaoMembroClass.php');
                                            $n = new convocacaoRitualisticaOGGNaoMembro();
                                            $totalConvocacaoNaoMembros = $n->total(null, null, null, $mesAtual, $anoAtual,$idOrganismoAfiliado);
                                            //echo $totalConvocacaoNaoMembros;
                                            $totalConvocacao = $totalConvocacaoMembros+$totalConvocacaoNaoMembros;

                                            if($totalConvocacao>0)
                                            {
                                                echo "<li><span class='fa fa-thumbs-up'></span> Registros de participação em Convocação Ritualística</li>";
                                            }else{
                                                echo "<li><span class='fa fa-thumbs-down'></span> Registros de participação em Convocação Ritualística</li>";
                                            }
                                            //Rito de Passagem
                                            include_once('model/ritoPassagemOGGMembroClass.php');
                                            $c = new ritoPassagemOGGMembro();
                                            $totalRPMembros = $c->total(null, null, null, $mesAtual, $anoAtual,$idOrganismoAfiliado);
                                            //echo $totalRPMembros;
                                            include_once('model/ritoPassagemOGGMembroTestemunhaClass.php');
                                            $c = new ritoPassagemOGGMembroTestemunha();
                                            $totalRPMembrosTestemunha = $c->total(null, null, null, $mesAtual, $anoAtual,$idOrganismoAfiliado);
                                            //echo $totalRPMembros;
                                            include_once('model/ritoPassagemOGGNaoMembroClass.php');
                                            $n = new ritoPassagemOGGNaoMembro();
                                            $totalRPNaoMembros = $n->total(null, null, null, $mesAtual, $anoAtual,$idOrganismoAfiliado);
                                            //echo $totalRPNaoMembros;
                                            $totalRP = $totalRPMembros+$totalRPMembrosTestemunha+$totalRPNaoMembros;

                                            if($totalRP>0)
                                            {
                                                echo "<li><span class='fa fa-thumbs-up'></span> Registros de participação em Ritos de Passagem</li>";
                                            }else{
                                                echo "<li><span class='fa fa-thumbs-down'></span> Registros de participação em Ritos de Passagem</li>";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <table width="100%">
                                        <tr>
                                            <td align="center">
                                                <div class="form-group" id="data_4">
                                                    <div class="input-group date">
                                                        <table width="100">
                                                            <tr>
                                                                <td>
                                                                <span class="btn btn-w-m btn-primary" style="cursor: pointer; color:#fff;">
                                                                    <i class="fa fa-calendar-o" style="color:#fff"></i>

                                                                    <input type="hidden" id="calendario" class="form-control" value="<?php echo $mesAtual;?>/<?php echo date('d');?>/<?php echo $anoAtual;?>" />
                                                                    <span style="color:#fff">
                                                                        <?php echo $mesAtualTexto;?><?php echo $anoAtual;?>
                                                                    </span>
                                                                </span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="paginaAtual" class="form-control" value="<?php echo $corpo;?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table cellspacing="10" width="<?php if(!$usuarioApenasLeitura){?>300<?php }else{?>200<?php }?>">
                                                    <tr>
                                                        <td>
                                                <a href="#" class="btn btn-info  dim btn-large-dim btn-outline"
                                                   onclick="abrirPopupRelatorioMensalOGG('<?php echo $idOrganismoAfiliado;?>','<?php echo $mesAtual;?>','<?php echo $anoAtual;?>');">
                                                    <i class="fa fa-print" value="Impressão"></i>
                                                </a>
                                                        </td>
                                                        <td>
                                                <!--
                                                <?php //if(in_array("4",$arrNivelUsuario)){?>
                                                    &nbsp;
                                                    <a href="#"
                                                       class="btn btn-info  dim btn-large-dim btn-outline"
                                                       data-target="#mySeeUpload" data-toggle="modal">
                                                        <i class="fa fa-cloud-upload"></i>
                                                    </a>
                                                <?php //}
                                                ?>
                                                &nbsp;-->
                                                        </td>
                                                        <td>
                                                <a href="#" target="_blank"
                                                   class="btn btn-info  dim btn-large-dim btn-outline"
                                                   onclick="listaUploadRelatorioMensalOGG('<?php echo $mesAtual; ?>','<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>')"
                                                   data-target="#mySeeListaUpload" data-toggle="modal">
                                                    <i class="fa fa-paste"></i>
                                                </a>

                                                        </td>
                                                        <td>
                                                            <div id="entregarX">
                                                                <?php
                                                                if($statusEntrega==1&&!$usuarioApenasLeitura){
                                                                    ?>
                                                                    <a href="#" id="entregar"
                                                                       class="btn btn-info dim btn-large-dim btn-outline"
                                                                       onClick="entregarOggMensal('<?php echo $mesAtual; ?>','<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>','<?php echo $_SESSION['seqCadast'];?>')"><i class="fa fa-location-arrow"></i> </a>
                                                                    <?php
                                                                }
                                                                $mestre = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
                                                                $secretario = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);

                                                                //$pjd = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,371);
                                                                //$sjd = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,373);
                                                                //$tjd = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,375);
                                                                //$ma = $rom->buscarAssinaturasEmOggMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,377);

                                                                //Montar Faltam assinar
                                                                $arr=array();
                                                                if(!$mestre)
                                                                {
                                                                    $arr[]="MESTRE DO ORGANISMO AFILIADO";
                                                                }
                                                                if(!$secretario)
                                                                {
                                                                    $arr[]="SECRETÁRIO DO ORGANISMO AFILIADO";
                                                                }


                                                                /*
                                                                if(!$sjd)
                                                                {
                                                                    $arr[]="SECRETÁRIO DA JUNTA DEPOSITÁRIA";
                                                                }
                                                                if(!$tjd)
                                                                {
                                                                    $arr[]="TESOUREIRO DA JUNTA DEPOSITÁRIA";
                                                                }
                                                                if(!$ma)
                                                                {
                                                                    $arr[]="MESTRE AUXILIAR DO ORGANISMO AFILIADO";
                                                                }
                                                                */
                                                                $oficiaisFaltamAssinar = implode(",",$arr);
                                                                if($statusEntrega==2)
                                                                {
                                                                    ?>

                                                                    <a href="#" data-container="body" data-toggle="popover" data-placement="top" data-content="<?php if($mestre&&$secretario){?>Todos Assinaram. Processo de entrega finalizado!<?php }else{?>Faltam assinar: <?php echo $oficiaisFaltamAssinar;}?>" data-original-title="" title="" aria-describedby="popover408083" style="display: <?php if($entregue==0){?>none<?php }else{?>block<?php }?>;" id="faltaAssinar"
                                                                       class="btn btn-info dim btn-large-dim btn-outline"
                                                                    ><i class="fa fa-pencil"></i> </a>
                                                                    <?php
                                                                }
                                                                if($statusEntrega==3)
                                                                {
                                                                    ?>
                                                                    <a href="#" data-container="body" data-toggle="popover" data-placement="top" data-content="Todos Assinaram. Processo de entrega finalizado!" data-original-title="" title="" aria-describedby="popover408083" style="display: <?php if($entregue==0){?>none<?php }else{?>block<?php }?>;" id="faltaAssinar"
                                                                       class="btn btn-info dim btn-large-dim btn-outline"
                                                                    ><i class="fa fa-thumbs-up"></i> </a>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <i class="fa fa-info-circle"></i> <b>Dados do Relatório</b>
                                        </div>
                                        <div class="panel-body">
                                            <p>
                                            <ul>
                                                <li><b>Arquivos enviados: <?php echo $upload; if($upload=="sim"){ echo " - Não precisa necessariamente de assinatura eletrônica!";}?></b></li>
                                                <li>Data de Início da criação: <?php if($dataCriacaoDocumento!="//"){ $criado=1; echo $dataCriacaoDocumento;}else{ $criado=0; echo " <b>não começou a ser criado</b>";}?></li>
                                                <li>Última alteração: <?php if($dataUltimaAlteracaoDocumento!="//"){ echo $dataUltimaAlteracaoDocumento;}else{ echo "--";}?></li>
                                                <li>Responsável(is) pela elaboração desse documento:<?php if(count($arrResp['nome'])==0){ echo "--";}?></li>
                                                <ol>
                                                    <?php if(count($arrResp['nome'])>0){?>
                                                        <?php foreach ($arrResp['nome'] as $k => $v){?>
                                                            <li><?php echo $v;?></li>
                                                        <?php }?>
                                                    <?php }?>
                                                </ol>
                                                <li>Oficiais que precisam assinar esse documento se for entregue eletronicamente:</li>
                                                <ol>
                                                    <li>MESTRE DO ORGANISMO AFILIADO</li>
                                                    <li>SECRETÁRIO DO ORGANISMO AFILIADO</li>
                                                    <?php if($idClassificacaoOa==1||$idClassificacaoOa==3){?><li>TESOUREIRO DA JUNTA DEPOSITÁRIA</li><?php }?>
                                                </ol>

                                            </ul>
                                            </p>
                                        </div>
                                    </div>
                                    <input type="hidden" name="criado" id="criado" value="<?php echo $criado;?>">
                                </div>
                                <div class="col-lg-6">
                                    <div class="panel panel-<?php echo $class;?>">
                                        <div class="panel-heading">
                                            <i class="fa <?php echo $icon;?>"></i> <b><?php echo $tituloBox;?></b>
                                        </div>
                                        <div class="panel-body">
                                            <?php

                                            ?>
                                            <p>
                                            <ul>
                                                <li>Data da Entrega (Ass. Eletrônica): <?php if($dataEntrega!=""){ echo $dataEntrega;}else{ echo "<span id='dataEntrega'>--</span>";}?></li>
                                                <li>Quem Entregou (Ass. Eletrônica): <?php if($quemEntregou!=""){ echo $quemEntregou;}else{ echo "<span id='quemEntregou'>--</span>";}?></li>
                                                <li>Oficiais que assinaram esse documento: <?php if($entregue==0||$arrOficiaisQueAssinaram==false){?>--<?php }?></li>
                                                <?php if($entregue==1){?>
                                                    <ol>
                                                        <?php
                                                        if(count($arrOficiaisQueAssinaram)>0) {
                                                            foreach ($arrOficiaisQueAssinaram as $v) {
                                                                ?>
                                                                <li><?php echo $v['nomeUsuario']; ?> - <?php echo $v['nomeFuncao']; ?></li>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ol>
                                                <?php }?>
                                                <li>Assinaturas: <?php if($dataEntregaCompletamente!=""){ echo $dataEntregaCompletamente;}else{ echo "--";}?></li>
                                                <li>Faltam assinar: <?php if($entregue==0||count($arrOficiaisFaltamAssinar)==0){ echo "<span id='faltamAssinar'>--</span>";}?></li>
                                                <?php if($entregue==1){?>
                                                    <ol>
                                                        <?php
                                                        if(count($arrOficiaisFaltamAssinar)>0) {
                                                            foreach ($arrOficiaisFaltamAssinar as $v) {
                                                                ?>
                                                                <li><?php echo $v; ?></li>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ol>
                                                <?php }?>
                                                <li>Código da Assinatura Eletrônica: <?php if($codigoAssinatura!=""){ echo $codigoAssinatura;}else{ echo "<span id='numeroAssinatura'>--</span>";}?></li>
                                            </ul>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInUp">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-cloud-upload modal-icon"></i>
                    <h4 class="modal-title">Envio do Relatório OGG Mensal Assinado</h4>
                </div>
                <div class="modal-body">
                    <form id="relatorioAtividade" name="relatorioAtividade" class="form-horizontal" method="post" enctype="multipart/form-data" onsubmit="return validaUpload()">
                        <div class="form-group">
                            <div class="col-sm-10">
                                <input name="anexo" id="anexo" type="file" />
                            </div>
                            <div id="anexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                        </div>
                        <?php echo $nomeOrganismo; ?>
                        <input type="hidden" name="nomeOrganismo" id="nomeOrganismo" value="<?php echo $nomeOrganismo;?>" />
                        <input type="hidden" name="nomeUsuario" id="nomeUsuario" value="<?php echo $dadosUsuario->getNomeUsuario();?>" />
                        <input type="hidden" name="mesAtual" id="mesAtual" value="<?php echo $mesAtual;?>" />
                        <input type="hidden" name="anoAtual" id="anoAtual" value="<?php echo $anoAtual;?>" />
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>" />
                        <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" value="<?php echo $_SESSION['idOrganismoAfiliado'];?>" />
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-info" value="Enviar" onclick="document.relatorioAtividade.submit();" />
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInUp">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-cloud-upload modal-icon"></i>
                    <h4 class="modal-title">Relatório Assinado</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="listaUpload"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

<?php //} ?>