<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
      
        include_once("controller/historicoOAController.php");
        $h = new historicoOAController();
        $dados = $h->buscaHistorico($_GET['id']);
        
        $dataFormatada = substr($dados->getData(),8,2)."/".substr($dados->getData(),5,2)."/".substr($dados->getData(),0,4);
      ?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>
$(document).ready(function(){
	$("#dataNascimento1").mask("99/99/9999");
});
</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Histórico OA</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaSubMenu">Histórico OA</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de alteração do Histórico do OA</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaHistoricoOA">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="formulario" class="form-horizontal" method="post" action="acoes/acaoAlterar.php" onSubmit="return validaHistoricoOA()" >
                        <input type="hidden" name="idOrganismoAfiliadoHistorico" id="idOrganismoAfiliadoHistorico" value="<?php echo $_GET['id'];?>">
                        <div class="row">
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox($dados->getFk_idOrganismoAfiliado());
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Classificação: </label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="classificacao" id="classificacao" style="max-width: 150px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1" <?php if($dados->getClassificacao()==1){ echo 'selected';}?>>Loja</option>
                                    <option value="2" <?php if($dados->getClassificacao()==2){ echo 'selected';}?>>Pronaos</option>
                                    <option value="3" <?php if($dados->getClassificacao()==3){ echo 'selected';}?>>Capítulo</option>
                                    <option value="4" <?php if($dados->getClassificacao()==4){ echo 'selected';}?>>Heptada</option>
                                    <option value="5" <?php if($dados->getClassificacao()==5){ echo 'selected';}?>>Atrium</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="data" maxlength="10" id="data" type="text" class="form-control" value="<?php echo $dataFormatada;?>" style="max-width: 102px"  required="required">
                            </div>
                        </div>
         
                        
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Salvar"
									>
									<i class="fa fa-check fa-white"></i>&nbsp; Salvar
								</button>
								&nbsp; <a href="?corpo=buscaHistoricoOA"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
	
					</form>
                </div>
            </div>
        </div>
	</div>
	
	

</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	