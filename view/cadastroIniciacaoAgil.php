<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>				<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Área de Iniciações</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>
	                        <li>
	                            <a href="index.html">Iniciações</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Cadastro Ágil</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->

				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Cadastro Ágil de Iniciação</h5>
									<div class="ibox-tools">
										<!--
										<a class="btn btn-xs btn-primary" href="http://localhost/soa/includes/ritualistica_agendadas.php">
											<i class="fa fa-share"></i>&nbsp; Ir Para Iniciações Agendadas
										</a>
										-->
									</div>
								</div>
								<div class="ibox-content">
									<form action="" class="form-horizontal">
										<div class="form-group">
											<label class="col-sm-3 control-label">Organismo Afiliado: </label>
		                                    <div class="col-sm-9">
												<input class="form-control col-sm-1" id="oa" type="text" value="" style="max-width: 52px">
												<input class="form-control col-sm-8" id="oa_nome" type="text" value="" style="max-width: 320px; margin-left: 12px">
		                                    </div>
		                               	</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Tipo de Membro:</label>
		                                    <div class="col-sm-9">
												<input class="form-control col-sm-3" id="ordem" type="text" value="" style="max-width: 52px">
												<select class="form-control col-sm-3" name="ordem" style="max-width: 320px; margin-left: 12px">
			                                        <option>Rosacruz</option>
			                                        <option>TOM</option>
			                                    </select>
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
											<label class="col-sm-3 control-label">Iniciação:</label>
		                                    <div class="col-sm-9">
												<input class="form-control col-sm-3" id="iniciacao_grau" type="text" value="" style="max-width: 52px">
												<select class="form-control col-sm-3" name="grau" style="max-width: 320px; margin-left: 12px">
			                                        <option selected="">1º Grau do Templo</option>
													<option>2º Grau do Templo</option>
													<option>3º Grau do Templo</option>
													<option>4º Grau do Templo</option>
													<option>5º Grau do Templo</option>
													<option>6º Grau do Templo</option>
													<option>7º Grau do Templo</option>
													<option>8º Grau do Templo</option>
													<option>9º Grau do Templo</option>
													<option>10º Grau do Templo</option>
													<option>11º Grau do Templo</option>
													<option>12º Grau do Templo</option>
			                                    </select>
		                                    </div>
		                               	</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Data:</label>
		                                    <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
												<span class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</span>
												<input type="text" class="form-control" value="03/04/2014" style="max-width: 102px">
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
											<label class="col-sm-3 control-label">Local: </label>
		                                    <div class="col-sm-9">
												<input class="form-control" id="oa_nome" type="text" value="" style="max-width: 500px">
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
											<label class="col-sm-3 control-label">Observação: </label>
		                                    <div class="col-sm-6">
		                                    	<div style="border: #ccc solid 1px">
												<textarea class="summernote" name="comentario"></textarea>
												</div>
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
							                <label class="col-lg-3 control-label">Confirmados: </label>
							                <div class="input-group col-lg-9">
								                <div style="margin-left: 15px">
								                	<select data-placeholder="Nenhum membro confirmado..." class="chosen-select" multiple style="width:350px;" tabindex="4">
										                <option>[333102] ADRIANA WESTPHALEN VESCIA</option>
														<option>[122227] CLAUDIO FURLAN</option>
														<option>[144575] DAMIANA SANTOS CABRAL</option>
														<option>[132011] JOSE RAMOS DE ASSUMPÇÃO</option>
														<option>[41087] VILMA PEGORETTI</option>
									                </select>
								                </div>
							                </div>
						                </div>
						                <div class="form-group" style="margin-bottom: 0px">
						                	<label class="col-sm-3 control-label">  </label>
		                                    <div class="col-sm-9">
							                	<h3>Inclusão de membro não agendado previamente</h3>
							                </div>
						                </div>
						                <div class="form-group form-inline">
											<label class="col-sm-3 control-label">Membros confirmados:</label>
		                                    <div class="col-sm-9">
		                                    	<input class="form-control" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="form-control" id="mestre_2" type="text" value="" style="min-width: 320px; margin-left: 12px">
												<div class="checkbox" style="padding: 2px">
													<label class="i-checks">
														<input type="checkbox" value=""> Companheiro
													</label>
												</div><br>
												Digite o código de afiliação do membro e confira o nome antes de clicar em Incluir<br>
												<button class="btn btn-xs btn-primary" style="margin-top: 11px"> Incluir</button><br>
		                                    </div>
		                               	</div>
		                               	<div class="hr-line-dashed"></div>
		                               	<div class="form-group">
		                               		<div class="col-sm-offset-3 col-sm-6">
		                               			<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
			                                    	<i class="fa fa-check fa-white"></i>&nbsp;
			                                    	Salvar
				                                </button>
				                                &nbsp;
												<button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="left" title="Ao editar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
				                                    <i class="fa fa-eraser fa-white"></i>&nbsp;
				                                    Limpar
				                                </button>
				                                &nbsp;
												<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#mySeeMotivoCancelamento" data-toggle="tooltip" data-placement="left" title="Ao cancelar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
				                                    <i class="fa fa-times fa-white"></i>&nbsp;
				                                    Cancelar
				                                </button>
		                               		</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->
				
				<!-- Conteúdo DE INCLUDE FIM -->
				