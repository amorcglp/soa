<?php 

$anoAtual 				= isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');
//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;

include_once('model/recebimentoClass.php');
include_once('model/despesaClass.php');
include_once('lib/functions.php');

$r = new Recebimento();
$d = new Despesa();

//Inicializar vari�veis
$arrDash = array();
$arrDash['titulo']				= "Gráfico das Iniciações Anual";
$arrDash['subTitulo']			= "";
$arrDash['valor']				= "Mês atual: ".mesExtensoPortugues(date('m'));
$arrDash['cor']					= "lazur-bg";//navy-bg,lazur-bg,yellow-bg 
$arrDash['nomeGrafico']			= "flot-chart2";
$arrDash['corGrafico1']			= "#48D1CC";
$arrDash['corGrafico2']			= "#19a0a1";
$arrDash['tooltip']				= "Gráfico das Iniciações Anual";
$arrDash['link']				= "#";

//Dados do Gráfico
for($i=1;$i<=12;$i++)
{
	if($r->retornaEntrada($i,$anoAtual,$idOrganismoAfiliado)!="")
	{
    	$arrDash['d1'][$i] = $r->retornaEntrada($i,$anoAtual,$idOrganismoAfiliado);
	}else{
		$arrDash['dl'][$i] = 0;
	}   	
}

for($i=1;$i<=12;$i++)
{
	if($d->retornaSaida($i,$anoAtual,$idOrganismoAfiliado)!="")
	{
    	$arrDash['d2'][$i] = $d->retornaSaida($i,$anoAtual,$idOrganismoAfiliado);
	}else{
		$arrDash['d2'][$i] = 0;
	}   	
}

//echo "<pre>";print_r($arrNivel);
include("templates/widget2.tpl.php");
?>	