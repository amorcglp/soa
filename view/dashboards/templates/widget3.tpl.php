<div class="col-lg-2">
	<a href="<?php echo $arrDash['link'];?>" style="color: white" data-toggle="tooltip"
		data-placement="bottom" data-html="true" title="<?php echo $arrDash['tooltip'];?>"
		data-original-title="Tooltip on bottom">
		<div id="teste" style="width: 100%; z-index: 0;">
			<div class="widget style1 <?php echo $arrDash['cor'];?>">
				<div class="row vertical-align">

					<div class="col-xs-3">
						<i class="<?php echo $arrDash['icone'];?> fa-3x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<h2 class="font-bold">
						<?php echo $arrDash['valor'];?>
						</h2>
					</div>

				</div>
			</div>
		</div> 
	</a>
</div>

<!-- Mainly scripts -->
<!--
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script
	src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script
	src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<!--
<script src="js/inspinia.js"></script>
<script
	src="js/plugins/pace/pace.min.js"></script>
<script>
$(document).on('ready',function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
-->