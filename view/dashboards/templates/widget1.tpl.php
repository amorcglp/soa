<div class="col-lg-3">
	<a href="<?php echo $arrDash['link'];?>" style="color: white" data-toggle="tooltip"
		data-placement="bottom" data-html="true" title="<?php echo $arrDash['tooltip'];?>"
		data-original-title="Tooltip on bottom">
		<div class="widget style1 <?php echo $arrDash['cor'];?>">
			<div class="row">
				<div class="col-xs-4">
					<i class="<?php echo $arrDash['icone'];?> fa-5x"></i>
				</div>
				<div class="col-xs-8 text-right">
					<span> <?php echo $arrDash['titulo'];?> </span>
					<h3 class="font-bold"><?php echo $arrDash['valor'];?></h3>
				</div>
			</div>
		</div>
	</a>
</div> 