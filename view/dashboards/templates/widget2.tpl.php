
       
     <div class="col-lg-6">
            <a href="<?php echo $arrDash['link'];?>" style="color: white" data-toggle="tooltip"
		data-placement="bottom" data-html="true" title="<?php echo $arrDash['tooltip'];?>"
		data-original-title="Tooltip on bottom">
                <div class="widget <?php echo $arrDash['cor'];?> no-padding">
                    <div class="p-m">
                        <h1 class="m-xs"><?php echo $arrDash['titulo'];?></h1>

                        <h3 class="font-bold no-margins">
                            <?php echo $arrDash['subTitulo'];?>
                        </h3>
                        <small><?php echo $arrDash['valor'];?></small>
                    </div>
                    <div class="flot-chart">
                        <div class="flot-chart-content" id="<?php echo $arrDash['nomeGrafico'];?>"></div>
                    </div>
                </div>
            </a>
     </div>
	
    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script>
        $(document).ready(function(){
            var d1 = [[1262304000000, <?php echo $arrDash['d1']['1'];?>], [1264982400000, <?php echo $arrDash['d1']['2'];?>], [1267401600000, <?php echo $arrDash['d1']['3'];?>], [1270080000000, <?php echo $arrDash['d1']['4'];?>], [1272672000000, <?php echo $arrDash['d1']['5'];?>], [1275350400000, <?php echo $arrDash['d1']['6'];?>], [1277942400000, <?php echo $arrDash['d1']['7'];?>], [1280620800000, <?php echo $arrDash['d1']['8'];?>], [1283299200000, <?php echo $arrDash['d1']['9'];?>], [1285891200000, <?php echo $arrDash['d1']['10'];?>], [1288569600000, <?php echo $arrDash['d1']['11'];?>], [1291161600000, <?php echo $arrDash['d1']['12'];?>]];
            var d2 = [[1262304000000, <?php echo $arrDash['d2']['1'];?>], [1264982400000, <?php echo $arrDash['d2']['2'];?>], [1267401600000, <?php echo $arrDash['d2']['3'];?>], [1270080000000, <?php echo $arrDash['d2']['4'];?>], [1272672000000, <?php echo $arrDash['d2']['5'];?>], [1275350400000, <?php echo $arrDash['d2']['6'];?>], [1277942400000, <?php echo $arrDash['d2']['7'];?>], [1280620800000, <?php echo $arrDash['d2']['8'];?>], [1283299200000, <?php echo $arrDash['d2']['9'];?>], [1285891200000, <?php echo $arrDash['d2']['10'];?>], [1288569600000, <?php echo $arrDash['d2']['11'];?>], [1291161600000, <?php echo $arrDash['d2']['12'];?>]];

            var data1 = [
                { label: "Data 1", data: d1, color: '<?php echo $arrDash['corGrafico1'];?>'},
                { label: "Data 2", data: d2, color: '<?php echo $arrDash['corGrafico2'];?>' }
            ];
            $.plot($("#<?php echo $arrDash['nomeGrafico'];?>"), data1, {
                xaxis: {
                    tickDecimals: 0
                },
                series: {
                    lines: {
                        show: true,
                        fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 1
                            }, {
                                opacity: 1
                            }]
                        },
                    },
                    points: {
                        width: 0.1,
                        show: false
                    },
                },
                grid: {
                    show: false,
                    borderWidth: 0
                },
                legend: {
                    show: false,
                }
            });
        });
    </script>


