<div class="col-lg-3">
	<div class="ibox float-e-margins">
		<div class="ibox-title">
        	<span class="label label-success pull-right"><?php echo $arrDash['classificacao'];?></span>
            	<h5><?php echo $arrDash['titulo'];?></h5>
        </div>
        <div class="ibox-content">
        	<h1 class="no-margins"><?php echo $arrDash['valor'];?></h1>
            <div class="stat-percent font-bold text-success"><?php echo $arrDash['total'];?> <i class="fa fa-bolt"></i></div>
            <small>Total </small>
        </div>
    </div>
</div> 