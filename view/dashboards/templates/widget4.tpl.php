<div class="col-lg-3">
	<a href="<?php echo $arrDash['link'];?>" style="color: white" data-toggle="tooltip"
		data-placement="bottom" data-html="true" title="<?php echo $arrDash['tooltip'];?>"
		data-original-title="Tooltip on bottom">
	<div class="widget-head-color-box <?php echo $arrDash['cor'];?> p-lg text-center">
		<div class="m-b-md">
			<h2 class="font-bold no-margins"><?php echo $arrDash['titulo'];?></h2>
			<small><?php echo $arrDash['subTitulo'];?></small>
		</div>
		<img src="<?php echo $arrDash['img'];?>" class="img-circle circle-border m-b-md"
			alt="<?php echo $arrDash['titulo'];?>">
		<div>
			<span><?php echo $arrDash['texto1'];?></span> <br> 
			<span><?php echo $arrDash['texto2'];?></span> <br> 
			<span><?php echo $arrDash['texto3'];?></span>
		</div>
	</div>
	</a>
</div>
