<?php 
$server=135;
$mesAtual 				= date('m');
$anoAtual 				= date('Y');

//Trimestre atual
if(date('m')==1||date('m')==2||date('m')==3)
{
    $trimestreAtual=1;
}
if(date('m')==4||date('m')==5||date('m')==6)
{
    $trimestreAtual=2;
}
if(date('m')==7||date('m')==8||date('m')==9)
{
    $trimestreAtual=3;
}
if(date('m')==10||date('m')==11||date('m')==12)
{
    $trimestreAtual=4;
}

//$idOrganismoAfiliado 	= $_SESSION['idOrganismoAfiliado'];

$total=0;//Contador de relatórios NÃO entregues

//Encontrar Data Inicial com base no saldo inicial
include_once('model/saldoInicialClass.php');
$si = new saldoInicial();
$mesSaldoInicial = "01";
$anoSaldoInicial = "2016";
$trimestreSaldoInicial="1";
$temSaldoInicial = 1;

$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if($resultado)
{
	foreach($resultado as $vetor)
	{
		$mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
		$anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
                
                //Trimestre atual
                if($mesSaldoInicial==1||$mesSaldoInicial==2||$mesSaldoInicial==3)
                {
                    $trimestreSaldoInicial=1;
                }
                if($mesSaldoInicial==4||$mesSaldoInicial==5||$mesSaldoInicial==6)
                {
                    $trimestreSaldoInicial=2;
                }
                if($mesSaldoInicial==7||$mesSaldoInicial==8||$mesSaldoInicial==9)
                {
                    $trimestreSaldoInicial=3;
                }
                if($mesSaldoInicial==10||$mesSaldoInicial==11||$mesSaldoInicial==12)
                {
                    $trimestreSaldoInicial=4;
                }
	}
}else{
    $temSaldoInicial = 0;
}

/**
 * Entrega das Atas de Reunião Mensal Assinadas
 */

include_once('model/ataReuniaoMensalAssinadaClass.php');
$arma = new ataReuniaoMensalAssinada();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
//echo "temsaldoinicial---->".$temSaldoInicial;
$totalAtaReuniaoMensal=0;
$arrMesesNaoEntreguesAtaReuniaoMensal=array();
$k=0;
while($ano<=$anoAtual)
{
	$resultado = $arma->listaAtaReuniaoMensalAssinada(null,$mes,$ano,$idOrganismoAfiliado);
	if(!$resultado&&$temSaldoInicial==1)
	{
            $arrMesesNaoEntreguesAtaReuniaoMensal[$k]['mes']=mesExtensoPortugues($mes);
            $arrMesesNaoEntreguesAtaReuniaoMensal[$k]['ano']=$ano;
            //echo "<br>aqui";
            $total++;
            $totalAtaReuniaoMensal++;
            $k++;
	}
	if($mes==12&&$ano<=$anoAtual)
	{
		$ano++;
		$mes=0;
	} 
        
	$mes++;
        if($ano==$anoAtual&&$mes>=$mesAtual)
        {
            $ano++;
        }
}

//echo "total Ata de Reunião Mensal=>".$totalAtaReuniaoMensal;exit();
/**
 * Entrega das Atas de Posse Assinadas
 */

include_once('model/ataPosseAssinadaClass.php');
$apa = new ataPosseAssinada();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalAtaPosse=0;
$arrAnosNaoEntreguesAtaPosse=array();
$k=0;
while($ano<$anoAtual||(($ano==$anoAtual)&&(date('m')>4)))
{
	$resultado = $apa->listaAtaPosseAssinada(null,null,$ano,$idOrganismoAfiliado);
	if(!$resultado&&$temSaldoInicial==1)
	{
            $arrAnosNaoEntreguesAtaPosse[$k]['ano']=$ano;
            $total++;
            $totalAtaPosse++;
            $k++;
	}
	
	$ano++;
	
}

/**
* Entrega do Relatório de Membros Ativos
*/
include_once('model/membrosRosacruzesAtivosClass.php');
$mra	= new MembrosRosacruzesAtivos();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioMembrosAtivos=0;
$arrMesesNaoEntreguesRelatorioMembrosAtivos=array();
$k=0;
while($ano<=$anoAtual)
{
       $mResultado	= $mra->retornaMembrosRosacruzesAtivos($mes,$ano,$idOrganismoAfiliado);
       if($mResultado['numeroAtualMembrosAtivos']==0&&$temSaldoInicial==1)
       {
            $arrMesesNaoEntreguesRelatorioMembrosAtivos[$k]['mes']=mesExtensoPortugues($mes);
            $arrMesesNaoEntreguesRelatorioMembrosAtivos[$k]['ano']=$ano;
            $total++;
            $totalRelatorioMembrosAtivos++;
            $k++;
       }
       if($mes==12&&$ano<=$anoAtual)
       {
               $ano++;
               $mes=0;
       }
       $mes++;
       if($ano==$anoAtual&&$mes>=$mesAtual)
       {
            $ano++;
       }
}

/**
 * Entrega do Relatório Financeiro Mensal
 */

include_once('model/relatorioFinanceiroMensalClass.php');
$rfm = new RelatorioFinanceiroMensal();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioFinanceiroMensal=0;
$arrMesesNaoEntreguesRelatorioFinanceiroMensal=array();
$k=0;
while($ano<=$anoAtual)
{
	$resultado = $rfm->listaRelatorioFinanceiroMensal($mes,$ano,$idOrganismoAfiliado);
	if(!$resultado&&$temSaldoInicial==1)
	{
            $arrMesesNaoEntreguesRelatorioFinanceiroMensal[$k]['mes']=mesExtensoPortugues($mes);
            $arrMesesNaoEntreguesRelatorioFinanceiroMensal[$k]['ano']=$ano;
            $total++;
            $totalRelatorioFinanceiroMensal++;
            $k++;
	}
	if($mes==12&&$ano<=$anoAtual)
	{
		$ano++;
		$mes=0;
	}
	$mes++;
        if($ano==$anoAtual&&$mes>=$mesAtual)
        {
            $ano++;
        }
}

/**
 * Entrega do Relatório Financeiro Anual
 */

include_once('model/relatorioFinanceiroAnualClass.php');
$rfa = new RelatorioFinanceiroAnual();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioFinanceiroAnual=0;
$arrAnosNaoEntreguesRelatorioFinanceiroAnual=array();
$k=0;
while($ano<$anoAtual)
{
	$resultado = $rfa->listaRelatorioFinanceiroAnual($ano,$idOrganismoAfiliado);
	if(!$resultado&&$temSaldoInicial==1)
	{
            $arrAnosNaoEntreguesRelatorioFinanceiroAnual[$k]['ano']=$ano;
            $total++;
            $totalRelatorioFinanceiroAnual++;
            $k++;
	}
	
	$ano++;
	
	
}

/**
 * Entrega do Relatório de Imóveis Anual
 */

include_once('model/imovelClass.php');
$ic = new imovel();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioImovelAnual=0;
$arrAnosNaoEntreguesRelatorioImovel=array();
$resultado = $ic->listaImovel($idOrganismoAfiliado);
$k=0;
if(!$resultado&&$temSaldoInicial==1)
{
    $arrAnosNaoEntreguesRelatorioImovel[$k]['ano']=$ano;
    $total++;
    $totalRelatorioImovelAnual++;
    $k++;
}
	


if($totalRelatorioImovelAnual>1)
{
    $totalRelatorioImovelAnual=1;
    $total= $total-($totalRelatorioImovelAnual-1);
}


//Verificar se oa tem Mestre da Classe dos Artesãos
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismoAfiliado;
$seqFuncao='315';//Mestre da Classe dos Artesãos
include 'js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
//echo "<pre>";print_r($obj);
$temMestreClasseArtesaos=0;
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $temMestreClasseArtesaos=1;
    
    /**
    * Entrega dos Relatórios da Classe dos Artesãos assinados
    */

   include_once('model/relatorioClasseArtesaosAssinadoClass.php');
   $rcaa = new relatorioClasseArtesaosAssinado();

   $mes = (int) $mesSaldoInicial;
   $ano = (int) $anoSaldoInicial;
   //echo "mes------>".$mes;
   //echo "ano------>".$ano;
   $totalRelatorioClasseArtesaosMensal=0;
   $arrMesesNaoEntreguesRelatorioClasseArtesaosMensal=array();
   while($ano<=$anoAtual)
   {
           $resultado = $rcaa->listaRelatorioClasseArtesaosAssinado(null,$idOrganismoAfiliado,$mes,$ano);
           if(!$resultado&&$temSaldoInicial==1&&$mes!=1&&$mes!=12)//Dispensar de eles entregarem relatorio em janeiro e dezembro
           {
               $arrMesesNaoEntreguesRelatorioClasseArtesaosMensal[]=mesExtensoPortugues($mes)."/".$ano; 
                   //$total++;
                   $totalRelatorioClasseArtesaosMensal++;
           }
           if($mes==12&&$ano<=$anoAtual)
           {
                   $ano++;
                   $mes=0;
           }
           $mes++;
           if($ano==$anoAtual&&$mes>=$mesAtual)
           {
                $ano++;
           }
   }
}

/**
 * Entrega do Relatório de Atividades Mensal
 */

include_once('model/relatorioAtividadeMensalClass.php');
$ram = new RelatorioAtividadeMensal();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioAtividadeMensal=0;
$arrMesesNaoEntreguesRelatorioAtividadeMensal=array();
$k=0;
while($ano<=$anoAtual)
{
	$resultado = $ram->listaRelatorioAtividadeMensal($mes,$ano,$idOrganismoAfiliado);
	if(!$resultado&&$temSaldoInicial==1)
	{
            $arrMesesNaoEntreguesRelatorioAtividadeMensal[$k]['mes']=mesExtensoPortugues($mes);
            $arrMesesNaoEntreguesRelatorioAtividadeMensal[$k]['ano']=$ano;
		$total++;
		$totalRelatorioAtividadeMensal++;
                $k++;
	}
	if($mes==12&&$ano<=$anoAtual)
	{
		$ano++;
		$mes=0;
	}
	$mes++;
        if($ano==$anoAtual&&$mes>=$mesAtual)
        {
            $ano++;
        }
}

//Verificar se oa tem Coordenadora de Columbas

$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismoAfiliado;
$seqFuncao='609';//Orientadora de Columbas
include 'js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$temCoordenadoraColumbas=0;
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $temCoordenadoraColumbas=1;
    
    /**
    * Entrega dos Relatórios das Columbas Assinados
    */
    /*
   include_once('model/relatorioTrimestralColumbaAssinadoClass.php');
   $r = new RelatorioTrimestralColumbaAssinado();

   $mes = (int) $mesSaldoInicial;
   $ano = (int) $anoSaldoInicial;
   $trimestre = (int) $trimestreSaldoInicial;
   //echo "mes------>".$mes;
   //echo "ano------>".$ano;
   $totalRelatorioTrimestralColumba=0;
   $arrMesesNaoEntreguesRelatorioTrimestralColumba=array();
   while($trimestre<=$trimestreAtual&&$ano<=$anoAtual)
   {
           $resultado = $r->listaRelatorioTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
           if(!$resultado&&$temSaldoInicial==1)
           {
                $arrMesesNaoEntreguesRelatorioTrimestralColumba[]=$trimestre."º/".$ano;
                    $total++;
                    $totalRelatorioTrimestralColumba++;
           }
           if($trimestre==4&&$ano<=$anoAtual)
           {
                   $ano++;
                   $trimestre=0;
           }
           $trimestre++;
   }
   */
   /**
    * Entrega dos Relatórios Atividades das Columbas Assinados
    */

   include_once('model/relatorioAtividadeTrimestralColumbaAssinadoClass.php');
   $r = new RelatorioAtividadeTrimestralColumbaAssinado();

   $mes = (int) $mesSaldoInicial;
   $ano = (int) $anoSaldoInicial;
   $trimestre = (int) $trimestreSaldoInicial;
   //echo "mes------>".$mes;
   //echo "ano------>".$ano;
   $totalRelatorioAtividadeTrimestralColumba=0;
   $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba=array();
   while($ano<=$anoAtual)
   {
           $resultado = $r->listaRelatorioAtividadeTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
           if(!$resultado&&$temSaldoInicial==1)
           {
               $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba[]=$trimestre."ºtrim./".$ano;
                    $total++;
                    $totalRelatorioAtividadeTrimestralColumba++;
           }
           if($trimestre==4&&$ano<=$anoAtual)
           {
                   $ano++;
                   $trimestre=0;
           }
           $trimestre++;
           if($ano==$anoAtual&&$trimestre>=$trimestreAtual)
            {
                $ano++;
            }
   }
}

//Inicializar vari�veis
$arrDash = array();
$arrDash['link']				= "?corpo=buscaRelatoriosNaoEntregues";

//echo "<pre>";print_r($arrNivel);
include("templates/ibox2.tpl.php");


?>	
<script src="/js/bootstrap-tooltip.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
});
</script>