<?php 
$mesAtual 				= date('m');
$anoAtual 				= date('Y');
//$idOrganismoAfiliado 	= $_SESSION['idOrganismoAfiliado'];
/*
 * Calcular saldo dos anos anteriores
 */
//Encontrar Saldo e Data Inicial
include_once('model/saldoInicialClass.php');
$si = new saldoInicial();
$mesSaldoInicial = "01";
$anoSaldoInicial = "2016";

$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if($resultado)
{
	foreach($resultado as $vetor)
	{
		$mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
		$anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
		$saldoInicial = (float) str_replace(",",".",str_replace(".","",$vetor['saldoInicial']));
	}
}

include_once('model/recebimentoClass.php');
include_once('model/despesaClass.php');

$r = new Recebimento();
$d = new Despesa();

//Encontrar Saldo dos Meses Anteriores
$mes=intval($mesSaldoInicial);
$ano=intval($anoSaldoInicial);
$saldoAno =0;
$saldoGeral =0;
//echo "<br>mesSaldoInicial=>".$mes;
//echo "<br>anoSaldoInicial=>".$ano;
$y=0;

while($ano<=$anoAtual)
{
	if($mes<=12)
	{
		//echo "<br>data=>".$mes."-".$ano;
		if($y==0)
		{
			$saldoGeral = retornaSaldo($r,$d,$mes,$ano,$idOrganismoAfiliado,$saldoInicial);
		}else{
			$saldoGeral = retornaSaldo($r,$d,$mes,$ano,$idOrganismoAfiliado,0);
		}	
		//echo "<br>saldoGeral=>".$saldoGeral;
		$saldoAno += $saldoGeral;

		$mes++;	
	}else{
		$ano++;
                $mes=1;
	}
        if($ano==$anoAtual&&$mes>$mesAtual)
        {
            break;
        }
	$y++;
}


//Inicializar vari�veis
$arrDash = array();
$arrDash['titulo']				= "Saldo Atual";
$arrDash['valor']				= "R$ ".number_format($saldoAno, 2, ',', '.');
$arrDash['icone']				= "fa fa-line-chart";
$arrDash['cor']					= "lazur-bg";//navy-bg,lazur-bg,yellow-bg 
$arrDash['link']				= "?corpo=buscaRelatorioMensalFinanceiro";
$arrDash['tooltip']				= "Saldo atual";

//echo "<pre>";print_r($arrNivel);
include("templates/widget1.tpl.php");


?>	