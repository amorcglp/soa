<?php 

$anoAtual 				= isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');
//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;

include_once('model/membrosRosacruzesAtivosClass.php');
include_once('lib/functions.php');

$mra = new MembrosRosacruzesAtivos();

//Inicializar vari�veis
$arrDash = array();
$arrDash['titulo']				= "Gráfico dos Membros Ativos no OA Anual";
$arrDash['subTitulo']			= "";
$arrDash['valor']				= "Mês atual: ".mesExtensoPortugues(date('m'));
$arrDash['cor']					= "lazur-bg";//navy-bg,lazur-bg,yellow-bg 
$arrDash['nomeGrafico']			= "flot-chart2";
$arrDash['corGrafico1']			= "#48D1CC";
$arrDash['corGrafico2']			= "#19a0a1";
$arrDash['tooltip']				= "Gráfico dos Membros Ativos no OA Anual";
$arrDash['link']				= "?corpo=buscaRelatorioAnualFinanceiro";

//Dados do Gráfico
for($i=1;$i<=12;$i++)
{
	if($mra->retornaMembrosRosacruzesAtivos($i,$anoAtual,$idOrganismoAfiliado)!="")
	{
		$arrM = $mra->retornaMembrosRosacruzesAtivos($i,$anoAtual,$idOrganismoAfiliado);
    	$arrDash['d2'][$i] = $arrM['numeroAtualMembrosAtivos'];
	}else{
		$arrDash['d2'][$i] = 0;
	}   	
}

for($i=1;$i<=12;$i++)
{
	$arrDash['d1'][$i] = 0; 	
}

//echo "<pre>";print_r($arrDash);
include("templates/widget2.tpl.php");
?>	