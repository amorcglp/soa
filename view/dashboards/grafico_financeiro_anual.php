<?php 

$anoAtual 				= isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');
//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;

include_once('model/recebimentoClass.php');
include_once('model/despesaClass.php');
include_once('lib/functions.php');

$r = new Recebimento();
$d = new Despesa();

//Inicializar vari�veis
$arrDash = array();
$arrDash['titulo']				= "Gráfico Financeiro Anual";
$arrDash['subTitulo']			= "";
$arrDash['valor']				= "Mês atual: ".mesExtensoPortugues(date('m'));
$arrDash['cor']					= "navy-bg";//navy-bg,lazur-bg,yellow-bg 
$arrDash['nomeGrafico']			= "flot-chart1";
$arrDash['corGrafico1']			= "#17a084";
$arrDash['corGrafico2']			= "#127e68";
$arrDash['tooltip']				= "Gráfico Financeiro Anual";
$arrDash['link']				= "?corpo=buscaRelatorioAnualFinanceiro";
$arrDash['d1']="";
//Dados do Gráfico
for($i=1;$i<=12;$i++)
{
	if($r->retornaEntrada($i,$anoAtual,$idOrganismoAfiliado)!=false)
	{
    	$arrDash['d1'][$i] = round($r->retornaEntrada($i,$anoAtual,$idOrganismoAfiliado));
	}else{
		$arrDash['d1'][$i] = 0;
	}   	
}

for($i=1;$i<=12;$i++)
{
	if($d->retornaSaida($i,$anoAtual,$idOrganismoAfiliado)!=false)
	{
    	$arrDash['d2'][$i] = round($d->retornaSaida($i,$anoAtual,$idOrganismoAfiliado));
	}else{
		$arrDash['d2'][$i] = 0;
	}   	
}
//echo "<pre>";print_r($arrDash);
//echo "<pre>";print_r($arrNivel);
include("templates/widget2.tpl.php");
?>	