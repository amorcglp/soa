<?php 

$mesAtual 				= date('m');
$anoAtual 				= date('Y');

//Trimestre atual
if(date('m')==1||date('m')==2||date('m')==3)
{
    $trimestreAtual=1;
}
if(date('m')==4||date('m')==5||date('m')==6)
{
    $trimestreAtual=2;
}
if(date('m')==7||date('m')==8||date('m')==9)
{
    $trimestreAtual=3;
}
if(date('m')==10||date('m')==11||date('m')==12)
{
    $trimestreAtual=4;
}

//$idOrganismoAfiliado 	= $_SESSION['idOrganismoAfiliado'];

$total=0;//Contador de relatórios entregues

//Encontrar Data Inicial com base no saldo inicial
include_once('model/saldoInicialClass.php');
$si = new saldoInicial();
$mesSaldoInicial = "01";
$anoSaldoInicial = "2016";
$trimestreSaldoInicial="1";

$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if($resultado)
{
	foreach($resultado as $vetor)
	{
		$mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
		$anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
                
                //Trimestre atual
                if($mesSaldoInicial==1||$mesSaldoInicial==2||$mesSaldoInicial==3)
                {
                    $trimestreSaldoInicial=1;
                }
                if($mesSaldoInicial==4||$mesSaldoInicial==5||$mesSaldoInicial==6)
                {
                    $trimestreSaldoInicial=2;
                }
                if($mesSaldoInicial==7||$mesSaldoInicial==8||$mesSaldoInicial==9)
                {
                    $trimestreSaldoInicial=3;
                }
                if($mesSaldoInicial==10||$mesSaldoInicial==11||$mesSaldoInicial==12)
                {
                    $trimestreSaldoInicial=4;
                }
	}
}



/**
 * Entrega das Atas de Reunião Mensal Assinadas
 */

include_once('model/ataReuniaoMensalAssinadaClass.php');
$arma = new ataReuniaoMensalAssinada();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalAtaReuniaoMensal=0;
while($mes<=$mesAtual&&$ano<=$anoAtual)
{
	$resultado = $arma->listaAtaReuniaoMensalAssinada(null,$mes,$ano,$idOrganismoAfiliado);
	if($resultado)
	{
		if(count($resultado)>0)
		{
                    //echo "<br>teste1";
			$total++;
			$totalAtaReuniaoMensal++;
		}
	}
	if($mes==12&&$ano<=$anoAtual)
	{
		$ano++;
		$mes=0;
	}
	$mes++;
        if($ano==$anoAtual&&$mes>=$mesAtual)
        {
            $ano++;
        }
}

/**
 * Entrega das Atas de Posse Assinadas
 */

include_once('model/ataPosseAssinadaClass.php');
$apa = new ataPosseAssinada();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalAtaPosse=0;
while($mes<=$mesAtual&&$ano<=$anoAtual)
{
	$resultado = $apa->listaAtaPosseAssinada(null,$mes,$ano,$idOrganismoAfiliado);
	if($resultado)
	{
		if(count($resultado)>0)
		{
                    //echo "<br>teste2";
			$total++;
			$totalAtaPosse++;
		}
	}
	if($mes==12&&$ano<=$anoAtual)
	{
		$ano++;
		$mes=0;
	}
	$mes++;
        if($ano==$anoAtual&&$mes>=$mesAtual)
        {
            $ano++;
        }
}

/**
* Entrega do Relatório de Membros Ativos
*/
include_once('model/membrosRosacruzesAtivosClass.php');
$mra	= new MembrosRosacruzesAtivos();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioMembrosAtivos=0;
while($ano<=$anoAtual)
{
       $mResultado	= $mra->retornaMembrosRosacruzesAtivos($mes,$ano,$idOrganismoAfiliado);
       if($mResultado['numeroAtualMembrosAtivos']>0)
       {
           //echo "<br>teste3";
               $total++;
               $totalRelatorioMembrosAtivos++;
       }
       if($mes==12&&$ano<=$anoAtual)
       {
               $ano++;
               $mes=0;
       }
       $mes++;
       if($ano==$anoAtual&&$mes>=$mesAtual)
       {
            $ano++;
       }
}

/**
 * Entrega do Relatório Financeiro Mensal
 */

include_once('model/relatorioFinanceiroMensalClass.php');
$rfm = new RelatorioFinanceiroMensal();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioFinanceiroMensal=0;
while($ano<=$anoAtual)
{
	$resultado = $rfm->listaRelatorioFinanceiroMensal($mes,$ano,$idOrganismoAfiliado);
	if($resultado)
	{
		if(count($resultado)>0)
		{
                    //echo "<br>teste4";
			$total++;
			$totalRelatorioFinanceiroMensal++;
		}
	}
	if($mes==12&&$ano<=$anoAtual)
	{
		$ano++;
		$mes=0;
	}
	$mes++;
        if($ano==$anoAtual&&$mes>=$mesAtual)
        {
            $ano++;
        }
}

/**
 * Entrega do Relatório Financeiro Anual
 */

include_once('model/relatorioFinanceiroAnualClass.php');
$rfa = new RelatorioFinanceiroAnual();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioFinanceiroAnual=0;
while($ano<=$anoAtual)
{
	$resultado = $rfa->listaRelatorioFinanceiroAnual($ano,$idOrganismoAfiliado);
	if($resultado)
	{
		if(count($resultado)>0)
		{
                    //echo "<br>teste5";
			$total++;
			$totalRelatorioFinanceiroAnual++;
		}
	}
	
	$ano++;
	
}

/**
 * Entrega do Relatório de Imóveis Anual
 */

include_once('model/imovelClass.php');
$ic = new imovel();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioImovelAnual=0;

$resultado = $ic->listaImovel($idOrganismoAfiliado);
if($resultado)
{
        if(count($resultado)>0)
        {
            //echo "<br>teste6";
            $total++;
            $totalRelatorioImovelAnual++;
        }
}

//Verificar se oa tem Mestre da Classe dos Artesãos
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismoAfiliado;
$seqFuncao='315';//Mestre da Classe dos Artesãos
include 'js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$temMestreClasseArtesaos=0;
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $temMestreClasseArtesaos=1;
    
    /**
    * Entrega dos Relatórios da Classe dos Artesãos Mensal Assinados
    */

   include_once('model/relatorioClasseArtesaosAssinadoClass.php');
   $rcaa = new relatorioClasseArtesaosAssinado();

   $mes = (int) $mesSaldoInicial;
   $ano = (int) $anoSaldoInicial;
   //echo "mes------>".$mes;
   //echo "ano------>".$ano;
   $totalRelatorioClasseArtesaosMensal=0;
   while($mes<=$mesAtual&&$ano<=$anoAtual)
   {
           $resultado = $rcaa->listaRelatorioClasseArtesaosAssinado(null,$idOrganismoAfiliado,$mes,$ano);
           if($resultado&&$mes!=1&&$mes!=12)
           {
                   if(count($resultado)>0)
                   {
                       //echo "<br>teste7";
                           $total++;
                           $totalRelatorioClasseArtesaosMensal++;
                   }
           }
           if($mes==12&&$ano<=$anoAtual)
           {
                   $ano++;
                   $mes=0;
           }
           $mes++;
           if($ano==$anoAtual&&$mes>=$mesAtual)
           {
                 $ano++;
           }
   }
}

/**
 * Entrega do Relatório de Atividades Mensal
 */

include_once('model/relatorioAtividadeMensalClass.php');
$ram = new RelatorioAtividadeMensal();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioAtividadeMensal=0;
while($ano<=$anoAtual)
{
	$resultado = $ram->listaRelatorioAtividadeMensal($mes,$ano,$idOrganismoAfiliado);
	if($resultado)
	{
		if(count($resultado)>0)
		{
                    //echo "<br>teste8";
			$total++;
			$totalRelatorioAtividadeMensal++;
		}
	}
	if($mes==12&&$ano<=$anoAtual)
	{
		$ano++;
		$mes=0;
	}
	$mes++;
        if($ano==$anoAtual&&$mes>=$mesAtual)
        {
             $ano++;
        }
}

//Verificar se oa tem Coordenadora de Columbas
$ocultar_json=1;
$naoAtuantes='N';
$atuantes='S';
$siglaOA=$siglaOrganismoAfiliado;
$seqFuncao='609';//Orientadora de Columbas
include 'js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return),true);
$temCoordenadoraColumbas=0;
if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
{
    $temCoordenadoraColumbas=1;
    
    /**
    * Entrega dos Relatórios das Columbas Assinados
    */
    /*
   include_once('model/relatorioTrimestralColumbaAssinadoClass.php');
   $r = new RelatorioTrimestralColumbaAssinado();

   $mes = (int) $mesSaldoInicial;
   $ano = (int) $anoSaldoInicial;
   $trimestre = (int) $trimestreSaldoInicial;
   //echo "mes------>".$mes;
   //echo "ano------>".$ano;
   $totalRelatorioTrimestralColumba=0;
   while($trimestre<=$trimestreAtual&&$ano<=$anoAtual)
   {
           $resultado = $r->listaRelatorioTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
           if($resultado)
           {
                   if(count($resultado)>0)
                   {
                       //echo "<br>teste9";
                           $total++;
                           $totalRelatorioTrimestralColumba++;
                   }
           }
           if($trimestre==4&&$ano<=$anoAtual)
           {
                   $ano++;
                   $trimestre=0;
           }
           $trimestre++;
   }
   */
   /**
    * Entrega dos Relatórios Atividades das Columbas Assinados
    */

   include_once('model/relatorioAtividadeTrimestralColumbaAssinadoClass.php');
   $r = new RelatorioAtividadeTrimestralColumbaAssinado();

   $mes = (int) $mesSaldoInicial;
   $ano = (int) $anoSaldoInicial;
   $trimestre = (int) $trimestreSaldoInicial;
   //echo "mes------>".$mes;
   //echo "ano------>".$ano;
   $totalRelatorioAtividadeTrimestralColumba=0;
   while($ano<=$anoAtual)
   {
           $resultado = $r->listaRelatorioAtividadeTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
           if($resultado)
           {
                   if(count($resultado)>0)
                   {
                       //echo "<br>teste10";
                           $total++;
                           $totalRelatorioAtividadeTrimestralColumba++;
                   }
           }
           if($trimestre==4&&$ano<=$anoAtual)
           {
                   $ano++;
                   $trimestre=0;
           }
           $trimestre++;
           if($ano==$anoAtual&&$trimestre>=$trimestreAtual)
           {
                 $ano++;
           }
   }
}

//Inicializar vari�veis
$arrDash = array();
$arrDash['tooltip']				= "Relatórios Entregues"
        . "\nAtas de Reunião Mensal: ".$totalAtaReuniaoMensal.""
        . "\nAtas de Posse: ".$totalAtaPosse.""
        . "\nRel. Membros Ativos: ".$totalRelatorioMembrosAtivos.""
        . "\nRel. Financ. Mensais: ".$totalRelatorioFinanceiroMensal.""
        . "\nRel. Financ. Anuais: ".$totalRelatorioFinanceiroAnual.""
        . "\nRel. Imovel Anuais: ".$totalRelatorioImovelAnual;
/*
                                                                    if($temMestreClasseArtesaos==1)
                                                                    {    
$arrDash['tooltip']				.= "                    Rel. Classe dos Artesãos: ".$totalRelatorioClasseArtesaosMensal."<br>";
                                                                    }
 */
$arrDash['tooltip']				.= "\nRel. de Atividades: ".$totalRelatorioAtividadeMensal; 
                                                                    if($temCoordenadoraColumbas==1&&$sessao->getValue("classificacaoOA")!="Pronaos")
                                                                    {    
//$arrDash['tooltip']				.= "                    Rel. Trim. Columbas: ".$totalRelatorioTrimestralColumba."<br>";
$arrDash['tooltip']				.= "\nRel. Trim. Ativ. Columbas: ".$totalRelatorioAtividadeTrimestralColumba;
                                                                    }
$arrDash['valor']				= $total;
$arrDash['icone']				= "fa fa-thumbs-up";
$arrDash['cor']					= "navy-bg";//navy-bg,lazur-bg,yellow-bg 
$arrDash['link']				= "#";

//echo "<pre>";print_r($arrNivel);
include("templates/widget3.tpl.php");


?>	