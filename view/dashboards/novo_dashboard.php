<?php
$server = 135;
$mesAtual = date('m');
$anoAtual = date('Y');

//Trimestre atual
if (date('m') == 1 || date('m') == 2 || date('m') == 3) {
    $trimestreAtual = 1;
}
if (date('m') == 4 || date('m') == 5 || date('m') == 6) {
    $trimestreAtual = 2;
}
if (date('m') == 7 || date('m') == 8 || date('m') == 9) {
    $trimestreAtual = 3;
}
if (date('m') == 10 || date('m') == 11 || date('m') == 12) {
    $trimestreAtual = 4;
}

//$idOrganismoAfiliado 	= $_SESSION['idOrganismoAfiliado'];

//$total = 0; //Contador de relatórios NÃO entregues
//Encontrar Data Inicial com base no saldo inicial
include_once('model/saldoInicialClass.php');
$si = new saldoInicial();
$mesSaldoInicial = "01";
$anoSaldoInicial = "2016";
$trimestreSaldoInicial = "1";
$temSaldoInicial = 1;

$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if ($resultado) {
    foreach ($resultado as $vetor) {
        $mesSaldoInicial = substr($vetor['dataSaldoInicial'], 5, 2);
        $anoSaldoInicial = substr($vetor['dataSaldoInicial'], 0, 4);

        //Trimestre atual
        if ($mesSaldoInicial == 1 || $mesSaldoInicial == 2 || $mesSaldoInicial == 3) {
            $trimestreSaldoInicial = 1;
        }
        if ($mesSaldoInicial == 4 || $mesSaldoInicial == 5 || $mesSaldoInicial == 6) {
            $trimestreSaldoInicial = 2;
        }
        if ($mesSaldoInicial == 7 || $mesSaldoInicial == 8 || $mesSaldoInicial == 9) {
            $trimestreSaldoInicial = 3;
        }
        if ($mesSaldoInicial == 10 || $mesSaldoInicial == 11 || $mesSaldoInicial == 12) {
            $trimestreSaldoInicial = 4;
        }
    }
} else {
    $temSaldoInicial = 0;
}

/**
 * Entrega da Agenda Anual do Organismo
 */

include_once('model/agendaAtividadeClass.php');
$ag = new AgendaAtividade();
$cobraAgendaAnual=false;
$resultado = $ag->listaAgendaAtividade($idOrganismoAfiliado,date('Y'));
if (!$resultado && $temSaldoInicial == 1) {
    $cobraAgendaAnual=true;
}


/**
 * Entrega das Atas de Reunião Mensal Assinadas
 */
include_once('model/ataReuniaoMensalAssinadaClass.php');
$arma = new ataReuniaoMensalAssinada();

include_once('model/ataReuniaoMensalClass.php');
$arm = new ataReuniaoMensal();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
//echo "temsaldoinicial---->".$temSaldoInicial;
$totalAtaReuniaoMensal = 0;
$arrMesesNaoEntreguesAtaReuniaoMensal = array();
$k = 0;
while ($ano <= $anoAtual) {
    $resultado = $arma->listaAtaReuniaoMensalAssinada(null, $mes, $ano, $idOrganismoAfiliado);
    if (!$resultado && $temSaldoInicial == 1) {
        $resultado2 = $arm->listaAtaReuniaoMensal($mes,$ano,$idOrganismoAfiliado,true);
        if (!$resultado2) {
            $arrMesesNaoEntreguesAtaReuniaoMensal[$k]['mes'] = mesExtensoPortugues($mes);
            $arrMesesNaoEntreguesAtaReuniaoMensal[$k]['ano'] = $ano;
            //echo "<br>aqui";
            //$total++;
            $totalAtaReuniaoMensal++;
            $k++;
        }
    }
    if ($mes == 12 && $ano <= $anoAtual) {
        $ano++;
        $mes = 0;
    }

    $mes++;
    if ($ano == $anoAtual && $mes >= $mesAtual) {
        $ano++;
    }
}

//echo "total Ata de Reunião Mensal=>".$totalAtaReuniaoMensal;exit();
/**
 * Entrega das Atas de Posse Assinadas
 */
include_once('model/ataPosseAssinadaClass.php');
$apa = new ataPosseAssinada();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalAtaPosse = 0;
$arrAnosNaoEntreguesAtaPosse = array();
$k = 0;
while ($ano < $anoAtual || (($ano == $anoAtual) && (date('m') > 4))) {
    $resultado = $apa->listaAtaPosseAssinada(null, null, $ano, $idOrganismoAfiliado);
    if (!$resultado && $temSaldoInicial == 1) {
        $arrAnosNaoEntreguesAtaPosse[$k]['ano'] = $ano;
        //$total++;
        $totalAtaPosse++;
        $k++;
    }

    $ano++;
}

/**
 * Entrega do Relatório de Membros Ativos
 */
include_once('model/membrosRosacruzesAtivosClass.php');
$mra = new MembrosRosacruzesAtivos();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioMembrosAtivos = 0;
$arrMesesNaoEntreguesRelatorioMembrosAtivos = array();
$k = 0;
while ($ano <= $anoAtual) {
    $mResultado = $mra->retornaMembrosRosacruzesAtivos($mes, $ano, $idOrganismoAfiliado);
    if ($mResultado['numeroAtualMembrosAtivos'] == 0 && $temSaldoInicial == 1) {
        $arrMesesNaoEntreguesRelatorioMembrosAtivos[$k]['mes'] = mesExtensoPortugues($mes);
        $arrMesesNaoEntreguesRelatorioMembrosAtivos[$k]['ano'] = $ano;
        //$total++;
        $totalRelatorioMembrosAtivos++;
        $k++;
    }
    if ($mes == 12 && $ano <= $anoAtual) {
        $ano++;
        $mes = 0;
    }
    $mes++;
    if ($ano == $anoAtual && $mes >= $mesAtual) {
        $ano++;
    }
}

/**
 * Entrega do Relatório Financeiro Mensal
 */
include_once('model/relatorioFinanceiroMensalClass.php');
$rfm = new RelatorioFinanceiroMensal();

include_once('model/financeiroMensalClass.php');
$fm = new FinanceiroMensal();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioFinanceiroMensal = 0;
$arrMesesNaoEntreguesRelatorioFinanceiroMensal = array();
$k = 0;
while ($ano <= $anoAtual) {
    $resultado = $rfm->listaRelatorioFinanceiroMensal($mes, $ano, $idOrganismoAfiliado);
    if (!$resultado && $temSaldoInicial == 1) {
        $resultado2 = $fm->listaFinanceiroMensal($mes,$ano,$idOrganismoAfiliado,true);
        if (!$resultado2) {
            $arrMesesNaoEntreguesRelatorioFinanceiroMensal[$k]['mes'] = mesExtensoPortugues($mes);
            $arrMesesNaoEntreguesRelatorioFinanceiroMensal[$k]['ano'] = $ano;
            //$total++;
            $totalRelatorioFinanceiroMensal++;
            $k++;
        }
    }
    if ($mes == 12 && $ano <= $anoAtual) {
        $ano++;
        $mes = 0;
    }
    $mes++;
    if ($ano == $anoAtual && $mes >= $mesAtual) {
        $ano++;
    }
}

/**
 * Entrega do Relatório Financeiro Anual
 */
include_once('model/relatorioFinanceiroAnualClass.php');
$rfa = new RelatorioFinanceiroAnual();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioFinanceiroAnual = 0;
$arrAnosNaoEntreguesRelatorioFinanceiroAnual = array();
$k = 0;
while ($ano < $anoAtual) {
    $resultado = $rfa->listaRelatorioFinanceiroAnual($ano, $idOrganismoAfiliado);
    if (!$resultado && $temSaldoInicial == 1) {
        $arrAnosNaoEntreguesRelatorioFinanceiroAnual[$k]['ano'] = $ano;
        //$total++;
        $totalRelatorioFinanceiroAnual++;
        $k++;
    }

    $ano++;
}

/**
* Entrega do Relatório de Imóveis Anual
*/

require ('model/imovelControleClass.php');

$ic = new imovelControle();

//$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$totalRelatorioImovelAnual=0;
$arrAnosNaoEntreguesRelatorioImovel=array();
$k = 0;
while($ano<=$anoAtual)
{
       $resultado = $ic->verificaImovelControle($idOrganismoAfiliado,$ano);
       if(!$resultado&&$temSaldoInicial==1)
       {
            $arrAnosNaoEntreguesRelatorioImovel[$k]['ano']=$ano;
            //$total++;
            $totalRelatorioImovelAnual++;
            $k++;
       }
       $ano++;
}


//Verificar se oa tem Mestre da Classe dos Artesãos
$ocultar_json = 1;
$naoAtuantes = 'N';
$atuantes = 'S';
$siglaOA = $siglaOrganismoAfiliado;
$seqFuncao = '315'; //Mestre da Classe dos Artesãos
include 'js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return), true);
//echo "<pre>";print_r($obj);
$temMestreClasseArtesaos = 0;
if (isset($obj['result'][0]['fields']['fArrayOficiais'][0])) {
    $temMestreClasseArtesaos = 1;

    /**
     * Entrega dos Relatórios da Classe dos Artesãos assinados
     */
    include_once('model/relatorioClasseArtesaosAssinadoClass.php');
    $rcaa = new relatorioClasseArtesaosAssinado();

    include_once('model/relatorioClasseArtesaosClass.php');
    $rca = new relatorioClasseArtesaos();

    $mes = (int) $mesSaldoInicial;
    $ano = (int) $anoSaldoInicial;
    //echo "mes------>".$mes;
    //echo "ano------>".$ano;
    $totalRelatorioClasseArtesaosMensal = 0;
    $arrMesesNaoEntreguesRelatorioClasseArtesaosMensal = array();
    while ($ano <= $anoAtual) {
        $resultado = $rcaa->listaRelatorioClasseArtesaosAssinado(null, $idOrganismoAfiliado, $mes, $ano);
        if (!$resultado && $temSaldoInicial == 1 && $mes != 1 && $mes != 12) {//Dispensar de eles entregarem relatorio em janeiro e dezembro
            $resultado2 = $rca->listaRelatorioClasseArtesaos($idOrganismoAfiliado,$mes,$ano,true);
            if (!$resultado2) {
                $arrMesesNaoEntreguesRelatorioClasseArtesaosMensal[] = mesExtensoPortugues($mes) . "/" . $ano;
                //$total++;
                $totalRelatorioClasseArtesaosMensal++;
            }
        }
        if ($mes == 12 && $ano <= $anoAtual) {
            $ano++;
            $mes = 0;
        }
        $mes++;
        if ($ano == $anoAtual && $mes >= $mesAtual) {
            $ano++;
        }
    }
}

/**
 * Entrega do Relatório de Atividades Mensal
 */
include_once('model/relatorioAtividadeMensalClass.php');
$ram = new RelatorioAtividadeMensal();

include_once('model/atividadeEstatutoMensalClass.php');
$aem = new atividadeEstatutoMensal();

$mes = (int) $mesSaldoInicial;
$ano = (int) $anoSaldoInicial;
//echo "mes------>".$mes;
//echo "ano------>".$ano;
$total=0;
$totalRelatorioAtividadeMensal = 0;
$arrMesesNaoEntreguesRelatorioAtividadeMensal = array();
$k = 0;
while ($ano <= $anoAtual) {
    $resultado = $ram->listaRelatorioAtividadeMensal($mes, $ano, $idOrganismoAfiliado);
    if (!$resultado && $temSaldoInicial == 1) {
        $resultado2 = $aem->listaAtividadeMensal($mes,$ano,$idOrganismoAfiliado,true);
        if (!$resultado2) {
            $arrMesesNaoEntreguesRelatorioAtividadeMensal[$k]['mes'] = mesExtensoPortugues($mes);
            $arrMesesNaoEntreguesRelatorioAtividadeMensal[$k]['ano'] = $ano;
            $total++;
            $totalRelatorioAtividadeMensal++;
            $k++;
        }

    }
    if ($mes == 12 && $ano <= $anoAtual) {
        $ano++;
        $mes = 0;
    }
    $mes++;
    if ($ano == $anoAtual && $mes >= $mesAtual) {
        $ano++;
    }
}

//Verificar se oa tem Coordenadora de Columbas

$ocultar_json = 1;
$naoAtuantes = 'N';
$atuantes = 'S';
$siglaOA = $siglaOrganismoAfiliado;
$seqFuncao = '609'; //Orientadora de Columbas
include 'js/ajax/retornaFuncaoMembro.php';
$obj = json_decode(json_encode($return), true);
$temCoordenadoraColumbas = 0;
if (isset($obj['result'][0]['fields']['fArrayOficiais'][0])) {
    $temCoordenadoraColumbas = 1;

    /**
     * Entrega dos Relatórios das Columbas Assinados
     */
    /*
      include_once('model/relatorioTrimestralColumbaAssinadoClass.php');
      $r = new RelatorioTrimestralColumbaAssinado();

      $mes = (int) $mesSaldoInicial;
      $ano = (int) $anoSaldoInicial;
      $trimestre = (int) $trimestreSaldoInicial;
      //echo "mes------>".$mes;
      //echo "ano------>".$ano;
      $totalRelatorioTrimestralColumba=0;
      $arrMesesNaoEntreguesRelatorioTrimestralColumba=array();
      while($trimestre<=$trimestreAtual&&$ano<=$anoAtual)
      {
      $resultado = $r->listaRelatorioTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
      if(!$resultado&&$temSaldoInicial==1)
      {
      $arrMesesNaoEntreguesRelatorioTrimestralColumba[]=$trimestre."º/".$ano;
      $total++;
      $totalRelatorioTrimestralColumba++;
      }
      if($trimestre==4&&$ano<=$anoAtual)
      {
      $ano++;
      $trimestre=0;
      }
      $trimestre++;
      }
     */
    /**
     * Entrega dos Relatórios Atividades das Columbas Assinados
     */
    include_once('model/relatorioAtividadeTrimestralColumbaAssinadoClass.php');
    $r = new RelatorioAtividadeTrimestralColumbaAssinado();

    include_once('model/columbaTrimestralClass.php');
    $ct = new columbaTrimestral();

    $mes = (int) $mesSaldoInicial;
    $ano = (int) $anoSaldoInicial;
    $trimestre = (int) $trimestreSaldoInicial;
    //echo "mes------>".$mes;
    //echo "ano------>".$ano;
    $totalRelatorioAtividadeTrimestralColumba = 0;
    $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba = array();
    while ($ano <= $anoAtual) {
        $resultado = $r->listaRelatorioAtividadeTrimestralColumbaAssinado($trimestre, $ano, $idOrganismoAfiliado);
        if (!$resultado && $temSaldoInicial == 1) {
            $resultado2 = $ct->listaColumbaTrimestral($trimestre,$ano,$idOrganismoAfiliado,true);
            if (!$resultado2) {
                $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba[] = $trimestre . "ºtrim./" . $ano;
                $total++;
                $totalRelatorioAtividadeTrimestralColumba++;
            }
        }
        if ($trimestre == 4 && $ano <= $anoAtual) {
            $ano++;
            $trimestre = 0;
        }
        $trimestre++;
        if ($ano == $anoAtual && $trimestre >= $trimestreAtual) {
            $ano++;
        }
    }
}

//Inicializar vari�veis
$arrDash = array();
$arrDash['link'] = "?corpo=buscaRelatoriosNaoEntregues";

//echo "<pre>";print_r($arrNivel);
//include("templates/ibox2.tpl.php");
?>
<script src="/js/bootstrap-tooltip.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("body").tooltip({selector: '[data-toggle=tooltip]'});
    });
</script>


<!--RELATORIOS NAO ENTREGUES-->
<div class="col-lg-4">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Relatórios Não Entregues</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <?php if($naoCobrarDashboard==0){?>
            <table class="table table-hover no-margins">
                <thead>
                    <tr>
                        <th>Relatório mais antigo</th>
                        <th>Mês</th>
                        <th>Ano</th>
                    </tr>
                </thead>
                <tbody>
<?php

//Agenda
if ($cobraAgendaAnual) {
        ?>
                            <tr>
                                <td>Agenda Anual de Atividades para o Portal</td>
                                <td>--</td>
                                <td><?php echo date('Y'); ?></td>
                            </tr>
        <?php
}

//Ata Reunião Mensal
if (count($arrMesesNaoEntreguesAtaReuniaoMensal) > 0) {
    foreach ($arrMesesNaoEntreguesAtaReuniaoMensal as $v) {
        ?>
                            <tr>
                                <td>Ata de Reunião Mensal</td>
                                <td><?php echo $v['mes']; ?></td>
                                <td><?php echo $v['ano']; ?></td>
                            </tr>
        <?php
        break;
    }
}
//Ata posse
if (count($arrAnosNaoEntreguesAtaPosse) > 0) {
    foreach ($arrAnosNaoEntreguesAtaPosse as $v) {
        ?>
                            <tr>
                                <td>Ata de Posse</td>
                                <td>--</td>
                                <td><?php echo $v['ano']; ?></td>
                            </tr>
        <?php
        break;
    }
}
//Membros Ativos
if (count($arrMesesNaoEntreguesRelatorioMembrosAtivos) > 0) {
    foreach ($arrMesesNaoEntreguesRelatorioMembrosAtivos as $v) {
        ?>
                            <tr>
                                <td>Membros Ativos</td>
                                <td><?php echo $v['mes']; ?></td>
                                <td><?php echo $v['ano']; ?></td>
                            </tr>
        <?php
        break;
    }
}
//Relatório Financeiro Mensal
if (count($arrMesesNaoEntreguesRelatorioFinanceiroMensal) > 0) {
    foreach ($arrMesesNaoEntreguesRelatorioFinanceiroMensal as $v) {
        ?>
                            <tr>
                                <td>Relatório Financeiro Mensal</td>
                                <td><?php echo $v['mes']; ?></td>
                                <td><?php echo $v['ano']; ?></td>
                            </tr>
        <?php
        break;
    }
}
                    //Relatório Financeiro Anual
                    if (count($arrAnosNaoEntreguesRelatorioFinanceiroAnual) > 0) {
                        foreach ($arrAnosNaoEntreguesRelatorioFinanceiroAnual as $v) {
                            ?>
                            <tr>
                                <td>Relatório Financeiro Anual</td>
                                <td>--</td>
                                <td><?php echo $v['ano']; ?></td>
                            </tr>
                            <?php
                            break;
                        }
                    }
                    //Imóveis

                    if (count($arrAnosNaoEntreguesRelatorioImovel) > 0) {
                        foreach ($arrAnosNaoEntreguesRelatorioImovel as $v) {
                            ?>
                            <tr>
                                <td>Imóveis</td>
                                <td>--</td>
                                <td><?php echo $v['ano']; ?></td>
                            </tr>
                            <?php
                            break;
                        }
                    }

                    //Relatório de Atividades Mensal
                    if (count($arrMesesNaoEntreguesRelatorioAtividadeMensal) > 0) {
                        foreach ($arrMesesNaoEntreguesRelatorioAtividadeMensal as $v) {
                            ?>
                            <tr>
                                <td>Relatório de Atividades Mensal</td>
                                <td><?php echo $v['mes']; ?></td>
                                <td><?php echo $v['ano']; ?></td>
                            </tr>
                            <?php
                            break;
                        }
                    }
                    ?>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><a href="<?php echo $arrDash['link']; ?>"><button type="button" class="btn btn-primary btn-xs">Ver Mais</button></a></td>
                    </tr>
                </tbody>
            </table>
            <?php }?>
        </div>
    </div>


                    <?php
                    include_once('model/membroOAClass.php');
                    $membroOA = new membroOA();
//$idOrganismoAfiliado 	= $_SESSION['idOrganismoAfiliado'];
                    $total = 0;
                    $resultado = $membroOA->listaMembroOA($idOrganismoAfiliado,0);
                    if ($resultado) {
                        $total = count($resultado);
                    }

//Inicializar vari�veis
                    $arrDash = array();
                    $arrDash['titulo1'] = "Membros no Organismo";
                    $arrDash['total'] = $total;
                    $arrDash['titulo2'] = "membros";

//echo "<pre>";print_r($arrNivel);
//include("templates/ibox3.tpl.php");
                    ?>

    <!--video aulas membros-->
    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo $arrDash['titulo1']; ?></h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><?php echo $arrDash['total']; ?></h1>
                <small><?php echo $arrDash['titulo2']; ?></small>
            </div>
        </div>
    </div>

                    <?php
                    include_once('model/videoAulaVisualizacaoClass.php');
                    $v = new videoAulaVisualizacao();
                    $seqCadast = $_SESSION['seqCadast'];

                    $totalVisualizada = 0;
                    $resultado = $v->contaVisualizacao($seqCadast);
                    if ($resultado) {
                        $totalVisualizada = count($resultado);
                    }

                    include_once('model/videoAulaClass.php');
                    $v2 = new videoAula();
                    $totalVideoAulas = 0;
                    $resultado2 = $v2->listaVideoAula(true);
                    if ($resultado2) {
                        $totalVideoAulas = count($resultado2);
                    }


                    if ($totalVideoAulas > 0) {
                        $diferencaVideoAulas = $totalVideoAulas - $totalVisualizada;
                    }

//Inicializar vari�veis
                    $arrDash = array();


                    $arrDash['titulo1'] = "Vídeo-Aulas";
                    $arrDash['titulo2'] = "Aulas a serem visualizadas";
                    if ($diferencaVideoAulas > 0) {
                        $arrDash['total'] = $diferencaVideoAulas;
                    } else {
                        $arrDash['total'] = 0;
                    }

//echo "<pre>";print_r($arrNivel);
//include("templates/ibox3.tpl.php");
                    ?>

    <!--video aulas membros-->
    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?php echo $arrDash['titulo1']; ?></h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><?php echo $arrDash['total']; ?></h1>
                <small><?php echo $arrDash['titulo2']; ?></small>
            </div>
        </div>
    </div>

    <!-- GRÁFICO FINANCEIRO -->
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Gráfico Financeiro</h5>
                <div class="ibox-tools">
                    <span class="label label-info pull-right">Anual</span>
                </div>
            </div>
            <div class="ibox-content no-padding">
                <div class="flot-chart m-t-lg" style="height: 55px;">
                    <div class="flot-chart-content" id="flot-chart1"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php


include_once('model/muralClass.php');
$mural = new Mural();

$total = $mural->totalPublicacoesMural();

//Inicializar vari�veis
$arrDash = array();
$arrDash['titulo'] = "Mural Digital";
$arrDash['total'] = $total;
$arrDash['link'] = "?corpo=muralDigital";


$resultado = $mural->listaUltimasPublicacoesMural(null,null,null,null,$_SESSION['seqCadast']);
//echo "<pre>";print_r($resultado);
//include("templates/ibox4.tpl.php");
?>

<!--MURAL DIGITAL-->
<div class="col-lg-4">
    <?php
    $funcoesUsuarioString = isset($_SESSION['funcoes']) ? $_SESSION['funcoes'] : null;
    $arrFuncoes = explode(",", $funcoesUsuarioString);

    //echo "Departamento: " . $sessao->getValue("fk_idDepartamento") . "<br>";
    //echo "Funções: <pre>"; print_r($arrFuncoes); echo "</pre>";

    if($sessao->getValue("seqCadast")==431328 //EMANUELLE SPACK - Assessoria de Comunicação -AMORC
        ||in_array("329",$arrFuncoes) //V. Ext. 151
        ||in_array("365",$arrFuncoes) //V. Ext. 201
        ||in_array("367",$arrFuncoes) //V. Ext. 203
        ||in_array("369",$arrFuncoes) //V. Ext. 205
        ||in_array("341",$arrFuncoes) //V. Ext. 163
        ||($sessao->getValue("fk_idDepartamento")==2)
        || ($sessao->getValue("fk_idDepartamento")==3)){
    ?>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><?php echo $arrDash['titulo']; ?></h5>
            <div class="ibox-tools">
                <!--<span class="label label-warning-light pull-right"><?php //echo $arrDash['total']; ?> Novas Postagens</span>-->
            </div>
        </div>
        <div class="ibox-content">
            <div>
                <div class="feed-activity-list">
<?php
foreach ($resultado as $m) {
    $seqCadast = $m['fk_seqCadastAtualizadoPor'];
    $p = new perfilUsuario();
    $resultadoPerfil = $p->listaAvatarUsuario($seqCadast);
    $src = "#";
    foreach ($resultadoPerfil as $avatar) {
        $src = $avatar['avatarUsuario'];
        $nome = $avatar['nomeUsuario'];
    }
    $data = substr($m['dtCadastroMural'], 8, 2) . "/" . substr($m['dtCadastroMural'], 5, 2) . "/" . substr($m['dtCadastroMural'], 0, 4) . " às " . substr($m['dtCadastroMural'], 11, 8);
    $dtCadastroMuralTimeAgo = timeAgo($m["dtCadastroMural"]);
    switch ($m['fk_idMuralCategoria'])
    {
        case 1:
            $categoriaMural = "Atividade";
            break;
        case 2:
            $categoriaMural = "Evento";
            break;
        case 3:
            $categoriaMural = "Notícia";
            break;
    }
    if(!file_exists($src)){
        $src="img/default-user.png";
    }
    ?>
                        <div class="feed-element">
                            <a href="#" class="pull-left">
                                <img alt="image" class="img-circle" src="<?php echo $src; ?>">
                            </a>
                            <div class="media-body ">
                                <small class="pull-right"> <?php echo $dtCadastroMuralTimeAgo; ?></small>
                                <strong><?php echo $nome; ?> </strong> postou um novo(a) <?php echo $categoriaMural;?> <br>
                                <small class="text-muted"><?php echo $data; ?></small>
                            </div>
                        </div>
    <?php
}
?>
                </div>
                <a href="<?php echo $arrDash['link']; ?>"><button class="btn btn-primary btn-block m-t"><i class="fa fa-arrow-down"></i> Ver Mais</button></a>
            </div>
        </div>
    </div>
    <?php }?>
    <!-- GRÁFICO FINANCEIRO -->
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Gráfico de Membros Ativos</h5>
                <div class="ibox-tools">
                    <span class="label label-info pull-right">Anual</span>
                </div>
            </div>
            <div class="ibox-content no-padding">
                <div class="flot-chart m-t-lg" style="height: 55px;">
                    <div class="flot-chart-content" id="flot-chart-dois"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once("model/notificacoesServerUsuarioClass.php");
$nsu = new notificacoesServerUsuario();

$totalMsgNotificacoesServer = $nsu->totalNaoVisualizadas($sessao->getValue("seqCadast"));

if ($totalMsgNotificacoesServer < 0) {
    $totalMsgNotificacoesServer = 0;
}

//Inicializar vari�veis
$arrDash = array();
$arrDash['tooltip'] = "Atualizações do Servidor";
$arrDash['valor'] = $totalMsgNotificacoesServer;
$arrDash['icone'] = "fa fa-tasks";
$arrDash['cor'] = "lazur-bg"; //navy-bg,lazur-bg,yellow-bg
$arrDash['link'] = "?corpo=listaDeAtualizacoesServer";
//echo "<pre>";print_r($arrNivel);
//include("templates/ibox5.tpl.php");
$resultadoUltimasNotificacoes = $nsu->listaUltimasNotificacoesServer($sessao->getValue("seqCadast"));

header('Content-Type: text/html; charset=iso-8859-1');
?>

<!-- ATUALIZAÇÕES DO SOA -->
<div class="col-lg-4">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Atualizações do SOA</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content ibox-heading">
            <h3><i class="fa fa-wrench"></i> <?php echo $totalMsgNotificacoesServer; ?> Nova(s) Atualizações</h3>
            <small><i class="fa fa-tim"></i> Você possui <?php echo $totalMsgNotificacoesServer; ?> nova(s) atualizações não visualizadas</small>
        </div>
        <div class="ibox-content">
            <div class="feed-activity-list">
<?php
foreach ($resultadoUltimasNotificacoes as $v) {
    //Verificar se já foi visualizada
    if (!$nsu->verificaVisualizacao($sessao->getValue("seqCadast"), $v["idNotificacao"])) {
        $dt = timeAgo($v["dataNotificacao"]);
        $data = substr($v['dataNotificacao'], 8, 2) . "/" . substr($v['dataNotificacao'], 5, 2) . "/" . substr($v['dataNotificacao'], 0, 4) . " às " . substr($v['dataNotificacao'], 11, 8);
        ?>
                        <div class="feed-element">
                            <div>
                                <small class="pull-right text-navy"><?php echo $dt; ?></small>
                                <strong><?php echo $v["tituloNotificacao"]; ?></strong>
                                <div><?php echo substr(strip_tags ( $v['descricaoNotificacao']), 0, 200);?>...  <a href="?corpo=detalheAtualizacoesServer&idNotificacao=<?php echo $v['idNotificacao']; ?>">+saiba mais</a></div>
                                <small class="text-muted"><?php echo $data; ?></small>
                            </div>
                        </div>
                        <?php break;}
                    }
                    ?>
            </div>
            <br><?php //echo $v["dataNotificacao"];?>
            <a href="?corpo=listaDeAtualizacoesServer"><button type="button" class="btn btn-primary btn-xs">Ver Mais</button></a>
        </div>
    </div>

    <div class="col-lg-12">
         <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Consulta de Membros</h5>
                    <div class="ibox-tools">
                        <!--
                        <a class="btn btn-xs btn-primary" href="?opcao=oficial_form.php">
                            <i class="fa fa-plus"></i> Novo Membro
                        </a>
                        -->
                    </div>
                </div>
                <div class="ibox-content">
                    <table style="border-spacing: 10px;border-collapse: separate;">
                                    <tr>
                                        <td>
                                            Tipo:
                                        </td>
                                        <td>
                                            <select id="tipo" name="tipo" class="form-control col-sm-3">
                                                <option value="1">R+C ou TOM</option>
                                                <option value="2">OGG</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Código de Afiliação:
                                        </td>
                                        <td>
                                            <input type="text" name="codigoAfiliacao" id="codigoAfiliacao" onkeypress="return SomenteNumero(event)">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            ou Nome:
                                        </td>
                                        <td>
                                            <input type="text" name="nome" id="nome">
                                        </td>
                                    </tr>
                                    <!--
                                    <tr>
                                        <td>
                                            <script src='https://www.google.com/recaptcha/api.js?hl=pt-BR'></script>
                                        </td>
                                        <td>
                                            <div class="g-recaptcha" data-sitekey="6LeythgUAAAAABw97Wfwu7sjKNpbGp-DpOaA5zx2"></div>
                                        </td>
                                    </tr>
                                    -->
                                    <tr>
                                        <td></td>
                                        <td>
                                            <a href="#" class="btn btn-outline btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codigoAfiliacao','nome','codigoAfiliacao','nome','','','myModalPesquisa','informacoesPesquisa','S','consultaMembro');">Pesquisar</a>
                                            &nbsp;
                                            <a href="#" class="btn btn-outline btn-primary" onclick="location.href='?corpo=#'">Atualizar página</a>

                                            <input type="hidden" id="abrirPesquisa" name="abrirPesquisa" value="1">
                                        </td>
                                    </tr>
                                </table>
                </div>
         </div>
    </div>
    <div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" onclick="fechaModalConsultaMembros()"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <i class="fa fa-users modal-icon"></i>
                <h4 class="modal-title">Consulta de Membros</h4>
            </div>
            <div class="modal-body">
                <table cellpadding="10">
                    <tbody>
                        <tr>
                            <td width="35%">Nome:</td>
                            <td><span id="m_nome"></span></td>
                        </tr>
                        <tr>
                            <td>Cód. Afiliação:</td>
                            <td><span id="m_codigoAfiliacao"></span></td>
                        </tr>
                        <!--
                        <tr>
                            <td>Endereço:</td><td><span id="logradouro"></span></td>
                        </tr>
                        <tr>
                            <td>Número:</td><td><span id="numero"></span></td>
                        </tr>
                        -->
                        <tr>
                            <td>Cidade:</td>
                            <td><span id="cidade"></span></td>
                        </tr>
                        <!--
                        <tr>
                            <td>CEP:</td><td><span id="cep"></span></td>
                        </tr>
                        -->
                        <tr>
                            <td>Data de Nascimento:</td>
                            <td><span id="dataNascimento"></span></td>
                        </tr>
                        <!--<tr>
                            <td>Profissão:</td>
                            <td><span id="profissao"></span></td>
                        </tr>
                        <tr>
                            <td>Ocupação:</td>
                            <td><span id="ocupacao"></span></td>
                        </tr>

                        <tr>
                            <td>RG:</td><td><span id="rg"></span></td>
                        </tr>
                        <tr>
                            <td>CPF:</td><td><span id="cpf"></span></td>
                        </tr>
                        <tr>
                            <td>Telefone:</td><td><span id="telefone"></span></td>
                        </tr>
                        <tr>
                            <td>E-mail:</td><td><span id="email"></span></td>
                        </tr>-->

                        <tr>
                            <td>Admissão:</td>
                            <td> <span id="admissao"></span></td>
                        </tr>
                        <tr>
                            <td>Afiliação R+C:</td>
                            <td> <span id="tipoAfiliacao"></span> <span id="dual"></span></td>
                        </tr>
                        <tr>
                            <td>Afiliação TOM:</td>
                            <td> <span id="tom"></span> <span id="tipoAfiliacaoTom"></span> <span id="dualTom"></span></td>
                        </tr>
                        <tr>
                            <td>Afiliado ao Organismo:</td>
                            <td> <span id="organismoAfiliado"></span></td>
                        </tr>
                        <tr id="tr_revistas" style="display:none">
                            <td>Revistas Impressas:</td>
                            <td> <span id="revistas"></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td> <br><span id="botaoDual"></span></td>
                        </tr>
                    </tbody>
                </table>
                <div class="panel-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>R+C<i class=""></i></center></a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <div class="mail-text h-200">
                                            <div class="alert alert-info">
                                                O <a class="alert-link" href="#">tempo de admissão</a> é relevante para liberar iniciação apenas quando trata-se do <a class="alert-link" href="#">companheiro</a> nos casos duais.
                                            </div>
                                            <table cellpading="5">
                                                <tr>
                                                    <td width="33%">Sit. Cadastral:</td>
                                                    <td> <span id="situacaoCadastralRC"></span></td>
                                                </tr>
                                                <tr id="tr_motivoDesligamentoRC" style="display:none">
                                                    <td>Motivo:</td>
                                                    <td> <span id="motivoDesligamentoRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Sit. Remessa:</td>
                                                    <td> <span id="situacaoRemessaRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Data da Quitação:</td>
                                                    <td> <span id="dataQuitacaoRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Grau:</td>
                                                    <td> <span id="grauRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Lote:</td>
                                                    <td> <span id="loteRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Tempo de Admissão (<a href="#" onclick="tabelaTempoCasa();">ver tabela</a>):</td>
                                                    <td> <span id="tempoCasaRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Iniciações disponíveis:</td>
                                                    <td><span id="iniciacoesPendentesRC"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Concluiu e reiniciou os ensinamentos:</td>
                                                    <td> <span id="reiniciouRC"></span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><center>TOM</center></a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table cellpading="5">
                                            <tr>
                                                <td>Sit. Cadastral:</td>
                                                <td> <span id="situacaoCadastralTOM"></span></td>
                                            </tr>
                                            <tr id="tr_motivoDesligamentoTom" style="display:none">
                                                    <td>Motivo:</td>
                                                    <td> <span id="motivoDesligamentoTom"></span></td>
                                                </tr>
                                            <tr>
                                                <td>Sit. Remessa:</td>
                                                <td> <span id="situacaoRemessaTOM"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Data da Quitação:</td>
                                                <td> <span id="dataQuitacaoTOM"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Lote:</td>
                                                <td> <span id="loteTOM"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Concluiu e reiniciou os ensinamentos:</td>
                                                <td> <span id="reiniciouTOM"></span></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><center>OGG</center></a>
                                </h5>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table cellpading="5">
                                            <tr>
                                                <td>Sit. Cadastral:</td>
                                                <td> <span id="situacaoCadastralOGG"></span></td>
                                            </tr>
                                            <tr id="tr_motivoDesligamentoOjp" style="display:none">
                                                    <td>Motivo:</td>
                                                    <td> <span id="motivoDesligamentoOjp"></span></td>
                                                </tr>
                                            <tr>
                                            <tr>
                                                <td>Sit. Remessa:</td>
                                                <td> <span id="situacaoRemessaOGG"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Data da Quitação:</td>
                                                <td> <span id="dataQuitacaoOGG"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Lote:</td>
                                                <td> <span id="loteOGG"></span></td>
                                            </tr>
                                            <tr>
                                                <td>Grau:</td>
                                                <td> <span id="grauOGG"></span></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><center>Iniciações R+C</center></a>
                                </h5>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">

                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="iniciacoes"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><center>Iniciações TOM</center></a>
                                </h5>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">

                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="iniciacoesTOM"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen"><center>Iniciações OGG</center></a>
                                </h5>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">

                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="iniciacoesOGG"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><center>Cargos Atuantes</center></a>
                                </h5>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="cargos"></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><center>Cargos Exercídos</center></a>
                                </h5>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div style="border: #ccc solid 0px">
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <div id="cargosNaoAtuantes"></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="fechaModalConsultaMembros()">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


</div>
