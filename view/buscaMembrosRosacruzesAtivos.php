<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("model/membrosRosacruzesAtivosClass.php");
include_once("lib/functions.php");

require_once 'model/usuarioClass.php';

$r = new MembrosRosacruzesAtivos();
$proximo_id = 1;
$resultado2 = $r->selecionaUltimoId();
if ($resultado2) {
    foreach ($resultado2 as $vetor) {
        $proximo_id = $vetor['Auto_increment'];
    }
}
/* * **
 * Montar navegação por meses
 * *** */
//$mesAtual = isset($_REQUEST['mesAtual'])?$_REQUEST['mesAtual']:date('m');
//$anoAtual = isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');


if(isset($_REQUEST['mesAtual']))
{
	$mesAtual = $_REQUEST['mesAtual'];
}else{

    if(isset($_SESSION['mesAtualMembrosAtivos']))
    {
        $mesAtual = $_SESSION['mesAtualMembrosAtivos'];
    }else{
        $mesAtual = date('m');
        $_SESSION['mesAtualMembrosAtivos'] = $mesAtual;
    }

}
//echo $mesAtual;
$_SESSION['mesAtualMembrosAtivos'] = $mesAtual;

if(isset($_REQUEST['anoAtual']))
{
	$anoAtual = $_REQUEST['anoAtual'];
}else{

	if(isset($_SESSION['anoAtualMembrosAtivos']))
	{
		$anoAtual = $_SESSION['anoAtualMembrosAtivos'];
	}else{
		$anoAtual = date('Y');
		$_SESSION['anoAtualMembrosAtivos'] = $anoAtual;
	}
}
//echo "anoatual=>".$anoAtual;
$_SESSION['anoAtualMembrosAtivos'] = $anoAtual;

/*
 * Montar anterior e próximo
 */

$mesAnterior = date('m', strtotime('-1 months', strtotime($anoAtual . "-" . $mesAtual . "-01")));
$mesProximo = date('m', strtotime('+1 months', strtotime($anoAtual . "-" . $mesAtual . "-01")));

if ($mesAtual == 1) {
    $anoAnterior = $anoAtual - 1;
} else {
    $anoAnterior = $anoAtual;
}
if ($mesAtual == 12) {
    $anoProximo = $anoAtual + 1;
} else {
    $anoProximo = $anoAtual;
}

/*
 * Montar texto para ser exibido
 */
$mesAtualTexto = mesExtensoPortugues($mesAtual);
$mesProximoTexto = mesExtensoPortugues($mesProximo);
$mesAnteriorTexto = mesExtensoPortugues($mesAnterior);

//Verificar se precisa bloquear registro
$bloqueia=false;
//Verificar se houve uma assinatura
include_once('model/financeiroMensalClass.php');
$f = new financeiroMensal();

$mestre = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,365);
$secretario = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,367);
$guardiao = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,369);
$pjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,371);
$sjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,373);
$tjd = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,375);
$ma = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,377);
$vdc = $f->buscarAssinaturasEmFinanceiroMensalPorDocumento($mesAtual,$anoAtual,$idOrganismoAfiliado,533);

//Verificar se houve uma assinatura
if($mestre||$secretario||$guardiao||$pjd||$sjd||$tjd||$ma||$vdc){
    $bloqueia=true;
}

$data_ini  = $anoAtual."-".$mesAtual."-01";
$data_end  = date("Y-m-d");

$meses = date_diffe($data_ini, $data_end);
//$meses=7;

if($meses>2)
{
    $bloqueia=true;
}

$liberaGLP=false;
if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
    $liberaGLP = true;
}
?>
<?php 
$seqCadastSolicitanteToken=0;
$liberarEmMassa=0;
//Se tem cookie que libera token
$liberaTokenUsuarioIdItemFuncionalidade=false;
if (isset($_COOKIE['liberarTokenUsuario'])) {
    //Verificar se token é da ata de reunião mensal

    $u = new Usuario();
    $resultado = $u->retornaTokenIntegridade($_COOKIE['liberarTokenUsuario'],7);
    if($resultado)
    {
        foreach($resultado as $vetor)
        {
            $liberarEmMassa = $vetor['liberarEmMassa'];
            if($vetor['mes']==$mesAtual&&$vetor['ano']==$anoAtual)
            {    
                $liberaTokenUsuarioIdItemFuncionalidade=true;
                //$timelife=$vetor['timelife'];
                //$timelifeExpira = $timelife + (60 * 60 );
                $date = new DateTime($vetor['dataInicial'].' 23:59:59');
                $timelife = $date->getTimestamp();
                $date2 = new DateTime($vetor['dataFinal'].' 23:59:59');
                $timelifeExpira = $date2->getTimestamp();
                $seqCadastSolicitanteToken=$vetor['seqCadast'];
                $mesTexto = mesExtensoPortugues($vetor['mes']);
                $ano = $vetor['ano'];
            }
        }
    }
    if($liberarEmMassa==1)
    {
        $bloqueia=false;
    } 
?>
<?php }

//Libera por login
$u = new Usuario();
$resultado77 = $u->retornaTokenPorLogin($_SESSION['loginUsuario'],date('Y-m-d'),7);
if($resultado77)
{
    foreach($resultado77 as $vetor77)
    {
        $liberarEmMassa = $vetor77['liberarEmMassa'];
        if($vetor77['mes']==$mesAtual&&$vetor77['ano']==$anoAtual)
        {
            $liberaTokenUsuarioIdItemFuncionalidade=true;

            //$timelife=$vetor['timelife'];
            //$timelifeExpira = $timelife + (60 * 60 );
            $date = new DateTime($vetor77['dataInicial'].' 23:59:59');
            $timelife = $date->getTimestamp();
            $date2 = new DateTime($vetor77['dataFinal'].' 23:59:59');
            $timelifeExpira = $date2->getTimestamp();
            $seqCadastSolicitanteToken=$vetor77['seqCadast'];
            $mesTexto = mesExtensoPortugues($vetor77['mes']);
            $ano = $vetor77['ano'];
        }
    }
    if($liberarEmMassa==1)
    {
        $bloqueia=false;
    }
}
//Fim libera por login

//Se for mes atual ou mes anterior liberar entradas

$mesAnteriorDaDataAtual=date('m', strtotime('-1 months', strtotime(date('Y')."-".date('m')."-01")));
$mesProximoDaDataAtual=date('m', strtotime('+1 months', strtotime(date('Y')."-".date('m')."-01")));

if(date('m')==1)
{
    $anoAnteriorDaDataAtual=$anoAtual-1;
}else{
    $anoAnteriorDaDataAtual=$anoAtual;
}
if(date('m')==12)
{
    $anoProximoDaDataAtual=$anoAtual+1;
}else{
    $anoProximoDaDataAtual=$anoAtual;
}

$liberaMesAtualOuAnterior=false;
if(
    ($_REQUEST['mesAtual']==date('m')&&$_REQUEST['anoAtual']==date('Y'))
    ||
    ($_REQUEST['mesAtual']==$mesAnteriorDaDataAtual&&$_REQUEST['anoAtual']==$anoAnteriorDaDataAtual)
)
{
    $liberaMesAtualOuAnterior=true;
}

if ($liberaTokenUsuarioIdItemFuncionalidade != false&&$seqCadastSolicitanteToken==$_SESSION['seqCadast']) {
    if($liberarEmMassa == 0)
    {
?>
<script>
    window.onload = function () {
        swal({
            title: "Aviso!",
            text: "O Token solicitado para a os Membros Ativos do Mês de <?php echo $mesTexto;?>, Ano <?php echo $ano;?> expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
            type: "warning",
            confirmButtonColor: "#1ab394"
        });
    }
</script>
<?php }else{
        ?>
                <script>
                    window.onload = function () {
                        swal({
                            title: "Aviso!",
                            text: "O Token solicitado todos os Relatórios de Membros Ativos expira em <?php echo date("d/m/Y H:i:s",$timelifeExpira);?>",
                            type: "warning",
                            confirmButtonColor: "#1ab394"
                        });
                    }
                </script>
                    <?php    
    }
}
?>

<?php
$zero = isset($_REQUEST['zero']) ? $_REQUEST['zero'] : null;
if ($zero == 1) {
?>
<script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "O número atual de membros ativos não pode ser menor ou igual à zero!",
                type: "error",
                confirmButtonColor: "#1ab394"
            });
        }
</script>
<?php } ?>


<?php $liberarEmMassa = 1;?>

<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Membros Rosacruzes Ativos no Organismo Afiliado (Mantenedores)</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Financeiro</a>
            </li>
            <li class="active">
                <strong>
                    <a>Membros Rosacruzes Ativos no Organismo Afiliado (Mantenedores)</a>
                </strong>
            </li>
        </ol>
    </div>

</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Membros Rosacruzes Ativos no Organismo Afiliado (Mantenedores)</h5>
                </div>

                <div class="ibox-content">
                    <div class="" style="width:100%;">
                        <table width="100%">
                            <td>
                                <?php if ($liberaGLP) { ?>
                                <button class="btn btn-sm btn-danger" type="button" onclick="document.getElementById('idFuncionalidade').value='7';document.getElementById('idItemFuncionalidade').value='0';document.getElementById('idOrganismoAfiliadoLiberarToken').value='<?php echo $idOrganismoAfiliado;?>';" data-target="#mySeeToken" data-toggle="modal" data-placement="left">
                                    <i class="fa fa-key"></i>
                                </button>
                                <?php }?>
                            </td>
                            <td align="right">
                                <div class="form-group" id="data_4">
                                    <table width="100">
                                        <tr>
                                            <td> 
                                                <div class="input-group date">
                                                    <span class="btn btn-w-m btn-primary" style="cursor: pointer; color:#FFF" ;>
                                                        <i class="fa fa-calendar-o" style="color:#fff"></i>

                                                        <input type="hidden" id="calendario" class="form-control" value="<?php echo $mesAtual;?>/<?php echo date('d');?>/<?php echo $anoAtual;?>" />
                                                        <span style="color:#fff">
                                                            <?php echo $mesAtualTexto;?><?php echo $anoAtual;?>
                                                        </span>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                    </div>
                    <input type="hidden" id="paginaAtual" class="form-control" value="<?php echo $corpo;?>" />
                    </td>
                    </table>
                </div>
                    <br />
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <a class="alert-link">Atenção:</a>Instruções para o primeiro preenchimento.
                        <br />
                        Insira no campo
                        <a class="alert-link">Novas Afiliações</a>a quantidade de membros ativos no Organismo Afiliado contabilzado até o último dia de
                        <a class="alert-link">dezembro</a>.
                        <br />
                        <a class="alert-link">Por exemplo:</a>se existem 57 membros ativos, insira este valor no campo
                        <a class="alert-link">Novas Afiliações</a>.
                    </div>
                    <form>
                        <table class="table table-striped table-bordered table-hover" id="editable2">
                            <thead>
                                <tr id="linha0">
                                    <th width="100">Mês Anterior</th>
                                    <th width="100">Novas Afiliações</th>
                                    <th width="100">Desligamentos por Inatividade</th>
                                    <th width="100">Reintegrados</th>
                                    <th width="100">Ações</th>
                                    <th width="200">Nº atual de Membros Ativos</th>
                                </tr>
                            </thead>
                            <tbody id="tabela">
                                <?php
                                $membrosAtivosMesAnterior = $r->retornaMembrosRosacruzesAtivos($mesAnterior, $anoAnterior, $idOrganismoAfiliado);
                                $arr = $r->retornaMembrosRosacruzesAtivos($mesAtual, $anoAtual, $idOrganismoAfiliado);
                                $mesAnteriorMA = $r->retornaMembrosRosacruzesAtivosMesesAnteriores($mesAtual, $anoAtual, $idOrganismoAfiliado);
                                ?>
                                <tr>
                                    <td>
                                        <div id="membrosAtivosMesAnterior">
                                            <?php echo $membrosAtivosMesAnterior['numeroAtualMembrosAtivos']; ?>
                                        </div>
                                    </td>
                                    <td>
                                        <?php if((!$bloqueia)||$liberaGLP||$liberaTokenUsuarioIdItemFuncionalidade==true||$liberarEmMassa==1||$liberaMesAtualOuAnterior){?>
                                        <input type="text" name="novasAfiliacoes" id="novasAfiliacoes" value="<?php echo $arr['novasAfiliacoes']; ?>" />
                                        <?php }else{?>
                                        <input type="text" name="novasAfiliacoes" id="novasAfiliacoes" value="<?php echo $arr['novasAfiliacoes']; ?>"  readonly="readonly" onclick="document.getElementById('idFuncionalidadeTicket').value='7';document.getElementById('mesTicket').value='<?php echo $mesAtual;?>';document.getElementById('anoTicket').value='<?php echo $anoAtual;?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado;?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"];?>';"/>
                                        <a class="btn btn-primary" data-target="#mySeeIntegridade" data-toggle="modal" data-placement="left" onclick="document.getElementById('idFuncionalidadeTicket').value='7';document.getElementById('mesTicket').value='<?php echo $mesAtual;?>';document.getElementById('anoTicket').value='<?php echo $anoAtual;?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado;?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"];?>';"><i class="fa fa-lock"></i></a>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?php if((!$bloqueia)||$liberaGLP||$liberaTokenUsuarioIdItemFuncionalidade==true||$liberarEmMassa==1||$liberaMesAtualOuAnterior){?>
                                        <input type="text" name="desligamentos" id="desligamentos" value="<?php echo $arr['desligamentos']; ?>" />
                                        <?php }else{?>
                                        <input type="text" name="desligamentos" id="desligamentos" value="<?php echo $arr['desligamentos']; ?>" readonly="readonly" onclick="document.getElementById('idFuncionalidadeTicket').value='7';document.getElementById('mesTicket').value='<?php echo $mesAtual;?>';document.getElementById('anoTicket').value='<?php echo $anoAtual;?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado;?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"];?>';"/>
                                        <a class="btn btn-primary" data-target="#mySeeIntegridade" data-toggle="modal" data-placement="left" onclick="document.getElementById('idFuncionalidadeTicket').value='7';document.getElementById('mesTicket').value='<?php echo $mesAtual;?>';document.getElementById('anoTicket').value='<?php echo $anoAtual;?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado;?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"];?>';"><i class="fa fa-lock"></i></a>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <?php if((!$bloqueia)||$liberaGLP||$liberaTokenUsuarioIdItemFuncionalidade==true||$liberarEmMassa==1||$liberaMesAtualOuAnterior){?>
                                        <input type="text" name="reintegrados" id="reintegrados" value="<?php echo $arr['reintegrados']; ?>" />
                                        <?php }else{?>
                                        <input type="text" name="reintegrados" id="reintegrados" value="<?php echo $arr['reintegrados']; ?>" readonly="readonly" />
                                        <a class="btn btn-primary" data-target="#mySeeIntegridade" data-toggle="modal" data-placement="left" onclick="document.getElementById('idFuncionalidadeTicket').value='7';document.getElementById('mesTicket').value='<?php echo $mesAtual;?>';document.getElementById('anoTicket').value='<?php echo $anoAtual;?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado;?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"];?>';"><i class="fa fa-lock"></i></a>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <div style='width:200'>
                                            <?php
                                            if(!$usuarioApenasLeitura) {
                                                if ((in_array("1", $arrNivelUsuario) && !$bloqueia) || $liberaGLP || $liberaTokenUsuarioIdItemFuncionalidade == true||$liberarEmMassa==1||$liberaMesAtualOuAnterior) {
                                                    ?>
                                                    <?php if ($arr['numeroAtualMembrosAtivos'] == 0) { ?>
                                                        <span id="botao">
                                                    <a href="#" class='btn btn-xs btn-primary'
                                                       onclick="cadastrarMembrosRosacruzesAtivos('<?php echo $proximo_id; ?>');">Salvar</a>
                                                </span>
                                                    <?php } else { ?>
                                                        <span id="botao">
                                                    <a href="#" class='btn btn-xs btn-primary'
                                                       onclick="editarMembrosRosacruzesAtivos('<?php echo $arr['idMembrosRosacruzesAtivos']; ?>');">Salvar</a>
                                                </span>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <span id="botao">
                                                    <a href="#" class='btn btn-xs btn-primary'
                                                       data-target="#mySeeIntegridade" data-toggle="modal"
                                                       data-placement="left"
                                                       onclick="document.getElementById('idFuncionalidadeTicket').value='7';document.getElementById('mesTicket').value='<?php echo $mesAtual; ?>';document.getElementById('anoTicket').value='<?php echo $anoAtual; ?>';document.getElementById('idOrganismoAfiliado').value='<?php echo $idOrganismoAfiliado; ?>';document.getElementById('seqCadastTicket').value='<?php echo $_SESSION["seqCadast"]; ?>';"><i
                                                                class="fa fa-lock"></i> Salvar</a>
                                                </span>
                                                <?php }
                                            }else{
                                                echo "<span class=\"btn btn-default btn-rounded\">Apenas Leitura</span>";
                                            }
                                            ?>
                                            
                                        </div>
                                    </td>
                                    <td>
                                        <div id="numeroAtualMembrosAtivos">
                                            <?php if ($arr['numeroAtualMembrosAtivos'] == 0) { ?>
                                                0
                                            <?php
                                                  } else {
                                                      echo ($membrosAtivosMesAnterior['numeroAtualMembrosAtivos'] + $arr['novasAfiliacoes'] + $arr['reintegrados']) - $arr['desligamentos'];
                                                  }
                                            ?>
                                        </div>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                        <input type="hidden" name="dataVerificacao" id="dataVerificacao" value="01/<?php echo $mesAtual; ?>/<?php echo $anoAtual; ?>" />
                        <input type="hidden" name="mesAtual" id="mesAtual" value="<?php echo $mesAtual; ?>" />
                        <input type="hidden" name="anoAtual" id="anoAtual" value="<?php echo $anoAtual; ?>" />
                        <input type="hidden" name="adicionar" id="adicionar" value="0" />
                        <input type="hidden" name="linhaAberta" id="linhaAberta" value="0" />
                        <input type="hidden" name="proximoId" id="proximoId" value="<?php echo $proximo_id; ?>" />
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast']; ?>" />
                        <input type="hidden" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" value="<?php echo $idOrganismoAfiliado; ?>" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->
<script>
document.getElementById('novasAfiliacoes').focus();
</script>
<div class="modal inmodal" id="mySeeIntegridade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-lock modal-icon"></i>
                <h4 class="modal-title">
                    Solicitação de Token
                    <input type="hidden" name="codigoItem" id="codigoItem">
                </h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUploadJaEntregue" style="background-color:white"></div>
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Lembre-se: você tem até 1 mês para enviar todos os Relatórios Assinados para a GLP.
                        <br />
                        Deseja ter acesso a esse item e regularizar as informações? Descreva o motivo e solicite seu Token.
                        <br />
                        Caso seja aprovada sua solicitação, você receberá uma notificação através do SOA e por e-mail.
                        <br />
                        Atenção: A liberação do Token depende diretamente da análise a ser realizada pelos colaboradores da GLP, portanto depende do horário de funcionamento da Grande Loja, por este motivo, pedimos um prazo de até 48h para as liberações.
                        <br />
                        <br />
                        <b>Motivo da liberação do token:</b>
                        <br />
                        <br />
                        <textarea rows="5" cols="50" name="motivoTicket" id="motivoTicket"></textarea>
                        <br />
                        <button type="button" class="btn btn-sm btn-info" data-dismiss="modal" onclick="abrirTicketParaLiberarFuncionalidade();">Abrir Ticket</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="seqCadastTicket" id="seqCadastTicket" value="<?php $_SESSION['seqCadast'];?>" />
                <input type="hidden" name="idFuncionalidadeTicket" id="idFuncionalidadeTicket" />
                <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" />
                <input type="hidden" name="mesTicket" id="mesTicket" />
                <input type="hidden" name="anoTicket" id="anoTicket" />
                <input type="hidden" name="idItemFuncionalidadeTicket" id="idItemFuncionalidadeTicket" value="0"/>
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeToken" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-unlock modal-icon"></i>
                <h4 class="modal-title">Liberar Token para Usuário</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <table>
                            <tr>
                                <td>Organismo:</td>
                                <td><span id="nomeOrganismoToken"><?php echo retornaNomeCompletoOrganismoAfiliado($idOrganismoAfiliado);?></span> </td>
                            </tr>
                            <tr>
                                <td></td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Login no SOA:</td>
                                <td><input type="text" name="usuarioToken" id="usuarioToken" /></td>
                            </tr>
                            <tr>
                                <td><br>Data Inicial:</td>
                                <td><br><input type="text" class="data" name="dataInicial" id="dataInicial" value="<?php echo date('d/m/Y');?>"></td>
                            </tr>
                            <tr>
                                <td><br>Data Final:</td>
                                <td>
                                    <br><input type="text" class="data" name="dataFinal" id="dataFinal">
                                </td>
                            </tr>
                            <tr>
                                <td><br>Mês do Relatório:</td>
                                <td><br><input type="text" name="mesToken" id="mesToken" /></td>
                            </tr>
                            <tr>
                                <td><br>Ano do Relatório:</td>
                                <td><br><input type="text" name="anoToken" id="anoToken" value="<?php echo date('Y');?>" /></td>
                            </tr>
                            <tr>
                                <td width="90px"><br>Liberar em massa:</td>
                                <td>
                                    <br><input type="checkbox" name="liberarEmMassa" id="liberarEmMassa" onclick="zerarMesAno();">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <br>
                                    <input type="hidden" name="idFuncionalidade" id="idFuncionalidade" />
                                    <input type="hidden" name="idOrganismoAfiliadoLiberarToken" id="idOrganismoAfiliadoLiberarToken" />
                                    <input type="hidden" name="idItemFuncionalidade" id="idItemFuncionalidade" value="<?php echo $anoAtual.$mesAtual.$idOrganismoAfiliado;?>"/>
                                    <button class="btn btn-sm btn-danger" type="button" onclick="liberarTokenUsuario();">
                                        <i class="fa fa-key"></i>Liberar
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeIntegridadeTicketAberto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp" id="modalItemJaEntregue">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-hourglass-start modal-icon"></i>
                <h4 class="modal-title">Ticket já está aberto para esse item aguarde uma resposta</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Você já abriu um ticket para esse item, aguarde resposta da GLP. Enquanto a GLP não liberar você não tem acesso a ele. Caso a GLP libere o acesso a esse item, você receberá um e-mail com o token para alteração.
                        <br />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>              
