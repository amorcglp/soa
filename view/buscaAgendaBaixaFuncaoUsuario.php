
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Baixa Automática Agendada com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$funcoesAtualizadas = isset($_REQUEST['funcoesAtualizadas'])?$_REQUEST['funcoesAtualizadas']:null;
$nome = isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
if($funcoesAtualizadas==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Funções do Usuário <?php echo $nome;?> atualizadas com sucesso.",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Agenda de Baixas Automáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="">Administração</a>
            </li>
            <li class="active">
                <a href="">Agenda de Baixas Automáticas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Agenda de Baixas Automáticas</h5>
                    
                    <div class="ibox-tools">
                    <a class="btn btn-xs btn-primary" href="?corpo=cadastroAgendaBaixaFuncaoUsuario" style="color: white">
                            <i class="fa fa-calendar"></i> Agendar Baixa Automática
                    </a>    
                    <!--    
                    <a class="btn btn-xs btn-warning" href="cron/atualiza_funcoes_usuario.php" target="_blank" style="color: white">
                            <i class="fa fa-refresh"></i> Atualizar Funções dos Usuários
                    </a>
                    <a class="btn btn-xs btn-warning" href="cron/baixa_funcoes_usuario.php" target="_blank" style="color: white">
                            <i class="fa fa-refresh"></i> Baixa Automática Funções de Todos os Usuários Atuantes no ORCZ
                    </a>    
                        
                    <?php //if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroUsuario">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    <?php //}?>
                    -->
                    </div>
                    
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                            	<th>Cod.</th>
                                <th><center>Data Inicial</center></th>
                                <th><center>Data Final</center></th>
                                <th><center>Quantidade de Dias para Prorrogar Baixa do Término do Mandato</center></th>
                                <th><center>Autor</center></th>
                                <th><center>Último a atualizar</center></th>
                                <th>Ações</th>
		            </tr>
                        </thead>
                        <tbody>
                            <?php include_once ("lib/webservice/retornaInformacoesMembro.php");?>
                            <?php
                            include_once("model/baixaAutomaticaFuncoesUsuarioClass.php");
                            $b = new baixaAutomaticaFuncoesUsuario();
                            $resultado = $b->selecionaAgenda();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                    	<td>
                                            <?php echo $vetor['idBaixaAutomaticaFuncoesUsuarioAgenda']; ?>
                                        </td>
                                        <td>
                                            <center>
                                                <?php echo substr($vetor['dataAgendadaInicial'],8,2)."/".substr($vetor['dataAgendadaInicial'],5,2)."/".substr($vetor['dataAgendadaInicial'],0,4); ?>
                                            </center>  
                                        </td>
                                        <td>
                                            <center>
                                                <?php echo substr($vetor['dataAgendadaFinal'],8,2)."/".substr($vetor['dataAgendadaFinal'],5,2)."/".substr($vetor['dataAgendadaFinal'],0,4); ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php echo $vetor['quantidadeDiasProrrogacaoBaixa']; ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php if($vetor['usuario']!=0){ echo retornaNomeCompleto($vetor['usuario']);} ?>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php if($vetor['usuarioAlteracao']!=0){ echo retornaNomeCompleto($vetor['usuarioAlteracao']);}else{ echo "--";} ?>
                                            </center>
                                        </td>
                                        <td>
                                            <?php if(in_array("2",$arrNivelUsuario)){?>
                                            <a class="btn btn-sm btn-info" href="?corpo=alteraAgendaBaixaFuncaoUsuario&idagenda=<?php echo $vetor['idBaixaAutomaticaFuncoesUsuarioAgenda'] ?>" data-rel="tooltip" title="">
                                                <i class="icon-edit icon-white"></i>  
                                                Editar                                            
                                            </a>
                                            <?php }?>
                                        </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->



<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

        /* Init DataTables */
        var oTable = $('#editable').dataTable();

        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });


    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row"]);

    }
</script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	