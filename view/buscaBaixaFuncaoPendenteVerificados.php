
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "O registro encontra-se na lista de Não Verificados!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$funcoesAtualizadas = isset($_REQUEST['funcoesAtualizadas'])?$_REQUEST['funcoesAtualizadas']:null;
$nome = isset($_REQUEST['nome'])?$_REQUEST['nome']:null;
if($funcoesAtualizadas==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Funções do Usuário <?php echo $nome;?> atualizadas com sucesso.",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Baixas de Função Pendentes (Verificados)</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="">Administração</a>
            </li>
            <li class="active">
                <a href="">Baixas de Função Pendentes (Verificados)</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Baixas de Funções Pendentes (Verificados)</h5>
                    
                    
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" href="?corpo=buscaBaixaFuncaoPendente" style="color: white">
                                <i class="fa fa-check"></i> Lista de Baixas de Função Pendentes
                        </a>    
                    </div>
                    
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                            	<th>SeqCadast</th>
                                <th>Usuário</th>
                                <th><center>Cód. de Afiliação</center></th>
                                <th><center>Cód. da Função</center></th>
		                <th>Data da Entrada</th>
		                <th>Término do Mandato</th>
                                <th>Mais de Um Cadastro</th>
		                <th>Sigla do OA</th>
                                <th>Ações</th>
		            </tr>
                        </thead>
                        <tbody>
                            <?php include_once ("lib/webservice/retornaInformacoesMembro.php");?>
                            <?php
                            include_once("model/baixaAutomaticaFuncoesUsuarioClass.php");
                            include_once("model/funcaoClass.php");
                            $b = new baixaAutomaticaFuncoesUsuario();
                            $resultado = $b->selecionaNotLog(1);

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                    	<td>
                                            <?php echo $vetor['seqCadast']; ?>
                                        </td>
                                        <td>
                                            <?php echo retornaNomeCompleto($vetor['seqCadast']); ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['codAfiliacaoMembroOficial']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['funcao']-1; ?> 
                                            <?php 
                                    $fu = new funcao();
                                    $resultado2 = $fu->buscarIdVinculoExterno($vetor['funcao']-1);
                                    if($resultado2)
                                    {
                                    	foreach($resultado2 as $vetor2)
                                    	{
											echo " - ".$vetor2['nomeFuncao']."<br>";
                                    	}
                                    }
                                    ?>
                                        </td>
                                        <td>
                                <center>
                                    <?php echo substr($vetor['dataEntrada'],8,2)."/".substr($vetor['dataEntrada'],5,2)."/".substr($vetor['dataEntrada'],0,4); ?>
                                </center>
                                </td>
                                <td>
                                    <?php echo substr($vetor['dataTerminoMandato'],8,2)."/".substr($vetor['dataTerminoMandato'],5,2)."/".substr($vetor['dataTerminoMandato'],0,4); ?>
                                </td>
                                <td>
                                    <?php if($vetor['existemMaisDeUmCadastro']==1){ echo "Sim";}else{ echo "Não";} ?>
                                </td>
                                <td>
                                    <?php echo $vetor['siglaOrganismo']; ?>
                                </td>
                                <td>
                                    <?php if(in_array("3",$arrNivelUsuario)){?>
                                        <span>
                                            <?php if ($vetor['status'] == 1) { ?>
                                                <a class="btn btn-sm btn-danger" onclick="alteraStatusBaixaFuncaoPendenteParaNaoVerificado('<?php echo $vetor['seqCadast'] ?>', '1','<?php echo $vetor['funcao'];?>','<?php echo $vetor['siglaOrganismo'];?>','<?php echo $vetor['dataEntrada'];?>','<?php echo $vetor['dataTerminoMandato'];?>')" data-rel="tooltip" title="Ativar"> 
                                                    Marcar como não verificado                                           
                                                </a>            
                                            <?php }?>
                                        </span>
                                    <?php }?>
                                </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->



<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

        /* Init DataTables */
        var oTable = $('#editable').dataTable();

        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });


    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row"]);

    }
</script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	