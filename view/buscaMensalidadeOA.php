<?php

@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("model/mensalidadeOAClass.php");

$semestre = isset($_REQUEST['semestre']) ? $_REQUEST['semestre'] : 0;
$anoAtual = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual'] : date('Y');
//$idOrganismoAfiliado 	= isset($idOrganismoAfiliado)?$idOrganismoAfiliado:null;

$anoAnterior = $anoAtual - 1;
$anoProximo = $anoAtual + 1;

//Página Inicial
if($semestre==0)
{
    if(date('m')<=12&&date('m')>=7)
    {
        $semestre=2;
    }else{
        $semestre=1;
    }
}


?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<script>

</script>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Mensalidade do Organismo Afiliado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaMensalidadeOA">Mensalidade do Organismo Afiliado</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <table width="100%">
                        <tr>
                            <td>
                                <h5>Lista de Membros Pagantes do Organismo Afiliado</h5>

                            </td>

                            <td align="right">
                                <?php if($anoAnterior>=2016){?>
                                    <a class="btn btn-xs btn-info" href="?corpo=buscaMensalidadeOA&anoAtual=<?php echo $anoAnterior; ?>&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>&semestre=2"> <i class="fa fa-chevron-left"></i> 2º semestre / <?php echo $anoAnterior; ?></a>
                                <?php }?>    
                                <a class="btn btn-xs btn-info"	href="?corpo=buscaMensalidadeOA&anoAtual=<?php echo $anoAtual; ?>&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>&semestre=1"><?php if ($semestre == 1) { ?><b><?php } ?> 1º semestre / <?php echo $anoAtual; ?><?php if ($semestre == 1) { ?></b><?php } ?> </a> 
                                <a class="btn btn-xs btn-info"	href="?corpo=buscaMensalidadeOA&anoAtual=<?php echo $anoAtual; ?>&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>&semestre=2"><?php if ($semestre == 2) { ?><b><?php } ?> 2º semestre / <?php echo $anoAtual; ?><?php if ($semestre == 2) { ?></b><?php } ?> </a>
                                <a class="btn btn-xs btn-info"	href="?corpo=buscaMensalidadeOA&anoAtual=<?php echo $anoProximo; ?>&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>&semestre=1"> 1º semestre / <?php echo $anoProximo; ?>  <i class="fa fa-chevron-right"></i> </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <div class="alert alert-warning alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link">Lembramos que:</a> Não é obrigatório a cobrança das mensalidades retroativas. 
                            Insira somente os meses pagos.
                        </div>
                        <thead>
                            <tr>
                                <th>
                                    Cód. de Afiliação
                                </th>
                                <th>
                                    Nome
                                </th>
                                <th><center><?php if ($semestre == 1) { ?>Jan/<?php echo $anoAtual;
} else { ?>Jul/<?php echo $anoAtual;
} ?></center></th>
                        <th><center><?php if ($semestre == 1) { ?>Fev/<?php echo $anoAtual;
} else { ?>Ago/<?php echo $anoAtual;
} ?></center></th>
                        <th><center><?php if ($semestre == 1) { ?>Mar/<?php echo $anoAtual;
} else { ?>Set/<?php echo $anoAtual;
} ?></center></th>
                        <th><center><?php if ($semestre == 1) { ?>Abr/<?php echo $anoAtual;
} else { ?>Out/<?php echo $anoAtual;
} ?></center></th>
                        <th><center><?php if ($semestre == 1) { ?>Mai/<?php echo $anoAtual;
                        } else { ?>Nov/<?php echo $anoAtual;
                        } ?></center></th>
                        <th><center><?php if ($semestre == 1) { ?>Jun/<?php echo $anoAtual;
                        } else { ?>Dez/<?php echo $anoAtual;
                        } ?></center></th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/membroOAController.php");

                            $m = new membroOAController();
                            $resultado = $m->listaMembroOA($idOrganismoAfiliado,0);

                            if ($resultado) {
                                foreach ($resultado as $vetor) {

                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                                    ?>
                                    <?php
                                    if (strtoupper($vetor["siglaOrganismoAfiliado"]) == strtoupper($sessao->getValue("siglaOA"))) {
                                        $arrDataPagamento = array();
                                        $mensalidade = new mensalidadeOA();
                                        $total = 0;
                                        $resultado2 = $mensalidade->listaMensalidadeOA($vetor['seqCadastMembroOa'], null, $semestre, $anoAtual, $idOrganismoAfiliado);
                                        if ($resultado2) {
                                            $total = count($resultado2);
                                            foreach ($resultado2 as $vetor2) {
                                                if ($vetor2['dataPagamento'] != "0000-00-00") {
                                                    $arrDataPagamento[] = substr($vetor2['dataPagamento'], 8, 2) . "/" . substr($vetor2['dataPagamento'], 5, 2) . "/" . substr($vetor2['dataPagamento'], 0, 4);
                                                } else {
                                                    $arrDataPagamento[] = "";
                                                }
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                [<?php echo $vetor['codigoAfiliacao']; ?>]
                                            </td>
                                            <td>    
            <?php echo $vetor['nomeMembroOa']; ?>
                                            </td>
                                            <td>
                                    <center>
                                        <input type="text" class="mascaraData" onkeypress="return SomenteNumero(event)" maxlenght="10" style="max-width: 92px"
                                               id="<?php echo $vetor['seqCadastMembroOa']; ?>_<?php if ($semestre == 1) { ?>1<?php } else { ?>7<?php } ?>_<?php echo $anoAtual; ?>"
                                               value="<?php if ($total > 0) { echo $arrDataPagamento[0];} ?>">
                                    </center>
                                            </td>
                                    <td>
                                    <center><input type="text" class="mascaraData" onkeypress="return SomenteNumero(event)" maxlenght="10" style="max-width: 92px" id="<?php echo $vetor['seqCadastMembroOa']; ?>_<?php if ($semestre == 1) { ?>2<?php } else { ?>8<?php } ?>_<?php echo $anoAtual; ?>" value="<?php if ($total > 0) {
                echo $arrDataPagamento[1];
            } ?>"></center>
                                    </td>
                                    <td>
                                    <center><input type="text" class="mascaraData" onkeypress="return SomenteNumero(event)" maxlenght="10" style="max-width: 92px" id="<?php echo $vetor['seqCadastMembroOa']; ?>_<?php if ($semestre == 1) { ?>3<?php } else { ?>9<?php } ?>_<?php echo $anoAtual; ?>" value="<?php if ($total > 0) {
                echo $arrDataPagamento[2];
            } ?>"></center>
                                    </td>
                                    <td>
                                    <center><input type="text" class="mascaraData" onkeypress="return SomenteNumero(event)" maxlenght="10" style="max-width: 92px" id="<?php echo $vetor['seqCadastMembroOa']; ?>_<?php if ($semestre == 1) { ?>4<?php } else { ?>10<?php } ?>_<?php echo $anoAtual; ?>" value="<?php if ($total > 0) {
                echo $arrDataPagamento[3];
            } ?>"></center>
                                    </td>
                                    <td>
                                    <center><input type="text" class="mascaraData" onkeypress="return SomenteNumero(event)" maxlenght="10" style="max-width: 92px" id="<?php echo $vetor['seqCadastMembroOa']; ?>_<?php if ($semestre == 1) { ?>5<?php } else { ?>11<?php } ?>_<?php echo $anoAtual; ?>" value="<?php if ($total > 0) {
                            echo $arrDataPagamento[4];
                        } ?>"></center>
                                    </td>
                                    <td>
                                    <center><input type="text" class="mascaraData" onkeypress="return SomenteNumero(event)" maxlenght="10" style="max-width: 92px" id="<?php echo $vetor['seqCadastMembroOa']; ?>_<?php if ($semestre == 1) { ?>6<?php } else { ?>12<?php } ?>_<?php echo $anoAtual; ?>" value="<?php if ($total > 0) {
                            echo $arrDataPagamento[5];
                        } ?>"></center>
                                    </td>
                                    <td>
            <?php if (in_array("1", $arrNivelUsuario)) { ?>
                                        <center>
                                            <button type="button" class="btn btn-sm btn-info" onclick="salvarMensalidadeOA('<?php echo $vetor['seqCadastMembroOa']; ?>', '<?php echo $semestre; ?>', '<?php echo $anoAtual; ?>', '<?php echo $idOrganismoAfiliado; ?>', '<?php echo $_SESSION['seqCadast']; ?>');">
                                                Salvar
                                            </button>
                                        </center>
            <?php } ?>
                                    </td>
                                    </tr>
            <?php
        }
    }
}
?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes da Ata de Posse</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Posse:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="dataPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora de inicio:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaInicioPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Endereço:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="enderecoPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="numeroPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bairro:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="bairroPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cidade:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="cidadePosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mestre Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="mestreRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Secretário Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="secretarioRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Presidente Junta Depositária Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="presidenteJuntaDepositariaRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Detalhes Adicionais:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="detalhesAdicionaisPosse"></span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Upload da Ata Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <a class="alert-link" href="#">Atenção</a> é preciso enviar a ata original para GLP por correio, mesmo fazendo o upload da ata assinada. Caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        <div class="col-sm-10">
                            <input name="anexo"
                                   id="anexo" type="file" />
                        </div>
                        <div id="anexoAlerta" class="col-sm-10"
                             style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
                            máximo de 10MB.</div>

                    </div>
                    <input type="hidden" id="idAtaUpload" name="idAtaUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Upload" onclick="document.ata.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
