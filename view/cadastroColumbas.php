<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Columbas Não Instaladas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaColumbasNaoInstaladas">Columbas Não Instaladas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de Cadastro de Columbas Não Instaladas do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaColumbasNaoInstaladas">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="formulario" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onsubmit="return validaCadastroColumbaNaoInstalada();">
                        <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Código de Afiliação: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="codigoAfiliacao" name="codigoAfiliacao" type="text"  value="" style="max-width: 83px"  required="required" >
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nome da Columba: </label>
                            <div class="col-sm-9">
                                <input class="form-control" id="nomeColumba" name="nomeColumba" type="text" value="" style="max-width: 343px" required="required">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codigoAfiliacao','nomeColumba','codigoAfiliacao','nomeColumba','seqCadastMembro','','myModalPesquisa','informacoesPesquisa','S');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data Prevista para Instalação:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataPrevistaInstalacao" maxlength="10" id="dataPrevistaInstalacao" type="text" class="form-control" style="max-width: 102px"  required="required">
                            </div>
                        </div>
                        <input type="hidden" id="tipo" name="tipo" value="2" >    
                        <input type="hidden" id="principalCompanheiro" name="principalCompanheiro" >
                        <input type="hidden" id="seqCadastMembro" name="seqCadastMembro" >
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-sm btn-success"
									data-toggle="tooltip" data-placement="left" title="Salvar"
									\>
									<i class="fa fa-check fa-white"></i>&nbsp; Salvar
								</button>
								&nbsp; <a href="?corpo=buscaColumbasNaoInstaladas"
									class="btn btn-sm btn-danger" data-toggle="tooltip"
									data-placement="left" title="Cancelar e voltar!"> <i
									class="fa fa-times fa-white"></i>&nbsp; Cancelar </a>
							</div>
						</div>
					</form>
                    
                </div>
            </div>
        </div>
	</div>
</div>


<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeLoadingAjax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
            <div class="cssload-box-loading" style="margin: 30% 50% 50% 50%;">
			</div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	