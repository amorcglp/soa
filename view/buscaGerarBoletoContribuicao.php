<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
      
      /***
       * Habilitar ou Desabilitar a geração de boletos 0 - Desabilita e 1 Habilita
       */
      $habilita = 1;
      ?>
<!--
<br><br>
<div class="alert alert-danger">
                                Item em manutenção.
                            </div>-->
			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Gerar Boleto de Contribuição AMORC</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="painelDeControle.php">Home</a>
                        	</li>
	                        <li>
	                            <a href="?corpo=buscaMembros">Financeiro</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Gerar Boleto de Contribuição AMORC</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
                    <?php if($habilita==0){?>
	            <div class="alert alert-warning">
                                Módulo em manutenção. <a class="alert-link" href="#">Voltará em breve</a>.
                    </div>
                    <?php }else{?>
	            <!-- Tabela Início -->
                    
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Gerar Boleto de Contribuição AMORC</h5>
									<div class="ibox-tools">
										
									</div>
								</div>
								<div class="ibox-content">
                                    <div class="alert alert-danger">
                                        <h3 class="font-bold"><i class="fa fa-info"></i> Aviso</h3><br>
                                        <p>Saudações Rosacruzes!</p>

                                        <p>Para os pagamentos por meio de Boleto Bancário gerados “nesta plataforma”, solicitamos que aguardem o prazo descrito abaixo para a efetuar o pagamento na rede bancária (inclusive, internet banking). Este prazo é necessário para que o número desse novo boleto seja devidamente registrado no sistema do Banco Central.Estamos nos ajustando às normas do FEBRABAN no que se refere ao registro automático destes boletos e em breve este registro será feito automático.</p>

                                        <p>Estimativa de prazo para o boleto ser reconhecido na rede bancária:</p>

                                        <p>Boletos gerados pela internet nos dias de semana até às 17h10: aguardar até 3 horas para efetuar o pagamento.</p>
                                        <p>Boletos gerados após às 17h10: aguardar até a manhã do próximo dia (útil) seguinte para efetuar o pagamento a partir das 8h30.</p>
                                        <p>Boletos gerados após às 17h10 de sexta-feira e finais de semanas ou feriados, somente na manhã do próximo dia útil para efetuar o pagamento a partir das 8h30.</p>
                                        <p>Agradecemos a compreensão de todos.</p>
                                        <p>Qualquer dúvida estamos à disposição.</p>
                                        <br>
                                        <p>Fraternalmente,</p>
                                        <p>Equipe de TI – GLP</p>
                                    </div>
									<table style="border-spacing: 10px;border-collapse: separate;">
                                                                                <tr>
											<td>
												Contribuição:
											</td>
											<td>
                                                <input type="radio" name="contribuicao" id="contribuicao" value="1" checked="checked" onchange="calculaQuantidadeMensalTrimestralAnual(this.value);">Mensalidade
                                                <input type="radio" name="contribuicao" id="contribuicao" value="2" onchange="calculaQuantidadeMensalTrimestralAnual(this.value);">Trimestralidade
                                                <input type="radio" name="contribuicao" id="contribuicao" value="3" onchange="calculaQuantidadeMensalTrimestralAnual(this.value);">Anuidade 
											</td>
										</tr>
                                        <tr>
											<td>
												Quantidade: 
											</td>
											<td>	
                                                <select id="quantidade" name="quantidade">
                                                    <option value="0">Selecione</option>
                                                    <option value="1" selected="selected">1</option>
                                                    <option value="2">2</option>
                                                    
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    
                                                    <option value="9">10</option>
                                                    <option value="9">11</option>
                                                </select>
											</td>
										</tr>
										<tr>
											<td>
												Tipo:
											</td>
											<td>
												<select id="tipo" name="tipo" class="form-control col-sm-3">
													<option value="1">Ordem Rosacruz (R+C)</option>
                                                                                                        <option value="6">Ordem Martinista (TOM)</option>
                                                                                                        <!--
													<option value="2">OGG</option>
													<option value="4">Não Membro</option>
													<option value="5">Cliente</option>
													
													<option value="7">Assinante Rosacruz</option>
													<option value="8">Amigos da AMORC (AMA)</option>
													<option value="9">Assinante AMORC Cultural</option>
													<option value="13">Assinante ORCJ</option>-->
												</select>
											</td>
										</tr>
										<tr>
											<td>
												Código de Afiliação: 
											</td>
											<td>	
												<input type="text" name="codigoAfiliacao" id="codigoAfiliacao" onkeypress="return SomenteNumero(event)">
											</td>
										</tr>	
										<tr>
											<td>
												ou Nome:
											</td>
											<td>
												<input type="text" name="nome" id="nome"><br>
											</td>
										</tr>
                                                                                									
										<tr>
                                            <td></td>
                                            <td>
                                                <a href="#" class="btn btn-outline btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codigoAfiliacao','nome','codigoAfiliacao','nome','','','myModalPesquisa','informacoesPesquisa','S','previaBoletoContribuicao');">Pesquisar</a>
                                                &nbsp;
                                                <a href="#" class="btn btn-outline btn-primary" onclick="limparCamposNomeCodigoAfiliacao()">Limpar campos</a>

                                            </td>
                                        </tr>
									</table>
								</div>
								<br>
							</div>
						</div>
					</div>
				</div>
                          
				<!-- Tabela Fim -->
                    <?php }?>
                               
		<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <i class="fa fa-barcode modal-icon"></i>
                        <h4 class="modal-title">Prévia da Geração</h4>
                    </div>
                    <div class="modal-body">
					<table cellpadding="10">
						<tbody>
							<tr>
								<td width="43%">Nome:</td><td><span id="m_nome"></span></td>
							</tr>
							<tr>
								<td>Cód. Afiliação:</td><td><span id="m_codigoAfiliacao"></span></td>
							</tr> 
							<tr>
								<td>Tipo de Afiliação:</td><td><span id="tipoAfiliacao"></span></td>
							</tr>
							<tr>
								<td>Estudos paralisados:</td><td><span id="situacaoMembro"></span></td>
							</tr>
							<tr>
								<td>Débito em Conta:</td><td><span id="debitoEmConta"></span></td>
							</tr>
                                                        <tr>
								<td>Modalidade de Remessa:</td><td><span id="modalidadeRemessa"></span></td>
							</tr>
							<tr id="tr_valor_taxa_afiliacao">
								<td>Valor da Taxa de Afiliação:</td><td><span id="valorTaxaAfiliacao"></span></td>
							</tr>
                                                        <tr id="tr_total_trimestralidades">
                                                            <td>Quantidade de <span id="contribuicaoTrim"></span>:</td><td><span id="totalTrimestralidades"></span></td>
							</tr>
                                                        <tr id="tr_total_valor">
                                                            <td>Valor da <span id="valorTexto"></span>:</td><td><span id="valor"></span></td>
							</tr>
							<tr id="tr_valor_contribuicao">
								<td>Valor Total:</td><td><span id="valorContribuicao"></span></td>
							</tr>
							<tr id="tr_assinatura" style="display: none">
								<td>Valor da Assinatura:</td><td><span id="valorAssinatura"></span></td>
							</tr>
                                                        <tr id="tr_aviso" style="display: none">
								<td><b>Aviso:</b></td><td><span id="msg"></span></td>
							</tr>
						</tbody>
					</table>			
			</div>
			<input type="hidden" name="nome_h" id="nome_h" value="">
			<input type="hidden" name="endereco" id="endereco" value="">
			<input type="hidden" name="numero" id="numero" value="">
			<input type="hidden" name="cidade" id="cidade" value="">
			<input type="hidden" name="estado" id="estado" value="">
			<input type="hidden" name="cep" id="cep" value="">
			<input type="hidden" name="codigoAfiliacao_h" id="codigoAfiliacao_h" value="0">
			<input type="hidden" name="tipoMembro" id="tipoMembro" value="0">
			<input type="hidden" name="seqCadast" id="seqCadast" value="0">
			<input type="hidden" name="codigoVeiculo" id="codigoVeiculo" value="0">
			<input type="hidden" name="codigoContaContribuicao" id="codigoContaContribuicao" value="0">
			<input type="hidden" name="valorBoleto" id="valorBoleto" value="0">
                        <input type="hidden" name="quantidadeTrim" id="quantidadeTrim" value="0">
			<input type="hidden" name="token" id="token" value="0">
			<input type="hidden" name="usuarioBoleto" id="usuarioBoleto" value="<?php echo $_SESSION['seqCadast'];?>">
                        <!-- TABELA DE CONTRIBUIÇÕES -->
                        <?php 
                        require_once("model/tabelaContribuicaoClass.php");
                        $t = new tabelaContribuicao();
                        $resultado = $t->listaTabelaContribuicao();
                        if($resultado)
                        {
                            foreach($resultado as $vetor)
                            {
                                //Anual
                                echo '<input type="hidden" name="modalidadeAnualIndividualReal'.$vetor['tipo'].'" id="modalidadeAnualIndividualReal'.$vetor['tipo'].'" value="'.$vetor['modalidadeAnualIndividualReal'].'">';
                                echo '<input type="hidden" name="modalidadeAnualDualReal'.$vetor['tipo'].'" id="modalidadeAnualDualReal'.$vetor['tipo'].'" value="'.$vetor['modalidadeAnualDualReal'].'">';
                                //Anual CFD
                                echo '<input type="hidden" name="modalidadeAnualIndividualCFD'.$vetor['tipo'].'" id="modalidadeAnualIndividualCFD'.$vetor['tipo'].'" value="'.$vetor['modalidadeAnualIndividualCFD'].'">';
                                echo '<input type="hidden" name="modalidadeAnualDualCFD'.$vetor['tipo'].'" id="modalidadeAnualDualCFD'.$vetor['tipo'].'" value="'.$vetor['modalidadeAnualDualCFD'].'">';
                                //Mensal
                                echo '<input type="hidden" name="modalidadeMensalIndividual'.$vetor['tipo'].'" id="modalidadeMensalIndividual'.$vetor['tipo'].'" value="'.$vetor['modalidadeMensalIndividual'].'">';
                                //Trimestral
                                echo '<input type="hidden" name="modalidadeTrimestralIndividualReal'.$vetor['tipo'].'" id="modalidadeTrimestralIndividualReal'.$vetor['tipo'].'" value="'.$vetor['modalidadeTrimestralIndividualReal'].'">';
                                
                            }    
                        }    
                        ?>
			<div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="gerarBoletoDual" onclick="gerarBoleto();" style="display: none">Gerar Boleto Dual</button>
				<button type="button" class="btn btn-primary" id="gerarBoleto" onclick="gerarBoleto();">Gerar Boleto</button>
                                <button type="button" class="btn btn-white" data-dismiss="modal" onclick="fecharPrevia();">Fechar</button>
			</div>
		</div>
	</div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>