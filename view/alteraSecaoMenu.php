
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Seções</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaSecaoMenu">Seções</a>
            </li>
            <li class="active">
                <a>Edição de Seção</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição de Seção</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaSecaoMenu">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    include_once("controller/secaoMenuController.php");

                    $sm = new secaoMenuController();
                    $dados = $sm->buscaSecaoMenu($_GET['id']);
                    ?>
                    <form id="cadastro" method="POST" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idSecaoMenu" id="idSecaoMenu" value="<?php echo $_GET['id']; ?>" />
                        <div class="form-group"><label class="col-sm-2 control-label">*Seção</label>
                            <div class="col-sm-10"><input type="text" name="nomeSecaoMenu" id="nomeSecaoMenu" class="form-control" value="<?php echo $dados->getNomeSecaoMenu(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Ícone</label>
                            <div class="col-sm-10"><input type="text" name="iconeSecaoMenu" id="iconeSecaoMenu" class="form-control" value="<?php echo $dados->getIconeSecaoMenu(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Arquivo</label>
                            <div class="col-sm-10"><input type="text" name="arquivoSecaoMenu" id="arquivoSecaoMenu" class="form-control" value="<?php echo $dados->getArquivoSecaoMenu(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Prioridade</label>
                            <div class="col-sm-10"><input type="text" name="prioridadeSecaoMenu" id="prioridadeSecaoMenu" class="form-control" value="<?php echo $dados->getPrioridadeSecaoMenu(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea rows="7" name="descricaoSecaoMenu" id="descricaoSecaoMenu" class="form-control" style="max-width: 500px" required="required"><?php echo $dados->getDescricaoSecaoMenu(); ?></textarea></div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->implode(Registro());
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

