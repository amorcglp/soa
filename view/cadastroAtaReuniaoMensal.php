<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Área de ATAs</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaReuniaoMensal">ATA</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaReuniaoMensal">Organismo Afiliado</a>
            </li>
            <li>
                <a href="?corpo=buscaAtaReuniaoMensal">Reunião Mensal</a>
            </li>
            <li class="active">
                <strong><a>Formulário de Criação</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de criação de ATA de Reunião Mensal</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtaReuniaoMensal">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaAtaReuniaoMensal()" >
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data da Reunião:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAtaReuniaoMensal" onBlur="validaData(this.value, this);" maxlength="10" id="dataAtaReuniaoMensal" type="text" class="form-control" style="max-width: 102px">
                            </div>
                        </div>
                       <div class="form-group">
                            <label class="col-sm-3 control-label">Hora de Início:</label>
                            <div class="col-sm-2 input-group" style="padding: 0px 0px 0px 15px">
                                <div class="input-group clockpicker"  data-autoclose="true">
                                <input type="text" class="form-control" onBlur="validaHora(this);" maxlength="5" id="horaInicialAtaReuniaoMensal" name="horaInicialAtaReuniaoMensal" value="">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            	</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mês de Competência: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="mesCompetencia" name="mesCompetencia" type="text" value="" style="max-width: 43px">
                                (Ex. Referente à Janeiro, então preencha 1)
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano de Competência: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="anoCompetencia" name="anoCompetencia" type="text" value="" style="max-width: 86px">
                                (Ex. Referente à 2015, então preencha 2015)
                            </div>
                        </div>
                        
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Membro presidente:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembroPresidente" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroPresidente" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroPresidente','nomeMembroPresidente','codAfiliacaoMembroPresidente','nomeMembroPresidente','h_seqCadastMembroPresidente','h_nomeMembroPresidente','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Digite o código de afiliação do membro e no outro campo acima o primeiro nome e pressione TAB
                                        <!--<input type="checkbox" value="" name="companheiroMembroPresidente" id="companheiroMembroPresidente"> Companheiro-->
                                   
                            </div>
                        </div>
                        <input type="hidden" name="h_seqCadastMembroPresidente" id="h_seqCadastMembroPresidente">
                        <input type="hidden" name="h_nomeMembroPresidente" id="h_nomeMembroPresidente">
                        <input type="hidden" name="h_seqCadastCompanheiroMembroPresidente" id="h_seqCadastCompanheiroMembroPresidente">
                        <input type="hidden" name="h_nomeCompanheiroMembroPresidente" id="h_nomeCompanheiroMembroPresidente">
                        <!--<div id="resultado"></div>-->
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Oficiais Administrativos:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembroOficial" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroOficial" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroOficial','nomeMembroOficial','codAfiliacaoMembroOficial','nomeMembroOficial','h_seqCadastMembroOficial','h_nomeMembroOficial','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                        <!--<input type="checkbox" class="companheiro" id="companheiroMembroOficial" value=""> Companheiro-->
                                <br>
                                Digite o código de afiliação do membro e no outro campo acima o primeiro nome e pressione TAB, confira o nome antes de clicar em Incluir<br>
                                <input type="hidden" id="h_seqCadastMembroOficial">
                                <input type="hidden" id="h_nomeMembroOficial">
                                <input type="hidden" id="h_seqCadastCompanheiroMembroOficial">
                                <input type="hidden" id="h_nomeCompanheiroMembroOficial">
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiOficiaisAtaReuniaoMensal();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="ata_oa_mensal_oficiais[]" id="ata_oa_mensal_oficiais" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Informe o número total dos demais demais participantes: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="numeroMembrosAtaReuniaoMensal" name="numeroMembrosAtaReuniaoMensal" type="text" value="" style="max-width: 43px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Monitor Regional de O.A presente? </label>
                            <div class="col-sm-9">
                                <input type="checkbox" name="monitorRegionalPresente" id="monitorRegionalPresente">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Leitura do relatório mensal anterior<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="topico_um" name="topico_um"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><center>Leitura da ata anterior</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_dois" name="topico_dois"></textarea>
                                            </div>
                                            <div id="rascunho2" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><center>Assuntos Pendentes</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_quatro" name="topico_quatro"></textarea>
                                            </div>
                                            <div id="rascunho4" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><center>Assuntos novos</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_cinco" name="topico_cinco"></textarea>
                                            </div>
                                            <div id="rascunho5" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><center>Expansão da Ordem</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_seis" name="topico_seis"></textarea>
                                            </div>
                                            <div id="rascunho6" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseEight"><center>Leitura das Comunicações da Suprema Grande Loja, da GLP e Grande Conselheiro(a)</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_oito" name="topico_oito"></textarea>
                                            </div>
                                            <div id="rascunho8" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseNine"><center>Leitura das Comunicações Gerais</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseNine" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_nove" name="topico_nove"></textarea>
                                            </div>
                                            <div id="rascunho9" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><center>Relatório das Comissões</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_tres" name="topico_tres"></textarea>
                                            </div>
                                            <div id="rascunho3" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><center>Palavra livre</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_sete" name="topico_sete"></textarea>
                                            </div>
                                            <div id="rascunho7" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTen"><center>Encerramento</center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseTen" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <textarea class="summernote" id="topico_dez" name="topico_dez"></textarea>
                                            </div>
                                            <div id="rascunho9" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Hora do encerramento:</label>
                            <div class="col-sm-2 input-group date" style="padding: 0px 0px 0px 15px">
                                <div class="input-group clockpicker"  data-autoclose="true">
                                <input type="text" class="form-control" onBlur="validaHora(this);" maxlength="5" id="horaFinalAtaReuniaoMensal" name="horaFinalAtaReuniaoMensal" value="">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            	</div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <?php 
                        	include_once 'model/organismoClass.php';
                            $o = new organismo();
                        	$nomeOrganismo="";
                        	$resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
		                    if($resultado2)
		                    {
			                    	foreach($resultado2 as $vetor2)
			                    	{
			                    		switch($vetor2['classificacaoOrganismoAfiliado']){
											case 1:
												$classificacao =  "Loja";
												break;
											case 2:
												$classificacao =  "Pronaos";
												break;
											case 3:
												$classificacao =  "Capítulo";
												break;
											case 4:
												$classificacao =  "Heptada";
												break;
											case 5:
												$classificacao =  "Atrium";
												break;
										}
										switch($vetor2['tipoOrganismoAfiliado']){
											case 1:
												$tipo = "R+C";
												break;
											case 2:
												$tipo = "TOM";
												break;
										}
											
										$nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];
                                                                                $siglaOrganismo = $vetor2["siglaOrganismoAfiliado"];
		                    	
		                    		}
		                    }
                        ?>
                        <input type="hidden" name="siglaOrganismoAta" id="siglaOrganismoAta" value="<?php echo $siglaOrganismo;?>">
                        <input type="hidden" name="nomeOrganismoAta" id="nomeOrganismoAta" value="<?php echo $nomeOrganismo;?>">
                        <input type="hidden" name="nomeUsuarioAta" id="nomeUsuarioAta" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
                        <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar a Ata" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaAtaReuniaoMensal" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-times-circle modal-icon"></i>
                <h4 class="modal-title">Edição de ATA de Reunião Mensal do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <label>Motivo da edição:</label>
                <div style="border: #ccc solid 1px">
                    <div class="summernote"></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="atualizaMotivoEdicaoATA();">Editar ATA</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>	