<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Tipos de Atividade</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="?corpo=cadastroAtividadeEstatutoTipo">Tipos de Atividade</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listagem de Tipos de Atividades</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeEstatutoTipoOrdem">
                            <i class="fa fa-share fa-white"></i> Ordem de Exibição
                        </a>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeEstatutoTipo">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Atividade</th>
                                <th>Destinando à:</th>
                                <th><center>Frequência Mensal</center></th>
                                <th><center>Disponibilidade</center></th>
                        		<th><center>Status</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/atividadeEstatutoTipoController.php");
                            include_once("controller/usuarioController.php");

                            $aetc = new atividadeEstatutoTipoController();
                            $um     = new Usuario();

                            $resultado = $aetc->listaAtividadeEstatutoTipo();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                            <tr>
                                <td>
                                    <?php echo $vetor['idTipoAtividadeEstatuto'];?>
                                </td>
                								<td>
                									<?php echo $vetor['nomeTipoAtividadeEstatuto'];?>
                								</td>
                								<td>
                                    <?php
                                    switch($vetor['classiOaTipoAtividadeEstatuto']){
                                        case 1: echo "Loja"; break;
                                        case 2: echo "Pronaos"; break;
                                        case 3: echo "Capítulo"; break;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <center>
                                        <?php
                                        echo $vetor['qntTipoAtividadeEstatuto'];
                                        ?>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <?php
                                        echo $vetor['portalTipoAtividadeEstatuto'] == 1 ? "Portal <br>" : "";
                                        echo $vetor['estatutoTipoAtividadeEstatuto'] == 1 ? "Acordo de Afiliação" : "";
                                        if($vetor['estatutoTipoAtividadeEstatuto'] == 1){
                                          if($vetor['ordemTipoAtividadeEstatuto'] == 999){
                                            echo " [--]";
                                          } else {
                                            switch($vetor['classiOaTipoAtividadeEstatuto']){
                                                case 1: echo "[L"; break;
                                                case 2: echo "[P"; break;
                                                case 3: echo "[C"; break;
                                            }
                                            echo $vetor['ordemTipoAtividadeEstatuto']."]";
                                          }
                                        }
                                        ?>
                                    </center>
                                </td>
                                <td>
	                                <center>
	                                    <div id="statusTarget<?php echo $vetor['idTipoAtividadeEstatuto'] ?>">
	                                        <?php
	                                        switch ($vetor['statusTipoAtividadeEstatuto']) {
	                                            case 0:
	                                                echo "<span class='badge badge-primary'>Ativo</span>";
	                                                break;
	                                            case 1:
	                                                echo "<span class='badge badge-danger'>Inativo</span>";
	                                                break;
	                                        }
	                                        ?>
	                                    </div>
	                                </center>
                                </td>
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idTipoAtividadeEstatuto']; ?>">
                                            <i class="fa fa-search-plus fa-white"></i>
                                            Detalhes
                                        </a>
                                        <a class="btn btn-sm btn-info" href="?corpo=alteraAtividadeEstatutoTipo&idTipoAtividadeEstatuto=<?php echo $vetor['idTipoAtividadeEstatuto']; ?>" data-rel="tooltip" title="">
                                            <i class="fa fa-edit fa-white"></i>
                                            Editar
                                        </a>
                                        <span id="status<?php echo $vetor['idTipoAtividadeEstatuto'] ?>">
        									<?php if ($vetor['statusTipoAtividadeEstatuto'] == 1) { ?>
                                                <a class="btn btn-sm btn-success" onclick="mudaStatus('<?php echo $vetor['idTipoAtividadeEstatuto'] ?>', '<?php echo $vetor['statusTipoAtividadeEstatuto'] ?>', 'atividadeEstatutoTipo')" data-rel="tooltip" title="Ativar">
                                                    Ativar
                                                </a>
        									<?php } else { ?>
                                                <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idTipoAtividadeEstatuto'] ?>', '<?php echo $vetor['statusTipoAtividadeEstatuto'] ?>', 'atividadeEstatutoTipo')" data-rel="tooltip" title="Inativar">

                                                    Inativar
                                                </a>
        									<?php } ?>
                                        </span>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->
<?php

$resultadoInfo = $aetc->listaAtividadeEstatutoTipo();

if ($resultadoInfo) {
	foreach ($resultadoInfo as $vetorInfo) {
        ?>
        <!-- Modal de detalhes do usuário -->
        <div class="modal inmodal" id="myModaInfo<?php echo $vetorInfo['idTipoAtividadeEstatuto']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Detalhe do Tipo de Atividade</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Atividade: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php echo $vetorInfo['nomeTipoAtividadeEstatuto']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Descrição: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php echo $vetorInfo['descricaoTipoAtividadeEstatuto']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Atividade Para o Organismo: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            switch($vetorInfo['classiOaTipoAtividadeEstatuto']){
                                                case 1:
                                                    echo "Loja";
                                                    break;
                                                case 2:
                                                    echo "Capítulo";
                                                    break;
                                                case 3:
                                                    echo "Pronaos";
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Disponibilidade: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                                echo $vetorInfo['portalTipoAtividadeEstatuto'] == 1 ? "Portal<br>" : "";
                                                echo $vetorInfo['estatutoTipoAtividadeEstatuto'] == 1 ? "Acordo de Afiliação <br>" : "";
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Quantidade Recomendada: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php echo $vetorInfo['qntTipoAtividadeEstatuto']." Atividades." ;?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Periodicidade: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            if(($vetorInfo['janeiroTipoAtividadeEstatuto']==1) && ($vetorInfo['fevereiroTipoAtividadeEstatuto']==1) &&
                                                ($vetorInfo['marcoTipoAtividadeEstatuto']==1) && ($vetorInfo['abrilTipoAtividadeEstatuto']==1) &&
                                                ($vetorInfo['maioTipoAtividadeEstatuto']==1) && ($vetorInfo['junhoTipoAtividadeEstatuto']==1) &&
                                                ($vetorInfo['julhoTipoAtividadeEstatuto']==1) && ($vetorInfo['agostoTipoAtividadeEstatuto']==1) &&
                                                ($vetorInfo['setembroTipoAtividadeEstatuto']==1) && ($vetorInfo['outubroTipoAtividadeEstatuto']==1) &&
                                                ($vetorInfo['novembroTipoAtividadeEstatuto']==1) && ($vetorInfo['dezembroTipoAtividadeEstatuto']==1)){
                                                echo "O ano inteiro";
                                            } else {
                                                echo $vetorInfo['janeiroTipoAtividadeEstatuto'] == 1 ? "Janeiro<br>" : "";
                                                echo $vetorInfo['fevereiroTipoAtividadeEstatuto'] == 1 ? "Fevereiro<br>" : "";
                                                echo $vetorInfo['marcoTipoAtividadeEstatuto'] == 1 ? "Março<br>" : "";
                                                echo $vetorInfo['abrilTipoAtividadeEstatuto'] == 1 ? "Abril<br>" : "";
                                                echo $vetorInfo['maioTipoAtividadeEstatuto'] == 1 ? "Maio<br>" : "";
                                                echo $vetorInfo['junhoTipoAtividadeEstatuto'] == 1 ? "Junho<br>" : "";
                                                echo $vetorInfo['julhoTipoAtividadeEstatuto'] == 1 ? "Julho<br>" : "";
                                                echo $vetorInfo['agostoTipoAtividadeEstatuto'] == 1 ? "Agosto<br>" : "";
                                                echo $vetorInfo['setembroTipoAtividadeEstatuto'] == 1 ? "Setembro<br>" : "";
                                                echo $vetorInfo['outubroTipoAtividadeEstatuto'] == 1 ? "Outubro<br>" : "";
                                                echo $vetorInfo['novembroTipoAtividadeEstatuto'] == 1 ? "Novembro<br>" : "";
                                                echo $vetorInfo['dezembroTipoAtividadeEstatuto'] == 1 ? "Dezembro<br>" : "";
                                            }?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Cadastrado em: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            echo substr($vetorInfo['dataCadastroTipoAtividadeEstatuto'],8,2)."/".
                                                substr($vetorInfo['dataCadastroTipoAtividadeEstatuto'],5,2)."/".
                                                substr($vetorInfo['dataCadastroTipoAtividadeEstatuto'],0,4)." às ".
                                                substr($vetorInfo['dataCadastroTipoAtividadeEstatuto'],10,6);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php if ($vetorInfo['dataAtualizadoTipoAtividadeEstatuto'] != '0000-00-00 00:00:00') { ?>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Atualizado em: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            echo substr($vetorInfo['dataAtualizadoTipoAtividadeEstatuto'],8,2)."/".
                                                substr($vetorInfo['dataAtualizadoTipoAtividadeEstatuto'],5,2)."/".
                                                substr($vetorInfo['dataAtualizadoTipoAtividadeEstatuto'],0,4)." às ".
                                                substr($vetorInfo['dataAtualizadoTipoAtividadeEstatuto'],10,6);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Atualizado Por: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php

                                            $resultadoAtualizadoPor = $um->buscaUsuario($vetorInfo['fk_seqCadastAtualizadoPor']);
                                            if ($resultadoAtualizadoPor) {
                                                foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                                    echo $vetorAtualizadoPor['nomeUsuario'];
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label" style="text-align: right; padding-top: 5px;">Status: </label>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <?php
                                            switch ($vetorInfo['statusTipoAtividadeEstatuto']) {
                                                case 0:
                                                    echo "<span class='badge badge-primary'>Ativo</span>";
                                                    break;
                                                case 1:
                                                    echo "<span class='badge badge-danger'>Inativo</span>";
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
