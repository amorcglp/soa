<?php
include_once("lib/functions.php");
?>
<div id="wrapper">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox">
                    <div class="ibox-content">
                        <h5 class="m-b-md">Status do ARGENTINA | SOA</h5>
                        <?php serverStatus(); ?>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="ibox">
                    <div class="ibox-content">
                        <h5>Total de Acessos Online</h5>
                        <h2>
                            <?php echo usuarioOnline();?>
                        </h2>
                    </div>
                </div>
            </div>

            <div class="col-lg-5">
                <div class="ibox">
                    <div class="ibox-content">
                        <h5>IP Online</h5>
                        <h2> <?php echo ipLogado(); ?></h2>
                        <div class="text-center">
                            <div id="sparkline5"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>