<?php

@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once('model/relatorioTrimestralColumbaAssinadoClass.php');
include_once('model/usuarioClass.php');
include_once('model/notificacaoGlpClass.php');

$rtca 		= new RelatorioTrimestralColumbaAssinado();
$usu		= new Usuario();
$u 		= new NotificacaoGlp();

if(isset($_REQUEST['mesAtual']))
{
	$mesAtual = $_REQUEST['mesAtual'];
}else{

		if(isset($_SESSION['mesAtualRelatorioTrimestralColumba']))
		{
			$mesAtual = $_SESSION['mesAtualRelatorioTrimestralColumba'];
		}else{
			$mesAtual = date('m');
			$_SESSION['mesAtualRelatorioTrimestralColumba'] = $mesAtual;
		}

}

$trimestreAtual=0;

if(isset($_REQUEST['trimestreAtual']))
{
	$trimestreAtual = $_REQUEST['trimestreAtual'];
}else{

	if(date('m')==1||date('m')==2||date('m')==3)
        {
            $trimestreAtual=1;
        }
        if(date('m')==4||date('m')==5||date('m')==6)
        {
            $trimestreAtual=2;
        }
        if(date('m')==7||date('m')==8||date('m')==9)
        {
            $trimestreAtual=3;
        }
        if(date('m')==10||date('m')==11||date('m')==12)
        {
            $trimestreAtual=4;
        }

}
//echo $mesAtual;
$_SESSION['mesAtualRelatorioTrimestralColumba'] = $mesAtual;

if(isset($_REQUEST['anoAtual']))
{
	$anoAtual = $_REQUEST['anoAtual'];
}else{

	if(isset($_SESSION['anoAtualRelatorioTrimestralColumba']))
	{
		$anoAtual = $_SESSION['anoAtualRelatorioTrimestralColumba'];
	}else{
		$anoAtual = date('Y');
		$_SESSION['anoAtualRelatorioTrimestralColumba'] = $anoAtual;
	}
}
//echo "anoatual=>".$anoAtual;
$_SESSION['anoAtualRelatorioTrimestralColumba'] = $anoAtual;


//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;
$usuario 				= isset($_REQUEST['usuario'])?$_REQUEST['usuario']:null;
$anexo 					= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;
/**
 * Upload do Relatório Assinado
 */
$relatorio=0;
$extensaoNaoValida=0;
if($anexo != "")
{
	//Verificar se já está cadastrado
	$rtca->setFk_idOrganismoAfiliado($idOrganismoAfiliado);
	$rtca->setTrimestre($trimestreAtual);
	$rtca->setAno($anoAtual);
	$rtca->setUsuario($usuario);

	$idRelatorio = $rtca->cadastraRelatorioTrimestralColumbaAssinado();

	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
		$extensao = strstr($_FILES['anexo']['name'], '.');
                if($extensao==".pdf"||
                        $extensao==".png"||
                        $extensao==".jpg"||
                        $extensao==".jpeg"||
                        $extensao==".PDF"||
                        $extensao==".PNG"||
                        $extensao==".JPG"||
                        $extensao==".JPEG"
                        )
                {
                    $caminho = "img/relatorio_trimestral_columba/" . basename($idRelatorio.".relatorio.trimestral.columba.".$trimestreAtual.".".$anoAtual);
                    //Guardar Link do Caminho no Banco de Dados
                    $rtca->setCaminho($caminho);
                    if($rtca->atualizaCaminhoRelatorioTrimestralColumbaAssinado($idRelatorio))
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/relatorio_trimestral_columba/';
                            $uploadfile = $uploaddir . basename($idRelatorio.".relatorio.trimestral.columba.".$trimestreAtual.".".$anoAtual);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            {
                                    $relatorio = 1;
                                    /**
                                     * Notificar GLP que o relatório assinado foi inserido
                                     */
                                    date_default_timezone_set('America/Sao_Paulo');
                                    $tituloNotificacao="Novo Relatorio Trimestral de Columbas Entregue";
                                    $tipoNotificacao=6;//Entregue
                                    $remetenteNotificacao=$sessao->getValue("seqCadast");
                                    $mensagemNotificacao="Relatorio Trimestral de Columbas Entregue pelo Organismo ".$_POST['nomeOrganismo'].", ".date("d/m/Y")." as ".date("H:i:s"). " por ".$_POST['nomeUsuario'];

                                    //Selecionar todos os usuários com departamento 2 e 3
                                    $resultadoUsuarios = $usu->listaUsuario(null,"2,3");
                                    $arrOficiais=array();
                                    if($resultadoUsuarios)
                                    {
                                            foreach ($resultadoUsuarios as $vetor)
                                            {
                                                    $arrOficiais[] = $vetor['idUsuario'];
                                            }
                                    }

                                    $resultado = $u->cadastroNotificacao(utf8_encode($tituloNotificacao),utf8_encode($mensagemNotificacao),$tipoNotificacao,$remetenteNotificacao);

                                    if ($resultado==true) {
                                            for ($i=0; $i<count($arrOficiais); $i++){
                                                    $ultimoId = $u->selecionaUltimoId();
                                                    $resultadoOficiais = $u->cadastroNotificacaoGlp($ultimoId,$arrOficiais[$i]);
                                            }
                                    }
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o relatório assinado!');</script>";
                            }
                    }
                }else{
                    $extensaoNaoValida=1;
                }
            }else{
                $extensaoNaoValida=2;
            }
	}
}

if($relatorio==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Relatório Assinado Enviado com Sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($extensaoNaoValida==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
/*
 * Verificar se já foi feito o upload do relatório assinado
 */
$caminhoRelatorioTrimestralColumba="";
if($rtca->verificaSeJaCadastrado($trimestreAtual,$anoAtual,$idOrganismoAfiliado))
{
	$resultado = $rtca->listaRelatorioTrimestralColumbaAssinado($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
	if($resultado)
	{
		foreach($resultado as $vetor)
		{
			$caminhoRelatorioTrimestralColumba = $vetor['caminho'];
		}
	}
}
?>
<?php
include_once('model/atividadeColumbaClass.php');
include_once('model/columbaRelatorioTrimestralClass.php');
include_once("lib/functions.php");


/****
 * Montar navegação por meses
 *****/

/*
 * Montar anterior e próximo
 */

$mesAnterior=date('m', strtotime('-1 months', strtotime($anoAtual."-".$mesAtual."-01")));
$mesProximo=date('m', strtotime('+1 months', strtotime($anoAtual."-".$mesAtual."-01")));

if($trimestreAtual==1)
{
	$anoAnterior=$anoAtual-1;
}else{
	$anoAnterior=$anoAtual;
}
if($trimestreAtual==4)
{
	$anoProximo=$anoAtual+1;
}else{
	$anoProximo=$anoAtual;
}

/*
 * Montar texto para ser exibido
 */
$mesAtualTexto = mesExtensoPortugues($mesAtual);
$mesProximoTexto = mesExtensoPortugues($mesProximo);
$mesAnteriorTexto = mesExtensoPortugues($mesAnterior);


$c = new ColumbaRelatorioTrimestral();
$ac = new atividadeColumba();

$trimestreAnterior=0;
$trimestreProximo=0;

if($trimestreAtual==1)
{
    $trimestreAnterior=4;
}else{
    $trimestreAnterior = $trimestreAtual-1;
}
if($trimestreAtual==4)
{
    $trimestreProximo=1;
}else{
    $trimestreProximo= $trimestreAtual+1;
}

include_once('model/columbaTrimestralClass.php');
$ct = new columbaTrimestral();
$resultado7 = $ct->listaColumbaTrimestral($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
//echo "<pre>";print_r($resultado7);
$entregue=0;
$entregueCompletamente=0;
$dataEntrega="";
$dataEntregaCompletamente="";
$statusEntrega=1;
if($resultado7)
{
    $entregue=1;
    $statusEntrega=2;
    foreach ($resultado7 as $v)
    {
        $codigoAssinatura = $v['numeroAssinatura'];
        $quemEntregou = $v['nomeUsuario']." [".$v['codigoDeAfiliacao']."]";
        $dataEntrega = substr($v['dataEntrega'],8,2)."/".substr($v['dataEntrega'],5,2)."/".substr($v['dataEntrega'],0,4)." às ".substr($v['dataEntrega'],11,8);
        //echo "dataEntrega:".$dataEntrega;
        if($v['entregueCompletamente']==1)
        {
            $entregueCompletamente=1;
            $statusEntrega=3;
            $dataEntregaCompletamente = substr($v['dataEntregouCompletamente'],8,2)."/".substr($v['dataEntregouCompletamente'],5,2)."/".substr($v['dataEntregouCompletamente'],0,4)." às ".substr($v['dataEntregouCompletamente'],11,8);
        }

    }
}

//Conforme status escolher a cor que o box aparecerá
switch ($statusEntrega)
{
    case 1:
        $class = "warning";
        $icon = "fa-location-arrow";
        $tituloBox = "Assinatura Eletrônica";
        break;
    case 2:
        $class = "default";
        $icon = "fa-pencil";
        $tituloBox = "O Documento não foi entregue completamente. Faltam assinaturas!";
        break;
    case 3:
        $class = "info";
        $icon = "fa-thumbs-up";
        $tituloBox = "O Documento foi 100% entregue";
        break;
}

//Ver se tem upload criado
include_once('model/columbaTrimestralClass.php');
$rct = new RelatorioTrimestralColumbaAssinado();
$resultado = $rct->listaRelatorioTrimestralColumbaAssinado($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
$upload = "não";
if($resultado) {
    if (count($resultado) > 0) {
        $upload = "sim";
    }
}

//Ver data mais antiga da criação desse relatório
include_once('model/atividadeColumbaClass.php');
$ac = new atividadeColumba();
$dataCriacao = $ac->dataCriacao($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
$dataCriacaoDocumento = substr($dataCriacao,8,2)."/".substr($dataCriacao,5,2)."/".substr($dataCriacao,0,4);

//Ver data da ultima alteração
$dataAlteracao = $ac->dataUltimaAlteracao($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
$dataUltimaAlteracaoDocumento = substr($dataAlteracao,8,2)."/".substr($dataAlteracao,5,2)."/".substr($dataAlteracao,0,4);

//Responsáveis pela construção do documento
$arrResp = $ac->responsaveis($trimestreAtual,$anoAtual,$idOrganismoAfiliado);

//Responsáveis pela alteracao do documento
$arrRespAlteracao = $ac->responsaveis($trimestreAtual,$anoAtual,$idOrganismoAfiliado);
foreach ($arrRespAlteracao['seq'] as $k => $v)
{
    if(!in_array($v,$arrRespAlteracao['seq']))
    {
        $arrResp['seq'][$i] = $v;
        $arrResp['nome'][$i] = $arrRespAlteracao['nome'][$k];
        $i++;
    }
}

//Ordernar responsáveis por ordem do inicio de criação
/*
rsort ($arrResp['nome']);
rsort ($arrResp['seq']);
rsort ($arrResp['data']);
*/
$arrResp = unique_multidim_array_vals($arrResp);

sort($arrResp['nome'], SORT_NATURAL | SORT_FLAG_CASE);


//echo "<pre>";print_r($arrResp['nome']);


//Faltam assinar
$ct = new columbaTrimestral();
$mestre = $ct->buscarAssinaturasEmOggMensalPorDocumento($trimestreAtual,$anoAtual,$idOrganismoAfiliado,365);
$secretario = $ct->buscarAssinaturasEmOggMensalPorDocumento($trimestreAtual,$anoAtual,$idOrganismoAfiliado,367);
$pjd = $ct->buscarAssinaturasEmOggMensalPorDocumento($trimestreAtual,$anoAtual,$idOrganismoAfiliado,371);

//Montar Faltam assinar
$arrOficiaisFaltamAssinar=array();
if(!$mestre)
{
    $arrOficiaisFaltamAssinar[]="MESTRE DO ORGANISMO AFILIADO";
}
if(!$secretario)
{
    $arrOficiaisFaltamAssinar[]="SECRETÁRIO DO ORGANISMO AFILIADO";
}

if(!$pjd&&($idClassificacaoOa==3||$idClassificacaoOa==1))
{
    $arrOficiaisFaltamAssinar[]="PRESIDENTE DA JUNTA DEPOSITÁRIA";
}
//$oficiaisFaltamAssinar = implode(", ",$arr);
$arrOficiaisQueAssinaram = $ct->buscarAssinaturasEmOggMensalPorDocumento($trimestreAtual,$anoAtual,$idOrganismoAfiliado);

?>


<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Relatório Trimestral de Columbas</h2>
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a>
			</li>
			<li><a>Columbas</a>
			</li>
			<li class="active"><strong>Relatório Trimestral de Columbas</strong>
			</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Relatório Trimestral de Columbas</h5>
					</div>
					<div class="ibox-content">
						<div class="row"></div>
						<div class="row">
							<div class="col-lg-6">
								<div class="widget lazur-bg p-xl">
									<ul class="list-unstyled m-t-md">
										<h2>
											<i class="fa fa-exclamation-triangle"></i> Avisos
										</h2>
										<br>
										<?php 
                                                                                    $resultado = $c->listaColumba($idOrganismoAfiliado);
                                                                                    if($resultado)
                                                                                    {    
                                                                                        ?>
                                                                                                <li><span class="fa fa-thumbs-up"></span> <label>COLUMBA(S) CADASTRADA(S)</label> ok!</li>
                                                                                        <?php 
                                                                                        }else{
                                                                                        ?>
                                                                                                <li><span class="fa fa-thumbs-down"></span> Não houve nenhuma entrada ou registro de <label>COLUMBAS CADASTRADAS</label>!</li>
                                                                                        <?php 
                                                                                    }
										?>
															
									</ul>

								</div>
							</div>
							<div class="col-lg-6">
								<table width="100%">
									<tr>
										<td align="center">
                                                                                    <?php if(($trimestreAnterior>=1)&&($anoAnterior>=2016)){?>
                                                                                    <a class="btn btn-info "
											href="?corpo=buscaRelatorioTrimestralColumbas&trimestreAtual=<?php echo $trimestreAnterior;?>&anoAtual=<?php echo $anoAnterior;?>"><i
												class="fa fa-calendar-o"></i> <?php echo $trimestreAnterior;?>º trimestre
												<?php echo $anoAnterior;?> </a>
                                                                                    <?php }?>
                                                                                    <a class="btn btn-info "
											href="#"><b><i class="fa fa-calendar-o"></i> <?php echo $trimestreAtual;?>º trimestre
											<?php echo $anoAtual;?> </b> </a> 
                                                                                    <a class="btn btn-info "
											href="?corpo=buscaRelatorioTrimestralColumbas&trimestreAtual=<?php echo $trimestreProximo;?>&anoAtual=<?php echo $anoProximo;?>"><i
												class="fa fa-calendar-o"></i> <?php echo $trimestreProximo;?>º trimestre
												<?php echo $anoProximo;?> </a>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td align="center">
											<a href="#"
											class="btn btn-info  dim btn-large-dim btn-outline"
											onClick="abrirPopupRelatorioTrimestralColumba('<?php echo $trimestreAtual;?>','<?php echo $anoAtual;?>','<?php echo $idOrganismoAfiliado;?>');"><i
											class="fa fa-print" value="Impressão"></i> </a>
                                            <!--
											<?php //if(in_array("4",$arrNivelUsuario)){?>
											&nbsp;	
											<a href="#"
											class="btn btn-info  dim btn-large-dim btn-outline"
											data-target="#mySeeUpload" data-toggle="modal"><i class="fa fa-cloud-upload"></i> </a>
											<?php //}?>
											&nbsp;	-->
											<a href="#" target="_blank"
											class="btn btn-info  dim btn-large-dim btn-outline"
											onClick="listaUploadRelatorioTrimestralColumba('<?php echo $trimestreAtual; ?>','<?php echo $anoAtual; ?>','<?php echo $idOrganismoAfiliado;?>')"
											data-target="#mySeeListaUpload" data-toggle="modal"><i class="fa fa-paste"></i> </a>
									</tr>
								</table>
							</div>
						</div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <i class="fa fa-info-circle"></i> <b>Dados do Relatório</b>
                                    </div>
                                    <div class="panel-body">
                                        <p>
                                        <ul>
                                            <li><b>Arquivos enviados: <?php echo $upload; if($upload=="sim"){ echo " - Não precisa necessariamente de assinatura eletrônica!";}?></b></li>
                                            <li>Data de Início da criação: <?php if($dataCriacaoDocumento!="//"){ $criado=1; echo $dataCriacaoDocumento;}else{ $criado=0; echo " <b>não começou a ser criado</b>";}?></li>
                                            <li>Última alteração: <?php if($dataUltimaAlteracaoDocumento!="//"){ echo $dataUltimaAlteracaoDocumento;}else{ echo "--";}?></li>
                                            <li>Responsável(is) pela construção desse documento:<?php if(count($arrResp['nome'])==0){ echo "--";}?></li>
                                            <ol>
                                                <?php if(count($arrResp['nome'])>0){?>
                                                    <?php foreach ($arrResp['nome'] as $k => $v){?>
                                                        <li><?php echo $v;?></li>
                                                    <?php }?>
                                                <?php }?>
                                            </ol>
                                            <li>Oficiais que precisam assinar esse documento se for entregue eletronicamente:</li>
                                            <ol>
                                                <li>MESTRE DO ORGANISMO AFILIADO</li>
                                                <li>SECRETÁRIO DO ORGANISMO AFILIADO</li>
                                                <?php if($idClassificacaoOa==1||$idClassificacaoOa==3){?><li>PRESIDENTE DA JUNTA DEPOSITÁRIA</li><?php }?>
                                            </ol>

                                        </ul>
                                        </p>
                                    </div>
                                </div>
                                <input type="hidden" name="criado" id="criado" value="<?php echo $criado;?>">
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-<?php echo $class;?>">
                                    <div class="panel-heading">
                                        <i class="fa <?php echo $icon;?>"></i> <b><?php echo $tituloBox;?></b>
                                    </div>
                                    <div class="panel-body">
                                        <?php

                                        ?>
                                        <p>
                                        <ul>
                                            <li>Data da Entrega (Ass. Eletrônica): <?php if($dataEntrega!=""){ echo $dataEntrega;}else{ echo "<span id='dataEntrega'>--</span>";}?></li>
                                            <li>Quem Entregou (Ass. Eletrônica): <?php if($quemEntregou!=""){ echo $quemEntregou;}else{ echo "<span id='quemEntregou'>--</span>";}?></li>
                                            <li>Oficiais que assinaram esse documento: <?php if($entregue==0||$arrOficiaisQueAssinaram==false){?>--<?php }?></li>
                                            <?php if($entregue==1){?>
                                                <ol>
                                                    <?php
                                                    if(count($arrOficiaisQueAssinaram)>0) {
                                                        foreach ($arrOficiaisQueAssinaram as $v) {
                                                            ?>
                                                            <li><?php echo $v['nomeUsuario']; ?> - <?php echo $v['nomeFuncao']; ?></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ol>
                                            <?php }?>
                                            <li>Assinaturas: <?php if($dataEntregaCompletamente!=""){ echo $dataEntregaCompletamente;}else{ echo "--";}?></li>
                                            <li>Faltam assinar: <?php if($entregue==0||count($arrOficiaisFaltamAssinar)==0){ echo "<span id='faltamAssinar'>--</span>";}?></li>
                                            <?php if($entregue==1){?>
                                                <ol>
                                                    <?php
                                                    if(count($arrOficiaisFaltamAssinar)>0) {
                                                        foreach ($arrOficiaisFaltamAssinar as $v) {
                                                            ?>
                                                            <li><?php echo $v; ?></li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ol>
                                            <?php }?>
                                            <li>Código da Assinatura Eletrônica: <?php if($codigoAssinatura!=""){ echo $codigoAssinatura;}else{ echo "<span id='numeroAssinatura'>--</span>";}?></li>
                                        </ul>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="wrapper wrapper-content animated fadeInRight">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <h5>Gráfico Financeiro Mensal</h5>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div id="chart_div"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Relatório Trimestral de Columbas Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="relatorio" name="relatorio" class="form-horizontal" method="post" enctype="multipart/form-data" onSubmit="return validaUpload()">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> é preciso enviar o relatório original para GLP por correio, mesmo fazendo o upload do relatório assinado. Caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"	id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<?php 
                        	include_once 'model/organismoClass.php';
                            $o = new organismo();
                        	$nomeOrganismo="";
                        	$resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
		                    if($resultado2)
		                    {
			                    	foreach($resultado2 as $vetor2)
			                    	{
			                    		switch($vetor2['classificacaoOrganismoAfiliado']){
											case 1:
												$classificacao =  "Loja";
												break;
											case 2:
												$classificacao =  "Pronaos";
												break;
											case 3:
												$classificacao =  "Capítulo";
												break;
											case 4:
												$classificacao =  "Heptada";
												break;
											case 5:
												$classificacao =  "Atrium";
												break;
										}
										switch($vetor2['tipoOrganismoAfiliado']){
											case 1:
												$tipo = "R+C";
												break;
											case 2:
												$tipo = "TOM";
												break;
										}
											
										$nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];
		                    	
		                    		}
		                    }
                        ?>
                    <input type="hidden" name="nomeOrganismo" id="nomeOrganismo" value="<?php echo $nomeOrganismo;?>"> 
                    <input type="hidden" name="nomeUsuario" id="nomeUsuario" value="<?php echo $dadosUsuario->getNomeUsuario();?>">   
                    <input type="hidden" name="trimestreAtual" id="mesAtual" value="<?php echo $trimestreAtual;?>">
                    <input type="hidden" name="anoAtual" id="anoAtual" value="<?php echo $anoAtual;?>">
                    <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                    <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" value="<?php echo $_SESSION['idOrganismoAfiliado'];?>">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.relatorio.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Relatório Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>



