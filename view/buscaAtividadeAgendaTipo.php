<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Tipos de Atividade Agenda</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Tipos de Atividade Agenda</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tipos de Atividades Agenda Cadastrados</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeAgendaTipo">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Atividade</th>
                                <th>Destinando à: </th>
                        		<th><center>Status</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/atividadeAgendaTipoController.php");
                            include_once("controller/usuarioController.php");

                            $aetc = new atividadeAgendaTipoController();
                            $um     = new Usuario();

                            $resultado = $aetc->listaAtividadeAgendaTipo();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
							<tr>
								<td>
									<?php echo validaSegurancaOutput($vetor['nome']);?>
								</td>
								<td>
                                    <?php
                                    switch($vetor['classiOaTipoAtividadeAgenda']){
                                        case 1: echo "Loja"; break;
                                        case 2: echo "Pronaos"; break;
                                        case 3: echo "Capítulo"; break;
                                    }
                                    ?>
                                </td>
                                <td>
	                                <center>
	                                    <div id="statusTarget<?php echo $vetor['idAtividadeAgendaTipo'] ?>">
	                                        <?php
	                                        switch ($vetor['statusTipoAtividadeAgenda']) {
	                                            case 0:
	                                                echo "<span class='badge badge-primary'>Ativo</span>";
	                                                break;
	                                            case 1:
	                                                echo "<span class='badge badge-danger'>Inativo</span>";
	                                                break;
	                                        }
	                                        ?>
	                                    </div>
	                                </center>
                                </td>
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idAtividadeAgendaTipo']; ?>">
                                            <i class="fa fa-search-plus fa-white"></i>  
                                            Detalhes                                            
                                        </a>
                                        <a class="btn btn-sm btn-info" href="?corpo=alteraAtividadeAgendaTipo&idAtividadeAgendaTipo=<?php echo $vetor['idAtividadeAgendaTipo']; ?>" data-rel="tooltip" title="">
                                            <i class="fa fa-edit fa-white"></i>  
                                            Editar                                            
                                        </a>
                                        <span id="status<?php echo $vetor['idAtividadeAgendaTipo'] ?>">
        									<?php if ($vetor['statusTipoAtividadeAgenda'] == 1) { ?>
                                                <a class="btn btn-sm btn-success" onclick="mudaStatus('<?php echo $vetor['idAtividadeAgendaTipo'] ?>', '<?php echo $vetor['statusTipoAtividadeAgenda'] ?>', 'atividadeAgendaTipo')" data-rel="tooltip" title="Ativar">
                                                    Ativar                                            
                                                </a>
        									<?php } else { ?>
                                                <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idAtividadeAgendaTipo'] ?>', '<?php echo $vetor['statusTipoAtividadeAgenda'] ?>', 'atividadeAgendaTipo')" data-rel="tooltip" title="Inativar">
                                                    
                                                    Inativar                                            
                                                </a>
        									<?php } ?>
                                        </span>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->
<?php

$resultadoInfo = $aetc->listaAtividadeAgendaTipo();

if ($resultadoInfo) {
	foreach ($resultadoInfo as $vetorInfo) {
        ?>
        <!-- Modal de detalhes do usuário -->
        <div class="modal inmodal" id="myModaInfo<?php echo $vetorInfo['idAtividadeAgendaTipo']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Detalhe do Tipo de Atividade</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Atividade: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php echo validaSegurancaOutput($vetorInfo['nome']); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Descrição: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php echo $vetorInfo['descricao']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Atividade Para o Organismo: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            switch($vetorInfo['classiOaTipoAtividadeAgenda']){
                                                case 1:
                                                    echo "Loja";
                                                    break;
                                                case 2:
                                                    echo "Capítulo";
                                                    break;
                                                case 3:
                                                    echo "Pronaos";
                                                    break;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>    
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Cadastrado em: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            echo substr($vetorInfo['dataCadastro'],8,2)."/".
                                                substr($vetorInfo['dataCadastro'],5,2)."/".
                                                substr($vetorInfo['dataCadastro'],0,4)." às ".
                                                substr($vetorInfo['dataCadastro'],10,6);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php 
                                if(substr($vetorInfo['dataAtualizado'],8,2)!="00")
                                {
                            ?> 
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Atualizado em: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php
                                            echo substr($vetorInfo['dataAtualizado'],8,2)."/".
                                                substr($vetorInfo['dataAtualizado'],5,2)."/".
                                                substr($vetorInfo['dataAtualizado'],0,4)." às ".
                                                substr($vetorInfo['dataAtualizado'],10,6);
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Atualizado Por: </label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">
                                            <?php

                                            $resultadoAtualizadoPor = $um->buscaUsuario($vetorInfo['usuarioAtualizacao']);
                                            if ($resultadoAtualizadoPor) {
                                                foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
                                                    echo validaSegurancaOutput($vetorAtualizadoPor['nomeUsuario']);
                                                }
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                            <!--
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-offset-1 control-label">Status: </label>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            <?php
                                            /*
                                            switch ($vetorInfo['statusTipoAtividadeAgenda']) {
                                                case 0:
                                                    echo "<span class='badge badge-primary'>Ativo</span>";
                                                    break;
                                                case 1:
                                                    echo "<span class='badge badge-danger'>Inativo</span>";
                                                    break;
                                            }
                                             */
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
				