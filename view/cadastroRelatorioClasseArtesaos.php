<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório da Classe dos Artesãos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaRelatorioClasseArtesaos">Administração</a>
            </li>
            <li class="active">
                <strong><a>Termo de Voluntariado</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de criação do Relatório da Classe dos Artesãos</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaRelatorioClasseArtesaos">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="relatorio" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaAtaReuniaoMensal()" >
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mês de Competência: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="2" onkeypress="return SomenteNumero(event)" id="mesCompetencia" name="mesCompetencia" type="text" value="" style="max-width: 43px">
                                (Ex. Referente à Janeiro, então preencha 1)
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ano de Competência: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="4" onkeypress="return SomenteNumero(event)" id="anoCompetencia" name="anoCompetencia" type="text" value="" style="max-width: 86px">
                                (Ex. Referente à 2015, então preencha 2015)
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Data do Encontro:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataEncontro" maxlength="10" id="dataEncontro" type="text" class="form-control" style="max-width: 102px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Número de Participantes: </label>
                            <div class="col-sm-9">
                                <input class="form-control" maxlength="7" onkeypress="return SomenteNumero(event)" id="numeroParticipantes" name="numeroParticipantes" type="text" value="" style="max-width: 150px" required="required">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Natureza da Mensagem apresentada<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="naturezaMensagemApresentada" name="naturezaMensagemApresentada"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Questões levantadas<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="questoesLevantadas" name="questoesLevantadas"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Clima Geral da Classe<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="climaGeralClasse" name="climaGeralClasse"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Surgiu algum eventual problema? </label>
                            <div class="col-sm-9">
                                <input type="radio" name="surgiuProblema" value="1">Sim
                                <input type="radio" name="surgiuProblema" value="2">Não
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Em caso afirmativo, pedimos a gentileza de descrever o ocorrido<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="surgiuProblemaQual" name="surgiuProblemaQual"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><center>Outras observações que o(a) Mestre de Classe dos Artesãos deseja fazer<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="observacoes" name="observacoes"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Membros participantes na Classe:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembro" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembro" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembro','nomeMembro','codAfiliacaoMembro','nomeMembro','h_seqCadastMembro','h_nomeMembro','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                        <!--<input type="checkbox" class="companheiro" id="companheiroMembroOficial" value=""> Companheiro-->
                                <br>
                                Digite o código de afiliação do membro e pesquise procurando encontrá-lo na base de dados, depois de encontrá-lo clique em "Incluir"<br>
                                <input type="hidden" id="h_seqCadastMembro">
                                <input type="hidden" id="h_nomeMembro">
                                <input type="hidden" id="h_seqCadastCompanheiroMembro">
                                <input type="hidden" id="h_nomeCompanheiroMembro">
                                <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiMembrosRelatorioClasseArtesaos();"> Incluir</a><br>
                                <div style="margin-top: 11px">
                                    <div class="input-group">
                                        <select data-placeholder="Nenhum oficial selecionado..." name="membros[]" id="membros" class="chosen-select_oficial" multiple="multiple" style="width:300px;" tabindex="4">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <?php 
                        	include_once 'model/organismoClass.php';
                            $o = new organismo();
                        	$nomeOrganismo="";
                        	$resultado2  = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
		                    if($resultado2)
		                    {
			                    	foreach($resultado2 as $vetor2)
			                    	{
			                    		switch($vetor2['classificacaoOrganismoAfiliado']){
											case 1:
												$classificacao =  "Loja";
												break;
											case 2:
												$classificacao =  "Pronaos";
												break;
											case 3:
												$classificacao =  "Capítulo";
												break;
											case 4:
												$classificacao =  "Heptada";
												break;
											case 5:
												$classificacao =  "Atrium";
												break;
										}
										switch($vetor2['tipoOrganismoAfiliado']){
											case 1:
												$tipo = "R+C";
												break;
											case 2:
												$tipo = "TOM";
												break;
										}
											
										$nomeOrganismo = $classificacao . " " . $tipo . " " .$vetor2["nomeOrganismoAfiliado"]. " - ".$vetor2["siglaOrganismoAfiliado"];
		                    	
		                    		}
		                    }
                        ?>
                        <input type="hidden" name="nomeOrganismoRelatorio" id="nomeOrganismoRelatorio" value="<?php echo $nomeOrganismo;?>">
                        <input type="hidden" name="nomeUsuarioRelatorio" id="nomeUsuarioRelatorio" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar" OnMouseOver="selecionaTudoMultipleSelect('membros');">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaRelatorioClasseArtesaos" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Edição de ATA de Reunião Mensal do OA -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	