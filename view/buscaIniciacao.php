<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<?php 

$arrOficial = array();
$arrOficial['nome'][0]							= "VILMA PEGORETTI";
$arrOficial['cod_afiliacao'][0]					= "41087";
$arrOficial['perfil'][0]						= "Rosacruz";
$arrOficial['obrigacao'][0]						= "6º Grau do Templo";
$arrOficial['data'][0]							= "17/10/14";
$arrOficial['hora_inicio'][0]					= "15:00";
$arrOficial['oa'][0]							= "PRONAOS R+C JARAGUA DO SUL - AMORC - SC107";
$arrOficial['imprime'][0]						= "Confirmados";
$arrOficial['status'][0]						= "1";

$arrOficial['nome'][1]							= "CLAUDIO FURLAN";
$arrOficial['cod_afiliacao'][1]					= "122227";
$arrOficial['perfil'][1]						= "Rosacruz";
$arrOficial['obrigacao'][1]						= "4º Grau do Templo";
$arrOficial['data'][1]							= "26/02/2015";
$arrOficial['hora_inicio'][1]					= "14:00";
$arrOficial['oa'][1]							= "LOJA R+C SAO PAULO-AMORC - SP101";
$arrOficial['imprime'][1]						= "Agendados";
$arrOficial['status'][1]						= "2";

$arrOficial['nome'][2]							= "JOSE RAMOS DE ASSUMP��O";
$arrOficial['cod_afiliacao'][2]					= "132011";
$arrOficial['perfil'][2]						= "TOM";
$arrOficial['obrigacao'][2]						= "2º Grau do Templo";
$arrOficial['data'][2]							= "17/12/14";
$arrOficial['hora_inicio'][2]					= "11:00";
$arrOficial['oa'][2]							= "LOJA R+C CAMPINAS - AMORC - SP301";
$arrOficial['imprime'][2]						= "Confirmados";
$arrOficial['status'][2]						= "1";

$arrOficial['nome'][3]							= "DAMIANA SANTOS CABRAL";
$arrOficial['cod_afiliacao'][3]					= "144575";
$arrOficial['perfil'][3]						= "Rosacruz";
$arrOficial['obrigacao'][3]						= "9º Grau do Templo";
$arrOficial['data'][3]							= "17/04/15";
$arrOficial['hora_inicio'][3]					= "09:00";
$arrOficial['oa'][3]							= "LOJA R+C BELEM-AMORC - PA101";
$arrOficial['imprime'][3]						= "Agendados";
$arrOficial['status'][3]						= "2";

$arrOficial['nome'][4]							= "ADRIANA WESTPHALEN VESCIA";
$arrOficial['cod_afiliacao'][4]					= "333102";
$arrOficial['perfil'][4]						= "TOM";
$arrOficial['obrigacao'][4]						= "3º Grau do Templo";
$arrOficial['data'][4]							= "05/11/14";
$arrOficial['hora_inicio'][4]					= "07:00";
$arrOficial['oa'][4]							= "LOJA R+C PORTO ALEGRE-AMORC - RS101";
$arrOficial['imprime'][4]						= "Confirmados";
$arrOficial['status'][4]						= "3";

?>
			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Área de Iniciações</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>
	                        <li>
	                            <a href="index.html">Iniciações</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Iniciações Agendadas</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
	            
				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Iniciações Agendadas</h5>
									<div class="ibox-tools">
										<a class="btn btn-xs btn-primary" href="">
											<i class="fa fa-plus"></i> Agendar Nova
										</a>
									</div>
								</div>
								<div class="ibox-content">
									<table class="table table-striped table-bordered table-hover dataTables-example" >
										<thead>
											<tr>
												<th>Obrigação</th>
												<th><center>Data/Hora de início</center></th>
												<th><center>Tipo</center></th>
												<th><center>Organismo Afiliado</center></th>
												<th><center>Status</center></th>
												<th><center>Ações</center></th>
											</tr>
										</thead>
										<tbody>
										 <tr>
											<?php if(count($arrOficial)>0)
									  		{ 
										  		for($i=0;$i<count($arrOficial['perfil']);$i++){
										  	?>
										 <tr>
											<td>
												<?php echo $arrOficial['obrigacao'][$i];?>
											</td>
											<td>
												<center>
													<?php echo $arrOficial['data'][$i];?><br>
													às <?php echo $arrOficial['hora_inicio'][$i];?> horas
													<?php if($arrOficial['status'][$i]==2){?>
														<div class="espacoAcoes">&nbsp;</div>
														<button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#mySeeReagendamento" data-toggle="popover" data-placement="right" title="" data-content="Ao reagendar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
						                                    <i class="fa fa-calendar fa-white"></i>&nbsp;
						                                    Reagendar
						                                </button>
													<?php }else{?>
													<?php }?>
												</center>
											</td>
											<td>
												<center>
												    <?php echo $arrOficial['perfil'][$i];?>
												</center>
											</td>
											<td>
												<center>
													<?php echo utf8_encode($arrOficial['oa'][$i]);?>
													<?php if($arrOficial['status'][$i]!=3){?>
														<hr/>
														<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#mySeeIncluiOficial" data-toggle="tooltip" data-placement="left" title="Inclua oficiais na Iniciação Ritualística">
						                                    <i class="fa fa-plus fa-white"></i>&nbsp;
						                                    Incluir Oficial
						                                </button>
													<?php }else{?>
													<?php }?>
												</center>
											</td>
											<td>
												<center>
													<div>
													<?php if($arrOficial['status'][$i]==1){?>
													<span class="label label-success">Já Aconteceu</span>
													<?php }else{?>
														<?php if($arrOficial['status'][$i]==2){?>
															<span class="label label-warning">Agendado</span>
														<?php }else{?>
															<span class="label label-danger">Cancelado</span>
														<?php }?>
													<?php }?>
													</div>
												</center>
											</td>
											<td>
												<center>
													<?php if($arrOficial['status'][$i]==1){?>
														<div class="btn-group">
								                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"><i class="fa fa-print fa-white"></i>&nbsp;Imprimir <span class="caret"></span></button>
								                            <ul class="dropdown-menu">
								                                <li><a href="#" onclick="abrirPopUpObrigacaoRitualisticaParticipante()"><?php echo $arrOficial['imprime'][$i];?></a></li>
								                                <li class="divider"></li>
								                                <li><a href="#" onclick="abrirPopUpImprimirTestemunhaObrigacaoRitualistica();">Testemunhas</a></li>
								                                <li><a href="#" onclick="abrirPopUpImprimirOficialObrigacaoRitualistica();">Oficiais</a></li>
								                            </ul>
								                        </div>
								                        &nbsp;
														<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
						                                    <i class="fa fa-search-plus fa-white"></i>&nbsp;
						                                    Detalhes
						                                </button>
						                                <br>
														<button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="left" title="Ao editar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
						                                    <i class="fa fa-edit fa-white"></i>&nbsp;
						                                    Editar
						                                </button>
													<?php }else{?>
														<?php if($arrOficial['status'][$i]==2){?>
															<div class="btn-group">
									                            <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"><i class="fa fa-print fa-white"></i>&nbsp;Imprimir <span class="caret"></span></button>
									                            <ul class="dropdown-menu">
									                                <li><a href="#" onclick="abrirPopUpObrigacaoRitualisticaParticipante()"><?php echo $arrOficial['imprime'][$i];?></a></li>
									                                <li class="divider"></li>
									                                <li><a href="#" onclick="abrirPopUpImprimirTestemunhaObrigacaoRitualistica();">Testemunhas</a></li>
									                                <li><a href="#" onclick="abrirPopUpImprimirOficialObrigacaoRitualistica();">Oficiais</a></li>
									                            </ul>
									                        </div>
									                        &nbsp;
															<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
							                                    <i class="fa fa-search-plus fa-white"></i>&nbsp;
							                                    Detalhes
							                                </button>
							                                <br>
															<button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="left" title="Ao editar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
							                                    <i class="fa fa-edit fa-white"></i>&nbsp;
							                                    Editar
							                                </button>
							                                &nbsp;
															<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#mySeeMotivoCancelamento" data-toggle="tooltip" data-placement="left" title="Ao cancelar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
							                                    <i class="fa fa-times fa-white"></i>&nbsp;
							                                    Cancelar
							                                </button>
														<?php }else{?>
															<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
							                                    <i class="fa fa-search-plus fa-white"></i>&nbsp;
							                                    Detalhes
							                                </button>
														<?php }?>
													<?php }?>
												</center>
											</td>
										</tr>
										<?php }
									  	}
									  	?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->
				
				<!-- Window MODAL Início -->
				
				<!-- Cancelamento de Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeMotivoCancelamento" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-times-circle modal-icon"></i>
								<h4 class="modal-title">Cancelamento de Iniciação Ritualística</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<div class="alert alert-danger">
	                                <a class="alert-link" href="#">Atenção! </a>Você têm certeza que deseja cancelar este agendamento? Ou talvez reagendar? Se for este o caso feche a janela atual e clique na opção <b>Reagendar</b> que se encontra abaixo da <b>Data/Hora de início</b> na lista.
	                            </div>
								<!--<div class="form-group">-->
									<label>Motivo do Cancelamento:</label>
									<div style="border: #ccc solid 1px">
										<div class="summernote"></div>
									</div>
								<!--</div>-->
							</div>
							<div class="modal-footer">
								<input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary" onclick="atualizaMotivoCancelamento();">Cancelar Ritual</button>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Reagendamento de Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeReagendamento" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-calendar modal-icon"></i>
								<h4 class="modal-title">Reagendamento de Iniciação Ritualística</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-lg-5 col-lg-offset-1">
										<label>Data/Hora de Início Atual:</label>
		                                <div class="input-group date col-lg-12">
		                                    <span class="input-group-addon">
		                                    	<i class="fa fa-calendar"></i>
		                                    </span>
		                                    <div class="form-control-static">&nbsp;&nbsp;&nbsp;03/04/2014</div>
		                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;às&nbsp;
		                                    <div class="form-control-static">&nbsp;&nbsp;&nbsp;11:00 horas.</div>
		                                </div>
									</div>
									<div class="col-lg-5 col-lg-offset-1">
										<label>Data/Hora de Início Nova:</label>
		                                <div class="input-group date col-lg-9">
		                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                                    <input type="text" class="form-control" value="03/04/2014">
		                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;às&nbsp;
		                                    <input type="text" class="form-control" value="">
		                                </div>
									</div>
								</div>
								<br>
                                <label>Motivo do Reagendamento:</label>
									<div style="border: #ccc solid 1px">
										<div class="summernote"></div>
									</div>
							</div>
							<div class="modal-footer">
								<input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal"  onclick="atualizaMotivoReagendamento();">Reagendar</button>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Detalhes de Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-search-plus modal-icon"></i>
								<h4 class="modal-title">Detalhes da Iniciação Ritualística</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<form action="" class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-3 control-label">Nível:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">3º Grau do Templo.</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Data:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">17/10/14</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Tipo:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">Rosacruz</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Organismo Afiliado:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">LOJA R+C PORTO ALEGRE-AMORC - RS101</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Status:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static"><span class="label label-success">Já Aconteceu</span></div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Observações:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum ut ante eget accumsan. Vivamus viverra pretium quam, nec semper.</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Motivo do Cancelamento:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static"><b>(01/10/2014) - </b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum ut ante eget accumsan. Vivamus viverra pretium quam, nec semper.</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Motivo do 1º Reagendamento:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static"><b>(01/10/2014) - </b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum ut ante eget accumsan. Vivamus viverra pretium quam, nec semper.</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Motivo do 2º Reagendamento:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static"><b>(01/10/2014) - </b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec condimentum ut ante eget accumsan. Vivamus viverra pretium quam, nec semper.</div>
	                                    </div>
	                               	</div>
								</form>
							</div>
							<div class="modal-footer">
								<input type="hidden" name="idinscricao" id="idinscricao">
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary" onclick="window.print();">Imprimir</button>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Inclusão de Oficial na Iniciação Ritualística -->
				<div class="modal inmodal" id="mySeeIncluiOficial" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content animated bounceInRight">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
								<i class="fa fa-plus modal-icon"></i>
								<h4 class="modal-title">Inclusão de Oficial na Iniciação Ritualística</h4>
								<!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
							</div>
							<div class="modal-body">
								<form action="" class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-3 control-label">Mestre:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="mestre_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Columba:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
											</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Mestre Auxiliar:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="mestre_2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Matre:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Capelão:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Grande Sacerdotisa:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Arquivista:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Arauto:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Port. do Archote:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Guia:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Medalhista:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Guardião Externo:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Guardião Interno:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
	                               	<div class="form-group">
										<label class="col-sm-3 control-label">Sonoplasta:</label>
	                                    <div class="col-sm-9">
	                                    	<div class="form-control-static">
												<input class="input focused" id="columba_1" type="text" value="" style="max-width: 52px">
												<input class="input" id="mestre_2" type="text" value="" style="min-width: 320px">
												<br>
												<input type="checkbox" id="inlineCheckbox2" value="option1"> Companheiro
											</div>
	                                    </div>
	                               	</div>
								</form>
							</div>
							<div class="modal-footer">
								<input type="hidden" name="idinscricao" id="idinscricao">
								<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
								<button type="button" class="btn btn-primary"">Incluir</button>
							</div>
						</div>
					</div>
				</div>
				<!-- Window MODAL Fim -->
				
				<!-- Conteúdo DE INCLUDE FIM -->
				
						<?php if(count($arrOficial)>0){ ?>
					<?php for($i=0;$i<count($arrOficial['nome']);$i++){ ?>
						<div class="modal hide fade" id="mySee<?php echo $i;?>" style="margin-left:-35%;min-width:70%">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">X</button>
								<h3><?php echo $arrOficial['imprime'][$i];?> - <?php echo utf8_encode($arrOficial['obrigacao'][$i]);?> (<?php echo $arrOficial['data'][$i];?>)</h3>
							</div>
							<div class="modal-body">
								<table cellpadding="10">
								<tbody>
								<tr>
									<table class="table">
										  <thead>
											  <tr>
												  <th>Cód. Afiliação</th>
												  <th>Nome</th>
												  <th class="oculta">Perfil</th>
												  <th class="oculta">Telefone</th>
												  <th class="oculta"><i class="icon-envelope"></i> E-Mail</th>                                     
											  </tr>
										  </thead>   
										  <tbody>
											<tr>
												<td>41087</td>
												<td>VILMA PEGORETTI</td>
												<td>Titular</td>   
												<td>(41)9999-9999</td> 
												<td>vilma@amorc.org.br</td>                                  
											</tr>
											<tr>
												<td>122227</td>
												<td>CLAUDIO FURLAN</td>
												<td>Companheiro</td>
												<td>(41)8888-8888</td> 
												<td>claudio@amorc.org.br</td>                                  
											</tr>
											<tr>
												<td>132011</td>
												<td>JOSE RAMOS DE ASSUMPÇÃO</td>
												<td>Titular</td>
												<td>(41)7777-7777</td> 
												<td>jose@amorc.org.br</td>                                       
											</tr>
											<tr>
												<td>144575</td>
												<td>DAMIANA SANTOS CABRAL</td>
												<td>Companheiro</td>
												<td>(41)6666-6666</td> 
												<td>damiana@amorc.org.br</td>                                      
											</tr>
											<tr>
												<td>333102</td>
												<td>ADRIANA WESTPHALEN VESCIA</td>
												<td>Companheiro</td>
												<td>(41)5555-5555</td> 
												<td>adriana@amorc.org.br</td>                                      
											</tr>                                   
										  </tbody>
									 </table>
								</tr>
								</tbody>	
								</table>
							</div>
							<input type="hidden" name="idinscricao" id="idinscricao">
							<div class="modal-footer">
								<a href="#" id="imprimir" class="btn" data-dismiss="modal" onclick="abrirPopUpObrigacaoRitualisticaParticipante()">Imprimir</a>
								<a href="#" class="btn" data-dismiss="modal">Fechar</a>
							</div>
						</div>
					<?php } ?>
				<?php } ?>