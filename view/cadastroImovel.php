<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Imóveis</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaImovel">Imóveis</a>
            </li>
            <li class="active">
                <a>Cadastro de Novo Imóvel</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Novo Imóvel</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaImovel">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="acoes/acaoCadastrar.php" onsubmit="return validaFormImoveis();">
                        <div class="form-group form-inline"><label class="col-sm-2 control-label">Organismo Afiliado</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-5 chosen-select" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" data-placeholder="Selecione um organismo..." style="max-width: 350px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();

                                    if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){
                                        $oc->criarComboBox();
                                    } else {
                                        $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                    }
                                    ?><!-- -->
                                	</select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Endereço</label>
                            <div class="col-sm-10"><input type="text" name="enderecoImovel" id="enderecoImovel" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Nº</label>
                            <div class="col-sm-10"><input type="text" name="numeroImovel" id="numeroImovel" class="form-control" style="max-width: 160px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Complemento</label>
                            <div class="col-sm-10"><input type="text" name="complementoImovel" id="complementoImovel" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label"><?php echo $GLOBALS['i18n']['bairro'] ?></label>
                            <div class="col-sm-10"><input type="text" name="bairroImovel" id="bairroImovel" class="form-control" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">País: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="paisImovel" id="paisImovel" style="max-width: 250px" onchange="getEstados(this.value, '#estadoImovel');">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'model/imovelClass.php';
                                    $im = new imovel();
                                    $resultado = $im->listaPais();

                                    if ($resultado) {
                                        foreach ($resultado as $vetor) {

                                            echo "<option value='".$vetor['id']."'>".$vetor['nome']."</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="estadoImvl">
                            <label class="col-sm-2 control-label">Estado: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="estadoImovel" id="estadoImovel" style="max-width: 250px" onchange="getCidades(this.value, '#cidadeImovel');">
                                    <option value="0" selected="selected">Selecione o país.</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="cidadeImvl">
                            <label class="col-sm-2 control-label">Cidade: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="cidadeImovel" id="cidadeImovel" style="max-width: 250px">
                                    <option value="0" selected="selected">Selecione o estado.</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">CEP</label>
                            <div class="col-sm-10">
                                <input type="text" name="cepImovel" id="cepImovel" class="form-control" style="max-width: 320px">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group"><label class="col-sm-2 control-label">Área Total: </label>
                            <div class="col-sm-10"><input type="text" name="areaTotalImovel" id="areaTotalImovel" class="form-control col-sm-2" style="max-width: 120px" required="required"><div class="col-sm-1" style="margin-top: 7px">m²</div></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Área Contruída: </label>
                            <div class="col-sm-10"><input type="text" name="areaConstruidaImovel" id="areaConstruidaImovel" class="form-control col-sm-2" style="max-width: 120px" required="required"><div class="col-sm-1" style="margin-top: 7px">m²</div></div>
                        </div>
                        <hr>
                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Propriedade: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="propriedadeTipoImovel" id="propriedadeTipoImovel" style="max-width: 250px" required="required">
                                    <option value="">Selecione...</option>
                                    <option value="0">Prédio</option>
                                    <option value="1">Sobrado</option>
                                    <option value="2">Casa</option>
                                    <option value="3">Barracão</option>
                                    <option value="4">Outros</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group form-inline"><label class="col-sm-2 control-label">Status da Propriedade: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="propriedadeStatusImovel" id="propriedadeStatusImovel" class="chosen-select" style="max-width: 250px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="1">Própria</option>
                                    <option value="2">Alugada</option>
                                    <option value="3">Cedida</option>
                                    <option value="4">Comodato</option>
                                </select>
                            </div>
                        </div>


                        <!-- Opções de Tipo de Imóvel: (Próprio) -->
                        <div id="proprioImovel">
                            <hr>
                            <div class="form-group" id="datapicker_imovel">
                                <label class="col-sm-2 control-label">Data de Fundação</label>
                                <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="dataFundacaoImovel" onkeypress="valida_data(this,'dataFundacaoImovel')" maxlength="10" id="dataFundacaoImovel" type="text" class="form-control" style="max-width: 105px">
                                    <div hidden id="data_invalida_dataFundacaoImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Propriedade em nome: </label>
                                <div class="col-sm-10">
                                    <select class="form-control col-sm-3" name="propriedadeEmNomeImovel" id="propriedadeEmNomeImovel" style="max-width: 250px">
                                        <option value="0">Selecione...</option>
                                        <option value="1">GLB</option>
                                        <option value="2">GLP</option>
                                        <option value="3">O.A.</option>
                                        <option value="4">Outros</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group"><label class="col-sm-2 control-label">Possui Escritura: </label>
                                <div class="col-sm-10">
                                    <select class="form-control col-sm-3" name="propriedadeEscrituraImovel" id="propriedadeEscrituraImovel" style="max-width: 250px">
                                        <option>Selecione...</option>
                                        <option value="0">Sim</option>
                                        <option value="1">Não</option>
                                    </select>
                                </div>
                            </div>
                            <div id="escrituraImovel">
                                <div class="form-group" id="tipoSelect"><label class="col-sm-2 control-label">Tipo de Escritura: </label>
                                    <div class="col-sm-10">
                                        <select class="form-control col-sm-3" name="fk_idTipoEscrituraImovel" id="fk_idTipoEscrituraImovel" style="max-width: 450px">
                                            <?php

                                            require_once ("model/imovelClass.php");

                                            $im = new imovel();
                                            $resultado = $im->listaTipoEscrituraImovel();
                                            if ($resultado) {
                                                foreach ($resultado as $vetor) {
                                            ?>
                                                    <option value="<?php echo $vetor['idTipoEscrituraImovel'] ?>"><?php echo $vetor['tipoEscrituraImovel'] ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <a data-toggle="modal" data-target="#modalTipoDeEscritura" style="margin-left: 10px"><span class="badge badge-primary" style="margin-top: 7px"><i class="fa fa-plus"></i> Cadastrar</span></a>
                                    </div>
                                </div>
                                <div class="form-group" id="datapicker_escritura">
                                    <label class="col-sm-2 control-label">Data da Escritura</label>
                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input onkeypress="valida_data(this,'propriedadeEscrituraDataImovel')" maxlength="10" name="propriedadeEscrituraDataImovel" id="propriedadeEscrituraDataImovel" type="text" class="form-control" style="max-width: 105px">
                                        <div hidden id="data_invalida_propriedadeEscrituraDataImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group"><label class="col-sm-2 control-label">Possui Registro do Imóvel: </label>
                                <div class="col-sm-10">
                                    <select class="form-control col-sm-3" name="registroImovel" id="registroImovel" style="max-width: 250px">
                                        <option>Selecione...</option>
                                        <option value="0">Sim</option>
                                        <option value="1">Não</option>
                                    </select>
                                </div>
                            </div>
                            <div id="registro">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nome do Registro de Imóveis</label>
                                    <div class="col-sm-10">
                                        <input type="text" maxlength="150" name="registroNomeImovel" id="registroNomeImovel" class="form-control" style="max-width: 320px">
                                    </div>
                                </div>
                                <div class="form-group" id="datapicker_registro_imovel">
                                    <label class="col-sm-2 control-label">Data do Registro de Imóvel</label>
                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input onkeypress="valida_data(this,'registroImovelDataImovel')" maxlength="10" name="registroImovelDataImovel" id="registroImovelDataImovel" type="text" class="form-control" style="max-width: 105px">
                                        <div hidden id="data_invalida_registroImovelDataImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                    </div>
                                </div>
                                <!--
                                <div class="form-group" id="registroImovelNumero">
                                    <label class="col-sm-2 control-label">Nº da Matrícula</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="registroImovelNumeroImovel" id="registroImovelNumeroImovel" class="form-control" style="max-width: 320px">
                                        
                                        <p>
                                            <input type="text" 
                                            name="matriculas" 
                                            id="matricula02" 
                                            class="form-control col-sm-2" 
                                            style="max-width: 320px">
                                            <a onClick="" class="btn btn-xs btn-success" style="color: white; margin-left: 5px; margin-top: 7px">
                                                <i class="fa fa-save fa-white"></i>
                                            </a>
                                            <a onClick="" class="btn btn-xs btn-danger" style="color: white; margin-left: 15px; margin-top: 7px">
                                                <i class="fa fa-trash-o fa-white"></i>
                                            </a>
                                        </p>
                                        
                                    </div>
                                </div>
                                <hr>
                                -->
                                <div class="form-group" id="registroImovelNumero">
                                    <label class="col-sm-2 control-label">
                                        Matrículas: 
                                        <a onClick="addInputMatriculaCadastrar();" class="btn btn-xs btn-primary" style="color: white; margin-left: 5px">
                                            <i class="fa fa-plus fa-white"></i>
                                        </a>
                                    </label>
                                    <input type="hidden" name="nroCampo" id="nroCampo" value="1">
                                    <div class="col-sm-10" id="campoMatriculas">
                                        <p id="paragrafo1">
                                            <input type="text" 
                                            name="matricula1" 
                                            id="matricula1" 
                                            class="form-control col-sm-2" 
                                            maxlength="14"
                                            style="max-width: 320px">
                                            <a onClick="limparImputMatriculaCadastrar();" 
                                               class="btn btn-xs btn-danger" 
                                               style="color: white; margin-left: 15px; margin-top: 7px">
                                                <i class="fa fa-eraser fa-white"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Valor Venal: </label>
                                <div class="col-sm-10">
                                    <div class="input-group" style="max-width: 200px">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control currency" maxlength="15" name="valorVenalImovel" id="valorVenalImovel">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"> Valor Atual Aproximado: </label>
                                <div class="col-sm-10">
                                    <div class="input-group" style="max-width: 200px">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control currency" maxlength="15" name="valorAproximadoImovel" id="valorAproximadoImovel">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Imóvel Avaliado? </label>
                                <div class="col-sm-10">
                                    <select class="form-control col-sm-3" name="avaliacaoImovel" id="avaliacaoImovel" style="max-width: 250px">
                                        <option>Selecione...</option>
                                        <option value="0">Sim</option>
                                        <option value="1">Não</option>
                                    </select>
                                </div>
                            </div>
                            <div id="avaliacaoValor"></div>
                            <div id="avaliacao">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Valor Avaliado: </label>
                                    <div class="col-sm-10">
                                        <div class="input-group" style="max-width: 200px">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control currency" maxlength="15" name="avaliacaoValorImovel" id="avaliacaoValorImovel">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nome do Avaliador</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avaliacaoNomeImovel" id="avaliacaoNomeImovel" class="form-control" style="max-width: 320px">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Creci do Avaliador</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="avaliacaoCreciImovel" id="avaliacaoCreciImovel" class="form-control" style="max-width: 320px">
                                    </div>
                                </div>
                                <div class="form-group" id="datapicker_escritura">
                                    <label class="col-sm-2 control-label">Data da Avaliação:</label>
                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input onkeypress="valida_data(this,'avaliacaoDataImovel')" maxlength="10" name="avaliacaoDataImovel" id="avaliacaoDataImovel" type="text" class="form-control" style="max-width: 105px">
                                        <div hidden id="data_invalida_avaliacaoDataImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Opções de Tipo de Imóvel: (Alugado/ Cedido/ Comodato) -->
                        <div id="alugadoImovel">
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Locador/Cessionário: </label>
                                <div class="col-sm-10">
                                    <input type="text" name="locadorImovel" id="locadorImovel" class="form-control" style="max-width: 320px">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Locatário/Cedente: </label>
                                <div class="col-sm-10">
                                    <input type="text" name="locatarioImovel" id="locatarioImovel" class="form-control" style="max-width: 320px">
                                </div>
                            </div>
                            <div class="form-group" id="datapicker_contrato_inicio">
                                <label class="col-sm-2 control-label">Início do Contrato/Acordo: </label>
                                <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input onkeypress="valida_data(this,'aluguelContratoInicioImovel')" maxlength="10" name="aluguelContratoInicioImovel" id="aluguelContratoInicioImovel" type="text" class="form-control" style="max-width: 105px">
                                    <div hidden id="data_invalida_aluguelContratoInicioImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                </div>
                            </div>
                            <div class="form-group" id="datapicker_contrato_fim">
                                <label class="col-sm-2 control-label">Término do Contrato/Acordo: </label>
                                <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input onkeypress="valida_data(this,'aluguelContratoFimImovel')" maxlength="10" name="aluguelContratoFimImovel" id="aluguelContratoFimImovel" type="text" class="form-control" style="max-width: 105px">
                                    <div hidden id="data_invalida_aluguelContratoFimImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Valor: </label>
                                <div class="col-sm-10">
                                    <div class="input-group" style="max-width: 200px">
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" class="form-control currency" maxlength="15" name="aluguelValorImovel" id="aluguelValorImovel">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Anotações Complementares: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; width: 500px">
                                    <div class="mail-text h-200">
                                        <textarea class="summernote" id="descricaoImovel" name="descricaoImovel"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" onclick="startProgress();" />
                                <a class="btn btn-white" href="?corpo=buscaImovel" id="cancelar" name="cancelar">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo de Modal Início -->

<!-- Modal Cadastrar Tipo de Escritura Início -->
<div class="modal inmodal fade" id="modalTipoDeEscritura" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                <h4 class="modal-title">Cadastrar Novo Tipo de Escritura</h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label class="col-sm-2 control-label">Tipo de Escritura: </label>
                        <div class="col-sm-10">
                            <input type="text" name="tipoEscritura" id="tipoEscritura" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12" id="alertTipoDeEscritura">
                        <div class="alert alert-info" style="text-align: justify">
                            <center>
                            Contribua com um novo <a class="alert-link">Tipo de Escritura</a>.
                            O qual será visível por todos!
                            </center>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal" onclick="trocaAlertTiposDeEscritura();">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="cadastraTipoDeEscritura();">Salvar nova opção</button>
            </div>
        </div>
    </div>
</div>

<!-- OnLoad Progress -->
<div class="modal inmodal" id="mySeeLoading" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="cssload-box-loading" style="margin: 30% 50% 50% 50%;">
        </div>
    </div>
</div>

<!-- Conteúdo de Modal Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>		