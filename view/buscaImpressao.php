<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório de Impressão</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
 
<?php
@include_once("controller/impressaoController.php");
@include_once("controller/documentoController.php");
@include_once("controller/impressaoDiscursoController.php");

$impressao = new impressaoController();
$documento = new documentoController();
$impressaoDiscurso = new ImpressaoDiscursoController();

$siglaOA = isset($_SESSION['siglaOA'])?$_SESSION['siglaOA']:null;

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Impressão de Documentos da AMORC-GLP</h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th><center>Cód. </center></th>
                                <th><center>Organismo </center></th>
                                <th><center>Código de Afiliação</center></th>
                                <th><center>Nome</center></th>
                                <th><center>Data e Hora</center></th>
                                <th><center>Arquivo Impresso</center></th>
                                <th><center>Motivo</center></th> <!-- Válido apenas para Discurso -->
                                 <th><center>Páginas Impressas</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(!($resultado = $impressao->listaImpressao($siglaOA))) {
                                    echo "<script type=\"text/javascript\">";
                                    echo "alert(\"Não há registro de impressão!\");";
                                    echo "</script>";
                                }
                                else {
                                    $contador = 0;
                                    foreach ($resultado as $vetor) {
                                        $contador++;
                                        if(($resultado2 = $documento->buscarDadosId($vetor['fk_idDocumento']))) {
                                            foreach ($resultado2 as $vetor2) {
                                             //   $doc = $vetor2['arquivo'];
                                                $descricao= ($vetor2['descricao']==null) ? $vetor2['arquivo'] : $vetor2['descricao'];
                                                $pagina= ($vetor2['paginaImpressao']==null) ? "Todas" : $vetor2['paginaImpressao'];
                                            }
                                        }
                                        else {
                                          //  $doc = "Nenhum";
                                            $pagina = $descricao = "Nenhum" ;
                                        }
                                        
                                        if(($resultado3= $impressaoDiscurso->buscarIdImpressao($vetor['idImpressao']))) {
                                            foreach ($resultado3 as $vetor3) {
                                                $motivo = ($vetor3['motivoDaImpressao']==""?"Não especificado":$vetor3['motivoDaImpressao']);
                                            }
                                        }
                                        else {
                                            $motivo = "Nenhum";
                                        }
                                        
                                        $dataOriginal = explode(' ', $vetor['dataHora']);
                                        if(count($dataOriginal)>0) {
                                            $partes = explode('-', $dataOriginal[0]);
                                            $dataHora = $partes[2] . '/' . $partes[1] . '/' . $partes[0] . ' ' . $dataOriginal[1];
                                        }
                                        else {
                                            $dataHora = $vetor['dataHora'];
                                        }
                                        ?>
                            <tr>
                                <td><center><?php echo $contador; ?></center></td>
                                <td><center><?php echo retornaNomeCompletoOrganismoAfiliado($vetor['idOrganismoAfiliado']); ?></center></td>
                                <td><b><center><?php echo $vetor['codigoDeAfiliacao']; ?></center></b></td>
                                <td><center><?php echo $vetor['nomeUsuario']; ?></center></td>
                                <td><center><?php echo $dataHora; ?></center></td>
                                <td><center><?php echo $descricao; ?></center></td>
                                <td><center><?php echo $motivo; ?></center></td>
                                <td><center><?php echo $pagina; ?></center></td>
                            </tr>
                            <?php
                                    }
                                }
                            ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
</body>
</html>