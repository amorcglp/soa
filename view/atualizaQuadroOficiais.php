
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Oficiais</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Cadastro de Oficiais</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Oficiais</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaOficial">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="ataReuniaoMensal" class="form-horizontal" method="post">
                        <div class="alert alert-danger">
                            Tenha bastante <a class="alert-link" href="#">atenção</a> ao cadastrar as informações abaixo. Caso você precise editá-las, ou caso o oficial saía antes do término do mandato, entre em contato com o setor de Organismo Afiliado +55 (41) 3351-3030 ou pelo e-mail: orgafil@amorc.org.br.
                        </div>
                        <div class="alert alert-danger">
                            Caso o membro seja <a class="alert-link" href="#">dual</a> e seu companheiro/titular esteja cadastrado como atuante em outro organismo o cadastro via SOA não será possível. Entre em contato com a GLP pelos dados passados acima para realizar o cadastro.
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Organismo:</label>
                            <div class="col-sm-9">
                                <select name="siglaOrganismoAfiliado" id="siglaOrganismoAfiliado" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/organismoController.php';
                                    $oc = new organismoController();
                                    $oc->criarComboBox(0, $sessao->getValue("siglaOA"), 1);
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Oficial Ritualístico:</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="codAfiliacaoMembroOficial" maxlength="7" type="text" value="" style="max-width: 76px">
                                <input class="form-control" id="nomeMembroOficial" type="text" maxlength="100" value="" style="min-width: 320px">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMembroOficial','nomeMembroOficial','codAfiliacaoMembroOficial','nomeMembroOficial','h_seqCadastMembroOficial','h_nomeMembroOficial','myModalPesquisa','informacoesPesquisa');">
                                    <icon class="fa fa-search"></icon>
                                </a>
                                <br>
                                Digite o código de afiliação do membro, no outro campo o primeiro nome e pressione TAB<br>
                                <input type="hidden" id="h_seqCadastMembroOficial">
                                <input type="hidden" id="h_nomeMembroOficial">
                                <input type="hidden" id="h_seqCadastCompanheiroMembroOficial">
                                <input type="hidden" id="h_nomeCompanheiroMembroOficial">
                            </div>
                        </div>
                        <!--  
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Tipo de Cargo:</label>
                            <div class="col-sm-9">
                                <select class="form-control col-sm-3" name="tipoCargo" id="tipoCargo" style="max-width: 300px" required="required">
                                    <option value="0">Selecione...</option>
                                    <option value="100">100 - Dignatário</option>
                                    <option value="200">200 - Administrativo</option>
                                    <option value="300">300 - Ritualistico</option>
                                    <option value="400">400 - Iniciático Equipe I</option>
                                    <option value="500">500 - Iniciático Equipe II e III</option>
                                    <option value="600">600 - Outros cargos em comissões</option>
                                    <option value="700">700 - Oficiais templários na GLP</option>
                                    <option value="800">800 - Dignatários SUPREMA e GLP</option>
                                    <option value="800">900 - Moradeiro</option>
                                </select>
                            </div>
                        </div>
                        -->
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Função:</label>
                            <div class="col-sm-9">
                                <select name="funcao" id="funcao" data-placeholder="Selecione um organismo..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    $arrFuncoes = explode(",", $_SESSION['funcoes']);
                                    if (in_array("1", $arrFuncoes)) {
                                        $permissaoGLP = 1;
                                    } else {
                                        $permissaoGLP = null;
                                    }
                                    include_once 'controller/funcaoController.php';
                                    $fc = new funcaoController();
                                    $fc->criarComboBox(0, 1, $permissaoGLP);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_data_entrada">
                            <label class="col-sm-3 control-label">Data de Entrada:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataEntrada" maxlength="10" id="dataEntrada" type="text" class="form-control" style="max-width: 102px">
                            </div>
                        </div>
                        <!--
                        <div class="form-group" id="datapicker_data_saida">
                            <label class="col-sm-3 control-label">Data de Saída:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataSaida" onBlur="validaData(this.value, this);" maxlength="10" id="dataSaida" type="text" class="form-control" style="max-width: 102px">
                            </div>
                        </div>
                        -->
                        <div class="form-group" id="datapicker_data_termino_mandato">
                            <label class="col-sm-3 control-label">Data de Término de Mandato:</label>
                            <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataTerminoMandato" maxlength="10" id="dataTerminoMandato" type="text" class="form-control" style="max-width: 102px">
                            </div>
                        </div>
                        <input type="hidden" name="principalCompanheiro" id="principalCompanheiro" value="">
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar a Ata" onClick="atualizarOficial()">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaOficial" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Edição de ATA de Reunião Mensal do OA -->
<div class="modal inmodal" id="mySeeMotivoEdicao" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-times-circle modal-icon"></i>
                <h4 class="modal-title">Edição de ATA de Reunião Mensal do OA</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <label>Motivo da edição:</label>
                <div style="border: #ccc solid 1px">
                    <div class="summernote"></div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="codInscricaoCancelada" id="codInscricaoCancelada" />
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="atualizaMotivoEdicaoATA();">Editar ATA</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Window MODAL Fim -->
<script>
</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	