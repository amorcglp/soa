<?php
      @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividade Para Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeAgendaTipo">Tipos de Atividade</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Tipos de Atividades</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Tipos de Atividade Agenda</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeAgendaTipo">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoCadastrar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Atividade: </label>
                            <div class="col-sm-10">
                                <input type="text" name="nome" id="nome" class="form-control" value="" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; max-width: 850px">
	                            	<div class="mail-text h-200">
										<textarea class="summernote" id="descricao" name="descricao"></textarea>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Destinado ao OA: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="classiOaTipoAtividadeAgenda" id="classiOaTipoAtividadeAgenda" style="max-width: 250px">
                                    <option value="0">Selecione</option>
                                    <option value="1">Loja</option>
                                    <option value="2">Pronaos</option>
                                    <option value="3">Capítulo</option>
                                    <!--<option value="4">Todos</option>-->
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" id="statusTipoAtividadeAgenda" name="statusTipoAtividadeAgenda" value="0">
                                <input type="hidden" id="usuario" name="usuario" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <a class="btn btn-white" href="?corpo=buscaAtividadeAgendaTipo"> Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>