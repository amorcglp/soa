
<!--<link href="css/plugins/cropper/cropper.min.css" rel="stylesheet">-->

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Anotações Pessoais
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <strong>
                    Anotações Pessoais
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Anotação salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$excluido = isset($_REQUEST['excluido'])?$_REQUEST['excluido']:null;
if($excluido==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Anotação excluída com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}


</script>
<?php }?>
<script>
    function avisoRemoveAnotacao(idAnotacao){
        var escolha = confirm("Deseja realmente excluir?");
        if (escolha == true) {
                window.location='?corpo=removeAnotacoes&idAnotacoes='+idAnotacao;               
        }
    }
</script>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Nova Anotação Pessoal</h5>
                    <div class="ibox-tools">
                        &nbsp;
                    </div>
                </div>
                <div class="ibox-content" style="height: 100%!important;">

                    <ul class="notes">
                        <?php
                        include_once("controller/anotacoesController.php");

                        $rrc = new anotacoesController();
                        $sessao = new criaSessao();
                        $resultado = $rrc->listaAnotacoes($sessao->getValue("seqCadast"));

                        if ($resultado) {
                            foreach ($resultado as $vetor) {
                                ?>
                                <li>
                                    <div>
                                        <small>
                                            <a href="?corpo=alteraAnotacoes&idAnotacoes=<?php echo $vetor['idAnotacoes']; ?>"><i class="fa fa-pencil"></i></a>
                                            <?php $dataFormatada = $vetor['horaAnotacoes']; ?>
                                            <?php if ($dataFormatada != '00:09') { ?>
                                                <?php echo substr($vetor['horaAnotacoes'], 0, 2) . ":" . substr($vetor['horaAnotacoes'], 3, 2); ?>
                                            <?php } else { ?>
                                                <?php echo "Hora Indisponível"; ?>
                                            <?php } ?>
                                            -
                                            <?php $dataFormatada = date('d/m/Y', strtotime($vetor['dataAnotacoes'])); ?>
                                            <?php if (($dataFormatada != '31/12/1969') && ($dataFormatada != '00/00/0000')) { ?>
                                                <?php echo $dataFormatada; ?>
                                            <?php } else { ?>
                                                <?php echo "Data Indisponível"; ?>
                                            <?php } ?>
                                        </small>
                                        <h4><?php echo $vetor['tituloAnotacoes']; ?></h4>
                                        <p><?php echo $vetor['conteudoAnotacoes']; ?></p>
                                        <!--<a href="?corpo=alteraAnotacoes&idAnotacoes=<?php // echo $vetor['idAnotacoes']; ?>"><i class="fa fa-pencil"></i></a>-->
                                        <a onclick="avisoRemoveAnotacao('<?php echo $vetor['idAnotacoes']; ?>');"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </li>
                            <?php
                            }
                        }
                        ?>
                        <li>
                            <div>
                                <h4><center>Nova Anotação</center></h4>
                                <p><center>Grave suas anotações no sistema!</center></p>
                                <p><a class="btn btn-sm btn-primary" href="?corpo=cadastroAnotacoes" style="color: white"><i class="fa fa-plus fa-white"></i> Clique aqui</a></p>
                            </div>
                        </li>
                    </ul>
                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                                &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Moment.js-->
<!--
<script src="js/moment.js"></script>
<script src="js/moment-with-locales.js"></script>
<!--
<script>

                    function mascara_data(data) {
                        var mydata = '';
                        mydata = mydata + data;
                        if (mydata.length == 2) {
                            mydata = mydata + '/';
                            document.forms[0].data.value = mydata;
                        }
                        if (mydata.length == 5) {
                            mydata = mydata + '/';
                            document.forms[0].data.value = mydata;
                        }
                        if (mydata.length == 10) {
                            verifica_data();
                        }
                    }

                    function verifica_data() {

                        dia = (document.forms[0].data.value.substring(0, 2));
                        mes = (document.forms[0].data.value.substring(3, 5));
                        ano = (document.forms[0].data.value.substring(6, 10));

                        situacao = "";
                        // verifica o dia valido para cada mes
                        if ((dia < 01) || (dia < 01 || dia > 30) && (mes == 04 || mes == 06 || mes == 09 || mes == 11) || dia > 31) {
                            situacao = "falsa";
                        }

                        // verifica se o mes e valido
                        if (mes < 01 || mes > 12) {
                            situacao = "falsa";
                        }

                        // verifica se e ano bissexto
                        if (mes == 2 && (dia < 01 || dia > 29 || (dia > 28 && (parseInt(ano / 4) != ano / 4)))) {
                            situacao = "falsa";
                        }

                        if (document.forms[0].data.value == "") {
                            situacao = "falsa";
                        }

                        if (situacao == "falsa") {
                            alert("Data inválida!");
                            document.forms[0].data.focus();
                        }
                    }

                    function mascara_hora(hora) {
                        var myhora = '';
                        myhora = myhora + hora;
                        if (myhora.length == 2) {
                            myhora = myhora + ':';
                            document.forms[0].hora.value = myhora;
                        }
                        if (myhora.length == 5) {
                            verifica_hora();
                        }
                    }

                    function verifica_hora() {
                        hrs = (document.forms[0].hora.value.substring(0, 2));
                        min = (document.forms[0].hora.value.substring(3, 5));

                        alert('hrs ' + hrs);
                        alert('min ' + min);

                        situacao = "";
                        // verifica data e hora
                        if ((hrs < 00) || (hrs > 23) || (min < 00) || (min > 59)) {
                            situacao = "falsa";
                        }

                        if (document.forms[0].hora.value == "") {
                            situacao = "falsa";
                        }

                        if (situacao == "falsa") {
                            alert("Hora inválida!");
                            document.forms[0].hora.focus();
                        }
                    }
</script>

<script>
    function mascara_data(data) {
        var mydata = '';
        mydata = mydata + data;
        if (mydata.length == 2) {
            mydata = mydata + '/';
            document.forms[0].data.value = mydata;
        }
        if (mydata.length == 5) {
            mydata = mydata + '/';
            document.forms[0].data.value = mydata;
        }
        if (mydata.length == 10) {
            verifica_data();
        }
    }

    function verifica_data() {

        dia = (document.forms[0].data.value.substring(0, 2));
        mes = (document.forms[0].data.value.substring(3, 5));
        ano = (document.forms[0].data.value.substring(6, 10));

        situacao = "";
        // verifica o dia valido para cada mes 
        if ((dia < 01) || (dia < 01 || dia > 30) && (mes == 04 || mes == 06 || mes == 09 || mes == 11) || dia > 31) {
            situacao = "falsa";
        }

        // verifica se o mes e valido 
        if (mes < 01 || mes > 12) {
            situacao = "falsa";
        }

        // verifica se e ano bissexto 
        if (mes == 2 && (dia < 01 || dia > 29 || (dia > 28 && (parseInt(ano / 4) != ano / 4)))) {
            situacao = "falsa";
        }

        if (document.forms[0].data.value == "") {
            situacao = "falsa";
        }

        if (situacao == "falsa") {
            alert("Data inválida!");
            document.forms[0].data.focus();
        }
    }

    function mascara_hora(hora) {
        var myhora = '';
        myhora = myhora + hora;
        if (myhora.length == 2) {
            myhora = myhora + ':';
            document.forms[0].hora.value = myhora;
        }
        if (myhora.length == 5) {
            verifica_hora();
        }
    }

    function verifica_hora() {
        hrs = (document.forms[0].hora.value.substring(0, 2));
        min = (document.forms[0].hora.value.substring(3, 5));

        alert('hrs ' + hrs);
        alert('min ' + min);

        situacao = "";
        // verifica data e hora 
        if ((hrs < 00) || (hrs > 23) || (min < 00) || (min > 59)) {
            situacao = "falsa";
        }

        if (document.forms[0].hora.value == "") {
            situacao = "falsa";
        }

        if (situacao == "falsa") {
            alert("Hora inválida!");
            document.forms[0].hora.focus();
        }
    }
</script>


         <!-- Informe as Horas: <input type="text" name="relogio" maxlength="8" onkeypress="dois_pontos(this)" onBlur="valida_horas(this)"><br> -->
<!--
<script type="text/javascript">

    function dois_pontos(tempo) {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.returnValue = false;
        }
        if (tempo.value.length == 2 || tempo.value.length == 5) {
            tempo.value += ":";
        }
    }

    function valida_horas(tempo) {
        horario = tempo.value.split(":");
        var horas = horario[0];
        var minutos = horario[1];
        if (horas > 24) { //para relógio de 12 horas altere o valor aqui
            alert("Horas inválidas");
            event.returnValue = false;
            relogio.focus()
        }
        if (minutos > 59) {
            alert("Minutos inválidos");
            event.returnValue = false;
            relogio.focus()
        }
    }

</script>-->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()

?>