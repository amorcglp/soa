<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("controller/usuarioController.php");
$um = new Usuario();
require_once ("controller/organismoController.php");
$oc = new organismoController();

include_once("model/estatutoAssinadoClass.php");
$apa = new estatutoAssinado();

include_once("model/estatutoClass.php");



/*
@include_once '../mailgun.php';
echo "teste2";
*/
$e = new estatuto();

$anexo                      = isset($_FILES['anexo']['name']) ? $_FILES['anexo']['name'] : null;

$idEstatuto                 = isset($_GET['idEstatuto']) ? $_GET['idEstatuto'] : null;
$alteraStatusEstatuto       = isset($_GET['alteraStatusEstatuto']) ? $_GET['alteraStatusEstatuto'] : null;
$motivoStatus               = isset($_GET['motivoStatus']) ? $_GET['motivoStatus'] : null;
$siglaOrganismo             = isset($_GET['siglaOrganismo']) ?$_GET['siglaOrganismo'] : null;
$emailOA                    = isset($_GET['emailOA'])?$_GET['emailOA']:null;
$nomeOA                     = isset($_GET['nomeOA'])?$_GET['nomeOA']:null;
 ?>
<?php
if($siglaOrganismo!=null) {
    $ocultar_json = 1;
    $naoAtuantes = 'N';
    $atuantes = 'S';
    $siglaOA = $siglaOrganismo;
    $seqFuncao = '201';//Mestre do OA
    //$seqFuncao=atualizaFuncao($seqFuncao,$classificacao);
    include 'js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return), true);
    $mestreOA = "";
    $mestreOA2 = "";
    if (isset($obj['result'][0]['fields']['fArrayOficiais'][0])) {
        $mestreOA = $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'];

    }
    if (isset($obj['result'][0]['fields']['fArrayOficiais'][1])) {
        $arr = ordenaOficialAtuanteRetirante(
            $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fNomClient'],
            $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fNomClient'],
            $obj['result'][0]['fields']['fArrayOficiais'][0]['fields']['fDatTerminMandat'],
            $obj['result'][0]['fields']['fArrayOficiais'][1]['fields']['fDatTerminMandat']
        );
        $mestreOA = $arr[0];
        $mestreOA2 = $arr[1];
    }
    if ($mestreOA == "") {
        $mestreOA = $mestreOA2;
    }
}
?>
<?php
/*
 * Alterar Status do Estatuto
 */
$statusAtualizado=0;
if($alteraStatusEstatuto!=null)
{
    $e->setIdEstatuto($idEstatuto);
    $e->setStatus($alteraStatusEstatuto);
    $e->setMotivoStatus($motivoStatus);
    if($e->alteraStatusEstatuto())
    {
        $statusAtualizado=1;
    }
    //Enviar e-mail para o organismo se status é igual a aprovação de registro em cartório
    if($alteraStatusEstatuto==1)
    {
        include_once('lib/phpmailer/class.phpmailer.php');

        //Enviar email com o token
        $texto = '<div>
            <div dir="ltr">
                    <table class="ecxbody-wrap"
                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                            <tbody>
                                    <tr
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                            <td
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                            <td class="ecxcontainer" width="600"
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                    valign="top">
                                                    <div class="ecxcontent"
                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                            <table class="ecxmain" width="100%" cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                    <tbody>
                                                                            <tr
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                    <td class="ecxcontent-wrap"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                            valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                            style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                            alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                    <br>
                                                                                    <br>
                                                                                    <br>
                                                                                            <div style="text-align: left;"></div>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                    <tbody>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top">AOS CUIDADOS DO(A) MESTRE: <b>' . strtoupper($mestreOA) . '</b>,</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">A GLP realizou análise nas informações para impressão do estatuto e aprovou o registro do mesmo em Cartório. Já é possível ao mestre entrar no cadastro de estatutos no SOA e fazer o donwload do estatuto. Basta acessar: <a href="https://soa.amorc.org.br" target="_blank">soa.amorc.org.br</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Sincera e Fraternalmente
                                                                                                                            <br>AMORC - Ordem Rosacruz</td>
                                                                                                            </tr>
                                                                                                    </tbody>
                                                                                            </table></td>
                                                                            </tr>
                                                                    </tbody>
                                                            </table>
                                                            <div class="footer"
                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                    <div style="text-align: left;"></div>
                                                                    <table width="100%"
                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <tbody>
                                                                                    <tr
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                            <td class="ecxaligncenter ecxcontent-block"
                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                    align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2015</td>
                                                                                    </tr>
                                                                            </tbody>
                                                                    </table>
                                                            </div>
                                                    </div>
                                                    <div style="text-align: left;"></div></td>
                                            <td
                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                    </tr>
                            </tbody>
                    </table>
                    </div>
            </div>';
        /*
                $servidor = "smtp.office365.com";
                $porta = "587";
                $usuario = "noresponseglp@amorc.org.br";
                $senha = "@w2xpglp";
        */
                /*
                $servidor = "smtp.outlook.com";
                $porta = "465";
                $usuario = "samufcaldas@hotmail.com.br";
                $senha = "safc3092";
                */

        $servidor = "smtp.gmail.com";
        $porta = "465";
        $usuario = "cpdglp@gmail.com";
        $senha = "@w2xpglp";

        #Disparar email
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPSecure = "ssl";
        $mail->SMTPAuth = true;
        $mail->Host = $servidor; // SMTP utilizado
        $mail->Port = $porta;
        $mail->Username = $usuario;
        $mail->Password = $senha;
        $mail->From = 'noresponseglp@amorc.org.br';
        $mail->AddReplyTo("atendimento@amorc.org.br", "Antiga e Mística Ordem Rosacruz - AMORC");
        $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
        //$mail->AddAddress("samufcaldas@hotmail.com.br");
        //$mail->AddAddress("tatiane.jesus@amorc.org.br");
        //$mail->AddAddress("lucianob@amorc.org.br");
        $mail->AddAddress("braz@amorc.org.br");
        //$mail->AddAddress($emailOA);
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
        $mail->Subject = 'Aprovação para Download e Registro em Cartório do Estatuto do Organismo - SOA';
        $mail->Body = $texto;
        $mail->AltBody = strip_tags($texto);
        $enviado = $mail->Send();

        /*
        $enviado = $mgClient->sendMessage($domain, array(
            'from'    => 'Atendimento <atendimento@amorc.org.br>',
            'to'      => strtoupper($mestreOA).' <'.$emailOA.'>',
            'subject' => 'Aprovação para Download e Registro em Cartório do Estatuto do Organismo - SOA',
            'text'    => 'Seu e-mail não suporta HTML',
            'html'    => $texto));
        if ($enviado) {
            $retorno['status'] = '1';
        } else {
            $retorno['status'] = '2';
        }*/
    }

    //Enviar e-mail para o organismo se status é igual a aprovação de registro em cartório
    if($alteraStatusEstatuto==3)
    {
        include_once('lib/phpmailer/class.phpmailer.php');

        //Enviar email com o token
        $texto = '<div>
            <div dir="ltr">
                    <table class="ecxbody-wrap"
                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                            <tbody>
                                    <tr
                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                            <td
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                            <td class="ecxcontainer" width="600"
                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                    valign="top">
                                                    <div class="ecxcontent"
                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                            <table class="ecxmain" width="100%" cellpadding="0"
                                                                    cellspacing="0"
                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                    <tbody>
                                                                            <tr
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                    <td class="ecxcontent-wrap"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                            valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                            style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                            alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                    <br>
                                                                                    <br>
                                                                                    <br>
                                                                                            <div style="text-align: left;"></div>
                                                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                    <tbody>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                            valign="top">AOS CUIDADOS DO SETOR DE ORGANISMOS AFILIADOS DA GLP,</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">O organismo '.$nomeOA.' acaba de confirmar que já realizou o registro do estatuto em Cartório.</td>
                                                                                                            </tr>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-block"
                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                            valign="top">Sincera e Fraternalmente
                                                                                                                            <br>AMORC - Ordem Rosacruz</td>
                                                                                                            </tr>
                                                                                                    </tbody>
                                                                                            </table></td>
                                                                            </tr>
                                                                    </tbody>
                                                            </table>
                                                            <div class="footer"
                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                    <div style="text-align: left;"></div>
                                                                    <table width="100%"
                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <tbody>
                                                                                    <tr
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                            <td class="ecxaligncenter ecxcontent-block"
                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                    align="center" valign="top">Sistema de Organismos Afiliados AMORC-GLP 2015</td>
                                                                                    </tr>
                                                                            </tbody>
                                                                    </table>
                                                            </div>
                                                    </div>
                                                    <div style="text-align: left;"></div></td>
                                            <td
                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                    valign="top"></td>
                                    </tr>
                            </tbody>
                    </table>
                    </div>
            </div>';
        /*
                $servidor = "smtp.office365.com";
                $porta = "587";
                $usuario = "noresponseglp@amorc.org.br";
                $senha = "@w2xpglp";
        */
        /*
        $servidor = "smtp.outlook.com";
        $porta = "465";
        $usuario = "samufcaldas@hotmail.com.br";
        $senha = "safc3092";
        */

        $servidor = "smtp.gmail.com";
        $porta = "465";
        $usuario = "cpdglp@gmail.com";
        $senha = "@w2xpglp";

        #Disparar email
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPSecure = "ssl";
        $mail->SMTPAuth = true;
        $mail->Host = $servidor; // SMTP utilizado
        $mail->Port = $porta;
        $mail->Username = $usuario;
        $mail->Password = $senha;
        $mail->From = 'noresponseglp@amorc.org.br';
        $mail->AddReplyTo("atendimento@amorc.org.br", "Antiga e Mística Ordem Rosacruz - AMORC");
        $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
        //$mail->AddAddress("samufcaldas@hotmail.com.br");
        //$mail->AddAddress("tatiane.jesus@amorc.org.br");
        //$mail->AddAddress("lucianob@amorc.org.br");
        $mail->AddAddress("braz@amorc.org.br");
        //$mail->AddAddress($emailOA);
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
        $mail->Subject = 'Registro em Cartório Realizado - SOA';
        $mail->Body = $texto;
        $mail->AltBody = strip_tags($texto);
        $enviado = $mail->Send();

        /*
        $enviado = $mgClient->sendMessage($domain, array(
            'from'    => 'Atendimento <atendimento@amorc.org.br>',
            'to'      => strtoupper($mestreOA).' <'.$emailOA.'>',
            'subject' => 'Aprovação para Download e Registro em Cartório do Estatuto do Organismo - SOA',
            'text'    => 'Seu e-mail não suporta HTML',
            'html'    => $texto));
        if ($enviado) {
            $retorno['status'] = '1';
        } else {
            $retorno['status'] = '2';
        }*/
    }
}   

/**
 * Upload da Ata Assinada
 */
$estatutoAssinado = 0;
$extensaoNaoValida = 0;
if ($anexo != "") {
    if ($_FILES['anexo']['name'] != "") {
        if(substr_count($_FILES['anexo']['name'],".")==1)
        {

            $extensao = strstr($_FILES['anexo']['name'], '.');
            if ($extensao == ".pdf" ||
                    $extensao == ".png" ||
                    $extensao == ".jpg" ||
                    $extensao == ".jpeg" ||
                    $extensao == ".PDF" ||
                    $extensao == ".PNG" ||
                    $extensao == ".JPG" ||
                    $extensao == ".JPEG"
            ) {
                $proximo_id = $apa->selecionaUltimoId();
                $caminho = "img/estatuto/" . basename($_POST["idEstatutoUpload"] . ".estatuto." . $proximo_id);
                $apa->setFk_idEstatuto($_POST["idEstatutoUpload"]);
                $apa->setCaminho($caminho);
                if ($apa->cadastroEstatutoAssinado()) {
                    $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/estatuto/';
                    $uploadfile = $uploaddir . basename($_POST["idEstatutoUpload"] . ".estatuto." . $proximo_id);
                    if (move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile)) {
                        $ataAssinada = 1;
                    } else {
                        echo "<script type='text/javascript'>alert('Erro ao enviar estatuto assinado!');</script>";
                    }
                }
            } else {
                $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php
$salvo = isset($_REQUEST['salvo']) ? $_REQUEST['salvo'] : null;
if ($salvo == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Estatuto salvo com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php } ?>
<?php
$jaCadastrado = isset($_REQUEST['jaCadastrado']) ? $_REQUEST['jaCadastrado'] : null;
if ($jaCadastrado == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Estatuto já cadastrado anteriormente!",
                type: "error",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if ($ataAssinada == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Estatuto assinado enviado com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
    <?php
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

if ($statusAtualizado == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Sucesso!",
                text: "Status do Estatuto atualizado com sucesso!",
                type: "success",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
    <?php
}
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Estatutos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaEstatuto">Cadastros</a>
            </li>
            <li class="active">
                <strong><a>Estatutos</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Estatutos</h5>
                    <div class="ibox-tools">
<?php if (in_array("1", $arrNivelUsuario)) { ?>
                            <a class="btn btn-xs btn-primary" href="?corpo=cadastroEstatuto">
                                <i class="fa fa-plus"></i> Novo Estatuto
                            </a>
<?php } ?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód.</th>
                                <th>Organismo</th>
                                <th><center>Data da Assembléia</center></th>
                                <th><center>Número de Presentes</center></th>
                        <th><center>Status do OA</center></th>
                        <th><center>Data da Entrega</center></th>
                        <th><center>Estatuto Assinado</center></th>
                        <th><center>Status</center></th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
<?php

$resultado = $e->listaEstatuto();

if ($resultado) {
    foreach ($resultado as $vetor) {

        $nomeOrganismoAfiliado = $vetor['nomeOrganismoAfiliado'];

            switch ($vetor['classificacaoOrganismoAfiliado']) {
                case 1:
                    $classificacao = "Loja";
                    break;
                case 2:
                    $classificacao = "Pronaos";
                    break;
                case 3:
                    $classificacao = "Capítulo";
                    break;
                case 4:
                    $classificacao = "Heptada";
                    break;
                case 5:
                    $classificacao = "Atrium";
                    break;
            }
            switch ($vetor['tipoOrganismoAfiliado']) {
                case 1:
                    $tipo = "R+C";
                    $tipo2 = "Rosacruz";
                    break;
                case 2:
                    $tipo = "TOM";
                    $tipo2 = "Martinista";
                    break;
            }

            switch ($vetor['paisOrganismoAfiliado']) {
                case 1:
                    $siglaPais = "BR";
                    break;
                case 2:
                    $siglaPais = "PT";
                    break;
                case 3:
                    $siglaPais = "AO";
                    break;
                case 4:
                    $siglaPais = "MZ";
                    break;
                default :
                    $siglaPais = "BR";
                    break;
            }
            $siglaOrganismo = $vetor['siglaOrganismoAfiliado'];
            $nomeOrganismo = $siglaOrganismo . " - " . $classificacao . " " . $tipo . " " . $nomeOrganismoAfiliado;
            $nomeOrganismo2 = $classificacao . " " . $tipo . " " . $nomeOrganismoAfiliado;
            $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];

            $seqOrganismoAfiliado = $vetor['seqCadast'];

            $ocultar_json = 1;
            $siglaOA = $siglaOrganismo;
            $pais = $siglaPais;
            //Consultar Webservice da Inovad
            if (realpath('js/ajax/retornaDadosOrganismo.php')) {
                include ('js/ajax/retornaDadosOrganismo.php');
            } else {
                include ('../js/ajax/retornaDadosOrganismo.php');
            }

            $obj = json_decode(json_encode($return), true);
            //echo "<pre>";print_r($obj);

            $dataElevacaoPronaos = "";
            $dataElevacaoCapitulo = "";
            $dataElevacaoLoja = "";
            $cidade = "";
            $uf = "";
            $rua = "";
            $numero = "";
            $bairro = "";
            $cnpj = "";
            $situacaoDB = "";
            if(isset($obj['result'][0]['fields']['fArrayOA']))
            {
                foreach ($obj['result'][0]['fields']['fArrayOA'] as $vetor2) {
                    $situacaoDB = $vetor2['fields']['fSituacaoOa'];
                    switch ($vetor2['fields']['fSituacaoOa']) {
                        case 'R':
                            $statusOrganismo = "Recesso";
                            break;
                        case 'A':
                            $statusOrganismo = "Ativo";
                            break;
                        case 'F':
                            $statusOrganismo = "Fechado";
                            break;
                    }

                    //Pegar Datas Históricas
                    $dataElevacaoPronaos = trim($vetor2['fields']['fDatElevacPronau']);
                    //$dataElevacaoPronaos = substr($dataElevacaoPronaos, 8, 2) . " de " . mesExtensoPortugues(substr($dataElevacaoPronaos, 5, 2)) . " de " . substr($dataElevacaoPronaos, 0, 4);

                    $dataElevacaoCapitulo = trim($vetor2['fields']['fDatElevacCapitu']);
                    //$dataElevacaoCapitulo = substr($dataElevacaoCapitulo, 8, 2) . " de " . mesExtensoPortugues(substr($dataElevacaoCapitulo, 5, 2)) . " de " . substr($dataElevacaoCapitulo, 0, 4);

                    $dataElevacaoLoja = trim($vetor2['fields']['fDatElevacLoja']);
                    //$dataElevacaoLoja = substr($dataElevacaoLoja, 8, 2) . " de " . mesExtensoPortugues(substr($dataElevacaoLoja, 5, 2)) . " de " . substr($dataElevacaoLoja, 0, 4);
                }
            }    
            
            

        $resposta3 = $apa->listaEstatutoAssinado($vetor['idEstatuto']);
        if ($resposta3) {
            $total = count($resposta3);
        } else {
            $total = 0;
        }
        ?>
                                    <?php if ((strtoupper($vetor["siglaOrganismoAfiliado"]) == strtoupper($sessao->getValue("siglaOA"))) || (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2))) { ?>
                                        <tr>
                                            <td>
                                        <?php

                                        echo $vetor['idEstatuto'];

                                        $nomeOA = $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado'];

                                        ?>
                                            </td>
                                            <td>
                                                <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                            </td>
                                            <td>
                                    <center>
            <?php echo substr($vetor['dataAssembleiaGeral'], 8, 2) . "/" . substr($vetor['dataAssembleiaGeral'], 5, 2) . "/" . substr($vetor['dataAssembleiaGeral'], 0, 4); ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php echo $vetor['numeroPresentes']; ?>
                                    </center>    
                                    </td>
                                    <td>
                                    <center>
                                        <?php echo $statusOrganismo; ?>
                                    </center>    
                                    </td>
                                    <td>
                                    <center>
            <?php echo substr($vetor['dataCadastro'], 8, 2) . "/" . substr($vetor['dataCadastro'], 5, 2) . "/" . substr($vetor['dataCadastro'], 0, 4); ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center><?php if ($total > 0) { ?><button class="btn btn-info  dim" type="button" onclick="listaUploadEstatuto('<?php echo $vetor['idEstatuto']; ?>')" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"><i class="fa fa-paste"></i> </button><?php } else {
                echo "Não enviado";
            } ?></center>
                                    </td>
                                    <td>
                                        <center>
                                        <?php 
                                        if($dataElevacaoPronaos==""&&$dataElevacaoCapitulo==""&&$dataElevacaoLoja=="")
                                        {
                                            echo '<span class="badge badge-danger">Favor enviar as informações de fundação/elevação <b>(Datas)</b><br>do Organismo para GLP (orgafil2@amorc.org.br)</span><br><br>';
                                        }
                                        switch ($vetor['status']){
                                            case 0:
                                                echo '<span class="badge badge-success">Aguardando aprovação da GLP</span>';
                                                break;
                                            case 1:
                                                echo '<span class="badge badge-primary">Aprovado para registro em cartório</span>';
                                                break;
                                            case 2:
                                                echo '<a href="#" onclick="listaUploadEstatutoMotivo('.$vetor['idEstatuto'].')" data-target="#mySeeListaMotivo" data-toggle="modal" data-placement="left"><span class="badge badge-danger">Não aprovado, veja aqui o motivo</span></a>';
                                                break;
                                            case 3:
                                                echo '<span class="badge badge-warning">Registro em cartório realizado. Envie o documento para GLP</span>';
                                                break;
                                            default :
                                                echo '<span class="badge badge-warning">Aguardando aprovação da GLP</span>';
                                        }

                                        ?>
                                        </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php if (in_array("2", $arrNivelUsuario)) { ?>
                                            <a href="?corpo=alteraEstatuto&idestatuto=<?php echo $vetor['idEstatuto']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar ATA dos OA's">
                                                <i class="fa fa-edit fa-white"></i>&nbsp;
                                                Editar
                                            </a>
                                        <?php } ?>
                                        <?php 
                                        if($dataElevacaoPronaos!=""||$dataElevacaoCapitulo!=""||$dataElevacaoLoja!="")
                                        {?>
                                        <a href="impressao/gerarEstatuto.php?idEstatuto=<?php echo $vetor['idEstatuto']; ?>" target="_blank">
                                            <button type="button" class="btn btn-sm btn-danger" title="Download Estatuto">
                                                <i class="fa fa-download fa-white"></i>&nbsp;
                                                Download do PDF
                                            </button>
                                        </a>
                                        <?php }?>
                                        <?php if(($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)){?>
                                            <?php if (in_array("4", $arrNivelUsuario)) { ?>
                                                <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('idEstatutoUpload').value =<?php echo $vetor['idEstatuto']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload">
                                                    <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                                    Enviar
                                                </button>
                                            <?php } ?>
                                            <br>
                                            <?php if($vetor['status']!=1){?>
                                            <a href="?corpo=buscaEstatuto&alteraStatusEstatuto=1&idEstatuto=<?php echo $vetor['idEstatuto']; ?>&siglaOrganismo=<?php echo $vetor['siglaOrganismoAfiliado']; ?>&emailOA=<?php echo $vetor['emailOrganismoAfiliado']; ?>">
                                                <button type="button" class="btn btn-outline btn-primary">
                                                    <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                                    Aprovar Registro em Cartório e Avisar
                                                </button>
                                            </a>
                                            <?php } ?>
                                            <?php if($vetor['status']!=2){?>
                                            <a href="" onclick="document.getElementById('idEstatuto').value=<?php echo $vetor['idEstatuto']; ?>" data-target="#mySeeMotivo" data-toggle="modal" data-placement="left">
                                                <button type="button" class="btn btn-outline btn-danger" title="Download Estatuto">
                                                    <i class="fa fa-thumbs-o-down"></i>&nbsp;
                                                    Desaprovar
                                                </button>
                                            </a>
                                            <?php }?>
                                        <?php }?>
                                        <?php if($vetor['status']==1){?>
                                        <a href="?corpo=buscaEstatuto&alteraStatusEstatuto=3&idEstatuto=<?php echo $vetor['idEstatuto']; ?>&nomeOA=<?php echo $nomeOA;?>">
                                            <button type="button" class="btn btn-outline btn-warning" title="Download Estatuto">
                                                <i class="fa fa-thumbs-o-up"></i>&nbsp;
                                                Registro em Cartório Realizado
                                            </button>
                                        </a>
                                        <?php }?>
                                    </center>
                                    </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Estatuto Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="10000000">
                    <div class="form-group">
                        <!--  
                        <div class="alert alert-danger">
       <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
</div>
                        -->
                        <div class="col-sm-10">
                            <input name="anexo"
                                   id="anexo" type="file" />
                        </div>
                        <div id="anexoAlerta" class="col-sm-10"
                             style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
                            máximo de 10MB.</div>

                    </div>
                    
                    <input type="hidden" id="idEstatutoUpload" name="idEstatutoUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-info" value="Enviar" onclick="document.ata.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Estatuto Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaUpload"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeMotivo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <form>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-thumbs-o-down modal-icon"></i>
                <h4 class="modal-title">Motivo da Desaprovação</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea rows="5" cols="50" name="motivoStatus" id="motivoStatus"></textarea>
                </div>
            </div>
            <input type="hidden" id="corpo" name="corpo" value="buscaEstatuto">
            <input type="hidden" id="alteraStatusEstatuto" name="alteraStatusEstatuto" value="2">
            <input type="hidden" id="idEstatuto" name="idEstatuto" value="">
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Salvar">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaMotivo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Motivo da Desaprovação</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div id="listaMotivoEstatuto"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
