<?php
include_once("controller/notificacaoGlpController.php");
include_once("controller/usuarioController.php");
include_once("model/notificacaoGlpClass.php");
include_once("model/usuarioClass.php");

$idNotificacaoGlp	= isset($_GET['idNotificacaoGlp']) ? json_decode($_GET['idNotificacaoGlp']) : '';

$ngc = new notificacaoGlpController();                                
$u = new Usuario();                                

$resultadoNotificacao = $ngc->listaNotificacaoGlp($idNotificacaoGlp);
if ($resultadoNotificacao){
	foreach ($resultadoNotificacao as $vetor){
		$tituloNotificacaoGlp			= $vetor['tituloNotificacaoGlp'];
		$mensagemNotificacaoGlp		= $vetor['mensagemNotificacaoGlp'];
		$dataNotificacaoGlp				= $vetor['dataNotificacaoGlp'];
		$tipoNotificacaoGlp				= $vetor['tipoNotificacaoGlp'];
	}
}
//Se for GLP então alterar status da notificação para todos da GLP
if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) {
    $liberaGLP = true;
    //Se for alguém da GLP então decrementar ticket como visto para todos
    $resultadoUsuarios = $u->listaUsuario(null,"2,3");
    if($resultadoUsuarios)
    {
            foreach ($resultadoUsuarios as $vetor)
            {
                 $nm = new NotificacaoGlp();
                 $nm->setFk_idNotificacaoGlp($idNotificacaoGlp);
                 $nm->setFk_seqCadast($vetor['idUsuario']);
                 $result = $nm->alteraStatusNotificacaoGlp();
            }
    }
}
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $tituloNotificacaoGlp; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li>
                <a href="?corpo=buscaNotificacoes">Notificações</a>
            </li>
            <li class="active">
                <strong><?php echo $tituloNotificacaoGlp; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight article">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="text-center article-title">
                        <span class="text-muted"><i class="fa fa-clock-o"></i>
							<?php 
								$data = date_create($dataNotificacaoGlp);
								echo date_format($data, 'd/m/Y - H:i:s');
							?>
						</span>
                        <h1>
                            <?php echo $tituloNotificacaoGlp; ?>
                        </h1>
                    </div>
                    <br>
                    <div style="text-align: justify">
                        <?php echo $mensagemNotificacaoGlp; ?>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="small text-right">
                                <h5>Autor:</h5>
								<i class="fa fa-institution"></i>
								<?php 
									$um = new Usuario();
									$resultadoRemetente = $um->buscaUsuario($vetor['fk_seqCadastRemetente']);
									if ($resultadoRemetente) {
										foreach ($resultadoRemetente as $vetorRemetente) {
											echo $vetorRemetente['nomeUsuario'];
						                            	
											$resultDepartamento = $ngc->buscaDepartamento($vetorRemetente['fk_idDepartamento']);
											if ($resultDepartamento) {
												foreach ($resultDepartamento as $vetorDepartamento) {
													echo " [".$vetorDepartamento['nomeDepartamento']."]";
												}
											}
										}
									}
								?>
                            </div>
                        </div>
                        <center><a href="?corpo=buscaNotificacoes"><i class="fa fa-reply"> </i> Voltar</a></center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>