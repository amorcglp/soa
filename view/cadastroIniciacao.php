<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>			<!-- Conteúdo DE INCLUDE INÍCIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Área de Iniciações</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="index.html">Home</a>
                        	</li>
	                        <li>
	                            <a href="index.html">Iniciações</a>
	                        </li>
	                        <li class="active">
	                            <strong><a>Agendar Iniciação</a></strong>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->

				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Agendar Iniciação</h5>
									<div class="ibox-tools">
										<a class="btn btn-xs btn-primary" href="?opcao=ritualistica_agendada.php">
											<i class="fa fa-share"></i>&nbsp; Ir Para Iniciações Agendadas
										</a>
									</div>
								</div>
								<div class="ibox-content">
									<form action="" class="form-horizontal">
										<div class="form-group">
											<label class="col-sm-3 control-label">Organismo Afiliado: </label>
		                                    <div class="col-sm-9">
												<input class="form-control col-sm-1" id="oa" type="text" value="" style="max-width: 52px">
												<input class="form-control col-sm-8" id="oa_nome" type="text" value="" style="max-width: 320px; margin-left: 12px">
		                                    </div>
		                               	</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Tipo de Membro:</label>
		                                    <div class="col-sm-9">
												<input class="form-control col-sm-1" id="ordem" type="text" value="" style="max-width: 52px">
												<select class="form-control col-sm-3" name="ordem" style="max-width: 320px; margin-left: 12px">
			                                        <option>Rosacruz</option>
			                                        <option>TOM</option>
			                                    </select>
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
											<label class="col-sm-3 control-label">Iniciação:</label>
		                                    <div class="col-sm-9">
												<input class="form-control col-sm-1" id="iniciacao_grau" type="text" value="" style="max-width: 52px">
												<select class="form-control col-sm-3" name="grau" style="max-width: 320px; margin-left: 12px">
			                                        <option selected="">1º Grau do Templo</option>
													<option>2º Grau do Templo</option>
													<option>3º Grau do Templo</option>
													<option>4º Grau do Templo</option>
													<option>5º Grau do Templo</option>
													<option>6º Grau do Templo</option>
													<option>7º Grau do Templo</option>
													<option>8º Grau do Templo</option>
													<option>9º Grau do Templo</option>
													<option>10º Grau do Templo</option>
													<option>11º Grau do Templo</option>
													<option>12º Grau do Templo</option>
			                                    </select>
		                                    </div>
		                               	</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Data:</label>
		                                    <div class="col-sm-9 input-group date" style="padding: 0px 0px 0px 15px">
												<span class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</span>
												<input type="text" class="form-control" value="03/04/2014" style="max-width: 102px">
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
											<label class="col-sm-3 control-label">Hora de Início: </label>
		                                    <div class="col-sm-9">
												<input class="form-control" id="hora_inicio" type="text" value="" style="max-width: 64px">
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
											<label class="col-sm-3 control-label">Local: </label>
		                                    <div class="col-sm-9">
												<input class="form-control" id="local" type="text" value="" style="max-width: 500px">
		                                    </div>
		                               	</div>
		                               	<div class="form-group">
											<label class="col-sm-3 control-label">Observação: </label>
		                                    <div class="col-sm-6">
		                                    	<div style="border: #ccc solid 1px">
												<textarea class="summernote" name="comentario"></textarea>
												</div>
		                                    </div>
		                               	</div>
		                               	<div class="hr-line-dashed"></div>
		                               	<div class="form-group">
		                               		<div class="col-sm-offset-3 col-sm-6">
		                               			<button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Confira detalhes da Iniciação Ritualística">
			                                    	<i class="fa fa-check fa-white"></i>&nbsp;
			                                    	Salvar
				                                </button>
				                                &nbsp;
												<button type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="left" title="Ao editar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
				                                    <i class="fa fa-eraser fa-white"></i>&nbsp;
				                                    Limpar
				                                </button>
				                                &nbsp;
												<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#mySeeMotivoCancelamento" data-toggle="tooltip" data-placement="left" title="Ao cancelar será enviado e-mail para a GLP, o Mestre Provincial e os membros agendados!">
				                                    <i class="fa fa-times fa-white"></i>&nbsp;
				                                    Cancelar
				                                </button>
		                               		</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->
				
				<!-- Conteúdo DE INCLUDE FIM -->
