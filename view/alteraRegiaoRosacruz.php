<?php
require_once("controller/regiaoRosacruzController.php");
require_once("model/criaSessaoClass.php");
?>
<style>

</style>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Região</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaRegiaoRosacruz">Região</a>
            </li>
            <li class="active">
                <a>Cadastro de Nova Região</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição de Nova Região</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaRegiaoRosacruz">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    include_once("controller/regiaoRosacruzController.php");

                    $rrc = new regiaoRosacruzController();
                    $dados = $rrc->buscaRegiaoRosacruz($_GET['id']);
                    ?>
                    <form id="cadastro" method="POST" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idRegiaoRosacruz" id="idRegiaoRosacruz" value="<?php echo $_GET['id']; ?>" />
                        <div class="form-group"><label class="col-sm-2 control-label">*Região</label>
                            <div class="col-sm-10"><input type="text" name="regiaoRosacruz" id="regiaoRosacruz" class="form-control" value="<?php echo $dados->getRegiaoRosacruz(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">*Divisão Política</label>
                            <div class="col-sm-10"><input type="text" name="divisaoPolitica" id="divisaoPolitica" class="form-control" value="<?php echo $dados->getDivisaoPolitica(); ?>" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">*País</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="paisRosacruz" id="paisRosacruz" style="max-width: 150px">
                                    <option value="0">Selecione...</option>
                                    <option value="1" <?php if ($dados->getPaisRosacruz() == 1) {
                        echo "selected";
                    } ?>>Brasil</option>
                                    <option value="2" <?php if ($dados->getPaisRosacruz() == 2) {
                        echo "selected";
                    } ?>>Portugal</option>
                                    <option value="3" <?php if ($dados->getPaisRosacruz() == 3) {
                        echo "selected";
                    } ?>>Angola</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea rows="7" name="descricaoRegiaoRosacruz" id="descricaoRegiaoRosacruz" class="form-control" style="max-width: 500px"><?php echo $dados->getDescricaoRegiaoRosacruz(); ?></textarea></div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
<!-- Tabela Fim -->

