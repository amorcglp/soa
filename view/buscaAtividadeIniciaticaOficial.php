<?php @include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

?>

<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades Iniciáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Equipes de Atividades Iniciáticas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<?php
include_once("controller/atividadeIniciaticaController.php");
include_once("controller/atividadeIniciaticaOficialController.php");
include_once("controller/organismoController.php");
include_once("model/organismoClass.php");

$aic = new atividadeIniciaticaController();
$aioc = new atividadeIniciaticaOficialController();
$aiom = new atividadeIniciaticaOficial();
$oc = new organismoController();
$om = new organismo();
$sessao = new criaSessao();

if($sessao->getValue("alerta-atividade-iniciatica-oficial")==null){
    $poteOAs=0;
    $resultadoOAs = $om->listaOrganismo(null, null, "siglaOrganismoAfiliado", $regiaoUsuario);
    if($resultadoOAs){
        foreach($resultadoOAs as $vetorOAs){
            $poteOAs++;
        }
    }
    if($poteOAs>1){
?>
        <script>
            window.onload = function(){
                swal({
                    title: "Aviso!",
                    text: "Você pode mudar o organismo listado no cabeçalho!",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
        <?php
    }
    $sessao->setValue("alerta-atividade-iniciatica-oficial", true);
}

$idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : $idOrganismoAfiliado;

if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
    $idOrg = $idOrganismo;
} else {
    $idOrg = $idOrganismoAfiliado;
}
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        Equipes de Atividades Iniciáticas
                        <?php
                        //if($idOrganismo!=''){
                        $dadosOrg = $oc->buscaOrganismo($idOrganismo);
                        $nomeOa = organismoAfiliadoNomeCompleto($dadosOrg->getClassificacaoOrganismoAfiliado(),$dadosOrg->getTipoOrganismoAfiliado(),$dadosOrg->getNomeOrganismoAfiliado(),$dadosOrg->getSiglaOrganismoAfiliado());
                        echo ' - '.$nomeOa;
                        //}
                        ?>
                    </h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciatica<?php if($idOrganismo!=''){ echo '&idOrganismoAfiliado='.$idOrganismo;} ?>">
                            <i class="fa fa-reply"></i> Voltar para área de Atividades
                        </a>
                        <?php if(!$usuarioApenasLeitura){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeIniciaticaOficial<?php if($idOrganismo!=''){ echo '&idOrganismoAfiliado='.$idOrganismo;} ?>">
                            <i class="fa fa-plus"></i> Cadastrar Nova
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-equipes-iniciaticas" >
                        <thead>
                        <tr>
                            <th><center>Ano R+C</center></th>
                            <th>Equipe</th>
                            <th><center>Registrado em</center></th>
                            <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $resultado = $aioc->listaAtividadeIniciaticaOficial($idOrg);
                        if ($resultado) {
                            foreach ($resultado as $vetor) {
                                ?>
                                <tr>
                                    <td>
                                        <center>
                                            <?php echo $vetor['anoAtividadeIniciaticaOficial']; ?>
                                        </center>
                                    </td>
                                    <td>
                                        <?php echo "Equipe Iniciática ".$vetor['tipoAtividadeIniciaticaOficial']; ?>
                                    </td>
                                    <td>
                                        <center>
                                            <?php echo substr($vetor['dataRegistroAtividadeIniciaticaOficial'], 8, 2) . "/" . substr($vetor['dataRegistroAtividadeIniciaticaOficial'], 5, 2) . "/" . substr($vetor['dataRegistroAtividadeIniciaticaOficial'], 0, 4) . " - " . substr($vetor['dataRegistroAtividadeIniciaticaOficial'], 11, 2) . ":" . substr($vetor['dataRegistroAtividadeIniciaticaOficial'], 14, 2); ?>
                                            <?php if($vetor['dataAtualizadaAtividadeIniciaticaOficial'] != '0000-00-00 00:00:00'){ ?>
                                                <br>
                                                <div style="margin-top: 5px">
                                                    Última atualização em: <br>
                                                    <?php echo substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'], 8, 2) . "/" . substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'], 5, 2) . "/" . substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'], 0, 4) . " - " . substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'], 11, 2) . ":" . substr($vetor['dataAtualizadaAtividadeIniciaticaOficial'], 14, 2);?>
                                                </div>
                                            <?php } ?>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <?php if(!$usuarioApenasLeitura){?>
                                                <?php if(($idOrganismoAfiliado == $vetor['fk_idOrganismoAfiliado']) || ($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){ ?>
                                                    <a class="btn btn-sm btn-info" href="" data-toggle="modal" data-target="#modalOficiais<?php echo $vetor['idAtividadeIniciaticaOficial']; ?>">
                                                        <i class="fa fa-edit fa-white"></i>
                                                        Incluir/Editar Oficiais
                                                    </a>
                                                <?php } ?>
                                            <?php }?>
                                            <a class="btn btn-sm btn-white" onclick="abrirPopUpAtividadeIniciaticaOficialRelacao(<?php echo $vetor['idAtividadeIniciaticaOficial']; ?>,<?php echo $vetor['fk_idOrganismoAfiliado']; ?>)"><i class="fa fa-print fa-white"></i> Imprimir</a>
                                        </center>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->
<?php
$resultado = $aioc->listaAtividadeIniciaticaOficial($idOrg);
$ocultar_json=1;

if ($resultado) {
    foreach ($resultado as $vetor) {
        $idEquipe       = $vetor['idAtividadeIniciaticaOficial'];
        $equipeTipo   = $vetor['tipoAtividadeIniciaticaOficial'];
        $equipeAno    = $vetor['anoAtividadeIniciaticaOficial'];
        //echo "<pre>";print_r($vetor);echo "</pre>";
        ?>
        <!-- Notificar Responsáveis -->
        <div class="modal inmodal" id="modalOficiais<?php echo $idEquipe;?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceInRight">
                    <form method="post" action="acoes/acaoAlterar.php">
                        <input type="hidden" id="formularioAlteraOficiais" name="formularioAlteraOficiais">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                            <i class="fa fa-users modal-icon"></i>
                            <h4 class="modal-title">Equipe Iniciática <?php echo $equipeTipo;?> - Ano [<?php echo $equipeAno;?>]</h4>
                        </div>
                        <div class="modal-body">
                            <?php
                            $seqCadast = $vetor['mestreAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoMestre         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeMestre                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompMestre        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompMestre             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompMestre        = "";
                                    $nomeCompMestre             = "";
                                }
                            } else {
                                $codAfiliacaoMestre         = "";
                                $nomeMestre                 = "";
                                $seqCadastCompMestre        = "";
                                $nomeCompMestre             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Mestre: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoMestre<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoMestre; ?>" style="max-width: 76px">
                                        <input placeholder="Nome Completo" class="form-control" id="nomeMestre<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeMestre; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMestre<?php echo $idEquipe; ?>','nomeMestre<?php echo $idEquipe; ?>','codAfiliacaoMestre<?php echo $idEquipe; ?>','nomeMestre<?php echo $idEquipe; ?>','mestreAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeMestre<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" name="tipo" id="tipo" value="1">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="mestreAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="mestreAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastMestre<?php echo $idEquipe; ?>" id="h_seqCadastMestre<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeMestre; ?>" name="h_nomeMestre<?php echo $idEquipe; ?>" id="h_nomeMestre<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompMestre; ?>" name="h_seqCadastCompanheiroMestre<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroMestre<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompMestre; ?>" name="h_nomeCompanheiroMestre<?php echo $idEquipe; ?>" id="h_nomeCompanheiroMestre<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['mestreAuxAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoMestreAux         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeMestreAux                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompMestreAux        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompMestreAux             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompMestreAux        = "";
                                    $nomeCompMestreAux             = "";
                                }
                            } else {
                                $codAfiliacaoMestreAux         = "";
                                $nomeMestreAux                 = "";
                                $seqCadastCompMestreAux        = "";
                                $nomeCompMestreAux             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Mestre Auxiliar: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoMestreAux<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoMestreAux; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeMestreAux<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeMestreAux; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMestreAux<?php echo $idEquipe; ?>','nomeMestreAux<?php echo $idEquipe; ?>','codAfiliacaoMestreAux<?php echo $idEquipe; ?>','nomeMestreAux<?php echo $idEquipe; ?>','mestreAuxAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeMestreAux<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="mestreAuxAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="mestreAuxAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastMestreAux<?php echo $idEquipe; ?>" id="h_seqCadastMestreAux<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeMestreAux; ?>" name="h_nomeMestreAux<?php echo $idEquipe; ?>" id="h_nomeMestreAux<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompMestreAux; ?>" name="h_seqCadastCompanheiroMestreAux<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroMestreAux<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompMestreAux; ?>" name="h_nomeCompanheiroMestreAux<?php echo $idEquipe; ?>" id="h_nomeCompanheiroMestreAux<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['arquivistaAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoArquivista         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeArquivista                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompArquivista        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompArquivista             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompArquivista        = "";
                                    $nomeCompArquivista             = "";
                                }
                            } else {
                                $codAfiliacaoArquivista         = "";
                                $nomeArquivista                 = "";
                                $seqCadastCompArquivista        = "";
                                $nomeCompArquivista             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Arquivista: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoArquivista<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoArquivista; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeArquivista<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeArquivista; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoArquivista<?php echo $idEquipe; ?>','nomeArquivista<?php echo $idEquipe; ?>','codAfiliacaoArquivista<?php echo $idEquipe; ?>','nomeArquivista<?php echo $idEquipe; ?>','arquivistaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeArquivista<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="arquivistaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="arquivistaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastArquivista<?php echo $idEquipe; ?>" id="h_seqCadastArquivista<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeArquivista; ?>" name="h_nomeArquivista<?php echo $idEquipe; ?>" id="h_nomeArquivista<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompArquivista; ?>" name="h_seqCadastCompanheiroArquivista<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroArquivista<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompArquivista; ?>" name="h_nomeCompanheiroArquivista<?php echo $idEquipe; ?>" id="h_nomeCompanheiroArquivista<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['capelaoAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoCapelao         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeCapelao                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompCapelao        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompCapelao             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompCapelao        = "";
                                    $nomeCompCapelao             = "";
                                }
                            } else {
                                $codAfiliacaoCapelao         = "";
                                $nomeCapelao                 = "";
                                $seqCadastCompCapelao        = "";
                                $nomeCompCapelao             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Capelão: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoCapelao<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoCapelao; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeCapelao<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeCapelao; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoCapelao<?php echo $idEquipe; ?>','nomeCapelao<?php echo $idEquipe; ?>','codAfiliacaoCapelao<?php echo $idEquipe; ?>','nomeCapelao<?php echo $idEquipe; ?>','capelaoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeCapelao<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="capelaoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="capelaoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastCapelao<?php echo $idEquipe; ?>" id="h_seqCadastCapelao<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCapelao; ?>" name="h_nomeCapelao<?php echo $idEquipe; ?>" id="h_nomeCapelao<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompCapelao; ?>" name="h_seqCadastCompanheiroCapelao<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroCapelao<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompCapelao; ?>" name="h_nomeCompanheiroCapelao<?php echo $idEquipe; ?>" id="h_nomeCompanheiroCapelao<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['matreAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoMatre         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeMatre                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompMatre        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompMatre             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompMatre        = "";
                                    $nomeCompMatre             = "";
                                }
                            } else {
                                $codAfiliacaoMatre         = "";
                                $nomeMatre                 = "";
                                $seqCadastCompMatre        = "";
                                $nomeCompMatre             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Matre: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoMatre<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoMatre; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeMatre<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeMatre; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMatre<?php echo $idEquipe; ?>','nomeMatre<?php echo $idEquipe; ?>','codAfiliacaoMatre<?php echo $idEquipe; ?>','nomeMatre<?php echo $idEquipe; ?>','matreAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeMatre<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="matreAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="matreAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastMatre<?php echo $idEquipe; ?>" id="h_seqCadastMatre<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeMatre; ?>" name="h_nomeMatre<?php echo $idEquipe; ?>" id="h_nomeMatre<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompMatre; ?>" name="h_seqCadastCompanheiroMatre<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroMatre<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompMatre; ?>" name="h_nomeCompanheiroMatre<?php echo $idEquipe; ?>" id="h_nomeCompanheiroMatre<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['grandeSacerdotisaAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoGrandeSacerdotisa         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeGrandeSacerdotisa                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompGrandeSacerdotisa        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompGrandeSacerdotisa             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompGrandeSacerdotisa        = "";
                                    $nomeCompGrandeSacerdotisa             = "";
                                }
                            } else {
                                $codAfiliacaoGrandeSacerdotisa         = "";
                                $nomeGrandeSacerdotisa                 = "";
                                $seqCadastCompGrandeSacerdotisa        = "";
                                $nomeCompGrandeSacerdotisa             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Grande Sacerdotisa: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoGrandeSacerdotisa<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoGrandeSacerdotisa; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeGrandeSacerdotisa<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeGrandeSacerdotisa; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGrandeSacerdotisa<?php echo $idEquipe; ?>','nomeGrandeSacerdotisa<?php echo $idEquipe; ?>','codAfiliacaoGrandeSacerdotisa<?php echo $idEquipe; ?>','nomeGrandeSacerdotisa<?php echo $idEquipe; ?>','grandeSacerdotisaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeGrandeSacerdotisa<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="grandeSacerdotisaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="grandeSacerdotisaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastGrandeSacerdotisa<?php echo $idEquipe; ?>" id="h_seqCadastGrandeSacerdotisa<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeGrandeSacerdotisa; ?>" name="h_nomeGrandeSacerdotisa<?php echo $idEquipe; ?>" id="h_nomeGrandeSacerdotisa<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompGrandeSacerdotisa; ?>" name="h_seqCadastCompanheiroGrandeSacerdotisa<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroGrandeSacerdotisa<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompGrandeSacerdotisa; ?>" name="h_nomeCompanheiroGrandeSacerdotisa<?php echo $idEquipe; ?>" id="h_nomeCompanheiroGrandeSacerdotisa<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['guiaAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoGuia         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeGuia                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompGuia        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompGuia             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompGuia        = "";
                                    $nomeCompGuia             = "";
                                }
                            } else {
                                $codAfiliacaoGuia         = "";
                                $nomeGuia                 = "";
                                $seqCadastCompGuia        = "";
                                $nomeCompGuia             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Guia: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoGuia<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoGuia; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeGuia<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeGuia; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGuia<?php echo $idEquipe; ?>','nomeGuia<?php echo $idEquipe; ?>','codAfiliacaoGuia<?php echo $idEquipe; ?>','nomeGuia<?php echo $idEquipe; ?>','guiaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeGuia<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="guiaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="guiaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastGuia<?php echo $idEquipe; ?>" id="h_seqCadastGuia<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeGuia; ?>" name="h_nomeGuia<?php echo $idEquipe; ?>" id="h_nomeGuia<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompGuia; ?>" name="h_seqCadastCompanheiroGuia<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroGuia<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompGuia; ?>" name="h_nomeCompanheiroGuia<?php echo $idEquipe; ?>" id="h_nomeCompanheiroGuia<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['guardiaoInternoAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoGuardiaoInterno         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeGuardiaoInterno                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompGuardiaoInterno        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompGuardiaoInterno             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompGuardiaoInterno        = "";
                                    $nomeCompGuardiaoInterno             = "";
                                }
                            } else {
                                $codAfiliacaoGuardiaoInterno         = "";
                                $nomeGuardiaoInterno                 = "";
                                $seqCadastCompGuardiaoInterno        = "";
                                $nomeCompGuardiaoInterno             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Guardião Interno: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoGuardiaoInterno<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoGuardiaoInterno; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeGuardiaoInterno<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeGuardiaoInterno; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGuardiaoInterno<?php echo $idEquipe; ?>','nomeGuardiaoInterno<?php echo $idEquipe; ?>','codAfiliacaoGuardiaoInterno<?php echo $idEquipe; ?>','nomeGuardiaoInterno<?php echo $idEquipe; ?>','guardiaoInternoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeGuardiaoInterno<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="guardiaoInternoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="guardiaoInternoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastGuardiaoInterno<?php echo $idEquipe; ?>" id="h_seqCadastGuardiaoInterno<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeGuardiaoInterno; ?>" name="h_nomeGuardiaoInterno<?php echo $idEquipe; ?>" id="h_nomeGuardiaoInterno<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompGuardiaoInterno; ?>" name="h_seqCadastCompanheiroGuardiaoInterno<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroGuardiaoInterno<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompGuardiaoInterno; ?>" name="h_nomeCompanheiroGuardiaoInterno<?php echo $idEquipe; ?>" id="h_nomeCompanheiroGuardiaoInterno<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['guardiaoExternoAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoGuardiaoExterno         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeGuardiaoExterno                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompGuardiaoExterno        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompGuardiaoExterno             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompGuardiaoExterno        = "";
                                    $nomeCompGuardiaoExterno             = "";
                                }
                            } else {
                                $codAfiliacaoGuardiaoExterno         = "";
                                $nomeGuardiaoExterno                 = "";
                                $seqCadastCompGuardiaoExterno        = "";
                                $nomeCompGuardiaoExterno             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Guardião Externo: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoGuardiaoExterno<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoGuardiaoExterno; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeGuardiaoExterno<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeGuardiaoExterno; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoGuardiaoExterno<?php echo $idEquipe; ?>','nomeGuardiaoExterno<?php echo $idEquipe; ?>','codAfiliacaoGuardiaoExterno<?php echo $idEquipe; ?>','nomeGuardiaoExterno<?php echo $idEquipe; ?>','guardiaoExternoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeGuardiaoExterno<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="guardiaoExternoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="guardiaoExternoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastGuardiaoExterno<?php echo $idEquipe; ?>" id="h_seqCadastGuardiaoExterno<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeGuardiaoExterno; ?>" name="h_nomeGuardiaoExterno<?php echo $idEquipe; ?>" id="h_nomeGuardiaoExterno<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompGuardiaoExterno; ?>" name="h_seqCadastCompanheiroGuardiaoExterno<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroGuardiaoExterno<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompGuardiaoExterno; ?>" name="h_nomeCompanheiroGuardiaoExterno<?php echo $idEquipe; ?>" id="h_nomeCompanheiroGuardiaoExterno<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['archoteAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoPortadorArchote         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomePortadorArchote                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompPortadorArchote        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompPortadorArchote             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompPortadorArchote        = "";
                                    $nomeCompPortadorArchote             = "";
                                }
                            } else {
                                $codAfiliacaoPortadorArchote         = "";
                                $nomePortadorArchote                 = "";
                                $seqCadastCompPortadorArchote        = "";
                                $nomeCompPortadorArchote             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Portador do Archote: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoPortadorArchote<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoPortadorArchote; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomePortadorArchote<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomePortadorArchote; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoPortadorArchote<?php echo $idEquipe; ?>','nomePortadorArchote<?php echo $idEquipe; ?>','codAfiliacaoPortadorArchote<?php echo $idEquipe; ?>','nomePortadorArchote<?php echo $idEquipe; ?>','portadorArchoteAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomePortadorArchote<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="portadorArchoteAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="portadorArchoteAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastPortadorArchote<?php echo $idEquipe; ?>" id="h_seqCadastPortadorArchote<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomePortadorArchote; ?>" name="h_nomePortadorArchote<?php echo $idEquipe; ?>" id="h_nomePortadorArchote<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompPortadorArchote; ?>" name="h_seqCadastCompanheiroPortadorArchote<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroPortadorArchote<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompPortadorArchote; ?>" name="h_nomeCompanheiroPortadorArchote<?php echo $idEquipe; ?>" id="h_nomeCompanheiroPortadorArchote<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['medalhistaAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoMedalhista         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeMedalhista                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompMedalhista        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompMedalhista             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompMedalhista        = "";
                                    $nomeCompMedalhista             = "";
                                }
                            } else {
                                $codAfiliacaoMedalhista         = "";
                                $nomeMedalhista                 = "";
                                $seqCadastCompMedalhista        = "";
                                $nomeCompMedalhista             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Medalhista: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoMedalhista<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoMedalhista; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeMedalhista<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeMedalhista; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoMedalhista<?php echo $idEquipe; ?>','nomeMedalhista<?php echo $idEquipe; ?>','codAfiliacaoMedalhista<?php echo $idEquipe; ?>','nomeMedalhista<?php echo $idEquipe; ?>','medalhistaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeMedalhista<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="medalhistaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="medalhistaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastMedalhista<?php echo $idEquipe; ?>" id="h_seqCadastMedalhista<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeMedalhista; ?>" name="h_nomeMedalhista<?php echo $idEquipe; ?>" id="h_nomeMedalhista<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompMedalhista; ?>" name="h_seqCadastCompanheiroMedalhista<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroMedalhista<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompMedalhista; ?>" name="h_nomeCompanheiroMedalhista<?php echo $idEquipe; ?>" id="h_nomeCompanheiroMedalhista<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['arautoAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoArauto         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeArauto                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompArauto        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompArauto             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompArauto        = "";
                                    $nomeCompArauto             = "";
                                }
                            } else {
                                $codAfiliacaoArauto         = "";
                                $nomeArauto                 = "";
                                $seqCadastCompArauto        = "";
                                $nomeCompArauto             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Arauto: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoArauto<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoArauto; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeArauto<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeArauto; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoArauto<?php echo $idEquipe; ?>','nomeArauto<?php echo $idEquipe; ?>','codAfiliacaoArauto<?php echo $idEquipe; ?>','nomeArauto<?php echo $idEquipe; ?>','arautoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeArauto<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="arautoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="arautoAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastArauto<?php echo $idEquipe; ?>" id="h_seqCadastArauto<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeArauto; ?>" name="h_nomeArauto<?php echo $idEquipe; ?>" id="h_nomeArauto<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompArauto; ?>" name="h_seqCadastCompanheiroArauto<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroArauto<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompArauto; ?>" name="h_nomeCompanheiroArauto<?php echo $idEquipe; ?>" id="h_nomeCompanheiroArauto<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['adjutorAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoAdjutor         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeAdjutor                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompAdjutor        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompAdjutor             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompAdjutor        = "";
                                    $nomeCompAdjutor             = "";
                                }
                            } else {
                                $codAfiliacaoAdjutor         = "";
                                $nomeAdjutor                 = "";
                                $seqCadastCompAdjutor        = "";
                                $nomeCompAdjutor             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Adjutor: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoAdjutor<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoAdjutor; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeAdjutor<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeAdjutor; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoAdjutor<?php echo $idEquipe; ?>','nomeAdjutor<?php echo $idEquipe; ?>','codAfiliacaoAdjutor<?php echo $idEquipe; ?>','nomeAdjutor<?php echo $idEquipe; ?>','adjutorAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeAdjutor<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="adjutorAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="adjutorAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastAdjutor<?php echo $idEquipe; ?>" id="h_seqCadastAdjutor<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeAdjutor; ?>" name="h_nomeAdjutor<?php echo $idEquipe; ?>" id="h_nomeAdjutor<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompAdjutor; ?>" name="h_seqCadastCompanheiroAdjutor<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroAdjutor<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompAdjutor; ?>" name="h_nomeCompanheiroAdjutor<?php echo $idEquipe; ?>" id="h_nomeCompanheiroAdjutor<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <?php
                            $seqCadast = $vetor['sonoplastaAtividadeIniciaticaOficial'];
                            include './js/ajax/retornaDadosMembroPorSeqCadast.php';
                            $obj2 = json_decode(json_encode($return),true);
                            if($obj2['result'][0]['fields']['fSeqCadast'] != 0){
                                $codAfiliacaoSonoplasta         = $obj2['result'][0]['fields']['fCodRosacruz'];
                                $nomeSonoplasta                 = $obj2['result'][0]['fields']['fNomCliente'];
                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'] != 0){
                                    $seqCadastCompSonoplasta        = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                    $nomeCompSonoplasta             = $obj2['result'][0]['fields']['fNomConjuge'];
                                } else {
                                    $seqCadastCompSonoplasta        = "";
                                    $nomeCompSonoplasta             = "";
                                }
                            } else {
                                $codAfiliacaoSonoplasta         = "";
                                $nomeSonoplasta                 = "";
                                $seqCadastCompSonoplasta        = "";
                                $nomeCompSonoplasta             = "";
                            }
                            ?>
                            <div class="row">
                                <div class="form-group form-inline">
                                    <label class="col-sm-12 control-label">Sonoplasta: </label>
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoSonoplasta<?php echo $idEquipe; ?>" maxlength="7" type="text" value="<?php echo $codAfiliacaoSonoplasta; ?>" style="max-width: 76px">
                                        <input placeholder="Nome completo" class="form-control" id="nomeSonoplasta<?php echo $idEquipe; ?>" type="text" maxlength="100" value="<?php echo $nomeSonoplasta; ?>" style="min-width: 320px">
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModalPesquisa" onclick="pesquisaFonetica('resultadoPesquisa','codAfiliacaoSonoplasta<?php echo $idEquipe; ?>','nomeSonoplasta<?php echo $idEquipe; ?>','codAfiliacaoSonoplasta<?php echo $idEquipe; ?>','nomeSonoplasta<?php echo $idEquipe; ?>','sonoplastaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>','h_nomeSonoplasta<?php echo $idEquipe; ?>','myModalPesquisa','informacoesPesquisa');">
                                            <icon class="fa fa-search"></icon>
                                        </a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                    </div>
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" maxlength="50" name="sonoplastaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" id="sonoplastaAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" class="form-control" style="max-width: 320px">
                                    <input type="hidden" value="<?php echo $seqCadast; ?>" name="h_seqCadastSonoplasta<?php echo $idEquipe; ?>" id="h_seqCadastSonoplasta<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeSonoplasta; ?>" name="h_nomeSonoplasta<?php echo $idEquipe; ?>" id="h_nomeSonoplasta<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $seqCadastCompSonoplasta; ?>" name="h_seqCadastCompanheiroSonoplasta<?php echo $idEquipe; ?>" id="h_seqCadastCompanheiroSonoplasta<?php echo $idEquipe; ?>">
                                    <input type="hidden" value="<?php echo $nomeCompSonoplasta; ?>" name="h_nomeCompanheiroSonoplasta<?php echo $idEquipe; ?>" id="h_nomeCompanheiroSonoplasta<?php echo $idEquipe; ?>">
                                </div>
                            </div>
                            <?php $seqCadast = 0; ?>
                            <hr>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-12 control-label">Anotações: </label>
                                    <div class="col-sm-12">
                                        <div style="border: #ccc solid 1px; max-width: 800px">
                                            <div class="mail-text h-200">
                                                <textarea class="summernote" id="anotacoesAtividadeIniciaticaOficial<?php echo $idEquipe; ?>" name="anotacoesAtividadeIniciaticaOficial<?php echo $idEquipe; ?>"><?php echo $vetor['anotacoesAtividadeIniciaticaOficial']; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="idAtividadeIniciaticaOficial" name="idAtividadeIniciaticaOficial" value="<?php echo $idEquipe ?>">
                            <input type="hidden" id="fk_idOrganismo" name="fk_idOrganismo" value="<?php echo $idOrganismo ?>">
                            <div id="buttonsDevelopment<?php echo $idEquipe;?>">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }
}
?>
<div class="modal inmodal fade" id="myModalPesquisa" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width: 900px;">
        <div class="modal-content">
            <div class="modal-body">
                <span id="resultadoPesquisa">
                </span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>        
<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->