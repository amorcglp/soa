<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Público salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Público</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Público</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Públicos Cadastrados</h5>
                    <div class="ibox-tools">
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroPublico">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Público</th>
                                <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/publicoController.php");

                            $pc = new publicoController();
                            $resultado = $pc->listaPublico();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['idPublico']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['publico']; ?>
                                        </td>
                                        
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                    <?php if(in_array("2",$arrNivelUsuario)){?>
                                        <a class="btn btn-sm btn-info" href="painelDeControle.php?corpo=alteraPublico&id=<?php echo $vetor['idPublico'] ?>" data-rel="tooltip" title="">
                                            <i class="icon-edit icon-white"></i>  
                                            Editar                                            
                                        </a>
                                    <?php }?>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

