<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("model/planoAcaoOrganismoMetaClass.php");
include_once("model/planoAcaoRegiaoMetaClass.php");
include_once("lib/functions.php");
include_once("model/organismoClass.php");
include_once('model/regiaoRosacruzClass.php');

$o = new organismo();
$reg = new regiaoRosacruz();
$parm = new planoAcaoOrganismoMeta();

$regiao = substr($sessao->getValue("siglaOA"),0,3);

$reg->setRegiaoRosacruz($regiao);
$resultado = $reg->buscaRegiaoRosacruzEquals();
if($resultado)
{
	foreach($resultado as $vetor)
	{
		$idRegiao = $vetor['idRegiaoRosacruz'];
	}
}

$resultado = $o->listaOrganismo(null, $sessao->getValue("siglaOA"));

if ($resultado) {
	foreach ($resultado as $vetor) {

		$nomeOrganismo = organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'], $vetor['tipoOrganismoAfiliado'], $vetor["nomeOrganismoAfiliado"], $vetor["siglaOrganismoAfiliado"]);
	}
}

/*
 * Gráfico do OA
 */
$anos = date('Y')+1;
$arrAlcancadas=array();
for($i=2014;$i<=$anos;$i++)
{
	$resultadoMeta = $parm->listaMetaOrganismo($idOrganismoAfiliado,1,$i);
	if($resultadoMeta)
	{
    	$arrAlcancadas['total'][$i] = count($resultadoMeta);
	}else{
		$arrAlcancadas['total'][$i] = 0;
	}
}

$arrNaoAlcancadas=array();
for($i=2014;$i<=$anos;$i++)
{
	$resultadoMeta = $parm->listaMetaOrganismo($idOrganismoAfiliado,0,$i,1);
	if($resultadoMeta)
	{
    	$arrNaoAlcancadas['total'][$i] = count($resultadoMeta);
	}else{
		$arrNaoAlcancadas['total'][$i] = 0;
	}
}

/*
 * Gráfico da Região
 */
$parm = new planoAcaoRegiaoMeta();
$anos = date('Y')+1;
$arrAlcancadasRegiao=array();
for($i=2013;$i<=$anos;$i++)
{
	$resultadoMeta = $parm->listaMetaRegiao($idRegiao,1,$i);
	if($resultadoMeta)
	{
    	$arrAlcancadasRegiao['total'][$i] = count($resultadoMeta);
	}else{
		$arrAlcancadasRegiao['total'][$i] = 0;
	}
}

$arrNaoAlcancadasRegiao=array();
for($i=2013;$i<=$anos;$i++)
{
	$resultadoMeta = $parm->listaMetaRegiao($idRegiao,0,$i,1);
	if($resultadoMeta)
	{
    	$arrNaoAlcancadasRegiao['total'][$i] = count($resultadoMeta);
	}else{
		$arrNaoAlcancadasRegiao['total'][$i] = 0;
	}
}

?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Anos');
      data.addColumn('number', 'Alcançadas');
      data.addColumn('number', 'Não Alcançadas');

      data.addRows([
    <?php 
    	for($i=2014;$i<=(date('Y')+1);$i++)
		{
    	?>                
	    <?php if($i==2014){?>                
	        ['<?php echo $i;?>',<?php echo $arrAlcancadas['total'][$i];?>,<?php echo $arrNaoAlcancadas['total'][$i];?>]
	    <?php }else{?>    
	        ,['<?php echo $i;?>',<?php echo $arrAlcancadas['total'][$i];?>,<?php echo $arrNaoAlcancadas['total'][$i];?>]
	    <?php }
	    ?>
    <?php }?>
      ]);

      var options = {
    	        hAxis: {
    	          title: 'Anos'
    	        },
    	        vAxis: {
    	          title: 'Total de Metas'
    	        }
    	      };

    	      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

    	      chart.draw(data, options);
    	    }
  </script>
  <script type="text/javascript">
google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Anos');
      data.addColumn('number', 'Alcançadas');
      data.addColumn('number', 'Não Alcançadas');

      data.addRows([
    <?php 
    	for($i=2013;$i<=(date('Y')+1);$i++)
		{
    	?>                
	    <?php if($i==2013){?>                
	        ['<?php echo $i;?>',<?php echo $arrAlcancadasRegiao['total'][$i];?>,<?php echo $arrNaoAlcancadasRegiao['total'][$i];?>]
	    <?php }else{?>    
	        ,['<?php echo $i;?>',<?php echo $arrAlcancadasRegiao['total'][$i];?>,<?php echo $arrNaoAlcancadasRegiao['total'][$i];?>]
	    <?php }
	    ?>
    <?php }?>
      ]);

      var options = {
    	        hAxis: {
    	          title: 'Anos'
    	        },
    	        vAxis: {
    	          title: 'Total de Metas'
    	        }
    	      };

    	      var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));

    	      chart.draw(data, options);
    	    }
  </script>


<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Metas</h2>
		<ol class="breadcrumb">
			<li><a href="index.html">Home</a>
			</li>
			<li><a>Estatísticas</a>
			</li>
			<li class="active"><strong>Metas</strong>
			</li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Metas</h5>
					</div>
					<div class="wrapper wrapper-content animated fadeInRight">
						<div class="row">
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Gráfico Metas Alcançadas x Metas Não Alcançadas do Organismo <?php echo $nomeOrganismo;?></h5>
									</div>
									<div class="ibox-content">
										<div class="row">
											<div id="chart_div"></div>
										</div>
									</div>
									<br><br>
									<div class="ibox-title">
										<h5>Gráfico Metas Alcançadas x Metas Não Alcançadas da Região <?php echo strtoupper($regiao);?></h5>
									</div>
									<div class="ibox-content">
										<div class="row">
											<div id="chart_div2"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


