<?php
include_once("controller/atividadeEstatutoController.php");
require_once ("model/atividadeEstatutoClass.php");

$idAtividadeEstatuto	= isset($_GET['idAtividadeEstatuto']) ? json_decode($_GET['idAtividadeEstatuto']) : '';

if($idAtividadeEstatuto==''){
    echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaAtividadeEstatuto';</script>";
}

$aec = new atividadeEstatutoController();
$dados = $aec->buscaAtividadeEstatuto($idAtividadeEstatuto);
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeEstatuto">Atividades</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Atividades</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Tipos de Atividade</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeEstatuto">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" onsubmit="return verificaCamposPreenchidosAtividadeSubmit();" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Atividade: </label>
                            <div class="col-sm-10">
                                <p class="form-control-static">
                                    <?php
                                    include_once("controller/atividadeEstatutoTipoController.php");
                                    $aetc    = new atividadeEstatutoTipoController();
                                    $dadosTipoAtividade = $aetc->buscaAtividadeEstatutoTipo($dados->getFkIdTipoAtividadeEstatuto());
                                    echo $dadosTipoAtividade->getNomeTipoAtividadeEstatuto();
                                    ?>
                                </p>
                                <input type="hidden" id="janeiro" value="<?php echo $dadosTipoAtividade->getJaneiroTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="fevereiro" value="<?php echo $dadosTipoAtividade->getFevereiroTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="marco" value="<?php echo $dadosTipoAtividade->getMarcoTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="abril" value="<?php echo $dadosTipoAtividade->getAbrilTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="maio" value="<?php echo $dadosTipoAtividade->getMaioTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="junho" value="<?php echo $dadosTipoAtividade->getJunhoTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="julho" value="<?php echo $dadosTipoAtividade->getJulhoTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="agosto" value="<?php echo $dadosTipoAtividade->getAgostoTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="setembro" value="<?php echo $dadosTipoAtividade->getSetembroTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="outubro" value="<?php echo $dadosTipoAtividade->getOutubroTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="novembro" value="<?php echo $dadosTipoAtividade->getNovembroTipoAtividadeEstatuto(); ?>">
                                <input type="hidden" id="dezembro" value="<?php echo $dadosTipoAtividade->getDezembroTipoAtividadeEstatuto(); ?>">
                            </div>
                        </div>
                        <input type="hidden" id="fk_idTipoAtividadeEstatuto" name="fk_idTipoAtividadeEstatuto" value="<?php echo $dados->getFkIdTipoAtividadeEstatuto(); ?>">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                Complemento ao Nome: <br>
                                <small class="text-navy">
                                    Preenchimento não obrigatório.
                                </small>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="complementoNomeAtividadeEstatuto" id="complementoNomeAtividadeEstatuto" value="<?php echo $dados->getComplementoNomeAtividadeEstatuto();?>" class="form-control" maxlength="60" style="max-width: 320px">
                                <p>
                                    (ideal para diferenciar atividades ou informar qual atividade do calendário anual está sendo cadastrado)
                                </p>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_anotacoes">
                            <label class="col-sm-2 control-label">Data</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAtividadeEstatuto" onkeypress="valida_data(this)" maxlength="10" id="dataAtividadeEstatuto" value="<?php echo $dados->getDataAtividadeEstatuto() ;?>" type="text" class="form-control" style="max-width: 105px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Hora: </label>
                            <div class="col-sm-10">
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                    <input type="text" class="form-control" id="horaAtividadeEstatuto" name="horaAtividadeEstatuto" value="<?php echo $dados->getHoraAtividadeEstatuto() ;?>" value="00:00" style="max-width: 70px">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Local: </label>
                            <div class="col-sm-10">
                                <input type="text" name="localAtividadeEstatuto" id="localAtividadeEstatuto" value="<?php echo $dados->getLocalAtividadeEstatuto();?>" class="form-control" maxlength="60" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título do Discurso: </label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloDiscursoAtividadeEstatuto" id="tituloDiscursoAtividadeEstatuto" value="<?php echo $dados->getTituloDiscursoAtividadeEstatuto();?>" class="form-control" maxlength="100" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Orador: </label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeOradorDiscursoAtividadeEstatuto" id="nomeOradorDiscursoAtividadeEstatuto" value="<?php echo $dados->getNomeOradorDiscursoAtividadeEstatuto();?>" class="form-control" maxlength="100" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; max-width: 850px">
	                            	<div class="mail-text h-200">
										<textarea class="summernote" id="descricaoAtividadeEstatuto" name="descricaoAtividadeEstatuto"><?php echo $dados->getDescricaoAtividadeEstatuto() ;?></textarea>
									</div>
								</div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast"); ?>">
                                <input type="hidden" id="idAtividadeEstatuto" name="idAtividadeEstatuto" value="<?php echo $idAtividadeEstatuto; ?>">
                                <input type="hidden" id="fk_idOrganismoAfiliado" name="fk_idOrganismoAfiliado" value="<?php echo $idOrganismoAfiliado; ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <a class="btn btn-white" href="?corpo=buscaAtividadeEstatuto"> Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->



<!-- Input Mask-->
<!--<script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>-->

<!-- Data picker -->
    <!--<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>-->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>