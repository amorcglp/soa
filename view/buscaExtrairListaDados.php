<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/usuarioController.php");
$um     = new Usuario();
require_once ("controller/organismoController.php");
$oc     = new organismoController();

?>
<?php
$gerouLista = isset($_REQUEST['gerouLista']) ? $_REQUEST['gerouLista'] : null;
if ($gerouLista == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({title: "Sucesso!", text: "Lista gerada com sucesso!", type: "success", confirmButtonColor: "#1ab394"});
        }
    </script>
<?php } ?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Extração de Lista de Dados</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Consultas</a>
            </li>
            <li class="active">
                <a href="buscaExtrairListaDados.php">Extração de Lista de Dados</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Extração de Lista de Dados</h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Organismo</th>
		                        <th><center>Extrair Lista</center></th>
                                        <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                            <th><center>Notificar</center></th>
                                        <?php } ?>
		                    </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("model/organismoClass.php");
                            include_once("model/relatorioFinanceiroAnualClass.php");
							include_once("lib/functions.php");
							
                            $o = new organismo();
                            
                            $resultado = $o->listaOrganismo();
                            

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                                    
                                    if ((strtoupper($vetor["siglaOrganismoAfiliado"]) == strtoupper($sessao->getValue("siglaOA"))) || (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2))) { 
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                            </td>
                                            <td>
                                                <center>

                                                                    <a href="#" 
                                                                    onClick="document.getElementById('siglaOA').value='<?php echo $vetor["siglaOrganismoAfiliado"];?>'"
                                                                    data-target="#mySeeListaDownload" data-toggle="modal"
                                                                    target="_blank">
                                                                            <button class="btn btn-info  dim" type="button"><i class="fa fa-download"></i> </button>
                                                                    </a>
                                                </center>
                                            </td>
                                            <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                            <td>
                                                    <center>

                                                                <a class="btn btn-sm btn-danger" href="#" onclick="abreModalNotificacao(<?php echo $vetor['idOrganismoAfiliado'];?>,null)">
                                                                    <i class="fa fa-envelope icon-white"></i>
                                                                    Notificar
                                                                </a>
                                            </center>
                                            </td>
                                            <?php } ?>
                                        </tr>
                            <?php
                                    }
                                
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<div class="modal inmodal" id="mySeeListaDownload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-download modal-icon"></i>
                <h4 class="modal-title">Download da Lista de Dados dos Membros Ativos</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
                                            <table width="100%">
                                                <tr>
                                                    <td align="center">
                                                <div style="text-align: justify!important;width: 150px">    
                                                    <input type="checkbox" checked="checked" onchange="marcardesmarcar();"> <b>Todos</b><br>
                                                    <input type="checkbox" name="linhaZero" id="linhaZero" checked="checked" class="marcar"> Código de Afiliação<br>
                                                    <input type="checkbox" name="linha1" id="linha1" checked="checked" class="marcar"> Nome Completo<br>
                                                    <input type="checkbox" name="linha2" id="linha2" checked="checked" class="marcar"> Telefone Residencial<br>
                                                    <input type="checkbox" name="linha3" id="linha3" checked="checked" class="marcar"> Telefone Comercial<br>
                                                    <input type="checkbox" name="linha4" id="linha4" checked="checked" class="marcar"> Celular<br>
                                                    <input type="checkbox" name="linha5" id="linha5" checked="checked" class="marcar"> E-mail<br>
                                                    <input type="checkbox" name="linha6" id="linha6" checked="checked" class="marcar"> Endereço<br>
                                                    <input type="checkbox" name="linha7" id="linha7" checked="checked" class="marcar"> Cidade<br>
                                                    <input type="checkbox" name="linha8" id="linha8" checked="checked" class="marcar"> Estado<br>
                                                    <input type="checkbox" name="linha9" id="linha9" checked="checked" class="marcar"> Pais<br>
                                                    <input type="checkbox" name="linha10" id="linha10" checked="checked" class="marcar"> Código Postal<br>
                                                    <input type="checkbox" name="linha11" id="linha11" checked="checked" class="marcar"> Grau<br>
                                                    <input type="checkbox" name="linha12" id="linha12" checked="checked" class="marcar"> Lote<br>
                                                    <input type="checkbox" name="linha13" id="linha13" checked="checked" class="marcar"> Sexo<br>
                                                    <input type="checkbox" name="linha14" id="linha14" checked="checked" class="marcar"> Data de Nasc.<br>
                                                </div>
                                                    </td>     
                                                </tr>
                                            </table>
					</div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="siglaOA" id="siglaOA">
                <button type="button" class="btn btn-primary" onclick="downloadListaDados();"><i class="fa fa-download"></i> Download (.xls)</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php 
modalNotificacao($sessao->getValue("seqCadast"));
?>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
