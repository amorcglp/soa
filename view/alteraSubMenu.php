
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Submenus</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaSubMenu">Submenu</a>
            </li>
            <li class="active">
                <a>Edição de Submenu</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição Submenu</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaSubMenu">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    include_once("controller/subMenuController.php");

                    $sm = new subMenuController();
                    $dados = $sm->buscaSubMenu($_GET['id']);
                    ?>
                    <form id="cadastro" method="POST" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idSubMenu" id="idSubMenu" value="<?php echo $_GET['id']; ?>" />
                        <div class="form-group"><label class="col-sm-2 control-label">*Seção</label>
                            <div class="col-sm-10"><input type="text" name="nomeSubMenu" id="nomeSubMenu" class="form-control" value="<?php echo $dados->getNomeSubMenu(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Seção</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idSecaoMenu" id="fk_idSecaoMenu" style="max-width: 150px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'controller/secaoMenuController.php';
                                    $sm = new secaoMenuController();
                                    $sm->criarComboBox($dados->getFkIdSecaoMenu());
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Arquivo</label>
                            <div class="col-sm-10"><input type="text" name="arquivoSubMenu" id="arquivoSubMenu" class="form-control" value="<?php echo $dados->getArquivoSubMenu(); ?>" style="max-width: 320px" required="required" >***Colocar "blank" para colocar em manutenção a opção</div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Prioridade</label>
                            <div class="col-sm-10"><input type="text" name="prioridadeSubMenu" id="prioridadeSubMenu" class="form-control" value="<?php echo $dados->getPrioridadeSubMenu(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Icone</label>
                            <div class="col-sm-10"><input type="text" name="iconeSubMenu" id="iconeSubMenu" class="form-control" value="<?php echo $dados->getIconeSubMenu(); ?>" style="max-width: 320px" required="required" ></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea rows="7" name="descricaoSubMenu" id="descricaoSubMenu" class="form-control" style="max-width: 500px" required="required"><?php echo $dados->getDescricaoSubMenu(); ?></textarea></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Mensagem de manutenção</label>
                            <div class="col-sm-10"><textarea rows="7" name="msgManutencao" id="msgManutencao" class="form-control" style="max-width: 500px"><?php echo $dados->getMsgManutencao(); ?></textarea></div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->implode(Registro());
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

