
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Sub-menu salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Sub-menus</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaSubMenu">Sub-menus</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Sub-menus Cadastrados</h5>
                    <div class="ibox-tools">
                    <?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroSubMenu">
                            <i class="fa fa-plus"></i> Cadastrar Novo
                        </a>
                    <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th style="min-width: 150px">Sub-menu</th>
                                <th style="min-width: 150px">Seção</th>
                                <th style="min-width: 150px">Arquivo</th>
                                <th style="min-width: 150px">Ícone</th>
                                <th style="min-width: 150px">Mensagem de Manuteção</th>
                                <th style="min-width: 150px">Status</th>
                                <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/subMenuController.php");

                            $smc = new subMenuController();
                            $resultado = $smc->listaSubMenu();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['nomeSubMenu']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['nomeSecaoMenu']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['arquivoSubMenu']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['iconeSubMenu']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['msgManutencao']; ?>
                                        </td>
                                        <td>
                                <center>
                                    <div id="statusTarget<?php echo $vetor['idSubMenu'] ?>">
                                        <?php
                                        switch ($vetor['statusSubMenu']) {
                                            case 0:
                                                echo "<span class='badge badge-primary'>Ativo</span>";
                                                break;
                                            case 1:
                                                echo "<span class='badge badge-danger'>Inativo</span>";
                                                break;
                                        }
                                        ?>
                                    </div>
                                </center>
                                </td>
                                <td><center>
                                    <div id="acoes_confirma_cancela">
                                    	<?php if(in_array("2",$arrNivelUsuario)){?>
                                        <a class="btn btn-sm btn-info" href="painelDeControle.php?corpo=alteraSubMenu&id=<?php echo $vetor['idSubMenu'] ?>" data-rel="tooltip" title="">
                                            <i class="icon-edit icon-white"></i>  
                                            Editar                                            
                                        </a>
                                        <?php }?>
                                        <?php if(in_array("3",$arrNivelUsuario)){?>
                                        <span id="status<?php echo $vetor['idSubMenu'] ?>">
        <?php if ($vetor['statusSubMenu'] == 1) { ?>
                                                <a class="btn btn-sm btn-primary" href="#" onclick="mudaStatus('<?php echo $vetor['idSubMenu'] ?>', '<?php echo $vetor['statusSubMenu'] ?>', 'subMenu')" data-rel="tooltip" title="Ativar"> 
                                                    Ativar                                            
                                                </a>
        <?php } else { ?>
                                                <a class="btn btn-sm btn-danger" href="#" onclick="mudaStatus('<?php echo $vetor['idSubMenu'] ?>', '<?php echo $vetor['statusSubMenu'] ?>', 'subMenu')" data-rel="tooltip" title="Inativar">
                                                    Inativar                                            
                                                </a>
        <?php } ?>
                                        </span>
                                        <?php }?>
                                    </div>
                                </center>
                                </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<script>
    $(document).ready(function () {
        $('.dataTables-example').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
            }
        });

        /* Init DataTables */
        var oTable = $('#editable').dataTable();

        /* Apply the jEditable handlers to the table */
        oTable.$('td').editable('../example_ajax.php', {
            "callback": function (sValue, y) {
                var aPos = oTable.fnGetPosition(this);
                oTable.fnUpdate(sValue, aPos[0], aPos[1]);
            },
            "submitdata": function (value, settings) {
                return {
                    "row_id": this.parentNode.getAttribute('id'),
                    "column": oTable.fnGetPosition(this)[2]
                };
            },
            "width": "90%",
            "height": "100%"
        });


    });

    function fnClickAddRow() {
        $('#editable').dataTable().fnAddData([
            "Custom row",
            "New row",
            "New row",
            "New row",
            "New row"]);

    }
</script>
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
				