<link href="css/plugins/cropper/cropper.min.css" rel="stylesheet">

<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/perfilUsuarioController.php");
require_once ("model/perfilUsuarioClass.php");
require_once ("model/criaSessaoClass.php");

$pc = new perfilController();
$sessao = new criaSessao();
$seqCadastUsuario = $sessao->getValue("seqCadast");
$dados = $pc->buscaUsuario($sessao->getValue("seqCadast"));
$avatar = $pc->listaAvatarUsuario($sessao->getValue("seqCadast"));
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            Contatos de <?php echo $dados->getNomeUsuario(); ?>
        </h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <strong>
                    Contatos
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Todos os contatos</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-primary" href="?corpo=perfilUsuario">
                            <i class="fa fa-mail-forward"></i> Ir para a página de perfil
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row" style="margin-bottom: 20px">
                        <div class="col-sm-offset-6 col-sm-6">
                            <input type="text" id="pesquisar" name="pesquisar" class="form-control" placeholder="Pesquisar contato">
                        </div>
                    </div>
                    <div class="row" id="conteudo"></div>
                    <div id="linha_tracejada" class="hr-line-dashed"></div>
                    <div class="text-center">
                        <div class="btn-group" id="paginador">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal's da página INICIO -->

<!-- Modal de atualização de informações pessoais -->


<!-- Modal's da página FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>