<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividade Para Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeEstatutoTipo">Tipos de Atividade</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Tipos de Atividades</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Tipos de Atividade</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeEstatutoTipo">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoCadastrar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Atividade: </label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeTipoAtividadeEstatuto" id="nomeTipoAtividadeEstatuto" class="form-control" value="" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; max-width: 850px">
	                            	<div class="mail-text h-200">
										<textarea class="summernote" id="descricaoTipoAtividadeEstatuto" name="descricaoTipoAtividadeEstatuto"></textarea>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Destinado ao OA: </label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="classiOaTipoAtividadeEstatuto" id="classiOaTipoAtividadeEstatuto" style="max-width: 250px">
                                    <option value="0">Selecione</option>
                                    <option value="1">Loja</option>
                                    <option value="2">Pronaos</option>
                                    <option value="3">Capítulo</option>
                                    <!--<option value="4">Todos</option>-->
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Disponibilidade </label>
                            <div class="col-sm-10">
                                <div class="i-checks">
                                    <input type="checkbox" value="portal" name="disponibilidade[]" class="col-lg-12"> Portal<br><br>
                                    <input type="checkbox" value="estatuto" name="disponibilidade[]" class="col-lg-12"> Acordo de Afiliação
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Quantidade Recomendada: </label>
                            <div class="col-sm-10">
                                <input type="text" name="qntTipoAtividadeEstatuto" id="qntTipoAtividadeEstatuto" class="form-control" value="" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Periodicidade de realização: </label>
                            <div class="col-sm-10">
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="janeiroAtividade" value="janeiroAtividade" name="frequencia[]" checked=""> <i></i> Janeiro </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="fevereiroAtividade" value="fevereiroAtividade" name="frequencia[]" checked=""> <i></i> Fevereiro </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="marcoAtividade" value="marcoAtividade" name="frequencia[]" checked=""> <i></i> Março </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="abrilAtividade" value="abrilAtividade" name="frequencia[]" checked=""> <i></i> Abril </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="maioAtividade" value="maioAtividade" name="frequencia[]" checked=""> <i></i> Maio </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="junhoAtividade" value="junhoAtividade" name="frequencia[]" checked=""> <i></i> Junho </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="julhoAtividade" value="julhoAtividade" name="frequencia[]" checked=""> <i></i> Julho </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="agostoAtividade" value="agostoAtividade" name="frequencia[]" checked=""> <i></i> Agosto </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="setembroAtividade" value="setembroAtividade" name="frequencia[]" checked=""> <i></i> Setembro </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="outubroAtividade" value="outubroAtividade" name="frequencia[]" checked=""> <i></i> Outubro </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="novembroAtividade" value="novembroAtividade" name="frequencia[]" checked=""> <i></i> Novembro </label>
                                </div>
                                <div class="i-checks">
                                    <label> <input type="checkbox" id="dezembroAtividade" value="dezembroAtividade" name="frequencia[]" checked=""> <i></i> Dezembro </label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <a class="btn btn-white" href="?corpo=buscaAtividadeEstatutoTipo"> Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->



<!-- Input Mask-->
<!--<script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>-->

<!-- Data picker -->
    <!--<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>-->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>