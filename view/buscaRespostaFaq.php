<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->
<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Resposta salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Respostas do FAQ</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Respostas do FAQ</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Respostas do FAQ</h5>
                    <div class="ibox-tools">
                    	<?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroRespostaFaq">
                            <i class="fa fa-plus"></i> Cadastrar Nova Resposta
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Título da Resposta</th>
                                <th><center>Resposta</center></th>
                        <th><center>Responsável pela Resposta</center></th>
                        <th><center>Data</center></th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/faqRespostaController.php");

                            $frc = new faqRespostaController();
                            $resultado = $frc->listaRespostaFaq();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['tituloRespostaFaq']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['respostaFaq']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['responsavelRespostaFaq']; ?>
                                        </td>
                                        <td>
                                            <?php
                                            $data = date_create($vetor['dataRespostaFaq']);
                                            echo date_format($data, 'd/m/Y H:i:s');
                                            ?>
                                        </td>
                                        <td>
                                <center>
                                    <div id="acoes_confirma_cancela">
                                    	<?php if(in_array("2",$arrNivelUsuario)){?>
                                        <a class="btn btn-sm btn-info" href="painelDeControle.php?corpo=alteraRespostaFaq&id=<?php echo $vetor['idRespostaFaq'] ?>" data-rel="tooltip" title="">
                                            <i class="icon-edit icon-white"></i>  
                                            Editar                                            
                                        </a>
                                        <?php }?>
                                        <?php if(in_array("3",$arrNivelUsuario)){?>
                                        <a class="btn btn-sm btn-danger" href="acoes/acaoExcluir&id=<?php echo $vetor['idRespostaFaq'] ?>" data-rel="tooltip" title="">
                                            Excluir                                            
                                        </a>
										<?php }?>
                                        </span>
                                    </div>
                                </center>
                                </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

