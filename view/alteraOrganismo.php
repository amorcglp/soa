<?php
include_once("controller/organismoController.php");
require_once ("model/organismoClass.php");
require_once ("model/cidadeClass.php");

$cid = new Cidade();

$oc = new organismoController();
$dados = $oc->buscaOrganismo($_GET['idOrganismoAfiliado']);
//echo "<pre>";print_r($dados);
//Buscar informações no webservice para pegar atualizações vindas do ORCZ
$ocultar_json=1;
$siglaOA=$dados->getSiglaOrganismoAfiliado();
switch ($dados->getPaisOrganismoAfiliado())
{
    case 1:
        $pais="BR";
        break;
    case 2:
        $pais="PT";
        break;
    case 3:
        $pais="RA";
        break;
    default :
        $pais="BR";
        break;
}
$paisDoOrganismo = $pais;
//echo $pais;
include 'js/ajax/retornaDadosOrganismo.php';
//echo "<pre>";print_r($return);


?>
<!-- Conteúdo DE INCLUDE INICIO -->
<script type="text/javascript">

$(function () {
    //Máscaras
    $("#telefoneFixoOrganismoAfiliado").mask("(999) 9999-9999");
    $("#faxOrganismoAfiliado").mask("(999) 9999-9999");
    //$("#outroTelefoneOrganismoAfiliado").mask("(999) 9999-9999");
    //$("#celularUsuario").mask("(999) 9999-9999");
    
    jQuery('input[type=tel]').mask("(999) 9999-9999?9").ready(function(event) {
    var target, phone, element;
    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
    phone = target.value.replace(/\D/g, '');
    element = $(target);
    element.unmask();
    if(phone.length > 10) {
    element.mask("(999) 99999-999?9");
    } else {
    element.mask("(999) 9999-9999?9");
    }
    });
})

</script>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaOrganismo">Organismos</a>
            </li>
            <li class="active">
                <a>Edição de Organismo</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edição de Organismo</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaOrganismo">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="post" action="acoes/acaoAlterar.php">
                    	<div class="form-group"><label class="col-sm-2 control-label">País</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" onchange="selecionaPais(this.value)" readonly name="paisOrganismoAfiliado" id="paisOrganismoAfiliado" style="max-width: 150px">
                                    <option value="" <?php if ($dados->getPaisOrganismoAfiliado() == 0) {
    echo 'selected';
}; ?>>Selecione...</option>
                                    <option value="1" <?php if ($dados->getPaisOrganismoAfiliado() == 1) {
    echo 'selected';
}; ?>>Brasil</option>
                                    <option value="2" <?php if ($dados->getPaisOrganismoAfiliado() == 2) {
    echo 'selected';
} ?>>Portugal</option>
                                    <option value="3" <?php if ($dados->getPaisOrganismoAfiliado() == 3) {
    echo 'selected';
} ?>>Angola</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Sigla</label>
                            <div class="col-sm-10"><input type="text" readonly maxlength="7" onChange="this.value = this.value.toUpperCase()" name="siglaOrganismoAfiliado" id="siglaOrganismoAfiliado" value="<?php echo $dados->getSiglaOrganismoAfiliado(); ?>" class="form-control" style="max-width: 160px"></div>
                        </div>
						<div class="form-group"><label class="col-sm-2 control-label">Código Afiliação</label>
                            <div class="col-sm-10"><input type="text" name="codigoAfiliacaoOrganismoAfiliado" readonly maxlength="7" onkeypress="return SomenteNumero(event)" value="<?php echo $dados->getCodigoAfiliacaoOrganismoAfiliado(); ?>" id="codigoAfiliacaoOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Organismo</label>
                            <div class="col-sm-10"><input type="text" readonly name="nomeOrganismoAfiliado" id="nomeOrganismoAfiliado" onChange="this.value = this.value.toUpperCase()" value="<?php echo $dados->getNomeOrganismoAfiliado(); ?>" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Região</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idRegiaoOrdemRosacruz" readonly id="fk_idRegiaoOrdemRosacruz" style="max-width: 150px">
                                    <option value="">Selecione...</option>
<?php
include_once 'controller/regiaoRosacruzController.php';
$rc = new regiaoRosaCruzController();
$rc->criarComboBox($dados->getFk_idRegiaoOrdemRosacruz());
?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Tipo</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" readonly name="tipoOrganismoAfiliado" id="tipoOrganismoAfiliado" style="max-width: 150px">
                                    <option value="0" <?php if ($dados->getTipoOrganismoAfiliado() == 0) {
    echo 'selected';
}; ?>>Selecione...</option>
                                    <option value="1" <?php if ($dados->getTipoOrganismoAfiliado() == 1) {
    echo 'selected';
}; ?>>R+C</option>
                                    <option value="2" <?php if ($dados->getTipoOrganismoAfiliado() == 2) {
    echo 'selected';
}; ?>>TOM</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Classificação</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" readonly name="classificacaoOrganismoAfiliado" id="classificacaoOrganismoAfiliado" style="max-width: 150px">
                                    <option value="0" <?php if ($dados->getClassificacaoOrganismoAfiliado() == 0) {
    echo 'selected';
}; ?>>Selecione...</option>
                                    <option value="1" <?php if ($dados->getClassificacaoOrganismoAfiliado() == 1) {
    echo 'selected';
}; ?>>Loja</option>
                                    <option value="2" <?php if ($dados->getClassificacaoOrganismoAfiliado() == 2) {
    echo 'selected';
}; ?>>Pronaos</option>
                                    <option value="3" <?php if ($dados->getClassificacaoOrganismoAfiliado() == 3) {
    echo 'selected';
}; ?>>Capítulo</option>
                                    <option value="4" <?php if ($dados->getClassificacaoOrganismoAfiliado() == 4) {
    echo 'selected';
}; ?>>Heptada</option>
                                    <option value="5" <?php if ($dados->getClassificacaoOrganismoAfiliado() == 5) {
    echo 'selected';
}; ?>>Atrium</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Atuação Temporária</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="atuacaoTemporaria" id="atuacaoTemporaria" style="max-width: 150px">
                                    <option value="0" <?php if ($dados->getAtuacaoTemporaria() == 0) {
    echo 'selected';
}; ?>>Selecione...</option>
                                    <option value="1" <?php if ($dados->getAtuacaoTemporaria() == 1) {
    echo 'selected';
}; ?>>Loja</option>
                                    <option value="2" <?php if ($dados->getAtuacaoTemporaria() == 2) {
    echo 'selected';
}; ?>>Pronaos</option>
                                    <option value="3" <?php if ($dados->getAtuacaoTemporaria() == 3) {
    echo 'selected';
}; ?>>Capítulo</option>
                                    <option value="4" <?php if ($dados->getAtuacaoTemporaria() == 4) {
    echo 'selected';
}; ?>>Heptada</option>
                                    <option value="5" <?php if ($dados->getAtuacaoTemporaria() == 5) {
    echo 'selected';
}; ?>>Atrium</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">E-mail</label>
                            <div class="col-sm-10"><input type="text" readonly name="emailOrganismoAfiliadoOrcz" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fDesEmail; ?>" id="emailOrganismoAfiliadoOrcz" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <?php if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) { ?>
                        <div class="form-group"><label class="col-sm-2 control-label">E-mail no SOA</label>
                            <div class="col-sm-10"><input type="text" name="emailOrganismoAfiliado" value="<?php echo $dados->getEmailOrganismoAfiliado(); ?>" id="emailOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Senha do E-mail</label>
                            <div class="col-sm-10" id="campoSenha"><input type="text" name="senhaEmailOrganismoAfiliado" id="senhaEmailOrganismoAfiliado" class="form-control" style="max-width: 320px" value="<?php echo $dados->getSenhaEmailOrganismoAfiliado(); ?>"></div>
                        </div>
                        <?php }else{
                            ?>
                            <input type="hidden" name="emailOrganismoAfiliado" value="<?php echo $dados->getEmailOrganismoAfiliado(); ?>" id="emailOrganismoAfiliado" class="form-control" style="max-width: 320px">
                            <input type="hidden" name="senhaEmailOrganismoAfiliado" id="senhaEmailOrganismoAfiliado" class="form-control" style="max-width: 320px" value="<?php echo $dados->getSenhaEmailOrganismoAfiliado(); ?>">
                            <?php
                        }?>
                        <div class="form-group"><label class="col-sm-2 control-label">Situação</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" readonly name="situacaoOrganismoAfiliado" id="situacaoOrganismoAfiliado" style="max-width: 300px">
                                    <option value="0">Selecione...</option>
                                    <option value="1" <?php if($dados->getSituacaoOrganismoAfiliado()==1){ echo "selected";}?>>Recesso</option>
                                    <option value="2" <?php if($dados->getSituacaoOrganismoAfiliado()==2){ echo "selected";}?>>Fechou</option>
                                    <option value="3" <?php if($dados->getSituacaoOrganismoAfiliado()==3){ echo "selected";}?>>Aberto</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">CNPJ</label>
                            <div class="col-sm-10"><input type="text" readonly name="cnpjOrganismoAfiliado"  value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fNumCnpj; ?>" id="cnpjOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">CEP</label>
                            <div class="col-sm-10"><input type="text" onkeypress="mascara_cep(this)" onblur="pesquisacep(this.value);" maxlength="9" name="cepOrganismoAfiliado" value="<?php echo trim($return->result[0]->fields->fArrayOA[0]->fields->fCodCep); ?>" id="cepOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Endereço</label>
                            <div class="col-sm-10"><input type="text" readonly name="enderecoOrganismoAfiliado" value="<?php echo trim($return->result[0]->fields->fArrayOA[0]->fields->fDesLograd); ?>" id="enderecoOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Nº</label>
                            <div class="col-sm-10"><input type="text" name="numeroOrganismoAfiliado" maxlength="6" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fNumEndere; ?>" id="numeroOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Complemento</label>
                            <div class="col-sm-10"><input type="text" name="complementoOrganismoAfiliado" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fDesCompleLograd; ?>" id="complementoOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Bairro</label>
                            <div class="col-sm-10"><input type="text" readonly name="bairroOrganismoAfiliado" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fNomBairro; ?>" id="bairroOrganismoAfiliado" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Cidade</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" readonly name="fk_idCidade" id="fk_idCidade" style="max-width: 300px">
                                    <option value="0">Selecione...</option>
                                    <?php
	                                    include_once 'controller/cidadeController.php';
	                                    $cc = new cidadeController();
	                                    $cc->criarComboBox($dados->getFk_idCidade(),$dados->getFk_idEstado());
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Estado</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" readonly name="fk_idEstado" id="fk_idEstado" onChange="carregaCombo(this.value, 'fk_idCidade');" style="max-width: 300px">
                                    <option value="0">Selecione...</option>
                                    <?php
	                                    include_once 'controller/estadoController.php';
	                                    $ec = new estadoController();
	                                    $ec->criarComboBox($dados->getFk_idEstado());
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Celular</label>
                            <div class="col-sm-10">
                                <input type="text" name="celularOrganismoAfiliado" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fNumCelula; ?>" id="celularOrganismoAfiliado" class="form-control" style="max-width: 320px" maxlength="12"> (máx. 12 caracteres)</div>
                        </div>    
                        <div class="form-group"><label class="col-sm-2 control-label">Telefone Fixo</label>
                            <div class="col-sm-10"><input type="text" name="telefoneFixoOrganismoAfiliado" id="telefoneFixoOrganismoAfiliado" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fNumTelefoFixo; ?>" class="form-control" style="max-width: 320px" maxlength="12"> (máx. 12 caracteres)</div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Outro Telefone</label>
                            <div class="col-sm-10"><input type="text" name="outroTelefoneOrganismoAfiliado" id="outroTelefoneOrganismoAfiliado" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fNumOutroTelefo; ?>" class="form-control" style="max-width: 320px" maxlength="12"> (máx. 12 caracteres)</div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Fax</label>
                            <div class="col-sm-10"><input type="text" name="faxOrganismoAfiliado" id="faxOrganismoAfiliado" class="form-control" value="<?php echo $return->result[0]->fields->fArrayOA[0]->fields->fNumFax; ?>" style="max-width: 320px" maxlength="20"> (máx. 20 caracteres)</div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Caixa Postal</label>
                            <div class="col-sm-10"><input type="text" name="caixaPostalOrganismoAfiliado" id="caixaPostalOrganismoAfiliado" value="<?php echo trim($return->result[0]->fields->fArrayOA[0]->fields->fNumCaixaPostal); ?>" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Cep da Caixa Postal</label>
                            <div class="col-sm-10"><input type="text" name="cepCaixaPostalOrganismoAfiliado" id="cepCaixaPostalOrganismoAfiliado" value="<?php echo trim($return->result[0]->fields->fArrayOA[0]->fields->fCodCepCaixaPostal); ?>" class="form-control" style="max-width: 320px"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Endereço para Correspondência</label>
                        	<div class="col-sm-10">
                            	<input type="radio" value="1" name="enderecoCorrespondenciaOrganismoAfiliado" id="enderecoCorrespondenciaOrganismoAfiliado"  <?php if($return->result[0]->fields->fArrayOA[0]->fields->fIdeEndereCorres==1){ echo "checked";}?>>Logradouro
                            	<input type="radio" value="2" name="enderecoCorrespondenciaOrganismoAfiliado" id="enderecoCorrespondenciaOrganismoAfiliado"  <?php if($return->result[0]->fields->fArrayOA[0]->fields->fIdeEndereCorres==2){ echo "checked";}?>>Caixa Postal
                            </div>
                        </div>
                         <div class="form-group"><label class="col-sm-2 control-label">Não cobrar relatórios</label>
                        	<div class="col-sm-10">
                                <div class="radio i-checks"><label class=""> <div class="iradio_square-green" style="position: relative;"><input type="radio" value="1" name="naoCobrarDashboard" id="naoCobrarDashboard_sim" <?php if($dados->getNaoCobrarDashboard() == 1){ echo "checked";}?> style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> <i></i> Sim </label></div>
                            	<div class="radio i-checks"><label class=""> <div class="iradio_square-green" style="position: relative;"><input type="radio" value="0" name="naoCobrarDashboard" id="naoCobrarDashboard_nao" <?php if($dados->getNaoCobrarDashboard() == 0){ echo "checked";}?> style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> <i></i> Não </label></div>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <input type="hidden" name="cidade" id="cidade" value="<?php echo $cid->retornaCidadeTexto($dados->getFk_idCidade());?>" />
                        <input type="hidden" name="latitude" id="latitude" value="<?php echo $dados->getLatitude();?>" />
                        <input type="hidden" name="longitude" id="logitude" value="<?php echo $dados->getLongitude();?>" />
                        <input type="hidden" name="paisOa" id="paisOa" value="<?php echo $paisDoOrganismo; ?>" />
                        <input type="hidden" name="idOrganismoAfiliado" id="idOrganismoAfiliado" value="<?php echo $_GET['idOrganismoAfiliado']; ?>" />
                        <input type="hidden" name="statusOrganismoAfiliado" id="statusOrganismoAfiliado" value="<?php echo $dados->getStatusOrganismoAfiliado(); ?>" />
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast']; ?>" />
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Atualizar" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro();
?>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

