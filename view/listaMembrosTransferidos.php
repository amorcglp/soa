<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php 
include_once("model/organismoClass.php");

$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Membro transferido com sucesso! Os oficiais do Organismo Afiliado de Destino já foram avisados.",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Lista de Membros Transferidos do Organismo Afiliado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Cadastro</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaTransferenciaMembro">Transferência de Membros do Organismo Afiliado</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Membros Transferidos do Organismo Afiliado</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" href="?corpo=buscaTransferenciaMembro" style="color:white">
                            <i class="fa fa-mail-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód. de Afiliação</th>
                                <th>Nome</th>
		                        <th>Organismo Afiliado Origem</th>
		                        <th>Organismo Afiliado Destino</th>
		                        <th><center>Data da Transferência</center></th>
		                        <th><center>Declaração</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/transferenciaMembroController.php");

                            $t = new transferenciaMembroController();
                            $resultado = $t->listaTransferenciaMembro();
							
                            if ($resultado) {
                                foreach ($resultado as $vetor) {

                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                                    ?>
                               <?php if(strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA"))){?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['codigoAfiliacao']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor['nome']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                        </td>
                                        <td>
                                        	<?php 
					                           
					                            $o = new organismo();
					                            $resultado2 = $o->listaOrganismo(null,null,null,null,null,$vetor['fk_idOrganismoAfiliadoDestino']);
												
					                            if ($resultado2) {
					                                foreach ($resultado2 as $vetor2) {
					
					                                    switch ($vetor2['classificacaoOrganismoAfiliado']) {
					                                        case 1:
					                                            $classificacao2 = "Loja";
					                                            break;
					                                        case 2:
					                                            $classificacao2 = "Pronaos";
					                                            break;
					                                        case 3:
					                                            $classificacao2 = "Capítulo";
					                                            break;
					                                        case 4:
					                                            $classificacao2 = "Heptada";
					                                            break;
					                                        case 5:
					                                            $classificacao2 = "Atrium";
					                                            break;
					                                    }
					                                    switch ($vetor2['tipoOrganismoAfiliado']) {
					                                        case 1:
					                                            $tipo2 = "R+C";
					                                            break;
					                                        case 2:
					                                            $tipo2 = "TOM";
					                                            break;
					                                    }
					                                }
					                                echo $vetor2["siglaOrganismoAfiliado"] . " - " . $classificacao2 . " " . $tipo2 . " " . $vetor2['nomeOrganismoAfiliado'];
					                            }
                                        	?>
                                        </td>
                                        <td>
                                            <center>
                                            	<?php echo substr($vetor['dataTransferencia'],8,2)."/".substr($vetor['dataTransferencia'],5,2)."/".substr($vetor['dataTransferencia'],0,4);?>
                                            </center>
                                        </td>
		                                <td>
			                                <center>
			                                    <a href="#" onclick="imprimirDeclaracaoTransferencia('<?php echo $vetor['idTransferenciaMembro']; ?>');" class="btn btn-sm btn-info">
			                                        <i class="fa fa-print fa-white"></i>&nbsp;
			                                        Imprimir Declaração
			                                    </a>
			                                </center>
		                                </td>
                                	</tr>
                            	<?php
                                    }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeDetalhes" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-search-plus modal-icon"></i>
                <h4 class="modal-title">Detalhes da Ata de Posse</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form action="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data da Posse:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="dataPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora de inicio:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="horaInicioPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Endereço:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="enderecoPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="numeroPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bairro:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="bairroPosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Cidade:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="cidadePosse"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mestre Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="mestreRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Secretário Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="secretarioRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Presidente Junta Depositária Retirante:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="presidenteJuntaDepositariaRetirante"></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Detalhes Adicionais:</label>
                        <div class="col-sm-9">
                            <div class="form-control-static"><span id="detalhesAdicionaisPosse"></span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Upload da Ata Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> é preciso enviar a ata original para GLP por correio, mesmo fazendo o upload da ata assinada. Caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="idAtaUpload" name="idAtaUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Upload" onclick="document.ata.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
