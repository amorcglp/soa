<?php
require_once("model/criaSessaoClass.php");
$sessao = new criaSessao();

include_once("controller/notificacaoAtualizacaoServerController.php");

$nsc = new notificacaoServerController();
$dados = $nsc->buscaNotificacaoServer($_GET['idNotificacao']);

include_once("model/notificacoesServerUsuarioClass.php");

$nsu = new notificacoesServerUsuario();
$nsu->setFk_idNotificacao($_GET['idNotificacao']);
$nsu->setFk_seqCadast($sessao->getValue("seqCadast"));
$result = $nsu->lista($sessao->getValue("seqCadast"), $_GET['idNotificacao']);
if (!$result) {
    $nsu->cadastro();
}
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $dados->getTituloNotificacao(); ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li>
                <a href="?corpo=listaDeAtualizacoesServer">Lista de Atualização SOA</a>
            </li>
            <li class="active">
                <strong><?php echo $dados->getTituloNotificacao(); ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight article">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="text-center article-title">
                        <span class="text-muted"><i class="fa fa-clock-o"></i> <?php $data = date_create($dados->getDataNotificacao());
echo date_format($data, 'd/m/Y - H:i:s'); ?></span>
                        <h1>
                            <?php echo $dados->getTituloNotificacao(); ?>
                        </h1>
                    </div>
                    <br>
                    <div style="text-align: justify">
                        <?php echo $dados->getDescricaoNotificacao(); ?>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="small text-right">
                                <h5>Autor:</h5>
                                <i class="fa fa-laptop"> </i> CPD - Desenvolvimento 

                            </div>

                        </div>
                        <center><a href="?corpo=listaDeAtualizacoesServer"><i class="fa fa-reply"> </i> Voltar</a></center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>