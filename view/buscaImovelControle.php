<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();?>
<!-- Conteúdo DE INCLUDE INICIO -->

	<!-- Caminho de Migalhas Início -->
	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-10">
			<h2>Imóveis</h2>
			<ol class="breadcrumb">
				<li>
					<a href="">Início</a>
				</li>
				<li class="active">
					<a href="">Controle de Documentos de Imóveis</a>
				</li>
			</ol>
		</div>
		<div class="col-lg-2">
		</div>
	</div>
	<!-- Caminho de Migalhas Fim -->
<?php
include_once("controller/organismoController.php");
include_once("controller/imovelControleController.php");
include_once("controller/imovelController.php");
include_once("controller/imovelAnexoTipoController.php");
include_once("controller/usuarioController.php");
require_once("model/organismoClass.php");
require_once("model/imovelClass.php");
require_once("model/funcaoUsuarioClass.php");
require_once("model/funcaoClass.php");
require_once("model/criaSessaoClass.php");

$oc 		= new organismoController();
$icc 		= new imovelControleController();
$icm 		= new imovelControle();
$ic 		= new imovelController();
$uc 		= new usuarioController();
$um 		= new Usuario();
$fum 		= new FuncaoUsuario();
$fm 		= new funcao();
$sessao 	= new criaSessao();
$iatc   = new imovelAnexoTipoController();
$iatm   = new ImovelAnexoTipo();

$dataAtual = date('Y');
$idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';
if($idOrganismo == ''){
	echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaImovel';</script>";
}
$data	= isset($_GET['referenteAnoImovelControle']) ? json_decode($_GET['referenteAnoImovelControle']) : $dataAtual;
?>
	<!-- Tabela Início -->
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Controle de Documentos Cadastrados
						<?php
							$dadosOrg = $oc->buscaOrganismo($idOrganismo);
							$nomeOa = organismoAfiliadoNomeCompleto($dadosOrg->getClassificacaoOrganismoAfiliado(),$dadosOrg->getTipoOrganismoAfiliado(),$dadosOrg->getNomeOrganismoAfiliado(),$dadosOrg->getSiglaOrganismoAfiliado());
							echo ' - '.$nomeOa.' ['.$data.']';
						?>
						</h5>
						<div class="ibox-tools">
							<?php if (($data-1) >= 2010) { ?>
								<a class="btn btn-xs btn-success" style="color: white" href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $idOrganismo; ?>&referenteAnoImovelControle=<?php echo $data - 1; ?>">
									<i class="fa fa-chevron-left"></i>&nbsp;Voltar Ano [<?php echo $data - 1; ?>]
								</a>
							<?php } ?>

							<?php if ($data != $dataAtual) { ?>

								<?php if ($dataAtual != ($data + 1)) { ?>
								<a class="btn btn-xs btn-success" style="color: white" href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $idOrganismo; ?>&referenteAnoImovelControle=<?php echo $dataAtual; ?>">
									&nbsp;Ano Atual&nbsp;[<?php echo $dataAtual; ?>]&nbsp;
								</a>
								<?php } ?>

								<a class="btn btn-xs btn-success" style="color: white" href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $idOrganismo; ?>&referenteAnoImovelControle=<?php echo $data + 1; ?>">
									[<?php echo $data + 1; ?>] Avançar Ano&nbsp;<i class="fa fa-chevron-right"></i>
								</a>
							<?php }?>

							<?php if(in_array("1",$arrNivelUsuario)){?>
								<a class="btn btn-xs btn-primary" href="?corpo=cadastroImovelControle<?php if($idOrganismo!=''){ echo '&idOrganismoAfiliado='.$idOrganismo;} ?>">
									<i class="fa fa-plus"></i> Cadastrar Novo
								</a>
							<?php } ?>
							<a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaImovel">
								<i class="fa fa-reply"></i> Voltar para área de Imóveis
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<table class="table table-striped table-bordered table-hover dataTables-example" >
							<thead>
								<tr>
									<th>Imovel</th>
									<th><center>Construção</center></th>
									<th><center>Manutenção</center></th>
									<th><center>Status</center></th>
									<th><center>Ações</center></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$resultado = $icc->listaImovelControle($idOrganismo,$data);
								$resultadoImovelEntregue = $icc->listaImovelControle($idOrganismo,$data);
								$statusImovel = 0; // 0 = Ativo
								$resultadoImovelTotal = $ic->listaImovel($idOrganismo, $statusImovel);

								$imovel=array();
								$imoveisSemControle=array();

								/* Lista dos entregues */
								$imovelEntregue=array();
								if ($resultado) {
									foreach ($resultado as $vetorImovelEntregue) {
										$imovelEntregue['fk_idImovel'][] = $vetorImovelEntregue['fk_idImovel'];
									}
								}
								/* Caso não tenha nenhum entregue ele simula uma casa com ID 0 */
								if (!$imovelEntregue){
									$imovelEntregue['fk_idImovel'][] = 0;
								}

								/* Lista dos não entregues */
								if ($resultadoImovelTotal) {
									foreach ($resultadoImovelTotal as $vetorImovel) {
										$imovel['idImovel'][] = $vetorImovel['idImovel'];
									}
								}
								if ($resultadoImovelTotal) {
									for($i=0;$i<count($imovel['idImovel']);$i++) {

										if(!in_array($imovel['idImovel'][$i],$imovelEntregue['fk_idImovel'])){
											$imoveisSemControle['imovel'][] = $imovel['idImovel'][$i];
										}

									}
								}

								if ($resultadoImovelEntregue) {
										foreach ($resultadoImovelEntregue as $vetor) {


											$dadosImovel = $ic->buscaImovel($vetor['fk_idImovel']);
											$complemento='';
											if($dadosImovel->getComplementoImovel()!=''){
												$complemento = ' - '.$dadosImovel->getComplementoImovel();
											}
											$nomeImovel = $dadosImovel->getEnderecoImovel().' - '.$dadosImovel->getNumeroImovel().$complemento;
								?>
								<tr>
									<td>
										<?php
											echo $nomeImovel;
										?>
									</td>
									<td>
										<center>
										<?php
											switch($vetor['construcaoImovelControle']){
												case 0: echo "<span class='badge badge-success'>Sim</span>"; break;
												case 1: echo "<span class='badge badge-warning'>Não</span>"; break;
											}
										?>
										</center>
									</td>
									<td>
										<center>
										<?php
											switch($vetor['manutencaoImovelControle']){
												case 0: echo "<span class='badge badge-success'>Sim</span>"; break;
												case 1: echo "<span class='badge badge-warning'>Não</span>"; break;
											}
										?>
										</center>
									</td>
									<td>
										<center>
											<div id="statusTarget<?php echo $vetor['idImovelControle']?>">
											<?php
												switch($vetor['statusImovelControle']){
													case 0: echo "<span class='badge badge-primary'>Ativo</span>"; break;
													case 1: echo "<span class='badge badge-danger'>Inativo</span>"; break;
												}
											?>
											</div>
										</center>
									</td>
									<td>
										<center>
										<div id="acoes_confirma_cancela">
											<a class="btn btn-sm btn-white" onclick="abrirPopUpImprimirImovelControle(<?php echo $vetor['idImovelControle'];?>);">
												<i class="fa fa-print fa-white"></i>
												Imprimir
											</a>
											<a class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idImovelControle'];?>">
												<i class="fa fa-search fa-white"></i>
												Detalhes
											</a>
											<?php if(in_array("2",$arrNivelUsuario)){?>
											<a class="btn btn-sm btn-info" href="?corpo=alteraImovelControle&idImovelControle=<?php echo $vetor['idImovelControle'];?>" data-rel="tooltip" title="">
												<i class="fa fa-edit fa-white"></i>
												Editar
											</a>
											<?php }?>
                                            <a class="btn btn-sm btn-warning" onclick="" data-target="#mySeeAnexos<?php echo $vetor['idImovelControle']; ?>" data-toggle="modal" data-placement="left" title="Anexos">
                                                <i class="fa fa-paperclip fa-white"></i>
                                                Anexos
                                            </a>
											<?php if(in_array("3",$arrNivelUsuario)){?>
											<span id="status<?php echo $vetor['idImovelControle']?>">
												<?php if($vetor['statusImovelControle']==1){?>
													<a class="btn btn-sm btn-primary" onclick="mudaStatus('<?php echo $vetor['idImovelControle']?>','<?php echo $vetor['statusImovelControle']?>','imovelControle')" data-rel="tooltip" title="Ativar">
														Ativar
													</a>
												<?php }else{?>
													<a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idImovelControle']?>','<?php echo $vetor['statusImovelControle']?>','imovelControle')" data-rel="tooltip" title="Inativar">
														Inativar
													</a>
												<?php }?>
											</span>
												<?php }?>
											<?php $titulo = "Controle de Documentos do ano ".$data." do Imóvel ".$nomeImovel; ?>
											<?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
												<!-- abreModalNotificacao(ID do Organismo , Título Padrão Desejado) -->
												<a class="btn btn-sm btn-warning" onclick="abreModalNotificacao(<?php echo $vetor['fk_idOrganismoAfiliado'];?>,'<?php echo $titulo ?>')">
													<i class="fa fa-envelope icon-white"></i>
													Notificar
												</a>
											<?php } ?>
										</div>
										</center>
									</td>
								</tr>
								<?php }
								}
								if ($idOrganismo!='') {
									if ($imoveisSemControle) {
										for($i=0;$i<count($imoveisSemControle['imovel']);$i++) {
											$idImovel = $imoveisSemControle['imovel'][$i];
											$dadosImovel = $ic->buscaImovel($idImovel);
											$complemento='';
											if($dadosImovel->getComplementoImovel()!=''){
												$complemento = ' - '.$dadosImovel->getComplementoImovel();
											}
											$nomeImovel = $dadosImovel->getEnderecoImovel().' - '.$dadosImovel->getNumeroImovel().$complemento;
										?>
										<tr>
											<td>
												<?php

													echo $nomeImovel;
												?>
											</td>
											<td>
												<center>

												</center>
											</td>
											<td>
												<center>

												</center>
											</td>
											<td>
												<center>
													<span class='badge badge-warning'>Não Entregue</span>
												</center>
											</td>
											<td><center>
													<div id="acoes_confirma_cancela">
													<?php $titulo = "Controle de Documentos do ano ".$data." do Imóvel ".$nomeImovel; ?>
													<?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
														<!-- abreModalNotificacao(ID do Organismo , Título Padrão Desejado) -->
														<a class="btn btn-sm btn-warning" onclick="abreModalNotificacao(<?php echo $idOrganismo;?>,'<?php echo $titulo ?>')">
															<i class="fa fa-envelope icon-white"></i>
															Notificar
														</a>
													<?php } ?>
													<a class="btn btn-sm btn-primary" href="?corpo=cadastroImovelControle&idOrganismoAfiliado=<?php echo $idOrganismo; ?>&idImovel=<?php echo $idImovel; ?>&referenteAnoImovelControle=<?php echo $data; ?>">
														<i class="fa fa-plus icon-white"></i>
														Cadastrar
													</a>
													</div>
												</center>
											</td>
										</tr>
										<?php
										}
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Tabela Fim -->

	<!-- Modal's da página INICIO -->
	<?php
	$resultado = $icc->listaImovelControle();

	if ($resultado) {
		foreach ($resultado as $vetor) {
	?>
		<!-- Modal de detalhes do Controle de Documentos de Imóveis -->
		<div class="modal inmodal" id="myModaInfo<?php echo $vetor['idImovelControle'];?>" tabindex="-1" role="dialog"  aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content animated fadeIn">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
						<h4 class="modal-title">Controle de Documentos de Imóvel</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Imóvel: </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php
												$dadosImovel = $ic->buscaImovel($vetor['fk_idImovel']);
												?>
												<?php echo $dadosImovel->getEnderecoImovel().' - '.$dadosImovel->getNumeroImovel().' - '.$dadosImovel->getComplementoImovel();?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Organismo Afiliado: </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php
													$dadosOrganismo = $oc->buscaOrganismo($vetor['fk_idOrganismoAfiliado']);
													switch ($dadosOrganismo->getClassificacaoOrganismoAfiliado()) {
														case 1: echo "Loja"; break;
														case 2: echo "Pronaos"; break;
														case 3: echo "Capítulo"; break;
														case 4: echo "Heptada"; break;
														case 5: echo "Atrium"; break;
													}
													switch ($dadosOrganismo->getTipoOrganismoAfiliado()) {
														case 1: echo "R+C"; break;
														case 2: echo "TOM"; break;
													}
													echo $dadosOrganismo->getNomeOrganismoAfiliado().' - '.$dadosOrganismo->getSiglaOrganismoAfiliado();
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Referente ao Ano de: </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php echo $vetor['referenteAnoImovelControle']; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">IPTU: </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php switch($vetor['propriedadeIptuImovelControle']){
													case 0: echo "Pago"; break;
													case 1: echo "Em debito"; break;
													case 2: echo "Isento"; break;
													case 3: echo "Imune"; break;
													case 4: echo "Outro"; break;
													}
													if($vetor['propriedadeIptuAnexoImovelControle']==''){
														echo ' - Não Anexado.';
													}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php if($vetor['propriedadeIptuImovelControle']==3 || $vetor['propriedadeIptuImovelControle']==4){?>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Observações IPTU: </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php echo $vetor['propriedadeIptuObsImovelControle']; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php }?>
							<?php if($vetor['propriedadeIptuAnexoImovelControle'] != ''){?>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">IPTU Anexado: </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php $arrLink = explode('.', $vetor['propriedadeIptuAnexoImovelControle']);
													$arrLinkNome	= $arrLink[count($arrLink)-2];
													$arrLinkExt		= $arrLink[count($arrLink)-1];
												?>
												<a target="_BLANK" href="<?php echo $vetor['propriedadeIptuAnexoImovelControle']; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php }?>
							<hr>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Houve Construção? </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php
													switch($vetor['construcaoImovelControle']){
														case 0:echo "Sim"; break;
														case 1: echo "Não"; break;
													}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php if($vetor['construcaoImovelControle'] == 1){?>
							<hr>
							<?php }?>
							<?php if($vetor['construcaoImovelControle'] == 0){?>
								<hr>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Possui Alvará de Construção: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['construcaoAlvaraImovelControle']){
															case 0: echo "Sim"; break;
															case 1: echo "Não"; break;
														}
														if(($vetor['construcaoAlvaraAnexoImovelControle']=='')&&($vetor['construcaoAlvaraImovelControle']==0)){
															echo ' - Não Anexado.';
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($vetor['construcaoAlvaraAnexoImovelControle'] != ''){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Alvará Anexado: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php $arrLink = explode('.', $vetor['construcaoAlvaraAnexoImovelControle']);
														$arrLinkNome	= $arrLink[count($arrLink)-2];
														$arrLinkExt		= $arrLink[count($arrLink)-1];
													?>
													<a target="_BLANK" href="<?php echo $vetor['construcaoAlvaraAnexoImovelControle']; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php }?>
								<br>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Restrições na Construção: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['construcaoRestricoesImovelControle']){
															case 0: echo "Sim"; break;
															case 1: echo "Não"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($vetor['construcaoRestricoesAnotacoesImovelControle'] != ''){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">São elas: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px; text-align: justify">
													<?php echo $vetor['construcaoRestricoesAnotacoesImovelControle']; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php }?>
								<br>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Construção Concluída: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['construcaoConcluidaImovelControle']){
															case 0: echo "Sim"; break;
															case 1: echo "Não"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($vetor['construcaoTerminoDataImovelControle'] != ''){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" id="datapicker_termino_construcao_label" style="text-align: right">
											<?php if($vetor['construcaoConcluidaImovelControle'] == 0){?>
												Data de Término da Construção:
											<?php } elseif($vetor['construcaoConcluidaImovelControle'] == 1){?>
												Prazo de Término da Construção:
											<?php } ?>
										</label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php if($vetor['construcaoTerminoDataImovelControle']!='0000-00-00'){ echo substr($vetor['construcaoTerminoDataImovelControle'],8,2)."/".substr($vetor['construcaoTerminoDataImovelControle'],5,2)."/".substr($vetor['construcaoTerminoDataImovelControle'],0,4);} else { echo 'Não Informado!';}?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php }?>
								<br>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Possui INSS da Construção: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['construcaoInssImovelControle']){
															case 0: echo "Sim"; break;
															case 1: echo "Não"; break;
														}
														if(($vetor['construcaoInssAnexoImovelControle']=='')&&($vetor['construcaoInssImovelControle']==0)){
															echo ' - Não Anexado.';
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($vetor['construcaoInssAnexoImovelControle'] != ''){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">INSS Anexado: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php $arrLink = explode('.', $vetor['construcaoInssAnexoImovelControle']);
														$arrLinkNome	= $arrLink[count($arrLink)-2];
														$arrLinkExt		= $arrLink[count($arrLink)-1];
													?>
													<a target="_BLANK" href="<?php echo $vetor['construcaoInssAnexoImovelControle']; ?>"><?php echo $arrLinkNome.'.'.$arrLinkExt; ?></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php }?>
								<hr>
							<?php }?>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Houve Manutenção? </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
												<?php
													switch($vetor['manutencaoImovelControle']){
														case 0: echo "Sim"; break;
														case 1: echo "Não"; break;
													}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<?php if($vetor['manutencaoImovelControle'] == 0){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Pintura: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['manutencaoPinturaImovelControle']){
															case 0: echo "Sim"; break;
															case 1: echo "Não"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($vetor['manutencaoImovelControle'] == 0){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['manutencaoPinturaImovelControle']){
															case 0: echo "Anual"; break;
															case 1: echo "Bienal"; break;
															case 2: echo "Trienal"; break;
															case 3: echo "Quadrienal"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php }?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Reparo de Hidráulica: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['manutencaoHidraulicaImovelControle']){
															case 0: echo "Sim"; break;
															case 1: echo "Não"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($vetor['manutencaoHidraulicaImovelControle'] == 0){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['manutencaoHidraulicaTipoImovelControle']){
															case 0: echo "Anual"; break;
															case 1: echo "Bienal"; break;
															case 2: echo "Trienal"; break;
															case 3: echo "Quadrienal"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php }?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Reparo de Elétrica: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['manutencaoEletricaImovelControle']){
															case 0: echo "Sim"; break;
															case 1: echo "Não"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php if($vetor['manutencaoEletricaImovelControle'] == 0){?>
								<div class="row">
									<div class="form-group">
										<label class="col-sm-4 control-label" style="text-align: right">Periodicidade: </label>
										<div class="col-sm-8">
											<div class="col-sm-12" style="max-width: 320px">
												<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;">
													<?php
														switch($vetor['manutencaoEletricaTipoImovelControle']){
															case 0: echo "Anual"; break;
															case 1: echo "Bienal"; break;
															case 2: echo "Trienal"; break;
															case 3: echo "Quadrienal"; break;
														}
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php }?>
								<hr>
							<?php }?>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Anotações Complementares: </label>
									<div class="col-sm-8">
										<div class="col-sm-12" style="max-width: 320px">
											<div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px; text-align: justify">
												<?php echo $vetor['anotacoesImovelControle']; ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="form-group">
									<label class="col-sm-4 control-label" style="text-align: right">Último a atualizar: </label>
									<div class="col-sm-8">
										<?php

										$resultadoAtualizadoPor = $um->buscaUsuario($vetor['fk_seqCadastAtualizadoPor']);
										if ($resultadoAtualizadoPor) {
											foreach ($resultadoAtualizadoPor as $vetorAtualizadoPor) {
												echo $vetorAtualizadoPor['nomeUsuario'];
											}
										}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>
		<?php
				}
			}
		?>

<!-- Modal's de ANEXOS DO IMÓVEL -->
<?php
$resultadoAnexo = $icc->listaImovelControle($idOrganismo,$data);
if ($resultadoAnexo) {
    foreach ($resultadoAnexo as $vetorAnexo) {
        ?>
        <div class="modal inmodal" id="mySeeAnexos<?php echo $vetorAnexo['idImovelControle']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated bounceInUp">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-paperclip modal-icon"></i>
                        <h4 class="modal-title">Anexos do Imóvel</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                        $objImovelAnexoTipo = $iatc->listaImovelAnexoTipo(1);
                        if($objImovelAnexoTipo){
                            foreach($objImovelAnexoTipo as $vetorImovelAnexoTipo){
                                ?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" style="text-align: right">
                                            <?php echo $vetorImovelAnexoTipo['nomeImovelAnexoTipo'] ?>:
                                        </label>
                                        <div class="col-sm-8">
                                            <div class="col-sm-12" style="max-width: 320px">
                                                <input
                                                    name="file_upload_imovel_controle_anexo<?php echo $vetorAnexo['idImovelControle'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>"
                                                    id="file_upload_imovel_controle_anexo<?php echo $vetorAnexo['idImovelControle'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>"
                                                    type="file"
                                                /><br>
                                                <div class="form-control-static" style="padding-top: 0px; padding-bottom: 10px;" name="listaAnexos<?php echo $vetorAnexo['idImovelControle'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>" id="listaAnexos<?php echo $vetorAnexo['idImovelControle'].$vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>">
                                                    <?php
                                                    $objImovelControleAnexo = $icm->listaImovelControleAnexo($vetorAnexo['idImovelControle'], $vetorImovelAnexoTipo['idImovelAnexoTipo']);
                                                    if($objImovelControleAnexo){
                                                        foreach($objImovelControleAnexo as $vetorImovelControleAnexo){
															$attach = $vetorImovelControleAnexo['full_path'];
															if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $attach)) {
																$attach = '/soa' . $attach; // treinamento
															}
															echo "<a target='_BLANK' href='" . $attach . "'>". $vetorImovelControleAnexo['nome_original'] . "</a><a style='margin-right: 25px' onclick='excluiImovelControleAnexo(" . $vetorImovelControleAnexo['idImovelControleAnexo'] . "," . $vetorAnexo['idImovelControle'] . "," . $vetorImovelControleAnexo['fk_idImovelAnexoTipo'] . ");'> <i class='fa fa-trash'></i></a><br>";
                                                        }
                                                    } else {
                                                        echo "Nenhum anexo enviado!";
                                                    }

                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <hr>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

		<div class="modal inmodal" id="mySeeNotificacao" name="mySeeNotificacao" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content animated bounceInRight">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
						<i class="fa fa-envelope modal-icon"></i>
						<h4 class="modal-title">Notificar Responsáveis</h4>
					</div>
					<div class="modal-body">
						<div id="conteudoModal"></div>
						<div class="row" style="display: none;" id="blocoTipo">
							<div class="form-group" id="tipoNotificacao">
								<br>
								<label>Mensagem do Tipo: </label>
								<div>
									<select class="form-control col-sm-3" name="tipoNotificacaoGlp" id="tipoNotificacaoGlp" required="required">
										<option value="0">Selecione...</option>
										<option value="2">Falta Documento</option>
										<option value="3">Erro de Documentação</option>
										<option value="4">Deve Atualizar</option>
										<option value="5">Outro</option>
									</select>
								</div>
								<br>
							</div>
							<br>
						</div>
						<div class="row" style="" id="blocoTitulo">
							<div class="form-group" style="margin-top: 20px;" id="tituloNotificacao">
								<label>Título:</label>
								<div>
									<input class="form-control" id="tituloNotificacaoGlp" name="tituloNotificacaoGlp" type="text" value="" style="min-width: 320px" required="required">
								</div>
							</div>
						</div>
						<div class="row" style="" id="blocoMensagem">
							<div class="form-group" style="margin-top: 10px;" id="mensagemNotificacao">
								<label>Mensagem:</label>
								<div style="border: #ccc solid 1px">
									<div class="summernote" id="mensagemNotificacaoGlp" name="mensagemNotificacaoGlp"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" id="fk_seqCadastRemetente" value="<?php echo $sessao->getValue("seqCadast"); ?>" />
						<div id="enviarNotificacaoButton"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal's da página FIM -->

	<!-- Conteúdo DE INCLUDE FIM -->