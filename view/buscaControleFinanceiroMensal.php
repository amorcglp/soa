<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/usuarioController.php");
$um     = new Usuario();
require_once ("controller/organismoController.php");
$oc     = new organismoController();
/*
if(isset($_FILES['anexo']))
{
	if ($_FILES['anexo']['name'] != "") {
		$caminho = "img/ata_reuniao_mensal/" . basename($_POST["idAtaUpload"].".ata.reuniao.mensal.".$_FILES['anexo']['name']);
		//Guardar Link do Caminho no Banco de Dados
		include_once("model/ataReuniaoMensalClass.php");
		$arm = new ataReuniaoMensal();
		$arm->setCaminhoOrganismoAfiliadoAssinada($caminho);
		if($arm->atualizaCaminhoOrganismoAfiliadoAssinada($_POST["idAtaUpload"]))
		{
			$uploaddir = 'C:\/xampp\/htdocs\/soa_mvc\/img\/ata_reuniao_mensal\/';
			$uploadfile = $uploaddir . basename($_POST["idAtaUpload"].".ata.reuniao.mensal.".$_FILES['anexo']['name']);
			echo(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile) ? "<script type='text/javascript'>alert('Ata assinada enviada com sucesso!');</script>"  : "<script type='text/javascript'>alert('Erro ao enviar a ata assinada!');</script>");
		}
	}
}
*/
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Controle Financeiro Mensal</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Financeiro</a>
            </li>
            <li>
                <a href="painelDeControle.php">Controle Financeiro</a>
            </li>
            <li class="active">
                <strong><a>Mensal</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Controle Financeiro Mensal</h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Mês/Ano</th>
                                <th>Organismo</th>
		                        <th><center>Data de Entrega</center></th>
		                        <th><center>Entregue por</center></th>
		                        <th><center>Relatório Assinado</center></th>
		                        <th><center>Notificação</center>
		                    </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("model/organismoClass.php");
                            include_once("model/relatorioFinanceiroMensalClass.php");
							include_once("lib/functions.php");
							
                            $o = new organismo();
                            
                            $resultado = $o->listaOrganismo();
                            

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                	
                            		$rfm = new RelatorioFinanceiroMensal();
                            		$resultado2 = $rfm->listaRelatorioFinanceiroMensal(date('m'),date('Y'),$vetor['idOrganismoAfiliado']);
                            		$total=0;
                            		if($resultado2)
                            		{
                            			foreach($resultado2 as $vetor2)
                            			{
                            				$dataCadastro = $vetor2['dataCadastro'];
                            				$nomeUsuario = $vetor2['nomeUsuario'];
                            			} 
                            			$total = count($resultado2);   	
                            		}
                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                                    ?>
                                    
                                    <tr>
                                        <td>
                                            <?php echo mesExtensoPortugues(date("m"))."/".date("Y"); ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                            <a style="float: right" class="btn-xs btn-primary btn-outline" href="?corpo=buscaControleFinanceiroMensalComFiltro&amp;idOrganismoAfiliado=<?php echo $vetor['idOrganismoAfiliado'];?>" data-rel="tooltip" title="">
	                                            Filtrar pelo O.A.
	                                        </a>
                                        </td>
                                        <td>
		                                <center>
		                                    <?php if($total>0){ echo substr($dataCadastro, 8, 2) . "/" . substr($dataCadastro, 5, 2) . "/" . substr($dataCadastro, 0, 4)." às ".substr($dataCadastro, 11, 8); }else{ echo "Não entregou este mês"; }?> 
		                                </center>
		                                </td>
		                                <td>
		                                	<center>
		                                            <?php if($total>0){ echo $nomeUsuario;}else{ echo "--";} ?>
		                                    </center>
		                                </td>
		                                <td>
		                                    <center>
		                                    	<?php  
		                                    		if($total>0){?>
		                                    			<a href="#" 
		                                    			onClick="listaUploadRelatorioFinanceiroMensal('<?php echo date('m'); ?>','<?php echo date('Y'); ?>','<?php echo $vetor['idOrganismoAfiliado'];?>')"
		                                    			data-target="#mySeeListaUpload" data-toggle="modal"
		                                    			target="_blank">
		                                    				<button class="btn btn-info  dim" type="button"><i class="fa fa-paste"></i> </button>
		                                    			</a>
		                                    			<?php }else{ ?>
		                                    			<button class="btn btn-warning " type="button"><i class="fa fa-warning"></i> <span class="bold">Não enviado</span></button>
		                                    	<?php }?>
		                                    </center>
		                                </td>
		                                <td>
		                                	<center>
			                                	<?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
			                                            <a class="btn btn-sm btn-danger" href="#" onclick="abreModalNotificacao(<?php echo $vetor['idOrganismoAfiliado'];?>,null)">
			                                                <i class="fa fa-envelope icon-white"></i>
			                                                Notificar
			                                            </a>
			                                    <?php } ?>
                                    		</center>
		                                </td>
                               		 </tr>
                            <?php
                                
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->



<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Relatório Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php 
modalNotificacao($sessao->getValue("seqCadast"));
?>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
