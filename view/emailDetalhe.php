<?php 
$vinculoEmailEnviado 	= isset($_REQUEST['vinculoEmailEnviado'])?$_REQUEST['vinculoEmailEnviado']:null;
$idEmail 				= isset($_REQUEST['idEmail'])?$_REQUEST['idEmail']:null;

include_once 'model/usuarioClass.php';
$u = new Usuario();
include_once 'model/emailClass.php';
$e = new email();

$retorno = $e->buscarEmailPorId($_REQUEST['idEmail']);
$assunto="";
$de="";
$mensagem="";
$origem="";
if($retorno)
{
	foreach ($retorno as $vetor)
	{
		$origem		= $vetor['de'];
		$assunto 	= $vetor['assunto'];
		$de 		= $vetor['nomeUsuario'];
		$mensagem 	= $vetor['mensagem'];
		$data		= substr($vetor['dataCadastro'],8,2)."/".substr($vetor['dataCadastro'],5,2)."/".substr($vetor['dataCadastro'],0,4)." ".substr($vetor['dataCadastro'],11,8);
	}
}
/**
 * Marcar mensagem como lida
 */
include_once 'model/emailDeParaClass.php';
$edp = new EmailDePara();
$retorno2 = $edp->marcarComoLida($_REQUEST['idEmail'],$_SESSION['seqCadast']);

//Ler novas
include_once 'controller/emailController.php';
$ec = new emailController();
$retorno = $ec->listaEmail(1,$_SESSION['seqCadast']);
$totalEmails=0;
$totalEmailsNovos=0;
if($retorno)
{
	$totalEmails = count($retorno);
}
if($retorno)
{
	foreach($retorno as $vetor)
	{

		if($vetor['nova']==1)
		{
			$totalEmailsNovos++;
		}
	}
}

//Contar Anexos
include_once 'model/emailAnexoClass.php';
$ea = new emailAnexo();
$retorno3 = $ea->listaEmailAnexo($idEmail);
$totalAnexos=0;
if($retorno3)
{
	$totalAnexos = count($retorno3);
}
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>E-mail Interno</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaEmail">E-mail Interno</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                            <a class="btn btn-block btn-primary compose-mail" href="?corpo=comporEmail">Escrever</a>
                            <div class="space-25"></div>
                            <h5>Pastas</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li><a href="?corpo=buscaEmail"> <i class="fa fa-inbox "></i> Caixa de Entrada <?php if($totalEmailsNovos>0){?><span class="label label-warning pull-right"><?php echo $totalEmailsNovos;?></span><?php }?> </a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=2"> <i class="fa fa-envelope-o"></i> E-mails enviados</a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=3"> <i class="fa fa-exclamation-circle"></i> Importante</a></li>
                                <li><a href="?corpo=buscaEmail&pastaInterna=4"> <i class="fa fa-trash-o"></i> Lixo</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="?corpo=comporEmail&responderEmail=<?php echo $origem; ?>&idEmail=<?php echo $idEmail; ?>" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Responder"><i class="fa fa-reply"></i> Responder</a>
                    <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Imprimir" onclick="imprimirEmail('<?php echo $_REQUEST['idEmail'];?>','<?php echo $vinculoEmailEnviado;?>');"><i class="fa fa-print"></i> </a>
                    <a href="#" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mover para a lixeira" onclick="moverMensagemParaLixeiraEmailUnico('<?php echo $_SESSION['seqCadast']?>','<?php echo $_REQUEST['idEmail'];?>');"><i class="fa fa-trash-o"></i> </a>
                </div>
                <h2>
                    Ver Mensagem
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">

                    <h5>
                        <span class="pull-right font-noraml"><?php echo $data;?></span>
                        <span class="font-noraml">De: </span><?php echo $de;?><br>
                        <span class="font-noraml">Para: </span>
                        <?php 
                        $edp = new EmailDePara();
                        if($vinculoEmailEnviado!=null&&$vinculoEmailEnviado!=0)
                        {
                        	$retorno5 = $edp->selecionarDestinatarios($vinculoEmailEnviado);
                        }else{
                        	$retorno5 = $edp->selecionarDestinatarios($idEmail);
                        }
                        $i=0;
                        if($retorno5)
                        {
                        	foreach($retorno5 as $vetor5)
                        	{
                        		$u = new Usuario();
                        		$retorno6 = $u->buscarIdUsuario($vetor5['para']);
                        		if($retorno6)
                        		{
                        			foreach($retorno6 as $vetor6)
                        			{
                        				if($i==0)
                        				{
                        					echo $vetor6['nomeUsuario'];
                        				}else{
                        					echo ", ".$vetor6['nomeUsuario'];
                        				}
                        				$i++;
                        			}
                        		}
                        	}
                        }
                        ?>.
                     </h5>
                        <br>
                        <h3><span class="font-noraml">Assunto: </span><?php echo $assunto;?></h3>
                    
                </div>
            </div>
                <div class="mail-box">


                <div class="mail-body">
                    <?php echo $mensagem;?>
                </div>
                    <div class="mail-attachment">
                    	<?php if($totalAnexos>0){?>
                        <p>
                            <span><i class="fa fa-paperclip"></i> <?php echo $totalAnexos;?> Anexo(s) </span>
                        </p>

                        <div class="attachment">
                        	<?php 
                        		$ea = new emailAnexo();
								$retorno4 = $ea->listaEmailAnexo($idEmail);
								if($retorno4)
								{
									foreach($retorno4 as $vetor4)
									{
			                        	?>
			                            <div class="file-box">
			                                <div class="file">
			                                    <a href="<?php echo $vetor4['caminho'];?>" target="_blank">
			                                        <span class="corner"></span>
			
			                                        <div class="icon">
			                                            <i class="fa fa-file"></i>
			                                        </div>
			                                        <div class="file-name">
			                                            <?php echo $vetor4['nome_arquivo'];?>
			                                            <br/>
			                                        </div>
			                                    </a>
			                                </div>
			                            </div>
			                            
			                            <?php 
									}    
			                            ?>
                            <?php }
                    		}    
                        ?>
                        <div class="clearfix"></div>
                        </div>
                        </div>
                        <div class="mail-body text-right tooltip-demo">
                                <a class="btn btn-sm btn-white" href="?corpo=comporEmail&responderEmail=<?php echo $origem; ?>&idEmail=<?php echo $idEmail; ?>"><i class="fa fa-reply"></i> Responder</a>
                                <a class="btn btn-sm btn-white" href="?corpo=comporEmail&encaminharEmail=1&idEmail=<?php echo $idEmail; ?>"><i class="fa fa-arrow-right"></i> Encaminhar</a>
                                <button title="" data-placement="top" data-toggle="tooltip" type="button" data-original-title="Imprimir" class="btn btn-sm btn-white" onclick="imprimirEmail('<?php echo $_REQUEST['idEmail'];?>','<?php echo $vinculoEmailEnviado;?>');"><i class="fa fa-print"></i> Imprimir</button>
                                <button title="" data-placement="top" data-toggle="tooltip" data-original-title="Mover para a lixeira" class="btn btn-sm btn-white" onclick="moverMensagemParaLixeiraEmailUnico('<?php echo $_SESSION['seqCadast']?>','<?php echo $_REQUEST['idEmail'];?>');"><i class="fa fa-trash-o"></i> Mover para a lixeira</button>
                        </div>
                        <div class="clearfix"></div>


                </div>
            </div>
        </div>
        