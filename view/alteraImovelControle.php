<?php
include_once("controller/imovelControleController.php");
require_once ("model/imovelControleClass.php");

$icc = new imovelControleController();
$dados = $icc->buscaImovelControle($_GET['idImovelControle']);
?>

			<!-- Conteúdo DE INCLUDE INICIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Imóveis</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="">Início</a>
                        	</li>
	                        <li>
	                            <a href="?corpo=buscaImovelControle">Controle de Documentos de Imóveis</a>
	                        </li>
	                        <li class="active">
	                            <a>Edição de Controle de Documentos de Imóveis</a>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
	            
				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
								    <h5>Edição de Controle de Documentos de Imóveis</h5>
								    <div class="ibox-tools">
                                        <?php
                                        $idOa = $o->listaOrganismo(null,$sessao->getValue("siglaOA"));
                                        if ($idOa) {
                                            foreach ($idOa as $vetoridOa) {
                                                $idOrganismo = $vetoridOa['idOrganismoAfiliado'];
                                            }
                                        }
                                        ?>
								    	<a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $idOrganismo; ?>">
											<i class="fa fa-reply fa-white"></i>&nbsp; Voltar
										</a>
								    </div>
								</div>
								<div class="ibox-content">
									<form class="form-horizontal" method="post" action="acoes/acaoAlterar.php" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-2 control-label">Organismo Afiliado: </label>
										    <div class="col-sm-10">
		                                        <div class="form-control-static" style="padding-top: 5px; padding-bottom: 10px;">
			                                        <?php
														include_once 'controller/organismoController.php';
														$oc = new organismoController();
														$dados2 = $oc->buscaOrganismo($dados->getFk_idOrganismoAfiliado());
													?>
													<?php
														switch ($dados2->getClassificacaoOrganismoAfiliado()) {
			                                                case 1:
			                                                    echo "Loja";
			                                                    break;
			                                                case 2:
			                                                    echo "Pronaos";
			                                                    break;
			                                                case 3:
			                                                    echo "Capítulo";
			                                                    break;
			                                                case 4:
			                                                    echo "Heptada";
			                                                    break;
			                                                case 5:
			                                                    echo "Atrium";
			                                                    break;
			                                            }
		                                            ?>
		                                            <?php
			                                            switch ($dados2->getTipoOrganismoAfiliado()) {
			                                                case 1:
			                                                    echo "R+C";
			                                                    break;
			                                                case 2:
			                                                    echo "TOM";
			                                                    break;
			                                            }
			                                        ?>
		                                            <?php echo $dados2->getNomeOrganismoAfiliado();?> - <?php echo $dados2->getSiglaOrganismoAfiliado();?>
			                                    </div>
		                                    </div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Imóvel: </label>
										    <div class="col-sm-10">
										    	<div class="form-control-static" style="padding-top: 5px; padding-bottom: 10px;">
										    	<?php 
												include_once("controller/imovelController.php");
												require_once ("model/imovelClass.php");
												
												$ic = new imovelController();
												$dados3 = $ic->buscaImovel($dados->getFk_idImovel());
												?>
												<?php echo $dados3->getEnderecoImovel().' - '.$dados3->getNumeroImovel().' - '.$dados3->getComplementoImovel();?>
												</div>
		                                    </div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Referente ao Ano: </label>
										    <div class="col-sm-10">
												<div class="form-control-static" style="padding-top: 5px; padding-bottom: 10px;">
													<?php echo $dados->getReferenteAnoImovelControle();?>
												</div>
		                                    </div>
										</div>
				                        <hr>
				                        <div class="form-group form-inline">
											<label class="col-sm-2 control-label">IPTU: </label>
										    <div class="col-sm-10">
												<select class="form-control col-sm-3" name="propriedadeIptuImovelControle" id="propriedadeIptuImovelControle" style="max-width: 250px" required="required">
				                                    <option>Selecione...</option>
												    <option value="0" <?php if ($dados->getPropriedadeIptuImovelControle()==0){ echo 'selected';};?>>Pago</option>
												    <option value="1" <?php if ($dados->getPropriedadeIptuImovelControle()==1){ echo 'selected';};?>>Em debito</option>
												    <option value="2" <?php if ($dados->getPropriedadeIptuImovelControle()==2){ echo 'selected';};?>>Isento</option>
												    <option value="3" <?php if ($dados->getPropriedadeIptuImovelControle()==3){ echo 'selected';};?>>Imune</option>
												    <option value="4" <?php if ($dados->getPropriedadeIptuImovelControle()==4){ echo 'selected';};?>>Outro</option>
										    	</select>
		                                    </div>
										</div>
										<div class="form-group" id="propriedadeIptuObs">
                                        	<label class="col-sm-2 control-label">Obs: </label>
                                        	<div class="col-sm-10">
                                            	<div style="border: #ccc solid 1px; max-width: 800px">
	                                                <div class="mail-text h-200">
														<textarea class="summernote" id="propriedadeIptuObsImovelControle" name="propriedadeIptuObsImovelControle"><?php echo $dados->getPropriedadeIptuObsImovelControle();?></textarea>
													</div>
												</div>
                                        	</div>
                                        </div>
                                        <!--
										<div class="form-group form-inline" id="propriedadeIptuAnexo">
											<label class="col-sm-2 control-label" id="labelPropriedadeIptuAnexo">Anexar IPTU: </label>
										    <div class="col-sm-10">
												<input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="10000000" />
												<input name="propriedadeIptuAnexoImovelControle" id="propriedadeIptuAnexoImovelControle" type="file" />
		                                    </div>
		                                    <div id="propriedadeIptuAnexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
		                                    <br>
		                                    <br>
										</div>
										-->
										<hr>
										<div class="panel-body">
			                                <div class="panel-group" id="accordion">
			                                    <div class="panel panel-default">
			                                        <div class="panel-heading">
			                                            <h5 class="panel-title">
			                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			                                                	<center>Construção</center>																
			                                                </a>
			                                            </h5>
			                                        </div>
			                                        <div id="collapseOne" class="panel-collapse collapse">
			                                            <div class="panel-body">
			                                                <div class="form-group"><label class="col-sm-2 control-label">Houve Construção? </label>
															    <div class="col-sm-10">
																	<select class="form-control col-sm-3" name="construcaoImovelControle" id="construcaoImovelControle" style="max-width: 250px" required="required">
                                                                        <option value="1" <?php if ($dados->getConstrucaoImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        <option value="0" <?php if ($dados->getConstrucaoImovelControle()==0){ echo 'selected';};?>>Sim</option>
															    	</select>
							                                    </div>
															</div>
                                                            <div id="construcao">
                                                                <!--
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Possui Alvará de Construção: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoAlvaraImovelControle" id="construcaoAlvaraImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getConstrucaoAlvaraImovelControle()==0){ echo 'selected';};?>>Sim</option>
                                                                            <option value="1" <?php if ($dados->getConstrucaoAlvaraImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group form-inline" id="construcaoAlvaraAnexo"><label class="col-sm-2 control-label">Anexar Alvará: </label>
                                                                    <div class="col-sm-10">
                                                                        <input name="construcaoAlvaraAnexoImovelControle" id="construcaoAlvaraAnexoImovelControle" type="file" />
                                                                    </div>
                                                                    <div id="construcaoAlvaraAnexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                                                                </div>
                                                                -->
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Restrições na Construção: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoRestricoesImovelControle" id="construcaoRestricoesImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getConstrucaoRestricoesImovelControle()==0){ echo 'selected';};?>>Sim</option>
                                                                            <option value="1" <?php if ($dados->getConstrucaoRestricoesImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="construcaoRestricoesAnotacoes">
                                                                    <label class="col-sm-2 control-label">Quais São Elas? </label>
                                                                    <div class="col-sm-10">
                                                                    	<div style="border: #ccc solid 1px; max-width: 800px">
							                                                <div class="mail-text h-200">
																				<textarea class="summernote" id="construcaoRestricoesAnotacoesImovelControle" name="construcaoRestricoesAnotacoesImovelControle"><?php echo $dados->getConstrucaoRestricoesAnotacoesImovelControle();?></textarea>
																			</div>
																		</div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Construção Concluída: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoConcluidaImovelControle" id="construcaoConcluidaImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getConstrucaoConcluidaImovelControle()==0){ echo 'selected';};?>>Sim</option>
                                                                            <option value="1" <?php if ($dados->getConstrucaoConcluidaImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="datapicker_termino_construcao">
                                                                    <label class="col-sm-2 control-label" id="datapicker_termino_construcao_label">Data/Prazo de Término da Construção:</label>
                                                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                        <input value="<?php echo $dados->getConstrucaoTerminoDataImovelControle();?>" onkeypress="valida_data(this,'construcaoTerminoDataImovelControle')" maxlength="10" name="construcaoTerminoDataImovelControle" id="construcaoTerminoDataImovelControle" type="text" class="form-control" style="max-width: 105px">
                                                                    	<div hidden id="data_invalida_construcaoTerminoDataImovelControle" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                                    </div>
                                                                </div>
                                                                <!--
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Possui INSS da Construção: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoInssImovelControle" id="construcaoInssImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getConstrucaoInssImovelControle()==0){ echo 'selected';};?>>Sim</option>
                                                                            <option value="1" <?php if ($dados->getConstrucaoInssImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group form-inline" id="construcaoInssAnexo"><label class="col-sm-2 control-label">Anexar INSS: </label>
                                                                    <div class="col-sm-10">
                                                                        <input name="construcaoInssAnexoImovelControle" id="construcaoInssAnexoImovelControle" type="file" />
                                                                    </div>
                                                                    <div id="construcaoInssAnexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                                                                </div>
                                                                -->
                                                            </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="panel panel-default">
			                                        <div class="panel-heading">
			                                            <h4 class="panel-title">
			                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><center>Manutenção</center></a>
			                                            </h4>
			                                        </div>
			                                        <div id="collapseTwo" class="panel-collapse collapse">
			                                            <div class="panel-body">
			                                                <div class="form-group"><label class="col-sm-2 control-label">Houve Manutenção? </label>
															    <div class="col-sm-10">
																	<select class="form-control col-sm-3" name="manutencaoImovelControle" id="manutencaoImovelControle" style="max-width: 250px" required="required">
                                                                        <option value="1" <?php if ($dados->getConstrucaoInssImovelControle()==1){ echo 'selected';};?>>Não</option>
																	    <option value="0" <?php if ($dados->getConstrucaoInssImovelControle()==0){ echo 'selected';};?>>Sim</option>
															    	</select>
							                                    </div>
															</div>
                                                            <div id="manutencao">
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Pintura: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoPinturaImovelControle" id="manutencaoPinturaImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getManutencaoPinturaImovelControle()==0){ echo 'selected';};?>>Sim</option>
                                                                            <option value="1" <?php if ($dados->getManutencaoPinturaImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="manutencaoPinturaTipo"><label class="col-sm-2 control-label">Periodicidade: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoPinturaTipoImovelControle" id="manutencaoPinturaTipoImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getManutencaoPinturaTipoImovelControle()==0){ echo 'selected';};?>>Anual</option>
                                                                            <option value="1" <?php if ($dados->getManutencaoPinturaTipoImovelControle()==1){ echo 'selected';};?>>Bienal</option>
                                                                            <option value="2" <?php if ($dados->getManutencaoPinturaTipoImovelControle()==2){ echo 'selected';};?>>Trienal</option>
                                                                            <option value="3" <?php if ($dados->getManutencaoPinturaTipoImovelControle()==3){ echo 'selected';};?>>Quadrienal</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Reparo de Hidráulica: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoHidraulicaImovelControle" id="manutencaoHidraulicaImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getManutencaoHidraulicaImovelControle()==0){ echo 'selected';};?>>Sim</option>
                                                                            <option value="1" <?php if ($dados->getManutencaoHidraulicaImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="manutencaoHidraulicaTipo"><label class="col-sm-2 control-label">Periodicidade: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoHidraulicaTipoImovelControle" id="manutencaoHidraulicaTipoImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getManutencaoHidraulicaTipoImovelControle()==0){ echo 'selected';};?>>Anual</option>
                                                                            <option value="1" <?php if ($dados->getManutencaoHidraulicaTipoImovelControle()==1){ echo 'selected';};?>>Bienal</option>
                                                                            <option value="2" <?php if ($dados->getManutencaoHidraulicaTipoImovelControle()==2){ echo 'selected';};?>>Trienal</option>
                                                                            <option value="3" <?php if ($dados->getManutencaoHidraulicaTipoImovelControle()==3){ echo 'selected';};?>>Quadrienal</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Reparo de Elétrica: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoEletricaImovelControle" id="manutencaoEletricaImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getManutencaoEletricaImovelControle()==0){ echo 'selected';};?>>Sim</option>
                                                                            <option value="1" <?php if ($dados->getManutencaoEletricaImovelControle()==1){ echo 'selected';};?>>Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="manutencaoEletricaTipo"><label class="col-sm-2 control-label">Periodicidade: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoEletricaTipoImovelControle" id="manutencaoEletricaTipoImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0" <?php if ($dados->getManutencaoEletricaTipoImovelControle()==0){ echo 'selected';};?>>Anual</option>
                                                                            <option value="1" <?php if ($dados->getManutencaoEletricaTipoImovelControle()==1){ echo 'selected';};?>>Bienal</option>
                                                                            <option value="2" <?php if ($dados->getManutencaoEletricaTipoImovelControle()==2){ echo 'selected';};?>>Trienal</option>
                                                                            <option value="3" <?php if ($dados->getManutencaoEletricaTipoImovelControle()==3){ echo 'selected';};?>>Quadrienal</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="panel panel-default">
			                                        <div class="panel-heading">
			                                            <h4 class="panel-title">
			                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><center>Anotações Complementares</center></a>
			                                            </h4>
			                                        </div>
			                                        <div id="collapseThree" class="panel-collapse collapse in">
			                                            <div class="panel-body">
			                                                <div style="border: #ccc solid 1px">
				                                                <div class="mail-text h-200">
																	<textarea class="summernote" id="anotacoesImovelControle" name="anotacoesImovelControle"><?php echo $dados->getAnotacoesImovelControle();?></textarea>
																</div>
															</div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										<hr>
										<div class="form-group">
										    <div class="col-sm-4 col-sm-offset-2">
										    	<input type="hidden" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" class="form-control" value="<?php echo $dados->getFk_idOrganismoAfiliado(); ?>">
										    	<input type="hidden" name="fk_idImovel" id="fk_idImovel" class="form-control" value="<?php echo $dados->getFk_idImovel(); ?>">
                                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
										    	<input type="hidden" name="referenteAnoImovelControle" id="referenteAnoImovelControle" class="form-control" value="<?php echo $dados->getReferenteAnoImovelControle(); ?>">
										    	<input type="hidden" name="seqCadast" id="seqCadast" class="form-control" value="<?php echo $sessao->getValue("seqCadast"); ?>">
										    	<input type="hidden" name="idImovelControle" id="idImovelControle" class="form-control" value="<?php echo $_GET['idImovelControle']; ?>" />
										        <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar"" />
										        <a href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $idOrganismo; ?>" class="btn btn-white">Cancelar</a>
										    </div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->

				<!-- Conteúdo DE INCLUDE FIM -->
				
				