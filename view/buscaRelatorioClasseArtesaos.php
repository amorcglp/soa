<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/usuarioController.php");
$um     = new Usuario();

require_once ("controller/organismoController.php");
$oc     = new organismoController();

include_once("model/relatorioClasseArtesaosAssinadoClass.php");
$rcaa = new relatorioClasseArtesaosAssinado();

/*
$anexo 	= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;
*/
/**
 * Upload do Relatorio
 */
/*
$relatorioAssinado=0;
$extensaoNaoValida = 0;
if($anexo != "")
{
	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo']['name'], '.');
                if ($extensao == ".pdf" ||
                        $extensao == ".png" ||
                        $extensao == ".jpg" ||
                        $extensao == ".jpeg" ||
                        $extensao == ".PDF" ||
                        $extensao == ".PNG" ||
                        $extensao == ".JPG" ||
                        $extensao == ".JPEG"
                ) {
                    $proximo_id = $rca->selecionaUltimoId();
                    $caminho = "img/relatorio_classe_artesaos/" . basename($_POST["idRelatorioUpload"].".relatorio.classe.artesaos.".$proximo_id);
                    $rca->setFk_idRelatorioClasseArtesaos($_POST["idRelatorioUpload"]);
                    $rca->setCaminho($caminho);
                    if($rca->cadastroRelatorioClasseArtesaosAssinado())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/relatorio_classe_artesaos/';
                            $uploadfile = $uploaddir . basename($_POST["idRelatorioUpload"].".relatorio.classe.artesaos.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            {
                                    $relatorioAssinado = 1;
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o relatorio assinado!');</script>";
                            }
                    }
                } else {
                    $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}
*/

if($relatorioAssinado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Relatório Assinado enviado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php 
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php
}

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Relatório da Classe dos Artesãos salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Relatório da Classe dos Artesãos já cadastrado anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório da Classe dos Artesãos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaRelatorioClasseArtesaos">Relatório</a>
            </li>
            <li class="active">
                <strong><a>Relatório da Classe dos Artesãos</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->
<br>
<div class="alert alert-warning alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <a class="alert-link">Atenção:</a> Só preencha este relatório se no seu Organismo conter <a class="alert-link">classe de Artesãos</a>. A GLP não cobra a entrega dos relatórios de <a class="alert-link">Janeiro e Fevereiro</a>. A entrega desses relatórios é <a class="alert-link">facultativa</a>.<br>
</div>
<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Relatórios da Classe dos Artesãos</h5>
                    <div class="ibox-tools">
                    	<?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroRelatorioClasseArtesaos">
                            <i class="fa fa-plus"></i> Novo Relatório
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th><center>Mês/Ano Competência</center></th>
                                <th>Organismo</th>
                                <th><center>Data do Encontro</center></th>
                                <th><center>Arquivos entregues</center></th>
                                <th>
                                    <center>Data de entrega (Ass. Eletrônica) </center>
                                </th>
                                <th>
                                    <center>Assinaturas</center>
                                </th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/relatorioClasseArtesaosController.php");

                            $rca = new relatorioClasseArtesaos();
                            $resultado = $rca->listaRelatorioClasseArtesaos($idOrganismoAfiliado);
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1: $classificacao = "Loja"; break;
                                        case 2: $classificacao = "Pronaos"; break;
                                        case 3: $classificacao = "Capítulo"; break;
                                        case 4: $classificacao = "Heptada"; break;
                                        case 5: $classificacao = "Atrium"; break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1: $tipo = "R+C"; break;
                                        case 2: $tipo = "TOM"; break;
                                    }
                                    
                                    $resposta3 = $rcaa->listaRelatorioClasseArtesaosAssinado($vetor['idRelatorioClasseArtesaos']);
                                    if($resposta3) {
                                    	$total = count($resposta3);
                                    } else {
                                    	$total = 0;
                                    }
                                    ?>
                                    <?php if((strtoupper($vetor["siglaOrganismoAfiliado"])==strtoupper($sessao->getValue("siglaOA")))||(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2))){?>
                                    <tr>
                                        <td>
                                            <center>
                                                <?php echo $vetor['mesCompetencia'] . "/" . $vetor['anoCompetencia']; ?>
                                            </center>
                                        </td>
                                        <td>
                                            <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                        </td>
                                        <td>
                                <center>
                                    <?php echo substr($vetor['dataEncontro'], 8, 2) . "/" . substr($vetor['dataEncontro'], 5, 2) . "/" . substr($vetor['dataEncontro'], 0, 4); ?>
                                </center>
                                </td>
                                <td>
                                    <center><?php if($total>0){?><button class="btn btn-info  dim" type="button" onclick="listaUploadRelatorioClasseArtesaos('<?php echo $vetor['idRelatorioClasseArtesaos']; ?>')" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"><i class="fa fa-paste"></i> </button><?php }else{ echo "Não enviado";}?></center>
                                </td>
                                        <td>
                                            <div id="pronto<?php echo $vetor['idRelatorioClasseArtesaos']; ?>">
                                                <center>
                                                    <?php if($vetor['entregue']==1){?>
                                                        <?php echo substr($vetor['dataEntrega'],8,2);?>/<?php echo substr($vetor['dataEntrega'],5,2);?>/<?php echo substr($vetor['dataEntrega'],0,4);?> às <?php echo substr($vetor['dataEntrega'],11,8);?>
                                                    <?php }else{?>
                                                        <b>Não Informado</b>
                                                    <?php }?>
                                                </center>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <center>
                                                    <?php if($vetor['entregueCompletamente']==1){?>
                                                        <?php echo substr($vetor['dataEntregouCompletamente'],8,2);?>/<?php echo substr($vetor['dataEntregouCompletamente'],5,2);?>/<?php echo substr($vetor['dataEntregouCompletamente'],0,4);?> às <?php echo substr($vetor['dataEntregouCompletamente'],11,8);?>
                                                    <?php }else{?>
                                                        <b>Faltam assinaturas</b>
                                                    <?php }?>
                                                </center>
                                            </div>
                                        </td>
                                <td>
                                <center>
                                    <?php
                                    $mestre = $rca->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($vetor['idRelatorioClasseArtesaos'],365);
                                    $secretario = $rca->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($vetor['idRelatorioClasseArtesaos'],367);

                                    /*
                                    $pjd = $rca->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($vetor['idRelatorioClasseArtesaos'],371);
                                    $sjd = $rca->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($vetor['idRelatorioClasseArtesaos'],373);
                                    $tjd = $rca->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($vetor['idRelatorioClasseArtesaos'],375);
                                    $ma = $rca->buscarAssinaturasEmRelatorioClasseArtesaosPorDocumento($vetor['idRelatorioClasseArtesaos'],377);
                                    */
                                    //Montar Faltam assinar
                                    $arr=array();
                                    if(!$mestre)
                                    {
                                        $arr[]="MESTRE DO ORGANISMO AFILIADO";
                                    }
                                    if(!$secretario)
                                    {
                                        $arr[]="SECRETÁRIO DO ORGANISMO AFILIADO";
                                    }

                                    /*
                                    if(!$sjd)
                                    {
                                        $arr[]="SECRETÁRIO DA JUNTA DEPOSITÁRIA";
                                    }
                                    if(!$tjd)
                                    {
                                        $arr[]="TESOUREIRO DA JUNTA DEPOSITÁRIA";
                                    }
                                    if(!$ma)
                                    {
                                        $arr[]="MESTRE AUXILIAR DO ORGANISMO AFILIADO";
                                    }
                                    */
                                    $oficiaisFaltamAssinar = implode(",",$arr);
                                    ?>
                                    <!--
                                    <button type="button" class="btn btn-sm btn-info" onclick="mostraDetalhesTermoVoluntariado('<?php echo $vetor['idTermoVoluntariado']; ?>');" data-toggle="modal" data-target="#mySeeDetalhes" data-toggle="tooltip" data-placement="left" title="Detalhes do Termo de Voluntariado">
                                        <i class="fa fa-search-plus fa-white"></i>&nbsp;
                                        Detalhes
                                    </button>
                                    -->
                                    <?php if(in_array("2",$arrNivelUsuario)&&$vetor['entregueCompletamente']==0){?>
                                    <a href="?corpo=alteraRelatorioClasseArtesaos&idrelatorio=<?php echo $vetor['idRelatorioClasseArtesaos']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar">
                                        <i class="fa fa-edit fa-white"></i>&nbsp;
                                        Editar
                                    </a>
                                    <?php }?>
                                    <button type="button" class="btn btn-sm btn-success" onclick="abrirPopUpRelatorioClasseArtesaos('<?php echo $vetor['idRelatorioClasseArtesaos']; ?>');" data-toggle="tooltip" data-placement="left" title="Imprimir">
                                        <i class="fa fa-print fa-white"></i>&nbsp;
                                        Imprimir
                                    </button>
                                    <!--
                                    <?php //if(in_array("4",$arrNivelUsuario)){?>
                                    <button type="button" class="btn btn-sm btn-warning" onclick="document.getElementById('idRelatorioUpload').value=<?php echo $vetor['idRelatorioClasseArtesaos']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload">
                                        <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                        Enviar Relatório Assinado
                                    </button>
                                    <?php //}?>

                                    <?php //if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                            <a class="btn btn-sm btn-danger" href="#" onclick="abreModalNotificacao(<?php echo $vetor['fk_idOrganismoAfiliado'];?>,null)">
                                                <i class="fa fa-envelope icon-white"></i>
                                                Notificar
                                            </a>
                                    <?php //} ?>
                                    -->
                                    <?php if($vetor['entregue']==0&&!$usuarioApenasLeitura){?>
                                    <a class="btn btn-sm btn-warning" href="#" id="entregar<?php echo $vetor['idRelatorioClasseArtesaos']; ?>"
                                       onclick="entregarRelatorioClasseArtesaos('<?php echo $vetor['idRelatorioClasseArtesaos']; ?>','<?php echo $_SESSION['seqCadast']; ?>')">
                                        <i class="fa fa-location-arrow icon-white"></i>
                                        Entregar com Ass. Eletrônica
                                    </a>
                                    <?php }?>
                                    <?php
                                    if($vetor['entregueCompletamente']==0) {
                                    ?>
                                    <button type="button"
                                            id="faltaAssinar<?php echo $vetor['idRelatorioClasseArtesaos']; ?>"
                                            class="btn btn-default" data-container="body"
                                            data-toggle="popover" data-placement="top"
                                            data-content="<?php if($mestre&&$secretario){?>Todos Assinaram. Processo de entrega finalizado!<?php } else { ?>Faltam assinar: <?php echo $oficiaisFaltamAssinar;}?>
                                                                " data-original-title="" title=""
                                            aria-describedby="popover408083"
                                            style="display: <?php if ($vetor['entregue'] == 0) { ?>none<?php } else { ?>block<?php } ?>">
                                        <i class="fa fa-pencil"></i> Acompanhar assinaturas
                                    </button>
                                    <?php
                                    }else{
                                    ?>
                                    <span class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Documento 100% entregue</span>
                                    <?php
                                      }
                                    ?>
                                </center>
                                </td>
                                </tr>
                            <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Detalhes da Ata -->
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Relatório Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="termo" name="termo" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="idRelatorioUpload" name="idRelatorioUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.termo.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Relatório Assinado</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php 
modalNotificacao($sessao->getValue("seqCadast"));
?>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
