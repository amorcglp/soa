<!-- Conteúdo DE INCLUDE INICIO -->
<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("model/listaMembrosAtividadeIniciaticaAssinadaClass.php");
$lma = new listaMembrosAtividadeIniciaticaAssinada();

include_once("model/listaOficiaisAtividadeIniciaticaAssinadaClass.php");
$loa = new listaOficiaisAtividadeIniciaticaAssinada();

/**
 * Upload da lista de membros assinada
 */
$anexo 	= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;

$listaMembrosAssinada=0;
$extensaoNaoValida = 0;
if($anexo != "")
{
    //echo "aquiiiiiii ooooooo braz...";
	if ($_FILES['anexo']['name'] != "") {
            if(substr_count($_FILES['anexo']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo']['name'], '.');
                if ($extensao == ".pdf" ||
                        $extensao == ".png" ||
                        $extensao == ".jpg" ||
                        $extensao == ".jpeg" ||
                        $extensao == ".PDF" ||
                        $extensao == ".PNG" ||
                        $extensao == ".JPG" ||
                        $extensao == ".JPEG"
                ) {
                    //echo "vai carregar os dados para o cadastro";
                    $proximo_id = $lma->selecionaUltimoId();
                    $caminho = "img/lista_membros_atividade_iniciatica/" . basename($_POST["idListaMembrosUpload"].".lista.membros.atividade.iniciatica.".$proximo_id);
                    $lma->setFk_idAtividadeIniciatica($_POST["idListaMembrosUpload"]);
                    $lma->setCaminho($caminho);
                    if($lma->cadastroListaMembrosAssinada())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/lista_membros_atividade_iniciatica/';
                            $uploadfile = $uploaddir . basename($_POST["idListaMembrosUpload"].".lista.membros.atividade.iniciatica.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
                            {
                                    $listaMembrosAssinada = 1;
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar a lista de membros assinada!');</script>";
                            }
                    }
                } else {
                    $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}

if($listaMembrosAssinada==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Lista de Membros assinada enviada com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php
}

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }

$anexo2 	= isset($_FILES['anexo2']['name'])?$_FILES['anexo2']['name']:null;

/**
 * Upload da lista de oficiais assinada
 */
$listaOficiaisAssinada=0;
$extensaoNaoValida = 0;
if($anexo2 != "")
{
    //echo "aqui ooooooooooooooooooooooooooooooo";
	if ($_FILES['anexo2']['name'] != "") {
            if(substr_count($_FILES['anexo2']['name'],".")==1)
            {
                $extensao = strstr($_FILES['anexo2']['name'], '.');
                if ($extensao == ".pdf" ||
                        $extensao == ".png" ||
                        $extensao == ".jpg" ||
                        $extensao == ".jpeg" ||
                        $extensao == ".PDF" ||
                        $extensao == ".PNG" ||
                        $extensao == ".JPG" ||
                        $extensao == ".JPEG"
                ) {
                    $proximo_id = $loa->selecionaUltimoId();
                    $caminho = "img/lista_oficiais_atividade_iniciatica/" . basename($_POST["idListaOficiaisUpload"].".lista.oficiais.atividade.iniciatica.".$proximo_id);
                    //echo $caminho;
                    $loa->setFk_idAtividadeIniciatica($_POST["idListaOficiaisUpload"]);
                    $loa->setCaminho($caminho);
                    if($loa->cadastroListaOficiaisAssinada())
                    {
                            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/lista_oficiais_atividade_iniciatica/';
                            $uploadfile = $uploaddir . basename($_POST["idListaOficiaisUpload"].".lista.oficiais.atividade.iniciatica.".$proximo_id);
                            if(move_uploaded_file($_FILES['anexo2']['tmp_name'], $uploadfile))
                            {
                                    $listaOficiaisAssinada = 1;
                            }else{
                                    echo "<script type='text/javascript'>alert('Erro ao enviar o juramento assinado!');</script>";
                            }
                    }
                } else {
                    $extensaoNaoValida = 1;
            }
        }else{
            $extensaoNaoValida = 2;
        }
    }
}

if($listaOficiaisAssinada==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Lista de Oficiais assinada enviada com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php
}

if ($extensaoNaoValida == 1) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php
}

if ($extensaoNaoValida == 2) {
    ?>
    <script>
        window.onload = function () {
            swal({
                title: "Aviso!",
                text: "Por favor retire todos os pontos, acentos e espaços do nome do arquivo e tente enviar novamente.",
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
        }
    </script>
<?php }
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades Iniciáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Atividades Iniciáticas Cadastradas em Modo Ágil</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<?php
/*
if($sessao->getValue("fk_idDepartamento")==1 || $sessao->getValue("fk_idDepartamento")==3) {
    ?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Atividades Iniciáticas</h5>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="alert alert-success">
                            <center>
                                Módulo ainda não disponível. <br>
                                <a class="alert-link">Calma! Falta pouco :]</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
*/
include_once("controller/atividadeIniciaticaController.php");
include_once("controller/atividadeIniciaticaOficialController.php");
include_once("controller/organismoController.php");
include_once("controller/atividadeIniciaticaColumbaController.php");

$aim = new atividadeIniciatica();
$aic = new atividadeIniciaticaController();
$aioc = new atividadeIniciaticaOficialController();
$oc = new organismoController();
$om = new organismo();
$sessao = new criaSessao();
$aicc = new atividadeIniciaticaColumbaController();

if($sessao->getValue("alerta-atividade-iniciatica-agil")==null){
    if (($funcoesUsuarioString == 0 && $_SESSION['fk_idDepartamento'] != 1) || $regiaoUsuario != null || $_SESSION['fk_idDepartamento'] == 2 || $_SESSION['fk_idDepartamento'] == 3) {
        ?>
        <script>
            window.onload = function(){
                swal({
                    title: "Aviso!",
                    text: "Você pode mudar o organismo listado no cabeçalho!",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
        <?php
    }
    $sessao->setValue("alerta-atividade-iniciatica-agil", true);
}

$idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';

if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
    $idOrg = $idOrganismo;
} else {
    $idOrg = $idOrganismoAfiliado;
}

?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Atividades Iniciáticas Cadastradas em Modo Ágil
                        <?php
                        if($idOrganismo!=''){
                            $nomeOa = retornaNomeCompletoOrganismoAfiliado($idOrganismo);
                            echo ' - '.$nomeOa;
                        }
                        ?>
                    </h5>
                    <div class="ibox-tools">
                        <?php if($idOrganismo!=''){ ?>
                            <?php if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){ ?>
                            <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciaticaAgil">
                                <i class="fa fa-undo"></i> Eliminar Filtro
                            </a>
                            <?php } ?>
                        <?php } ?>
                        <!--
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciaticaColumba&idOrganismoAfiliado=<?php // echo $idOrganismoAfiliado; ?>">
                            <i class="fa fa-share"></i> Columbas
                        </a>
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciaticaOficial&idOrganismoAfiliado=<?php // echo $idOrganismoAfiliado; ?>">
                            <i class="fa fa-share"></i> Equipes Iniciáticas
                        </a>
                        -->
                        <!--
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciaticaMembro&idOrganismoAfiliado=<?php echo $idOrganismoAfiliado; ?>">
                            <i class="fa fa-users"></i> Verificar pendências de membro
                        </a>
                        -->
                        <a class="btn btn-xs btn-primary" href="?corpo=buscaAtividadeIniciatica">
                            <i class="fa fa-share fa-white"></i> Atividades Iniciáticas Agendadas
                        </a>
                        <?php if(!$usuarioApenasLeitura){ ?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroAtividadeIniciaticaAgil">
                            <i class="fa fa-plus"></i> Agendar Nova
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <?php if($idOrg==''){ ?>
                                    <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                        <th>Organismo</th>
                                    <?php } ?>
                                <?php } ?>
                                <th>Grau</th>
                                <th><center>Data</center></th>
                                <th><center>Registrado em</center></th>
                        		<th><center>Status</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                        <?php
                        $resultado = $aic->listaAtividadeIniciatica($idOrganismoAfiliado,null,1, null,  null, null, false);
                        if ($resultado) {
                            foreach ($resultado as $vetor) {
                                if($vetor['statusAtividadeIniciatica'] != 3){
                            ?>
                            <tr>
                                <td>
                                    <?php echo $vetor['idAtividadeIniciatica']; ?>
                                </td>
                                <?php if ($idOrg == '') { ?>
                                    <?php if (($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)) { ?>
                                        <td>
                                            <?php // if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                            <!--<a class="btn-xs btn-primary btn-outline" href="?corpo=buscaAtividadeIniciatica&idOrganismoAfiliado=<?php // echo $vetor['fk_idOrganismoAfiliado']; ?>" data-rel="tooltip" title="">Filtrar</a>-->
                                            <?php // } ?>
                                            <?php
                                            echo retornaNomeCompletoOrganismoAfiliado($vetor['fk_idOrganismoAfiliado']);
                                            ?>
                                        </td>
                                    <?php } ?>
                                <?php } ?>
                                <td>
                                    <?php echo retornaTipoAtividadeParaExibicao($vetor['tipoAtividadeIniciatica']); ?>
                                </td>
                                <td>
                                    <center>
                                        <?php echo substr($vetor['dataRealizadaAtividadeIniciatica'], 8, 2) . "/" . substr($vetor['dataRealizadaAtividadeIniciatica'], 5, 2) . "/" . substr($vetor['dataRealizadaAtividadeIniciatica'], 0, 4); ?>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <?php echo substr($vetor['dataRegistroAtividadeIniciatica'], 8, 2) . "/" . substr($vetor['dataRegistroAtividadeIniciatica'], 5, 2) . "/" . substr($vetor['dataRegistroAtividadeIniciatica'], 0, 4) . " - " . substr($vetor['dataRegistroAtividadeIniciatica'], 11, 2) . ":" . substr($vetor['dataRegistroAtividadeIniciatica'], 14, 2); ?>
                                        <?php if ($vetor['dataAtualizadaAtividadeIniciatica'] != '0000-00-00 00:00:00') { ?>
                                            <br>
                                            <div>
                                                Última atualização em: <br>
                                                <?php echo substr($vetor['dataAtualizadaAtividadeIniciatica'], 8, 2) . "/" . substr($vetor['dataAtualizadaAtividadeIniciatica'], 5, 2) . "/" . substr($vetor['dataAtualizadaAtividadeIniciatica'], 0, 4) . " - " . substr($vetor['dataAtualizadaAtividadeIniciatica'], 11, 2) . ":" . substr($vetor['dataAtualizadaAtividadeIniciatica'], 14, 2); ?>
                                            </div>
                                        <?php } ?>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <div
                                            class="statusTarget<?php echo $vetor['idAtividadeIniciatica'] ?>">
                                            <?php echo retornaStatusAtividadeSpan($vetor['statusAtividadeIniciatica']); ?>
                                            <?php
                                            echo "<br><br>";
                                            if (($sessao->getValue("fk_idDepartamento") == 2) || ($sessao->getValue("fk_idDepartamento") == 3) && ($vetor['statusAtividadeIniciatica'] == 2)){
                                                $organismo = isset($_GET['idOrganismoAfiliado']) ? $_GET['idOrganismoAfiliado'] : 0;
                                            ?>
                                            <a class="btn btn-sm btn-danger"
                                               onclick="cancelarPosteriormenteAtividadeIniciatica(<?php echo $vetor['idAtividadeIniciatica']; ?>,<?php echo $organismo; ?>,1)">
                                                <i class="fa fa-times-circle fa-white"></i>
                                                Cancelar Posteriormente
                                            </a>
                                            <?php } ?>
                                        </div>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                        <?php if (($sessao->getValue("fk_idDepartamento") == 2) || ($sessao->getValue("fk_idDepartamento") == 3)) { ?>
                                            <div class="btn-group">
                                                <button data-toggle="dropdown"
                                                        class="btn btn-warning btn-sm dropdown-toggle"><i
                                                        class="fa fa-share fa-white"></i>&nbsp; Filtrar
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <?php if ($idOrganismo == '') { ?>
                                                        <li>
                                                            <a href="?corpo=buscaAtividadeIniciaticaAgil&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Atividades
                                                                Iniciáticas Cadastradas em Modo Ágil deste
                                                                Organismo</a></li>
                                                        <li class="divider"></li>
                                                    <?php } ?>
                                                    <li>
                                                        <a href="?corpo=buscaAtividadeIniciaticaOficial&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Equipes
                                                            Iniciáticas deste Organismo</a></li>
                                                    <li>
                                                        <a href="?corpo=buscaAtividadeIniciaticaColumba&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Columbas
                                                            deste Organismo</a></li>
                                                    <li>
                                                        <a href="?corpo=buscaAtividadeIniciaticaMembro&idOrganismoAfiliado=<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">Membros
                                                            com Pendências</a></li>
                                                </ul>
                                            </div>
                                        <?php } ?>
                                        <div class="btn-group">
                                            <?php if(!$usuarioApenasLeitura){?>
                                            <button data-toggle="dropdown"
                                                    class="btn btn-warning btn-sm dropdown-toggle"><i
                                                    class="fa fa-share fa-white"></i>&nbsp; Arquivos <span
                                                    class="caret"></span></button>
                                            <?php }?>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" data-target="#mySeeUpload"
                                                       data-toggle="modal" data-placement="left"
                                                       onclick="document.getElementById('idListaMembrosUpload').value=<?php echo $vetor['idAtividadeIniciatica']; ?>">Enviar
                                                        Lista Assinada de Membros</a></li>
                                                <li><a href="#" data-target="#mySeeListaUpload"
                                                       data-toggle="modal" data-placement="left"
                                                       onclick="listaUploadMembrosAtividadeIniciatica('<?php echo $vetor['idAtividadeIniciatica']; ?>')">Listas
                                                        Assinadas de Membros</a></li>
                                                <li><a href="#" data-target="#mySeeUploadOficiais"
                                                       data-toggle="modal" data-placement="left"
                                                       onclick="document.getElementById('idListaOficiaisUpload').value=<?php echo $vetor['idAtividadeIniciatica']; ?>">Enviar
                                                        Lista Assinada de Oficiais</a></li>
                                                <li><a href="#" data-target="#mySeeListaUploadOficiais"
                                                       data-toggle="modal" data-placement="left"
                                                       onclick="listaUploadOficiaisAtividadeIniciatica('<?php echo $vetor['idAtividadeIniciatica']; ?>')">Listas
                                                        Assinadas de Oficiais</a></li>
                                            </ul>
                                        </div>
                                        <a class="btn btn-sm btn-success" href="" data-toggle="modal"
                                           data-target="#modalMembros<?php echo $vetor['idAtividadeIniciatica']; ?>">
                                            <i class="fa fa-users fa-white"></i>
                                            Participantes
                                        </a>
                                    </center>
                                    <input type="hidden" id="tipoObrigracao<?php echo $vetor['idAtividadeIniciatica'] ?>" name="tipoObrigracao<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['tipoAtividadeIniciatica']; ?>">
                                    <input type="hidden" id="dataObrigacao<?php echo $vetor['idAtividadeIniciatica'] ?>" name="dataObrigacao<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['dataRealizadaAtividadeIniciatica']."T".$vetor['horaRealizadaAtividadeIniciatica']; ?>">
                                    <input type="hidden" id="descricaoLocal<?php echo $vetor['idAtividadeIniciatica'] ?>" name="descricaoLocal<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['localAtividadeIniciatica']; ?>">
                                    <input type="hidden" id="fk_idOrganismoAfiliado<?php echo $vetor['idAtividadeIniciatica'] ?>" name="fk_idOrganismoAfiliado<?php echo $vetor['idAtividadeIniciatica'] ?>" value="<?php echo $vetor['fk_idOrganismoAfiliado']; ?>">
                                </td>
                            </tr>
                            <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<?php
$resultado = $aic->listaAtividadeIniciatica($idOrganismoAfiliado,null,1);
if ($resultado) {
    foreach ($resultado as $vetor) {
        ?>
        <!-- Notificar Responsáveis -->
        <div class="modal inmodal fade" id="modalMembros<?php echo $vetor['idAtividadeIniciatica'];?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <i class="fa fa-users modal-icon"></i>
                        <h4 class="modal-title">
                            <?php
                            if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
                                if($idOrganismo==''){
                                    $dados = $oc->buscaOrganismo($vetor['fk_idOrganismoAfiliado']);
                                    $nomeOa = organismoAfiliadoNomeCompleto($dados->getClassificacaoOrganismoAfiliado(),
																																						$dados->getTipoOrganismoAfiliado(),
																																						$dados->getNomeOrganismoAfiliado(),
																																						$dados->getSiglaOrganismoAfiliado());
                                    echo $nomeOa;
                                    echo '<br>';
                                }
                            }
                            ?>
                            Membros Cadastrados na Atividade <?php echo $vetor['idAtividadeIniciatica']; ?>
                        </h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th style="min-width: 200px"><center>Status</center></th>
                                    <th style="min-width: 150px"><center>Ações</center></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $resultadoMembros = $aim->listaAtividadeIniciaticaMembros($vetor['idAtividadeIniciatica']);
                            if ($resultadoMembros) {
                                foreach ($resultadoMembros as $vetorMembros) {
                                    $statusMembro = $vetorMembros['statusAtividadeIniciaticaMembro'];
                                    //$statusMembro = 2;
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $vetorMembros['nomeAtividadeIniciaticaMembro']; ?>
                                        </td>
                                        <td style="min-width: 200px">
                                            <center id="statusMembroAtividade<?php echo $vetorMembros['seqCadastAtividadeIniciaticaMembro'].$vetor['idAtividadeIniciatica']; ?>">
                                                <?php
                                                switch($statusMembro){
                                                    case 0: echo "<span class='badge badge-warning'>Pendente</span>"; break;
                                                    case 1: echo "<span class='badge badge-success'>Confirmado</span>"; break;
                                                    case 2: echo "<span class='badge badge-primary'>Compareceu</span>"; break;
                                                    case 3: echo "<span class='badge badge-danger'>Cancelado</span>"; break;
                                                    case 4: echo "<span class='badge badge-danger' style='max-width: 200px'>".$vetorMembros['mensagemAtividadeIniciaticaMembro']."</span>"; break;
                                                }
                                                ?>
                                            </center>
                                        </td>
                                        <td style="min-width: 150px">
                                            <center id="acoesMembroAtividade<?php echo $vetorMembros['seqCadastAtividadeIniciaticaMembro'].$vetor['idAtividadeIniciatica']; ?>">
                                                <?php
                                                if($statusMembro == 0){
                                                    echo "<a class='btn-xs btn-success' onclick='confirmaAgendamentoMembroAtividadeIniciatica(".$vetorMembros['seqCadastAtividadeIniciaticaMembro'].",".$vetor['idAtividadeIniciatica'].")'>Confirmar</a>
                                                        &nbsp
                                                        <a class='btn-xs btn-danger' onclick='cancelaAgendamentoMembroAtividadeIniciatica(".$vetorMembros['seqCadastAtividadeIniciaticaMembro'].",".$vetor['idAtividadeIniciatica'].")'>Cancelar</a>";
                                                } elseif ($statusMembro == 1){
                                                    echo "<a class='btn-xs btn-danger' onclick='cancelaAgendamentoMembroAtividadeIniciatica(".$vetorMembros['seqCadastAtividadeIniciaticaMembro'].",".$vetor['idAtividadeIniciatica'].")'>Cancelar</a>
                                                        &nbsp
                                                        <a class='btn-xs btn-primary' onclick='confirmaParticipacaoMembroAtividadeIniciatica(".$vetorMembros['seqCadastAtividadeIniciaticaMembro'].",".$vetor['idAtividadeIniciatica'].")'>Presente</a>";
                                                } elseif ($statusMembro == 2){
                                                    echo "--";
                                                } elseif ($statusMembro == 3){
                                                    echo "--";
                                                } elseif ($statusMembro == 4){
																										if ($vetorMembros['mensagemAtividadeIniciaticaMembro'] === "Iniciação já cadastrada!"){
																											echo "--";
																										} else {
                                                    	echo "<a class='btn-xs btn-primary' onclick='atualizarObrigacaoRitualisticaMembroNovamente(".$vetorMembros['seqCadastAtividadeIniciaticaMembro'].",".$vetor['idAtividadeIniciatica'].")'>Tentar Novamente</a>";
																										}
																								} else {
                                                    echo "--";
                                                }
                                                ?>
                                            </center>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            } else {
                            ?>
                                <tr>
                                    <td colspan="3">
                                        <div class="alert alert-danger">
                                            <center>Ainda não há nenhum membro cadastrado nessa atividade!</center>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-sm btn-primary" onclick="abrirPopUpAtividadeIniciaticaMembrosParaAssinatura(<?php echo $vetor['idAtividadeIniciatica']; ?>,<?php echo $vetor['fk_idOrganismoAfiliado']; ?>)">
                            <i class="fa fa-print fa-white"></i>
                            Imprimir
                        </a>

                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<!-- Modal's da página FIM -->
<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio da Lista de Membros Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="membros" name="membros" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>

					</div>
					<input type="hidden" id="idListaMembrosUpload" name="idListaMembrosUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.membros.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeUploadOficiais" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio da Lista de Oficiais Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="oficiais" name="oficiais" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo2"
								id="anexo2" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>

					</div>
					<input type="hidden" id="idListaOficiaisUpload" name="idListaOficiaisUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.oficiais.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Lista de Membros Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUploadOficiais" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Lista de Oficiais Assinada</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUploadOficiais"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- Conteúdo DE INCLUDE FIM -->

<?php // } ?>
