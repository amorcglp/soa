<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2><?php echo $nomeSecaoMenu;?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Index</a>
            </li>
            <li class="active">
                <strong><?php echo $nomeSecaoMenu;?></strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <?php if($msgManutencao!=""){?>
                    <div class="alert alert-info">
                        <h3 class="font-bold"><i class="fa fa-cogs"></i> Em Manutenção</h3><br>
                        <?php //echo $msgManutencao;?>
			<p>Dignos Oficiais, sabemos da importância desta funcionalidade para o bom andamento dos trabalhos administrativos dentro dos Organismos Afiliados, entretanto, as mudanças que fizemos no sistema para melhorar a performance impactaram diretamente na estrutura do sistema que precisou ser refeita.

Estamos na fase final do projeto com foco total. Muito em breve teremos novidades.

Agradecemos a compreensão de todos! Nos colocamos a disposição. Equipe de desenvolvimento do SOA, soa@amorc.org.br</p>
                    </div>
                <?php }else{?>
                    <div class="alert alert-info">
                        <h3 class="font-bold"><i class="fa fa-cogs"></i> Em Manutenção</h3><br>
                        Por favor, caso seja necessário encaminhar as informações por e-mail!
                    </div>
               <?php }?>
            </div>
        </div>
    </div>
</div>