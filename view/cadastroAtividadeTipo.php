<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividade Para Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAnotacoes">Atividade Para Organismos</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Atividade Para Organismos</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Atividade Para Organismos</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeTipo">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoCadastrar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Atividade: </label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeAtividadeTipo" id="nomeAtividadeTipo" class="form-control" value="" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; max-width: 850px">
	                            	<div class="mail-text h-200">
										<textarea class="summernote" id="descricaoAtividadeTipo" name="descricaoAtividadeTipo"></textarea>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top: 30px">
                            <label class="col-sm-2 control-label">Destinado ao O.A.: <br/><small class="text-navy">e frequências mensais</small></label>
                            <div class="col-sm-10">
                                <div class="checkbox i-checks">
                                	<label class="col-md-2" style="padding-left: 0px; max-width: 100px">
										<input type="checkbox" value="" id="lojaAtividadeTipo" name="lojaAtividadeTipo" onclick="showQtdAtividade(this);">
										<i></i> Loja 
									</label>
									<div class="col-md-1" id="qntLojaAtividade">
										<input type="text" placeholder="qtde." name="qntLojaAtividadeTipo" id="qntLojaAtividadeTipo" class="form-control input-sm" value="" style="max-width: 55px">
									</div>
								</div>
								
                                <div class="checkbox i-checks" style="margin-top: 30px">
	                                <label class="col-md-2" style="padding-left: 0px; max-width: 100px"> 
		                                <input type="checkbox" value="" id="pronaosAtividadeTipo" name="pronaosAtividadeTipo"> 
		                                <i></i> Pronaos 
	                                </label>
	                                <div class="col-md-1" id="qntPronaosAtividade">
										<input type="text" placeholder="qtde." name="qntPronaosAtividadeTipo" id="qntPronaosAtividadeTipo" class="form-control input-sm" value="" style="max-width: 55px">
									</div>
                                </div>
                                
                                <div class="checkbox i-checks" style="margin-top: 30px">
	                                <label class="col-md-2" style="padding-left: 0px; max-width: 100px"> 
		                                <input type="checkbox" value="" id="capituloAtividadeTipo" name="capituloAtividadeTipo"> 
		                                <i></i> Capítulo 
	                                </label>
	                                <div class="col-md-1" id="qntCapituloAtividade">
										<input type="text" placeholder="qtde." name="qntCapituloAtividadeTipo" id="qntCapituloAtividadeTipo" class="form-control input-sm" value="" style="max-width: 55px">
									</div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <a class="btn btn-white" href="?corpo=buscaAtividadeTipo"> Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->



<!-- Input Mask-->
<!--<script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>-->

<!-- Data picker -->
    <!--<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>-->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>