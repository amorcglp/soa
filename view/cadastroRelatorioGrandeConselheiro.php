<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<!-- INÍCIO SCRIPT's PRÓPRIOS -->			
<script>

</script>
<!-- FIM SCRIPT's PRÓPRIOS -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório Semestral do Grande Conselheiro</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="">Relatórios</a>
            </li>
            <li class="active">
                <a href="">Relatório Semestral do Grande Conselheiro</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Formulário de criação do Relatório Semestral do Grande Conselheiro</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaRelatorioGrandeConselheiro">
                            <i class="fa fa-reply"></i> Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form name="relatorioGrandeConselheiro" class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" onSubmit="return validaRelatorioGrandeConselheiro()" >
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Regiao:</label>
                            <div class="col-sm-9">
                                <select name="regiao" id="regiao" data-placeholder="Selecione uma regiao..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <?php
                                    include_once 'controller/regiaoRosacruzController.php';
                                    $rrc = new regiaoRosacruzController();
                                    $rrc->criarComboBox(0,substr($sessao->getValue("siglaOA"),0,3));
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-inline">
                            <label class="col-sm-3 control-label">Semestre:</label>
                            <div class="col-sm-9">
                                <select name="semestre" id="semestre" data-placeholder="Selecione um semestre..." class="chosen-select" style="width:350px;" tabindex="2">
                                    <option value="0">Selecione</option>
                                    <option value="1">1º</option>
                                    <option value="2">2º</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-3 control-label">Ano: </label>
                            <div class="col-sm-3">
                                <input class="form-control" maxlength="4" id="ano" name="ano" type="text" value="<?php echo date('Y');?>" style="max-width: 90px">
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(1);"><center>01) O(a) Frater/Soror trabalha com um Plano de Gestão em sua Região?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta1"
														id="pergunta1">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario1" name="comentario1"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"  onclick="fechaOutrasAbasRelatorioGrandeConselheiro(2);"><center>02) Marque os Organismos Afiliados que permanecem sob sua supervisão:<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <?php
                                    include_once 'model/organismoClass.php';
                                    include_once 'lib/functions.php';
                                    $o = new organismo();
                                    $resultado = $o->listaOrganismo(null,null,null,substr($sessao->getValue("siglaOA"),0,3));
                                    ?>
                                    <div id="collapse2" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<center>
													<div style="text-align:left;width: 50%;">
													<?php 
													if($resultado)
													{
														foreach($resultado as $vetor)
														{
															$nomeOrganismo = organismoAfiliadoNomeCompleto($vetor['classificacaoOrganismoAfiliado'],$vetor['tipoOrganismoAfiliado'],$vetor['nomeOrganismoAfiliado'],$vetor['siglaOrganismoAfiliado']); 
													?>
													<input type="checkbox" name="supervisao[]"  value="<?php echo $vetor['idOrganismoAfiliado'];?>" class="supervisao"> <?php echo $nomeOrganismo;?><br>
													<?php 
														}
													}
													?>
													</div>
												</center>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
						<div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(3);"><center>03) Consta em seu Plano a permanente Expansão Rosacruz?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta3"
														id="pergunta3">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario3" name="comentario3"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(4);"><center>04) Desenvolve atividades junto a Comunidade que possam permitir o acesso ao Organismo Afiliado de não-membros?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta4"
														id="pergunta4">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario4" name="comentario4"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(5);"><center>05) Sua Região desenvolve atividades abertas a Não-Membros (Seção 700)? Em quais Organismos Afiliados? Solicitamos informar dias e horários.<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse5" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta5"
														id="pergunta5">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario5" name="comentario5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(6);"><center>06) Como anda a comunicação entre os Organismos Afiliados e o Grande Conselheiro. Você envia regularmente circulares atualizando os assuntos da GLP?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse6" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta6"
														id="pergunta6">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario6" name="comentario6"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(7);"><center>07) O(a) Frater/Soror tem procurado praticar a integração com outras Regiões?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse7" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta7"
														id="pergunta7">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario7" name="comentario7"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse8" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(8);"><center>08) O(a) Frater/Soror tem acompanhado através do "Check List" enviado pela GLP, a atualização do Manual Administrativo?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse8" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta8"
														id="pergunta8">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario8" name="comentario8"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse9" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(9);"><center>09) Os Organismos Afiliados de sua Região tem promovido trabalhos metafísicos visando harmonização e sustentação dos mesmos?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse9" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta9"
														id="pergunta9">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario9" name="comentario9"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse10" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(10);"><center>10) Sua Região tem desenvolvido atividades da Classe dos Artesãos?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse10" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta10"
														id="pergunta10">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario10" name="comentario10"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse11" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(11);"><center>11) Considera o número de membros de sua Região razoável e de acordo com a realidade e especificidades de sua circunscrição?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse11" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta11"
														id="pergunta11">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario11" name="comentario11"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse12" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(12);"><center>12) O(a) Frater/Soror tem um Monitor Regional para cada Organismo Afiliado e se mantém em frequente comunicação com eles?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse12" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta12"
														id="pergunta12">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario12" name="comentario12"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse13" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(13);"><center>13) Marque os Monitores Regionais que lhe auxiliam na sua Região:<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <?php
                                    $ocultar_json=1;
                                    $nomeParcialFuncao='MONITOR';
                                    $atuantes='S';
						            $naoAtuantes='N';
                                    include 'js/ajax/retornaFuncaoMembro.php';
						            $obj = json_decode(json_encode($return),true);
                                    ?>
                                    <div id="collapse13" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<center>
												<div style="text-align:left;width: 50%;">
												<?php 
													foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor)
						                        	{
						                        		if($vetor['fields']['fCodMembro']!=0)
						                        		{
						                        			if(substr($vetor['fields']['fSigOrgafi'],0,3)==substr($sessao->getValue("siglaOA"),0,3)){
												?>
													<input type="checkbox" name="monitores[]" value="<?php echo $vetor['fields']['fSeqCadast'];?>" class="monitores"> <?php echo $vetor['fields']['fNomClient'];?><br>
												<?php 
						                        			}
						                        		}
						                        	}
												?>
												</div>	
												</center>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse14" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(14);"><center>14) Sua Região tem desenvolvido atividades que permitem descobrir potenciais servidores entre os neófitos? Quais?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse14" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta14"
														id="pergunta14">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario14" name="comentario14"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse15" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(15);"><center>15) Está previsto em seu modelo de gestão algum tipo de treinamento Reuniões Preparatórias de Oficiais, Seminários, Reproari, Redig, etc.?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse15" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta15"
														id="pergunta15">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario15" name="comentario15"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse16" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(16);"><center>16) Consta em seu modelo de gestão participar de algumas Assembléias Gerais em Lojas, Capítulos e Pronaos?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse16" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta16"
														id="pergunta16">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario16" name="comentario16"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse17" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(17);"><center>17) Com que frequência o(a) Frater/Soror tem reuniões presenciais com os Monitores Regionais e Oficiais de Organismos Afiliados?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse17" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta17"
														id="pergunta17">
														<option value="0">Selecione</option>
														<option value="1">1 vez por mês</option>
														<option value="2">1 vez por trimestre</option>
														<option value="3">1 vez por semestre</option>
														<option value="4">1 vez por ano</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario17" name="comentario17"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse18" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(18);"><center>18) O(a) Frater/Soror supervisiona os Relatórios Mensais Financeiros e Atas para acompanhamento do desempenho e situação política financeira de seus OAs?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse18" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta18"
														id="pergunta18">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario18" name="comentario18"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse19" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(19);"><center>19) O(a) Frater/Soror tem um Monitor para acompanhar a remessa regular desses documentos? (Controladoria)<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse19" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta19"
														id="pergunta19">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario19" name="comentario19"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse20" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(20);"><center>20) A sua Região tem realizado ERIS? Jornadas Místicas?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse20" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta20"
														id="pergunta20">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario20" name="comentario20"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse21" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(21);"><center>21) A sua Região tem realizado Convenções Regionais?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse21" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta21"
														id="pergunta21">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario21" name="comentario21"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse22" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(22);"><center>22) Em sua Região recentemente houve a Implantação de GDs, fundação de Pronaos, Elevação de Capítulo a Loja? Quais?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse22" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta22"
														id="pergunta22">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario22" name="comentario22"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse23" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(23);"><center>23) O(a) Frater/Soror tem se colocado como um amigo do Organismo Afiliado, evitando posicionamentos radicais, sendo um ponto de apoio e procurando difundir o espírito fraternal entre os Rosacruzes e os Oficiais?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse23" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta23"
														id="pergunta23">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario23" name="comentario23"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse24" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(24);"><center>24) O(a) Frater/Soror tem visitado cada Organismo Afiliado de sua Região, pelo menos uma vez durante o ano?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse24" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta24"
														id="pergunta24">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario24" name="comentario24"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse25" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(25);"><center>25) Como Mestre Provincial o Irmão/Irmã tem participado de Conventículos e Reuniões da TOM?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse25" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta25"
														id="pergunta25">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario25" name="comentario25"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse26" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(26);"><center>26) O(a) Frater/Soror tem se empenhado para que todos os Organismos de sua Região funcionem em harmonia e cordialidade, com estrita observância do Manual Administrativo, Ritualístico e Iniciático?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse26" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta26"
														id="pergunta26">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario26" name="comentario26"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse27" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(27);"><center>27) Verifica se os símbolos tradicionais da Ordem estão sendo respeitados e estão devidamente dispostos nos nossos Templos?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse27" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta27"
														id="pergunta27">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario27" name="comentario27"></textarea>
                                                </div>
                                            </div>
                                            <div id="rascunho1" style="height:14px; text-align:right; margin-right: 10px"></div><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse28" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(28);"><center>28) Quando há violação das normas, diferenças de opinião, alguma irregularidade manifesta no trabalho, decoro ou procedimentos, o(a) Frater/Soror apresenta sugestões e conselhos e serve de mediador na administração de conflitos?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse28" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta28"
														id="pergunta28">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario28" name="comentario28"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse29" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(29);"><center>29) O(a) Frater/Soror faz os ajustes e alterações que julga necessários para resolver a situação e mantém a GLP informada?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse29" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta29"
														id="pergunta29">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario29" name="comentario29"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse30" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(30);"><center>30) O(a) Frater/Soror procura se manter informado sobre a seleção de Oficiais, para o novo Ano Rosacruz? (Obs. Quando julgar necessário, através de carta, telefonema, e-mail ou relatório separado, deverá comunicar-nos seu parecer sobre as recomendações)<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse30" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta30"
														id="pergunta30">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario30" name="comentario30"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse31" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(31);"><center>31) O(a) Frater/Soror cuida para que na apresentação de discursos preparados pelos estudantes rosacruzes, somente sejam esboçados conceitos Rosacruzes e assuntos condizentes com os objetivos e ensinamentos da Ordem? (Obs. Zelar para que não sejam muitos extensos. Não devem ultrapassar uma hora! E sempre nos enviar uma cópia daqueles que julgar interessantes para publicação)<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse31" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta31"
														id="pergunta31">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario31" name="comentario31"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse32" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(32);"><center>32) Tem algum assunto relevante, que julgue ser necessário a apresentação, na próxima Reunião do Grande Conselho?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse32" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta32"
														id="pergunta32">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario32" name="comentario32"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse33" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(33);"><center>33) O(a) Frater/Soror está atento(a) sobre atividades de outros movimentos que possam prejudicar a Ordem de alguma maneira, ou através de convites para afiliação ou por proselitismo de outras organizações? (Obs. Comunicar o fato e se possível, enviar cópia da literatura para exame e arquivo da GLP. O(a) Frater/Soror também deve estar a par de eventuais ataques à AMORC, por fanáticos religiosos e ou outros grupos)<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse33" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta33"
														id="pergunta33">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario33" name="comentario33"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse34" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(34);"><center>34) O(a) Frater/Soror procura manter e zelar pela boa imagem da AMORC na sua Região, desfazendo qualquer mal-entendido?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse34" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta34"
														id="pergunta34">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario34" name="comentario34"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse35" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(35);"><center>35) O(a) Frater/Soror poderia nos enviar um relatório sucinto, à parte, relacionando todos os Organismos Afiliados de sua região, mencionando a situação de cada um e se os mesmos estão cumprindo as determinações do Acordo de Afiliação?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse35" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta35"
														id="pergunta35">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
														<option value="3">Raramente</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario35" name="comentario35"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse36" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(36);"><center>36) O(a) Grande Conselheiro(a) conta com apoio direto da Divisão de Organismos Afiliados, que deve assisti-lo nessa tarefa. Qual a sua opnião sobre a referida Divisão quanto a estar atuando de forma satisfatória?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse36" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Classifique de 1 a 5 (1 mais baixa, 5 mais alta): </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta36"
														id="pergunta36">
														<option value="0">Selecione</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario36" name="comentario36"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse37" onclick="fechaOutrasAbasRelatorioGrandeConselheiro(37);"><center>37) O(a) Frater/Soror tem alguma sugestão para que esse relacionamento possa ser melhorado?<i class=""></i></center></a>
                                        </h5>
                                    </div>
                                    <div id="collapse37" class="panel-collapse collapse">
                                        <div class="panel-body">
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Resposta: </label>
												<div class="col-sm-3">
													<select class="form-control col-sm-3" name="pergunta37"
														id="pergunta37">
														<option value="0">Selecione</option>
														<option value="1">Sim</option>
														<option value="2">Não</option>
													</select>
												</div>
											</div>
											<br><br>
											<div style="width: 100%">
												<label class="col-sm-3 control-label">Comentários: </label>
												<div class="col-sm-3">
													
												</div>
											</div>
											<br><br>
											<div style="border: #ccc solid 1px">
                                                <div class="mail-text h-200">
                                                    <textarea class="summernote" id="comentario37" name="comentario37"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                        	<input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        	<input type="hidden" name="nomeUsuarioRelatorio" id="nomeUsuarioRelatorio" value="<?php echo $dadosUsuario->getNomeUsuario();?>">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="left" title="Salvar a Ata" OnMouseOver="selecionaTudoMultipleSelect('ata_oa_mensal_oficiais');">
                                    <i class="fa fa-check fa-white"></i>&nbsp;
                                    Salvar
                                </button>
                                &nbsp;
                                <a href="?corpo=buscaPlanoAcaoRegiao" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="left" title="Cancelar e voltar!">
                                    <i class="fa fa-times fa-white"></i>&nbsp;
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<!-- Window MODAL Fim -->
<script>

</script>
<!-- Conteúdo DE INCLUDE FIM -->
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>	