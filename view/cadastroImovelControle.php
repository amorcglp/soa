<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
			<!-- Conteúdo DE INCLUDE INICIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Imóveis</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="">Início</a>
                        	</li>
	                        <li>
	                            <a href="?corpo=buscaImovelControle">Controle de Documentos de Imóveis</a>
	                        </li>
	                        <li class="active">
	                            <a>Cadastro de Novo Controle de Documentos de Imóveis</a>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
	            
				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
								    <h5>Cadastro de Novo Controle de Documentos de Imóveis</h5>
								    <div class="ibox-tools">
                                        <?php
										$idOrganismo					= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';
										$idImovel						= isset($_GET['idImovel']) ? json_decode($_GET['idImovel']) : '';
										$referenteAnoImovelControle		= isset($_GET['referenteAnoImovelControle']) ? json_decode($_GET['referenteAnoImovelControle']) : '';
                                        ?>
								    	<a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaImovelControle<?php if($idOrganismo!=''){ echo '&idOrganismoAfiliado='.$idOrganismo;} ?><?php if($referenteAnoImovelControle!=''){ echo '&referenteAnoImovelControle='.$referenteAnoImovelControle;} ?>">
											<i class="fa fa-reply fa-white"></i>&nbsp; Voltar
										</a>
								    </div>
								</div>
								<div class="ibox-content">
									<form class="form-horizontal" method="post" action="acoes/acaoCadastrar.php" enctype="multipart/form-data">
										<div class="form-group"><label class="col-sm-2 control-label">Organismo Afiliado: </label>
										    <div class="col-sm-10">
												<select class="form-control col-sm-3 chosen-select" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" style="max-width: 250px" required="required">
			                                        <option value="0">Selecione...</option>
			                                        <?php
														include_once 'controller/organismoController.php';
                                                        require_once("model/criaSessaoClass.php");
														$oc = new organismoController();
                                                        $sessao = new criaSessao();

                                                        if(($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){
                                                            $oc->criarComboBox($idOrganismo);
                                                        } else {
                                                            $oc->criarComboBox(0,$sessao->getValue("siglaOA"));
                                                        }
			                                        ?>
			                                    </select>
		                                    </div>
										</div>
										<input type="hidden" id="idImovelCadastrar" name="idImovelCadastrar" value="<?php echo $idImovel; ?>">
										<input type="hidden" id="referenteAnoImovelControleCadastrar" name="referenteAnoImovelControleCadastrar" value="<?php echo $referenteAnoImovelControle; ?>">
										<div class="form-group" id="idImovel"><label class="col-sm-2 control-label">Imóvel: </label>
										    <div class="col-sm-10">
												<select class="form-control col-sm-3" name="fk_idImovel" id="fk_idImovel" style="max-width: 400px" required="required">
			                                        <option value="">Selecione...</option>
			                                    </select>
		                                    </div>
										</div>
										<div class="form-group" id="referenteAno">
											<label class="col-sm-2 control-label">Referente ao Ano: </label>
										    <div class="col-sm-10">
												<select class="form-control col-sm-3" name="referenteAnoImovelControle" id="referenteAnoImovelControle" style="max-width: 250px" required="required">
				                                    <option>Selecione...</option>
										    	</select>
		                                    </div>
										</div>
				                        <hr>
										<div class="form-group form-inline">
											<label class="col-sm-2 control-label">IPTU: </label>
										    <div class="col-sm-10">
												<select class="form-control col-sm-3" name="propriedadeIptuImovelControle" id="propriedadeIptuImovelControle" style="max-width: 250px" required="required">
				                                    <option>Selecione...</option>
												    <option value="0">Pago</option>
												    <option value="1">Em debito</option>
												    <option value="2">Isento</option>
												    <option value="3">Imune</option>
												    <option value="4">Outro</option>
										    	</select>
		                                    </div>
										</div>
										<div class="form-group" id="propriedadeIptuObs">
                                        	<label class="col-sm-2 control-label">Obs: </label>
                                        	<div class="col-sm-10">
                                            	<div style="border: #ccc solid 1px; max-width: 800px">
	                                                <div class="mail-text h-200">
														<textarea class="summernote" id="propriedadeIptuObsImovelControle" name="propriedadeIptuObsImovelControle"></textarea>
													</div>
												</div>
                                        	</div>
                                        </div>
                                        <!--
										<div class="form-group form-inline" id="propriedadeIptuAnexo">
											<label class="col-sm-2 control-label" id="labelPropriedadeIptuAnexo">Anexar IPTU: </label>
										    <div class="col-sm-10">
												<input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="10000000" />
												<input name="propriedadeIptuAnexoImovelControle" id="propriedadeIptuAnexoImovelControle" type="file" />
		                                    </div>
											<div id="propriedadeIptuAnexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
		                                    <br>
		                                    <br>
										</div>
										-->
                                        <hr>
										<div class="panel-body">
			                                <div class="panel-group" id="accordion">
			                                    <div class="panel panel-default">
			                                        <div class="panel-heading">
			                                            <h5 class="panel-title">
			                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			                                                	<center>Construção</center>																
			                                                </a>
			                                            </h5>
			                                        </div>
			                                        <div id="collapseOne" class="panel-collapse collapse">
			                                            <div class="panel-body">
			                                                <div class="form-group"><label class="col-sm-2 control-label">Houve Construção? </label>
															    <div class="col-sm-10">
																	<select class="form-control col-sm-3" name="construcaoImovelControle" id="construcaoImovelControle" style="max-width: 250px" required="required">
                                                                        <option value="1">Não</option>
                                                                        <option value="0">Sim</option>
															    	</select>
							                                    </div>
															</div>
                                                            <div id="construcao">
                                                                <!--
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Possui Alvará de Construção: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoAlvaraImovelControle" id="construcaoAlvaraImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Sim</option>
                                                                            <option value="1">Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group form-inline" id="construcaoAlvaraAnexo"><label class="col-sm-2 control-label">Anexar Alvará: </label>
                                                                    <div class="col-sm-10">
                                                                        <input name="construcaoAlvaraAnexoImovelControle" id="construcaoAlvaraAnexoImovelControle" type="file" />
                                                                    </div>
                                                                    <div id="construcaoAlvaraAnexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                                                                </div>
                                                                -->
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Restrições na Construção: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoRestricoesImovelControle" id="construcaoRestricoesImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Sim</option>
                                                                            <option value="1">Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="construcaoRestricoesAnotacoes">
                                                                    <label class="col-sm-2 control-label">Quais São? </label>
                                                                    <div class="col-sm-10">
                                                                        <div style="border: #ccc solid 1px; max-width: 800px">
							                                                <div class="mail-text h-200">
																				<textarea class="summernote" id="construcaoRestricoesAnotacoesImovelControle" name="construcaoRestricoesAnotacoesImovelControle"></textarea>
																			</div>
																		</div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Construção Concluída: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoConcluidaImovelControle" id="construcaoConcluidaImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Sim</option>
                                                                            <option value="1">Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="datapicker_termino_construcao">
                                                                    <label class="col-sm-2 control-label" id="datapicker_termino_construcao_label">Data/Prazo de Término da Construção:</label>
                                                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                        <input onkeypress="valida_data(this,'construcaoTerminoDataImovelControle')" maxlength="10" name="construcaoTerminoDataImovelControle" id="construcaoTerminoDataImovelControle" type="text" class="form-control" style="max-width: 105px">
                                                                    	<div hidden id="data_invalida_construcaoTerminoDataImovelControle" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                                    </div>
                                                                </div>
                                                                <!--
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Possui INSS da Construção: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="construcaoInssImovelControle" id="construcaoInssImovelControle" style="max-width: 250px">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Sim</option>
                                                                            <option value="1">Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group form-inline" id="construcaoInssAnexo"><label class="col-sm-2 control-label">Anexar INSS: </label>
                                                                    <div class="col-sm-10">
                                                                        <input name="construcaoInssAnexoImovelControle" id="construcaoInssAnexoImovelControle" type="file" />
                                                                    </div>
                                                                    <div id="construcaoInssAnexoAlerta" class="col-sm-10" style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho máximo de 10MB.</div>
                                                                </div>
                                                                -->
                                                            </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="panel panel-default">
			                                        <div class="panel-heading">
			                                            <h4 class="panel-title">
			                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><center>Manutenção</center></a>
			                                            </h4>
			                                        </div>
			                                        <div id="collapseTwo" class="panel-collapse collapse">
			                                            <div class="panel-body">
			                                                <div class="form-group"><label class="col-sm-2 control-label">Houve Manutenção? </label>
															    <div class="col-sm-10">
																	<select class="form-control col-sm-3" name="manutencaoImovelControle" id="manutencaoImovelControle" style="max-width: 250px" required="required">
                                                                        <option value="1">Não</option>
																	    <option value="0">Sim</option>
															    	</select>
							                                    </div>
															</div>
                                                            <div id="manutencao">
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Pintura: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoPinturaImovelControle" id="manutencaoPinturaImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Sim</option>
                                                                            <option value="1">Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="manutencaoPinturaTipo"><label class="col-sm-2 control-label">Periodicidade: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoPinturaTipoImovelControle" id="manutencaoPinturaTipoImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Anual</option>
                                                                            <option value="1">Bienal</option>
                                                                            <option value="2">Trienal</option>
                                                                            <option value="3">Quadrienal</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Reparo de Hidráulica: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoHidraulicaImovelControle" id="manutencaoHidraulicaImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Sim</option>
                                                                            <option value="1">Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="manutencaoHidraulicaTipo"><label class="col-sm-2 control-label">Periodicidade: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoHidraulicaTipoImovelControle" id="manutencaoHidraulicaTipoImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Anual</option>
                                                                            <option value="1">Bienal</option>
                                                                            <option value="2">Trienal</option>
                                                                            <option value="3">Quadrienal</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <div class="form-group"><label class="col-sm-2 control-label">Reparo de Elétrica: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoEletricaImovelControle" id="manutencaoEletricaImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Sim</option>
                                                                            <option value="1">Não</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group" id="manutencaoEletricaTipo"><label class="col-sm-2 control-label">Periodicidade: </label>
                                                                    <div class="col-sm-10">
                                                                        <select class="form-control col-sm-3" name="manutencaoEletricaTipoImovelControle" id="manutencaoEletricaTipoImovelControle" style="max-width: 250px" required="required">
                                                                            <option>Selecione...</option>
                                                                            <option value="0">Anual</option>
                                                                            <option value="1">Bienal</option>
                                                                            <option value="2">Trienal</option>
                                                                            <option value="3">Quadrienal</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div class="panel panel-default">
			                                        <div class="panel-heading">
			                                            <h4 class="panel-title">
			                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><center>Anotações Complementares</center></a>
			                                            </h4>
			                                        </div>
			                                        <div id="collapseThree" class="panel-collapse collapse in">
			                                            <div class="panel-body">
			                                                <div style="border: #ccc solid 1px">
				                                                <div class="mail-text h-200">
																	<textarea class="summernote" id="anotacoesImovelControle" name="anotacoesImovelControle"></textarea>
																</div>
															</div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
										<hr>
										<div class="form-group">
										    <div class="col-sm-4 col-sm-offset-2">
										    	<input type="hidden" name="seqCadast" id="seqCadast" class="form-control" value="<?php echo $sessao->getValue("seqCadast"); ?>">
                                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
										        <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar"" />
										        <a href="?corpo=buscaImovelControle&idOrganismoAfiliado=<?php echo $idOrganismo; ?>" class="btn btn-white">Cancelar</a>
										    </div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->

				<!-- Conteúdo DE INCLUDE FIM -->
				
				<?php
				/*
				require_once ("model/imovelControleClass.php");
				
				$icc = new imovelControle();
				$resultado = $icc->retornaAnosJaCadastrados($_GET['idImovelControle']);
			    if ($resultado) {
            		foreach ($resultado as $vetor) {
            			
            		}
			    }
			    for ($i = date("Y"); $i >= 1950; $i--) {
			        echo "<option value=".$i.">".$i."</option>";
			    }
			    */
			    ?>