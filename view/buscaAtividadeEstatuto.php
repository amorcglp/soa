<?php
/*
function validaCPF2($cpf)
{

// Extrai somente os números
    $cpf = preg_replace('/[^0-9]/is', '', $cpf);

// Verifica se foi informado todos os digitos corretamente
    if (strlen($cpf) != 11) {
        return false;
    }
// Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11

    if (preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }
// Faz o calculo para validar o CPF

    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf{$c} * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ($cpf{$c} != $d) {
            return false;
        }
    }
    return true;
}
$cpf="043.253.859-32";
$resultado = validaCPF2($cpf);
if($resultado)
{
    echo "valido";
}else{
    echo "não valido";
}
?>
<br>
<?php
$palavra = "polaco";
if(strrev($palavra)!=$palavra)
{
    echo "não é palidromo";
}else{
    echo "é palidromo";
}*/
?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades do Organismo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Atividades do Organismo</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/atividadeEstatutoController.php");
include_once("controller/atividadeEstatutoTipoController.php");
include_once("controller/usuarioController.php");
include_once("controller/organismoController.php");

$aec    = new atividadeEstatutoController();
$aetc    = new atividadeEstatutoTipoController();
$um     = new Usuario();
$om     = new organismo();


//Excluir por ID Atividade
$excluir = isset($_REQUEST['excluirAtividadeEstatuto'])?$_REQUEST['excluirAtividadeEstatuto']:0;
$excluiu=0;
if($excluir!=0)
{   
    $excluiu = $aec->excluirAtividade($excluir);
}

    
/*
if($sessao->getValue("fk_idDepartamento")==1) {
    ?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Atividades Iniciáticas</h5>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="alert alert-success">
                            <center>
                                Módulo ainda não disponível. <br>
                                <a class="alert-link">Calma! Falta pouco :]</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else {
*/


if($sessao->getValue("alerta-atividade-estatuto")==null){
    if (($funcoesUsuarioString == 0 && $_SESSION['fk_idDepartamento'] != 1) || $regiaoUsuario != null || $_SESSION['fk_idDepartamento'] == 2 || $_SESSION['fk_idDepartamento'] == 3) {
?>


        <script>
            window.onload = function(){
                swal({
                    title: "Aviso!",
                    text: "Você pode mudar o organismo listado no cabeçalho!",
                    confirmButtonColor: "#1ab394"
                });
            }
        </script>
        <?php
    }
    $sessao->setValue("alerta-atividade-estatuto", true);
}

?>
        
<?php 
if($excluiu!=0){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Atividade excluída com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>

<?php 
if($excluiuTodasDoMes!=0){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Todas as atividades foram excluídas com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
        

<?php
$mesAtual   = date('m');
$anoAtual   = date('Y');

$mes	        = isset($_GET['mesAtual']) ? json_decode($_GET['mesAtual']) : $mesAtual;
$ano	        = isset($_GET['anoAtual']) ? json_decode($_GET['anoAtual']) : $anoAtual;

$mesAnterior    = $mes - 1;
if($mesAnterior < 1){
    //echo "entrou";
    $mesAnterior    = 12;
    $anoAnterior    = $ano - 1;
} else {
    $anoAnterior = $ano;
}

$mesProximo     = $mes + 1;
if($mesProximo > 12){
    $mesProximo     = 1;
    $anoProximo     = $ano + 1;
} else {
    $anoProximo = $ano;
}

//Excluir todas as atividades do mes
$excluirTodasDoMes = isset($_REQUEST['excluirAtividadeEstatutoTodasDoMes'])?$_REQUEST['excluirAtividadeEstatutoTodasDoMes']:0;
$excluiuTodasDoMes=0;
if($excluirTodasDoMes!=0)
{   
    $excluiuTodasDoMes = $aec->excluirTodasAtividadesDoMes($excluirTodasDoMes,$mes,$ano);
}

$mesEscolhido = isset($mes) ? $mes : 0;
$anoEscolhido = isset($ano) ? $ano : 0;

// S E L E C I O N A R   T O D A S  A S  A T I V I D A D E S                        
$resultado = $aec->listaAtividadeEstatuto($idOrganismoAfiliado,$mes, $ano);
?>

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Atividades do Organismo</h5>
                    <div class="ibox-tools">
                        
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="" style="width:100%;">
                        <table width="100%">
                            <tr>
                                <td>
                                    <?php if(!$usuarioApenasLeitura){?>
                                    <a class="btn btn-primary" href="?corpo=cadastroAtividadeEstatuto">
                                        <i class="fa fa-plus"></i> Adicionar
                                    </a>
                                    <?php }?>
                                    <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                        <?php if($resultado){?>
                                            <a class="btn btn-sm btn-danger" href="#" onclick="excluirTodasAtividadesDoMes('<?php echo $idOrganismoAfiliado; ?>','<?php echo $mes;?>','<?php echo $ano;?>');">
                                                 <i class="fa fa-trash icon-white"></i>
                                                 Excluir Todas as Atividades do mês
                                            </a>
                                        <?php }?>
                                     <?php } ?>
                                </td>
                                <td align="right">
                                    <div class="form-group date" id="data_4">
                                        <div class="input-group date">
                                            <table width="100">
                                                <tr> 
                                                    <td>
                                                        <br>
                                                        <span class="btn btn-w-m btn-primary" style="cursor: pointer; color:#FFF";>
                                                            <i class="fa fa-calendar-o" style="color:#fff"></i>
                                                            <input type="hidden" id="calendario" class="form-control" value="<?php echo $mes;?>/<?php echo date('d');?>/<?php echo $ano;?>" />
                                                            <span style="color:#fff">
                                                                <?php echo mesExtensoPortugues($mes);?><?php echo $ano;?>
                                                            </span>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div> 
                                    <input type="hidden" id="paginaAtual" class="form-control" value="<?php echo $corpo;?>">
                                
                                    
                                </td>
                            </tr>
                        </table>
                        </div>  
                    <br>
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Atividade</th>
                                <th>Data</th>
                                <th><center>Cadastrado em:</center></th>
                        		<th><center>Realização</center></th>
                                        <th><center>Classificação</center></th>
                        		<th><center>Ações</center></th>
                        	</tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($resultado) {
                            foreach ($resultado as $vetor) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $vetor['idAtividadeEstatuto']; ?>
                                    </td>
                                    <td>
                                        <?php
                                        $dadosTipoAtividade = $aetc->buscaAtividadeEstatutoTipo($vetor['fk_idTipoAtividadeEstatuto']);
                                        echo $dadosTipoAtividade->getNomeTipoAtividadeEstatuto();
                                        if($vetor['complementoNomeAtividadeEstatuto'] != ''){
                                            echo " ".$vetor['complementoNomeAtividadeEstatuto'];
                                        }
                                        ?> (Cód. <?php echo $dadosTipoAtividade->getIdTipoAtividadeEstatuto();?>)
                                    </td>
                                    <td>
                                        <?php
                                        echo substr($vetor['dataAtividadeEstatuto'],8,2)."/".
                                            substr($vetor['dataAtividadeEstatuto'],5,2)."/".
                                            substr($vetor['dataAtividadeEstatuto'],0,4)." às ".
                                            substr($vetor['horaAtividadeEstatuto'],0,5);
                                        ?>
                                    </td>
                                    <td>
                                        <center>
                                        <?php
                                        echo substr($vetor['dataCadastradoAtividadeEstatuto'],8,2)."/".substr($vetor['dataCadastradoAtividadeEstatuto'],5,2)."/".substr($vetor['dataCadastradoAtividadeEstatuto'],0,4)." às ".substr($vetor['dataCadastradoAtividadeEstatuto'],10,6);
                                        if($vetor['dataAtualizadoAtividadeEstatuto'] != '0000-00-00 00:00:00'){
                                            echo "<br>";
                                            echo "<div style='font-size: 12px; font-weight: bold;'>Atualizado em: ".
                                                "<br>".
                                                substr($vetor['dataAtualizadoAtividadeEstatuto'],8,2)."/".
                                                substr($vetor['dataAtualizadoAtividadeEstatuto'],5,2)."/".
                                                substr($vetor['dataAtualizadoAtividadeEstatuto'],0,4)." às ".
                                                substr($vetor['dataAtualizadoAtividadeEstatuto'],10,6).
                                                "</div>";
                                        }
                                        ?>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <div class="statusTarget<?php echo $vetor['idAtividadeEstatuto'] ?>">
                                                <?php
                                                switch ($vetor['statusAtividadeEstatuto']) {
                                                    case 0:
                                                        echo "<span class='badge badge-primary'>Ativo</span>";
                                                        break;
                                                    case 1:
                                                        echo "<span class='badge badge-danger'>Inativo</span>";
                                                        break;
                                                    case 2:
                                                        echo "<span class='badge badge-success'>Realizada</span>";
                                                        break;
                                                }
                                                ?>
                                                <?php
                                                if(($vetor['statusAtividadeEstatuto'] == 2)){
                                                    if (($sessao->getValue("fk_idDepartamento") == 2) || ($sessao->getValue("fk_idDepartamento") == 3)){
                                                        echo "<br><br>";
                                                        $mes = isset($_GET['mes']) ? $mes : 0;
                                                        $ano = isset($_GET['ano']) ? $ano : 0;
                                                        ?>
                                                        <a class="btn btn-sm btn-danger"
                                                           onclick="cancelarRealizacaoAtividade(<?php echo $vetor['idAtividadeEstatuto']; ?>,<?php echo $mes; ?>,<?php echo $ano; ?>)">
                                                            <i class="fa fa-times-circle fa-white"></i>
                                                            Cancelar Realização
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </center>
                                    </td>
                                    <td>
                                    <center><b>
                                           <?php
                                                    switch ($vetor['classiOaTipoAtividadeEstatuto'])
                                                    {
                                                        case 2:
                                                            echo "Pronaos";
                                                            break;
                                                        case 3:
                                                            echo "Capítulo";
                                                            break;
                                                        case 1:
                                                            echo "Loja";
                                                            break;
                                                        default :
                                                            echo "--";
                                                            break;
                                                    }
                                            ?></b>
                                        </center>    
                                    </td>
                                    <td>
                                        <center>
                                            <div class="btn-group">
                                                <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle"><i class="fa fa-users fa-white"></i>&nbsp; Participantes <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="" data-toggle="modal" data-target="#myModaNroVisitantes<?php echo $vetor['idAtividadeEstatuto']; ?>">
                                                            Visitantes
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="" data-toggle="modal" data-target="#myModaNroFrequentadores<?php echo $vetor['idAtividadeEstatuto']; ?>">
                                                            Frequentadores
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="" data-toggle="modal" data-target="#myModalResponsaveis<?php echo $vetor['idAtividadeEstatuto']; ?>">
                                                            Oficiais Responsáveis
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a class="btn btn-sm btn-success" href="" data-toggle="modal" data-target="#myModaInfo<?php echo $vetor['idAtividadeEstatuto']; ?>">
                                                <i class="fa fa-search-plus fa-white"></i>
                                                Detalhes
                                            </a>
                                            <?php $titulo = "Atividade ".$dadosTipoAtividade->getNomeTipoAtividadeEstatuto(); ?>
                                            <?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
                                                <!-- abreModalNotificacao(ID do Organismo , Título Padrão Desejado) -->
                                                <a class="btn btn-sm btn-danger" href="?corpo=buscaAtividadeEstatuto&excluirAtividadeEstatuto=<?php echo $vetor['idAtividadeEstatuto']; ?>&mesAtual=<?php echo $mesEscolhido;?>&anoAtual=<?php echo $anoEscolhido;?>">
                                                    <i class="fa fa-trash icon-white"></i>
                                                    Excluir
                                                </a>
                                            <?php } ?>
                                            <?php if(!$usuarioApenasLeitura){?>
                                                <?php if($vetor['statusAtividadeEstatuto'] != 2){ ?>
                                                <div id="botaoEditarAtividade<?php echo $vetor['idAtividadeEstatuto'] ?>">
                                                    <a class="btn btn-sm btn-info" href="?corpo=alteraAtividadeEstatuto&idAtividadeEstatuto=<?php echo $vetor['idAtividadeEstatuto']; ?>" data-rel="tooltip" title="">
                                                        <i class="fa fa-edit fa-white"></i>
                                                        Editar
                                                    </a>
                                                    <span id="status<?php echo $vetor['idAtividadeEstatuto'] ?>">
                                                    <?php if ($vetor['statusAtividadeEstatuto'] == 1) { ?>
                                                        <a class="btn btn-sm btn-primary" onclick="mudaStatus('<?php echo $vetor['idAtividadeEstatuto'] ?>', '<?php echo $vetor['statusAtividadeEstatuto'] ?>', 'atividadeEstatuto')" data-rel="tooltip" title="Ativar">
                                                            Ativar
                                                        </a>
                                                    <?php } else { ?>
                                                        <a class="btn btn-sm btn-danger" onclick="mudaStatus('<?php echo $vetor['idAtividadeEstatuto'] ?>', '<?php echo $vetor['statusAtividadeEstatuto'] ?>', 'atividadeEstatuto')" data-rel="tooltip" title="Inativar">
                                                            Inativar
                                                        </a>
                                                    <?php } ?>
                                                    </span>
                                                    <a class="btn btn-sm btn-warning"
                                                       onclick="atualizarAtividadeEstatutoRealizada(<?php echo $vetor['idAtividadeEstatuto']; ?>,<?php echo substr($vetor['dataAtividadeEstatuto'],8,2);?>,<?php echo substr($vetor['dataAtividadeEstatuto'],5,2)?>,<?php echo substr($vetor['dataAtividadeEstatuto'],0,4) ?>)">
                                                        Confirmar Realização?
                                                    </a>
                                                </div>
                                                <?php } ?>
                                            <?php }?>
                                        </center>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->

<?php
/** Modal de Notificação  da Atividade */
//modalNotificacao($sessao->getValue("seqCadast"));
?>

<?php
/** Modal de Detalhes da Atividade */
$resultadoInfo      = $aec->listaAtividadeEstatuto($idOrganismoAfiliado,$mes, $ano);
if ($resultadoInfo) {
    foreach ($resultadoInfo as $vetorInfo) {
        //echo "<pre>"; print_r($vetorInfo); echo "</pre>";
    ?>
        <div class="modal inmodal" id="myModaInfo<?php echo $vetorInfo['idAtividadeEstatuto']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Detalhe da Atividade</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Atividade: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php
                                        $dadosTipoAtividade = $aetc->buscaAtividadeEstatutoTipo($vetorInfo['fk_idTipoAtividadeEstatuto']);
                                        echo $dadosTipoAtividade->getNomeTipoAtividadeEstatuto();
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Complemento ao Nome: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php echo $vetorInfo['complementoNomeAtividadeEstatuto']; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Local: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php echo $vetorInfo['localAtividadeEstatuto']; ?>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Título do Discurso: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php echo $vetorInfo['tituloDiscursoAtividadeEstatuto']; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Orador: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php echo $vetorInfo['nomeOradorDiscursoAtividadeEstatuto']; ?>
                                    </div>
                                </div>
                            </div>





                            <div class="form-group">
                                <label class="col-sm-3 control-label">Realizada em: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php
                                        echo substr($vetorInfo['dataAtividadeEstatuto'], 8, 2) . "/" .
                                            substr($vetorInfo['dataAtividadeEstatuto'], 5, 2) . "/" .
                                            substr($vetorInfo['dataAtividadeEstatuto'], 0, 4) . " às " .
                                            substr($vetorInfo['horaAtividadeEstatuto'], 0, 5);
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Cadastrado em: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php
                                        echo substr($vetorInfo['dataCadastradoAtividadeEstatuto'], 8, 2) . "/" .
                                            substr($vetorInfo['dataCadastradoAtividadeEstatuto'], 5, 2) . "/" .
                                            substr($vetorInfo['dataCadastradoAtividadeEstatuto'], 0, 4) . " às " .
                                            substr($vetorInfo['dataCadastradoAtividadeEstatuto'], 10, 6);
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <?php if ($vetorInfo['dataAtualizadoAtividadeEstatuto'] != '0000-00-00 00:00:00') { ?>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Atualizado em: </label>
                                    <div class="col-sm-9">
                                        <div class="form-control-static">
                                            <?php
                                            echo substr($vetorInfo['dataAtualizadoAtividadeEstatuto'], 8, 2) . "/" .
                                                substr($vetorInfo['dataAtualizadoAtividadeEstatuto'], 5, 2) . "/" .
                                                substr($vetorInfo['dataAtualizadoAtividadeEstatuto'], 0, 4) . " às " .
                                                substr($vetorInfo['dataAtualizadoAtividadeEstatuto'], 10, 6);
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                            <?php $labelCadastroOuAtualizado = ($vetorInfo['dataAtualizadoAtividadeEstatuto'] != '0000-00-00 00:00:00') ? "Atualizado" : "Cadastrado"; ?>

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo $labelCadastroOuAtualizado; ?>
                                    Por: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php
                                        $resultadoAtualizadoPor = $um->buscaUsuario($vetorInfo['fk_seqCadastAtualizadoPor']);
                                        if ($resultadoAtualizadoPor) {
                                            foreach ($resultadoAtualizadoPor as $vet) {
                                                echo $vetorInfo['nomeUsuario'];
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Status: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                    <div class="statusTarget<?php echo $vetorInfo['idAtividadeEstatuto']; ?>">
                                        <?php

                                        switch ($vetorInfo['statusAtividadeEstatuto']) {
                                            case 0:
                                                echo "<span class='badge badge-primary'>Ativo</span>";
                                                break;
                                            case 1:
                                                echo "<span class='badge badge-danger'>Inativo</span>";
                                                break;
                                            case 2:
                                                echo "<span class='badge badge-success'>Realizada</span>";
                                                break;
                                        }
                                        ?>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Descrição: </label>
                                <div class="col-sm-9">
                                    <div class="form-control-static">
                                        <?php echo htmlspecialchars($vetorInfo['descricaoAtividadeEstatuto']); ?>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <hr>
                        <p align="right">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
}
?>

<?php
/** Modal de Número de Participantes da Atividade */
$resultadoParticipantes      = $aec->listaAtividadeEstatuto($idOrganismoAfiliado,$mes, $ano);
if ($resultadoParticipantes) {
    foreach ($resultadoParticipantes as $vetorParticipantes) {
    ?>
    <div class="modal inmodal" id="myModaNroVisitantes<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                    <h4 class="modal-title">Número de Visitantes</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">
                        <center>
                            <a class="alert-link">Membros ainda não filiados!</a>.
                        </center>
                    </div>
                    <input type="hidden" value="<?php echo $vetorParticipantes['nroParticipanteAtividadeEstatuto']; ?>" id="numeroAtualParticipantes<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>" name="numeroAtualParticipantes<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                    <div class="row">
                        <div class="col-lg-4 col-lg-offset-4" id="numeroParticipantes<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                            <center>
                                <h2><?php echo $vetorParticipantes['nroParticipanteAtividadeEstatuto']; ?></h2>
                            </center>
                        </div>
                    </div>
                    <div class="row" id="botaoEdicaoNroMembros<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                        <?php if($vetorParticipantes['statusAtividadeEstatuto'] != 2){ ?>
                        <div class="col-lg-4 col-lg-offset-4" id="botaoParticipantes<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                            <center>
                                <a class="btn btn-xs btn-primary" onclick="abreInputNroParticipantes(<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>);">
                                    Editar
                                </a>
                            </center>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="modal-footer" id="botaoAtividadeEstatutoRealizada<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                    <!--
                    <?php if ($vetorParticipantes['statusAtividadeEstatuto'] != 2) { ?>
                        <a class="btn btn-sm btn-primary"
                           onclick="atualizarAtividadeEstatutoRealizada(<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>)">
                            <i class="fa fa-check fa-white"></i>
                            Atividade Realizada
                        </a>
                    <?php } ?>
                    -->
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
}
?>

<?php
/** Modal de Número de Frequentadores da Atividade */
$resultadoFrequentadores      = $aec->listaAtividadeEstatuto($idOrganismoAfiliado,$mes, $ano);
if ($resultadoFrequentadores) {
    foreach ($resultadoFrequentadores as $vetorFrequentadores) {
        ?>
        <div class="modal inmodal" id="myModaNroFrequentadores<?php echo $vetorFrequentadores['idAtividadeEstatuto']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Número de Frequentadores</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-warning">
                            <center>
                                <a class="alert-link">Não considerar oficiais do organismo que participarem da atividade!</a>.
                            </center>
                        </div>
                        <input type="hidden" value="<?php echo $vetorFrequentadores['nroFrequentadorAtividadeEstatuto']; ?>" id="numeroAtualFrequentadores<?php echo $vetorFrequentadores['idAtividadeEstatuto']; ?>" name="numeroAtualFrequentadores<?php echo $vetorFrequentadores['idAtividadeEstatuto']; ?>">
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-4" id="numeroFrequentadores<?php echo $vetorFrequentadores['idAtividadeEstatuto']; ?>">
                                <center>
                                    <h2><?php echo $vetorFrequentadores['nroFrequentadorAtividadeEstatuto']; ?></h2>
                                </center>
                            </div>
                        </div>
                        <div class="row" id="botaoEdicaoNroMembrosFrequentadores<?php echo $vetorFrequentadores['idAtividadeEstatuto']; ?>">
                            <?php if($vetorFrequentadores['statusAtividadeEstatuto'] != 2){ ?>
                                <div class="col-lg-4 col-lg-offset-4" id="botaoFrequentadores<?php echo $vetorFrequentadores['idAtividadeEstatuto']; ?>">
                                    <center>
                                        <a class="btn btn-xs btn-primary" onclick="abreInputNroFrequentadores(<?php echo $vetorFrequentadores['idAtividadeEstatuto']; ?>);">
                                            Editar
                                        </a>
                                    </center>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>

<?php
/** Modal de Responsáveis da Atividade */
$resultadoParticipantes      = $aec->listaAtividadeEstatuto($idOrganismoAfiliado,$mes, $ano);
if ($resultadoParticipantes) {
    foreach ($resultadoParticipantes as $vetorParticipantes) {
        ?>
        <div class="modal inmodal" id="myModalResponsaveis<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title">Oficial Responsável pela Atividade</h4>
                    </div>
                    <div class="modal-body">
                        <div id="oficiaisResponsaveis<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Cód. Afil.</th>
                                    <th>Nome</th>
                                    <th><center>Status</center></th>
                                    <th><center>Ações</center></th>
                                </tr>
                                </thead>
                                <tbody id="bodyTabelaOficiaisResponsaveis<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                                <?php
                                /** Modal de Responsáveis da Atividade */
                                $resultadoOficiaisResponsaveis      = $aec->listaAtividadeEstatutoOficiais($vetorParticipantes['idAtividadeEstatuto']);
                                $qntOficiaisResponsaveis=0;
                                if ($resultadoOficiaisResponsaveis) {
                                    foreach ($resultadoOficiaisResponsaveis as $vetorOficiaisResponsaveis) {
                                ?>
                                    <tr>
                                        <td><?php echo $vetorOficiaisResponsaveis['codAfiliacaoOficial'] ?></td>
                                        <td>
                                            <?php
                                            $arrNome				= explode(" ",$vetorOficiaisResponsaveis['nomeOficial']);
                                            $arrNomeDecrescente		= $arrNome[count($arrNome)-1];
                                            echo ucfirst(mb_strtolower($arrNome[0],'UTF-8'))." ".ucfirst(mb_strtolower($arrNomeDecrescente,'UTF-8'));
                                            ?>
                                        </td>
                                        <td>
                                            <center>
                                                <div id="statusTarget<?php echo $vetorParticipantes['idAtividadeEstatuto'] . $vetorOficiaisResponsaveis['seqCadastOficial']; ?>">
                                                    <?php
                                                    switch ($vetorOficiaisResponsaveis['statusOficial']) {
                                                        case 0:
                                                            echo "<span class='badge badge-primary'>Ativo</span>";
                                                            break;
                                                        case 1:
                                                            echo "<span class='badge badge-danger'>Inativo</span>";
                                                            break;
                                                    }
                                                    ?>
                                                </div>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <?php if($vetorParticipantes['statusAtividadeEstatuto'] != 2){ ?>
                                                <div id="botaoOficialResponsavel<?php echo $vetorParticipantes['idAtividadeEstatuto'] . $vetorOficiaisResponsaveis['seqCadastOficial']; ?>" class="botaoOficialResponsavel<?php echo $vetorParticipantes['idAtividadeEstatuto'];?>">
                                                    <?php
                                                    switch ($vetorOficiaisResponsaveis['statusOficial']) {
                                                        case 0:
                                                            echo "<a class='btn btn-xs btn-danger' onclick='inativaOficialResponsavel(" .  $vetorParticipantes['idAtividadeEstatuto'] . " , " . $vetorOficiaisResponsaveis['seqCadastOficial'] . ")' data-rel='tooltip' title='Inativar'> Inativar </a>";
                                                            break;
                                                        case 1:
                                                            echo "<a class='btn btn-xs btn-primary' onclick='ativaOficialResponsavel(" .  $vetorParticipantes['idAtividadeEstatuto'] . " , " . $vetorOficiaisResponsaveis['seqCadastOficial'] . ")' data-rel='tooltip' title='Inativar'> Ativar </a>";
                                                            break;
                                                    }
                                                    ?>
                                                </div>
                                                <?php } else { ?>
                                                --
                                                <?php } ?>
                                            </center>
                                        </td>
                                    </tr>
                                <?php
                                        if($vetorOficiaisResponsaveis['statusOficial'] == 0){
                                            $qntOficiaisResponsaveis++;
                                        }
                                    }
                                } else {
                                ?>
                                    <tr>
                                        <td colspan="4">
                                            <div class="alert alert-danger">
                                                <center>Nenhum responsável cadastrado!</center>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                                <input type="hidden" value="<?php echo $qntOficiaisResponsaveis; ?>" id="qntOficiaisResponsaveis<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>" name="qntOficiaisResponsaveis<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>" >
                                </tbody>
                            </table>
                            <?php if($vetorParticipantes['statusAtividadeEstatuto'] != 2){ ?>
                            <hr>
                            <div class="row" id="botaoIncluirOficial<?php echo $vetorParticipantes['idAtividadeEstatuto'];?>">
                                <div class="form-group form-inline">
                                    <div class="col-sm-12">
                                        <input placeholder="Código" class="form-control" id="codAfiliacaoMembroOficial<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>" maxlength="7" type="text" value="" style="max-width: 76px">
                                        <input placeholder="Apenas o primeiro nome" class="form-control" id="nomeMembroOficial<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>" type="text" maxlength="100" value="" style="min-width: 320px" onBlur="retornaDadosMembroOficialECompanheiroAtividadeEstatuto(<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>);">
                                        <!-- <input type="checkbox" class="companheiro" id="companheiroMembroOficial<?php // echo $vetorParticipantes['idAtividadeEstatuto']; ?>" value=""> Companheiro -->
                                        &nbsp;
                                        <a class="btn btn-xs btn-primary" style="margin-top: 11px" onclick="incluiOficialResponsavelAtividade(<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>);"> Incluir</a>
                                        <br>
                                        Após inserir o nome pressione TAB
                                        <input type="hidden" id="h_seqCadastMembroOficial<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                                        <input type="hidden" id="h_nomeMembroOficial<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                                        <input type="hidden" id="h_seqCadastCompanheiroMembroOficial<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                                        <input type="hidden" id="h_nomeCompanheiroMembroOficial<?php echo $vetorParticipantes['idAtividadeEstatuto']; ?>">
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
?>
<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php //}; ?>
