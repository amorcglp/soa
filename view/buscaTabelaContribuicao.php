
<!-- Conteúdo DE INCLUDE INICIO -->
<?php
$pag = isset($_REQUEST['pag']) ?$_REQUEST['pag']:0;

$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Tabela de Contribuição salva com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Tabela de Contribuição</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="?corpo=buscaSubMenu">Tabela de Contribuição</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tabela de Contribuição</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                            <div class="col-md-2">
                                <a href="#" onclick="navegaTabelaContribuicao(0);" class="btn btn-block <?php if($pag==0){ echo "";}else{ echo "btn-outline";}?> btn-primary">R+C Impressa</a>
                            </div>
                            <div class="col-md-1">
                                <a href="#" onclick="navegaTabelaContribuicao(1);" class="btn btn-block <?php if($pag==1){ echo "";}else{ echo "btn-outline";}?> btn-primary">R+C Vir.</a>
                            </div>
                            <div class="col-md-2">
                                <a href="#" onclick="navegaTabelaContribuicao(2);" class="btn btn-block <?php if($pag==2){ echo "";}else{ echo "btn-outline";}?> btn-primary">R+C Virtual+Impressa</a>
                            </div>
                            <div class="col-md-1">
                                <a href="#" onclick="navegaTabelaContribuicao(3);" class="btn btn-block <?php if($pag==3){ echo "";}else{ echo "btn-outline";}?> btn-primary">TOM</a>
                            </div>
                            <div class="col-md-2">
                                <a href="#" onclick="navegaTabelaContribuicao(4);" class="btn btn-block <?php if($pag==4){ echo "";}else{ echo "btn-outline";}?> btn-primary">OGG Impressa</a>
                            </div>
                            <div class="col-md-2">
                                <a href="#" onclick="navegaTabelaContribuicao(5);" class="btn btn-block <?php if($pag==5){ echo "";}else{ echo "btn-outline";}?> btn-primary">OGG Virtual</a>
                            </div>
                            <div class="col-md-2">
                                <a href="#" onclick="navegaTabelaContribuicao(6);" class="btn btn-block <?php if($pag==6){ echo "";}else{ echo "btn-outline";}?> btn-primary">OGG Vir.+Impressa</a>
                            </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                            <?php
                            include_once("controller/tabelaContribuicaoController.php");

                            $t = new tabelaContribuicaoController();
                            $resultado = $t->listaTabelaContribuicao();
                            $i=0;
                            $pag++;
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                      $tipo = $vetor['tipo'];
                                      $tipoTexto = $vetor['tipoTexto'];
                                      if($pag==$tipo) {
                                          ?>
                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Mensal Individual
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeMensalIndividual']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeMensalIndividual']; ?>','modalidadeMensalIndividual')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeMensalIndividual']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeMensalIndividual">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Trimestral Individual Real
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeTrimestralIndividualReal']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeTrimestralIndividualReal']; ?>','modalidadeTrimestralIndividualReal')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeTrimestralIndividualReal']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeTrimestralIndividualReal">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Trimestral Individual Dolar
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeTrimestralIndividualDolar']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeTrimestralIndividualDolar']; ?>','modalidadeTrimestralIndividualDolar')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeTrimestralIndividualDolar']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeTrimestralIndividualDolar">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Trimestral Individual Euro
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeTrimestralIndividualEuro']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeTrimestralIndividualEuro']; ?>','modalidadeTrimestralIndividualEuro')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeTrimestralIndividualEuro']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeTrimestralIndividualEuro">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Anual Individual Real
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeAnualIndividualReal']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeAnualIndividualReal']; ?>','modalidadeAnualIndividualReal')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeAnualIndividualReal']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeAnualIndividualReal">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Anual Individual CFD
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeAnualIndividualCFD']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeAnualIndividualCFD']; ?>','modalidadeAnualIndividualCFD')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeAnualIndividualCFD']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeAnualIndividualCFD">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Mensal Dual
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeMensalDual']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeMensalDual']; ?>','modalidadeMensalDual')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeMensalDual']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeMensalDual">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Trimestral Dual Real
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeTrimestralDualReal']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeTrimestralDualReal']; ?>','modalidadeTrimestralDualReal')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeTrimestralDualReal']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeTrimestralDualReal">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Trimestral Dual Dolar
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeTrimestralDualDolar']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeTrimestralDualDolar']; ?>','modalidadeTrimestralDualDolar')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeTrimestralDualDolar']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeTrimestralDualDolar">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Trimestral Dual Euro
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeTrimestralDualEuro']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeTrimestralDualEuro']; ?>','modalidadeTrimestralDualEuro')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeTrimestralDualEuro']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeTrimestralDualEuro">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Anual Dual Real
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeAnualDualReal']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeAnualDualReal']; ?>','modalidadeAnualDualReal')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeAnualDualReal']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeAnualDualReal">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Modalidade Anual Dual CFD
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['modalidadeAnualDualCFD']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['modalidadeAnualDualCFD']; ?>','modalidadeAnualDualCFD')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['modalidadeAnualDualCFD']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="modalidadeAnualDualCFD">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Taxa Inscrição Real
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['taxaInscricaoReal']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['taxaInscricaoReal']; ?>','taxaInscricaoReal')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['taxaInscricaoReal']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="taxaInscricaoReal">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Taxa Inscrição Dolar
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['taxaInscricaoDolar']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['taxaInscricaoDolar']; ?>','taxaInscricaoDolar')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['taxaInscricaoDolar']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="taxaInscricaoDolar">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-4">
                                              (<?php echo $tipoTexto; ?>) Taxa Inscrição Euro
                                          </div>
                                          <div class="col-md-4" id="<?php echo $i; ?>">
                                              <?php echo $vetor['taxaInscricaoEuro']; ?>
                                          </div>
                                          <div class="col-md-4" id="editar<?php echo $i; ?>">
                                              <a href="#" class="btn btn-outline btn-primary"
                                                 onclick="abrirCampoTabelaContribuicao('<?php echo $i; ?>','<?php echo $tipo; ?>','<?php echo $vetor['taxaInscricaoEuro']; ?>','taxaInscricaoEuro')">Editar</a>
                                              <input type="hidden" id="valorDefault<?php echo $i; ?>" value="<?php echo $vetor['taxaInscricaoEuro']; ?>">
                                              <input type="hidden" id="nomeCampo<?php echo $i; ?>" value="taxaInscricaoEuro">
                                          </div>
                                          <?php $i++; ?>

                                          <div class="col-md-12">
                                              <hr>
                                          </div>
                                          <?php
                                      }
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="linha" value="">
<input type="hidden" id="tipo" value="">
<input type="hidden" id="campoDB" value="">

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
				