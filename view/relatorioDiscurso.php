<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório de Impressão de Discursos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php?corpo=buscaDiscurso">Discursos</a>
            </li>
            <li class="active">
                <strong>
                    <a>Relatório de Impressão</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<?php
include_once 'controller/impressaoController.php';

$impressao = new impressaoController();

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista de Discursos</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaDiscurso">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th><center>Quantidade de Impressão</center></th>
                                <th><center>Número da Mensagem</center></th>
                                <th><center>Título da Mensagem</center></th>
                                <th><center>Data/Hora de Impressão</center></th>
                                <th><center>Motivo da Impressão</center></th>
                                <th><center>Usuário</center></th>
                             </tr>
                        </thead>
                        <tbody>
                            <?php
                               $resultado = $impressao->listaTodasImpressao(true);
                                
                                if($resultado) {
                                    foreach ($resultado as $vetor) {
                           ?>
                            <tr>
                                <td><center><?php echo $vetor['qtdeImpressao']; ?></center></td>
                                <td><center><?php echo $vetor['numeroMensagem']; ?></center></td>
                                <td><center><?php echo $vetor['tituloMensagem']; ?></center></td>
                                <td><center><?php echo $vetor['dataHora']; ?></center></td>
                                <td><center><?php echo $vetor['motivoDaImpressao']; ?></center></td>
                                <td><center><?php echo $vetor['nomeUsuario']; ?></center></td>
                            </tr>
                             <?php               
                                    }
                                }
                            ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->
