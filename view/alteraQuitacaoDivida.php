<?php
include_once("controller/quitacaoDividaController.php");

$qdc = new quitacaoDividaController();
$dados = $qdc->buscaQuitacaoDivida($_GET['id']);

?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Quitação de Dívidas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaFuncao">Função</a>
            </li>
            <li class="active">
                <a>Cadastro de Quitação de Dívidas</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Quitação de Dívidas</h5>
                    <div ibox-tools></div>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="acoes/acaoAlterar.php">
                        <input type="hidden" name="idQuitacaoDivida" id="idQuitacaoDivida" value="<?php echo $_GET['id']; ?>" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Dívida</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idDivida" id="fk_idDivida" style="max-width: 300px" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'model/dividaClass.php';
                                    $d = new Divida();
                                    $resultado = $d->listaDivida($idOrganismoAfiliado);
                                    if($resultado)
                                    {
                                        foreach ($resultado as $vetor)
                                        {
                                            ?>
                                                <option value="<?php echo $vetor['idDivida'];?>"
                                                <?php 
                                                    if($dados->fk_idDivida==$vetor['idDivida'])
                                                    {
                                                        echo "selected";
                                                    } 
                                                ?>><?php echo $vetor['descricaoDivida'];?></option>
                                            <?php
                                        }    
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_ata">
                            <label class="col-sm-2 control-label">Data:</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataPagamento" maxlength="10" id="dataPagamento" type="text" 
                                       value="<?php echo substr($dados->dataPagamento,8,2)."/".substr($dados->dataPagamento,5,2)."/".substr($dados->dataPagamento,0,4);?>"
                                       class="form-control" style="max-width: 102px"  required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">*Valor do Pagamento</label>
                            <div class="col-sm-10">
                                <input type="text" name="valorPagamento" id="valorPagamento" class="form-control currency" value="<?php echo $dados->valorPagamento;?>" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Observação</label>
                            <div class="col-sm-10">
                                <textarea rows="7" name="observacao" id="observacao" class="form-control" style="max-width: 500px"><?php echo $dados->observacao;?></textarea>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_SESSION['seqCadast'];?>">
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>