<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
                         @usuarioOnline();?>
<?php
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Sua pergunta foi enviada com sucesso! Estamos verificando uma solução.",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>FAQ</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li class="active">
                <strong>Perguntas Frequentes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="ibox-content m-b-sm border-bottom">
                <div class="text-center p-lg">
                    <h2>Se você não encontrar uma resposta para sua dúvida</h2>
                    <span>simplesmente nos </span>
                    <a href="painelDeControle.php?corpo=cadastroFaq"><button class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <span class="bold"> Envie uma pergunta</span></button></a>
                </div>
            </div>

            <?php
            include_once("controller/faqRespostaController.php");

            $frc = new faqRespostaController();
            $resultado = $frc->listaRespostaFaq();
            $i = 0;
            if ($resultado) {
                foreach ($resultado as $vetor) {
                    ?>
                    <div class="faq-item">
                        <div class="row">
                            <div class="col-md-7">
                                <a data-toggle="collapse" href="#faq<?php echo $i; ?>" class="faq-question">
                                    <?php echo $vetor['tituloRespostaFaq']; ?>
                                </a>
                                <small>Criada por <strong><?php echo $vetor['responsavelRespostaFaq']; ?></strong> &nbsp; -- &nbsp; 
                                    <i class="fa fa-calendar"></i> 
                                    <?php
                                    $data = date_create($vetor['dataRespostaFaq']);
                                    echo date_format($data, "d/m/Y H:i:s");
                                    ?>
                                </small>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="faq<?php echo $i; ?>" class="panel-collapse collapse ">
                                            <div class="faq-answer">
                                                <p>
                                                    <?php echo $vetor['respostaFaq'] ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
            }
            ?>
        </div>
    </div>
</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>

<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>