
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atualizações no SOA</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Início</a>
            </li>
            <li class="active">
                <strong>Atualizações SOA</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Atualizações do SOA</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover issue-tracker">
                            <tbody>
                                <?php
                                include_once("controller/notificacaoAtualizacaoServerController.php");

                                $nc = new notificacaoServerController();
                                $resultado = $nc->listaNotificacaoServer(1);

                                if ($resultado) {
                                    foreach ($resultado as $vetor) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?php
                                                switch ($vetor['tipoNotificacao']) {
                                                    case 1:
                                                        echo "<span class='badge badge-success'>Melhoria</span>";
                                                        break;
                                                    case 2:
                                                        echo "<span class='badge badge-warning'>Corrigido</span>";
                                                        break;
                                                    case 3:
                                                        echo "<span class='badge badge-info'>Novidade</span>";
                                                        break;
                                                    case 4:
                                                        echo "<span class='badge badge-danger'>Erro</span>";
                                                        break;
                                                }
                                                ?>
                                            </td>
                                            <td class="issue-info">
                                                <a href="?corpo=detalheAtualizacoesServer&idNotificacao=<?php echo $vetor['idNotificacao']; ?>">
                                                    <?php echo $vetor['tituloNotificacao']; ?>
                                                </a>
                                                <br>
                                                <small>
                                                    <?php echo substr(str_replace("<p>", "", str_replace("</p>", "", $vetor['descricaoNotificacao'])), 0, 100); ?>... <a href="?corpo=detalheAtualizacoesServer&idNotificacao=<?php echo $vetor['idNotificacao']; ?>">+saiba mais</a> 
                                                </small>
                                            </td>
                                            <td>
                                                <i class="fa fa-laptop"></i> CPD - Desenvolvimento
                                            </td>
                                            <td>
                                                <?php
                                                $data = date_create($vetor['dataNotificacao']);
                                                echo date_format($data, 'd/m/Y H:i:s');
                                                ?>
                                            </td>
                                            <td>
                                            	<?php 
                                            	//include_once("model/notificacaoAtualizacaoServerClass.php");
                                				$nsu = new notificacoesServerUsuario();
                                				$resultado = $nsu->lista($sessao->getValue("seqCadast"),$vetor['idNotificacao']);
                                				if($resultado)
                                				{
                                					
			                                				?>	
			                                            			<span class='badge badge-primary'>Visualizada</span>
			                                            	<?php }else{?>
			                                            			<span class='badge badge-warning'>Não visualizada</span>
			                                            	<?php 
                                				}    
			                                            ?>	
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>
<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>