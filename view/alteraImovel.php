<?php
include_once("controller/imovelController.php");
require_once ("model/imovelClass.php");

$idAlteraImovel	= isset($_GET['idImovel']) ? json_decode($_GET['idImovel']) : '';

if($idAlteraImovel==''){
    echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaImovel';</script>";
}

$ic             = new imovelController();
$dados          = $ic->buscaImovel($idAlteraImovel);
// $matriculas     = $ic->listaImovelMatricula($idAlteraImovel);
$matriculas     = false;
?>
			<!-- Conteúdo DE INCLUDE INICIO -->
				
				<!-- Caminho de Migalhas Início -->
		        <div class="row wrapper border-bottom white-bg page-heading">
	                <div class="col-lg-10">
	                    <h2>Imóveis</h2>
	                    <ol class="breadcrumb">
	                    	<li>
                            	<a href="">Início</a>
                        	</li>
	                        <li>
	                            <a href="?corpo=buscaOrganismo">Imóveis</a>
	                        </li>
	                        <li class="active">
	                            <a>Altera Imóvel</a>
	                        </li>
	                    </ol>
	                </div>
	                <div class="col-lg-2">
	                </div>
	            </div>
	            <!-- Caminho de Migalhas Fim -->
	            
				<!-- Tabela Início -->
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
								    <h5>Altera Imóvel</h5>
								    <div class="ibox-tools">
								    	<a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaImovel">
											<i class="fa fa-reply fa-white"></i>&nbsp; Voltar
										</a>
								    </div>
								</div>
								<div class="ibox-content">
									<form name="alteraImovel" class="form-horizontal" method="post" enctype="multipart/form-data" action="acoes/acaoAlterar.php" novalidate="" onsubmit="return validaFormImoveis();">
                                        <input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="10000000" />
										<div class="form-group"><label class="col-sm-2 control-label">Organismo Afiliado</label>
										    <div class="col-sm-10">
												<div class="form-control-static" style="padding-top: 5px; padding-bottom: 10px;">
			                                        <?php
														include_once 'controller/organismoController.php';
														$oc = new organismoController();
														$dados2 = $oc->buscaOrganismo($dados->getFk_idOrganismoAfiliado());
														switch ($dados2->getClassificacaoOrganismoAfiliado()) {
			                                                case 1: echo "Loja "; break;
			                                                case 2: echo "Pronaos "; break;
			                                                case 3: echo "Capítulo "; break;
			                                                case 4: echo "Heptada "; break;
			                                                case 5: echo "Atrium "; break;
			                                            }
			                                            switch ($dados2->getTipoOrganismoAfiliado()) {
			                                                case 1: echo "R+C "; break;
			                                                case 2: echo "TOM "; break;
			                                            }
			                                            echo $dados2->getNomeOrganismoAfiliado().' - '.$dados2->getSiglaOrganismoAfiliado();
                                                    ?>
			                                    </div>
		                                    </div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Endereço</label>
										    <div class="col-sm-10">
										    	<input type="text" name="enderecoImovel" id="enderecoImovel" value="<?php echo $dados->getEnderecoImovel();?>" class="form-control" style="max-width: 320px" required="required">
										    </div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">Nº</label>
										    <div class="col-sm-10"><input type="text" onkeypress="apenasNumeros()" name="numeroImovel" id="numeroImovel" value="<?php echo $dados->getNumeroImovel();?>" class="form-control" style="max-width: 160px" required="required"></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">Complemento</label>
										    <div class="col-sm-10"><input type="text" name="complementoImovel" id="complementoImovel" value="<?php echo $dados->getComplementoImovel();?>" class="form-control" style="max-width: 320px"></div>
										</div>
										<div class="form-group"><label class="col-sm-2 control-label">Bairro</label>
										    <div class="col-sm-10"><input type="text" name="bairroImovel" id="bairroImovel" value="<?php echo $dados->getBairroImovel();?>" class="form-control" style="max-width: 320px" required="required"></div>
										</div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Pais: </label>
                                            <div class="col-sm-10">
                                                <select class="form-control col-sm-3" name="paisImovel" id="paisImovel" style="max-width: 250px" onchange="getEstados(this.value, '#estadoImovel');">
                                                    <option value="0">Selecione</option>
                                                    <?php
                                                    include_once 'model/imovelClass.php';
                                                    $im = new imovel();
                                                    $resultado = $im->listaPais();

                                                    if ($resultado) {
                                                        foreach ($resultado as $vetor) {

                                                            echo "<option value='".$vetor['id']."'";

                                                            if($dados->getFkIdPais()==$vetor['id']){
                                                                echo "selected";
                                                            }

                                                            echo ">".$vetor['nome']."</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" id="estadoImvl">
                                            <label class="col-sm-2 control-label">Estado: </label>
                                            <div class="col-sm-10">
                                                <select class="form-control col-sm-3" name="estadoImovel" id="estadoImovel" style="max-width: 250px" onchange="getCidades(this.value, '#cidadeImovel');">
                                                    <option value="0">Selecione</option>
                                                    <?php
                                                    include_once 'model/imovelClass.php';
                                                    $im = new imovel();
                                                    $resultado = $im->listaEstado();

                                                    if ($resultado) {
                                                        foreach ($resultado as $vetor) {

                                                            echo "<option value='".$vetor['id']."'";

                                                                if($dados->getFk_idEstado()==$vetor['id']){
                                                                    echo "selected";
                                                                }

                                                            echo ">".$vetor['nome']."</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" id="cidadeImvl">
                                            <label class="col-sm-2 control-label">Cidade: </label>
                                            <div class="col-sm-10">
                                                <select class="form-control col-sm-3" name="cidadeImovel" id="cidadeImovel" style="max-width: 250px">
                                                    <option value="0" selected="selected">Selecione o estado.</option>
                                                </select>
                                            </div>
                                        </div>
										<div class="form-group"><label class="col-sm-2 control-label">CEP</label>
										    <div class="col-sm-10"><input type="text" name="cepImovel" id="cepImovel" value="<?php echo $dados->getCepImovel();?>" class="form-control" style="max-width: 320px"></div>
										</div>
                                        <hr>
                                        <div class="form-group"><label class="col-sm-2 control-label">Área Total: </label>
                                            <div class="col-sm-10"><input value="<?php echo $dados->getAreaTotalImovel();?>" type="text" name="areaTotalImovel" id="areaTotalImovel" class="form-control col-sm-2" style="max-width: 120px" required="required"><div class="col-sm-1" style="margin-top: 7px">m²</div></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Área Contruída: </label>
                                            <div class="col-sm-10"><input value="<?php echo $dados->getAreaConstruidaImovel();?>" type="text" name="areaConstruidaImovel" id="areaConstruidaImovel" class="form-control col-sm-2" style="max-width: 120px" required="required"><div class="col-sm-1" style="margin-top: 7px">m²</div></div>
                                        </div>
                                        <hr>
                                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Propriedade: </label>
                                            <div class="col-sm-10">
                                                <select class="form-control col-sm-3" name="propriedadeTipoImovel" id="propriedadeTipoImovel" style="max-width: 250px" required="required">
                                                    <option value="">Selecione...</option>
                                                    <option value="0" <?php if ($dados->getPropriedadeTipoImovel()==0){ echo 'selected';};?>>Prédio</option>
                                                    <option value="1" <?php if ($dados->getPropriedadeTipoImovel()==1){ echo 'selected';};?>>Sobrado</option>
                                                    <option value="2" <?php if ($dados->getPropriedadeTipoImovel()==2){ echo 'selected';};?>>Casa</option>
                                                    <option value="3" <?php if ($dados->getPropriedadeTipoImovel()==3){ echo 'selected';};?>>Barracão</option>
                                                    <option value="4" <?php if ($dados->getPropriedadeTipoImovel()==4){ echo 'selected';};?>>Outros</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group form-inline"><label class="col-sm-2 control-label">Status da Propriedade: </label>
                                            <div class="col-sm-10">
                                                <select class="form-control col-sm-3" name="propriedadeStatusImovel" id="propriedadeStatusImovel" class="chosen-select" style="max-width: 250px" required="required">
                                                    <option value="0" <?php if ($dados->getPropriedadeStatusImovel()==0){ echo 'selected';};?>>Selecione...</option>
                                                    <option value="1" <?php if ($dados->getPropriedadeStatusImovel()==1){ echo 'selected';};?>>Própria</option>
                                                    <option value="2" <?php if ($dados->getPropriedadeStatusImovel()==2){ echo 'selected';};?>>Alugada</option>
                                                    <option value="3" <?php if ($dados->getPropriedadeStatusImovel()==3){ echo 'selected';};?>>Cedida</option>
                                                    <option value="4" <?php if ($dados->getPropriedadeStatusImovel()==4){ echo 'selected';};?>>Comodato</option>
                                                </select>
                                            </div>
                                        </div>


                                        <!-- Opções de Tipo de Imóvel: (Próprio) -->
                                        <div id="proprioImovel">
                                            <hr>
                                            <div class="form-group" id="datapicker_imovel">
                                                <label class="col-sm-2 control-label">Data de Fundação</label>
                                                <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input value="<?php if($dados->getDataFundacaoImovel()!='00/00/0000'){ echo $dados->getDataFundacaoImovel();}?>" name="dataFundacaoImovel" onkeypress="valida_data(this,'dataFundacaoImovel')" maxlength="10" id="dataFundacaoImovel" type="text" class="form-control" style="max-width: 105px">
                                                    <div hidden id="data_invalida_dataFundacaoImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label">Propriedade em nome: </label>
                                                <div class="col-sm-10">
                                                    <select class="form-control col-sm-3" name="propriedadeEmNomeImovel" id="propriedadeEmNomeImovel" style="max-width: 250px">
                                                        <option value="0" <?php if ($dados->getPropriedadeEmNomeImovel()==0){ echo 'selected';};?>>Selecione...</option>
                                                        <option value="1" <?php if ($dados->getPropriedadeEmNomeImovel()==1){ echo 'selected';};?>>GLB</option>
                                                        <option value="2" <?php if ($dados->getPropriedadeEmNomeImovel()==2){ echo 'selected';};?>>GLP</option>
                                                        <option value="3" <?php if ($dados->getPropriedadeEmNomeImovel()==3){ echo 'selected';};?>>O.A.</option>
                                                        <option value="4" <?php if ($dados->getPropriedadeEmNomeImovel()==4){ echo 'selected';};?>>Outros</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group"><label class="col-sm-2 control-label">Possui Escritura: </label>
                                                <div class="col-sm-10">
                                                    <select class="form-control col-sm-3" name="propriedadeEscrituraImovel" id="propriedadeEscrituraImovel" style="max-width: 250px">
                                                        <option value="1">Selecione...</option>
                                                        <option value="0" <?php if ($dados->getPropriedadeEscrituraImovel()==0){ echo 'selected';};?>>Sim</option>
                                                        <option value="1" <?php if ($dados->getPropriedadeEscrituraImovel()==1){ echo 'selected';};?>>Não</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="escrituraImovel">
                                                <div class="form-group" id="tipoSelect"><label class="col-sm-2 control-label">Tipo de Escritura: </label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control col-sm-3" name="fk_idTipoEscrituraImovel" id="fk_idTipoEscrituraImovel" style="max-width: 450px">
                                                            <option>Selecione...</option>
                                                            <?php

                                                            require_once ("model/imovelClass.php");

                                                            $im = new imovel();
                                                            $resultado = $im->listaTipoEscrituraImovel();
                                                            if ($resultado) {
                                                                foreach ($resultado as $vetor) {
                                                                    ?>
                                                                    <option value="<?php echo $vetor['idTipoEscrituraImovel'] ?>" <?php if($dados->getFk_idTipoEscrituraImovel() == $vetor['idTipoEscrituraImovel']){ echo 'selected';} ?> ><?php echo $vetor['tipoEscrituraImovel'] ?></option>
                                                                <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <a data-toggle="modal" data-target="#modalTipoDeEscritura" style="margin-left: 10px"><span class="badge badge-primary" style="margin-top: 7px"><i class="fa fa-plus"></i> Cadastrar</span></a>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="datapicker_escritura">
                                                    <label class="col-sm-2 control-label">Data da Escritura</label>
                                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input value="<?php if($dados->getPropriedadeEscrituraDataImovel()!='00/00/0000'){ echo $dados->getPropriedadeEscrituraDataImovel(); }?>" onkeypress="valida_data(this,'propriedadeEscrituraDataImovel')" maxlength="10" name="propriedadeEscrituraDataImovel" id="propriedadeEscrituraDataImovel" type="text" class="form-control" style="max-width: 105px">
                                                        <div hidden id="data_invalida_propriedadeEscrituraDataImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group"><label class="col-sm-2 control-label">Possui Registro do Imóvel: </label>
                                                <div class="col-sm-10">
                                                    <select class="form-control col-sm-3" name="registroImovel" id="registroImovel" style="max-width: 250px">
                                                        <option value="1">Selecione...</option>
                                                        <option value="0" <?php if ($dados->getRegistroImovel()==0){ echo 'selected';};?>>Sim</option>
                                                        <option value="1" <?php if ($dados->getRegistroImovel()==1){ echo 'selected';};?>>Não</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="registro">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Nome do Registro de Imóveis</label>
                                                    <div class="col-sm-10">
                                                        <input value="<?php echo $dados->getRegistroNomeImovel(); ?>" type="text" name="registroNomeImovel" id="registroNomeImovel" class="form-control" style="max-width: 320px">
                                                    </div>
                                                </div>
                                                <div class="form-group" id="datapicker_registro_imovel">
                                                    <label class="col-sm-2 control-label">Data do Registro de Imóvel</label>
                                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input value="<?php if($dados->getRegistroImovelDataImovel()!='00/00/0000'){ echo $dados->getRegistroImovelDataImovel();}?>" onkeypress="valida_data(this,'registroImovelDataImovel')" maxlength="10" name="registroImovelDataImovel" id="registroImovelDataImovel" type="text" class="form-control" style="max-width: 105px">
                                                        <div hidden id="data_invalida_registroImovelDataImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                    </div>
                                                </div>
                                                <!--
                                                <div class="form-group" id="registroImovelNumero">
                                                    <label class="col-sm-2 control-label">Nº da Matrícula</label>
                                                    <div class="col-sm-10">
                                                        <input value="<?php if($dados->getRegistroImovelNumeroImovel()!=0){ echo $dados->getRegistroImovelNumeroImovel();}?>" type="text" name="registroImovelNumeroImovel" id="registroImovelNumeroImovel" class="form-control" style="max-width: 320px">
                                                    </div>
                                                </div>
                                                -->
                                                <div class="form-group" id="registroImovelNumero">
                                                    <label class="col-sm-2 control-label">
                                                        Matrículas: 
                                                        <a onClick="addInputMatriculaAlterar('<?php echo $idAlteraImovel; ?>');" class="btn btn-xs btn-primary" style="color: white; margin-left: 5px">
                                                            <i class="fa fa-plus fa-white"></i>
                                                        </a>
                                                    </label>
                                                    <div class="col-sm-10" id="campoMatriculas">
                                                        <?php
                                                        $pos=1;
                                                        if($matriculas){
                                                            foreach($matriculas as $matricula){
                                                            ?>
                                                            <p id="paragrafo<?php echo $pos; ?>">
                                                                <input type="text" 
                                                                name="matricula<?php echo $pos; ?>" 
                                                                id="matricula<?php echo $pos; ?>" 
                                                                class="form-control col-sm-2" 
                                                                maxlength="14"
                                                                onkeypress="exibirBotaoAlterarNumeroMatriculaImovel('<?php echo $pos; ?>');"
                                                                value="<?php echo $matricula['numeroImovelMatricula']; ?>"
                                                                style="max-width: 320px">
                                                                <a onClick="removerNumeroMatriculaImovel('<?php echo $idAlteraImovel; ?>','<?php echo $pos; ?>');" 
                                                                   class="btn btn-xs btn-danger" 
                                                                   style="color: white; margin-left: 15px; margin-top: 7px">
                                                                    <i class="fa fa-trash-o fa-white"></i>
                                                                </a>
                                                                <a onClick="alterarNumeroMatriculaImovel('<?php echo $idAlteraImovel; ?>','<?php echo $matricula['numeroImovelMatricula']; ?>','<?php echo $pos; ?>');" 
                                                                class="btn btn-xs btn-success" 
                                                                id="botaoAlterar<?php echo $pos; ?>"
                                                                style="display: none; color: white; margin-left: 7px; margin-top: 7px">
                                                                    <i class="fa fa-save fa-white"></i>
                                                                </a>
                                                                <small id="msg<?php echo $pos; ?>" class="text-navy" style="margin-left: 7px"></small>
                                                            </p>
                                                            <?php
                                                            $pos++;
                                                            }
                                                        } else {
                                                        ?>
                                                        <p id="paragrafo1">
                                                            <input type="text" 
                                                            name="matricula1" 
                                                            id="matricula1" 
                                                            class="form-control col-sm-2" 
                                                            maxlength="14"
                                                            onkeypress="exibirBotaoAlterarNumeroMatriculaImovel('1');"
                                                            style="max-width: 320px">
                                                            <a onClick="removerNumeroMatriculaImovel('<?php echo $idAlteraImovel; ?>','1');" 
                                                               class="btn btn-xs btn-danger" 
                                                               style="color: white; margin-left: 15px; margin-top: 7px">
                                                                <i class="fa fa-trash-o fa-white"></i>
                                                            </a>
                                                            <a onClick="alterarNumeroMatriculaImovel('<?php echo $idAlteraImovel; ?>','','1');" 
                                                                class="btn btn-xs btn-success" 
                                                                id="botaoAlterar1"
                                                                style="display: none; color: white; margin-left: 7px; margin-top: 7px">
                                                                <i class="fa fa-save fa-white"></i>
                                                            </a>
                                                            <small id="msg1" class="text-navy" style="margin-left: 7px"></small>
                                                        </p>
                                                        <?php } ?>
                                                    </div>
                                                    <input type="hidden" name="nroCampo" id="nroCampo" value="<?php echo $pos;?>">
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Valor Venal: </label>
                                                <div class="col-sm-10">
                                                    <div class="input-group" style="max-width: 200px">
                                                        <span class="input-group-addon">R$</span>
                                                        <input value="<?php echo $dados->getValorVenalImovel();?>" type="text" class="form-control currency" maxlength="15" name="valorVenalImovel" id="valorVenalImovel">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"> Valor Atual Aproximado: </label>
                                                <div class="col-sm-10">
                                                    <div class="input-group" style="max-width: 200px">
                                                        <span class="input-group-addon">R$</span>
                                                        <input value="<?php echo $dados->getValorAproximadoImovel();?>" type="text" class="form-control currency" maxlength="15" name="valorAproximadoImovel" id="valorAproximadoImovel">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-sm-2 control-label">Imóvel Avaliado? </label>
                                                <div class="col-sm-10">
                                                    <select class="form-control col-sm-3" name="avaliacaoImovel" id="avaliacaoImovel" style="max-width: 250px">
                                                        <option value="1">Selecione...</option>
                                                        <option value="0" <?php if ($dados->getAvaliacaoImovel()==0){ echo 'selected';};?>>Sim</option>
                                                        <option value="1" <?php if ($dados->getAvaliacaoImovel()==1){ echo 'selected';};?>>Não</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="avaliacaoValor"></div>
                                            <div id="avaliacao">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Valor Avaliado: </label>
                                                    <div class="col-sm-10">
                                                        <div class="input-group" style="max-width: 200px">
                                                            <span class="input-group-addon">R$</span>
                                                            <input value="<?php echo $dados->getAvaliacaoValorImovel();?>" type="text" class="form-control currency" maxlength="15" name="avaliacaoValorImovel" id="avaliacaoValorImovel">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Nome do Avaliador</label>
                                                    <div class="col-sm-10">
                                                        <input value="<?php echo $dados->getAvaliacaoNomeImovel();?>" type="text" name="avaliacaoNomeImovel" id="avaliacaoNomeImovel" class="form-control" style="max-width: 320px">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Creci do Avaliador</label>
                                                    <div class="col-sm-10">
                                                        <input value="<?php echo $dados->getAvaliacaoCreciImovel();?>" type="text" name="avaliacaoCreciImovel" id="avaliacaoCreciImovel" class="form-control" style="max-width: 320px">
                                                    </div>
                                                </div>
                                                <div class="form-group" id="datapicker_escritura">
                                                    <label class="col-sm-2 control-label">Data da Avaliação:</label>
                                                    <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input value="<?php if($dados->getAvaliacaoDataImovel()!='00/00/0000'){ echo $dados->getAvaliacaoDataImovel();}?>" onkeypress="valida_data(this,'avaliacaoDataImovel')" maxlength="10" name="avaliacaoDataImovel" id="avaliacaoDataImovel" type="text" class="form-control" style="max-width: 105px">
                                                        <div hidden id="data_invalida_avaliacaoDataImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Opções de Tipo de Imóvel: (Alugado/ Cedido/ Comodato) -->
                                        <div id="alugadoImovel">
                                            <hr>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Locador/Cessionário: </label>
                                                <div class="col-sm-10">
                                                    <input value="<?php echo $dados->getLocadorImovel();?>" type="text" name="locadorImovel" id="locadorImovel" class="form-control" style="max-width: 320px">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Locatário/Cedente: </label>
                                                <div class="col-sm-10">
                                                    <input value="<?php echo $dados->getLocatarioImovel();?>" type="text" name="locatarioImovel" id="locatarioImovel" class="form-control" style="max-width: 320px">
                                                </div>
                                            </div>
                                            <div class="form-group" id="datapicker_contrato_inicio">
                                                <label class="col-sm-2 control-label">Início do Contrato/Acordo: </label>
                                                <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input value="<?php if($dados->getAluguelContratoInicioImovel()!='00/00/0000'){ echo $dados->getAluguelContratoInicioImovel();}?>" onkeypress="valida_data(this,'aluguelContratoInicioImovel')" maxlength="10" name="aluguelContratoInicioImovel" id="aluguelContratoInicioImovel" type="text" class="form-control" style="max-width: 105px">
                                                    <div hidden id="data_invalida_aluguelContratoInicioImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="datapicker_contrato_fim">
                                                <label class="col-sm-2 control-label">Término do Contrato/Acordo: </label>
                                                <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input value="<?php if($dados->getAluguelContratoFimImovel()!='00/00/0000'){ echo $dados->getAluguelContratoFimImovel();}?>" onkeypress="valida_data(this,'aluguelContratoFimImovel')" maxlength="10" name="aluguelContratoFimImovel" id="aluguelContratoFimImovel" type="text" class="form-control" style="max-width: 105px">
                                                    <div hidden id="data_invalida_aluguelContratoFimImovel" style="color: #FA8072; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Data inválida!</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Valor de Aluguel: </label>
                                                <div class="col-sm-10">
                                                    <div class="input-group" style="max-width: 200px">
                                                        <span class="input-group-addon">R$</span>
                                                        <input value="<?php echo $dados->getAluguelValorImovel();?>" type="text" class="form-control currency" maxlength="15" name="aluguelValorImovel" id="aluguelValorImovel">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Anotações Complementares: </label>
                                            <div class="col-sm-10">
                                                <div style="border: #ccc solid 1px; width: 500px">
                                                    <div class="mail-text h-200">
                                                        <textarea class="summernote" id="descricaoImovel" name="descricaoImovel"><?php echo $dados->getDescricaoImovel();?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

										<hr>
										<div class="form-group">
										    <div class="col-sm-4 col-sm-offset-2">
                                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
										    	<input type="hidden" name="idAlteraImovel" id="idAlteraImovel" value="<?php echo $idAlteraImovel; ?>" />
										    	<input type="hidden" name="fk_idOrganismoAfiliado" id="fk_idOrganismoAfiliado" value="<?php echo $dados->getFk_idOrganismoAfiliado(); ?>" />
												<input type="hidden" name="statusImovel" id="statusImovel" value="<?php echo $dados->getStatusImovel(); ?>" />
										        <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar" onclick="startProgress();" />
                                                <a class="btn btn-white" href="?corpo=buscaImovel" id="cancelar" name="cancelar">Cancelar</a>
										    </div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tabela Fim -->

                <!-- Conteúdo de Modal Início -->

                <!-- Modal Cadastrar Tipo de Escritura Início -->
                <div class="modal inmodal fade" id="modalTipoDeEscritura" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
                                <h4 class="modal-title">Cadastrar Novo Tipo de Escritura</h4>
                            </div>
                            <div class="modal-body">
                                <br>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-2 control-label">Tipo de Escritura: </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="tipoEscritura" id="tipoEscritura" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-12" id="alertTipoDeEscritura">
                                        <div class="alert alert-info" style="text-align: justify">
                                            <center>
                                                Contribua com um novo <a class="alert-link">Tipo de Escritura</a>.
                                                O qual será visível por todos!
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal" onclick="trocaAlertTiposDeEscritura();">Fechar</button>
                                <button type="button" class="btn btn-primary" onclick="cadastraTipoDeEscritura();">Salvar nova opção</button>
                            </div>
                        </div>
                    </div>
                </div>

		<!-- OnLoad Progress -->
		<div class="modal inmodal" id="mySeeLoading" tabindex="-1" role="dialog" aria-hidden="true">
    			<div class="modal-dialog modal-lg" >
        			<div class="cssload-box-loading" style="margin: 30% 50% 50% 50%;">
				</div>
    			</div>
		</div>

                <!-- Conteúdo de Modal Fim -->

				<!-- Conteúdo DE INCLUDE FIM -->
				
				