<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();

include_once("controller/usuarioController.php");
$um     = new Usuario();
require_once ("controller/organismoController.php");
$oc     = new organismoController();
include_once("model/relatorioGrandeConselheiroPlanoClass.php");
$rgcp = new relatorioGrandeConselheiroPlano();

$anexo	= isset($_FILES['anexo']['name'])?$_FILES['anexo']['name']:null;

/**
 * Upload do Plano de Gestão
 */

$planoGestao=0;
$extensaoNaoValida=0;
if($anexo != "")
{
	if ($_FILES['anexo']['name'] != "") {
            $extensao = strstr($_FILES['anexo']['name'], '.');
            if($extensao==".pdf"||
                    $extensao==".png"||
                    $extensao==".jpg"||
                    $extensao==".jpeg"||
                    $extensao==".PDF"||
                    $extensao==".PNG"||
                    $extensao==".JPG"||
                    $extensao==".JPEG"
                    )
            {
		$extensao = strstr($_FILES['anexo']['name'], '.');
		$proximo_id = $rgcp->selecionaUltimoId();
		$caminho = "img/relatorio_grande_conselheiro/" . basename($_POST["idRelatorioGrandeConselheiroUpload"].".plano.gestao.relatorio.grande.conselheiro.".$proximo_id.$extensao);
		$rgcp->setFkIdRelatorioGrandeConselheiro($_POST["idRelatorioGrandeConselheiroUpload"]);
		$rgcp->setCaminho($caminho);
		$rgcp->setNomeArquivo($_FILES['anexo']['name']);
		$rgcp->setExtensao($extensao);
		if($rgcp->cadastroPlano())
		{
			$uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/img/relatorio_grande_conselheiro/';
			//$uploaddir = 'C:\/xampp\/htdocs\/soa\/img\/relatorio_grande_conselheiro\/';
			$uploadfile = $uploaddir . basename($_POST["idRelatorioGrandeConselheiroUpload"].".plano.gestao.relatorio.grande.conselheiro.".$proximo_id.$extensao);
			if(move_uploaded_file($_FILES['anexo']['tmp_name'], $uploadfile))
			{
				$planoGestao = 1;
			}else{
				echo "<script type='text/javascript'>alert('Erro ao enviar o plano de gestão! Avise a TI');</script>";
			}
		}
            }else{
                $extensaoNaoValida=1;
            }
	}
}

include_once("model/relatorioGrandeConselheiroClass.php");
$gc = new relatorioGrandeConselheiro();

if($planoGestao==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Plano de Gestão enviado com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->

<?php 
$salvo = isset($_REQUEST['salvo'])?$_REQUEST['salvo']:null;
if($salvo==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Relatório salvo com sucesso!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<?php 
$jaCadastrado = isset($_REQUEST['jaCadastrado'])?$_REQUEST['jaCadastrado']:null;
if($jaCadastrado==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Relatório já cadastrado anteriormente!",
        type: "error",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }

if($extensaoNaoValida==1){?>
<script>
window.onload = function(){
	swal({
        title: "Aviso!",
        text: "Extensão do arquivo inválida! O sistema só aceita as extensões: JPG, JPEG, PNG ou PDF!",
        type: "warning",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }
?>

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Relatório Semestral dos Grandes Conselheiros</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaRelatorioGrandeConselheiro">Relatórios</a>
            </li>
            <li>
                <a href="?corpo=buscaRelatorioGrandeConselheiro">Relatório Semestral dos Grandes Conselheiros</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Lista dos Relatórios Semestrais dos Grandes Conselheiros</h5>
                    <div class="ibox-tools">
                    	<?php if(in_array("1",$arrNivelUsuario)){?>
                        <a class="btn btn-xs btn-primary" href="?corpo=cadastroRelatorioGrandeConselheiro">
                            <i class="fa fa-plus"></i> Novo Relatório
                        </a>
                        <?php }?>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Cód.</th>
                                <th>Região</th>
                                <th><center>Semestre</center></th>
                        <th><center>Data da Entrega</center></th>
                        <th><center>Plano de Gestão e outros relatórios</center></th>
                                <th>
                                    <center>Data de entrega (Ass. Eletrônica) </center>
                                </th>
                                <th>
                                    <center>Assinaturas</center>
                                </th>
                        <th><center>Ações</center></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("controller/relatorioGrandeConselheiroController.php");

                            $rsgcc = new relatorioGrandeConselheiroController();
                            $resultado = $rsgcc->listaRelatorioGrandeConselheiro();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                	
                                	$resposta3 = $rgcp->listaRelatorioGrandeConselheiroPlano($vetor['idRelatorioGrandeConselheiro']);
                                    if($resposta3)
                                    {
                                    	$total = count($resposta3);
                                    }else{
                                    	$total = 0;
                                    }
	
                                    ?>
                                    <?php if((strtoupper($vetor["regiaoRosacruz"])==strtoupper(substr($sessao->getValue("siglaOA"),0,3)))||(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2))){?>
                                    <tr>
                                        <td>
                                            <?php echo $vetor['idRelatorioGrandeConselheiro']; ?>
                                        </td>
                                        <td>
                                            <?php echo $vetor["regiaoRosacruz"]; ?>
                                        </td>
                                        <td>
                                <center>
                                    <?php echo $vetor['semestre'] . "/" . $vetor['ano']; ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <?php echo substr($vetor['dataCriacao'], 8, 2) . "/" . substr($vetor['dataCriacao'], 5, 2) . "/" . substr($vetor['dataCriacao'], 0, 4); ?>
                                </center>
                                </td>
                                <td>
                                    <center><?php if($total>0){?><button class="btn btn-info  dim" type="button" onclick="listaUploadRelatorioGestao('<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>')" data-target="#mySeeListaUpload" data-toggle="modal" data-placement="left"><i class="fa fa-paste"></i> </button><?php }else{ echo "Não enviado";}?></center>
                                </td>
                                        <td>
                                            <div id="pronto<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>">
                                                <center>
                                                    <?php if($vetor['entregue']==1){?>
                                                        <?php echo substr($vetor['dataEntrega'],8,2);?>/<?php echo substr($vetor['dataEntrega'],5,2);?>/<?php echo substr($vetor['dataEntrega'],0,4);?> às <?php echo substr($vetor['dataEntrega'],11,8);?>
                                                    <?php }else{?>
                                                        <b>Não Informado</b>
                                                    <?php }?>
                                                </center>
                                            </div>
                                        </td>
                                        <td>
                                            <div>
                                                <center>
                                                    <?php if($vetor['entregueCompletamente']==1){?>
                                                        <?php echo substr($vetor['dataEntregouCompletamente'],8,2);?>/<?php echo substr($vetor['dataEntregouCompletamente'],5,2);?>/<?php echo substr($vetor['dataEntregouCompletamente'],0,4);?> às <?php echo substr($vetor['dataEntregouCompletamente'],11,8);?>
                                                    <?php }else{?>
                                                        <b>Faltam assinaturas</b>
                                                    <?php }?>
                                                </center>
                                            </div>
                                        </td>
                                <td>
                                <center>
                                    <?php
                                    $grandeConselheiro = $gc->buscarAssinaturasEmRelatorioGrandeConselheiroPorDocumento($vetor['idRelatorioGrandeConselheiro'],329);

                                    //Montar Faltam assinar
                                    $arr=array();
                                    if(!$grandeConselheiro)
                                    {
                                        $arr[]="GRANDE CONSELHEIRO";
                                    }

                                    $oficiaisFaltamAssinar = implode(",",$arr);
                                    ?>
                                    <?php if(in_array("2",$arrNivelUsuario)&&$vetor['entregueCompletamente']==0){?>
                                    <a href="?corpo=alteraRelatorioGrandeConselheiro&idrelatorio_grande_conselheiro=<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Editar">
                                        <i class="fa fa-edit fa-white"></i>&nbsp;
                                        Editar
                                    </a>
                                    <?php }?>
                                    <button type="button" class="btn btn-sm btn-success" onclick="abrirPopUpImprimirRelatorioGrandeConselheiro('<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>');" data-toggle="tooltip" data-placement="left" title="Imprimir">
                                        <i class="fa fa-print fa-white"></i>&nbsp;
                                        Imprimir
                                    </button>

                                    <?php if(in_array("4",$arrNivelUsuario)){?>
                                    <button type="button" class="btn btn-sm btn-info" onclick="document.getElementById('idRelatorioGrandeConselheiroUpload').value=<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>" data-target="#mySeeUpload" data-toggle="modal" data-placement="left" title="Upload">
                                        <i class="fa fa-cloud-upload fa-white"></i>&nbsp;
                                        Envio do Plano de Gestão e outros relatórios
                                    </button>
                                    <?php }?>
                                    <?php if($vetor['entregue']==0){?>
                                    <a class="btn btn-sm btn-warning" href="#" id="entregar<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>"
                                       onclick="entregarRelatorioGrandeConselheiro('<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>','<?php echo $_SESSION['seqCadast']; ?>')">
                                        <i class="fa fa-location-arrow icon-white"></i>
                                        Entregar com Ass. Eletrônica
                                    </a>
                                    <?php }?>
                                    <?php
                                    if($vetor['entregueCompletamente']==0) {
                                        ?>
                                    <button type="button"
                                            id="faltaAssinar<?php echo $vetor['idRelatorioGrandeConselheiro']; ?>"
                                            class="btn btn-default" data-container="body"
                                            data-toggle="popover" data-placement="top"
                                            data-content="<?php if($grandeConselheiro){?>Grande Conselheiro assinou. Processo de entrega finalizado!<?php } else { ?>Faltam assinar: <?php echo $oficiaisFaltamAssinar;}?>
                                                                " data-original-title="" title=""
                                            aria-describedby="popover408083"
                                            style="display: <?php if ($vetor['entregue'] == 0) { ?>none<?php } else { ?>block<?php } ?>">
                                        <i class="fa fa-pencil"></i> Acompanhar assinaturas
                                    </button>
                                    <?php
                                    }else{
                                        ?>
                                    <span class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Documento 100% entregue</span>
                                    <?php
                                    }
                                    ?>
                                </center>
                                </td>
                                </tr>
                            <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->


<div class="modal inmodal" id="mySeeUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Envio do Plano de Gestão e outros relatórios</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
                <form id="ata" name="ata" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<!--  
						<div class="alert alert-danger">
                               <a class="alert-link" href="#">Atenção</a> caso precise atualizar o documento enviado, basta enviar novamente através do botão abaixo.
                        </div>
                        -->
						<div class="col-sm-10">
							<input name="anexo"
								id="anexo" type="file" />
						</div>
						<div id="anexoAlerta" class="col-sm-10"
							style="color: #4F5B93; font-weight: normal; margin-top: 7px;">&nbsp;&nbsp;&nbsp;Tamanho
							máximo de 10MB.</div>
							
					</div>
					<input type="hidden" id="idRelatorioGrandeConselheiroUpload" name="idRelatorioGrandeConselheiroUpload" value="0">
                </form>
            </div>
            <div class="modal-footer">
            	<input type="submit" class="btn btn-info" value="Enviar" onclick="document.ata.submit();">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="mySeeListaUpload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInUp">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-cloud-upload modal-icon"></i>
                <h4 class="modal-title">Plano de Gestão e outros relatórios</h4>
                <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
            </div>
            <div class="modal-body">
					<div class="form-group">
						<div id="listaUpload"></div>
					</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php 
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
