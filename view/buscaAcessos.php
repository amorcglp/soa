<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Acessos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li class="active">
                <a href="">Acessos</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<!-- Caminho de Migalhas Fim -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Acessos ao SOA</h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>SeqCadast</th>
                                <th>IP</th>
                                <th>Nome</th>
                                <th>Login</th>
                                <th>Sigla OA</th>
                                <th>Data de Acesso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            include_once("model/acessoClass.php");

                            $ac = new acesso();
                            $resultado = $ac->listaAcesso();

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $vetor['idAcesso']; ?>
                                </td>
                                <td>
                                    <?php echo $vetor['seqCadast']; ?>
                                </td>
                                <td>
                                    <?php echo $vetor['ip']; ?>
                                </td>
                                <td>
                                    <?php echo $vetor['nome']; ?>
                                </td>
                                <td>
                                    <?php echo $vetor['login']; ?>
                                </td>
                                <td>
                                    <?php echo $vetor['siglaOA']; ?>
                                </td>
                                <td>
                                    <?php
                                    $data_mysql = $vetor['dataCadastro'];
                                    $timestamp = strtotime($data_mysql);
                                    echo date('d/m/Y H:i:s', $timestamp);
                                    ?>
                                </td>
                            </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>