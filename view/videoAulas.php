<?php
 @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
      
      include_once("model/videoAulaVisualizacaoClass.php");
?>
<script src='js/pubhtml5-light-box-api-min.js'></script>
<body>
    <h1>Biblioteca de Vídeos-Aulas</h1>
    <input type="hidden" name="arquivo" id="arquivo" value="">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Bibliotecas de Vídeos-Aulas</h5>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th><center>Título</center></th>
                            <th><center>Descrição</center></th>
                            <th><center>Visualização</center></th>
                            <th><center>Ações</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            include_once("controller/videoAulaController.php");

                            $c = new videoAulaController();
                            $resultado = $c->listaVideoAula(true);
//echo "<pre>";print_r($resultado);
                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                    $vis = new videoAulaVisualizacao();
                                    //echo print_r($vis);
                                    $total=0;
                                    
                                    $resultado2 = $vis->contaVisualizacao($_SESSION['seqCadast'],$vetor['idVideoAula']);
                                    //echo "<pre>";print_r($resultado2);
                                    if($resultado2)
                                    {
                                        $total = count($resultado2);
                                    } 
                                       
                                    ?>
                                <tr>
                                    <td> <?php echo $vetor['titulo']; ?></td>
                                    <td> <?php echo $vetor['descricao']; ?></td>
                                    <td> <div id="visualizacao<?php echo $vetor['idVideoAula']; ?>"><?php if($total>0){?><span class="badge badge-success">Visualizada</span><?php }else{ ?> <span class="badge badge-danger">Não Visualizada</span><?php }?></div></td>
                                    <td>
                            <center>
                                <a href="#"  data-rel='fh5-light-box-demo' data-href=' <?php echo $vetor['link']; ?>' data-rel='fh5-light-box-demo' data-href='https://online.pubhtml5.com/umzz/etic/' data-width='900' data-height='500' data-title='<?php echo $vetor['titulo']; ?>'>
                                    <button type="button" id="visu" class="btn btn-sm btn-success" onclick="visualizarVideoAula('<?php echo $_SESSION['seqCadast']; ?>','<?php echo $vetor['idVideoAula']; ?>');">
                                        <i class="fa fa-video-camera fa-white"></i>&nbsp;
                                        Visualizar
                                    </button>
                                </a>
                            </center>
                            </td>
                            </tr>
                                <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#visu').click(
            function() {
                
            });
        })
    </script>    