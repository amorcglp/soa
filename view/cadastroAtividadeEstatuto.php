<?php @include_once("lib/functions.php");
      @include_once("../lib/functions.php");
      @usuarioOnline();
      
    date_default_timezone_set('America/Sao_Paulo');
    include_once 'controller/atividadeEstatutoTipoController.php';
    include_once 'controller/organismoController.php';
    $aetc   = new atividadeEstatutoTipoController();
    $oc     = new organismoController();
    $dadosOrganismo = $oc->buscaOrganismo($idOrganismoAfiliado);

    //Buscar informações no webservice para pegar atualizações vindas do ORCZ
    $ocultar_json=1;
    $siglaOA=$dadosOrganismo->getSiglaOrganismoAfiliado();
    switch ($dadosOrganismo->getPaisOrganismoAfiliado())
    {
        case 1:
            $pais="BR";
            break;
        case 2:
            $pais="PT";
            break;
        case 3:
            $pais="AO";
            break;
        default :
            $pais="BR";
            break;
    }
    $paisDoOrganismo = $pais;
    //echo $pais;
    include 'js/ajax/retornaDadosOrganismo.php';
    //echo "<pre>";print_r($return);
    $elevacaoLojaWS = trim($return->result[0]->fields->fArrayOA[0]->fields->fDatElevacLoja);
    $elevacaoCapituloWS =  trim($return->result[0]->fields->fArrayOA[0]->fields->fDatElevacCapitu);
    $elevacaoPronaosWS =  trim($return->result[0]->fields->fArrayOA[0]->fields->fDatElevacPronau);

    //Tratamento das datas
    $elevacaoLoja                   = substr($elevacaoLojaWS,0,7);
    $elevacaoCapitulo               = substr($elevacaoCapituloWS,0,7);
    $elevacaoPronaos                = substr($elevacaoPronaosWS,0,7);

    $elevacao=6;//para não carregar nada
    $entrouAqui=0;
    $textoClassificacao="";


    $elevacao = $dadosOrganismo->getClassificacaoOrganismoAfiliado();

    if($dadosOrganismo->getAtuacaoTemporaria()==0){

        $elevacaoLoja                   = strtotime($elevacaoLoja."-01");
        $elevacaoCapitulo               = strtotime($elevacaoCapitulo."-01");
        $elevacaoPronaos                = strtotime($elevacaoPronaos."-01");
        $dataAtual                      = strtotime(date('Y')."-".date('m')."-01");


        
        if(($elevacaoPronaosWS!="")&&($dataAtual>=$elevacaoPronaos)) {
            $elevacao=2;
            $textoClassificacao="Pronaos";
        }
        if(($elevacaoCapituloWS!="")&&($dataAtual>=$elevacaoCapitulo)) {
            $elevacao=3;
            $textoClassificacao="Capítulo";
        }
        if(($elevacaoLojaWS!="")&&($dataAtual>=$elevacaoLoja)) {
            $entrouAqui=777;
            $elevacao=1;
            $textoClassificacao="Loja";
        }
    }else{    
        $atuacaoTemporaria  = $dadosOrganismo->getAtuacaoTemporaria();
    }

    //echo "elevação=>".$elevacao;
      ?>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividade Para Organismos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaAtividadeEstatutoTipo">Tipos de Atividade</a>
            </li>
            <li class="active">
                <strong>
                    <a>Cadastro de Tipos de Atividades</a>
                </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Tipos de Atividade</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeEstatuto">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="post" onsubmit="return verificaCamposPreenchidosAtividadeSubmit();" class="form-horizontal" action="acoes/acaoCadastrar.php">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Atividade de <?php echo $textoClassificacao;?>: <?php //echo $elevacao;?></label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-5 chosen-select" onchange="retornaPeriodicidadeTipoAtividade(this.value);" name="fk_idTipoAtividadeEstatuto" id="fk_idTipoAtividadeEstatuto" data-placeholder="Selecione um organismo..." style="max-width: 500px" required="required">
                                    <option value="0">Selecione...</option>
                                    <?php
                                    if($atuacaoTemporaria==0)
                                    {    
                                        $aetc->criarComboBox(0, $elevacao,null,true,true);
                                    }else{
                                        $aetc->criarComboBox(0, $atuacaoTemporaria,null,true,true);
                                    }
                                    ?>
                                </select>&nbsp;<br><p id="spanPeriodicidade">Periodicidade: -- <p>
                            </div>
                            <input type="hidden" id="janeiro" value="0">
                            <input type="hidden" id="fevereiro" value="0">
                            <input type="hidden" id="marco" value="0">
                            <input type="hidden" id="abril" value="0">
                            <input type="hidden" id="maio" value="0">
                            <input type="hidden" id="junho" value="0">
                            <input type="hidden" id="julho" value="0">
                            <input type="hidden" id="agosto" value="0">
                            <input type="hidden" id="setembro" value="0">
                            <input type="hidden" id="outubro" value="0">
                            <input type="hidden" id="novembro" value="0">
                            <input type="hidden" id="dezembro" value="0">
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                Complemento ao Nome: <br>
                                <small class="text-navy">
                                    Preenchimento não obrigatório.
                                </small>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" name="complementoNomeAtividadeEstatuto" id="complementoNomeAtividadeEstatuto" class="form-control" maxlength="60" style="max-width: 320px">
                                <p>
                                    (ideal para diferenciar atividades ou informar qual atividade do calendário anual está sendo cadastrado)
                                </p>
                            </div>
                        </div>
                        <div class="form-group" id="datapicker_anotacoes">
                            <label class="col-sm-2 control-label">Data</label>
                            <div class="col-sm-10 input-group date" style="padding: 0px 0px 0px 15px">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="dataAtividadeEstatuto" id="dataAtividadeEstatuto" onkeypress="valida_data(this)" maxlength="10" type="text" class="form-control" style="max-width: 105px">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Hora: </label>
                            <div class="col-sm-10">
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                    <input type="text" class="form-control" id="horaAtividadeEstatuto" name="horaAtividadeEstatuto" value="00:00" style="max-width: 70px">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Local: </label>
                            <div class="col-sm-10">
                                <input type="text" name="localAtividadeEstatuto" id="localAtividadeEstatuto" class="form-control" maxlength="60" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título do Discurso: </label>
                            <div class="col-sm-10">
                                <input type="text" name="tituloDiscursoAtividadeEstatuto" id="tituloDiscursoAtividadeEstatuto" class="form-control" maxlength="100" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Orador: </label>
                            <div class="col-sm-10">
                                <input type="text" name="nomeOradorDiscursoAtividadeEstatuto" id="nomeOradorDiscursoAtividadeEstatuto" class="form-control" maxlength="100" style="max-width: 320px" required="required">
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição: </label>
                            <div class="col-sm-10">
                                <div style="border: #ccc solid 1px; max-width: 850px">
	                            	<div class="mail-text h-200">
										<textarea class="summernote" id="descricaoAtividadeEstatuto" name="descricaoAtividadeEstatuto"></textarea>
									</div>
								</div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="hidden" id="fk_seqCadastAtualizadoPor" name="fk_seqCadastAtualizadoPor" value="<?php echo $sessao->getValue("seqCadast") ?>">
                                <input type="hidden" id="fk_idOrganismoAfiliado" name="fk_idOrganismoAfiliado" value="<?php echo $idOrganismoAfiliado; ?>">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <a class="btn btn-white" href="?corpo=buscaAtividadeEstatuto"> Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->



<!-- Input Mask-->
<!--<script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>-->

<!-- Data picker -->
    <!--<script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>-->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>