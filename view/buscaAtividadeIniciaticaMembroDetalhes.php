<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
?>

<script src="js/jquery-2.1.1.js"></script>
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atividades Iniciáticas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Início</a>
            </li>
            <li class="active">
                <a href="">Detalhe de Atividades Iniciáticas dos Membro</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<?php
flush();
    include_once("controller/atividadeIniciaticaController.php");
    include_once("controller/atividadeIniciaticaOficialController.php");
    include_once("controller/organismoController.php");
    include_once("model/membroPotencialOAClass.php");

    $aic = new atividadeIniciaticaController();
    $ai = new atividadeIniciatica();
    $aioc = new atividadeIniciaticaOficialController();
    $aiom = new atividadeIniciaticaOficial();
    $oc = new organismoController();
    $o = new organismo();
    $mp = new membroPotencialOA();

    $sessao = new criaSessao();

    $idOrganismo	= isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';

    if($idOrganismo == ''){
        echo "<script type='text/javascript'>window.location = './painelDeControle.php?corpo=buscaAtividadeIniciatica';</script>";
    }

    if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)) {
        $idOrg = $idOrganismo;
    } else {
        $idOrg = $idOrganismoAfiliado;
    }
?>
<?php
flush();
$resultado = $mp->listaMembrosPorIdOrganismo($idOrg);

$arrayMembros=array();

$ocultar_json=1;

$b=0;
if ($resultado) {
    foreach ($resultado as $vetor) {
        $seqCadast = $vetor['seqCadastMembroOa'];

        $arrayMembros[$b]['seqCadast']                  = $seqCadast;
        $arrayMembros[$b]['nome']                       = $vetor['fNomCliente'];
        $arrayMembros[$b]['codAfiliacao']               = $vetor['fCodRosacruz'];
        $arrayMembros[$b]['lote']                       = $vetor['fNumLoteAtualRosacr'];
        $arrayMembros[$b]['tempoDeMembro']              = $vetor['fDatImplantacaoCadastro'];
        $arrayMembros[$b]['situacaoRemessa']            = $vetor['fIdeTipoSituacRemessRosacr'];

        include './js/ajax/retornaObrigacoesRitualisticasDoMembro.php';
        $obj2 = json_decode(json_encode($return),true);
        //echo "<pre>";print_r($obj2);echo "</pre>";

        if (count($obj2['result'][0]['fields']['fArrayObrigacoes']) > 0) {
            $c = 0;
            foreach ($obj2['result'][0]['fields']['fArrayObrigacoes'] as $vetorObrigacoes) {
                //echo "<pre>";print_r($vetorObrigacoes);echo "</pre>";

                if(($vetorObrigacoes['fields']['fSeqTipoObriga'] == 0) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 1) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 3) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 5) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 7) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 9) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 11) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 13) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 15) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 17) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 19) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 32) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 33) ||
                    ($vetorObrigacoes['fields']['fSeqTipoObriga'] == 34))
                {
                    $arrayMembros[$b]['atividades'][] = $vetorObrigacoes['fields']['fSeqTipoObriga'];
                    $arrayMembros[$b]['atividadesDados'][] = $vetorObrigacoes['fields'];
                    $c++;
                }
            }
            if($c==0){
                $arrayMembros[$b]['atividades'][] = 0;
            }
        } else {
            $arrayMembros[$b]['atividades'][] = 0;
        }
        $b++;
    }
}
flush();
for($i=0; $i < count($arrayMembros); $i++){
    if(!empty($arrayMembros[$i]['atividades']) && !empty($arrayMembros[$i]['atividadesDados'])){
        if (!sort($arrayMembros[$i]['atividades'])) {
            exit();
        }
        if (!sort($arrayMembros[$i]['atividadesDados'])) {
            exit();
        }
    }
}
//echo "<pre>";print_r($arrayMembros);echo "</pre>";
?>

<div class="wrapper wrapper-content animated fadeInRight">
<?php flush(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        Detalhe de Atividades Iniciáticas dos Membro
                        <?php
                        if($idOrganismo!=''){
                            $nomeOa = retornaNomeCompletoOrganismoAfiliado($idOrganismo);
                            echo ' - '.$nomeOa;
                        }
                        ?>
                    </h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaAtividadeIniciatica">
                            <i class="fa fa-reply"></i> Voltar para área de Atividades
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <!--<input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">-->
                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
                        <thead>
                            <tr>
                                <th data-toggle="true">Nome</th>
                                <th>Cód. Afiliação</th>

                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>
                                <th data-hide="all">----------------------------------</th>
                                <th data-hide="all">Atividade Iniciática</th>
                                <th data-hide="all">Data</th>
                                <th data-hide="all">Organismo</th>

                                <th>Lote</th>
                                <th>Membro Desde</th>
                                <!--
                                <th>Ações</th>
                                -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if(!empty($arrayMembros)){
                                for($i=0; $i < count($arrayMembros); $i++){
                                    ?>
                                    <tr>
                                        <td><?php echo $arrayMembros[$i]['nome'];?></td>
                                        <td><?php echo $arrayMembros[$i]['codAfiliacao'];?></td>

                                        <?php
                                        if(!empty($arrayMembros[$i]['atividadesDados'])){


                                            if(in_array("3", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==3) {

                                                        echo "<td>1º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 1º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("5", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==5) {

                                                        echo "<td>2º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 2º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("7", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==7) {

                                                        echo "<td>3º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 3º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("9", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==9) {

                                                        echo "<td>4º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 4º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("11", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==11) {

                                                        echo "<td>5º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 5º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("13", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==13) {

                                                        echo "<td>6º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 6º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("15", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==15) {

                                                        echo "<td>7º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 7º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("17", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==17) {

                                                        echo "<td>8º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 8º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("19", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==19) {

                                                        echo "<td>9º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 9º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("32", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==32) {

                                                        echo "<td>10º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 10º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("33", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==33) {

                                                        echo "<td>11º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                        echo "<td>------------------------------------------</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 11º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                                echo "<td>------------------------------------------</td>";
                                            }

                                            if(in_array("34", $arrayMembros[$i]['atividades'])){
                                                for($z=0; $z < count($arrayMembros[$i]['atividadesDados']); $z++) {
                                                    if($arrayMembros[$i]['atividadesDados'][$z]['fSeqTipoObriga']==34) {

                                                        echo "<td>12º Grau de Templo</td>";
                                                        echo "<td><span class='pie'>" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 8, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 5, 2) . "/" . substr($arrayMembros[$i]['atividadesDados'][$z]['fDatObriga'], 0, 4) . "</span></td>";
                                                        echo "<td>" . retornaNomeCompletoOrganismoAfiliadoPelaSigla($arrayMembros[$i]['atividadesDados'][$z]['fSigOrgafi']) . "</td>";
                                                    }
                                                }
                                            } else {
                                                echo "<td> 12º Grau de Templo (Não Realizado/Informado)</td>";
                                                echo "<td><span class='pie'> -- </span></td>";
                                                echo "<td> -- </td>";
                                            }

                                        } else {
                                            imprimeDadosMembroSemIniciacoes();
                                        }
                                        ?>

                                        <td><?php echo $arrayMembros[$i]['lote'];?></td>
                                        <td><?php
                                                echo substr($arrayMembros[$i]['tempoDeMembro'], 8, 2) . "/" . substr($arrayMembros[$i]['tempoDeMembro'], 5, 2) . "/" . substr($arrayMembros[$i]['tempoDeMembro'], 0, 4);
                                            ?></td>
                                        <!--
                                        <td><a class="btn btn-xs btn-warning"><i class="fa fa-envelope text-white"></i> Informar Ajustes</a></td>
                                        -->
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Modal's da página INICIO -->


<!-- Modal's da página FIM -->

<!-- Conteúdo DE INCLUDE FIM -->
