
<!-- Conteúdo DE INCLUDE INICIO -->

<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Opção Sub-menu</h2>
        <ol class="breadcrumb">
            <li>
                <a href="">Home</a>
            </li>
            <li>
                <a href="?corpo=buscaOpcaoSubMenu">Opção Sub-menu</a>
            </li>
            <li class="active">
                <a>Cadastro de Novo Sub-menus</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cadastro de Nova Opção Sub-menu</h5>
                    <div class="ibox-tools">
                        <a class="btn btn-xs btn-warning" style="color: white" href="?corpo=buscaOpcaoSubMenu">
                            <i class="fa fa-reply fa-white"></i>&nbsp; Voltar
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form  id="cadastro" class="form-horizontal" method="POST" class="form-horizontal" action="acoes/acaoCadastrar.php">
                        <div class="form-group"><label class="col-sm-2 control-label">Opção Sub-menu</label>
                            <div class="col-sm-10"><input type="text" name="nomeOpcaoSubMenu" id="nomeOpcaoSubMenu" class="form-control" value="" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Seção</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idSecaoMenu" id="fk_idSecaoMenu" style="max-width: 150px" onChange="carregaCombo(this.value, 'fk_idSubMenu')" required="required">
                                    <option value="">Selecione...</option>
                                    <?php
                                    include_once 'controller/secaoMenuController.php';
                                    $sm = new secaoMenuController();
                                    $sm->criarComboBox();
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Sub-menu</label>
                            <div class="col-sm-10">
                                <select class="form-control col-sm-3" name="fk_idSubMenu" id="fk_idSubMenu" style="max-width: 150px" required="required">
                                    <option value="">Selecione...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Arquivo</label>
                            <div class="col-sm-10"><input type="text" name="arquivoOpcaoSubMenu" id="arquivoOpcaoSubMenu" class="form-control" value="" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Ícone</label>
                            <div class="col-sm-10"><input type="text" name="iconeOpcaoSubMenu" id="iconeOpcaoSubMenu" class="form-control" value="" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Prioridade</label>
                            <div class="col-sm-10"><input type="text" name="prioridadeOpcaoSubMenu" id="prioridadeOpcaoSubMenu" class="form-control" value="" style="max-width: 320px" required="required"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea rows="7" name="descricaoOpcaoSubMenu" id="descricaoOpcaoSubMenu" class="form-control" style="max-width: 500px" required="required"></textarea></div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input class="btn btn-primary" type="submit" id="salvar" name="salvar" value="Salvar">
                                <input class="btn btn-white" type="reset" id="cancelar" name="cancelar" value="Cancelar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Conteúdo DE INCLUDE FIM -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>