<?php
@include_once("lib/functions.php");
@include_once("../lib/functions.php");
@usuarioOnline();
include_once("controller/usuarioController.php");
$um     = new Usuario();
require_once ("controller/organismoController.php");
$oc     = new organismoController();

//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );

$hoje = date("Y-m-d")."T00:00:00";

include_once('lib/functions.php');
include_once('lib/phpmailer/class.phpmailer.php');
include_once('model/organismoClass.php');
include_once('model/usuarioClass.php');
include_once('model/funcaoUsuarioClass.php');

$funcaoUsuario = new FuncaoUsuario();

$organismo = new organismo();

$resultadoOrg = $organismo->listaOrganismo();

$idOrganismoAfiliado 	= isset($_REQUEST['idOrganismoAfiliado'])?$_REQUEST['idOrganismoAfiliado']:null;
$siglaOrganismo		= isset($_REQUEST['sigla'])?$_REQUEST['sigla']:null;
$idSiglaPais		= isset($_REQUEST['siglaPais'])?$_REQUEST['siglaPais']:null;



$total=0;//Contador de relatórios NÃO entregues

$usuario = new Usuario();

$resultadoUsu = $usuario->listaUsuario($siglaOrganismo);

$atualizaFuncao=0;

if(isset($siglaOrganismo)&&isset($idOrganismoAfiliado))
{
    if($idSiglaPais==1)
    {
        $siglaPais = "BR";
    }else{
        if($idSiglaPais==2)
        {
            $siglaPais = "PT";
        }else{
            $siglaPais = "RA";
        }
    }
    if($resultadoUsu)
    {
        foreach($resultadoUsu as $vetorUsu)
        {
                //Apagar todas as funções do usuário na tabela FUNCAO_USUARIO
                $funcaoUsuario->setFk_seq_cadast($vetorUsu['seqCadast']);
                $resultado = $funcaoUsuario->removePorUsuario();

                //Recuperar dados da Função
                $ocultar_json=1;
                $naoAtuantes='N';
                $atuantes='S';
                $seqCadast=$vetorUsu['seqCadast'];
                include 'js/ajax/retornaFuncaoMembro.php';
                $obj = json_decode(json_encode($return),true);
                //echo "<pre>";print_r($obj);
                if(is_array($obj))
                {

                    $arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];

                    if(count($arrFuncoes)>0)
                    {
                        foreach($arrFuncoes as $vetor)
                        {
                                if($vetor['fields']['fDatSaida']==0)//Verificar apenas campos que a data de saida não foi cadastrada
                                {
                                        $dataTerminoMandato = $vetor['fields']['fDatTerminMandat'];
                                        $codFuncao = $vetor['fields']['fCodFuncao'];
                                        $tipoCargo = $vetor['fields']['fCodTipoFuncao'];
                                        $dataEntrada = $vetor['fields']['fDatEntrad'];
                                        $desFuncao = substr($vetor['fields']['fDesFuncao'],0,3);
                                        $codFuncaoEx = $vetor['fields']['fCodFuncao'];
                                        $codFuncaoEx++;
                                        //echo "ATUANTE=>".$atuantes." / NAO ATUANTE=>".$naoAtuantes."<br>";
                                        //echo "desFuncao:".$desFuncao."<br>";
                                        if($desFuncao!="EX-")
                                        {
                                                //Se o Mandato termina hoje entao atualizar no ORCZ
                                                if($dataTerminoMandato==$hoje)
                                                {
                                                        $siglaOrganismoAfiliado			= $siglaOrganismo;
                                                        $codAfiliacaoMembroOficial		= $vetorUsu['codigoDeAfiliacao'];
                                                        $tipoMembro				= 1;
                                                        $tipoCargo				= $tipoCargo;
                                                        $funcao					= $codFuncaoEx;
                                                        $dataEntrada				= $dataEntrada;
                                                        $dataSaida				= $hoje;
                                                        $companheiro				= '';

                                                        // Instancia a classe
                                                        $ws = new Ws();

                                                        // Nome do Método que deseja chamar
                                                        $method = 'AtualizarOficial';

                                                        // Parametros que serão enviados à chamada
                                                        $params = array('CodUsuario' => 'lucianob',
                                                                                        'CodMembro' => $codAfiliacaoMembroOficial,
                                                                                        'IdeTipoMembro' => $tipoMembro,
                                                                                        'IdeCompanheiro' => $companheiro,
                                                                                        'SigOrgafi' => $siglaOrganismoAfiliado,
                                                                                        'SeqTipoFuncaoOficial' => $tipoCargo,
                                                                                        'SeqFuncao' => $funcao,
                                                                                        'DatHoraEntrada' => $dataEntrada,
                                                                                        'DatSaida' => $dataSaida,
                                                                                        'DatTerminMandato' => $dataTerminoMandato,
                                                                                        'IdeTipoMotivoSaida' => ''
                                                                                );

                                                        // Chamada do método
                                                        $return = $ws->callMethod($method, $params, 'lucianob');
                                                        //echo "<br>O usuário ".$vetorUsu['nomeUsuario']." foi atualizado no ORCZ";

                                                        // Pegar id da Função atual e remover na tabela funcao_usuario
                                                        include_once('model/funcaoClass.php');
                                                        $f = new funcao();
                                                        include_once('model/funcaoUsuarioClass.php');
                                                        //echo "<pre>";print_r($descFuncao);

                                                        $f->setVinculoExterno($funcao);
                                                        $retorno = $f->buscaVinculoExterno();
                                                        foreach ($retorno as $vetor2)
                                                        {
                                                                $fu = new funcaoUsuario();
                                                                $fu->setFkSeqCadast($seqCadast);
                                                                $fu->setFkIdFuncao($vetor2['idFuncao']);
                                                                if($fu->verificaSeJaExiste())
                                                                {
                                                                        $fu->remove();
                                                                }
                                                        }


                                                }else{
                                                        // Pegar id da Função atual e remover na tabela funcao_usuario
                                                        include_once('model/funcaoClass.php');
                                                        $f = new funcao();
                                                        include_once('model/funcaoUsuarioClass.php');
                                                        //echo "<pre>";print_r($descFuncao);

                                                        $f->setVinculoExterno($codFuncao);
                                                        $retorno = $f->buscaVinculoExterno();
                                                        foreach ($retorno as $vetor2)
                                                        {
                                                                $fu = new funcaoUsuario();
                                                                $fu->setFk_seq_cadast($seqCadast);
                                                                $fu->setFk_idFuncao($vetor2['idFuncao']);
                                                                $fu->setDataInicioMandato($dataEntrada);
                                                                $fu->setDataFimMandato($dataTerminoMandato);
                                                                $fu->setSiglaOA($siglaOrganismo);
                                                                if(!$fu->verificaSeJaExiste())
                                                                {
                                                                        $fu->cadastra();
                                                                        //echo "<br>Funcao ".$vetor2['nomeFuncao']." foi adicionada para o usuário do SOA";
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                    }
                }
        }

    }
    $atualizaFuncao =1;
}
?>
<!-- Conteúdo DE INCLUDE INÍCIO -->
<?php 
if($atualizaFuncao==1){?>
<script>
window.onload = function(){
	swal({
        title: "Sucesso!",
        text: "Funções Atualizadas com sucesso para o Organismo <?php echo $siglaOrganismo;?>!",
        type: "success",
        confirmButtonColor: "#1ab394"
    });
}
</script>
<?php }?>
<!-- Caminho de Migalhas Início -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Atualizar Função por Organismo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="painelDeControle.php">Home</a>
            </li>
            <li>
                <a href="painelDeControle.php">Administração</a>
            </li>
            <li class="active">
                <strong><a>Atualizar Função por Organismo</a></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Caminho de Migalhas Fim -->

<!-- Tabela Início -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Atualizar Função por Organismo</h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Organismo</th>
                                <th><center>Atualizar</center></th>
                            </tr>    
                        </thead>
                        <tbody>
                            <?php
                            include_once("model/organismoClass.php");
                            include_once("model/relatorioFinanceiroMensalClass.php");
							include_once("lib/functions.php");
							
                            $o = new organismo();
                            
                            $resultado = $o->listaOrganismo();
                            

                            if ($resultado) {
                                foreach ($resultado as $vetor) {
                                	
                            		$rfm = new RelatorioFinanceiroMensal();
                            		$resultado2 = $rfm->listaRelatorioFinanceiroMensal(date('m'),date('Y'),$vetor['idOrganismoAfiliado']);
                            		$total=0;
                            		if($resultado2)
                            		{
                            			foreach($resultado2 as $vetor2)
                            			{
                            				$dataCadastro = $vetor2['dataCadastro'];
                            				$nomeUsuario = $vetor2['nomeUsuario'];
                            			} 
                            			$total = count($resultado2);   	
                            		}
                                    switch ($vetor['classificacaoOrganismoAfiliado']) {
                                        case 1:
                                            $classificacao = "Loja";
                                            break;
                                        case 2:
                                            $classificacao = "Pronaos";
                                            break;
                                        case 3:
                                            $classificacao = "Capítulo";
                                            break;
                                        case 4:
                                            $classificacao = "Heptada";
                                            break;
                                        case 5:
                                            $classificacao = "Atrium";
                                            break;
                                    }
                                    switch ($vetor['tipoOrganismoAfiliado']) {
                                        case 1:
                                            $tipo = "R+C";
                                            break;
                                        case 2:
                                            $tipo = "TOM";
                                            break;
                                    }
                                    ?>
                                    
                                    <tr>
                                        <td>
                                            <?php echo $vetor["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor['nomeOrganismoAfiliado']; ?>
                                        </td>
		                                <td>
		                                	<center>
			                                	<?php if(($sessao->getValue("fk_idDepartamento")==3) || ($sessao->getValue("fk_idDepartamento")==2)){ ?>
			                                            <a class="btn btn-sm btn-info" href="?corpo=buscaAtualizarFuncao&sigla=<?php echo $vetor["siglaOrganismoAfiliado"];?>&idOrganismoAfiliado=<?php echo $vetor["idOrganismoAfiliado"];?>&siglaPais=<?php echo $vetor["paisOrganismoAfiliado"];?>" onclick="abreModalNotificacao(<?php echo $vetor['idOrganismoAfiliado'];?>,null)">
			                                                <i class="fa fa-refresh icon-white"></i>
			                                                Atualizar
			                                            </a>
			                                    <?php } ?>
                                    		</center>
		                                </td>
                               		 </tr>
                            <?php
                                
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot>
                            <!--
                            <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th>Engine version</th>
                                    <th>CSS grade</th>
                            </tr>
                            -->
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Tabela Fim -->

<!-- Window MODAL Início -->

<?php
require_once 'controller/logController.php';
$registro = new LogSistemaController();
$registro->Registro()
?>
<!-- Window MODAL Fim -->

<!-- Conteúdo DE INCLUDE FIM -->
