<?php

include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

$db = include('../firebase.php');

$documentsFirebase = $db->collection("balances")->documents();
$arrOasFirebase = [];

foreach($documentsFirebase as $documentProjecao)
    if($documentProjecao->exists())
        $arrOasFirebase[] = $documentProjecao->id();

if(count($arrOasFirebase) < 1)
    die("Não existe nenhum organismo com projeção no firebase.\n");

$o = new organismo;
$array = [];
foreach($o->getAllId(['status' => 0]) as $item) {
    if(!in_array($item['Id'], $arrOasFirebase))
        array_push($array, $item['Id']);
}

//$array = [136];

if(count($array) < 1)
    die("Não existem OAS para enviar entradas para o Firebase\n");

foreach ($array as $idOrganismoAfiliado) {
    echo "Processamento Financeiro ID: " . $idOrganismoAfiliado . "\n";
    $inicio     = time();

    $saldoInicial = popularSaldoInicial($idOrganismoAfiliado);
    if ($saldoInicial==null)
        continue;

    popularEntradasMensais($idOrganismoAfiliado);
    popularProjecao($idOrganismoAfiliado, $saldoInicial['valueOpeningBalance']);

    $fim        = time();
    $tempo      = $fim - $inicio;
    echo "[" . $idOrganismoAfiliado . "] Tempo de processamento: " . $tempo . "\n\n";
}

function popularSaldoInicial($idOrganismoAfiliado) {

    global $db;

    $si                                     = new saldoInicial();
    $mesSaldoInicialString                  = str_pad(strval($si->getMesSaldoInicial($idOrganismoAfiliado)), 2, "0", STR_PAD_LEFT);
    $anoSaldoInicialString                  = strval($si->getAnoSaldoInicial($idOrganismoAfiliado));
    $valorSaldoInicialString                = strval(number_format($si->getValorSaldoInicial($idOrganismoAfiliado),2,'.',''));
    $dataSaldoInicial                       = strval($si->getDataCompletaSaldoInicial());
    $dataSaldoInicialString                 = strval(substr($dataSaldoInicial,8,2)."/".substr($dataSaldoInicial,5,2)."/".substr($dataSaldoInicial,0,4)." 00:00:00");
    $usuarioSaldoInicialString              = strval($si->getUsuarioSaldoInicial());
    $ultimoAtualizarSaldoInicialString      = strval($si->getUltimoAtualizarSaldoInicial());

    $dataArray = [
        'lastUpdatedOpeningBalance' => $ultimoAtualizarSaldoInicialString,
        'userOpeningBalance' => $usuarioSaldoInicialString,
        'updateData' => date("d/m/Y H:i:s"),
        'monthOpeningBalance' => $mesSaldoInicialString,
        'yearOpeningBalance' => $anoSaldoInicialString,
        'valueOpeningBalance' => $valorSaldoInicialString,
        'dateOpeningBalance' => $dataSaldoInicialString,
        'balance' => 1,
        'status' => true
    ];

    echo "Saldo Inicial: " . $valorSaldoInicialString . "\n";

    if ($valorSaldoInicialString == "0.00")
        return null;

    $docRef2 = $db->collection("balances")->document($idOrganismoAfiliado);
    $docRef2->set($dataArray);

    return $dataArray;
}

function popularEntradasMensais($idOrganismoAfiliado) {

    global $db;

    echo "[" . $idOrganismoAfiliado . "] popularEntradasMensais [Início]: " . date("d/m/Y H:i:s") . "\n";

    $totalRecebimentos  = 0;
    $totalDespesas      = 0;

    $r = new Recebimento();
    $arrRecebimentos = $r->listaRecebimento(null, null, $idOrganismoAfiliado);
    if (count($arrRecebimentos)) {
        foreach ($arrRecebimentos as $v) {
            if ($v['codigoAfiliacao'] == "")
                $v['codigoAfiliacao'] = 0;
            $mes                    = substr($v['dataRecebimento'], 5, 2);
            $ano                    = substr($v['dataRecebimento'], 0, 4);
            $v['dataRecebimento']   = substr($v['dataRecebimento'], 8, 2) . "/" . substr($v['dataRecebimento'], 5, 2) . "/" . substr($v['dataRecebimento'], 0, 4);
            $v['dataCadastro']      = substr($v['dataCadastro'], 8, 2) . "/" . substr($v['dataCadastro'], 5, 2) . "/" . substr($v['dataCadastro'], 0, 4) . " " . substr($v['dataCadastro'], 11, 8);

            $docRef5 = $db->collection("balances/" . $idOrganismoAfiliado . "/recebimentos")->newDocument();
            $docRef5->set([
                'mes'                   => $mes,
                'ano'                   => $ano,
                'idRecebimentoSOA'      => $v['idRecebimento'],
                'fk_idOrganismoAfiliado'=> $idOrganismoAfiliado,
                'dataRecebimento'       => $v['dataRecebimento'] . " 00:00:00",
                'descricaoRecebimento'  => $v['descricaoRecebimento'],
                'recebemosDe'           => $v['recebemosDe'],
                'codigoAfiliacao'       => $v['codigoAfiliacao'],
                'valorRecebimento'      => $v['valorRecebimento'],
                'categoriaRecebimento'  => $v['categoriaRecebimento'],
                'userRecebimentos'      => $v['usuario'],
                'ultimoAtualizar'       => $v['ultimoAtualizar'],
                'dataCadastro'          => $v['dataCadastro'],
                'balance'               => 1,
                'updateData'            => date('d/m/Y H:i:s')
            ]);
            echo "[" . $idOrganismoAfiliado . "] Recebimento [" .$totalRecebimentos . "]: " . $v['dataRecebimento'] . " | " . $v['valorRecebimento'] . "\n";
            $totalRecebimentos++;
        }
    }

    echo "[" . $idOrganismoAfiliado . "] popularEntradasMensais [Meio - recebimentos/despesas]: " . date("d/m/Y H:i:s") . "\n";

    $d = new Despesa();
    $arrDespesas = $d->listaDespesa(null, null, $idOrganismoAfiliado);
    if (count($arrDespesas)) {
        foreach ($arrDespesas as $v) {
            $mes                    = substr($v['dataDespesa'], 5, 2);
            $ano                    = substr($v['dataDespesa'], 0, 4);
            $v['dataDespesa']       = substr($v['dataDespesa'], 8, 2) . "/" . substr($v['dataDespesa'], 5, 2) . "/" . substr($v['dataDespesa'], 0, 4);
            $v['dataCadastro']      = substr($v['dataCadastro'], 8, 2) . "/" . substr($v['dataCadastro'], 5, 2) . "/" . substr($v['dataCadastro'], 0, 4) . " " . substr($v['dataCadastro'], 11, 8);

            $docRef6 = $db->collection("balances/" . $idOrganismoAfiliado . "/despesas")->newDocument();
            $docRef6->set([
                'mes'                   => $mes,
                'ano'                   => $ano,
                'idDespesaSOA'          => $v['idDespesa'],
                'fk_idOrganismoAfiliado'=> $idOrganismoAfiliado,
                'dataDespesa'           => $v['dataDespesa'] . " 00:00:00",
                'descricaoDespesa'      => $v['descricaoDespesa'],
                'pagoA'                 => $v['pagoA'],
                'valorDespesa'          => $v['valorDespesa'],
                'categoriaDespesa'      => $v['categoriaDespesa'],
                'userDespesas'          => $v['usuario'],
                'ultimoAtualizar'       => $v['ultimoAtualizar'],
                'dataCadastro'          => $v['dataCadastro'],
                'balance'               => 1,
                'updateData'            => date('d/m/Y H:i:s')

            ]);
            echo "[" . $idOrganismoAfiliado . "] Despesa [" .$totalDespesas . "]: " . $v['dataDespesa'] . " | " . $v['valorDespesa'] . "\n";
            $totalDespesas++;
        }
    }

    echo "===================================================\n";
    echo "Firebase:\n";
    echo "Total de documentos de recebimentos no Firebase: " . $totalRecebimentos . "\n";
    echo "Total de documentos de despesas no Firebase: " . $totalDespesas . "\n";

    echo "===================================================\n";
    echo "[" . $idOrganismoAfiliado . "] popularEntradasMensais [Fim]: " . date("d/m/Y H:i:s" . "\n");
}

function popularProjecao($idOrganismoAfiliado, $saldoInicial) {

    echo "[" . $idOrganismoAfiliado . "] popularProjecao [Início]: " . date("d/m/Y H:i:s") . "\n";

    global $db;

    $recebimentos = $db->collection("balances/" . $idOrganismoAfiliado . "/recebimentos");
    $queryRecebimentos = $recebimentos->documents();

    $despesas = $db->collection("balances/" . $idOrganismoAfiliado . "/despesas");
    $queryDespesas = $despesas->documents();

    $saldoMesAnterior = $saldoInicial;
    $monthBalance = 0;

    echo "[" . $idOrganismoAfiliado . "] popularProjecao [Meio]: " . date("d/m/Y H:i:s") . "\n";

    for($ano = 2016; $ano <= 2020; $ano++) {
        for($mes = 1; $mes <= 12; $mes++) {
            $mesString = strval(str_pad($mes,2,'0',STR_PAD_LEFT));
            $anoString = strval($ano);

            $arr = retornaSaldoRetornoArrayFirebase($queryRecebimentos, $queryDespesas, $mesString, $anoString, $saldoMesAnterior, true);

            $totalEntries = $arr['totalEntries'];
            $totalOutputs = $arr['totalOutputs'];

            $monthBalance = ($saldoMesAnterior + $totalEntries) - $totalOutputs;

            $docRef = $db->collection("balances/" . $idOrganismoAfiliado . "/balance")->newDocument();
            $docRef->set([
                'fk_idOrganismoAfiliado'    => (string) $idOrganismoAfiliado,
                'month'                     => $mesString,
                'year'                      => $anoString,
                'previousBalance'           => number_format($saldoMesAnterior, 2, '.', ''),
                'totalEntries'              => number_format($totalEntries, 2, '.', ''),
                'totalOutputs'              => number_format($totalOutputs, 2, '.', ''),
                'monthBalance'              => number_format($monthBalance, 2, '.', ''),
                'updateData'                => date('d/m/Y H:i:s'),
                'userBalance'               => strval('540135')
            ]);
            echo "Projeção [" . $anoString . "/" . $mesString . "]: " . $monthBalance . "\n";
            $saldoMesAnterior = $monthBalance;
        }
    }
    echo "[" . $idOrganismoAfiliado . "] popularProjecao [Fim]: " . date("d/m/Y H:i:s") . " | Saldo no Firebase: " . $monthBalance . "\n";
}