<?php

echo "<b>Cron em produção: Preenchimento nos Arquivos do Saldo</b><br><br>";

include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

//$idOrganismoAfiliado = $idNaoCadastrado;
//$idOrganismoAfiliado = 1;//Teste com Loja CURITIBA
//$idOrganismoAfiliado = 81;//Teste com Loja São Paulo
//$idOrganismoAfiliado = 157;//Teste com Loja Rio de Janeiro
//$idOrganismoAfiliado = 136;//Teste com Loja BH
//$idOrganismoAfiliado = 70;//Teste com Blumenal

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']:null;

//Verificar se acabou de preencher todas as projeções

if($idOrganismoAfiliado != null) {
    $o = new organismo();
    $arr = $o->retornaSiglaOa($idOrganismoAfiliado);
    $idOrganismoAfiliado = $arr['id'];
    echo "<br>Id do Oa Atualizado:" . $arr['id'];
    echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
    echo "<br>Sigla:" . $arr['sigla'];
}else{

    echo "<b>Aviso: procurando popular todas as projeções de todos os oas cadastrados</b><br><br>";

    //Contar o numero de arquivos txt na pasta se bate com o numero de organismos cadastrados
    $o = new organismo();
    $arrOrganismos = $o->listaOrganismo();

    $pasta = '../saldo/';
    $arquivos = glob("$pasta{*.txt}", GLOB_BRACE);

    echo "<br>Total de Arquivos na Pasta Saldo: <b>" . count($arquivos)."</b><br>";
    echo "<br>Total de Organismos cadastrados: <b>" . count($arrOrganismos)."</b><br><br>";

    if(count($arquivos)==count($arrOrganismos))
    {
        echo "<br><br><b>=======================A cron terminou o preenchimento de todas as projeções=======================</b>";
        exit();
    }else{
        //echo "<pre>";print_r($arrOrganismos);exit();
        if($arrOrganismos)
        {
            for ($i=0;$i<count($arrOrganismos); $i++)
            {
                //Verificar próximo organismo que não possui projeção, se não possui sair do foreach
                $arquivo = "../saldo/".$arrOrganismos[$i]['idOrganismoAfiliado'].".txt";
                if(!file_exists ($arquivo)) {
                    $idOrganismoAfiliado = $arrOrganismos[$i]['idOrganismoAfiliado'];
                    $i=count($arrOrganismos);//Forçar sair do for
                }
            }
        }
        $arr = $o->retornaSiglaOa($idOrganismoAfiliado);
        $idOrganismoAfiliado = $arr['id'];
        echo "<br>Proximo id do Oa Atualizado no Loop:" . $arr['id'];
        echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
        echo "<br>Sigla:" . $arr['sigla'];

    }
}

//De posse do id do organismo seguir fazendo a projeção

/*
 * Preparar as variáveis para a projeção
 */

$anoAtual=date('Y')+1;
$mesAtual=date('m');

$r = new Recebimento();
$d = new Despesa();

/*
 * Calcular saldo do mês anterior
 */

//Encontrar Saldo e Data Inicial
$si = new saldoInicial();

$temSaldoInicial = $si->temSaldoInicial($idOrganismoAfiliado);

if($temSaldoInicial) {

    $saldoInicial = 0;
    $resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
    if ($resultado) {
        foreach ($resultado as $vetor) {
            $mesSaldoInicial = substr($vetor['dataSaldoInicial'], 5, 2);
            $anoSaldoInicial = substr($vetor['dataSaldoInicial'], 0, 4);
            $saldoInicial = (float)str_replace(",", ".", str_replace(".", "", $vetor['saldoInicial']));
        }
    }

    //Encontrar Saldo dos Meses Anteriores
    $mes = intval($mesSaldoInicial);
    $ano = intval($anoSaldoInicial);
    $saldoMesesAnteriores = 0;
    $saldoGeral = 0;

    $texto = "";

    $y = 0;

    $i = 0;

    /*
     * Montar projeção
     */

    while ($ano <= $anoAtual) {
        //echo "<br>data=>".$mes."-".$ano;
        if ($mes <= 12) {
            if ($mes == $mesSaldoInicial && $ano == $anoSaldoInicial) {
                $saldoMesesAnteriores = $saldoInicial;
            }

            $arr = retornaSaldoRetornoArray($r, $d, $mes, $ano, $idOrganismoAfiliado, $saldoMesesAnteriores);

            echo "<pre>";
            print_r($arr);

            if ($mes == 12 && $ano == $anoAtual) {
                $texto .= $mes . "@" . $ano . "@" . $saldoMesesAnteriores . "@" . $arr['totalEntradasDoMes'] . "@" . $arr['totalSaidasDoMes'] . "@" . $arr['saldoMes'];
            } else {
                $texto .= $mes . "@" . $ano . "@" . $saldoMesesAnteriores . "@" . $arr['totalEntradasDoMes'] . "@" . $arr['totalSaidasDoMes'] . "@" . $arr['saldoMes'] . "@";
            }

            $saldoMesesAnteriores = $arr['saldoMes'];
            $mes++;
        } else {
            $ano++;
            $mes = 1;
        }

        $i++;
    }


    echo "<br><br>" . $texto . "<br>";

    /*
     * Criar de fato arquivo com a projeção
     */

    $arquivo = "../saldo/" . $idOrganismoAfiliado . ".txt";

    //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
    $fp = fopen($arquivo, "w+");

    //Escreve no arquivo aberto.
    fwrite($fp, $texto);

    //Fecha o arquivo.
    fclose($fp);

    echo "<br><b>Ultimo Saldo do Mes:" . $arr['saldoMes'] . "</b><br><br>";

    //Verificação se o arquivo existe
    if(file_exists ($arquivo)) {
        echo "<br><b>Arquivo existe na pasta [saldo]!</b><br><br>";
    }
}else{

    echo "<br><b>Organismo sem saldo inicial cadastrado!</b>";

    //Criar arquivo txt na pasta saldo com 0 para toda projeção de saldo do organismo

    $texto = "";

    //Iniciar mês e ano com o inicio do sistema
    $mes = 1;
    $ano = 2016;

    /*
     * Montar projeção
     */

    while ($ano <= $anoAtual) {

        if ($mes <= 12) {

            //Popular zero para cada mês
            $saldoMesesAnteriores = 0;
            $totalEntradasDoMes = 0;
            $totalSaidasDoMes = 0;
            $saldoMes = 0;

            if ($mes == 12 && $ano == $anoAtual) {
                $texto .= $mes . "@" . $ano . "@" . $saldoMesesAnteriores . "@" . $totalEntradasDoMes . "@" . $totalSaidasDoMes . "@" . $saldoMes;
            } else {
                $texto .= $mes . "@" . $ano . "@" . $saldoMesesAnteriores . "@" . $totalEntradasDoMes . "@" . $totalSaidasDoMes . "@" . $saldoMes . "@";
            }

            $mes++;

        } else {

            $ano++;
            $mes = 1;

        }
    }


    echo "<br><br>" . $texto . "<br>";

    /*
     * Criar de fato arquivo com a projeção
     */

    $arquivo = "../saldo/" . $idOrganismoAfiliado . ".txt";

    //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
    $fp = fopen($arquivo, "w+");

    //Escreve no arquivo aberto.
    fwrite($fp, $texto);

    //Fecha o arquivo.
    fclose($fp);

    //Verificação se o arquivo existe
    if(file_exists ($arquivo)) {
        echo "<br><b>Arquivo existe na pasta [saldo] com projeção zero 0!</b><br><br>";
    }
}


?>