<?php
echo "Cron em produção<br>";
include_once('../lib/functions.php');
include_once('../model/relatorioFinanceiroMensalClass.php');
include_once('../model/usuarioClass.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');
include_once('../model/membrosRosacruzesAtivosClass.php');
include_once('../model/saldoFinanceiroClass.php');
include_once('../model/valoresDoMesClass.php');
include_once('../model/valoresDoMesDespesasClass.php');
include_once('../model/saldosAnterioresDoMesClass.php');
include_once('../model/valoresDoMesEntradasClass.php');

$arrCadastrados=array();
$sf = new saldoFinanceiro();
$respCadastrados = $sf->retornaOrganismosCadastrados();
if($respCadastrados)
{
    foreach ($respCadastrados as $v) {
        $arrCadastrados[] = $v['fk_idOrganismoAfiliado'];
    }
}

//$idOrganismoAfiliado = $idNaoCadastrado;
//$idOrganismoAfiliado = 1;//Teste com Loja CURITIBA
//$idOrganismoAfiliado = 81;//Teste com Loja São Paulo
//$idOrganismoAfiliado = 157;//Teste com Loja Rio de Janeiro
//$idOrganismoAfiliado = 136;//Teste com Loja BH
//$idOrganismoAfiliado = 70;//Teste com Blumenal

if(!isset($idOrganismoAfiliado)) {
    $o = new organismo();
    $arr = $o->retornaOaNaoCadastrado($arrCadastrados);
    $idOrganismoAfiliado = $arr['id'];
    echo "<br>Id do Oa Nao Cadastrado:" . $arr['id'];
    echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
    echo "<br>Sigla:" . $arr['sigla'];
}

$rfm 		= new RelatorioFinanceiroMensal();
$usu		= new Usuario();

$anoAtual=date('Y')+1;
$mesAtual=date('m');

$si = new saldoInicial();
$temSaldoInicial = 1;
$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if(!$resultado)
{
    $temSaldoInicial = 0;
}

$mesAnterior=date('m', strtotime('-1 months', strtotime($anoAtual."-".$mesAtual."-01")));
$mesProximo=date('m', strtotime('+1 months', strtotime($anoAtual."-".$mesAtual."-01")));

if($mesAtual==1)
{
    $anoAnterior=$anoAtual-1;
}else{
    $anoAnterior=$anoAtual;
}
if($mesAtual==12)
{
    $anoProximo=$anoAtual+1;
}else{
    $anoProximo=$anoAtual;
}

/*
 * Montar texto para ser exibido
 */

$mesAtualTexto = mesExtensoPortugues($mesAtual);
$mesProximoTexto = mesExtensoPortugues($mesProximo);
$mesAnteriorTexto = mesExtensoPortugues($mesAnterior);


$r = new Recebimento();
$d = new Despesa();
$membrosRosacruzesAtivos = new MembrosRosacruzesAtivos();

/*
 * Calcular saldo do mês anterior
 */

//Encontrar Saldo e Data Inicial
$si = new saldoInicial();
$mesSaldoInicial = "01";
$anoSaldoInicial = "2016";
$saldoInicial=0;
$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
if($resultado)
{
    foreach($resultado as $vetor)
    {
        $mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
        $anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
        $saldoInicial = (float) str_replace(",",".",str_replace(".","",$vetor['saldoInicial']));
    }
}

//Encontrar Saldo dos Meses Anteriores
$mes=intval($mesSaldoInicial);
$ano=intval($anoSaldoInicial);
$saldoMesesAnteriores=0;
$saldoGeral=0;
//echo "<br>mesSaldoInicial=>".$mes;
//echo "<br>anoSaldoInicial=>".$ano;

$y=0;
if($mesAtual==$mesSaldoInicial&&$anoAtual==$anoSaldoInicial)
{
    $saldoGeral = $saldoInicial;
    $saldoMesesAnteriores = $saldoGeral;
}else{
    $i=0;
    while($ano<=$anoAtual)
    {
        //echo "<br>data=>".$mes."-".$ano;
        if($mes<=12)
        {
            if($mes==$mesSaldoInicial&&$ano==$anoSaldoInicial)
            {
                $saldoMesesAnteriores = $saldoInicial;
            }

            $arr = retornaSaldoRetornoArray($r,$d,$mes,$ano,$idOrganismoAfiliado,$saldoMesesAnteriores);

            //inserir histórico até o mês anterior
            $sf	= new saldoFinanceiro();
            $sf->setMes($mes);
            $sf->setAno($ano);
            $dataSaldoFinanceiro = $ano."-".$mes."-01";
            $sf->setDataSaldoFinanceiro($dataSaldoFinanceiro);
            $sf->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $sf->setSaldoAnterior($arr['saldoAnterior']);
            $sf->setTotalEntradas($arr['totalEntradas']);
            $sf->setTotalSaidas($arr['totalSaidas']);
            $sf->setSaldoMes($arr['saldoMes']);
            $sf->cadastroSaldoFinanceiro();

            //inserir histórico com total para cada mes em tabela separada por causa da trigger

            $vdm	= new valoresDoMes();
            $vdm->setMes($mes);
            $vdm->setAno($ano);
            $dataValores = $ano."-".$mes."-01";
            $vdm->setDataValores($dataValores);
            $vdm->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $vdm->setTotalEntradasDoMes($arr['totalEntradasDoMes']);
            $vdm->setTotalSaidasDoMes($arr['totalSaidasDoMes']);
            $vdm->setTotalSaldoDoMes($arr['saldoMes']);
            $vdm->cadastro();

            $vdmd	= new valoresDoMesDespesas();
            $vdmd->setMes($mes);
            $vdmd->setAno($ano);
            $vdmd->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $vdmd->setValorDespesa($arr['totalSaidasDoMes']);
            $vdmd->cadastro();

            $sdm	= new saldosAnterioresDoMes();
            $sdm->setMes($mes);
            $sdm->setAno($ano);
            $sdm->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $sdm->setSaldo($arr['saldoAnterior']);
            $sdm->cadastro();

            $vdme	= new valoresDoMesEntradas();
            $vdme->setMes($mes);
            $vdme->setAno($ano);
            $vdme->setFkIdOrganismoAfiliado($idOrganismoAfiliado);
            $vdme->setValorEntrada($arr['totalEntradas']);
            $vdme->cadastro();

            $saldoMesesAnteriores = $arr['saldoMes'];
            $mes++;
        }else{
            $ano++;
            $mes=1;
        }

        $i++;
    }
}

echo "<br><b>Ultimo Saldo do Mes:".$arr['saldoMes']."</b>";

echo "<br><b>=======================Terminou a cron=======================</b>";

?>