<?php

if ($argc < 4) {
    echo "Primeiro argumento: ID do Organismo.\n";
    echo "Segundo argumento: Ano\n";
    echo "Terceiro argumento: Mês\n";
    exit(1);
}
$idOrganismoAfiliado = $argv[1];

require_once '../vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;

include_once('../lib/functions.php');

$soadesenvolvimento = new FirestoreClient([
    'projectId' => 'soadesenvolvimento'
]);

echo "Financeiro ID: " . $idOrganismoAfiliado . "\n";
$inicio     = time();

$recebimentos = $soadesenvolvimento->collection("balances/" . $idOrganismoAfiliado . "/recebimentos")->orderBy("dataRecebimento", "asc");
$queryRecebimentos = $recebimentos->documents();

$qntRecebimentos = 0;
$totalRecebimentos = 0;
foreach ($queryRecebimentos as $documentRecebimentos) {
    if ($documentRecebimentos->exists()) {
        $dataRecebimentos = $documentRecebimentos->data();
        if ($dataRecebimentos['ano'] == $argv[2]) {
            if ($dataRecebimentos['mes'] == $argv[3]) {
                $dia = explode("/", $dataRecebimentos['dataRecebimento']);
                echo "[" . $dataRecebimentos['ano'] . "]" . "[" . $dataRecebimentos['mes'] . "]" . "[" . $dia[0] . "]" . "[" . $dataRecebimentos['dataCadastro'] . "[" . $documentRecebimentos->id() . "]" . "[" . $dataRecebimentos['idRecebimentoSOA'] . "]" . " Valor: R$ " . $dataRecebimentos['valorRecebimento'] . "\n";
                $totalRecebimentos += $dataRecebimentos['valorRecebimento'];
                $qntRecebimentos++;
            }
        }
    }
}
echo "Recebimentos: ". $qntRecebimentos ."\n";
echo "Total: ". $totalRecebimentos ."\n";

$fim        = time();
$tempo      = $fim - $inicio;
echo "[" . $idOrganismoAfiliado . "] Tempo de processamento: " . (number_format($tempo/60, 2)) . "m (" . $tempo . "s) \n\n";