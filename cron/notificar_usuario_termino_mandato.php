<?php
$server = 138;
//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );

$hoje = date("Y-m-d")."T00:00:00";

include_once('../lib/functions.php');
include_once('../lib/phpmailer/class.phpmailer.php');
include_once('../model/organismoClass.php');
include_once('../model/usuarioClass.php');
include_once('../model/funcaoUsuarioClass.php');

$funcaoUsuario = new FuncaoUsuario();

$organismo = new organismo();

$resultadoOrg = $organismo->listaOrganismo();

if($resultadoOrg)
{
	foreach($resultadoOrg as $vetorOrg)
	{
		$idOrganismoAfiliado 	= $vetorOrg['idOrganismoAfiliado'];
		$siglaOrganismo			= $vetorOrg['siglaOrganismoAfiliado'];
		$total=0;//Contador de relatórios NÃO entregues
	
		//Procurar mestres cadastrados no sistema para este organismo
		$usuario = new Usuario();
		
		$resultadoUsu = $usuario->listaUsuario($siglaOrganismo);//Mestre deste OA
		
		if($resultadoUsu)
		{
			foreach($resultadoUsu as $vetorUsu)
			{
				//Apagar todas as funções do usuário na tabela FUNCAO_USUARIO
				$funcaoUsuario->setFk_seq_cadast($vetorUsu['seqCadast']);
				$resultado = $funcaoUsuario->removePorUsuario();
				
				//Recuperar
				//echo "Nome:".$vetorUsu['nomeUsuario']."<br>";
				$ocultar_json=1;
				$naoAtuantes='N';
				$seqCadast=$vetorUsu['seqCadast'];
				include '../js/ajax/retornaFuncaoMembro.php';
				$obj = json_decode(json_encode($return),true);
				//echo "<pre>".print_r($obj['result'][0]['fields']['fArrayOficiais']);
				$arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];
				
				//Verificar se o mandato expira daqui a sete dias
				date_default_timezone_set('America/Sao_Paulo');
				$data7dias  = date('Y-m-d', strtotime("+7 days"));//Data daqui a sete dias
				$data7dias = $data7dias."T00:00:00";
				
				foreach($arrFuncoes as $vetor)
				{
					if($vetor['fields']['fDatSaida']==0)//Verificar apenas campos que a data de saida não foi cadastrada
					{
						$dataTerminoMandato = $vetor['fields']['fDatTerminMandat'];
						$codFuncao = $vetor['fields']['fCodFuncao'];
						$tipoCargo = $vetor['fields']['fCodTipoFuncao'];
						$dataEntrada = $vetor['fields']['fDatEntrad'];
						$desFuncao = substr($vetor['fields']['fDesFuncao'],0,3);
						echo "desFuncao:".$desFuncao."<br>";
						//echo "desFuncao:".$desFuncao."<br>";
						if($desFuncao!="EX-")
						{
							echo "FUNÇÃO=>".$vetor['fields']['fDesFuncao']."-DATATERMINOMANDT=>".$dataTerminoMandato."-DAT7DIAS=>".$data7dias."<bR>";
							if($dataTerminoMandato==$data7dias)
							{
								echo "VAI MANDAR E-MAIL";
								//Se o Mandato termina daqui a sete dias enviar email avisando
								$dataFormatadaTerminoMandato = substr($dataTerminoMandato,8,2)."/".substr($dataTerminoMandato,5,2)."/".substr($dataTerminoMandato,0,4);
								//Enviar email 
									$texto = '<div>
									<div dir="ltr">
										<table class="ecxbody-wrap"
											style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
											<tbody>
												<tr
													style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
													<td
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
														valign="top"></td>
													<td class="ecxcontainer" width="600"
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
														valign="top">
														<div class="ecxcontent"
															style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
															<table class="ecxmain" width="100%" cellpadding="0"
																cellspacing="0"
																style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
																<tbody>
																	<tr
																		style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<td class="ecxcontent-wrap"
																			style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
																			valign="top"><img src="http://i.imgur.com/FKg7aai.png"
																			style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
																			alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
																		<br>
																		<br>
																		<br>
																			<div style="text-align: left;"></div>
																			<table width="100%" cellpadding="0" cellspacing="0"
																				style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																				<tbody>
																					<tr
																						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																						<td class="ecxcontent-block"
																							style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
																							valign="top">Estimado(a) <b>'.strtoupper($vetorUsu['nomeUsuario']).'</b>,</td>
																					</tr>
																					<tr
																						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																						<td class="ecxcontent-block"
																							style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																							valign="top">Saudações Fraternais! </td>
																					</tr>
																					<tr
																						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																						<td class="ecxcontent-block"
																							style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																							valign="top">Por meio deste informamos que seu mandato para a função de <b>'.$vetor['fields']['fDesFuncao'].'</b> terminará em <b>'.$dataFormatadaTerminoMandato.'</b>.</td>
																					</tr>
																					<tr
																						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																						<td class="ecxcontent-block"
																							style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																							valign="top">A partir da data acima mencionada seu acesso ao sistema do SOA será negado para essa função.</td>
																					</tr>
																					<tr
																						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																						<td class="ecxcontent-block"
																							style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																							valign="top"><font color="red">Essa é uma mensagem automática, não há necessidade de resposta!</font></td>
																					</tr>
																					<tr
																						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																						<td class="ecxcontent-block"
																							style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																							valign="top">Em caso de dúvida, por favor entre em contato: atendimentoportal@amorc.org.br</td>
																					</tr>
																					<tr
																						style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																						<td class="ecxcontent-block"
																							style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																							valign="top"> 
																							Ordem Rosacruz, AMORC – Grande Loja da Jurisdição de Língua Portuguesa<br>
					                														Rua Nicarágua, 2620, Curitiba – Paraná<br>
					                														Telefone: (+55) 041- 3351-3000</td>
																					</tr>
																				</tbody>
																			</table></td>
																	</tr>
																</tbody>
															</table>
															<div class="footer"
																style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
																<div style="text-align: left;"></div>
																<table width="100%"
																	style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																	<tbody>
																		<tr
																			style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																			<td class="ecxaligncenter ecxcontent-block"
																				style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
																				align="center" valign="top">AMORC-GLP '.date('Y').'</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
														<div style="text-align: left;"></div></td>
													<td
														style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
														valign="top"></td>
												</tr>
											</tbody>
										</table>
										</div>
									</div>';
													
									$servidor="smtp.office365.com";
									$porta="587";
									$usuario="noresponse@amorc.org.br";
									$senha="Vuju3216";
								
									//echo $senha;
									#Disparar email
									$mail = new PHPMailer();
									$mail->isSMTP();
									$mail->SMTPSecure = "tls";
									$mail->SMTPAuth = true;
									$mail->Host = $servidor;	// SMTP utilizado
									$mail->Port = $porta;  	
									$mail->Username = $usuario;
									$mail->Password = $senha;
									$mail->From = 'noresponse@amorc.org.br';
									$mail->AddReplyTo("atendimento@amorc.org.br","Antiga e Mística Ordem Rosacruz - AMORC");
									$mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
									//$mail->AddAddress("luis.fernando@amorc.org.br");
									//$mail->AddAddress("tatiane.jesus@amorc.org.br");
									//$mail->AddAddress("lucianob@amorc.org.br");
									//$mail->AddAddress("samufcaldas@hotmail.com.br");
									//$mail->AddAddress("samuel.caldas@hotmail.com.br");
									//$mail->AddAddress("luciano.bastos@gmail.com");
									//$mail->AddAddress("braz@amorc.org.br");
									//$mail->AddAddress("amigoeterno10@hotmail.com");
									//$mail->AddAddress($vetorUsu['emailUsuario']);
									$mail->isHTML(true);
									$mail->CharSet = 'utf-8';
									//$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
									$mail->Subject = 'Término do Mandato - AMORC';
									$mail->Body = $texto;
									$mail->AltBody = strip_tags($texto); 
									//echo $texto;
									
									$enviado = $mail->Send();
									if($enviado)
									{
										//$r = $ner->atualizaNotificacaoEnvioRemessa($vetor['seq_cadast'],$vetor['lote'],$vetor['tipo_membro']);
										echo "Email enviado com sucesso para <b>".$vetorUsu['emailUsuario']."</b>";
									}	
							}
						}
					}
				}		
			}
		}
	}
}

?>	