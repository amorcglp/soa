<?php

ini_set('max_execution_time', 0);
ini_set("display_errors", 1 );

include_once('../lib/functions.php');
include_once('../model/higienizacaoIniciacoesLogClass.php');
include_once('../model/higienizacaoIniciacoesNotLogClass.php');
require_once '../webservice/wsInovad.php';

$higienizacaoIniciacoesNotLog = new higienizacaoIniciacoesNotLog();

$resultado = $higienizacaoIniciacoesNotLog->listaHigienizacaoIniciacoesNotLog(0,2);

if($resultado) {

    foreach ($resultado as $vetor) {

        $id             = $vetor['idHigienizacaoIniciacoesNotLog'];
        $seqCadast      = $vetor['seqCadast'];
        echo "<br>Id:".$id."-seqCadast:".$seqCadast;

        $tipoObrigacao  = $vetor['seqTipoObrigacao'];
        $descricaoLocal = $vetor['localObrigacao'];
        $dataObrigacao  = $vetor['dataObrigacao'];
        $regiao         = $vetor['regiao'];
        $siglaOA        = $vetor['siglaOA'];

        $grauSOA = retornaGrauRC($tipoObrigacao);

        $arrGrausOrcz = array();
        $arrGrausOrcz[]="0";

        $teste=null;

        //Buscar Nome do Usuário vindo do ORCZ
        $vars = array('seq_cadast' => $seqCadast);
        $resposta = json_decode(json_encode(restAmorc("membros/[".$seqCadast."]/ritualistico/1",$vars)),true);
        $obj2 = json_decode(json_encode($resposta),true);
        echo "<pre>";print_r($obj2);

        if (isset($obj2['data'][$seqCadast][0])) {
            foreach($obj2['data'][$seqCadast] as $vetor2)
            {
                $grauOrcz = retornaGrauRC($vetor2['seq_tipo_obriga']);
                //echo "==>GrauOrcz:".$grauOrcz;

                if(!in_array($grauOrcz,$arrGrausOrcz)) {

                    $arrGrausOrcz[] = $grauOrcz;

                }
            }

        }

        echo "<br>Iniciações do membro no ORCZ:";
        echo "<pre>";print_r($arrGrausOrcz);
        echo "<br>grauSOA=->".$grauSOA;

        if($grauSOA!=0) {
            echo "<br>entrou aqui";
            if (!in_array($grauSOA, $arrGrausOrcz)) {


                echo "--><b>Vai inserir o grau:</b> " . $grauSOA;
                //Insere grau para o membro

                $credentials = montaCredenciais("producao");

                $varsObrigacao = Array('form_params' =>
                    Array('seq_tipo_obriga' => $tipoObrigacao,
                        'sig_orgafi' => $siglaOA,
                        'des_local_obriga' => "templo do organismo",
                        'dat_obriga' => $dataObrigacao,
                        'cod_usuari' => 'lucianob'),
                    'headers' => Array('Authorization' => 'Basic ' . $credentials),
                    'verify' => false);

                //print_r($vars);exit();

                $resposta = json_decode(json_encode(restAmorc("membros/[" . $seqCadast . "]/ritualistico/1", $varsObrigacao, 'POST')), true);
                $obj3 = json_decode(json_encode($resposta), true);
                echo "<pre>";
                print_r($obj3);
                //exit();

                if ($obj3['success']) {

                    //É preciso gerar log do que foi importado para o Orcz
                    //Pois depois eles vão querer saber se o registro foi manual ou da importação
                    $higienizacaoIniciacoesLog = new higienizacaoIniciacoesLog();
                    $higienizacaoIniciacoesLog->setSeqCadast($seqCadast);
                    $higienizacaoIniciacoesLog->setSeqTipoObrigacao($tipoObrigacao);
                    $higienizacaoIniciacoesLog->setLocalObrigacao($descricaoLocal);
                    $higienizacaoIniciacoesLog->setDataObrigacao($dataObrigacao);
                    $higienizacaoIniciacoesLog->setRegiao($regiao);
                    $higienizacaoIniciacoesLog->setSiglaOA($siglaOA);
                    $higienizacaoIniciacoesLog->cadastraHigienizacaoIniciacoesLog();

                    //Apagar registro de not log
                    if ($higienizacaoIniciacoesNotLog->apagaRegistro($id)) {
                        echo "<br><b>[" . $grauSOA . "]º Grau importado</b>";
                    }
                }
            }else{
                //Apagar registro de not log
                if ($higienizacaoIniciacoesNotLog->apagaRegistro($id)) {
                    echo "<br><b>[" . $grauSOA . "]º Grau apagado porque já existia nas iniciações do membro!</b>";
                }
            }
        }

    }
}

echo "<br><br><b>Higienização Finalizada com sucesso!</b>";


?>	
