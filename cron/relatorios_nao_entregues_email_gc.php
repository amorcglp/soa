<?php 
session_start();

require '../vendor/autoload.php';
use Mailgun\Mailgun;
# Instantiate the client.
$mgClient = new Mailgun('key-24e5c1eb843300db6922fee30623bcf0');
/** Official */
$domain = "mg.amorc.org.br";
/** Sandbox */
//$domain = "sandbox622f068a94064201a9d4021ad9bd9042.mailgun.org";

$arrData = array();
$arrData['key'] = 777;
$logged=true;
$naoSetarCookie=true;
require_once ('../sec/make.php');
$_SESSION['tk']= $token;
$_SESSION['vk']= $arrData['key'];
set_time_limit(0);
//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );
$server = 135;
$mesAtual 				= date('m');
$anoAtual 				= date('Y');


//Trimestre atual
if(date('m')==1||date('m')==2||date('m')==3)
{
    $trimestreAtual=1;
}
if(date('m')==4||date('m')==5||date('m')==6)
{
    $trimestreAtual=2;
}
if(date('m')==7||date('m')==8||date('m')==9)
{
    $trimestreAtual=3;
}
if(date('m')==10||date('m')==11||date('m')==12)
{
    $trimestreAtual=4;
}

include_once('../lib/functions.php');
include_once('../lib/phpmailer/class.phpmailer.php');
include_once('../model/regiaoRosacruzClass.php');
include_once('../model/organismoClass.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/ataReuniaoMensalAssinadaClass.php');
include_once('../model/ataPosseAssinadaClass.php');
include_once('../model/membrosRosacruzesAtivosClass.php');
include_once('../model/relatorioFinanceiroMensalClass.php');
include_once('../model/relatorioFinanceiroAnualClass.php');
include_once('../model/imovelControleClass.php');
include_once('../model/usuarioClass.php');
include_once('../model/notificacaoRelatoriosNaoEntreguesEmailGcClass.php');
include_once('../model/agendaAtividadeClass.php');

$regiao = new regiaoRosacruz();

$resultadoRegiao = $regiao->listaRegiaoRosacruz(1);
//echo count($resultadoRegiao);exit();
if($resultadoRegiao)
{
	foreach($resultadoRegiao as $vetorRegiao)
	{
		//Procurar mestres cadastrados no sistema para este organismo
				$usuario = new Usuario();
				
				$resultadoUsu = $usuario->listaUsuario(null,null,"151",null,null,null,null,null,$vetorRegiao['idRegiaoRosacruz'],null,"0","1",null,1);//Grande Conselheiro desta Região
				//echo "<pre>";print_r($resultadoUsu);
				if($resultadoUsu)
				{
					foreach($resultadoUsu as $vetorUsu)
					{
                                            $n = new notificacaoRelatoriosNaoEntreguesEmailGc();
                                            if(!$n->verificaSeJaExiste($vetorUsu['idUsuario'], $vetorRegiao['regiaoRosacruz']))
                                            { 
                                                //echo "entrou aqui";exit();
                                                $oasSemSaldoInicial=0;
						//Enviar email 
							$texto = '<div>
							<div dir="ltr">
								<table class="ecxbody-wrap"
									style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
									<tbody>
										<tr
											style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
											<td
												style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
												valign="top"></td>
											<td class="ecxcontainer" width="600"
												style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
												valign="top">
												<div class="ecxcontent"
													style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
													<table class="ecxmain" width="100%" cellpadding="0"
														cellspacing="0"
														style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
														<tbody>
															<tr
																style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																<td class="ecxcontent-wrap"
																	style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
																	valign="top"><img src="http://i.imgur.com/FKg7aai.png"
																	style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
																	alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
																<br>
																<br>
																<br>
																	<div style="text-align: left;"></div>
																	<table width="100%" cellpadding="0" cellspacing="0"
																		style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<tbody>
																			<tr
																				style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																				<td class="ecxcontent-block"
																					style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
																					valign="top">Estimado(a) <b>'.strtoupper($vetorUsu['nomeUsuario']).'</b>,</td>
																			</tr>
																			<tr
																				style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																				<td class="ecxcontent-block"
																					style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																					valign="top">Saudações Fraternais! </td>
																			</tr>
																			<tr
																				style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																				<td class="ecxcontent-block"
																					style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																					valign="top">Por meio deste informamos que não foram entregues os seguintes relatórios assinados no sistema SOA da região '.$vetorRegiao['regiaoRosacruz'].':</td>
																			</tr>
																			<tr
																				style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																				<td class="ecxcontent-block"
																					style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																					valign="top">';
																				
																				$organismo = new organismo();
																		
																				$resultadoOrg = $organismo->listaOrganismo(null,null,null,null,$vetorRegiao['idRegiaoRosacruz'],null,null,1);
																				$naoCobrarDashboard =0;
																				if($resultadoOrg)
																				{
																					foreach($resultadoOrg as $vetorOrg)
																					{
                                                                                                                                                                            $naoCobrarDashboard = $vetorOrg['naoCobrarDashboard'];
																						switch ($vetorOrg['classificacaoOrganismoAfiliado']) {
																				            case 1:
																				                $classificacao = "Loja";
																				                break;
																				            case 2:
																				                $classificacao = "Pronaos";
																				                break;
																				            case 3:
																				                $classificacao = "Capítulo";
																				                break;
																				            case 4:
																				                $classificacao = "Heptada";
																				                break;
																				            case 5:
																				                $classificacao = "Atrium";
																				                break;
																				        }
																				        switch ($vetorOrg['tipoOrganismoAfiliado']) {
																				            case 1:
																				                $tipo = "R+C";
																				                break;
																				            case 2:
																				                $tipo = "TOM";
																				                break;
																				        }
																				
                                                                                                                                                                                $nomeOrganismo 			= $vetorOrg["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetorOrg["nomeOrganismoAfiliado"];
																						$idOrganismoAfiliado 	= $vetorOrg['idOrganismoAfiliado'];
																						$siglaOrganismo			= $vetorOrg['siglaOrganismoAfiliado'];
                                                                                                                                                                                
																						$total=0;//Contador de relatórios NÃO entregues
																						
																						//Encontrar Data Inicial com base no saldo inicial
																						
																						$si = new saldoInicial();
																						$mesSaldoInicial = "01";
																						$anoSaldoInicial = "2016";
                                                                                                                                                                                $trimestreSaldoInicial="1";
                                                                                                                                                                                $temSaldoInicial = 1;
																						
																						$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
																						$ativacao=0;
																						if($resultado)
																						{
																							$ativacao = count($resultado);
																							foreach($resultado as $vetor)
																							{
																								$mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
																								$anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
                                                                                                                                                                                                
                                                                                                                                                                                                //Trimestre atual
                                                                                                                                                                                                if($mesSaldoInicial==1||$mesSaldoInicial==2||$mesSaldoInicial==3)
                                                                                                                                                                                                {
                                                                                                                                                                                                    $trimestreSaldoInicial=1;
                                                                                                                                                                                                }
                                                                                                                                                                                                if($mesSaldoInicial==4||$mesSaldoInicial==5||$mesSaldoInicial==6)
                                                                                                                                                                                                {
                                                                                                                                                                                                    $trimestreSaldoInicial=2;
                                                                                                                                                                                                }
                                                                                                                                                                                                if($mesSaldoInicial==7||$mesSaldoInicial==8||$mesSaldoInicial==9)
                                                                                                                                                                                                {
                                                                                                                                                                                                    $trimestreSaldoInicial=3;
                                                                                                                                                                                                }
                                                                                                                                                                                                if($mesSaldoInicial==10||$mesSaldoInicial==11||$mesSaldoInicial==12)
                                                                                                                                                                                                {
                                                                                                                                                                                                    $trimestreSaldoInicial=4;
                                                                                                                                                                                                }
																							}
																						}else{
                                                                                                                                                                                    $temSaldoInicial = 0;
                                                                                                                                                                                    $oasSemSaldoInicial++;
                                                                                                                                                                                }
                                                                                                                                                                                
                                                                                                                                                                                /**
                                                                                                                                                                                * Entrega da Agenda Anual do Organismo
                                                                                                                                                                                */

                                                                                                                                                                               $ag = new AgendaAtividade();
                                                                                                                                                                               $cobraAgendaAnual=false;
                                                                                                                                                                               $totalAgendaAtividades=0;
                                                                                                                                                                               $resultado = $ag->listaAgendaAtividade($idOrganismoAfiliado,date('Y'));
                                                                                                                                                                               if (!$resultado) {
                                                                                                                                                                                   $cobraAgendaAnual=true;
                                                                                                                                                                                   $totalAgendaAtividades=1;
                                                                                                                                                                                   $total++;
                                                                                                                                                                               }
																						
																						/**
																						 * Entrega das Atas de Reunião Mensal Assinadas
																						 */
																						
																						
																						$arma = new ataReuniaoMensalAssinada();
																						
																						$mes = (int) $mesSaldoInicial;
																						$ano = (int) $anoSaldoInicial;
																						//echo "mes------>".$mes;
																						//echo "ano------>".$ano;
																						$totalAtaReuniaoMensal=0;
																						$arrMesesNaoEntreguesAtaReuniaoMensal=array();
																						while($ano<=$anoAtual)
																						{
																							$resultado = $arma->listaAtaReuniaoMensalAssinada(null,$mes,$ano,$idOrganismoAfiliado);
																							if(!$resultado)
																							{
																								$arrMesesNaoEntreguesAtaReuniaoMensal[]=mesExtensoPortugues($mes)."/".$ano;
																								$total++;
																								$totalAtaReuniaoMensal++;
																							}
																							if($mes==12&&$ano<=$anoAtual)
																							{
																								$ano++;
																								$mes=0;
																							}
																							$mes++;
                                                                                                                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                                                                                                                        {
                                                                                                                                                                                            $ano++;
                                                                                                                                                                                        }
																						}
																						
																						/**
																						 * Entrega das Atas de Posse Assinadas
																						 */
																						
																						
																						$apa = new ataPosseAssinada();
																						
																						//$mes = (int) $mesSaldoInicial;
																						$ano = (int) $anoSaldoInicial;
																						//echo "mes------>".$mes;
																						//echo "ano------>".$ano;
																						$totalAtaPosse=0;
																						$arrAnosNaoEntreguesAtaPosse=array();
                                                                                                                                                                                
																						while($ano<$anoAtual||(($ano==$anoAtual)&&(date('m')>4)))
																						{
																							$resultado = $apa->listaAtaPosseAssinada(null,null,$ano,$idOrganismoAfiliado);
																							if(!$resultado)
																							{
																								$arrAnosNaoEntreguesAtaPosse[]=$ano;
																								$total++;
																								$totalAtaPosse++;
																							}	
																							$ano++;
																						}
																						
																						/**
																						 * Entrega do Relatório de Membros Ativos
																						 */
																				
																						$mra	= new MembrosRosacruzesAtivos();
																						
																						$mes = (int) $mesSaldoInicial;
																						$ano = (int) $anoSaldoInicial;
																						//echo "mes------>".$mes;
																						//echo "ano------>".$ano;
																						$totalRelatorioMembrosAtivos=0;
																						$arrMesesNaoEntreguesRelatorioMembrosAtivos=array();
																						while($ano<=$anoAtual)
																						{
																							$mResultado	= $mra->retornaMembrosRosacruzesAtivos($mes,$ano,$idOrganismoAfiliado);
																							if($mResultado['numeroAtualMembrosAtivos']==0)
																							{
																								$arrMesesNaoEntreguesRelatorioMembrosAtivos[]=mesExtensoPortugues($mes)."/".$ano;
																								$total++;
																								$totalRelatorioMembrosAtivos++;
																							}
																							if($mes==12&&$ano<=$anoAtual)
																							{
																								$ano++;
																								$mes=0;
																							}
																							$mes++;
                                                                                                                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                                                                                                                        {
                                                                                                                                                                                            $ano++;
                                                                                                                                                                                        }
																						}
																						
																						/**
																						 * Entrega do Relatório Financeiro Mensal
																						 */
																						
																						
																						$rfm = new RelatorioFinanceiroMensal();
																						
																						$mes = (int) $mesSaldoInicial;
																						$ano = (int) $anoSaldoInicial;
																						//echo "mes------>".$mes;
																						//echo "ano------>".$ano;
																						$totalRelatorioFinanceiroMensal=0;
																						$arrMesesNaoEntreguesRelatorioFinanceiroMensal=array();
																						while($ano<=$anoAtual)
																						{
																							$resultado = $rfm->listaRelatorioFinanceiroMensal($mes,$ano,$idOrganismoAfiliado);
																							if(!$resultado)
																							{
																								$arrMesesNaoEntreguesRelatorioFinanceiroMensal[]=mesExtensoPortugues($mes)."/".$ano;
																								$total++;
																								$totalRelatorioFinanceiroMensal++;
																							}
																							if($mes==12&&$ano<=$anoAtual)
																							{
																								$ano++;
																								$mes=0;
																							}
																							$mes++;
                                                                                                                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                                                                                                                        {
                                                                                                                                                                                            $ano++;
                                                                                                                                                                                        }
																						}
																						
																						/**
																						 * Entrega do Relatório Financeiro Anual
																						 */
																						
																						
																						$rfa = new RelatorioFinanceiroAnual();
																						
																						//$mes = (int) $mesSaldoInicial;
																						$ano = (int) $anoSaldoInicial;
																						//echo "mes------>".$mes;
																						//echo "ano------>".$ano;
																						$totalRelatorioFinanceiroAnual=0;
																						$arrAnosNaoEntreguesRelatorioFinanceiroAnual=array();
																						while($ano<$anoAtual)
																						{
																							$resultado = $rfa->listaRelatorioFinanceiroAnual($ano,$idOrganismoAfiliado);
																							if(!$resultado)
																							{
																								$arrAnosNaoEntreguesRelatorioFinanceiroAnual[]=$ano;
																								$total++;
																								$totalRelatorioFinanceiroAnual++;
																							}
																							
																							$ano++;
																							
																							
																						}
																						
                                                                                                                                                                                /**
                                                                                                                                                                                * Entrega do Relatório de Imóveis Anual
                                                                                                                                                                                */


                                                                                                                                                                               $ic = new imovelControle();

                                                                                                                                                                               //$mes = (int) $mesSaldoInicial;
                                                                                                                                                                               $ano = (int) $anoSaldoInicial;
                                                                                                                                                                               //echo "mes------>".$mes;
                                                                                                                                                                               //echo "ano------>".$ano;
                                                                                                                                                                               $totalRelatorioImovelAnual=0;
                                                                                                                                                                               $arrAnosNaoEntreguesRelatorioImovel=array();
                                                                                                                                                                               while($ano<=$anoAtual)
                                                                                                                                                                               {
                                                                                                                                                                                       $resultado = $ic->listaImovelControle($idOrganismoAfiliado,$ano);
                                                                                                                                                                                       if(!$resultado&&$temSaldoInicial==1)
                                                                                                                                                                                       {
                                                                                                                                                                                                       $arrAnosNaoEntreguesRelatorioImovel[]=$ano;
                                                                                                                                                                                                       $total++;
                                                                                                                                                                                                       $totalRelatorioImovelAnual++;
                                                                                                                                                                                       }

                                                                                                                                                                                       $ano++;

                                                                                                                                                                               }



                                                                                                                                                                                
                                                                                                                                                                                //Verificar se oa tem Mestre da Classe dos Artesãos
                                                                                                                                                                                /*
                                                                                                                                                                                $ocultar_json=1;
                                                                                                                                                                                $seqCadast=0;
                                                                                                                                                                                $naoAtuantes='N';
                                                                                                                                                                                $atuantes='S';
                                                                                                                                                                                $siglaOA=$siglaOrganismo;
                                                                                                                                                                                $seqFuncao='315';//Mestre da Classe dos Artesãos
                                                                                                                                                                                include '../js/ajax/retornaFuncaoMembro.php';
                                                                                                                                                                                $obj = json_decode(json_encode($return),true);
                                                                                                                                                                                $temMestreClasseArtesaos=0;
                                                                                                                                                                                $arrMesesNaoEntreguesRelatorioClasseArtesaos=array();
                                                                                                                                                                                if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                                                                                                                                                                                {
                                                                                                                                                                                    $temMestreClasseArtesaos=1;

                                                                                                                                                                                    /**
                                                                                                                                                                                    * Entrega dos Relatórios da Classe dos Artesãos assinados
                                                                                                                                                                                    */
                                                                                                                                                                                   /* 
                                                                                                                                                                                   include_once('../model/relatorioClasseArtesaosAssinadoClass.php');
                                                                                                                                                                                   $rcaa = new relatorioClasseArtesaosAssinado();

                                                                                                                                                                                   $mes = (int) $mesSaldoInicial;
                                                                                                                                                                                   $ano = (int) $anoSaldoInicial;
                                                                                                                                                                                   //echo "mes------>".$mes;
                                                                                                                                                                                   //echo "ano------>".$ano;
                                                                                                                                                                                   $totalRelatorioClasseArtesaosMensal=0;
                                                                                                                                                                                   $arrMesesNaoEntreguesRelatorioClasseArtesaos=array();
                                                                                                                                                                                   while($mes<$mesAtual&&$ano<=$anoAtual)
                                                                                                                                                                                   {
                                                                                                                                                                                           $resultado = $rcaa->listaRelatorioClasseArtesaosAssinado(null,$idOrganismoAfiliado,$mes,$ano);
                                                                                                                                                                                           if(!$resultado&&$temSaldoInicial==1&&$mes!=1&&$mes!=12)//Dispensar de eles entregarem relatorio em janeiro e dezembro
                                                                                                                                                                                           {
                                                                                                                                                                                                   $arrMesesNaoEntreguesRelatorioClasseArtesaos[]=mesExtensoPortugues($mes)."/".$ano;
                                                                                                                                                                                                   $total++;
                                                                                                                                                                                                   $totalRelatorioClasseArtesaosMensal++;
                                                                                                                                                                                           }
                                                                                                                                                                                           if($mes==12&&$ano<=$anoAtual)
                                                                                                                                                                                           {
                                                                                                                                                                                                   $ano++;
                                                                                                                                                                                                   $mes=0;
                                                                                                                                                                                           }
                                                                                                                                                                                           $mes++;
                                                                                                                                                                                   }
                                                                                                                                                                                }
                                                                                                                                                                                */
                                                                                                                                                                                /**
                                                                                                                                                                                 * Entrega do Relatório de Atividades Mensal
                                                                                                                                                                                 */

                                                                                                                                                                                include_once('../model/relatorioAtividadeMensalClass.php');
                                                                                                                                                                                $ram = new RelatorioAtividadeMensal();

                                                                                                                                                                                $mes = (int) $mesSaldoInicial;
                                                                                                                                                                                $ano = (int) $anoSaldoInicial;
                                                                                                                                                                                //echo "mes------>".$mes;
                                                                                                                                                                                //echo "ano------>".$ano;
                                                                                                                                                                                $totalRelatorioAtividadeMensal=0;
                                                                                                                                                                                $arrMesesNaoEntreguesRelatorioAtividadeMensal=array();
                                                                                                                                                                                while($ano<=$anoAtual)
                                                                                                                                                                                {
                                                                                                                                                                                        $resultado = $ram->listaRelatorioAtividadeMensal($mes,$ano,$idOrganismoAfiliado);
                                                                                                                                                                                        if(!$resultado&&$temSaldoInicial==1)
                                                                                                                                                                                        {
                                                                                                                                                                                                $arrMesesNaoEntreguesRelatorioAtividadeMensal[]=mesExtensoPortugues($mes)."/".$ano;
                                                                                                                                                                                                $total++;
                                                                                                                                                                                                $totalRelatorioAtividadeMensal++;
                                                                                                                                                                                        }
                                                                                                                                                                                        if($mes==12&&$ano<=$anoAtual)
                                                                                                                                                                                        {
                                                                                                                                                                                                $ano++;
                                                                                                                                                                                                $mes=0;
                                                                                                                                                                                        }
                                                                                                                                                                                        $mes++;
                                                                                                                                                                                        if($ano==$anoAtual&&$mes>=$mesAtual)
                                                                                                                                                                                        {
                                                                                                                                                                                            $ano++;
                                                                                                                                                                                        }
                                                                                                                                                                                }

                                                                                                                                                                                //Verificar se oa tem Coordenadora de Columbas
                                                                                                                                                                                $ocultar_json=1;
                                                                                                                                                                                $naoAtuantes='N';
                                                                                                                                                                                $atuantes='S';
                                                                                                                                                                                $siglaOA=$siglaOrganismo;
                                                                                                                                                                                $seqCadast=0;
                                                                                                                                                                                $seqFuncao='609';//Orientadora de Columbas
                                                                                                                                                                                include '../js/ajax/retornaFuncaoMembro.php';
                                                                                                                                                                                $obj2 = json_decode(json_encode($return),true);
                                                                                                                                                                                $temCoordenadoraColumbas=0;
                                                                                                                                                                                if(isset($obj2['result'][0]['fields']['fArrayOficiais'][0]))
                                                                                                                                                                                {
                                                                                                                                                                                    $temCoordenadoraColumbas=1;
                                                                                                                                                                                    
                                                                                                                                                                                    /**
                                                                                                                                                                                    * Entrega dos Relatórios das Columbas Assinados
                                                                                                                                                                                    */

                                                                                                                                                                                   include_once('../model/relatorioTrimestralColumbaAssinadoClass.php');
                                                                                                                                                                                   $r = new RelatorioTrimestralColumbaAssinado();

                                                                                                                                                                                   $mes = (int) $mesSaldoInicial;
                                                                                                                                                                                   $ano = (int) $anoSaldoInicial;
                                                                                                                                                                                   $trimestre = (int) $trimestreSaldoInicial;
                                                                                                                                                                                   //echo "mes------>".$mes;
                                                                                                                                                                                   //echo "ano------>".$ano;
                                                                                                                                                                                   $totalRelatorioTrimestralColumba=0;
                                                                                                                                                                                   $arrMesesNaoEntreguesRelatorioTrimestralColumba=array();
                                                                                                                                                                                   while($ano<=$anoAtual)
                                                                                                                                                                                   {
                                                                                                                                                                                           $resultado = $r->listaRelatorioTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
                                                                                                                                                                                           if(!$resultado&&$temSaldoInicial==1)
                                                                                                                                                                                           {
                                                                                                                                                                                                    $arrMesesNaoEntreguesRelatorioTrimestralColumba[]=$trimestre."ºtrim/".$ano;
                                                                                                                                                                                                    //$total++;
                                                                                                                                                                                                    $totalRelatorioTrimestralColumba++;
                                                                                                                                                                                           }
                                                                                                                                                                                           if($trimestre==4&&$ano<=$anoAtual)
                                                                                                                                                                                           {
                                                                                                                                                                                                   $ano++;
                                                                                                                                                                                                   $trimestre=0;
                                                                                                                                                                                           }
                                                                                                                                                                                           $trimestre++;
                                                                                                                                                                                           if($ano==$anoAtual&&$trimestre>=$trimestreAtual)
                                                                                                                                                                                           {
                                                                                                                                                                                                $ano++;
                                                                                                                                                                                           }
                                                                                                                                                                                   }

                                                                                                                                                                                   /**
                                                                                                                                                                                    * Entrega dos Relatórios Atividades das Columbas Assinados
                                                                                                                                                                                    */

                                                                                                                                                                                   include_once('../model/relatorioAtividadeTrimestralColumbaAssinadoClass.php');
                                                                                                                                                                                   $r = new RelatorioAtividadeTrimestralColumbaAssinado();

                                                                                                                                                                                   $mes = (int) $mesSaldoInicial;
                                                                                                                                                                                   $ano = (int) $anoSaldoInicial;
                                                                                                                                                                                   $trimestre = (int) $trimestreSaldoInicial;
                                                                                                                                                                                   //echo "mes------>".$mes;
                                                                                                                                                                                   //echo "ano------>".$ano;
                                                                                                                                                                                   $totalRelatorioAtividadeTrimestralColumba=0;
                                                                                                                                                                                   $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba=array();
                                                                                                                                                                                   while($ano<=$anoAtual)
                                                                                                                                                                                   {
                                                                                                                                                                                           $resultado = $r->listaRelatorioAtividadeTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
                                                                                                                                                                                           if(!$resultado&&$temSaldoInicial==1)
                                                                                                                                                                                           {
                                                                                                                                                                                                    $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba[]=$trimestre."ºtrim/".$ano;
                                                                                                                                                                                                    $total++;
                                                                                                                                                                                                    $totalRelatorioAtividadeTrimestralColumba++;
                                                                                                                                                                                           }
                                                                                                                                                                                           if($trimestre==4&&$ano<=$anoAtual)
                                                                                                                                                                                           {
                                                                                                                                                                                                   $ano++;
                                                                                                                                                                                                   $trimestre=0;
                                                                                                                                                                                           }
                                                                                                                                                                                           $trimestre++;
                                                                                                                                                                                           if($ano==$anoAtual&&$trimestre>=$trimestreAtual)
                                                                                                                                                                                           {
                                                                                                                                                                                                $ano++;
                                                                                                                                                                                           }
                                                                                                                                                                                   }
                                                                                                                                                                                }
                                                                                                                                                                                
																						$texto .= '<b>Organismo '.$nomeOrganismo.'</b>:<br><br>';
																						if($naoCobrarDashboard==0)
                                                                                                                                                                                {
                                                                                                                                                                                    if($temSaldoInicial==1)
                                                                                                                                                                                    {    
                                                                                                                                                                                        if($total>0)
                                                                                                                                                                                        {
                                                                                                                                                                                                $texto .= '								Agenda Anual de Atividades para o Portal: '.$totalAgendaAtividades;
                                                                                                                                                                                                if ($cobraAgendaAnual) 
                                                                                                                                                                                                { 
                                                                                                                                                                                                        $texto .= '								('.date('Y').')';
                                                                                                                                                                                                }
                                                                                                                                                                                                $texto .= '								<br>Atas de Reunião Mensal: '.$totalAtaReuniaoMensal;
                                                                                                                                                                                                if(count($arrMesesNaoEntreguesAtaReuniaoMensal)>0)
                                                                                                                                                                                                { 
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrMesesNaoEntreguesAtaReuniaoMensal).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                $texto .= '								<br>Atas de Posse: '.$totalAtaPosse;
                                                                                                                                                                                                if(count($arrAnosNaoEntreguesAtaPosse)>0) 
                                                                                                                                                                                                {
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrAnosNaoEntreguesAtaPosse).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                $texto .= '								<br>Relatórios de Membros Ativos: '.$totalRelatorioMembrosAtivos;
                                                                                                                                                                                                if(count($arrMesesNaoEntreguesRelatorioMembrosAtivos)>0) 
                                                                                                                                                                                                {
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrMesesNaoEntreguesRelatorioMembrosAtivos).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                $texto .= '								<br>Relatórios Financ. Mensais: '.$totalRelatorioFinanceiroMensal;
                                                                                                                                                                                                if(count($arrMesesNaoEntreguesRelatorioFinanceiroMensal)>0) 
                                                                                                                                                                                                {
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrMesesNaoEntreguesRelatorioFinanceiroMensal).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                $texto .= '								<br>Relatórios Financ. Anuais: '.$totalRelatorioFinanceiroAnual;
                                                                                                                                                                                                if(count($arrAnosNaoEntreguesRelatorioFinanceiroAnual)>0) 
                                                                                                                                                                                                {
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrAnosNaoEntreguesRelatorioFinanceiroAnual).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                $texto .= '								<br>Relatórios Imóveis Anuais: '.$totalRelatorioImovelAnual;
                                                                                                                                                                                                if(count($arrAnosNaoEntreguesRelatorioImovel)>0) 
                                                                                                                                                                                                {
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrAnosNaoEntreguesRelatorioImovel).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                /*    
                                                                                                                                                                                                if(count($arrMesesNaoEntreguesRelatorioClasseArtesaos)>0&&$temMestreClasseArtesaos==1)
                                                                                                                                                                                                {    
                                                                                                                                                                                                    $texto	.= "                                                    <br>Rel. Classe dos Artesãos: ".$totalRelatorioClasseArtesaosMensal;
                                                                                                                                                                                                    $texto .= '								('.implode(", ",$arrMesesNaoEntreguesRelatorioClasseArtesaos).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                 */
                                                                                                                                                                                                if(count($arrMesesNaoEntreguesRelatorioAtividadeMensal)>0)
                                                                                                                                                                                                { 
                                                                                                                                                                                                    $texto	.= "                                                            <br>Rel. de Atividades: ".$totalRelatorioAtividadeMensal;
                                                                                                                                                                                                    $texto .= '								('.implode(", ",$arrMesesNaoEntreguesRelatorioAtividadeMensal).')';
                                                                                                                                                                                                }
                                                                                                                                                                                                if($temCoordenadoraColumbas==1&&$classificacao!="Pronaos")
                                                                                                                                                                                                {   
                                                                                                                                                                                                    /*
                                                                                                                                                                                                    if(count($arrMesesNaoEntreguesRelatorioTrimestralColumba)>0)
                                                                                                                                                                                                    {
                                                                                                                                                                                                        $texto	.= "                                                    <br>Rel. Trim. Columbas: ".$totalRelatorioTrimestralColumba;
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrMesesNaoEntreguesRelatorioTrimestralColumba).')';
                                                                                                                                                                                                    }
                                                                                                                                                                                                     */
                                                                                                                                                                                                    if(count($arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba)>0)
                                                                                                                                                                                                    {
                                                                                                                                                                                                        $texto	.= "                                                    <br>Rel. Trim. Ativ. Columbas: ".$totalRelatorioAtividadeTrimestralColumba;
                                                                                                                                                                                                        $texto .= '								('.implode(", ",$arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba).')';
                                                                                                                                                                                                    }
                                                                                                                                                                                                }

                                                                                                                                                                                                $texto .= '								<br>TOTAL DE RELATÓRIOS NÃO ENTREGUES: '.$total;
                                                                                                                                                                                        }else{
                                                                                                                                                                                                if($ativacao==1)
                                                                                                                                                                                                {
                                                                                                                                                                                                        $texto .= '								TOTAL DE RELATÓRIOS NÃO ENTREGUES: '.$total;
                                                                                                                                                                                                        $texto .= '								<br><br><b>PARABÉNS pelo empenho à toda equipe do Organismo '.$nomeOrganismo.'!</b>';
                                                                                                                                                                                                }else{
                                                                                                                                                                                                        $texto .= '								TOTAL DE RELATÓRIOS ENTREGUES: 0';
                                                                                                                                                                                                        $texto .= '								<br><br><b>Não consta nenhum relatório entregue do Organismo '.$nomeOrganismo.'!</b>';
                                                                                                                                                                                                }
                                                                                                                                                                                        }
                                                                                                                                                                                        $texto .= '<br><br><br>';
                                                                                                                                                                                    }else{
                                                                                                                                                                                       $texto .= '<font color=red><b>*Módulo financeiro não inicializado!</b></font><br><br>';
                                                                                                                                                                                    }
                                                                                                                                                                                }else{
                                                                                                                                                                                    echo "<font color=red><b>*Para esse organismo não devem ser cobrados relatórios</b></font><br><br>";
                                                                                                                                                                                }
																					}
																				}
																			
		
														$texto .= '				</td>
																															</tr>';
                                                                                                                                                                                                                                         $texto .= '    <tr
																																style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																																<td class="ecxcontent-block"
																																	style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																																	valign="top"><hr><font color="gray">*<i>Módulo Financeiro não inicializado</i>:  Organismo que ainda não enviou nenhum relatório mensal/trimestral através do sistema SOA.
                                                                                                                                                                                                                                                                                Recomendamos que os oficiais administrativos em exercício, caso não tenham realizado o seu cadastro individual, que utilizem a opção “Cadastre-se aqui” através do sistema SOA na internet em http://soa.amorc.org.br<br>
                                                                                                                                                                                                                                                                                Acessando o Sistema SOA existirá a opção de “Video-aulas” que contém as instruções fundamentais para o melhor uso do sistema e entrega dos relatórios pelo meio digital.</font></td>
																															</tr>
																															<tr
																																style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																																<td class="ecxcontent-block"
																																	style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																																	valign="top"><font color="red">Essa é uma mensagem automática, não há necessidade de resposta!</font></td>
																															</tr>
																															<tr
																																style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																																<td class="ecxcontent-block"
																																	style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																																	valign="top">Em caso de dúvida, por favor entre em contato: atendimentoportal@amorc.org.br</td>
																															</tr>
																															<tr
																																style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																																<td class="ecxcontent-block"
																																	style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																																	valign="top"> 
																																	Ordem Rosacruz, AMORC – Grande Loja da Jurisdição de Língua Portuguesa<br>
															                														Rua Nicarágua, 2620, Curitiba – Paraná<br>
															                														Telefone: (+55) 041- 3351-3000</td>
																															</tr>
																														</tbody>
																													</table></td>
																											</tr>
																										</tbody>
																									</table>
																									<div class="footer"
																										style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
																										<div style="text-align: left;"></div>
																										<table width="100%"
																											style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																											<tbody>
																												<tr
																													style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																													<td class="ecxaligncenter ecxcontent-block"
																														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
																														align="center" valign="top">AMORC-GLP '.date('Y').'</td>
																												</tr>
																											</tbody>
																										</table>
																									</div>
																								</div>
																								<div style="text-align: left;"></div></td>
																							<td
																								style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
																								valign="top"></td>
																						</tr>
																					</tbody>
																				</table>
																				</div>
																			</div>';
														/*	
														$servidor = "smtp.gmail.com";
                                                                                                                $porta = "465";
                                                                                                                $usuario = "cpdglp@gmail.com";
                                                                                                                $senha = "@w2xpglp";
												
														//echo $texto;
														#Disparar email
														$mail = new PHPMailer();
                                                                                                                $mail->isSMTP();
                                                                                                                $mail->SMTPSecure = "ssl";
                                                                                                                $mail->SMTPAuth = true;
                                                                                                                $mail->Host = $servidor; // SMTP utilizado
                                                                                                                $mail->Port = $porta;
                                                                                                                $mail->Username = $usuario;
                                                                                                                $mail->Password = $senha;
                                                                                                                $mail->From = 'cpdglp@gmail.com';
                                                                                                                $mail->AddReplyTo("atendimento@amorc.org.br", "Antiga e Mística Ordem Rosacruz - AMORC");
                                                                                                                $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
														//$mail->AddAddress("luis.fernando@amorc.org.br");
														//$mail->AddAddress("tatiane.jesus@amorc.org.br");
														//$mail->AddAddress("lucianob@amorc.org.br");
														//$mail->AddAddress("samufcaldas@hotmail.com.br");
														//$mail->AddAddress("samuel.caldas@amorc.org.br");
														//$mail->AddAddress("luciano.bastos@gmail.com");
														//$mail->AddAddress("amigoeterno10@hotmail.com");
														//$mail->AddAddress("braz@amorc.org.br");
														$mail->AddAddress($vetorUsu['emailUsuario']);
														$mail->isHTML(true);
														$mail->CharSet = 'utf-8';
														//$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
														$mail->Subject = 'Notificação de Entrega de Relatório - AMORC';
														$mail->Body = $texto;
														$mail->AltBody = strip_tags($texto);
                                                                                                                 
														//echo $texto;
                                                                                                                $enviado = $mail->Send();                                                                                                                         
                                                                                                                */
                                                                                                                                                                                                                                         
                                                                                                                # Make the call to the client.
                                                                                                                
                                                                                                                $enviado = $mgClient->sendMessage($domain, array(
                                                                                                                        'from'    => 'TI <atendimento@amorc.org.br>',
                                                                                                                        'to'      => $vetorUsu['nomeUsuario'].' <'.$vetorUsu['emailUsuario'].'>',
                                                                                                                        'subject' => 'Notificação de Entrega de Relatório - AMORC',
                                                                                                                        'text'    => 'Seu e-mail não suporta HTML',
                                                                                                                        'html'    => $texto));
														
                                                                                                                 /*                                                                                                                           
														 $enviado = $mgClient->sendMessage($domain, array(
                                                                                                                        'from'    => 'Atendimento <atendimento@amorc.org.br>',
                                                                                                                        'to'      => 'Braz Alves <amigoeterno10@hotmail.com>',
                                                                                                                        'subject' => 'Notificação de Entrega de Relatório - AMORC',
                                                                                                                        'text'    => 'Seu e-mail não suporta HTML',
                                                                                                                        'html'    => $texto));
                                                                                                                  */
														 if($enviado)
														 {
                                                                                                                                                                                                                                       
                                                                                                                     //exit();
                                                                                                                     //Guardar log de que o email foi enviado
                                                                                                                    $n = new notificacaoRelatoriosNaoEntreguesEmailGc();
                                                                                                                    $n->setFk_idUsuario($vetorUsu['idUsuario']);
                                                                                                                    $n->setRegiao($vetorRegiao['regiaoRosacruz']);
                                                                                                                    $n->setNome(strtoupper($vetorUsu['nomeUsuario']));
                                                                                                                    $n->setEmail($vetorUsu['emailUsuario']);
                                                                                                                    $res = $n->cadastra();
                                                                                                                    
                                                                                                                    //Exibir mensagem de sucesso
                                                                                                                    echo "<br>Email enviado com sucesso para <b>".strtoupper($vetorUsu['nomeUsuario'])."-email:".$vetorUsu['emailUsuario']."</b>";
                                                                                                                    //echo "<br>Email enviado com sucesso para <b>".strtoupper($vetorUsu['nomeUsuario'])."-email:samuel.caldas@amorc.org.br</b>";
                                                                                                                    //exit();
														 }else{
                                                                                                                     echo "não enviou";
                                                                                                                     //exit();
                                                                                                                 }
                                                                                                                 
                                                                                                                 
														 
                                            }				 
			}
		}
	}
}


echo "<br><br><b>Não existem mais grandes conselheiros para serem avisados. Cron executada com sucesso!</b>";
?>	