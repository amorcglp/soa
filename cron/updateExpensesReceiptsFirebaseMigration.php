<?php

require_once '../vendor/autoload.php';

use Google\Cloud\Firestore\FirestoreClient;

include_once('../lib/functions.php');

$soadesenvolvimento = new FirestoreClient([
    'projectId' => 'soadesenvolvimento'
]);
$soatreinamento = new FirestoreClient([
    'projectId' => 'soatreinamento'
]);

$documentsDesenvolvimento = $soadesenvolvimento->collection("balances")->documents();
$arrOasDesenvolvimento = [];
foreach($documentsDesenvolvimento as $documentDesenvolvimento)
    if($documentDesenvolvimento->exists())
        $arrOasDesenvolvimento[] = $documentDesenvolvimento->id();

$documentsOasTreinamento = $soatreinamento->collection("balances")->documents();
$arrOasTreinamento = [];
foreach($documentsOasTreinamento as $documentTreinamento)
    if($documentTreinamento->exists())
        $arrOasTreinamento[] = $documentTreinamento->id();

$organismos = [];
foreach($arrOasDesenvolvimento as $item) {
    if(!in_array($item, $arrOasTreinamento))
        array_push($organismos, $item);
}

if(count($organismos) < 1)
    die("Não existe nenhum organismo com projeção no firebase.\n");

//$organismos = [1];
foreach ($organismos as $idOrganismoAfiliado) {
    echo "Processamento Financeiro ID: " . $idOrganismoAfiliado . "\n";
    $inicio     = time();

    $saldoInicial   = $soadesenvolvimento->collection("balances")->document($idOrganismoAfiliado);
    $snapshot       = $saldoInicial->snapshot();
    $data          = $snapshot->data();

    $saldoInicialNovo = $soatreinamento->collection("balances")->document($idOrganismoAfiliado);
    $saldoInicialNovo->set($data);
    echo "Saldo Inicial migrado.\n";

    $recebimentos = $soadesenvolvimento->collection("balances/" . $idOrganismoAfiliado . "/recebimentos");
    $queryRecebimentos = $recebimentos->documents();
    foreach ($queryRecebimentos as $documentRecebimentos) {
        if ($documentRecebimentos->exists()) {
            $dataRecebimentos = $documentRecebimentos->data();
            $recebimentosNovos = $soatreinamento->collection("balances/" . $idOrganismoAfiliado . "/recebimentos")->newDocument();
            $recebimentosNovos->set($dataRecebimentos);
        }
    }
    echo "Recebimentos migrados.\n";

    $despesas = $soadesenvolvimento->collection("balances/" . $idOrganismoAfiliado . "/despesas");
    $queryDespesas = $despesas->documents();
    foreach ($queryDespesas as $documentDespesas) {
        if ($documentDespesas->exists()) {
            $dataDespesas = $documentDespesas->data();
            $despesasNovos = $soatreinamento->collection("balances/" . $idOrganismoAfiliado . "/despesas")->document($documentDespesas->id());
            $despesasNovos->set($dataDespesas);
        }
    }
    echo "Despesas migradas.\n";

    $balance = $soadesenvolvimento->collection("balances/" . $idOrganismoAfiliado . "/balance");
    $queryBalance = $balance->documents();
    foreach ($queryBalance as $documentBalance) {
        if ($documentBalance->exists()) {
            $dataBalance = $documentBalance->data();
            $balanceNovos = $soatreinamento->collection("balances/" . $idOrganismoAfiliado . "/balance")->document($documentBalance->id());
            $balanceNovos->set($dataBalance);
        }
    }
    echo "Projeção migrada.\n";

    $fim        = time();
    $tempo      = $fim - $inicio;
    echo "[" . $idOrganismoAfiliado . "] Tempo de processamento: " . (number_format($tempo/60, 2)) . "m (" . $tempo . "s) \n\n";
}