<?php

//Conectar PDO
$pdo = new PDO('mysql:host=localhost;dbname=tom_soa', "tom_soa", "@w2xpglp");
//$pdo = new PDO('mysql:host=localhost;dbname=soa2', "root", "");
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

set_time_limit(0);
error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1);
ini_set('max_execution_time', 5000);

include_once('../lib/functions.php');
include_once('../model/regiaoRosacruzClass.php');
include_once('../model/organismoClass.php');
include_once('../model/membroPotencialOAClass.php');
include_once '../model/wsClass.php';

//$membroPotencialOA = new membroPotencialOA();

//Selecionar uma região que não foi cadastrada hoje
$hoje = date("Y-m-d");

$regiaoRosacruz = new regiaoRosacruz();

$organismo = new organismo();
$regCadastrada=array();
$resultadoReg1 = $regiaoRosacruz->listaRegiaoRosacruzCadastradaMembroPotencial($hoje);
if ($resultadoReg1) {
    foreach ($resultadoReg1 as $vetorReg1) {
        $regCadastrada[] = $vetorReg1['regiao']; 
    }
}
if(count($regCadastrada)==0)
{
    $membroPotencialOA = new membroPotencialOA();
    $membroPotencialOA->limpaTabela();
}    
echo "<pre>";print_r($regCadastrada);
$resultaReg = $regiaoRosacruz->listaRegiaoRosacruz(1);

$i=0;
$regiaoNaoCadastrada="XX";
if ($resultaReg) {
    foreach ($resultaReg as $vetorReg) {
        echo "<br>".$vetorReg['regiaoRosacruz']; 
        if(!in_array($vetorReg['regiaoRosacruz'],$regCadastrada)&&$vetorReg['regiaoRosacruz']!="PT1"&&$vetorReg['regiaoRosacruz']!="RA1")
        {
            if($i==0)
            {
                $regiaoNaoCadastrada = $vetorReg['regiaoRosacruz'];
            }
            $i++;
        }
    }
}
echo "<br><br>Região: " . $regiaoNaoCadastrada;
$resultadoOrg = $organismo->listaOrganismo(null, null, null, $regiaoNaoCadastrada);

//Limpar todos os dados da tabela de membros em potencial
//$membroPotencialOA->limpaTabela();
$res = false;
if ($resultadoOrg) {
    
    
    
    foreach ($resultadoOrg as $vetorOrg) {
        $siglaOA = $vetorOrg['siglaOrganismoAfiliado'];
        echo "<br><br>SIGLA DO OA: " . $siglaOA . "<br><br>";
        /*
        define('ocultar_json',1);
        define('saldoRemessaRosacruz',"P");
        define('situacaoRemessaRosacruz',1);
        define('situacaoCadastralRosacruz',1);
        */
       
        $ocultar_json = 1;
        $saldoRemessaRosacruz = "P";
        $situacaoRemessaRosacruz = 1;
        $situacaoCadastralRosacruz = 1;

        $server = 138;
        include '../js/ajax/retornaRelacaoMembrosPorOaAtuacaoCep.php';
        $arrMembros = $return->result[0]->fields->fArrayMembros;
        echo "<pre>";
        print_r($arrMembros);

        if (count($arrMembros) > 0) {
            foreach ($arrMembros as $seq) {

                //Puxar outras informações para guardar na classe do membro em potencial
                // Instancia a classe
                $ws2 = new Ws($server);

                // Nome do Método que deseja chamar
                $method2 = 'RetornaDadosMembroPorSeqCadast';

                // Parametros que serão enviados à chamada
                $params2 = array('CodUsuario' => 'lucianob',
                    'SeqCadast' => $seq);

                // Chamada do método
                $return2 = $ws2->callMethod($method2, $params2, 'lucianob');

                $arrDadosPessoais = $return2->result[0]->fields;

                $membroPotencialOA = new membroPotencialOA();
                //echo "->array completo:<pre>".$arrDadosPessoais."<br>";exit();
                $membroPotencialOA->setFk_idOrganismoAfiliado($vetorOrg['idOrganismoAfiliado']);
                $membroPotencialOA->setRegiao(substr($siglaOA,0,3));
                $membroPotencialOA->setSiglaOA($siglaOA);
                $membroPotencialOA->setSeqCadastMembroOa($seq);
                $membroPotencialOA->setFIdeTipoPessoa($arrDadosPessoais->fIdeTipoPessoa);
                $membroPotencialOA->setFNomCliente($arrDadosPessoais->fNomCliente);
                $membroPotencialOA->setFNomConjuge($arrDadosPessoais->fNomConjuge);
                $membroPotencialOA->setFIdeSexo($arrDadosPessoais->fIdeSexo);
                $membroPotencialOA->setFDatNascimento($arrDadosPessoais->fDatNascimento);
                $membroPotencialOA->setFCodCpf($arrDadosPessoais->fCodCpf);
                $membroPotencialOA->setFCodRg($arrDadosPessoais->fCodRg);
                $membroPotencialOA->setFDesOrgaoEmissao($arrDadosPessoais->fDesOrgaoEmissao);
                $membroPotencialOA->setFCodCnpj($arrDadosPessoais->fCodCnpj);
                $membroPotencialOA->setFCodInscricaoEstadual($arrDadosPessoais->fCodInscricaoEstadual);
                $membroPotencialOA->setFCodEstadoCivil($arrDadosPessoais->fCodEstadoCivil);
                $membroPotencialOA->setFCodGrauInstrucao($arrDadosPessoais->fCodGrauInstrucao);
                $membroPotencialOA->setFSeqOcupacao($arrDadosPessoais->fSeqOcupacao);
                $membroPotencialOA->setFSeqEspecializacao($arrDadosPessoais->fSeqEspecializacao);
                $membroPotencialOA->setFSeqFormacao($arrDadosPessoais->fSeqFormacao);
                $membroPotencialOA->setFCodCliente($arrDadosPessoais->fCodCliente);
                $membroPotencialOA->setFCodNaoMembro($arrDadosPessoais->fCodNaoMembro);
                $membroPotencialOA->setFCodRosacruz($arrDadosPessoais->fCodRosacruz);
                $membroPotencialOA->setFCodOgg($arrDadosPessoais->fCodOgg);
                $membroPotencialOA->setFCodAma($arrDadosPessoais->fCodAma);
                $membroPotencialOA->setFCodAssinanteRosacruz($arrDadosPessoais->fCodAssinanteRosacruz);
                $membroPotencialOA->setFCodAssinanteAmorcCultural($arrDadosPessoais->fCodAssinanteAmorcCultural);
                $membroPotencialOA->setFCodAssinanteOrdemJuvenil($arrDadosPessoais->fCodAssinanteOrdemJuvenil);
                $membroPotencialOA->setFCodArtista($arrDadosPessoais->fCodArtista);
                $membroPotencialOA->setFCodPalestrante($arrDadosPessoais->fCodPalestrante);
                $membroPotencialOA->setFCodLeitor($arrDadosPessoais->fCodLeitor);
                $membroPotencialOA->setFSigOA($arrDadosPessoais->fSigOA);
                $membroPotencialOA->setFIdeTipoOa($arrDadosPessoais->fIdeTipoOa);
                $membroPotencialOA->setFIdeTipoTom($arrDadosPessoais->fIdeTipoTom);
                $membroPotencialOA->setFNomLogradouro($arrDadosPessoais->fNomLogradouro);
                $membroPotencialOA->setFNumEndereco($arrDadosPessoais->fNumEndereco);
                $membroPotencialOA->setFDesComplementoEndereco($arrDadosPessoais->fDesComplementoEndereco);
                $membroPotencialOA->setFNomBairro($arrDadosPessoais->fNomBairro);
                $membroPotencialOA->setFNomCidade($arrDadosPessoais->fNomCidade);
                $membroPotencialOA->setFSigUf($arrDadosPessoais->fSigUf);
                $membroPotencialOA->setFCodCepEndereco($arrDadosPessoais->fCodCepEndereco);
                $membroPotencialOA->setFSigPaisEndereco($arrDadosPessoais->fSigPaisEndereco);
                $membroPotencialOA->setFNumCaixaPostal($arrDadosPessoais->fNumCaixaPostal);
                $membroPotencialOA->setFCodCepCaixaPostal($arrDadosPessoais->fCodCepCaixaPostal);
                $membroPotencialOA->setFIdeEnderecoCorrespondencia($arrDadosPessoais->fIdeEnderecoCorrespondencia);
                $membroPotencialOA->setFNumTelefone($arrDadosPessoais->fNumTelefone);
                $membroPotencialOA->setFNumFax($arrDadosPessoais->fNumFax);
                $membroPotencialOA->setFNumTelefoFixo($arrDadosPessoais->fNumTelefoFixo);
                $membroPotencialOA->setFNumCelula($arrDadosPessoais->fNumCelula);
                $membroPotencialOA->setFNumOutroTelefo($arrDadosPessoais->fNumOutroTelefo);
                $membroPotencialOA->setFDesOutroTelefo($arrDadosPessoais->fDesOutroTelefo);
                $membroPotencialOA->setFDesEmail($arrDadosPessoais->fDesEmail);
                $membroPotencialOA->setFDatImplantacaoCadastro($arrDadosPessoais->fDatImplantacaoCadastro);
                $membroPotencialOA->setFDatCadastro($arrDadosPessoais->fDatCadastro);
                $membroPotencialOA->setFCodUsuarioCadastro($arrDadosPessoais->fCodUsuarioCadastro);
                $membroPotencialOA->setFIdeTipoFiliacRosacruz($arrDadosPessoais->fIdeTipoFiliacRosacruz);
                $membroPotencialOA->setFSeqCadastPrincipalRosacruz($arrDadosPessoais->fSeqCadastPrincipalRosacruz);
                $membroPotencialOA->setFSeqCadastCompanheiroRosacruz($arrDadosPessoais->fSeqCadastCompanheiroRosacruz);
                $membroPotencialOA->setFIdeTipoFiliacTom($arrDadosPessoais->fIdeTipoFiliacTom);
                $membroPotencialOA->setFSeqCadastPrincipalTom($arrDadosPessoais->fSeqCadastPrincipalTom);
                $membroPotencialOA->setFSeqCadastCompanheiroTom($arrDadosPessoais->fSeqCadastCompanheiroTom);
                $membroPotencialOA->setFIdeContribuicaoCarne($arrDadosPessoais->fIdeContribuicaoCarne);
                $membroPotencialOA->setFCodRosacruzCompanheiro($arrDadosPessoais->fCodRosacruzCompanheiro);
                $membroPotencialOA->setFIdeTipoMoeda($arrDadosPessoais->fIdeTipoMoeda);
                $membroPotencialOA->setFIdeModoFaturamento($arrDadosPessoais->fIdeModoFaturamento);
                $membroPotencialOA->setFIdeDependenciaAdministrativa($arrDadosPessoais->fIdeDependenciaAdministrativa);
                $membroPotencialOA->setFDesObservacao($arrDadosPessoais->fDesObservacao);
                $membroPotencialOA->setFIdeTipoSituacMembroRosacr($arrDadosPessoais->fIdeTipoSituacMembroRosacr);
                $membroPotencialOA->setFIdeTipoSituacMembroOjp($arrDadosPessoais->fIdeTipoSituacMembroOjp);
                $membroPotencialOA->setFIdeTipoSituacMembroTom($arrDadosPessoais->fIdeTipoSituacMembroTom);
                $membroPotencialOA->setFIdeTipoSituacMembroAma($arrDadosPessoais->fIdeTipoSituacMembroAma);
                $membroPotencialOA->setFIdeTipoSituacRemessRosacr($arrDadosPessoais->fIdeTipoSituacRemessRosacr);
                $membroPotencialOA->setFIdeTipoSituacRemessOjp($arrDadosPessoais->fIdeTipoSituacRemessOjp);
                $membroPotencialOA->setFIdeTipoSituacRemessTom($arrDadosPessoais->fIdeTipoSituacRemessTom);
                $membroPotencialOA->setFIdeTipoSituacRemessAma($arrDadosPessoais->fIdeTipoSituacRemessAma);
                $membroPotencialOA->setFDatQuitacRosacr($arrDadosPessoais->fDatQuitacRosacr);
                $membroPotencialOA->setFDatQuitacOjp($arrDadosPessoais->fDatQuitacOjp);
                $membroPotencialOA->setFDatQuitacTom($arrDadosPessoais->fDatQuitacTom);
                $membroPotencialOA->setFDatQuitacAma($arrDadosPessoais->fDatQuitacAma);
                $membroPotencialOA->setFNumLoteAtualRosacr($arrDadosPessoais->fNumLoteAtualRosacr);
                $membroPotencialOA->setFNumLoteAtualOjp($arrDadosPessoais->fNumLoteAtualOjp);
                $membroPotencialOA->setFNumLoteAtualTom($arrDadosPessoais->fNumLoteAtualTom);
                $membroPotencialOA->setFNumLoteAtualAma($arrDadosPessoais->fNumLoteAtualAma);
                $membroPotencialOA->setFNumLoteLimiteRosacr($arrDadosPessoais->fNumLoteLimiteRosacr);
                $membroPotencialOA->setFNumLoteLimiteTom($arrDadosPessoais->fNumLoteLimiteTom);
                $membroPotencialOA->setFDatAdmissRosacr($arrDadosPessoais->fDatAdmissRosacr);
                $membroPotencialOA->setFSeqCadastOaAtuacao(0);
                $membroPotencialOA->setFSigOaAtuacao("");

                $res = $membroPotencialOA->cadastroMembroPotencialOA($pdo);
                if ($res) {
                    echo "<br><br>INSERIU:" . $seq;
                }
                //usleep(1000000);
            }
        }
    }
}
    

$c = NULL; //fechar conexao
 
?>	