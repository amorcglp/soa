<?php

# Include the Autoloader (see "Libraries" for install instructions)
$base = dirname(dirname(__FILE__));
require $base . '/vendor/autoload.php';
use Mailgun\Mailgun;



# Instantiate the client.
$mgClient = new Mailgun('key-24e5c1eb843300db6922fee30623bcf0');
/** Official */
$domain = "mg.amorc.org.br";
/** Sandbox */
//$domain = "sandbox622f068a94064201a9d4021ad9bd9042.mailgun.org";

    $arr=array();
    //include_once('../lib/phpmailer/class.phpmailer.php');
    include_once($base . '/model/notificacaoAtualizacaoServerClass.php');
    $n = new NotificacaoServer();
    $tituloNotificacao="";
    $textoNotificacao="";
    $hoje=date("Y-m-d");
    //Selecionar notificacao
    $respNotificacao = $n->listaNotificacaoServer(1,$hoje,true);
    if($respNotificacao)
    {
        foreach ($respNotificacao as $notificacao)
        {
            $idNotificacao = $notificacao['idNotificacao'];
            $tituloNotificacao = $notificacao['tituloNotificacao'];
            $textoNotificacao = $notificacao['descricaoNotificacao'];
            $statusNotificacao = $notificacao['statusNotificacao'];
            
            //Selecionar usuários já cadastrados
            include_once($base . '/model/notificacaoAtualizacaoServerNotificadosClass.php');
            $nsn = new NotificacaoServerNotificados();
            
            $c =0;
            $menosEstesUsuarios=null;
            $respNotificados = $nsn->lista($idNotificacao,$hoje,true);
            if($respNotificados)
            {
                foreach($respNotificados as $vetor2)
                {
                    if($c==0)
                    {
                        $menosEstesUsuarios=$vetor2['seqCadast'];
                    }else{
                        $menosEstesUsuarios .= ",".$vetor2['seqCadast'];
                    }
                    $c++;
                }    
            }    
            //echo "====>".$menosEstesUsuarios;
            include_once($base . '/model/usuarioClass.php');
            $u = new Usuario();
            //contar quantos ativos existem
            $resUsuario = $u->listaUsuario(null, null, null, null, null, "seqCadast", 1, null, null,null, "0", "20",$menosEstesUsuarios);
            //echo "<pre>";print_r($resUsuario);
            if (is_array($resUsuario)) 
            { 
                if(isset($resUsuario)&&$tituloNotificacao!=""&&$textoNotificacao!=""&&$statusNotificacao!=1)
                {
                    foreach($resUsuario as $v)
                    {
                        $texto = '<div>
                                        <div dir="ltr">
                                                <table class="ecxbody-wrap"
                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                                                        <tbody>
                                                                <tr
                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                        <td
                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                                                valign="top"></td>
                                                                        <td class="ecxcontainer" width="600"
                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                                                valign="top">
                                                                                <div class="ecxcontent"
                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                                                        <table class="ecxmain" width="100%" cellpadding="0"
                                                                                                cellspacing="0"
                                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                                                <tbody>
                                                                                                        <tr
                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                <td class="ecxcontent-wrap"
                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                                                        valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                                                        style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                                                        alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                                                <br>
                                                                                                                <br>
                                                                                                                <br>
                                                                                                                        <div style="text-align: left;"></div>
                                                                                                                        <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                <tbody>
                                                                                                                                        <tr
                                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                <td class="ecxcontent-block"
                                                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                                                        valign="top">Estimado(a) <b>'.strtoupper($v['nomeUsuario']).'</b>,</td>
                                                                                                                                        </tr>
                                                                                                                                        <tr
                                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                <td class="ecxcontent-block"
                                                                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                        valign="top">Saudações Fraternais! </td>
                                                                                                                                        </tr>
                                                                                                                                        <tr
                                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                <td class="ecxcontent-block"
                                                                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                        valign="top">Por meio deste informamos as novas atualizações do sistema SOA:</td>
                                                                                                                                        </tr>
                                                                                                                                        <tr
                                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                <td class="ecxcontent-block"
                                                                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                        valign="top">';
                                                                                                                                                        $texto .= $tituloNotificacao;
                                                                                                                                                        $texto .= "<br><br>";
                                                                                                                                                        $texto .= $textoNotificacao;

                                                                                                $texto .= '				</td>
                                                                                                                                        </tr>
                                                                                                                                        ';
                                                                                                $texto .= '     			<tr
                                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                <td class="ecxcontent-block"
                                                                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                        valign="top"><font color="red">Essa é uma mensagem automática, não há necessidade de resposta!</font></td>
                                                                                                                                        </tr>
                                                                                                                                        <tr
                                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                <td class="ecxcontent-block"
                                                                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                        valign="top">Em caso de dúvida, por favor entre em contato: atendimentoportal@amorc.org.br</td>
                                                                                                                                        </tr>
                                                                                                                                        <tr
                                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                <td class="ecxcontent-block"
                                                                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                        valign="top"> 
                                                                                                                                                        Ordem Rosacruz, AMORC – Grande Loja da Jurisdição de Língua Portuguesa<br>
                                                                                                                                        Rua Nicarágua, 2620, Curitiba – Paraná<br>
                                                                                                                                        Telefone: (+55) 041- 3351-3000</td>
                                                                                                                                        </tr>
                                                                                                                                </tbody>
                                                                                                                        </table></td>
                                                                                                        </tr>
                                                                                                </tbody>
                                                                                        </table>
                                                                                        <div class="footer"
                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                                                <div style="text-align: left;"></div>
                                                                                                <table width="100%"
                                                                                                        style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                        <tbody>
                                                                                                                <tr
                                                                                                                        style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                        <td class="ecxaligncenter ecxcontent-block"
                                                                                                                                style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                                                align="center" valign="top">AMORC-GLP '.date('Y').'</td>
                                                                                                                </tr>
                                                                                                        </tbody>
                                                                                                </table>
                                                                                        </div>
                                                                                </div>
                                                                                <div style="text-align: left;"></div></td>
                                                                        <td
                                                                                style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                                                valign="top"></td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                                </div>
                                        </div>';
                                        //echo $texto;
                                        //$z++;

                                        $servidor="smtp.office365.com";
                                        $porta="587";
                                        $usuario="noresponseglp@amorc.org.br";
                                        $senha="@w2xpglp";

                                        //echo $senha;
                                        #Disparar email
                                        /*
                                        $mail = new PHPMailer();
                                        $mail->isSMTP();
                                        $mail->SMTPSecure = "tls";
                                        $mail->SMTPAuth = true;
                                        $mail->Host = $servidor;	// SMTP utilizado
                                        $mail->Port = $porta;
                                        $mail->Username = $usuario;
                                        $mail->Password = $senha;
                                        $mail->From = 'noresponseglp@amorc.org.br';
                                        $mail->AddReplyTo("atendimento@amorc.org.br","Antiga e Mística Ordem Rosacruz - AMORC");
                                        $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
                                        //$mail->AddAddress("luis.fernando@amorc.org.br");
                                        //$mail->AddAddress("tatiane.jesus@amorc.org.br");
                                        //$mail->AddAddress("lucianob@amorc.org.br");
                                        //$mail->AddAddress("samufcaldas@hotmail.com.br");
                                        //$mail->AddAddress("samuel.caldas@amorc.org.br");
                                        //$mail->AddAddress("luciano.bastos@gmail.com");
                                        //$mail->AddAddress("braz@amorc.org.br");
                                        $mail->AddAddress("amigoeterno10@hotmail.com");
                                        //$mail->AddAddress($v['emailUsuario']);
                                        $mail->isHTML(true);
                                        $mail->CharSet = 'utf-8';
                                        //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
                                        $mail->Subject = 'Atualização do SOA: '.$tituloNotificacao;
                                        $mail->Body = $texto;
                                        $mail->AltBody = strip_tags($texto);

                                        //$arr['status']=1;

                                        $enviado = $mail->Send();
                                        if($enviado)
                                        {
                                         */

                                        # Make the call to the client.
                                        //echo "<br>Teste Input E-amil:".$v['emailUsuario'];
                                        if($v['emailUsuario']!=""&&strpos($v['emailUsuario'],'@'))
                                        {        
                                            $result = $mgClient->sendMessage($domain, array(
                                                            'from'    => 'Atendimento GLP <noresponseglp@amorc.org.br>',
                                                            'to'      => strtoupper($v['nomeUsuario']).' <'.$v['emailUsuario'].'>',
                                                            'subject' => 'Atualização do SOA: '.$tituloNotificacao,
                                                            'text'    => '',
                                                            'html'    => $texto));

                                            /*
                                            $result = $mgClient->sendMessage($domain, array(
                                                            'from'    => 'Atendimento GLP <noresponseglp@amorc.org.br>',
                                                            'to'      => strtoupper($v['nomeUsuario']).' <amigoeterno10@hotmail.com>',
                                                            'subject' => 'Atualização do SOA: '.$tituloNotificacao,
                                                            'text'    => '',
                                                            'html'    => $texto));
                                            */                
                                        }
                                        if($result)
                                        {    
                                            //Registrar que a notificação foi enviada para este usuário
                                            $nsn= new NotificacaoServerNotificados();
                                            $nsn->setFk_idNotificacao($idNotificacao);
                                            $nsn->setSeqCadast($v['seqCadast']);
                                            $nsn->cadastra();
                                            echo "Cron executada com sucesso! Envidado para Nome: ".strtoupper($v['nomeUsuario'])."/E-mail: ".$v['emailUsuario']."<br>";
                                        }else{
                                            echo "Erro ao enviar notificação para esse usuário Nome: ".strtoupper($v['nomeUsuario'])."/E-mail: ".$v['emailUsuario']."<br>";
                                        }
                                        //exit();

                    }
                    
                }else{
                    //$arr['status']=0;
                }
            }else{
                    echo "<b>>>Não existem mais usuários para enviar a notificação (".$idNotificacao.")<<</b><br>";
            }
        }
    }else{
        echo "<b>>>Não existe nenhuma notificação de atualização agendada para hoje e que seja ativa<<</b><br>";
    }
    //echo json_encode($arr);
	
?>
