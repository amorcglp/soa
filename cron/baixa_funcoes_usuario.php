<?php 
session_start();
//echo "====SESSAO====";
//echo "<pre>";print_r($_SESSION);
?>
<meta charset="utf-8">
<script src=\"../js/jquery-2.1.1.js\"></script>
<script src=\"../js/bootstrap.min.js\"></script>
<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
<script src=\"../js/functions3.js\"></script>
<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
<script src=\"../js/inspinia.js\"></script>
<script src=\"../js/plugins/pace/pace.min.js\"></script>
<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
<script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
<script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
<script src=\"../js/jquery.uploadify.min.js\"></script>
<?php 

$server=135;
$continuarTentandoBaixa=false;
$_SESSION['arrFuncaoBaixa']=array();

set_time_limit(0);
error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1);

//ini_set('max_execution_time', 5000);

include_once('../lib/functions.php');
include_once('../model/regiaoRosacruzClass.php');
include_once('../model/organismoClass.php');
include_once('../model/baixaAutomaticaFuncoesUsuarioClass.php');
include_once '../model/wsClass.php';
include_once('../model/funcaoUsuarioClass.php');
include_once('../model/funcaoClass.php');
include_once('../model/funcaoUsuarioClass.php');

//$membroPotencialOA = new membroPotencialOA();

//Selecionar uma região que não foi cadastrada hoje
$hoje = date("Y-m-d");
//$hoje = null;
$baixaAutomaticaFuncoesUsuario = new baixaAutomaticaFuncoesUsuario();

$organismo = new organismo();
$oaCadastrado=array();
$resultadoAgenda = $baixaAutomaticaFuncoesUsuario->selecionaAgenda(null,$hoje);
if($resultadoAgenda)
{
    foreach ($resultadoAgenda as $vetorAgenda)
    {    
        $resultadoOa1 = $baixaAutomaticaFuncoesUsuario->listaBaixaAutomaticaFuncoesUsuarioHoje($vetorAgenda['dataAgendadaInicial'],$vetorAgenda['dataAgendadaFinal']);
        if ($resultadoOa1) {
            foreach ($resultadoOa1 as $vetorOa1) {
                $oaCadastrado[] = $vetorOa1['siglaOA']; 
            }
        }
        
        if(count($oaCadastrado)==0)
        {
            $baixaAutomaticaFuncoesUsuario = new baixaAutomaticaFuncoesUsuario();
            //$baixaAutomaticaFuncoesUsuario->limpaTabela();
        }    
        //echo "<pre>";print_r($oaCadastrado);
        $organismo = new organismo();
        $resultaOa = $organismo->listaOrganismo(null,null,null,null,null,null,null,1);

        $i=0;
        $y=0;
        $organismoNaoCadastrado="XX";
        if ($resultaOa) {
            foreach ($resultaOa as $vetorOa) {
                //echo "<br>".$y."-".$vetorOa['siglaOrganismoAfiliado']; 
                if(!in_array($vetorOa['siglaOrganismoAfiliado'],$oaCadastrado))
                {
                    if($i==0)
                    {
                        $organismoNaoCadastrado = $vetorOa['siglaOrganismoAfiliado'];
                    }
                    $i++;
                }
                $y++;
            }
        }
        //Selecionar Organismos da Região Não cadastrada
        if($organismoNaoCadastrado!="XX")
        {    
            echo "<br><br>Organismo: " . $organismoNaoCadastrado;

        }
        $resultadoOrg = $organismo->listaOrganismo(null, $organismoNaoCadastrado);

        $i=1;
        $baixaAconteceu=0;
        $mudouOA=0;
        $naoAtualizouSigla=0;
        $siglaOA="";
        $arrPais=array();

        //Limpar todos os dados da tabela de membros em potencial
        //$membroPotencialOA->limpaTabela();
        $res = false;
        if ($resultadoOrg) {

            foreach ($resultadoOrg as $vetorOrg) 
            {
                //Selecionar Sigla do Pais do Organismo
                switch ($vetorOrg['paisOrganismoAfiliado'])
                {
                    case 1:
                        $siglaPais = "BR";
                        $arrPais[0]="BR";
                        break;
                    case 2:
                        $siglaPais = "PT";
                        $arrPais[0]="PT";
                        break;
                    case 3:
                        $siglaPais = "AO";
                        $arrPais[0]="AO";
                        break;
                    case 4:
                        $siglaPais = "MZ";
                        $arrPais[0]="MZ";
                        break;
                    default:
                        $siglaPais = "BR";
                        $arrPais[0]="BR";
                        break;
                }
                $siglaOA = $vetorOrg['siglaOrganismoAfiliado'];
                //Se é um organismo do exterior, buscar brasileiros que atuam lá fora
                if($siglaPais=="PT"||$siglaPais=="AO"||$siglaPais=="MZ")
                {
                    $arrPais[1]="BR";
                }    
                $hoje = date("Y-m-d")."T00:00:00";
                
                if(count($arrPais)>0)
                {
                    foreach($arrPais as $vetorArrPais)
                    {    
                        $siglaPais = $vetorArrPais;
                        $seqFuncao='0';
                        $nomeParcialFuncao='';
                        $seqCadast='';
                        /*
                        echo "<br><br>".$siglaPais;
                        echo "<pre>";print_r($arrPais);
                        echo "<br>siglaOA:".$siglaOA;    
                         */
                        //Recuperar dados de todas as funções atuantes
                        $ocultar_json=1;
                        $naoAtuantes='N';
                        $atuantes='S';
                        $tipoMembro=1;//R+C
                        include '../js/ajax/retornaFuncaoMembro.php';

                        $obj = json_decode(json_encode($return),true);
                        //echo "<pre>".print_r($obj['result'][0]['fields']['fArrayOficiais']);
                        $arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];
                        echo "<pre>";print_r($arrFuncoes);
                       
                        if(count($arrFuncoes)>0)
                        {
                            echo "<br><br>=>total de funcoes neste OA:".count($arrFuncoes)."<br><br>";

                            foreach($arrFuncoes as $vetor)
                            {
                                    $seqCadast=0;//Zerar seqCadast a cada lopping
                                    $companheiro="";//Tornar companheiro principal a cada looping
                                    $siglaOAAtuacao="";
                                    if($vetor['fields']['fDatSaida']==0)//Verificar apenas campos que a data de saida não foi cadastrada
                                    {
                                            $seqCadast = $vetor['fields']['fSeqCadast'];
                                            $seqCadastOriginal = $vetor['fields']['fSeqCadast'];
                                            $dataTerminoMandato = $vetor['fields']['fDatTerminMandat'];
                                            $codFuncao = $vetor['fields']['fCodFuncao'];
                                            $codFuncaoOriginalEx = $vetor['fields']['fCodFuncao'];
                                            $tipoCargo = $vetor['fields']['fCodTipoFuncao'];
                                            $codigoDeAfiliacao = $vetor['fields']['fCodMembro'];
                                            $dataEntrada = $vetor['fields']['fDatEntrad'];
                                            $desFuncao = substr($vetor['fields']['fDesFuncao'],0,3);
                                            $desFuncaoCompleta = $vetor['fields']['fDesFuncao'];
                                            $codFuncaoEx = $vetor['fields']['fCodFuncao'];
                                            $siglaOrganismoWS = $vetor['fields']['fSigOrgafi'];
                                            $siglaOrganismoOriginal = $vetor['fields']['fSigOrgafi'];
                                            $nomeOficial = $vetor['fields']['fNomClient'];
                                            $codFuncaoEx++;
                                            $codFuncaoOriginalEx++;
                                            //echo "ATUANTE=>".$atuantes." / NAO ATUANTE=>".$naoAtuantes."<br>";
                                            //Se a baixa acontece entre os meses 10 ou 11 não acrescentar 40 dias
                                            if($vetorAgenda['quantidadeDiasProrrogacaoBaixa']==0)
                                            {
                                                $data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                            }else{    
                                                $data1_inteiro = strtotime("+".$vetorAgenda['quantidadeDiasProrrogacaoBaixa']." days",strtotime(substr($dataTerminoMandato,0,10)));
                                            }
                                            //echo "<br><br>data1:".date('d/m/Y',$data1_inteiro)."<br><br>";
                                            $data2_inteiro = strtotime(date("Y-m-d"));
                                            //echo "/data2:".$data2_inteiro."/";
                                            //echo "desFuncao:".$desFuncao."<br>";
                                            if($desFuncao!="EX-")
                                            {
                                                    //Se o Mandato termina hoje entao atualizar no ORCZ
                                                    
                                                    if($data1_inteiro<$data2_inteiro)
                                                    {	
                                                        echo "<br>Separou para dar baixa==>".$seqCadastOriginal;
                                                        
                                                        $atualizouSigla=0;
                                                        $naoAtualizouSigla=0;
                                                        $registroAconteceu=0;
                                                        $mudouOA=0;
                                                        $conseguiuInformacoesDoOficial=0;
                                                        $companheiro="";
                                                        $siglaOAAtuacao="";
                                                        $seqCadastPrincipal=0;
                                                        $ERRO1=false;
                                                        $ERRO2=false;
                                                        $dual="N";
                                                        $naoDeixarCadastrarFuncao=false;
                                                        $erroAoPegarInformacoesDoOficial=false;

                                                        //Liberar variáveis da sessão caso não for dado baixa ainda
                                                        if(isset($_SESSION['arrFuncaoCompanheiro'][$codigoDeAfiliacao][$siglaOrganismoOriginal]))
                                                        {    
                                                            if(in_array($codFuncaoOriginalEx,$_SESSION['arrFuncaoCompanheiro'][$codigoDeAfiliacao][$siglaOrganismoOriginal]))
                                                            {
                                                                $key = array_search($codFuncaoOriginalEx, $_SESSION['arrFuncaoCompanheiro'][$codigoDeAfiliacao][$siglaOrganismoOriginal]);
                                                                if($key!==false){
                                                                    unset($_SESSION['arrFuncaoCompanheiro'][$codigoDeAfiliacao][$siglaOrganismoOriginal][$key]);
                                                                }
                                                            }        
                                                        }
                                                        if(isset($_SESSION['arrFuncaoPrincipal'][$codigoDeAfiliacao][$siglaOrganismoOriginal]))
                                                        {    
                                                            if(in_array($codFuncaoOriginalEx,$_SESSION['arrFuncaoPrincipal'][$codigoDeAfiliacao][$siglaOrganismoOriginal]))
                                                            {
                                                                $key = array_search($codFuncaoOriginalEx, $_SESSION['arrFuncaoPrincipal'][$codigoDeAfiliacao][$siglaOrganismoOriginal]);
                                                                if($key!==false){
                                                                    unset($_SESSION['arrFuncaoPrincipal'][$codigoDeAfiliacao][$siglaOrganismoOriginal][$key]);
                                                                }
                                                            }        
                                                        }
                                                        //Pegar informação se é companheiro entre outras do oficial
                                                        $ocultar_json=1;
                                                        // Instancia a classe
                                                        $ws21 = new Ws($server);

                                                        // Nome do Método que deseja chamar
                                                        $method21 = 'RetornaDadosMembroPorSeqCadast';

                                                        // Parametros que serão enviados à chamada
                                                        $params21 = array('CodUsuario' => 'lucianob',
                                                                                        'SeqCadast' => $seqCadastOriginal);

                                                        // Chamada do método
                                                        $return21 = $ws21->callMethod($method21, $params21, 'lucianob');
                                                        $obj2 = json_decode(json_encode($return21),true);
                                                        //echo "<pre>";print_r($obj2);
                                                        if(isset($obj2['result']))
                                                        {    
                                                            if(substr($obj2['result'][0]['fields']['fDesMensag'],0,4)=="Erro")
                                                            {
                                                                $erroAoPegarInformacoesDoOficial=true;
                                                            }    
                                                            if($obj2['result'][0]['fields']['fDesMensag']=="Busca com sucesso !" || isset($obj2['result'][0]['fields']['fNomCliente']))
                                                            {
                                                                $siglaOAAtuacao = $obj2['result'][0]['fields']['fSigOaAtuacaoRosacr'];
                                                                if($obj2['result'][0]['fields']['fIdeTipoFiliacRosacruz']=="D"||$obj2['result'][0]['fields']['fIdeTipoFiliacRosacruz']=="C")
                                                                {    
                                                                    $dual="S";
                                                                    $seqCadastPrincipal = $obj2['result'][0]['fields']['fSeqCadastPrincipalRosacruz'];
                                                                }else{
                                                                    $dual="N";
                                                                    $tipoMembro=1;
                                                                }
                                                                echo "<br><br>===============seqCadast:".$seqCadastOriginal."==================";
                                                                echo "<br>seqCadastCompanheiro:".$obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];
                                                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz']!=0&&$obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz']==$seqCadastOriginal)
                                                                {
                                                                    $companheiro    = 'S';
                                                                }else{
                                                                    $companheiro    = '';
                                                                }
                                                                if($obj2['result'][0]['fields']['fIdeTipoTom']=="S")
                                                                {
                                                                    $tipoMembro			= 1;
                                                                }else{
                                                                    $tipoMembro			= 1;
                                                                }
                                                                //Verificar se é companheiro e se é TOM
                                                                if($obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz']!=0&&$companheiro=="S"&&$tipoMembro==6)
                                                                {
                                                                    //Pegar dados do principal
                                                                    $seqCadast2 = $obj2['result'][0]['fields']['fSeqCadastPrincipalRosacruz'];//R+C
                                                                    $ocultar_json=1;
                                                                    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
                                                                    $objPrincipal = json_decode(json_encode($return),true);
                                                                    if(count($objPrincipal['result'])>0)
                                                                    { 
                                                                        if($objPrincipal['result'][0]['fields']['fDesMensag']=="Busca com sucesso !")
                                                                        {
                                                                            if($objPrincipal['result'][0]['fields']['fIdeTipoTom']=="N")
                                                                            {
                                                                                //Entao atribuir rosacruz para esse companheiro
                                                                                $tipoMembro = 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                //Verificar se é principal e se é individual na TOM
                                                                if($obj2['result'][0]['fields']['fSeqCadastPrincipalRosacruz']!=0&&$companheiro==""&&$tipoMembro==6)
                                                                {
                                                                    //Pegar dados do principal
                                                                    $seqCadast2 = $obj2['result'][0]['fields']['fSeqCadastCompanheiroRosacruz'];//R+C
                                                                    $ocultar_json=1;
                                                                    include '../js/ajax/retornaDadosMembroPorSeqCadast.php';
                                                                    $objCompanheiro = json_decode(json_encode($return),true);
                                                                    if(count($objCompanheiro['result'])>0)
                                                                    { 
                                                                        if($objCompanheiro['result'][0]['fields']['fDesMensag']=="Busca com sucesso !")
                                                                        {
                                                                            if($objCompanheiro['result'][0]['fields']['fIdeTipoTom']=="N")
                                                                            {
                                                                                //Entao atribuir rosacruz para esse companheiro
                                                                                $tipoMembro = 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                $conseguiuInformacoesDoOficial=1;
                                                            }else{
                                                                echo "<br><br>====Nao conseguiu buscar informacoes no oficial no ORCZ====".$seqCadastOriginal;
                                                                $conseguiuInformacoesDoOficial=0;
                                                            }
                                                        }else{
                                                            echo "<br><br>Erro ao retornar Tipo e Companheiro-seqCadast:".$seqCadast."-OA:".$siglaOrganismoOriginal;
                                                            exit();
                                                            $ERRO1=true;
                                                        }
                                                        //echo "<br>Comparacao OA do Seq com OA da funcao:(".$obj2['result'][0]['fields']['fSigOaAtuacaoRosacr']."-".$siglaOrganismoWS.")";
                                                        //Pegar informação do OA de atuação e atualizar caso seja diferente do oa de atuação da função
                                                        echo "<br><br> Sigla do OA de Atuacao da pessoa: (".$siglaOAAtuacao.") - Sigla do OA da funcao: (".$siglaOrganismoWS.")";
                                                        if($siglaOAAtuacao!=$siglaOrganismoWS)//R+C
                                                        {
                                                            echo '<br>Encontrou diferenca na atuacao dos OAs: oaATUACAO ('.$obj2['result'][0]['fields']['fSigOaAtuacaoRosacr'].') - oaFUNCAO ('.$siglaOrganismoWS.') - seqCadast:'.$seqCadast.'<br>';
                                                            $siglaOrganismoWS = $obj2['result'][0]['fields']['fSigOaAtuacaoRosacr'];//R+C
                                                            $atualizouSigla=1;
                                                            $siglaOrganismoAnterior = $obj2['result'][0]['fields']['fSigOaAtuacaoRosacr'];
                                                        }  

                                                        //Atualizar OA de atuação do OFICIAL
                                                        if(($atualizouSigla==1||$obj2['result'][0]['fields']['fSeqCadastOaAtuacaoRosacr']=="0")&&$conseguiuInformacoesDoOficial==1)//R+C
                                                        {
                                                            $continuarTentandoBaixa=true;

                                                            // Instancia a classe
                                                            $ws5 = new Ws($server);

                                                            // Nome do Método que deseja chamar
                                                            $method5 = 'AtualizarOaDeAtuacaoDoMembro';

                                                            // Parametros que serão enviados à chamada
                                                            $params5 = array('CodUsuario' => 'lucianob',
                                                                                            'SeqCadast' => $seqCadastOriginal,
                                                                                            'SigOaAtuacaoRosacr' => $siglaOrganismoOriginal,//R+C
                                                                                            'SigOaAtuacaoTom' => '', 
                                                                                    );

                                                            // Chamada do método
                                                            $return5 = $ws5->callMethod($method5, $params5, 'lucianob');

                                                            $objAtualizacaoOA5 = json_decode(json_encode($return5),true);
                                                            //echo "<br>seqCadastAtualizacao1:".$seqCadast;
                                                            //echo "siglaOAAtualizacao1:".$siglaOrganismoOriginal;
                                                            //echo "<pre>";print_r($objAtualizacaoOA);
                                                            if(isset($objAtualizacaoOA5['result']))
                                                            {    
                                                                if($objAtualizacaoOA5['result'][0]['fields']['fIdeAtualizacaoOk']=="S")
                                                                {
                                                                    $mudouOA=1;
                                                                    echo "<br><br>====Conseguiu atualizar a sigla do OA no ORCZ====".$siglaOrganismoOriginal."===seqCadast:".$seqCadastOriginal;
                                                                }else{
                                                                    echo "<br><br>====Não conseguiu atualizar a sigla do OA no ORCZ====";
                                                                }
                                                            }else{
                                                                echo "<br><br>Erro ao atualizar OA DE ATUACAO DO OFICIAL-seqCadast:".$seqCadast."-OA:".$siglaOrganismoOriginal;
                                                                exit();
                                                                $ERRO1=true;
                                                            }

                                                            //Se for dual e companheiro atualizar OA de atuação do principal
                                                            if($dual=="S"&&$companheiro=="S")
                                                            {    
                                                                $mudouOA=0;
                                                                // Instancia a classe
                                                                $ws4 = new Ws($server);

                                                                // Nome do Método que deseja chamar
                                                                $method4 = 'AtualizarOaDeAtuacaoDoMembro';

                                                                // Parametros que serão enviados à chamada
                                                                $params4 = array('CodUsuario' => 'lucianob',
                                                                                                'SeqCadast' => $seqCadastPrincipal,
                                                                                                'SigOaAtuacaoRosacr' => $siglaOrganismoOriginal,//R+C
                                                                                                'SigOaAtuacaoTom' => '', 
                                                                                        );

                                                                // Chamada do método
                                                                $return4 = $ws4->callMethod($method4, $params4, 'lucianob');

                                                                $objAtualizacaoOA2 = json_decode(json_encode($return4),true);
                                                                //echo "<br>seqCadastAtualizacao1:".$seqCadast;
                                                                //echo "siglaOAAtualizacao1:".$siglaOrganismoOriginal;
                                                                //echo "<pre>";print_r($objAtualizacaoOA);
                                                                if(isset($objAtualizacaoOA2['result']))
                                                                {    
                                                                    if($objAtualizacaoOA2['result'][0]['fields']['fIdeAtualizacaoOk']=="S")
                                                                    {
                                                                        $mudouOA=1;
                                                                        echo "<br><br>====Conseguiu atualizar a sigla do OA do PRINCIPAL no ORCZ====";
                                                                    }else{
                                                                        echo "<br><br>====Nao conseguiu atualizar a sigla do OA do PRINCIPAL no ORCZ====";
                                                                    }
                                                                }else{
                                                                    echo "<br><br>Erro ao atualizar OA DE ATUACAO DO OFICIAL PRINCIPAL-seqCadast:".$seqCadastOriginal."-OA:".$siglaOrganismoOriginal;
                                                                    exit();
                                                                    $ERRO2=true;
                                                                }
                                                            }
                                                        }else{
                                                            $naoAtualizouSigla=1;
                                                        }    

                                                        $siglaOrganismoAfiliado		= $siglaOA;
                                                        $codAfiliacaoMembroOficial	= $codigoDeAfiliacao;
                                                        $tipoCargo			= $tipoCargo;
                                                        $funcao				= $codFuncaoEx;
                                                        $dataEntrada			= $dataEntrada;

                                                        if(($dataTerminoMandato!="")&&($naoAtualizouSigla==1||$mudouOA==1)&&$ERRO1==false&&$ERRO2==false&&$conseguiuInformacoesDoOficial==1)
                                                        {    

                                                            //Se o codigo de afiliação é igual a zero não continuar dando baixa
                                                            if($codAfiliacaoMembroOficial!=0)
                                                            {    
                                                                $continuarTentandoBaixa=true;
                                                            }
                                                            //Se a data de entrada é vazio não continuar dando baixa
                                                            if($dataEntrada==""||$codAfiliacaoMembroOficial==0||$dataTerminoMandato==""||$erroAoPegarInformacoesDoOficial==true||$desFuncaoCompleta=="livre este codigo")
                                                            {
                                                                $baixaAutomaticaFuncoesUsuario->cadastraNotLog($seqCadastOriginal,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato);
                                                                $continuarTentandoBaixa=false;
                                                            }  

                                                            //Se estiver tentando dar baixa em uma função do principal não deixar dar baixa
                                                            if($dual=="S"&&$companheiro==""&&isset($_SESSION['arrFuncaoCompanheiro'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal]))
                                                            {
                                                                if(in_array($codFuncaoOriginalEx,$_SESSION['arrFuncaoCompanheiro'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal]))
                                                                {
                                                                    $naoDeixarCadastrarFuncao=true;
                                                                }
                                                            }
                                                            if($dual=="S"&&$companheiro=="S"&&isset($_SESSION['arrFuncaoPrincipal'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal]))
                                                            {
                                                                if(in_array($codFuncaoOriginalEx,$_SESSION['arrFuncaoPrincipal'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal]))
                                                                {
                                                                    $naoDeixarCadastrarFuncao=true;
                                                                }        
                                                            }

                                                            if($dual=="S")
                                                            {
                                                                $tipoMembro = 1;
                                                            }

                                                            //Verificação de duplicados
                                                            if(isset($_SESSION['arrFuncaoBaixa'][$seqCadastOriginal][$siglaOrganismoOriginal]))
                                                            {
                                                                //echo "<pre>";print_r($_SESSION['arrFuncaoBaixa'][$seqCadastOriginal][$siglaOrganismoOriginal]);
                                                                if(in_array($codFuncaoOriginalEx,$_SESSION['arrFuncaoBaixa'][$seqCadastOriginal][$siglaOrganismoOriginal]))
                                                                {
                                                                    $baixaAutomaticaFuncoesUsuario->cadastraDuplicados($seqCadastOriginal,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato);
                                                                }
                                                            }

                                                            /*
                                                            if($naoDeixarCadastrarFuncao==false)
                                                            {    
                                                                echo "<br>NaoDeixarCadastrarFuncao:".$naoDeixarCadastrarFuncao;
                                                            }
                                                             */
                                                            //echo "<br>erroAoPegarInformacoesDoOficial:".$erroAoPegarInformacoesDoOficial;
                                                            //echo "<br>DesFuncaoCompleta:".$desFuncaoCompleta;
                                                            //echo "<br>ConseguiuInformacoesDoOficial:".$conseguiuInformacoesDoOficial;
                                                            if($naoDeixarCadastrarFuncao==false&&$erroAoPegarInformacoesDoOficial==false&&trim($desFuncaoCompleta)!="livre este codigo"&&$conseguiuInformacoesDoOficial==1)
                                                            {   

                                                                // Instancia a classe
                                                                $ws2 = new Ws($server);

                                                                // Nome do Método que deseja chamar
                                                                $method2 = 'AtualizarOficial';

                                                                echo "<br><br>===parametros===";
                                                                echo "<br>seqCadast:".$seqCadastOriginal;
                                                                echo "<br>CodMembro:".$codAfiliacaoMembroOficial;
                                                                echo "<br>IdeTipoMembro:".$tipoMembro;
                                                                echo "<br>IdeCompanheiro:".$companheiro;
                                                                echo "<br>SigOrgafi:".$siglaOrganismoOriginal;
                                                                echo "<br>SeqTipoFuncaoOficial:".$tipoCargo;
                                                                echo "<br>SeqFuncao:".$codFuncaoOriginalEx;
                                                                echo "<br>DatHoraEntrada:".$dataEntrada;
                                                                echo "<br>DatSaida:";
                                                                echo "<br>DatTerminMandato:".$dataTerminoMandato;
                                                                echo "<br>IdeTipoMotivoSaida:T";
                                                                echo "<br>===fim parametros===";
                                                                if($dual=="S"&&$companheiro=="S")
                                                                {
                                                                    $_SESSION['arrFuncaoCompanheiro'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal][]=$codFuncaoOriginalEx;
                                                                    $_SESSION['arrFuncaoCompanheiro'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal]["codigoAfiliacao"]=$codAfiliacaoMembroOficial;
                                                                } 
                                                                if($dual=="S"&&$companheiro=="")
                                                                {
                                                                    $_SESSION['arrFuncaoPrincipal'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal][]=$codFuncaoOriginalEx;
                                                                    $_SESSION['arrFuncaoPrincipal'][$codAfiliacaoMembroOficial][$siglaOrganismoOriginal]["codigoAfiliacao"]=$codAfiliacaoMembroOficial;
                                                                } 
                                                                $_SESSION['arrFuncaoBaixa'][$seqCadastOriginal][$siglaOrganismoOriginal][]=$codFuncaoOriginalEx;
                                                                //echo "<pre>";print_r($_SESSION['arrFuncaoBaixa'][$seqCadastOriginal][$siglaOrganismoOriginal]);

                                                                // Parametros que serão enviados à chamada
                                                                $params2 = array('CodUsuario' => 'lucianob',
                                                                                                'CodMembro' => $codAfiliacaoMembroOficial,
                                                                                                'IdeTipoMembro' => $tipoMembro,
                                                                                                'IdeCompanheiro' => $companheiro,
                                                                                                'SigOrgafi' => $siglaOrganismoOriginal,
                                                                                                'SeqTipoFuncaoOficial' => $tipoCargo,
                                                                                                'SeqFuncao' => $codFuncaoOriginalEx,//função trocada para ex
                                                                                                'DatHoraEntrada' => $dataEntrada,
                                                                                                'DatSaida' => '',//Durante a baixa automática não preencher data de saida
                                                                                                'DatTerminMandato' => $dataTerminoMandato,
                                                                                                'IdeTipoMotivoSaida' => 'T'//Dizer que o motivo da saída é o termino do mandato 
                                                                                        );

                                                                // Chamada do método
                                                                $return2 = $ws2->callMethod($method2, $params2, 'lucianob');
                                                                $objBaixa = json_decode(json_encode($return2),true);
                                                                if($objBaixa['result'][0]['fields']['fIdeInclusaoOk']=="S")
                                                                {
                                                                    //Caso esteja na lista de pendentes tirar da lista
                                                                    if($baixaAutomaticaFuncoesUsuario->verificaSeJaExisteNotLog($seqCadastOriginal, $codAfiliacaoMembroOficial, $tipoMembro, $companheiro, $siglaOrganismoOriginal, $tipoCargo, $funcao, $dataEntrada, $dataTerminoMandato))
                                                                    {
                                                                        $baixaAutomaticaFuncoesUsuario->excluiNotLog($seqCadastOriginal, $codAfiliacaoMembroOficial, $tipoMembro, $companheiro, $siglaOrganismoOriginal, $tipoCargo, $funcao, $dataEntrada, $dataTerminoMandato);
                                                                    }        
                                                                    //echo "<pre>";print_r($return2);
                                                                    echo "<br>".$i." - O oficial ".$nomeOficial." - SeqCadast(".$seqCadastOriginal.")";
                                                                    if($dual=="S")
                                                                    {
                                                                        echo " - Dual";
                                                                        if($companheiro=="S")
                                                                        {    
                                                                            echo " - Companheiro";
                                                                        }else{
                                                                            echo " - Principal";
                                                                        }
                                                                    }else{
                                                                        echo " - Individual";
                                                                    }
                                                                    echo " - Tipo: ".$tipoMembro." - cuja funcao e ".$vetor['fields']['fDesFuncao']." - Cod. ".$codigoDeAfiliacao." recebeu baixa no ORCZ - ".$siglaOrganismoOriginal;

                                                                    $i++;
                                                                    $seqCadast=0;
                                                                    $companheiro="";
                                                                    $siglaOAAtuacao="";

                                                                    //GERAR LOG DA BAIXA AUTOMÁTICA
                                                                    $baixaAutomaticaFuncoesUsuario->cadastraLog($seqCadastOriginal,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato);

                                                                    $baixaAconteceu=1;

                                                                    $registroAconteceu=1;
                                                                }else{
                                                                    if(isset($objBaixa['result'][0]['fields']['fDesMensag']))
                                                                    {    
                                                                        echo "<br>".$objBaixa['result'][0]['fields']['fDesMensag'];
                                                                        if(($dataEntrada!=""&&$codAfiliacaoMembroOficial!=0&&$dataTerminoMandato!=""&&$erroAoPegarInformacoesDoOficial==false&&$desFuncaoCompleta!="livre este codigo")||(substr($objBaixa['result'][0]['fields']['fDesMensag'],0,18)=="Erro! Existem mais"))
                                                                        {
                                                                            $existemMaisDeUmCadastro=0;
                                                                            if(substr($objBaixa['result'][0]['fields']['fDesMensag'],0,18)=="Erro! Existem mais")
                                                                            {
                                                                                $existemMaisDeUmCadastro=1;
                                                                            } 
                                                                            echo "<br>existemMaisDeUmCadastro=>".$existemMaisDeUmCadastro."<br>";
                                                                            $baixaAutomaticaFuncoesUsuario->cadastraNotLog($seqCadastOriginal,$codAfiliacaoMembroOficial,$tipoMembro,$companheiro,$siglaOrganismoOriginal,$tipoCargo,$funcao,$dataEntrada,$dataTerminoMandato,$existemMaisDeUmCadastro);
                                                                            $continuarTentandoBaixa=false;
                                                                        }
                                                                    }
                                                                    $ERRO2=true;
                                                                    echo "<br>Erro ao dar baixa na função do oficial -".$i." - O oficial ".$nomeOficial." - SeqCadast(".$seqCadastOriginal.") - Comp. ".$companheiro." - Tipo: ".$tipoMembro." - cuja função e ".$vetor['fields']['fDesFuncao']." - Cod. ".$codigoDeAfiliacao." NÃO recebeu baixa no ORCZ";
                                                                }    
                                                            }
                                                        }
                                                        /*
                                                        //Se atualizou a sigla voltar para a sigla anterior
                                                        if($atualizouSigla==1&&$mudouOA==1&&$ERRO1==false&&$conseguiuInformacoesDoOficial==1)
                                                        {
                                                            // Instancia a classe
                                                            $ws4 = new Ws($server);

                                                            // Nome do Método que deseja chamar
                                                            $method4 = 'AtualizarOaDeAtuacaoDoMembro';

                                                            // Parametros que serão enviados à chamada
                                                            $params4 = array('CodUsuario' => 'lucianob',
                                                                                            'SeqCadast' => $seqCadast,
                                                                                            'SigOaAtuacaoRosacr' => $siglaOrganismoAnterior,//R+C
                                                                                            'SigOaAtuacaoTom' => '', 
                                                                                    );

                                                            // Chamada do método
                                                            $return4 = $ws4->callMethod($method4, $params4, 'lucianob');
                                                        } 
                                                        */
                                                        // Pegar id da Função atual e remover na tabela funcao_usuario
                                                        if($registroAconteceu==1&&$ERRO1==false&&$conseguiuInformacoesDoOficial==1)
                                                        {    
                                                            $f = new funcao();

                                                            //echo "<pre>";print_r($descFuncao);

                                                            $f->setVinculoExterno($funcao);
                                                            $retorno77 = $f->buscaVinculoExterno();
                                                            if($retorno77)
                                                            {    
                                                                foreach ($retorno77 as $vetor2)
                                                                {
                                                                        $fu = new funcaoUsuario();
                                                                        $fu->setFk_seq_cadast($seqCadastOriginal);
                                                                        $fu->setFk_idFuncao($vetor2['idFuncao']);
                                                                        if($fu->verificaSeJaExiste())
                                                                        {
                                                                                if($fu->remove(date("Y-m-d")))
                                                                                {
                                                                                    echo " - Também foi removida as funções do SOA para este usuário";
                                                                                }    

                                                                        }
                                                                }
                                                            }    
                                                        }    
                                                    }
                                            }
                                    }
                            }

                        }else{
                            echo "<br><br>Webservice parece não estar de pé, ou REALMENTE não tem nenhuma função cadastrada neste OA<br><br>";
                        }
                    }
                }
            }
        }    

        if($siglaOA!=""&&$continuarTentandoBaixa==false)
        {   
            $totalDeOas = (int)count($resultaOa); 
            $totalDeOasJaCadastrado = (int) count($oaCadastrado);
            $faltaOa = $totalDeOas-$totalDeOasJaCadastrado;
            //Fazer registro de região não cadastrada como "cadastrada"
            $baixaAutomaticaFuncoesUsuario->setRegiao(substr($organismoNaoCadastrado,0,3));
            $baixaAutomaticaFuncoesUsuario->setSiglaOA($organismoNaoCadastrado);
            if($baixaAutomaticaFuncoesUsuario->cadastraBaixaAutomaticaFuncoesUsuario())
            {
                echo "<br><br>=====Terminou de dar baixa no organismo: ".$siglaOA."====<br><br>=====Falta ".$faltaOa." organismo(s) para terminar a baixa=====";
            } 
        }else{
            if($siglaOA!="")
            {    
                echo "<br><br>=====Continuar tentando baixa em ".$siglaOA."====";
            }else{
                echo "<br><br>=====Terminou baixa em TODOS OS ORGANISMOS!====";
            }
        }   
    }
}else{
    echo "<br><br>====Hoje não foi um dia agendado para ter baixa automática====";
}    



?>	