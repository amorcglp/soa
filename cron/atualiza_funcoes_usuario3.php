<?php 

ini_set('max_execution_time', 0);
session_start();
//error_reporting(E_ALL);
ini_set("display_errors", 1 );

$hoje = date("Y-m-d")."T00:00:00";

/*
if(realpath('/var/www/soa/lib/functions.php')){
	include_once '/var/www/soa/lib/functions.php';
}else{*/
        include_once('../lib/functions.php');
//}
/*
if(realpath('/var/www/soa/model/organismoClass.php')){
	include_once '/var/www/soa/model/organismoClass.php';
}else{*/
	include_once('../model/organismoClass.php');
//}
/*
if(realpath('/var/www/soa/model/usuarioClass.php')){
    include_once '/var/www/soa/model/usuarioClass.php';
}else{*/
    include_once('../model/usuarioClass.php');
//}
/*
if(realpath('/var/www/soa/model/funcaoUsuarioClass.php')){
	include_once '/var/www/soa/model/funcaoUsuarioClass.php';
}else{*/
        include_once('../model/funcaoUsuarioClass.php');
//}
/*
if(realpath('/var/www/soa/model/funcaoClass.php')){
	include_once '/var/www/soa/model/funcaoClass.php';
}else{*/
        include_once('../model/funcaoClass.php');
//}
/*
if(realpath('/var/www/soa/webservice/wsInovad.php')){
	require_once '/var/www/soa/webservice/wsInovad.php';
}else{*/
        require_once '../webservice/wsInovad.php';
//}
/*
$vars = array('seq_cadast' => '[6930]');
$resposta = json_decode(json_encode(restAmorc("membros/oficiais",$vars)),true);
$obj = json_decode(json_encode($resposta),true);
echo "<pre>";print_r($obj);exit();
*/
$funcaoUsuario = new FuncaoUsuario();

$total=0;//Contador de relatórios NÃO entregues

//Procurar mestres cadastrados no sistema para este organismo
$usuario = new Usuario();

$start = isset($_GET['start'])?$_GET['start']:null;
$limit = isset($_GET['limit'])?$_GET['limit']:null;


$resultadoUsu = $usuario->listaUsuario(null,null,null,null,null,null,1,null,null,null,$start,$limit);//Todos os usuários ativos
$m=1;
if($resultadoUsu)
{
        foreach($resultadoUsu as $vetorUsu)
        {
            $temFuncao = false;
            
                $seqCadast = $vetorUsu['seqCadast'];
                //$seqCadast=88969;
                echo "=====================<br>";
                echo date("H:i:s")."-";
                echo $seqCadast."-";
                
                $array = [0 => $seqCadast];
                 $teste = '';
                foreach ($array as $key => $value){
                    if($teste != ''){
                        $teste = $teste.', '.$value;
                    }else{
                        $teste = $value;
                    }
                }
                
                //Buscar Nome do Usuário vindo do ORCZ
                $vars = array('seq_cadast' => $teste);
                $resposta = json_decode(json_encode(restAmorc("membros",$vars)),true);;
                $obj2 = json_decode(json_encode($resposta),true);
                //echo "<pre>";print_r($obj2);exit();
                if(isset($obj2['data'][0]['nom_client']))
                {    
                    $nomeOficial = $obj2['data'][0]['nom_client'];
                    echo $nomeOficial;

                    //Limpar funções do usuário
                    $funcaoUsuario = new FuncaoUsuario();
                    $funcaoUsuario->setFk_seq_cadast($seqCadast);
                    $funcaoUsuario->removePorUsuario();
                    echo "-Apagou Funções-";
                }    
                //Buscar Funções
                $vars = array('seq_cadast' => '['.$teste.']');
                $resposta = json_decode(json_encode(restAmorc("membros/oficiais",$vars)),true);
                $obj = json_decode(json_encode($resposta),true);
                //echo "<pre>";print_r($obj);exit();
                    
                if(isset($obj['data'][0]))
                {   
                    echo "Tem Funções-";
                    
                    $arrFuncoes = $obj['data'][0]['funcoes'];
                    //echo "<pre>";print_r($arrFuncoes);exit();

                    /**
                     * Priorização de lotação para funções administrativas
                     */
                    //Varrer funções priorizando lotação para funções administrativas

                    $siglaOrganismoLotacao="";

                    foreach($arrFuncoes as $vetor10)
                    {

                        if($vetor10['seq_tipo_funcao_oficial']==100)//Se tiver dignitária priorizar lotação
                        {
                            if($vetor10['dat_saida']==0&&isset($vetor10['funcao'])&&isset($vetor10['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                            {
                                $dataTerminoMandato = $vetor10['dat_termin_mandat'];
                                $codFuncao = $vetor10['funcao']['seq_funcao'];
                                $tipoCargo = $vetor10['funcao']['seq_tipo_funcao_oficial'];
                                $dataEntrada = $vetor10['dat_entrad'];
                                $desFuncao = substr($vetor10['funcao']['des_funcao'],0,3);

                                $siglaOrganismoWS = $vetor10['oa']['sig_orgafi'];

                                $data1_inteiro = strtotime("+40 days",strtotime(substr($dataTerminoMandato,0,10)));
                                //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                $data2_inteiro = strtotime(date("Y-m-d"));

                                //if($desFuncao!="EX-")
                                //{
                                    //echo "entrou diferente de ex"."<br>";
                                    if($data1_inteiro>=$data2_inteiro)
                                    {	
                                        //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                        //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                        if($vetor10['dat_saida']==null)
                                        { 
                                            if($vetor10['funcao']['ide_tipo_oa']=='O')
                                            {    
                                                //Pegar a sigla do Organismo
                                                $siglaOrganismoLotacao = $vetor10['oa']['sig_orgafi'];
                                            }
                                        }
                                    }
                                //}
                            }
                        }    
                    }
                    //Se não conseguiu encontrar uma lotação para função dignitária pegar administrativa
                    if($siglaOrganismoLotacao=="")
                    {
                        foreach($arrFuncoes as $vetor11)
                        {
                            //echo "<pre>";print_r($vetor);exit();
                            if($vetor11['seq_tipo_funcao_oficial']==200)//Se tiver administrativa priorizar lotação
                            {
                                if($vetor11['dat_saida']==0&&isset($vetor11['funcao'])&&isset($vetor11['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                                {
                                    $dataTerminoMandato = $vetor11['dat_termin_mandat'];
                                    $codFuncao = $vetor11['funcao']['seq_funcao'];
                                    $tipoCargo = $vetor11['funcao']['seq_tipo_funcao_oficial'];
                                    $dataEntrada = $vetor11['dat_entrad'];
                                    $desFuncao = substr($vetor11['funcao']['des_funcao'],0,3);

                                    $siglaOrganismoWS = $vetor11['oa']['sig_orgafi'];

                                    $data1_inteiro = strtotime("+40 days",strtotime(substr($dataTerminoMandato,0,10)));
                                    //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                    $data2_inteiro = strtotime(date("Y-m-d"));

                                    //if($desFuncao!="EX-")
                                    //{
                                        //echo "entrou diferente de ex"."<br>";
                                        if($data1_inteiro>=$data2_inteiro)
                                        {	
                                            //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                            //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                            if($vetor11['dat_saida']==null)
                                            { 
                                                if($vetor11['funcao']['ide_tipo_oa']=='O')
                                                {
                                                    $siglaOrganismoLotacao = $vetor11['oa']['sig_orgafi'];
                                                }
                                            }
                                        }
                                    //}
                                }
                            }    
                        }
                    }
                    //Se não conseguiu encontrar uma lotação para função administrativa então pegar de qualquer outro tipo
                    if($siglaOrganismoLotacao=="")
                    {
                        foreach($arrFuncoes as $vetor12)
                        {
                            if($vetor12['dat_saida']==0&&isset($vetor12['funcao'])&&isset($vetor12['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                            {
                                $dataTerminoMandato = $vetor12['dat_termin_mandat'];
                                $codFuncao = $vetor12['funcao']['seq_funcao'];
                                $tipoCargo = $vetor12['funcao']['seq_tipo_funcao_oficial'];
                                $dataEntrada = $vetor12['dat_entrad'];
                                $desFuncao = substr($vetor12['funcao']['des_funcao'],0,3);

                                $siglaOrganismoWS = $vetor12['oa']['sig_orgafi'];

                                $data1_inteiro = strtotime("+40 days",strtotime(substr($dataTerminoMandato,0,10)));
                                //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                $data2_inteiro = strtotime(date("Y-m-d"));

                                //if($desFuncao!="EX-")
                                //{
                                    //echo "entrou diferente de ex"."<br>";
                                    if($data1_inteiro>=$data2_inteiro)
                                    {	
                                        //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                        //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                        if($vetor12['dat_saida']==null)
                                        {    			

                                            if($vetor12['funcao']['ide_tipo_oa']=='O')
                                            {
                                                $siglaOrganismoLotacao = $vetor12['oa']['sig_orgafi'];
                                            }
                                        }
                                    }
                                //}
                            }    
                        }
                    }

                    //echo "<br>Sigla atualizada:".$siglaOrganismoLotacao;exit();

                    foreach($arrFuncoes as $vetor13)
                    {
                            //echo $vetor['funcao']['des_funcao']."-".$vetor['dat_saida']."<br>";
                            if($vetor13['dat_saida']==0&&isset($vetor13['funcao'])&&isset($vetor13['oa']))//Verificar apenas campos que a data de saida não foi cadastrada
                            {
                                if($vetor13['funcao']['ide_tipo_oa']=="O") {//R+Ccccccccccccccccccccccccccccccccccccccccccccccccc veja aqui óoooo
                                    //echo "entrou data nula"."<br>";
                                    $dataTerminoMandato = $vetor13['dat_termin_mandat'];
                                    $codFuncao = $vetor13['funcao']['seq_funcao'];
                                    $tipoCargo = $vetor13['funcao']['seq_tipo_funcao_oficial'];
                                    $dataEntrada = $vetor13['dat_entrad'];
                                    $desFuncao = substr($vetor13['funcao']['des_funcao'], 0, 3);

                                    $siglaOrganismoWS = $vetor13['oa']['sig_orgafi'];

                                    $data1_inteiro = strtotime("+40 days", strtotime(substr($dataTerminoMandato, 0, 10)));
                                    //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                    $data2_inteiro = strtotime(date("Y-m-d"));

                                    // if($desFuncao!="EX-")
                                    // {
                                    //echo "entrou diferente de ex"."<br>";
                                    if ($data1_inteiro >= $data2_inteiro) {
                                        //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                        //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                        if ($vetor13['dat_saida'] == null) {
                                            $f = new funcao();
                                            $f->setVinculoExterno($codFuncao);
                                            $retorno7 = $f->buscaVinculoExterno();
                                            if ($retorno7) {
                                                foreach ($retorno7 as $vetor7) {
                                                    $fu = new funcaoUsuario();
                                                    $fu->setFk_seq_cadast($seqCadast);
                                                    $fu->setFk_idFuncao($vetor7['idFuncao']);
                                                    $fu->setDataInicioMandato(substr($dataEntrada, 0, 10));
                                                    $fu->setDataFimMandato(substr($dataTerminoMandato, 0, 10));
                                                    $fu->setSiglaOA($siglaOrganismoWS);
                                                    if (!$fu->verificaSeJaExiste()) {
                                                        if ($fu->cadastra()) {
                                                            $temFuncao = true;
                                                            echo $m . "-" . $nomeOficial . " ->Funcao: " . $vetor7['nomeFuncao'] . " => Lotação: " . $siglaOrganismoLotacao;
                                                            echo "<br>=====================<br>";
                                                            $m++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //}
                                }
                            }
                    }
                    //Atualizado a lotação do usuario
                    if($siglaOrganismoLotacao!="")
                    {  
                        if($temFuncao)
                        {    
                            $usua = new Usuario();
                            $usua->setSeqCadast($seqCadast);
                            $usua->setSiglaOA($siglaOrganismoLotacao);
                            $resp = $usua->alteraLotacaoUsuario();
                        }else{
                            $usua = new Usuario();
                            $usua->setSeqCadast($seqCadast);
                            $usua->setSiglaOA("PR113");
                            $resp = $usua->alteraLotacaoUsuario();
                        }
                    }
                    //Se tem função então ativar status do usuário
                    if($temFuncao)
                    {
                        $usua = new Usuario();
                        $usua->setSeqCadast($seqCadast);
                        $usua->setStatusUsuario(0);
                        $resp = $usua->alteraStatusUsuario();
                    }    
                }    
                
                //exit();
        }
}

echo "<br><br><b>Cron executada com sucesso!</b>";

?>	
