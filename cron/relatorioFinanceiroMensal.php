<?php

if ($argc < 2) {
    echo "Primeiro argumento: ID do Organismo.\n";
    echo "Segundo argumento: Ano\n";
    echo "Terceiro argumento: Mês\n";
    exit(1);
}
$idOrganismoAfiliado = $argv[1];
$ano = $argv[2];
$mes = $argv[3];

include_once('../model/saldoInicialClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

include_once('../lib/functions.php');


echo "Financeiro ID: " . $idOrganismoAfiliado . "\n";
$inicio     = time();


$si = new saldoInicial();
$r = new Recebimento();
$d = new Despesa();

$dataSaldoInicial                   = $si->getDataCompletaSaldoInicial($idOrganismoAfiliado);
$saldoInicial                       = $si->getValorSaldoInicial($idOrganismoAfiliado);
$totalRecebimentoAteMesAnterior     = $r->totalRecebimentosAteMesAnterior($idOrganismoAfiliado, $dataSaldoInicial, $ano, $mes);
$totalDespesaAteMesAnterior         = $d->totalDespesasAteMesAnterior($idOrganismoAfiliado, $dataSaldoInicial, $ano, $mes);

echo "Saldo Inicial: ". $saldoInicial ."\n";
echo "Recebimentos: ". $totalRecebimentoAteMesAnterior ."\n";
echo "Despesas: ". $totalDespesaAteMesAnterior ."\n\n";

echo "Data Saldo Inicial: ". $dataSaldoInicial ."\n";
echo "Data Mês Anterior: " . $ano . "-" . $mes . "-" . date("t", mktime(0,0,0,$mes,'01',$ano)) . "\n\n";

echo "Saldo do Mês Anterior: ". (($totalRecebimentoAteMesAnterior - $totalDespesaAteMesAnterior) + $saldoInicial) ."\n\n";

$fim        = time();
$tempo      = $fim - $inicio;
echo "[" . $idOrganismoAfiliado . "] Tempo de processamento: " . (number_format($tempo/60, 2)) . "m (" . $tempo . "s) \n\n";



