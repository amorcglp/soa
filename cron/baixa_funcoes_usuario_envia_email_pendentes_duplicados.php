<meta charset="utf-8">
<?php 

$totalPendentes =0;
$totalDuplicados=0;

include_once('../model/baixaAutomaticaFuncoesUsuarioClass.php');
include_once('../lib/phpmailer/class.phpmailer.php');

//Selecionar uma região que não foi cadastrada hoje
$baixaAutomaticaFuncoesUsuario = new baixaAutomaticaFuncoesUsuario();

//Verifica se foi enviado e-mail hoje
$hoje=date("Y-m-d");
$resultadoEmail = $baixaAutomaticaFuncoesUsuario->selecionaEnvioEmailPendentesDuplicados($hoje);
if(!$resultadoEmail)
{    

    //Contar se existem funções com baixa pendente
    $resultado = $baixaAutomaticaFuncoesUsuario->selecionaNotLog();
    if($resultado)
    {
        $totalPendentes = count($resultado);
    }    
    //Contar se existem funções duplicadas que precisam ser higienizadas
    $resultado2 = $baixaAutomaticaFuncoesUsuario->selecionaDuplicados();
    if($resultado2)
    {
        $totalDuplicados = count($resultado2);
    }
    echo "Relatório";
    echo "<br>==================================================================";
    echo "<br>Total de Funções com Baixas Pendentes não verificadas: ".$totalPendentes;
    echo "<br>Total de Funções Duplicadas não verificadas: ".$totalDuplicados;


    //Enviar e-mail com relatório para Eleda e Carol 5 dias após ter rodado a baixa
    $texto = '<div>
                                            <div dir="ltr">
                                                    <table class="ecxbody-wrap"
                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
                                                            <tbody>
                                                                    <tr
                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                            <td
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                                                    valign="top"></td>
                                                                            <td class="ecxcontainer" width="600"
                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
                                                                                    valign="top">
                                                                                    <div class="ecxcontent"
                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
                                                                                            <table class="ecxmain" width="100%" cellpadding="0"
                                                                                                    cellspacing="0"
                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
                                                                                                    <tbody>
                                                                                                            <tr
                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                    <td class="ecxcontent-wrap"
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
                                                                                                                            valign="top"><img src="http://i.imgur.com/FKg7aai.png"
                                                                                                                            style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
                                                                                                                            alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
                                                                                                                    <br>
                                                                                                                    <br>
                                                                                                                    <br>
                                                                                                                            <div style="text-align: left;"></div>
                                                                                                                            <table width="100%" cellpadding="0" cellspacing="0"
                                                                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                    <tbody>
                                                                                                                                            <tr
                                                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                    <td class="ecxcontent-block"
                                                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
                                                                                                                                                            valign="top">Estimados <b>Colaboradores do Setor de Organismos Afiliados</b>,</td>
                                                                                                                                            </tr>
                                                                                                                                            <tr
                                                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                    <td class="ecxcontent-block"
                                                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                            valign="top">Saudações Fraternais! </td>
                                                                                                                                            </tr>
                                                                                                                                            <tr
                                                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                    <td class="ecxcontent-block"
                                                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                            valign="top">';
                                                                                                    if($totalPendentes>0)
                                                                                                    {
                                                                                                            $texto .= '<br>Total de Funções com Baixas Pendentes não verificadas no SOA: '.$totalPendentes;
                                                                                                    }
                                                                                                    if($totalPendentes>0)
                                                                                                    {
                                                                                                            $texto .= '<br>Total de Funções Duplicadas não verificadas no SOA: '.$totalDuplicados;
                                                                                                    }
                                                                                                    if($totalPendentes==0&&$totalDuplicados==0)
                                                                                                    {
                                                                                                            $texto .= '<br>Parabéns ao Setor, pois não existem baixas pendentes ou funções duplicadas para serem verificadas!'.$totalDuplicados;
                                                                                                    }
                                                                                                    $texto .= '				</td>
                                                                                                                                            </tr>
                                                                                                                                            <tr
                                                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                    <td class="ecxcontent-block"
                                                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                            valign="top"><font color="red">Essa é uma mensagem automática, não há necessidade de resposta!</font></td>
                                                                                                                                            </tr>
                                                                                                                                            <tr
                                                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                    <td class="ecxcontent-block"
                                                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                            valign="top">Em caso de dúvida, por favor entre em contato: ti@amorc.org.br</td>
                                                                                                                                            </tr>
                                                                                                                                            <tr
                                                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                                                    <td class="ecxcontent-block"
                                                                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
                                                                                                                                                            valign="top"> 
                                                                                                                                                            Ordem Rosacruz, AMORC – Grande Loja da Jurisdição de Língua Portuguesa<br>
                                                                                                                                            Rua Nicarágua, 2620, Curitiba – Paraná<br>
                                                                                                                                            Telefone: (+55) 041- 3351-3000</td>
                                                                                                                                            </tr>
                                                                                                                                    </tbody>
                                                                                                                            </table></td>
                                                                                                            </tr>
                                                                                                    </tbody>
                                                                                            </table>
                                                                                            <div class="footer"
                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
                                                                                                    <div style="text-align: left;"></div>
                                                                                                    <table width="100%"
                                                                                                            style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                            <tbody>
                                                                                                                    <tr
                                                                                                                            style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
                                                                                                                            <td class="ecxaligncenter ecxcontent-block"
                                                                                                                                    style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
                                                                                                                                    align="center" valign="top">AMORC-GLP '.date('Y').'</td>
                                                                                                                    </tr>
                                                                                                            </tbody>
                                                                                                    </table>
                                                                                            </div>
                                                                                    </div>
                                                                                    <div style="text-align: left;"></div></td>
                                                                            <td
                                                                                    style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
                                                                                    valign="top"></td>
                                                                    </tr>
                                                            </tbody>
                                                    </table>
                                                    </div>
                                            </div>';
                                            echo $texto;

                                            $servidor="smtp.office365.com";
                                            $porta="587";
                                            $usuario="noresponseglp@amorc.org.br";
                                            $senha="@w2xpglp";

                                            //echo $senha;
                                            #Disparar email
                                            $mail = new PHPMailer();
                                            $mail->isSMTP();
                                            $mail->SMTPSecure = "tls";
                                            $mail->SMTPAuth = true;
                                            $mail->Host = $servidor;	// SMTP utilizado
                                            $mail->Port = $porta;
                                            $mail->Username = $usuario;
                                            $mail->Password = $senha;
                                            $mail->From = 'noresponseglp@amorc.org.br';
                                            $mail->AddReplyTo("atendimento@amorc.org.br","Antiga e Mística Ordem Rosacruz - AMORC");
                                            $mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
                                            //$mail->AddAddress("luis.fernando@amorc.org.br");
                                            $mail->AddAddress("tatiane.jesus@amorc.org.br");
                                            //$mail->AddAddress("lucianob@amorc.org.br");
                                            //$mail->AddAddress("samufcaldas@hotmail.com.br");
                                            //$mail->AddAddress("samuel.caldas@hotmail.com.br");
                                            //$mail->AddAddress("luciano.bastos@gmail.com");
                                            $mail->AddAddress("braz@amorc.org.br");
                                            $mail->AddAddress("amigoeterno10@hotmail.com");
                                            //$mail->AddAddress($vetorUsu['emailUsuario']);
                                            $mail->isHTML(true);
                                            $mail->CharSet = 'utf-8';
                                            //$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
                                            $mail->Subject = 'Baixas nas Pendentes nas Funções e Funções Duplicadas no ORCZ - AMORC';
                                            $mail->Body = $texto;
                                            $mail->AltBody = strip_tags($texto); 
                                            //echo $texto;
                                            $enviado = $mail->Send();
                                            if($enviado)
                                            {
                                                 //Guardar log de que o email foi enviado
                                                if($baixaAutomaticaFuncoesUsuario->cadastraEnvioEmailPendentesDuplicados())
                                                {    
                                                    echo "<br>==================================================================";
                                                    echo "<br><b>Email enviado com sucesso!</b>";
                                                }
                                                //exit();
                                            }

}else{
    echo "<br>==================================================================";
    echo "<br><b>Hoje já foi enviado e-mail!</b>";
}
?>	