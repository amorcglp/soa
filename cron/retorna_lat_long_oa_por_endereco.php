<?php 
require_once '../vendor/autoload.php';
use JeroenDesloovere\Geolocation\Geolocation;

$endereco = ($_REQUEST['endereco'])?$_REQUEST['endereco']:null;
$numero = ($_REQUEST['numero'])?$_REQUEST['numero']:null;;
//$cidade = ($_GET['cidade'])?$_GET['cidade']:null;
$cep = ($_REQUEST['cep'])?$_REQUEST['cep']:null;
$siglaPais = ($_REQUEST['siglaPais'])?$_REQUEST['siglaPais']:null;
$fkcidade = ($_REQUEST['fkcidade'])?$_REQUEST['fkcidade']:null;

   
require_once ("../model/cidadeClass.php");
$cid = new Cidade();
$cidade = $cid->retornaCidadeTexto($fkcidade);
//echo $fkcidade."-".$cidade;
$geo = new Geolocation();
$arr = $geo->getCoordinates(
    $endereco,
    $numero,
    $cidade,
    $cep,
    $siglaPais
);
//echo "<pre>";print_r($arr);
if($arr['latitude']!="")
{    
    echo '{"status":"endereco","latitude":"'.$arr['latitude'].'","longitude":"'.$arr['longitude'].'"}';
}else{
    $geo = new Geolocation();
    $arr = $geo->getCoordinates(
        NULL,
        NULL,
        $cidade,
        $cep,
        $siglaPais
    );
    if($arr['latitude']!="")
    { 
        echo '{"status":"cep","latitude":"'.$arr['latitude'].'","longitude":"'.$arr['longitude'].'"}';             
    }else{
        $geo = new Geolocation();
        $arr = $geo->getCoordinates(
            NULL,
            NULL,
            $cidade,
            NULL,
            $siglaPais
        );
        echo '{"status":"cidade","latitude":"'.$arr['latitude'].'","longitude":"'.$arr['longitude'].'"}';             
    }

}

           