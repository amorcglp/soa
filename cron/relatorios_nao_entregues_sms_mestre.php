<?php 
set_time_limit(0);
//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );
$server = 138;
$mesAtual 				= date('m');
$anoAtual 				= date('Y');

//Trimestre atual
if(date('m')==1||date('m')==2||date('m')==3)
{
    $trimestreAtual=1;
}
if(date('m')==4||date('m')==5||date('m')==6)
{
    $trimestreAtual=2;
}
if(date('m')==7||date('m')==8||date('m')==9)
{
    $trimestreAtual=3;
}
if(date('m')==10||date('m')==11||date('m')==12)
{
    $trimestreAtual=4;
}

include_once('../lib/functions.php');
include_once('../lib/phpmailer/class.phpmailer.php');
include_once('../model/organismoClass.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/ataReuniaoMensalAssinadaClass.php');
include_once('../model/ataPosseAssinadaClass.php');
include_once('../model/membrosRosacruzesAtivosClass.php');
include_once('../model/relatorioFinanceiroMensalClass.php');
include_once('../model/relatorioFinanceiroAnualClass.php');
include_once('../model/imovelControleClass.php');
include_once('../model/usuarioClass.php');
include_once('../model/notificacaoRelatoriosNaoEntreguesSmsMestreClass.php');

$organismo = new organismo();

$resultadoOrg = $organismo->listaOrganismo();

if($resultadoOrg)
{
	foreach($resultadoOrg as $vetorOrg)
	{
		switch ($vetorOrg['classificacaoOrganismoAfiliado']) {
            case 1:
                $classificacao = "Loja";
                break;
            case 2:
                $classificacao = "Pronaos";
                break;
            case 3:
                $classificacao = "Capítulo";
                break;
            case 4:
                $classificacao = "Heptada";
                break;
            case 5:
                $classificacao = "Atrium";
                break;
        }
        switch ($vetorOrg['tipoOrganismoAfiliado']) {
            case 1:
                $tipo = "R+C";
                break;
            case 2:
                $tipo = "TOM";
                break;
        }

        $nomeOrganismo 			= $vetorOrg["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetorOrg["nomeOrganismoAfiliado"];
		$idOrganismoAfiliado 	= $vetorOrg['idOrganismoAfiliado'];
		$siglaOrganismo			= $vetorOrg['siglaOrganismoAfiliado'];
		$total=0;//Contador de relatórios NÃO entregues
		
		//Encontrar Data Inicial com base no saldo inicial
		
		$si = new saldoInicial();
		$mesSaldoInicial = "01";
		$anoSaldoInicial = "2016";
                $trimestreSaldoInicial="1";
                $temSaldoInicial = 1;
		$ativacao=0;
		$resultado = $si->listaSaldoInicial($idOrganismoAfiliado);
		if($resultado)
		{
			$ativacao = count($resultado);
			foreach($resultado as $vetor)
			{
				$mesSaldoInicial = substr($vetor['dataSaldoInicial'],5,2);
				$anoSaldoInicial = substr($vetor['dataSaldoInicial'],0,4);
                                
                                //Trimestre atual
                                if($mesSaldoInicial==1||$mesSaldoInicial==2||$mesSaldoInicial==3)
                                {
                                    $trimestreSaldoInicial=1;
                                }
                                if($mesSaldoInicial==4||$mesSaldoInicial==5||$mesSaldoInicial==6)
                                {
                                    $trimestreSaldoInicial=2;
                                }
                                if($mesSaldoInicial==7||$mesSaldoInicial==8||$mesSaldoInicial==9)
                                {
                                    $trimestreSaldoInicial=3;
                                }
                                if($mesSaldoInicial==10||$mesSaldoInicial==11||$mesSaldoInicial==12)
                                {
                                    $trimestreSaldoInicial=4;
                                }
			}
		}else{
                    $temSaldoInicial=0;
                }
		
		/**
		 * Entrega das Atas de Reunião Mensal Assinadas
		 */
		
		
		$arma = new ataReuniaoMensalAssinada();
		
		$mes = (int) $mesSaldoInicial;
		$ano = (int) $anoSaldoInicial;
		//echo "mes------>".$mes;
		//echo "ano------>".$ano;
		$totalAtaReuniaoMensal=0;
		$arrMesesNaoEntreguesAtaReuniaoMensal=array();
		while($mes<$mesAtual&&$ano<=$anoAtual)
		{
			$resultado = $arma->listaAtaReuniaoMensalAssinada(null,$mes,$ano,$idOrganismoAfiliado);
			if(!$resultado)
			{
				$arrMesesNaoEntreguesAtaReuniaoMensal[]=mesExtensoPortugues($mes)."/".$ano;
				$total++;
				$totalAtaReuniaoMensal++;
			}
			if($mes==12&&$ano<=$anoAtual)
			{
				$ano++;
				$mes=0;
			}
			$mes++;
		}
		
		/**
		 * Entrega das Atas de Posse Assinadas
		 */
		
		
		$apa = new ataPosseAssinada();
		
		//$mes = (int) $mesSaldoInicial;
		$ano = (int) $anoSaldoInicial;
		//echo "mes------>".$mes;
		//echo "ano------>".$ano;
		$totalAtaPosse=0;
		$arrAnosNaoEntreguesAtaPosse=array();
		while($ano<=$anoAtual)
		{
			$resultado = $apa->listaAtaPosseAssinada(null,null,$ano,$idOrganismoAfiliado);
			if(!$resultado)
			{
				$arrAnosNaoEntreguesAtaPosse[]=$ano;
				$total++;
				$totalAtaPosse++;
			}
			
			$ano++;
			
		}
		
		/**
		 * Entrega do Relatório de Membros Ativos
		 */

		$mra	= new MembrosRosacruzesAtivos();
		
		$mes = (int) $mesSaldoInicial;
		$ano = (int) $anoSaldoInicial;
		//echo "mes------>".$mes;
		//echo "ano------>".$ano;
		$totalRelatorioMembrosAtivos=0;
		$arrMesesNaoEntreguesRelatorioMembrosAtivos=array();
		while($mes<$mesAtual&&$ano<=$anoAtual)
		{
			$mResultado	= $mra->retornaMembrosRosacruzesAtivos($mes,$ano,$idOrganismoAfiliado);
			if($mResultado['numeroAtualMembrosAtivos']==0)
			{
				$arrMesesNaoEntreguesAtaReuniaoMensal[]=mesExtensoPortugues($mes)."/".$ano;
				$total++;
				$totalAtaReuniaoMensal++;
			}
			if($mes==12&&$ano<=$anoAtual)
			{
				$ano++;
				$mes=0;
			}
			$mes++;
		}
		
		/**
		 * Entrega do Relatório Financeiro Mensal
		 */
		
		
		$rfm = new RelatorioFinanceiroMensal();
		
		$mes = (int) $mesSaldoInicial;
		$ano = (int) $anoSaldoInicial;
		//echo "mes------>".$mes;
		//echo "ano------>".$ano;
		$totalRelatorioFinanceiroMensal=0;
		$arrMesesNaoEntreguesRelatorioFinanceiroMensal=array();
		while($mes<$mesAtual&&$ano<=$anoAtual)
		{
			$resultado = $rfm->listaRelatorioFinanceiroMensal($mes,$ano,$idOrganismoAfiliado);
			if(!$resultado)
			{
				$arrMesesNaoEntreguesRelatorioFinanceiroMensal[]=mesExtensoPortugues($mes)."/".$ano;
				$total++;
				$totalRelatorioFinanceiroMensal++;
			}
			if($mes==12&&$ano<=$anoAtual)
			{
				$ano++;
				$mes=0;
			}
			$mes++;
		}
		
		/**
		 * Entrega do Relatório Financeiro Anual
		 */
		
		
		$rfa = new RelatorioFinanceiroAnual();
		
		//$mes = (int) $mesSaldoInicial;
		$ano = (int) $anoSaldoInicial;
		//echo "mes------>".$mes;
		//echo "ano------>".$ano;
		$totalRelatorioFinanceiroAnual=0;
		$arrAnosNaoEntreguesRelatorioFinanceiroAnual=array();
		while($ano<$anoAtual)
		{
			$resultado = $rfa->listaRelatorioFinanceiroAnual($ano,$idOrganismoAfiliado);
			if(!$resultado)
			{
				$arrAnosNaoEntreguesRelatorioFinanceiroAnual[]=$ano;
				$total++;
				$totalRelatorioFinanceiroAnual++;
			}
			
			$ano++;
			
			
		}
		
		/**
		 * Entrega do Relatório de Imóveis Anual
		 */
		
		
		$ic = new imovelControle();
		
		//$mes = (int) $mesSaldoInicial;
		$ano = (int) $anoSaldoInicial;
		//echo "mes------>".$mes;
		//echo "ano------>".$ano;
		$totalRelatorioImovelAnual=0;
		$arrAnosNaoEntreguesRelatorioImovel=array();
		while($ano<=$anoAtual)
		{
			$resultado = $ic->listaImovelControle($idOrganismoAfiliado,$ano);
			if(!$resultado)
			{
					$arrAnosNaoEntreguesRelatorioImovel[]=$ano;
					$total++;
					$totalRelatorioImovelAnual++;
			}
			
			$ano++;
			
		}
                
                //Verificar se oa tem Mestre da Classe dos Artesãos
                $ocultar_json=1;
                $naoAtuantes='N';
                $atuantes='S';
                $siglaOA=$siglaOrganismo;
                $seqFuncao='315';//Mestre da Classe dos Artesãos
                include '../js/ajax/retornaFuncaoMembro.php';
                $obj = json_decode(json_encode($return),true);
                $temMestreClasseArtesaos=0;
                $arrMesesNaoEntreguesRelatorioClasseArtesaos=array();
                if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                {
                    $temMestreClasseArtesaos=1;

                    /**
                    * Entrega dos Relatórios da Classe dos Artesãos assinados
                    */

                   include_once('../model/relatorioClasseArtesaosAssinadoClass.php');
                   $rcaa = new relatorioClasseArtesaosAssinado();

                   $mes = (int) $mesSaldoInicial;
                   $ano = (int) $anoSaldoInicial;
                   //echo "mes------>".$mes;
                   //echo "ano------>".$ano;
                   $totalRelatorioClasseArtesaosMensal=0;
                   while($mes<$mesAtual&&$ano<=$anoAtual)
                   {
                           $resultado = $rcaa->listaRelatorioClasseArtesaosAssinado(null,$idOrganismoAfiliado,$mes,$ano);
                           if(!$resultado&&$mes!=1&&$mes!=12)//Dispensar de eles entregarem relatorio em janeiro e dezembro
                           {
                                   $arrMesesNaoEntreguesRelatorioClasseArtesaos[]=mesExtensoPortugues($mes)."/".$ano;
                                   $total++;
                                   $totalRelatorioClasseArtesaosMensal++;
                           }
                           if($mes==12&&$ano<=$anoAtual)
                           {
                                   $ano++;
                                   $mes=0;
                           }
                           $mes++;
                   }
                }

                /**
                 * Entrega do Relatório de Atividades Mensal
                 */

                include_once('../model/relatorioAtividadeMensalClass.php');
                $ram = new RelatorioAtividadeMensal();

                $mes = (int) $mesSaldoInicial;
                $ano = (int) $anoSaldoInicial;
                //echo "mes------>".$mes;
                //echo "ano------>".$ano;
                $totalRelatorioAtividadeMensal=0;
                $arrMesesNaoEntreguesRelatorioAtividadeMensal=array();
                while($mes<$mesAtual&&$ano<=$anoAtual)
                {
                        $resultado = $ram->listaRelatorioAtividadeMensal($mes,$ano,$idOrganismoAfiliado);
                        if(!$resultado)
                        {
                                $arrMesesNaoEntreguesRelatorioAtividadeMensal[]=mesExtensoPortugues($mes)."/".$ano;
                                $total++;
                                $totalRelatorioAtividadeMensal++;
                        }
                        if($mes==12&&$ano<=$anoAtual)
                        {
                                $ano++;
                                $mes=0;
                        }
                        $mes++;
                }

                //Verificar se oa tem Coordenadora de Columbas
                $ocultar_json=1;
                $naoAtuantes='N';
                $atuantes='S';
                $siglaOA=$siglaOrganismo;
                $seqFuncao='609';//Orientadora de Columbas
                include '../js/ajax/retornaFuncaoMembro.php';
                $obj = json_decode(json_encode($return),true);
                $temCoordenadoraColumbas=0;
                if(isset($obj['result'][0]['fields']['fArrayOficiais'][0]))
                {
                    $temCoordenadoraColumbas=1;

                    /**
                    * Entrega dos Relatórios das Columbas Assinados
                    */

                   include_once('../model/relatorioTrimestralColumbaAssinadoClass.php');
                   $r = new RelatorioTrimestralColumbaAssinado();

                   $mes = (int) $mesSaldoInicial;
                   $ano = (int) $anoSaldoInicial;
                   $trimestre = (int) $trimestreSaldoInicial;
                   //echo "mes------>".$mes;
                   //echo "ano------>".$ano;
                   $totalRelatorioTrimestralColumba=0;
                   $arrMesesNaoEntreguesRelatorioTrimestralColumba=array();
                   while($trimestre<=$trimestreAtual&&$ano<=$anoAtual)
                   {
                           $resultado = $r->listaRelatorioTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
                           if(!$resultado)
                           {
                                    $arrMesesNaoEntreguesRelatorioTrimestralColumba[]=$trimestre."/".$ano;
                                    $total++;
                                    $totalRelatorioTrimestralColumba++;
                           }
                           if($trimestre==4&&$ano<=$anoAtual)
                           {
                                   $ano++;
                                   $trimestre=0;
                           }
                           $trimestre++;
                   }

                   /**
                    * Entrega dos Relatórios Atividades das Columbas Assinados
                    */

                   include_once('../model/relatorioAtividadeTrimestralColumbaAssinadoClass.php');
                   $r = new RelatorioAtividadeTrimestralColumbaAssinado();

                   $mes = (int) $mesSaldoInicial;
                   $ano = (int) $anoSaldoInicial;
                   $trimestre = (int) $trimestreSaldoInicial;
                   //echo "mes------>".$mes;
                   //echo "ano------>".$ano;
                   $totalRelatorioAtividadeTrimestralColumba=0;
                   $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba=array();
                   while($trimestre<=$trimestreAtual&&$ano<=$anoAtual)
                   {
                           $resultado = $r->listaRelatorioAtividadeTrimestralColumbaAssinado($trimestre,$ano,$idOrganismoAfiliado);
                           if(!$resultado)
                           {
                                    $arrMesesNaoEntreguesRelatorioAtividadeTrimestralColumba[]=$trimestre."/".$ano;
                                    $total++;
                                    $totalRelatorioAtividadeTrimestralColumba++;
                           }
                           if($trimestre==4&&$ano<=$anoAtual)
                           {
                                   $ano++;
                                   $trimestre=0;
                           }
                           $trimestre++;
                   }
                }
		
		//Procurar mestres cadastrados no sistema para este organismo
		$usuario = new Usuario();
		
		$resultadoUsu = $usuario->listaUsuarioVinculoFuncao($siglaOrganismo,null,"201");//Mestre deste OA
		
		if($resultadoUsu)
		{
			foreach($resultadoUsu as $vetorUsu)
			{
				$arrNome = explode(" ",$vetorUsu['nomeUsuario']);
				$texto = "Ola ".$arrNome[0].",";
				//$texto = "Ola Fr Helio,";
				//$texto = "Ola Marcelo,";
				//$texto = "Ola Fr Rudy,";
				if($total>0)
				{
					$texto .= 'Nao foram entregues os relatorios deste mes no SOA. Enviamos para o seu email a descricao dos relatorios faltantes.';
				}else{
					if($ativacao==1)
					{
						$texto .= 'Todos os relatorios foram entregues no SOA deste mes. PARABENS a toda equipe pelo empenho!';
					}else{
						$texto .= 'Nao foram entregues os relatorios deste mes no SOA. Enviamos para o seu email a descricao dos relatorios faltantes.';
					}
				}
				if(substr($vetorUsu['celularUsuario'],1,1)==0)
				{
					$vetorUsu['celularUsuario'] = substr($vetorUsu['celularUsuario'],2,14);
				}	
                                //Definir ddi
                                switch(substr($siglaOrganismo,0,2))
                                {   
                                    case "PT":
                                        $ddi = "351";//Portugal
                                        break;
                                    case "RA":
                                        $ddi = "244";//Luanda
                                        break;
                                    default :
                                        $ddi = "55";//Brasil
                                        break;
                                }   
                                if($vetorUsu['celularUsuario']!="")
                                {    
                                    $celular = $ddi;
                                    $celular .= str_replace(" ","",str_replace("-","",str_replace(")","",str_replace("(","",$vetorUsu['celularUsuario']))));
                                    //$celular="554196516900";//Luciano
                                    //$celular="554197765203";//Samuel
                                    //$celular="554199330330";//Hélio
                                    //$celular="554191497185";//Marcelo
                                    //$celular="554199983233";//Rudy
                                    $celular="554197296119";//Braz
                                    echo "-->Tel:".$celular;

                                    /*** Mobi Pronto ***/
                                    $credencial= URLEncode("2D247BCD044C0753CB7AA7944B7270905980623A"); //**Credencial da Conta 40 caracteres
                                    $token= URLEncode("c5d5cf"); //**Token da Conta 6 caracteres
                                    $principal = URLEncode("aaa");  //* SEU CODIGO PARA CONTROLE, não colocar e-mail
                                    $auxuser = URLEncode("AUX_USER"); //* SEU CODIGO PARA CONTROLE, não colocar e-mail
                                    $mobile= URLEncode($celular); //* Numero do telefone  FORMATO: PAÍS+DDD(DOIS DÍGITOS)+NÚMERO
                                    $sendproj = URLEncode("N"); //* S = Envia o SenderId antes da mensagem , N = Não envia o SenderId
                                    $msg=$texto; // Mensagem
                                    //$msg="teste do braz";
                                    $msg=mb_convert_encoding($msg, "UTF-8"); // Converte a mensagem para não ocorrer erros com caracteres semi-gráficos
                                    $msg = URLEncode($msg); 
                                    $response =fopen("https://www.mpgateway.com/v_3_00/sms/smspush/enviasms.aspx?CREDENCIAL=".$credencial."&TOKEN=".$token."&PRINCIPAL_USER=".$principal."&AUX_USER=".$auxuser."&MOBILE=".$mobile."&SEND_PROJECT=".$sendproj."&MESSAGE=".$msg,"r");
                                    $status_code= fgets($response,4);
                                    echo "-Codigo retornando do fopen=";
                                    echo $status_code;
                                    if($status_code=="000")
                                    {
                                         //Guardar log de que o sms foi enviado
                                        $n = new notificacaoRelatoriosNaoEntreguesSmsMestre();
                                        $n->setFk_idUsuario($vetorUsu['idUsuario']);
                                        $n->setFk_idOrganismoAfiliado($idOrganismoAfiliado);
                                        $n->setNome(strtoupper($vetorUsu['nomeUsuario']));
                                        $n->setCelular($celular);
                                        $res = $n->cadastra();

                                        echo " -- Cadastro log do sms realizado com sucesso";
                                    }    
                                    echo "<br>";exit();
                                }
			}
		}
	}
}

?>	