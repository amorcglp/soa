<?php 

//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );

$hoje = date("Y-m-d")."T00:00:00";

include_once("../model/organismoClass.php");
$organismo = new organismo();

include_once("../controller/membroOAController.php");
$m = new membroOAController();
$resultado = $m->listaMembroOA();

echo "
	<meta charset=\"utf-8\">
	<script src=\"../js/jquery-2.1.1.js\"></script>
	<script src=\"../js/bootstrap.min.js\"></script>
	<script src=\"../js/plugins/metisMenu/jquery.metisMenu.js\"></script>
	<script src=\"../js/plugins/slimscroll/jquery.slimscroll.min.js\"></script>
	<script src=\"../js/plugins/jeditable/jquery.jeditable.js\"></script>
	<script src=\"../js/functions3.js\"></script>
	<script src=\"../js/plugins/chosen/chosen.jquery.js\"></script>
	<script src=\"../js/plugins/summernote/summernote.min.js\"></script>
	<script src=\"../js/plugins/datapicker/bootstrap-datepicker.js\"></script>
	<script src=\"../js/plugins/cropper/cropper.min.js\"></script>
	<script src=\"../js/plugins/dataTables/jquery.dataTables.js\"></script>
	<script src=\"../js/plugins/dataTables/dataTables.bootstrap.js\"></script>
	<script src=\"../js/plugins/dataTables/dataTables.responsive.js\"></script>
	<script src=\"../js/plugins/dataTables/dataTables.tableTools.min.js\"></script>
	<script src=\"../js/inspinia.js\"></script>
	<script src=\"../js/plugins/pace/pace.min.js\"></script>
	<script src=\"../js/plugins/toastr/toastr.min.js\"></script>
	<script src=\"../js/jquery.maskedinput.js\" type=\"text/javascript\"></script>
	<script src=\"../js/jquery.maskMoney.js\" type=\"text/javascript\"></script>
    <script src=\"../js/plugins/iCheck/icheck.min.js\"></script>
    <script src=\"../js/plugins/blueimp/jquery.blueimp-gallery.min.js\"></script>
	<script src=\"../js/jquery.uploadify.min.js\"></script>
	";

if($resultado)
{
	foreach($resultado as $vetor)
	{
		$idOrganismoAfiliado 	= $vetor['idOrganismoAfiliado'];
		$seqCadast				= $vetor['seqCadastMembroOa'];
		
		$siglaOa="";
		$resultado = $organismo->listaOrganismo(null,null,null,null,null,$idOrganismoAfiliado);
		if($resultado)
		{
			foreach($resultado as $vetor)
			{
				$siglaOa = $vetor['siglaOrganismoAfiliado'];
			}
		}
	
		echo "<script>atualizaOaAtuacao('".$seqCadast."','".$siglaOa."');</script>";
		echo "<br>Membro ".$seqCadast." foi atualizado para o organismo ".$siglaOa."<br>";
	}
}

?>	