<?php 

//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );
$server = 138;
$hoje = date("Y-m-d")."T00:00:00";

include_once('../lib/functions.php');
include_once('../lib/phpmailer/class.phpmailer.php');
include_once('../model/organismoClass.php');
include_once('../model/usuarioClass.php');
include_once('../model/funcaoUsuarioClass.php');

$funcaoUsuario = new FuncaoUsuario();

$organismo = new organismo();

$resultadoOrg = $organismo->listaOrganismo();

if($resultadoOrg)
{
	foreach($resultadoOrg as $vetorOrg)
	{
		$idOrganismoAfiliado 	= $vetorOrg['idOrganismoAfiliado'];
		$siglaOrganismo			= $vetorOrg['siglaOrganismoAfiliado'];
		$total=0;//Contador de relatórios NÃO entregues
	
		//Procurar mestres cadastrados no sistema para este organismo
		$usuario = new Usuario();
		
		$resultadoUsu = $usuario->listaUsuario($siglaOrganismo,1);//apenas usuários deste organismo e que seja do departamento 1
		
		if($resultadoUsu)
		{
			foreach($resultadoUsu as $vetorUsu)
			{
				//Apagar todas as funções do usuário na tabela FUNCAO_USUARIO
				$funcaoUsuario->setFk_seq_cadast($vetorUsu['seqCadast']);
				$resultado = $funcaoUsuario->removePorUsuario();
				
				//Recuperar
				echo "Nome:".$vetorUsu['nomeUsuario']."<br>";
				$ocultar_json=1;
				$naoAtuantes='N';
				$seqCadast=$vetorUsu['seqCadast'];
				include '../js/ajax/retornaFuncaoMembro.php';
				$obj = json_decode(json_encode($return),true);
				echo "<pre>".print_r($obj['result'][0]['fields']['fArrayOficiais']);
				//$arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];
				if(!isset($obj['result']))//Usuário não atua mais com nenhuma função
				{
					$usuario->setStatusUsuario(1);//Setar Status Inativo
					$usuario->setSeqCadast($vetorUsu['seqCadast']);
					$usuario->alteraStatusUsuario();//Alterar o status do usuário para inativo
					echo "<br>Usuario ".$vetorUsu['nomeUsuario']." inativado com sucesso!";
				}
			}
		}
	}
}

?>	