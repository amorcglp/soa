<?php
//echo "<br>Forçando 1 execução";
//exit;


//echo "<b>Cron: Envio de Recebimentos e Despesas para o Firebase</b><br><br>";

// echo "<br>Tempo 1 - Início da Execução da Cron: ".date("d/m/Y H:i:s");

// require_once '../vendor/autoload.php';

// use Google\Cloud\Firestore\FirestoreClient;

$browser = isset($_REQUEST['browser'])? $_REQUEST['browser']: false;

if(!$browser)
{
    echo "<br>Rodando cron no server do Jupiter, não no browser";

    $dirName = dirname(__FILE__);
    $arrDirName = explode("/",$dirName);
    echo "Dir:<pre>";print_r($arrDirName);

    $play="localhost";
    if(in_array('treinamento',$arrDirName))
    {
        echo "<br>Rodando cron em Treinamento";
        $play="treinamento";
    }else{
        if(in_array('soa',$arrDirName)) {
            echo "<br>Rodando cron em Produção";
            $play="producao";
        }else{
            echo "<br>Rodando em Localhost...não é permitido rodar essa cron! Encerrando o script!";
            exit;
        }
    }

    // $projectId = (strcmp(UrlAtual(), "producao")==0) ? "sistemadeorganismosafiliados" : "";
    // $projectId = (strcmp(UrlAtual(), "treinamento")==0 or strcmp(UrlAtual(), "local")==0) ? "soadesenvolvimento" : "";
} else {

    echo "<br>Rodando cron no browser";

    // function UrlAtual(){
    //     return $_SERVER['HTTP_HOST'];
    // }

    // $projectId = UrlAtual()=="soa.amorc.org.br" ? "sistemadeorganismosafiliados" : "";
    // $projectId = (strcmp(UrlAtual(), "treinamento.amorc.org.br")==0 or strcmp(UrlAtual(), "soa.local")==0) ? "soadesenvolvimento" : "";
}


// echo "<br>ProjectId no Firebase- ".$projectId;

$db = include('../firebase.php');

// Create the Cloud Firestore client
// $db = new FirestoreClient([
//     'projectId' => $projectId
// ]);

$arrDocumentosFirebase=array();

include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

//$idOrganismoAfiliado = $idNaoCadastrado;
//$idOrganismoAfiliado = 1;//Teste com Loja CURITIBA
//$idOrganismoAfiliado = 81;//Teste com Loja São Paulo
//$idOrganismoAfiliado = 157;//Teste com Loja Rio de Janeiro
//$idOrganismoAfiliado = 136;//Teste com Loja BH
//$idOrganismoAfiliado = 70;//Teste com Blumenal

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']:null;//Organismo alvo: 160

if($idOrganismoAfiliado==null)
{
    //Realizar get no firebase para ver quais organismos ainda não foram populados
    $documentsFirebase = $db->collection("balances")->documents();

    $arrOas=array();

    /*

    $arrFoco=array(
        3,
        4,
        5,
        6,
        27,
        28,
        29,
        30,
        31,
        32,
        33,
        37,
        39,
        50,
        51,
        52,
        53,
        54,
        57,
        59,
        60,
        61,
        62,
        63,
        64
    );*/

    /*
    $arrFoco=array(
        282,
        298
    );
    */

/*
    $arrFoco=array(
        1
    );
*/

    //Organismos Problema 297 NAZARE E 23 CASCAVEL

    $arrFoco=array(
        297,
        23
    );

    //Levam mais de 8 minutos
    /*
    $arrOas=array(
        170,
        183,
        89,
        234,
        117,
        9,
        268,
        205,
        146,
        114,
        181,
        162,
        164,
        36,
        4,
        88,
        112,
        180,
        30,
        179,
        94,
        277,
        104,
        165,
        160,
        50,
        16,
        176,
        136,
        83,
        140,
        157
    );
    */
    //echo "<br>Array Foco: ";print_r($arrFoco);

    foreach ($documentsFirebase as $documentProjecao) {
        if ($documentProjecao->exists()) {
            $arrOas[] = $documentProjecao->id();
        }else{
            echo "<br><br>Não existe nenhum organismo com projeção no firebase. Fim do script!";
            $arrOas=array();
        }
    }

    $arrFinalParaDecidir=array();
    foreach ($arrFoco as $v)
    {
        if(!in_array($v,$arrOas))//O oa foco não pode estar no firebase
        {
            $arrFinalParaDecidir[] = $v;
        }

    }


    //echo "<br>Oas que sobraram para o foco:<pre>";print_r($arrFinalParaDecidir);

    $o = new organismo();
    if(count($arrFinalParaDecidir)>0)
    {
        $idOrganismoAfiliado = $o->retornaOaNaoCadastradoFirebase($arrFinalParaDecidir);
    }

}

if(count($arrFinalParaDecidir)>0) {

    //Saldo Inicial
    $si = new saldoInicial();
    $mesSaldoInicialString = str_pad(strval($si->getMesSaldoInicial($idOrganismoAfiliado)), 2, "0", STR_PAD_LEFT);
    $anoSaldoInicialString = strval($si->getAnoSaldoInicial($idOrganismoAfiliado));
    $valorSaldoInicialString = strval(number_format($si->getValorSaldoInicial($idOrganismoAfiliado),2,'.',''));
    $dataSaldoInicial = strval($si->getDataCompletaSaldoInicial());
    $dataSaldoInicialString = strval(substr($dataSaldoInicial,8,2)."/".substr($dataSaldoInicial,5,2).substr($dataSaldoInicial,0,4)." 00:00:00");
    $usuarioSaldoInicialString = strval($si->getUsuarioSaldoInicial());
    $ultimoAtualizarSaldoInicialString = strval($si->getUltimoAtualizarSaldoInicial());

    echo "<br>Valor Saldo Inicial=>" . $valorSaldoInicialString;

    //echo "<br>Criando documento do organismo";

    $dataArray =
        [
            'lastUpdatedOpeningBalance' => $ultimoAtualizarSaldoInicialString,
            'userOpeningBalance' => $usuarioSaldoInicialString,
            'updateData' => date("d/m/Y H:i:s"),
            'monthOpeningBalance' => $mesSaldoInicialString,
            'yearOpeningBalance' => $anoSaldoInicialString,
            'valueOpeningBalance' => $valorSaldoInicialString,
            'dateOpeningBalance' => $dataSaldoInicialString,
            'balance'=> 1
        ];

    $docRef2 = $db->collection("balances")->document($idOrganismoAfiliado);
    $docRef2->set($dataArray);

    /*
    echo "<br>Criando a colection information";

    $dataArray =
        [
            'user' => 550110,
            'updateData'=> date("d/m/Y H:i:s")
        ];

    $docRef2 = $db->collection("balances/" . $idOrganismoAfiliado . "/information")->newDocument();
    $docRef2->set($dataArray);
    */
    /*
    echo "<br>Criando a colection balance";

    $dataArray =
    [
        'empty' => 0
    ];

    $docRef2 = $db->collection("balances/" . $idOrganismoAfiliado . "/balance")->newDocument();
    $docRef2->set($dataArray);
    */
    //echo "<br>Criando a colection recebimentos";

    $dataArray =
        [
            'empty' => 0
        ];

    $docRef3 = $db->collection("balances/" . $idOrganismoAfiliado . "/recebimentos")->newDocument();
    $docRef3->set($dataArray);
    $idR = $docRef3->id();


    echo "<br>Criando a colection despesas";

    $dataArray =
        [
            'empty' => 0
        ];

    $docRef4 = $db->collection("balances/" . $idOrganismoAfiliado . "/despesas")->newDocument();
    $docRef4->set($dataArray);
    $idD = $docRef4->id();

    echo "<br>idEmptyFileRecebimentos:" . $idR;
    echo "<br>idEmptyFileDespesas:" . $idD;

    //echo "continua braz...";exit;

    $o = new organismo();
    $arr = $o->retornaSiglaOa($idOrganismoAfiliado);
    $idOrganismoAfiliado = $arr['id'];
    echo "<br>Id do Oa Atualizado:" . $arr['id'];
    echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
    echo "<br>Sigla:" . $arr['sigla'];


    //De posse do id do organismo seguir fazendo a projeção

    /*
     * Preparar as variáveis para a projeção
     */

    $anoAtual = date('Y') + 1;
    $mesAtual = date('m');

    $r = new Recebimento();
    $d = new Despesa();

    /*
     * Calcular saldo do mês anterior
     */

    //Encontrar Saldo e Data Inicial
    $si = new saldoInicial();

    $mes = (int)$si->getMesSaldoInicial($idOrganismoAfiliado);
    $ano = $si->getAnoSaldoInicial($idOrganismoAfiliado);
    $saldoInicial = $si->getValorSaldoInicial($idOrganismoAfiliado);
    $saldoMesesAnteriores = $saldoInicial;
    $temSaldoInicial = $si->temSaldoInicial($idOrganismoAfiliado);

    if ($temSaldoInicial) {

        //Pegar todos os registros de recebimentos do organismo no SOA

        $r = new Recebimento();
        $arrRecebimentos = $r->listaRecebimento(null, null, $idOrganismoAfiliado);
        //echo "<br>Tempo 2 - Pegou todos os registros de Recebimentos do SOA: ".date("d/m/Y H:i:s");

        //Pegar todos os registros de despesas do organismo no SOA

        $d = new Despesa();
        $arrDespesas = $d->listaDespesa(null, null, $idOrganismoAfiliado);
        //echo "<br>Tempo 3 - Pegou todos os registros de Despesas do SOA: ".date("d/m/Y H:i:s");


        //Looping 1 - Envio dos Recebimentos
        if (count($arrRecebimentos) > 0) {
            foreach ($arrRecebimentos as $v) {

                //echo "<br>idSOARecebimentos=>".$v['idRecebimento'];

                //Tratamentos

                if ($v['codigoAfiliacao'] == "") {
                    $v['codigoAfiliacao'] = 0;
                }

                $mes = substr($v['dataRecebimento'], 5, 2);
                $ano = substr($v['dataRecebimento'], 0, 4);

                $v['dataRecebimento'] = substr($v['dataRecebimento'], 8, 2) . "/" . substr($v['dataRecebimento'], 5, 2) . "/" . substr($v['dataRecebimento'], 0, 4);
                $v['dataCadastro'] = substr($v['dataCadastro'], 8, 2) . "/" . substr($v['dataCadastro'], 5, 2) . "/" . substr($v['dataCadastro'], 0, 4) . " " . substr($v['dataCadastro'], 11, 8);

                $v['valorRecebimento'] = str_replace(".", "", $v['valorRecebimento']);//Tirar milhar
                $v['valorRecebimento'] = str_replace(",", ".", $v['valorRecebimento']);//Mudar para .

                //Cadastro no firebase

                # [START fs_set_document]
                $dataArrayRecebimento =
                    [
                        'fk_idOrganismoAfiliado' => $idOrganismoAfiliado,
                        'mes' => $mes,
                        'ano' => $ano,
                        'idRecebimentoSOA' => $v['idRecebimento'],
                        'categoriaRecebimento' => $v['categoriaRecebimento'],
                        'codigoAfiliacao' => $v['codigoAfiliacao'],
                        'dataRecebimento' => $v['dataRecebimento'] . " 00:00:00",
                        'descricaoRecebimento' => $v['descricaoRecebimento'],
                        'recebemosDe' => $v['recebemosDe'],
                        'ultimoAtualizar' => $v['ultimoAtualizar'],
                        'valorRecebimento' => $v['valorRecebimento'],//Fazer conversão para float
                        'userRecebimentos' => $v['usuario'],
                        'dataCadastro' => $v['dataCadastro'],
                        'balance' => 1,
                        'updateData' => date('d/m/Y H:i:s')

                    ];

                $docRef5 = $db->collection("balances/" . $idOrganismoAfiliado . "/recebimentos")->newDocument();
                $docRef5->set($dataArrayRecebimento);

            }
        }


        echo "<br>Tempo 4 - Enviou os recebimentos do SOA para o Firebase: " . date("d/m/Y H:i:s");

        //Looping 1 - Envio dos Recebimentos
        if (count($arrDespesas) > 0) {

            foreach ($arrDespesas as $v) {

                //Tratamentos

                $mes = substr($v['dataDespesa'], 5, 2);
                $ano = substr($v['dataDespesa'], 0, 4);

                $v['dataDespesa'] = substr($v['dataDespesa'], 8, 2) . "/" . substr($v['dataDespesa'], 5, 2) . "/" . substr($v['dataDespesa'], 0, 4);
                $v['dataCadastro'] = substr($v['dataCadastro'], 8, 2) . "/" . substr($v['dataCadastro'], 5, 2) . "/" . substr($v['dataCadastro'], 0, 4) . " " . substr($v['dataCadastro'], 11, 8);

                $v['valorDespesa'] = str_replace(".", "", $v['valorDespesa']);//Tirar milhar
                $v['valorDespesa'] = str_replace(",", ".", $v['valorDespesa']);//Mudar para .

                //Cadastro no firebase

                # [START fs_set_document]
                $dataArrayDespesa =
                    [
                        'fk_idOrganismoAfiliado' => $idOrganismoAfiliado,
                        'mes' => $mes,
                        'ano' => $ano,
                        'idDespesaSOA' => $v['idDespesa'],
                        'categoriaDespesa' => $v['categoriaDespesa'],
                        'dataDespesa' => $v['dataDespesa'] . " 00:00:00",
                        'descricaoDespesa' => $v['descricaoDespesa'],
                        'pagoA' => $v['pagoA'],
                        'ultimoAtualizar' => $v['ultimoAtualizar'],
                        'valorDespesa' => $v['valorDespesa'],//Fazer conversão para float
                        'userDespesas' => $v['usuario'],
                        'dataCadastro' => $v['dataCadastro'],
                        'balance' => 1,
                        'updateData' => date('d/m/Y H:i:s')

                    ];

                $docRef6 = $db->collection("balances/" . $idOrganismoAfiliado . "/despesas")->newDocument();
                $docRef6->set($dataArrayDespesa);

            }
        }

        //echo "<br>Tempo 5 - Enviou as despesas do SOA para o Firebase: ".date("d/m/Y H:i:s");

        //Ações finais

        echo "<br>===================================================";
        echo "<br>Apagar arquivos empty:";
        $db->collection("balances/" . $idOrganismoAfiliado . "/recebimentos")->document($idR)->delete();
        $db->collection("balances/" . $idOrganismoAfiliado . "/despesas")->document($idD)->delete();

        $docRef7 = $db->collection("balances/" . $idOrganismoAfiliado . "/despesas");
        $query = $docRef7->documents();
        $totalDocumentosDespesas = 0;

        foreach ($query as $d) {
            if ($d->exists()) {
                $totalDocumentosDespesas++;
            }
        }

        $docRef8 = $db->collection("balances/" . $idOrganismoAfiliado . "/recebimentos");
        $queryRec = $docRef8->documents();
        $totalDocumentosRecebimentos = 0;

        foreach ($queryRec as $d2) {
            if ($d2->exists()) {
                $totalDocumentosRecebimentos++;
            }
        }

        echo "<br>===================================================";
        echo "<br>Firebase (sem empty):";
        echo "<br>Total de documentos de despesas no Firebase (sem empty): " . $totalDocumentosDespesas;
        echo "<br>Total de documentos de recebimentos no Firebase (sem empty): " . $totalDocumentosRecebimentos;

        echo "<br>===================================================";
    }

    echo "<br>Tempo 6 - Fim da execução da Cron: " . date("d/m/Y H:i:s");

}else{
    echo "<br>Não existem Oas para enviar entradas para o Firebase";
}
?>