<?php 
require '../vendor/autoload.php';
use JeroenDesloovere\Geolocation\Geolocation;

if(isset($_GET['seqCadast'])&&isset($_GET['formato']))
{  
    $seqCadast = ($_GET['seqCadast'])?$_GET['seqCadast']:null;
    $formato = ($_GET['formato'])?$_GET['formato']:null;

      
        if($seqCadast!=null)
        {    
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';
            }else{
                    require_once './model/wsClass.php';
            }

            // Instancia a classe
            $ws = new Ws();

            // Nome do Método que deseja chamar
            $method = 'RetornaDadosMembroPorSeqCadast';

            // Parametros que serão enviados à chamada
               
            $params = array('CodUsuario' => 'lucianob',
                                            'SeqCadast'=>$seqCadast
                                            );
            
            //echo '<pre>';print_r($params);
            // Chamada do método
            $return = $ws->callMethod($method, $params, 'lucianob');

            $obj = json_decode(json_encode($return),true);
//echo '<pre>'; print_r($obj);
            $endereco="";
            $numero="";
            $bairro="";
            $cidade="";
            $uf="";
            $cep="";
            $siglaPais="";
            $seqCadast='';

            //echo $obj['result'][0]['fields']['fSeqCadast'];

            if($obj)
            {    
                $seqCadast = $obj['result'][0]['fields']['fSeqCadast'];
                $endereco = trim($obj['result'][0]['fields']['fNomLogradouro']);
                $numero = trim($obj['result'][0]['fields']['fNumEndereco']);
                $bairro = trim($obj['result'][0]['fields']['fNomBairro']);
                $cidade = trim($obj['result'][0]['fields']['fNomCidade']);
                $uf = trim($obj['result'][0]['fields']['fSigUf']);
                $cep = trim($obj['result'][0]['fields']['fCodCepEndereco']);
                $siglaPais = trim($obj['result'][0]['fields']['fSigPaisEndereco']);
        
                //echo $endereco; echo '-'.$numero; echo '-'.$cidade; echo '-'.$cep; echo '-'.$siglaPais; 

                if($seqCadast!='')
                { 
                    $geo = new Geolocation();
                    $arr = $geo->getCoordinates(
                        $endereco,
                        $numero,
                        $cidade,
                        $cep,
                        $siglaPais
                    );
if($formato=='json')
{
    echo '{"seqCadast":"'.$seqCadast.'","latitude":"'.$arr['latitude'].'","longitude":"'.$arr['longitude'].'"}';
}
if($formato=='html')
{
    echo $seqCadast.'\t'.$arr['latitude'].'\t'.$arr['longitude'];
}    
                }else{
                    echo 'Erro';
                }
            }
            }else{?>
            Se faz necessário passar o seqCadast do membro
            <?php } ?>
        <?php } ?>
