<?php

foreach([
    [
        '../../lib/functions.php',
        '../lib/functions.php',
        './lib/functions.php'
    ],
    [
        '../../model/saldoInicialClass.php',
        '../model/saldoInicialClass.php',
        './model/saldoInicialClass.php'
    ],
    [
        '../../model/organismoClass.php',
        '../model/organismoClass.php',
        './model/organismoClass.php'
    ],
    [
        '../../model/recebimentoClass.php',
        '../model/recebimentoClass.php',
        './model/recebimentoClass.php'
    ],
    [
        '../../model/despesaClass.php',
        '../model/despesaClass.php',
        './model/despesaClass.php'
    ]
] as $includePath) {
    foreach($includePath as $path) {
        if(realpath($path)) {
            require_once $path;
            break;
        }
    }
}

$debug = isset($_REQUEST['debug']) ?$_REQUEST['debug']:false;

if($debug)
{
    echo "<br>Tempo 1 - Início da Execução da Cron: ".date("d/m/Y H:i:s");
}

$db = include('../firebase.php');

if($debug) {
    echo "<br>Tempo 2 - Busca no Firebase (recebimentos, despesas e recebimentos): " . date("d/m/Y H:i:s");
}

//Recebendo variáveis do functions.js

$mesInt                         = isset($_REQUEST['mes']) ?intval($_REQUEST['mes']):null;
$anoInt                         = isset($_REQUEST['ano']) ?intval($_REQUEST['ano']):null;
$mesString                      = isset($_REQUEST['mes']) ?strval ($_REQUEST['mes']):null;
$anoString                      = isset($_REQUEST['ano']) ?strval ($_REQUEST['ano']):null;
$fk_idOrganismoAfiliado         = isset($_REQUEST['fk_idOrganismoAfiliado']) ? intval($_REQUEST['fk_idOrganismoAfiliado']):null;//Organismo alvo: 160
$fk_idOrganismoAfiliadoString   = isset($_REQUEST['fk_idOrganismoAfiliado']) ? strval($_REQUEST['fk_idOrganismoAfiliado']):null;//Organismo alvo: 160

//$idIgnorarRecebimento=null,$valorRecebimento=null,$idIgnorarDespesa=null,$valorDespesa=null para o cálculo
$idIgnorarRecebimento           = isset($_REQUEST['idIgnorarRecebimento']) ?$_REQUEST['idIgnorarRecebimento']:null;
$valorRecebimento               = isset($_REQUEST['valorRecebimento']) ?$_REQUEST['valorRecebimento']:null;
$idIgnorarDespesa               = isset($_REQUEST['idIgnorarDespesa']) ?$_REQUEST['idIgnorarDespesa']:null;
$valorDespesa                   = isset($_REQUEST['valorDespesa']) ?$_REQUEST['valorDespesa']:null;

$usuario                        = isset($_REQUEST['usuario']) ?$_REQUEST['usuario']:null;
$ultimoId                       = isset($_REQUEST['ultimoId']) ?$_REQUEST['ultimoId']:null;//Esse campo é usado para não ser considerado no calculo final quando o registro é editado

$categoriaEntrada               = isset($_REQUEST['categoriaEntrada']) ?$_REQUEST['categoriaEntrada']:null;
$idRefDocumentoFirebase         = isset($_REQUEST['idRefDocumentoFirebase']) ?$_REQUEST['idRefDocumentoFirebase']:null;

if($debug) {
    echo "<br>Parametros: ";
    echo "<br>MesInt:" . $mesInt;
    echo "<br>AnoInt:" . $anoInt;
    echo "<br>MesString:" . $mesString;
    echo "<br>AnoString:" . $anoString;
    echo "<br>Fk_idOrganismoAfiliado:" . $fk_idOrganismoAfiliado;
    echo "<br>IdIgnorarRecebimento:" . $idIgnorarRecebimento;
    echo "<br>ValorRecebimento:" . $valorRecebimento;
    echo "<br>IdIgnorarDespesa:" . $idIgnorarDespesa;
    echo "<br>ValorDespesa:" . $valorDespesa;
    echo "<br>Usuario:" . $usuario;
    echo "<br>UltimoId:" . $ultimoId;
    echo "<br>Categoria de Entrada:" . $categoriaEntrada;
    echo "<br>idRefDocumentoFirebase:" . $idRefDocumentoFirebase;
}
/*
//Saldo Inicial DA BASE DO SOA
$si = new saldoInicial();
$mesSaldoInicial = $si->getMesSaldoInicial($fk_idOrganismoAfiliado);
$anoSaldoInicial = $si->getAnoSaldoInicial($fk_idOrganismoAfiliado);
$valorSaldoInicial = $si->getValorSaldoInicial($fk_idOrganismoAfiliado);
*/

//Pegar informações do saldo inicial independente da categoria de entrada, pois é preciso essa informação para o array final ordenado
$saldoAnterior=0;
$saldoInicialFirebase = $db->collection("balances")->document($fk_idOrganismoAfiliado);
$snapshot = $saldoInicialFirebase->snapshot();

//echo "saldoInicialFirebase<pre>";print_r($snapshot);

$arrSI=$snapshot->data();

if($arrSI)
{
    $mesSaldoInicial = $arrSI['monthOpeningBalance'];
    $anoSaldoInicial = $arrSI['yearOpeningBalance'];
    $saldoAnterior = floatval($arrSI['valueOpeningBalance']);
    $valorSaldoInicial = floatval($arrSI['valueOpeningBalance']);

    $arrSI['balance']=1;//Int
    $arrSI['ultimoAtualizar']=$usuario;

    $temSaldoInicialFirebase=1;
}


//echo "<br>Saldo Anterior:".$saldoAnterior;

/*
 * Projeção
 */

//Conversão para string das variaveis principais
$fk_idOrganismoAfiliadoString = (string) $fk_idOrganismoAfiliado;

$idDocumentoAlteracao=0;

if($debug) {
    echo "<br>Tempo 3 - Busca no Firebase (balances): " . date("d/m/Y H:i:s");
}

/*
 * Buscando balances no Firebase
 */
//echo "<br>Teste query do Firebase:<pre>";print_r($query);

$b2 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/balance");
$queryPro = $b2->documents();

$z=0;

$ultimoSaldo=0;

//echo "<br>Query Balance Com Documentos do Firebase do OA<pre>";
//print_r($queryPro);exit;

$arrProjecaoFinal = array();

foreach ($queryPro as $documentProjecao)
{
    if ($documentProjecao->exists()) {

        $arrP = $documentProjecao->data();
        $arrP['id'] = $documentProjecao->id();

        $month = intval($arrP['month']);
        $year = intval($arrP['year']);

        $arrProjecaoFinal[$year][$month] = $arrP;
    }
}

if($debug) {
    echo "<br>Tempo 5 -Iniciando busca nos Documentos Recebimentos e Despesas do Firebase para Projeção: " . date("d/m/Y H:i:s");
}

//$parametro=floatval(str_replace(",",".",str_replace(".","",$valorRecebimento)));
//$arr = retornaSaldoRetornoArray($r,$d,$mes,$ano,$fk_idOrganismoAfiliado,$saldoAnterior,null,$parametro);

/*
 * Cálculo montagem do arr do Firebase
 */

//echo "<br>INICIANDO TESTE BRAZ";

//$mesString='01';
//$anoString='2017';

//echo "<br>mesString: " . $mesString;
//echo "<br>anoString: " . $anoString;

$arrRec = array();

//Pegar todos os recebimentos
$recebimentos = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/recebimentos");
$queryRecebimentos = $recebimentos->documents();

//echo "<br>rows: " . count($queryRecebimentos->rows());

//Pegar recebimentos do mes atual
$queryWhereR = $recebimentos->where('mes', '=', $mesString)->where('ano', '=', $anoString);
$queryRecebimentosPontual = $queryWhereR->documents();

if($categoriaEntrada != null && $idRefDocumentoFirebase != null) {
    if ($categoriaEntrada == "recebimento") {

        //Percorrer recebimentos do mes atual
        foreach ($queryRecebimentosPontual as $documentRec) {
            if ($documentRec->exists()) {

                //echo "<br>id:".$documentRec->id();
                if($idRefDocumentoFirebase==$documentRec->id())
                {
                    $arrRec=$documentRec->data();
                    $arrRec['balance'] = 1;//Int
                    $arrRec['ultimoAtualizar'] = $usuario;
                    break;
                }
            }
        }
    }
}

//echo "<br>Teste Array Rec<pre>";print_r($arrRec);

$arrDes=array();

//Pegar todos as despesas
$despesas = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/despesas");
$queryDespesas = $despesas->documents();

//Pegar despesas do mes atual para fechar balance
$queryWhereD = $despesas->where('mes', '=', $mesString)->where('ano', '=', $anoString);
$queryDespesasPontual = $queryWhereD->documents();

if($categoriaEntrada!=null&&$idRefDocumentoFirebase!=null) {
    if ($categoriaEntrada == "despesa") {

        //Percorrer recebimentos do mes atual
        foreach ($queryDespesasPontual as $documentDes) {
            if ($documentDes->exists()) {

                //echo "<br>id:".$documentRec->id();
                if($idRefDocumentoFirebase==$documentDes->id())
                {
                    $arrDes=$documentDes->data();
                    $arrDes['balance']=1;//Int
                    $arrDes['ultimoAtualizar']=$usuario;
                    break;
                }
            }
        }
    }
}


//echo "<br>Array Vindo do Firebase para o Looping Sem Ordenação:<pre>";print_r($arrProjecaoFinal);

//Ordenar array por ano e mes
ksort($arrProjecaoFinal);

$arrFinal=array();
$w=0;
foreach($arrProjecaoFinal as $key => $v)
{
    for($x=1;$x<=12;$x++)
    {
        //$parametro=floatval(str_replace(",",".",str_replace(".","",$valorRecebimento)));

        $mesInteracao = $x;
        $arrFinal[$key][$mesInteracao] = $arrProjecaoFinal[$key][$mesInteracao];

        if($mesInteracao==1)
        {
            $mesAnterior=12;
            $anoAnterior=$key-1;
        }else{
            $mesAnterior=$mesInteracao-1;
            $anoAnterior=$key;
        }

        $idDocumentoAlteracaoLooping=$arrFinal[$key][$mesInteracao]['id'];

        if($mesInteracao==$mesSaldoInicial&&$key==$anoSaldoInicial)
        {
            //echo "<br>Ano Si:".$anoSaldoInicial." Mes Si:".$mesSaldoInicial;
            $saldoInicial = $valorSaldoInicial;
        }else{
            //echo "<br>Ano A:".$anoAnterior." Mes A:".$mesAnterior;
            $saldoInicial = $arrFinal[$anoAnterior][$mesAnterior]['monthBalance'];//Saldo anterior pontual ou saldo inicial do organismo tratado anteriormente
        }

        if($key>=$anoInt&&$mesInteracao>=$mesInt)//Atualizar array a partir do
        {
            if($debug) {
                echo "<br>Afetando=>" . $mesInteracao . "/" . $key;
            }

            if($w==0)
            {
                $saldoMesesAnteriores = $saldoInicial;
            }


            /*
             * Cálculo conforme parâmetros passados - Recebimento
             */

            //echo "<br>Tempo 6 -Calculos das entradas: ".date("d/m/Y H:i:s");

            $mesString = strval(str_pad($mesInteracao,2,'0',STR_PAD_LEFT));
            $anoString = strval($key);

            $arr = retornaSaldoRetornoArrayFirebase($queryRecebimentos,$queryDespesas,$mesString,$anoString,$saldoMesesAnteriores);

            //Definição final das entradas depois do evento
            $totalEntries = $arr['totalEntries'];
            $totalOutputs = $arr['totalOutputs'];

            //Cálculo do saldo

            $monthBalance = ($saldoMesesAnteriores+$totalEntries)-$totalOutputs;

            //Arrumar array final atualizando...
            $arrFinal[$key][$mesInteracao]['previousBalance']=$saldoMesesAnteriores;
            $arrFinal[$key][$mesInteracao]['totalEntries']=$totalEntries;
            $arrFinal[$key][$mesInteracao]['totalOutputs']=$totalOutputs;
            $arrFinal[$key][$mesInteracao]['monthBalance']=$monthBalance;

            $saldoMesesAnteriores = $monthBalance;

            $ultimoSaldo = $monthBalance;

            $w++;

            /*
             * Imprimir saida final do saldo
             */
            if($debug) {
                echo "<br>Saldo anterior:" . $saldoMesesAnteriores;
                echo "<br>Entradas:" . $totalEntries;
                echo "<br>Saidas:" . $totalOutputs;
                echo "<br>Saldo do Mes:" . $monthBalance;
            }

            //Envio para o Firebase

            $mesString = str_pad(strval($mesInteracao), 2, '0', STR_PAD_LEFT);
            $anoString = strval($key);

            # [START fs_set_document]
            $dataArrayNewBalance =
                [
                    'fk_idOrganismoAfiliado'   => $fk_idOrganismoAfiliadoString,
                    'month' => $mesString,
                    'year' => $anoString,
                    'previousBalance' => number_format($arrFinal[$key][$mesInteracao]['previousBalance'], 2, '.', ''),
                    'totalEntries' => number_format($arrFinal[$key][$mesInteracao]['totalEntries'], 2, '.', ''),
                    'totalOutputs' => number_format($arrFinal[$key][$mesInteracao]['totalOutputs'], 2, '.', ''),
                    'monthBalance' => number_format($arrFinal[$key][$mesInteracao]['monthBalance'], 2, '.', ''),
                    'updateData' => date('d/m/Y H:i:s'),
                    'userBalance' => strval($usuario)
                ];

            //echo "<br>Array Final para o Firebase:<pre>";print_r($dataArrayNewBalance);

            if($debug) {
                echo "<br>d:" . $d;
                echo "<br>Id Documento a Ser Alterado:" . $idDocumentoAlteracaoLooping;
                echo "<br><br>Array que será atualizado no Firebase (Projeção Looping):<pre>";
                print_r($dataArrayNewBalance);
            }

            //Atualizando Firebase
            //echo "<br>Iniciando Update do Registro:";



            $docRef7 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/balance")->document($idDocumentoAlteracaoLooping);
            $docRef7->set($dataArrayNewBalance);
        }
    }

    //Quando chegar no mes 12 voltar para o mes de janeiro
    if($mesString==12)
    {
        //echo "<br>entrou aqui e voltou para janeiro";
        $x=1;
        $mesInt=1;
        $mesInteracao=1;
    }
}

if($debug) {
    echo "<br>Array Final Ordenado:<pre>";
    print_r($arrFinal);
}


//exit;

/*
 * Atualizar registro no firebase dizendo que o saldo foi calculado
 */
if($debug) {
    echo "<br>Tempo 9 - Informando o Firebase que o registro possui projeção calculada: " . date("d/m/Y H:i:s");
}

//echo "Teste Array Rec:<br>";print_r($arrRec);

if($categoriaEntrada != null && $idRefDocumentoFirebase != null && $arrRec)
{
    if ($categoriaEntrada == "recebimento")
    {
        $docRef9 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/recebimentos")->document($idRefDocumentoFirebase);
        $docRef9->set($arrRec);
    }
}
if($categoriaEntrada != null && $idRefDocumentoFirebase != null && $arrDes)
{
    if ($categoriaEntrada == "despesa")
    {
        $docRef10 = $db->collection("balances/" . $fk_idOrganismoAfiliado . "/despesas")->document($idRefDocumentoFirebase);
        $docRef10->set($arrDes);
    }
}
if($categoriaEntrada != null && $arrSI)
{
    if ($categoriaEntrada == "saldoInicial")
    {
        $docRef11 = $db->collection("balances")->document($fk_idOrganismoAfiliado);
        $docRef11->set($arrSI);
    }
}

//echo "<br><br>";

$arrJson = array('balance' => $ultimoSaldo);

if(!$debug) {
    echo json_encode($arrJson);
}

//apagar empty nos recebimentos se tiver


//echo "<br><br>";

//echo "<br>";////////////////////////////////////////////////////////////////////////NÃO ESQUECER DE TIRAR
if($debug) {
    echo "<br>Tempo 10 - Fim do script de atualização do Saldo no Firebase: " . date("d/m/Y H:i:s");
}