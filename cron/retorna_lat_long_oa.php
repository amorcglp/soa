<?php 
require '../vendor/autoload.php';
use JeroenDesloovere\Geolocation\Geolocation;

if(isset($_GET['sigla'])&&isset($_GET['formato'])&&isset($_GET['siglaPais']))
{  
    $sigla = ($_GET['sigla'])?$_GET['sigla']:null;
    $siglaPais = ($_GET['siglaPais'])?$_GET['siglaPais']:null;
    $formato = ($_GET['formato'])?$_GET['formato']:null;

    if($siglaPais!=null)
    {    
        if($sigla!=null)
        {    
            if(realpath('../model/wsClass.php')){
                    require_once '../model/wsClass.php';
            }else{
                    require_once './model/wsClass.php';
            }

            // Instancia a classe
            $ws = new Ws();

            // Nome do Método que deseja chamar
            $method = 'RetornaRelacaoOA';

            // Parametros que serão enviados à chamada
            if($siglaPais!='AO')
            {    
                $params = array('CodUsuario' => 'lucianob',
                                            'IdeTipoOrganismo'=>'',
                                            'SigOrganismoAfiliado'=>$sigla,
                                            'IdeDescartarOaPorSituacao'=>'',
                                            'SigPaisOrganismo'=>$siglaPais,
                                            'SigRegiaoBrasil'=>'',
                                            'SigAgrupamentoRegiao'=>'',
                                            'NomLocali'=>''
                                            );
            }else{
                //echo 'entrou no else';
                $params = array('CodUsuario' => 'lucianob',
                                            'IdeTipoOrganismo'=>'L',
                                            'SigOrganismoAfiliado'=>'RA101',
                                            'IdeDescartarOaPorSituacao'=>'',
                                            'SigPaisOrganismo'=>'AO',
                                            'SigRegiaoBrasil'=>'',
                                            'SigAgrupamentoRegiao'=>'',
                                            'NomLocali'=>'Luanda'
                                            );
            }
            //echo '<pre>';print_r($params);
            // Chamada do método
            $return = $ws->callMethod($method, $params, 'lucianob');

            $obj = json_decode(json_encode($return),true);
//echo '<pre>'; print_r($obj);
            $endereco="";
            $numero="";
            $bairro="";
            $cidade="";
            $uf="";
            $cep="";
            $siglaPais="";
            $seqCadast='';

            //echo $obj['result'][0]['fields']['fArrayOA'][0]['fields']['fSeqCadast'];

            if($obj)
            {    
                $seqCadast = $obj['result'][0]['fields']['fArrayOA'][0]['fields']['fSeqCadast'];
                $endereco = trim($obj['result'][0]['fields']['fArrayOA'][0]['fields']['fDesLograd']);
                $numero = trim($obj['result'][0]['fields']['fArrayOA'][0]['fields']['fNumEndere']);
                $bairro = trim($obj['result'][0]['fields']['fArrayOA'][0]['fields']['fNomBairro']);
                $cidade = trim($obj['result'][0]['fields']['fArrayOA'][0]['fields']['fNomLocali']);
                $uf = trim($obj['result'][0]['fields']['fArrayOA'][0]['fields']['fSigUf']);
                $cep = trim($obj['result'][0]['fields']['fArrayOA'][0]['fields']['fCodCep']);
                $siglaPais = trim($obj['result'][0]['fields']['fArrayOA'][0]['fields']['fSigPaisEndere']);
        

                if($seqCadast!='')
                { 
                    $geo = new Geolocation();
                    $arr = $geo->getCoordinates(
                        $endereco,
                        $numero,
                        $cidade,
                        $cep,
                        $siglaPais
                    );
if($formato=='json')
{
    echo '{"seqCadast":"'.$seqCadast.'","latitude":"'.$arr['latitude'].'","longitude":"'.$arr['longitude'].'"}';
}
if($formato=='html')
{
    echo $seqCadast.'\t'.$arr['latitude'].'\t'.$arr['longitude'];
}    
                }else{
                    echo 'Erro';
                }
            }
            }else{?>
            Se faz necessário passar a sigla do OA
            <?php } ?>
        <?php } ?>
<?php } ?>