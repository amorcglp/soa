<?php 
/*
//error_reporting(E_ALL);
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
ini_set("display_errors", 1 );

include_once('../model/funcaoUsuarioClass.php');

$funcaoUsuario = new FuncaoUsuario();

$resultado = $funcaoUsuario->limpaFuncaoUsuario();
if($resultado)
{
    echo "limpou!";
}
 */
date_default_timezone_set('America/Sao_Paulo');
include_once('../lib/phpmailer/class.phpmailer.php');
$texto = '<div>
					<div dir="ltr">
						<table class="ecxbody-wrap"
							style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background: rgb(246, 246, 246);">
							<tbody>
								<tr
									style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
									<td
										style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
										valign="top"></td>
									<td class="ecxcontainer" width="600"
										style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important;"
										valign="top">
										<div class="ecxcontent"
											style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; padding: 20px;">
											<table class="ecxmain" width="100%" cellpadding="0"
												cellspacing="0"
												style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; border: 1px solid rgb(233, 233, 233); background: rgb(255, 255, 255);">
												<tbody>
													<tr
														style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
														<td class="ecxcontent-wrap"
															style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 20px;"
															valign="top"><img src="http://i.imgur.com/FKg7aai.png"
															style="text-decoration: none; width: auto; max-width: 100%; clear: both; display: block;"
															alt="Ordem Rosacruz Sol Alado" width="100%" height="100"> <br>
														<br>
														<br>
														<br>
															<div style="text-align: left;"></div>
															<table width="100%" cellpadding="0" cellspacing="0"
																style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																<tbody>
																	<tr
																		style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<td class="ecxcontent-block"
																			style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0 0 20px;"
																			valign="top">Estimado(a) <b>'.strtoupper($vetorUsu['nomeUsuario']).'</b>,</td>
																	</tr>
																	<tr
																		style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<td class="ecxcontent-block"
																			style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																			valign="top">Saudações Fraternais! </td>
																	</tr>
																	<tr
																		style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<td class="ecxcontent-block"
																			style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																			valign="top">FOI EXECUTADA A LIMPEZA DAS FUNÇÕES DOS USUÁRIOS PELA CRON limpa_funcoes_usuario.php! Horário: '.date('d/m/Y H:i:s').'.</td>
																	</tr>
																	<tr
																		style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<td class="ecxcontent-block"
																			style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																			valign="top"><font color="red">Essa é uma mensagem automática, não há necessidade de resposta!</font></td>
																	</tr>
																	<tr
																		style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<td class="ecxcontent-block"
																			style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																			valign="top">Em caso de dúvida, por favor entre em contato: atendimentoportal@amorc.org.br</td>
																	</tr>
																	<tr
																		style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
																		<td class="ecxcontent-block"
																			style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; padding: 0px 0px 20px;"
																			valign="top"> 
																			Ordem Rosacruz, AMORC – Grande Loja da Jurisdição de Língua Portuguesa<br>
	                														Rua Nicarágua, 2620, Curitiba – Paraná<br>
	                														Telefone: (+55) 041- 3351-3000</td>
																	</tr>
																</tbody>
															</table></td>
													</tr>
												</tbody>
											</table>
											<div class="footer"
												style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; padding: 20px;">
												<div style="text-align: left;"></div>
												<table width="100%"
													style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
													<tbody>
														<tr
															style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px;">
															<td class="ecxaligncenter ecxcontent-block"
																style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; text-align: center; padding: 0 0 20px;"
																align="center" valign="top">AMORC-GLP '.date('Y').'</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div style="text-align: left;"></div></td>
									<td
										style="text-align: left; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top;"
										valign="top"></td>
								</tr>
							</tbody>
						</table>
						</div>
					</div>';
					//echo $texto;
                                    
                                        
					$servidor = "smtp.office365.com";
                                        $porta = "587";
                                        $usuario = "noresponseglp@amorc.org.br";
                                        $senha = "@w2xpglp";
				
					//echo $senha;
					#Disparar email
					$mail = new PHPMailer();
					$mail->isSMTP();
					$mail->SMTPSecure = "tls";
					$mail->SMTPAuth = true;
					$mail->Host = $servidor;	// SMTP utilizado
					$mail->Port = $porta;  	
					$mail->Username = $usuario;
					$mail->Password = $senha;
					$mail->From = 'noresponseglp@amorc.org.br';
					$mail->AddReplyTo("atendimento@amorc.org.br","Antiga e Mística Ordem Rosacruz - AMORC");
					$mail->FromName = 'Atendimento - Ordem Rosacruz, AMORC - GLP';
					//$mail->AddAddress("luis.fernando@amorc.org.br");
					//$mail->AddAddress("tatiane.jesus@amorc.org.br");
					//$mail->AddAddress("lucianob@amorc.org.br");
					//$mail->AddAddress("samufcaldas@hotmail.com.br");
					//$mail->AddAddress("samuel.caldas@hotmail.com.br");
					//$mail->AddAddress("luciano.bastos@gmail.com");
					$mail->AddAddress("ti@amorc.org.br");
					//$mail->AddAddress("amigoeterno10@hotmail.com");
					//$mail->AddAddress($vetorUsu['emailUsuario']);
					$mail->isHTML(true);
					$mail->CharSet = 'utf-8';
					//$texto = "teste de mensagem da cron para o luiz e para a tati, teste da acentuação";
					$mail->Subject = 'CRON EXECUTADA - AMORC';
					$mail->Body = $texto;
					$mail->AltBody = strip_tags($texto); 
					
                                        
					$enviado = $mail->Send();
					if($enviado)
					{
                                             echo "Você fez algo errado! Essa ação pode ser executada apenas pela equipe de TI, a mesma foi avisada que cron foi executada";
					}
?>	