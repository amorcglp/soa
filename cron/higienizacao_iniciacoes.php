<?php

ini_set('max_execution_time', 0);

include_once('../lib/functions.php');
include_once('../model/organismoClass.php');
include_once('../model/atividadeIniciaticaClass.php');
include_once('../model/higienizacaoIniciacoesClass.php');
include_once('../model/higienizacaoIniciacoesLogClass.php');
include_once('../model/higienizacaoIniciacoesNotLogClass.php');
require_once '../webservice/wsInovad.php';

$atividadeIniciatica            = new atividadeIniciatica();
$higienizacaoIniciacoes         = new higienizacaoIniciacoes();
$higienizacaoIniciacoesLog      = new higienizacaoIniciacoesLog();
$higienizacaoIniciacoesNotLog   = new higienizacaoIniciacoesNotLog();
$oa                             = new organismo();

//Pesquisar todos os ids cadastrados
$arr=array();
$arrMembrosSemRegistro=array(15915,16484);

$resultado = $atividadeIniciatica->listaAtividadeIniciatica(
    235,
    null,
    null,
    4,
    0,
    2,
    true,
    $arr,
    $arrMembrosSemRegistro
    );

if ($resultado) {
    foreach ($resultado as $vetor) {

        if (
            $vetor['statusAtividadeIniciaticaMembro'] == 4

        ) {

            //Campos para inserir
            $idAtividadeMembro = $vetor['idAtividadeIniciaticaMembro'];
            echo "IdMembro=>".$idAtividadeMembro;
            $idAtividade = $vetor['fk_idAtividadeIniciatica'];
            $seqCadast = $vetor['seqCadastAtividadeIniciaticaMembro'];
            $siglaOA = $vetor['siglaOrganismoAfiliado'];
            $nome = $vetor['nomeAtividadeIniciaticaMembro'];
            $status = $vetor['statusAtividadeIniciaticaMembro'];
            $tipoObrigacao = converteGrauSOAParaOrcz($vetor['tipoAtividadeIniciatica']);//arrumar
            $descricaoLocal = $vetor['localAtividadeIniciatica'];
            $dataObrigacao = $vetor['dataRealizadaAtividadeIniciatica'];
            $horaObrigacao = $vetor['horaRealizadaAtividadeIniciatica'];
            $organismoNaoCadastrado = $siglaOA;

            $arrGrausOrcz = array();
            $arrGrausOrcz[] = "0";


            echo "<hr>Id:".$idAtividade." - OA: ".$siglaOA."<br><br>" . $nome . "<hr>";

            //Buscar Nome do Usuário vindo do ORCZ
            $vars = array('seq_cadast' => $teste);
            $resposta = json_decode(json_encode(restAmorc("membros/[" . $seqCadast . "]/ritualistico/1", $vars)), true);
            $obj2 = json_decode(json_encode($resposta), true);
            //echo "<pre>";print_r($obj2);

            if (isset($obj2['data'][$seqCadast][0])) {
                foreach ($obj2['data'][$seqCadast] as $vetor2) {
                    $grauOrcz = retornaGrauRC($vetor2['seq_tipo_obriga'])."-".substr($vetor2['dat_obriga'],0,10);
                    //echo "==>GrauOrcz:".$grauOrcz;

                    if (!in_array($grauOrcz, $arrGrausOrcz)) {

                        $arrGrausOrcz[] = $grauOrcz;

                    }
                }

            }

            echo "<br><b>Status:".$status."</b><br>";

            echo "<br>Iniciações do membro no ORCZ:";
            echo "<pre>";
            print_r($arrGrausOrcz);

            $grauSOA = retornaGrauRCSOA($vetor['tipoAtividadeIniciatica']);

            $grauSOA = $grauSOA."-".substr($vetor['dataRealizadaAtividadeIniciatica'],0,10);

            if ($grauSOA != 0) {
                if (!in_array($grauSOA, $arrGrausOrcz)) {

                    echo "--><b>Vai inserir o grau:</b> " . $grauSOA;
                    //Insere grau para o membro

                    $credentials = montaCredenciais("producao");

                    $dataObrigacao = $dataObrigacao . " " . $horaObrigacao;

                    $varsObrigacao = Array('form_params' =>
                        Array('seq_tipo_obriga' => $tipoObrigacao,
                            'sig_orgafi' => $siglaOA,
                            'des_local_obriga' => str_replace(",", "", utf8_encode($descricaoLocal)),
                            'dat_obriga' => $dataObrigacao,
                            'cod_usuari' => 'lucianob'),
                        'headers' => Array('Authorization' => 'Basic ' . $credentials),
                        'verify' => false);

                    //print_r($vars);exit();

                    $resposta = json_decode(json_encode(restAmorc("membros/[" . $seqCadast . "]/ritualistico/1", $varsObrigacao, 'POST')), true);
                    $obj3 = json_decode(json_encode($resposta), true);
                    echo "<pre>";
                    print_r($obj3);
                    //exit();

                    if ($obj3['success']) {
                        echo "<br><b>[" . $grauSOA . "]º Grau importado</b>";
                        //É preciso gerar log do que foi importado para o Orcz
                        //Pois depois eles vão querer saber se o registro foi manual ou da importação
                        $higienizacaoIniciacoesLog = new higienizacaoIniciacoesLog();
                        $higienizacaoIniciacoesLog->setFkIdAtividadeIniciatica($idAtividade);
                        $higienizacaoIniciacoesLog->setSeqCadast($seqCadast);
                        $higienizacaoIniciacoesLog->setSeqTipoObrigacao($tipoObrigacao);
                        $higienizacaoIniciacoesLog->setLocalObrigacao($descricaoLocal);
                        $higienizacaoIniciacoesLog->setDataObrigacao($dataObrigacao);
                        $higienizacaoIniciacoesLog->setRegiao(substr($organismoNaoCadastrado, 0, 3));
                        $higienizacaoIniciacoesLog->setSiglaOA($siglaOA);
                        $higienizacaoIniciacoesLog->cadastraHigienizacaoIniciacoesLog();
                    } else {
                        //Erro
                        echo "<br><b>[" . $grauSOA . "]º Erro ao importar o Grau</b>";
                        $higienizacaoIniciacoesNotLog = new higienizacaoIniciacoesNotLog();
                        $higienizacaoIniciacoesNotLog->setFkIdAtividadeIniciatica($idAtividade);
                        $higienizacaoIniciacoesNotLog->setSeqCadast($seqCadast);
                        $higienizacaoIniciacoesNotLog->setSeqTipoObrigacao($tipoObrigacao);
                        $higienizacaoIniciacoesNotLog->setLocalObrigacao($descricaoLocal);
                        $higienizacaoIniciacoesNotLog->setDataObrigacao($dataObrigacao);
                        $higienizacaoIniciacoesNotLog->setRegiao(substr($organismoNaoCadastrado, 0, 3));
                        $higienizacaoIniciacoesNotLog->setSiglaOA($siglaOA);
                        $higienizacaoIniciacoesNotLog->cadastraHigienizacaoIniciacoesNotLog();
                    }

                } else {
                    echo "<br>[" . $grauSOA . "]º Grau não importado, já tinha no ORCZ";

                }

            }

            //Registrar a Iniciação como avaliada
            $higienizacaoIniciacoes = new higienizacaoIniciacoes();
            $higienizacaoIniciacoes->setFkIdAtividadeIniciaticaMembro($idAtividadeMembro);
            $higienizacaoIniciacoes->setSeqCadast($seqCadast);
            $higienizacaoIniciacoes->setFkIdAtividadeIniciatica($idAtividade);
            $higienizacaoIniciacoes->setRegiao(substr($siglaOA,0,3));
            $higienizacaoIniciacoes->setSiglaOA($siglaOA);
            $higienizacaoIniciacoes->cadastraHigienizacaoIniciacoes();

        }

    }
}

//echo "<br>Començando higienização de complemento";

//include('higienizacao_iniciacoes_complemento.php');

echo "<br>====Higienização executada com sucesso====";
?>	
