<?php

echo "<b>Cron em produção: Preenchimento nos Arquivos do Saldo do Ano Seguinte</b><br><br>";

include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

echo "<b>Aviso: procurando popular com zero a projeção do ano seguinte para todas as projeções de todos os oas cadastrados (por segurança)</b><br><br>";

//Contar o numero de arquivos txt na pasta se bate com o numero de organismos cadastrados
$o = new organismo();
$arrOrganismos = $o->listaOrganismo();

//Pegar o ultimo id do ultimo organismo cadastrado
$idUltimoOaCadastrado = $o->getIdUltimoOrganismoCadastrado();

echo "<br><br>Id do ultimo Organismo cadastrado:".$idUltimoOaCadastrado;

//Abrir projeção do arquivo do ultimo oa cadastrado e ver até qual ano está populado
$arquivo = "../saldo/".$idUltimoOaCadastrado.".txt";//Projeção do ultimo oa cadastrado

if(file_exists ($arquivo)) {

    //Pegar ultimo ano populado

    //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
    $fp = fopen($arquivo, "r");

    //Lê o conteúdo do arquivo aberto.
    while (!feof($fp)) {
        $valor = fgets($fp, 4096);

        $arr = explode("@", $valor);
    }

    //echo "<pre>";print_r($arr);

    echo "<br><br>Total de posições do array da projeção do ultimo organismo: ".count($arr);

    $posicaoAno = count($arr)-5;

    $anoProjecao=$arr[$posicaoAno];

    echo "<br><br>Ano final da proejeção do ultimo oa cadastrado: ".$anoProjecao;

    //Verificar se esse ano é igual ao ano atual
    if($anoProjecao==(date("Y")))
    {
        $temQuePopularAnoSeguinte=true;
        echo "<br><br>Tem que popular o ano seguinte";
    }else{
        $temQuePopularAnoSeguinte=false;
        echo "<br><br>Não tem que popular o ano seguinte";
        exit();
    }
}


$pasta = '../saldo/';
$arquivos = glob("$pasta{*.txt}", GLOB_BRACE);

echo "<br>Total de Arquivos na Pasta Saldo: <b>" . count($arquivos)."</b><br>";
echo "<br>Total de Organismos cadastrados: <b>" . count($arrOrganismos)."</b><br><br>";

//Forçar que tem que popular ano seguinte apenas para teste
//$temQuePopularAnoSeguinte=true;

if($temQuePopularAnoSeguinte)
{
    if($arrOrganismos)
    {
        for ($i=0;$i<count($arrOrganismos); $i++)
        {
            //Verificar próximo organismo que não possui projeção, se não possui sair do foreach
            $arquivo = "../saldo/".$arrOrganismos[$i]['idOrganismoAfiliado'].".txt";
            if(file_exists ($arquivo)) {

                //O organismo selecionado tem ano seguinte populado?

                //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
                $fp = fopen($arquivo, "r");

                //Lê o conteúdo do arquivo aberto.
                while (!feof($fp)) {
                    $valor = fgets($fp, 4096);

                    $arr2 = explode("@", $valor);
                }

                $posicaoAno2 = count($arr2)-5;

                $anoProjecao2=$arr2[$posicaoAno2];

                echo "<br><br>Ano final da proejeção do organismo selecionado no looping: ".$anoProjecao2;

                //Verificar se esse ano é igual ao ano atual
                if($anoProjecao2==(date("Y"))) {
                    $idOrganismoAfiliado = $arrOrganismos[$i]['idOrganismoAfiliado'];
                    $i = count($arrOrganismos);//Forçar sair do for
                }
            }

            //Verificação se o script percorreu todos os organismos e não encontrou mais nada para alterar
            $z = $i+1;
            $maisNadaParaAlterar=false;
            if($z==count($arrOrganismos))
            {
                $maisNadaParaAlterar=true;
            }
        }
    }

    if($maisNadaParaAlterar==true)
    {
        echo "<br><br><b>Verificou-se que não há mais nada para alterar!</b>";
        //exit();
    }

    //Testes: forçar alteração do arquivo 298 - PR114 - GRANDE LOJA apenas para ver resultado
    /*
    $maisNadaParaAlterar=false;
    $idOrganismoAfiliado=298;
    $anoProjecao2=2020;*/

    if($maisNadaParaAlterar==false)
    {
        //De posse do id do organismo acrescentar proximo ano

        $arr3 = $o->retornaSiglaOa($idOrganismoAfiliado);
        echo "<br>Proximo id a popular proximo ano:" . $arr3['id'];
        echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr3['id']);
        echo "<br>Sigla:" . $arr3['sigla'];
        echo "<br>Ano final da projeção:" . $anoProjecao2;

        /*
         * Preparar as variáveis para a projeção
         */

        $ano=$anoProjecao2;
        $anoAtual=$anoProjecao2+1;
        $mes=1;

        while ($ano <= $anoAtual) {
            //echo "<br>data=>".$mes."-".$ano;
            if ($mes <= 12) {

                //Popular zero para cada mês
                $saldoMesesAnteriores = 0;
                $totalEntradasDoMes = 0;
                $totalSaidasDoMes = 0;
                $saldoMes = 0;

                if ($mes == 12 && $ano == $anoAtual) {
                    $texto .= $mes . "@" . $ano . "@" . $saldoMesesAnteriores . "@" . $totalEntradasDoMes . "@" . $totalSaidasDoMes . "@" . $saldoMes;
                } else {
                    $texto .= $mes . "@" . $ano . "@" . $saldoMesesAnteriores . "@" . $totalEntradasDoMes . "@" . $totalSaidasDoMes . "@" . $saldoMes . "@";
                }

                $mes++;
            } else {
                $ano++;
                $mes = 1;
            }
        }

        echo "<br><br>" . $texto . "<br>";

        /*
         * Ler arquivo atual e colocar tudo na string 1
         */

        $arquivo = "../saldo/" . $idOrganismoAfiliado . ".txt";

        $fp = fopen($arquivo, "r");

        //Lê o conteúdo do arquivo aberto.
        while (!feof($fp)) {
            $valor = fgets($fp, 4096);

            $string1 = $valor;
        }

        //Unir tudo...
        $textoFinal = $string1.$texto;

        echo "<br><br>String final:".$textoFinal;

        /*
         * Criar de fato arquivo com a projeção
         */

        $arquivo = "../saldo/" . $idOrganismoAfiliado . ".txt";

        //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
        $fp = fopen($arquivo, "w+");

        //Escreve no arquivo aberto.
        fwrite($fp, $textoFinal);

        //Fecha o arquivo.
        fclose($fp);

        //Verificação se o arquivo existe
        if(file_exists ($arquivo)) {
            echo "<br><b>Arquivo existe na pasta [saldo] com projeção para o ano seguinte!</b><br><br>";
        }
    }else{
        echo "<br><br><b>=======================A cron terminou o preenchimento de todas as projeções para o ano seguinte=======================</b>";
    }


}


?>