<?php

function atualizaFuncao($codigoFuncao,$tipo)
{
    $retorno = "";

    //Conversão de funções de capítulo
    if($tipo=="Capítulo")
    {
        switch ($codigoFuncao) {
            case '201':
                $retorno = 240;
                break;
            case '203':
                $retorno = 242;
                break;
            case '207':
                $retorno = 244;
                break;
            case '209':
                $retorno = 246;
                break;
            case '211':
                $retorno = 248;
                break;
            case '213':
                $retorno = 250;
                break;
            case '230':
                $retorno = 252;
                break;
        }
    }

    //Conversão de funções de pronaos
    if($tipo=="Pronaos")
    {
        switch ($codigoFuncao) {
            case '201':
                $retorno = 260;
                break;
            case '203':
                $retorno = 262;
                break;
            case '207':
                $retorno = 264;
                break;
            case '230':
                $retorno = 266;
                break;
        }
    }

    //Conversão de funções de capítulo
    if($tipo=="Loja")
    {
        switch ($codigoFuncao) {
            case '201':
                $retorno = 280;
                break;
            case '203':
                $retorno = 282;
                break;
            case '207':
                $retorno = 284;
                break;
            case '209':
                $retorno = 286;
                break;
            case '211':
                $retorno = 288;
                break;
            case '213':
                $retorno = 290;
                break;
            case '230':
                $retorno = 292;
                break;
        }
    }

    if($retorno=="") {
        $retorno = $codigoFuncao;
    }

    return $retorno;

}

require_once '../webservice/wsInovad.php';
require_once '../model/funcaoClass.php';
require_once '../model/funcaoUsuarioClass.php';
require_once '../model/usuarioClass.php';
require_once '../conexaoOrcz.php';

$x=1;
$continua=true;
$limit=20;

//Selecionar todos os membros oficiais administrativos
$seq_tipos_funcoes=200;
$vars = array('seq_tipo_funcoes' => '['.$seq_tipos_funcoes.']');
$resposta = json_decode(json_encode(restAmorc("membros/oficiais",$vars)),true);
$obj = json_decode(json_encode($resposta),true);


if(isset($obj['data'][0]))
{
  $totalPagFinal = ceil($obj['total'] % $limit);

  while ($x <= $totalPagFinal && $continua == true) {

          foreach($obj['data'] as $vetor)
          {
            $seqCadast = $vetor['seq_cadast'];

            $arrFuncoes = $vetor['funcoes'];

            $siglaOrganismoLotacao="";


            foreach($arrFuncoes as $vetor10) {

                    if ($vetor10['seq_tipo_funcao_oficial'] == 200)//Selecionar apenas dignitários
                    {

                        if ($vetor10['dat_saida'] == 0)//Verificar apenas campos que a data de saida não foi cadastrada
                        {
                            $siglaOrganismoWS = $vetor10['oa']['sig_orgafi'];
                            $siglaOA = "";
                            $vars3 = array('sig_orgafi' => $siglaOrganismoWS);
                            $resposta3 = json_decode(json_encode(restAmorc("oa", $vars3)), true);
                            $obj3 = json_decode(json_encode($resposta3), true);


                            if (isset($obj3['data'][0]['nom_client'])) {
                                $siglaOA = $obj3['data'][0]['sig_orgafi'];
                                $dbTipo = $obj3['data'][0]['ide_tipo_oa'];
                                $seqCadastOa = $obj3['data'][0]['seq_cadast'];
                            } else {
                                $status = 0;
                            }

                            switch ($dbTipo) {
                                case 'L':
                                    $tipo = 'Loja';
                                    break;
                                case 'P':
                                    $tipo = 'Pronaos';
                                    break;
                                case 'C':
                                    $tipo = 'Capítulo';
                                    break;
                                case 'A':
                                    $tipo = 'Atrium';
                                    break;
                                case 'H':
                                    $tipo = 'Heptada';
                                    break;
                                default:
                                    echo "Tipo não encontrado";
                                    exit();
                                    break;
                            }

                            $dataTerminoMandato = $vetor10['dat_termin_mandat'];
                            $codFuncao = $vetor10['funcao']['seq_funcao'];


                            $tipoCargo = $vetor10['funcao']['seq_tipo_funcao_oficial'];
                            $dataEntrada = $vetor10['dat_entrad'];
                            $desFuncao = substr($vetor10['funcao']['des_funcao'], 0, 3);


                            $data1_inteiro = strtotime($dataTerminoMandato);
                            $data2_inteiro = strtotime(date("Y-m-d"));

                            if ($desFuncao != "EX-" && ($dbTipo == "L" || $dbTipo == "C" || $dbTipo == "P")) {

                                if ($data1_inteiro >= $data2_inteiro) {


                                    $codFuncaoAtualizada = atualizaFuncao($codFuncao, $tipo);

                                    $sql = " UPDATE OA_OFICIAL SET seq_funcao = :codFuncaoAtualizada
                                      WHERE seq_cadast_membro = :seqCadast
                                      and	seq_cadast_oa = :seqCadastOa 
                                      and	dat_entrad = :dataEntrada ";

                                    $dataEntrada = str_replace(" ","T",substr($dataEntrada,0,19));

                                    echo "<br>".$codFuncaoAtualizada;
                                    echo "<br>".$seqCadast;
                                    echo "<br>".$seqCadastOa;
                                    echo "<br>".$dataEntrada;

                                    $c = new conexaoSOAORCZ();
                                    $con = $c->openSOA();
                                    //$sth = $con->prepare($sql);
                                    $c->checkStatus();

                                    $codFuncaoAtualizada=201;

                                    $sth->bindParam(':codFuncaoAtualizada', $codFuncaoAtualizada, PDO::PARAM_INT);
                                    $sth->bindParam(':seqCadast', $seqCadast, PDO::PARAM_INT);
                                    $sth->bindParam(':seqCadastOa', $seqCadastOa, PDO::PARAM_INT);
                                    $sth->bindParam(':dataEntrada', $dataEntrada, PDO::PARAM_STR);


                                    $resultado = $c->run($sth,true);
                                    if ($resultado){
                                        echo "atualizou".$seqCadast;
                                    }else{
                                        echo "não atualizou".$seqCadast;
                                    }

                                    exit();

                                    //Verificar se ele tem cadastro no SOA
                                    $u = new Usuario();
                                    if ($u->temUsuario($seqCadast)) {
                                        //Selecionar idFuncao conforme vinculoExterno
                                        $f = new funcao();
                                        $idFuncao = $f->buscaIdFuncaoPorVinculoExterno($codFuncaoAtualizada);

                                        //Se tiver cadastro Atualizar funções na tabela do SOA
                                        $fu = new FuncaoUsuario();
                                        $fu->fk_seq_cadast = $seqCadast;
                                        $fu->removePorUsuario();

                                        $fu = new FuncaoUsuario();
                                        $fu->fk_idFuncao = $idFuncao;
                                        $fu->fk_seq_cadast = $seqCadast;
                                        $fu->dataInicioMandato = $dataEntrada;
                                        $fu->dataFimMandato = $dataTerminoMandato;
                                        $fu->siglaOA = $siglaOA;
                                        $fu->cadastra();
                                    }

                                }
                            }
                        }
                    }
                }
            }

      $x++;
      $vars = array('seq_tipo_funcoes' => '['.$seq_tipos_funcoes.']','limit' => $limit,'page' => $x);
      $resposta = json_decode(json_encode(restAmorc("membros/oficiais",$vars)),true);
      $obj = json_decode(json_encode($resposta),true);
      if(isset($obj['data'][0]))
      {
        $continua=true;
      }else{
        $continua=false;
      }
 }
}

