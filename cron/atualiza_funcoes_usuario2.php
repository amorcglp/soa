<?php 
ini_set('max_execution_time', 0);
session_start();
$tokenLiberado=true;
ini_set("error_reporting", E_ALL & ~E_DEPRECATED);
$arrData = array();
$arrData['key'] = 777;
$logged=true;
$naoSetarCookie=true;
require_once ('../sec/make.php');
$_SESSION['tk']= $token;
$_SESSION['vk']= $arrData['key'];

if(realpath('../../model/wsClass.php')){
	require_once '../../model/wsClass.php';
}else{
	if(realpath('../model/wsClass.php')){
		require_once '../model/wsClass.php';	
	}else{
		require_once './model/wsClass.php';
	}
}

$server = 135;
//error_reporting(E_ALL);
ini_set("display_errors", 1 );

$hoje = date("Y-m-d")."T00:00:00";

include_once('../lib/functions.php');
include_once('../model/organismoClass.php');
include_once('../model/usuarioClass.php');
include_once('../model/funcaoClass.php');
include_once('../model/funcaoUsuarioClass.php');

$funcaoUsuario = new FuncaoUsuario();

$total=0;//Contador de relatórios NÃO entregues

//Procurar mestres cadastrados no sistema para este organismo
$usuario = new Usuario();

$start = isset($_GET['start'])?$_GET['start']:null;
$limit = isset($_GET['limit'])?$_GET['limit']:null;

$resultadoUsu = $usuario->listaUsuario(null,null,null,null,null,null,true,null,null,null,$start,$limit);//Todos os ativos
$m=1;
if($resultadoUsu)
{
        foreach($resultadoUsu as $vetorUsu)
        {
            
                $seqCadast = $vetorUsu['seqCadast'];
                
                //Buscar Pais do Usuário vindo do ORCZ

                $ws = new Ws($server);

                // Nome do Método que deseja chamar
                $method = 'RetornaDadosMembroPorSeqCadast';

                // Parametros que serão enviados à chamada
                $params = array('CodUsuario' => 'lucianob',
                                                'SeqCadast' => $seqCadast);

                // Chamada do método
                $return = $ws->callMethod($method, $params, 'lucianob');

                $obj2 = json_decode(json_encode($return),true);

                $siglaPais   = $obj2['result'][0]['fields']['fSeqCadast'] != 0 ? $obj2['result'][0]['fields']['fSigPaisEndereco'] : "BR";

                //Limpar funções do usuário
                $funcaoUsuario = new FuncaoUsuario();
                $funcaoUsuario->setFk_seq_cadast($seqCadast);
                $funcaoUsuario->removePorUsuario();

                //Recuperar dados da Função
                $naoAtuantes='N';
                $atuantes='S';
                $ocultar_json=1;

                //$seqCadast=$seqCadast;
                $tipoMembro=1;//Apenas fun��es R+C
                
                $seqCadast              = isset($seqCadast)?$seqCadast:0;
                $siglaOA	 	= isset($siglaOA)?$siglaOA:'';
                $atuantes		= isset($atuantes)?$atuantes:'S';
                $naoAtuantes            = isset($naoAtuantes)?$naoAtuantes:'N';
                $seqFuncao		= isset($seqFuncao)?$seqFuncao:0;
                $tipoMembro		= isset($tipoMembro)?$tipoMembro:'';
                $nomeParcialFuncao = isset($nomeParcialFuncao)?$nomeParcialFuncao:'';
                
                // Instancia a classe
                $ws = new Ws($server);
                // Nome do Método que deseja chamar
                $method = 'RetornaRelacaoOficiais';

                // Parametros que serão enviados à chamada
                $params = array('CodUsuario' => 'lucianob',
                                                'SeqCadast' => $seqCadast,
                                                'SeqFuncao' => $seqFuncao,
                                                'NomParcialFuncao' => $nomeParcialFuncao,
                                                'NomLocaliOficial' => '',
                                                'SigRegiaoBrasilOficial' => '',
                                                'SigPaisOficial' => $siglaPais,
                                                'SigOrganismoAfiliado' => $siglaOA,
                                                'SigAgrupamentoRegiao' => '',
                                                'IdeListarAtuantes' => $atuantes,
                                                'IdeListarNaoAtuantes' => $naoAtuantes,
                                'IdeTipoMembro' => $tipoMembro 
                                        );

                // Chamada do método
                $return = $ws->callMethod($method, $params, 'lucianob');
                $obj = json_decode(json_encode($return),true);
                //echo "<pre>";print_r($obj);exit();
                $arrFuncoes = $obj['result'][0]['fields']['fArrayOficiais'];
                //echo "<pre>";print_r($arrFuncoes);exit();

                /**
                 * Priorização de lotação para funções administrativas
                 */
                //Varrer funções priorizando lotação para funções administrativas
                $siglaOrganismoLotacao="";

                foreach($arrFuncoes as $vetor)
                {
                    if($vetor['fields']['fCodTipoFuncao']==100)//Se tiver dignitária priorizar lotação
                    {
                        $siglaOrganismoLotacao = $vetor['fields']['fSigOrgafi'];
                    }    
                }
                //Se não conseguiu encontrar uma lotação para função dignitária pegar administrativa
                if($siglaOrganismoLotacao=="")
                {
                    foreach($arrFuncoes as $vetor)
                    {
                        if($vetor['fields']['fCodTipoFuncao']==200)//Se tiver administrativa priorizar lotação
                        {
                            $siglaOrganismoLotacao = $vetor['fields']['fSigOrgafi'];
                        }    
                    }
                }
                //Se não conseguiu encontrar uma lotação para função administrativa então pegar de qualquer outro tipo
                if($siglaOrganismoLotacao=="")
                {
                    foreach($arrFuncoes as $vetor)
                    {
                        $siglaOrganismoLotacao = $vetor['fields']['fSigOrgafi'];
                    }
                }

                //echo "<br>Sigla atualizada:".$siglaOrganismoLotacao;exit();

                foreach($arrFuncoes as $vetor)
                {
                        if($vetor['fields']['fDatSaida']==0)//Verificar apenas campos que a data de saida não foi cadastrada
                        {
                                $dataTerminoMandato = $vetor['fields']['fDatTerminMandat'];
                                $codFuncao = $vetor['fields']['fCodFuncao'];
                                $tipoCargo = $vetor['fields']['fCodTipoFuncao'];
                                $dataEntrada = $vetor['fields']['fDatEntrad'];
                                $desFuncao = substr($vetor['fields']['fDesFuncao'],0,3);
                                $codFuncaoEx = $vetor['fields']['fCodFuncao'];
                                $siglaOrganismoWS = $vetor['fields']['fSigOrgafi'];
                                $codFuncaoEx++;
                                $data1_inteiro = strtotime("+40 days",strtotime(substr($dataTerminoMandato,0,10)));
                                //$data1_inteiro = strtotime(substr($dataTerminoMandato,0,10));
                                $data2_inteiro = strtotime(date("Y-m-d"));

                                if($desFuncao!="EX-")
                                {                                                        
                                    if($data1_inteiro>=$data2_inteiro)
                                    {	
                                        //echo "<br><br>data1TerminoMandato:".date_format($data1_inteiro, "d/m/Y")."<br><br>";
                                        //echo "<br><br>data2Hoje:".date_format($data2_inteiro, "d/m/Y")."<br><br>";
                                        if(empty($vetor['fields']['fDatSaida']))
                                        {    			
                                            $f = new funcao();								
                                            $f->setVinculoExterno($codFuncao);
                                            $retorno = $f->buscaVinculoExterno();
                                            if($retorno)
                                            {    
                                                foreach ($retorno as $vetor2)
                                                {
                                                    $fu = new funcaoUsuario();
                                                    $fu->setFk_seq_cadast($seqCadast);
                                                    $fu->setFk_idFuncao($vetor2['idFuncao']);
                                                    $fu->setDataInicioMandato(substr($dataEntrada,0,10));
                                                    $fu->setDataFimMandato(substr($dataTerminoMandato,0,10));
                                                    $fu->setSiglaOA($siglaOrganismoWS);
                                                    if(!$fu->verificaSeJaExiste())
                                                    {
                                                        if($fu->cadastra())
                                                        {    
                                                            echo "<br>Funcao ".$vetor2['nomeFuncao']." foi adicionada para o usuário do SOA"; 
                                                            echo "<br>".$m;
                                                            $m++;
                                                        }
                                                    }
                                                }
                                            }    
                                        }
                                    }
                                }
                        }
                }

                //Atualizado a lotação do usuario
                if($siglaOrganismoLotacao!="")
                {    
                    $usua = new Usuario();
                    $usua->setSeqCadast($seqCadast);
                    $usua->setSiglaOA($siglaOrganismoLotacao);
                    $resp = $usua->alteraLotacaoUsuario();
                }
        }
}

echo "<br><br><b>Cron executada com sucesso!</b>";

?>	