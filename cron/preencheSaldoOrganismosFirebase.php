<?php

$usuario = "550110";//Saldo Calculado e Informado como string por Braz Alves

$db = include('../firebase.php');

$arrDocumentosFirebase=array();
$totalDocumentosAtualizados=0;

include_once('../lib/functions.php');
include_once('../model/saldoInicialClass.php');
include_once('../model/organismoClass.php');
include_once('../model/recebimentoClass.php');
include_once('../model/despesaClass.php');

//$idOrganismoAfiliado = $idNaoCadastrado;
//$idOrganismoAfiliado = 1;//Teste com Loja CURITIBA
//$idOrganismoAfiliado = 81;//Teste com Loja São Paulo
//$idOrganismoAfiliado = 157;//Teste com Loja Rio de Janeiro
//$idOrganismoAfiliado = 136;//Teste com Loja BH
//$idOrganismoAfiliado = 70;//Teste com Blumenal

$idOrganismoAfiliado = isset($_REQUEST['idOrganismoAfiliado']) ? $_REQUEST['idOrganismoAfiliado']:null;//Organismo alvo: 160

if($idOrganismoAfiliado==null)
{
    //echo "<br>Entrou para pegar foco";

    $arrOas=array();

    /*

    $arrFoco=array(
            3,
            4,
            5,
            6,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            37,
            39,
            50,
            51,
            52,
            53,
            54,
            57,
            59,
            60,
            61,
            62,
            63,
            64
    );
    */
/*
    $arrFoco=array(
        1
    );
*/

    //Organismos Problema 297 NAZARE E 23 CASCAVEL

    $arrFoco=array(
        297,
        23
    );

    //Levam mais de 8 minutos
    /*
    $arrOas=array(
        170,
        183,
        89,
        234,
        117,
        9,
        268,
        205,
        146,
        114,
        181,
        162,
        164,
        36,
        4,
        88,
        112,
        180,
        30,
        179,
        94,
        277,
        104,
        165,
        160,
        50,
        16,
        176,
        136,
        83,
        140,
        157
    );
    */
    echo "<br>Array Foco: ";print_r($arrFoco);

    foreach ($arrFoco as $keyF => $f)
    {
        //Realizar get no firebase para ver quais organismos ainda não foram populados
        $documentsFirebase = $db->collection("balances/" . $f . "/balance")->documents();
        //echo "<br>Pegou os documentos de saldo existentes para o organismo";

        foreach ($documentsFirebase as $documentProjecao) {
            if ($documentProjecao->exists()) {
                $arrOas[] = $f;//Guardar oas que possuem saldo
                break;
            }else{
                echo "<br><br>Não existe nenhum organismo com projeção no firebase. Fim do script!";exit;
            }
        }
    }



    $arrFinalParaDecidir=array();
    foreach ($arrFoco as $v)
    {
        if(!in_array($v,$arrOas))//O oa foco não pode estar no firebase
        {
            $arrFinalParaDecidir[] = $v;
        }

    }

    echo "Array para Decidir:<pre>";print_r($arrFinalParaDecidir);

    $o = new organismo();
    if(count($arrFinalParaDecidir)>0) {
        echo "<br>Vai puxar o id do proximo oa";
        $idOrganismoAfiliado = $o->retornaOaNaoCadastradoFirebase($arrFinalParaDecidir);
    }
}

echo "<br>idOaParaPopular=>" . $idOrganismoAfiliado;

//exit;

if(count($arrFinalParaDecidir)>0) {



    //exit;

    echo "<br>Criando a colection balance";

    $dataArray =
        [
            'empty' => 0
        ];

    $docRef2 = $db->collection("balances/" . $idOrganismoAfiliado . "/balance")->newDocument();
    $docRef2->set($dataArray);
    $idB = $docRef2->id();//Guardar id do empty

    $o = new organismo();
    $arr = $o->retornaSiglaOa($idOrganismoAfiliado);
    $idOrganismoAfiliado = $arr['id'];
    echo "<br>Id do Oa Atualizado:" . $arr['id'];
    echo "<br>Nome:" . retornaNomeCompletoOrganismoAfiliado($arr['id']);
    echo "<br>Sigla:" . $arr['sigla'];


    //De posse do id do organismo seguir fazendo a projeção

    /*
     * Preparar as variáveis para a projeção
     */

    $anoAtual = date('Y') + 1;
    $mesAtual = date('m');

    $r = new Recebimento();
    $d = new Despesa();

    /*
     * Calcular saldo do mês anterior
     */

    //Encontrar Saldo e Data Inicial
    $si = new saldoInicial();

    $mes = intval($si->getMesSaldoInicial($idOrganismoAfiliado));
    $ano = $si->getAnoSaldoInicial($idOrganismoAfiliado);
    $saldoInicial = $si->getValorSaldoInicial($idOrganismoAfiliado);

    $saldoMesesAnteriores = $saldoInicial;

    $temSaldoInicial = $si->temSaldoInicial($idOrganismoAfiliado);

    echo "<br>Saldo Meses Anteriores:" . $saldoMesesAnteriores;

    //Tratamento para inserir tudo string no firebase
    $idOrganismoAfiliadoString = (string)$idOrganismoAfiliado;

    echo "<br>Tempo 4 - Início do Looping: " . date("d/m/Y H:i:s");

    if ($temSaldoInicial) {

        //Pegar todos os registros de recebimentos do organismo no SOA
        /*
        $r = new Recebimento();
        $arrRecebimentos = $r->listaRecebimento(null,null,$idOrganismoAfiliado);
        echo "<br>Tempo 2 - Pegou todos os registros de Recebimentos do SOA: ".date("d/m/Y H:i:s");
        exit();*/

        //Cadastro da Projeção
        while ($ano <= $anoAtual) {
            //echo "<br>data=>".$mes."-".$ano;
            if ($mes <= 12) {

                $arr = retornaSaldoRetornoArray($r, $d, $mes, $ano, $idOrganismoAfiliado, $saldoMesesAnteriores);

                //echo "<br>Array Saldo:<pre>";print_r($arr);

                //Cadastro da projeção no firebase
                $mesInput = (string)str_pad($mes, 2, '0', STR_PAD_LEFT);
                $anoInput = (string)$ano;

                # [START fs_set_document]
                $dataArrayComSaldo =
                    [
                        'fk_idOrganismoAfiliado' => $idOrganismoAfiliadoString,
                        'month' => $mesInput,
                        'year' => $anoInput,
                        'previousBalance' => number_format($arr['saldoAnterior'], 2, '.', ''),
                        'totalEntries' => number_format($arr['totalEntradasDoMes'], 2, '.', ''),
                        'totalOutputs' => number_format($arr['totalSaidasDoMes'], 2, '.', ''),
                        'monthBalance' => number_format($arr['saldoMes'], 2, '.', ''),
                        'userBalance' => $usuario,
                        'updateData' => date('d/m/Y H:i:s')
                    ];

                echo "<br>Enviou para o Firebase (".$mesInput."/".$mesInput."):<pre>";print_r($dataArrayComSaldo);

                $docRef2 = $db->collection("balances/" . $idOrganismoAfiliado . "/balance")->newDocument();
                $docRef2->set($dataArrayComSaldo);

                $totalDocumentosAtualizados++;

                $saldoMesesAnteriores = $arr['saldoMes'];
                $mes++;

                //echo "<br>fim cadastro";


            } else {
                $ano++;
                $mes = 1;
            }
        }
        //echo "<br><br>Finalização do looping";
    } else {

        echo "<br><b>Organismo sem saldo inicial cadastrado!</b>";

        //Iniciar mês e ano com o inicio do sistema
        $mes = "01";
        $ano = "2016";

        /*
         * Montar projeção
         */

        while ($ano <= $anoAtual) {

            if ($mes <= 12) {

                $dataArray =
                    [
                        'fk_idOrganismoAfiliado' => $idOrganismoAfiliadoString,
                        'month' => $mes,
                        'year' => $ano,
                        'previousBalance' => 0,
                        'totalEntries' => 0,
                        'totalOutputs' => 0,
                        'monthBalance' => 0,
                        'user' => $usuario
                    ];

                $docRef2 = $db->collection("balances/" . $idOrganismoAfiliado . "/balance")->newDocument();
                $docRef2->set($dataArray);

                $totalDocumentosAtualizados++;

                $mes++;

            } else {

                $ano++;
                $mes = 1;

            }
        }

    }

    echo "<br>Tempo 5 - Fim do Looping: " . date("d/m/Y H:i:s");

    echo "<br>===================================================";
    echo "<br><br><b>Total de Documentos Atualizados no Firebase:</b>" . $totalDocumentosAtualizados;

    echo "<br>===================================================";
    echo "<br><br><b>Ultimo saldo registrado para esse organismo:</b>" . $dataArrayComSaldo['monthBalance'];

    echo "<br>===================================================";
    echo "<br>Apagar arquivos empty:";
    $db->collection("balances/" . $idOrganismoAfiliado . "/balance")->document($idB)->delete();

    echo "<br>Tempo 6 - Fim da execução da Cron: " . date("d/m/Y H:i:s");
}else{
    echo "<br>Não existem Oas para enviar saldo para o Firebase";
}


?>