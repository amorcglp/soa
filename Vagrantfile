Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"
  config.vm.network "forwarded_port", guest: 80, host: 8081
  config.vm.network "forwarded_port", guest: 3306, host: 33060    # MariaDB
  config.vm.network "forwarded_port", guest: 443, host: 44300     # HTTPS
  config.vm.synced_folder ".", "/vagrant"
  config.vm.provision "shell", inline: <<-SCRIPT
    # Box para ambiente de desenvolvimento com PHP 7.1/7.0/5.6, MariaDB 10.0, Apache 2.4

  	### Nome do projeto
  	APP_NAME=soa.dev

    ### Defina aqui a versão do PHP
    # opções válidas: 7.1, 7.0, 5.6
    PHP_VERSION=5.6

    ### Versão do phpMyAdmin
    PHPMYADMIN_VERSION=4.7.2

    ### NodeJS/NPM
    # opções válidas: 8, 6
    #NODE_VERSION=6

    ### Banco de dados
    DB_NAME=soa

    ENCODING=(utf8 utf8_unicode_ci)
    #ENCODING=(utf8mb4 utf8mb4_unicode_ci)

    ### AVISO: não é necessário editar daqui pra baixo!

    export DEBIAN_FRONTEND=noninteractive

    # SSL
    PATH_SSL=/etc/apache2/ssl
    PATH_CNF=${PATH_SSL}/$APP_NAME.cnf
    PATH_KEY=${PATH_SSL}/$APP_NAME.key
    PATH_CRT=${PATH_SSL}/$APP_NAME.crt

    # Arquivo de log
    LOGFILE=/vagrant/install-log.txt

    # Extensões para o PHP
    PHP_EXT="php${PHP_VERSION}-mcrypt php${PHP_VERSION}-mysql php${PHP_VERSION}-curl php${PHP_VERSION}-mbstring php${PHP_VERSION}-soap php${PHP_VERSION}-xml php${PHP_VERSION}-zip php-imagick"

    # Módulos do Apache
    APACHE_MOD="rewrite ssl proxy_fcgi setenvif"

    # Função para imprimir informações na tela e para a execução de comandos
    _exec() {
      local msg="$1"
      shift 1

      if [ $# -gt 1 ]; then
        local proc=1
        for cmm; do
          _exec "$msg (${proc}/$#)" "$cmm"
          let proc++
        done
      else
        echo -n "${msg}..."
        echo -e "\n# ${msg}\n>>> $*\n" >> $LOGFILE
        echo "$(eval $* &>>$LOGFILE && echo "ok!" || echo "falhou!")"
      fi
    }

    #
    # !EXECUÇÃO DO SCRIPT!
    #

    # Limpa o arquivo de log
    test -f $LOGFILE && rm -f $LOGFILE

    _exec "Adicionado repositórios úteis" \
      "add-apt-repository -y ppa:ondrej/php" \
      "apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8" \
      "add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirror.ufscar.br/mariadb/repo/10.0/ubuntu xenial main'"

    _exec "Atualizando pacotes" "apt-get update"

    _exec "Instalando Apache" "apt-get -y install apache2"

  	_exec "Configurando Apache" \
  		"sed -i 's/www-data$/ubuntu/g' /etc/apache2/envvars" \
  		"chown -R ubuntu:ubuntu /var/log/apache2"

  	test -f /etc/apache2/sites-available/$APP_NAME.conf || {
  		cat << EOF > /etc/apache2/sites-available/$APP_NAME.conf
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName $APP_NAME
	ServerAlias www.$APP_NAME
	DocumentRoot /vagrant

	<Directory /vagrant>
    Options All
 		AllowOverride All
 		Require all granted
	</Directory>

	ErrorLog \\${APACHE_LOG_DIR}/${APP_NAME}-error.log
	CustomLog \\${APACHE_LOG_DIR}/${APP_NAME}-access.log combined
</VirtualHost>
EOF

  		_exec "Finalizando configuração do Apache" \
  			"a2dissite 000-default" \
  			"a2ensite $APP_NAME"

  	}

    test -f /etc/apache2/sites-available/$APP_NAME-ssl.conf || {
      test -d $PATH_SSL || mkdir $PATH_SSL
      cat << EOF > $PATH_CNF
[ req ]
prompt = no
default_bits = 2048
default_keyfile = $PATH_KEY
encrypt_key = no
default_md = sha256
distinguished_name = req_distinguished_name
x509_extensions = v3_ca

[ req_distinguished_name ]
O=Vagrant
C=UN
CN=$APP_NAME

[ v3_ca ]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alternate_names

[ alternate_names ]
DNS.1 = $APP_NAME
EOF

    cat << EOF > /etc/apache2/sites-available/$APP_NAME-ssl.conf
<IfModule mod_ssl.c>
    <VirtualHost *:443>

        ServerAdmin webmaster@localhost
        ServerName $APP_NAME
        ServerAlias www.$APP_NAME
        DocumentRoot /vagrant

        <Directory /vagrant>
          Options All
       		AllowOverride All
       		Require all granted
      	</Directory>

        ErrorLog \\${APACHE_LOG_DIR}/error.log
        CustomLog \\${APACHE_LOG_DIR}/access.log combined

        SSLEngine on

        SSLCertificateFile      $PATH_CRT
        SSLCertificateKeyFile   $PATH_KEY

        <FilesMatch ".(cgi|shtml|phtml|php)$">
            SSLOptions +StdEnvVars
        </FilesMatch>
        <Directory /usr/lib/cgi-bin>
            SSLOptions +StdEnvVars
        </Directory>

        BrowserMatch "MSIE [2-6]" \
            nokeepalive ssl-unclean-shutdown \
            downgrade-1.0 force-response-1.0
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

    </VirtualHost>
</IfModule>
EOF

    _exec "Criando certificado" \
      "sed -i '/copy_extensions\\ =\\ copy/s/^#\\ //g' /etc/ssl/openssl.cnf" \
      "openssl genrsa -out $PATH_KEY 2048" \
      "openssl req -new -x509 -config $PATH_CNF -out $PATH_CRT -days 365" \
      "a2ensite $APP_NAME-ssl"

    }

  	_exec "Preparando configuração para MariaDB" \
      	"echo 'mariadb-server-10.0 mysql-server/data-dir select \\"\\"' | debconf-set-selections" \
      	"echo 'mariadb-server-10.0 mysql-server/root_password password' | debconf-set-selections" \
  		  "echo 'mariadb-server-10.0 mysql-server/root_password_again password' | debconf-set-selections"

  	_exec "Instalando MariaDB" "apt-get -y install mariadb-server"

  	_exec "Configurando MariaDB" \
        "mysql -u root -e 'GRANT ALL ON *.* TO root@\\"0.0.0.0\\" IDENTIFIED BY \\"root\\" WITH GRANT OPTION;'"

    _exec "Criando banco de dados" \
      "mysql -u root -e 'create database if not exists ${DB_NAME} character set ${ENCODING[0]} collate ${ENCODING[1]};'"

    test -d /vagrant/provision-sql && {
      for sql in /vagrant/provision-sql/*.sql; do
        _exec "Inserindo dados de '$(basename $sql .sql)'" \
          "mysql -u root ${DB_NAME} -e 'source $sql';"
      done
    }

    _exec "Instalando PHP ${PHP_VERSION}" "apt-get -y install php${PHP_VERSION} php${PHP_VERSION}-fpm libapache2-mod-php${PHP_VERSION}"
    _exec "Instalando extensões para PHP ${PHP_VERSION}" "apt-get -y install $PHP_EXT"

    _exec "Configurando PHP para o modo de desenvolvimento" \
        "sed -i 's/error_reporting = .*/error_reporting = E_ALL/' /etc/php/${PHP_VERSION}/apache2/php.ini" \
        "sed -i 's/display_errors = .*/display_errors = On/' /etc/php/${PHP_VERSION}/apache2/php.ini"

    _exec "Ativando módulos do Apache" "a2enmod $APACHE_MOD"

    _exec "Finalizando configuração do Apache" "a2enconf php${PHP_VERSION}-fpm"

    test -d /vagrant/phpmyadmin || {

      _exec "Baixando phpMyAdmin $PHPMYADMIN_VERSION" \
        "wget -c --timeout=5 https://files.phpmyadmin.net/phpMyAdmin/${PHPMYADMIN_VERSION}/phpMyAdmin-${PHPMYADMIN_VERSION}-all-languages.zip"

      _exec "Configurando phpMyAdmin" \
        "apt-get -y install unzip" \
        "unzip phpMyAdmin-${PHPMYADMIN_VERSION}-all-languages.zip" \
        "mv phpMyAdmin-${PHPMYADMIN_VERSION}-all-languages /vagrant/phpmyadmin" \
        "rm -f phpMyAdmin-${PHPMYADMIN_VERSION}-all-languages.zip"
    }

    test -f /vagrant/phpmyadmin/config.inc.php || cat << EOF > /vagrant/phpmyadmin/config.inc.php
<?php
\\$i = 1;
\\$cfg['Servers'][\\$i]['auth_type'] = 'config';
\\$cfg['Servers'][\\$i]['AllowNoPassword'] = true;
\\$cfg['UploadDir'] = '';
\\$cfg['SaveDir'] = '';
EOF
    test -f ~/.phpmyadmin || {
      test -f /vagrant/phpmyadmin/sql/create_tables.sql && {
        _exec "Criando base de dados para phpMyAdmin" \
          "mysql -u root -e 'source /vagrant/phpmyadmin/sql/create_tables.sql';"
        _exec "Finalizando configuração da base de dados para phpMyAdmin" \
          "touch ~/.phpmyadmin"
      }
    }

  	test -x /usr/local/bin/composer || {
        _exec "Instalando composer" \
        	"wget -O- -q https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer"
    }

  	#_exec "Instalando NodeJS" \
    #    "wget -O- -q https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash -" \
  	#    "apt-get -y install nodejs build-essential" \
  	#    "ln -sf /usr/bin/{nodejs,node}"

  	_exec "Reiniciando serviços" \
  		"systemctl reload mysql" \
      "systemctl reload php${PHP_VERSION}-fpm" \
  		"systemctl reload apache2"

  SCRIPT
end
