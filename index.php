<?php

require_once 'loadEnv.php';

if(getenv('ENVIRONMENT') === 'development')
    ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

include('https.php');


session_start();
include("controller/validacaoRequisicaoWebServiveController.php"); //Arquivo que realiza a validação da requisição
//$chaveR = isset($_REQUEST["chave"])?$_REQUEST["chave"]:null;

//include('webservice/nusoap.php');

//$_SESSION['subsistema'] =  0 ;//Registro do subsistema na sessão
// Recupera o login
$login = isset($_POST["username"]) ? htmlspecialchars(addslashes(trim(mysql_escape_string($_POST["username"])))) : null;

// Recupera a senha, a criptografando em MD5

$senha = isset($_POST["password"]) ? htmlspecialchars(trim(mysql_escape_string($_POST["password"]))) : null;

//$senhaSoa 	= isset($_POST["password"]) ? htmlspecialchars(trim(mysql_escape_string($_POST["password"]))) : null;

if (
        (strlen($login) > 30) || (strlen($senha) > 32)
) {
    die("Boa tentativa.");
}

//chamar arquivo da Control
require_once("model/conexaoClass.php");
require_once("model/subGrupoClass.php");
require_once("model/loginClass.php");

//estanciar o arquivo
$login = new login();
$subgrupo = new Subgrupo();

/**

 * Executa a consulta no banco de dados.

 * Caso o número de linhas retornadas seja 1 o login é válido,

 * caso 0, inválido.

 */
$dados = $login->efetuaLogin();

$total = count($dados);

// Caso o usuário tenha digitado um login válido o número de linhas será 5..

if ($total == 6) {
    // Usuário não forneceu a senha ou o login
    if ($login == null || $senha == null) {

        echo "Digite sua senha e login";
    } else {

        // Agora verifica a senha
        if (!strcmp($senha, $dados["senhaUsuario"])) {

            //if($dados["fkgrupo"]!=93)//Se não for usuário
            //{
            //if($_SESSION['chaveCredemail']!=$chaveR)
            //{
            //	echo "Boa tentativa"; 
            //}else{
            // TUDO OK! Agora, passa os dados para a sessão e redireciona o usuário

            $_SESSION["idusuario"] = $dados["idusuario"];

            $_SESSION["nome"] = stripslashes($dados["nome"]);

            $_SESSION["login"] = stripslashes($dados["login"]);

            $_SESSION["chaveAMORC"] = md5(stripslashes($dados["login"]));

            $_SESSION["fkgrupo"] = $dados["fkgrupo"];

            //Selecionar Subgrupos que o usuário pertence		
            $arrSubgruposUsuario = $subgrupo->selecionaSubgrupos(null, null, $dados["idusuario"]);

            $_SESSION["subgrupos"] = $arrSubgruposUsuario;

            //Pegar região rosacruz caso ele possua
            $_SESSION["regiao_rosacruz"] = $dados["regiao_rosacruz"];

            //session_regenerate_id();//gerar novo id para sessão

            echo "<script>location.href='./main.php'</script>";

            exit;
            //}
            //}else{
            //echo "<br>Informe ao Administrador o porque você quer permissão!";
            //}
        }

        // Senha inválido
        else {
            echo "Senha inválida!";
        }
    }
}

// Login inválido
else {
    try {
        //Tentar realizar conexão com perfil SOA
        //$client = new SoapClient("http://192.1.1.135:8085/wsdl/IAmorcSSDM");

        //$client->soap_defencoding = 'UTF-8';
        //$client->decode_utf8 = true;

        /*
          $arrResponse 	= $client->ValidarLoginUsuario($login,$senhaSoa,$DatHoraProces,$NumProces);
          //echo "<br>".$senhaSoa;
          //echo "<pre>";print_r($arrResponse);

          if($arrResponse->CodUsuari!=""&&$arrResponse->NomUsuari!=""&&$arrResponse->NomSintetUsuari!="")
          {

          /*
          $_SESSION["idusuario"]   	= 1;

          $_SESSION["nome"] 			= stripslashes($arrResponse->NomUsuari);

          $_SESSION["login"] 			= stripslashes($arrResponse->CodUsuari);

          $_SESSION["chaveAMORC"]		= md5(stripslashes($arrResponse->CodUsuari));

          $_SESSION["fkgrupo"]    	= 5;
         */

        //session_regenerate_id();//gerar novo id para sessão
        /*
          echo "<script>alert('Login inválido. Por favor ligue para +55 41-3351-3031 ou envie um e-mail para xxx@amorc.org.br para confirmar suas credenciais!');location.href='./login_soa.php'</script>";

          exit;
          }else{
          echo "Senha ou login inválidos!";
          }
         */
    } catch (SoapFault $fault) {
        //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
    }

    //echo "O login fornecido é inexistente!";
}

include("login.php");


