<?php
require_once 'loadEnv.php';

$anoAtual = date('Y');
$mesAtual = date('m');

include('https.php');

@include_once("lib/functions.php");
@include_once("../lib/functions.php");

@include_once("lib/functionsSeguranca.php");
@include_once("../lib/functionsSeguranca.php");

@usuarioOnline();

if (isset($_GET['naoAssinarPorEnquanto']) && $_GET['naoAssinarPorEnquanto'] == true) {
    $_SESSION['naoAssinarPorEnquanto'] = true;
}

if (isset($_GET['corpo'])) {
    setcookie('corpo', $_GET['corpo'], time() + 86400, "/");
} else {
    setcookie('corpo', '', time() + 86400, "/");
}
?>
<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Sistema dos Organismos Afiliados</title>

        <link href="img/icon_page.png" rel="shortcut icon" >

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!--<link href="font-awesome/css/font-awesome.css" rel="stylesheet">-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link href="css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

        <!-- Summernote -->
        <link href="css/plugins/summernote/summernote.css" rel="stylesheet">
        <link href="css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

        <!-- Data Tables -->
        <!--
        <link href="css/plugins/dataTables/dataTables.bootstrap.css"
              rel="stylesheet">
        <link href="css/plugins/dataTables/dataTables.responsive.css"
              rel="stylesheet">
        <link href="css/plugins/dataTables/dataTables.tableTools.min.css"
              rel="stylesheet">-->
        <link href="css/plugins/dataTables/datatables.min.css"
              rel="stylesheet">

        <link href="css/animate.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
        <link href="css/plugins/chosen/chosen.css" rel="stylesheet">
        <link href="css/plugins/iCheck/custom.css" rel="stylesheet">

        <!-- Uploadify -->
        <!-- <link href='css/uploadify.css' rel='stylesheet'> -->
        <link href='lib/uploadifive/uploadifive.css' rel='stylesheet'>

        <!--Steps-->
        <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">

        <!-- Clock Picker -->
        <link href="css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

        <!-- Sweet Alert -->
        <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <!-- Loading Ajax -->
        <link href="css/loadingajax.css" rel="stylesheet">

        <!-- FooTable -->
        <link href="css/plugins/footable/footable.core.css" rel="stylesheet">

        <link href="css/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
        <link href="css/plugins/fullcalendar/fullcalendar.print.css" rel='stylesheet' media='print'>

        <link href="css/plugins/select2/select2.min.css" rel="stylesheet">



    </head>
    <style type="text/css" media="screen and (max-width: 750px)">
        #titulo_soa{
            font-size: 20px!important;
        }
        li.side-menu {
            margin:0 0 10px 0;
        }
    </style>

    <body>

        <?php
        require_once("model/conexaoClass.php");
        require_once('lib/functions.php');
        require_once("controller/perfilUsuarioController.php");
        require_once("model/organismoClass.php");
        require_once("model/criaSessaoClass.php");
        require_once('model/subMenuClass.php');
        require_once('model/opcaoSubMenuClass.php');
        require_once('model/departamentoSubMenuClass.php');
        require_once('model/funcaoSubMenuClass.php');
        require_once("model/notificacaoAtualizacaoServerClass.php");
        require_once("model/notificacaoGlpClass.php");
        require_once("model/ticketClass.php");
        require_once("model/notificacoesServerUsuarioClass.php");
        require_once("model/usuarioClass.php");
        include_once('model/membrosRosacruzesAtivosClass.php');

        require_once 'sec/token.php';

        $sessao = new criaSessao();

        if (!isset($_SESSION['seqCadast'])) {
            echo "<script type='text/javascript'> window.location = 'login.php'; </script>";
        }

//Alert Facebook
        /*
        $u = new Usuario();
        $resposta = $u->verificaVinculoFacebook($_SESSION['seqCadast'], null, true, false, true);
        if ($resposta) {
            ?>
            <div class="alert alert-warning alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button" onclick="fechaAvisoFacebook('<?php echo $_SESSION['seqCadast']; ?>');">×</button>
                Deseja acessar o SOA com o Facebook? Vincule sua conta clicando <a class="alert-link" href="facebook.php?registroFacebook=true&seqCadast=<?php echo $_SESSION['seqCadast']; ?>">aqui</a>.
            </div>
            <?php
        }
        */
//Internacionalização
        i18n(
                "BR"
        );

        $organismo = new organismo();

    //echo "=========================>paisUsuario:".$sessao->getValue("paisUsuario");

        $registro = $sessao->getValue('registro');
        $limite = $sessao->getValue('limite');
        if ($registro) {
            $segundos = time() - $registro;
        }
        if ($segundos > $limite) {
            session_destroy();
            echo "<script type='text/javascript'> window.location = 'telaDeBloqueio.php'; </script>";
        } else {
            $sessao->setValue("registro", time());
        }

        $resultado = $organismo->listaOrganismo(null, $sessao->getValue("siglaOA"));
//echo "<pre>";print_r($resultado);
        $naoCobrarDashboard=0;
        if ($resultado) {
            foreach ($resultado as $vetor) {
                $idOrganismoAfiliado = $vetor['idOrganismoAfiliado'];
                $idSiglaPais = $vetor['paisOrganismoAfiliado'];
                $siglaOrganismoAfiliado = $vetor['siglaOrganismoAfiliado'];
                $fk_idRegiaoPainel = $vetor['fk_idRegiaoOrdemRosacruz'];
                $naoCobrarDashboard = $vetor['naoCobrarDashboard'];
            }
        } else {
            $idOrganismoAfiliado = 1;
            $idSiglaPais = 1;
        }

        $arrNivelUsuario = isset($_SESSION['niveis']) ? explode(",", $_SESSION['niveis']) : NULL;

        //Verificação apenas leitura
        $usuarioApenasLeitura = false;
        if(!in_array("1", $arrNivelUsuario)
            && !in_array("2", $arrNivelUsuario)
            && !in_array("3", $arrNivelUsuario)
            && !in_array("4", $arrNivelUsuario)
            && !in_array("5", $arrNivelUsuario)
        )
        {
            $usuarioApenasLeitura = true;
        }

        /***************************
         * ATUALIZA NOTIFICACOES CHAT
         ***************************/
        $from   = isset($_SESSION['seqCadast']) ? $_SESSION['seqCadast'] : null;
        $para   = isset($_GET['para']) ? $_GET['para'] : null;
        $corpo  = isset($_GET['corpo']) ? $_GET['corpo'] : null;

        if ($corpo == "chat" && $para != null) {
            include_once("model/chatClass.php");
            $chat = new Chat();
            $chat->setFromChat($from);
            $chat->setParaChat($para);
            //Zerar todas as novas para
            $return = $chat->apagarNovas();
        }
        include_once("controller/usuarioController.php");
        $u = new Usuario();
        $totalMsgChat = $u->totalSeMandouMensagemNova($sessao->getValue("seqCadast"));


        /***************************
         * ATUALIZA NOTIFICACOES EMAIL
         ***************************/
        $idEmail = isset($_GET['idEmail']) ? $_GET['idEmail'] : null;
        $corpo = isset($_GET['corpo']) ? $_GET['corpo'] : null;
        if ($corpo == "emailDetalhe" && $idEmail != null) {
            include_once 'model/emailDeParaClass.php';
            $edp = new EmailDePara();
            $retorno2 = $edp->marcarComoLida($idEmail, $_SESSION['seqCadast']);
        }

        //echo "teste do braz 6";

        include_once 'controller/emailController.php';
        //echo "teste do braz 6.2";
        $ec = new emailController();
        $res = $ec->listaEmail(null, $_SESSION['seqCadast']);
        //echo "teste do braz 6.5";
        $totalMsgEmail = 0;
        /*
        if ($res) {
            foreach ($res as $vetor) {
                if ($vetor['nova'] == 1) {
                    $totalMsgEmail++;
                }
            }
        }*/
        //echo "teste do braz 7";
        /*         * ***************************************
         * ATUALIZA NOTIFICA??ES ATUALIZA??O SERVER
         * **** *********************************** */
        $idNotificacao = isset($_GET['idNotificacao']) ? $_GET['idNotificacao'] : null;
        if ($corpo == "detalheAtualizacoesServer" && $idNotificacao != null) {
            include_once("model/notificacoesServerUsuarioClass.php");

            $nsu = new notificacoesServerUsuario();
            $nsu->setFk_idNotificacao($idNotificacao);
            $nsu->setFk_seqCadast($sessao->getValue("seqCadast"));
            $result = $nsu->lista($sessao->getValue("seqCadast"), $idNotificacao);
            if (!$result) {
                $nsu->cadastro();
            }
        }

        /*         * ***************************************
         * ATUALIZA NOTIFICA??ES ENVIADAS PELA GLP
         * **** *********************************** */
        $idTicket = isset($_GET['d']) ? $_GET['d'] : null;
        if ($corpo == "buscaTicketDetalhe" && $idTicket != null) {
            include_once("model/ticketClass.php");
            $t = new Ticket();
            $t->alteraStatusTicketUsuario(1, $idTicket, $sessao->getValue("seqCadast"));
        }

        /*         * ***************************************
         * Caso seja um oficial que atua na regi
         * **** *********************************** */
        $funcoesUsuarioString = isset($_SESSION['funcoes']) ? $_SESSION['funcoes'] : null;
        //echo "teste regiao do usuario=>".$funcoesUsuarioString;
        include_once('model/funcaoClass.php');
        $f = new Funcao();
        $resultado = $f->listaFuncao(null, 1);
        $arrFuncoesDeQuemAtuaRegiao = array();
        if ($resultado) {
            foreach ($resultado as $vetor) {
                $arrFuncoesDeQuemAtuaRegiao[] = $vetor['idFuncao'];
            }
        }

        $regiaoUsuario = null;
        $arrFuncoes = explode(",", $funcoesUsuarioString);

        foreach ($arrFuncoes as $v) {
            if (in_array($v, $arrFuncoesDeQuemAtuaRegiao)) {
                $regiaoUsuario = $sessao->getValue("regiao");
            }
        }

//Verificação do token
        if ($tokenLiberado) {
        //echo "teste do braz aqui agora";
            ?>
            <div id="wrapper">
                <nav class="navbar-default navbar-static-side" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="nav-header">
                                <div class="dropdown profile-element">
            <?php
            $pc = new perfilController();
            $dadosUsuario = $pc->buscaUsuario($sessao->getValue("seqCadast"));
            $avatarUsuario = $pc->listaAvatarUsuario($sessao->getValue("seqCadast"));
            ?>
                                    <span>
                                        <img style="max-width: 48px" alt="image" class="img-circle m-t-xs img-responsive" src="<?php echo $avatarUsuario->getAvatarUsuario(); ?>" />
                                    </span>
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                        <span class="clear">
                                            <span class="block m-t-xs">
                                                <strong class="font-bold">
                                    <?php
                                    $arrNome = explode(" ", $dadosUsuario->getNomeUsuario());
                                    echo ucfirst(mb_strtolower($arrNome[0], 'UTF-8')) . " " . ucfirst(mb_strtolower($arrNome[1], 'UTF-8'));
                                    ?>
                                                </strong>
                                            </span>
                                            <span class="text-muted text-xs block">
                                                <i class="fa fa-gear"></i> Painel de Usuário <b class="caret"></b>
                                            </span>
                                        </span>
                                    </a>
                                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                        <li>
                                            <a href="?corpo=perfilUsuario">Perfil</a>
                                        </li>
                                        <li>
                                            <a href="?corpo=buscaContatos">Contato</a>
                                        </li>
                                        <li>
                                            <a href="?corpo=buscaNotificacoes">Notificações</a>
                                        </li>
    <?php
    //if(in_array("329",$arrFuncoes) ||($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){
    ?>
                                        <li>
                                            <a href="?corpo=muralDigital">Mural Digital</a>
                                        </li>
    <?php
    //}
    ?>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="model/logoutClass.php">Sair</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="logo-element">SOA</div>
                            </li>
                            <li><a href="painelDeControle.php"><i class="fa fa-th-large"></i> <span
                                        class="nav-label">Início</span> </a>
                            </li>
                            <li>
                                <a href="manuais/manualSOA29032019.pdf" target="_blank"><i class="fa fa-file"></i> <span class="nav-label">Manual do SOA</span> </a>
                            </li>
                            <li>
                                <a href="#" onclick="chamaModalAssinatura();"><i class="fa fa-pencil"></i> <span class="nav-label">Assinatura Eletrônica</span> </a>
                            </li>
    <?php
    //if(in_array("329",$arrFuncoes) ||($sessao->getValue("fk_idDepartamento")==2) || ($sessao->getValue("fk_idDepartamento")==3)){
    ?>
                            <li>
                                <a href="?corpo=muralDigital">
                                    <i class="fa fa-newspaper-o"></i>
                                    <span class="nav-label">Mural Digital</span> 
                                </a>
                            </li>
                            <?php
                            //}
                            ?>
                            <?php

                            $corpo =  (!empty($_GET['corpo'])) ? $_GET['corpo'] : "vazio";

                            $secaoAtiva = 0;
                            $subMenuAtivo = 0;
                            $opcaoSubMenuAtiva = 0;

                            $sm = new subMenu();
                            $resSubMenu = $sm->listaSubMenu(null, $corpo);
                            if ($resSubMenu) {
                                foreach ($resSubMenu as $vetor) {
                                    $secaoAtiva = $vetor['fk_idSecaoMenu'];
                                    $subMenuAtivo = $vetor['idSubMenu'];
                                    $nomeSubMenu = $vetor['nomeSubMenu'];
                                    $nomeSecaoMenu = $vetor['nomeSecaoMenu'];
                                    $msgManutencao = $vetor['msgManutencao'];
                                }
                            }

                            $osm = new opcaoSubMenu();
                            $resOpcaoSubMenu = $osm->listaOpcaoSubMenu($corpo);
                            if ($resOpcaoSubMenu) {
                                foreach ($resOpcaoSubMenu as $vetor) {
                                    //$secaoAtiva = $vetor['fk_idSecaoMenu'];
                                    //$subMenuAtivo = $vetor['fk_idSubMenu'];
                                    $opcaoSubMenuAtiva = $vetor['idOpcaoSubMenu'];
                                }
                            }
                            //echo "subMenuAtivo=>".$subMenuAtivo;
                            $fk_idDepartamento = isset($_SESSION['fk_idDepartamento']) ? json_decode($_SESSION['fk_idDepartamento']) : '';


                            if ($fk_idDepartamento != "") {

                                $dsm = new DepartamentoSubMenu();
                                $dsm->setFkIdDepartamento($fk_idDepartamento);
                                $resOpcoesMarcadas = $dsm->buscaPorDepartamento();
                                $arrayOpcoesMarcadas = array();
                                $arrayOpcoesMarcadas['fk_idSubMenu'][] = "";
                                if ($resOpcoesMarcadas) {
                                    foreach ($resOpcoesMarcadas as $vetorOpcoesMarcadas) {
                                        $arrayOpcoesMarcadas['fk_idDepartamento'][] = $vetorOpcoesMarcadas['fk_idDepartamento'];
                                        $arrayOpcoesMarcadas['fk_idSubMenu'][] = $vetorOpcoesMarcadas['fk_idSubMenu'];
                                    }
                                }

                                $fsm = new FuncaoSubMenu();
                                $resOpcoesMarcadasFuncoes = $fsm->buscaPorFuncao($funcoesUsuarioString);
                                $arrayOpcoesMarcadasFuncoes = array();
                                $arrayOpcoesMarcadasFuncoes['fk_idSubMenu'][] = "";
                                if ($resOpcoesMarcadasFuncoes) {
                                    foreach ($resOpcoesMarcadasFuncoes as $vetorOpcoesMarcadasFuncoes) {
                                        $arrayOpcoesMarcadasFuncoes['fk_idFuncao'][] = $vetorOpcoesMarcadasFuncoes['fk_idFuncao'];
                                        $arrayOpcoesMarcadasFuncoes['fk_idSubMenu'][] = $vetorOpcoesMarcadasFuncoes['fk_idSubMenu'];
                                    }
                                }

                                //Listar
                                $sql = "SELECT * from secaoMenu as sec
							inner join subMenu as sub on sub.fk_idSecaoMenu = sec.idSecaoMenu";
                                if ($funcoesUsuarioString == 0) {
                                    $sql .= " inner join departamento_subMenu as dsm on dsm.fk_idSubMenu = sub.idSubMenu ";
                                } else {

                                    $sql .= " inner join funcao_subMenu as fsm on fsm.fk_idSubmenu = sub.idSubMenu ";
                                }
                                $sql .= " where 1=1 and statusSecaoMenu = 0";
                                if ($funcoesUsuarioString == 0) {
                                    $sql .= " and fk_idDepartamento = :idDepartamento ";
                                } else {
                                    $ids = explode(",", $funcoesUsuarioString);
                                    for ($i = 0; $i < count($ids); $i++) {
                                        if ($i == 0) {
                                            $inQuery = ":" . $i;
                                        } else {
                                            $inQuery .= ",:" . $i;
                                        }
                                    }
                                    $sql .= " and fsm.fk_idFuncao IN  (" . $inQuery . ") ";
                                }
                                $sql .= "
							group by sec.idSecaoMenu
							order by CAST(sec.prioridadeSecaoMenu AS UNSIGNED),nomeSecaoMenu
							";
                                //echo $sql;
                                $c = new conexaoSOA();
                                $con = $c->openSOA();
                                $sth = $con->prepare($sql);

                                if ($funcoesUsuarioString == 0) {
                                    $sth->bindParam(':idDepartamento', $fk_idDepartamento, PDO::PARAM_INT);
                                } else {
                                    foreach ($ids as $k => $id) {
                                        $sth->bindValue(":" . $k, $id);
                                    }
                                }

                                $resultado = $c->run($sth);

                                if ($resultado) {
                                    foreach ($resultado as $dadosSecaoMenu) {

                                        $sql2 = "SELECT * from subMenu as sub ";
                                        if ($funcoesUsuarioString == 0) {
                                            $sql2 .= " inner join departamento_subMenu as dsm on dsm.fk_idSubMenu = sub.idSubMenu ";
                                        } else {

                                            $sql2 .= " inner join funcao_subMenu as fsm on fsm.fk_idSubmenu = sub.idSubMenu ";
                                        }
                                        $sql2 .= " where fk_idSecaoMenu = :idSecaoMenu and statusSubMenu = 0 ";
                                        if ($funcoesUsuarioString == 0) {
                                            $sql2 .= " and fk_idDepartamento = :idDepartamento ";
                                        } else {
                                            $ids2 = explode(",", $funcoesUsuarioString);
                                            for ($i = 0; $i < count($ids2); $i++) {
                                                if ($i == 0) {
                                                    $inQuery2 = ":" . $i;
                                                } else {
                                                    $inQuery2 .= ",:" . $i;
                                                }
                                            }
                                            $sql2 .= " and fsm.fk_idFuncao IN (" . $inQuery2 . ") ";
                                            //echo "<br><br>".$sql2."<br><br>";
                                        }
                                        $sql2 .= " group by sub.idSubMenu";
                                        $sql2 .= " order by CAST(sub.prioridadeSubMenu AS UNSIGNED),sub.idSubMenu";
                                        //echo $sql2;
                                        $c2 = new conexaoSOA();
                                        $con2 = $c2->openSOA();
                                        $sth2 = $con2->prepare($sql2);

                                        $sth2->bindParam(':idSecaoMenu', $dadosSecaoMenu['idSecaoMenu'], PDO::PARAM_INT);
                                        if ($funcoesUsuarioString == 0) {
                                            $sth2->bindParam(':idDepartamento', $fk_idDepartamento, PDO::PARAM_INT);
                                        } else {
                                            foreach ($ids2 as $r => $id2) {
                                                $sth2->bindValue(":" . $r, $id2);
                                            }
                                        }

                                        $resultado2 = $c2->run($sth2);
                                        ?>
                                        <li <?php if ($secaoAtiva == $dadosSecaoMenu['idSecaoMenu']) { ?>
                                                class="active" <?php } ?>><a href="?corpo=<?php echo $dadosSecaoMenu['arquivoSecaoMenu']; ?>"
                                                                     href="?corpo=<?php
                                        if ($dadosSecaoMenu['arquivoSecaoMenu'] != "") {
                                            echo $dadosSecaoMenu['arquivoSecaoMenu'];
                                        }
                                        ?>"><i
                                                    class="<?php echo $dadosSecaoMenu['iconeSecaoMenu']; ?>"></i> <span
                                                    class="nav-label"><?php echo str_replace("\xE2\x80\x8C", "", $dadosSecaoMenu['nomeSecaoMenu']); ?>
                                                </span> <?php if (count($resultado2) > 0) { ?><span
                                                        class="fa arrow"></span> <?php } ?> </a> <?php
                                        if (count($resultado2) > 0) {
                                            ?>
                                                <ul id="segundoNivel<?php echo $dadosSecaoMenu['idSecaoMenu']; ?>" class="nav nav-second-level">
                                            <?php
                                            foreach ($resultado2 as $dadosSubMenu) {

                                                $sql3 = "SELECT * from opcaoSubMenu as osm ";
                                                if ($funcoesUsuarioString == 0) {
                                                    $sql3 .= " inner join departamento_opcaoSubMenu as dosm on dosm.fk_idOpcaoSubMenu = osm.idOpcaoSubMenu";
                                                } else {
                                                    $sql3 .= " inner join funcao_opcaoSubMenu as fosm on fosm.fk_idOpcaoSubMenu = osm.idOpcaoSubMenu";
                                                }
                                                $sql3 .= " where fk_idSubMenu = :idSubMenu and statusOpcaoSubMenu = 0 ";
                                                if ($funcoesUsuarioString == 0) {
                                                    $sql3 .= " and dosm.fk_idDepartamento = :idDepartamento ";
                                                } else {
                                                    $ids3 = explode(",", $funcoesUsuarioString);
                                                    for ($i = 0; $i < count($ids3); $i++) {
                                                        if ($i == 0) {
                                                            $inQuery3 = ":" . $i;
                                                        } else {
                                                            $inQuery3 .= ",:" . $i;
                                                        }
                                                    }
                                                    $sql3 .= " and fosm.fk_idFuncao IN (" . $inQuery3 . ") ";
                                                }
                                                $sql3 .= "
											group by osm.idOpcaoSubMenu";
                                                $sql3 .= "
											order by CAST(osm.prioridadeOpcaoSubMenu AS UNSIGNED),osm.idOpcaoSubMenu";
                                                //echo $sql3;
                                                $c3 = new conexaoSOA();
                                                $con3 = $c3->openSOA();
                                                $sth3 = $con3->prepare($sql3);

                                                $sth3->bindParam(':idSubMenu', $dadosSubMenu['idSubMenu'], PDO::PARAM_INT);
                                                if ($funcoesUsuarioString == 0) {
                                                    $sth3->bindParam(':idDepartamento', $fk_idDepartamento, PDO::PARAM_INT);
                                                } else {
                                                    foreach ($ids3 as $u => $id3) {
                                                        $sth3->bindValue(":" . $u, $id3);
                                                    }
                                                }
                                                $resultado3 = $c3->run($sth3);
                                                ?>
                                                        <li <?php if ($subMenuAtivo == $dadosSubMenu['idSubMenu']) { ?>
                                                                class="active" <?php } ?>><a
                                                                href="?corpo=<?php
                                                        if ($dadosSubMenu['arquivoSubMenu'] != "") {
                                                            echo $dadosSubMenu['arquivoSubMenu'];
                                                            ?><?php
                                                        } else {
                                                            echo "#";
                                                        }
                                                        ?>"><i
                                                                    class="<?php echo $dadosSubMenu['iconeSubMenu']; ?>"></i> <?php echo str_replace("\xE2\x80\x8C", "", $dadosSubMenu['nomeSubMenu']); ?>
                                                        <?php if (count($resultado3) > 0) { ?><span class="fa arrow"></span>
                                                        <?php } ?> </a> <?php
                                                        //$areas=array();
                                                        if (count($resultado3) > 0) {
                                                            ?>
                                                                <ul id="terceiroNivel<?php echo $dadosSubMenu['idSubMenu']; ?>" class="nav nav-third-level">
                                                            <?php
                                                            foreach ($resultado3 as $dadosOpcaoSubMenu) {
                                                                ?>
                                                                        <li
                                <?php if ($opcaoSubMenuAtiva == $dadosOpcaoSubMenu['idOpcaoSubMenu']) { ?>
                                                                                class="active" <?php } ?>><a
                                                                                href="?corpo=<?php
                                                                        if ($dadosOpcaoSubMenu['arquivoOpcaoSubMenu'] != "") {
                                                                            echo $dadosOpcaoSubMenu['arquivoOpcaoSubMenu'];
                                                                            ?><?php
                                                                        } else {
                                                                            echo "#";
                                                                        }
                                                                        ?>"><i
                                                                                    class="<?php echo $dadosOpcaoSubMenu['iconeOpcaoSubMenu']; ?>"></i>
                                                                        <?php echo str_replace("\xE2\x80\x8C", "", $dadosOpcaoSubMenu['nomeOpcaoSubMenu']); ?> </a>
                                                                        </li>
                                                                    <?php
                                                                }
                                                                ?>
                                                                </ul> <?php
                                                                }
                                                                ?></li>
                                                                <?php
                                                            }
                                                            ?>
                                                </ul> <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?></li>
                            <!--<li><a href="?corpo=buscaEmail"><i class="fa fa-envelope"></i> <span class="nav-label">E-mail </span> </a>-->
                            </li>
                            <li><a href="?corpo=buscaAnotacoes"><i class="fa fa-edit"></i> <span class="nav-label">Anotações </span> </a>
                            </li>
                            <li><a href="?corpo=chat"><i class="fa fa-comments"></i> <span class="nav-label">Chat </span> </a>
                            </li>
                            <li><a href="?corpo=biblioteca"><i class="fa fa-folder-open"></i> <span class="nav-label">Biblioteca </span> </a>
                            <li><a href="?corpo=videoAulas"><i class="fa fa-video-camera"></i> <span class="nav-label">Vídeo-Aulas </span> </a>
                            <li><a href="?corpo=telefonesUteis"><i class="fa fa-phone"></i> <span class="nav-label">Telefones Úteis </span> </a>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <div class="row border-bottom">
                        <nav class="navbar navbar-static-top" role="navigation"
                             style="margin-bottom: 0">
                            <div class="navbar-header">
                                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
    <?php
    //Se Monitor Reg Organismos Afiliados ou Monitor Reg Assuntos Martinistas
    //Monitor Reg Organismos Afiliados
    $ocultar_json = 1;
    $seqCadast = $_SESSION['seqCadast'];
    $seqFuncao = '155';
    $atuantes = 'S';
    $naoAtuantes = 'N';
    include 'js/ajax/retornaFuncaoMembro.php';
    $obj = json_decode(json_encode($return), true);
    $proprioOA = false;
    $arrOALotacaoMonitorRegional = array();
    $totalFuncoesMonitorRegional = count($obj['result'][0]['fields']['fArrayOficiais']);
    //echo "TOTAL FUNÇÕES=>".$totalFuncoes;
    if ($totalFuncoesMonitorRegional > 0) {
        if ($totalFuncoesMonitorRegional == 1) {
            $proprioOA = true;
        } else {
            foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor) {
                $arrOALotacaoMonitorRegional[] = $vetor['fields']['fSigOrgafi'];
            }
        }
    }
    //echo "<pre>";print_r($arrOALotacaoMonitorRegional);
    //Monitor Reg Assuntos Martinistas
    $ocultar_json = 1;
    $seqCadast = $_SESSION['seqCadast'];
    $seqFuncao = 157;
    $atuantes = 'S';
    $naoAtuantes = 'N';
    include 'js/ajax/retornaFuncaoMembro.php';
    $arrOALotacaoMonitorRegionalTOM = array();
    $obj = json_decode(json_encode($return), true);
    $totalFuncoesMonitorRegionalTOM = count($obj['result'][0]['fields']['fArrayOficiais']);
    if ($totalFuncoesMonitorRegionalTOM > 0) {
        if ($totalFuncoesMonitorRegionalTOM == 1) {
            $proprioOA = true;
        } else {
            foreach ($obj['result'][0]['fields']['fArrayOficiais'] as $vetor) {
                $arrOALotacaoMonitorRegionalTOM[] = $vetor['fields']['fSigOrgafi'];
            }
        }
    }
    //Limpar variáveis da memória para não impactar outras partes do sistema
    unset($ocultar_json);
    unset($seqCadast);
    unset($seqFuncao);
    unset($atuantes);
    unset($naoAtuantes);

    //Unir os dois arrays
    $arrOALotacao = array_merge($arrOALotacaoMonitorRegional, $arrOALotacaoMonitorRegionalTOM);

    $o = new organismo();
    $nomeOrganismo = " Ordem Rosacruz, AMORC-GLP - PR113"; //Default
    if ($sessao->getValue("siglaOA") != "") {
        $resultado = $o->listaOrganismo(null, $sessao->getValue("siglaOA"));

        if ($resultado) {
            foreach ($resultado as $vetor) {

                $idClassificacaoOa= $vetor['classificacaoOrganismoAfiliado'];
                $siglaOaSelecionada= $vetor['siglaOrganismoAfiliado'];

                switch ($vetor['classificacaoOrganismoAfiliado']) {
                    case 1:
                        $classificacao = "Loja";
                        break;
                    case 2:
                        $classificacao = "Pronaos";
                        break;
                    case 3:
                        $classificacao = "Capítulo";
                        break;
                    case 4:
                        $classificacao = "Heptada";
                        break;
                    case 5:
                        $classificacao = "Atrium";
                        break;
                    default:
                        $classificacao = "Loja";
                        break;
                }
                switch ($vetor['tipoOrganismoAfiliado']) {
                    case 1:
                        $tipo = "R+C";
                        break;
                    case 2:
                        $tipo = "TOM";
                        break;
                }
                switch ($vetor['paisOrganismoAfiliado']) {
                    case 1:
                        $siglaPais = "BR";
                        break;
                    case 2:
                        $siglaPais = "PT";
                        break;
                    case 3:
                        $siglaPais = "AO";
                        break;
                    case 4:
                        $siglaPais = "MZ";
                        break;
                    default :
                        $siglaPais = "BR";
                        break;
                }

                $idOrga2 = $vetor['idOrganismoAfiliado'];

                $sessao->setValue("classificacaoOA", $classificacao);
                $sessao->setValue("classificacaoOAId", $vetor['classificacaoOrganismoAfiliado']);
                if($vetor['siglaOrganismoAfiliado']!="PR113") {
                    $nomeOrganismo = $classificacao . " " . $tipo . " " . $vetor["nomeOrganismoAfiliado"] . " - " . $vetor["siglaOrganismoAfiliado"];
                }else{
                    $nomeOrganismo = $vetor["nomeOrganismoAfiliado"] . " - " . $vetor["siglaOrganismoAfiliado"];
                }
                $siglaOrga = $vetor["siglaOrganismoAfiliado"];
            }
        }
    }
    ?>
                                <h1 style="width: 700px; margin: 13px 0 0 30px; " id="titulo_soa">
                                    &nbsp;SOA - <?php echo $nomeOrganismo;
                                                    $nomeOrganismoAfiliadoCompleto = $nomeOrganismo; ?>
                                <?php if ((($funcoesUsuarioString == 0 && $_SESSION['fk_idDepartamento'] != 1) || ($regiaoUsuario != null && ($proprioOA == false || in_array($sessao->getValue("siglaOA"), $arrOALotacao))) || $_SESSION['fk_idDepartamento'] == 2 || $_SESSION['fk_idDepartamento'] == 3)) { ?>
                                        <a href="#" data-toggle="modal" data-target="#mySeeLotacao" data-toggle="tooltip" data-placement="left" title="Oas"> <i class="fa fa-globe"></i></a>
                                <?php } ?>
                                </h1>
                            </div>



                            <ul class="nav navbar-top-links navbar-right">
                                <?php if ($sessao->getValue("paisUsuario") == "PT") { ?>
                                    <img src="bandeira/portugal.png">
                                <?php } ?>
                                <?php if ($sessao->getValue("paisUsuario") == "AO") { ?>
                                    <img src="bandeira/angola.png">
                                <?php } ?>
                                <!--   
                               <li class="dropdown"><a class="dropdown-toggle count-info"
                                                       data-toggle="dropdown" href="#"> <i class="fa fa-comments"></i> <span
                                           class="label label-warning"><?php //echo $totalMsgChat; ?> </span>
                                   </a>
                                   <ul class="dropdown-menu dropdown-messages">
                                <?php
                                /*
                                  $u = new Usuario();
                                  $resultado = $u->listaUsuarioMensagensChatNovas($sessao->getValue("seqCadast"), "seqCadast", "1", null, "1");
                                  $diferencaHoras = "";
                                  date_default_timezone_set('America/Sao_Paulo');
                                  if ($resultado) {
                                  foreach ($resultado as $vetor2) {
                                  if ($vetor2['nova'] == 1) {
                                  $arrNome = explode(" ", $vetor2['nomeUsuario']);
                                  $dia = substr($vetor2['data'], 8, 2);
                                  $mes = substr($vetor2['data'], 5, 2);
                                  $ano = substr($vetor2['data'], 0, 4);
                                  $hora = substr($vetor2['data'], 11, 2);
                                  $minuto = substr($vetor2['data'], 14, 2);
                                  $segundo = substr($vetor2['data'], 17, 2);


                                  $mktimeDataMin = mktime(date("H") - $hora, date("i") - $minuto, date("s") - $segundo, date("m") - $mes, date("d") - $dia, date("Y") - $ano);
                                  $diferencaMinutos = date('i', $mktimeDataMin);
                                  ?>
                                  <li>
                                  <div class="dropdown-messages-box">
                                  <a href="?corpo=chat&para=<?php echo $vetor2['seqCadast']; ?>"
                                  class="pull-left"> <img alt="image" class="img-circle"
                                  src="<?php if ($vetor2['avatarUsuario'] == "") { ?>img/default-user-small.png<?php
                                  } else {
                                  echo $vetor2['avatarUsuario'];
                                  }
                                  ?>"> </a>
                                  <div class="media-body">
                                  <small class="pull-right"><?php
                                  if (intval($diferencaMinutos) < 60 && $dia == date("d") && $mes == date("m") && $ano == date("Y") && $hora == date("H")) {
                                  echo $diferencaMinutos . " min atrás";
                                  }
                                  ?> </small> De <strong><?php echo $arrNome[0]; ?> </strong> <br>
                                  <small class="text-muted">Data: <?php echo $dia . "/" . $mes . "/" . $ano; ?>
                                  às <?php echo $hora . ":" . $minuto . ":" . $segundo; ?> </small>
                                  </div>
                                  </div>
                                  </li>
                                  <li class="divider"></li>
                                  <?php
                                  }
                                  }
                                  }
                                  ?>
                                  <!--
                                  <li>
                                  <div class="text-center link-block">
                                  <a href="?corpo=chat"> <i class="fa fa-envelope"></i> <strong>Ver
                                  todas as mensagens de Chat</strong> </a>
                                  </div>
                                  </li>
                                  </ul>
                                  </li>-->
                                  <!--
                                  <li class="dropdown"><a class="dropdown-toggle count-info"
                                  data-toggle="dropdown" href="#"> <i class="fa fa-envelope"></i> <span
                                  class="label label-primary"><?php //echo $totalMsgEmail;?></span> </a>
                                  <ul class="dropdown-menu dropdown-messages">
                                  <?php
                                  /*
                                  $u = new Usuario();
                                  $resultado = $u->listaUsuarioMensagensEmailNovas($sessao->getValue("seqCadast"));
                                  if ($resultado) {
                                  foreach ($resultado as $vetor7) {
                                  ?>
                                  <li>
                                  <div class="dropdown-messages-box">
                                  <a href="?corpo=emailDetalhe&idEmail=<?php echo $vetor7['idEmail'];?>" class="pull-left">
                                  <img alt="image" class="img-circle" src="<?php if ($vetor7['avatarUsuario'] == "") { ?>img/default-user-small.png<?php
                                  } else {
                                  echo $vetor7['avatarUsuario'];
                                  }
                                  ?>"> </a>
                                  <div class="media-body">
                                  <strong><?php echo $vetor7['nomeUsuario'];?></strong>
                                  <br>
                                  <?php echo $vetor7['assunto'];?>
                                  <br>
                                  <small class="text-muted"><?php echo substr($vetor7['dataCadastro'],8,2)."/".substr($vetor7['dataCadastro'],5,2)."/".substr($vetor7['dataCadastro'],0,4)." ".substr($vetor7['dataCadastro'],11,8);?></small>
                                  </div>
                                  </div>
                                  </li>
                                  <li class="divider"></li>
                                  <?php
                                  //}
                                  //} */
                                ?>
                                <!--
                                <li>
                                    <div class="text-center link-block">
                                        <a href="?corpo=buscaEmail"> <i class="fa fa-envelope"></i> <strong>Ver
                                                todas as Mensagens de E-mail</strong> </a>
                                    </div>
                                </li>
                            </ul>
                        </li>-->
                                <li class="dropdown"><?php
                                $ns = new NotificacaoServer();
                                $resultado = $ns->listaNotificacaoServer(1);
                                //echo "<pre>";print_r($resultado);
                                $arrNotificacoes = array();
                                if ($resultado) {
                                    $i = 0;
                                    $totalGeral = count($resultado);
                                    foreach ($resultado as $vetor) {
                                        if ($i < 4) {
                                            $arrNotificacoes['idNotificacao'][$i] = $vetor['idNotificacao'];
                                            $arrNotificacoes['tituloNotificacao'][$i] = $vetor['tituloNotificacao'];
                                        }
                                        $i++;
                                    }
                                } else {
                                    $totalGeral = 0;
                                }

                                $nsu = new notificacoesServerUsuario();
                                $resultado2 = $nsu->lista($sessao->getValue("seqCadast"));
                                $arrNotificacoesVistas = array();
                                if ($resultado2) {
                                    $totalUsuario = count($resultado2);
                                    foreach ($resultado2 as $vetor2) {
                                        $arrNotificacoesVistas[] = $vetor2['fk_idNotificacao'];
                                    }
                                } else {
                                    $totalUsuario = 0;
                                }
                                ?> <a class="dropdown-toggle count-info" data-toggle="dropdown"
                                       href="#"> <i class="fa fa-tasks"></i> <span
                                            class="label label-info"><?php
                                    $totalMsgNotificacoesServer = $totalGeral - $totalUsuario;
                                    if ($totalMsgNotificacoesServer < 0) {
                                        $totalMsgNotificacoesServer = 0;
                                    }
                                    $nsu = new notificacoesServerUsuario();
                                    $totalMsgNotificacoesServer = $nsu->totalNaoVisualizadas($sessao->getValue("seqCadast"));
                                    echo $totalMsgNotificacoesServer;
                                    ?>
                                        </span> </a>
                                    <ul class="dropdown-menu dropdown-alerts">
                                    <?php
                                    $totalMsgNotificacoesServer = $totalGeral - $totalUsuario;


                                    if (count($arrNotificacoes) > 0) {
                                        for ($i = 0; $i < count($arrNotificacoes['idNotificacao']); $i++) {

                                            if (!in_array($arrNotificacoes['idNotificacao'][$i], $arrNotificacoesVistas)) {
                                                ?>
                                                    <li><a
                                                            href="?corpo=detalheAtualizacoesServer&idNotificacao=<?php echo $arrNotificacoes['idNotificacao'][$i]; ?>">
                                                            <div>
                                                                <i class="fa fa-upload fa-fw"></i>
                                                <?php echo $arrNotificacoes['tituloNotificacao'][$i]; ?>
                                                            </div> </a>
                                                    </li>
                                                    <li class="divider"></li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>

                                        <li>
                                            <div class="text-center link-block">
                                                <a href="painelDeControle.php?corpo=listaDeAtualizacoesServer">
                                                    <strong>Ver todas as Atualizações</strong> </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>

                                <!--
                                        Dropdown Notifica??es enviadas pela GLP, setor de Organismos Afiliados
                                        Classe inclu?da notificacaoGlpClass,
                                -->

                                <!--<li class="dropdown">-->
    <?php
    /*
      $nglp = new NotificacaoGlp();

      $resultado = $nglp->listaNotificacaoDropdown(1, $sessao->getValue("seqCadast"));

      //echo "<pre>";print_r($resultado);

      $arrNotificacoesGlp = array();

      if ($resultado) {
      $i = 0;
      $totalGeral = count($resultado);
      foreach ($resultado as $vetor) {
      if ($i < 4) {
      $arrNotificacoesGlp['idNotificacaoGlp'][$i] = $vetor['idNotificacaoGlp'];
      $arrNotificacoesGlp['tituloNotificacaoGlp'][$i] = $vetor['tituloNotificacaoGlp'];
      $arrNotificacoesGlp['tipoNotificacaoGlp'][$i] = $vetor['tipoNotificacaoGlp'];
      }
      $i++;
      }
      } else {
      $totalGeral = 0;
      }

      $resultado2 = $nglp->lista($sessao->getValue("seqCadast"));

      $arrNotificacoesGlpVistas = array();

      if ($resultado2) {
      $totalUsuario = count($resultado2);
      foreach ($resultado2 as $vetor2) {
      $arrNotificacoesGlpVistas[] = $vetor2['fk_idNotificacaoGlp'];
      }
      } else {
      $totalUsuario = 0;
      }
      //echo "Total Geral: ".$totalGeral."<br>";
      //echo "Total Usuario: ".$totalUsuario."<br>";
      ?>
      <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
      <i class="fa fa-exclamation-triangle"></i>
      <span class="label label-info"><?php echo $totalGeral; ?></span>
      </a>
      <ul class="dropdown-menu dropdown-alerts">
      <?php
      $totalMsgNotificacoes = $totalGeral;
      if (count($arrNotificacoesGlp) > 0) {
      for ($i = 0; $i < count($arrNotificacoesGlp['idNotificacaoGlp']); $i++) {
      ?>
      <li>
      <a href="?corpo=detalheNotificacao&idNotificacaoGlp=<?php echo $arrNotificacoesGlp['idNotificacaoGlp'][$i]; ?>">
      <div>
      <!--<i class="fa fa-upload fa-fw"></i>-->
      <center>
      <?php
      switch ($arrNotificacoesGlp['tipoNotificacaoGlp'][$i]) {
      case 1:
      echo "<span class='badge badge-danger'>Não Entregue</span><br>";
      break;
      case 2:
      echo "<span class='badge badge-warning'>Falta Documento</span><br>";
      break;
      case 3:
      echo "<span class='badge badge-info'>Erro de Documentação</span><br>";
      break;
      case 4:
      echo "<span class='badge badge-danger'>Deve Atualizar</span><br>";
      break;
      case 5:
      echo "<span class='badge badge-success'>Outros</span>";
      break;
      case 6:
      echo "<span class='badge badge-success'>Entregue</span>";
      break;
      case 7:
      echo "<span class='badge badge-success'>Transferência</span>";
      break;
      }
      ?>
      </center>
      <?php echo $arrNotificacoesGlp['tituloNotificacaoGlp'][$i]; ?>
      </div>
      </a>
      </li>
      <li class="divider"></li>
      <?php
      }
      } */
    ?>
                                <!--
                                        <li>
                                            <div class="text-center link-block">
                                                <a href="painelDeControle.php?corpo=buscaNotificacoes">
                                                    <strong>Ver todas as Notificações</strong> </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                -->    
                                <!--
                                <li class="dropdown">
                                <?php
                                $t = new Ticket();
                                $resultadoTicketGeral = $t->listaTicketDropdown(1, $sessao->getValue("seqCadast"));
                                $arrTicket = array();
                                if ($resultadoTicketGeral) {
                                    if (!empty($resultadoTicketGeral)) {
                                        $i = 0;
                                        $totalGeralTicket = count($resultadoTicketGeral);
                                        foreach ($resultadoTicketGeral as $vetorTicketGeral) {

                                            if ($i < 4) {
                                                $arrTicket['idTicket'][$i] = $vetorTicketGeral['idTicket'];
                                                $arrTicket['tituloTicket'][$i] = $vetorTicketGeral['tituloTicket'];
                                                $arrTicket['descricaoTicket'][$i] = $vetorTicketGeral['descricaoTicket'];
                                                $arrTicket['statusTicket'][$i] = $vetorTicketGeral['statusTicket'];
                                                $arrTicket['dataAtualizacaoTicket'][$i] = $vetorTicketGeral['dataAtualizacaoTicket'];
                                            }
                                            $i++;
                                        }
                                    } else {
                                        $totalGeralTicket = 0;
                                    }
                                } else {
                                    $totalGeralTicket = 0;
                                }
                                /*
                                  $resultadoTicketUsuario = $t->lista($sessao->getValue("seqCadast"));
                                  if ($resultadoTicketUsuario) {
                                  $totalUsuarioTicket = $resultadoTicketUsuario;
                                  } else {
                                  $totalUsuarioTicket = 0;
                                  }
                                  //echo "Total Geral Ticket: ".$totalGeralTicket."<br>";
                                  //echo "Total Usuario Ticket: ".$totalUsuarioTicket."<br>";
                                  ?>
                                  <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                  <i class="fa fa-ticket"></i>
                                  <span class="label label-info"><?php echo $totalGeralTicket; ?></span>
                                  </a>
                                  <ul class="dropdown-menu dropdown-alerts">
                                  <?php
                                  //$totalMsgTicket = $totalGeralTicket;
                                  if (count($arrTicket) > 0) {
                                  for ($i = 0; $i < count($arrTicket['idTicket']); $i++) {
                                  ?>
                                  <li>
                                  <a href="?corpo=buscaTicketDetalhe&d=<?php echo $arrTicket['idTicket'][$i]; ?>">
                                  <div>
                                  <?php
                                  $label = $arrTicket['dataAtualizacaoTicket'][$i] == '0000-00-00 00:00:00' ? "<center><span class='pull-right label label-primary'>Novo</span></center>" : "<center><span class='pull-right label label-primary'>Modificado</span></center>";
                                  echo $label;
                                  $statusTicket = "";
                                  switch ($arrTicket['statusTicket'][$i]) {
                                  case 0: $statusTicket = "[Aberto] "; break;
                                  case 1: $statusTicket = "[Em andamento] "; break;
                                  case 2: $statusTicket = "[Fechado] "; break;
                                  case 3: $statusTicket = "[Indeferido!] "; break;
                                  }

                                  echo "Ticket " . $arrTicket['idTicket'][$i]."<br>";
                                  echo $statusTicket." ".$arrTicket['tituloTicket'][$i]."<br>";

                                  $descricaoTicket = str_split($arrTicket['descricaoTicket'][$i],40);
                                  $descricaoFormatado = strlen($descricaoTicket[0]) == 40 ? $descricaoTicket[0]."..." : $descricaoTicket[0];
                                  echo $descricaoFormatado."<br>";
                                  ?>
                                  </div>
                                  </a>
                                  </li>
                                  <li class="divider"></li>
                                  <?php
                                  }
                                  } */
                                ?><!--
                                <li>
                                    <div class="text-center link-block">
                                        <a href="painelDeControle.php?corpo=buscaTicket">
                                            <strong>Ver todos os Tickets</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                                -->
                                <li class="dropdown">
                                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                        <i class="fa fa-bell"></i>  <span class="label label-primary"><?php echo $totalGeralTicket + $totalMsgEmail + $totalMsgChat; ?></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-alerts">
                                        <li>
                                            <a href="?corpo=buscaTicket">
                                                <div>
                                                    <i class="fa fa-ticket fa-fw"></i> Ver seus tickets
                                                    <span class="pull-right text-muted small">
                                                        <span class="label label-primary"><?php echo $totalGeralTicket; ?></span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="?corpo=chat">
                                                <div>
                                                    <i class="fa fa-comments fa-fw"></i> Ver suas mensagens
                                                    <span class="pull-right text-muted small">
                                                        <span class="label label-primary"><?php echo $totalMsgChat; ?></span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <!--
                                        <li>
                                            <a href="?corpo=buscaEmail">
                                                <div>
                                                    <i class="fa fa-envelope fa-fw"></i> Ver seus e-mails
                                                    <span class="pull-right text-muted small">
                                                        <span class="label label-primary"><?php //echo $totalMsgEmail; ?></span>
                                                    </span>
                                                </div>
                                            </a>
                                        </li>-->
                                    </ul>
                                </li>
                                <li><a href="model/logoutClass.php"> <i class="fa fa-sign-out"></i> Sair </a>
                                </li>
                            </ul>

                        </nav>
                    </div>
                    <!-- Modal Lota??o -->
                    <div class="modal inmodal" id="mySeeLotacao" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content animated bounceInRight">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <i class="fa fa-refresh modal-icon"></i>
                                    <h4 class="modal-title">Alterar Lotação</h4>
                                    <!--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->
                                </div>
                                <div class="modal-body">
                                    <div class="form-group" style="padding-left:25%">
                                        <right>
                                            <div>
                                                Pesquisar: <input type="text" name="search" id="search" onkeypress="filtraOrganismos(this.value, '<?php echo $regiaoUsuario; ?>', '<?php echo $corpo; ?>');" onkeyUp="filtraOrganismos(this.value, '<?php echo $regiaoUsuario; ?>', '<?php echo $corpo; ?>');">
                                            </div>
                                        </right>
                                        <br>
                                        <div id="organismos">
    <?php
    $resultado2 = $o->listaOrganismo(null, null, "siglaOrganismoAfiliado", $regiaoUsuario, null, null, null, 1, $arrOALotacao, null, null, 1);

    if ($resultado2) {
        foreach ($resultado2 as $vetor2) {

            switch ($vetor2['classificacaoOrganismoAfiliado']) {
                case 1:
                    $classificacao = "Loja";
                    break;
                case 2:
                    $classificacao = "Pronaos";
                    break;
                case 3:
                    $classificacao = "Capítulo";
                    break;
                case 4:
                    $classificacao = "Heptada";
                    break;
                case 5:
                    $classificacao = "Atrium";
                    break;
            }
            $classificacaoTemporaria="";
            switch ($vetor2['classificacaoTemporaria']) {
                case 1:
                    $classificacaoTemporaria = "Loja";
                    break;
                case 2:
                    $classificacaoTemporaria = "Pronaos";
                    break;
                case 3:
                    $classificacaoTemporaria = "Capítulo";
                    break;
                case 4:
                    $classificacaoTemporaria = "Heptada";
                    break;
                case 5:
                    $classificacaoTemporaria = "Atrium";
                    break;
            }

            switch ($vetor2['tipoOrganismoAfiliado']) {
                case 1:
                    $tipo = "R+C";
                    break;
                case 2:
                    $tipo = "TOM";
                    break;
            }

            $nomeOrganismo = $vetor2["siglaOrganismoAfiliado"] . " - " . $classificacao . " " . $tipo . " " . $vetor2["nomeOrganismoAfiliado"];
            ?>
                                                    <a href="alteraLotacao.php?corpo=<?php echo $corpo; ?>&sigla=<?php echo $vetor2["siglaOrganismoAfiliado"]; ?>"><i class="fa fa-globe"></i> <?php echo $nomeOrganismo; ?></a><br><br>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal inmodal" id="mySeeAssinatura" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg" style="width: 50%">
                            <div class="modal-content animated bounceInUp">
                                <div class="modal-header">
                                    <i class="fa fa-pencil modal-icon"></i>
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <form id="imp" name="imp" class="form-horizontal" method="post" action="assinarEletronicamente.php" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <center><h1>Assinatura Eletrônica</h1><br><b><?php echo $nomeOrganismoAfiliadoCompleto;?></b><br><br>Suas funções:<br>
                                                    <?php
                                                    $temAlgoParaAssinar=false;
                                                        require_once 'model/funcaoClass.php';
                                                        require_once 'model/funcaoUsuarioClass.php';
                                                        $funcao = new funcao();
                                                        $resultadoFuncoes = $funcao->listaFuncaoPorIdFuncao(null,null,$_SESSION['funcoes']);
                                                        $arrFuncoesDoUsuario=array();
                                                        $arrFuncoesDoUsuarioRegiao=array();
                                                        if($resultadoFuncoes)
                                                        {
                                                            foreach ($resultadoFuncoes as $fun)
                                                            {
                                                                //Puxar lotação do usuário
                                                                $fu = new funcaoUsuario();
                                                                $siglaOaFuncao="";
                                                                $resultaDadosFuncao = $fu->retornaDadosPorFuncaoPorUsuario($_SESSION['seqCadast'],$fun['idFuncao']);
                                                                if($resultaDadosFuncao)
                                                                {
                                                                    foreach ($resultaDadosFuncao as $v)
                                                                    {
                                                                            $siglaOaFuncao = $v['siglaOA'];
                                                                    }
                                                                }
                                                                echo "[".$fun['vinculoExterno']."] ".$fun['nomeFuncao']." - ".$siglaOaFuncao."<br>";
                                                                $arrFuncoesDoUsuario[] = $fun['vinculoExterno']."@@".$siglaOaFuncao;
                                                                $arrFuncoesDoUsuarioRegiao[] = $fun['vinculoExterno']."@@".substr($siglaOaFuncao,0,3);
                                                            }
                                                        }else{
                                                            echo "--";
                                                        }

                                                        $arrFuncoesImportantesParaAssinatura = array();
                                                        if((in_array("201@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Mestre
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =365;//id do soa
                                                        }
                                                        if((in_array("203@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Secretário
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =367;//id do soa
                                                        }

                                                        if((in_array("211@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Tesoureiro
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =375;//id do soa
                                                        }

                                                        if((in_array("617@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Verificação de Contas
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =533;//id do soa
                                                        }

                                                        //Funções opcionais para assinatura
                                                        if((in_array("605@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Coord. Ogg opcional
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =521;//id do soa
                                                        }
                                                        if((in_array("609@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Coord. Columbas opcional
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =525;//id do soa
                                                        }
                                                        if((in_array("315@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Mestre da Classe de Artesãos
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =407;//id do soa
                                                        }
                                                        if((in_array("151@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)))//Grande Conselheiro
                                                        {
                                                            $arrFuncoesImportantesParaAssinatura[] =329;//id do soa
                                                        }


                                                    ?>
                                                </center>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="alert alert-warning alert-dismissable">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                    <a class="alert-link">Atenção:</a>
                                                    <br />
                                                    Apenas conceda sua assinatura <a class="alert-link">se você revisou o documento</a>. Se você não fez a revisão, <a class="alert-link">clique no mês correspondente ao documento que você quer revisar</a>. Apenas aparecerão abaixo os documentos que estão com o status de <a class="alert-link">"Pronto e Entregue"</a> no sistema. É expressamente <a class="alert-link">proibido emprestar login e senha</a> para assinar eletrônicamente.
                                                </div>
                                                <?php

                                                $temAta = false;
                                                $temRelatorioFinanceiro = false;
                                                $temRelatorioEstatuto = false;
                                                $temRelatorioOgg = false;
                                                $temRelatorioColumba = false;
                                                $temRelatorioClasseArtesaos = false;
                                                $temRelatorioGrandeConselheiro = false;

                                                require_once ('model/ataReuniaoMensalClass.php');
                                                $arm = new ataReuniaoMensal();
                                                $resultado1 = $arm->buscarAtaReuniaoMensalQuePrecisaAssinatura($idOrganismoAfiliado);
                                                $arrAtasAssinadas = $arm->buscarAssinaturasEmAtaReuniaoMensal($_SESSION['seqCadast']);
                                                $x=0;$y=0;
                                                if($resultado1 &&
                                                (in_array("201@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)//Mestre do Organismo
                                                    ||in_array("203@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Secretário do Organismo
                                                )
                                                ) {
                                                    //Verificar se tem ata

                                                    foreach ($resultado1 as $v1) {
                                                        if (!in_array($v1['idAtaReuniaoMensal'], $arrAtasAssinadas)) {
                                                            $temAta = true;
                                                        }
                                                    }
                                                }

                                                require_once ('model/financeiroMensalClass.php');
                                                $fm = new financeiroMensal();
                                                $resultado2 = $fm->listaFinanceiroMensal(null,null,$idOrganismoAfiliado);
                                                //echo "<pre>";print_r($resultado2);
                                                $arrRelatoriosFinanceirosAssinados = $fm->buscarAssinaturasEmFinanceiroMensal($_SESSION['seqCadast']);
                                                $x=0;$y=0;
                                                if($resultado2 &&
                                                (in_array("201@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)//Mestre do Organismo
                                                    ||in_array("203@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Secretário do Organismo
                                                    ||in_array("211@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Tesoureiro
                                                    ||in_array("617@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Comissão Verificação de Contas
                                                )
                                                ) {
                                                    //Verificar se tem relatório financeiro

                                                    foreach ($resultado2 as $v2) {
                                                        if (!in_array($v2['mes'] . "@@" . $v2['ano'] . "@@" . $v2['fk_idOrganismoAfiliado'], $arrRelatoriosFinanceirosAssinados)) {
                                                            $temRelatorioFinanceiro = true;
                                                        }
                                                    }
                                                }

                                                require_once ('model/atividadeEstatutoMensalClass.php');
                                                $aem = new atividadeEstatutoMensal();
                                                $resultado3 = $aem->listaAtividadeMensal(null,null,$idOrganismoAfiliado);
                                                //echo "<pre>";print_r($_SESSION['seqCadast']);
                                                $arrRelatoriosAtividadesAssinados = $aem->buscarAssinaturasEmAtividadeMensal($_SESSION['seqCadast']);
                                                //echo "<pre>";print_r($arrRelatoriosAtividadesAssinados);
                                                $x=0;$y=0;
                                                if($resultado3 &&
                                                    (in_array("201@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)//Mestre do Organismo
                                                        ||in_array("203@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Secretário do Organismo
                                                    )
                                                ) {
                                                    //Verificar se tem relatório financeiro

                                                    foreach ($resultado3 as $v3) {
                                                        if (!in_array($v3['mes'] . "@@" . $v3['ano'] . "@@" . $v3['fk_idOrganismoAfiliado'], $arrRelatoriosAtividadesAssinados)) {
                                                            $temRelatorioEstatuto = true;
                                                        }
                                                    }
                                                }

                                                require_once ('model/oggMensalClass.php');
                                                $om = new oggMensal();
                                                $resultado4 = $om->listaOggMensal(null,null,$idOrganismoAfiliado);
                                                //echo "<pre>";print_r($resultado4);
                                                $arrRelatoriosOggAssinados = $om->buscarAssinaturasEmOggMensal($_SESSION['seqCadast']);
                                                $x=0;$y=0;
                                                if($resultado4 &&
                                                    (in_array("201@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)//Mestre do Organismo
                                                        ||in_array("203@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Secretário do Organismo
                                                        ||in_array("605@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Coordenador OGG
                                                    )
                                                ) {
                                                    //Verificar se tem relatório financeiro

                                                    foreach ($resultado4 as $v4) {
                                                        if (!in_array($v4['mes'] . "@@" . $v4['ano'] . "@@" . $v4['fk_idOrganismoAfiliado'], $arrRelatoriosOggAssinados)) {
                                                            $temRelatorioOgg = true;
                                                        }
                                                    }
                                                }

                                                require_once ('model/columbaTrimestralClass.php');
                                                $ct = new columbaTrimestral();
                                                $resultado5 = $ct->listaColumbaTrimestral(null,null,$idOrganismoAfiliado);
                                                //echo "<pre>";print_r($resultado4);
                                                $arrRelatoriosColumbaAssinados = $ct->buscarAssinaturasEmColumbaTrimestral($_SESSION['seqCadast']);
                                                $x=0;$y=0;
                                                if($resultado5 &&
                                                    (in_array("201@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)//Mestre do Organismo
                                                        ||in_array("203@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Secretário do Organismo
                                                        ||in_array("609@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Coordenador Columba
                                                    )
                                                ) {
                                                    //Verificar se tem relatório financeiro

                                                    foreach ($resultado5 as $v5) {
                                                        if (!in_array($v5['trimestre'] . "@@" . $v5['ano'] . "@@" . $v5['fk_idOrganismoAfiliado'], $arrRelatoriosColumbaAssinados)) {
                                                            $temRelatorioColumba = true;
                                                        }
                                                    }
                                                }

                                                require_once ('model/relatorioClasseArtesaosClass.php');
                                                $rca = new relatorioClasseArtesaos();
                                                $resultado6 = $rca->buscarRelatorioClasseArtesaosQuePrecisaAssinatura($idOrganismoAfiliado);
                                                $arrRelatoriosClasseArtesaosAssinados = $rca->buscarAssinaturasEmRelatorioClasseArtesaos($_SESSION['seqCadast']);
                                                $x=0;$y=0;
                                                if($resultado6 &&
                                                    (in_array("201@@".$siglaOaSelecionada,$arrFuncoesDoUsuario)//Mestre do Organismo
                                                        ||in_array("203@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Secretário do Organismo
                                                        ||in_array("315@@".$siglaOaSelecionada,$arrFuncoesDoUsuario) //Mestre da Classe de Artesãos
                                                        )

                                                ) {
                                                    //Verificar se tem ata

                                                    foreach ($resultado6 as $v6) {
                                                        if (!in_array($v6['idRelatorioClasseArtesaos'], $arrRelatoriosClasseArtesaosAssinados)) {
                                                            $temRelatorioClasseArtesaos = true;
                                                        }
                                                    }
                                                }

                                                require_once ('model/relatorioGrandeConselheiroClass.php');
                                                require_once ('model/regiaoRosacruzClass.php');
                                                $rg =  new regiaoRosacruz();
                                                $rg->setRegiaoRosacruz(substr($siglaOrga,0,3));
                                                $idRegiao = $rg->retornaIdRegiaoRosacruz();

                                                $rgc = new relatorioGrandeConselheiro();
                                                $resultado7 = $rgc->buscarRelatorioGrandeConselheiroQuePrecisaAssinatura($idRegiao);
                                                $arrRelatoriosGrandeConselheiroAssinados = $rgc->buscarAssinaturasEmRelatorioGrandeConselheiro($_SESSION['seqCadast']);
                                                $x=0;$y=0;
                                                if($resultado7 &&
                                                    in_array("151@@".substr($siglaOaSelecionada,0,3),$arrFuncoesDoUsuarioRegiao) //Grande Conselheiro

                                                ) {
                                                    //Verificar se tem ata

                                                    foreach ($resultado7 as $v7) {
                                                        if (!in_array($v7['idRelatorioGrandeConselheiro'], $arrRelatoriosGrandeConselheiroAssinados)) {
                                                            $temRelatorioGrandeConselheiro = true;
                                                        }
                                                    }
                                                }

                                                if($temAta
                                                        ||$temRelatorioFinanceiro
                                                        ||$temRelatorioEstatuto
                                                        ||$temRelatorioOgg
                                                        ||$temRelatorioColumba
                                                        ||$temRelatorioClasseArtesaos
                                                        ||$temRelatorioGrandeConselheiro
                                                ) {
                                                    ?>
                                                    <h3><b>Assinaturas Pendentes</b></h3>
                                                    <div style="text-align: right"><input type="checkbox"
                                                                                          name="checkboxCheckAll"
                                                                                          id="checkboxCheckAll"
                                                                                          onclick="checkAllCheck();">
                                                        <b>Marcar Tudo</b></div>
                                                    <?php
                                                }
                                                        if($temAta){
                                                            ?>
                                                            <hr>
                                                            <h4><i class="fa fa-pencil"></i>&nbsp;*Atas de Reunião
                                                                Mensal</h4>
                                                        <table width="100%">
                                                            <?php
                                                            foreach ($resultado1 as $v1) {
                                                                if (!in_array($v1['idAtaReuniaoMensal'], $arrAtasAssinadas)) {
                                                                    if ($x == 0) {
                                                                        $x++;
                                                                        ?>
                                                                        <tr>
                                                                        <?php
                                                                    }
                                                                    if ($y < 7) {
                                                                        $temAlgoParaAssinar = true;
                                                                        ?>
                                                                        <td><input type="checkbox"
                                                                                   class="checkboxes ataReuniaoMensal"
                                                                                   name="ata_reuniao_mensal[]" id=""
                                                                                   value="<?php echo $v1['idAtaReuniaoMensal']; ?>">
                                                                            <a href="#"
                                                                               onclick="abrirPopUpAtaReuniaoMensal('<?php echo $v1['idAtaReuniaoMensal']; ?>');"><?php echo str_pad($v1['mesCompetencia'], 2, "0", STR_PAD_LEFT);; ?>
                                                                                /<?php echo $v1['anoCompetencia']; ?></a><br>
                                                                        </td>
                                                                        <?php
                                                                        $y++;
                                                                        if($y==6) {
                                                                            $x = 0;
                                                                        }
                                                                    }
                                                                    if ($y == 6) {
                                                                        $y=0;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                }
                                                            } ?>
                                                        </table>
                                                        <?php
                                                    }
                                                    ?>
                                                    <?php
                                                        $x = 0;
                                                        $y = 0;
                                                        if ($temRelatorioFinanceiro) {
                                                            ?>
                                                            <hr>
                                                            <h4><i class="fa fa-pencil"></i>&nbsp;*Relatórios
                                                                Financeiros Mensais</h4>
                                                        <table width="100%">
                                                            <?php
                                                            foreach ($resultado2 as $v2) {
                                                                if (!in_array($v2['mes'] . "@@" . $v2['ano'] . "@@" . $v2['fk_idOrganismoAfiliado'], $arrRelatoriosFinanceirosAssinados)) {
                                                                    if ($x == 0) {
                                                                        $x++;
                                                                        ?>
                                                                        <tr>
                                                                        <?php
                                                                    }
                                                                    if ($y < 7) {
                                                                        $temAlgoParaAssinar = true;
                                                                        ?>
                                                                        <td><input type="checkbox"
                                                                                   class="checkboxes financeiroMensal"
                                                                                   name="relatorio_financeiro_mensal[]"
                                                                                   id=""
                                                                                   value="<?php echo $v2['mes']; ?>@@<?php echo $v2['ano']; ?>@@<?php echo $v2['fk_idOrganismoAfiliado']; ?>">
                                                                            <a href="#"
                                                                               onClick="abrirPopupRelatorioFinanceiroMensalPreview('<?php echo $v2['mes']; ?>','<?php echo $v2['ano']; ?>','<?php echo $idOrganismoAfiliado; ?>','0','0');"><?php echo str_pad($v2['mes'], 2, "0", STR_PAD_LEFT); ?>
                                                                                /<?php echo $v2['ano']; ?></a><br></td>
                                                                        <?php
                                                                        $y++;
                                                                        if($y==6) {
                                                                            $x = 0;
                                                                        }
                                                                    }
                                                                    if ($y == 6) {
                                                                        $y=0;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                }
                                                            } ?>
                                                        </table>
                                                        <?php
                                                        }

                                                    $x = 0;
                                                    $y = 0;
                                                    if ($temRelatorioEstatuto) {
                                                        ?>
                                                        <hr>
                                                        <h4><i class="fa fa-pencil"></i>&nbsp;*Relatórios
                                                            de Acordo de Afiliação Mensais</h4>
                                                        <table width="100%">
                                                            <?php
                                                            foreach ($resultado3 as $v3) {
                                                                if (!in_array($v3['mes'] . "@@" . $v3['ano'] . "@@" . $v3['fk_idOrganismoAfiliado'], $arrRelatoriosAtividadesAssinados)) {
                                                                    if ($x == 0) {
                                                                        $x++;
                                                                        ?>
                                                                        <tr>
                                                                        <?php
                                                                    }
                                                                    if ($y < 7) {
                                                                        $temAlgoParaAssinar = true;

                                                                        /*
                                                                         * Seleção dos rosacruzes ativos no OA
                                                                         */
                                                                        $mesAnterior    = date('m', strtotime('-1 months', strtotime($v3['ano']."-".$v3['mes']."-01")));
                                                                        $anoAnterior    = ($v3['mes'] == 1) ? $v3['ano']-1 : $v3['ano'];

                                                                        $membrosRosacruzesAtivos    = new MembrosRosacruzesAtivos();
                                                                        $membrosAtivosMesAnterior           = $membrosRosacruzesAtivos->retornaMembrosRosacruzesAtivos($mesAnterior, $anoAnterior, $idOrganismoAfiliado);
                                                                        $arr                                = $membrosRosacruzesAtivos->retornaMembrosRosacruzesAtivos($v3['mes'], $v3['ano'], $idOrganismoAfiliado);
                                                                        $numeroMembrosAtivosRelatorio       = ($membrosAtivosMesAnterior['numeroAtualMembrosAtivos'] + $arr['novasAfiliacoes'] + $arr['reintegrados']) - $arr['desligamentos'];
                                                                        ?>
                                                                        <td><input type="checkbox"
                                                                                   class="checkboxes atividadeMensal"
                                                                                   name="relatorio_atividade_mensal[]"
                                                                                   id=""
                                                                                   value="<?php echo $v3['mes']; ?>@@<?php echo $v3['ano']; ?>@@<?php echo $v3['fk_idOrganismoAfiliado']; ?>">
                                                                            <a href="#"
                                                                               onClick="abrirPopupRelatorioMensalAtividade('<?php echo $idOrganismoAfiliado; ?>','<?php echo $v3['mes']; ?>','<?php echo $v3['ano']; ?>','<?php echo $numeroMembrosAtivosRelatorio; ?>','0','0','0');"><?php echo str_pad($v3['mes'], 2, "0", STR_PAD_LEFT); ?>
                                                                                /<?php echo $v3['ano']; ?></a><br></td>
                                                                        <?php
                                                                        $y++;
                                                                        if($y==6) {
                                                                            $x = 0;
                                                                        }
                                                                    }
                                                                    if ($y == 6) {
                                                                        $y=0;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                }
                                                            } ?>
                                                        </table>
                                                        <?php
                                                    }

                                                    $x = 0;
                                                    $y = 0;
                                                    if ($temRelatorioOgg) {
                                                        ?>
                                                        <hr>
                                                        <h4><i class="fa fa-pencil"></i>&nbsp;*Relatórios
                                                            OGG Mensais</h4>
                                                        <table width="100%">
                                                            <?php
                                                            foreach ($resultado4 as $v4) {
                                                                if (!in_array($v4['mes'] . "@@" . $v4['ano'] . "@@" . $v4['fk_idOrganismoAfiliado'], $arrRelatoriosOggAssinados)) {
                                                                    if ($x == 0) {
                                                                        $x++;
                                                                        ?>
                                                                        <tr>
                                                                        <?php
                                                                    }
                                                                    if ($y < 7) {
                                                                        $temAlgoParaAssinar = true;
                                                                        ?>
                                                                        <td><input type="checkbox"
                                                                                   class="checkboxes oggMensal"
                                                                                   name="relatorio_ogg_mensal[]"
                                                                                   id=""
                                                                                   value="<?php echo $v4['mes']; ?>@@<?php echo $v4['ano']; ?>@@<?php echo $v4['fk_idOrganismoAfiliado']; ?>">
                                                                            <a href="#"
                                                                               onClick="abrirPopupRelatorioMensalOGG('<?php echo $idOrganismoAfiliado; ?>',<?php echo $v4['mes']; ?>','<?php echo $v4['ano']; ?>');"><?php echo str_pad($v4['mes'], 2, "0", STR_PAD_LEFT); ?>
                                                                                /<?php echo $v4['ano']; ?></a><br></td>
                                                                        <?php
                                                                        $y++;
                                                                        if($y==6) {
                                                                            $x = 0;
                                                                        }
                                                                    }
                                                                    if ($y == 6) {
                                                                        $y=0;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                }
                                                            } ?>
                                                        </table>
                                                        <?php
                                                    }


                                                    $x = 0;
                                                    $y = 0;
                                                    if ($temRelatorioColumba) {
                                                        ?>
                                                        <hr>
                                                        <h4><i class="fa fa-pencil"></i>&nbsp;*Relatórios
                                                            de Columbas Trimestral</h4>
                                                        <table width="100%">
                                                            <?php
                                                            foreach ($resultado5 as $v5) {
                                                                if (!in_array($v5['trimestre'] . "@@" . $v5['ano'] . "@@" . $v5['fk_idOrganismoAfiliado'], $arrRelatoriosColumbaAssinados)) {
                                                                    if ($x == 0) {
                                                                        $x++;
                                                                        ?>
                                                                        <tr>
                                                                        <?php
                                                                    }
                                                                    if ($y < 7) {
                                                                        $temAlgoParaAssinar = true;
                                                                        ?>
                                                                        <td><input type="checkbox"
                                                                                   class="checkboxes columbaTrimestral"
                                                                                   name="relatorio_columba_trimestral[]"
                                                                                   id=""
                                                                                   value="<?php echo $v5['trimestre']; ?>@@<?php echo $v5['ano']; ?>@@<?php echo $v5['fk_idOrganismoAfiliado']; ?>">
                                                                            <a href="#"
                                                                               onClick="abrirPopupRelatorioTrimestralColumba('<?php echo $v5['trimestre']; ?>','<?php echo $v5['ano']; ?>','<?php echo $idOrganismoAfiliado; ?>');"><?php echo str_pad($v5['trimestre'], 2, "0", STR_PAD_LEFT); ?>
                                                                                /<?php echo $v5['ano']; ?></a><br></td>
                                                                        <?php
                                                                        $y++;
                                                                        if($y==6) {
                                                                            $x = 0;
                                                                        }
                                                                    }
                                                                    if ($y == 6) {
                                                                        $y=0;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                }
                                                            } ?>
                                                        </table>
                                                        <?php
                                                    }

                                                    if($temRelatorioClasseArtesaos){
                                                        ?>
                                                        <hr>
                                                        <h4><i class="fa fa-pencil"></i>&nbsp;*Relatórios da Classe de Artesãos</h4>
                                                        <table width="100%">
                                                            <?php
                                                            foreach ($resultado6 as $v6) {
                                                                if (!in_array($v6['idRelatorioClasseArtesaos'], $arrRelatoriosClasseArtesaosAssinados)) {
                                                                    if ($x == 0) {
                                                                        $x++;
                                                                        ?>
                                                                        <tr>
                                                                        <?php
                                                                    }
                                                                    if ($y < 7) {
                                                                        $temAlgoParaAssinar = true;
                                                                        ?>
                                                                        <td><input type="checkbox"
                                                                                   class="checkboxes relatorioClasseArtesaos"
                                                                                   name="relatorio_classe_artesaos[]" id=""
                                                                                   value="<?php echo $v6['idRelatorioClasseArtesaos']; ?>">
                                                                            <a href="#"
                                                                               onclick="abrirPopUpRelatorioClasseArtesaos('<?php echo $v6['idRelatorioClasseArtesaos']; ?>');"><?php echo str_pad($v6['mesCompetencia'], 2, "0", STR_PAD_LEFT); ?>
                                                                                /<?php echo $v6['anoCompetencia']; ?></a><br>
                                                                        </td>
                                                                        <?php
                                                                        $y++;
                                                                        if($y==6) {
                                                                            $x = 0;
                                                                        }
                                                                    }
                                                                    if ($y == 6) {
                                                                        $y=0;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                }
                                                            } ?>
                                                        </table>
                                                        <?php
                                                    }

                                                    if($temRelatorioGrandeConselheiro){
                                                        ?>
                                                        <hr>
                                                        <h4><i class="fa fa-pencil"></i>&nbsp;*Relatórios dos Grandes Conselheiros Regionais</h4>
                                                        <table width="100%">
                                                            <?php
                                                            foreach ($resultado7 as $v7) {
                                                                if (!in_array($v7['idRelatorioGrandeConselheiro'], $arrRelatoriosGrandeConselheiroAssinados)) {
                                                                    if ($x == 0) {
                                                                        $x++;
                                                                        ?>
                                                                        <tr>
                                                                        <?php
                                                                    }
                                                                    if ($y < 7) {
                                                                        $temAlgoParaAssinar = true;
                                                                        ?>
                                                                        <td><input type="checkbox"
                                                                                   class="checkboxes relatorioGrandeConselheiro"
                                                                                   name="relatorio_grande_conselheiro[]" id=""
                                                                                   value="<?php echo $v7['idRelatorioGrandeConselheiro']; ?>">
                                                                            <a href="#"
                                                                               onclick="abrirPopUpImprimirRelatorioGrandeConselheiro('<?php echo $v7['idRelatorioGrandeConselheiro']; ?>');"><?php echo str_pad($v7['semestre'], 2, "0", STR_PAD_LEFT); ?>
                                                                                /<?php echo $v7['ano']; ?></a><br>
                                                                        </td>
                                                                        <?php
                                                                        $y++;
                                                                        if($y==6) {
                                                                            $x = 0;
                                                                        }
                                                                    }
                                                                    if ($y == 6) {
                                                                        $y=0;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <?php
                                                                }
                                                            } ?>
                                                        </table>
                                                        <?php
                                                    }



                                                    //
                                                if($temAta==false
                                                        &&$temRelatorioFinanceiro==false
                                                        &&$temRelatorioEstatuto==false
                                                        &&$temRelatorioOgg==false
                                                        &&$temRelatorioColumba==false
                                                        &&$temRelatorioClasseArtesaos==false
                                                        &&$temRelatorioGrandeConselheiro==false
                                                ){
                                                    echo "<center>Não existem itens pendentes</center>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <input type="hidden" name="idRegiaoRosacruz" id="idRegiaoRosacruz" value="<?php echo $idRegiao;?>">
                                        <input type="hidden" name="classificacaoOa" id="classificacaoOa" value="<?php echo $idClassificacaoOa;?>">
                                        <input type="hidden" name="funcoesImportantesParaAssinatura" id="funcoesImportantesParaAssinatura" value="<?php echo implode(",",$arrFuncoesImportantesParaAssinatura);?>">
                                        <?php if(($sessao->getValue("fk_idDepartamento") == 3) || ($sessao->getValue("fk_idDepartamento") == 2)){?><a class="btn btn-white" href="?corpo=validarAssinaturaIndividual"><i class="fa fa-check-square"></i>&nbsp;Validar assinatura</a><a class="btn btn-white" href="?corpo=informarCienciaAssinaturaEletronica"><i class="fa fa-info"></i>&nbsp;Ciente</a>&nbsp;<a class="btn btn-white" href="?corpo=validarAssinatura"><i class="fa fa-check-square"></i>&nbsp;Validar documento</a><?php }?>
                                        <a class="btn btn-white" href="?corpo=historicoAssinaturaEletronica"><i class="fa fa-history"></i>&nbsp;Histórico</a>
                                        <a href="#" class="btn btn-white" onclick="return location.href='painelDeControle.php?naoAssinarPorEnquanto=true'"><i class="fa fa-mail-reply"></i>&nbsp;Ver depois</a>
                                        <?php if($temAlgoParaAssinar){?>
                                        <a href="#" class="btn btn-primary" name="assinar" id="assinar" onclick="assinarEletronicamente('<?php echo $_SESSION['seqCadast'];?>','<?php echo $_SERVER['REMOTE_ADDR'];?>','<?php echo $idOrganismoAfiliado;?>')"><i class="fa fa-pencil"></i>&nbsp;Assinar</a>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- http://localhost/soa_mvc/painelDeControle.php?corpo=buscaDepartamento -->
                    <div id="corpo">
                                            <?php
                                            require_once 'model/urlClass.php';

                                            $u = new url();

                                            //echo "corpo: " . $corpo;

                                            include $u->trocaUrl($corpo);
                                            ?>
                    </div>


                    <!-- Rodap? In?cio -->
                    <div class="footer">
                        <div class="pull-right">
                            Desenvolvido pelo <strong>Departamento de TI</strong> [<?php echo getenv('ENVIRONMENT');?>].
                        </div>
                        <div>
                            <strong>Copyright</strong> AMORC-GLP &copy; 2015
                        </div>
                    </div>
                    <!-- Rodap? Fim -->

                </div>
                <!-- Corpo Fim -->

            </div>
            <!-- P?gina Fim -->

            <!-- Mainly scripts -->
            
            <!-- Mainly scripts -->
            <script src="js/jquery-3.1.1.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
            <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
            <script src="js/plugins/jeditable/jquery.jeditable.js"></script>
            
            <!-- Flot -->
           
            <script src="js/plugins/flot/jquery.flot.spline.js"></script>
            <script src="js/plugins/flot/jquery.flot.resize.js"></script>
            <script src="js/plugins/flot/jquery.flot.pie.js"></script>
            <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
            <script src="js/plugins/flot/curvedLines.js"></script>
            
            <!-- Peity -->
            <script src="js/plugins/peity/jquery.peity.min.js"></script>
            <script src="js/demo/peity-demo.js"></script>
            <!-- Custom and plugin javascript -->
            <script src="js/inspinia.js"></script>
            <script src="js/plugins/pace/pace.min.js"></script>
            <!-- jQuery UI -->
            <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>
            <!-- Jvectormap -->
            
            <!-- Data Tables -->
            <script src="js/plugins/dataTables/datatables.min.js"></script>
            <!--
            <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>
            <script src="js/plugins/dataTables/dataTables.responsive.js"></script>
            <script src="js/plugins/dataTables/dataTables.tableTools.min.js"></script>->

            <!--<script src="js/jquery.uploadifive.min.js"></script>-->
            <script src="lib/uploadifive-v1.2.2/jquery.uploadifive.js"></script>

            <!-- Nestable List -->
            <script src="js/plugins/nestable/jquery.nestable.js"></script>

            <script src="js/functions.js"></script>
            <script src="js/functions3.js"></script>
            <script src="js/functions4.js"></script>

            <!-- Chosen -->
            <script src="js/plugins/chosen/chosen.jquery.js"></script>

            <!-- SummerNote -->
            <script src="js/plugins/summernote/summernote.min.js"></script>

            

            <!-- Image cropper -->
            <script src="js/plugins/cropper/cropper.min.js"></script>

            <!-- FooTable -->
            <script src="js/plugins/footable/footable.all.min.js"></script>

            

            <!-- Toastr script -->
            <script src="js/plugins/toastr/toastr.min.js"></script>

            <script src="js/jquery.maskedinput.js" type="text/javascript"></script>
            <script src="js/jquery.maskMoney.js" type="text/javascript"></script>

            <script type="text/javascript" src="js/paginadorPerfilUsuarioContatos.js"></script>

            <!-- iCheck -->
            <script src="js/plugins/iCheck/icheck.min.js"></script>

            <!-- Full Calendar -->
            <script src="js/plugins/fullcalendar/moment.min.js"></script>
            <script src="js/plugins/fullcalendar/fullcalendar.min.js"></script>

            <!-- blueimp gallery -->
            <script src="js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>

            <!-- Multiple file upload plugin -->
            <!-- <script src="js/jquery.uploadify.min.js"></script> -->
            <script src="lib/uploadifive/jquery.uploadifive.min.js"></script>

            <!-- Flot Charts -->
            <script src="js/plugins/flot/jquery.flot.js"></script>
            <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
            <script src="js/plugins/flot/jquery.flot.resize.js"></script>

            
            <!-- aqui -->           
            
            
            
            <script src="js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
            <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
            <!-- Sparkline -->
            <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
            <!-- Sparkline demo data  -->
            <script src="js/demo/sparkline-demo.js"></script>
            <!-- ChartJS-->
            <script src="js/plugins/chartJs/Chart.min.js"></script>

            <!-- Clock Picker -->
            <script src="js/plugins/clockpicker/clockpicker.js"></script>

            <!-- Sweet alert -->
            <script src="js/plugins/sweetalert/sweetalert.min.js"></script>

            <!-- Select2 -->
            <script src="js/plugins/select2/select2.full.min.js"></script>

        <script src="js/plugins/ckeditor/ckeditor.js"></script>
        <script src="js/plugins/ckeditor/samples/js/sample.js"></script>

            
            

            <!-- Steps -->
            <script src="js/plugins/steps/jquery.steps.min.js"></script>

            <script type="text/javascript" src="js/functions2.js"></script>

            <script type="text/javascript" src="js/functions5.js"></script>
            
            
            
            <!-- Data picker -->
            <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>

            

            <!-- Page-Level Scripts -->
            <script>

                                                    $(document).ready(function () {
                                                        //Upload
                                                        $('#file_upload_ticket_anexo').uploadifive({
                                                            'uploadScript': 'js/ajax/uploadTicketAnexo.php',
                                                            'auto': true,
                                                            'width': 200,
                                                            'height': 34,
                                                            'uploadLimit': 50,
                                                            'fileSizeLimit': '10MB',
                                                            'fileTypeExts': '*.jpeg; *.jpg; *.png; *.gif; *.pdf; *.rar; *.zip; *.odt; *.doc; *.docx',
                                                            'buttonText': '<i class="fa fa-upload fa-white"></i> Adicionar Arquivo',
                                                            'onUpload': function (file) {
                                                                var data = {
                                                                    'idTicket': $('#fk_idTicket').val()
                                                                };
                                                                $('#file_upload_ticket_anexo').uploadifive('settings', 'formData', data);
                                                            },
                                                            'onUploadComplete': function (file, data, response) {
                                                                if (response && (data != 1 && escape(data) != '%uFEFF%uFEFF1')) {
                                                                    $("#" + file.id + " .data").html("<font color='red'><br /><br />Erro: " + data + "</font>");
                                                                } else {
                                                                    console.log("Documento Enviado!");
                                                                    getTicketAnexo($('#fk_idTicket').val());
                                                                    cadastraNovaNotificacaoAtualizacao($('#fk_seqCadastRemetente').val(), $('#fk_idTicket').val(), "anexou um documento ao Ticket!")
                                                                }
                                                            },
                                                            'onSWFReady': function () {}
                                                        });
                                                    });
            </script>
            <script>
                <?php if(!isset($_SESSION['naoAssinarPorEnquanto'])&&
                ($temAta==true||$temRelatorioFinanceiro==true||$temRelatorioEstatuto==true)
                ){?>
                $('#mySeeAssinatura').modal({});
                <?php }?>
            </script>
            <?php
//GRÁFICO FINANCEIRO ANUAL
//$anoAtual 				= isset($_REQUEST['anoAtual'])?$_REQUEST['anoAtual']:date('Y');
//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;

    include_once('model/recebimentoClass.php');
    include_once('model/despesaClass.php');
    //include_once('lib/functions.php');

    $r = new Recebimento();
    $d = new Despesa();

//Inicializar vari�veis
    $arrDash = array();
    $arrDash['titulo'] = "Gráfico Financeiro Anual";
    $arrDash['subTitulo'] = "";
    $arrDash['valor'] = "Mês atual: " . mesExtensoPortugues(date('m'));
    $arrDash['cor'] = "navy-bg"; //navy-bg,lazur-bg,yellow-bg 
    $arrDash['nomeGrafico'] = "flot-chart1";
    $arrDash['corGrafico1'] = "#17a084";
    $arrDash['corGrafico2'] = "#127e68";
    $arrDash['tooltip'] = "Gráfico Financeiro Anual";
    $arrDash['link'] = "?corpo=buscaRelatorioAnualFinanceiro";
    $arrDash['d1'] = "";
//Dados do Gráfico
    for ($i = 1; $i <= 12; $i++) {
        if ($r->retornaEntrada($i, $anoAtual, $idOrganismoAfiliado) != false) {
            $arrDash['d1'][$i] = round($r->retornaEntrada($i, $anoAtual, $idOrganismoAfiliado));
        } else {
            $arrDash['d1'][$i] = 0;
        }
    }

    for ($i = 1; $i <= 12; $i++) {
        if ($d->retornaSaida($i, $anoAtual, $idOrganismoAfiliado) != false) {
            $arrDash['d2'][$i] = round($d->retornaSaida($i, $anoAtual, $idOrganismoAfiliado));
        } else {
            $arrDash['d2'][$i] = 0;
        }
    }
//echo "<pre>";print_r($arrDash);
//echo "<pre>";print_r($arrNivel);
//include("templates/ibox6.tpl.php");
    ?>
            <script>
    $(document).ready(function () {


        var d1 = [[1262304000000, <?php echo $arrDash['d1']['1']; ?>], [1264982400000, <?php echo $arrDash['d1']['2']; ?>], [1267401600000, <?php echo $arrDash['d1']['3']; ?>], [1270080000000, <?php echo $arrDash['d1']['4']; ?>], [1272672000000, <?php echo $arrDash['d1']['5']; ?>], [1275350400000, <?php echo $arrDash['d1']['6']; ?>], [1277942400000, <?php echo $arrDash['d1']['7']; ?>], [1280620800000, <?php echo $arrDash['d1']['8']; ?>], [1283299200000, <?php echo $arrDash['d1']['9']; ?>], [1285891200000, <?php echo $arrDash['d1']['10']; ?>], [1288569600000, <?php echo $arrDash['d1']['11']; ?>], [1291161600000, <?php echo $arrDash['d1']['12']; ?>]];
        var d2 = [[1262304000000, <?php echo $arrDash['d2']['1']; ?>], [1264982400000, <?php echo $arrDash['d2']['2']; ?>], [1267401600000, <?php echo $arrDash['d2']['3']; ?>], [1270080000000, <?php echo $arrDash['d2']['4']; ?>], [1272672000000, <?php echo $arrDash['d2']['5']; ?>], [1275350400000, <?php echo $arrDash['d2']['6']; ?>], [1277942400000, <?php echo $arrDash['d2']['7']; ?>], [1280620800000, <?php echo $arrDash['d2']['8']; ?>], [1283299200000, <?php echo $arrDash['d2']['9']; ?>], [1285891200000, <?php echo $arrDash['d2']['10']; ?>], [1288569600000, <?php echo $arrDash['d2']['11']; ?>], [1291161600000, <?php echo $arrDash['d2']['12']; ?>]];

        var data1 = [
            {label: "Data 1", data: d1, color: '<?php echo $arrDash['corGrafico1']; ?>'},
            {label: "Data 2", data: d2, color: '<?php echo $arrDash['corGrafico2']; ?>'}
        ];
        $.plot($("#flot-chart1"), data1, {
            xaxis: {
                tickDecimals: 0
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                                opacity: 1
                            }, {
                                opacity: 1
                            }]
                    }
                },
                points: {
                    width: 0.1,
                    show: false
                }
            },
            grid: {
                show: false,
                borderWidth: 0
            },
            legend: {
                show: false
            }
        });

        var lineData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "Example dataset",
                    backgroundColor: "rgba(26,179,148,0.5)",
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",
                    data: [48, 48, 60, 39, 56, 37, 30]
                },
                {
                    label: "Example dataset",
                    backgroundColor: "rgba(220,220,220,0.5)",
                    borderColor: "rgba(220,220,220,1)",
                    pointBackgroundColor: "rgba(220,220,220,1)",
                    pointBorderColor: "#fff",
                    data: [65, 59, 40, 51, 36, 25, 40]
                }
            ]
        };

        var lineOptions = {
            responsive: true
        };


        var ctx = document.getElementById("lineChart").getContext("2d");
        new Chart(ctx, {type: 'line', data: lineData, options: lineOptions});


    });
</script>
<?php
                    $anoAtual = isset($_REQUEST['anoAtual']) ? $_REQUEST['anoAtual'] : date('Y');
//$idOrganismoAfiliado 	= isset($_SESSION['idOrganismoAfiliado'])?$_SESSION['idOrganismoAfiliado']:null;

                    //include_once('lib/functions.php');

                    $mra = new MembrosRosacruzesAtivos();

//Inicializar vari�veis
                    $arrDash = array();
                    $arrDash['titulo'] = "Gráfico dos Membros Ativos no OA Anual";
                    $arrDash['subTitulo'] = "";
                    $arrDash['valor'] = "Mês atual: " . mesExtensoPortugues(date('m'));
                    $arrDash['cor'] = "lazur-bg"; //navy-bg,lazur-bg,yellow-bg 
                    $arrDash['nomeGrafico'] = "flot-chart2";
                    $arrDash['corGrafico1'] = "#48D1CC";
                    $arrDash['corGrafico2'] = "#19a0a1";
                    $arrDash['tooltip'] = "Gráfico dos Membros Ativos no OA Anual";
                    $arrDash['link'] = "?corpo=buscaRelatorioAnualFinanceiro";

//Dados do Gráfico
                    for ($i = 1; $i <= 12; $i++) {
                        if ($mra->retornaMembrosRosacruzesAtivos($i, $anoAtual, $idOrganismoAfiliado) != "") {
                            $arrM = $mra->retornaMembrosRosacruzesAtivos($i, $anoAtual, $idOrganismoAfiliado);
                            $arrDash['d2'][$i] = $arrM['numeroAtualMembrosAtivos'];
                        } else {
                            $arrDash['d2'][$i] = 0;
                        }
                    }

                    for ($i = 1; $i <= 12; $i++) {
                        $arrDash['d1'][$i] = 0;
                    }

//echo "<pre>";print_r($arrDash);
//include("templates/widget2.tpl.php");
                    ?>
           
                
                <script>
    $(document).ready(function () {


        var dx1 = [[1262304000000, <?php echo $arrDash['d1']['1']; ?>], [1264982400000, <?php echo $arrDash['d1']['2']; ?>], [1267401600000, <?php echo $arrDash['d1']['3']; ?>], [1270080000000, <?php echo $arrDash['d1']['4']; ?>], [1272672000000, <?php echo $arrDash['d1']['5']; ?>], [1275350400000, <?php echo $arrDash['d1']['6']; ?>], [1277942400000, <?php echo $arrDash['d1']['7']; ?>], [1280620800000, <?php echo $arrDash['d1']['8']; ?>], [1283299200000, <?php echo $arrDash['d1']['9']; ?>], [1285891200000, <?php echo $arrDash['d1']['10']; ?>], [1288569600000, <?php echo $arrDash['d1']['11']; ?>], [1291161600000, <?php echo $arrDash['d1']['12']; ?>]];
        var dx2 = [[1262304000000, <?php echo $arrDash['d2']['1']; ?>], [1264982400000, <?php echo $arrDash['d2']['2']; ?>], [1267401600000, <?php echo $arrDash['d2']['3']; ?>], [1270080000000, <?php echo $arrDash['d2']['4']; ?>], [1272672000000, <?php echo $arrDash['d2']['5']; ?>], [1275350400000, <?php echo $arrDash['d2']['6']; ?>], [1277942400000, <?php echo $arrDash['d2']['7']; ?>], [1280620800000, <?php echo $arrDash['d2']['8']; ?>], [1283299200000, <?php echo $arrDash['d2']['9']; ?>], [1285891200000, <?php echo $arrDash['d2']['10']; ?>], [1288569600000, <?php echo $arrDash['d2']['11']; ?>], [1291161600000, <?php echo $arrDash['d2']['12']; ?>]];

        var data1 = [
            {label: "Data 1", data: dx1, color: '<?php echo $arrDash['corGrafico1']; ?>'},
            {label: "Data 2", data: dx2, color: '<?php echo $arrDash['corGrafico2']; ?>'}
        ];
        $.plot($("#flot-chart-dois"), data1, {
            xaxis: {
                tickDecimals: 0
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                                opacity: 1
                            }, {
                                opacity: 1
                            }]
                    }
                },
                points: {
                    width: 0.1,
                    show: false
                }
            },
            grid: {
                show: false,
                borderWidth: 0
            },
            legend: {
                show: false
            }
        });

        var lineData = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "Example dataset",
                    backgroundColor: "rgba(26,179,148,0.5)",
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",
                    data: [48, 48, 60, 39, 56, 37, 30]
                },
                {
                    label: "Example dataset",
                    backgroundColor: "rgba(220,220,220,0.5)",
                    borderColor: "rgba(220,220,220,1)",
                    pointBackgroundColor: "rgba(220,220,220,1)",
                    pointBorderColor: "#fff",
                    data: [65, 59, 40, 51, 36, 25, 40]
                }
            ]
        };

        var lineOptions = {
            responsive: true
        };


        var ctx = document.getElementById("lineChart").getContext("2d");
        new Chart(ctx, {type: 'line', data: lineData, options: lineOptions});


    });
</script>
 <script>
    <?php
    if ($corpo == 'buscaImovel') {
        $resultadoAnexo = $ic->listaImovel($idOrg);
        if ($resultadoAnexo) {
            foreach ($resultadoAnexo as $vetorAnexo) {
                $objImovelAnexoTipo = $iatc->listaImovelAnexoTipo(0);
                if ($objImovelAnexoTipo) {
                    foreach ($objImovelAnexoTipo as $vetorImovelAnexoTipo) {
                        ?>
                                    $(document).ready(function () {
                                        //Upload
                                        $('#file_upload_imovel_anexo' +<?php echo $vetorAnexo['idImovel'] . $vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>).uploadifive({
                                            'formData': {
                                                'idImovel': '<?php echo $vetorAnexo['idImovel']; ?>',
                                                'idImovelAnexoTipo': '<?php echo $vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>'
                                            },
                                            'uploadScript': 'js/ajax/uploadImovelAnexo.php',
                                            'auto': true,
                                            'width': 200,
                                            'height': 34,
                                            'uploadLimit': 50,
                                            'fileSizeLimit': '10MB',
                                            'fileTypeExts': '*.jpeg; *.jpg; *.png; *.gif; *.pdf',
                                            'buttonText': '<i class="fa fa-upload fa-white"></i> Carregar Anexo',
                                            'onUploadComplete': function (file, data, response) {
                                                if (response && (data != 1 && escape(data) != '%uFEFF%uFEFF1')) {
                                                    $("#" + file.id + " .data").html("<font color='red'><br /><br />Erro: " + data + "</font>");
                                                } else {
                                                    getImovelAnexo(<?php echo $vetorAnexo['idImovel']; ?>,<?php echo $vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>);
                                                }
                                            },
                                            'onSWFReady': function () {}
                                        });
                                    });
                        <?php
                    }
                }
            }
        }
    }
    $dataAtual = date('Y');
    $idOrganismoImovelControle = isset($_GET['idOrganismoAfiliado']) ? json_decode($_GET['idOrganismoAfiliado']) : '';
    $data = isset($_GET['referenteAnoImovelControle']) ? json_decode($_GET['referenteAnoImovelControle']) : $dataAtual;
    //echo "corpo: " . $corpo . "|" . "idOrganismoImovelControle: " . $idOrganismoImovelControle;
    if ($corpo == 'buscaImovelControle') {
        $resultadoAnexo = $icc->listaImovelControle($idOrganismoImovelControle, $data);
        if ($resultadoAnexo) {
            foreach ($resultadoAnexo as $vetorAnexo) {
                $objImovelAnexoTipo = $iatc->listaImovelAnexoTipo(1);
                if ($objImovelAnexoTipo) {
                    foreach ($objImovelAnexoTipo as $vetorImovelAnexoTipo) {
                        ?>
                                    $(document).ready(function () {
                                        //Upload
                                        $('#file_upload_imovel_controle_anexo' +<?php echo $vetorAnexo['idImovelControle'] . $vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>).uploadifive({
                                            'formData': {
                                                'idImovelControle': '<?php echo $vetorAnexo['idImovelControle']; ?>',
                                                'idImovelAnexoTipo': '<?php echo $vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>'
                                            },
                                            'uploadScript': 'js/ajax/uploadImovelControleAnexo.php',
                                            'auto': true,
                                            'width': 200,
                                            'height': 34,
                                            'uploadLimit': 50,
                                            'fileSizeLimit': '10MB',
                                            'fileTypeExts': '*.jpeg; *.jpg; *.png; *.gif; *.pdf',
                                            'buttonText': '<i class="fa fa-upload fa-white"></i> Carregar Anexo',
                                            'onUploadComplete': function (file, data, response) {
                                                if (response && (data != 1 && escape(data) != '%uFEFF%uFEFF1')) {
                                                    $("#" + file.id + " .data").html("<font color='red'><br /><br />Erro: " + data + "</font>");
                                                } else {
                                                    getImovelControleAnexo(<?php echo $vetorAnexo['idImovelControle']; ?>,<?php echo $vetorImovelAnexoTipo['idImovelAnexoTipo']; ?>);
                                                }
                                            },
                                            'onSWFReady': function () {}
                                        });
                                    });
                        <?php
                    }
                }
            }
        }
    }
    ?>


    <?php if ($corpo == 'muralDigital') { ?>

                    var pagina = 1;   //página inicial
                    var numitens = 5; //quantidade de itens a ser mostrado por página
                    var exibidoPara = $('#exibicaoPublicacaoMural').val();

                    $(document).ready(function () {
                        listaPublicacoesMural(1, pagina, numitens);
                    });

        <?php $timestamp = time(); ?>
                    $(function () {
                        $('#imagemPublicacaoMural').uploadifive({

                            //'buttonClass'      : 'btn btn-primary',
                            //'buttonText'       : '<i class="fa fa-upload fa-white"></i> Carregar Anexo',
                            'width': 150,
                            'fileSizeLimit': '10MB',
                            //'uploadLimit'      : 1,
                            'multi': false,
                            'auto': false,
                            'checkScript': 'lib/check-exists.php',
                            'formData': {
                                'timestamp': '<?php echo $timestamp; ?>',
                                'token': '<?php echo md5('unique_salt' . $timestamp); ?>'
                            },
                            'queueID': 'queue',
                            'uploadScript': 'js/ajax/uploadPublicacaoMuralImagemUploadFive.php',
                            'onUploadComplete': function (file, data) {
                                console.log('onUploadComplete');
                                console.log(data);
                                if ($.isNumeric(data)) {
                                    $("#fk_idMuralImagem").val(data);

                                    console.log("fk_idMuralImagem antes de cadastraPublicacaoMural: " + $("#fk_idMuralImagem").val());
                                    cadastraPublicacaoMural()
                                } else {
                                    console.log(data);
                                    //cadastraPublicacaoMural();
                                }
                            },
                            'onAddQueueItem': function (file) {
                                console.log('onAddQueueItem');
                                $('#imagemPublicacaoMural').uploadifive('clearQueue');
                                console.log('The file ' + file.name + ' was added to the queue!');
                                $("#imagemCarregada").val(1);
                            },
                            'onCancel': function () {
                                console.log('onCancel');
                                $("#fk_idMuralImagem").val(0);
                                $("#imagemCarregada").val(0);
                            },
                            'onError': function (errorType) {
                                console.log('The error was: ' + errorType);
                            }
                        });
                    });

                    $(function () {
                        $('#imagemPublicacaoMuralAlterar').uploadifive({
                            'buttonClass': 'btn btn-primary',
                            'buttonText': '<i class="fa fa-upload fa-white"></i> Carregar Anexo',
                            'fileSizeLimit': '10MB',
                            'multi': false,
                            'auto': false,
                            'checkScript': 'lib/check-exists.php',
                            'queueID': 'queueAlterar',
                            'uploadScript': 'js/ajax/uploadPublicacaoMuralImagemUploadFive.php',
                            'onUploadComplete': function (file, data) {
                                if ($.isNumeric(data)) {
                                    alteraImagemPublicacaoMural(data)
                                    $('#imagemPublicacaoMuralAlterar').uploadifive('clearQueue');
                                } else {
                                    console.log(data);
                                }
                            },
                            'onAddQueueItem': function (file) {
                                $('#imagemPublicacaoMuralAlterar').uploadifive('clearQueue');
                                console.log('The file ' + file.name + ' was added to the queue!');
                            },
                            'onCancel': function () {

                            },
                            'onError': function (errorType) {
                                console.log('The error was: ' + errorType);
                            }
                        });
                    });

    <?php } ?>

    <?php if ($corpo == 'muralDigitalAdmin') { ?>

                    var pagina = 1;   //página inicial
                    var numitens = 150; //quantidade de itens a ser mostrado por página

                    $(document).ready(function () {
                        listaPublicacoesMuralAdmin(pagina, numitens);
                    });

    <?php } ?>
            </script>

            <script type="text/javascript">
                $(document).ready(function () {
                    $(".select2").select2();

                     
                        $('.dataTables-example').DataTable({
                            pageLength: 25,
                            responsive: true,
                            dom: '<"html5buttons"B>lTfgitp',
                            buttons: [
                                { extend: 'copy'},
                                {extend: 'csv'},
                                {extend: 'excel', title: 'ExampleFile'},
                                {extend: 'pdf', title: 'ExampleFile'},

                                {extend: 'print',
                                 customize: function (win){
                                        $(win.document.body).addClass('white-bg');
                                        $(win.document.body).css('font-size', '10px');

                                        $(win.document.body).find('table')
                                                .addClass('compact')
                                                .css('font-size', 'inherit');
                                }
                                }
                            ]

                        });

                   

    

                    /* Init DataTables */
                   // var oTable = $('#editable').dataTable();

                    /* Apply the jEditable handlers to the table */
                    /*
                    oTable.$('td').editable('../example_ajax.php', {
                        "callback": function (sValue, y) {
                            var aPos = oTable.fnGetPosition(this);
                            oTable.fnUpdate(sValue, aPos[0], aPos[1]);
                        },
                        "submitdata": function (value, settings) {
                            return {
                                "row_id": this.parentNode.getAttribute('id'),
                                "column": oTable.fnGetPosition(this)[2]
                            };
                        },
                        "width": "90%",
                        "height": "100%"
                    });*/


                });
            </script>
            <script>
                $(document).on('ready', function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
                $(document).on('ready', document, function () {
                    $('[rel="popover"]').data('popover')
                });
                $(document).on('ready', document, function () {
                    $('.clockpicker').clockpicker();
                });

            </script>
            <style>
                body.DTTT_Print {
                    background: #fff;

                }
                .DTTT_Print #page-wrapper {
                    margin: 0;
                    background:#fff;
                }

                button.DTTT_button, div.DTTT_button, a.DTTT_button {
                    border: 1px solid #e7eaec;
                    background: #fff;
                    color: #676a6c;
                    box-shadow: none;
                    padding: 6px 8px;
                }
                button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
                    border: 1px solid #d2d2d2;
                    background: #fff;
                    color: #676a6c;
                    box-shadow: none;
                    padding: 6px 8px;
                }

                .dataTables_filter label {
                    margin-right: 5px;
                }
            </style>
            <script>removerBugMenu();</script>
        </body>
    </html>
    <?php
    require_once 'controller/logController.php';
    $registro = new LogSistemaController();
    $registro->Registro();
} else {
    echo " Acesso negado ao Sistema";
}
?>
