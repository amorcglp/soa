<?php
/***
Conexão com o ORCZ
***/

class conexaoSOAORCZ {

    private $host = "192.1.1.147\ORCZMIG";
    private $user = "ti";
    private $password = "@Cpdadmin@";
    private $port = "14335";
    private $con = NULL;
    public $sth = NULL;
    private $dbname = "amorc_teste4";

    public function openSOA() {

        $this->con = new PDO("dblib:host=".$this->host.",".$this->port.";dbname=".$this->dbname, "$this->user",  "$this->password");

        return $this->con;
    }

    public function close() {

        $this->con = NULL;
    }

    public function checkStatus() {

        if (!$this->con) {

            echo "<h3> O sistema não está conectado a [$this->dbname] em [$this->host]</h3>";
        } else {

            echo "<h3> O sistema está conectado a [$this->dbname] em [$this->host]</h3>";
        }
    }
    /**
     * Método que executa a query retornando resultados ou booleano
     * @param type $sth
     * @param type $bool
     * @return boolean, result
     */

    public function run($sth,$bool=null,$total=null,$verifica=null){

        try {
            if($sth->execute())
            {
                $resultado = $sth->fetchAll();
                $this->close();
                //print_r($sth);
                if($bool==null&&$total==null&&$verifica==null)
                {
                    return $resultado;
                }else{
                    if($bool!=null&&$bool!=false)
                    {
                        return true;
                    }
                    if($total!=null&&$total!=false)
                    {
                        return count($resultado);
                    }
                    if($verifica!=null&&$verifica!=false)
                    {
                        $totalRows = count($resultado);
                        if($totalRows>0)
                        {
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
            }else{
                echo "<br>===================================================";
                echo "<br>Erro do RUN:<br>";
                var_dump($sth);
                $sth->errorInfo();
                $this->close();
                return false;
            }
        } catch (PDOException $e) {

        }
    }

    /**
    $data = array(
        'fk_idTicket'=>$fk_idTicket,
        'fk_idDepartamento'=>$fk_idDepartamento
    );
    //echo "QUERY: " . $this->parms("classe:metodo",$sql,$data);

     */
    function parms($class,$string,$data) {
        $indexed=$data==array_values($data);
        foreach($data as $k=>$v) {
            if(is_string($v)) $v="$v";
            if($indexed) $string=preg_replace('/\?/',$v,$string,1);
            else $string=str_replace(":$k",$v,$string);
        }
        return "[".$class.": " . $string."]<br>";
    }

}
?>?>