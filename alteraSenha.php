<!DOCTYPE html>
<?php
if(!$_SERVER['HTTPS']) {
    header( "Location: "."https://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
}
//Verificar se token está espirado
require_once 'model/usuarioClass.php';
$u = new Usuario();
$resultado = $u->retornaToken($_GET['tk']);
$time=0;
if($resultado)
{  
    //echo "<pre>";print_r($resultado);
    foreach($resultado as $vetor)
    {
        $time = $vetor['timelife'];
    }
}else{
    echo "Erro. Favor avisar equipe de TI";
}
//echo "time: ".$time;
if($time!=0)
{
    $next =  $time + (30 * 60);//30 minutos
}else{
    $next=0;
}    
//echo "next: ".$next;
  
session_start();
$arrData = array();
$arrData['key'] = 777;
$logged=true;
require_once ('sec/make.php');
$_SESSION['tk']= $token;
$_SESSION['vk']= $arrData['key'];
    ?>
    <html>

        <head>

            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <title>SOA | Alterar a senha</title>
            <link href="img/icon_page.png" rel="shortcut icon">
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

            <!-- Toastr style -->
            <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

            <!-- Sweet Alert -->
            <link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

            <link href="css/animate.css" rel="stylesheet">
            <link href="css/style.css" rel="stylesheet">

            <!-- Loading Ajax -->
            <link href="css/loadingajax.css" rel="stylesheet">

        </head>

        <body class="gray-bg">

            <div class="middle-box text-center loginscreen  animated fadeInDown">
                <div>
                    <br>
                    <div>
                        <img src="img/solado.png">
                        <!--<h1 class="logo-name">SOA</h1>-->
                    </div>
                    <br>
                    <h3>SOA - Sistema de Organismos Afiliados</h3>
                    <?php
                    if($next!=0)
                    {
                    ?>    
                    <p>Alteração da Senha</p>
                    <form class="m-t" role="form" action="" method="post">
                        <?php     
                            if($next>=time())
                            {
                            ?>
                            <div class="form-group">
                                <input type="password" name="novaSenha" id="novaSenha" class="form-control" maxlength="8" placeholder="NOVA SENHA - MÁXIMO 8 CARACTERES" required="">
                            </div>
                            <div class="form-group">
                                <input type="password" name="novaSenha2" id="novaSenha2" class="form-control"  maxlength="8" placeholder="DIGITE NOVAMENTE A NOVA SENHA" required="">
                            </div>
                            <input type="hidden" id="t" name="t" value="<?php echo $_GET['tk']; ?>" />
                            <button type="button" id="alterar" class="btn btn-primary block full-width m-b">Alterar</button>
                            <?php
                            }else{
                            ?>
                            O Token foi expirado. Clique <a href="esqueciSenha.php">aqui</a> e faça uma nova solicitação.
                            <?php
                            }
                            if($next>=time())
                            {
                        ?>
                    </form>
                    <a class="btn btn-sm btn-white btn-block" href="login.php">Login</a>
                    <p class="m-t"> <small>Sistema de Organismos Afiliados AMORC-GLP &copy; 2015</small> </p>
                    <?php
                            }
                    }else{
                            echo "Acesso Negado!";
                        }
                    ?>
                </div>
            </div>

            <!-- Mainly scripts -->
            <script src="js/jquery-2.1.1.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
            <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

            <!-- Custom and plugin javascript -->
            <script src="js/inspinia.js"></script>
            <script src="js/plugins/pace/pace.min.js"></script>

            <script src="js/functions.js"></script>

            <!-- Toastr script -->
            <script src="js/plugins/toastr/toastr.min.js"></script>

            <script src="js/jquery.maskedinput.js" type="text/javascript"></script>

            <!-- Sweet alert -->
            <script src="js/plugins/sweetalert/sweetalert.min.js"></script>

            <script type="text/javascript">
                $(function () {
                    $('#alterar').click(function () {

                        if ($('#novaSenha').val() == "")
                        {
                            alert('Preencha o campo nova senha');
                        } else if ($('#novaSenha2').val() == "")
                        {
                            alert('Preencha o campo nova senha novamente');
                        } else if ($('#novaSenha2').val() != $('#novaSenha').val())
                        {
                            alert('As senhas não conferem digite novamente');
                        } else {
                            //Chamar função para alterar a senha
                            alterarSenha();
                        }
                    });
                })
            </script>
        </body>

    </html>
