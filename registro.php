<!DOCTYPE html>
<?php
if(!$_SERVER['HTTPS']) {
    header( "Location: "."https://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
}
session_start();
$arrData = array();
$arrData['key'] = 777;
$logged=true;
require_once ('sec/make.php');
$_SESSION['tk']= $token;
$_SESSION['vk']= $arrData['key'];
?>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SOA | Cadastro</title>
        <link href="img/icon_page.png" rel="shortcut icon">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Toastr style -->
    	<link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">
    	
    	<!-- Sweet Alert -->
    	<link href="css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

        <link href="css/animate.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        
        <!-- Loading Ajax -->
    	<link href="css/loadingajax.css" rel="stylesheet">

    </head>

    <body class="gray-bg">

        <div class="middle-box text-center loginscreen  animated fadeInDown">
            <div>
                <br>
                <div>
                    <img src="img/solado.png">
                    <!--<h1 class="logo-name">SOA</h1>-->
                </div>
                <br>
                <h3>SOA - Sistema de Organismos Afiliados</h3>
                <p>Cadastro</p>
                <center>
                <div class="cssload-box-loading" id="loadingAjax" style="margin: 30% 50% 50% 50%;display:none">
				</div>
				</center>
                <form class="m-t" role="form" action="" method="post">
                    <div class="form-group">
                        <input type="text" name="nomeUsuario" id="nomeUsuario" class="form-control" placeholder="NOME" required="" onChange="this.value = this.value.toUpperCase()" onblur="geraLoginUsuario()">
                    </div>
                    <div class="form-group">
                        <input type="text" name="codigoAfiliacaoUsuario" id="codigoAfiliacaoUsuario" class="form-control" placeholder="CÓD. DE AFILIAÇÃO - APENAS NÚMEROS" required="" onkeypress="return SomenteNumero(event)" onblur="retornaDados()">
                    </div>
                    <div class="form-group">
                        <input type="text" name="telefoneResidencialUsuario" id="telefoneResidencialUsuario" class="form-control" placeholder="(DDD) TELEFONE RESIDENCIAL" required="">
                    </div>
                    <div class="form-group">
                        <input type="tel" name="celularUsuario" id="celularUsuario" class="form-control" placeholder="(DDD) CELULAR" required="">
                    </div>
                    <div class="form-group">
                        <input type="text" name="telefoneComercialUsuario" id="telefoneComercialUsuario" class="form-control" placeholder="(DDD) TELEFONE COMERCIAL" required="">
                    </div>
                    <div class="form-group">
                        <input type="text" name="emailUsuario" id="emailUsuario" class="form-control" placeholder="E-MAIL" required="" onChange="this.value = this.value.toLowerCase()">
                    </div>
                    
                    <input type="hidden" name="jaCadastrado" id="jaCadastrado">
                    <input type="hidden" name="seq_cadast" id="seq_cadast">
                    <input type="hidden" name="siglaPais" id="siglaPais">
                    <input type="hidden" name="loginUsuario" id="loginUsuario">
                    <input type="hidden" name="senhaUsuario" id="senhaUsuario">
                    <input type="hidden" name="regiaoUsuario" id="regiaoUsuario">
                    <input type="hidden" name="siglaOA" id="siglaOA" value="PR113">
                    <input type="hidden" name="totalFuncoes" id="totalFuncoes" value="0">
                    <input type="hidden" name="verificaKey" id="verificaKey" value="<?php echo $arrData['key'];?>">
                    
                    <button type="button" id="cadastrar" class="btn btn-primary block full-width m-b">Cadastrar</button>
                </form>
                <a class="btn btn-sm btn-white btn-block" href="login.php">Login</a>
                <p class="m-t"> <small>Sistema de Organismos Afiliados AMORC-GLP &copy; 2015</small> </p>
            </div>
        </div>
        
		            
        <!-- Mainly scripts -->
	    <script src="js/jquery-2.1.1.js"></script>
	    <script src="js/bootstrap.min.js"></script>
	    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
	    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	
	    <!-- Custom and plugin javascript -->
	    <script src="js/inspinia.js"></script>
	    <script src="js/plugins/pace/pace.min.js"></script>

		<script src="js/functions.js"></script>
		
		<!-- Toastr script -->
	    <script src="js/plugins/toastr/toastr.min.js"></script>
	
		<script src="js/jquery.maskedinput.js" type="text/javascript"></script>
		
		<!-- Sweet alert -->
    	<script src="js/plugins/sweetalert/sweetalert.min.js"></script>
		
	    <script type="text/javascript">
	        $(function () {
				//Máscaras
	        	$("#telefoneResidencialUsuario").mask("(999) 9999-9999");
	        	$("#telefoneComercialUsuario").mask("(999) 9999-9999");
	        	//$("#celularUsuario").mask("(999) 9999-9999");

	        	jQuery('input[type=tel]').mask("(999) 9999-9999?9").ready(function(event) {
	                var target, phone, element;
	                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
	                phone = target.value.replace(/\D/g, '');
	                element = $(target);
	                element.unmask();
	                if(phone.length > 10) {
	                    element.mask("(999) 99999-999?9");
	                } else {
	                    element.mask("(999) 9999-9999?9");
	                }
	            });
	        	
	            var i = -1;
	            var toastCount = 0;
	            var $toastlast;
	            var getMessage = function () {
	                var msg = 'Hi, welcome to Inspinia. This is example of Toastr notification box.';
	                return msg;
	            };

	            toastr.options = {
						  "closeButton": true,
						  "debug": false,
						  "progressBar": false,
						  "positionClass": "toast-top-center",
						  "onclick": null,
						  "showDuration": "400",
						  "hideDuration": "1000000",
						  "timeOut": "7000000",
						  "extendedTimeOut": "1000000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						}
	
	            $('#cadastrar').click(function (){
		            if($('#nomeUsuario').val()=="")
		            {
		            	swal({title:"Aviso!",text:"Preencha o campo nome",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
		            }else if($('#codigoAfiliacaoUsuario').val()=="")
		            {
		            	swal({title:"Aviso!",text:"Preencha o campo código de afiliação",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
		            }else if($('#telefoneResidencialUsuario').val()==""&&$('#celularUsuario').val()==""&&$('#telefoneComercialUsuario').val()=="")
		            {
		            	swal({title:"Aviso!",text:"Preencha pelo menos 1 telefone",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
		            }else if($('#emailUsuario').val()=="")
		            {
		            	swal({title:"Aviso!",text:"Preencha o campo e-mail. Caso não tenha, crie um no GMAIL acesse https://accounts.google.com/SignUp?service=mail&hl=pt_br",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
		            }else if(!validaEmail($('#emailUsuario').val()))
			        {
		            	swal({title:"Aviso!",text:"Preencha corretamente o campo e-mail",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
			        }else if($('#loginUsuario').val()=="")
			        {
			        	swal({title:"Aviso!",text:"Impossível gerar o login. Entre em contato com o setor de Organismos orgafil3@amorc.org.br",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
			        }else if($('#seq_cadast').val()=="0"||$('#seq_cadast').val()=="")
			        {
			        	swal({title:"Aviso!",text:"Não encontramos seu cadastro no sistema interno. Se seu primeiro nome possui acento, tente cadastrar sem. Se mesmo assim persistirem os problemas entre em contato com o setor de Organismos Afiliados",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
			        }else if($('#jaCadastrado').val()=="1")
			        {
			        	swal({title:"Aviso!",text:"Você já foi cadastrado. Entre em contato com o setor de Organismos Afiliados para saber seu login e sua senha",type:"warning",confirmButtonColor:"#1ab394"});
		            	return false;
			        }else{
		                if($('#totalFuncoes').val()==0)
                        {
                            swal({title:"Aviso!",text:"Para se cadastrar no SOA é necessário que você seja um oficial credenciado, e que estas informações já tenha sido registradas e atualizadas na GLP, desta forma você poderá se cadastrar no SOA através do [cadastre-se aqui]",type:"warning",confirmButtonColor:"#1ab394"});
                            return false;
                        }else {
                            // Chamar o cadastro de usuários
                            cadastraUsuario();
                            // Se tiver funções então setar permissao
                            if ($('#totalFuncoes').val() > 0) {
                                setarPermissaoMembro();
                            }

                            // Mostrar mensagem de sucesso
                            swal({
                                title: "Sucesso!",
                                text: 'Anote seu login e sua senha. Seu login é ' + $('#loginUsuario').val() + ' - Sua senha é ' + $('#senhaUsuario').val() + ' Clique em login logo abaixo e efetue seu acesso',
                                type: "success",
                                confirmButtonColor: "#1ab394"
                            });

                            // Limpar campos
                            $('#nomeUsuario').val("");
                            $('#codigoAfiliacaoUsuario').val("");
                            $('#telefoneResidencialUsuario').val("");
                            $('#celularUsuario').val("");
                            $('#telefoneComercialUsuario').val("");
                            $('#emailUsuario').val("");
                            $('#loginUsuario').val("");
                            $('#senhaUsuario').val("");
                            $('#seq_cadast').val("");
                            $('#totalFuncoes').val(0);
                            $('#regiaoUsuario').val("");
                            $('#siglaOA').val("PR113");
                        }
			        }
	            });
	            function getLastToast(){
	                return $toastlast;
	            }
	            $('#clearlasttoast').click(function () {
	                toastr.clear(getLastToast());
	            });
	            $('#cleartoasts').click(function () {
	                toastr.clear();
	            });
                    
                    document.getElementById("nomeUsuario").onkeypress = function(e) {
                        var chr = String.fromCharCode(e.which);
                        if ("qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM ".indexOf(chr) < 0)
                          return false;
                      };
	        })
	    </script>
    </body>

</html>